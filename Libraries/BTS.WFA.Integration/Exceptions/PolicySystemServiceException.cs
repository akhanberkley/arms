﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.Integration
{
    class PolicySystemServiceException : Exception
    {
        public PolicySystemServiceException(string strText, Exception innerException)
            : base(strText, innerException)
        {
        }

        public PolicySystemServiceException(string strText)
            : base(strText)
        {
        }
    
    }
}
