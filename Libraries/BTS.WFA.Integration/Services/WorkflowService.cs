﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using BTS.WFA.Integration.Models;
using System.Xml.XPath;
using System.Xml;

namespace BTS.WFA.Integration.Services
{
    public class WorkflowService : BaseService
    {
        public static WorkflowCreateResult CreateInstance(WorkflowSystemDetails settings, WorkflowItem item)
        {
            var results = new WorkflowCreateResult();
            try
            {
                using (var service = CreateWebServiceClient<BPMService.DWImportSoapClient, BPMService.DWImportSoap>(settings.BpmUrl, 30))
                {
                    string shortdate = DateTime.Now.ToString("yyyy-MM-dd");
                    string longdate = DateTime.Now.ToString("s");
                    XNamespace ns = "http://intranet.bts.wrbc/DWServices/DWImport";
                    XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";

                    var contactNode = new XElement(ns + "Contact", new XAttribute("Name", settings.ContactName),
                                                                   new XAttribute("Email", settings.ContactEmail),
                                                                   new XAttribute("Phone", ""));


                    var schemaLocation = GetSchemaUrl(service, settings.CompanyNumber, settings.FilenetDocClass);
                    var importNode = new XElement(ns + "VA3iImport", new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                                                                     new XAttribute(xsi + "schemaLocation", schemaLocation),
                                                                     new XAttribute("DocCreateDate", shortdate),
                                                                     new XAttribute("DocClass", settings.FilenetDocClass),
                                                                     new XAttribute("CompanyNumber", settings.CompanyNumber));

                    var requestInfoNode = new XElement(ns + "RequestInfo", new XAttribute("RequestCreateDate", shortdate),
                                                                           new XElement(ns + "Requestor", contactNode),
                                                                           new XElement(ns + "NotificationSuccess", new XAttribute("RequestNotification", "0"), contactNode),
                                                                           new XElement(ns + "NotificationFail", new XElement(ns + "BusinessContact", contactNode),
                                                                                                                 new XElement(ns + "TechnicalContact", contactNode)),
                                                                           new XElement(ns + "RequestCreateType", new XElement(ns + "ApplicationName", settings.ApplicationName)));
                    importNode.Add(requestInfoNode);

                    var batchNode = new XElement(ns + "BatchInfo");
                    importNode.Add(batchNode);

                    var transactionNode = new XElement(ns + "Transaction");
                    batchNode.Add(transactionNode);
                    var tranAttributes = new XElement(ns + "TranAttributes");
                    transactionNode.Add(tranAttributes);

                    var policyAttrData = new XElement(ns + "PolicyAttrData");
                    tranAttributes.Add(policyAttrData);
                    if (!String.IsNullOrEmpty(item.PolicyNumber))
                        policyAttrData.Add(new XAttribute("PolicyNumber", item.PolicyNumber));
                    if (item.PolicyEffectiveDate != null)
                        policyAttrData.Add(new XAttribute("EffectiveDate", item.PolicyEffectiveDate.Value.ToString("yyyy-MM-dd")));
                    if (item.PolicyExpirationDate != null)
                        policyAttrData.Add(new XAttribute("ExpDate", item.PolicyExpirationDate.Value.ToString("yyyy-MM-dd")));
                    if (!String.IsNullOrEmpty(item.InsuredName))
                        policyAttrData.Add(new XAttribute("InsuredName", item.InsuredName));
                    if (!String.IsNullOrEmpty(item.Account))
                        policyAttrData.Add(new XAttribute("Account", item.Account));
                    if (!String.IsNullOrEmpty(item.AgencyNumber))
                        policyAttrData.Add(new XAttribute("AgencyNumber", item.AgencyNumber));
                    if (!String.IsNullOrEmpty(item.AgencyName))
                        policyAttrData.Add(new XAttribute("AgencyName", item.AgencyName));
                    if (!String.IsNullOrEmpty(item.SubmissionNumber))
                        policyAttrData.Add(new XAttribute("SubmissionNum", item.SubmissionNumber));

                    var tranAttrData = new XElement(ns + "TranAttrData", new XAttribute("TranType", item.TransactionType));
                    tranAttributes.Add(tranAttrData);

                    if (settings.BpmVersion == 6)
                    {
                        tranAttrData.Add(new XAttribute("ProcessType", item.ProcessType));
                        tranAttrData.Add(new XAttribute("ProcessStatus", item.Status));
                    }
                    else
                        tranAttrData.Add(new XAttribute("Status", item.Status));

                    if (item.Note != null)
                        transactionNode.Add(new XElement(ns + "NoteRecord", new XAttribute("NoteDateTime", longdate), new XAttribute("NoteType", "UNW"), new XAttribute("NoteScope", item.Note.Scope), new XAttribute("NoteUser", item.Note.User), new XAttribute("NoteValue", item.Note.Text)));

                    if (item.File != null)
                    {
                        var documentRecord = new XElement(ns + "DocumentRecord", new XAttribute("FileExtension", item.File.Extension), new XAttribute("Primary", "1"), new XAttribute("Zipped", "0"), new XAttribute(xsi + "type", "Internal"));
                        transactionNode.Add(documentRecord);

                        var docAttrData = new XElement(ns + "DocAttrData", new XAttribute("DocType", item.File.DocumentType));
                        documentRecord.Add(docAttrData);
                        if(!String.IsNullOrEmpty(item.File.Remarks))
                            docAttrData.Add(new XAttribute("DocRemarks", item.File.Remarks));

                        documentRecord.Add(new XElement(ns + "DocumentData", Convert.ToBase64String(item.File.Data)));
                    }

                    var xmlDoc = new XmlDocument();
                    using (XmlReader xmlReader = importNode.CreateReader())
                        xmlDoc.Load(xmlReader);

                    string idSentence = service.Submit(xmlDoc);

                    int startPoint = idSentence.IndexOf("#") + 1;
                    results.InstanceId = idSentence.Substring(startPoint, idSentence.IndexOf(":") - startPoint);
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }

        private static string GetSchemaUrl(BPMService.DWImportSoapClient service, string companyNumber, string docClass)
        {
            string cacheKey = "BPMSchema" + companyNumber + docClass;
            string url = GetCacheItem(cacheKey) as string;
            if (url == null)
            {
                url = service.GetSchemaURL(companyNumber, docClass);
                SetCacheItem(cacheKey, url, new TimeSpan(1, 0, 0));
            }

            return url;
        }
    }
}
