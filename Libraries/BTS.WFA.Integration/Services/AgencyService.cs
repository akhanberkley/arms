﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using BTS.WFA.Integration.Models;
using System.Xml.XPath;

namespace BTS.WFA.Integration.Services
{
    public class AgencyService : BaseService
    {
        public static AllAgenciesResult GetAllAgencies(string apsUrl)
        {
            var results = new AllAgenciesResult();
            try
            {
                using (var service = CreateWebServiceClient<APSService.APSServiceClient, APSService.APSService>(apsUrl, 30))
                {
                    //APS explodes if we try to grab them all at once
                    int agenciesPerPage = 50;
                    int page = 0;
                    while (true)
                    {
                        var agencies = service.getAllAgenciesPaged(page * agenciesPerPage, agenciesPerPage);
                        if (agencies != null)
                            results.AgencyCodes.AddRange(agencies.Select(a => a.agencyCode));

                        if (agencies == null || agencies.Length != agenciesPerPage)
                            break;
                        else
                            page++;
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }

        public static AgencySearchResult GetAgency(string apsUrl, string agencyCode, bool useParentsLicensedStates, IEnumerable<string> personnelRoles)
        {
            var results = new AgencySearchResult();
            try
            {
                using (var service = CreateWebServiceClient<APSService.APSServiceClient, APSService.APSService>(apsUrl, 60))
                {
                    var agency = service.getAgencyWithPersonnelByCode(agencyCode);
                    //If we found the agency and it's not marked as a "Group"
                    if (agency != null && (agency.agencyGroupType == null || !String.Equals(agency.agencyGroupType.description, "Group", StringComparison.CurrentCultureIgnoreCase)))
                    {
                        DateTime tempDate; decimal tempDecimal;
                        var a = results.Agency = new Agency();

                        a.Id = agency.id;
                        a.AgencyCode = agency.agencyCode;
                        a.AgencyName = (!String.IsNullOrEmpty(agency.dbaName)) ? agency.dbaName : ((!String.IsNullOrEmpty(agency.legalName)) ? agency.legalName : agency.shortName);
                        a.Active = (agency.agencyStatus != null && agency.agencyStatus.description == "Active");
                        a.FEIN = agency.taxId;
                        if (DateTime.TryParse(agency.appointmentDate, out tempDate))
                            a.EffectiveDate = tempDate;

                        if (agency.locations != null)
                            foreach (var addr in agency.locations)
                            {
                                var l = new AgencyLocation();
                                l.IsPrimary = addr.isPrimaryLocation;

                                l.Address1 = addr.line1;
                                l.Address2 = addr.line2;
                                l.City = addr.city;
                                if (addr.usState != null) l.State = addr.usState.description;
                                l.PostalCode = addr.zip;

                                if (addr.locationCommunications != null)
                                {
                                    var phone = addr.locationCommunications.FirstOrDefault(lc => lc.communicationType.description.IndexOf("PHONE", StringComparison.CurrentCultureIgnoreCase) >= 0);
                                    if (phone != null && Decimal.TryParse(phone.communicationValue, out tempDecimal))
                                        l.Phone = tempDecimal;

                                    var fax = addr.locationCommunications.FirstOrDefault(lc => lc.communicationType.description.IndexOf("FAX", StringComparison.CurrentCultureIgnoreCase) >= 0);
                                    if (fax != null && Decimal.TryParse(fax.communicationValue, out tempDecimal))
                                        l.Fax = tempDecimal;

                                    var email = addr.locationCommunications.FirstOrDefault(lc => lc.communicationType.description.IndexOf("MAIL", StringComparison.CurrentCultureIgnoreCase) >= 0);
                                    if (email != null)
                                        l.Email = email.communicationValue;
                                }

                                a.Locations.Add(l);
                            }

                        //For some reason this can apparently have a value that's null
                        foreach (var b in (agency.agencyBranches ?? new APSService.agencyBranch[0]).Where(b => b.branch != null))
                            a.Branches.Add(new AgencyBranch() { Id = b.branch.id, Code = b.branch.code, Name = b.branch.name });

                        if (DateTime.TryParse(agency.renewalCancelDate, out tempDate))
                            a.RenewalCancelDate = tempDate;
                        if (DateTime.TryParse(agency.newCancelDate, out tempDate))
                            a.NewCancelDate = tempDate;

                        a.ReferralAgencyCode = (agency.transferredToAgency != null) ? agency.transferredToAgency.agencyCode : null;
                        if (DateTime.TryParse(agency.transferDate, out tempDate))
                            a.ReferralDate = tempDate;

                        List<string> uwCodes = new List<string>();
                        foreach (var uw in agency.agencyUnderwritingUnits ?? new APSService.agencyUnderwritingUnit[0])
                        {
                            var u = new AgencyUnderwritingUnit();
                            u.Id = uw.id;
                            u.UnderwritingUnitId = uw.underwritingUnit.id;
                            u.Code = uw.underwritingUnit.code;
                            u.Description = uw.underwritingUnit.description;

                            a.UnderwritingUnits.Add(u);
                            uwCodes.Add(u.Code);
                        }

                        var agents = (agency.agencyPersons ?? new APSService.agencyPersonnel[0]).Where(ap => ap.agencyPersonnelRoles != null && ap.agencyPersonnelRoles.Any(r => r.agencyPersonnelRoleType.description == "Producer"));
                        foreach (var agentDetail in agents)
                        {
                            var individualDetail = agentDetail.personnel;
                            if (agentDetail != null)
                            {
                                var ag = new Agent();
                                ag.Id = agentDetail.id;
                                ag.PersonnelId = individualDetail.id;
                                ag.AgentNumber = !String.IsNullOrEmpty(agentDetail.agentNumber) ? agentDetail.agentNumber : individualDetail.producerCode;

                                ag.NamePrefix = (individualDetail.namePrefix != null) ? individualDetail.namePrefix.label : null;
                                ag.FirstName = individualDetail.firstName;
                                ag.MiddleName = individualDetail.middleName;
                                ag.LastName = individualDetail.lastName;
                                ag.NameSuffix = (individualDetail.nameSuffix != null) ? individualDetail.nameSuffix.label : null;
                                ag.TaxId = individualDetail.socialSecurityNumber;

                                if (DateTime.TryParse(agentDetail.expirationDate, out tempDate))
                                    ag.CancelDate = tempDate;
                                if (DateTime.TryParse(agentDetail.effectiveDate, out tempDate))
                                    ag.EffectiveDate = tempDate;
                                if (DateTime.TryParse(individualDetail.dateOfBirth, out tempDate))
                                    ag.DOB = tempDate;

                                if (individualDetail.personnelCommunications != null)
                                {
                                    var phones = individualDetail.personnelCommunications.Where(l => (l.communicationType.formatter ?? "").IndexOf("PHONE", StringComparison.CurrentCultureIgnoreCase) >= 0);
                                    if (phones.Any())
                                    {
                                        var p = phones.ToArray();
                                        if (Decimal.TryParse(p[0].communicationValue, out tempDecimal))
                                            ag.PrimaryPhone = tempDecimal;
                                        if (p.Length > 1 && Decimal.TryParse(p[1].communicationValue, out tempDecimal))
                                            ag.SecondaryPhone = tempDecimal;
                                    }

                                    var fax = individualDetail.personnelCommunications.FirstOrDefault(l => (l.communicationType.formatter ?? "").IndexOf("FAX", StringComparison.CurrentCultureIgnoreCase) >= 0);
                                    if (fax != null && Decimal.TryParse(fax.communicationValue, out tempDecimal))
                                        ag.Fax = tempDecimal;

                                    var email = individualDetail.personnelCommunications.FirstOrDefault(l => (l.communicationType.formatter ?? "").IndexOf("EMAIL", StringComparison.CurrentCultureIgnoreCase) >= 0);
                                    if (email != null)
                                        ag.EmailAddress = email.communicationValue;
                                }

                                a.Agents.Add(ag);
                            }
                        }

                        Action<long> getLicensedStates = (long agencyId) =>
                        {
                            var stateLicenses = service.getAgencyLicensedStates(agencyId.ToString());
                            foreach (var sl in stateLicenses ?? new APSService.agencyLicense[0])
                                a.LicensedStates.Add(new AgencyLicensedState() { Id = sl.id, State = sl.licenseState.description, LicenseType = sl.licenseType.description });
                        };

                        Action getRelatedAgencies = () =>
                        {
                            var relatedAgencies = service.getRelatedAgencies(agencyCode, 0);
                            if (relatedAgencies != null)
                            {
                                var agencyAssns = relatedAgencies.FirstOrDefault(r => r.agencyCode == agency.agencyCode);
                                if (agencyAssns != null && agencyAssns.agencyXRefParents != null && agencyAssns.agencyXRefParents.Length > 0)
                                {
                                    var parentAgency = agencyAssns.agencyXRefParents[0].parentAgency;
                                    if (parentAgency != null)
                                        a.Parent = new AgencySummary() { Id = parentAgency.id, AgencyCode = parentAgency.agencyCode, AgencyName = parentAgency.dbaName };
                                }

                                a.HasChildren = relatedAgencies.Any(r => r.agencyXRefParents != null && r.agencyXRefParents.Length > 0 && r.agencyXRefParents[0].parentAgency != null && r.agencyXRefParents[0].parentAgency.id == a.Id);

                                if (useParentsLicensedStates)
                                    getLicensedStates((a.Parent != null) ? a.Parent.Id : agency.id);
                            }
                        };
                        Action getPersonnel = () =>
                        {
                            if (uwCodes.Count > 0 && personnelRoles != null && personnelRoles.Any())
                            {
                                var personnel = service.getPersonnelAssignmentsByAgencyIdsUWCodesRoleTypes(agency.id, uwCodes.ToArray(), personnelRoles.ToArray(), 0);
                                foreach (var p in (personnel ?? new APSService.personnelAssigned[0]))
                                {
                                    var unit = a.UnderwritingUnits.First(uw => uw.Code == p.underwritingUnitCode);
                                    unit.Personnel.Add(new AgencyUnderwritingUnitPersonnel()
                                    {
                                        Id = long.Parse(p.id),
                                        IsPrimaryRole = (p.isPrimary == "true"),
                                        RoleCode = p.roleCode,
                                        UserName = p.userId,
                                        FullName = p.fullName,
                                        Initials = p.userInitials,
                                        RetirementDate = (String.IsNullOrEmpty(p.retirementDate)) ? (DateTime?)null : DateTime.Parse(p.retirementDate)
                                    });
                                }
                            }
                        };
                        Action getNotes = () =>
                        {
                            try
                            {
                                var notes = service.getAgencyNotesByCategory(agency.id, 0);
                                foreach (var n in notes ?? new APSService.agencyNote[0])
                                    a.Notes.Add(new AgencyNote() { Category = n.agencyNoteCategoryType.label, Summary = n.summary, Details = n.detail, CreatedDate = DateTime.TryParse(n.occurrenceDate, out tempDate) ? tempDate : (DateTime?)null, LastModified = DateTime.TryParse(n.dateEntered, out tempDate) ? tempDate : (DateTime?)null });
                            }
                            catch (Exception ex)
                            {
                                results.SecondaryExceptions.Add(ex);
                            }
                        };


                        var tasks = new List<Task> {
                            Task.Factory.StartNew(getRelatedAgencies),
                            Task.Factory.StartNew(getPersonnel),
                            Task.Factory.StartNew(getNotes)
                        };
                        if (!useParentsLicensedStates)
                            Task.Factory.StartNew(() => getLicensedStates(agency.id));

                        Task.WaitAll(tasks.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }

        public static PersonnelSearchResult GetPersonnelByRole(string apsUrl, IEnumerable<string> roleCodes)
        {
            var results = new PersonnelSearchResult();
            try
            {
                results = new PersonnelSearchResult();
                using (var service = CreateWebServiceClient<APSService.APSServiceClient, APSService.APSService>(apsUrl, 30))
                {
                    Dictionary<string, Personnel> underwriters = new Dictionary<string, Personnel>();
                    long tempLong; DateTime tempDate;
                    foreach (var rc in roleCodes ?? new string[0])
                    {
                        var personnel = service.getPersonnelByRoleCode(rc);
                        foreach (var p in personnel ?? new Integration.APSService.personnelAssigned[0])
                        {
                            if (underwriters.ContainsKey(p.id))
                                underwriters[p.id].RoleCodes.Add(p.roleCode);
                            else
                            {
                                var person = new Personnel()
                                {
                                    Id = long.TryParse(p.id, out tempLong) ? tempLong : 0,
                                    FullName = p.fullName,
                                    Initials = p.userInitials,
                                    UserName = p.userId,
                                    RetirementDate = DateTime.TryParse(p.retirementDate, out tempDate) ? tempDate : (DateTime?)null
                                };

                                person.RoleCodes.Add(p.roleCode);
                                underwriters.Add(p.id, person);
                                results.Personnel.Add(person);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }

    }
}
