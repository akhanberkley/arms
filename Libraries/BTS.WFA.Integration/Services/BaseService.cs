﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Runtime.Caching;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace BTS.WFA.Integration.Services
{
    public class BaseService
    {
        private static MemoryCache cache = new MemoryCache("BTS.WFA.Integration");
        protected static object GetCacheItem(string key)
        {
            return cache[key];
        }
        protected static object RemoveCacheItem(string key)
        {
            return cache.Remove(key);
        }
        protected static void SetCacheItem(string key, object value, TimeSpan length)
        {
            cache.Add(key, value, DateTime.Now.Add(length));
        }

        protected static T CreateWebServiceClient<T, Y>(string url, int timeoutInSeconds)
            where T : ClientBase<Y>
            where Y : class
        {
            var binding = new BasicHttpBinding() { MaxReceivedMessageSize = int.MaxValue, SendTimeout = new TimeSpan(0, 0, timeoutInSeconds) };
            binding.ReaderQuotas.MaxArrayLength = int.MaxValue;
            binding.ReaderQuotas.MaxBytesPerRead = int.MaxValue;
            binding.ReaderQuotas.MaxDepth = int.MaxValue;
            binding.ReaderQuotas.MaxNameTableCharCount = int.MaxValue;
            binding.ReaderQuotas.MaxStringContentLength = int.MaxValue;

            var service = (T)Activator.CreateInstance(typeof(T), new object[] { binding, new EndpointAddress(url) });
            service.Endpoint.Behaviors.Add(new Providers.ServiceCallInspector());
            
            return service;
        }

        protected static HttpClient CreateHttpClient(string baseUrl, int timeoutInSeconds)
        {
            var client = new HttpClient(new Providers.HttpClientLogHandler(new HttpClientHandler() { UseDefaultCredentials = true }));
            client.BaseAddress = new Uri(baseUrl);
            client.Timeout = new TimeSpan(0, 0, timeoutInSeconds);

            return client;
        }

        protected static T Translate<T>(object value)
        {
            if (value == null || value == DBNull.Value)
            {
                return default(T);
            }
            else
            {
                Type t = typeof(T);
                if (t == typeof(string))
                    return (T)(object)value.ToString().Trim();
                if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
                    return (T)Convert.ChangeType(value, t.GetGenericArguments()[0]);
                else
                    return (T)Convert.ChangeType(value, typeof(T));
            }
        }
    }
}