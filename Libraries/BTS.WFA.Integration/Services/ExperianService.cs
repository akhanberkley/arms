﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using BTS.WFA.Integration.Models;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using System.Net.Http;

namespace BTS.WFA.Integration.Services
{
    public class ExperianService : BaseService
    {
        public static ExperianSearchResult Search(Models.AdvancedClientSettings settings, ExperianSearchCriteria criteria)
        {
            ExperianSearchResult results = new ExperianSearchResult();
            try
            {
                using (var service = CreateHttpClient(settings.Url, 30))
                {
                    service.DefaultRequestHeaders.Add("SOAPAction", "http://wrberkley.com/services/CreditReportSupplementalService/v1/CreditReportSearch");

                    XNamespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
                    XNamespace bts = "http://bts.services.wrberkley.com";
                    XNamespace oas = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
                    var envelope = new XElement(soapenv + "Envelope", new XAttribute(XNamespace.Xmlns + "soapenv", soapenv),
                                                                      new XAttribute(XNamespace.Xmlns + "bts", bts),
                                                                      new XAttribute(XNamespace.Xmlns + "oas", oas));

                    envelope.Add(new XElement(soapenv + "Header", new XElement(oas + "Security", new XElement(oas + "UsernameToken", new XElement(oas + "Username", settings.Username),
                                                                                                                                     new XElement(oas + "Password", "")),
                                                                                                 new XElement(bts + "dsik", settings.Dsik))));

                    XNamespace wrb_m = "http://www.wrberkley.com/schema/message/creditreportsupplemental/1";
                    XNamespace wrb_d = "http://www.wrberkley.com/schema/data/datacomponents/1";
                    var root = new XElement(wrb_m + "CreditReportOrderInqRq", new XAttribute("xmlns", "http://www.ACORD.org/standards/PC_Surety/ACORD1/xml/"),
                                                                              new XAttribute(XNamespace.Xmlns + "wrb-m", wrb_m),
                                                                              new XAttribute(XNamespace.Xmlns + "wrb-d", wrb_d));
                    envelope.Add(new XElement(soapenv + "Body", root));

                    var rns = root.GetDefaultNamespace();
                    root.Add(new XElement(rns + "RqUID", "00000000-0000-0000-0000-000000000000"));
                    root.Add(new XElement(rns + "TransactionRequestDt", DateTime.Now.ToString("o")));
                    root.Add(new XElement(rns + "CurCd", "USD"));
                    root.Add(new XElement(rns + "SPName", "ExperianCommercial"));
                    root.Add(new XElement(rns + "ReferenceNumber", "Workflow Apps"));
                    root.Add(new XElement(wrb_d + "NewReportInd", 0));

                    var gpi = new XElement(rns + "GeneralPartyInfo");
                    root.Add(new XElement(rns + "InsuredOrPrincipal", gpi));

                    root.Add(new XElement(rns + "InsuredOrPrincipal", new XElement(rns + "GeneralPartyInfo", new XElement(rns + "NameInfo", new XElement(rns + "CommlName", new XElement(rns + "CommercialName", criteria.Name))),
                                                                                                             new XElement(rns + "Addr", new XElement(rns + "Addr1", criteria.Address1),
                                                                                                                                        new XElement(rns + "City", criteria.City),
                                                                                                                                        new XElement(rns + "StateProvCd", criteria.State),
                                                                                                                                        new XElement(rns + "PostalCode", criteria.PostalCode)))));

                    var response = service.PostAsync("", new StringContent(envelope.ToString())).Result;
                    var content = response.Content.ReadAsStringAsync().Result;
                    if (!response.IsSuccessStatusCode)
                        throw new UnexpectedServiceResultException(content);
                    else
                    {
                        XDocument doc = XDocument.Parse(content);
                        XElement partySearchRs = (doc.Root.FirstNode as XElement).FirstNode as XElement;
                        var dns = partySearchRs.GetDefaultNamespace();

                        var msgStatus = partySearchRs.Element(dns + "MsgStatus");
                        if (msgStatus.Value != "Success")
                            throw new UnexpectedServiceResultException(msgStatus.ToString());
                        else
                        {
                            foreach (var r in partySearchRs.Descendants(dns + "GeneralPartyInfo"))
                            {
                                var match = new ExperianClient();
                                var node = r.XPathSelectElement("*[local-name()='NameInfo']/*[local-name()='CommlName']/*[local-name()='CommercialName']");
                                if (node != null)
                                    match.Name = node.Value;

                                node = r.XPathSelectElement("*[local-name()='NameInfo']/*[local-name()='NonTaxIdentity'][*[local-name()='NonTaxIdTypeCd']='Experian']/*[local-name()='NonTaxId']");
                                if (node != null)
                                    match.Id = long.Parse(node.Value);

                                node = r.XPathSelectElement("*[local-name()='Communications']/*[local-name()='PhoneInfo']/*[local-name()='PhoneNumber']");
                                if (node != null)
                                    match.PhoneNumber = node.Value;

                                node = r.XPathSelectElement("*[local-name()='Addr']");
                                if (node != null)
                                    foreach (var elm in node.Descendants())
                                    {
                                        switch (elm.Name.LocalName)
                                        {
                                            case "Addr1": match.Address1 = elm.Value; break;
                                            case "Addr2": match.Address2 = elm.Value; break;
                                            case "City": match.City = elm.Value; break;
                                            case "StateProvCd": match.State = elm.Value; break;
                                            case "PostalCode": match.PostalCode = elm.Value; break;
                                        }
                                    }

                                results.Matches.Add(match);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }

    }
}
