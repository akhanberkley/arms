﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using BTS.WFA.Integration.Models;
using System.Xml.XPath;
using System.Data.SqlClient;

namespace BTS.WFA.Integration.Services
{
    public class CWGService : BaseService
    {
        public static CWGPolicyResults GetSubmissionsToSync(CWGCompany company, string connectionString, DateTime lastRun)
        {
            var results = new CWGPolicyResults();

            try
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    var cmd = String.Format("SELECT * FROM Cl_Sys_Data_Pull_Aplus_PDR WHERE Date_Run > '{0}' AND Policy_Tran_Type <> 'RN'", lastRun.ToString());
                    using (var dr = (new SqlCommand(cmd, conn)).ExecuteReader(System.Data.CommandBehavior.CloseConnection))
                    {
                        while (dr.Read())
                        {
                            var p = new CWGPolicy();
                            p.RunDate = Translate<DateTime>(dr["Date_Run"]);
                            p.PolicyNumber = Translate<string>(dr["Policy_Num"]);
                            if (!String.IsNullOrEmpty(p.PolicyNumber))
                                p.PolicyNumber = p.PolicyNumber.Substring(2);

                            p.PolicyTransactionType = Translate<string>(dr["Policy_Tran_Type"]);
                            p.BusCategoryCode = Translate<string>(dr["Bus_Cat"]);
                            p.ProgramCode = Translate<string>(dr["Prog_Code"]);
                            p.SICCode = Translate<string>(dr["SIC"]);
                            p.Premium = Translate<decimal>(dr["TW_Prem"]);

                            if (company == CWGCompany.CWG)
                            {
                                p.PolicyStatus = Translate<string>(dr["Policy_Status"]);
                                p.PolicyEffectiveDate = Translate<DateTime?>(dr["Policy_Eff_Dt"]);
                                p.NAICSCode = Translate<string>(dr["NAICS"]);
                                p.PriorCarrier = Translate<string>(dr["PriorCarrier"]);
                            }

                            results.Policies.Add(p);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }
    }
}
