﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using BTS.WFA.Integration.Models;
using BTS.WFA.Integration.PolicyStarTransactional;
using System.Xml.XPath;
using System.Data.SqlClient;

namespace BTS.WFA.Integration.Services
{
    public class PolicyStarTransactionalService : BaseService
    {
        /// <summary>
        /// Invokes the Policy Star (Genesys) Transactional Web Service to return policies for a specified policy number.
        /// </summary>
        /// <param name="url">The web service end point url.</param>
        /// <param name="policyNumber">The policy number.</param>
        /// <param name="companyNumber">The company number.</param>
        /// <returns>A PolicySystemResults object.</returns>
        public static PolicySystemResults GetPolicyByPolicyNumber(string url, string policyNumber, string companyNumber)
        {
            PolicySystemResults results = new PolicySystemResults();

            try
            {
                using (var service = CreateWebServiceClient<PremiumAuditServiceClient, PremiumAuditService>(url, 180))
                {
                    var policies = service.getPolicyByPolicyNbr(policyNumber, string.Empty, companyNumber);
                    if (policies != null && policies.policyAuditDO != null)
                    {
                        foreach (PolicyAuditDO p in policies.policyAuditDO)
                        {
                            //Build the policy
                            Policy policy = BuildPolicy(p);
                            results.Policies.Add(policy);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }

        #region Private Methods
        private static Policy BuildPolicy(PolicyAuditDO ds)
        {
            Policy policy = new Policy();

            if (ds.agency != null)
            {
                policy.Agency = BuildAgency(ds.agency);
            }
            
            policy.BranchCode = ReadString(ds.branchCode);
            policy.BusinessDescription = ReadString(ds.businessDesc);
            policy.BusinessType = ReadString(ds.businessType);

            //if (!String.IsNullOrEmpty(ds.cancelDate))
            //    policy.CancelDate = DateTime.Parse(ds.cancelDate);

            policy.Carrier = ReadString(ds.carrier);
            policy.ClientId = ReadString(ds.clientId);
            //customDetails
            if (ds.customDetails != null)
            {
                policy.CustomDetails = BuildCustomDetails(ds.customDetails);
            }
            if (!String.IsNullOrEmpty(ds.effectiveDate))
                policy.EffectiveDate = DateTime.Parse(ds.effectiveDate); //ReadDateTime(pdrPolicy.effectiveDate);
            if (!String.IsNullOrEmpty(ds.expirationDate))
                policy.ExpirationDate = DateTime.Parse(ds.expirationDate); //ReadDateTime(pdrPolicy.expirationDate)
            policy.EntityCode = ReadString(ds.entityCode);
            policy.EntityInsured = ReadString(ds.entityInsured);
            policy.EntityName = ReadString(ds.entityName);
            policy.Fein = ReadString(ds.FEIN);
            policy.LineOfBusiness = ReadString(ds.LOB);
            //namedInsured
            if (ds.namedInsured != null)
            {
                policy.NamedInsureds = BuildNamedInsured(ds.namedInsured);
            }
            //policyAddresses
            if (ds.policyAddress.Length > 0)
            {
                for (int i = 0; i < ds.policyAddress.Length; i++)
                {
                    policy.PolicyAddresses.Add(BuildAddress(ds.policyAddress[i]));
                }
            }
            policy.Id = ReadString(ds.policyId);
            policy.Number = ReadString(ds.policyNbr);
            policy.SequenceNumber = ReadString(ds.policySeqNbr); //ReadInteger(pdrPolicy.policySeqNbr)
            policy.Status = ReadString(ds.policyStatus);
            policy.Symbol = ReadString(ds.policySymbol);
            //products
            if (ds.products != null)
            {
                policy.Products = BuildSecondaryProducts(ds.products);
            }
            //profitCenter
            //riskitem
            policy.TotalEstimatedPremium = ReadString(ds.totalEstimatedPremium); //ReadDecimal(pdrPolicy.totalEstimatedPremium)
            //transactionProcessDate
            //units
            if (ds.units != null)
            {
                policy.Units = BuildUnits(ds.units);
            }
            policy.NonRenewableFlag = ReadString(ds.nonRenewableFlag);
            policy.ClientSequenceNumber = ReadString(ds.clientSeqNum);
            //ownerlegalname
            policy.UnderwriterCode = ReadString(ds.underwriterCode);
            //notes
            policy.PortfolioId = ReadString(ds.portfolioId);
            policy.AuditFlag = ds.auditFlag;
            policy.CorpId = ds.corpId;
            policy.BranchName = ReadString(ds.branchName);
            policy.SicCode = ReadString(ds.SICCode);
            policy.UnderwriterId = ReadString(ds.underwriterId);
            policy.UnderwriterName = ReadString(ds.underwriterName);
            policy.CompnayName = ReadString(ds.companyName);
            policy.CancelPendingAuditFlag = ReadString(ds.cancelPendingAuditFlag);

            if (!String.IsNullOrEmpty(ds.statusEffectiveDate))
                policy.StatusEffectiveDate = DateTime.Parse(ds.statusEffectiveDate);

            policy.BillType = ReadString(ds.billType);


            /******************
             * Snipped from ARMS\Libraries\Berkley.BLC.Import\Builders\TransactionalPolicyStarBaseImportBuilder.cs
             * Line 541
             ******************/
            //Get the profit center from the custom details
            //if (pdrPolicy.customDetails != null)
            //{
            //    foreach (PDRCustomDetailDO customDetail in pdrPolicy.customDetails)
            //    {
            //        if (ReadString(customDetail.customDetailName).ToUpper() == "PCC")
            //        {
            //            policy.ProfitCenter = ReadString(customDetail.customDetailValue);
            //        }
            //    }
            //}
            //policy.BranchCode = ReadString(pdrPolicy.branchCode);
            //if (!string.IsNullOrWhiteSpace(pdrPolicy.carrier))
            //    policy.Carrier = ReadString(pdrPolicy.carrier);
            //else
            //    policy.Carrier = ReadString(pdrPolicy.companyName);
            //policy.EffectiveDate = ReadDateTime(pdrPolicy.effectiveDate);
            //policy.ExpireDate = ReadDateTime(pdrPolicy.expirationDate);
            //policy.Mod = ReadInteger(pdrPolicy.policySeqNbr);
            //policy.Number = ReadString(pdrPolicy.policyNbr);
            //policy.Symbol = ReadString(pdrPolicy.policySymbol);
            //policy.Premium = ReadDecimal(pdrPolicy.totalEstimatedPremium);

            //if (pdrPolicy.products != null)
            //{
            //    foreach (SecondaryProductDO product in pdrPolicy.products)
            //    {
            //        if (ReadString(product.lobHazardDescription) != string.Empty)
            //        {
            //            policy.HazardGrade = StripNonNumericChars(ReadString(product.lobHazardDescription)).ToString();
            //        }
            //    }
            //}

            ////try and find past hazard grade for the policy
            //if (policy.HazardGrade == string.Empty)
            //{
            //    Policy pastPolicy = Policy.GetOne("Number = ? && !ISNULL(HazardGrade) && HazardGrade != '' && Insured.CompanyID = ?", policy.Number, berkleyCompany.ID);
            //    policy.HazardGrade = (pastPolicy != null) ? pastPolicy.HazardGrade : "";
            //}

            ////Add the policy status to the list for setting the client status later
            //policyStatusList.Add(ReadString(pdrPolicy.nonRenewableFlag));

            return policy;
        }

        private static PDRAgency BuildAgency(AgencyDO ds)
        {
            PDRAgency agency = new PDRAgency();

            if (ds.address != null)
            {
                agency.Address = BuildAddress(ds.address);
            }
            
            agency.BranchOfficeId = ds.agencyBranchOffId;
            agency.Id = ReadString(ds.agencyId);
            agency.Name = ReadString(ds.agencyName);
            agency.Number = ReadString(ds.agencyNumber);
            agency.PhoneNumber = ReadString(ds.agencyPhone);
            agency.TaxNumber = ReadString(ds.agencyTaxNbr);

            return agency;
        }

        private static PDRAddress BuildAddress(PDRAddressDO ds)
        {
            PDRAddress address = new PDRAddress();

            address.Address1 = ReadString(ds.address1);
            address.Address2 = ReadString(ds.address2);
            address.Address3 = ReadString(ds.address3);
            address.Address4 = ReadString(ds.address4);
            address.Id = ds.addressId;
            address.BillingFlag = ReadString(ds.billingFLG);
            address.City = ReadString(ds.city);
            address.ClientId = ReadString(ds.clientId);
            address.County = ReadString(ds.county);
            address.HouseNumber = ReadString(ds.houseNbr);
            address.InternalAddressId = ds.intAddressId;
            address.MailingFlag = ReadString(ds.mailingFLG);
            address.PostalCode = ReadString(ds.postalCode);
            address.PrimaryFlag = ReadString(ds.primaryFLG);
            address.StateProvCode = ReadString(ds.stateProvCd);

            return address;
        }

        private static PDRCoverage BuildCoverage(PDRCoverageDO ds)
        {
            PDRCoverage coverage = new PDRCoverage();

            /*
            ◾accumulatedF - nillable; type string
            ◾actExpAmt type double
            ◾adjActExpAmt type double
            ◾adjAvgExpAmt type double
            ◾autoSymbol1C - nillable; type string
            ◾autoSymbol1Name - nillable; type string
            ◾autoSymbol2C - nillable; type string
            ◾autoSymbol2Name - nillable; type string
            ◾autoSymbol3C - nillable; type string
            ◾autoSymbol3Name - nillable; type string
            ◾autoSymbol4C - nillable; type string
            ◾autoSymbol4Name - nillable; type string
            ◾balToMinPremM type double
            ◾billToDPremM type double
            ◾calcPremM type double
            ◾classCodeCov - nillable; type string
            ◾classCodeDesc - nillable; type string
            ◾classCodeOvr - nillable; type string
            ◾classItemNumber type long
            ◾commissionM type double
            ◾compositePremM type double
            */
            //coverages
            if (ds.coverage != null)
            {
                coverage.Coverages = BuildCoverages(ds.coverage);
            }

            coverage.Code = ReadString(ds.coverageCode);
            coverage.Description = ReadString(ds.coverageDesc);
            coverage.Id = ds.coverageId;
            coverage.DeductableAmount1 = ds.deductAmt1;
            coverage.DeductableAmount2 = ds.deductAmt2;
            coverage.DeductableAmount3 = ds.deductAmt3;
            coverage.DeductableTypeName1 = ReadString(ds.deductTypeName1);
            coverage.DeductableTypeName2 = ReadString(ds.deductTypeName2);
            coverage.DeductableTypeName3 = ReadString(ds.deductTypeName3);
            /*
            ◾deductValTypId1 type long
            ◾deductValTypId2 type long
            ◾deductValTypId3 type long
            ◾depositAmt type double
            ◾enteredRate type double
            ◾estExposureAmt type double
            ◾finalRate type double
            ◾flatChargeF - nillable; type string
            */
            coverage.FullEarnedPremiumFlag = ReadString(ds.fullEarnedPremF);
            /*
            ◾itemCodeOrText - nillable; type string
            */
            coverage.LimitAmount1 = ds.limitAmt1;
            coverage.LimitAmount2 = ds.limitAmt2;
            coverage.LimitAmount3 = ds.limitAmt3;
            /*
            ◾limitAmt1 type double
            ◾limitAmt2 type double
            ◾limitAmt3 type double
            ◾limitTypeName1 - nillable; type string
            ◾limitTypeName2 - nillable; type string
            ◾limitTypeName3 - nillable; type string
            ◾limitValTypId1 type long
            ◾limitValTypId2 type long
            ◾limitValTypId3 type long
            ◾manualRateF - nillable; type string
            ◾minTermPremM type double
            ◾parentCoverageCode - nillable; type string
            ◾parentCoverageDesc - nillable; type string
            ◾parentCoverageId type long
            ◾premCoverageM type double
            ◾premRollUpAccum type double
            ◾premTotalCovM type double
            ◾provisionalP type double
            ◾ratingMethodC - nillable; type string
            ◾ratingMethodName - nillable; type string
            ◾rdfR type double
            ◾rptBasisName - nillable; type string
            ◾rptgAvgApplyF - nillable; type string
            ◾rptgBasisC - nillable; type string
            ◾rptgPeriodC - nillable; type string
            ◾rptgPeriodDay type long
            ◾rptgPeriodName - nillable; type string
            */
            coverage.SequenceNumber = ds.seqNbr;
            coverage.SpecialCoverageCode = ReadString(ds.specialCvgC);
            coverage.StateCode = ReadString(ds.stateCode);
            /*
            ◾subClassCodeCov - nillable; type string
            ◾subClassCodeOvr - nillable; type string
            ◾subLine - nillable; type string
            ◾tfsCommsnM type double
            ◾tfsPremM type double
            ◾unitcoverageId type long
            ◾usePolicyCovF - nillable; type string
            ◾premiumBasis - nillable; type string
            ◾auditableClassCodeF - nillable; type string
            ◾rateBasis - nillable; type string
            */

            //customDetails
            if (ds.customDetails != null)
            {
                coverage.CustomDetails = BuildCustomDetails(ds.customDetails);
            }

            return coverage;
        }

        private static PDRName BuildName(PDRNameDO ds)
        {
            PDRName name = new PDRName();

            name.ClientCategory = ReadString(ds.clientCategory);
            name.ClientSequenceNumber = ds.clientSeqNbrI;
            name.ClientId = ds.clientId;
            name.ClientType = ReadString(ds.clientType);
            name.Id = ds.intNameId;
            name.BusinessName = ReadString(ds.nameBusiness);
            name.FirstName = ReadString(ds.nameFirst);
            name.MiddleName = ReadString(ds.nameMiddle);
            name.LastName = ReadString(ds.nameLast);
            name.Prefix = ReadString(ds.namePrefixCd);
            name.Suffix = ReadString(ds.nameSuffixCd);
            name.Type = ReadString(ds.nameType);
            name.PrimaryNameFlag = ReadString(ds.primaryNameF);
            if (ds.communications != null)
            {
                name.Communications = BuildCommunications(ds.communications);
            }
            
            return name;
        }

        private static PDRRateModFactor BuildRateModFactor(RateModFactor ds)
        {
            PDRRateModFactor rateMod = new PDRRateModFactor();

            rateMod.CustomeDetailDomId = ReadString(ds.customDtlDomId);
            rateMod.Code = ReadString(ds.rtModCode);
            rateMod.Factor = ReadString(ds.rtModFactor);
            rateMod.State = ReadString(ds.rtModState);

            return rateMod;
        }

        private static PDRSecondaryProduct BuildSecondaryProduct(SecondaryProductDO ds)
        {
            PDRSecondaryProduct product = new PDRSecondaryProduct();

            product.ClaimsMade = ReadString(ds.claimsMade);
            product.CommissionM = ds.commissionM;
            product.CompanyCode = ReadString(ds.companyCode);
            product.CompanyId = ds.companyId;
            product.CompanyName = ReadString(ds.companyName);
            
            //coverages
            if (ds.coverages != null)
            {
                product.Coverages = BuildCoverages(ds.coverages);
            }

            product.EntityCode = ReadString(ds.entityC);
            product.EntityName = ReadString(ds.entityName);
            product.ExperienceLiabP = ds.experienceLiabP;
            product.ExperienceMiscP = ds.experienceMiscP;
            product.ExperiencePdP = ds.experiencePdP;
            product.FeesTotalM = ds.feesTotalM;
            product.FleetFlag = ReadString(ds.fleetF);

            //forms
            if (ds.forms != null)
            {
                product.Forms = new List<PDRForm>();  // #TODO: BuildForms(ds.forms);
            }

            product.LobHazardDescription = ReadString(ds.lobHazardDescription);
            product.PlanCode = ReadString(ds.planCode);
            product.PlanId = ds.planId;
            product.PlanName = ReadString(ds.planName);
            product.PremiumTotalM = ds.premTotalM;
            product.Code = ReadString(ds.productCode);
            product.Id = ds.productId;
            product.Name = ReadString(ds.productName);
            product.ProgramCode = ReadString(ds.programCode);
            product.ProgramId = ds.programId;
            product.ProgramName = ReadString(ds.programName);
            product.ProvisionalRateFactor = ReadString(ds.provisionalRateF);

            // Rate Mod Factors
            if (ds.rateMod != null)
            {
                product.RateModFactors = BuildRateModFactors(ds.rateMod);
            }

            // Units
            if (ds.units != null)
            {
                product.Units = BuildUnits(ds.units);
            }
            
            return product;
        }

        private static PDRUnit BuildUnit(PDRUnitDO ds)
        {
            PDRUnit unit = new PDRUnit();

            if (ds.address != null)
            {
                unit.Address = BuildAddress(ds.address);
            }

            if (ds.coverage != null)
            {
                unit.Coverages = BuildCoverages(ds.coverage);
            }
            
            unit.Description = ReadString(ds.unitDesc);
            unit.EntityC = ReadString(ds.entityC);
            unit.EntityName = ReadString(ds.entityName);
            unit.ProductId = ds.productId;
            unit.ProtectionClass = ReadString(ds.protectionClass);
            unit.SprinkleredFlag = ReadString(ds.sprinkleredF);
            unit.StateCode = ReadString(ds.stateCode);
            unit.VehicleSymbolCode = ReadString(ds.vehicleSymbolC);
            unit.VehicleSymbolName = ReadString(ds.vehicleSymbolName);
            unit.VinVerifyFlag = ReadString(ds.vinVerifyF);
            unit.Year = ds.unitYear;

            // Units
            if (ds.unit != null)
            {
                unit.Units = BuildUnits(ds.unit);
            }

            return unit;
        }

        private static PDRCommunication BuildCommunication(PDRCommunicationDO ds)
        {
            PDRCommunication comm = new PDRCommunication();
            comm.Type = ReadString(ds.communicationType);
            comm.Value = ReadString(ds.communicationValue);
            return comm;
        }

        #region Build Lists of above types
        private static List<PDRCustomDetail> BuildCustomDetails(PDRCustomDetailDO[] ds)
        {
            List<PDRCustomDetail> customDetails = new List<PDRCustomDetail>();

            foreach (PDRCustomDetailDO obj in ds)
            {
                PDRCustomDetail detail = new PDRCustomDetail();
                detail.Id = obj.cstmDtlId;
                detail.Name = obj.customDetailName;
                detail.Value = obj.customDetailValue;
                detail.Description = obj.customDetailDesc;
                detail.DomTbId = obj.cstmDtlDomTbId;

                customDetails.Add(detail);
            }

            return customDetails;
        }

        private static List<PDRCoverage> BuildCoverages(PDRCoverageDO[] ds)
        {
            List<PDRCoverage> coverages = new List<PDRCoverage>();

            foreach (PDRCoverageDO obj in ds)
            {
                var coverage = BuildCoverage(obj);
                coverages.Add(coverage);
            }

            return coverages;
        }

        private static List<PDRNamedInsured> BuildNamedInsured(PDRNamedInsuredDO[] ds)
        {
            List<PDRNamedInsured> namedInsureds = new List<PDRNamedInsured>();

            foreach (PDRNamedInsuredDO obj in ds)
            {
                PDRNamedInsured insured = new PDRNamedInsured();
                List<PDRAddress> addresses = new List<PDRAddress>();
                List<PDRName> names = new List<PDRName>();

                insured.Id = obj.nmInsuredId;
                insured.PrimaryNamedInsuredFlag = obj.primaryNmInsF;

                if (obj.insAddress != null)
                {
                    foreach (PDRAddressDO a in obj.insAddress)
                    {
                        var address = BuildAddress(a);
                        addresses.Add(address);
                    }
                }
                insured.Addresses = addresses;

                if (obj.insName != null)
                {
                    foreach (PDRNameDO n in obj.insName)
                    {
                        var name = BuildName(n);
                        names.Add(name);
                    }
                }
                insured.Names = names;
                
                namedInsureds.Add(insured);
            }

            return namedInsureds;
        }

        private static List<PDRRateModFactor> BuildRateModFactors(RateModFactor[] ds)
        {
            List<PDRRateModFactor> rateMods = new List<PDRRateModFactor>();

            foreach (RateModFactor r in ds)
            {
                var mod = BuildRateModFactor(r);
                rateMods.Add(mod);
            }

            return rateMods;
        }

        private static List<PDRSecondaryProduct> BuildSecondaryProducts(SecondaryProductDO[] ds)
        {
            List<PDRSecondaryProduct> products = new List<PDRSecondaryProduct>();

            foreach (SecondaryProductDO obj in ds)
            {
                var product = BuildSecondaryProduct(obj);
                products.Add(product);
            }

            return products;
        }

        private static List<PDRUnit> BuildUnits(PDRUnitDO[] ds)
        {
            List<PDRUnit> units = new List<PDRUnit>();

            foreach (PDRUnitDO obj in ds)
            {
                var unit = BuildUnit(obj);
                units.Add(unit);
            }

            return units;
        }
        
        private static List<PDRCommunication> BuildCommunications(PDRCommunicationDO[] ds)
        {
            List<PDRCommunication> comms = new List<PDRCommunication>();

            foreach (PDRCommunicationDO obj in ds)
            {
                var comm = BuildCommunication(obj);
                comms.Add(comm);
            }

            return comms;
        }
        #endregion

        private static string ReadString(object obj)
        {
            var str = (obj != null) ? obj.ToString().Trim() : string.Empty;
            return (!string.IsNullOrWhiteSpace(str)) ? str : null;
        }
        #endregion
    }
}