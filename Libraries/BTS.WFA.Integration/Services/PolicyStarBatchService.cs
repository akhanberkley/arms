﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using BTS.WFA.Integration.Models;
using System.Xml.XPath;
using System.Data.SqlClient;

namespace BTS.WFA.Integration.Services
{
    public class PolicyStarBatchService : BaseService
    {
        public static PolicyStarPolicyResults GetPolicies(string url, string companyNumber, DateTime sinceLastModified)
        {
            PolicyStarPolicyResults results = new PolicyStarPolicyResults();

            try
            {
                using (var service = CreateWebServiceClient<PolicyStarBatch.PremiumAuditServiceClient, PolicyStarBatch.PremiumAuditService>(url, 180))
                {
                    var maxDate = DateTime.Now.AddYears(10).ToString("MM/dd/yyyy");
                    var policies = service.getPolicyByDateRange("1/1/2005", maxDate, sinceLastModified.ToString("MM/dd/yyyy"), maxDate, companyNumber);
                    if (policies != null && policies.policyDo != null)
                    {
                        foreach (var p in policies.policyDo)
                        {
                            var policy = new PolicyStarPolicy();
                            results.Policies.Add(policy);

                            policy.PolicyNumber = p.policyNo;
                            policy.PolicyMod = p.policySeqNo;
                            policy.LastModifiedDate = DateTime.Parse(p.lastModifiedDate);

                            if (!String.IsNullOrEmpty(p.effectiveDate))
                                policy.EffectiveDate = DateTime.Parse(p.effectiveDate);
                            if (!String.IsNullOrEmpty(p.expirationDate))
                                policy.ExpirationDate = DateTime.Parse(p.expirationDate);

                            var details = service.getEstimatedPremium(p.policyNo, p.policySeqNo, companyNumber);
                            if (details != null && details.Length > 0)
                            {
                                var attr = details[0];

                                policy.Status = attr.policyStatus.Trim();
                                policy.StatusEffectiveDate = DateTime.Parse(attr.statusEffectiveDate);
                                policy.CancelPendingAudit = (attr.cancelPendingAuditFlag == "Y");
                                if (!String.IsNullOrEmpty(attr.cancelDate))
                                    policy.CancelDate = DateTime.Parse(attr.cancelDate);
                                if (!String.IsNullOrEmpty(attr.writtenPremiumToDate))
                                    policy.Premium = Decimal.Parse(attr.writtenPremiumToDate);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }
    }
}
