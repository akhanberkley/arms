﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using BTS.WFA.Integration.Models;
using System.Xml.XPath;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace BTS.WFA.Integration.Services
{
    public class ClearanceService : BaseService
    {
        public static SubmissionSearchResult GetSubmission(string bcsUrl, string companyNumber, string submissionNumber)
        {
            var results = new SubmissionSearchResult();
            try
            {
                using (var client = CreateHttpClient(bcsUrl, 30))
                {
                    //This line allows for bad server certs; doesn't belong here though (exists for testing purposes)
                    //System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, cert, chain, errors) => true);

                    var response = client.GetAsync("api/" + companyNumber + "/Submission/" + submissionNumber).Result;
                    if (!response.IsSuccessStatusCode)
                        throw new UnexpectedServiceResultException(String.Format("Submission Request Failed: {0} - {1}: {2}", (int)response.StatusCode, response.ReasonPhrase, response.Content.ReadAsStringAsync().Result));
                    else
                    {
                        dynamic jsonSubmission = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                        if (jsonSubmission != null)
                        {
                            var s = results.Submission = new Submission();
                            s.SubmissionNumber = jsonSubmission.SubmissionNumber;
                            s.PolicyNumber = jsonSubmission.PolicyNumber;
                            s.ClientId = jsonSubmission.Clients[0].ClientCoreId;

                            if (jsonSubmission.Agency != null)
                            {
                                s.AgencyNumber = jsonSubmission.Agency.Number;
                                s.AgencyName = jsonSubmission.Agency.Name;
                            }
                            if (jsonSubmission.Agent != null)
                            {
                                s.AgentName = jsonSubmission.Agent.FullName;
                                s.AgentPhoneNumber = jsonSubmission.Agent.PhoneNumber;
                            }
                            if (jsonSubmission.Underwriter != null)
                            {
                                s.Underwriter = jsonSubmission.Underwriter.FullName;
                            }

                            foreach (var addr in jsonSubmission.Clients[0].Addresses)
                            {
                                if (addr.Included == true)
                                    s.Locations.Add(new SubmissionLocation()
                                    {
                                        BuildingNumber = addr.BuildingNumber,
                                        Address1 = addr.Address1,
                                        City = addr.City,
                                        State = addr.State,
                                        PostalCode = addr.PostalCode
                                    });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }
    }
}
