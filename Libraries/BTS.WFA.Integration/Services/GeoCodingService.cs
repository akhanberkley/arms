﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using BTS.WFA.Integration.Models;

namespace BTS.WFA.Integration.Services
{
    public class GeoCodingService : BaseService
    {
        public static GeoCodingSearchResult SimpleSearch(string giswsUrl, string city, string state, string zip)
        {
            GeoCodingSearchResult results = new GeoCodingSearchResult();
            try
            {
                using (var service = CreateWebServiceClient<GISWS.GISWSSoapClient, GISWS.GISWSSoap>(giswsUrl, 15))
                {
                    XElement serviceResults = null;
                    if (!String.IsNullOrEmpty(zip))
                        serviceResults = service.GetCityStateCounty(new GISWS.CityStateCountyInput() { ZipCode = zip });
                    else
                        serviceResults = service.GetZipCounty(new GISWS.ZipCountyInput() { City = city, State = state });

                    if (serviceResults != null)
                    {
                        foreach (var r in serviceResults.Descendants("GISCityStateZip"))
                        {
                            var match = new GeoCodingAddress();
                            match.City = r.Element("City").Value.Trim();
                            match.State = r.Element("State").Value.Trim();
                            match.PostalCode = r.Element("ZipCode").Value.Trim();
                            match.County = r.Element("County").Value.Trim();

                            results.Matches.Add(match);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }

        public static GeoCodingSearchResult ComplexSearch(AdvancedClientSettings settings, GeoCodingAddress criteria)
        {
            GeoCodingSearchResult results = new GeoCodingSearchResult();
            try
            {
                using (var service = CreateWebServiceClient<AdvClientSearchWS.advClientSearchServiceClient, AdvClientSearchWS.advClientSearchService>(settings.Url, 10))
                {
                    var serviceCriteria = new AdvClientSearchWS.validateAddress()
                    {
                        address = new AdvClientSearchWS.Addr_Type()
                        {
                            Addr1 = new AdvClientSearchWS.C64() { Value = criteria.Addr1 },
                            Addr2 = new AdvClientSearchWS.C64() { Value = criteria.Addr2 },
                            City = new AdvClientSearchWS.C32() { Value = criteria.City },
                            StateProv = new AdvClientSearchWS.C60() { Value = criteria.State },
                            PostalCode = new AdvClientSearchWS.C11() { Value = criteria.PostalCode },
                            County = new AdvClientSearchWS.C50() { Value = criteria.County },
                            Latitude = new AdvClientSearchWS.C12() { Value = criteria.Latitude.ToString() },
                            Longitude = new AdvClientSearchWS.C13() { Value = criteria.Longitude.ToString() }
                        }
                    };
                    var searchResults = service.validateAddress(settings.ToSecurityToken(), serviceCriteria);
                    if (searchResults.AddrResults != null && searchResults.AddrResults.Addr != null)
                    {
                        foreach (var r in searchResults.AddrResults.Addr)
                        {
                            var match = new GeoCodingAddress();
                            match.Addr1 = r.Addr1.Value;
                            match.Addr2 = (r.Addr2 == null) ? null : r.Addr2.Value;
                            match.City = r.City.Value;
                            match.State = r.StateProv.Value;
                            match.PostalCode = r.PostalCode.Value ?? "";
                            if (match.PostalCode.Length > 5)
                                match.PostalCode = match.PostalCode.Substring(0, 5);
                            match.County = r.County.Value;
                            match.Longitude = decimal.Parse(r.Longitude.Value);
                            match.Latitude = decimal.Parse(r.Latitude.Value);

                            results.Matches.Add(match);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }
    }
}
