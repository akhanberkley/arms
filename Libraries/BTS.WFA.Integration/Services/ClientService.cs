﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using BTS.WFA.Integration.Models;
using System.Xml.XPath;
using System.Text.RegularExpressions;
using System.Web;
using System.Net.Http;

namespace BTS.WFA.Integration.Services
{
    public class ClientService : BaseService
    {
        #region Client Core Specific
        private static string GenerateBasicXml(ClientSystemSettings settings)
        {
            var xml = new XElement("TFGGlobalBasicXMLDO");
            xml.Add(new XElement("systemCd", settings.SystemCode));
            xml.Add(new XElement("username", settings.Username));
            xml.Add(new XElement("password", settings.Password));
            xml.Add(new XElement("localID", settings.LocalId));

            return xml.ToString(SaveOptions.DisableFormatting);
        }
        private static string GenerateIdSearchXml(string clientId, string corpId, DateTime? effectiveDate)
        {
            XElement xml = new XElement("TFGClientExportIDInfoDO");
            xml.Add(new XElement("ClientID", clientId));
            xml.Add(new XElement("EffectiveDate", ToClientCoreDateString(effectiveDate ?? DateTime.Now)));
            xml.Add(new XElement("SeqNbr", 1000));
            xml.Add(new XElement("corpID", corpId));

            return xml.ToString(SaveOptions.DisableFormatting);
        }
        private static string GeneratePortfolioSearchXml(ClientSearchCriteria criteria)
        {
            XElement xml = new XElement("request_portfolio_search");
            xml.Add(new XElement("portfolio_id", criteria.PortfolioId));
            xml.Add(new XElement("portfolio_name", criteria.PortfolioName));
            xml.Add(new XElement("effective_date", DateTime.Now.ToShortDateString()));
            xml.Add(new XElement("corp_id", criteria.CorpId));

            return xml.ToString(SaveOptions.DisableFormatting);
        }
        private static string GenerateStandardSearchXml(ClientSearchCriteria criteria)
        {
            XElement xml = new XElement("TFGClientSearchXMLDO");
            xml.Add(new XElement("searchType", "Client"));
            xml.Add(new XElement("clientType", criteria.ClientType));
            xml.Add(new XElement("firstName", criteria.FirstName));
            xml.Add(new XElement("lastName", criteria.LastName));
            xml.Add(new XElement("middleName", criteria.MiddleName));
            xml.Add(new XElement("businessName", criteria.BusinessName));
            xml.Add(new XElement("taxID", criteria.TaxId));
            xml.Add(new XElement("taxIDType", criteria.TaxIdType));
            xml.Add(new XElement("houseNumber", criteria.HouseNumber));
            xml.Add(new XElement("address1", criteria.Address1));
            xml.Add(new XElement("countryCD", criteria.CountryName));
            xml.Add(new XElement("stateCD", criteria.StateName));
            xml.Add(new XElement("cityName", criteria.CityName));
            xml.Add(new XElement("countyName", criteria.CountyName));
            xml.Add(new XElement("countyID", criteria.CountyId));
            xml.Add(new XElement("zipCode", criteria.PostalCode));
            xml.Add(new XElement("corpID", criteria.CorpId));

            return xml.ToString(SaveOptions.DisableFormatting);
        }

        private static void ParseCCIdMatch(Func<string> searchFunction, ClientSearchResult results)
        {
            var resultXml = searchFunction();
            XDocument xml = XDocument.Load(new System.IO.StringReader(resultXml));
            XElement node = xml.FirstNode as XElement;
            if (node == null)
                throw new UnexpectedServiceResultException("No response from service");
            else if (node.Name == "ERROR")
            {
                //Don't consider it an error if we get this message (it usually means that Id wasn't found (or exists in another db)
                if (!node.Value.Contains("Access key violation for userID"))
                    throw new UnexpectedServiceResultException(node.Value);
            }
            else
            {
                var client = new Client();
                results.Matches.Add(client);

                var clientNode = node.Element("client");
                if (clientNode != null)
                {
                    client.Id = long.Parse(GetNodeValue(clientNode, "client_id"));
                    client.ClientType = GetNodeValue(clientNode, "client_type");
                    client.TaxId = GetNodeValue(clientNode, "tax_id");

                    var segmentNode = clientNode.Element("group-owner-info-dO");
                    if (segmentNode != null)
                        client.Segment = GetNodeValue(segmentNode, "user-group-code");

                    foreach (var n in clientNode.Descendants("names"))
                    {
                        var name = new ClientName();
                        client.Names.Add(name);

                        name.SequenceNumber = int.Parse(GetAttributeValue(n, "seq_nbr"));
                        name.BusinessName = GetNodeValue(n, "name_business");
                        name.IsPrimary = (GetNodeValue(n, "primary_name_flag") == "Y");
                    }

                    var portfolio = clientNode.Element("portfolio_summary");
                    if (portfolio != null)
                    {
                        client.PortfolioId = long.Parse(GetNodeValue(portfolio, "portfolio_id"));
                        client.PortfolioClientCount = int.Parse(GetNodeValue(portfolio, "client_count"));
                        client.PorftolioName = GetNodeValue(portfolio, "portfolio_name");
                    }
                }

                foreach (var n in node.Descendants("address-do"))
                {
                    int tempInt;
                    var addr = new ClientAddress();
                    client.Addresses.Add(addr);

                    addr.Id = (int.TryParse(GetNodeValue(n, "address_id"), out tempInt)) ? tempInt : 0;
                    addr.HouseNumber = GetNodeValue(n, "house_nbr");
                    addr.Address1 = GetNodeValue(n, "address1");
                    addr.Address2 = GetNodeValue(n, "address2");
                    addr.City = GetNodeValue(n, "city");
                    addr.State = GetNodeValue(n, "state_prov_cd");
                    addr.PostalCode = GetNodeValue(n, "postal_code");
                    addr.County = GetNodeValue(n, "county");
                    addr.Country = GetNodeValue(n, "country_cd");
                    addr.IsPrimary = (GetNodeValue(n, "primary_address_flag") == "Y");
                }

                CorrectAddresses(client.Addresses);
            }
        }
        private static void ParseCCMatches(string resultXml, List<Client> matches)
        {
            XDocument xml = XDocument.Load(new System.IO.StringReader(resultXml));
            XElement node = xml.FirstNode as XElement;
            if (node == null)
                throw new UnexpectedServiceResultException("No response from service");
            else if (node.Name == "ERROR")
            {
                if (node.Value.Contains("Too many results returned"))
                    throw new TooManyResultsException("Too many results were returned.  Please narrow your search criteria");
                else
                    throw new UnexpectedServiceResultException(node.Value);
            }
            else
            {
                foreach (var r in node.Elements("TFGClientSearchResultXMLDO"))
                {
                    var client = new Client();
                    matches.Add(client);

                    var clientInfo = r.Element("client-info");
                    if (clientInfo != null)
                    {
                        client.Id = long.Parse(GetNodeValue(clientInfo, "client-iD"));
                        client.ClientType = GetNodeValue(clientInfo, "client_type");
                        client.TaxId = GetNodeValue(clientInfo, "tax_id");

                        var segmentNode = r.Element("group-owner-info-dO");
                        if (segmentNode != null)
                            client.Segment = GetNodeValue(segmentNode, "user-group-code");
                    }

                    foreach (var n in r.Element("names").Descendants("name"))
                    {
                        var name = new ClientName();
                        client.Names.Add(name);

                        var seqNumber = GetNodeValue(n, "seqNumber") ?? GetAttributeValue(n, "seq_nbr");
                        name.SequenceNumber = int.Parse(seqNumber);
                        name.BusinessName = GetNodeValue(n, "nameBusiness");
                        name.IsPrimary = (GetNodeValue(n, "primary_name_flag") == "Y");
                    }
                    foreach (var n in r.Element("addresses").Descendants("address"))
                    {
                        int tempInt;
                        var addr = new ClientAddress();
                        client.Addresses.Add(addr);

                        addr.Id = (int.TryParse(GetNodeValue(n, "address_id"), out tempInt)) ? tempInt : 0;
                        addr.HouseNumber = GetNodeValue(n, "house_nbr");
                        addr.Address1 = GetNodeValue(n, "address1");
                        addr.Address2 = GetNodeValue(n, "address2");
                        addr.City = GetNodeValue(n, "city");
                        addr.State = GetNodeValue(n, "state_prov_cd");
                        addr.PostalCode = GetNodeValue(n, "postal_code");
                        addr.Country = GetNodeValue(n, "country_cd");
                        addr.IsPrimary = (GetNodeValue(n, "primary_addr_flag") == "Y");
                    }

                    CorrectAddresses(client.Addresses);

                    var portfolio = r.Element("portfolio_summary");
                    if (portfolio != null)
                    {
                        client.PortfolioId = long.Parse(GetNodeValue(portfolio, "portfolio_id"));
                        client.PortfolioClientCount = int.Parse(GetNodeValue(portfolio, "client_count"));
                        client.PorftolioName = GetNodeValue(portfolio, "portfolio_name");
                    }
                }
            }
        }

        private static string GenerateClientXml(Client client)
        {
            DateTime now = DateTime.Now;
            XDocument xml = new XDocument();

            var root = new XElement("ClientImportData");
            xml.Add(root);
            root.Add(new XElement("Signature", new XElement("record_type", "CLIENT IMPORT"),
                                               new XElement("version-number", "1.0"),
                                               new XElement("Locale-Code", "en_US")));

            var c = new XElement("Client", new XElement("DuplicateClientOption", "1"),
                                           new XElement("ClientType", client.ClientType),
                                           new XElement("CorporationCode", client.CorporationCode),
                                           new XElement("VendorFlag", "N"),
                                           new XElement("CliEffectiveDate", ToClientCoreDateString(now)),
                                           new XElement("Owner", client.Segment),
                                           new XElement("Status", "A"));
            if (client.Id != null)
                c.Add(new XElement("CCClientId", client.Id));
            if (client.PortfolioId != null)
                c.Add(new XElement("PortfolioId", client.PortfolioId));

            root.Add(c);

            foreach (var n in client.Names)
            {
                var node = new XElement("NameInfo", new XElement("BusinessName", n.BusinessName),
                                                    new XElement("ClearanceNameType", n.NameType),
                                                    new XElement("DisplayFlag", (n.IsPrimary) ? "Y" : "N"));
                if (n.SequenceNumber.HasValue)
                    node.Add(new XElement("NameSeqNum", n.SequenceNumber));

                c.Add(node);
            }

            foreach (var a in client.Addresses)
            {
                var node = new XElement("Address", new XElement("DuplicateAddressOption", 1),
                                                   new XElement("AddrEffectiveDate", ToClientCoreDateString(now)),
                                                   new XElement("HouseNumber", a.HouseNumber),
                                                   new XElement("ADDR1", a.Address1),
                                                   new XElement("ADDR2", a.Address2),
                                                   new XElement("CityName", a.City),
                                                   new XElement("State", a.State),
                                                   new XElement("Zip", a.PostalCode),
                                                   new XElement("County", a.County),
                                                   new XElement("Country", a.Country),
                                                   new XElement("ClearanceAddressType", a.AddressType),
                                                   new XElement("DisplayFlag", (a.IsPrimary) ? "Y" : "N"));
                if (a.Id.HasValue)
                    node.Add(new XElement("CCAddressId", a.Id));

                c.Add(node);
            }

            if (!String.IsNullOrEmpty(client.TaxId))
                c.Add(new XElement("PersonalInfo", new XElement("TaxIdTypeCode", "FEIN"),
                                                   new XElement("TaxId", client.TaxId)));

            if (!String.IsNullOrEmpty(client.PhoneNumber))
                c.Add(new XElement("Communication", new XElement("Type", "CTWORKPHN"),
                                                    new XElement("Value", (new Regex(@"[^\d]")).Replace(client.PhoneNumber, "")),
                                                    new XElement("DisplayFlag", "Y")));

            return xml.ToString();
        }
        private static string GeneratePortfolioUpdateXml(long portfolioId, string portfolioName)
        {
            XDocument xml = new XDocument();
            xml.Add(new XElement("portfolio_list", new XElement("portfolio",
                                                                new XElement("portfolio_id", portfolioId),
                                                                new XElement("portfolio_name", portfolioName))));

            return xml.ToString();
        }
        
        public static ClientSaveResult UpdatePrimaryAddress(ClientSystemSettings settings, long clientId, long addressId)
        {
            ClientSaveResult results = new ClientSaveResult();
            try
            {
                using (var service = CreateWebServiceClient<ClientServiceSF.ClientCoreWebServiceClient, ClientServiceSF.ClientCoreWebService>(settings.Url, 60))
                    service.setPrimaryAddressFlag(clientId, addressId);
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }
        #endregion

        #region CMS Specific
        private static void ParseCMSMatches(System.Net.Http.HttpResponseMessage response, ClientSearchResult results)
        {
            if (!response.IsSuccessStatusCode)
            {
                //If it wasn't found, we just won't return anything; otherwise return an error
                if (response.StatusCode != System.Net.HttpStatusCode.NotFound)
                    throw new UnexpectedServiceResultException(String.Format("Client Get By Id Failed: {0} - {1}: {2}", (int)response.StatusCode, response.ReasonPhrase, response.Content.ReadAsStringAsync().Result));
            }
            else
            {
                XNamespace wrbd = "http://www.wrberkley.com/schema/data/datacomponents/1";
                XNamespace accord = "http://www.ACORD.org/standards/PC_Surety/ACORD1/xml/";
                XDocument xml = XDocument.Parse(response.Content.ReadAsStringAsync().Result);
                XElement node = xml.FirstNode as XElement;

                if (node == null)
                    throw new UnexpectedServiceResultException("No response from service");
                else
                {
                    foreach(var clientResource in node.Descendants(wrbd + "ClientResource"))
                    {
                        var client = new Client();
                        results.Matches.Add(client);

                        var iopNode = clientResource.Element(accord + "InsuredOrPrincipal");
                        if (iopNode != null)
                        {
                            var idInfoNode = iopNode.Element(accord + "ItemIdInfo");
                            if (idInfoNode != null)
                            {
                                client.Id = long.Parse(GetNodeValue(idInfoNode, accord + "InsurerId"));
                                client.CmsId = Guid.Parse(GetNodeValue(idInfoNode, accord + "SystemId"));
                            }

                            var generalPartyInfo = iopNode.Element(accord + "GeneralPartyInfo");
                            if (generalPartyInfo != null)
                            {
                                var nameInfo = generalPartyInfo.Element(accord + "NameInfo");
                                if (nameInfo != null)
                                {
                                    foreach (var n in nameInfo.Descendants(accord + "CommlName"))
                                    {
                                        var name = new ClientName();
                                        client.Names.Add(name);

                                        name.BusinessName = GetNodeValue(n, accord + "CommercialName");

                                        var wrbdCommlName = n.Element(wrbd + "CommlName");
                                        if (wrbdCommlName != null)
                                        {
                                            name.CmsId = Guid.Parse(GetNodeValue(wrbdCommlName.Element(accord + "ItemIdInfo"), accord + "SystemId"));
                                            name.IsPrimary = (GetNodeValue(wrbdCommlName, accord + "PrimaryInd") == "1");
                                        }
                                    }

                                    foreach (var n in nameInfo.Descendants(accord + "SupplementaryNameInfo"))
                                    {
                                        var name = new ClientName();
                                        client.Names.Add(name);

                                        name.BusinessName = GetNodeValue(n, accord + "SupplementaryName");
                                        name.NameType = GetNodeValue(n, accord + "SupplementaryNameCd");

                                        var wrbdSupplementaryNameInfo = n.Element(wrbd + "SupplementaryNameInfo");
                                        if (wrbdSupplementaryNameInfo != null)
                                        {
                                            name.CmsId = Guid.Parse(GetNodeValue(wrbdSupplementaryNameInfo.Element(accord + "ItemIdInfo"), accord + "SystemId"));
                                            name.IsPrimary = (GetNodeValue(wrbdSupplementaryNameInfo, accord + "PrimaryInd") == "1");
                                        }
                                    }

                                    foreach (var n in nameInfo.Descendants(accord + "TaxIdentity"))
                                    {
                                        var typeCode = GetNodeValue(n, accord + "TaxIdTypeCd");
                                        switch (typeCode)
                                        {
                                            case "FEIN":
                                                client.TaxId = GetNodeValue(n, accord + "TaxId");
                                                break;
                                        }
                                    }
                                    foreach (var n in nameInfo.Descendants(accord + "NonTaxIdentity"))
                                    {
                                        var typeCode = GetNodeValue(n, accord + "NonTaxIdTypeCd");
                                        switch (typeCode)
                                        {
                                            case "Experian":
                                                client.ExperianId = GetNodeValue(n, accord + "NonTaxId");
                                                break;
                                        }
                                    }
                                }

                                foreach (var n in generalPartyInfo.Descendants(accord + "Addr"))
                                {
                                    var addr = new ClientAddress();
                                    client.Addresses.Add(addr);

                                    var itemIdInfo = n.Element(accord + "ItemIdInfo");
                                    if (itemIdInfo != null)
                                    {
                                        addr.Id = int.Parse(GetNodeValue(itemIdInfo, accord + "InsurerId"));
                                        addr.CmsId = Guid.Parse(GetNodeValue(itemIdInfo, accord + "SystemId"));
                                    }

                                    addr.Address1 = GetNodeValue(n, accord + "Addr1");
                                    addr.Address2 = GetNodeValue(n, accord + "Addr2");
                                    addr.City = GetNodeValue(n, accord + "City");
                                    addr.State = GetNodeValue(n, accord + "StateProvCd");
                                    addr.PostalCode = GetNodeValue(n, accord + "PostalCode");
                                    addr.County = GetNodeValue(n, accord + "County");
                                    addr.Country = GetNodeValue(n, accord + "Country");
                                    addr.IsPrimary = (GetNodeValue(n.Element(wrbd + "Addr"), accord + "PrimaryInd") == "1");
                                }

                                var communications = generalPartyInfo.Element(accord + "Communications");
                                if (communications != null)
                                {
                                    foreach (var n in generalPartyInfo.Descendants(accord + "PhoneInfo"))
                                    {
                                        var comm = new ClientCommunication();
                                        comm.Id = Guid.Parse(GetNodeValue(n.Element(wrbd + "PhoneInfo").Element(accord + "ItemIdInfo"), accord + "SystemId"));
                                        comm.Type = GetNodeValue(n, accord + "PhoneTypeCd");
                                        comm.Value = GetNodeValue(n, accord + "PhoneNumber");
                                        client.Communications.Add(comm);
                                    }
                                }
                            }

                            var wrbdIopNode = iopNode.Element(wrbd + "InsuredOrPrincipal");
                            if (wrbdIopNode != null)
                            {
                                var portfolioInfoNode = wrbdIopNode.Element(wrbd + "ClientPortfolioInfo");
                                if (portfolioInfoNode != null)
                                {
                                    client.PortfolioId = long.Parse(GetNodeValue(portfolioInfoNode.Element(accord + "ItemIdInfo"), accord + "InsurerId"));
                                    client.PortfolioCmsId = Guid.Parse(GetNodeValue(portfolioInfoNode.Element(accord + "ItemIdInfo"), accord + "SystemId"));
                                    client.PorftolioName = GetNodeValue(portfolioInfoNode, wrbd + "ClientPortfolioName");
                                    //    client.PortfolioClientCount = int.Parse(GetNodeValue(portfolio, "client_count"));
                                }
                            }

                            //var segmentNode = clientNode.Element("group-owner-info-dO");
                            //if (segmentNode != null)
                            //    client.Segment = GetNodeValue(segmentNode, "user-group-code");
                        }

                        CorrectAddresses(client.Addresses);
                    }
                }
            }
        }
        #endregion

        #region Search
        public static ClientSearchResult Search(ClientSystemSettings clientCoreSettings, ClientSearchCriteria criteria, int timeOut)
        {
            ClientSearchResult results = new ClientSearchResult();
            try
            {
                if (clientCoreSettings.UsesCmsService)
                {
                    using (var client = CreateHttpClient(clientCoreSettings.Url, timeOut))
                    {
                        client.DefaultRequestHeaders.Add("DSIK", clientCoreSettings.DSIK);
                        HttpResponseMessage response = null;
                        var query = HttpUtility.ParseQueryString(String.Empty);
                        
                        if (criteria.IsClientIdSearch)
                        {
                            Guid cmsId;
                            var isCmdId = Guid.TryParse(criteria.ClientId, out cmsId);
                            response = client.GetAsync("esb/CMS/services/rest/clients" + ((!isCmdId) ? "/clientNumber/" : "/") + criteria.ClientId).Result;
                        }
                        //else if(!String.IsNullOrEmpty(criteria.TaxId))
                        //{
                            
                        //}
                        else
                        {
                            if (!String.IsNullOrEmpty(criteria.BusinessName))
                                query["CommercialName"] = criteria.BusinessName;
                            if (!String.IsNullOrEmpty(criteria.Address1))
                                query["Addr1"] = criteria.Address1;
                            if (!String.IsNullOrEmpty(criteria.CityName))
                                query["City"] = criteria.CityName;
                            if (!String.IsNullOrEmpty(criteria.StateName))
                                query["StateProvCd"] = criteria.StateName;
                            if (!String.IsNullOrEmpty(criteria.PostalCode))
                                query["PostalCode"] = criteria.PostalCode;

                            response = client.GetAsync("esb/CMS/services/rest/clients/names/addresses?" + query.ToString()).Result;
                        }

                        ParseCMSMatches(response, results);
                    }
                }
                else
                {
                    var serviceProcessingOptions = new List<ClientServiceSF.processingOption>();
                    if (clientCoreSettings.SearchFilters != null)
                        foreach (var p in clientCoreSettings.SearchFilters)
                            serviceProcessingOptions.Add((ClientServiceSF.processingOption)Enum.Parse(typeof(ClientServiceSF.processingOption), p));

                    string basicXml = GenerateBasicXml(clientCoreSettings);

                    if (clientCoreSettings.UsesPortfolioService)
                    {
                        using (var service = CreateWebServiceClient<ClientServiceSF.ClientCoreWebServiceClient, ClientServiceSF.ClientCoreWebService>(clientCoreSettings.Url, timeOut))
                        {
                            if (criteria.IsClientIdSearch)
                            {
                                var searchXml = GenerateIdSearchXml(criteria.ClientId, criteria.CorpId, criteria.EffectiveDate);
                                ParseCCIdMatch((() => service.getClientInfo(basicXml, searchXml, serviceProcessingOptions.ToArray())), results);
                            }
                            else if (criteria.IsPortfolioSearch)
                                ParseCCMatches(service.performPortfolioSearch(basicXml, GeneratePortfolioSearchXml(criteria)), results.Matches);
                            else
                                ParseCCMatches(service.performClientSearch(basicXml, GenerateStandardSearchXml(criteria), serviceProcessingOptions.ToArray()), results.Matches);
                        }
                    }
                    else
                    {
                        using (var service = CreateWebServiceClient<ClientCoreWebService.ClientCoreWebServiceClient, ClientCoreWebService.ClientCoreWebService>(clientCoreSettings.Url, timeOut))
                        {
                            if (criteria.IsClientIdSearch)
                            {
                                var searchXml = GenerateIdSearchXml(criteria.ClientId, criteria.CorpId, criteria.EffectiveDate);
                                ParseCCIdMatch((() => service.getClientInfo(basicXml, searchXml)), results);
                            }
                            else
                                ParseCCMatches(service.performClientSearch(basicXml, GenerateStandardSearchXml(criteria)), results.Matches);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }
        public static ClientPhoneNumberSearchResult PhoneNumberSearch(AdvancedClientSettings advClientSettings, string phoneNumber)
        {
            ClientPhoneNumberSearchResult results = new ClientPhoneNumberSearchResult();
            try
            {
                using (var service = CreateWebServiceClient<AdvClientSearchWS.advClientSearchServiceClient, AdvClientSearchWS.advClientSearchService>(advClientSettings.Url, 15))
                {
                    phoneNumber = phoneNumber.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
                    var phSearchResults = service.performClientSearchBasedOnPhoneNumber(advClientSettings.ToSecurityToken(), new AdvClientSearchWS.performClientSearchBasedOnPhoneNumber() { phoneNumber = phoneNumber, function = "test" });

                    if (phSearchResults != null)
                    {
                        foreach (var ci in phSearchResults.ClienInfo)
                        {
                            if (!String.IsNullOrEmpty(ci.ClientID))
                                results.ClientCoreIds.Add(long.Parse(ci.ClientID));
                            else
                            {
                                if (ci.InsuredOrPrincipal != null && ci.InsuredOrPrincipal.GeneralPartyInfo != null)
                                {
                                    var gp = ci.InsuredOrPrincipal.GeneralPartyInfo;
                                    if (gp.NameInfo != null && gp.NameInfo.Length > 0 && gp.NameInfo[0].CommlName != null && gp.NameInfo[0].CommlName.CommercialName != null)
                                    {
                                        var business = results.MatchedBusiness = new ClientPhoneNumberBusiness();
                                        business.Name = gp.NameInfo[0].CommlName.CommercialName.Value;

                                        if (gp.Addr != null && gp.Addr.Length > 0)
                                        {
                                            var a = gp.Addr[0];
                                            business.Address1 = (a.Addr1 != null && a.Addr1.Value != null) ? a.Addr1.Value : null;
                                            business.Address2 = (a.Addr2 != null && a.Addr2.Value != null) ? a.Addr2.Value : null;
                                            business.City = (a.City != null && a.City.Value != null) ? a.City.Value : null;
                                            business.State = (a.StateProv != null && a.StateProv.Value != null) ? a.StateProv.Value : null;
                                            business.PostalCode = (a.PostalCode != null && a.PostalCode.Value != null) ? a.PostalCode.Value : null;
                                            business.Country = (a.Country != null && a.Country.Value != null) ? a.Country.Value : null;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }

        private static void CorrectAddresses(List<ClientAddress> addresses)
        {
            //Client core data regarding primary address is unreliable: make sure there's only one, and if there's none set it to the first
            if (addresses.Count > 0)
            {
                var primaryAddressCount = addresses.Count(a => a.IsPrimary);
                if (primaryAddressCount == 0)
                    addresses[0].IsPrimary = true;
                else if (primaryAddressCount > 1)
                {
                    var primary = addresses.First(a => a.IsPrimary);
                    foreach (var a in addresses)
                        a.IsPrimary = (a == primary);
                }
            }
        }

        private static string GetNodeValue(XElement parentNode, XName nodeName)
        {
            var node = (parentNode == null) ? null : parentNode.Element(nodeName);
            return (node == null) ? null : node.Value;
        }
        private static string GetAttributeValue(XElement node, string attributeName)
        {
            var attr = node.Attribute(attributeName);
            return (attr == null) ? null : attr.Value;
        }
        #endregion

        public static ClientSaveResult Save(ClientSystemSettings settings, Client client, string corpId)
        {
            ClientSaveResult results = new ClientSaveResult();
            string resultXml = null;
            try
            {
                if (settings.UsesCmsService)
                {
                    //Populate results.CmsId and results.ClientCoreId
                }
                else
                {
                    string clientXml = GenerateClientXml(client);
                    string basicXml = GenerateBasicXml(settings);
                    if (settings.UsesPortfolioService)
                        using (var service = CreateWebServiceClient<ClientServiceSF.ClientCoreWebServiceClient, ClientServiceSF.ClientCoreWebService>(settings.Url, 60))
                            resultXml = service.importClient(basicXml, clientXml);
                    else
                        using (var service = CreateWebServiceClient<ClientCoreWebService.ClientCoreWebServiceClient, ClientCoreWebService.ClientCoreWebService>(settings.Url, 60))
                            resultXml = service.importClient(basicXml, clientXml);

                    if (!String.IsNullOrEmpty(resultXml))
                    {
                        var resultDoc = XDocument.Parse(resultXml);
                        XElement node = resultDoc.FirstNode as XElement;
                        if (node == null)
                            throw new UnexpectedServiceResultException("No response from service");
                        else if (node.Name == "ERROR")
                            throw new UnexpectedServiceResultException(node.Value);
                        else
                        {
                            var id = GetNodeValue(node, "big-decimal");
                            results.Id = long.Parse(id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }
        public static BaseServiceCallResult UpdatePortfolioName(ClientSystemSettings settings, long portfolioId, string portfolioName)
        {
            BaseServiceCallResult results = new BaseServiceCallResult();
            string resultXml = null;
            try
            {
                string basicXml = GenerateBasicXml(settings);
                string updateXml = GeneratePortfolioUpdateXml(portfolioId, portfolioName);

                using (var service = CreateWebServiceClient<ClientServiceSF.ClientCoreWebServiceClient, ClientServiceSF.ClientCoreWebService>(settings.Url, 60))
                {
                    resultXml = service.updatePortfolio(basicXml, updateXml);
                    if ((resultXml ?? "").Contains("ERROR"))
                        throw new Exception(String.Format("Failed to update Portfolio Name: {0}", resultXml));
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }

        public static OFACSearchResults SearchOFAC(AdvancedClientSettings settings, string searchDescription, OFACSearchCriteria criteria)
        {
            var results = new OFACSearchResults();
            try
            {
                using (var service = CreateWebServiceClient<AdvClientSearchWS.advClientSearchServiceClient, AdvClientSearchWS.advClientSearchService>(settings.Url, 30))
                {
                    var advCriteria = new AdvClientSearchWS.performOFACSearch()
                    {
                        name = criteria.Name,
                        address = criteria.Address,
                        city = criteria.City,
                        state = criteria.State,
                        zip = criteria.PostalCode,
                        country = criteria.Country,
                        function = searchDescription
                    };

                    var ofacResponse = service.performOFACSearch(settings.ToSecurityToken(), advCriteria);
                    if (ofacResponse != null && ofacResponse.OFACResults != null && ofacResponse.OFACResults.OFACHit != null)
                    {
                        foreach (var hit in ofacResponse.OFACResults.OFACHit)
                        {
                            var match = new OFACSearchMatch() { Score = hit.Score };
                            if (hit.EffectiveDt != null)
                                match.EffectiveDate = hit.EffectiveDt.Value;
                            if (hit.NameInfo != null && hit.NameInfo.PersonName != null && hit.NameInfo.PersonName.GivenName != null)
                                match.Name = hit.NameInfo.PersonName.GivenName.Value;

                            results.Matches.Add(match);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Exception = ex;
            }

            return results;
        }

        private static string ToClientCoreDateString(DateTime date)
        {
            return (date.Year * 1000 + date.DayOfYear).ToString();
        }
        private static DateTime FromClientCoreDateString(string date)
        {
            int value = int.Parse(date);
            return (new DateTime(value / 1000, 1, 1)).AddDays((value % 1000) - 1);
        }
    }
}
