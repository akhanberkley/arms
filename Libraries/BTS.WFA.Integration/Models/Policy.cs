﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace BTS.WFA.Integration.Models
{
    public class PolicySystemResults : BaseServiceCallResult
    {
        public PolicySystemResults()
        {
            Policies = new List<Policy>();
        }

        public List<Policy> Policies { get; set; }
    }

    /// <summary>
    /// Represents the PolicyAuditDO
    /// </summary>
    public class Policy
    {
        public Policy()
        {
            Agency = new PDRAgency();
            CustomDetails = new List<PDRCustomDetail>();
            NamedInsureds = new List<PDRNamedInsured>();
            PolicyAddresses = new List<PDRAddress>();
            Products = new List<PDRSecondaryProduct>();
        }

        public PDRAgency Agency { get; set; }
        //public DateTime? AnniversaryRateDate { get; set; }
        //public string BillingIdNbr { get; set; }
        public string BranchCode { get; set; }
        public string BusinessDescription { get; set; }
        public string BusinessType { get; set; }
        public DateTime? CancelDate { get; set; }
        public string Carrier { get; set; }
        public string ClientId { get; set; }
        public List<PDRCustomDetail> CustomDetails { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string EntityCode { get; set; }
        public string EntityInsured { get; set; }
        public string EntityName { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string Fein { get; set; }
        public string LineOfBusiness { get; set; }
        public List<PDRNamedInsured> NamedInsureds { get; set; }
        public List<PDRAddress> PolicyAddresses { get; set; }
        public string Id { get; set; }
        public string Number { get; set; }
        public string SequenceNumber { get; set; }
        public string Status { get; set; }
        public string Symbol { get; set; }
        public List<PDRSecondaryProduct> Products { get; set; }
        //public string ProfitCenter { get; set; }
        //public string RiskItem { get; set; }
        public string TotalEstimatedPremium { get; set; }
        //public DateTime? TransactionProcessDate { get; set; }
        public List<PDRUnit> Units { get; set; }
        public string NonRenewableFlag { get; set; }
        public string ClientSequenceNumber { get; set; }
        public string OwnerLegalName { get; set; }
        public string UnderwriterCode { get; set; }
        //public List<PDRNotes> Notes { get; set; }
        public string PortfolioId { get; set; }
        public string AuditFlag { get; set; }
        public long CorpId { get; set; }
        public string BranchName { get; set; }
        public string SicCode { get; set; }
        public string UnderwriterId { get; set; }
        public string UnderwriterName { get; set; }
        //public string PrimaryProductState { get; set; }
        public string CompnayName { get; set; }
        public string CancelPendingAuditFlag { get; set; }
        public DateTime? StatusEffectiveDate { get; set; }
        public string BillType { get; set; }
    }
    
    /// <summary>
    /// Represents a PDRAddressDO
    /// </summary>
    public class PDRAddress
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public long Id { get; set; }
        public string BillingFlag { get; set; }
        public string City { get; set; }
        public string ClientId { get; set; }
        public string County { get; set; }
        public string HouseNumber { get; set; }
        public long InternalAddressId { get; set; }
        public string MailingFlag { get; set; }
        public string PostalCode { get; set; }
        public string PrimaryFlag { get; set; }
        public string StateProvCode { get; set; }
    }

    /// <summary>
    /// Represents a AgencyDO
    /// </summary>
    public class PDRAgency
    {
        public PDRAgency()
        {
            Address = new PDRAddress();
        }

        public PDRAddress Address { get; set; }
        public int BranchOfficeId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string PhoneNumber { get; set; }
        public string TaxNumber { get; set; }        
    }

    /// <summary>
    /// Represents a PDRCoverageDO
    /// </summary>
    public class PDRCoverage
    {
        public PDRCoverage()
        {
            CustomDetails = new List<PDRCustomDetail>();
        }

        /*
        ◾accumulatedF - nillable; type string
        ◾actExpAmt type double
        ◾adjActExpAmt type double
        ◾adjAvgExpAmt type double
        ◾autoSymbol1C - nillable; type string
        ◾autoSymbol1Name - nillable; type string
        ◾autoSymbol2C - nillable; type string
        ◾autoSymbol2Name - nillable; type string
        ◾autoSymbol3C - nillable; type string
        ◾autoSymbol3Name - nillable; type string
        ◾autoSymbol4C - nillable; type string
        ◾autoSymbol4Name - nillable; type string
        ◾balToMinPremM type double
        ◾billToDPremM type double
        ◾calcPremM type double
        */
        public string ClassCodeCov { get; set; }
        public string ClassCodeDesc { get; set; }
        public string ClassCodeOvr { get; set; }
        /*
        ◾classItemNumber type long
        ◾commissionM type double
        ◾compositePremM type double
        */
        public List<PDRCoverage> Coverages { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public long Id { get; set; }
        public double DeductableAmount1 { get; set; }
        public double DeductableAmount2 { get; set; }
        public double DeductableAmount3 { get; set; }
        public string DeductableTypeName1 { get; set; }
        public string DeductableTypeName2 { get; set; }
        public string DeductableTypeName3 { get; set; }
        /*
        ◾deductValTypId1 type long
        ◾deductValTypId2 type long
        ◾deductValTypId3 type long
        ◾depositAmt type double
        ◾enteredRate type double
        */
        public double EstimatedExposureAmmount { get; set; }
        /*
        ◾finalRate type double
        ◾flatChargeF - nillable; type string
        */
        public string FullEarnedPremiumFlag { get; set; }
        /*
        ◾itemCodeOrText - nillable; type string
        */
        public double LimitAmount1 { get; set; }
        public double LimitAmount2 { get; set; }
        public double LimitAmount3 { get; set; }
        /*
        ◾limitTypeName1 - nillable; type string
        ◾limitTypeName2 - nillable; type string
        ◾limitTypeName3 - nillable; type string
        ◾limitValTypId1 type long
        ◾limitValTypId2 type long
        ◾limitValTypId3 type long
        ◾manualRateF - nillable; type string
        ◾minTermPremM type double
        ◾parentCoverageCode - nillable; type string
        ◾parentCoverageDesc - nillable; type string
        ◾parentCoverageId type long
        ◾premCoverageM type double
        ◾premRollUpAccum type double
        ◾premTotalCovM type double
        ◾provisionalP type double
        ◾ratingMethodC - nillable; type string
        ◾ratingMethodName - nillable; type string
        ◾rdfR type double
        ◾rptBasisName - nillable; type string
        ◾rptgAvgApplyF - nillable; type string
        ◾rptgBasisC - nillable; type string
        ◾rptgPeriodC - nillable; type string
        ◾rptgPeriodDay type long
        ◾rptgPeriodName - nillable; type string
        */
        public long SequenceNumber { get; set; }
        public string SpecialCoverageCode { get; set; }
        public string StateCode { get; set; }
        /*
        ◾subClassCodeCov - nillable; type string
        ◾subClassCodeOvr - nillable; type string
        ◾subLine - nillable; type string
        ◾tfsCommsnM type double
        ◾tfsPremM type double
        ◾unitcoverageId type long
        ◾usePolicyCovF - nillable; type string
        ◾premiumBasis - nillable; type string
        ◾auditableClassCodeF - nillable; type string
        ◾rateBasis - nillable; type string
        */
        public List<PDRCustomDetail> CustomDetails { get; set; }
    }

    /// <summary>
    /// Represents a PDRCustomDetailDO
    /// </summary>
    public class PDRCustomDetail
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public int? DomTbId { get; set; }
    }

    /// <summary>
    /// Represents a PDRFormDO
    /// </summary>
    public class PDRForm
    {
        public long DefaultId { get; set; }
        public string Description { get; set; }
        public long DomId { get; set; }
        public string Number { get; set; }
        //public List<PDROfficer> Officer { get; set; }
        public string StateCode { get; set; }
        public long UnitFormId { get; set; }
        //public List<PDRFormVariableColumn> VariableColumns { get; set; }
    }

    /// <summary>
    /// Represents a PDRNamedInsuredDO
    /// </summary>
    public class PDRNamedInsured
    {
        public PDRNamedInsured()
        {
            Addresses = new List<PDRAddress>();
            Names = new List<PDRName>();
        }

        public long Id { get; set; }
        public string PrimaryNamedInsuredFlag { get; set; }
        public List<PDRAddress> Addresses { get; set; }
        public List<PDRName> Names { get; set; }
    }

    /// <summary>
    /// Represents a PDRNameDO
    /// </summary>
    public class PDRName
    {
        public string ClientCategory { get; set; }
        public long ClientId { get; set; }
        public long ClientSequenceNumber { get; set; }
        public string ClientType { get; set; }
        public long Id { get; set; }
        public string BusinessName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public string Type { get; set; }
        public string PrimaryNameFlag { get; set; }
        public List<PDRCommunication> Communications { get; set; }
        //public string InternalStateIdT { get; set; }
        public string DocketNumber { get; set; }
        public string TaxId { get; set; }
        //public string TaxIdType { get; set; }
        
    }

    /// <summary>
    /// Represents a RateModFactor
    /// </summary>
    public class PDRRateModFactor
    {
        public string Code { get; set; }
        public string Factor { get; set; }
        public string State { get; set; }
        public string CustomeDetailDomId { get; set; }
    }

    /// <summary>
    /// Represents a SecondaryProductDO
    /// </summary>
    public class PDRSecondaryProduct
    {
        public string ClaimsMade { get; set; }
        public double CommissionM { get; set; }
        public string CompanyCode { get; set; }
        public long CompanyId { get; set; }
        public string CompanyName { get; set; }
        public List<PDRCoverage> Coverages { get; set; }
        public string EntityCode { get; set; }
        public string EntityName { get; set; }
        public double ExperienceLiabP { get; set; }
        public double ExperienceMiscP { get; set; }
        public double ExperiencePdP { get; set; }
        public double FeesTotalM { get; set; }
        public string FleetFlag { get; set; }
        public List<PDRForm> Forms { get; set; }
        /*
        ◾lobComboF - nillable; type string
        ◾lobFamiliesC - nillable; type string
        ◾lobFamiliesName - nillable; type string
        ◾lobHazardTypeC - nillable; type string
        ◾lobHazardTypeName - nillable; type string
        */
        public string LobHazardDescription { get; set; }
        /*
        ◾lobSpecificTypC - nillable; type string
        ◾lobSpecificTypName - nillable; type string
        ◾logicalTableC - nillable; type string
        ◾logicalTableName - nillable; type string
        ◾multPolDiscF - nillable; type string
        ◾multPolDiscFctr type double
        ◾noteAttachedF - nillable; type string
        ◾origEffD - nillable; type string
        ◾overviewTotM type double
        */
        public string PlanCode { get; set; }
        public long PlanId { get; set; }
        public string PlanName { get; set; }
        public double PremiumTotalM { get; set; }
        public string Code { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public string ProgramCode { get; set; }
        public long ProgramId { get; set; }
        public string ProgramName { get; set; }
        public string ProvisionalRateFactor { get; set; }
        public List<PDRRateModFactor> RateModFactors { get; set; }
        /*
        ◾ratingMethodC - nillable; type string
        ◾ratingMethodName - nillable; type string
        ◾reinsuranceTypeC - nillable; type string
        ◾scheduleLiabP type double
        ◾scheduleMiscP type double
        ◾schedulePdP type double
        ◾stateCode - nillable; type string
        ◾surchargeTotalM type double
        ◾sysadminProcessD - nillable; type string
        ◾taxTotalM type double
        ◾termAdjustFactor type double
        ◾tfsCommsnM type double
        ◾totVhInFleetLb type long
        ◾totVhInFleetPd type long
        ◾unitcommonId type long
        */
        public List<PDRUnit> Units { get; set; }
    }

    /// <summary>
    /// Represents a PDRUnitDO
    /// </summary>
    public class PDRUnit
    {
        public PDRUnit()
        {
            //Coverages = new List<PDRCoverage>();
            //Units = new List<PDRUnit>();
            //CustomDetails = new List<PDRCustomDetail>();
        }

        public PDRAddress Address { get; set; }
        /*
        ◾basicPremM type double
        ◾bceg - nillable; type string
        ◾bldgValuationC - nillable; type string
        ◾bldgValuationName - nillable; type string
        ◾blockT - nillable; type string
        ◾bodyTypeT - nillable; type string
        ◾causeOfLossC - nillable; type string
        ◾causeOfLossName - nillable; type string
        ◾cityFeesM type double
        ◾cityTaxM type double
        ◾classCodeUnit - nillable; type string
        ◾commissionM type double
        ◾commsnMinAdjM type double
        ◾constructionC - nillable; type string
        ◾constructionName - nillable; type string
        ◾countyFeesM type double
        ◾countyTaxM type double
        ◾coverage - nillable; type ArrayOfPDRCoverageDO // <------------------
        */
        public List<PDRCoverage> Coverages { get; set; }
        /*
        ◾dwellingTypeC - nillable; type string
        ◾dwellingTypeName - nillable; type string
        */
        public string EntityC { get; set; }
        public string EntityName { get; set; }
        /*
        ◾feesTotalM type double
        ◾groupIiSymbol - nillable; type string
        ◾guarantyM type double
        ◾identificationTxt - nillable; type string
        ◾lobDate1D type long
        ◾lobDate2D type long
        ◾lobUnitFactorR type double
        ◾logicalTableC - nillable; type string
        ◾logicalTableName - nillable; type string
        ◾lotT - nillable; type string
        ◾makeT - nillable; type string
        ◾modelT - nillable; type string
        ◾nbrOfStories type long
        ◾noteAttachedF - nillable; type string
        ◾numberOfAcresI type double
        ◾occupancyC - nillable; type string
        ◾occupancyName - nillable; type string
        ◾occupancyP type double
        ◾origEffD - nillable; type string
        ◾overviewTotM type double
        ◾parentOviewTotM type double
        ◾parentShrDataId type long
        ◾planCode - nillable; type string
        ◾planId type long
        ◾planName - nillable; type string
        ◾premMinAdjM type double
        ◾premTotalM type double
        ◾primarySecondryC - nillable; type string
        ◾primarySecondryName - nillable; type string
        */
        public long ProductId { get; set; }
        /*
        ◾productUnitcommonId type long
        ◾protClassOvrF - nillable; type string
        ◾protClassPivF - nillable; type string
        */
        public string ProtectionClass { get; set; }
        /*
        ◾provisionalRateF - nillable; type string
        ◾rangeT - nillable; type string
        ◾ratingMethodC - nillable; type string
        ◾ratingMethodName - nillable; type string
        ◾riskNbr type long
        ◾sectionT - nillable; type string
        ◾shrDataId type long
        */
        public string SprinkleredFlag { get; set; }
        public string StateCode { get; set; }
        /*
        ◾subClassCodeUnt - nillable; type string
        ◾subDivisionT - nillable; type string
        ◾surchargeM type double
        ◾symbolOvrF - nillable; type string
        ◾symbolPivF - nillable; type string
        ◾taxTotalM type double
        ◾termAdjustFactor type double
        ◾townshipT - nillable; type string
        */
        public List<PDRUnit> Units { get; set; }
        /*
        ◾unitcommonId type long
        */
        public string Description { get; set; }
        public int Year { get; set; }
        public string VehicleSymbolCode { get; set; }
        public string VehicleSymbolName { get; set; }
        public string VinVerifyFlag { get; set; }
        public List<PDRCustomDetail> CustomDetails { get; set; }
        public long AttachNumber { get; set; }
        public string ProtectiveSafeGuard { get; set; }
        /*
        ◾contractorsEquipmentCoverageRows - nillable; type ArrayOfContractorsEquipmentCoverageRow // <------------------
        */
    }

    /// <summary>
    /// Represents a PDRCommunicationDO
    /// </summary>
    public class PDRCommunication
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }

    /// <summary>
    /// Represents a PDRNotesDO
    /// </summary>
    //public class PDRNote
    //{
    //}
}