﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BTS.WFA.Integration.Models
{
    public class ClientSearchCriteria
    {
        [StringifyIgnoreAttribute]
        public string CorpId { get; set; }
        [StringifyIgnoreAttribute]
        public string ClientType { get; set; }

        public string ClientId { get; set; }
        public DateTime? EffectiveDate { get; set; }

        public string TaxId { get; set; }
        public string TaxIdType { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string BusinessName { get; set; }

        public string HouseNumber { get; set; }
        public string Address1 { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string CountyName { get; set; }
        public string CountyId { get; set; }
        public string PostalCode { get; set; }

        public long? PortfolioId { get; set; }
        public string PortfolioName { get; set; }

        [StringifyIgnoreAttribute]
        public bool IsClientIdSearch { get { return !String.IsNullOrEmpty(ClientId); } }
        [StringifyIgnoreAttribute]
        public bool IsPortfolioSearch { get { return PortfolioId.HasValue || !String.IsNullOrEmpty(PortfolioName); } }

        public string Stringify()
        {
            List<string> criteria = new List<string>();
            foreach (var prop in this.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(StringifyIgnoreAttribute))))
            {
                var value = prop.GetGetMethod().Invoke(this, null);
                if(value != null)
                {
                    var name = Regex.Replace(prop.Name, "([a-z])([A-Z])", "$1 $2");
                    criteria.Add(name + ": " + value.ToString().Replace("*", ""));
                }
            }

            return String.Join(", ", criteria);
        }
    }

    public class StringifyIgnoreAttribute : Attribute { }

    public class ClientPhoneNumberSearchResult : BaseServiceCallResult
    {
        public ClientPhoneNumberSearchResult()
        {
            ClientCoreIds = new List<long>();
        }

        public List<long> ClientCoreIds { get; set; }
        public ClientPhoneNumberBusiness MatchedBusiness { get; set; }
    }

    public class ClientPhoneNumberBusiness
    {
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }

    public class ClientSearchResult : BaseServiceCallResult
    {
        public ClientSearchResult()
        {
            Matches = new List<Client>();
        }

        public List<Client> Matches { get; set; }
    }

    public class ClientSaveResult : BaseServiceCallResult
    {
        public long? Id { get; set; }
        public Guid? CmsId { get; set; }
    }

    public class Client
    {
        public Client()
        {
            Names = new List<ClientName>();
            Addresses = new List<ClientAddress>();
            Communications = new List<ClientCommunication>();
        }

        public long? Id { get; set; }
        public Guid? CmsId { get; set; }
        public string Segment { get; set; }
        public string CorporationCode { get; set; }
        public string ClientType { get; set; }
        public string TaxId { get; set; }
        public string ExperianId { get; set; }
        public string PhoneNumber { get; set; }

        public long? PortfolioId { get; set; }
        public Guid? PortfolioCmsId { get; set; }
        public int? PortfolioClientCount { get; set; }
        public string PorftolioName { get; set; }

        public List<ClientName> Names { get; set; }
        public List<ClientAddress> Addresses { get; set; }
        public List<ClientCommunication> Communications { get; set; }
    }

    public class ClientName
    {
        public bool IsPrimary { get; set; }
        public int? SequenceNumber { get; set; }
        public Guid? CmsId { get; set; }
        public string NameType { get; set; }
        public string BusinessName { get; set; }
    }

    public class ClientAddress
    {
        public bool IsPrimary { get; set; }
        public int? Id { get; set; }
        public Guid? CmsId { get; set; }
        public string AddressType { get; set; }
        public string HouseNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
    }

    public class ClientCommunication
    {
        public Guid Id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
    }
}