﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.Integration.Models
{
    public class TooManyResultsException : Exception
    {
        public TooManyResultsException(string message) : base(message) { }
    }
    public class UnexpectedServiceResultException : Exception
    {
        public UnexpectedServiceResultException(string message) : base(message) { }
    }
}
