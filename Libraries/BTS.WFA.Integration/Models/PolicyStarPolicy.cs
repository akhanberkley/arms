﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.Integration.Models
{
    public class PolicyStarPolicyResults : BaseServiceCallResult
    {
        public PolicyStarPolicyResults()
        {
            Policies = new List<PolicyStarPolicy>();
        }

        public List<PolicyStarPolicy> Policies { get; set; }
    }

    public class PolicyStarPolicy
    {
        public string PolicyNumber { get; set; }
        public string PolicyMod { get; set; }
        
        public string Status { get; set; }
        public DateTime StatusEffectiveDate { get; set; }
        
        public bool CancelPendingAudit { get; set; }
        public DateTime? CancelDate { get; set; }

        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public decimal? Premium { get; set; }
    }
}