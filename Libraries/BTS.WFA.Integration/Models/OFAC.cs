﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.Integration.Models
{
    public class OFACSearchCriteria
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }

    public class OFACSearchResults : BaseServiceCallResult
    {
        public OFACSearchResults()
        {
            Matches = new List<OFACSearchMatch>();
        }

        public List<OFACSearchMatch> Matches { get; set; }
    }

    public class OFACSearchMatch
    {
        public string Name { get; set; }
        public string EffectiveDate { get; set; }
        public int Score { get; set; }
    }
}
