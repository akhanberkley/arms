﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.Integration.Models
{
    public class GeoCodingSearchResult : BaseServiceCallResult
    {
        public GeoCodingSearchResult()
        {
            Matches = new List<GeoCodingAddress>();
        }

        public List<GeoCodingAddress> Matches { get; set; }
    }

    public class GeoCodingAddress
    {
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string County { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
    }
}
