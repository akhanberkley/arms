﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.Integration.Models
{
    public class ClientSystemSettings
    {
        public string Url { get; set; }
        public string SystemCode { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string LocalId { get; set; }
        public string DSIK { get; set; }
        public bool UsesPortfolioService { get; set; }
        public bool UsesCmsService { get; set; }
        public IEnumerable<string> SearchFilters { get; set; }
    }

    public class AdvancedClientSettings
    {
        public string Url { get; set; }
        public string Username { get; set; }
        public string Dsik { get; set; }

        internal AdvClientSearchWS.Security ToSecurityToken()
        {
            return new AdvClientSearchWS.Security() { dsik = Dsik, UsernameToken = new AdvClientSearchWS.UsernameTokenType() { Username = new AdvClientSearchWS.AttributedString() { Value = Username } } };
        }
    }
}
