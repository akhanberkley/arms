﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.Integration.Models
{
    public class ExperianSearchResult : BaseServiceCallResult
    {
        public ExperianSearchResult()
        {
            Matches = new List<ExperianClient>();
        }

        public List<ExperianClient> Matches { get; set; }
    }

    public class ExperianSearchCriteria
    {
        public string Name { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
    }

    public class ExperianClient
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
    }
}
