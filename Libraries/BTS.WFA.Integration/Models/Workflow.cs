﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.Integration.Models
{
    public class WorkflowSystemDetails
    {
        public string BpmUrl { get; set; }
        public int BpmVersion { get; set; }
        public string ApplicationName { get; set; }
        public string CompanyNumber { get; set; }
        public string FilenetDocClass { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
    }

    public class WorkflowItem
    {
        public string TransactionType { get; set; }
        public string Status { get; set; }
        public string ProcessType { get; set; }

        public string PolicyNumber { get; set; }
        public DateTime? PolicyEffectiveDate { get; set; }
        public DateTime? PolicyExpirationDate { get; set; }
        public string InsuredName { get; set; }
        public string Account { get; set; }
        public string AgencyNumber { get; set; }
        public string AgencyName { get; set; }
        public string SubmissionNumber { get; set; }

        public WorkflowNote Note { get; set; }
        public WorkflowFile File { get; set; }
    }

    public class WorkflowNote
    {
        public string Scope { get; set; }
        public string User { get; set; }
        public string Text { get; set; }
    }

    public class WorkflowFile
    {
        public byte[] Data { get; set; }
        public string Extension { get; set; }
        public string DocumentType { get; set; }
        public string Remarks { get; set; }
    }

    public class WorkflowCreateResult : BaseServiceCallResult
    {
        public string InstanceId { get; set; }
    }

}
