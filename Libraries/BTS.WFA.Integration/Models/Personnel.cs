﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.Integration.Models
{
    public class PersonnelSearchResult : BaseServiceCallResult
    {
        public PersonnelSearchResult()
        {
            Personnel = new List<Personnel>();
        }
        public List<Personnel> Personnel { get; set; }
    }

    public class Personnel
    {
        public Personnel()
        {
            RoleCodes = new List<string>();
        }

        public long Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Initials { get; set; }
        public DateTime? RetirementDate { get; set; }
        public List<string> RoleCodes { get; set; }
    }
}
