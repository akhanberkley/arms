﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.Integration.Models
{
    public class AgencySearchResult : BaseServiceCallResult
    {
        public AgencySearchResult()
        {
            SecondaryExceptions = new List<Exception>();
        }

        public Agency Agency { get; set; }
        public List<Exception> SecondaryExceptions { get; set; }
    }

    public class AllAgenciesResult : BaseServiceCallResult
    {
        public AllAgenciesResult()
        {
            AgencyCodes = new List<string>();
        }

        public List<string> AgencyCodes { get; set; }
    }

    public class AgencySummary
    {
        public long Id { get; set; }
        public string AgencyCode { get; set; }
        public string AgencyName { get; set; }
    }

    public class Agency : AgencySummary
    {
        public Agency()
        {
            Locations = new List<AgencyLocation>();
            UnderwritingUnits = new List<AgencyUnderwritingUnit>();
            LicensedStates = new List<AgencyLicensedState>();
            Branches = new List<AgencyBranch>();
            Agents = new List<Agent>();
            Notes = new List<AgencyNote>();
        }

        public bool Active { get; set; }
        public string FEIN { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? RenewalCancelDate { get; set; }
        public DateTime? NewCancelDate { get; set; }

        public string ReferralAgencyCode { get; set; }
        public DateTime? ReferralDate { get; set; }

        public AgencySummary Parent { get; set; }
        public bool HasChildren { get; set; }

        public List<AgencyLocation> Locations { get; set; }
        public List<AgencyUnderwritingUnit> UnderwritingUnits { get; set; }
        public List<AgencyLicensedState> LicensedStates { get; set; }
        public List<AgencyBranch> Branches { get; set; }
        public List<Agent> Agents { get; set; }
        public List<AgencyNote> Notes { get; set; }
    }

    public class AgencyLocation
    {
        public bool IsPrimary { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        public decimal? Phone { get; set; }
        public decimal? Fax { get; set; }
        public string Email { get; set; }
    }

    public class AgencyUnderwritingUnit
    {
        public AgencyUnderwritingUnit()
        {
            Personnel = new List<AgencyUnderwritingUnitPersonnel>();
        }

        public long Id { get; set; }
        public long UnderwritingUnitId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public List<AgencyUnderwritingUnitPersonnel> Personnel { get; set; }
    }

    public class AgencyUnderwritingUnitPersonnel
    {
        public long Id { get; set; }
        public bool IsPrimaryRole { get; set; }
        public string RoleCode { get; set; }

        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Initials { get; set; }

        public DateTime? RetirementDate { get; set; }
    }

    public class AgencyBranch
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class AgencyLicensedState
    {
        public long Id { get; set; }
        public string LicenseType { get; set; }
        public string State { get; set; }
    }

    public class AgencyNote
    {
        public string Category { get; set; }
        public string Summary { get; set; }
        public string Details { get; set; }
        public DateTime? LastModified { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class Agent
    {
        public long Id { get; set; }
        public long PersonnelId { get; set; }
        public string AgentNumber { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? CancelDate { get; set; }
        public DateTime? DOB { get; set; }

        public string NamePrefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string NameSuffix { get; set; }
        public string TaxId { get; set; }

        public decimal? PrimaryPhone { get; set; }
        public decimal? SecondaryPhone { get; set; }
        public decimal? Fax { get; set; }
        public string EmailAddress { get; set; }
    }
}