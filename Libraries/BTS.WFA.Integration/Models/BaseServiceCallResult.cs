﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.Integration.Models
{
    public class BaseServiceCallResult
    {
        public bool Succeeded { get { return (Exception == null); } }
        public Exception Exception { get; set; }
    }
}
