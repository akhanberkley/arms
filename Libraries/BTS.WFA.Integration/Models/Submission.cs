﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.Integration.Models
{
    public class SubmissionSearchResult : BaseServiceCallResult
    {
        public Submission Submission { get; set; }
    }

    public class Submission
    {
        public Submission()
        {
            Locations = new List<SubmissionLocation>();
        }

        public string SubmissionNumber { get; set; }
        public string PolicyNumber { get; set; }
        public long ClientId { get; set; }

        public string Underwriter { get; set; }

        public string AgencyNumber { get; set; }
        public string AgencyName { get; set; }

        public string AgentName { get; set; }
        public string AgentPhoneNumber { get; set; }

        public string SICCode { get; set; }

        public List<SubmissionLocation> Locations { get; set; }
    }

    public class SubmissionLocation
    {
        public string BuildingNumber { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
    }
}
