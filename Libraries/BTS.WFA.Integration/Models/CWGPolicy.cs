﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.Integration.Models
{
    public enum CWGCompany
    {
        CWG,
        BNP
    }

    public class CWGPolicyResults : BaseServiceCallResult
    {
        public CWGPolicyResults()
        {
            Policies = new List<CWGPolicy>();
        }
        
        public List<CWGPolicy> Policies { get; set; }
    }

    public class CWGPolicy
    {
        public DateTime RunDate { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicyTransactionType { get; set; }
        public string PolicyStatus { get; set; }
        public DateTime? PolicyEffectiveDate { get; set; }
        public string BusCategoryCode { get; set; }
        public string ProgramCode { get; set; }
        public string SICCode { get; set; }
        public string NAICSCode { get; set; }
        public string PriorCarrier { get; set; }
        public decimal Premium { get; set; }
    }
}