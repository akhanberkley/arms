﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace BTS.WFA.Integration.Providers
{
    internal class ServiceCallInspector : IEndpointBehavior, IClientMessageInspector
    {
        public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters) { }
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) { }
        public void Validate(ServiceEndpoint endpoint) { }
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            if (Application.IsLoggingServiceCalls())
            {
                Endpoint = endpoint;
                clientRuntime.MessageInspectors.Add(this);
            }
        }

        //Message Inspector Pieces
        private ServiceEndpoint Endpoint { get; set; }
        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            var stopWatch = correlationState as Stopwatch;
            stopWatch.Stop();
            Application.LogResponse(Endpoint.Address.ToString(), reply.ToString(), stopWatch.ElapsedMilliseconds);
        }
        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
        {
            Application.LogRequest(Endpoint.Address.ToString(), request.ToString());
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            return stopWatch;
        }
    }
}
