﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BTS.WFA.Integration.Providers
{
    internal class HttpClientLogHandler : DelegatingHandler
    {
        public HttpClientLogHandler(HttpMessageHandler innerHandler)
            : base(innerHandler) { }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Stopwatch stopWatch = null;
            if (Application.IsLoggingServiceCalls())
            {
                var requestData = request.ToString();
                if (request.Content != null)
                    requestData += Environment.NewLine + request.Content.ReadAsStringAsync().Result;

                Application.LogRequest(request.RequestUri.ToString(), requestData);
                stopWatch = new Stopwatch();
                stopWatch.Start();
            }

            var responseTask = base.SendAsync(request, cancellationToken);

            if (Application.IsLoggingServiceCalls())
            {
                var response = responseTask.Result;
                stopWatch.Stop();
                var responseData = response.ToString();
                if (response.Content != null)
                    responseData += Environment.NewLine + response.Content.ReadAsStringAsync().Result;

                Application.LogResponse(request.RequestUri.ToString(), responseData, stopWatch.ElapsedMilliseconds);
            }

            return responseTask;
        }
    }
}
