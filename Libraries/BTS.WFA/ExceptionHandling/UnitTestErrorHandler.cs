﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Management;

namespace BTS.WFA.ExceptionHandling
{
    public class UnitTestErrorHandler : IExceptionHandler
    {
        public int ApplicationErrorCount { get; private set; }

        public void HandleException(HealthMonitorSettings settings, WebBaseErrorEvent errorEvent)
        {
            ApplicationErrorCount++;
        }
    }
}
