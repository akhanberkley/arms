﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Management;
using BTS.WFA.Providers;

namespace BTS.WFA.ExceptionHandling
{
    public class SqlErrorHandler : IExceptionHandler
    {
        public void HandleException(HealthMonitorSettings settings, WebBaseErrorEvent errorEvent)
        {
            using (var db = new Data.WfaSqlDb())
            {
                db.Exceptions.Add(new Data.WfaException()
                {
                    LoggedDate = DateTime.Now,
                    ApplicationName = settings.ApplicationName,
                    ApplicationEnvironment = settings.ApplicationEnvironment,
                    ExceptionInformation = HealthMonitorProvider.ExceptionInformation(errorEvent.ErrorException),
                    ExceptionContext = (errorEvent.EventSource == null) ? null : HealthMonitorProvider.Serialize(errorEvent.EventSource),
                    StackTrace = HealthMonitorProvider.StackTraceInformation(errorEvent.ErrorException),
                    WebRequestData = (errorEvent is WebRequestErrorEvent) ? HealthMonitorProvider.RequestInformation((errorEvent as WebRequestErrorEvent).RequestInformation) : null
                });

                db.SaveChanges();
            }
        }

        public static dynamic GetLoggedExceptionContext(int logId)
        {
            using (var db = new Data.WfaSqlDb())
            {
                var dbException = db.Exceptions.First(l => l.Id == logId);
                
                return HealthMonitorProvider.Deserialize(dbException.ExceptionContext);
            }
        }
    }
}
