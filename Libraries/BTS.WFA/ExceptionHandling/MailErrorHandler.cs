﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Management;
using BTS.WFA.Providers;

namespace BTS.WFA.ExceptionHandling
{
    public class MailErrorHandler : IExceptionHandler
    {
        public void HandleException(HealthMonitorSettings settings, WebBaseErrorEvent raisedEvent)
        {
            string formattedEvent = FormatEvent(raisedEvent);
            string subject = String.Format("{0} event in {1} ({2}): {3}", settings.ApplicationEnvironment,
                                                                          settings.ApplicationName,
                                                                          Environment.MachineName,
                                                                          raisedEvent.ErrorException.GetBaseException().GetType().Name);

            (new SmtpClient()).Send(new MailMessage(settings.EmailFromAddress, settings.EmailToAddress, subject, formattedEvent));
        }

        private string FormatEvent(WebBaseErrorEvent raisedEvent)
        {
            StringBuilder sb = new StringBuilder();

            if (raisedEvent is WebRequestErrorEvent)
                WriteSection(sb, "Request information:", HealthMonitorProvider.RequestInformation((raisedEvent as WebRequestErrorEvent).RequestInformation));

            FormatWebErrorEvent(sb, raisedEvent as WebBaseErrorEvent);

            return sb.ToString();
        }

        private void FormatWebErrorEvent(StringBuilder sb, WebBaseErrorEvent errorEvent)
        {
            WriteSection(sb, "Unhandled Exception:", HealthMonitorProvider.ExceptionInformation(errorEvent.ErrorException));
            WriteSection(sb, "Context:", HealthMonitorProvider.Serialize(errorEvent.EventSource));
            WriteSection(sb, "Exception stack trace:", HealthMonitorProvider.StackTraceInformation(errorEvent.ErrorException));
            WriteSection(sb, "Application information:", HealthMonitorProvider.ApplicationInformation(WebBaseEvent.ApplicationInformation));
            WriteSection(sb, "Process/thread information:", HealthMonitorProvider.ProcessInformation(errorEvent.ProcessInformation));
        }

        private void WriteSection(StringBuilder sb, string title, string details)
        {
            if (sb.Length > 0)
                sb.AppendLine();
            sb.AppendLine(title);
            sb.AppendLine(details);
        }
    }
}
