﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Management;

namespace BTS.WFA.ExceptionHandling
{
    public class OutputWindowLogger : IExceptionHandler
    {
        public void HandleException(HealthMonitorSettings settings, WebBaseErrorEvent errorEvent)
        {
            var ex = errorEvent.ErrorException;
            while (ex != null)
            {
                Debug.WriteLine(Providers.HealthMonitorProvider.ExceptionMessage(ex));
                Debug.WriteLine(ex.StackTrace);
                ex = ex.InnerException;
            }
        }
    }
}
