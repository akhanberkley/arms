﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Management;

namespace BTS.WFA.ExceptionHandling
{
    public class ConsoleHandler : IExceptionHandler
    {
        public void HandleException(HealthMonitorSettings settings, WebBaseErrorEvent errorEvent)
        {
            var ex = errorEvent.ErrorException;
            while (ex != null)
            {
                Console.WriteLine(Providers.HealthMonitorProvider.ExceptionMessage(ex));
                Console.WriteLine(ex.StackTrace);
                ex = ex.InnerException;
            }

            Console.ReadLine();
        }
    }
}
