﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.Logging
{
    public interface ILogger
    {
        bool LogsDatabase { get; }
        bool LogsServices { get; }
        void Log(string item);
    }
}
