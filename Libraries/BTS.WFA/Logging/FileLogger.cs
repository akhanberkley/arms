﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.Logging
{
    public class FileLogger : ILogger
    {
        public bool LogsDatabase { get { return true; } }
        public bool LogsServices { get { return true; } }

        private string FileLocation { get; set; }

        public FileLogger()
        {
            FileLocation = String.Format("{0}Log-{1}.txt", AppDomain.CurrentDomain.BaseDirectory, DateTime.Now.ToString("s").Replace(":", "-"));
        }
        public void Log(string item)
        {
            System.IO.File.AppendAllText(FileLocation, item);
        }
    }
}
