﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.Logging
{
    public class EmailServiceLogger : ILogger
    {
        public bool LogsDatabase { get { return false; } }
        public bool LogsServices { get { return true; } }

        private string ToAddress { get; set; }
        private SmtpClient MailClient { get; set; }

        public EmailServiceLogger(string toAddress)
        {
            ToAddress = toAddress;
            MailClient = new SmtpClient();
        }
        public void Log(string item)
        {
            MailClient.Send(new MailMessage(ToAddress, ToAddress, "Debug Event Log Item", item));
        }
    }
}
