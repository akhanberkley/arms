﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.Logging
{
    public class StringLogger : ILogger
    {
        public bool LogsDatabase { get { return true; } }
        public bool LogsServices { get { return true; } }

        public StringLogger()
        {
            Results = new StringBuilder();
        }

        public StringBuilder Results { get; private set; }

        public void Log(string item)
        {
            Results.Append(item);
        }
    }
}
