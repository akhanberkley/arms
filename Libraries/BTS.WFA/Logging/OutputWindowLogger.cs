﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.Logging
{
    public class OutputWindowLogger : ILogger
    {
        public bool LogsDatabase { get { return true; } }
        public bool LogsServices { get { return true; } }

        public void Log(string item)
        {
            Debug.Write(item);
        }
    }
}
