﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Management;
using Newtonsoft.Json;

namespace BTS.WFA
{
    public abstract class WorkflowApplication<T> : Application where T : class
    {
        protected static Type DbType;

        protected static void Initialize(Type dbType, params object[] handlers)
        {
            DbType = dbType;
            AddHandlers(handlers);
        }

        public static T GetDatabaseInstance()
        {
            var db = Activator.CreateInstance(DbType) as T;
            var dbContext = db as DbContext;
            if (DatabaseLoggers.Count > 0 && dbContext != null)
                dbContext.Database.Log = (s => DatabaseLoggers.ForEach(l => l.Log(s)));

            return db;
        }

        public static string GetDatabaseEnvironment()
        {
            string environment = null;
            var dbContext = GetDatabaseInstance() as DbContext;
            if (dbContext != null)
                environment = dbContext.Database.Connection.Database;

            return environment;
        }
    }

    public abstract class Application
    {
        protected static List<ExceptionHandling.IExceptionHandler> ExceptionHandlers = new List<ExceptionHandling.IExceptionHandler>();
        protected static List<Logging.ILogger> DatabaseLoggers = new List<Logging.ILogger>();
        protected static List<Logging.ILogger> ServiceLoggers = new List<Logging.ILogger>();

        /// <summary>
        /// Helper method for applications that dynamically set their handlers
        /// </summary>
        public static List<object> CreateHandlers(params string[] handlerTypes)
        {
            var assembly = Assembly.GetExecutingAssembly();

            var handlers = new List<object>();
            foreach (var h in handlerTypes)
                handlers.Add(assembly.CreateInstance(h));

            return handlers;
        }

        public static void AddHandlers(params object[] handlers)
        {
            if (handlers != null)
                foreach (var h in handlers)
                {
                    if (h is ExceptionHandling.IExceptionHandler)
                        ExceptionHandlers.Add((ExceptionHandling.IExceptionHandler)h);
                    else if (h is Logging.ILogger)
                    {
                        var logger = h as Logging.ILogger;
                        if (logger.LogsDatabase)
                            DatabaseLoggers.Add(logger);
                        if (logger.LogsServices)
                            ServiceLoggers.Add(logger);
                    }
                }
        }

        public static object[] GetStandardDebugHandlers()
        {
            return new object[] { new Logging.OutputWindowLogger(), new ExceptionHandling.UnitTestErrorHandler(), new ExceptionHandling.OutputWindowLogger() };
        }

        public static void LogItem(string item)
        {
            foreach (var l in DatabaseLoggers.Union(ServiceLoggers))
                l.Log(item);
        }

        public static bool IsLoggingServiceCalls()
        {
            return (ServiceLoggers.Count > 0);
        }
        public static void LogRequest(string url, string message)
        {
            foreach (var l in ServiceLoggers)
                l.Log("-- Message TO: " + url + Environment.NewLine + message + Environment.NewLine);
        }
        public static void LogResponse(string url, string message, long responseTimeInMs)
        {
            foreach (var l in ServiceLoggers)
                l.Log("-- Message FROM: " + url + Environment.NewLine
                       + message + Environment.NewLine
                       + "-- Completed in " + responseTimeInMs.ToString() + " ms" + Environment.NewLine + Environment.NewLine);
        }

        public static void HandleException(Exception ex, object context)
        {
            //Get this manually handled exception into the HealthMonitoring pipeline
            (new HealthMonitoringErrorEvent(ex, context)).Raise();
        }
        internal static void HandleException(HealthMonitorSettings settings, WebBaseErrorEvent raisedEvent)
        {
            foreach (var h in ExceptionHandlers)
                h.HandleException(settings, raisedEvent);
        }
    }

    public class HealthMonitorSettings
    {
        public string EmailToAddress { get; set; }
        public string EmailFromAddress { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationEnvironment { get; set; }
    }

    public class HealthMonitoringErrorEvent : WebRequestErrorEvent
    {
        public HealthMonitoringErrorEvent(Exception ex, object context) : base(ex.Message, context, 987000, ex) { }
    }
}
