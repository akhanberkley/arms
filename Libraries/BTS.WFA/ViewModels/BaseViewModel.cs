﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BTS.WFA.ViewModels
{
    public class BaseViewModel
    {
        public BaseViewModel()
        {
            ValidationResults = new List<ValidationResult>();
        }

        [IgnoreDataMember]
        public bool ItemNotFound { get; set; }
        [IgnoreDataMember]
        public bool ItemCreated { get; set; }

        [IgnoreDataMember]
        public List<ValidationResult> ValidationResults { get; set; }

        public void AddValidation(string property, string message, params object[] formatInputs)
        {
            ValidationResults.Add(new ValidationResult(property, message, formatInputs));
        }
    }
}
