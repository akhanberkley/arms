﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ViewModels
{
    public class ListItemViewModel
    {
        public string Description { get; set; }
        public int Id { get; set; }
    }

    public class CodeListItemViewModel
    {
        public string Description { get; set; }
        public string Code { get; set; }
    }
}
