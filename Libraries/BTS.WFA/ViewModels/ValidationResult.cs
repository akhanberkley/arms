﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.ViewModels
{
    public class ValidationResult
    {
        public ValidationResult() { }
        public ValidationResult(string property, string message, params object[] formatInputs)
        {
            Property = property;
            Message = (formatInputs != null && formatInputs.Length > 0) ? String.Format(message, formatInputs) : message;
        }

        public string Property { get; set; }
        public string Message { get; set; }
    }
}
