﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BTS.WFA.ViewModels
{
    public class JavascriptException : Exception { }

    public class JavascriptExceptionViewModel
    {
        public string Exception { get; set; }
        public string Cause { get; set; }
    }
}
