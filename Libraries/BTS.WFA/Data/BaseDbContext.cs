﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace BTS.WFA.Data
{
    public class BaseDbContext : DbContext
    {
        public BaseDbContext(string connectionString)
            : base(connectionString)
        {
        }

        public BaseDbContext(string connectionString, System.Data.Entity.Infrastructure.DbCompiledModel model)
            : base(connectionString, model)
        {
        }

        public BaseDbContext()
            : base(ConfigurationManager.AppSettings["ApplicationConnectionString"])
        {
        }

        public static DbModelBuilder CreateModel(DbModelBuilder modelBuilder, string schema)
        {
            throw new NotImplementedException();//not sure what this method does; was generated with all configurations (I removed it and put in this notimplemented exception for now)
        }

        public DbContextTransaction GetTransaction(System.Data.IsolationLevel isoLevel = System.Data.IsolationLevel.ReadCommitted)
        {
            return Database.BeginTransaction(isoLevel);
        }

        public bool HasChanges()
        {
            return ChangeTracker.HasChanges();
        }

        public bool EntityHasChanges<T>(T item) where T : class
        {
            var contextItem = ChangeTracker.Entries<T>().FirstOrDefault(i => i.Entity == item);
            return (contextItem != null && contextItem.State != EntityState.Unchanged);
        }
        public bool EntityIsNew<T>(T item) where T : class
        {
            var contextItem = ChangeTracker.Entries<T>().FirstOrDefault(i => i.Entity == item);
            return (contextItem != null && contextItem.State == EntityState.Added);
        }
        public void Reload<T>(T item) where T : class
        {
            var entry = ChangeTracker.Entries<T>().FirstOrDefault(i => i.Entity == item);
            if (entry != null)
                entry.Reload();
        }
    }
}
