﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace BTS.WFA.Data
{
    public class BaseMockContext
    {
        public BaseMockContext()
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition().GetInterface("IEnumerable") != null)
                {
                    var tableType = prop.PropertyType.GetGenericArguments()[0];
                    var tableInstance = Activator.CreateInstance(typeof(MockDbSet<>).MakeGenericType(tableType), tableType.Name);
                    prop.SetValue(this, tableInstance, null);
                }
            }
        }

        public DbContextTransaction GetTransaction(System.Data.IsolationLevel isoLevel = System.Data.IsolationLevel.ReadCommitted)
        {
            return null;
        }

        public int SaveChanges()
        {
            return 0;
        }
        public bool HasChanges()
        {
            return true;
        }
        public bool EntityHasChanges<T>(T item) where T : class
        {
            return true;
        }
        public bool EntityIsNew<T>(T item) where T : class
        {
            return true;
        }
        public void Reload<T>(T item) where T : class
        {
        }
        public void Dispose()
        {
        }
    }
}
