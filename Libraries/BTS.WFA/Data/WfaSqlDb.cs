using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Reflection;
using System.Linq;
using System.Configuration;

namespace BTS.WFA.Data
{
    public class WfaSqlDb : BTS.WFA.Data.BaseDbContext
    {
        public IDbSet<WfaException> Exceptions { get; set; }

        static WfaSqlDb()
        {
            Database.SetInitializer<WfaSqlDb>(null);
        }

        public WfaSqlDb()
            : base(ConfigurationManager.AppSettings["ApplicationConnectionString"])
        {
        }

        public WfaSqlDb(string connectionString)
            : base(connectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
        }
    }

    public class WfaException
    {
        public int Id { get; set; }
        public DateTime LoggedDate { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationEnvironment { get; set; }
        public string ExceptionInformation { get; set; }
        public string StackTrace { get; set; }
        public string ExceptionContext { get; set; }
        public string WebRequestData { get; set; }
        
    }
    internal class WfaExceptionConfiguration : EntityTypeConfiguration<WfaException>
    {
        public WfaExceptionConfiguration()
        {
            ToTable("dbo.WfaExceptionLog");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.LoggedDate).HasColumnName("LoggedDate").IsRequired();
            Property(x => x.ApplicationName).HasColumnName("ApplicationName").IsRequired().HasMaxLength(100);
            Property(x => x.ApplicationEnvironment).HasColumnName("ApplicationEnvironment").IsRequired().HasMaxLength(100);
            Property(x => x.ExceptionInformation).HasColumnName("ExceptionInformation").IsRequired().IsMaxLength();
            Property(x => x.StackTrace).HasColumnName("StackTrace").IsOptional().IsMaxLength();
            Property(x => x.ExceptionContext).HasColumnName("ExceptionContext").IsOptional().IsMaxLength();
            Property(x => x.WebRequestData).HasColumnName("WebRequestData").IsOptional().IsMaxLength();
        }
    }
}
