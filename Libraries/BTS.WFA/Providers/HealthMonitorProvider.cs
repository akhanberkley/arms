﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Management;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BTS.WFA.Providers
{
    public class HealthMonitorProvider : WebEventProvider
    {
        private HealthMonitorSettings settings = null;
        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);

            settings = new HealthMonitorSettings()
            {
                EmailToAddress = config.Get("to"),
                EmailFromAddress = config.Get("from"),
                ApplicationName = config.Get("application"),
                ApplicationEnvironment = config.Get("environment")
            };
        }

        public override void Flush() { }
        public override void Shutdown() { }
        public override void ProcessEvent(WebBaseEvent raisedEvent)
        {
            if (raisedEvent is WebBaseErrorEvent)
                Application.HandleException(settings, raisedEvent as WebBaseErrorEvent);
        }

        public static string GetEnvironmentName()
        {
            var config = ConfigurationManager.GetSection("system.web/healthMonitoring") as HealthMonitoringSection;
            return config.Providers["WfaProvider"].Parameters["environment"];
        }

        public static string Serialize(object obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj, Formatting.Indented);
            }
            catch (Exception ex)
            {
                return "Failed to Serialize Object: " + ex.Message;
            }
        }
        public static dynamic Deserialize(string data)
        {
            return JsonConvert.DeserializeObject(data);
        }
        public static T Deserialize<T>(JToken token)
        {
            return token.ToObject<T>();
        }

        public static string ExceptionMessage(Exception ex)
        {
            StringBuilder msg = new StringBuilder();

            var entityVal = ex as System.Data.Entity.Validation.DbEntityValidationException;
            if (entityVal != null)
            {
                foreach (var eve in entityVal.EntityValidationErrors)
                {
                    msg.AppendFormat("{0} {1} validation errors:", eve.Entry.State, eve.Entry.Entity.GetType().Name);
                    msg.AppendLine();
                    foreach (var ve in eve.ValidationErrors)
                    {
                        msg.AppendFormat("-Property: {0}, Error: {1}", ve.PropertyName, ve.ErrorMessage);
                        msg.AppendLine();
                    }
                }
            }
            else
                msg.Append(ex.Message);

            return msg.ToString();
        }
        public static string RequestInformation(WebRequestInformation webRequestInfo)
        {
            object requestInfo = null;
            object sessionInfo = null;

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            if (context != null)
            {
                if (context.Request != null)
                {
                    requestInfo = new
                    {
                        UrlReferrer = (context.Request.UrlReferrer != null) ? context.Request.UrlReferrer.PathAndQuery : null,
                        RefererFullUrl = context.Request.Headers["RefererFullUrl"],
                        RequestType = context.Request.RequestType,
                        FormInformation = (context.Request.Form == null) ? null : context.Request.Form.AllKeys.Select(k => new { Key = k, Value = context.Request.Form[k] })
                    };
                }

                if (context.Session != null)
                    sessionInfo = context.Session.Keys.Cast<string>().Select(k => new { Key = k, Value = context.Session[k] });
            }

            return Serialize(new
                {
                    RequestUrl = webRequestInfo.RequestUrl,
                    RequestPath = webRequestInfo.RequestPath,
                    UserName = (webRequestInfo.Principal != null) ? webRequestInfo.Principal.Identity.Name : "Anonymous",
                    UserHostAddress = webRequestInfo.UserHostAddress,
                    RequestData = requestInfo,
                    SessionData = sessionInfo
                });
        }
        public static string ExceptionInformation(Exception exception)
        {
            if (exception == null)
                return null;
            else
            {
                object innerException = exception.InnerException;

                var aggException = exception as AggregateException;
                if (aggException != null)
                {
                    var innerExceptions = new List<object>();
                    foreach (var e in aggException.InnerExceptions)
                        innerExceptions.Add(e);
                    innerException = innerExceptions;
                }

                return Serialize(new
                {
                    TypeName = exception.GetType().Name,
                    Message = ExceptionMessage(exception),
                    InnerException = innerException
                });
            }
        }
        public static string StackTraceInformation(Exception exception)
        {
            if (exception == null || exception.StackTrace == null)
                return null;
            else
                return exception.StackTrace + ((exception.InnerException == null) ? "" : Environment.NewLine + StackTraceInformation(exception.InnerException));
        }
        public static string ApplicationInformation(WebApplicationInformation appInfo)
        {
            if (appInfo == null)
                return Serialize(new { Machine = Environment.MachineName });
            else
                return Serialize(new
                {
                    Machine = Environment.MachineName,
                    Domain = appInfo.ApplicationDomain,
                    VirtualPath = appInfo.ApplicationVirtualPath,
                    ApplicationPath = appInfo.ApplicationPath
                });
        }
        public static string ProcessInformation(WebProcessInformation info)
        {
            object processInfo = null;
            object webProcessInfo = null;
            try
            {
                System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess();

                processInfo = new
                    {
                        OSVersion = Environment.OSVersion.ToString(),
                        FrameworkVersion = Environment.Version.ToString(),
                        Statistics = new[] {
                            String.Format("Nonpaged system memory size (KB): {0:#,##0}", process.NonpagedSystemMemorySize64 / 1024),
                            String.Format("Paged system memory size (KB): {0:#,##0}", process.PagedSystemMemorySize64 / 1024),
                            String.Format("Paged memory size (MB): {0:#,##0}", process.PagedMemorySize64 / 1048576),
                            String.Format("Peak paged memory size (MB): {0:#,##0}", process.PeakPagedMemorySize64 / 1048576),
                            String.Format("Peak virtual memory size (MB): {0:#,##0}", process.PeakVirtualMemorySize64 / 1048576),
                            String.Format("Peak working set (MB): {0:#,##0}", process.PeakWorkingSet64 / 1048576),
                            String.Format("Private memory size (MB): {0:#,##0}", process.PrivateMemorySize64 / 1048576),
                            String.Format("Total processor time (Seconds): {0:#,##0}", process.TotalProcessorTime.TotalSeconds),
                            String.Format("Virtual memory size (MB): {0:#,##0}", process.VirtualMemorySize64 / 1048576),
                            String.Format("Working set (MB): {0:#,##0}", process.WorkingSet64 / 1048576),
                            String.Format("Thread Count: {0:#,##0}", (process.Threads == null ? 0 : process.Threads.Count)),
                            String.Format("Up time (Minutes): {0:#,##0}", (new TimeSpan(DateTime.Now.Ticks - process.StartTime.Ticks)).TotalMinutes)
                        }
                    };
            }
            catch (Exception) { }

            if (webProcessInfo != null)
                webProcessInfo = new
                {
                    ProcessId = info.ProcessID,
                    ProcessName = info.ProcessName,
                    AccountName = info.AccountName
                };

            return Serialize(new
                {
                    Web = webProcessInfo,
                    Machine = processInfo
                });
        }
    }
}