﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.Integration.Tests.Clearance
{
    [TestClass]
    public class TestEnvironment : IntegrationBaseTest
    {
        #region AIC
        private static string aicUrl = "https://btsdedevwfa02/";

        [TestMethod]
        public void AICSearchBySubmissionNumber()
        {
            var submission = Services.ClearanceService.GetSubmission(aicUrl, "52", "431487");
            submission = Services.ClearanceService.GetSubmission(aicUrl, "123", "123");
        }
        #endregion
    }
}
