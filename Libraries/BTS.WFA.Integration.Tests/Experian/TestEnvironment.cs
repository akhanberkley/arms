﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.Integration.Tests.Experian
{
    [TestClass]
    public class TestEnvironment : IntegrationBaseTest
    {
        #region AIC
        private static Models.AdvancedClientSettings aicSettings = new Models.AdvancedClientSettings() { Url = "http://mgb-tst/esb/creditreportsupplemental", Dsik = TestDSIKs.AIC, Username = "jangolano" };

        [TestMethod]
        public void AICSearch()
        {
            var results = Services.ExperianService.Search(aicSettings, new Models.ExperianSearchCriteria()
            {
                Name = "Berkley",
                Address1 = "3840 109th street",
                City = "Urbandale",
                State = "IA",
                PostalCode = "50322"
            });

            results = Services.ExperianService.Search(aicSettings, new Models.ExperianSearchCriteria()
            {
                Name = "Youngers Consulting",
                Address1 = "8300 Twana Dr",
                City = "Urbandale",
                State = "IA",
                PostalCode = "50322"
            });
        }
        #endregion
    }
}
