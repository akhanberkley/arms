﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.Integration.Tests.Clients
{
    [TestClass]
    public class TestEnvironmentOFAC : IntegrationBaseTest
    {
        [TestMethod]
        public void OFACMiss()
        {
            var advClientSettings = new Models.AdvancedClientSettings() { Url = "http://mgb-tst/esb/AdvCliSearchWS/", Dsik = TestDSIKs.BPS, Username = "jangolan" };

            var criteria = new Models.OFACSearchCriteria()
            {
                Name = "John Doe",
                Address = "",
                City = "Urbandale",
                State = "IA",
                PostalCode = "50322",
                Country = "USA"
            };

            var results = Services.ClientService.SearchOFAC(advClientSettings, "OFACMissTest", criteria);
            Assert.IsTrue(results.Matches.Count == 0);
        }

        [TestMethod]
        public void HitAllCompanies()
        {
            var dsiks = new string[] 
            { 
                TestDSIKs.AIC,
                TestDSIKs.CWG,
                TestDSIKs.BMG,
                TestDSIKs.USG,
                TestDSIKs.CCIC,
                TestDSIKs.CSM,
                TestDSIKs.BRS,
                TestDSIKs.BSEL,
                TestDSIKs.BLS,
                TestDSIKs.BRAC,
                TestDSIKs.BNP,
                TestDSIKs.BARS,
                TestDSIKs.BTU,
                TestDSIKs.BFM,
                TestDSIKs.FIN
            };

            var criteria = new Models.OFACSearchCriteria()
                {
                    Name = "Osama Bin Laden",
                    Address = "1400 1st Ave",
                    City = "New York",
                    State = "NY",
                    PostalCode = "10021",
                    Country = "USA"
                };

            foreach(var dsik in dsiks)
            {
                var results = Services.ClientService.SearchOFAC(new Models.AdvancedClientSettings() { Url = "http://mgb-tst/esb/AdvCliSearchWS/", Dsik = dsik, Username = "jangolan" }, "OFACHitTest", criteria);
            }
        }

    }
}
