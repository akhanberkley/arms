﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.Integration.Tests.Clients
{
    [TestClass]
    public class TestEnvironment : IntegrationBaseTest
    {
        private Models.ClientSearchCriteria CreateCriteria()
        {
            return new Models.ClientSearchCriteria()
            {
                CorpId = "500040",
                ClientType = "Seg Bus"
            };
        }

        #region AIC
        private Models.ClientSystemSettings AICccSettings = new Models.ClientSystemSettings() { Url = "http://aic-tst/ClientCoreService/services/ClientCoreService", LocalId = "1", SystemCode = "FAB", Username = "GlobalBatchUser", Password = "GlobalBatchUser", UsesPortfolioService = true, SearchFilters = new[] { "filterDuplicateNames", "filterDuplicateAddresses" } };

        [TestMethod]
        public void AICSearchCriteria()
        {
            var criteria = CreateCriteria();
            criteria.BusinessName = "*test*";
            var results = Services.ClientService.Search(AICccSettings, criteria, 60);
            Assert.IsTrue(results.Exception is Models.TooManyResultsException);
            criteria.StateName = "IA";
            results = Services.ClientService.Search(AICccSettings, criteria, 60);
            Assert.IsTrue(results.Matches.Count > 0);
        }

        [TestMethod]
        public void AICSearchById()
        {
            var criteria = CreateCriteria();

            criteria.ClientId = "217345";
            var results = Services.ClientService.Search(AICccSettings, criteria, 60);
            Assert.IsTrue(results.Succeeded);
        }

        [TestMethod]
        public void AICSearchDetailedClient()
        {
            var criteria = CreateCriteria();
            criteria.BusinessName = "*youngers*";

            var stringified = criteria.Stringify();

            var results = Services.ClientService.Search(AICccSettings, criteria, 60);
            Assert.IsTrue(results.Succeeded);

            criteria = CreateCriteria();
            criteria.ClientId = "216353";
            results = Services.ClientService.Search(AICccSettings, criteria, 60);
            Assert.IsTrue(results.Succeeded);
        }

        [TestMethod]
        public void AICSearchPortfolio()
        {
            var criteria = CreateCriteria();
            criteria.PortfolioId = 212724;
            var results = Services.ClientService.Search(AICccSettings, criteria, 60);
            Assert.IsTrue(results.Matches.Count > 0);
        }

        [TestMethod]
        public void AICSaveClient()
        {
            string uniqueId = DateTime.Now.Ticks.ToString();
            var client = new Models.Client();
            client.ClientType = "Seg Bus";
            client.CorporationCode = "52";
            client.Segment = "AICALL";
            client.PhoneNumber = "5159990373";
            client.TaxId = "123456789";

            client.Names.Add(new Models.ClientName() { BusinessName = "ImportTest" + uniqueId, NameType = "Insured", IsPrimary = true });
            client.Names.Add(new Models.ClientName() { BusinessName = "ImportTestDBA" + uniqueId, NameType = "DBA", IsPrimary = false });

            client.Addresses.Add(new Models.ClientAddress() { AddressType = "Both", HouseNumber = "123", Address1 = "1st Street", City = "Nowhereville", State = "IA", County = "Polk", Country = "USA", PostalCode = "50322" });

            var results = Services.ClientService.Save(AICccSettings, client, "500040");
        }

        [TestMethod]
        public void AICUpdateClient()
        {
            var client = new Models.Client();
            client.Id = 217345;
            client.PortfolioId = 217345;
            client.ClientType = "Seg Bus";
            client.CorporationCode = "52";
            client.Segment = "AICALL";
            client.PhoneNumber = "5159990373";
            client.TaxId = "123456789";

            client.Names.Add(new Models.ClientName() { SequenceNumber = 1, BusinessName = "ImportTestUpdate", NameType = "Insured", IsPrimary = true });
            client.Names.Add(new Models.ClientName() { SequenceNumber = 2, BusinessName = "ImportTestDBAUpdate", NameType = "DBA", IsPrimary = false });

            client.Addresses.Add(new Models.ClientAddress() { Id = 343644, AddressType = "Both", HouseNumber = "123", Address1 = "1st Street", City = "Nowhereville", State = "IA", County = "Polk", Country = "USA", PostalCode = "50322" });

            var results = Services.ClientService.Save(AICccSettings, client, "500040");
        }

        #endregion

        #region CWG
        private Models.ClientSystemSettings CWGccSettings = new Models.ClientSystemSettings() { Url = "http://cwg-tst/ClientCoreService/services/ClientCoreService", LocalId = "1", SystemCode = "FAB", Username = "GlobalBatchUser", Password = "GlobalBatchUser", UsesPortfolioService = true, SearchFilters = new[] { "filterDuplicateNames", "filterDuplicateAddresses", "keepLowestNameSequence", "keepLowestAddressSequence" } };

        [TestMethod]
        public void SearchCWG()
        {
            var criteria = new Models.ClientSearchCriteria()
            {
                ClientId = "691088"
            };

            var updatedClient = Services.ClientService.Search(CWGccSettings, criteria, 60);
            Assert.IsTrue(updatedClient.Matches[0].Addresses[0].HouseNumber == "8301");

            criteria.EffectiveDate = new DateTime(2014, 11, 24);
            var oldClient = Services.ClientService.Search(CWGccSettings, criteria, 60);
            Assert.IsTrue(oldClient.Matches[0].Addresses[0].HouseNumber == "8300");
        }
        #endregion

        #region CSM
        private Models.ClientSystemSettings CSMccSettings = new Models.ClientSystemSettings() { Url = "http://csm-tst/GlobalWebServiceRouter/services/ClientCoreWebService", LocalId = "1", SystemCode = "FAB", Username = "GlobalBatchUser", Password = "GlobalBatchUser", UsesPortfolioService = false };

        [TestMethod]
        public void SearchCSM()
        {
            var criteria = new Models.ClientSearchCriteria()
            {
                BusinessName = "*test*",
                CorpId = "500040",
                ClientType = "Seg Bus"
            };

            var results = Services.ClientService.Search(CSMccSettings, criteria, 60);
            Assert.IsTrue(results.Matches.Count > 0);

            criteria.StateName = "IA";
            results = Services.ClientService.Search(CSMccSettings, criteria, 60);
            Assert.IsTrue(results.Succeeded);
        }
        #endregion

        #region FIN
        private Models.ClientSystemSettings FINccSettings = new Models.ClientSystemSettings() { Url = "http://ssp-tst/ClientCoreService/services/ClientCoreService", LocalId = "1", SystemCode = "FAB", Username = "finclearance", Password = "bcsdata", UsesPortfolioService = true, SearchFilters = new[] { "filterDuplicateNames", "filterDuplicateAddresses", "keepLowestNameSequence", "keepLowestAddressSequence" } };

        [TestMethod]
        public void SearchFIN()
        {
            var criteria = new Models.ClientSearchCriteria()
            {
                //ClientId = "1100069565",
                BusinessName = "*Johnny Football*",
                CorpId = "500040",
                ClientType = "Seg Bus"
            };

            var results = Services.ClientService.Search(FINccSettings, criteria, 60);
            Assert.IsTrue(results.Matches.Count > 0);
        }
        #endregion
    }
}
