﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.Integration.Tests.Clients
{
    [TestClass]
    public class ProdEnvironment : IntegrationBaseTest
    {
        private Models.ClientSearchCriteria CreateCriteria()
        {
            return new Models.ClientSearchCriteria()
            {
                CorpId = "500040",
                ClientType = "Seg Bus"
            };
        }

        #region BLS
        private Models.ClientSystemSettings BLSccSettings = new Models.ClientSystemSettings() { Url = "http://bls-prd/GlobalWebServiceRouter/services/ClientCoreWebService", LocalId = "1", SystemCode = "FAB", Username = "ClearanceAcct", Password = "welcome" };

        [TestMethod]
        public void BLSSearchCriteria()
        {
            var criteria = CreateCriteria();
            criteria.BusinessName = "*test*";
            var results = Services.ClientService.Search(BLSccSettings, criteria, 60);
            Assert.IsTrue(results.Matches.Count > 0);
        }

        [TestMethod]
        public void BLSSearchById()
        {
            var criteria = CreateCriteria();
            criteria.ClientId = "invalidid";
            var results = Services.ClientService.Search(BLSccSettings, criteria, 60);
            Assert.IsTrue(!results.Succeeded);

            criteria.ClientId = "11506";
            results = Services.ClientService.Search(BLSccSettings, criteria, 60);
            Assert.IsTrue(results.Succeeded);
        }
        #endregion

        #region BMAG
        private Models.ClientSystemSettings BMAGccSettings = new Models.ClientSystemSettings() { Url = "http://bmg-prd/ClientCoreService/services/ClientCoreService", LocalId = "1", SystemCode = "FAB", Username = "GlobalBatchUser", Password = "GlobalBatchUser", UsesPortfolioService = true, SearchFilters = new[] { "filterDuplicateAddresses", "filterDuplicateNames" } };

        [TestMethod]
        public void BMAGSearchById()
        {
            var criteria = CreateCriteria();
            criteria.ClientId = "60045787";
            var results = Services.ClientService.Search(BMAGccSettings, criteria, 60);
            Assert.IsTrue(results.Succeeded);
        }
        #endregion

        #region USG
        private Models.ClientSystemSettings USGccSettings = new Models.ClientSystemSettings() { Url = "http://usg-prd/ClientCoreService/services/ClientCoreService", LocalId = "1", SystemCode = "FAB", Username = "GlobalBatchUser", Password = "GlobalBatchUser", UsesPortfolioService = true, SearchFilters = new[] { "filterDuplicateAddresses", "filterDuplicateNames" } };
        //private Models.ClientCoreSettings USGccSettings = new Models.ClientCoreSettings() { Url = "http://usg-prd/GlobalWebServiceRouter/services/ClientCoreWebService", LocalId = "1", SystemCode = "FAB", Username = "GlobalBatchUser", Password = "GlobalBatchUser", UsesPortfolioService = false };

        [TestMethod]
        public void USGSearchById()
        {
            var criteria = CreateCriteria();
            criteria.ClientId = "197906";
            var results = Services.ClientService.Search(USGccSettings, criteria, 60);
            Assert.IsTrue(results.Succeeded);
        }
        #endregion
    }
}
