﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.Integration.Tests.Clients
{
    [TestClass]
    public class IntEnvironment : IntegrationBaseTest
    {
        private Models.ClientSearchCriteria CreateCriteria()
        {
            return new Models.ClientSearchCriteria()
            {
                CorpId = "500040",
                ClientType = "Seg Bus"
            };
        }

        #region BARS
        private Models.ClientSystemSettings BARSccSettings = new Models.ClientSystemSettings() { Url = "http://ssp-int/ClientCoreService/services/ClientCoreService", LocalId = "1", SystemCode = "FAB", Username = "barsclearance", Password = "bcsdata", UsesPortfolioService = true, SearchFilters = new[] { "filterDuplicateNames", "filterDuplicateAddresses", "keepLowestNameSequence", "keepLowestAddressSequence", "skipPortfolioSearch" } };

        [TestMethod]
        public void SearchCriteriaBARS()
        {
            var criteria = CreateCriteria();
            criteria.BusinessName = "*Jen B - COP - IA/BARS - 1052 edit test*";
            var results = Services.ClientService.Search(BARSccSettings, criteria, 60);
            Assert.IsTrue(results.Matches.Count > 0);
        }

        [TestMethod]
        public void SearchByIdBARS()
        {
            var criteria = CreateCriteria();
            criteria.ClientId = "5034858";
            var results = Services.ClientService.Search(BARSccSettings, criteria, 60);
            Assert.IsTrue(results.Matches.Count > 0);
        }

        [TestMethod]
        public void UpdatePrimaryAddressBARS()
        {
            var results = Services.ClientService.UpdatePrimaryAddress(BARSccSettings, 5034858, 48842);
            Assert.IsTrue(results.Succeeded);
        }
        #endregion

        #region USG
        private Models.ClientSystemSettings USGccSettings = new Models.ClientSystemSettings() { Url = "http://mgb-dev/", UsesCmsService = true, DSIK = "7b8a807f-efe7-4d03-9821-4f79b8c30ee9" };

        [TestMethod]
        public void SearchByIdUSG()
        {
            var criteria = new Models.ClientSearchCriteria();
            //criteria.ClientId = "10161";
            criteria.ClientId = "195019";
            var results = Services.ClientService.Search(USGccSettings, criteria, 60);
            Assert.IsTrue(results.Matches.Count > 0);

            //Invalid client
            criteria.ClientId = "10161124124";
            results = Services.ClientService.Search(USGccSettings, criteria, 60);
            Assert.IsTrue(results.Matches.Count == 0);

            //By Guid
            criteria.ClientId = "2ed090e3-e24d-438c-8b0d-7604c280a789";
            results = Services.ClientService.Search(USGccSettings, criteria, 60);
            Assert.IsTrue(results.Matches.Count > 0);
        }

        [TestMethod]
        public void SearchCriteriaUSG()
        {
            var criteria = new Models.ClientSearchCriteria();
            criteria.BusinessName = "Glenn";
            criteria.PostalCode = "73134";
            criteria.CityName = "Oklahoma City";
            var results = Services.ClientService.Search(USGccSettings, criteria, 60);
            Assert.IsTrue(results.Matches.Count > 0);

            criteria = new Models.ClientSearchCriteria();
            criteria.BusinessName = "Scottz";
            criteria.StateName = "IA";
            results = Services.ClientService.Search(USGccSettings, criteria, 60);
            Assert.IsTrue(results.Matches.Count > 0);
        }

        #endregion
    }
}
