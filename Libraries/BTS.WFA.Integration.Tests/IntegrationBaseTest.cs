﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.Integration.Tests
{
    public class IntegrationBaseTest
    {
        [TestInitialize]
        public void Initialization()
        {
            Application.AddHandlers(Application.GetStandardDebugHandlers());
        }

        public class TestDSIKs
        {
            public static string AIC = "e8d1c216-01f8-432a-87fe-661c23264327";
            public static string CWG = "2710ff04-de7e-4e63-a470-e2068b5859e1";
            public static string BMG = "84efd6f3-cae3-4df8-8ed9-377343870379";
            public static string USG = "7b8a807f-efe7-4d03-9821-4f79b8c30ee9";
            public static string CCIC = "e0753355-dacd-40fd-bcea-13c169cc6098";
            public static string CSM = "1f88f9fb-7be5-4742-b6ff-c21718aea213";
            public static string BRS = "c4b095b4-53b0-400a-88eb-1c67059377f0";
            public static string BSEL = "3e8b4879-6d33-4be5-88ff-ee10442bf26d";
            public static string BLS = "c3dc9331-0703-4cdf-8de5-45a245921ac9";
            public static string BRAC = "d0fc729a-e787-46e3-8d97-af4eaae7dc43";
            public static string BNP = "12803e2e-8637-428e-ac1b-782a90b1c307";
            public static string BARS = "2c476153-fdda-4565-91c0-f893426998c9";
            public static string BTU = "19c80673-ccd5-4e5f-b173-79c67500113f";
            public static string BFM = "7b8a807f-efe7-4d03-9821-4f79b8c30ee9";
            public static string FIN = "a9c20dee-a8f8-439d-a217-7863149e0d0f";
            public static string BPS = "cafab6f0-72f7-2276-3255-e801044639e8";
        }
    }
}
