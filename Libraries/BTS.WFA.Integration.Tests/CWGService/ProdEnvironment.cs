﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace BTS.WFA.Integration.Tests.CWGService
{
    [TestClass]
    public class ProdEnvironment : IntegrationBaseTest
    {
        [TestMethod]
        public void GetCWGSubmissions()
        {
            var results = Services.CWGService.GetSubmissionsToSync(BTS.WFA.Integration.Models.CWGCompany.CWG, "Data Source=BTSDECWGSQL01;Initial Catalog=CWGClearance;password=falcon16;persist security info=True;user id=CWGClearanceUser;pooling=true", DateTime.Now.Date.AddDays(-2));
            var items = results.Policies.Where(s => !String.IsNullOrEmpty(s.PriorCarrier) || !String.IsNullOrEmpty(s.ProgramCode)).ToArray();
        }

        [TestMethod]
        public void GetBNPSubmissions()
        {
            var results = Services.CWGService.GetSubmissionsToSync(BTS.WFA.Integration.Models.CWGCompany.BNP, "Data Source=BTSDECWGSQL01;Initial Catalog=BNPG_Clearance;password=t4!chuC8;persist security info=True;user id=BNPClearance;pooling=true", DateTime.Now.Date.AddDays(-2));
            var items = results.Policies.Where(s => !String.IsNullOrEmpty(s.PriorCarrier) || !String.IsNullOrEmpty(s.ProgramCode)).ToArray();
        }
    }
}
