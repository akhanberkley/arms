﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.Integration.Tests.GeoCoding
{
    [TestClass]
    public class TestEnvironment : IntegrationBaseTest
    {
        private static string simpleUrl = "http://int-portalwebservices/GISWS/GISWS.asmx";
        private static string advSearchUrl = "http://mgb-tst/esb/AdvCliSearchWS/";

        [TestMethod]
        public void SimpleSearch()
        {
            var results = Services.GeoCodingService.SimpleSearch(simpleUrl, null, null, "50322");
            Assert.AreEqual(true, results.Succeeded);
            Assert.AreEqual(1, results.Matches.Count);

            results = Services.GeoCodingService.SimpleSearch(simpleUrl, "Des Moines", "IA", null);
            Assert.AreEqual(true, results.Succeeded);
            Assert.AreNotEqual(0, results.Matches.Count);

            results = Services.GeoCodingService.SimpleSearch(simpleUrl, null, null, "asdf");
            Assert.AreEqual(true, results.Succeeded);
            Assert.AreEqual(0, results.Matches.Count);

            results = Services.GeoCodingService.SimpleSearch("http://wrong", null, null, "50322");
            Assert.AreEqual(false, results.Succeeded);
            Assert.AreNotEqual(null, results.Exception);
        }

        #region AIC
        private static Models.AdvancedClientSettings AICadvSettings = new Models.AdvancedClientSettings() { Url = advSearchUrl, Dsik = TestDSIKs.AIC, Username = "Test" };
        [TestMethod]
        public void ComplexSearch()
        {
            var results = Services.GeoCodingService.ComplexSearch(AICadvSettings, new Models.GeoCodingAddress() { Addr1 = "3840 109th", City = "Des Moines", State = "IA", PostalCode = "50322" });
            Assert.AreEqual(true, results.Succeeded);
            Assert.AreEqual(15, results.Matches.Count);

            results = Services.GeoCodingService.ComplexSearch(AICadvSettings, new Models.GeoCodingAddress() { Addr1 = "abc 123", City = "Des Moines", State = "IA", County = "polk" });
            Assert.AreEqual(true, results.Succeeded);

            results = Services.GeoCodingService.ComplexSearch(AICadvSettings, new Models.GeoCodingAddress() { Addr1 = "asdfasd", State = "IA" });
            Assert.AreEqual(true, results.Succeeded);

            results = Services.GeoCodingService.ComplexSearch(new Models.AdvancedClientSettings() { Url = advSearchUrl, Dsik = "baddsik" }, new Models.GeoCodingAddress() { Addr1 = "3840 109th", City = "Des Moines", State = "IA", PostalCode = "50322" });
            Assert.AreEqual(false, results.Succeeded);

            results = Services.GeoCodingService.ComplexSearch(new Models.AdvancedClientSettings() { Url = "http://wrong", Dsik = TestDSIKs.AIC }, new Models.GeoCodingAddress() { Addr1 = "3840 109th", City = "Des Moines", State = "IA", PostalCode = "50322" });
            Assert.AreEqual(false, results.Succeeded);
        }
        #endregion
    }
}
