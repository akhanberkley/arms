﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace BTS.WFA.Integration.Tests.PolicyStar
{
    [TestClass]
    public class ProdEnvironment
    {
        #region AIC
        private static string AICBatchUrl = "http://aicbatch-prd/BTS_PStar_SupportUtilitiesWeb/services/PremiumAuditService";
        private static string AICTransactionalUrl = "http://aic-prd/PolicySTAR_WebServices/services/PremiumAuditService";

        [TestMethod]
        public void AICGetPolicyByPolicyNumber()
        {
            var results = Services.PolicyStarTransactionalService.GetPolicyByPolicyNumber(AICTransactionalUrl, "0287129", "52");
            Assert.IsTrue(results.Succeeded);
        }

        [TestMethod]
        public void AICGetPolicyByPolicyNumberHasResults()
        {
            var results = Services.PolicyStarTransactionalService.GetPolicyByPolicyNumber(AICTransactionalUrl, "0305942", "52");
            Assert.IsTrue(results.Policies.Count > 0);
        }
        #endregion
    }
}
