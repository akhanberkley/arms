﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.Integration.Tests.PolicyStar
{
    [TestClass]
    public class TestEnvironment : IntegrationBaseTest
    {
        #region AIC
        private static string AICBatchUrl = "http://aicbatch-tst/BTS_PStar_SupportUtilitiesWeb/services/PremiumAuditService";
        private static string AICTransactionalUrl = "http://aic-prd/PolicySTAR_WebServices/services/PremiumAuditService";

        [TestMethod]
        public void AICSearch()
        {
            var x = Services.PolicyStarBatchService.GetPolicies(AICBatchUrl, "52", DateTime.Now.AddDays(-1));
        }

        [TestMethod]
        public void AICGetPolicyByPolicyNumber()
        {
            var results = Services.PolicyStarTransactionalService.GetPolicyByPolicyNumber(AICTransactionalUrl, "0305942", "52");
            Assert.IsTrue(results.Succeeded);
        }

        [TestMethod]
        public void AICGetPolicyByPolicyNumberHasResults()
        {
            var results = Services.PolicyStarTransactionalService.GetPolicyByPolicyNumber(AICTransactionalUrl, "0305942", "52");
            Assert.IsTrue(results.Policies.Count > 0);
        }
        #endregion
    }
}
