﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.Integration.Tests.Agencies
{
    [TestClass]
    public class BMAGTestEnvironment : IntegrationBaseTest
    {
        #region BMAG
        [TestMethod]
        public void BMAGstuff()
        {
            var settings = new Models.WorkflowSystemDetails()
            {
                BpmUrl = "http://genie2-tst/GenieService/Import",
                BpmVersion = 5,
                ApplicationName = "Account Reporting Management System",
                ContactName = "Account Reporting Management System",
                ContactEmail = "arms.server@wrberkley.com",
                CompanyNumber = "12",
                FilenetDocClass = "BMG_COMMERCIAL"
            };

            var item = new Models.WorkflowItem();
            item.TransactionType = "BMAGSurvey";
            item.Status = "Active";
            item.SubmissionNumber = "73600";
            item.Note = new Models.WorkflowNote() { Scope = "Transaction", User = "ARMS", Text = "Survey request created in ARMS" };

            byte[] fileData = Convert.FromBase64String("PGh0bWw+DQo8aGVhZD4NCgk8dGl0bGU+QVJNUyBTdXJ2ZXkgU3RhdHVzIGZvciAjMDwvdGl0bGU+DQo8L2hlYWQ+DQo8ZnJhbWVzZXQ+DQoJPGZyYW1lIGZyYW1lYm9yZGVyPSIwIiBzcmM9Imh0dHA6Ly9idHNkZWRldndmYTAxOjE4OTUvUmVhZE9ubHlTdXJ2ZXkuYXNweD9zdXJ2ZXlpZD04ZDQ0NDk5Yi0yMjcxLTQzMjMtOTMxYi1lNGE3MWE3OGRmZmUiIC8+PC9mcmFtZXNldD4NCjwvaHRtbD4NCg==");
            item.File = new Models.WorkflowFile() { Extension = "HTML", DocumentType = "LCR", Remarks = "Survey Request and Reports Attached", Data = fileData };

            var instance = Services.WorkflowService.CreateInstance(settings, item);
        }
        #endregion
    }
}
