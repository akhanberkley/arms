﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.Integration.Tests.Agencies
{
    [TestClass]
    public class ProductionEnvironment : IntegrationBaseTest
    {
        #region AIC
        private static string aicUrl = "http://aic-prd/apsws/services/APSService";

        [TestMethod]
        public void AICSearch()
        {
            var issueAgency = Services.AgencyService.GetAgency(aicUrl, "99521", false, null);
        }
        #endregion

        #region CWG
        private static string cwgUrl = "http://cwg-prd/apsws/services/APSService";

        [TestMethod]
        public void CWGSearch()
        {
            var issueAgency = Services.AgencyService.GetAgency(cwgUrl, "99908", false, null);
        }
        #endregion

        #region BLS
        private static string blsUrl = "http://bls-prd/apsws/services/APSService";

        [TestMethod]
        public void BLSSearch()
        {
            var issueAgency = Services.AgencyService.GetAgency(blsUrl, "01026", false, null);
        }
        #endregion

        #region BNP
        private static string bnpUrl = "http://bnp-prd/apsws/services/APSService";

        [TestMethod]
        public void BNPSearch()
        {
            var child = Services.AgencyService.GetAgency(bnpUrl, "31109", false, null);
            var parent = Services.AgencyService.GetAgency(bnpUrl, "99909", false, null);
        }
        #endregion

        #region USG
        private static string usgUrl = "http://usg-prd/apsws/services/APSService";

        [TestMethod]
        public void USGSearch()
        {
            var multilocation = Services.AgencyService.GetAgency(usgUrl, "03918", false, null);
        }
        #endregion

        #region BFM
        private static string bfmUrl = "http://bfm-prd/apsws/services/APSService";

        [TestMethod]
        public void BFMSearch()
        {
            var missingAgency = Services.AgencyService.GetAgency(bfmUrl, "03631", false, null);
        }
        #endregion

        #region BMG
        private static string bmgUrl = "http://bmg-prd/apsws/services/APSService";

        [TestMethod]
        public void BMGSearch()
        {
            var agency = Services.AgencyService.GetAgency(bmgUrl, "00112", false, null);
        }
        #endregion

        #region BARS
        private static string barsUrl = "http://bar-prd/apsws/services/APSService";

        [TestMethod]
        public void BARSearch()
        {
            var agency = Services.AgencyService.GetAgency(barsUrl, "41359", false, new[] { "UW", "NU", "AN" });
            var x = agency.Agency.UnderwritingUnits.FirstOrDefault(u => u.Code == "TX");
        }
        #endregion

    }
}
