﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.Integration.Tests.Agencies
{
    [TestClass]
    public class TestEnvironment : IntegrationBaseTest
    {
        #region BARS
        private static string barsUrl = "http://bar-tst/apsws/services/APSService";

        [TestMethod]
        public void BARSSearchById()
        {
            var agency = Services.AgencyService.GetAgency(barsUrl, "41377", false, null);
        }
        #endregion

        #region AIC
        private static string aicUrl = "http://aic-tst/apsws/services/APSService";

        [TestMethod]
        public void AICGetAllActive()
        {
            Services.AgencyService.GetAllAgencies(aicUrl);
        }
        [TestMethod]
        public void AICSearchById()
        {
            var agency = Services.AgencyService.GetAgency(aicUrl, "07080", false, null);
        }
        [TestMethod]
        public void AICParentChild()
        {
            var child = Services.AgencyService.GetAgency(aicUrl, "01017", false, null);
            var parent = Services.AgencyService.GetAgency(aicUrl, "01010", false, null);
        }
        #endregion

        #region CWG
        private static string cwgUrl = "http://cwg-tst/apsws/services/APSService";

        [TestMethod]
        public void CWGSearch()
        {
            //var agency = Services.AgencyService.GetAgency(cwgUrl, "40639", false, null);
            var agency = Services.AgencyService.GetAgency(cwgUrl, "40736", false, new[] { "UW" });
        }
        #endregion

        #region BRS
        private static string brsUrl = "http://brs-tst/apsws/services/APSService";

        [TestMethod]
        public void BRSSearch()
        {
            //var agency = Services.AgencyService.GetAgency(brsUrl, "189", true, null);//sub of 181
            var agency = Services.AgencyService.GetAgency(brsUrl, "0563", true, null);//sub of 0413
        }
        #endregion

        #region BLS
        private static string blsUrl = "http://bls-tst/apsws/services/APSService";

        [TestMethod]
        public void BLSSearch()
        {
            var agency = Services.AgencyService.GetAgency(blsUrl, "22222IL-1", false, null);
        }
        #endregion

        #region BMAG
        private static string bmagUrl = "http://bmg-tst/apsws/services/APSService";

        [TestMethod]
        public void BMAGSearch()
        {
            var agency = Services.AgencyService.GetAgency(bmagUrl, "02955", false, new[] { "UW" });
        }
        #endregion

        #region USG
        private static string usgUrl = "http://usg-tst/apsws/services/APSService";

        [TestMethod]
        public void USGSearch()
        {
            var agency = Services.AgencyService.GetAgency(usgUrl, "01090", false, new[] { "UW" });
        }
        #endregion
    }
}
