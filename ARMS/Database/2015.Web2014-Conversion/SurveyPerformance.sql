IF NOT EXISTS(SELECT * FROM sys.indexes  WHERE name='IX_SurveyNote_SurveyId' AND object_id = OBJECT_ID('SurveyNote'))
	CREATE NONCLUSTERED INDEX [IX_SurveyNote_SurveyId] ON [dbo].[SurveyNote] ([SurveyID])
IF NOT EXISTS(SELECT * FROM sys.indexes  WHERE name='IX_SurveyDocument_SurveyId' AND object_id = OBJECT_ID('SurveyDocument'))
	CREATE NONCLUSTERED INDEX [IX_SurveyDocument_SurveyId] ON [dbo].[SurveyDocument] ([SurveyID])