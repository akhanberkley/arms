using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import.Prospect
{
	public class BlcProspectWebServiceException : Exception
	{
		public BlcProspectWebServiceException(string strText)
			: base(strText)
		{

		}

        public BlcProspectWebServiceException(string strText, Exception innerException)
            : base(strText, innerException)
        {

        }

	}
}
