/*===========================================================================
  File:			FileNetServerP8.cs
  
  Summary:		Manages the interoperability with the FileNet server. 
			
  Classes:		FileNetServerP8
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:   	WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Berkley.BLC.Entities;
using System.Configuration;

namespace Berkley.Imaging.Server.FileNet
{
    /// <summary>
    /// Manages the interoperability with the FileNet server. 
    /// </summary>
    public class FileNetServerP8 : IImagingServerP8
    {
        #region fields
        /// <summary>
        /// The current configuration.
        /// </summary>
        private Berkley.BLC.Core.ConfigurationType configuration;
        /// <summary>
        /// The company.
        /// </summary>
        private Company imagingCompany;
        /// <summary>
        /// The company's FileNet user name - set when FileNetServerP8 instance is created.
        /// </summary>
        private string fileNetUsername;
        /// <summary>
        /// The company's FileNet password - set when FileNetServerP8 instance is created.
        /// </summary>
        private string fileNetPassword;
        /// <summary>
        /// The company's FileNet document class - NOW REQUIRED FOR P8 CALLS - set when FileNetServerP8 instance is created.
        /// </summary>
        private string fileNetDocClass;
        /// <summary>
        /// The company's Company Number - Needed to find documents in companies with multiple DocClasses
        /// </summary>
        //WA-3060 Doug Bruce 2015-10-06
        private string fileNetCompanyNo;

        private bool supportsFileNetIndexPolicySymbol;
        /// <summary>
        /// IBM SQL minimum date.  If you try to store or use C#'s minimum date (01/01/0001) with IBM SQL, it will crash.
        /// </summary>
        private DateTime SQLMinimumDate = DateTime.Parse("12/31/1752 20:00");
        /// <summary>
        /// Dynamic endpoint added to P8 to match the way IS FileNet server is initialized.
        /// </summary>
        //WA-905 Doug Bruce 2015-09-17 - Added to programmatically set P8 endpoint
        private string endpointName = String.Empty;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileNetServerP8() 
        {
            SetEndpointName();
        }
        /// <summary>
        /// Default constructor. Connects to the server specified by the 
        /// configuration parameter.
        /// </summary>
        /// <param name="configuration">The configuration which to connect.</param>
        /// <param name="imagingCompany">The company.</param>
        public FileNetServerP8(Berkley.BLC.Core.ConfigurationType configuration, Company imagingCompany)
        {
            this.configuration = configuration;
            this.imagingCompany = imagingCompany;

            //These are set implicitly based on the company passed in.
            this.fileNetUsername = imagingCompany.FileNetUsernameP8;
            this.fileNetPassword = imagingCompany.FileNetPasswordP8;
            this.fileNetDocClass = imagingCompany.FileNetDocClassP8;
            this.fileNetCompanyNo = imagingCompany.Number;
            this.supportsFileNetIndexPolicySymbol = imagingCompany.SupportsFileNetIndexPolicySymbol;

            SetEndpointName();
        }

        /// <summary>
        /// Constructor allowing explicit property initialization. Connects to the server specified by the 
        /// configuration parameter.
        /// </summary>
        /// <param name="configuration">The configuration which to connect.</param>
        /// <param name="imagingCompany">The company.</param>
        /// <param name="FileNetUserName">Explicitly set the FileNetUsername (instead of taking it from imagingCompany)</param>
        /// <param name="FileNetPassword">Explicitly set the FileNetPassword (instead of taking it from imagingCompany)</param>
        /// <param name="FileNetDocClass">Explicitly set the FileNetDocClass (instead of taking it from imagingCompany)</param>
        /// <param name="supportsFileNetIndexPolicySymbol"></param>
        public FileNetServerP8(Berkley.BLC.Core.ConfigurationType configuration, Company imagingCompany, string FileNetUserName, string FileNetPassword, string FileNetDocClass, bool supportsFileNetIndexPolicySymbol)
        {
            this.configuration = configuration;
            this.imagingCompany = imagingCompany;
            this.fileNetUsername = FileNetUserName;
            this.fileNetPassword = FileNetPassword;
            this.fileNetDocClass = FileNetDocClass;
            this.fileNetCompanyNo = imagingCompany.Number;
            this.supportsFileNetIndexPolicySymbol = supportsFileNetIndexPolicySymbol;

            SetEndpointName();
        }
        #endregion

        #region IImagingServerP8 Members
        /// <summary>
        /// Specifies the configuration to which the instance is targeted.
        /// </summary>
        public Berkley.BLC.Core.ConfigurationType Configuration
        {
            get
            {
                return this.configuration;
            }
            set
            {
                this.configuration = value;
            }
        }
        /// <summary>
        /// Specifies the configuration to FileNetUsername
        /// </summary>
        public string FileNetUserName
        {
            get
            {
                return this.fileNetUsername;
            }
            set
            {
                this.fileNetUsername = value;
            }
        }

        /// <summary>
        /// Specifies the configuration to FileNetPassword.
        /// </summary>
        public string FileNetPassword
        {
            get
            {
                return this.fileNetPassword;
            }
            set
            {
                this.fileNetPassword = value;
            }
        }

        /// <summary>
        /// Specifies the configuration to FileNetDocClass.
        /// </summary>
        public string FileNetDocClass
        {
            get
            {
                return this.fileNetDocClass;
            }
            set
            {
                this.fileNetDocClass = value;
            }
        }

        //WA-3060 Doug Bruce 2015-10-06 - Required to find docs for companies with multiple DocClasses
        /// <summary>
        /// Specifies the configuration to FileNetDocClass.
        /// </summary>
        public string FileNetCompanyNo
        {
            get
            {
                return this.fileNetCompanyNo;
            }
            set
            {
                this.fileNetCompanyNo = value;
            }
        }

        /// <summary>
        /// Specifies the FileNet uri to call.
        /// </summary>
        //WA-905 Doug Bruce 2015-09-17 - Added to programmatically set the P8
        //endpoint to make it obey the "ARMSConfigurationType" config setting.
        //One exception: PEI should ALWAYS point to production FileNet.
        public string EndpointName
        {
            get
            {
                return this.endpointName;
            }
            set
            {
                this.endpointName = value;
            }
        }

        /// <summary>
        /// Specifies the company.
        /// </summary>
        public bool SupportsFileNetIndexPolicySymbol
        {
            get
            {
                return this.supportsFileNetIndexPolicySymbol;
            }
            set
            {
                this.supportsFileNetIndexPolicySymbol = value;
            }
        }

        /// <summary>
        /// Specifies the company.
        /// </summary>
        public Company ImagingCompany
        {
            get
            {
                return this.imagingCompany;
            }
            set
            {
                this.imagingCompany = value;
            }
        }

        /// <summary>
        /// Sets the EndpointName property based on current environment and web.config entries.
        /// </summary>
        /// <param name="documentIn">The FileNet document to work on.</param>
        /// <returns>DocumentP8 document.</returns>
        //WA-905 Doug Bruce 2015-09-22 - Dynamic endpoint support
        public void SetEndpointName()
        {
            if (configuration == BLC.Core.ConfigurationType.Production)
            {
                string endpointConfigName = ConfigurationManager.AppSettings["ARMS-P8FileNetEndpoint-Prod"];
                if (endpointConfigName != String.Empty)
                {
                    endpointName = endpointConfigName;
                }
                else
                {
                    throw new Exception("FileNet P8 Uri (ARMS-P8FileNetEndpoint-Prod) is not set in the configuration file.");
                }
            }
            else
            {
                string endpointConfigName = ConfigurationManager.AppSettings["ARMS-P8FileNetEndpoint-Test"];
                if (endpointConfigName != String.Empty)
                {
                    endpointName = endpointConfigName;
                }
                else
                {
                    throw new Exception("FileNet P8 Uri (ARMS-P8FileNetEndpoint-Test) is not set in the configuration file.");
                }
            }
        }

        /// <summary>
        /// Retrieves an existing document (by Id) from the imaging 
        /// server.
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <returns>A document. If none is found, null is returned.</returns>
        //public IDocumentP8 RetrieveDocument(string id, string fileNetUsername, string fileNetPassword)
        public DocumentP8 RetrieveDocument(string id)
        {
            return RetrieveDocument(id, 0, 0);
        }
        /// <summary>
        /// Retrieves an existing document (by Id and page) from the imaging 
        /// server. The page parameter is currently exclusive to FileNet, and
        /// is not included in the IImagingServer interface. 
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <param name="page">The page to return.</param>
        /// <param name="attempts">The number of attempts at retrieving the list of indexes.</param>
        /// <param name="overrideDocClass">DocClass to override the one from the company object.</param>
        /// <returns>A document. If none is found, null is returned.</returns>
        //public IDocumentP8 RetrieveDocument(string id, int page, int attempts, string fileNetUsername, string fileNetPassword)
        public DocumentP8 RetrieveDocument(string id, int page, int attempts)
        {
            DocumentP8 result;

            try
            {
                /*
                 * obtain a reference to the FileNet server
                 */
                FileNetServiceP8.P8ServiceClient p8 = new FileNetServiceP8.P8ServiceClient(this.endpointName);

                /*
                 * This causes FileNet to ignore any parameters that it can't reconcile with the company sent in.
                 * Pass as last parameter of most P8 calls.
                 */
                List<FileNetServiceP8.entry> additionalParams = new List<FileNetServiceP8.entry>();
                FileNetServiceP8.entry fnEntry = new FileNetServiceP8.entry();
                fnEntry.key = "ignoreInvalidProperties";
                fnEntry.value = "true";
                additionalParams.Add(fnEntry);

                /*
                 * This allows Filename parameter (and possibly others relating to the file) to be passed back.
                 * Pass additionalParams as last parameter of most P8 calls.
                 */
                fnEntry = new FileNetServiceP8.entry();
                fnEntry.key = "includeFilenameProperties";
                fnEntry.value = "true";
                additionalParams.Add(fnEntry);

                /*
                 * retrieve the document
                 */
                if (IsValidId(id))
                {
                    FileNetServiceP8.document doc1 = p8.getDocumentByID(fileNetUsername, fileNetPassword, fileNetDocClass, id, true, additionalParams.ToArray());
                    result = ImportFromFileNet(doc1);
                }
                else
                {
                    result = null;
                }
            }
            catch (System.Web.Services.Protocols.SoapException se)
            {
                /*
                 * Error in receiving RPC.: Socket closed, so try again
                 */
                if (attempts <= 5)
                {
                    //attempt to add again
                    result = RetrieveDocument(id, page, attempts + 1);
                }
                else
                {
                    throw new Exception("Attempted to retrieve document 5 times from imaging without success.  See inner exception for details.", se);
                }
            }

            return result;
        }

        /// <summary>
        /// Retrieves the page count index. 
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <returns>The page count.</returns>
        public Int32 RetrievePageCount(string id)
        {
            int result = 0;

            /*
             * This causes FileNet to ignore any parameters that it can't reconcile with the company sent in.
             * Pass additionalParams as last parameter of most P8 calls.
             */
            List<FileNetServiceP8.entry> additionalParams = new List<FileNetServiceP8.entry>();
            FileNetServiceP8.entry fnEntry = new FileNetServiceP8.entry();
            fnEntry.key = "ignoreInvalidProperties";
            fnEntry.value = "true";
            additionalParams.Add(fnEntry);

            /*
             * This allows Filename parameter (and possibly others relating to the file) to be passed back.
             * Pass additionalParams as last parameter of most P8 calls.
             */
            fnEntry = new FileNetServiceP8.entry();
            fnEntry.key = "includeFilenameProperties";
            fnEntry.value = "true";
            additionalParams.Add(fnEntry);

            /*
             * retrieve the document
             */
            if (IsValidId(id))
            {
                FileNetServiceP8.P8ServiceClient p8 = new FileNetServiceP8.P8ServiceClient(this.endpointName);
                FileNetServiceP8.property[] propertiesArray = p8.getDocumentProperties(fileNetUsername, fileNetPassword, fileNetDocClass, id, additionalParams.ToArray());

                foreach (FileNetServiceP8.property prop in propertiesArray)
                {
                    if (prop.name == FileNetPropertyP8.FileName)    //WA-756, Doug Bruce, 2015-06-23 - P8 - Used for finding/counting pages in a multipage document (split over multiple ContentList entries).
                    {
                        result++;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Retrieves a list of indexes (by the filter passed) from the imaging server.
        /// </summary>
        /// <param name="document">The document to get the company entity from.</param>
        /// <param name="policyNumbers">The policy number filter for the list of indexes.</param>
        /// <param name="clientID">The client id filter for the list of indexes.</param>
        /// <param name="docID">The doc id filter for the list of indexes.</param>
        /// <param name="docTypes">The doc type filter for the list of indexes.</param>
        /// <param name="entryStartDate">The entry start date filter for the list of indexes.</param>
        /// <param name="entryEndDate">The entry end date filter for the list of indexes.</param>
        /// <param name="attempts">The number of attempts at retrieving the list of indexes.</param>
        /// <param name="fileNetPolicyLike">True if looking for partial policy numbers.</param>
        /// <returns>A document. If none is found, null is returned.</returns>
        public List<DocumentP8> RetrieveIndexes(DocumentP8 document, string[] policyNumbers, string clientID, string docID, string[] docTypes, DateTime entryStartDate, DateTime entryEndDate, int attempts, bool fileNetPolicyLike)
        {
            string strConditions = string.Empty;

            //Build the list of conditions
            try
            {
                FileNetDocumentP8 doc = document as FileNetDocumentP8;

                //Build the list of conditions
                //WA-3060 Doug Bruce 2015-10-06 - fileNetCompanyNo is set explictly (in _serverP8 instance) by the caller.
                string whereConditions = doc.BuildFilters(configuration, policyNumbers, clientID, docID, docTypes, entryStartDate, entryEndDate, fileNetPolicyLike,this.fileNetCompanyNo);

                string returnedIndexes = "F_DOCNUMBER,PolicyNo,PolicySymbol,DocType,F_ENTRYDATE,DocRemarks";
                if (!SupportsFileNetIndexPolicySymbol)
                {
                    returnedIndexes = returnedIndexes.Replace("PolicySymbol,", string.Empty);
                }

                /*
                 * obtain a reference to the FileNet server
                 */
                FileNetServiceP8.P8ServiceClient p8 = new FileNetServiceP8.P8ServiceClient(this.endpointName);

                List<FileNetServiceP8.searchCriteria> searchCriteria = new List<FileNetServiceP8.searchCriteria>();
                FileNetServiceP8.searchCriteria srchCriteria = new FileNetServiceP8.searchCriteria();
                srchCriteria.docClass = fileNetDocClass;
                srchCriteria.whereClause = "(" + whereConditions + ")";
                //srchCriteria.contentSearchString = null;
                srchCriteria.fields = returnedIndexes;
                srchCriteria.maxResults = "30";
                srchCriteria.searchTimeoutSeconds = 5;
                searchCriteria.Add(srchCriteria);

                List<FileNetServiceP8.entry> searchOptions = new List<FileNetServiceP8.entry>();
                FileNetServiceP8.entry srchOptions = new FileNetServiceP8.entry();
                srchOptions.key = "orderByInsertion";
                srchOptions.value = "desc";
                searchOptions.Add(srchOptions);

                /*
                 * This causes FileNet to ignore any parameters that it can't reconcile with the company sent in.
                 * Pass additionalParams as last parameter of most P8 calls.
                 */
                List<FileNetServiceP8.entry> additionalParams = new List<FileNetServiceP8.entry>();
                FileNetServiceP8.entry fnEntry = new FileNetServiceP8.entry();
                fnEntry.key = "ignoreInvalidProperties";
                fnEntry.value = "true";
                additionalParams.Add(fnEntry);

                /*
                 * This allows Filename parameter (and possibly others relating to the file) to be passed back.
                 * Pass additionalParams as last parameter of most P8 calls.
                 */
                fnEntry = new FileNetServiceP8.entry();
                fnEntry.key = "includeFilenameProperties";
                fnEntry.value = "true";
                additionalParams.Add(fnEntry);

                FileNetServiceP8.document[] documents = null;

                /*
                * retrieve the indexes
                */
                documents = p8.searchReleasedDocuments(fileNetUsername, fileNetPassword, searchCriteria.ToArray(), false, searchOptions.ToArray(), additionalParams.ToArray());

                /*
                 * For previous way this was done, see FileNetServer.csetDocumentP8[];
                */
                List<DocumentP8> dList = new List<DocumentP8>();

                if (documents != null)
                {
                    foreach (FileNetServiceP8.document d in documents)
                    {
                        dList.Add(ImportFromFileNet(d));
                    }
                }

                return dList;
                //return dList.ToArray() as FileNetDocumentP8[];
            }
            catch (System.Web.Services.Protocols.SoapException se)
            {
                /*
                 * Error retrieving error message from IS., error code: FN_IS_RA_10370
                 */
                if (attempts <= 5)
                {
                    //attempt to retrieve again
                    return RetrieveIndexes(document, policyNumbers, clientID, docID, docTypes, entryStartDate, entryEndDate, attempts + 1, fileNetPolicyLike);
                }
                else
                {
                    throw new Exception(string.Format("Attempted to retrieve the indexes 5 times from imaging without success using the following search conditions: {0}.  See inner exception for details.", strConditions), se);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Failed to fetch indexes with the following search conditions: {0}", strConditions), ex);
            }
        }
        /// <summary>
        /// Removes an existing document (by Id and page) from the imaging 
        /// server. The page parameter is currently exclusive to FileNet, and
        /// is not included in the IImagingServer interface. 
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <returns>A completed string. If none is found, null is returned.</returns>
        public void RemoveDocument(string id)
        {
            int idValue;
            bool IsPreP8id = int.TryParse(id, out idValue);       //Determine if Id is int (IS) or guid (P8).
            /*
             * remove the document
             */
            if (IsValidId(id))
            {
                FileNetServiceP8.P8ServiceClient p8 = new FileNetServiceP8.P8ServiceClient(this.endpointName);

                /*
                 * This causes FileNet to ignore any parameters that it can't reconcile with the company sent in.
                 * Pass additionalParams as last parameter of most P8 calls.
                 */
                List<FileNetServiceP8.entry> additionalParams = new List<FileNetServiceP8.entry>();

                //FileNetServiceP8.document update = new FileNetServiceP8.document();
                FileNetServiceP8.property[] indexes = p8.getDocumentProperties(fileNetUsername, fileNetPassword, fileNetDocClass, id, additionalParams.ToArray());

                //since we can no longer remove the actual document form FileNet, 
                //mimic a delete by removing the client id index.  This will prevent
                //the document to appear in apps like BPM
                if (indexes.Length > 0)
                {
                    List<FileNetServiceP8.property> indexKillList = new List<FileNetServiceP8.property>();

                    foreach (FileNetServiceP8.property index in indexes)
                    {
                        if (index.name == "Id" && IsPreP8id)
                        {
                            id = index.value;   //This will get the guid for the P8 search.
                        }

                        if (fileNetUsername.ToLower().Contains("bog") && (index.name == FileNetPropertyP8.InsuredName ||
                            index.name == FileNetPropertyP8.AgencyNumber || index.name == FileNetPropertyP8.DocType))
                        {
                            //Can't remove these required indexes for BOG due to FileNet requirements
                        }
                        else
                        {
                            if (index.name == FileNetPropertyP8.ClientID || index.name == FileNetPropertyP8.PolicySymbol ||
                                index.name == FileNetPropertyP8.ScanDate || index.name == FileNetPropertyP8.DocRemarks ||
                                index.name == FileNetPropertyP8.EffectiveDate || index.name == FileNetPropertyP8.ExpirationDate ||
                                index.name == FileNetPropertyP8.PolicyNumber || index.name == FileNetPropertyP8.SubmissionNumber)
                            {
                                if (index.dataType.ToString() == "DATE")
                                {
                                    index.value = string.Empty;
                                    index.dateValue = SQLMinimumDate;
                                    indexKillList.Add(index);
                                }
                                else
                                {
                                    index.value = string.Empty;
                                    indexKillList.Add(index);
                                }
                            }
                        }
                    }

                    //Now, Client related indexes are blank and compiled into a subset of the original properties list.
                    // Send the array of just these to FileNet to update.  That will cause the doc to "disappear" to ARMS.

                    FileNetServiceP8.property[] props = indexKillList.ToArray();

                    p8.updateDocumentProperties(fileNetUsername, fileNetPassword, fileNetDocClass, id, props, additionalParams.ToArray());
                }
            }
            else
            {
                throw new InvalidCastException(string.Format("The FileNet Reference is invalid: {0}.", id));
            }
        }

        /// <summary>
        /// Adds an new document to the imaging system.
        /// </summary>
        /// <param name="documentIn">The document to add.</param>
        /// <returns>The document Id.</returns>
        public string Add(DocumentP8 documentIn)
        {
            return Add(documentIn, 0);
        }
        /// <summary>
        /// Adds an new document to the imaging system.
        /// </summary>
        /// <param name="documentIn">The document to add.</param>
        /// <param name="attempts">The number of attempts at adding the doc.</param>
        /// <returns>The document Id.</returns>
        private string Add(DocumentP8 documentIn, int attempts)
        {
            string id;

            /*
             * obtain a reference to the FileNet server
             */
            FileNetServiceP8.P8ServiceClient p8 = new FileNetServiceP8.P8ServiceClient(this.endpointName);

            try
            {
                if (documentIn != null)
                {
                    //Translate current docs to form P8 understands
                    FileNetServiceP8.document documentOut = new FileNetServiceP8.document();

                    //Content (Files)
                    List<FileNetServiceP8.content> cList = new List<FileNetServiceP8.content>();
                    foreach (ContentP8 contentIn in documentIn.ContentList)
                    {
                        FileNetServiceP8.content contentOut = new FileNetServiceP8.content();
                        contentOut.content1 = contentIn.ContentBytes;
                        contentOut.fileName = contentIn.FileName;
                        contentOut.mimeType = contentIn.MimeType;
                        cList.Add(contentOut);
                    }

                    //Properties (Indexes)
                    List<FileNetServiceP8.property> pList = new List<FileNetServiceP8.property>();
                    foreach (PropertyP8 propertyIn in documentIn.Properties)
                    {
                        FileNetServiceP8.property propertyOut = new FileNetServiceP8.property();
                        propertyOut.name = propertyIn.Name;
                        propertyOut.value = propertyIn.Value;
                        pList.Add(propertyOut);
                    }

                    //Document
                    documentOut.contentList = cList.ToArray();

                    if (documentOut.contentList.Length > 0)  //Make sure at least one file exists to pull filename from.
                    {
                        FileNetServiceP8.property pDocTitle = new FileNetServiceP8.property();
                        pDocTitle.name = "DocumentTitle";
                        pDocTitle.value = Path.GetFileName(documentOut.contentList[0].fileName);   //Take filename from first file (should be only one for our uses).
                        pList.Add(pDocTitle);
                    }

                    documentOut.properties = pList.ToArray();
                    documentOut.guid = documentIn.Id;
                    documentOut.documentClass = documentIn.DocClass;

                    /*
                     * This causes FileNet to ignore any parameters that it can't reconcile with the company sent in.
                     * Pass additionalParams as last parameter of most P8 calls.
                     */
                    List<FileNetServiceP8.entry> additionalParams = new List<FileNetServiceP8.entry>();

                    FileNetServiceP8.entry fnEntry = new FileNetServiceP8.entry();
                    fnEntry.key = "ignoreInvalidProperties";
                    fnEntry.value = "true";
                    additionalParams.Add(fnEntry);

                    /*
                     * This allows Filename parameter (and possibly others relating to the file) to be passed back.
                     * Pass additionalParams as last parameter of most P8 calls.
                     */
                    fnEntry = new FileNetServiceP8.entry();
                    fnEntry.key = "includeFilenameProperties";
                    fnEntry.value = "true";
                    additionalParams.Add(fnEntry);

                    //PAYOFF: FileNetP8 call
                    string transID = "";
                    id = p8.addDocument(fileNetUsername, fileNetPassword, documentOut, transID, additionalParams.ToArray());
                }
                else
                {
                    id = string.Empty;
                }
            }
            catch (NullReferenceException)
            {
                /*
                 * swallow the NullReferenceException, but float any others to 
                 * the caller.
                 */
                id = string.Empty;
            }
            catch (System.Net.WebException we)
            {
                /*
                 * An existing connection was forcibly closed by the remote host so
                 * try resubmitting the request.
                 */
                int allowed = int.Parse(Berkley.BLC.Core.CoreSettings.AddDocAllowedAttempts);
                if (attempts <= allowed)
                {
                    //release the connection
                    p8.Close();

                    //reset the position
                    foreach (ContentP8 file in documentIn.ContentList)
                    {
                        file.Content.Position = 0;
                    }

                    //attempt to add again
                    id = Add(documentIn, attempts + 1);
                }
                else
                {
                    throw new Exception(string.Format("Attempted to submit document {0} times to imaging without success.  See inner exception for details.", allowed), we);
                }
            }
            catch (System.Web.Services.Protocols.SoapException se)
            {
                /*
                 * com.bts.imaging.rs.exception.ImagingServiceException
                 */
                int allowed = int.Parse(Berkley.BLC.Core.CoreSettings.AddDocAllowedAttempts);
                if (attempts <= allowed)
                {
                    //release the connection
                    p8.Close();

                    //reset the position
                    foreach (ContentP8 file in documentIn.ContentList)
                    {
                        file.Content.Position = 0;
                    }

                    //attempt to add again
                    id = Add(documentIn, attempts + 1);
                }
                else
                {
                    throw new Exception(string.Format("Attempted to submit document {0} times to imaging without success.  See inner exception for details.", allowed), se);
                }
            }
            catch (System.Xml.XmlException xe)
            {
                /*
                 * com.bts.imaging.rs.exception.ImagingServiceException
                 */
                int allowed = int.Parse(Berkley.BLC.Core.CoreSettings.AddDocAllowedAttempts);
                if (attempts <= allowed)
                {
                    //release the connection
                    p8.Close();

                    //reset the position
                    foreach (ContentP8 file in documentIn.ContentList)
                    {
                        file.Content.Position = 0;
                    }

                    //attempt to add again
                    id = Add(documentIn, attempts + 1);
                }
                else
                {
                    throw new Exception(string.Format("Attempted to submit document {0} times to imaging without success.  See inner exception for details.", allowed), xe);
                }
            }
            finally
            {
                /*
                 * release the connection
                 */
                p8.Close();
            }

            /*
             * return the results
             */
            return id;
        }

        /// <summary>
        /// Updates an existing document in the imaging system.
        /// </summary>
        /// <param name="documentIn">The document to update.</param>
        public void UpdateProperties(string docId, List<PropertyP8> updatedProperties)
        {
            /*
             * obtain a reference to the FileNet server
             */
            FileNetServiceP8.P8ServiceClient p8 = new FileNetServiceP8.P8ServiceClient(this.endpointName);

            /*
             * This causes FileNet to ignore any parameters that it can't reconcile with the company sent in.
             * Pass additionalParams as last parameter of most P8 calls.
             */
            List<FileNetServiceP8.entry> additionalParams = new List<FileNetServiceP8.entry>();

            FileNetServiceP8.entry fnEntry = new FileNetServiceP8.entry();
            fnEntry.key = "ignoreInvalidProperties";
            fnEntry.value = "true";
            additionalParams.Add(fnEntry);

            /*
             * This allows Filename parameter (and possibly others relating to the file) to be passed back.
             * Pass additionalParams as last parameter of most P8 calls.
             */
            fnEntry = new FileNetServiceP8.entry();
            fnEntry.key = "includeFilenameProperties";
            fnEntry.value = "true";
            additionalParams.Add(fnEntry);

            try
            {
                FileNetServiceP8.property[] updatedPropArray = new FileNetServiceP8.property[updatedProperties.Count];

                for (int i=0; i<updatedProperties.Count; i++)
                {
                    FileNetServiceP8.property thisProp = new FileNetServiceP8.property();
                    thisProp.name = updatedProperties[i].Name;
                    thisProp.value = updatedProperties[i].Value;
                    updatedPropArray[i] = thisProp;
                }

                p8.updateDocumentProperties(fileNetUsername, fileNetPassword, fileNetDocClass, docId, updatedPropArray, additionalParams.ToArray());
            }
            catch (NullReferenceException)
            {
                /*
                 * swallow the NullReferenceException, but float any others to 
                 * the caller.
                 */
            }
            finally
            {
                /*
                 * release the connection
                 */
                p8.Close();
            }
        }

        public string showProperties(FileNetServiceP8.property[] props)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < props.Length; i++)
            {
                result.Append(props[i].name);
                result.Append("=");
                result.Append(props[i].value);
                result.Append("\r\n");
            }
            return result.ToString();
        }

        /*
         *Utility functions to translate from the raw FileNet document format to ARMS internal document format (which includes company,policy,etc info). 
         */

        /// <summary>
        /// Copies information from a FileNetServiceP8 document into a DocumentP8 object.
        /// </summary>
        /// <param name="documentIn">The FileNet document to work on.</param>
        /// <returns>DocumentP8 document.</returns>
        public DocumentP8 ImportFromFileNet(FileNetServiceP8.document documentIn)
        {
            if (documentIn != null)
            {
                DocumentP8 documentOut = new DocumentP8();
                documentOut.ContentList = ImportContents(documentIn.contentList);
                documentOut.Properties = ImportProperties(documentIn.properties);
                documentOut.DocClass = documentIn.documentClass;
                documentOut.Id = documentIn.guid;
                documentOut.FolderPath = documentIn.folderPath;
                documentOut.Company = null;
                documentOut.DocNumber = GetThisStringProperty(documentIn.properties, FileNetIndexP8.DocNumber);
                documentOut.DocRemarks = GetThisStringProperty(documentIn.properties, FileNetIndexP8.DocRemarks);
                documentOut.DocType = GetThisStringProperty(documentIn.properties, FileNetIndexP8.DocType);
                documentOut.EntryDate = GetThisStringProperty(documentIn.properties, FileNetIndexP8.EntryDate);
                documentOut.MimeType = GetThisStringProperty(documentIn.properties, FileNetIndexP8.MimeType);
                documentOut.Extension = GetExtensionFromMimeType(documentOut.MimeType);
                //Try to provide a good suggested Attachment filename for attaching to Audit.
                //Priority:
                //1 - DocRemarks
                //2 - FileName
                //3 - ID (guid)
                documentOut.FileName = documentOut.DocRemarks;
                if (documentOut.FileName == null)
                {
                    documentOut.FileName = Path.GetFileName(GetThisStringProperty(documentIn.properties, FileNetIndexP8.FileName));
                }
                if (documentOut.FileName == null)
                {
                    documentOut.FileName = Path.GetFileName(GetThisStringProperty(documentIn.properties, FileNetIndexP8.ID));
                }
                documentOut.PolicyNumber = GetThisStringProperty(documentIn.properties, FileNetIndexP8.PolicyNumber);
                documentOut.PolicySymbol = GetThisStringProperty(documentIn.properties, FileNetIndexP8.PolicySymbol);
                documentOut.Type = Imaging.DocumentType.Underwriting;
                documentOut.PageCount = 0;
                return documentOut;
            }
            else
            {
                return null;
            }
        }

        private string GetThisStringProperty(FileNetServiceP8.property[] props, string name)
        {
            string result = String.Empty;
            for (int i=0; i<props.Length; i++){
                //WA-3060 Doug Bruce 2015-10-06 - prevent "Obj not set" error by short-circuiting null name condition.
                if (props[i].name!=null && props[i].name.ToLower() == name.ToLower())
                {
                    result = props[i].value;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Copies information from FileNetServiceP8 document content list into a DocumentP8 doc content list.
        /// </summary>
        /// <param name="filesFileNetIn">The FileNet document contentList (previously files) to work on.</param>
        /// <returns>IContentCollectionP8 content list.</returns>
        private IContentCollectionP8 ImportContents(FileNetServiceP8.content[] filesFileNetIn)
        {
            if (filesFileNetIn != null)
            {
                IContentCollectionP8 filesP8Out = new IContentCollectionP8();
                for (int i = 0; i < filesFileNetIn.Length; i++)
                {
                    ContentP8 fileP8 = new ContentP8();
                    fileP8.ContentBytes = new byte[filesFileNetIn[i].contentMTOM.Length];
                    fileP8.ContentBytes = filesFileNetIn[i].contentMTOM;
                    fileP8.FileName = filesFileNetIn[i].fileName;
                    fileP8.MimeType = filesFileNetIn[i].mimeType;
                    filesP8Out.Add(fileP8);
                }
                return filesP8Out;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Copies information from FileNetServiceP8 document property list into a DocumentP8 doc property list.
        /// </summary>
        /// <param name="idxsFileNetIn">The FileNet document Properties (previously indexes) to work on.</param>
        /// <returns>IPropertyCollectionP8 property list.</returns>
        private IPropertyCollectionP8 ImportProperties(FileNetServiceP8.property[] idxsFileNetIn)
        {
            if (idxsFileNetIn != null)
            {
                IPropertyCollectionP8 idxsP8Out = new IPropertyCollectionP8();
                for (int i = 0; i < idxsFileNetIn.Length; i++)
                {
                    ////Put in test to see if they are indexes vs other properties?
                    PropertyP8 idxP8 = new PropertyP8();
                    idxP8.Name = idxsFileNetIn[i].name;
                    idxP8.Value = idxsFileNetIn[i].value;
                    idxsP8Out.Add(idxP8);
                }
                return idxsP8Out;
            }
            else
            {
                return null;
            }
        }

        /*
         *Utility functions to translate from ARMS internal document format (which includes company,policy,etc info) to the raw FileNet document format. 
         */

        /// Copies information from a DocumentP8 document into a FileNetServiceP8 object.
        /// </summary>
        /// <param name="documentIn">The DocumentP8 document to work on.</param>
        /// <returns>FileNetServiceP8.document[] document array.</returns>
        public FileNetServiceP8.document ExportToFileNet(DocumentP8 documentIn, bool includeProperties)
        {
            FileNetServiceP8.document documentOut = new FileNetServiceP8.document();
            try
            {
                documentOut.contentList = ExportContents(documentIn.ContentList);
                if (includeProperties)  //WA-674 Doug Bruce 2015-05-14 - no properties are saved when attaching a doc to a survey.
                {
                    documentOut.properties = ExportProperties(documentIn.Properties);
                }
                documentOut.documentClass = documentIn.DocClass;
                documentOut.guid = documentIn.Id;
                documentOut.folderPath = documentIn.FolderPath;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
            return documentOut;
        }

        /// <summary>
        /// Copies information from DocumentP8 document content list into a FileNetServiceP8 doc content array.
        /// </summary>
        /// <param name="contentListIn">The ContentP8 collection to work on.</param>
        /// <returns>FileNetServiceP8.content[] content array.</returns>
        private FileNetServiceP8.content[] ExportContents(IContentCollectionP8 contentListIn)
        {
            List<FileNetServiceP8.content> contentListOut = new List<FileNetServiceP8.content>();
            //{"Unable to cast object of type 'Berkley.Imaging.ContentP8' to type 'Berkley.Imaging.Server.FileNet.FileNetServiceP8.content'."}
            //foreach (FileNetServiceP8.content content in contentListIn)
            for (int i=0; i<contentListIn.Count; i++)
            {
                FileNetServiceP8.content content = new FileNetServiceP8.content();
                content.contentMTOM = contentListIn[i].ContentBytes;
                content.fileName = contentListIn[i].FileName;
                content.mimeType = contentListIn[i].MimeType;
                contentListOut.Add(content);
            }
            return contentListOut.ToArray();
        }

        /// <summary>
        /// Copies information from DocumentP8 document property list into a FileNetServiceP8 doc property array.
        /// </summary>
        /// <param name="propertiesIn">The PropertyP8 collection to work on.</param>
        /// <returns>FileNetServiceP8.property[] property array.</returns>
        private FileNetServiceP8.property[] ExportProperties(IPropertyCollectionP8 propertiesIn)
        {
            List<FileNetServiceP8.property> propertiesOut = new List<FileNetServiceP8.property>();
            //foreach (FileNetServiceP8.property prop in propertiesIn)
            for (int i = 0; i < propertiesIn.Count; i++)
            {
                FileNetServiceP8.property prop = new FileNetServiceP8.property();
                prop.name = propertiesIn[i].Name;
                prop.value = propertiesIn[i].Value;
                propertiesOut.Add(prop);
            }
            return propertiesOut.ToArray();
        }

        /// <summary>
        /// Creates a new, empty document.
        /// </summary>
        /// <returns>A new, empty document.</returns>
        public DocumentP8 CreateDocument()
        {
            return new FileNetDocumentP8();
        }
        /// <summary>
        /// Creates a new, empty file.
        /// </summary>
        /// <returns>A new, empty file.</returns>
        public ContentP8 CreateFile()
        {
            return new FileNetContentP8();
        }
        /// <summary>
        /// Creates a new, empty index.
        /// </summary>
        /// <returns>A new, empty index.</returns>
        public PropertyP8 CreateIndex()
        {
            return new FileNetPropertyP8();
        }
        #endregion

        #region methods
        /// <summary>
        /// Sets items required for FileNet call for this company.
        /// </summary>
        /// <param name="Username">This company's FileNet P8 username</param>
        /// <param name="Password">This company's FileNet P8 password</param>
        /// <param name="DocClass">This company's FileNet P8 Doc Class</param>
        public void SetUser(string fnUsername, string fnPassword, string fnDocClass, string fnCompanyNo)
        {
            this.fileNetUsername = fnUsername;
            this.fileNetPassword = fnPassword;
            this.fileNetDocClass = fnDocClass;
            this.fileNetCompanyNo = fnCompanyNo;
        }
        /// <summary>
        /// Determines whether or not the provided id is valid. FileNet P8
        /// uses guid Ids, but also accepts the previous int Ids for compatibility.
        /// This method determines if the id is either a valid guid or int.
        /// </summary>
        /// <param name="id">The string to test.</param>
        /// <returns>True if the id is valid, false otherwise.</returns>
        private static bool IsValidId(string id)
        {
            Guid resultP8;
            if (System.Guid.TryParse(id, out resultP8))
            {
                return true;
            }
            else
            {
                int resultIS;
                return int.TryParse(id, out resultIS);
            }
        }

        /// <summary>
        /// Returns a document's extension based on the MIME type passed and status of P8BypassPDFConversion company parameter.
        /// </summary>
        /// <param name="mimeType">The MIME Type.</param>
        public static string GetExtensionFromMimeType(string mimeType)
        {
            return GetExtensionFromMimeType(mimeType, true);   //.tif comes back .tif, not .pdf
        }

        /// <summary>
        /// Returns a document's extension based on the MIME type and passed.
        /// </summary>
        /// <param name="mimeType">The MIME Type.</param>
        /// <param name="returnImageExt">if .tif mime type, true returns .tif ext, false returns .pdf ext</param>
        private static string GetExtensionFromMimeType(string mimeType, bool returnImageExt)
        {
            string result;
            mimeType = mimeType.Trim().ToLower();

            if (mimeType.Contains("pdf"))
            {
                result = ".pdf";
            }
            else if (mimeType.Contains("rtf"))
            {
                result = ".rtf";
            }
            else if (mimeType.Contains("wordprocessingml.document"))        //Doug Bruce 2015-05-18 - transferred from ARMS MimeType table
            {
                result = ".docx";
            }
            else if (mimeType.Contains("wordprocessingml.template"))        //WA-683 Doug Bruce 2015-05-18 - transferred from ARMS MimeType table
            {
                result = ".dotx";
            }
            else if (mimeType.Contains("word.document.macroenabled.12"))    //Doug Bruce 2015-05-18 - transferred from ARMS MimeType table
            {
                result = ".docm";
            }
            else if (mimeType.Contains("word.template.macroenabled.12"))    //WA-684 Doug Bruce 2015-05-18 - transferred from ARMS MimeType table
            {
                result = ".dotm";
            }
            else if (mimeType.Contains("msword") || mimeType.Contains("rfc822"))    //Leave general Word case for last
            {
                result = ".doc";
            }
            else if (mimeType.Contains("excel"))
            {
                result = ".xls";
            }
            else if (mimeType.Contains("spreadsheetml"))
            {
                result = ".xlsx";
            }
            else if (mimeType.Contains("gif"))
            {
                result = ".gif";
            }
            else if (mimeType.Contains("octet"))
            {
                result = ".tif";
            }
            else if (mimeType.Contains("tif") || mimeType.Contains("InterCAP_CGM"))
            {
                result = (returnImageExt) ? ".tif" : ".pdf";
            }
            else if (mimeType.Contains("jpeg") || mimeType.Contains("jpg"))
            {
                result = ".jpg";
            }
            else if (mimeType.Contains("bmp"))
            {
                result = ".bmp";
            }
            else if (mimeType.Contains("plain"))
            {
                result = ".txt";
            }
            else if (mimeType.Contains("powerpoint.presentation.macroenabled.12"))  //WA-685 Doug Bruce 2015-05-18 - transferred from ARMS MimeType table
            {
                result = ".pptm";
            }
            else if (mimeType.Contains("presentationml.presentation"))             //Doug Bruce 2015-05-18 - transferred from ARMS MimeType table
            {
                result = ".pptx";
            }
            else if (mimeType.Contains("powerpoint.slideshow.macroenabled.12"))    //WA-687 Doug Bruce 2015-05-18 - transferred from ARMS MimeType table
            {
                result = ".ppsm";
            }
            else if (mimeType.Contains("presentationml.slideshow"))                //WA-686 Doug Bruce 2015-05-18 - transferred from ARMS MimeType table
            {
                result = ".ppsx";
            }
            else if (mimeType.Contains("powerpoint"))                              //Leave general powerpoint case for last
            {
                result = ".ppt";
            }
            else if (mimeType.Contains("vnd.ms-xpsdocument") || mimeType.Contains("oxps"))
            {
                result = ".xps";
            }
            // WA-43 Doug Bruce 2015-05-08 - Add mime types
            //http://www.freeformatter.com/mime-types-list.html
            //http://www.sitepoint.com/web-foundations/mime-types-complete-list/
            else if (mimeType.Contains("application/vnd.ms-outlook"))
            {
                result = ".msg";
            }
            else if (mimeType.Contains("postscript"))
            {
                result = ".ai";
            }
            else if (mimeType.Contains("png"))
            {
                result = ".png";
            }
            else if (mimeType.Contains("mp3"))
            {
                result = ".mp3";
            }
            else if (mimeType.Contains("mp4"))
            {
                result = ".mp4";
            }
            else if (mimeType.Contains("x-ms-wma"))
            {
                result = ".wma";
            }
            else if (mimeType.Contains("rar"))
            {
                result = ".rar";
            }
            else if (mimeType.Contains("rar"))
            {
                result = ".rar";
            }
            else if (mimeType.Contains("csv"))
            {
                result = ".csv";
            }
            else if (mimeType.Contains("onenote"))
            {
                result = ".one";
            }
            else if (mimeType.Contains("x-msvideo"))
            {
                result = ".avi";
            }
            else if (mimeType.Contains("mpeg"))
            {
                result = ".mpeg";
            }
            else if (mimeType.Contains("x-ms-wmv"))
            {
                result = ".wmv";
            }
            else if (mimeType.Contains("html"))
            {
                result = ".htm";
            }
            else if (mimeType.Contains("xhtml+xml"))
            {
                result = ".xhtml";
            }
            else if (mimeType.Contains("xml"))
            {
                result = ".xml";
            }
            else if (mimeType.Contains("text/x-vcard"))
            {
                result = ".vcf";
            }
            else if (mimeType.Contains("application/vnd.visio"))
            {
                result = ".vsd";
            }
            else if (mimeType.Contains("application/vnd.ms-project"))
            {
                result = ".mpp";
            }
            else if (mimeType.Contains("image/svg+xml"))
            {
                result = ".svg";
            }
            else if (mimeType.Contains("image/vnd.adobe.photoshop"))
            {
                result = ".psd";
            }
            else if (mimeType.Contains("video/quicktime"))
            {
                result = ".mov";
            }
            else if (mimeType.Contains("application/vnd.ms-project"))
            {
                result = ".mpp";
            }
            else if (mimeType.Contains("application/x-mimearchive"))
            {
                result = ".mhtml";
            }
            else
            {
                throw new NotSupportedException(string.Format("Was not expecting a MIME type of: '{0}'.", mimeType));
            }

            return result;
        }
        #endregion
    }
}
