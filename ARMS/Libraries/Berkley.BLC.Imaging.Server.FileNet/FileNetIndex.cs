/*===========================================================================
  File:			FileNetIndex.cs
  
  Summary:		Contains the implementation of an index in FileNet.
			
  Classes:		FileNetIndex
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging.Server.FileNet
{
	/// <summary>
	/// Contains the implementation of an index in FileNet.
	/// </summary>
	public class FileNetIndex : Index, IIndex
	{
        #region Constants

        /// <summary>
        /// Doc Number Field Name.
        /// </summary>
        public const string DocNumber = "F_DOCNUMBER";
        /// <summary>
        /// Policy Number Field Name.
        /// </summary>
        public const string PolicyNumber = "POLICY_NO";
        /// <summary>
        /// Insured Name Field Name.
        /// </summary>
        public const string InsuredName = "INSURED_NAME";
        /// <summary>
        /// Client ID Field Name.
        /// </summary>
        public const string ClientID = "CLIENT_ID";
        /// <summary>
        /// Account ID Field Name.
        /// </summary>
        public const string AccountID = "ACCOUNT_ID";
        /// <summary>
        /// Policy Symbol Field Name.
        /// </summary>
        public const string PolicySymbol = "POLICY_SYMBOL";
        /// <summary>
        /// Doc Type Field Name.
        /// </summary>
        public const string DocType = "DOC_TYPE";
        /// <summary>
        /// Entry Date Field Name.
        /// </summary>
        public const string EntryDate = "F_ENTRYDATE";
        /// <summary>
        /// Doc Remarks Field Name.
        /// </summary>
        public const string DocRemarks = "DOC_REMARKS";
        /// <summary>
        /// MIME type Field Name.
        /// </summary>
        public const string MimeType = "F_DOCFORMAT";
        /// <summary>
        /// Page Count Field Name.
        /// </summary>
        public const string PageCount = "F_PAGES";
        /// <summary>
        /// Effective Date Field Name.
        /// </summary>
        public const string EffectiveDate = "EFFECTIVE_DATE";
        /// <summary>
        /// Expiration Date Field Name.
        /// </summary>
        public const string ExpirationDate = "EXP_DATE";
        /// <summary>
        /// Scan Date Field Name.
        /// </summary>
        public const string ScanDate = "SCAN_DATE";
        /// <summary>
        /// Submission Number Field Name.
        /// </summary>
        public const string SubmissionNumber = "SUBMISSION_NO";
        /// <summary>
        /// Agency Number Field Name.
        /// </summary>
        public const string AgencyNumber = "AGENCY_NO";
        /// <summary>
        /// Source System Field Name.
        /// </summary>
        public const string SourceSystem = "SOURCE_SYSTEM";

        #endregion

        #region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		public FileNetIndex(){}
		/// <summary>
		/// Instantiates the object using the state available within the 
		/// provided Proxy.IDMIndex object.
		/// </summary>
		/// <param name="index">The Proxy.IDMIndex object from which to 
		/// extract the state for use in preloading the instance.</param>
		internal FileNetIndex(Proxy.Index index)
		{
			if (index != null)
			{
				this.Name = index.item;
				this.Value = index.value;
			}
		}
		#endregion

		#region methods
		/// <summary>
		/// Converts the instance of the object into a Proxy.IDMIndex equivalent.
		/// </summary>
		/// <returns>A Proxy.IDMIndex representation of the instance.</returns>
		internal Proxy.Index Convert()
		{
			Proxy.Index result = new Proxy.Index();

			result.item = this.Name;
			result.value = this.Value;

			return result;
		}

        #endregion
	}
	/// <summary>
	/// Contains the implementation of an index in FileNet.
	/// </summary>
    public class FileNetIndexP8 : Berkley.Imaging.PropertyP8
    {
        #region Constants

        /// <summary>
        /// Doc Id Field Name.
        /// </summary>
        public const string ID = "Id";		            //WA-43, Doug Bruce, 2015-05-15 - Added P8 GUID id so searches can be done by both IS int (F_DOCNUMBER) and P8 guid.
        /// <summary>
        /// Doc Number Field Name.
        /// </summary>
        public const string DocNumber = "F_DOCNUMBER";  //WA-43, Doug Bruce, 2015-04-15 - P8 - Explicitly defined in P8 column mapping Excel doc.
        /// <summary>
        /// Policy Number Field Name.
        /// </summary>
        public const string PolicyNumber = "PolicyNo";
        /// <summary>
        /// Client ID Field Name.
        /// </summary>
        public const string ClientID = "ClientID";
        /// <summary>
        /// AccountID Field Name.
        /// </summary>
        public const string AccountID = "AccountID";
        /// <summary>
        /// Policy Symbol Field Name.
        /// </summary>
        public const string PolicySymbol = "PolicySymbol";
        /// <summary>
        /// Policy Mod Field Name.
        /// </summary>
        public const string PolicyMod = "PolicyMod";
        /// <summary>
        /// Doc Type Field Name.
        /// </summary>
        public const string DocType = "DocType";
        /// <summary>
        /// Entry Date Field Name.
        /// </summary>
        public const string EntryDate = "F_ENTRYDATE";  //WA-43, Doug Bruce, 2015-04-15 - P8 - Not defined in P8 column mapping Excel doc.
        /// <summary>
        /// Filename Field Name.
        /// </summary>
        public const string FileName = "FileName";  //WA-3060 Doug Bruce 2015-10-07 - P8 - need to get this property
        /// <summary>
        /// Doc Remarks Field Name.
        /// </summary>
        public const string DocRemarks = "DocRemarks";
        /// <summary>
        /// Doc extension Field Name.
        /// </summary>
        public const string Extension = "F_DOCFORMAT";  //WA-43, Doug Bruce, 2015-04-15 - P8 - Not defined in P8 column mapping Excel doc.
        /// <summary>
        /// MIME type Field Name.
        /// </summary>
        public const string MimeType = "MimeType";      //WA-43, Doug Bruce, 2015-04-15 - P8 - Not defined in P8 column mapping Excel doc.
        /// <summary>
        /// Page Count Field Name.
        /// </summary>
        public const string PageCount = "F_PAGES";      //WA-43, Doug Bruce, 2015-04-15 - P8 - Not defined in P8 column mapping Excel doc.
        /// <summary>
        /// Insured Field Name.
        /// </summary>
        public const string InsuredName = "InsuredName";
        /// <summary>
        /// Agency No Field Name.
        /// </summary>
        public const string AgencyNumber = "AgencyNo";
        /// <summary>
        /// Effective Date Field Name.
        /// </summary>
        public const string EffectiveDate = "EffectiveDate";
        /// <summary>
        /// Expiration Date Field Name.
        /// </summary>
        public const string ExpirationDate = "ExpirationDate";
        /// <summary>
        /// Batch Field Name.
        /// </summary>
        public const string BatchName = "BatchName";
        /// <summary>
        /// Submission No.
        /// </summary>
        public const string SubmissionNo = "SubmissionNo";
        /// <summary>
        /// Source System
        /// </summary>
        public const string SourceSystem = "SourceSystem";

        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileNetIndexP8() { }
        /// <summary>
        /// Instantiates the object using the state available within the 
        /// provided Proxy.IDMIndex object.
        /// </summary>
        /// <param name="index">The Proxy.IDMIndex object from which to 
        /// extract the state for use in preloading the instance.</param>
        internal FileNetIndexP8(PropertyP8 index)
        {
            if (index != null)
            {
                this.Name = index.Name;
                this.Value = index.Value;
            }
        }
        #endregion

        #region methods
        /// <summary>
        /// Converts the instance of the object into a Proxy.IDMIndex equivalent.
        /// </summary>
        /// <returns>A Proxy.IDMIndex representation of the instance.</returns>
        internal PropertyP8 Convert()
        {
            PropertyP8 result = new PropertyP8();
            result.Init();

            result.Name = this.Name;
            result.Value = this.Value;

            return result;
        }
        #endregion methods
    }
}