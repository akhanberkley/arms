/*===========================================================================
  File:			FileNetPropertyP8.cs
  
  Summary:		Contains the implementation of an index in FileNet.
			
  Classes:		FileNetPropertyP8
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:   	WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging.Server.FileNet
{
	/// <summary>
	/// Contains the implementation of an index in FileNet.
	/// </summary>
	public class FileNetPropertyP8 : PropertyP8 //, IPropertyP8
	{
        #region Constants

        /// <summary>
        /// Doc Id Field Name.
        /// </summary>
        public const string ID = "Id";		                //WA-621, Doug Bruce, 2015-05-15 - Added P8 GUID id so searches can be done by both IS int (F_DOCNUMBER) and P8 guid.
        /// <summary>
        /// Doc Number Field Name.
        /// </summary>
        public const string DocNumber = "F_DOCNUMBER";		//WA-621, Doug Bruce, 2015-04-30 - P8 - Same value in mapping Excel doc.
        /// <summary>
        /// Policy Number Field Name.
        /// </summary>
        public const string PolicyNumber = "PolicyNo";
        /// <summary>
        /// Insured Name Field Name.
        /// </summary>
        public const string InsuredName = "InsuredName";
        /// <summary>
        /// Client ID Field Name.
        /// </summary>
        public const string ClientID = "ClientID";
        /// <summary>
        /// Account ID Field Name.
        /// </summary>
        public const string AccountID = "AccountID";
        /// <summary>
        /// Policy Symbol Field Name.
        /// </summary>
        public const string PolicySymbol = "PolicySymbol";
        /// <summary>
        /// Policy Mod Field Name.
        /// </summary>
        public const string PolicyMod = "PolicyMod";
        /// <summary>
        /// Doc Type Field Name.
        /// </summary>
        public const string DocType = "DocType";
        /// <summary>
        /// Entry Date Field Name.
        /// </summary>
        public const string EntryDate = "F_ENTRYDATE";		//WA-621, Doug Bruce, 2015-04-30 - P8 - Not defined in P8 column mapping Excel doc.
        /// <summary>
        /// Doc Remarks Field Name.
        /// </summary>
        public const string DocRemarks = "DocRemarks";
        /// <summary>
        /// MIME type Field Name.
        /// </summary>
        public const string MimeType = "MimeType";
        /// <summary>
        /// Page Count Field Name.
        /// </summary>
        public const string PageCount = "F_PAGES";			//WA-621, Doug Bruce, 2015-04-30 - P8 - Not defined in P8 column mapping Excel doc.
        /// <summary>
        /// Effective Date Field Name.
        /// </summary>
        public const string EffectiveDate = "EffectiveDate";
        /// <summary>
        /// Expiration Date Field Name.
        /// </summary>
        public const string ExpirationDate = "ExpirationDate";
        /// <summary>
        /// Scan Date Field Name.
        /// </summary>
        public const string ScanDate = "ScanDate";
        /// <summary>
        /// Batch Field Name.
        /// </summary>
        public const string BatchName = "BatchName";
        /// <summary>
        /// Submission Number Field Name.
        /// </summary>
        public const string SubmissionNumber = "SubmissionNo";
        /// <summary>
        /// Agency Number Field Name.
        /// </summary>
        public const string AgencyNumber = "AgencyNo";
        /// <summary>
        /// Source System Field Name.
        /// </summary>
        public const string SourceSystem = "SourceSystem";
        /// <summary>
        /// Page Count Field Name.
        /// </summary>
        public const string FileName = "Filename";	//WA-756, Doug Bruce, 2015-06-23 - P8 - Used for finding/counting pages in a multipage document (split over multiple ContentList entries).

        #endregion

        #region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		public FileNetPropertyP8(){}
		/// <summary>
		/// Instantiates the object using the state available within the 
		/// provided IDMPropertyP8 object.
		/// </summary>
		/// <param name="index">The IDMPropertyP8 object from which to 
		/// extract the state for use in preloading the instance.</param>
		internal FileNetPropertyP8(PropertyP8 index)
		{
			if (index != null)
			{
				this.Name = index.Name;
				this.Value = index.Value;
			}
		}
		#endregion

		#region methods
		/// <summary>
		/// Converts the instance of the object into a IDMPropertyP8 equivalent.
		/// </summary>
		/// <returns>A IDMPropertyP8 representation of the instance.</returns>
		internal PropertyP8 Convert()
		{
			PropertyP8 result = new PropertyP8();

            result.Name = this.Name;
            result.Value = this.Value;

			return result;
		}
		#endregion
	}
}