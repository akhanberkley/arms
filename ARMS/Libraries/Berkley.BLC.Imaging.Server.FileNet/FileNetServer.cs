/*===========================================================================
  File:			FileNetServer.cs
  
  Summary:		Manages the interoperability with the FileNet server. 
			
  Classes:		FileNetServer
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.Globalization;
using System.Collections;
using System.Xml;
using System.Data;
using System.IO;

using Berkley.BLC.Entities;
using System.Collections.Generic;
using System.Configuration; //WA-905 Doug Bruce 2015-09-22 - Dynamic FileNet Uri

namespace Berkley.Imaging.Server.FileNet
{
    /// <summary>
    /// Manages the interoperability with the FileNet server. 
    /// </summary>
    public class FileNetServer : IImagingServer
    {
        #region fields
        /// <summary>
        /// The current configuration.
        /// </summary>
        private Berkley.BLC.Core.ConfigurationType configuration;
        /// <summary>
        /// The company's FileNet user name - set when FileNetServerP8 instance is created.
        /// </summary>
        private string fileNetUsernameP8;
        /// <summary>
        /// The company's FileNet password - set when FileNetServerP8 instance is created.
        /// </summary>
        private string fileNetPasswordP8;
        /// <summary>
        /// The company's FileNet document class - NOW REQUIRED FOR P8 CALLS - set when FileNetServerP8 instance is created.
        /// </summary>
        private string fileNetDocClassP8;
        /// <summary>
        /// The company.
        /// </summary>
        private Company imagingCompany;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileNetServer() { }
        /// <summary>
        /// Default constructor. Connects to the server specified by the 
        /// configuration parameter.
        /// </summary>
        /// <param name="configuration">The configuration which to connect.</param>
        /// <param name="imagingCompany">The company.</param>
        public FileNetServer(Berkley.BLC.Core.ConfigurationType configuration, Company imagingCompany)
        {
            this.configuration = configuration;
            this.imagingCompany = imagingCompany;
        }
        #endregion

        #region IImagingServer Members
        /// <summary>
        /// Specifies the configuration to which the instance is targeted.
        /// </summary>
        public Berkley.BLC.Core.ConfigurationType Configuration
        {
            get
            {
                return this.configuration;
            }
            set
            {
                this.configuration = value;
            }
        }
        /// <summary>
        /// Specifies the company.
        /// </summary>
        public Company ImagingCompany
        {
            get
            {
                return this.imagingCompany;
            }
            set
            {
                this.imagingCompany = value;
            }
        }
        /// <summary>
        /// Retrieves an existing document (by Id) from the imaging 
        /// server.
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        /// <returns>A document. If none is found, null is returned.</returns>
        public IDocument RetrieveDocument(string id, string fileNetUsername, string fileNetPassword)
        {
            return RetrieveDocument(id, 0, 0, fileNetUsername, fileNetPassword);
        }
        /// <summary>
        /// Retrieves an existing document (by Id and page) from the imaging 
        /// server. The page parameter is currently exclusive to FileNet, and
        /// is not included in the IImagingServer interface. 
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <param name="page">The page to return.</param>
        /// <param name="attempts">The number of attempts at retrieving the list of indexes.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        /// <returns>A document. If none is found, null is returned.</returns>
        public IDocument RetrieveDocument(string id, int page, int attempts, string fileNetUsername, string fileNetPassword)
        {
            IDocument result;

            try
            {
                //WA-703 Doug Bruce 2015-05-22 - For compatibilty - if a doc was stored in P8 and the company rolls back to IS - P8 guid id needs to be translated to IS int id.
                if (IsP8Guid(id))
                {
                    id = GetISDocNumberFromP8Guid(id);
                }

                /*
                 * retrieve the document
                 */
                if (IsValidId(id))
                {
                    Proxy proxy = new Proxy(configuration);
                    Proxy.Document document = proxy.getDocument(fileNetUsername, fileNetPassword, Convert.ToInt32(id), page);

                    /*
                     * convert the IDMDoc to an IDocument
                     */
                    result = new FileNetDocument(document);
                }
                else
                {
                    result = null;
                }
            }
            catch (System.Web.Services.Protocols.SoapException se)
            {
                /*
                 * Error in receiving RPC.: Socket closed, so try again
                 */
                if (attempts <= 5)
                {
                    //attempt to add again
                    result = RetrieveDocument(id, page, attempts + 1, fileNetUsername, fileNetPassword);
                }
                else
                {
                    throw new Exception("Attempted to retrieve document 5 times from imaging without success.  See inner exception for details.", se);
                }
            }

            return result;
        }

        /// <summary>
        /// Retrieves the page count index. 
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        /// <returns>The page count.</returns>
        public Int32 RetrievePageCount(string id, string fileNetUsername, string fileNetPassword)
        {
            int result = 0;

            //WA-703 Doug Bruce 2015-05-22 - For compatibilty - if a doc was stored in P8 and the company rolls back to IS - P8 guid id needs to be translated to IS int id.
            if (IsP8Guid(id))
            {
                id = GetISDocNumberFromP8Guid(id);
            }

            /*
             * retrieve the document
             */
            if (IsValidId(id))
            {
                Proxy proxy = new Proxy(configuration);
                Proxy.Index[] indexes = proxy.getDocumentIndexes(fileNetUsername, fileNetPassword, Convert.ToInt32(id));

                foreach (Proxy.Index index in indexes)
                {
                    if (index.item == FileNetIndex.PageCount)
                    {
                        result = Convert.ToInt32(index.value);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Retrieves a list of indexes (by the filter passed) from the imaging server.
        /// </summary>
        /// <param name="document">The document to get the company entity from.</param>
        /// <param name="policyNumbers">The policy number filter for the list of indexes.</param>
        /// <param name="clientID">The client id filter for the list of indexes.</param>
        /// <param name="docID">The doc id filter for the list of indexes.</param>
        /// <param name="docTypes">The doc type filter for the list of indexes.</param>
        /// <param name="entryStartDate">The entry start date filter for the list of indexes.</param>
        /// <param name="entryEndDate">The entry end date filter for the list of indexes.</param>
        /// <param name="attempts">The number of attempts at retrieving the list of indexes.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        /// <returns>A document. If none is found, null is returned.</returns>
        public IDocument[] RetrieveIndexes(IDocument document, string[] policyNumbers, string clientID, string docID, string[] docTypes, DateTime entryStartDate, DateTime entryEndDate, int attempts, string fileNetUsername, string fileNetPassword)
        {
            string strConditions = string.Empty;

            //Build the list of conditions
            try
            {
                FileNetDocument doc = document as FileNetDocument;
                strConditions = doc.BuildFilters(configuration, policyNumbers, clientID, docID, docTypes, entryStartDate, entryEndDate);


                string returnedIndexes = "F_DOCNUMBER,POLICY_NO,POLICY_SYMBOL,DOC_TYPE,F_ENTRYDATE,DOC_REMARKS,F_DOCFORMAT,F_PAGES";
                if (!ImagingCompany.SupportsFileNetIndexPolicySymbol)
                {
                    returnedIndexes = returnedIndexes.Replace("POLICY_SYMBOL,", string.Empty);
                }

                /*
                * retrieve the indexes
                */
                Proxy proxy = new Proxy(configuration);
                string xml = proxy.searchDocuments(fileNetUsername, fileNetPassword, returnedIndexes, strConditions, "F_ENTRYDATE", 0);

                // load the xml doc with the xml returned from the service
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);

                ArrayList list = new ArrayList();

                XmlNodeList rows = xmlDoc.SelectNodes("//row");
                foreach (XmlNode row in rows)
                {
                    FileNetDocument retrievedDoc = new FileNetDocument();

                    XmlNodeList cols = row.ChildNodes;
                    foreach (XmlNode col in cols)
                    {
                        switch (col.Attributes["columnName"].Value)
                        {
                            case FileNetIndex.DocNumber:
                                retrievedDoc.DocNumber = col.InnerXml;
                                break;
                            case FileNetIndex.PolicyNumber:
                                retrievedDoc.PolicyNumber = col.InnerXml;
                                break;
                            case FileNetIndex.ClientID:
                                retrievedDoc.ClientID = col.InnerXml;
                                break;
                            case FileNetIndex.AccountID:
                                retrievedDoc.AccountID = col.InnerXml;
                                break;
                            case FileNetIndex.PolicySymbol:
                                retrievedDoc.PolicySymbol = col.InnerXml;
                                break;
                            case FileNetIndex.DocType:
                                retrievedDoc.DocType = col.InnerXml;
                                break;
                            case FileNetIndex.EntryDate:
                                retrievedDoc.EntryDate = col.InnerXml;
                                break;
                            case FileNetIndex.DocRemarks:
                                retrievedDoc.DocRemarks = col.InnerXml;
                                break;
                            case FileNetIndex.MimeType:
                                //clean up the MimeType, FileNet sometimes includes a document name
                                string strTmp = col.InnerXml;
                                if (strTmp.Contains(";name"))
                                {
                                    int intIndex = strTmp.IndexOf(";name");
                                    int intLength = strTmp.Length - intIndex;
                                    retrievedDoc.MimeType = strTmp.Remove(intIndex, intLength);
                                }
                                else
                                {
                                    retrievedDoc.MimeType = strTmp;
                                }
                                break;
                            case FileNetIndex.PageCount:
                                retrievedDoc.PageCount = int.Parse(col.InnerXml);
                                break;
                            default:
                                throw new NotSupportedException("Not expecting column name: '" + col.Attributes["columnName"].Value + "'.");
                        }
                    }

                    //only add documents with valid MIME types
                    if (retrievedDoc.MimeType != null && retrievedDoc.MimeType.Trim().Length > 0)
                    {
                        list.Add(retrievedDoc);
                    }
                }

                return list.ToArray(typeof(FileNetDocument)) as FileNetDocument[];
            }
            catch (System.Web.Services.Protocols.SoapException se)
            {
                /*
                 * Error retrieving error message from IS., error code: FN_IS_RA_10370
                 */
                if (attempts <= 5)
                {
                    //attempt to retrieve again
                    return RetrieveIndexes(document, policyNumbers, clientID, docID, docTypes, entryStartDate, entryEndDate, attempts + 1, fileNetUsername, fileNetPassword);
                }
                else
                {
                    throw new Exception(string.Format("Attempted to retrieve the indexes 5 times from imaging without success using the following search conditions: {0}.  See inner exception for details.", strConditions), se);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Failed to fetch indexes with the following search conditions: {0}", strConditions), ex);
            }
        }
        /// <summary>
        /// Removes an existing document (by Id and page) from the imaging 
        /// server. The page parameter is currently exclusive to FileNet, and
        /// is not included in the IImagingServer interface. 
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        /// <returns>A completed string. If none is found, null is returned.</returns>
        public void RemoveDocument(string id, string fileNetUsername, string fileNetPassword)
        {
            //WA-703 Doug Bruce 2015-05-22 - For compatibilty - if a doc was stored in P8 and the company rolls back to IS - P8 guid id needs to be translated to IS int id.
            if (IsP8Guid(id))
            {
                id = GetISDocNumberFromP8Guid(id);
            }

            /*
             * remove the document
             */
            if (IsValidId(id))
            {
                Proxy proxy = new Proxy(configuration);

                Proxy.Document update = new Proxy.Document();
                Proxy.Index[] indexes = proxy.getDocumentIndexes(fileNetUsername, fileNetPassword, Convert.ToInt64(id));
                update.attribute = proxy.getDocumentAttributes(fileNetUsername, fileNetPassword, Convert.ToInt64(id));

                //since we can no longer remove the actual document form FileNet, 
                //mimic a delete by removing the client id index.  This will prevent
                //the document to appear in apps like BPM
                if (indexes.Length > 0)
                {
                    ArrayList indexList = new ArrayList();
                    foreach (Proxy.Index index in indexes)
                    {
                        if (index.item == FileNetIndex.ClientID ||  index.item == FileNetIndex.PolicySymbol || index.item == FileNetIndex.ScanDate ||
                            index.item == FileNetIndex.DocRemarks || index.item == FileNetIndex.EffectiveDate || index.item == FileNetIndex.ExpirationDate ||
                            index.item == FileNetIndex.PolicyNumber || index.item == FileNetIndex.SubmissionNumber)
                        {
                            index.value = string.Empty;
                        }

                        //Can't remove these required indexes for BOG due to FileNet requirements
                        if (!fileNetUsername.ToLower().Contains("bog") && (index.item == FileNetIndex.InsuredName ||
                            index.item == FileNetIndex.AgencyNumber || index.item == FileNetIndex.DocType))
                            index.value = string.Empty;

                        indexList.Add(index);
                    }

                    //Finally, add the same indexes and attributes back (excluding the client id)
                    update.indexes = indexList.ToArray(typeof(Proxy.Index)) as Proxy.Index[];

                    proxy.updateDocument(fileNetUsername, fileNetPassword, update);
                }
            }
            else
            {
                throw new InvalidCastException(string.Format("The FileNet Reference is invalid: {0}.", id));
            }
        }

        /// <summary>
        /// Adds an new document to the imaging system.
        /// </summary>
        /// <param name="document">The document to add.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        /// <returns>The document Id.</returns>
        public string Add(IDocument document, string fileNetUsername, string fileNetPassword)
        {
            return Add(document, 0, fileNetUsername, fileNetPassword);
        }
        /// <summary>
        /// Adds an new document to the imaging system.
        /// </summary>
        /// <param name="document">The document to add.</param>
        /// <param name="attempts">The number of attempts at adding the doc.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        /// <returns>The document Id.</returns>
        private string Add(IDocument document, int attempts, string fileNetUsername, string fileNetPassword)
        {
            string id;

            /*
             * obtain a reference to the FileNet server
             */
            Proxy connection = new Proxy(configuration);

            try
            {
                /*
                 * convert the IDocument into a IDMDoc and send the 
                 * results to FileNet.
                 */
                FileNetDocument target = document as FileNetDocument;
                if (target != null)
                {
                    Proxy.Document save = target.Convert(this.configuration);

                    long? newId = connection.addDocument(fileNetUsername, fileNetPassword, save);
                    id = newId.ToString();
                }
                else
                {
                    id = string.Empty;
                }
            }
            catch (NullReferenceException)
            {
                /*
                 * swallow the NullReferenceException, but float any others to 
                 * the caller.
                 */
                id = string.Empty;
            }
            catch (System.Net.WebException we)
            {
                /*
                 * An existing connection was forcibly closed by the remote host so
                 * try resubmitting the request.
                 */
                int allowed = int.Parse(Berkley.BLC.Core.CoreSettings.AddDocAllowedAttempts);
                if (attempts <= allowed)
                {
                    //release the connection
                    connection.Dispose();

                    //reset the position
                    foreach (IFile file in document.Files)
                    {
                        file.Content.Position = 0;
                    }

                    //attempt to add again
                    id = Add(document, attempts + 1, fileNetUsername, fileNetPassword);
                }
                else
                {
                    throw new Exception(string.Format("Attempted to submit document {0} times to imaging without success.  See inner exception for details.", allowed), we);
                }
            }
            catch (System.Web.Services.Protocols.SoapException se)
            {
                /*
                 * com.bts.imaging.rs.exception.ImagingServiceException
                 */
                int allowed = int.Parse(Berkley.BLC.Core.CoreSettings.AddDocAllowedAttempts);
                if (attempts <= allowed)
                {
                    //release the connection
                    connection.Dispose();

                    //reset the position
                    foreach (IFile file in document.Files)
                    {
                        file.Content.Position = 0;
                    }

                    //attempt to add again
                    id = Add(document, attempts + 1, fileNetUsername, fileNetPassword);
                }
                else
                {
                    throw new Exception(string.Format("Attempted to submit document {0} times to imaging without success.  See inner exception for details.", allowed), se);
                }
            }
            catch (System.Xml.XmlException xe)
            {
                /*
                 * com.bts.imaging.rs.exception.ImagingServiceException
                 */
                int allowed = int.Parse(Berkley.BLC.Core.CoreSettings.AddDocAllowedAttempts);
                if (attempts <= allowed)
                {
                    //release the connection
                    connection.Dispose();

                    //reset the position
                    foreach (IFile file in document.Files)
                    {
                        file.Content.Position = 0;
                    }

                    //attempt to add again
                    id = Add(document, attempts + 1, fileNetUsername, fileNetPassword);
                }
                else
                {
                    throw new Exception(string.Format("Attempted to submit document {0} times to imaging without success.  See inner exception for details.", allowed), xe);
                }
            }
            finally
            {
                /*
                 * release the connection
                 */
                connection.Dispose();
            }

            /*
             * return the results
             */
            return id;
        }
        /// <summary>
        /// Updates an existing document in the imaging system.
        /// </summary>
        /// <param name="document">The document to update.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        public void Update(IDocument document, string fileNetUsername, string fileNetPassword)
        {
            /*
             * obtain a reference to the FileNet server
             */
            Proxy connection = new Proxy(configuration);

            try
            {
                /*
                 * convert the IDocument into a IDMDoc and send the 
                 * results to FileNet.
                 */
                FileNetDocument target = document as FileNetDocument;
                if (target != null)
                {
                    Proxy.Document save = target.Convert();
                    save.attribute = connection.getDocumentAttributes(fileNetUsername, fileNetPassword, Convert.ToInt64(document.DocNumber));

                    connection.updateDocument(fileNetUsername, fileNetPassword, save);
                }
            }
            catch (NullReferenceException)
            {
                /*
                 * swallow the NullReferenceException, but float any others to 
                 * the caller.
                 */
            }
            finally
            {
                /*
                 * release the connection
                 */
                connection.Dispose();
            }
        }
        /// <summary>
        /// Creates a new, empty document.
        /// </summary>
        /// <returns>A new, empty document.</returns>
        public IDocument CreateDocument()
        {
            return new FileNetDocument();
        }
        /// <summary>
        /// Creates a new, empty file.
        /// </summary>
        /// <returns>A new, empty file.</returns>
        public IFile CreateFile()
        {
            return new FileNetFile();
        }
        /// <summary>
        /// Creates a new, empty index.
        /// </summary>
        /// <returns>A new, empty index.</returns>
        public IIndex CreateIndex()
        {
            return new FileNetIndex();
        }
        #endregion

        #region methods
        /// <summary>
        /// Sets items required for FileNet call for this company.
        /// </summary>
        /// <param name="Username">This company's FileNet P8 username</param>
        /// <param name="Password">This company's FileNet P8 password</param>
        /// <param name="DocClass">This company's FileNet P8 Doc Class</param>
        public void SetUserP8(string fnUsernameP8, string fnPasswordP8, string fnDocClassP8)
        {
            this.fileNetUsernameP8 = fnUsernameP8;
            this.fileNetPasswordP8 = fnPasswordP8;
            this.fileNetDocClassP8 = fnDocClassP8;
        }

        /// <summary>
        /// Determines whether or not the provided id is valid. FileNet
        /// uses numeric Ids. This method determines if the id is numeric.
        /// </summary>
        /// <param name="id">The string to test.</param>
        /// <returns>True if the id is valid, false otherwise.</returns>
        private static bool IsValidId(string id)
        {
            bool isValid = true;

            if (id != null && id.Length > 0)
            {
                for (int i = 0; i < id.Length && isValid; i++)
                {
                    if (!char.IsDigit(id[i]))
                        isValid = false;
                }
            }
            else
            {
                isValid = false;
            }

            return isValid;
        }

        private bool IsP8Guid(string id)     //WA-703 Doug Bruce 2015-05-22 - For guid/int backwards compatibility (if doc stored in P8, then company rolls back to IS)
        {
            Guid resultP8;
            return System.Guid.TryParse(id, out resultP8);
        }

        private string GetISDocNumberFromP8Guid(string id)   //WA-703 Doug Bruce 2015-05-22 - Get and return IS int id from P8 guid id.
        {
            string intID = String.Empty;

            /*
             * obtain a reference to the FileNet server
             */

            //WA-905 Doug Bruce 2015-09-22 - Dynamic FileNet Uri
            string endpointName = String.Empty;
            if (configuration == BLC.Core.ConfigurationType.Production)
            {
                endpointName = ConfigurationManager.AppSettings["ARMS-P8FileNetEndpoint-Prod"];
                if (endpointName == String.Empty)
                {
                    throw new Exception("FileNet P8 Uri (ARMS-P8FileNetEndpoint-Prod) is not set in the configuration file.");
                }
            }
            else
            {
                endpointName = ConfigurationManager.AppSettings["ARMS-P8FileNetEndpoint-Test"];
                if (endpointName == String.Empty)
                {
                    throw new Exception("FileNet P8 Uri (ARMS-P8FileNetEndpoint-Test) is not set in the configuration file.");
                }
            }

            FileNetServiceP8.P8ServiceClient p8 = new FileNetServiceP8.P8ServiceClient(endpointName);

            /*
             * This causes FileNet to ignore any parameters that it can't reconcile with the company sent in.
             * Pass as last parameter of most P8 calls.
             */
            List<FileNetServiceP8.entry> additionalParams = new List<FileNetServiceP8.entry>();
            FileNetServiceP8.entry fnEntry = new FileNetServiceP8.entry();
            fnEntry.key = "ignoreInvalidProperties";
            fnEntry.value = "true";
            additionalParams.Add(fnEntry);

            fnEntry = new FileNetServiceP8.entry();
            fnEntry.key = "includeFilenameProperties";
            fnEntry.value = "true";
            additionalParams.Add(fnEntry);

            try
            {
                /*
			     * retrieve the document properties
			     */
                FileNetServiceP8.property[] props = p8.getDocumentProperties(fileNetUsernameP8, fileNetPasswordP8, fileNetDocClassP8, id, additionalParams.ToArray());

                for (int i = 0; i < props.Length; i++)
                {
                    //Get the document number - IS will be able to work with this.
                    if (props[i].name == FileNetPropertyP8.DocNumber)
                    {
                        intID = props[i].value;
                        break;
                    }
                }

                p8.Close();
            }
            catch (System.Net.WebException we)
            {
                throw new Exception(string.Format("Failed to retrieve indexed document id {0} from imaging.  See inner exception for details.", id), we);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }

            return intID;
        }

        #endregion
    }
}
