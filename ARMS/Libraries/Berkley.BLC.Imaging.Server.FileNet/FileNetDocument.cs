/*===========================================================================
  File:			FileNetDocument.cs
  
  Summary:		Contains the implementation of a document in FileNet.
			
  Classes:		FileNetDocument
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.Globalization;
using System.Collections;
using System.Text;
using Berkley.Imaging;
using Berkley.BLC.Entities;

namespace Berkley.Imaging.Server.FileNet
{
    /// <summary>
    /// Contains the implementation of a document in FileNet.
    /// </summary>
    public class FileNetDocument : Document, IDocument
    {
        #region constants
        /// <summary>
        /// The name of the mandatory 'company number' index.
        /// </summary>
        private const string IDX_COMPANY_NUMBER = "COMPANY_NO";
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileNetDocument() { }
        /// <summary>
        /// Instantiates the object using the state available within the 
        /// provided Proxy.IDMDoc object.
        /// </summary>
        /// <param name="document">The Proxy.IDMDoc object from which to 
        /// extract the state for use in preloading the instance.</param>
        internal FileNetDocument(Proxy.Document document)
        {
            if (document != null)
            {
                /*
                 * set the document attributes
                 */
                if (document.attribute != null)
                {
                    this.Id = document.attribute.documentID.ToString(CultureInfo.InvariantCulture);
                    this.Extension = document.attribute.fileExt;
                    this.MimeType = document.attribute.mimeType;
                }

                /*
                 * add the files
                 */
                int fileCount = document.files.Length;
                if (document.files != null && fileCount > 0)
                {
                    for (int i = 0; i < fileCount; i++)
                    {
                        if (document.files[i] != null)
                            this.Files.Add(new FileNetFile(document.files[i]));
                    }
                }

                /*
                 * add the indices
                 */
                int indices = document.indexes.Length;
                if (document.indexes != null && indices > 0)
                {
                    for (int i = 0; i < indices; i++)
                    {
                        if (document.indexes[i] != null)
                            this.Indexes.Add(new FileNetIndex(document.indexes[i]));
                    }
                }
            }
        }
        #endregion

        #region properties
        /// <summary>
        /// Overridden. The company to whom the document belongs.
        /// </summary>
        public override Company Company
        {
            get
            {
                return base.Company;
            }
            set
            {
                /*
                 * FileNet requires a 'company number' index. This can
                 * be pulled from the Company object that is being set
                 * here.
                 */
                if (value != null && !this.Indexes.Contains(IDX_COMPANY_NUMBER))
                {
                    /*
                     * the company number index has not yet been added, so
                     * create it and add it to the collection.
                     */
                    IIndex index = new FileNetIndex();
                    index.Name = IDX_COMPANY_NUMBER;
                    index.Value = value.Number;
                    this.Indexes.Add(index);
                }

                /*
                 * if the company changes, the documentClass is no 
                 * longer valid.
                 */
                if (base.Company != value)
                {
                    /*
                     * make sure that the 'company number' index is correct
                     */
                    IIndex index = this.Indexes[IDX_COMPANY_NUMBER];
                    if (index != null && value != null)
                        index.Value = value.Number;
                }

                /*
                 * set the value
                 */
                base.Company = value;
            }
        }
        #endregion

        #region methods
        /// <summary>
        /// Converts the instance of the object into a Proxy.IDMDoc equivalent.
        /// </summary>
        /// <param name="configuration">The specified configuration from which
        /// to garner some conversion information.</param>
        /// <returns>A Proxy.IDMDoc representation of the instance.</returns>
        internal Proxy.Document Convert(Berkley.BLC.Core.ConfigurationType configuration)
        {
            Proxy.Document result = new Proxy.Document();

            /*
             * add the document attributes
             */
            result.attribute = new Proxy.Attribute();
            result.attribute.docClass = this.Company.FileNetDocClass;
            result.attribute.documentID = 0;
            result.attribute.entryDate = DateTime.Today.ToShortDateString();
            result.attribute.fileExt = this.FileName;
            result.attribute.mimeType = this.MimeType;
            result.attribute.pageCount = this.Files.Count;

            /*
             * attach the file(s)
             */
            int files = this.Files.Count;
            if (files > 0)
            {
                result.files = new Proxy.File[files];
                for (int i = 0; i < files; i++)
                {
                    result.files[i] = ((FileNetFile)(this.Files[i])).Convert();
                }
            }
            else
            {
                /*
                 * at least one file is required for a document.
                 */
                throw new MissingFileException();
            }

            /*
             * add the indices
             */
            int indices = this.Indexes.Count;
            if (indices > 0)
            {
                result.indexes = new Proxy.Index[indices];
                for (int i = 0; i < indices; i++)
                {
                    result.indexes[i] = ((FileNetIndex)(this.Indexes[i])).Convert();
                }
            }

            return result;
        }

        /// <summary>
        /// Converts the instance of the object into a Proxy.IDMDoc equivalent.
        /// </summary>
        /// <returns>A Proxy.IDMDoc representation of the instance.</returns>
        internal Proxy.Document Convert()
        {
            Proxy.Document result = new Proxy.Document();

            /*
             * attach the file(s)
             */
            int files = this.Files.Count;
            if (files > 0)
            {
                result.files = new Proxy.File[files];
                for (int i = 0; i < files; i++)
                {
                    result.files[i] = ((FileNetFile)(this.Files[i])).Convert();
                }
            }
            else
            {
                /*
                 * at least one file is required for a document.
                 */
                throw new MissingFileException();
            }

            /*
             * add the indices
             */
            int indices = this.Indexes.Count;
            if (indices > 0)
            {
                result.indexes = new Proxy.Index[indices];
                for (int i = 0; i < indices; i++)
                {
                    result.indexes[i] = ((FileNetIndex)(this.Indexes[i])).Convert();
                }
            }

            return result;
        }

        /// <summary>
        /// Builds a string arrary of filters for the query.
        /// </summary>
        /// <param name="configuration">The specified configuration from</param>
        /// <param name="policyNumbers">The policy number filter for the list of indexes.</param>
        /// <param name="clientID">The client id filter for the list of indexes.</param>
        /// <param name="docID">The doc id filter for the list of indexes.</param>
        /// <param name="docTypes">The doc type filter for the list of indexes.</param>
        /// <param name="entryStartDate">The entry start date filter for the list of indexes.</param>
        /// <param name="entryEndDate">The entry end date filter for the list of indexes.</param>
        /// <returns>A string array of filters.</returns>
        internal string BuildFilters(Berkley.BLC.Core.ConfigurationType configuration, string[] policyNumbers, string clientID, string docID, string[] docTypes, DateTime entryStartDate, DateTime entryEndDate)
        {
            //Build the array of conditions
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("COMPANY_NO='{0}'", this.Company.Number);
            sb.AppendFormat(" AND ");
            sb.AppendFormat("F_DOCCLASSNAME='{0}'", this.Company.FileNetDocClass);
            sb.AppendFormat(" AND ");

            for (int i = 0; i < policyNumbers.Length; i++)
            {
                string policyNumber = policyNumbers[i];
                if (policyNumber.Trim().Length > 0)
                {
                    if (i == 0)
                    {
                        sb.AppendFormat("(");
                    }
                    
                    sb.AppendFormat("POLICY_NO='{0}'", policyNumber.Trim());
                    sb.AppendFormat(" OR ");

                    if (i == (policyNumbers.Length - 1))
                    {
                        sb.Remove(sb.Length - 4, 4);
                        sb.AppendFormat(")");
                        sb.AppendFormat(" AND ");
                    }
                }
            }

            if (clientID.Trim().Length > 0)
            {
                sb.AppendFormat("{0}='{1}'", this.Company.FileNetClientIDIndexName, clientID);
                sb.AppendFormat(" AND ");
            }

            if (docID.Trim().Length > 0)
            {
                sb.AppendFormat("F_DOCNUMBER='{0}'", docID);
            }

            if (docID.Trim().Length <= 0)
            {
                for (int i = 0; i < docTypes.Length; i++)
                {
                    string docType = docTypes[i];
                    if (docType.Trim().Length > 0)
                    {
                        if (i == 0)
                        {
                            sb.AppendFormat("(");
                        }

                        sb.AppendFormat("DOC_TYPE='{0}'", docType.Trim());
                        sb.AppendFormat(" OR ");

                        if (i == (docTypes.Length - 1))
                        {
                            sb.Remove(sb.Length - 4, 4);
                            sb.AppendFormat(")");
                            sb.AppendFormat(" AND ");
                        }
                    }
                }

                sb.AppendFormat("F_ENTRYDATE>='{0}'", entryStartDate.ToShortDateString());
                sb.AppendFormat(" AND ");
                sb.AppendFormat("F_ENTRYDATE<='{0}'", entryEndDate.ToShortDateString());
            }

            return sb.ToString();
        }
        #endregion
    }
}