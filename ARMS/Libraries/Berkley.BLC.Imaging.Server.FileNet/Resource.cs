/*===========================================================================
  File:			Resource.cs
  
  Summary:		Assists the process of gathering resources associated with 
				this assembly.
			
  Classes:		Resource
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.Globalization;
using System.Reflection;
using System.Resources;

namespace Berkley.Imaging.Server.FileNet
{
	/// <summary>
	/// Assists the process of gathering resources associated with this assembly.
	/// </summary>
	internal sealed class Resource
	{
		#region inner class
		/// <summary>
		/// Provides a reference to a single ResourceManager object that is
		/// directed at this assembly.
		/// </summary>
		private sealed class ResourceManagerSingleton
		{
			/// <summary>
			/// The ResourceManager singleton.
			/// </summary>
			private static volatile ResourceManager instance;
			/// <summary>
			/// Serves as a mutex to manage initialization of the singleton.
			/// </summary>
			private static object syncRoot = new Object();

			/// <summary>
			/// Private constructor to force singleton usage.
			/// </summary>
			private ResourceManagerSingleton(){}

			/// <summary>
			/// A reference to the ResourceManager singleton
			/// </summary>
			public static ResourceManager Instance
			{
				get
				{
					/*
					 * if the singleton has not been created..
					 */
					if (instance == null) 
					{
						/*
						 * block any other threads from creating the singleton.
						 */
						lock (syncRoot) 
						{
							/*
							 * create singleton 
							 */
							if (instance == null) 
								instance = new ResourceManager(typeof(Resource).FullName, typeof(Resource).Assembly);
						}
					}

					/*
					 * return the result
					 */
					return instance;
				}
			}
		}
		#endregion

		#region fields
		#endregion

		#region constructors
		/// <summary>
		/// Private constructor used to disallow instantiation of this class.
		/// </summary>
		private Resource() {}
		#endregion

		#region properties
		#endregion

		#region methods
		/// <summary>
		/// Gets the value of the string resource.
		/// </summary>
		/// <param name="name">The name of the resource to get.</param>
		/// <returns>The value of the string resource.</returns>
		public static string GetString(string name)
		{
			return ResourceManagerSingleton.Instance.GetString(name, System.Threading.Thread.CurrentThread.CurrentCulture);
		}
		#endregion
	}
}