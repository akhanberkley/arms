/*===========================================================================
  File:			FileNetDocumentP8.cs
  
  Summary:		Contains the implementation of a document in FileNet.
			
  Classes:		FileNetDocumentP8
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:   	WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.Globalization;
using System.Collections;
using System.Text;
using Berkley.Imaging;
using Berkley.BLC.Entities;

namespace Berkley.Imaging.Server.FileNet
{
	/// <summary>
	/// Contains the implementation of a document in FileNet.
	/// </summary>
    public class FileNetDocumentP8 : DocumentP8 //, IDocumentP8
	{
		#region constants
		/// <summary>
		/// The name of the mandatory 'company number' property.
		/// </summary>
        private const string IDX_COMPANY_NUMBER = "CompanyNo";
		#endregion

        #region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
        public FileNetDocumentP8() { }
		/// <summary>
		/// Instantiates the object using the state available within the 
		/// provided IDMDoc object.
		/// </summary>
		/// <param name="document">The IDMDoc object from which to 
		/// extract the state for use in preloading the instance.</param>
        internal FileNetDocumentP8(DocumentP8 document)
		{
			if (document != null)
			{
				/*
                 * set the document attributes
                 */
                //if (document.attribute != null)
                //{
                //    this.Id = document.attribute.documentID.ToString(CultureInfo.InvariantCulture);
                //    this.Extension = document.attribute.fileExt;
                //    this.MimeType = document.attribute.mimeType;
                //}

				/*
                 * add the contentList
                 */
                //Modified by Vilas Meshram: 01/24/2013: to handle multipale pagaes of .tif and displaying as a single multipage .tif file
                int fileCount = document.ContentList.Count;
                if (document.ContentList != null)
                {
                    //BRING THIS BACK
                    //if (fileCount > 1 && (document.attribute.mimeType == "image/tiff"))
                    //{
                    //    //
                    //    //Multi-Frame
                    //    //
                    //    DocumentTiff((DocumentP8)document);
                    //}
                    //else if (fileCount > 0)
                    //{
                    //
                    //Single page
                    //
                    for (int i = 0; i < fileCount; i++)
                    {
                        if (document.ContentList[i] != null)
                            this.ContentList.Add(document.ContentList[i]);
                    }
                    //}
                }

                if (document.ContentList != null && fileCount > 0)
                {
                    for (int i = 0; i < fileCount; i++)
                    {
                        if (document.ContentList[i] != null)
                            this.ContentList.Add(new FileNetContentP8((ContentP8)document.ContentList[i]));
                    }
                }

				/*
                 * add the properties
                 */
                int indices = document.Properties.Count;
                if (document.Properties != null && indices > 0)
                {
                    for (int i = 0; i < indices; i++)
                    {
                        if (document.Properties[i] != null)
                            this.Properties.Add(new FileNetPropertyP8((PropertyP8)document.Properties[i]));
                    }
                }
            }
		}

		#endregion

        #region properties
		/// <summary>
		/// Overridden. The company to whom the document belongs.
		/// </summary>
        public override Company Company
		{
			get
			{
				return base.Company;
			}

			set
			{
				/*
                 * FileNet requires a 'company number' property. This can
                 * be pulled from the Company object that is being set
                 * here.
                 */
                if (value != null && !this.Properties.Contains(IDX_COMPANY_NUMBER))
				{
					/*
                     * the company number property has not yet been added, so
                     * create it and add it to the collection.
                     */
                    PropertyP8 property = new FileNetPropertyP8();
					property.Name = IDX_COMPANY_NUMBER;
					property.Value = value.Number;
					this.Properties.Add(property);
				}

				/*
                 * if the company changes, the documentClass is no 
                 * longer valid.
                 */
                if (base.Company != value)
				{
					/*
                     * make sure that the 'company number' property is correct
                     */
                    PropertyP8 property = this.Properties[IDX_COMPANY_NUMBER];
					if (property != null && value != null)
						property.Value = value.Number;
				}

				/*
                 * set the value
                 */
                base.Company = value;
			}
		}

		#endregion

        #region methods
		/// <summary>
		/// Converts the instance of the object into a IDMDoc equivalent.
		/// </summary>
		/// <param name="configuration">The specified configuration from which
		/// to garner some conversion information.</param>
		/// <returns>A IDMDoc representation of the instance.</returns>
        internal DocumentP8 Convert(Berkley.BLC.Core.ConfigurationType configuration)
		{
			DocumentP8 result = new DocumentP8();

			/*
             * add the document attributes
             */
            //result.attribute = new Attribute();
            //result.attribute.docClass = this.Company.FileNetDocClass;
            //result.attribute.documentID = 0;
            //result.attribute.entryDate = DateTime.Today.ToShortDateString();
            //result.attribute.fileExt = this.FileName;
            //result.attribute.mimeType = this.MimeType;
            //result.attribute.pageCount = this.ContentList.Count;

			/*
             * attach the file(s)
             */
            int contentList = this.ContentList.Count;
            if (contentList > 0)
            {
                IContentCollectionP8 cc = new IContentCollectionP8();
                for (int i = 0; i < contentList; i++)
                {
                    cc.Add(((FileNetContentP8)(this.ContentList[i])).Convert());
                }
                result.ContentList = cc;
            }
            else
            {
                /*
                 * at least one file is required for a document.
                 */
                throw new MissingFileException();
            }

			/*
             * add the properties
             */
            int properties = this.Properties.Count;
			if (properties > 0)
			{
				result.Properties = new IPropertyCollectionP8();
				for (int i = 0; i < properties; i++)
				{
					result.Properties[i] = ((FileNetPropertyP8)(this.Properties[i])).Convert();
				}
			}

			return result;
		}

		/// <summary>
		/// Converts the instance of the object into a IDMDoc equivalent.
		/// </summary>
		/// <returns>A IDMDoc representation of the instance.</returns>
        internal DocumentP8 Convert()
		{
			DocumentP8 result = new DocumentP8();

			/*
             * attach the file(s)
             */
            int contentList = this.ContentList.Count;
			if (contentList > 0)
			{
                IContentCollectionP8 cc = new IContentCollectionP8();
                for (int i = 0; i < contentList; i++)
                {
                    cc.Add(((FileNetContentP8)(this.ContentList[i])).Convert());
                }
                result.ContentList = cc;
            }
			else
			{
				/*
                 * at least one file is required for a document.
                 */
                throw new MissingFileException();
			}

			/*
             * add the properties
             */
            int properties = this.Properties.Count;
			if (properties > 0)
			{
                result.Properties = new IPropertyCollectionP8();
				for (int i = 0; i < properties; i++)
				{
					result.Properties[i] = ((FileNetPropertyP8)(this.Properties[i])).Convert();
				}
			}

			return result;
		}

        /// <summary>
		/// Builds a string arrary of filters for the query.
		/// </summary>
		/// <param name="configuration">The specified configuration from</param>
		/// <param name="policyNumbers">The policy number filter for the list of properties.</param>
		/// <param name="clientID">The client id filter for the list of properties.</param>
		/// <param name="docID">The doc id filter for the list of properties.</param>
		/// <param name="docTypes">The doc type filter for the list of properties.</param>
		/// <param name="entryStartDate">The entry start date filter for the list of properties.</param>
		/// <param name="entryEndDate">The entry end date filter for the list of properties.</param>
		/// <returns>A string array of filters.</returns>
        internal string  BuildFilters(Berkley.BLC.Core.ConfigurationType configuration, string[] policyNumbers, string clientID, string docID, string[] docTypes, DateTime entryStartDate, DateTime entryEndDate, bool fileNetPolicyLike, string overrideCompanyNo)
		{
            //WA-3060 Doug Bruce 2015-10-06 - need CompanyNo to be overridden to match during multiple DocClass processing.
            if (overrideCompanyNo == null || overrideCompanyNo == "")
            {
                overrideCompanyNo = this.Company.Number;
            }
            //Build the array of conditions
            StringBuilder sb = new StringBuilder();
			sb.AppendFormat("CompanyNo='{0}'", overrideCompanyNo);

            //sb.AppendFormat(" AND ");
            //sb.AppendFormat("F_DOCCLASSNAME='{0}'", this.Company.FileNetDocClass);

            if (policyNumbers.Length > 0)
            {
                sb.AppendFormat(" AND ");
            }
            for (int i = 0; i < policyNumbers.Length; i++)            {
                string policyNumber = policyNumbers[i];
                if (policyNumber.Trim().Length > 0)
                {
                    if (i == 0)
                    {
                        sb.AppendFormat("(");
                    }
                    
                    //sb.AppendFormat("PolicyNo='{0}'", policyNumber.Trim());

                    if (fileNetPolicyLike)
                    {
                        sb.AppendFormat("{0} LIKE '%{1}%'", FileNetPropertyP8.PolicyNumber, policyNumber);
                    }
                    else
                    {
                        sb.AppendFormat("{0}='{1}'", FileNetPropertyP8.PolicyNumber, policyNumber);
                    }

                    sb.AppendFormat(" OR ");

                    if (i == (policyNumbers.Length - 1))
                    {
                        sb.Remove(sb.Length - 4, 4);
                        sb.AppendFormat(")");
                        //sb.AppendFormat(" AND ");
                    }
                }
            }

            if (clientID.Trim().Length > 0)
            {
                sb.AppendFormat(" AND ");
                sb.AppendFormat("{0}='{1}'", this.Company.FileNetClientIDIndexNameP8, clientID); //WA-697 Doug Bruce 2015-05-21 - Add field for P8 so ClientID search will work correctly for all companies.
            }

            if (docID.Trim().Length > 0)    //WA-674 Doug Bruce 2015-05-15 - Fixed Document ID to handle both IS int and P8 guid.
            {
                sb.AppendFormat(" AND ");
                if (IsP8GuidId(docID))
                {
                    if (docID.IndexOf(",") == 0)    //WA-758 Doug Bruce 2015-08-03 - change sql where clause to handle comma separated docid's
                    {
                        sb.AppendFormat("{0}='{1}'", FileNetPropertyP8.ID, docID);
                    }
                    else
                    {
                        sb.AppendFormat("(");
                        string[] multipleDocIds = docID.Split(',');    //WA-758 Doug Bruce 2015-08-03 - change sql where clause to handle comma separated docid's
                        for (int i = 0; i < multipleDocIds.Length; i++)
                        {
                            if (i > 0)
                            {
                                sb.AppendFormat(" OR ");
                            }
                            sb.AppendFormat("{0}='{1}'", FileNetPropertyP8.ID, multipleDocIds[i]);
                        }
                        sb.AppendFormat(")");
                    }
                }
                else
                {
                    if (docID.IndexOf(",") == 0)
                    {
                        sb.AppendFormat("{0}={1}", FileNetPropertyP8.DocNumber, docID);
                    }
                    else
                    {
                        sb.AppendFormat("(");
                        string[] multipleDocIds = docID.Split(',');    //WA-758 Doug Bruce 2015-08-03 - change sql where clause to handle comma separated docid's
                        for (int i = 0; i < multipleDocIds.Length; i++)
                        {
                            if (i > 0)
                            {
                                sb.AppendFormat(" OR ");
                            }
                            sb.AppendFormat("{0}={1}", FileNetPropertyP8.DocNumber, multipleDocIds[i]);
                        }
                        sb.AppendFormat(")");
                    }
                }
            }

            if (docID.Trim().Length <= 0)
            {
                if (docTypes.Length > 0)    //WA-698 Doug Bruce 2015-05-20 - corrects SQL error with extra/missing AND
                {
                    if (docTypes[0].Trim().Length > 0)     //If no docTypes are entered on the screen, the .Length is still 1.  Check to make sure it's not empty!
                    {
                        sb.AppendFormat(" AND ");
                    }
                }
                for (int i = 0; i < docTypes.Length; i++)
                {
                    string docType = docTypes[i];
                    if (docType.Trim().Length > 0)
                    {
                        if (i == 0)
                        {
                            sb.AppendFormat("(");
                        }

                        sb.AppendFormat("{0}='{1}'", FileNetPropertyP8.DocType, docType.Trim());
                        sb.AppendFormat(" OR ");

                        if (i == (docTypes.Length - 1))
                        {
                            sb.Remove(sb.Length - 4, 4);
                            sb.AppendFormat(")");
                        }
                    }
                }
            }

            if (entryStartDate > DateTime.MinValue)
            {
                sb.AppendFormat(" AND ");
                sb.AppendFormat("{0}>={1}", FileNetPropertyP8.EntryDate, entryStartDate.ToString("yyyyMMddT000000Z"));
            }

            if (entryEndDate < DateTime.MaxValue)
            {
                sb.AppendFormat(" AND ");
                sb.AppendFormat("{0}<={1}", FileNetPropertyP8.EntryDate, entryEndDate.ToString("yyyyMMddT235959Z"));
            }

            return sb.ToString();
        }
        
        private static bool IsP8GuidId(string id)
        {
            Guid resultP8;
            return (System.Guid.TryParse(id, out resultP8));
        }

        #endregion
    }
}