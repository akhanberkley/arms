/*===========================================================================
  ContentP8:	FileNetContentP8.cs
  
  Summary:		Contains the implementation of a file in FileNet.
			
  Classes:		FileNetContentP8
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:   	WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging.Server.FileNet
{
	/// <summary>
	/// Contains the implementation of a file in FileNet.
	/// </summary>
	public class FileNetContentP8 : ContentP8   //, IContentP8
	{
		#region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		internal FileNetContentP8(){}
		/// <summary>
		/// Instantiates the object using the state available within the 
		/// provided Proxy.IDMContentP8 object.
		/// </summary>
		/// <param name="file">The Proxy.IDMContentP8 object from which to 
		/// extract the state for use in preloading the instance.</param>
		internal FileNetContentP8(ContentP8 file)
		{
			if (file != null)
			{
				byte[] buffer = new byte[file.ContentBytes.Length];
                file.ContentBytes.CopyTo(buffer, 0);
				this.Content = new System.IO.MemoryStream(buffer);
			}
		}
		#endregion

		#region methods
		/// <summary>
		/// Converts the instance of the object into a Proxy.IDMContentP8 equivalent.
		/// </summary>
		/// <returns>A Proxy.IDMContentP8 representation of the instance.</returns>
		internal ContentP8 Convert()
		{
			ContentP8 result = new ContentP8();

            result.ContentBytes = new byte[this.Content.Length];
            this.Content.Read(result.ContentBytes, 0, result.ContentBytes.Length);

			return result;
		}
		#endregion
	}
}