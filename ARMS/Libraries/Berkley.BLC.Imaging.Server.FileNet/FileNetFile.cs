/*===========================================================================
  File:			FileNetFile.cs
  
  Summary:		Contains the implementation of a file in FileNet.
			
  Classes:		FileNetFile
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging.Server.FileNet
{
	/// <summary>
	/// Contains the implementation of a file in FileNet.
	/// </summary>
	public class FileNetFile : File, IFile
	{
		#region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		internal FileNetFile(){}
		/// <summary>
		/// Instantiates the object using the state available within the 
		/// provided Proxy.IDMFile object.
		/// </summary>
		/// <param name="file">The Proxy.IDMFile object from which to 
		/// extract the state for use in preloading the instance.</param>
		internal FileNetFile(Proxy.File file)
		{
			if (file != null)
			{
				byte[] buffer = new byte[file.data.Length];
				file.data.CopyTo(buffer, 0);
				this.Content = new System.IO.MemoryStream(buffer);
			}
		}
		#endregion

		#region methods
		/// <summary>
		/// Converts the instance of the object into a Proxy.IDMFile equivalent.
		/// </summary>
		/// <returns>A Proxy.IDMFile representation of the instance.</returns>
		internal Proxy.File Convert()
		{
			Proxy.File result = new Proxy.File();

			result.data = new byte[this.Content.Length];
			this.Content.Read(result.data, 0, result.data.Length);

			return result;
		}
		#endregion
	}
}