﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.Domains
{
    public class SurveyStatusCode
    {
        public const string CanceledSurvey = "CC";
        public const string InProgress = "IP";
        public const string OnHoldSurvey = "OH";
        public const string AssignedReview = "RA";
        public const string UnassignedReview = "RU";
        public const string AssignedSurvey = "SA";
        public const string ClosedSurvey = "SC";
        public const string ReturningToCompany = "SR";
        public const string UnassignedSurvey = "SU";
        public const string AwaitingAcceptance = "SW";
    }
}