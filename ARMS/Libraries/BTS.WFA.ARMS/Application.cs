﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS
{
    public class Application : BTS.WFA.WorkflowApplication<Data.IArmsDb>
    {
        public static Dictionary<string, CompanyDetail> CompanyKeyMap { get; private set; }
        public static IEnumerable<string> SystemAdministratorUserNames { get; set; }

        /// <summary>
        /// This method should be called prior to any use of this Assembly to configure how it should work
        /// </summary>
        public static void Initialize<A>(params object[] handlers) where A : Data.IArmsDb
        {
            Initialize(typeof(A), handlers);
            InitializeAutoMapperConfigs();

            SystemAdministratorUserNames = (ConfigurationManager.AppSettings["SystemAdminAccounts"] ?? "").Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            CompanyKeyMap = Services.CompanyService.GetCompanyKeyMap();

        }

        private static void InitializeAutoMapperConfigs()
        {

        }
    }
}
