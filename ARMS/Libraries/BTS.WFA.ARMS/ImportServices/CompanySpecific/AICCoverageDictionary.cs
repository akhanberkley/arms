﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class AICCoverageDictionary : CoverageDictionary
    {
        public AICCoverageDictionary()
        {
        }

        #region Business Owner
        protected override List<CoverageColumn> CreateBusinessOwners()
        {
            List<CoverageColumn> oColumns = new List<CoverageColumn>();

            #region Business Owner
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.BoGeneralLiabilityOccurrenceLimits],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CoverageGroupType.Enum.GeneralLiability,
                                                        CoverageValueType.Enum.OccuranceLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB_OCCR }, 0));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.BoGeneralLiabilityAggregateLimits],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CoverageGroupType.Enum.GeneralLiability,
                                                        CoverageValueType.Enum.AggregateLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB_AGG }, 1));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.BoBusinessIncomeLimits],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CoverageGroupType.Enum.BusinessIncome,
                                                        CoverageValueType.Enum.Limits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 2));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.BoBusinessIncomeFromDependentProperties],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CoverageGroupType.Enum.BusinessIncome,
                                                        CoverageValueType.Enum.FromDependentProperties,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.BINC_DPND }, 3));
            #endregion

            #region Commercial Auto
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerAutoLiability],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialAuto,
                                                        CoverageGroupType.Enum.NonOwnerAuto,
                                                        CoverageValueType.Enum.Liability,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 4));
            #endregion

            #region Inland Marine
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                        CoverageGroupType.Enum.ContractorsEquipmentLeased,
                                                        CoverageValueType.Enum.OwnedEquipment,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                        CoverageGroupType.Enum.ContractorsEquipmentLeased,
                                                        CoverageValueType.Enum.ToOthers,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                        CoverageGroupType.Enum.ContractorsEquipmentLeased,
                                                        CoverageValueType.Enum.FromOthers,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 7));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImToolFloaterLimits],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                        CoverageGroupType.Enum.ToolFloater,
                                                        CoverageValueType.Enum.YourTools,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 8));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                        CoverageGroupType.Enum.ToolFloater,
                                                        CoverageValueType.Enum.EmployeeTools,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 9));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImComputerHardwareLimits],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                        CoverageGroupType.Enum.ComputerEquipment,
                                                        CoverageValueType.Enum.HardwareLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 10));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImSignsScheduledLimits],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                        CoverageGroupType.Enum.Signs,
                                                        CoverageValueType.Enum.ScheduledLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 11));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                        CoverageGroupType.Enum.Signs,
                                                        CoverageValueType.Enum.UnscheduledLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 12));
            #endregion

            #region Commercial Umbrella
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CoverageGroupType.Enum.CommercialUmbrellaNCR,
                                                        CoverageValueType.Enum.OccuranceLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 13));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CuAggregateLimits],
                                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
                                                        CoverageGroupType.Enum.CommercialUmbrellaNCR,
                                                        CoverageValueType.Enum.AggregateLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 14));
            #endregion

            #region Crime
            //oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits],
            //                                       CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
            //                                       CoverageGroupType.Enum.Crime,
            //                                       CoverageValueType.Enum.InsideThePremesisTheftOfMoneyAndSecurites,
            //                                       new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 15));

            //oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits],
            //                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
            //                                        CoverageGroupType.Enum.Crime,
            //                                        CoverageValueType.Enum.InsideThePremesisRobberyOfMoneyAndSecurites,
            //                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 16));

            //oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits],
            //                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
            //                                        CoverageGroupType.Enum.Crime,
            //                                        CoverageValueType.Enum.OutsideThePremesisRobberyOfMoneyAndSecurites,
            //                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 17));

            //oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits],
            //                                        CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner,
            //                                        CoverageGroupType.Enum.Crime,
            //                                        CoverageValueType.Enum.TheftPerEmployee,
            //                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 18));
            #endregion

            return oColumns;
        }
        #endregion

        #region Workers Comp
        protected override List<CoverageColumn> CreateWorkersComp()
        {
            List<CoverageColumn> oColumns = new List<CoverageColumn>();
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.WcExposure],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.WorkersComp,
                                                    CoverageGroupType.Enum.Exposure,
                                                    CoverageValueType.Enum.ClassCodePayrollEmployees,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));
            return oColumns;
        }
        #endregion

        #region Commercial Output
        protected override List<CoverageColumn> CreateCommercialOutput()
        {
            List<CoverageColumn> oColumns = new List<CoverageColumn>();
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CoBuildingLimit],
                                        CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialOutputPolicy,
                                        CoverageGroupType.Enum.Building,
                                        CoverageValueType.Enum.Limits,
                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 0));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CoBuildingCatastropheLimit],
                                        CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialOutputPolicy,
                                        CoverageGroupType.Enum.Building,
                                        CoverageValueType.Enum.CatastropheLimits,
                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.DN_CATLMT }, 1));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CoPersonalPropertyLimit],
                                        CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialOutputPolicy,
                                        CoverageGroupType.Enum.PersonalProperty,
                                        CoverageValueType.Enum.Limits,
                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));
            return oColumns;
        }
        #endregion

        #region Commercial Auto
        protected override List<CoverageColumn> CreateCommercialAuto()
        {
            List<CoverageColumn> oColumns = new List<CoverageColumn>();
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialAuto,
                                                    CoverageGroupType.Enum.CommercialAuto,
                                                    CoverageValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB }, 0));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialAuto,
                                                    CoverageGroupType.Enum.CommercialAuto,
                                                    CoverageValueType.Enum.PhysicalDamageLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.COMP }, 1));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CaVehicleCount],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialAuto,
                                                    CoverageGroupType.Enum.CommercialAuto,
                                                    CoverageValueType.Enum.NumberOfVehicles,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialAuto,
                                                    CoverageGroupType.Enum.NonOwnerAuto,
                                                    CoverageValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 3));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialAuto,
                                                    CoverageGroupType.Enum.NonOwnerAuto,
                                                    CoverageValueType.Enum.PhysicalDamageLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 4));
            return oColumns;
        }
        #endregion

        #region Commercial Crime
        protected override List<CoverageColumn> CreateCommercialCrime()
        {
            List<CoverageColumn> oColumns = new List<CoverageColumn>();
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits],
                                                   CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialCrime,
                                                   CoverageGroupType.Enum.Crime,
                                                   CoverageValueType.Enum.InsideThePremesisTheftOfMoneyAndSecurites,
                                                   new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialCrime,
                                                    CoverageGroupType.Enum.Crime,
                                                    CoverageValueType.Enum.InsideThePremesisRobberyOfMoneyAndSecurites,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialCrime,
                                                    CoverageGroupType.Enum.Crime,
                                                    CoverageValueType.Enum.OutsideThePremesisRobberyOfMoneyAndSecurites,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialCrime,
                                                    CoverageGroupType.Enum.Crime,
                                                    CoverageValueType.Enum.TheftPerEmployee,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));
            return oColumns;
        }
        #endregion

        #region Commercial Umbrella
        protected override List<CoverageColumn> CreateCommercialUmbrella()
        {
            List<CoverageColumn> oColumns = new List<CoverageColumn>();
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialUmbrella,
                                                    CoverageGroupType.Enum.CommercialUmbrellaNCR,
                                                    CoverageValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 0));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CuAggregateLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialUmbrella,
                                                    CoverageGroupType.Enum.CommercialUmbrellaNCR,
                                                    CoverageValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 1));
            return oColumns;
        }
        #endregion

        #region Garage
        protected override List<CoverageColumn> CreateGarage()
        {
            List<CoverageColumn> oColumns = new List<CoverageColumn>();
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaComprehensiveDeductable],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CoverageGroupType.Enum.Comprehensive,
                                                    CoverageValueType.Enum.Deductable,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaComprehensiveLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CoverageGroupType.Enum.Comprehensive,
                                                    CoverageValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaSpecialCausesOfLossDeductable],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CoverageGroupType.Enum.SpecifiedCausesOfLoss,
                                                    CoverageValueType.Enum.Deductable,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaSpecialCausesOfLossLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CoverageGroupType.Enum.SpecifiedCausesOfLoss,
                                                    CoverageValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaCollisionDeductable],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CoverageGroupType.Enum.Collision,
                                                    CoverageValueType.Enum.Deductable,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaCollisionLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CoverageGroupType.Enum.Collision,
                                                    CoverageValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaDealerClassOfOpRegularOPFurnishedAutos],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                                                    CoverageGroupType.Enum.ClassOfOperations,
                                                    CoverageValueType.Enum.RegularOpFurnishedAutos,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaDealerClassOfOpRegularOPNonFurnishedAutos],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                                                    CoverageGroupType.Enum.ClassOfOperations,
                                                    CoverageValueType.Enum.RegularOpNonFurnishedAutos,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 7));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaDealerClassOfOpEmployeesNonRegularOp],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                                                    CoverageGroupType.Enum.ClassOfOperations,
                                                    CoverageValueType.Enum.EmployeesNonRegularOp,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 8));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaDealerClassOfOpNonEmployeesUnder25],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                                                    CoverageGroupType.Enum.ClassOfOperations,
                                                    CoverageValueType.Enum.NonEmployeesUnder25,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 9));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaDealerClassOfOpNonEmployeesOver25],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                                                    CoverageGroupType.Enum.ClassOfOperations,
                                                    CoverageValueType.Enum.NonEmployeesOver25,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 10));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaDealerPhysicalDamageComprehensiveLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                                                    CoverageGroupType.Enum.PhysicalDamage,
                                                    CoverageValueType.Enum.ComprehensiveLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 11));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaDealerPhysicalDamageCollisionLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                                                    CoverageGroupType.Enum.PhysicalDamage,
                                                    CoverageValueType.Enum.CollisionLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 12));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionVehicleCount],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                                                    CoverageGroupType.Enum.DriveawayCollision,
                                                    CoverageValueType.Enum.NumberOfVehicles,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 13));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionTripCount],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                                                    CoverageGroupType.Enum.DriveawayCollision,
                                                    CoverageValueType.Enum.NumberOfTrips,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 14));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionPriceNew],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                                                    CoverageGroupType.Enum.DriveawayCollision,
                                                    CoverageValueType.Enum.PriceNew,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 15));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionMileage],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                                                    CoverageGroupType.Enum.DriveawayCollision,
                                                    CoverageValueType.Enum.Mileage,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 16));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionDeductable],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Garage,
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                                                    CoverageGroupType.Enum.DriveawayCollision,
                                                    CoverageValueType.Enum.Deductable,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 17));
            return oColumns;
        }
        #endregion

        #region General Liablility
        protected override List<CoverageColumn> CreateGeneralLiability()
        {
            List<CoverageColumn> oColumns = new List<CoverageColumn>();
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.GeneralLiability,
                                                    CoverageGroupType.Enum.GeneralLiability,
                                                    CoverageValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR }, 0));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.GeneralLiability,
                                                    CoverageGroupType.Enum.GeneralLiability,
                                                    CoverageValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_AGG }, 1));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.GeneralLiability,
                                                    CoverageGroupType.Enum.GeneralLiability,
                                                    CoverageValueType.Enum.Exposures,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.GeneralLiability,
                                                    CoverageGroupType.Enum.ProductsOperations,
                                                    CoverageValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROD }, 3));
            return oColumns;
        }
        #endregion

        #region Commercial Property
        protected override List<CoverageColumn> CreateCommercialProperty()
        {
            List<CoverageColumn> oColumns = new List<CoverageColumn>();
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialProperty,
                                                    CoverageGroupType.Enum.CommercialUmbrellaNCR,
                                                    CoverageValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 0));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CuAggregateLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialProperty,
                                                    CoverageGroupType.Enum.CommercialUmbrellaNCR,
                                                    CoverageValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 1));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialProperty,
                                                    CoverageGroupType.Enum.BusinessIncome,
                                                    CoverageValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 2));
            return oColumns;
        }
        #endregion

        #region Inland Marine
        protected override List<CoverageColumn> CreateInlandMarine()
        {
            List<CoverageColumn> oColumns = new List<CoverageColumn>();
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.MotorTruckCargo,
                                                    CoverageValueType.Enum.LegalLiabilityLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CRGO_SCHED }, 0));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.MotorTruckCargo,
                                                    CoverageValueType.Enum.VehicleLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROP_VEH }, 1));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.BuildersRisk,
                                                    CoverageValueType.Enum.ConstructionLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CSE_CONS }, 2));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.BuildersRisk,
                                                    CoverageValueType.Enum.ExistingBuildingLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.JSITE_SCHD }, 3));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilitySpoilageLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.WarehouseLiability,
                                                    CoverageValueType.Enum.SpoilageLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilityColdStorageLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.WarehouseLiability,
                                                    CoverageValueType.Enum.ColdStorageLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImJewelersBlockPremisesLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.JewelersBlock,
                                                    CoverageValueType.Enum.YourPremisesLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.ContractorsEquipmentLeased,
                                                    CoverageValueType.Enum.OwnedEquipment,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.FROM_OTH }, 7));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.ContractorsEquipmentLeased,
                                                    CoverageValueType.Enum.ToOthers,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TO_OTH }, 8));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.ContractorsEquipmentLeased,
                                                    CoverageValueType.Enum.FromOthers,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PRMS_RENT }, 9));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImToolFloaterLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.ToolFloater,
                                                    CoverageValueType.Enum.YourTools,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.YOUR_TOOL }, 10));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.ToolFloater,
                                                    CoverageValueType.Enum.EmployeeTools,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EMP_TOOL }, 11));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImComputerHardwareLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.ComputerEquipment,
                                                    CoverageValueType.Enum.HardwareLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 12));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImComputerSoftwareMediaLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.ComputerEquipment,
                                                    CoverageValueType.Enum.SoftwareMedia,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 13));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImSignsScheduledLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.Signs,
                                                    CoverageValueType.Enum.ScheduledLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN }, 14));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.Signs,
                                                    CoverageValueType.Enum.UnscheduledLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN_USCHD }, 15));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImAccountsReceivableInsuredPremises1],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.AccountsReceivable,
                                                    CoverageValueType.Enum.InsuredPremises1,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 16));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImAccountsReceivableInsuredPremises2],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.AccountsReceivable,
                                                    CoverageValueType.Enum.InsuredPremises2,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 17));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImValuablePapersProperty1],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.ValuablePapers,
                                                    CoverageValueType.Enum.Property1,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 18));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImValuablePapersProperty2],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.ValuablePapers,
                                                    CoverageValueType.Enum.Property2,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 19));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImValuablePapersProperty3],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.ValuablePapers,
                                                    CoverageValueType.Enum.Property3,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 20));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImValuablePapersAllOtherCoveredProperty],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.ValuablePapers,
                                                    CoverageValueType.Enum.AllOtherCoveredProperty,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 21));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImFineArtsFloaterOnPremises],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.FineArtsFloater,
                                                    CoverageValueType.Enum.OnPremises,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 22));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImFineArtsFloaterTransportation],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.FineArtsFloater,
                                                    CoverageValueType.Enum.Transportation,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 23));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImBaileesCoveragePremises],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.BaileesCoverage,
                                                    CoverageValueType.Enum.Premises,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 24));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImBaileesCoverageTransit],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.BaileesCoverage,
                                                    CoverageValueType.Enum.Transit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 25));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImTransitCoverageHiredCarrier],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.TransitCoverage,
                                                    CoverageValueType.Enum.HiredCarrier,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 26));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.ImTransitCoverageOwnedVehicle],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                                                    CoverageGroupType.Enum.TransitCoverage,
                                                    CoverageValueType.Enum.OwnedVehicle,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 27));
            return oColumns;
        }
        #endregion

        #region Ocean Marine
        protected override List<CoverageColumn> CreateOceanMarine()
        {
            List<CoverageColumn> oColumns = new List<CoverageColumn>();
            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.OmPhysicalDamageVesselLimits],
                                                   CompanyLineOfBusinessEntity.LineOfBusinessType.OceanMarine,
                                                   CoverageGroupType.Enum.PhysicalDamage,
                                                   CoverageValueType.Enum.VesselLimits,
                                                   new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.OmPhysicalDamageExcessCollisionLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.OceanMarine,
                                                    CoverageGroupType.Enum.PhysicalDamage,
                                                    CoverageValueType.Enum.ExcessCollisionLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.OmPhysicalDamageBreachOfWarrantyLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.OceanMarine,
                                                    CoverageGroupType.Enum.PhysicalDamage,
                                                    CoverageValueType.Enum.BreachofWarrantyLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.OmLegalLiabilityLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.OceanMarine,
                                                    CoverageGroupType.Enum.LegalLiability,
                                                    CoverageValueType.Enum.LegalLiabilityLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.OmLegalLiabilityMedicalPaymentsLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.OceanMarine,
                                                    CoverageGroupType.Enum.LegalLiability,
                                                    CoverageValueType.Enum.MedicalPaymentsLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.OmBoatDealersCoverageDLimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.OceanMarine,
                                                    CoverageGroupType.Enum.BoatDealer,
                                                    CoverageValueType.Enum.CoverageDLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.OmBoatDealersCoverageALimits],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.OceanMarine,
                                                    CoverageGroupType.Enum.BoatDealer,
                                                    CoverageValueType.Enum.CoverageALimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.OmBuildersRiskAmountInsured],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.OceanMarine,
                                                    CoverageGroupType.Enum.BuildersRisk,
                                                    CoverageValueType.Enum.AmountInsured,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 7));

            oColumns.Add(new CoverageColumn(moCoverageDictionary[CoverageMap.OmBuildersRiskAgreedValue],
                                                    CompanyLineOfBusinessEntity.LineOfBusinessType.OceanMarine,
                                                    CoverageGroupType.Enum.BuildersRisk,
                                                    CoverageValueType.Enum.AgreedValue,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 8));
            return oColumns;
        }
        #endregion

        #region Package
        protected override List<CoverageColumn> CreatePackages()
        {
            List<CoverageColumn> oPackageList = new List<CoverageColumn>();

            #region Commercial Auto
            oPackageList.Add(new CoverageColumn(
                                moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                                CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialAuto,
                                CoverageGroupType.Enum.CommercialAuto,
                                CoverageValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB }, 0));

            oPackageList.Add(new CoverageColumn(
                                moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit],
                                CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                                CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialAuto,
                                CoverageGroupType.Enum.CommercialAuto,
                                CoverageValueType.Enum.PhysicalDamageLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.COMP }, 1));

            oPackageList.Add(new CoverageColumn(
                                moCoverageDictionary[CoverageMap.CaVehicleCount],
                                CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                                CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialAuto,
                                CoverageGroupType.Enum.CommercialAuto,
                                CoverageValueType.Enum.NumberOfVehicles,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oPackageList.Add(new CoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                                CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialAuto,
                                CoverageGroupType.Enum.NonOwnerAuto,
                                CoverageValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 3));

            oPackageList.Add(new CoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                                CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialAuto,
                                CoverageGroupType.Enum.NonOwnerAuto,
                                CoverageValueType.Enum.PhysicalDamageLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 4));
            #endregion

            #region Inland Marine
            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                    CoverageGroupType.Enum.ContractorsEquipmentLeased,
                    CoverageValueType.Enum.OwnedEquipment,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CONTR_CAT }, 5));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                    CoverageGroupType.Enum.ContractorsEquipmentLeased,
                    CoverageValueType.Enum.ToOthers,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TO_OTH }, 6));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                    CoverageGroupType.Enum.ContractorsEquipmentLeased,
                    CoverageValueType.Enum.FromOthers,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.FROM_OTH }, 7));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.ImToolFloaterLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                    CoverageGroupType.Enum.ToolFloater,
                    CoverageValueType.Enum.YourTools,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NEW_EQUIP }, 8));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                    CoverageGroupType.Enum.ToolFloater,
                    CoverageValueType.Enum.EmployeeTools,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EMP_TOOL }, 9));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.ImComputerHardwareLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                    CoverageGroupType.Enum.ComputerEquipment,
                    CoverageValueType.Enum.HardwareLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EDP_HDWR }, 10));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.ImSignsScheduledLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                    CoverageGroupType.Enum.Signs,
                    CoverageValueType.Enum.ScheduledLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN }, 11));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine,
                    CoverageGroupType.Enum.Signs,
                    CoverageValueType.Enum.UnscheduledLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN_USCHD }, 12));
            #endregion

            #region Commercial Umbrella
            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Unknown,
                    CoverageGroupType.Enum.CommercialUmbrellaNCR,
                    CoverageValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 13));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.CuAggregateLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Unknown,
                    CoverageGroupType.Enum.CommercialUmbrellaNCR,
                    CoverageValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 14));
            #endregion

            #region General Liability
            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.GeneralLiability,
                    CoverageGroupType.Enum.GeneralLiability,
                    CoverageValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR, PolicyStarCoverageValueType.Enum.LIAB_OCCR }, 15));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.GeneralLiability,
                    CoverageGroupType.Enum.GeneralLiability,
                    CoverageValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_AGG, PolicyStarCoverageValueType.Enum.LIAB_AGG }, 16));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.GeneralLiability,
                    CoverageGroupType.Enum.GeneralLiability,
                    CoverageValueType.Enum.Exposures,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL1 }, 17));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.GeneralLiability,
                    CoverageGroupType.Enum.ProductsOperations,
                    CoverageValueType.Enum.Limits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROD, PolicyStarCoverageValueType.Enum.PROD_CPT }, 18));
            #endregion

            #region Crime
            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Unknown,
                    CoverageGroupType.Enum.Crime,
                    CoverageValueType.Enum.InsideThePremesisTheftOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 19));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Unknown,
                    CoverageGroupType.Enum.Crime,
                    CoverageValueType.Enum.InsideThePremesisRobberyOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 20));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Unknown,
                    CoverageGroupType.Enum.Crime,
                    CoverageValueType.Enum.OutsideThePremesisRobberyOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 21));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Unknown,
                    CoverageGroupType.Enum.Crime,
                    CoverageValueType.Enum.TheftPerEmployee,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EMP_THFT_B }, 22));
            #endregion

            #region Property
            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialProperty,
                    CoverageGroupType.Enum.BusinessIncome,
                    CoverageValueType.Enum.Limits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 23));
            #endregion

            #region Workers Comp
            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.WcExposure],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.WorkersComp,
                    CoverageGroupType.Enum.Exposure,
                    CoverageValueType.Enum.ClassCodePayrollEmployees,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 24));
            #endregion

            #region Dealer
            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GaDealerClassOfOpRegularOPFurnishedAutos],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                    CoverageGroupType.Enum.ClassOfOperations,
                    CoverageValueType.Enum.RegularOpFurnishedAutos,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 25));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GaDealerClassOfOpRegularOPNonFurnishedAutos],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                    CoverageGroupType.Enum.ClassOfOperations,
                    CoverageValueType.Enum.RegularOpNonFurnishedAutos,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 26));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GaDealerClassOfOpEmployeesNonRegularOp],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                    CoverageGroupType.Enum.ClassOfOperations,
                    CoverageValueType.Enum.EmployeesNonRegularOp,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 27));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GaDealerClassOfOpNonEmployeesUnder25],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                    CoverageGroupType.Enum.ClassOfOperations,
                    CoverageValueType.Enum.NonEmployeesUnder25,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 28));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GaDealerClassOfOpNonEmployeesOver25],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                    CoverageGroupType.Enum.ClassOfOperations,
                    CoverageValueType.Enum.NonEmployeesOver25,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 29));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GaDealerPhysicalDamageComprehensiveLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                    CoverageGroupType.Enum.PhysicalDamage,
                    CoverageValueType.Enum.ComprehensiveLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 30));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GaDealerPhysicalDamageCollisionLimits],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                    CoverageGroupType.Enum.PhysicalDamage,
                    CoverageValueType.Enum.CollisionLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 31));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionVehicleCount],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                    CoverageGroupType.Enum.DriveawayCollision,
                    CoverageValueType.Enum.NumberOfVehicles,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 32));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionTripCount],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                    CoverageGroupType.Enum.DriveawayCollision,
                    CoverageValueType.Enum.NumberOfTrips,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 33));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionPriceNew],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                    CoverageGroupType.Enum.DriveawayCollision,
                    CoverageValueType.Enum.PriceNew,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 34));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionMileage],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                    CoverageGroupType.Enum.DriveawayCollision,
                    CoverageValueType.Enum.Mileage,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 35));

            oPackageList.Add(new CoverageColumn(
                    moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionDeductable],
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Package,
                    CompanyLineOfBusinessEntity.LineOfBusinessType.Dealer,
                    CoverageGroupType.Enum.DriveawayCollision,
                    CoverageValueType.Enum.Deductable,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 36));
            #endregion

            return oPackageList;
        }
        #endregion

        public override int GetMaxColumnNumber()
        {
            //largest column number plus one
            return 87;
        }

        protected override void BuildCoverageMap()
        {
            // Business Owner
            moCoverageDictionary[CoverageMap.BoGeneralLiabilityOccurrenceLimits] = 4;
            moCoverageDictionary[CoverageMap.BoGeneralLiabilityAggregateLimits] = 5;
            moCoverageDictionary[CoverageMap.BoBusinessIncomeLimits] = 6;
            moCoverageDictionary[CoverageMap.BoBusinessIncomeFromDependentProperties] = 7;

            // Commercial Output
            moCoverageDictionary[CoverageMap.CoBuildingLimit] = 9;
            moCoverageDictionary[CoverageMap.CoBuildingCatastropheLimit] = 10;
            moCoverageDictionary[CoverageMap.CoPersonalPropertyLimit] = 11;

            // Commercial Umbrella
            moCoverageDictionary[CoverageMap.CuOccurrenceLimits] = 39;
            moCoverageDictionary[CoverageMap.CuAggregateLimits] = 40;

            // Workers Comp
            moCoverageDictionary[CoverageMap.WcExposure] = 12;

            // Commercial Auto
            moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit] = 13;
            moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit] = 14;
            moCoverageDictionary[CoverageMap.CaVehicleCount] = 15;
            moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit] = 16;
            moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit] = 17;
            moCoverageDictionary[CoverageMap.CaNonOwnerAutoLiability] = 8;

            // Commercial Crime
            moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits] = 41;
            moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits] = 42;
            moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits] = 44;
            moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits] = 45;

            // Garage
            moCoverageDictionary[CoverageMap.GaComprehensiveDeductable] = 68;
            moCoverageDictionary[CoverageMap.GaComprehensiveLimits] = 73;
            moCoverageDictionary[CoverageMap.GaSpecialCausesOfLossDeductable] = 69;
            moCoverageDictionary[CoverageMap.GaSpecialCausesOfLossLimits] = 72;
            moCoverageDictionary[CoverageMap.GaCollisionDeductable] = 70;
            moCoverageDictionary[CoverageMap.GaCollisionLimits] = 71;

            moCoverageDictionary[CoverageMap.GaDealerClassOfOpRegularOPFurnishedAutos] = 56;
            moCoverageDictionary[CoverageMap.GaDealerClassOfOpRegularOPNonFurnishedAutos] = 57;
            moCoverageDictionary[CoverageMap.GaDealerClassOfOpEmployeesNonRegularOp] = 58;
            moCoverageDictionary[CoverageMap.GaDealerClassOfOpNonEmployeesUnder25] = 59;
            moCoverageDictionary[CoverageMap.GaDealerClassOfOpNonEmployeesOver25] = 60;
            moCoverageDictionary[CoverageMap.GaDealerPhysicalDamageComprehensiveLimits] = 61;
            moCoverageDictionary[CoverageMap.GaDealerPhysicalDamageCollisionLimits] = 62;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionVehicleCount] = 63;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionTripCount] = 64;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionPriceNew] = 65;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionMileage] = 66;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionDeductable] = 67;

            // General Liability
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits] = 21;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits] = 22;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures] = 23;
            moCoverageDictionary[CoverageMap.GlProductsOperationsLimits] = 24;

            // Commercial Property
            moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits] = 43;

            // Inland Marine
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits] = 25;
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits] = 26;
            moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits] = 27;
            moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits] = 28;
            moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilitySpoilageLimits] = 29;
            moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilityColdStorageLimits] = 30;
            moCoverageDictionary[CoverageMap.ImJewelersBlockPremisesLimits] = 31;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment] = 46;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits] = 32;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits] = 33;
            moCoverageDictionary[CoverageMap.ImToolFloaterLimits] = 34;
            moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits] = 35;
            moCoverageDictionary[CoverageMap.ImComputerHardwareLimits] = 36;
            moCoverageDictionary[CoverageMap.ImComputerSoftwareMediaLimits] = 86;
            moCoverageDictionary[CoverageMap.ImSignsScheduledLimits] = 37;
            moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits] = 38;
            moCoverageDictionary[CoverageMap.ImAccountsReceivableInsuredPremises1] = 74;
            moCoverageDictionary[CoverageMap.ImAccountsReceivableInsuredPremises2] = 75;
            moCoverageDictionary[CoverageMap.ImValuablePapersProperty1] = 76;
            moCoverageDictionary[CoverageMap.ImValuablePapersProperty2] = 77;
            moCoverageDictionary[CoverageMap.ImValuablePapersProperty3] = 78;
            moCoverageDictionary[CoverageMap.ImValuablePapersAllOtherCoveredProperty] = 79;
            moCoverageDictionary[CoverageMap.ImFineArtsFloaterOnPremises] = 80;
            moCoverageDictionary[CoverageMap.ImFineArtsFloaterTransportation] = 81;
            moCoverageDictionary[CoverageMap.ImBaileesCoveragePremises] = 82;
            moCoverageDictionary[CoverageMap.ImBaileesCoverageTransit] = 83;
            moCoverageDictionary[CoverageMap.ImTransitCoverageHiredCarrier] = 84;
            moCoverageDictionary[CoverageMap.ImTransitCoverageOwnedVehicle] = 85;

            // Ocean Marine
            moCoverageDictionary[CoverageMap.OmPhysicalDamageVesselLimits] = 47;
            moCoverageDictionary[CoverageMap.OmPhysicalDamageExcessCollisionLimits] = 48;
            moCoverageDictionary[CoverageMap.OmPhysicalDamageBreachOfWarrantyLimits] = 49;
            moCoverageDictionary[CoverageMap.OmLegalLiabilityLimits] = 50;
            moCoverageDictionary[CoverageMap.OmLegalLiabilityMedicalPaymentsLimits] = 51;
            moCoverageDictionary[CoverageMap.OmBoatDealersCoverageDLimits] = 52;
            moCoverageDictionary[CoverageMap.OmBoatDealersCoverageALimits] = 55;
            moCoverageDictionary[CoverageMap.OmBuildersRiskAmountInsured] = 53;
            moCoverageDictionary[CoverageMap.OmBuildersRiskAgreedValue] = 54;
        }
    }
}
