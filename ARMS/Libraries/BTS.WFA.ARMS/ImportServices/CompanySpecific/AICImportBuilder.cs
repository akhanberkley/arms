﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class AICImportBuilder : BaseImportBuilder, IImportBuilder
    {
        #region properties

        private StoredProcedureBO moStoredProcedure = null;
        private CompanyPolicyEntity moPolicyFieldEntity = new CompanyPolicyEntity();
        private CompanyInsuredEntity moInsuredFieldEntity = new CompanyInsuredEntity();
        private CompanyAgencyEntity moAgencyFieldEntity = new CompanyAgencyEntity();
        private CompanyLocationEntity moLocationFieldEntity = new CompanyLocationEntity();
        private CompanyBuildingEntity moBuildingFieldEntity = new CompanyBuildingEntity();
        private CompanyClaimEntity moCompanyClaimEntity = new CompanyClaimEntity();
        private CompanyRateModFactorEntity moCompanyRateModFactorEntity = new CompanyRateModFactorEntity();
        private AICLineOfBusiness moLineOfBusiness = new AICLineOfBusiness();

        #region Line Of Business / Coverage
        private CompanyCoverageEntity moCoverageFieldEntity = new CompanyCoverageEntity();
        private AICCoverageDictionary moCoverageDictionary = new AICCoverageDictionary();
        #endregion

        #endregion

        public AICImportBuilder(string strClientId, string strPolicyNumber, string strAgentNumber)
        {
            base.Initialize(this, strClientId, strPolicyNumber, strAgentNumber);
            // Rearrange fields in moPolicyFieldEntity as necessary
        }

        #region Members
        public CompanyCoverageEntity CoverageFieldEntity
        {
            get { return moCoverageFieldEntity; }
        }

        // The derived classes can change the field mapping
        public CompanyPolicyEntity PolicyFieldEntity
        {
            get { return moPolicyFieldEntity; }
        }

        // The derived classes can change the field mapping
        public CompanyInsuredEntity InsuredFieldEntity
        {
            get { return moInsuredFieldEntity; }
        }

        public CompanyAgencyEntity AgencyFieldEntity
        {
            get { return moAgencyFieldEntity; }
        }

        public CompanyLocationEntity LocationFieldEntity
        {
            get { return moLocationFieldEntity; }
        }

        public CompanyBuildingEntity BuildingFieldEntity
        {
            get { return moBuildingFieldEntity; }
        }

        public CompanyLineOfBusinessEntity LineOfBusinessFieldEntity
        {
            get { return moLineOfBusiness; }
        }

        public CompanyClaimEntity ClaimFieldEntity
        {
            get { return moCompanyClaimEntity; }
        }

        public CompanyRateModFactorEntity RateModFactorFieldEntity
        {
            get { return moCompanyRateModFactorEntity; }
        }

        public string CompanyAbbreviation
        {
            get { return "AIC"; }
        }
        #endregion

        #region Methods

        private string TrimPolicyNumber(string strPolicyNumber)
        {
            if (strPolicyNumber.Length > 2)
            {
                string strReturn = strPolicyNumber.Substring(2, strPolicyNumber.Length - 2);
                return strReturn;
            }
            else
            {
                return strPolicyNumber;
            }
        }

        public InsuredBO BuildComplete(InsuredBO oInsuredBO)
        {
            // The BlcDataSet has been filled out.  Do the Insert into the BLC DB
            // If all of the company (derived) classes do the same thing, put the basic insert/update
            // in BaseImportBuilder
            Policy oPolicy;
            AICLineOfBusiness oLineOfBusiness = new AICLineOfBusiness();
            CompanyLineOfBusinessEntity.LineOfBusinessType eType;
            List<Policy> oPolicyList = oInsuredBO.PolicyList;
            for (int i = 0; i < oPolicyList.Count; i++)
            {
                oPolicy = oPolicyList[i];
                eType = oLineOfBusiness.GetType(oPolicy.PolicySymbol);
                oPolicy.LineOfBusinessCode = oLineOfBusiness.GetBlcName(eType);
                oPolicy.PolicyNumber = TrimPolicyNumber(oPolicy.PolicyNumber);
                oPolicyList[i] = oPolicy;
            }
            //Trim the policies for the rate mods
            for (int j = 0; j < oInsuredBO.RateModFactorList.Count; j++)
            {
                oInsuredBO.RateModFactorList[j].PolicyNumber = TrimPolicyNumber(oInsuredBO.RateModFactorList[j].PolicyNumber);
            }
            return oInsuredBO;
        }

        public StoredProcedureBO GetStoredProcedure()
        {
            if (moStoredProcedure == null)
            {
                // #TODO: Rectify - connection string
                string strConnection = null; // CoreSettings.AICConnectionString;
                CompanyStoredProcedureEntityList oList = new CompanyStoredProcedureEntityList(strConnection, base.ClientId, base.PolicyNumber, base.AgentNumber);
                //oList.Load(Settings.Default.AICImportFileList);
                moStoredProcedure = new StoredProcedureBO(oList);
            }
            return moStoredProcedure;
        }

        public StoredProcedureBO GetStoredProcedure(CompanyStoredProcedureEntity.SpType eType)
        {
            if (moStoredProcedure == null)
            {
                // #TODO: Rectify - connection string
                string strConnection = null; //CoreSettings.AICConnectionString;
                CompanyStoredProcedureEntityList oList = new CompanyStoredProcedureEntityList(strConnection, base.ClientId, base.PolicyNumber, base.AgentNumber);
                //oList.Load(Settings.Default.AICImportFileList);
                moStoredProcedure = new StoredProcedureBO(oList, eType);
            }
            return moStoredProcedure;
        }

        public List<CoverageColumn> GetCoverage(CompanyLineOfBusinessEntity.LineOfBusinessType eType)
        {
            return moCoverageDictionary.GetColumnList(eType);
        }

        public int GetMaxCoverageColumns()
        {
            return moCoverageDictionary.GetColumnCount();
        }

        public int GetMaxColumnNumber()
        {
            return moCoverageDictionary.GetMaxColumnNumber();
        }

        #endregion
    }
}
