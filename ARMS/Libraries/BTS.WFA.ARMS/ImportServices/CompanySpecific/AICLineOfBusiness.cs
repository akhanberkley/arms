﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class AICLineOfBusiness : CompanyLineOfBusinessEntity
    {
        public AICLineOfBusiness()
        {
        }

        protected override void DerivedBuildMap(ref Dictionary<int, string> oDictionary)
        {
            // Pipe delimit multiple symbols for a LOB
            oDictionary.Add((int)LineOfBusinessType.BusinessOwner, GetCompanyPolicySymbols(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"),"BO"));
            oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"), "CA"));
            oDictionary.Add((int)LineOfBusinessType.CommercialCrime, GetCompanyPolicySymbols(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"), "CR"));
            oDictionary.Add((int)LineOfBusinessType.CommercialProperty, GetCompanyPolicySymbols(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"), "CP"));
            oDictionary.Add((int)LineOfBusinessType.Garage, GetCompanyPolicySymbols(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"), "GA"));
            oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"), "GL"));
            oDictionary.Add((int)LineOfBusinessType.CommercialUmbrella, GetCompanyPolicySymbols(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"), "CU"));
            oDictionary.Add((int)LineOfBusinessType.InlandMarine, GetCompanyPolicySymbols(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"), "IM"));
            oDictionary.Add((int)LineOfBusinessType.OceanMarine, GetCompanyPolicySymbols(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"), "OM"));
            oDictionary.Add((int)LineOfBusinessType.Package, GetCompanyPolicySymbols(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"), "PK"));
            oDictionary.Add((int)LineOfBusinessType.WorkersComp, GetCompanyPolicySymbols(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"), "WC"));

            //oDictionary.Add((int)LineOfBusinessType.BusinessOwner, GetCompanyPolicySymbols(Entities.Company.AcadiaInsuranceCompany, Entities.LineOfBusiness.BusinessOwner));
            //oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(Entities.Company.AcadiaInsuranceCompany, Entities.LineOfBusiness.CommercialAuto));
            //oDictionary.Add((int)LineOfBusinessType.CommercialCrime, GetCompanyPolicySymbols(Entities.Company.AcadiaInsuranceCompany, Entities.LineOfBusiness.CommercialCrime));
            //oDictionary.Add((int)LineOfBusinessType.CommercialProperty, GetCompanyPolicySymbols(Entities.Company.AcadiaInsuranceCompany, Entities.LineOfBusiness.CommercialProperty));
            //oDictionary.Add((int)LineOfBusinessType.Garage, GetCompanyPolicySymbols(Entities.Company.AcadiaInsuranceCompany, Entities.LineOfBusiness.Garage));
            //oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(Entities.Company.AcadiaInsuranceCompany, Entities.LineOfBusiness.GeneralLiability));
            //oDictionary.Add((int)LineOfBusinessType.CommercialUmbrella, GetCompanyPolicySymbols(Entities.Company.AcadiaInsuranceCompany, Entities.LineOfBusiness.CommercialUmbrella));
            //oDictionary.Add((int)LineOfBusinessType.InlandMarine, GetCompanyPolicySymbols(Entities.Company.AcadiaInsuranceCompany, Entities.LineOfBusiness.InlandMarine));
            //oDictionary.Add((int)LineOfBusinessType.OceanMarine, GetCompanyPolicySymbols(Entities.Company.AcadiaInsuranceCompany, Entities.LineOfBusiness.OceanMarine));
            //oDictionary.Add((int)LineOfBusinessType.Package, GetCompanyPolicySymbols(Entities.Company.AcadiaInsuranceCompany, Entities.LineOfBusiness.Package));
            //oDictionary.Add((int)LineOfBusinessType.WorkersComp, GetCompanyPolicySymbols(Entities.Company.AcadiaInsuranceCompany, Entities.LineOfBusiness.WorkersCompensation));

            moBlcLobDictionary.Add(LineOfBusinessType.BusinessOwner, "BO");
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, "CA");
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialCrime, "CR");
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialProperty, "CP");
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialUmbrella, "CU");
            moBlcLobDictionary.Add(LineOfBusinessType.Dealer, "DR");
            moBlcLobDictionary.Add(LineOfBusinessType.Garage, "GA");
            moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, "GL");
            moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, "IM");
            moBlcLobDictionary.Add(LineOfBusinessType.OceanMarine, "OM");
            moBlcLobDictionary.Add(LineOfBusinessType.Package, "PK");
            moBlcLobDictionary.Add(LineOfBusinessType.WorkersComp, "WC");

            //moBlcLobDictionary.Add(LineOfBusinessType.BusinessOwner, Entities.LineOfBusiness.BusinessOwner.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, Entities.LineOfBusiness.CommercialAuto.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.CommercialCrime, Entities.LineOfBusiness.CommercialCrime.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.CommercialProperty, Entities.LineOfBusiness.CommercialProperty.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.CommercialUmbrella, Entities.LineOfBusiness.CommercialUmbrella.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.Dealer, Entities.LineOfBusiness.Dealer.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.Garage, Entities.LineOfBusiness.Garage.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, Entities.LineOfBusiness.GeneralLiability.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, Entities.LineOfBusiness.InlandMarine.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.OceanMarine, Entities.LineOfBusiness.OceanMarine.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.Package, Entities.LineOfBusiness.Package.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.WorkersComp, Entities.LineOfBusiness.WorkersCompensation.Code);
        }

    }
}
