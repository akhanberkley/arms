﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class AICTransactionalImportBuilder : TransactionalImportBuilder, IImportBuilder
    {
        #region properties
        private CompanyPolicyEntity moPolicyFieldEntity = new CompanyPolicyEntity();
        private CompanyInsuredEntity moInsuredFieldEntity = new CompanyInsuredEntity();
        private CompanyAgencyEntity moAgencyFieldEntity = new CompanyAgencyEntity();
        private CompanyLocationEntity moLocationFieldEntity = new CompanyLocationEntity();
        private CompanyBuildingEntity moBuildingFieldEntity = new CompanyBuildingEntity();
        private CompanyClaimEntity moCompanyClaimEntity = new CompanyClaimEntity();
        private CompanyRateModFactorEntity moCompanyRateModFactorEntity = new CompanyRateModFactorEntity();
        private AICLineOfBusiness moLineOfBusiness = new AICLineOfBusiness();
        #endregion 

        #region Line Of Business / Coverage
        private CompanyCoverageEntity moCoverageFieldEntity = new CompanyCoverageEntity();
        private AICCoverageDictionary moCoverageDictionary = new AICCoverageDictionary();
        #endregion
        
        public AICTransactionalImportBuilder(string policyNumber, CompanyDetail company)
        {
            base.Initialize(this, policyNumber, company);
        }

        #region Members
        public CompanyCoverageEntity CoverageFieldEntity
        {
            get { return moCoverageFieldEntity; }
        }

        // The derived classes can change the field mapping
        public CompanyPolicyEntity PolicyFieldEntity
        {
            get { return moPolicyFieldEntity; }
        }

        // The derived classes can change the field mapping
        public CompanyInsuredEntity InsuredFieldEntity
        {
            get { return moInsuredFieldEntity; }
        }

        public CompanyAgencyEntity AgencyFieldEntity
        {
            get { return moAgencyFieldEntity; }
        }

        public CompanyLocationEntity LocationFieldEntity
        {
            get { return moLocationFieldEntity; }
        }

        public CompanyBuildingEntity BuildingFieldEntity
        {
            get { return moBuildingFieldEntity; }
        }

        public CompanyLineOfBusinessEntity LineOfBusinessFieldEntity
        {
            get { return moLineOfBusiness; }
        }

        public CompanyClaimEntity ClaimFieldEntity
        {
            get { return moCompanyClaimEntity; }
        }

        public CompanyRateModFactorEntity RateModFactorFieldEntity
        {
            get { return moCompanyRateModFactorEntity; }
        }

        public string CompanyAbbreviation
        {
            get { return "AIC"; }
        }
        #endregion

        #region Methods
        public StoredProcedureBO GetStoredProcedure()
        {
            return null;
        }

        public StoredProcedureBO GetStoredProcedure(CompanyStoredProcedureEntity.SpType eType)
        {
            return null;
        }

        public InsuredBO BuildComplete(InsuredBO oInsuredBO)
        {
            // The BlcDataSet has been filled out.  Do the Insert into the BLC DB
            // If all of the company (derived) classes do the same thing, put the basic insert/update
            // in BaseImportBuilder
            Policy oPolicy;
            AICLineOfBusiness oLineOfBusiness = new AICLineOfBusiness();
            CompanyLineOfBusinessEntity.LineOfBusinessType eType;
            List<Policy> oPolicyList = oInsuredBO.PolicyList;

            oPolicyList = oPolicyList.OrderByDescending(pol => pol.PolicyMod).ToList();

            //**************************************************************
            //Only take the most recent policy mod - Hack for P* issue
            List<Policy> filteredPolicies = new List<Policy>();

            foreach (Policy p in oPolicyList)
            {
                if (filteredPolicies == null || filteredPolicies.Where(c => c.PolicyNumber == p.PolicyNumber).Count() == 0)
                {
                    filteredPolicies.Add(p);
                }
                else
                {
                    oInsuredBO.PolicyList.Remove(p);
                }
            }

            if (filteredPolicies.Count > 0)
                oPolicyList = filteredPolicies;
            //***************************************************************

            List<Location> locations = oInsuredBO.PolicyLocationList;
            List<Location> filteredLocation = new List<Location>();

            locations = locations.OrderBy(loc => loc.PolicySymbol).ToList();
            foreach (Location l in locations)
            {
                if ((filteredLocation == null || filteredLocation.Where(c => c.StreetLine1 == l.StreetLine1).Count() == 0) && oPolicyList.Where(p => p.PolicySymbol == l.PolicySymbol).Count() > 0)
                {
                    filteredLocation.Add(l);
                }
                else
                {
                    oInsuredBO.PolicyLocationList.Remove(l);
                }
            }

            //Buildings
            List<BuildingBO> buildings = oInsuredBO.BuildingList;
            List<BuildingBO> filteredBuilding = new List<BuildingBO>();

            buildings = buildings.OrderBy(b => b.LocationId).ToList();
            foreach (BuildingBO b in buildings)
            {
                if ((filteredBuilding == null || filteredBuilding.Where(c => c.Contents == b.Contents && c.BuildingNumber == b.BuildingNumber && c.LocationAsInt == b.LocationAsInt).Count() == 0))
                {
                    filteredBuilding.Add(b);
                }
                else
                {
                    oInsuredBO.BuildingList.Remove(b);
                }
            }

            //Coverages
            List<CoverageBO> coverages = oInsuredBO.CoverageList;
            List<CoverageBO> filteredCoverages = new List<CoverageBO>();

            coverages = coverages.OrderBy(b => b.PolicyMod).ToList();
            //int j = 1;
            foreach (CoverageBO cov in coverages)
            {
                if (filteredPolicies.Where(c => c.PolicyNumber == cov.PolicyNumber && c.PolicyMod.ToString() == cov.PolicyMod).Count() == 0)
                {
                    oInsuredBO.CoverageList.Remove(cov);
                }
            }

            for (int i = 0; i < oPolicyList.Count; i++)
            {
                oPolicy = oPolicyList[i];
                eType = oLineOfBusiness.GetType(oPolicy.PolicySymbol);
                oPolicy.LineOfBusinessCode = oLineOfBusiness.GetBlcName(eType);
                oPolicyList[i] = oPolicy;
            }

            // #TODO: Rectify
            //attempt to get the assigned underwriter from BCS
            //Prospect.ProspectWebService service = new Prospect.ProspectWebService();
            //Underwriter underwriter = service.GetAssignedUnderwriter(base.PolicyNumber, base.BerkleyCompany);
            //oInsuredBO.InsuredEntity.Underwriter = (underwriter != null) ? underwriter.Code : string.Empty;

            return oInsuredBO;
        }
        
        public Agency BuildForAgency(string agencyNumber)
        {
            //Berkley.Import.PolicyStar.APSWebService service = new Berkley.Import.PolicyStar.APSWebService(Company.AcadiaInsuranceCompany.APSEndPointUrl);
            //APSProxy.agency apsAgency = service.GetAgency(agencyNumber);

            //return base.BuildAgency(apsAgency);
            throw new NotImplementedException();
        }

        public List<CoverageColumn> GetCoverage(CompanyLineOfBusinessEntity.LineOfBusinessType eType)
        {
            //throw new NotImplementedException();
            return moCoverageDictionary.GetColumnList(eType);
        }

        public int GetMaxCoverageColumns()
        {
            //throw new NotImplementedException();
            return moCoverageDictionary.GetColumnCount();
        }

        public int GetMaxColumnNumber()
        {
            //throw new NotImplementedException();
            return moCoverageDictionary.GetMaxColumnNumber();
        }
        #endregion


    }
}
