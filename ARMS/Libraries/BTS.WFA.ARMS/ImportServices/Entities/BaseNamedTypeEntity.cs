﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public abstract class BaseNamedTypeEntity : INamedTypeEntity
    {
        private Dictionary<int, string> moDictionary = new Dictionary<int, string>();

        public BaseNamedTypeEntity()
        {
            BuildMap(ref moDictionary);
        }

        /// <summary>
        /// The derived class must implement this function to build up the moDictionary
        /// </summary>
        /// <param name="oColumnMapDictionary"></param>
        protected abstract void BuildMap(ref Dictionary<int, string> oColumnMapDictionary);

        #region INamedTypeEntity Members
        public string this[int iKey]
        {
            get
            {
                return moDictionary[iKey];
            }
            set
            {
                moDictionary[iKey] = value;
            }
        }

        public Dictionary<int, string> NameDictionary
        {
            get { return moDictionary; }
        }
        #endregion
    }

    public interface INamedTypeEntity
    {
        string this[int iKey] { get; set; }
        Dictionary<int, string> NameDictionary { get; }
    }
}
