﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CompanyCoverageEntity : BaseCompanyEntity
    {
        #region Enum
        /// <summary>
        /// Translates the meaning of each column in the policy system's policy
        /// file.
        /// </summary>
        public enum FieldColumns
        {
            ClientId = 0,
            PolicyNumber,
            PolicyMod,
            PolicySymbol
        }
        #endregion

        public CompanyCoverageEntity()
        {
        }

        public bool IsMappedField(int iColumn)
        {
            return base.ColumnMapDictionary.ContainsValue(iColumn);
        }

        protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
        {
            oColumnMapDictionary.Add((int)FieldColumns.ClientId, 0);
            oColumnMapDictionary.Add((int)FieldColumns.PolicyNumber, 1);
            oColumnMapDictionary.Add((int)FieldColumns.PolicyMod, 2);
            oColumnMapDictionary.Add((int)FieldColumns.PolicySymbol, 3);
        }
    }
}
