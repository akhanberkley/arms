﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CompanyClaimEntity : BaseCompanyEntity
    {
        #region Enum
        /// <summary>
        /// Translates the meaning of each column in the policy system's policy
        /// file.
        /// </summary>
        public enum ClaimColumn
        {
            /// <summary>
            /// The policy number to which the diary is applicable.
            /// </summary>
            PolicyNumber = 0,
            /// <summary>
            /// The policy version to which the diary is applicable.
            /// </summary>
            PolicyVersion = 1,
            /// <summary>
            /// The date of the claim.
            /// </summary>
            DateOfLoss = 2,
            Claimant = 3,
            ClaimAmount = 4,
            ClaimComment = 5
        }
        #endregion

        public CompanyClaimEntity()
        {
        }

        protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
        {
            oColumnMapDictionary.Add((int)ClaimColumn.PolicyNumber, 0);
            oColumnMapDictionary.Add((int)ClaimColumn.PolicyVersion, 1);
            oColumnMapDictionary.Add((int)ClaimColumn.DateOfLoss, 2);
            oColumnMapDictionary.Add((int)ClaimColumn.Claimant, 3);
            oColumnMapDictionary.Add((int)ClaimColumn.ClaimAmount, 4);
            oColumnMapDictionary.Add((int)ClaimColumn.ClaimComment, 5);
        }
    }
}
