﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CompanyPolicyEntity : BaseCompanyEntity
    {
        #region Enum
        /// <summary>
        /// Translates the meaning of each column in the policy system's policy
        /// file.
        /// </summary>
        public enum PolicyColumn
        {
            Undefined = -1,
            /// <summary>
            /// The client identifier from the policy system.
            /// </summary>
            ClientID = 0,
            /// <summary>
            /// The policy number.
            /// </summary>
            PolicyNumber,
            /// <summary>
            /// The policy version.
            /// </summary>
            PolicyMod,
            /// <summary>
            /// The policy symbol.
            /// </summary>
            PolicySymbol,
            /// <summary>
            /// The effective date of the policy.
            /// </summary>
            EffectiveDate,
            /// <summary>
            /// The expiration date of the policy.
            /// </summary>
            ExpirationDate,
            /// <summary>
            /// The premium amount.
            /// </summary>
            PremiumAmount,
            /// <summary>
            /// The branch code.
            /// </summary>
            BranchCode,
            /// <summary>
            /// The hazard grade.
            /// </summary>
            HazardGrade,
            /// <summary>
            /// The insurance carrier.
            /// </summary>
            Carrier,
            /// <summary>
            /// The profit center.
            /// </summary>
            ProfitCenter,
            /// <summary>
            /// The policy status.
            /// </summary>
            PolicyStatus
        }
        #endregion

        public CompanyPolicyEntity()
        {
        }

        protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
        {
            oColumnMapDictionary.Add((int)PolicyColumn.ClientID, 0);
            oColumnMapDictionary.Add((int)PolicyColumn.PolicyNumber, 1);
            oColumnMapDictionary.Add((int)PolicyColumn.PolicyMod, 2);
            oColumnMapDictionary.Add((int)PolicyColumn.PolicySymbol, 3);
            oColumnMapDictionary.Add((int)PolicyColumn.EffectiveDate, 4);
            oColumnMapDictionary.Add((int)PolicyColumn.ExpirationDate, 5);
            oColumnMapDictionary.Add((int)PolicyColumn.PremiumAmount, 6);
            oColumnMapDictionary.Add((int)PolicyColumn.BranchCode, 7);
            oColumnMapDictionary.Add((int)PolicyColumn.HazardGrade, 8);
            oColumnMapDictionary.Add((int)PolicyColumn.Carrier, 9);
            oColumnMapDictionary.Add((int)PolicyColumn.ProfitCenter, 10);
            oColumnMapDictionary.Add((int)PolicyColumn.PolicyStatus, 11);
        }
    }
}
