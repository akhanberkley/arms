﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CompanyRateModFactorEntity : BaseCompanyEntity
    {
        #region Enum
        /// <summary>
        /// Translates the meaning of each column in the policy system's policy
        /// file.
        /// </summary>
        public enum RateModFactorColumn
        {
            /// <summary>
            /// The client id of the insured.
            /// </summary>
            ClientID = 0,
            /// <summary>
            /// The policy number to which the rate mod is applicable.
            /// </summary>
            PolicyNumber = 1,
            /// <summary>
            /// The policy version to which the rate mod is applicable.
            /// </summary>
            PolicyVersion = 2,
            /// <summary>
            /// The date of the claim.
            /// </summary>
            State = 3,
            Description = 4,
            Rate = 5
        }
        #endregion

        public CompanyRateModFactorEntity()
        {
        }

        protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
        {
            oColumnMapDictionary.Add((int)RateModFactorColumn.ClientID, 0);
            oColumnMapDictionary.Add((int)RateModFactorColumn.PolicyNumber, 1);
            oColumnMapDictionary.Add((int)RateModFactorColumn.PolicyVersion, 2);
            oColumnMapDictionary.Add((int)RateModFactorColumn.State, 3);
            oColumnMapDictionary.Add((int)RateModFactorColumn.Description, 4);
            oColumnMapDictionary.Add((int)RateModFactorColumn.Rate, 5);
        }
    }
}
