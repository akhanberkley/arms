﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CompanyLocationEntity : BaseCompanyEntity
    {
        #region Enum
        /// <summary>
        /// Translates the meaning of each column in the policy system's policy
        /// file.
        /// </summary>
        public enum LocationColumn
        {
            Unknown = -1,
            ClientId = 0,
            PolicySystemKey,
            LocationId,
            LocationAddress,
            LocationCity,
            LocationState,
            LocationZip
        }
        #endregion

        public CompanyLocationEntity()
        {
        }

        protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
        {
            oColumnMapDictionary.Add((int)LocationColumn.ClientId, 0);
            oColumnMapDictionary.Add((int)LocationColumn.PolicySystemKey, 1);
            oColumnMapDictionary.Add((int)LocationColumn.LocationId, 2);
            oColumnMapDictionary.Add((int)LocationColumn.LocationAddress, 3);
            oColumnMapDictionary.Add((int)LocationColumn.LocationCity, 4);
            oColumnMapDictionary.Add((int)LocationColumn.LocationState, 5);
            oColumnMapDictionary.Add((int)LocationColumn.LocationZip, 6);
        }
    }
}
