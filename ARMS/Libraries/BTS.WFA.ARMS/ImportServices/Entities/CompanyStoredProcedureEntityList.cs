﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    /// <summary>
    /// Contains a List of Stored Procedures specific for a Company
    /// The definition of the list is held in the Config file in XML
    /// CompanySpEntityList also holds the Connection String and KEY for the DATA to Import from the Company DB 
    /// </summary>
    public class CompanyStoredProcedureEntityList : List<CompanyStoredProcedureEntity>
    {
        private string mstrConnectionString;
        private string mstrClientId;
        private string mstrPolicyNumber;
        private string mstrAgentNumber;

        public CompanyStoredProcedureEntityList(string strConnectionString, string strClientId, string strPolicyNumber, string strAgentNumber)
        {
            mstrConnectionString = strConnectionString;
            mstrClientId = strClientId;
            mstrPolicyNumber = strPolicyNumber;
            mstrAgentNumber = strAgentNumber;
        }

        public void Load(string strConfigSection)
        {
            //NameValueCollection oRows = new BerkleyXmlKeyValueCollection(strConfigSection).KeyValues;
            //foreach (string strKey in oRows.Keys) 
            //{
            //    this.Add(new CompanySpEntity(strKey, oRows[strKey], mstrConnectionString, mstrClientId, mstrPolicyNumber, mstrAgentNumber));
            //}
        }
    }
}
