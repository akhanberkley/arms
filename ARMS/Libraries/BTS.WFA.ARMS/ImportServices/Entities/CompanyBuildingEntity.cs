﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CompanyBuildingEntity : BaseCompanyEntity
    {
        #region Enum
        /// <summary>
        /// Translates the meaning of each column in the policy system's policy
        /// file.
        /// </summary>
        public enum BuildingColumn
        {
            Unknown = -1,
            ClientId = 0,
            PolicySystemKey,
            LocationId,
            BuildingNumber,
            YearBuilt,
            SprinklerSystem,
            PublicProtection,
            LocationOccupancy,
            StockValues,
            BuildingContents,
            BuildingValue,
            BusinessInterruption,
            CoinsurancePercentage,
            BuildingValuationType
        }
        #endregion

        public CompanyBuildingEntity()
        {
        }

        protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
        {
            int iColumn = 0;
            oColumnMapDictionary.Add((int)BuildingColumn.ClientId, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.PolicySystemKey, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.LocationId, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.BuildingNumber, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.YearBuilt, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.SprinklerSystem, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.PublicProtection, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.LocationOccupancy, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.StockValues, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.BuildingContents, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.BuildingValue, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.BusinessInterruption, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.CoinsurancePercentage, iColumn++);
            oColumnMapDictionary.Add((int)BuildingColumn.BuildingValuationType, iColumn++);
        }
    }
}
