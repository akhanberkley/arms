﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public abstract class CompanyLineOfBusinessEntity : BaseNamedTypeEntity
    {
        #region Enum
        // Define every line of business here
        // Add the Text that represents the LOB in the BuildMap() function
        // The Text will be found in the COVERAGE "Policy Symbol" column
        public enum LineOfBusinessType
        {
            Unknown,
            BusinessOwner,
            CommercialAuto,
            CommercialCrime,
            CommercialFire,
            CommercialOutputPolicy,
            CommercialProperty,
            CommercialUmbrella,
            Dealer,
            Garage,
            GeneralLiability,
            InlandMarine,
            OceanMarine,
            Package,
            WorkersComp,
            DirectorsAndOfficers,
            Transportation
        }
        #endregion

        protected Dictionary<LineOfBusinessType, string> moBlcLobDictionary = new Dictionary<LineOfBusinessType, string>();

        public CompanyLineOfBusinessEntity()
        {
        }

        public string[] GetAllLobNames()
        {
            string[] rstrNames;
            List<string> oReturn = new List<string>();

            foreach (LineOfBusinessType eIteratedType in base.NameDictionary.Keys)
            {
                rstrNames = base.NameDictionary[(int)eIteratedType].Split('|');
                oReturn.Add(rstrNames[0]);
            }
            return oReturn.ToArray();
        }

        // Search the Company map for the Line of Business
        public LineOfBusinessType GetType(string strLineOfBusiness)
        {
            string[] rstrNames;

            strLineOfBusiness = strLineOfBusiness.ToUpper();
            foreach (LineOfBusinessType eIteratedType in base.NameDictionary.Keys)
            {
                rstrNames = base.NameDictionary[(int)eIteratedType].Split('|');
                foreach (string strName in rstrNames)
                {
                    // FIX HERE!
                    if (strName == strLineOfBusiness)
                    {
                        return eIteratedType;
                    }
                }
            }
            throw new Exception("Unknown Line of Business from policy system : " + strLineOfBusiness);
        }

        public LineOfBusinessType GetTypeFromBlcCode(string strLineOfBusiness)
        {
            LineOfBusinessType eType = LineOfBusinessType.Unknown;
            string[] rstrNames;

            strLineOfBusiness = strLineOfBusiness.ToUpper();
            foreach (LineOfBusinessType eIteratedType in base.NameDictionary.Keys)
            {
                rstrNames = moBlcLobDictionary[eIteratedType].Split('|');
                foreach (string strName in rstrNames)
                {
                    if (strName == strLineOfBusiness)
                    {
                        eType = eIteratedType;
                        break;
                    }
                }
            }
            return eType;
        }

        public string GetBlcName(LineOfBusinessType eType)
        {
            string[] rstrLob = moBlcLobDictionary[eType].Split('|');
            return rstrLob[0];
        }

        public string GetCompanyLob(string strBlcLobName)
        {
            strBlcLobName = strBlcLobName.ToUpper();

            foreach (LineOfBusinessType eIteratedType in moBlcLobDictionary.Keys)
            {
                if (strBlcLobName == moBlcLobDictionary[eIteratedType])
                {
                    string strReturn = base.NameDictionary[(int)eIteratedType];
                    string[] rstrParts = strReturn.Split('|');
                    return rstrParts[0];
                }
            }
            return "";
        }

        public string GetCompanyPolicySymbols(Guid companyId, string lineOfBusinessCode)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.CompanyLineOfBusinesses.Where(l => l.CompanyId == companyId && l.LineOfBusinessCode == lineOfBusinessCode).Select(p => p.PolicySymbols).SingleOrDefault();
            }
        }

        protected override void BuildMap(ref Dictionary<int, string> oColumnMapDictionary)
        {
            DerivedBuildMap(ref oColumnMapDictionary);
        }

        protected abstract void DerivedBuildMap(ref Dictionary<int, string> oColumnMapDictionary);
    }
}
