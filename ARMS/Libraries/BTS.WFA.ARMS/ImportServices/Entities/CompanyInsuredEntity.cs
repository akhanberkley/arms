﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CompanyInsuredEntity : BaseCompanyEntity
    {
        #region Enum
        /// <summary>
        /// Translates the meaning of each column in the policy system's policy
        /// file.
        /// </summary>
        public enum InsuredColumn
        {
            Undefined = -1,
            /// <summary>
            /// The client identifier from the policy system.
            /// </summary>
            ClientID = 0,
            /// <summary>
            /// The agency number related to the insured.
            /// </summary>
            AgencyNumber,
            /// <summary>
            /// The name of the insured.
            /// </summary>
            InsuredName1,
            InsuredName2,
            /// <summary>
            /// The street address of the insured.
            /// </summary>
            StreetAddress1,
            StreetAddress2,
            StreetAddress3,
            /// <summary>
            /// The city of the insured.
            /// </summary>
            City,
            /// <summary>
            /// The state of the insured.
            /// </summary>
            State,
            /// <summary>
            /// The zip code of the insured.
            /// </summary>
            ZipCode,
            /// <summary>
            /// The insured's business operations.
            /// </summary>
            BusinessOperations,
            /// <summary>
            /// The SIC Code of the business type of the insured.
            /// </summary>
            SICCode,
            /// <summary>
            /// The owner's name of the insured.
            /// </summary>
            OwnerName,
            /// <summary>
            /// The owner's phone number.
            /// </summary>
            OwnerPhone,
            /// <summary>
            /// The assigned underwriter.
            /// </summary>
            Underwriter,
            /// <summary>
            /// The business category.
            /// </summary>
            BusinessCategory,
            /// <summary>
            /// The NAICS code.
            /// </summary>
            NAICSCode,
        }
        #endregion

        public CompanyInsuredEntity()
        {
        }

        protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
        {
            oColumnMapDictionary.Add((int)InsuredColumn.ClientID, 0);
            oColumnMapDictionary.Add((int)InsuredColumn.AgencyNumber, 1);
            oColumnMapDictionary.Add((int)InsuredColumn.InsuredName1, 2);
            oColumnMapDictionary.Add((int)InsuredColumn.InsuredName2, 3);
            oColumnMapDictionary.Add((int)InsuredColumn.StreetAddress1, 4);
            oColumnMapDictionary.Add((int)InsuredColumn.StreetAddress2, 5);
            oColumnMapDictionary.Add((int)InsuredColumn.StreetAddress3, 6);
            oColumnMapDictionary.Add((int)InsuredColumn.City, 7);
            oColumnMapDictionary.Add((int)InsuredColumn.State, 8);
            oColumnMapDictionary.Add((int)InsuredColumn.ZipCode, 9);
            oColumnMapDictionary.Add((int)InsuredColumn.BusinessOperations, 10);
            oColumnMapDictionary.Add((int)InsuredColumn.SICCode, 11);
            oColumnMapDictionary.Add((int)InsuredColumn.OwnerName, 12);
            oColumnMapDictionary.Add((int)InsuredColumn.OwnerPhone, 13);
            oColumnMapDictionary.Add((int)InsuredColumn.Underwriter, 14);
            oColumnMapDictionary.Add((int)InsuredColumn.BusinessCategory, 15);
            oColumnMapDictionary.Add((int)InsuredColumn.NAICSCode, 16);
        }
    }
}
