﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class PolicyStarCoverageValueType : BaseNamedTypeEntity
    {
        #region Enum
        // Information for the Reconcile - information for the ARMS DB from policy star
        public enum Enum
        {
            NONE,
            PROD,
            CGL_AGG,
            CGL_OCCR,
            EA_OCCR,
            GENL_AGG,
            LIAB_OCCR,
            LIAB_AGG,
            NOWN_LIAB,
            HRD_LIAB,
            PROD_CPT,
            CRGO_SCHED,
            PROP_VEH,
            CSE_CONS,
            JSITE_SCHD,
            FROM_OTH,
            PRMS_RENT,
            TO_OTH,
            EMP_TOOL,
            YOUR_TOOL,
            HDWR_SCHED,
            SIGN,
            SIGN_USCHD,
            LIAB,
            COMP,
            TE,
            BINC_DPND,
            DN_CATLMT,
            TOT_VH_IN_FLEET_LB,
            CGL1,
            CONTR_CAT,
            NEW_EQUIP,
            CRGO_CAT,
            EDP_HDWR,
            BLDRS_CAT,
            TRNP,
            TRNP_SCHED,
            INST_CAT,
            INST_JSITE,
            TRNS_IN,
            TEMP_STO,
            PROF_OCCR,
            PROF_AGG,
            ABSE_OCCR,
            ABSE_AGG,
            EMP_THFT_B,
            EQUIPMENT
        }
        #endregion

        public PolicyStarCoverageValueType()
        {
        }
        protected override void BuildMap(ref Dictionary<int, string> oDictionary)
        {
            oDictionary.Add((int)Enum.PROD, "PROD");
            oDictionary.Add((int)Enum.CGL_AGG, "CGL_AGG");
            oDictionary.Add((int)Enum.CGL_OCCR, "CGL_OCCR");
            oDictionary.Add((int)Enum.EA_OCCR, "EA_OCCR");
            oDictionary.Add((int)Enum.GENL_AGG, "GENL_AGG");
            oDictionary.Add((int)Enum.LIAB_OCCR, "LIAB_OCCR");
            oDictionary.Add((int)Enum.LIAB_AGG, "LIAB_AGG");
            oDictionary.Add((int)Enum.NONE, "");
            oDictionary.Add((int)Enum.NOWN_LIAB, "NOWN_LIAB");
            oDictionary.Add((int)Enum.HRD_LIAB, "HRD_LIAB");
            oDictionary.Add((int)Enum.PROD_CPT, "PROD_CPT");
            oDictionary.Add((int)Enum.CRGO_SCHED, "CRGO_SCHED");
            oDictionary.Add((int)Enum.PROP_VEH, "PROP_VEH");
            oDictionary.Add((int)Enum.CSE_CONS, "CSE_CONS");
            oDictionary.Add((int)Enum.JSITE_SCHD, "JSITE_SCHD");
            oDictionary.Add((int)Enum.FROM_OTH, "FROM_OTH");
            oDictionary.Add((int)Enum.PRMS_RENT, "PRMS_RENT");
            oDictionary.Add((int)Enum.TO_OTH, "TO_OTH");
            oDictionary.Add((int)Enum.EMP_TOOL, "EMP_TOOL");
            oDictionary.Add((int)Enum.YOUR_TOOL, "YOUR_TOOL");
            oDictionary.Add((int)Enum.HDWR_SCHED, "HDWR_SCHED");
            oDictionary.Add((int)Enum.SIGN, "SIGN");
            oDictionary.Add((int)Enum.SIGN_USCHD, "SIGN_USCHD");
            oDictionary.Add((int)Enum.LIAB, "LIAB");
            oDictionary.Add((int)Enum.COMP, "COMP");
            oDictionary.Add((int)Enum.TE, "TE");
            oDictionary.Add((int)Enum.BINC_DPND, "BINC_DPND");
            oDictionary.Add((int)Enum.DN_CATLMT, "DN_CATLMT");
            oDictionary.Add((int)Enum.TOT_VH_IN_FLEET_LB, "TOT_VH_IN_FLEET_LB");
            oDictionary.Add((int)Enum.CGL1, "CGL1");
            oDictionary.Add((int)Enum.CONTR_CAT, "CONTR_CAT");
            oDictionary.Add((int)Enum.NEW_EQUIP, "NEW_EQUIP");
            oDictionary.Add((int)Enum.CRGO_CAT, "CRGO_CAT");
            oDictionary.Add((int)Enum.EDP_HDWR, "EDP_HDWR");
            oDictionary.Add((int)Enum.BLDRS_CAT, "BLDRS_CAT");
            oDictionary.Add((int)Enum.TRNP, "TRNP");
            oDictionary.Add((int)Enum.TRNP_SCHED, "TRNP_SCHED");
            oDictionary.Add((int)Enum.INST_CAT, "INST_CAT");
            oDictionary.Add((int)Enum.INST_JSITE, "INST_JSITE");
            oDictionary.Add((int)Enum.TRNS_IN, "TRNS_IN");
            oDictionary.Add((int)Enum.TEMP_STO, "TEMP_STO");
            oDictionary.Add((int)Enum.PROF_OCCR, "PROF_OCCR");
            oDictionary.Add((int)Enum.PROF_AGG, "PROF_AGG");
            oDictionary.Add((int)Enum.ABSE_OCCR, "ABSE_OCCR");
            oDictionary.Add((int)Enum.ABSE_AGG, "ABSE_AGG");
            oDictionary.Add((int)Enum.EMP_THFT_B, "EMP_THFT_B");
            oDictionary.Add((int)Enum.EQUIPMENT, "EQUIPMENT");
        }
    }
}
