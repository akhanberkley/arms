﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CoverageNameKey
    {
        private string mstrKey = "";
        private static string mstrDelimiter = "^";

        public CoverageNameKey(CoverageBO oCoverage)
        {
            if (oCoverage.SubLineOfBusinessAsString.Length > 0)
            {
                StringBuilder oBuilder = new StringBuilder();
                oBuilder.Append(oCoverage.LineOfBusinessAsString.Trim());
                oBuilder.Append(mstrDelimiter);
                oBuilder.Append(oCoverage.SubLineOfBusinessAsString.Trim());
                oBuilder.Append(mstrDelimiter);
                oBuilder.Append(oCoverage.CoverageNameType.Trim());
                mstrKey = oBuilder.ToString();
            }
            else
            {
                StringBuilder oBuilder = new StringBuilder();
                oBuilder.Append(oCoverage.LineOfBusinessAsString.Trim());
                oBuilder.Append(mstrDelimiter);
                oBuilder.Append(oCoverage.CoverageNameType.Trim());
                mstrKey = oBuilder.ToString();
            }
        }

        public CoverageNameKey(string strLineOfBusiness, string strSubLineOfBusiness, string strNameType)
        {
            StringBuilder oBuilder = new StringBuilder();
            oBuilder.Append(strLineOfBusiness.Trim());
            oBuilder.Append(mstrDelimiter);
            oBuilder.Append(strSubLineOfBusiness.Trim());
            oBuilder.Append(mstrDelimiter);
            oBuilder.Append(strNameType.Trim());
            mstrKey = oBuilder.ToString();
        }

        public CoverageNameKey(string strLineOfBusiness, string strNameType)
        {
            StringBuilder oBuilder = new StringBuilder();
            oBuilder.Append(strLineOfBusiness.Trim());
            oBuilder.Append(mstrDelimiter);
            oBuilder.Append(strNameType.Trim());
            mstrKey = oBuilder.ToString();
        }

        public string Key
        {
            get { return mstrKey; }
        }
    }
}
