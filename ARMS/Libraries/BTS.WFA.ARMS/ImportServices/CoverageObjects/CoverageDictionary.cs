﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CoverageDictionary
    {
        #region Coverage Map Enum
        public enum CoverageMap
        {
            // Business Owner --
            BoGeneralLiabilityOccurrenceLimits,
            BoGeneralLiabilityAggregateLimits,
            BoBusinessIncomeLimits,
            BoBusinessIncomeFromDependentProperties,

            // Workers Comp --
            WcExposure,

            // Commercial Umbrella
            CuOccurrenceLimits,
            CuAggregateLimits,

            // Commercial Output --			
            CoBuildingLimit,
            CoBuildingCatastropheLimit,
            CoPersonalPropertyLimit,

            // Commercial Auto --
            CaBodilyInjuryLimit,
            CaPhysicalDamageLimit,
            CaVehicleCount,
            CaNonOwnerBodilyInjuryLimit,
            CaNonOwnerPhysicalDamageLimit,
            CaNonOwnerAutoLiability,
            CaGaragekeepersLimits,
            CaNonownedHiredAutoLimits,
            CaTractors,
            CaTrailers,
            CaServiceUnits,
            CaCommercialUnits,
            CaPPTsExHeavy,
            CaPPTsHeavy,
            CaPPTsMedium,
            CaPPTsLight,
            CaActivePlates,
            CaInactivePlates,

            // Commercial Crime --
            CcInsidePremisesTheftLimits,
            CcInsidePremisesRobberyLimits,
            CcOutsidePremisesRobberyLimits,
            CcTheftPerEmployeeLimits,
            CcForgeryAlterationLimits,
            CcClientsPropertyLimits,
            CcFundsTransferFraudLimits,
            CcComputerFraudLimits,

            // Garage --
            GaComprehensiveDeductable,
            GaComprehensiveLimits,
            GaSpecialCausesOfLossDeductable,
            GaSpecialCausesOfLossLimits,
            GaCollisionDeductable,
            GaCollisionLimits,
            GaDealerClassOfOpRegularOPFurnishedAutos,
            GaDealerClassOfOpRegularOPNonFurnishedAutos,
            GaDealerClassOfOpEmployeesNonRegularOp,
            GaDealerClassOfOpNonEmployeesUnder25,
            GaDealerClassOfOpNonEmployeesOver25,
            GaDealerPhysicalDamageComprehensiveLimits,
            GaDealerPhysicalDamageCollisionLimits,
            GaDealerDriveawayCollisionVehicleCount,
            GaDealerDriveawayCollisionTripCount,
            GaDealerDriveawayCollisionPriceNew,
            GaDealerDriveawayCollisionMileage,
            GaDealerDriveawayCollisionDeductable,
            GaGaragekeepersLimits,

            // General Liability --
            GlGeneralLiabilityOccurrenceLimits,
            GlGeneralLiabilityAggregateLimits,
            GlGeneralLiabilityExposures,
            GlProductsOperationsLimits,
            GlFRPLiabilityLimits,
            GlNonOwnerBodilyInjuryLimit,
            GlNonOwnerPhysicalDamageLimit,
            GlProfessionalLiabilityOccurrenceLimits,
            GlProfessionalLiabilityAggregateLimits,
            GlSexualAbuseOccuranceLimits,
            GlSexualAbuseAggregateLimits,

            // Commerical Property --
            CpBusinessIncomeLimits,

            // Inland Marine --
            ImMotorTruckCargoLegalLiabilityLimits,
            ImMotorTruckCargoVehicleLimits,
            ImMotorTruckCargoTerminalCoverage,
            ImMotorTruckCargoTerminalLimits,
            ImBuildersRiskConstructionLimits,
            ImBuildersRiskExistingBuildingLimits,
            ImWarehouseLegalLiabilitySpoilageLimits,
            ImWarehouseLegalLiabilityColdStorageLimits,
            ImJewelersBlockPremisesLimits,
            ImContractorsEquipmentOwnedEquipment,
            ImContractorsEquipmentToOthersLimits,
            ImContractorsEquipmentFromOthersLimits,
            ImToolFloaterLimits,
            ImToolFloaterEmployeeToolsLimits,
            ImComputerHardwareLimits,
            ImComputerSoftwareMediaLimits,
            ImSignsScheduledLimits,
            ImSignsUnscheduledLimits,
            ImAccountsReceivableInsuredPremises1,
            ImAccountsReceivableInsuredPremises2,
            ImValuablePapersProperty1,
            ImValuablePapersProperty2,
            ImValuablePapersProperty3,
            ImValuablePapersAllOtherCoveredProperty,
            ImFineArtsFloaterOnPremises,
            ImFineArtsFloaterTransportation,
            ImBaileesCoveragePremises,
            ImBaileesCoverageTransit,
            ImTransitCoverageHiredCarrier,
            ImTransitCoverageOwnedVehicle,
            ImTransitTransportationLimit,
            ImTransitTransportationScheduleLimit,
            ImInstallFloaterInstallationCatostrophic,
            ImInstallFloaterJobsiteSchedule,
            ImInstallFloaterInTransit,
            ImInstallFloaterTemporaryLocations,
            ImMobileEquipmentLimits,
            ImEDPEquipmentHardwareLimits,
            ImEDPEquipmentSoftwareLimits,
            ImEquipment,

            // Ocean Marine
            OmPhysicalDamageVesselLimits,
            OmPhysicalDamageExcessCollisionLimits,
            OmPhysicalDamageBreachOfWarrantyLimits,
            OmLegalLiabilityLimits,
            OmLegalLiabilityMedicalPaymentsLimits,
            OmBoatDealersCoverageDLimits,
            OmBoatDealersCoverageALimits,
            OmBuildersRiskAmountInsured,
            OmBuildersRiskAgreedValue,
        }
        #endregion

        protected Dictionary<CoverageMap, int> moCoverageDictionary = new Dictionary<CoverageMap, int>();
        private Dictionary<CompanyLineOfBusinessEntity.LineOfBusinessType, List<CoverageColumn>> moLineOfBusinessDictionary = new Dictionary<CompanyLineOfBusinessEntity.LineOfBusinessType, List<CoverageColumn>>();

        public CoverageDictionary()
        {
            BuildCoverageMap();
            BuildColumns();
        }

        public int GetColumnCount()
        {
            return moCoverageDictionary.Count;
        }

        public virtual int GetMaxColumnNumber()
        {
            return 0;
        }

        public List<CoverageColumn> GetColumnList(CompanyLineOfBusinessEntity.LineOfBusinessType eLineOfBusiness)
        {
            return moLineOfBusinessDictionary[eLineOfBusiness];
        }

        protected virtual void BuildColumns()
        {
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.BusinessOwner, CreateBusinessOwners());
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialAuto, CreateCommercialAuto());
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialCrime, CreateCommercialCrime());
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialOutputPolicy, CreateCommercialOutput());
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialProperty, CreateCommercialProperty());
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.CommercialUmbrella, CreateCommercialUmbrella());
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.Garage, CreateGarage());
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.GeneralLiability, CreateGeneralLiability());
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.InlandMarine, CreateInlandMarine());
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.Package, CreatePackages());
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.WorkersComp, CreateWorkersComp());
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.OceanMarine, CreateOceanMarine());
            moLineOfBusinessDictionary.Add(CompanyLineOfBusinessEntity.LineOfBusinessType.Transportation, CreateTransporation());
        }

        #region Business Owners
        protected virtual List<CoverageColumn> CreateBusinessOwners()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region Workers Comp
        protected virtual List<CoverageColumn> CreateWorkersComp()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region Commercial Output
        protected virtual List<CoverageColumn> CreateCommercialOutput()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region Commercial Auto
        protected virtual List<CoverageColumn> CreateCommercialAuto()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region Commercial Crime
        protected virtual List<CoverageColumn> CreateCommercialCrime()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region Commercial Umbrella
        protected virtual List<CoverageColumn> CreateCommercialUmbrella()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region Garage
        protected virtual List<CoverageColumn> CreateGarage()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region General Liablility
        protected virtual List<CoverageColumn> CreateGeneralLiability()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region Commercial Property
        protected virtual List<CoverageColumn> CreateCommercialProperty()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region Transporation
        protected virtual List<CoverageColumn> CreateTransporation()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region Inland Marine
        protected virtual List<CoverageColumn> CreateInlandMarine()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region Ocean Marine
        protected virtual List<CoverageColumn> CreateOceanMarine()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region Packages
        protected virtual List<CoverageColumn> CreatePackages()
        {
            return new List<CoverageColumn>();
        }
        #endregion

        #region Build Coverage MAP
        protected virtual void BuildCoverageMap()
        {
            //this section is company specific
        }
        #endregion
    }
}
