﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CoverageNameDictionary
    {
        private Dictionary<string, Guid> moCoverageDict = new Dictionary<string,Guid>();
        private IArmsDb moDb;

        public CoverageNameDictionary()
        {
        }

        public void Clear()
        {
            moCoverageDict.Clear();
        }

        public Guid GetId(CoverageBO oCoverage)
        {
            string strKey = new CoverageNameKey(oCoverage).Key;
            if (moCoverageDict.ContainsKey(strKey))
            {
                return moCoverageDict[strKey];
            }
            else
            {
                throw new Exception("CoverageName not found" + oCoverage.ToString());
            }
        }

        public void Load(CompanyDetail oCompany)
        {
            using (moDb = Application.GetDatabaseInstance())
            {
                CoverageName[] roCoverageNames = moDb.CoverageNames.Where(cn => cn.CompanyId == oCompany.CompanyId).ToArray();
                foreach (CoverageName oCoverageName in roCoverageNames)
                {
                    if (!string.IsNullOrWhiteSpace(oCoverageName.SubLineOfBusinessCode) && oCoverageName.SubLineOfBusinessCode.Length > 0)
                    {
                        moCoverageDict.Add(new CoverageNameKey(oCoverageName.LineOfBusinessCode, oCoverageName.SubLineOfBusinessCode, oCoverageName.CoverageNameTypeCode).Key, oCoverageName.CoverageNameId);
                    }
                    else
                    {
                        moCoverageDict.Add(new CoverageNameKey(oCoverageName.LineOfBusinessCode, oCoverageName.CoverageNameTypeCode).Key, oCoverageName.CoverageNameId);
                    }

                }
            }
        }
    }
}
