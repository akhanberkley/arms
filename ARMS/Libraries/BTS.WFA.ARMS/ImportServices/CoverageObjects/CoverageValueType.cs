﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    /// <summary>
    /// The values in the Dictionary map back to entries
    /// in the CoverageValueType BLC DB Table
    /// </summary>
    public class CoverageValueType : BaseNamedTypeEntity
    {
        #region Enum
        // Information for the Reconcile - information for the BLC DB
        public enum Enum
        {
            //Limits
            Limits,					//LI	Limits
            AggregateLimits,		//AL	Aggregate Limits
            OccuranceLimits,		//OL	Occurance Limits
            HardwareLimits,			//HL	Hardware Limits
            ScheduledLimits,		//SC	Scheduled Limits
            UnscheduledLimits,		//US	Unsheduled Limits
            CatastropheLimits,		//CL	Catastrophe Limits 
            LegalLiabilityLimits,	//LL	Legal Liability Limits
            VehicleLimits,			//VL	Vehicle Limits
            ConstructionLimits,		//CO	Construction Limits
            ExistingBuildingLimits,	//EB	Existing Building Limits
            SpoilageLimits,			//SL	Spoiliage Limits
            ColdStorageLimits,		//CS	Cold Storage Limits
            YourPremisesLimits,		//YP	Your Premises Limits
            ExposureLimits,			//EL	Exposure Limits
            BlanketLimits,			//BL	Blanket Limits
            ProductsOperationsLimits,//PO	Products/Operations Limits
            BodilyInjuryLimit,      //BI
            PhysicalDamageLimit,    //PD
            NumberOfVehicles,       //NU
            Payroll,				//PR	Payroll
            Values,					//VA	Values
            Liability,				//LB	Liability
            ClassCodePayrollEmployees,//CC	Code
            Contents,				//CT	Contents
            Exposures,				//EX	Exposures
            FromDependentProperties,//DP	From Dependent Properties
            ToOthers,				//TO	To Others
            FromOthers,				//FO	From Others
            YourTools,				//YT	Your Tools
            EmployeeTools,			//ET	Employee Tools 
            InsideThePremesisRobberyOfMoneyAndSecurites,	//IP	Inside the Premises Robbery of Money & Securities
            OutsideThePremesisRobberyOfMoneyAndSecurites,	//OP	Outside the Premeses Robbery of Money & Securities
            InsideThePremesisTheftOfMoneyAndSecurites,	    //IT	Inside the Premises Theft of Money & Securities
            TheftPerEmployee,			                    //TE	Theft Per Employee
            VesselLimits,           //VE
            ExcessCollisionLimits,  //EC
            BreachofWarrantyLimits, //BW
            LiabilityLimits,        //LE
            MedicalPaymentsLimits,  //MP
            CoverageDLimits,         //CV
            CoverageALimits,         //CA
            AmountInsured,          //AI
            AgreedValue,            //AV
            RegularOpFurnishedAutos,    //IR
            RegularOpNonFurnishedAutos, //IN
            EmployeesNonRegularOp,      //IF
            NonEmployeesUnder25,        //IU
            NonEmployeesOver25,         //IO
            ComprehensiveLimits,        //CP
            CollisionLimits,            //CN
            NumberOfTrips,              //NT
            PriceNew,                   //PN
            Mileage,                    //ML
            Deductable,                 //DD
            OwnedEquipment,             //OE
            ForgeryAlteration,          //FA
            ClientsProperty,            //CZ
            FundsTransferFraud,         //FT
            ComputerFraud,              //CF
            InsuredPremises1,           //I1
            InsuredPremises2,           //I2
            Property1,                  //P1
            Property2,                  //P2
            Property3,                  //P3
            AllOtherCoveredProperty,    //AC
            OnPremises,                 //OR
            Transportation,             //TR
            TransportationLimit,        //TR
            TransportationScheduleLimit, //TR
            Premises,                   //PT
            Transit,                    //TL
            HiredCarrier,               //HC
            OwnedVehicle,               //OV
            SoftwareMedia,              //SF
            Garagekeeper,               //GA
            InstallationCatostrophic,   //IC
            JobsiteSchedule,            //JS
            InTransit,                  //IT
            TemporaryLocations,         //TL
            Tractors,                   //TA
            Trailers,                   //TZ
            ServiceUnits,               //SU
            CommercialUnits,            //CM
            PPTsExHeavy,                //PX
            PPTsHeavy,                  //PH
            PPTsMedium,                 //PM
            PPTsLight,                  //PL
            ActivePlates,               //AP
            InactivePlates,             //TP
            TerminalCoverage,           //TC
            TerminalLimit,              //TM
            NonownedHiredAutoLimit,     //NO
            Equipment,                  //Inland Marine Equipment (EQ)
        }
        #endregion

        public CoverageValueType()
        {
        }

        protected override void BuildMap(ref Dictionary<int, string> oDictionary)
        {
            oDictionary.Add((int)Enum.Limits, "LI");
            oDictionary.Add((int)Enum.AggregateLimits, "AL");
            oDictionary.Add((int)Enum.OccuranceLimits, "OL");
            oDictionary.Add((int)Enum.HardwareLimits, "HL");
            oDictionary.Add((int)Enum.ScheduledLimits, "SC");
            oDictionary.Add((int)Enum.UnscheduledLimits, "US");
            oDictionary.Add((int)Enum.CatastropheLimits, "CL");
            oDictionary.Add((int)Enum.LegalLiabilityLimits, "LL");
            oDictionary.Add((int)Enum.VehicleLimits, "VL");
            oDictionary.Add((int)Enum.ConstructionLimits, "CO");
            oDictionary.Add((int)Enum.ExistingBuildingLimits, "EB");
            oDictionary.Add((int)Enum.SpoilageLimits, "SL");
            oDictionary.Add((int)Enum.ColdStorageLimits, "CS");
            oDictionary.Add((int)Enum.YourPremisesLimits, "YP");
            oDictionary.Add((int)Enum.ExposureLimits, "EL");
            oDictionary.Add((int)Enum.BlanketLimits, "BL");
            oDictionary.Add((int)Enum.ProductsOperationsLimits, "PO");
            oDictionary.Add((int)Enum.BodilyInjuryLimit, "BI");
            oDictionary.Add((int)Enum.PhysicalDamageLimit, "PD");
            oDictionary.Add((int)Enum.NumberOfVehicles, "NU");
            oDictionary.Add((int)Enum.Payroll, "PR");
            oDictionary.Add((int)Enum.Values, "VA");
            oDictionary.Add((int)Enum.Liability, "LB");
            oDictionary.Add((int)Enum.ClassCodePayrollEmployees, "CC");
            oDictionary.Add((int)Enum.Contents, "CT");
            oDictionary.Add((int)Enum.Exposures, "EX");
            oDictionary.Add((int)Enum.FromDependentProperties, "DP");
            oDictionary.Add((int)Enum.ToOthers, "TO");
            oDictionary.Add((int)Enum.FromOthers, "FO");
            oDictionary.Add((int)Enum.YourTools, "YT");
            oDictionary.Add((int)Enum.EmployeeTools, "ET");
            oDictionary.Add((int)Enum.InsideThePremesisRobberyOfMoneyAndSecurites, "IP");
            oDictionary.Add((int)Enum.OutsideThePremesisRobberyOfMoneyAndSecurites, "OP");
            oDictionary.Add((int)Enum.InsideThePremesisTheftOfMoneyAndSecurites, "IT");
            oDictionary.Add((int)Enum.TheftPerEmployee, "TE");
            oDictionary.Add((int)Enum.VesselLimits, "VE");
            oDictionary.Add((int)Enum.ExcessCollisionLimits, "EC");
            oDictionary.Add((int)Enum.BreachofWarrantyLimits, "BW");
            oDictionary.Add((int)Enum.LiabilityLimits, "LE");
            oDictionary.Add((int)Enum.MedicalPaymentsLimits, "MP");
            oDictionary.Add((int)Enum.CoverageDLimits, "CV");
            oDictionary.Add((int)Enum.CoverageALimits, "CA");
            oDictionary.Add((int)Enum.AmountInsured, "AI");
            oDictionary.Add((int)Enum.AgreedValue, "AV");
            oDictionary.Add((int)Enum.RegularOpFurnishedAutos, "IR");
            oDictionary.Add((int)Enum.RegularOpNonFurnishedAutos, "IN");
            oDictionary.Add((int)Enum.EmployeesNonRegularOp, "IF");
            oDictionary.Add((int)Enum.NonEmployeesUnder25, "IU");
            oDictionary.Add((int)Enum.NonEmployeesOver25, "IO");
            oDictionary.Add((int)Enum.ComprehensiveLimits, "CP");
            oDictionary.Add((int)Enum.CollisionLimits, "CN");
            oDictionary.Add((int)Enum.NumberOfTrips, "NT");
            oDictionary.Add((int)Enum.PriceNew, "PN");
            oDictionary.Add((int)Enum.Mileage, "ML");
            oDictionary.Add((int)Enum.Deductable, "DD");
            oDictionary.Add((int)Enum.OwnedEquipment, "OE");
            oDictionary.Add((int)Enum.ForgeryAlteration, "FA");
            oDictionary.Add((int)Enum.ClientsProperty, "CZ");
            oDictionary.Add((int)Enum.FundsTransferFraud, "FT");
            oDictionary.Add((int)Enum.ComputerFraud, "CF");
            oDictionary.Add((int)Enum.InsuredPremises1, "I1");
            oDictionary.Add((int)Enum.InsuredPremises2, "I2");
            oDictionary.Add((int)Enum.Property1, "P1");
            oDictionary.Add((int)Enum.Property2, "P2");
            oDictionary.Add((int)Enum.Property3, "P3");
            oDictionary.Add((int)Enum.AllOtherCoveredProperty, "AC");
            oDictionary.Add((int)Enum.OnPremises, "OR");
            oDictionary.Add((int)Enum.Transportation, "TR");
            oDictionary.Add((int)Enum.TransportationLimit, "TI");
            oDictionary.Add((int)Enum.TransportationScheduleLimit, "TS");
            oDictionary.Add((int)Enum.Premises, "PT");
            oDictionary.Add((int)Enum.Transit, "TL");
            oDictionary.Add((int)Enum.HiredCarrier, "HC");
            oDictionary.Add((int)Enum.OwnedVehicle, "OV");
            oDictionary.Add((int)Enum.SoftwareMedia, "SF");
            oDictionary.Add((int)Enum.Garagekeeper, "GA");
            oDictionary.Add((int)Enum.InstallationCatostrophic, "IC");
            oDictionary.Add((int)Enum.JobsiteSchedule, "JS");
            oDictionary.Add((int)Enum.InTransit, "IA");
            oDictionary.Add((int)Enum.TemporaryLocations, "TY");
            oDictionary.Add((int)Enum.Tractors, "TA");
            oDictionary.Add((int)Enum.Trailers, "TZ");
            oDictionary.Add((int)Enum.ServiceUnits, "SU");
            oDictionary.Add((int)Enum.CommercialUnits, "CM");
            oDictionary.Add((int)Enum.PPTsExHeavy, "PX");
            oDictionary.Add((int)Enum.PPTsHeavy, "PH");
            oDictionary.Add((int)Enum.PPTsMedium, "PM");
            oDictionary.Add((int)Enum.PPTsLight, "PL");
            oDictionary.Add((int)Enum.ActivePlates, "AP");
            oDictionary.Add((int)Enum.InactivePlates, "TP");
            oDictionary.Add((int)Enum.TerminalCoverage, "TC");
            oDictionary.Add((int)Enum.TerminalLimit, "TM");
            oDictionary.Add((int)Enum.NonownedHiredAutoLimit, "NO");
            oDictionary.Add((int)Enum.Equipment, "EQ");
        }
    }
}
