﻿using BTS.WFA.ARMS.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CoverageKey
    {
        private string mstrKey;

        public CoverageKey(CoverageBO oCoverage)
        {
            CoverageNameKey oCoverageNameKey = new CoverageNameKey(oCoverage);
            StringBuilder oBuilder = new StringBuilder();
            oBuilder.Append(oCoverageNameKey.Key);
            oBuilder.Append("!");
            oBuilder.Append(oCoverage.CoverageValueType.Trim());
            mstrKey = oBuilder.ToString();
        }

        public CoverageKey(Coverage oCoverage)
        {
            CoverageNameKey oCoverageNameKey;
            if (oCoverage.CoverageName.SubLineOfBusinessCode.Length > 0)
            {
                oCoverageNameKey = new CoverageNameKey(oCoverage.CoverageName.LineOfBusinessCode, oCoverage.CoverageName.SubLineOfBusinessCode, oCoverage.CoverageName.CoverageNameTypeCode);
            }
            else
            {
                oCoverageNameKey = new CoverageNameKey(oCoverage.CoverageName.LineOfBusinessCode, oCoverage.CoverageName.CoverageNameTypeCode);
            }
            StringBuilder oBuilder = new StringBuilder();
            oBuilder.Append(oCoverageNameKey.Key);
            oBuilder.Append("!");
            oBuilder.Append(oCoverage.CoverageTypeCode.Trim());
            mstrKey = oBuilder.ToString();
        }

        public string Key
        {
            get { return mstrKey; }
        }
    }
}
