﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CoverageColumn
    {
        private CompanyLineOfBusinessEntity.LineOfBusinessType meLineOfBusiness;
        private CompanyLineOfBusinessEntity.LineOfBusinessType meSubLineOfBusiness;
        private CoverageGroupType.Enum meCoverageGroupType;
        private CoverageValueType.Enum meCoverageValueType;
        private PolicyStarCoverageValueType.Enum[] mePolicyStarCoverageValueTypes;
        private string meStagingCoverageValueType;
        private int miColumnNumber = 0;
        private int miDisplayOrder = -1;
        private CoverageGroupType moCoverageGroupType = new CoverageGroupType();
        private CoverageValueType moCoverageValueType = new CoverageValueType();
        private PolicyStarCoverageValueType moPolicyStarCoverageValueType = new PolicyStarCoverageValueType();

        public CoverageColumn()
        {
        }

        public CoverageColumn(	int iColumnNumber,
                                        CompanyLineOfBusinessEntity.LineOfBusinessType eLineOfBusiness,
                                        CoverageGroupType.Enum eCoverageGroupType,
                                        CoverageValueType.Enum eCoverageValueType,
                                        PolicyStarCoverageValueType.Enum[] ePolicyStarCoverageValueTypes,
                                        int iDisplayOrder )
        {
            miColumnNumber = iColumnNumber;
            meLineOfBusiness = eLineOfBusiness;
            meCoverageGroupType = eCoverageGroupType;
            meCoverageValueType = eCoverageValueType;
            mePolicyStarCoverageValueTypes = ePolicyStarCoverageValueTypes;
            miDisplayOrder = iDisplayOrder;
        }

        public CoverageColumn(int iColumnNumber,
                                CompanyLineOfBusinessEntity.LineOfBusinessType eLineOfBusiness,
                                CompanyLineOfBusinessEntity.LineOfBusinessType eSubLineOfBusiness,
                                CoverageGroupType.Enum eCoverageGroupType,
                                CoverageValueType.Enum eCoverageValueType,
                                PolicyStarCoverageValueType.Enum[] ePolicyStarCoverageValueTypes,
                                int iDisplayOrder )
        {
            miColumnNumber = iColumnNumber;
            meLineOfBusiness = eLineOfBusiness;
            meSubLineOfBusiness = eSubLineOfBusiness;
            meCoverageGroupType = eCoverageGroupType;
            meCoverageValueType = eCoverageValueType;
            mePolicyStarCoverageValueTypes = ePolicyStarCoverageValueTypes;
            miDisplayOrder = iDisplayOrder;
        }

        public CoverageColumn(int iColumnNumber,
                                        CompanyLineOfBusinessEntity.LineOfBusinessType eLineOfBusiness,
                                        CoverageGroupType.Enum eCoverageGroupType,
                                        CoverageValueType.Enum eCoverageValueType,
                                        string stagingCoverageValueType,
                                        int iDisplayOrder)
        {
            miColumnNumber = iColumnNumber;
            meLineOfBusiness = eLineOfBusiness;
            meCoverageGroupType = eCoverageGroupType;
            meCoverageValueType = eCoverageValueType;
            meStagingCoverageValueType = stagingCoverageValueType;
            miDisplayOrder = iDisplayOrder;
        }

        public CoverageColumn(int iColumnNumber,
                                CompanyLineOfBusinessEntity.LineOfBusinessType eLineOfBusiness,
                                CompanyLineOfBusinessEntity.LineOfBusinessType eSubLineOfBusiness,
                                CoverageGroupType.Enum eCoverageGroupType,
                                CoverageValueType.Enum eCoverageValueType,
                                string stagingCoverageValueType,
                                int iDisplayOrder)
        {
            miColumnNumber = iColumnNumber;
            meLineOfBusiness = eLineOfBusiness;
            meSubLineOfBusiness = eSubLineOfBusiness;
            meCoverageGroupType = eCoverageGroupType;
            meCoverageValueType = eCoverageValueType;
            meStagingCoverageValueType = stagingCoverageValueType;
            miDisplayOrder = iDisplayOrder;
        }

        #region Members
        public CompanyLineOfBusinessEntity.LineOfBusinessType LineOfBusiness
        {
            get { return meLineOfBusiness; }
        }

        public CompanyLineOfBusinessEntity.LineOfBusinessType SubLineOfBusiness
        {
            get { return meSubLineOfBusiness; }
        }

        public CoverageGroupType.Enum CoverageGroupType
        {
            get { return meCoverageGroupType; }
        }

        public string CoverageGroupTypeAsString
        {
            get 
            {
                return moCoverageGroupType[(int)meCoverageGroupType];
            }
        }

        public CoverageValueType.Enum CoverageValueType
        {
            get { return meCoverageValueType; }
        }

        public string CoverageValueTypeAsString
        {
            get 
            {
                return moCoverageValueType[(int)meCoverageValueType];
            }
        }

        public PolicyStarCoverageValueType.Enum[] PolicyStarCoverageValueTypes
        {
            get { return mePolicyStarCoverageValueTypes; }
        }
        
        public string StagingCoverageValueType
        {
            get { return meStagingCoverageValueType; }
        }

        public List<string> CoverageCodesAsStringArray
        {
            get
            {
                List<string> list = new List<string>();
                if (mePolicyStarCoverageValueTypes != null)
                {
                    foreach (PolicyStarCoverageValueType.Enum code in mePolicyStarCoverageValueTypes)
                    {
                        list.Add(moPolicyStarCoverageValueType[(int)code].ToString());
                    }
                }

                return list;
            }
        }
        
        public int ColumnNumber
        {
            get { return miColumnNumber; }
        }

        public int DisplayOrder
        {
            get { return miDisplayOrder; }
        }
        
        #endregion

        #region Methods
        #endregion
    }
}
