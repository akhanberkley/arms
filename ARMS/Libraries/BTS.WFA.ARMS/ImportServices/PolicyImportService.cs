﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ARMS.ViewModels;
using Int = BTS.WFA.Integration;

namespace BTS.WFA.ARMS.ImportServices
{
    public class PolicyImportService
    {
        /*
        public static List<PolicyViewModel> GetPolicies(CompanyViewModel company, string policyNumber)
        {
            
            string serviceEndPointUrl = company.PolicyStarEndPointUrl;
            var srvResult = Int.Services.PolicyStarTransactionalService.GetPolicyByPolicyNumber(serviceEndPointUrl, policyNumber, company.CompanyNumber);

            if (!srvResult.Succeeded)
                Application.HandleException(new Exception("Policy Import Failed", srvResult.Exception), new { CompanyNumber = company.CompanyNumber, PolicyNumber = policyNumber, ServiceEndPointUrl = serviceEndPointUrl });
            else if (srvResult.Policies.Count > 0)
                //return srvResult.Policies;
                // MAP The List<Int.Models.Policy> srvResult.Policies into a List<PolicyViewModel>
            
        }
        */

        //temporarily just returning a t/f that if found results until I can map the data to handle it in ARMS
        public static bool FindPolicies(CompanyDetail company, string policyNumber)
        {
            string serviceEndPointUrl = "http://aic-prd/PolicySTAR_WebServices/services/PremiumAuditService";
            
            var srvResult = Int.Services.PolicyStarTransactionalService.GetPolicyByPolicyNumber(serviceEndPointUrl, policyNumber, company.Summary.CompanyNumber);

            if (!srvResult.Succeeded)
                Application.HandleException(new Exception("Policy Import Failed", srvResult.Exception), new { Company = company.Summary, PolicyNumber = policyNumber, ServiceEndPointUrl = serviceEndPointUrl });
            else if (srvResult.Policies.Count > 0)
                return true;

            return false;

        }
    }
}
