﻿using BTS.WFA.ARMS.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class LocationImportBuilder
    {
        private int _buildingCount = 0;
        private Location _location = null;

        public LocationImportBuilder(Location location, int buildingCount)
        {
            _location = location;
            _buildingCount = buildingCount;
        }

        public int BuildingCount
        {
            get
            {
                return _buildingCount;
            }
        }

        public Location Location
        {
            get
            {
                return _location;
            }
        }
    }
}
