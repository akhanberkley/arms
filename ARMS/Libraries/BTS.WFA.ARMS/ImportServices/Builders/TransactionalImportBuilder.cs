﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Collections;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data;
using BTS.WFA.ARMS.Data.Models;
using IntSrvc = BTS.WFA.Integration.Services;
using Int = BTS.WFA.Integration.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public abstract class TransactionalImportBuilder
    {
        #region properties
        private IImportBuilder _moImportBuilder;
        private InsuredBO _moInsuredBO = new InsuredBO();
        private CompanyDetail _company;
        private string _policyNumber;
        private ArrayList policyStatusList = new ArrayList();
        private List<string> moProperCaseExclusions = new List<string>();
        #endregion

        public TransactionalImportBuilder()
        {
        }

        public void Initialize(IImportBuilder oImportBuilder, string policyNumber, CompanyDetail company)
        {
            if (company == null) throw new ArgumentNullException("company");

            _moImportBuilder = oImportBuilder;
            _company = company;
            _policyNumber = policyNumber;
        }

        #region members
        protected string PolicyNumber
        {
            get { return _policyNumber; }
        }

        protected CompanyDetail BerkleyCompany
        {
            get { return _company; }
        }

        //protected string SearchParameters
        //{
        //    get
        //    {
        //        string strReturn = "";
        //        if (mstrPolicyNumber != null && mstrPolicyNumber.Length > 0)
        //        {
        //            strReturn = "Number: '" + mstrPolicyNumber + "'. ";
        //        }
        //        return strReturn;
        //    }
        //}
        #endregion

        #region methods
        //public InsuredViewModel BuildRequestFromImport...
        
        internal InsuredBO Build(Int.PolicySystemResults importResults, bool IsQuote) 
        {
            //Traverse the products of the policies looking for unique locations
            SortedList locationList = new SortedList();

            //Check if the policy number exist in the returned results
            bool policyNumberMatched = PolicyMatched(importResults);
            
            foreach (Int.Policy policyDO in importResults.Policies)
            {
                if (_moInsuredBO.InsuredEntity == null &&
                    ((policyDO.Number == _policyNumber && policyNumberMatched) ||
                    (policyDO.Number != _policyNumber && !policyNumberMatched) || IsQuote))
                {
                    // Build Insured
                    if (policyDO.NamedInsureds != null)
                    {
                        BuildInsured(policyDO, ref _moInsuredBO);
                    }
                    else
                    {
                        // #TODO: Fix Exception type - Found no Insured error
                        Debug.WriteLine(string.Format("No Insured Found. TODO: Throw exception."));
                    }
                        
                    // Build Agency
                    if (policyDO.Agency != null)
                    {
                        // determine if the company uses APS or not
                        if (!string.IsNullOrEmpty(_company.APSEndPointUrl))
                        {
                            try
                            {
                                // APS Service Call
                                var apsImportResults = IntSrvc.AgencyService.GetAgency(_company.APSEndPointUrl, policyDO.Agency.Number, false, null);
                                _moInsuredBO.AgencyEntity = BuildAgency(apsImportResults.Agency);
                            }
                            catch
                            {
                                // falls back to the policy system data
                                _moInsuredBO.AgencyEntity = BuildAgency(policyDO.Agency);
                            }
                        }
                        else
                        {
                            // uses the policy system data
                            _moInsuredBO.AgencyEntity = BuildAgency(policyDO.Agency);
                        }
                    }
                }

                //Build Policy
                Policy policy = BuildPolicy(policyDO);
                if (!string.IsNullOrEmpty(policy.PolicyNumber) 
                    && policy.PolicyMod != Int32.MinValue 
                    && !string.IsNullOrEmpty(policy.PolicySymbol)
                    && policyDO.Products != null
                    )
                {
                    _moInsuredBO.PolicyList.Add(policy);

                    // Build Coverages
                    List<CoverageBO> coverageList = BuildCoverageList(policyDO.Products, policy);
                    foreach (CoverageBO coverage in coverageList)
                    { 
                        //_moInsuredBO.CoverageList.Add(new CoverageKey(coverage), coverage)
                        _moInsuredBO.CoverageList.Add(coverage);
                    }

                    if (policyDO.Products != null)
                    {
                        // Build Rate Modifications
                        foreach (Int.PDRSecondaryProduct product in policyDO.Products)
                        {
                            if (product.RateModFactors != null)
                            {
                                foreach (Int.PDRRateModFactor rateMod in product.RateModFactors)
                                {
                                    _moInsuredBO.RateModFactorList.Add(BuildRateModFactor(rateMod, policy));
                                }
                            }
                        }
                        
                        // Locations and Buildings
                        foreach (Int.PDRSecondaryProduct sProduct in policyDO.Products)
                        {
                            if (sProduct.Units != null) //Locations
                            {
                                foreach (Int.PDRUnit locationUnit in sProduct.Units)
                                {
                                    if (ReadString(locationUnit.EntityName).ToUpper() == "LOCATION" || ReadString(locationUnit.EntityC).ToUpper() == "LOC")
                                    {
                                        // Build and add location 
                                        Location location;

                                        if (!locationList.Contains(BuildLocationKey(locationUnit)))
                                        {
                                            location = BuildLocation(locationUnit, policy);
                                            _moInsuredBO.PolicyLocationList.Add(location);

                                            //retain location for uniqueness
                                            locationList.Add(BuildLocationKey(locationUnit), location);
                                        }
                                        else
                                        {
                                            //use previous built location
                                            location = locationList[BuildLocationKey(locationUnit)] as Location;

                                            //even though we can reuse the previous built location, we need to check if this current one has a protection class entered
                                            int publicProtection = StripNonNumericChars(locationUnit.ProtectionClass);
                                            if (location.PublicProtection == Int32.MinValue && publicProtection != Int32.MinValue)
                                            {
                                                location.PublicProtection = publicProtection;
                                            }
                                        }

                                        if (locationUnit.Units.Count > 0) //Buildings
                                        {
                                            foreach (Int.PDRUnit buildingUnit in locationUnit.Units)
                                            {
                                                if (ReadString(buildingUnit.EntityName).ToUpper() == "BUILDING" || ReadString(buildingUnit.EntityC).ToUpper() == "BLD")
                                                {
                                                    // Build and add building 
                                                    _moInsuredBO.BuildingList.Add(BuildBuilding(buildingUnit, location));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Check if any policies were found
            if (_moInsuredBO.PolicyList.Count <= 0)
            {
                // #TODO Fix Exception type
                //throw new BlcNoInsuredException("No policies were found for " + SearchParameters);
            }

            //Default a location
            if (_moInsuredBO.PolicyLocationList.Count <= 0)
            {
                //If no locations, add the insured address as the default location
                _moInsuredBO.PolicyLocationList.Add(BuildLocation(_moInsuredBO.InsuredEntity));
            }

            //Set the account status based on the statuses of all the insured's policies
            //Only if all policies are non-renewed, is the client set as non-renewed
            //_moInsuredBO.InsuredEntity.NonrenewedDate = (!policyStatusList.Contains("N")) ? DateTime.Now : DateTime.MinValue;
            if (!policyStatusList.Contains("N"))
            {
                _moInsuredBO.InsuredEntity.NonrenewedDate = DateTime.Now;
            }

            return _moImportBuilder.BuildComplete(_moInsuredBO);
        }

        private Insured BuildInsured(Int.Policy pdrPolicy, ref InsuredBO oInsuredBO) 
        {
            Insured insured = new Insured();
            insured.InsuredId = Guid.NewGuid();
            insured.BusinessOperations = pdrPolicy.BusinessDescription;
            insured.Underwriter = pdrPolicy.UnderwriterCode;
            insured.PortfolioId = pdrPolicy.PortfolioId;
            //insured.CompanyWebsite = "test url";
            
            // Pull the SIC Code from the custome details
            if (pdrPolicy.CustomDetails.Count > 0)
            {
                foreach (var detail in pdrPolicy.CustomDetails)
                {
                    if(detail.Name.ToUpper() == "SIC_CD")
                    {
                        insured.SicCode = (!string.IsNullOrEmpty(detail.Value)) ? detail.Value.PadLeft(4, '0') : string.Empty;
                    }
                }
            }

            // Loop through the Named Insureds and create the insured record where they Primary Flag is true
            for (int i = 0; i < pdrPolicy.NamedInsureds.Count; i++)
            {
                if (pdrPolicy.NamedInsureds[i] != null)
                {
                    if (pdrPolicy.NamedInsureds[i].PrimaryNamedInsuredFlag == "Y") // Primary Insured
                    {
                        // build this insured object
                        insured.ClientId = pdrPolicy.NamedInsureds[i].Names[0].ClientId.ToString();
                        insured.Name = pdrPolicy.NamedInsureds[i].Names[0].BusinessName;
                        insured.DotNumber = pdrPolicy.NamedInsureds[i].Names[0].DocketNumber;

                        // Insured Addresses
                        if (pdrPolicy.PolicyAddresses != null)
                        {
                            bool primaryAddressFound = false;
                            for (int j = 0; j < pdrPolicy.PolicyAddresses.Count; j++)
                            {
                                if (pdrPolicy.PolicyAddresses[j].PrimaryFlag == "Y" ||
                                    pdrPolicy.PolicyAddresses.Count == 1)
                                {
                                    //PDRAddress 
                                    var address = pdrPolicy.PolicyAddresses[j];
                                    insured.StreetLine1 = ReadStreetAddress(address.HouseNumber, address.Address1);
                                    insured.StreetLine2 = address.Address2;
                                    insured.City = address.City;
                                    insured.StateCode = address.StateProvCode;
                                    insured.ZipCode = address.PostalCode;
                                    primaryAddressFound = true;
                                }
                            }

                            //If a primary address is not returned, then just grab the first address
                            if (primaryAddressFound == false && pdrPolicy.PolicyAddresses.Count > 0)
                            {
                                //PDRAddressDO
                                var address = pdrPolicy.PolicyAddresses[0];
                                insured.StreetLine1 = ReadStreetAddress(address.HouseNumber, address.Address1);
                                insured.StreetLine2 = address.Address2;
                                insured.City = address.City;
                                insured.StateCode = address.StateProvCode;
                                insured.ZipCode = address.PostalCode;
                            }
                        }

                        // Get Owner's Information
                        #region from 2013
                        //if (pdrPolicy.namedInsured[i].insName[0].communications != null)
                        //{
                        //    foreach (PDRCommunicationDO communication in pdrPolicy.namedInsured[i].insName[0].communications)
                        //    {
                        //        if (ReadString(communication.communicationType).ToUpper() == "CTWORKPHN")
                        //        {
                        //            oBlcInsured.OwnerPhone = ReadString(communication.communicationValue);
                        //        }
                        //        else if (ReadString(communication.communicationType).ToUpper() == "PHONE_NUM")
                        //        {
                        //            oBlcInsured.OwnerPhone = ReadString(communication.communicationValue);
                        //        }
                        //        else if (ReadString(communication.communicationType).ToUpper() == "CTHOMEPHN" && oBlcInsured.OwnerAltPhone == string.Empty)
                        //        {
                        //            oBlcInsured.OwnerAltPhone = ReadString(communication.communicationValue);
                        //        }
                        //        else if (ReadString(communication.communicationType).ToUpper() == "CTCELLPHN" && oBlcInsured.OwnerAltPhone == string.Empty)
                        //        {
                        //            oBlcInsured.OwnerAltPhone = ReadString(communication.communicationValue);
                        //        }
                        //        else if (ReadString(communication.communicationType).ToUpper() == "CTSFDIRPHN" && oBlcInsured.OwnerAltPhone == string.Empty)
                        //        {
                        //            oBlcInsured.OwnerAltPhone = ReadString(communication.communicationValue);
                        //        }
                        //        else if (ReadString(communication.communicationType).ToUpper() == "CTSFDIRPHN" && oBlcInsured.OwnerAltPhone == string.Empty)
                        //        {
                        //            oBlcInsured.OwnerFax = ReadString(communication.communicationValue);
                        //        }
                        //        else if (ReadString(communication.communicationType).ToUpper() == "CTEMAIL")
                        //        {
                        //            oBlcInsured.OwnerEmail = ReadString(communication.communicationValue);
                        //        }
                        //    }
                        //}
                        #endregion
                        //if (insured.Names[0].Communications != null)
                        //{
                        //    foreach (var comm in insured.Names[0].Communications)
                        //    {
                        //        // get by nodes
                        //    }
                        //}

                        // Owners name
                        oInsuredBO.Owner = pdrPolicy.OwnerLegalName;
                    }
                    else
                    {
                        // Second named insured
                        insured.Name2 = pdrPolicy.NamedInsureds[i].Names[0].BusinessName;
                    }
                }
            }

            oInsuredBO.InsuredEntity = insured;
            return insured;
        }

        private Agency BuildAgency(Int.PDRAgency pdrAgency)
        {
            Agency agency = new Agency();
            agency.AgencyId = Guid.NewGuid();
            agency.CompanyId = _company.CompanyId;

            // Name and Agency Number
            agency.AgencyName = pdrAgency.Name;
            agency.AgencyNumber = pdrAgency.Number;

            // Address
            if(pdrAgency.Address != null)
            {
                agency.StreetLine1 = ReadStreetAddress(pdrAgency.Address.HouseNumber, pdrAgency.Address.Address1);
                agency.StreetLine2 = pdrAgency.Address.Address2;
                agency.City = pdrAgency.Address.City;
                agency.StateCode = pdrAgency.Address.StateProvCode;
                agency.ZipCode = pdrAgency.Address.PostalCode;
            }

            // Contact
            agency.PhoneNumber = pdrAgency.PhoneNumber;
            
            return agency;
        }

        /// <summary>
        /// Builds an <see cref="agency"/> for a given agency code in the APS xml file.
        /// </summary>
        /// <param name="apsAgency">The agency data object.</param>
        /// <returns>The Agency Entity <see cref="Agency"/>.</returns>
        internal Agency BuildAgency(Int.Agency apsAgency)
        {
            Agency agency = new Agency();
            agency.AgencyId = Guid.NewGuid();
            agency.CompanyId = _company.CompanyId;

            // Name and Agency Number
            agency.AgencyName = apsAgency.AgencyName;
            agency.AgencyNumber = apsAgency.AgencyCode;

            // Address
            foreach (Int.AgencyLocation location in apsAgency.Locations)
            {
                if (location.IsPrimary)
                {
                    agency.StreetLine1 = location.Address1;
                    agency.StreetLine2 = location.Address2;
                    agency.City = location.City;
                    agency.StateCode = (location.State != null) ? location.State : string.Empty;
                    agency.ZipCode = location.PostalCode;

                    //contact info
                    agency.PhoneNumber = location.Phone.ToString();
                    agency.FaxNumber = location.Fax.ToString();
                }
            }

            return agency;
        }

        private Policy BuildPolicy(Int.Policy pdrPolicy)
        {
            Policy policy = new Policy();
            policy.PolicyId = Guid.NewGuid();

            // Get the profit center from the custom details
            if (pdrPolicy.CustomDetails != null)
            {
                foreach (Int.PDRCustomDetail customDetail in pdrPolicy.CustomDetails)
                {
                    if (ReadString(customDetail.Name).ToUpper() == "PCC")
                    {
                        policy.ProfitCenterCode = ReadString(customDetail.Value);
                    }
                }
            }

            policy.BranchCode = pdrPolicy.BranchCode;
            if (!string.IsNullOrWhiteSpace(pdrPolicy.Carrier))
                policy.Carrier = pdrPolicy.Carrier;
            else
                policy.Carrier = pdrPolicy.CompnayName;
            policy.EffectiveDate = pdrPolicy.EffectiveDate;
            policy.ExpireDate = pdrPolicy.ExpirationDate;
            policy.PolicyMod = ReadInteger(pdrPolicy.SequenceNumber);
            policy.PolicyNumber = pdrPolicy.Number;
            policy.PolicySymbol = pdrPolicy.Symbol;
            policy.Premium = ReadDecimal(pdrPolicy.TotalEstimatedPremium);

            if (pdrPolicy.Products != null)
            {
                foreach (Int.PDRSecondaryProduct sProduct in pdrPolicy.Products)
                {
                    if (!string.IsNullOrEmpty(sProduct.LobHazardDescription))
                    {
                        policy.HazardGrade = StripNonNumericChars(ReadString(sProduct.LobHazardDescription)).ToString();
                    }
                }
            }

            // Try and find past hazard grade for the policy
            if (policy.HazardGrade == string.Empty)
            {
                // TODO: Rectify - Hazard Grade from Previous Policy
                //using (var moDb = Application.GetDatabaseInstance())
                //{
                //    Policy pastPolicy = moDb.Policies.Select(p => p.PolicyNumber == pdrPolicy.Number && p.HazardGrade != null)).First();
                //}
                //Policy pastPolicy = Policy.GetOne("Number = ? && !ISNULL(HazardGrade) && HazardGrade != '' && Insured.CompanyID = ?", policy.Number, berkleyCompany.ID);
                Policy pastPolicy = null;
                policy.HazardGrade = (pastPolicy != null) ? pastPolicy.HazardGrade : "";
            }

            // Add the policy status to the list for setting the client status later
            policyStatusList.Add(pdrPolicy.NonRenewableFlag);

            return policy;
        }

        /// <summary>
        /// Builds an identifier for a given location
        /// </summary>
        /// <param name="pdrLocation">The PDRUnitDO data object.</param>
        /// <returns>The a key genereted from the locatino info.</returns>
        private string BuildLocationKey(Int.PDRUnit pdrLocation)
        {
            StringBuilder sb = new StringBuilder();

            if (pdrLocation != null && pdrLocation.Address != null)
            {
                sb.Append(ReadInteger(ReadString(pdrLocation.AttachNumber)));
                sb.Append("|");
                sb.Append(ReadString(pdrLocation.Address.HouseNumber));
                sb.Append("|");
                sb.Append(ReadString(pdrLocation.Address.City));
                sb.Append("|");
                sb.Append(ReadState(pdrLocation.Address.StateProvCode));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Builds a <see cref="Location"/> for a given PDRUnitDO in the PolicyStar xml file.
        /// </summary>
        /// <param name="starsLocation">The PDRUnitDO data object.</param>
        /// <param name="policy">The policy this loaction was extracted from.</param>
        /// <returns>The Location Entity <see cref="Location"/>.</returns>
        private Location BuildLocation(Int.PDRUnit pdrLocation, Policy policy)
        {
            Location location = new Location();
            location.LocationId = Guid.NewGuid();

            if (pdrLocation != null && pdrLocation.Address != null)
            {
                location.LocationNumber = ReadInteger(ReadString(pdrLocation.AttachNumber));
                location.PolicySymbol = policy.PolicySymbol;
                location.PolicySystemKey = string.Format("{0}-{1}", location.LocationNumber, policy.PolicyNumber);
                location.StreetLine1 = ReadStreetAddress(pdrLocation.Address.HouseNumber, pdrLocation.Address.Address1);
                location.StreetLine2 = ReadString(pdrLocation.Address.Address2);
                location.City = ReadString(pdrLocation.Address.City);
                location.StateCode = ReadState(pdrLocation.Address.StateProvCode);
                location.ZipCode = ReadString(pdrLocation.Address.PostalCode);
                location.IsActive = true;

                //Discovered on 3/29/10 that the public protection is at the location level in p*
                location.PublicProtection = StripNonNumericChars(pdrLocation.ProtectionClass);

                //Discovered on 5/24/10 that the spinkler indicator is at the location level in p*
                if (ReadString(pdrLocation.ProtectiveSafeGuard) != string.Empty)
                {
                    if (ReadString(pdrLocation.ProtectiveSafeGuard) == "P-1")
                    {
                        location.HasSprinklerSystem = 1;
                    }
                    else
                    {
                        location.HasSprinklerSystem = 0;
                    }
                }
            }

            return location;
        }

        /// <summary>
        /// Builds a <see cref="Location"/> for a given PDRUnitDO in the PolicyStar xml file.
        /// </summary>
        /// <param name="starsLocation">The PDRUnitDO data object.</param>
        /// <param name="policy">The policy this loaction was extracted from.</param>
        /// <returns>The Location Entity <see cref="Location"/>.</returns>
        private Location BuildLocation(Insured insured)
        {
            Location location = new Location();
            location.LocationId = Guid.NewGuid();

            location.PolicySystemKey = string.Format("1-{0}", PolicyNumber);
            location.LocationNumber = 1;
            location.IsActive = true;
            location.StreetLine1 = _moInsuredBO.InsuredEntity.StreetLine1;
            location.StreetLine2 = _moInsuredBO.InsuredEntity.StreetLine2;
            location.City = _moInsuredBO.InsuredEntity.City;
            location.StateCode = _moInsuredBO.InsuredEntity.StateCode;
            location.ZipCode = _moInsuredBO.InsuredEntity.ZipCode;

            return location;
        }

        /// <summary>
        /// Builds a <see cref="Building"/> for a given PDRUnitDO in the PolicyStar xml file.
        /// </summary>
        /// <param name="starsBuilding">The PDRUnitDO data object.</param>
        /// <param name="location">The location that the building will be tied to.</param>
        /// <returns>The Building Entity <see cref="Building"/>.</returns>
        private BuildingBO BuildBuilding(Int.PDRUnit pdrBuilding, Location location)
        {
            BuildingBO building = new BuildingBO();
            building.BuildingId = Guid.NewGuid();
            building.BuildingNumber = ReadShort(ReadString(pdrBuilding.AttachNumber));
            building.PolicySystemKey = location.PolicySystemKey;
            building.LocationAsInt = location.LocationNumber;

            //the following values are actually coverages in PDR
            if (pdrBuilding.Coverages != null)
            {
                foreach (Int.PDRCoverage coverage in pdrBuilding.Coverages)
                {
                    switch (ReadString(coverage.Code).ToUpper())
                    {
                        case "BPP":
                            building.Contents = ReadDecimal(coverage.LimitAmount1);
                            break;
                        case "BLDG":
                            building.BuildingValue = ReadDecimal(coverage.LimitAmount1);
                            break;
                        case "STK":
                            building.StockValues = ReadDecimal(coverage.LimitAmount1);
                            break;
                        case "TE":
                            building.BusinessInterruption = ReadDecimal(coverage.LimitAmount1);
                            break;
                    }
                }
            }

            building.LocationOccupancy = ReadString(pdrBuilding.Description);
            building.PublicProtection = location.PublicProtection;
            building.HasSprinklerSystem = location.HasSprinklerSystem;
            building.YearBuilt = ReadInteger(ReadString(pdrBuilding.Year));

            return building;
        }

        private Location BuildPolicyLocation(Int.PDRUnit pdrLocation, Policy policy)
        {
            Location location = new Location();
            location.LocationId = Guid.NewGuid();

            if (pdrLocation != null && pdrLocation.Address != null)
            {
                location.LocationNumber = ReadInteger(pdrLocation.AttachNumber.ToString());
                location.PolicySymbol = policy.PolicySymbol;
                location.PolicySystemKey = string.Format("{0}-{1}", location.LocationNumber, policy.PolicyNumber);
                location.StreetLine1 = ReadStreetAddress(pdrLocation.Address.HouseNumber, pdrLocation.Address.Address1);
                location.StreetLine2 = pdrLocation.Address.Address2;
                location.City = pdrLocation.Address.City;
                location.StateCode = pdrLocation.Address.StateProvCode;
                location.ZipCode = pdrLocation.Address.PostalCode;
                location.IsActive = true;

                //Discovered on 3/29/10 that the public protection is at the location level in p*
                location.PublicProtection = StripNonNumericChars(pdrLocation.ProtectionClass);

                //Discovered on 5/24/10 that the spinkler indicator is at the location level in p*
                if (ReadString(pdrLocation.ProtectiveSafeGuard) != string.Empty)
                {
                    if (ReadString(pdrLocation.ProtectiveSafeGuard) == "P-1")
                    {
                        location.HasSprinklerSystem = 1;
                    }
                    else
                    {
                        location.HasSprinklerSystem = 0;
                    }
                }
            }

            return location;
        }

        private Location BuildInsuredLocation(Insured insured)
        {
            Location location = new Location();
            location.LocationId = Guid.NewGuid();

            location.PolicySystemKey = string.Format("1-{0}", PolicyNumber);
            location.LocationNumber = 1;
            location.IsActive = true;
            location.StreetLine1 = _moInsuredBO.InsuredEntity.StreetLine1;
            location.StreetLine2 = _moInsuredBO.InsuredEntity.StreetLine2;
            location.City = _moInsuredBO.InsuredEntity.City;
            location.StateCode = _moInsuredBO.InsuredEntity.StateCode;
            location.ZipCode = _moInsuredBO.InsuredEntity.ZipCode;

            return location;
        }

        /// <summary>
        /// Builds a <see cref="RateModificationFactor"/> for a given RateModFactor in the PolicyStar xml file.
        /// </summary>
        /// <param name="policyStarRateMod">The RateModFactor data object.</param>
        /// <param name="policy">The policy to tie the rate mod factor to.</param>
        /// <returns>The RateModificationFactor Entity <see cref="RateModificationFactor"/>.</returns>
        private RateModFactorBO BuildRateModFactor(Int.PDRRateModFactor pdrRateMod, Policy policy)
        {
            RateModFactorBO rateModFactorBO = new RateModFactorBO();
            RateModificationFactor factor = new RateModificationFactor();
            
            factor.RateModificationFactorId = Guid.NewGuid();
            factor.StateCode = ReadState(pdrRateMod.State);
            factor.Rate = ReadDecimal(pdrRateMod.Factor);
            factor.RateModificationFactorTypeCode = ReadRateModType(pdrRateMod.Code);

            rateModFactorBO.Entity = factor;
            rateModFactorBO.PolicyMod = policy.PolicyMod;
            rateModFactorBO.PolicyNumber = policy.PolicyNumber;

            return rateModFactorBO;
        }

        /// <summary>
        /// Builds an of <see cref="Coverage"/> entities for a given list of PDRCoverageDOs in the PolicyStar xml file.
        /// </summary>
        /// <param name="oRow">The PDRCoverageDO data object.</param>
        /// <returns>An array of Coverage Entitities.</returns>
        private List<CoverageBO> BuildCoverageList(List<Int.PDRSecondaryProduct> pdrProducts, Policy policy)
        {
            List<CoverageBO> list = new List<CoverageBO>();

            // Field Mapping for the Coverage/Line of Business
            CompanyCoverageEntity coverageFields = _moImportBuilder.CoverageFieldEntity;
            
            // Defines each LOB
            CompanyLineOfBusinessEntity lineOfBusiness = _moImportBuilder.LineOfBusinessFieldEntity;

            // Convert that to a Typed name
            // Converts a symbol like GL into "GeneralLiability" from the enum
            CompanyLineOfBusinessEntity.LineOfBusinessType eType = lineOfBusiness.GetType(policy.PolicySymbol);

            // Switch Policy Symbol and get the field mapping for this LOB
            // takes the "GeneralLiability" and creats this field mapping...
            List<CoverageColumn> companyColumnList = _moImportBuilder.GetCoverage(eType);

            try
            {
                if (companyColumnList != null)
                {
                    // Iterage Rows in Coverage - Ignore the column numbers defined in CompanyCoverageEntity
                    foreach (CoverageColumn coverageColumn in companyColumnList)
                    {
                        bool matchFound = false;

                        CoverageBO coverageBO = null;

                        if (pdrProducts != null && coverageColumn.CoverageValueType == CoverageValueType.Enum.ClassCodePayrollEmployees)
                        {
                            foreach (Int.PDRSecondaryProduct product in pdrProducts)
                            {
                                if (product.Units != null)
                                {
                                    foreach (Int.PDRUnit unit in product.Units)
                                    {
                                        if (unit.Coverages != null)
                                        {
                                            foreach (Int.PDRCoverage unitCoverage in unit.Coverages)
                                            {
                                                if (IsMeaningful(unitCoverage.ClassCodeCov))
                                                {
                                                    coverageBO = BuildCoverage(unitCoverage, coverageColumn, policy, lineOfBusiness);
                                                    list.Add(coverageBO);
                                                    matchFound = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (coverageColumn.CoverageValueType == CoverageValueType.Enum.NumberOfVehicles)
                        {
                            int vehicleCount = 0;
                            if (pdrProducts != null)
                            {
                                foreach (Int.PDRSecondaryProduct product in pdrProducts)
                                {
                                    if (product.Units != null)
                                    {
                                        foreach (Int.PDRUnit unit in product.Units)
                                        {
                                            if (ReadString(unit.EntityName).ToUpper() == "VEHICLE" || ReadString(unit.EntityC).ToUpper() == "VH")
                                            {
                                                vehicleCount = vehicleCount + 1;
                                                matchFound = true;
                                            }
                                        }
                                    }
                                }
                            }

                            Int.PDRCoverage coverage = new Int.PDRCoverage();
                            coverage.LimitAmount1 = vehicleCount;

                            coverageBO = BuildCoverage(coverage, coverageColumn, policy, lineOfBusiness);
                            list.Add(coverageBO);
                        }
                        else if (coverageColumn.CoverageValueType == CoverageValueType.Enum.Exposures)
                        {
                            double exposureAmount = 0;
                            if (pdrProducts != null)
                            {
                                foreach (Int.PDRSecondaryProduct product in pdrProducts)
                                {
                                    if (product.Units != null)
                                    {
                                        foreach (Int.PDRUnit unit in product.Units)
                                        {
                                            if (unit.Coverages != null)
                                            {
                                                foreach (Int.PDRCoverage unitCoverage in unit.Coverages)
                                                {
                                                    if (coverageColumn.CoverageCodesAsStringArray.Contains(ReadString(unitCoverage.Code)))
                                                    {
                                                        exposureAmount = exposureAmount + unitCoverage.EstimatedExposureAmmount;
                                                        matchFound = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            Int.PDRCoverage coverage = new Int.PDRCoverage();
                            coverage.LimitAmount1 = exposureAmount;

                            coverageBO = BuildCoverage(coverage, coverageColumn, policy, lineOfBusiness);
                            list.Add(coverageBO);
                        }
                        else
                        {
                            if (pdrProducts != null)
                            {
                                foreach (Int.PDRSecondaryProduct product in pdrProducts)
                                {
                                    if (product.Coverages != null)
                                    {
                                        foreach (Int.PDRCoverage parentCoverage in product.Coverages)
                                        {
                                            if (coverageColumn.CoverageCodesAsStringArray.Contains(ReadString(parentCoverage.Code)) &&
                                                ReadString(parentCoverage.LimitAmount1) != string.Empty &&
                                                ReadString(parentCoverage.LimitAmount1) != "0" &&
                                                matchFound == false)
                                            {
                                                coverageBO = BuildCoverage(parentCoverage, coverageColumn, policy, lineOfBusiness);
                                                list.Add(coverageBO);
                                                matchFound = true;
                                                break;
                                            }

                                            if (parentCoverage.Coverages != null)
                                            {
                                                foreach (Int.PDRCoverage childCoverage in parentCoverage.Coverages)
                                                {
                                                    if (coverageColumn.CoverageCodesAsStringArray.Contains(ReadString(childCoverage.Code)) &&
                                                        ReadString(childCoverage.LimitAmount1) != string.Empty &&
                                                        ReadString(childCoverage.LimitAmount1) != "0" &&
                                                        matchFound == false)
                                                    {
                                                        coverageBO = BuildCoverage(childCoverage, coverageColumn, policy, lineOfBusiness);
                                                        list.Add(coverageBO);
                                                        matchFound = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        if (product.Units != null)
                                        {
                                            foreach (Int.PDRUnit unit in product.Units)
                                            {
                                                if (unit.Coverages != null)
                                                {
                                                    foreach (Int.PDRCoverage parentCoverage in unit.Coverages)
                                                    {
                                                        if (coverageColumn.CoverageCodesAsStringArray.Contains(ReadString(parentCoverage.Code)) &&
                                                            ReadString(parentCoverage.LimitAmount1) != string.Empty &&
                                                            ReadString(parentCoverage.LimitAmount1) != "0" &&
                                                            matchFound == false)
                                                        {
                                                            coverageBO = BuildCoverage(parentCoverage, coverageColumn, policy, lineOfBusiness);
                                                            list.Add(coverageBO);
                                                            matchFound = true;
                                                            break;
                                                        }

                                                        if (parentCoverage.Coverages != null)
                                                        {
                                                            foreach (Int.PDRCoverage childCoverage in parentCoverage.Coverages)
                                                            {
                                                                if (coverageColumn.CoverageCodesAsStringArray.Contains(ReadString(childCoverage.Code)) &&
                                                                    ReadString(childCoverage.LimitAmount1) != string.Empty &&
                                                                    ReadString(childCoverage.LimitAmount1) != "0" &&
                                                                    matchFound == false)
                                                                {
                                                                    coverageBO = BuildCoverage(childCoverage, coverageColumn, policy, lineOfBusiness);
                                                                    list.Add(coverageBO);
                                                                    matchFound = true;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (coverageBO == null) //Coverage not found in P*, add an empty coverage
                        {
                            list.Add(BuildCoverage(null, coverageColumn, policy, lineOfBusiness));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exception newEx = new Exception(string.Format(ex.Message + ". Policy {0}", policy.PolicyNumber), ex.InnerException);
                throw newEx;
            }

            return list;
        }

        private CoverageBO BuildCoverage(Int.PDRCoverage pdrCoverage, CoverageColumn column, Policy policy, CompanyLineOfBusinessEntity lob)
        {
            CoverageBO coverage = new CoverageBO();
            coverage.PolicyNumber = policy.PolicyNumber;
            coverage.PolicyMod = (policy.PolicyMod != int.MinValue) ? policy.PolicyMod.ToString() : string.Empty;

            //class codes have multiple fields for a value
            if (column.CoverageValueType == CoverageValueType.Enum.ClassCodePayrollEmployees && coverage != null)
            {
                // TODO: Rectify
                //coverage.Value = string.Format("{0} - {1}|{2}|{3}",
                //    ReadString(coverage.classCodeCov),
                //    ReadString(coverage.classCodeDesc),
                //    ReadString(coverage.estExposureAmt),
                //    ReadString(""));
            }
            else
            {
                //coverage.Value = (coverage != null) ? ReadString(coverage.limitAmt1) : string.Empty;
            }

            coverage.LineOfBusinessAsString = lob.GetBlcName(column.LineOfBusiness);
            coverage.CompanyAbbreviation = _moImportBuilder.CompanyAbbreviation;

            // The name of the coverage i.e. General Liability
            coverage.CoverageNameType = column.CoverageGroupTypeAsString;

            // A name given to the value itself i.e. Limits
            coverage.CoverageValueType = column.CoverageValueTypeAsString;

            if (column.SubLineOfBusiness != CompanyLineOfBusinessEntity.LineOfBusinessType.Unknown)
            {
                coverage.SubLineOfBusinessAsString = lob.GetBlcName(column.SubLineOfBusiness);
            }

            return coverage;
        }

        public InsuredBO BuildEmptyCoverages(string strBlcLineOfBusiness)
        {
            CompanyLineOfBusinessEntity oLineOfBusiness = _moImportBuilder.LineOfBusinessFieldEntity;

            Policy policy = new Policy() { PolicyId = Guid.NewGuid() };
            policy.PolicySymbol = oLineOfBusiness.GetCompanyLob(strBlcLineOfBusiness);

            _moInsuredBO.CoverageList.AddRange(BuildCoverageList(null, policy));

            return _moInsuredBO;
        }

        internal bool PolicyMatched(Int.PolicySystemResults importResults)
        {
            bool matched = false;

            foreach (Int.Policy policy in importResults.Policies)
            {
                if (policy.Number == _policyNumber && policy.NamedInsureds != null)
                {
                    foreach (Int.PDRNamedInsured namedInsured in policy.NamedInsureds)
                    {
                        if (namedInsured != null)
                            matched = true;
                    }
                }
            }

            return matched;
        }
            
        //private AgencyViewModel BuildAgency(dataset)
        //...

        // BuildRequestFromQuote
        // BuildRequestFromPolicy
        // BuildRequestFromPolicy(Company)
        // BuildRequestFromPolicy(Url)
        // bool PolicyExists
        // Build
        // BuildForAccountStatus
        // BuildAgency(AgencyDO pdrAgency)
        // BuildAgency(APSProxy.agency aspAgency)
        // BuildPolicy
        //...

        #endregion

        #region Read Routines

        private string ReadStreetAddress(string streetNumber, string streetAddress)
        {
            string result = string.Empty;

            //check for a street number
            if (!string.IsNullOrEmpty(streetNumber) && streetNumber.Length > 0)
            {
                result += streetNumber + " ";
            }

            //check for a street address
            if (!string.IsNullOrEmpty(streetAddress) && streetAddress.Length > 0)
            {
                result += streetAddress;
            }

            return result;
        }

        protected string ReadString(object obj)
        {
            return (obj != null) ? obj.ToString().Trim() : string.Empty;
        }

        /// <summary>
        /// Returns an value from the object specified.
        /// </summary>
        /// <param name="amount">The object from which to find the value.</param>
        /// <returns>The value.</returns>
        protected decimal ReadDecimal(object amount)
        {
            decimal result = decimal.MinValue;

            try
            {
                if (amount != null)
                    result = decimal.Parse(amount.ToString(), FormatProvider);
            }
            catch (ArgumentNullException)
            {
                // return decimal.MinValue
            }
            catch (FormatException)
            {
                // return decimal.MinValue
            }
            catch (OverflowException)
            {
                // return decimal.MinValue
            }

            return result;
        }

        /// <summary>
        /// Returns an value from the array, row, and column specified.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The value.</returns>
        protected int ReadInteger(string value)
        {
            int result = int.MinValue;

            if (IsMeaningful(value))
            {
                try
                {
                    result = int.Parse(value, FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // return decimal.MinValue
                }
                catch (FormatException)
                {
                    // return decimal.MinValue
                }
                catch (OverflowException)
                {
                    // return decimal.MinValue
                }
            }

            return result;
        }

        /// <summary>
        /// Returns an value from the array, row, and column specified.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The value.</returns>
        protected Int16 ReadShort(string value)
        {
            Int16 result = Int16.MinValue;

            if (IsMeaningful(value))
            {
                try
                {
                    result = Int16.Parse(value, FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // return Int16.MinValue;
                }
                catch (FormatException)
                {
                    // return Int16.MinValue;
                }
                catch (OverflowException)
                {
                    // return Int16.MinValue;
                }
            }

            return result;
        }

        /// <summary>
        /// Parses a date/time string from the input files and converts it 
        /// into a representative <see cref="DateTime"/> object.
        /// </summary>
        /// <param name="dateTime">The string to parse.</param>
        /// <returns>A DateTime representative of the dateTime.</returns>
        private DateTime ReadDateTime(string dateTime)
        {
            DateTime result = DateTime.MinValue;

            if (IsMeaningful(dateTime))
            {
                try
                {
                    result = Convert.ToDateTime(dateTime, FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // swallow the error and return DateTime.MinValue
                }
                catch (FormatException)
                {
                    try
                    {
                        result = DateTime.ParseExact(dateTime, "yyyyddM", FormatProvider);
                    }
                    catch (FormatException)
                    {
                        try
                        {
                            result = DateTime.ParseExact(dateTime, "yyyyddMM", FormatProvider);
                        }
                        catch (FormatException)
                        {

                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Parses a date string from the input files and converts it 
        /// into a representative <see cref="DateTime"/> object.
        /// </summary>
        /// <param name="date">The string to parse.</param>
        /// <returns>A DateTime representative of the date.</returns>
        private DateTime ReadDate(string date)
        {
            DateTime result = DateTime.MinValue;

            if (IsMeaningful(date))
            {
                try
                {
                    result = DateTime.ParseExact(date, "MM/dd/yyyy", FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // swallow the error and return DateTime.MinValue
                }
                catch (FormatException)
                {
                    // swallow the error and return DateTime.MinValue
                }
            }

            return result;
        }

        //private string ReadUnderwriter(string fullName)
        //{
        //    Underwriter underwriter = Underwriter.GetOne("Name LIKE ? && CompanyID = ?", "%" + fullName + "%", berkleyCompany.ID);

        //    ////try one more time with a broader wild card search in cases of mispellings
        //    //if (underwriter == null)
        //    //{

        //    //}

        //    return (underwriter != null) ? underwriter.Code : fullName;
        //}

        //private string ReadStreetAddress(string streetNumber, string streetAddress)
        //{
        //    string result = string.Empty;

        //    streetNumber = ReadString(streetNumber);
        //    streetAddress = ReadString(streetAddress);

        //    //check for a street number
        //    if (streetNumber.Length > 0)
        //    {
        //        result += streetNumber + " ";
        //    }

        //    //check for a street address
        //    if (streetAddress.Length > 0)
        //    {
        //        result += streetAddress;
        //    }

        //    return result;
        //}

        public static string TrimZipCode(string strZipCode)
        {
            if (strZipCode.Length > 5)
            {
                for (int i = strZipCode.Length - 1; i >= strZipCode.Length - 4; i--)
                {
                    if (strZipCode[i] != '0')
                    {
                        return strZipCode;
                    }
                }
                return strZipCode.Substring(0, 5);
            }
            else
            {
                return strZipCode;
            }
        }

        private string ReadState(string strState)
        {
            strState = ReadString(strState);

            // Only allow valid state codes from the PDR
            //if (strState != string.Empty && State.GetOne("Code = ?", strState) != null)
            //if (strState != string.Empty && 
            //{
            //    return strState;
            //}
            //else
            //{
            //    return string.Empty;
            //}

            return strState;
        }

        private string ReadRateModType(string rateModType)
        {
            if (rateModType == null)
                return string.Empty;

            if (rateModType.ToUpper().Contains("CNTRCT"))
            {
                return "C"; //RateModificationFactorType.Construction.Code;
            }
            else if (rateModType.ToUpper().Contains("EXPER"))
            {
                return "E"; //RateModificationFactorType.Experience.Code;
            }
            else
            {
                return "U"; //RateModificationFactorType.Unknown.Code;
            }
        }

        /// <summary>
        /// Determines whether or not a string has anything in it.
        /// </summary>
        /// <param name="text">The string to test.</param>
        /// <returns>True if the string has content, false otherwise.</returns>
        protected bool IsMeaningful(string text)
        {
            return text != null && text.Trim().Length > 0;
        }

        /// <summary>
        /// Determines whether or not a string has anything in it.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>True if the value has content, false otherwise.</returns>
        protected bool IsMeaningful(int value)
        {
            return value > int.MinValue && value < int.MaxValue;
        }

        /// <summary>
        /// Determines whether or not a decimal has anything in it.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>True if the value has content, false otherwise.</returns>
        protected bool IsMeaningful(decimal value)
        {
            return value > decimal.MinValue && value < decimal.MaxValue;
        }

        /// <summary>
        /// Determines whether or not a value has any meaningful content.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>True if the value has content, false otherwise.</returns>
        protected bool IsMeaningful(Array value)
        {
            return value != null && value.Length > 0;
        }

        /// <summary>
        /// Returns a format provider useful for string parsing.
        /// </summary>
        protected static IFormatProvider FormatProvider
        {
            get
            {
                return System.Globalization.CultureInfo.InvariantCulture;
            }
        }

        private int StripNonNumericChars(string number)
        {
            string result = string.Empty;

            char[] characters = number.ToCharArray();
            foreach (char character in characters)
            {
                if (Char.IsDigit(character))
                {
                    result += character;
                }
            }

            return !string.IsNullOrEmpty(result) ? Convert.ToInt32(result) : Int32.MinValue;
        }
        #endregion
    }
}
