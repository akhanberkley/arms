﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    /// <summary>
    /// Summary description for IImportBuilder.
    /// </summary>
    public interface IImportBuilder
    {
        StoredProcedureBO GetStoredProcedure();
        StoredProcedureBO GetStoredProcedure(CompanyStoredProcedureEntity.SpType eType);

        CompanyPolicyEntity PolicyFieldEntity { get; }
        CompanyInsuredEntity InsuredFieldEntity { get; }
        CompanyAgencyEntity AgencyFieldEntity { get; }
        CompanyLocationEntity LocationFieldEntity { get; }
        CompanyBuildingEntity BuildingFieldEntity { get; }
        CompanyCoverageEntity CoverageFieldEntity { get; }
        CompanyClaimEntity ClaimFieldEntity { get; }
        CompanyRateModFactorEntity RateModFactorFieldEntity { get; }
        CompanyLineOfBusinessEntity LineOfBusinessFieldEntity { get; }

        string CompanyAbbreviation { get; }

        InsuredBO BuildComplete(InsuredBO oInsuredBO);
        List<CoverageColumn> GetCoverage(CompanyLineOfBusinessEntity.LineOfBusinessType eType);
        int GetMaxCoverageColumns();
        int GetMaxColumnNumber();
    }
}