﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class InsuredBO
    {
        private string mstrOwner = string.Empty;
        private string mstrOwnerPhone = string.Empty;
        private string mstrOwnerAltPhone = string.Empty;
        private string mstrOwnerFax = string.Empty;
        private string mstrOwnerEmail = string.Empty;
        private string mstrOwnerTitle = string.Empty;
        private string mstrAgencyError = string.Empty;
        private string mstrPreviousClientId = string.Empty;
        private Insured moInsured;
        private Agency moAgency;

        // Used by Migration
        private bool mfFoundInDb = false;

        private List<Location> moPolicyLocationList = new List<Location>();
        private List<LocationContact> moLocationContactList = new List<LocationContact>();
        private List<Policy> moPolicyList = new List<Policy>();
        private List<BuildingBO> moBuildingBOList = new List<BuildingBO>();
        private List<ClaimBO> moClaimBOList = new List<ClaimBO>();
        private List<RateModFactorBO> moRateModFactorBOList = new List<RateModFactorBO>();
        private CoverageBO moCoverageBO = new CoverageBO();
        private List<CoverageBO> moCoverageBOList = new List<CoverageBO>();
        private List<AgencyEmail> moAgencyEmailList = new List<AgencyEmail>();

        private Policy[] mroExistingPolicies = null;
        private Location[] mroExistingLocations = null;

        public InsuredBO()
        {
        }

        #region Members
        /// <summary>
        /// Set the PreviousClientId to instruct Reconcile to update the ClientId
        /// </summary>
        public string PreviousClientId
        {
            get { return mstrPreviousClientId; }
            set { mstrPreviousClientId = value; }
        }

        //public string AgencyError
        //{
        //    get { return mstrAgencyError; }
        //    set { mstrAgencyError = value; }
        //}

        //#region Existing Entities
        public Policy[] ExistingPolicies
        {
            get { return mroExistingPolicies; }
            set { mroExistingPolicies = value; }
        }

        public Location[] ExistingLocations
        {
            get { return mroExistingLocations; }
            set { mroExistingLocations = value; }
        }
        //#endregion

        public List<CoverageBO> CoverageList
        {
            get { return moCoverageBOList; }
        }

        public List<ClaimBO> ClaimList
        {
            get { return moClaimBOList; }
        }

        public List<RateModFactorBO> RateModFactorList
        {
            get { return moRateModFactorBOList; }
        }

        /// <summary>
        /// Returned from the Insured Stored Procedure - goes in the LocationContact BLC Table
        /// </summary>
        public string Owner
        {
            get { return mstrOwner; }
            set { mstrOwner = value; }
        }

        // Migrated
        public string OwnerTitle
        {
            get { return mstrOwnerTitle; }
            set { mstrOwnerTitle = value; }
        }

        /// <summary>
        /// Returned from the Insured Stored Procedure - goes in the LocationContact BLC Table
        /// </summary>
        public string OwnerPhone
        {
            get { return mstrOwnerPhone; }
            set { mstrOwnerPhone = value; }
        }

        /// <summary>
        /// Returned from the Insured Stored Procedure - goes in the LocationContact BLC Table
        /// </summary>
        public string OwnerAltPhone
        {
            get { return mstrOwnerAltPhone; }
            set { mstrOwnerAltPhone = value; }
        }

        /// <summary>
        /// Returned from the Insured Stored Procedure - goes in the LocationContact BLC Table
        /// </summary>
        public string OwnerFax
        {
            get { return mstrOwnerFax; }
            set { mstrOwnerFax = value; }
        }

        /// <summary>
        /// Returned from the Insured Stored Procedure - goes in the LocationContact BLC Table
        /// </summary>
        public string OwnerEmail
        {
            get { return mstrOwnerEmail; }
            set { mstrOwnerEmail = value; }
        }

        public Insured InsuredEntity
        {
            get { return moInsured; }
            set { moInsured = value; }
        }

        public Agency AgencyEntity
        {
            get { return moAgency; }
            set { moAgency = value; }
        }

        public List<AgencyEmail> AgencyEmailList
        {
            get { return moAgencyEmailList; }
        }

        public List<Location> PolicyLocationList
        {
            get { return moPolicyLocationList; }
        }

        public List<LocationContact> LocationContactList
        {
            get { return moLocationContactList; }
        }

        public List<Policy> PolicyList
        {
            get { return moPolicyList; }
        }

        public List<BuildingBO> BuildingList
        {
            get { return moBuildingBOList; }
        }

        // For Migration
        public bool FoundInDb
        {
            get { return mfFoundInDb; }
            set { mfFoundInDb = value; }
        }
        #endregion

        #region Methods
        public void SetInsuredId(Guid oInsuredId)
        {
            moInsured = new Insured() { InsuredId = new Guid(oInsuredId.ToString()) };
        }

        // BLC Buildings set by the Base Builder
        public List<BuildingBO> GetBuildingListByLocation(Location oLocation)
        {
            List<BuildingBO> oReturnList = new List<BuildingBO>();

            foreach (BuildingBO oBuilding in moBuildingBOList)
            {
                //company specif hack for A+Plus
                //if (this.InsuredEntity.CompanyID == Company.ContinentalWesternGroup.ID)
                //{
                //    if (oLocation.Number == oBuilding.LocationAsInt)
                //    {
                //        oReturnList.Add(oBuilding);
                //    }
                //}
                //else
                //{
                    // Use the Policy KEY to find the Buildings for a Policy
                    if (oLocation.PolicySystemKey == oBuilding.PolicySystemKey)
                    {
                        oReturnList.Add(oBuilding);
                    }
                //}
            }
            return oReturnList;
        }
        #endregion
    }
}
