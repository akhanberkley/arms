﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class ClaimBO
    {
        private Claim moClaim;
        private string mstrPolicyNumber;
        private int miPolicyMod;

        public ClaimBO()
        {
        }

        public Claim Entity
        {
            get { return moClaim; }
            set { moClaim = value; }
        }

        public string PolicyNumber
        {
            get { return mstrPolicyNumber; }
            set { mstrPolicyNumber = value; }
        }

        public int PolicyMod
        {
            get { return miPolicyMod; }
            set { miPolicyMod = value; }
        }

    }
}
