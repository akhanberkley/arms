﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class LocationBO : Location
    {
        private int _publicProtection = Int32.MinValue;
        private short _hasSprinklerSystem = Int16.MinValue;
        
        public LocationBO()
            : base()
        {
            base.LocationId = Guid.NewGuid();
        }

        #region Members
        /// <summary>
        /// Gets or sets the public protection for the location that will apply to the buildings at that location.
        /// </summary>
        public int PublicProtection
        {
            get
            {
                return _publicProtection;
            }
            set
            {
                _publicProtection = value;
            }
        }

        /// <summary>
        /// Gets or sets the has sprinklery system flag for the location that will apply to the buildings at that location.
        /// </summary>
        public short HasSprinklerSystem
        {
            get
            {
                return _hasSprinklerSystem;
            }
            set
            {
                _hasSprinklerSystem = value;
            }
        }

        #endregion
    }
}
