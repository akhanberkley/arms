﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ImportServices
{
    public class StoredProcedureBO
    {
        private CompanyStoredProcedureEntityList moSpList;
        private Dictionary<CompanyStoredProcedureEntity.SpType, DataTable> moDictionary = new Dictionary<CompanyStoredProcedureEntity.SpType, DataTable>();

        public StoredProcedureBO(CompanyStoredProcedureEntityList oSpList)
        {
            moSpList = oSpList;
            //BuildTables();
        }

        public StoredProcedureBO(CompanyStoredProcedureEntityList oSpList, CompanyStoredProcedureEntity.SpType eType)
        {
            moSpList = oSpList;
            //BuildOneTable(eType);
        }

        #region Members
        public Dictionary<CompanyStoredProcedureEntity.SpType, DataTable> DataTables
        {
            get
            {
                return moDictionary;
            }
        }
        #endregion

        #region Methods
        public DataTable GetTable(CompanyStoredProcedureEntity.SpType eType)
        {
            if (moDictionary.ContainsKey(eType))
            {
                return moDictionary[eType];
            }
            else
            {
                return null;
            }
        }

        private void BuildOneTable(CompanyStoredProcedureEntity.SpType eType)
        {
            DataTable oDataTable;
            foreach (CompanyStoredProcedureEntity oCompanyEntity in moSpList)
            {
                if (oCompanyEntity.CompanySpType == eType)
                {
                    //switch (oCompanyEntity.CompanySpType)
                    //{
                    //    case CompanyStoredProcedureEntity.SpType.Agency:
                    //        oDataTable = OleDbImportSprocs(oCompanyEntity.Name, oCompanyEntity.ConnectionString, oCompanyEntity.PolicyNumber, oCompanyEntity.ClientId, oCompanyEntity.AgentNumber);
                    //        break;

                    //    case CompanyStoredProcedureEntity.SpType.Insured:
                    //    case CompanyStoredProcedureEntity.SpType.RateModFactor:
                    //        oDataTable = OleDbImportSprocs(oCompanyEntity.Name, oCompanyEntity.ConnectionString, oCompanyEntity.PolicyNumber);
                    //        break;

                    //    default:
                    //        oDataTable = OleDbImportSprocs(oCompanyEntity.Name, oCompanyEntity.ConnectionString, oCompanyEntity.PolicyNumber, oCompanyEntity.ClientId);
                    //        break;
                    //}
                    //moDictionary[oCompanyEntity.CompanySpType] = oDataTable;
                    //break;
                }
            }
        }

        private void BuildTables()
        {
            DataTable oDataTable;
            foreach (CompanyStoredProcedureEntity oCompanyEntity in moSpList)
            {
                //switch (oCompanyEntity.CompanySpType)
                //{
                //    case CompanySpEntity.SpType.Agency:
                //        oDataTable = OleDbImportSprocs(oCompanyEntity.Name, oCompanyEntity.ConnectionString, oCompanyEntity.PolicyNumber, oCompanyEntity.ClientId, oCompanyEntity.AgentNumber);
                //        break;

                //    case CompanyStoredProcedureEntity.SpType.Insured:
                //    case CompanyStoredProcedureEntity.SpType.RateModFactor:
                //        oDataTable = OleDbImportSprocs(oCompanyEntity.Name, oCompanyEntity.ConnectionString, oCompanyEntity.PolicyNumber);
                //        break;

                //    default:
                //        oDataTable = OleDbImportSprocs(oCompanyEntity.Name, oCompanyEntity.ConnectionString, oCompanyEntity.PolicyNumber, oCompanyEntity.ClientId);
                //        break;
                //}
                //moDictionary[oCompanyEntity.CompanySpType] = oDataTable;
            }
        }

        /// <summary>
        /// Wrapper for stored procedure 'OleDbImportSprocs'.
        /// </summary>
        /// <remarks>Generated on 7/6/2006</remarks>
        /// <param name="spName">The name of the stored procedure.</param>
        /// <param name="connStr">The connection string.</param>
        /// <param name="policyNumber">Value for parameter '@policy'.</param>
        /// <param name="clientID">Value for parameter '@client'.</param>
        public static DataTable OleDbImportSprocs(string spName, string connStr, string policyNumber, string clientID, string strAgencyNumber)
        {
            try
            {
                DataTable dt = new DataTable();

                //if (policyNumber.Length == 0 && clientID.Length == 0 && strAgencyNumber.Length == 0)
                //{
                //    return dt;
                //}

                //using (OleDbConnection conn = new OleDbConnection(connStr))
                //{
                //    OleDbCommand cmd = conn.CreateCommand();
                //    cmd.CommandText = spName;
                //    cmd.CommandType = CommandType.StoredProcedure;

                //    //paramters
                //    OleDbParameter paramPolicyNumber = cmd.CreateParameter();
                //    paramPolicyNumber.Direction = ParameterDirection.Input;
                //    paramPolicyNumber.ParameterName = "@policy";
                //    paramPolicyNumber.OleDbType = OleDbType.VarChar;
                //    paramPolicyNumber.Value = policyNumber;

                //    OleDbParameter paramClientID = cmd.CreateParameter();
                //    paramClientID.Direction = ParameterDirection.Input;
                //    paramClientID.ParameterName = "@client";
                //    paramClientID.OleDbType = OleDbType.VarChar;
                //    paramClientID.Value = clientID;

                //    OleDbParameter paramAgencyNumber = cmd.CreateParameter();
                //    paramAgencyNumber.Direction = ParameterDirection.Input;
                //    paramAgencyNumber.ParameterName = "@agency";
                //    paramAgencyNumber.OleDbType = OleDbType.VarChar;
                //    paramAgencyNumber.Value = strAgencyNumber;

                //    cmd.Parameters.Add(paramPolicyNumber);
                //    cmd.Parameters.Add(paramClientID);
                //    cmd.Parameters.Add(paramAgencyNumber);

                //    OleDbDataAdapter ad = new OleDbDataAdapter(cmd);

                //    ad.Fill(dt);
                //    cmd.Dispose();
                //}

                return dt;
            }
            catch (Exception oException)
            {
                StringBuilder oBuilder = new StringBuilder();
                oBuilder.Append("Error when calling Store Procedure.  Stored Procedure Name=");
                oBuilder.Append(spName);
                oBuilder.Append(" Policy Number=");
                oBuilder.Append(policyNumber);
                oBuilder.Append(" Client ID=");
                oBuilder.Append(clientID);
                oBuilder.Append(" Error=");
                oBuilder.Append(oException.Message);
                //throw new BlcImportException(oBuilder.ToString());
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Wrapper for stored procedure 'OleDbImportSprocs'.
        /// </summary>
        /// <remarks>Generated on 7/6/2006</remarks>
        /// <param name="spName">The name of the stored procedure.</param>
        /// <param name="connStr">The connection string.</param>
        /// <param name="policyNumber">Value for parameter '@policy'.</param>
        /// <param name="clientID">Value for parameter '@client'.</param>
        public static DataTable OleDbImportSprocs(string spName, string connStr, string policyNumber, string clientID)
        {
            try
            {
                DataTable dt = new DataTable();

                //if (policyNumber.Length == 0 && clientID.Length == 0)
                //{
                //    return dt;
                //}

                //using (OleDbConnection conn = new OleDbConnection(connStr))
                //{
                //    OleDbCommand cmd = conn.CreateCommand();
                //    cmd.CommandText = spName;
                //    cmd.CommandType = CommandType.StoredProcedure;

                //    //paramters
                //    OleDbParameter paramPolicyNumber = cmd.CreateParameter();
                //    paramPolicyNumber.Direction = ParameterDirection.Input;
                //    paramPolicyNumber.ParameterName = "@policy";
                //    paramPolicyNumber.OleDbType = OleDbType.VarChar;
                //    paramPolicyNumber.Value = policyNumber;

                //    OleDbParameter paramClientID = cmd.CreateParameter();
                //    paramClientID.Direction = ParameterDirection.Input;
                //    paramClientID.ParameterName = "@client";
                //    paramClientID.OleDbType = OleDbType.VarChar;
                //    paramClientID.Value = clientID;

                //    cmd.Parameters.Add(paramPolicyNumber);
                //    cmd.Parameters.Add(paramClientID);

                //    OleDbDataAdapter ad = new OleDbDataAdapter(cmd);

                //    ad.Fill(dt);
                //    cmd.Dispose();
                //}

                return dt;
            }
            catch (Exception oException)
            {
                StringBuilder oBuilder = new StringBuilder();
                oBuilder.Append("Error when calling Store Procedure.  Stored Procedure Name=");
                oBuilder.Append(spName);
                oBuilder.Append(" Policy Number=");
                oBuilder.Append(policyNumber);
                oBuilder.Append(" Client ID=");
                oBuilder.Append(clientID);
                oBuilder.Append(" Error=");
                oBuilder.Append(oException.Message);
                //throw new BlcImportException(oBuilder.ToString());
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Wrapper for stored procedure 'OleDbImportSprocs'.
        /// </summary>
        /// <remarks>Generated on 7/6/2006</remarks>
        /// <param name="spName">The name of the stored procedure.</param>
        /// <param name="connStr">The connection string.</param>
        /// <param name="policyNumber">Value for parameter '@policy'.</param>
        public static DataTable OleDbImportSprocs(string spName, string connStr, string policyNumber)
        {
            try
            {
                DataTable dt = new DataTable();

                //if (policyNumber.Length == 0)
                //{
                //    return dt;
                //}

                //using (OleDbConnection conn = new OleDbConnection(connStr))
                //{
                //    OleDbCommand cmd = conn.CreateCommand();
                //    cmd.CommandText = spName;
                //    cmd.CommandType = CommandType.StoredProcedure;

                //    //paramters
                //    OleDbParameter paramPolicyNumber = cmd.CreateParameter();
                //    paramPolicyNumber.Direction = ParameterDirection.Input;
                //    paramPolicyNumber.ParameterName = "@policy";
                //    paramPolicyNumber.OleDbType = OleDbType.VarChar;
                //    paramPolicyNumber.Value = policyNumber;

                //    cmd.Parameters.Add(paramPolicyNumber);

                //    OleDbDataAdapter ad = new OleDbDataAdapter(cmd);

                //    ad.Fill(dt);
                //    cmd.Dispose();
                //}

                return dt;
            }
            catch (Exception oException)
            {
                StringBuilder oBuilder = new StringBuilder();
                oBuilder.Append("Error when calling Store Procedure.  Stored Procedure Name=");
                oBuilder.Append(spName);
                oBuilder.Append(" Policy Number=");
                oBuilder.Append(policyNumber);
                oBuilder.Append(" Error=");
                oBuilder.Append(oException.Message);
                //throw new BlcImportException(oBuilder.ToString());
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Wrapper for stored procedure 'SQLDbImportSprocs'.
        /// </summary>
        /// <param name="spName">The name of the stored procedure.</param>
        /// <param name="connStr">The connection string.</param>
        /// <param name="policyNumber">Id value for the sp.</param>
        public static DataSet SQLDbImportSprocs(string spName, string connStr, string id)
        {
            try
            {
                if (string.IsNullOrEmpty(spName))
                {
                    throw new ArgumentNullException("spName parameter for the stored procedure cannot be null");
                }

                if (string.IsNullOrEmpty(connStr))
                {
                    throw new ArgumentNullException("connStr parameter for the stored procedure cannot be null");
                }

                if (string.IsNullOrEmpty(id))
                {
                    throw new ArgumentNullException("Id parameter for the stored procedure cannot be null");
                }

                DataSet ds = new DataSet();

                //using (SqlConnection conn = new SqlConnection(connStr))
                //{
                //    SqlCommand cmd = conn.CreateCommand();
                //    cmd.CommandText = spName;
                //    cmd.CommandType = CommandType.StoredProcedure;

                //    //paramters
                //    SqlParameter param = cmd.CreateParameter();
                //    param.Direction = ParameterDirection.Input;
                //    param.ParameterName = "@id";
                //    param.SqlDbType = SqlDbType.VarChar;
                //    param.Value = id;

                //    cmd.Parameters.Add(param);

                //    SqlDataAdapter ad = new SqlDataAdapter(cmd);

                //    ad.Fill(ds);
                //    cmd.Dispose();
                //}

                return ds;
            }
            catch (Exception oException)
            {
                StringBuilder oBuilder = new StringBuilder();
                oBuilder.Append("Error when calling Store Procedure.  Stored Procedure Name=");
                oBuilder.Append(spName);
                oBuilder.Append(" id=");
                oBuilder.Append(id);
                oBuilder.Append(" Error=");
                oBuilder.Append(oException.Message);
                //throw new BlcImportException(oBuilder.ToString());
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}
