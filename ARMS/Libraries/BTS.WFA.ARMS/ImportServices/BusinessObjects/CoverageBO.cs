﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class CoverageBO
    {
        private string mstrLineOfBusiness = "";
        private string mstrSubLineOfBusiness = "";
        private string mstrCompanyAbbreviation = "";
        private string mstrCoverageValueType = "";
        private string mstrCoverageNameType = "";
        private string mstrPolicyNumber = "";
        private string mstrPolicyMod = "";
        private string mstrValue = "";
        private bool mfReport = false;
        private List<CoverageDetail> mlsCoverageDetail = null;

        public CoverageBO()
        {
        }

        #region Members
        public string Value
        {
            get { return mstrValue; }
            set { mstrValue = value; }
        }

        public string LineOfBusinessAsString
        {
            get { return mstrLineOfBusiness; }
            set { mstrLineOfBusiness = value; }
        }

        public string SubLineOfBusinessAsString
        {
            get { return mstrSubLineOfBusiness; }
            set { mstrSubLineOfBusiness = value; }
        }

        public string CompanyAbbreviation
        {
            get { return mstrCompanyAbbreviation; }
            set { mstrCompanyAbbreviation = value; }
        }

        public string CoverageValueType
        {
            get { return mstrCoverageValueType; }
            set { mstrCoverageValueType = value; }
        }

        public string CoverageNameType
        {
            get { return mstrCoverageNameType; }
            set { mstrCoverageNameType = value; }
        }

        public string PolicyNumber
        {
            get { return mstrPolicyNumber; }
            set { mstrPolicyNumber = value; }
        }

        public string PolicyMod
        {
            get { return mstrPolicyMod; }
            set { mstrPolicyMod = value; }
        }

        public List<CoverageDetail> CoverageDetails
        {
            get { return mlsCoverageDetail; }
            set { mlsCoverageDetail = value; }
        }

        // Used by the Migration code
        public bool Report
        {
            get { return mfReport; }
            set { mfReport = value; }
        }
        #endregion

        #region Methods
        public Coverage CreateCoverage(Guid oCoverageId, Guid oInsuredId, Guid oCoverageNameId, Guid oPolicyId)
        {
            Coverage oNewCoverage = new Coverage();
            oNewCoverage.CoverageId = oCoverageId;
            oNewCoverage.InsuredId = oInsuredId;
            oNewCoverage.CoverageNameId = oCoverageNameId;
            oNewCoverage.CoverageTypeCode = mstrCoverageValueType;
            oNewCoverage.PolicyId = oPolicyId;
            oNewCoverage.CoverageValue = mstrValue;

            return oNewCoverage;
        }

        public List<CoverageDetail> CreateCoverageDetail(Guid oCoverageId, Guid oInsuredId, Guid? oPolicyId, List<CoverageDetail> oDetails)
        {
            List<CoverageDetail> cdList = new List<CoverageDetail>();
            foreach (CoverageDetail cd in oDetails)
            {
                CoverageDetail oNewCoverageDetail = new CoverageDetail();
                oNewCoverageDetail.CoverageDetailId = Guid.NewGuid();
                oNewCoverageDetail.CoverageId = oCoverageId;
                oNewCoverageDetail.InsuredId = oInsuredId;
                oNewCoverageDetail.PolicyId = oPolicyId;
                oNewCoverageDetail.DateCreated = DateTime.Now;
                oNewCoverageDetail.CoverageDetailValue = cd.CoverageDetailValue;
                cdList.Add(oNewCoverageDetail);
            }
            return cdList;
        }

        public override string ToString()
        {
            StringBuilder oBuilder = new StringBuilder();
            oBuilder.Append("Line of business=");
            oBuilder.Append(mstrLineOfBusiness);
            oBuilder.Append(" SubLine of business=");
            oBuilder.Append(mstrSubLineOfBusiness);

            oBuilder.Append(" Company=");
            oBuilder.Append(mstrCompanyAbbreviation);
            oBuilder.Append(" Coverage value type=");
            oBuilder.Append(mstrCoverageValueType);
            oBuilder.Append(" Coverage name type=");
            oBuilder.Append(mstrCoverageNameType);

            return oBuilder.ToString();
        }
        #endregion

    }
}
