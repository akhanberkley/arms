﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class BuildingBO : Building
    {
        private int miLocation = -1;
        private string mstrPolicySystemKey = "";

        public BuildingBO()
            : base()
        {
            base.BuildingId = Guid.NewGuid();
        }

        #region Members
        public string PolicySystemKey
        {
            get { return mstrPolicySystemKey; }
            set { mstrPolicySystemKey = value; }
        }

        public int LocationAsInt
        {
            get { return miLocation; }
            set { miLocation = value; }
        }
        #endregion

        #region Methods
        //public static decimal DigitsFromString(string strRhs, ref bool fHasTextPart)
        //{
        //    string[] rstrParts = strRhs.Split(' ', '/','\\');
        //    bool[] rfDollarPart = new bool[rstrParts.Length];
        //    List<string> oNumbersList = new List<string>();
        //    List<string> oDollarsList = new List<string>();
        //    int iCount = 0;
        //    string strDigitString = "";

        //    fHasTextPart = rstrParts.Length > 1;

        //    // Multiple parts can have numbers, grab the part that has a $
        //    foreach (string strPart in rstrParts)
        //    {
        //        if (strPart.Contains("$"))
        //        {
        //            rfDollarPart[iCount] = true;
        //        }
        //        else
        //        {
        //            rfDollarPart[iCount] = false;
        //        }
        //        iCount++;
        //    }

        //    iCount = 0;
        //    foreach (string strPart in rstrParts)
        //    {
        //        strDigitString = "";
        //        foreach (char c in strPart)
        //        {
        //            if (Char.IsDigit(c))
        //            {
        //                strDigitString += c;
        //            }
        //        }
        //        if (strDigitString.Length > 0)
        //        {
        //            if(rfDollarPart[iCount] == true)
        //            {
        //                oDollarsList.Add(strDigitString);
        //            }
        //            else
        //            {
        //                oNumbersList.Add(strDigitString);
        //            }
        //        }
        //        iCount++;
        //    }
        //    if (oDollarsList.Count > 0)
        //    {
        //        decimal iHighest = 0;
        //        decimal iCurrent;

        //        foreach (string strFigure in oDollarsList)
        //        {
        //            iCurrent = Convert.ToDecimal(strFigure);

        //            if (iCurrent > iHighest)
        //            {
        //                iHighest = iCurrent;
        //            }
        //        }
        //        return iHighest;
        //    }
        //    else if (oNumbersList.Count > 0)
        //    {
        //        decimal iHighest = 0;
        //        decimal iCurrent;

        //        foreach (string strFigure in oNumbersList)
        //        {
        //            iCurrent = Convert.ToDecimal(strFigure);

        //            if (iCurrent > iHighest)
        //            {
        //                iHighest = iCurrent;
        //            }
        //        }
        //        return iHighest;
        //    }
        //    else
        //    {
        //        return decimal.MinValue;
        //    }
        //}
        #endregion
    }
}
