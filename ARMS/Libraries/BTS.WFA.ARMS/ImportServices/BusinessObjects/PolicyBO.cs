﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class PolicyBO
    {
        private Policy Policy;
        private Insured Insured;
        private Agency Agency;

        private List<Location> moPolicyLocationList = new List<Location>();
        private List<LocationContact> moLocationContactList = new List<LocationContact>();
        //private List<Policy> moPolicyList = new List<Policy>();
        
        private List<BuildingBO> moBuildingList = new List<BuildingBO>();
        private List<ClaimBO> moClaimList = new List<ClaimBO>();
        private List<RateModFactorBO> moRateModFactorList = new List<RateModFactorBO>();
        private List<Coverage> moCoverageList = new List<Coverage>();

        #region from 2013
        //private BlcCoverage moBlcCoverage = new BlcCoverage();
        //private BlcCoverageList<BlcCoverage> moCoverageList = new BlcCoverageList<BlcCoverage>();
        //private List<AgencyEmail> moAgencyEmailList = new List<AgencyEmail>();

        //private Policy[] mroPolicies = null;
        //private Location[] mroLocations = null;
        #endregion

        public PolicyBO()
        {
        }

        #region members
        #endregion

        #region methods
        #endregion
    }
}
