﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class RateModFactorBO
    {
        private RateModificationFactor moRateModFactor;
        private string mstrPolicyNumber;
        private int miPolicyMod;

        public RateModFactorBO()
        {
        }

        public RateModificationFactor Entity
        {
            get { return moRateModFactor; }
            set { moRateModFactor = value; }
        }

        public string PolicyNumber
        {
            get { return mstrPolicyNumber; }
            set { mstrPolicyNumber = value; }
        }

        public int PolicyMod
        {
            get { return miPolicyMod; }
            set { miPolicyMod = value; }
        }
    }
}
