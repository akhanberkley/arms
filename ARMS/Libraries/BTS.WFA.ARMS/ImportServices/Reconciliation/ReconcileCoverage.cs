﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class ReconcileCoverage
    {
        private InsuredBO moInsuredBO;
        private CompanyDetail moCompany;
        private IArmsDb moDb;
        private CoverageNameDictionary moCoverageNameDict;
        private List<Guid> policiesToDelete = new List<Guid>();
        private List<Coverage> moAddList = new List<Coverage>();
        private List<CoverageDetail> moCovDetailList = new List<CoverageDetail>();
        /*
        // The Hybrid Approach tried to only update, delete, and add the Coverages as appropriate
        // The problem was that some (as it turns out most) coverages share their LOB, Sub-LOB, and Coverage Type Name
        // and therefore need to be deleted and re-added.  It turns out that it is faster to 
        // do a delete by InsuredId and then re-add all of the coverages.
        */
        #region Hybrid Approach
        //private Dictionary<int, BlcCoverage> moAddBlcCoverages = new Dictionary<int, BlcCoverage>();
        //private Dictionary<Guid, VWCoverage> moDeleteList = new Dictionary<Guid, VWCoverage>();
        //private List<Coverage> moUpdateList = new List<Coverage>();
        //private BlcCoverageList<VWCoverage> moFoundBlcCoverageList = new BlcCoverageList<VWCoverage>();
        #endregion

        public ReconcileCoverage(IArmsDb db, CompanyDetail oCompany, InsuredBO oInsuredBO)
        {
            moCoverageNameDict = new CoverageNameDictionary();
            moDb = db;
            moCompany = oCompany;
            moInsuredBO = oInsuredBO;
        }

        #region Members
        #endregion

        #region Methods
        public void Reconcile(List<CoverageBO> oCoverageBOList, Hashtable existingPolicies, ReconcilePolicy oReconcilePolicy)
        {
            Guid oInsuredId = moInsuredBO.InsuredEntity.InsuredId;
            moCoverageNameDict.Load(moCompany);
            bool fFound;
            List<CoverageBO> oFoundList = new List<CoverageBO>();
            List<CoverageBO> oStartList = new List<CoverageBO>();
            //CoverageBO oCoverageBO;
            
            Coverage[] roCoverages = moDb.Coverages.Where(c => c.InsuredId == oInsuredId).ToArray(); // returning 0
            //VWCoverage[] roCoverages = VWCoverage.GetArray("InsuredID = ?", oInsuredId);
            
            // Convert the Coverage View rows (read only) into BlcCoverage so we can Trim()
            foreach (Coverage oCoverage in roCoverages)
            {
                CoverageBO nCoverageBO = new CoverageBO();
                nCoverageBO.CoverageNameType = oCoverage.CoverageName.CoverageNameTypeCode.Trim();
                nCoverageBO.CoverageValueType = oCoverage.CoverageTypeCode.Trim();
                nCoverageBO.LineOfBusinessAsString = oCoverage.CoverageName.LineOfBusinessCode.Trim();
                nCoverageBO.SubLineOfBusinessAsString = oCoverage.CoverageName.SubLineOfBusinessCode.Trim();
                nCoverageBO.PolicyNumber = oCoverage.Policy.PolicyNumber;
                nCoverageBO.PolicyMod = oCoverage.Policy.PolicyMod.ToString();
                nCoverageBO.Value = oCoverage.CoverageValue.Trim();
                oStartList.Add(nCoverageBO);
            }
            #region from 2013
            //foreach (VWCoverage oCoverage in roCoverages)
            //{
            //    oBlcCoverage = new BlcCoverage();
            //    oBlcCoverage.CoverageNameType = oCoverage.CoverageNameTypeCode == null ? String.Empty : oCoverage.CoverageNameTypeCode.Trim();
            //    oBlcCoverage.CoverageValueType = oCoverage.CoverageTypeCode == null ? String.Empty : oCoverage.CoverageTypeCode.Trim();
            //    oBlcCoverage.LineOfBusinessAsString = oCoverage.LineOfBusinessCode == null ? String.Empty : oCoverage.LineOfBusinessCode.Trim();
            //    oBlcCoverage.SubLineOfBusinessAsString = oCoverage.SubLineOfBusinessCode == null ? String.Empty : oCoverage.SubLineOfBusinessCode.Trim();
            //    oBlcCoverage.PolicyNumber = oCoverage.PolicyNumber;
            //    oBlcCoverage.PolicyMod = Convert.ToString(oCoverage.PolicyMod);
            //    oBlcCoverage.Value = oCoverage.CoverageValue == null ? String.Empty : oCoverage.CoverageValue.Trim();
            //    oStartList.Add(oBlcCoverage);
            //}
            #endregion

            // If we have the same number of rows coming in as we do in the DB
            // and all of criteria are the same, skip touching the DB
            foreach (CoverageBO coverageBO in oCoverageBOList)
            {
                fFound = false;
                foreach (CoverageBO oIteratedCoverage in oStartList.ToArray())
                {
                    if (coverageBO.CoverageNameType == oIteratedCoverage.CoverageNameType)
                    {
                        if (coverageBO.CoverageValueType == oIteratedCoverage.CoverageValueType)
                        {
                            if (coverageBO.LineOfBusinessAsString == oIteratedCoverage.LineOfBusinessAsString)
                            {
                                if (coverageBO.SubLineOfBusinessAsString == oIteratedCoverage.SubLineOfBusinessAsString)
                                {
                                    if (coverageBO.PolicyNumber == oIteratedCoverage.PolicyNumber)
                                    {
                                        if (coverageBO.PolicyMod == oIteratedCoverage.PolicyMod)
                                        {
                                            if (coverageBO.Value == oIteratedCoverage.Value)
                                            {
                                                // Remove the item from the list of CoverageBO that we started with
                                                oStartList.Remove(oIteratedCoverage);
                                                // The found list should end up with all of the CoverageBO converted from the VWCoverage DB items
                                                oFoundList.Add(coverageBO);
                                                fFound = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (!fFound)
                {
                    break;
                }
            }

            #region Hybrid Approach
            //List<BlcCoverage> oBlcCoverages;
            //VWCoverage[] roCoverages = VWCoverage.GetArray("InsuredID = ?", oInsuredId);
            //CoverageKey oCoverageKey;
            //bool fRemoveExisting;

            //if (roCoverages.Length > 0)
            //{
            //    foreach (VWCoverage oVwCoverage in roCoverages)
            //    {
            //        oCoverageKey = new CoverageKey(oVwCoverage);

            //        // If we end up with more than one entry in a coverage "group", we need to delete and insert
            //        fRemoveExisting = RemoveCoveragesInTheSameGroup(oCoverageKey, oVwCoverage);

            //        // If the count if different, clear these entries out and start over
            //        // This is critical as the LOB, Sub-LOB, ValueType, and NameType can be reused.
            //        oBlcCoverages = oRhsBlcCoverageList.Get(oCoverageKey);
            //        if (oBlcCoverages != null)
            //        {
            //            if(fRemoveExisting)
            //            {
            //                // The old have been removed, add the new for sure
            //                foreach (BlcCoverage oBlcCoverage in oBlcCoverages)
            //                {
            //                    // Only add it once - could duplicate if CoverageKey.Key is duplicated in DB Coverage table which can happen
            //                    moAddBlcCoverages[oBlcCoverage.GetHashCode()] = oBlcCoverage; 
            //                }
            //            }
            //            else if(oBlcCoverages.Count > 1)
            //            {
            //                foreach (BlcCoverage oBlcCoverage in oBlcCoverages)
            //                {
            //                    // Only add it once - could duplicate if CoverageKey.Key is duplicated in DB Coverage table which can happen
            //                    moAddBlcCoverages[oBlcCoverage.GetHashCode()] = oBlcCoverage; 
            //                }
            //                moDeleteList[oVwCoverage.CoverageID] = oVwCoverage;
            //            }
            //            else if(oBlcCoverages.Count == 1)
            //            {
            //                if (oBlcCoverages[0].Value != oVwCoverage.CoverageValue)
            //                {
            //                    moUpdateList.Add(oBlcCoverages[0].CreateCoverage(oVwCoverage.CoverageID, oInsuredId, oVwCoverage.CoverageNameID));
            //                }
            //            }
            //            else
            //            {
            //                moDeleteList[oVwCoverage.CoverageID] = oVwCoverage;
            //            }
            //        }
            //        else
            //        {
            //            moDeleteList[oVwCoverage.CoverageID] = oVwCoverage;
            //        }
            //    }
            //}
            //else
            #endregion

            // Accounts for missing, new and different items
            // If all of the items are not the same, nuke the DB and add the new items
            if (oStartList.Count != 0 || oFoundList.Count != oCoverageBOList.Count)
            {
                // Add them all
                foreach (CoverageBO oInboundCoverage in oCoverageBOList)
                {
                    Guid policyID;
                    string key = string.Format("{0}-{1}", oInboundCoverage.PolicyNumber, oInboundCoverage.PolicyMod);

                    if (existingPolicies.Contains(key)) //existing
                    {
                        policyID = new Guid(existingPolicies[key].ToString());
                    }
                    else //new
                    {
                        ReconcilePolicyKey oPolicyKey = new ReconcilePolicyKey(oInboundCoverage.PolicyNumber, oInboundCoverage.PolicyMod);
                        policyID = oReconcilePolicy.GetPolicy(oPolicyKey.ToArray()).PolicyId;
                    }

                    //determine which policiy ids for coverages that will need to be deleted so they can be added back
                    if (!policiesToDelete.Contains(policyID))
                    {
                        policiesToDelete.Add(policyID);
                    }

                    Coverage cov = oInboundCoverage.CreateCoverage(Guid.NewGuid(), oInsuredId, moCoverageNameDict.GetId(oInboundCoverage), policyID);
                    moAddList.Add(cov);

                    //Coverage Details
                    if (oInboundCoverage.CoverageDetails != null && oInboundCoverage.CoverageDetails.Count > 0)
                    {
                        moCovDetailList.AddRange(oInboundCoverage.CreateCoverageDetail(cov.CoverageId, cov.InsuredId, cov.PolicyId, oInboundCoverage.CoverageDetails));
                    }
                }
            }

        }

        #region Hybrid Approach
        //private bool RemoveCoveragesInTheSameGroup(CoverageKey oCoverageKey, VWCoverage oVwCoverage)
        //{
        //    bool fReturn = false;
        //    List<VWCoverage> oExistingList;

        //    moFoundBlcCoverageList.Add(oCoverageKey, oVwCoverage);
        //    oExistingList = moFoundBlcCoverageList.Get(oCoverageKey);
        //    if (oExistingList.Count > 1)
        //    {
        //        foreach(VWCoverage oCoverageToDelete in moFoundBlcCoverageList)
        //        {
        //            moDeleteList[oCoverageToDelete.CoverageID] = oCoverageToDelete;
        //            fReturn = true;
        //        }
        //    }
        //    return fReturn;
        //}
        #endregion

        public void AddQueued()
        {
            foreach (Coverage oCoverage in moAddList)
            {
                moDb.Coverages.Add(oCoverage);
            }

            if (moCovDetailList != null && moCovDetailList.Count > 0)
            {
                foreach (CoverageDetail oCoverageDetail in moCovDetailList)
                {
                    moDb.CoverageDetails.Add(oCoverageDetail);
                }
            }

            #region Hybrid Approach
            //foreach(BlcCoverage oBlcCoverage in moAddBlcCoverages.Values)
            //{
            //    oNewCoverage = oBlcCoverage.CreateCoverage(Guid.NewGuid(), moBlcInsured.InsuredEntity.ID, moCoverageNameTable.GetId(oBlcCoverage));
            //    if (oNewCoverage.IsReadOnly)
            //    {
            //        oNewCoverage = oNewCoverage.GetWritableInstance();
            //    }
            //    oNewCoverage.Save(moTransaction);
            //}

            //foreach (Coverage oCoverage in moUpdateList)
            //{
            //    DB.Engine.StartTracking(oCoverage, InitialState.Updated);
            //    oCoverage.Save(moTransaction);
            //}
            #endregion
        }

        public void DeleteQueued()
        {
            foreach (Guid policyID in policiesToDelete)
            {
                moDb.CoverageDetails.RemoveRange(moDb.CoverageDetails.Where(cd => cd.InsuredId == moInsuredBO.InsuredEntity.InsuredId && cd.PolicyId == policyID));
                moDb.Coverages.RemoveRange(moDb.Coverages.Where(c => c.InsuredId == moInsuredBO.InsuredEntity.InsuredId && c.PolicyId == policyID));
            }
            //try
            //{
            //    foreach (Guid policyID in policiesToDelete)
            //    {
            //        //moDb.CoverageDetails.RemoveRange()

            //        //moDb.CoverageDetails.Remove(oInsured);
            //        //moDb.Coverages.Remove(oInsured);

            //        // It's a lot faster to delete all of the rows at once
            //        //DB.Engine.ExecuteDelete(typeof(CoverageDetail), string.Format("InsuredId = '{0}' AND PolicyID = '{1}'", moBlcInsured.InsuredEntity.ID, policyID));
            //        //DB.Engine.ExecuteDelete(typeof(Coverage), string.Format("InsuredId = '{0}' AND PolicyID = '{1}'", moBlcInsured.InsuredEntity.ID, policyID));
            //    }
            //}
            //catch (Exception oException)
            //{
            //    throw new Exception("Exception during Coverage delete: " + oException.Message);
            //}

            #region Hybrid Approach
            //Coverage oCoverage;
            //foreach (VWCoverage oEntity in moDeleteList.Values)
            //{
            //    oCoverage = new Coverage(oEntity.CoverageID);
            //    DB.Engine.StartTracking(oCoverage, InitialState.Updated);
            //    oCoverage.Delete(moTransaction);
            //}
            #endregion
        }

        public void Clear()
        {
            if (moCoverageNameDict != null)
            {
                moCoverageNameDict.Clear();
            }
        }
        #endregion
    }
}
