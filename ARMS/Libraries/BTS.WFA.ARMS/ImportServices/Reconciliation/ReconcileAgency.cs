﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class ReconcileAgency : IReconcile<Agency>
    {
        private ReconcileObject<Agency> rObject;
        private List<Agency> moAgencyInsertList = new List<Agency>();
        private List<Agency> moAgencyUpdateList = new List<Agency>();
        private List<AgencyEmail> moAgencyEmailList;
        private CompanyDetail moCompany;
        private IArmsDb moDb;

        public ReconcileAgency(IArmsDb db, CompanyDetail oCompany, Agency oBuiltAgency, List<AgencyEmail> oEmailList)
        {
            moCompany = oCompany;
            moAgencyEmailList = oEmailList;
            moDb = db;
            rObject = new ReconcileObject<Agency>(moDb, new string[] { oBuiltAgency.AgencyNumber }, oBuiltAgency, this);
        }

        public ReconcileAgency(CompanyDetail oCompany)
        {
            moCompany = oCompany;
        }

        #region Members
        public Agency Entity
        {
            get { return rObject.Entity; }
        }

        public ReconcileObject<Agency> Object
        {
            get { return rObject; }
        }
        #endregion

        #region Methods
        public void Reconcile(IArmsDb moDb, Agency oBuiltAgency)
        {
                rObject = new ReconcileObject<Agency>(moDb, new string[] { oBuiltAgency.AgencyNumber }, oBuiltAgency, this);
                rObject.Reconcile();
                AddQueued();
                UpdateQueued();
                moDb.SaveChanges();
            }

        public void Reconcile()
        {
            rObject.Reconcile();
        }

        public Agency GetExisting(string[] rstrForeignKey)
        {
                string key = rstrForeignKey[0];
                Agency agency = moDb.Agencies.Where(a => a.AgencyNumber == key && a.CompanyId == moCompany.CompanyId).FirstOrDefault();
                return agency;            
            }

        public Agency Insert(Agency oNewEntity)
        {
            oNewEntity.CompanyId = moCompany.CompanyId;
            moAgencyInsertList.Add(oNewEntity);
            return oNewEntity;
        }

        public Agency Update(Agency oNewEntity, Agency oExistingEntity)
        {
            //AutoMapper
            //oExistingEntity = Mapper.Map(oNewEntity, oExistingEntity);
            moAgencyUpdateList.Add(oExistingEntity);

            rObject.Entity = oExistingEntity;

            // Update the AgencyID in the AgencyEmail
            if (moAgencyEmailList != null && moAgencyEmailList.Count > 0)
            {
                for (int i = 0; i < moAgencyEmailList.Count; i++)
                {
                    if (moAgencyEmailList[i].AgencyId == oNewEntity.AgencyId)
                    {
                        moAgencyEmailList[i].AgencyId = oExistingEntity.AgencyId;
                    }
                }
            }
            return oExistingEntity;
        }

        public void Delete(Agency oEntity)
        {
            // Intentionally not implemented
        }

        public void AddQueued()
        {
            foreach (Agency oAgency in moAgencyInsertList)
            {
                moDb.Agencies.Add(oAgency);
            }
        }

        public void UpdateQueued()
        {
            ReconcileAgencyEmail oReconcileAgencyEmail;

            foreach (Agency oAgency in moAgencyUpdateList)
            {
                moDb.Agencies.Add(oAgency);
            }

            if (moAgencyEmailList != null && moAgencyEmailList.Count > 0)
            {
                oReconcileAgencyEmail = new ReconcileAgencyEmail(moDb, moCompany, moAgencyEmailList);
                oReconcileAgencyEmail.Reconcile();
            }

        }

        public void DeleteQueued()
        {
            // Intentionally not implemented
        }

        public bool Equal(Agency oLhs, Agency oRhs)
        {
            return oLhs.AgencyNumber == oRhs.AgencyNumber;
        }

        public void Clear()
        {
            moAgencyInsertList.Clear();
            moAgencyUpdateList.Clear();
        }
        #endregion
    }
}
