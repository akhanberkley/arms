﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class ReconcileObject<T>
    {
        private T moEntity;
        private bool mfAlreadyExists = false;
        private IArmsDb moDb;
        private T moNewEntity;
        private IReconcile<T> moIReconcile = null;

        /// <summary>
        /// Use this constructor along with the Reconcile() call.
        /// Use when entities need to be deleted from the DB during Reconciliation.
        /// </summary>
        /// <param name="oTransaction"></param>
        public ReconcileObject(IArmsDb db)
        {
            moDb = db;
        }

        /// <summary>
        /// Performs an Update or Insert based on the Entity's presence in the DB
        /// This constructor will not take into account deleting a DB entity
        /// </summary>
        /// <param name="strForeignKey"></param>
        /// <param name="oNewEntity"></param>
        /// <param name="oIReconcile"></param>
        /// <param name="oTransaction"></param>
        public ReconcileObject(IArmsDb db, string[] rstrForeignKey, T oNewEntity, IReconcile<T> oIReconcile)
        {
            moDb = db;
            moNewEntity = oNewEntity;
            moIReconcile = oIReconcile;
            moEntity = oIReconcile.GetExisting(rstrForeignKey);
        }

        /// <summary>
        /// Performs an Update or Insert based on the Entity's presence in the DB
        /// This constructor will not take into account deleting a DB entity
        /// </summary>
        /// <param name="strForeignKey"></param>
        /// <param name="oNewEntity"></param>
        /// <param name="oIReconcile"></param>
        /// <param name="oTransaction"></param>
        public ReconcileObject(IArmsDb db, T oNewEntity, IReconcile<T> oIReconcile)
        {
            moDb = db;
            moNewEntity = oNewEntity;
            moIReconcile = oIReconcile;
        }

        #region Members
        public T Entity
        {
            get { return moEntity; }
            set { moEntity = value; }
        }

        public bool AlreadyExists
        {
            get { return mfAlreadyExists; }
        }

        //public IArmsDb DB
        //{
        //    get { return moDb; }
        //}
        #endregion

        #region Methods
        public void Reconcile()
        {
            if (moEntity != null)
            {
                mfAlreadyExists = true;
                moEntity = moIReconcile.Update(moNewEntity, moEntity);
            }
            else
            {
                moEntity = moIReconcile.Insert(moNewEntity);
            }
        }

        public void Reconcile(T[] roExisting, T[] roNew, IReconcile<T> oIReconcile)
        {
            bool fFound = false;
            foreach (T oExistingLocation in roExisting)
            {
                fFound = false;
                foreach (T oNewLocation in roNew)
                {
                    if (oIReconcile.Equal(oExistingLocation, oNewLocation))
                    {
                        fFound = true;
                        oIReconcile.Update(oNewLocation, oExistingLocation);
                        break;
                    }
                }
                if (!fFound)
                {
                    oIReconcile.Delete(oExistingLocation);
                }
            }

            foreach (T oNewEntity in roNew)
            {
                fFound = false;
                foreach (T oExistingLocation in roExisting)
                {
                    if (oIReconcile.Equal(oExistingLocation, oNewEntity))
                    {
                        fFound = true;
                        break;
                    }
                }
                if (!fFound)
                {
                    oIReconcile.Insert(oNewEntity);
                }
            }
        }

        public State GetExisting(string strForeignKey)
        {
            return moDb.States.Where(s => s.StateCode == strForeignKey).FirstOrDefault();
            //State oState = State.GetOne("StateCode = ?", strForeignKey);
            //State oState = State.Select(s => s.StateCode = strForeignKey).FirstOrDefault();
            //return oState;
        }
        #endregion
    }
}
