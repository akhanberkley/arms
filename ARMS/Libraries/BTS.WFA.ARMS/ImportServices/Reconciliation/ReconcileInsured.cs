﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data;
using BTS.WFA.ARMS.Data.Models;
using System.Collections;

namespace BTS.WFA.ARMS.ImportServices
{
    public class ReconcileInsured : IReconcile<Insured>
    {
        private ReconcileObject<Insured> rObject;
        private InsuredBO moInsuredBO;
        private CompanyDetail moCompany;
        private Survey moSurvey;
        private IArmsDb moDb;
        private List<Insured> moDeleteInsuredList = new List<Insured>();
        private List<Insured> moInsertInsuredList = new List<Insured>();
        private List<Insured> moUpdateInsuredList = new List<Insured>();

        public ReconcileInsured(InsuredBO oInsuredBO, CompanyDetail oCompany)
        {
            moInsuredBO = oInsuredBO;
            moCompany = oCompany;
        }

        public ReconcileInsured(InsuredBO oInsuredBO, Survey oSurvey, CompanyDetail oCompany)
        {
            moInsuredBO = oInsuredBO;
            moCompany = oCompany;
            moSurvey = oSurvey;
        }

        #region Members
        public Insured InsuredEntity
        {
            get { return rObject.Entity; }
        }

        Insured IReconcile<Insured>.Entity
        {
            get { return rObject.Entity; }
        }

        public ReconcileObject<Insured> Object
        {
            get { return rObject; }
        }
        #endregion

        #region Methods
        public void Reconcile()
        {
            using (moDb = Application.GetDatabaseInstance())
            {
                Reconcile(moDb, false);
                moDb.SaveChanges();
            }
        }

        public void NewInsert()
        {
            using (moDb = Application.GetDatabaseInstance())
            {
                Reconcile(moDb, true);
                moDb.SaveChanges();
            }
        }

        private void Reconcile(IArmsDb moDb, bool newInsert)
        {
            Exception oCaughtException = null;
            ReconcileCoverage oReconcileCoverage = null;
            ReconcileAgency oReconcileAgency = null;
            ReconcilePolicy oReconcilePolicy = null;
            ReconcileLocation oReconcileLocation = null;
            //Insured oInsuredFromPolicy = null;
            string strClientId = String.Empty;

            try
            {
                // Reconcile Coverages
                oReconcileCoverage = new ReconcileCoverage(moDb, moCompany, moInsuredBO);
                Location[] roBuiltLocations = moInsuredBO.PolicyLocationList.ToArray();
                Policy[] roBuiltPolicies = moInsuredBO.PolicyList.ToArray();

                // Reconcile Agency
                oReconcileAgency = new ReconcileAgency(moDb, moCompany, moInsuredBO.AgencyEntity, moInsuredBO.AgencyEmailList);
                oReconcileAgency.Reconcile();
                moInsuredBO.AgencyEntity = oReconcileAgency.Entity;

                // Reconcile Insured
                if (moInsuredBO.PreviousClientId != String.Empty)
                {
                    strClientId = moInsuredBO.PreviousClientId;
                }
                else
                {
                    strClientId = moInsuredBO.InsuredEntity.ClientId;
                }

                if(!newInsert)
                {
                    if (moSurvey == null)
                        throw new ArgumentNullException("survey");
                    else
                        rObject = new ReconcileObject<Insured>(moDb, new string[] { moSurvey.InsuredId.ToString() }, moInsuredBO.InsuredEntity, this);
                }
                else
                {
                    rObject = new ReconcileObject<Insured>(moDb, moInsuredBO.InsuredEntity, this);
                }

                // Run the Recondile
                rObject.Reconcile();
                moInsuredBO.InsuredEntity = rObject.Entity;
                
                // #TODO: Rectify
                // Set polices to NOT active
                //DB.Engine.ExecuteScalar(string.Format("UPDATE Policy set IsActive=0 where InsuredID='{0}'", moInsuredBO.InsuredEntity.ID));

                // Get existing Insured Locations and Policies from db
                moInsuredBO.ExistingLocations = moDb.Locations.Where(l => l.InsuredId == moInsuredBO.InsuredEntity.InsuredId).ToArray();
                moInsuredBO.ExistingPolicies = moDb.Policies.Where(l => l.InsuredId == moInsuredBO.InsuredEntity.InsuredId).ToArray();
                
                Hashtable existingPolicies = new Hashtable(moInsuredBO.ExistingPolicies.Length);
                foreach (Policy policy in moInsuredBO.ExistingPolicies)
                {
                    string key = string.Format("{0}-{1}", policy.PolicyNumber, policy.PolicyMod);
                    if (!existingPolicies.ContainsKey(key))
                    {
                        existingPolicies.Add(key, policy.PolicyId);
                    }
                }

                // Policies // Do before the Locations - The ReconcilePolicy takes care of the Claim and Rate Mods
                oReconcilePolicy = new ReconcilePolicy(moDb, moInsuredBO);
                oReconcilePolicy.Object.Reconcile(moInsuredBO.ExistingPolicies, roBuiltPolicies, oReconcilePolicy);

                // Locations // ReconcileLocation takes care of Building
                oReconcileLocation = new ReconcileLocation(moDb, moInsuredBO);
                oReconcileLocation.Object.Reconcile(moInsuredBO.ExistingLocations, roBuiltLocations, oReconcileLocation);

                // Coverages
                oReconcileCoverage.Reconcile(moInsuredBO.CoverageList, existingPolicies, oReconcilePolicy);

                // Delete Queued
                //No delete for agencies?
                oReconcilePolicy.DeleteQueued();
                oReconcileLocation.DeleteQueued();
                oReconcileCoverage.DeleteQueued();

                // Add Queued
                oReconcileAgency.AddQueued();
                AddQueued();
                oReconcilePolicy.AddQueued();
                oReconcileLocation.AddQueued();
                oReconcileCoverage.AddQueued();

                // Update Queued
                oReconcileAgency.UpdateQueued();
                UpdateQueued();
                oReconcilePolicy.UpdateQueued();
                oReconcileLocation.UpdateQueued();
                //No update for coverages?
            }
            catch (Exception oException)
            {
                oCaughtException = oException;
            }
            finally
            {
                try
                {
                    Clear();
                    if (oReconcileAgency != null)
                    {
                        oReconcileAgency.Clear();
                    }
                    if (oReconcilePolicy != null)
                    {
                        oReconcilePolicy.Clear();
                    }
                    if (oReconcileLocation != null)
                    {
                        oReconcileLocation.Clear();
                    }
                    if (oReconcileCoverage != null)
                    {
                        oReconcileCoverage.Clear();
                    }
                }
                catch (Exception oClearException)
                {
                    if (oCaughtException == null)
                    {
                        oCaughtException = oClearException;
                    }
                    else
                    {
                        oCaughtException.Data.Add("Clear Exception", " ALSO Clear() Exception!!!: " + oClearException.Message);
                    }
                }
                if (oCaughtException != null)
                {
                    throw oCaughtException;
                }
            }
        }

        // #TODO: Rectify
        public Insured GetInsuredFromIncomingPolicies()
        {
            //string strSelect;
            //Guid oGuid;
            //Insured oInsured;

            //foreach (Policy oPolicy in moBlcInsured.PolicyList)
            //{
            //    strSelect = String.Format("SELECT dbo.Insured.InsuredID FROM dbo.Insured INNER JOIN dbo.Policy ON dbo.Insured.InsuredID = dbo.Policy.InsuredID INNER JOIN dbo.Company ON dbo.Insured.CompanyID = dbo.Company.CompanyID WHERE (dbo.Policy.PolicyNumber LIKE '{0}%') AND (dbo.Company.CompanyID = '{1}')", oPolicy.Number, moCompany.ID);
            //    object o = DB.Engine.ExecuteScalar(strSelect);
            //    if (o != null)
            //    {
            //        oGuid = (Guid)o;
            //        oInsured = Insured.GetOne("ID = ?", (Guid)o);
            //        return oInsured;
            //    }
            //}
            return null;
        }

        // #TODO: Rectify
        public Insured GetInsuredFromExistingSurvey()
        {
            //string strSelect;
            //Guid oGuid;
            //Insured oInsured;

            //foreach (Policy oPolicy in moBlcInsured.PolicyList)
            //{
            //    strSelect = String.Format("SELECT dbo.Insured.InsuredID FROM dbo.Insured INNER JOIN dbo.Policy ON dbo.Insured.InsuredID = dbo.Policy.InsuredID INNER JOIN dbo.Company ON dbo.Insured.CompanyID = dbo.Company.CompanyID WHERE (dbo.Policy.PolicyNumber LIKE '{0}%') AND (dbo.Company.CompanyID = '{1}')", oPolicy.Number, moCompany.ID);
            //    object o = DB.Engine.ExecuteScalar(strSelect);
            //    if (o != null)
            //    {
            //        oGuid = (Guid)o;
            //        oInsured = Insured.GetOne("ID = ?", (Guid)o);
            //        return oInsured;
            //    }
            //}
            return null;
        }

        Insured IReconcile<Insured>.GetExisting(string[] rstrForeignKey)
        {
            using (moDb = Application.GetDatabaseInstance())
            {
                string key = rstrForeignKey[0];
                Insured insured = moDb.Insureds.Where(i => i.InsuredId.ToString() == key).FirstOrDefault();
                return insured;
            }
        }

        private string VerifySicCode(string strSicCode)
        {
            // Verify that the code exists in the BLC Table
            Sic oSic = null;
            oSic = moDb.Sics.Where(s => s.SicCode == strSicCode).FirstOrDefault();
            
            if (oSic == null)
            {
                // If it doesn't exist, remove the code
                return string.Empty;
            }
            else
            {
                return strSicCode;
            }
        }

        Insured IReconcile<Insured>.Insert(Insured oNewEntity)
        {
            oNewEntity.CompanyId = moCompany.CompanyId;
            oNewEntity.AgencyId = moInsuredBO.AgencyEntity.AgencyId;
            oNewEntity.SicCode = VerifySicCode(oNewEntity.SicCode);
            moInsertInsuredList.Add(oNewEntity);
            return oNewEntity;
        }

        Insured IReconcile<Insured>.Update(Insured oNewEntity, Insured oExistingEntity)
        {
            oNewEntity.CompanyId = moCompany.CompanyId;
            oNewEntity.AgencyId = moInsuredBO.AgencyEntity.AgencyId;
            oNewEntity.SicCode = VerifySicCode(oNewEntity.SicCode);
            //oExistingEntity = Mapper.Map(oNewEntity, oExistingEntity);
            moUpdateInsuredList.Add(oExistingEntity);
            return oExistingEntity;
        }

        public void Delete(Insured oEntity)
        {
            moDeleteInsuredList.Add(oEntity);
        }

        public void AddQueued()
        {
            foreach (Insured oInsured in moInsertInsuredList)
            {
                moDb.Insureds.Add(oInsured);
            }
        }

        public void UpdateQueued()
        {
            foreach (Insured oInsured in moUpdateInsuredList)
            {
                moDb.Insureds.Add(oInsured);
            }
        }

        public void DeleteQueued()
        {
            foreach (Insured oInsured in moDeleteInsuredList)
            {
                moDb.Insureds.Remove(oInsured);
            }
        }

        public bool Equal(Insured oLhs, Insured oRhs)
        {
            return oLhs.ClientId == oRhs.ClientId;
        }

        public void Clear()
        {
            moDeleteInsuredList.Clear();
            moInsertInsuredList.Clear();
            moUpdateInsuredList.Clear();
        }

        // Replacing these with the AutoMapper library
        //public Insured Assign(Insured oLhs, Insured oRhs)
        //{
        //    oLhs.ClientId = oRhs.ClientId;
        //    oLhs.PortfolioId = oRhs.PortfolioId;

        //    //oInsured.ClientID = oRhs._clientID;
        //    //oInsured.PortfolioID = oRhs.PortfolioID;
        //    //if (oRhs._companyID != Guid.Empty)
        //    //{
        //    //    oInsured.CompanyID = oRhs._companyID;
        //    //}
        //    //if (oRhs.AgencyID != Guid.Empty)
        //    //{
        //    //    oInsured.AgencyID = oRhs.AgencyID;
        //    //}
        //    //// The existing (LHS - Left Hand Side) object is protected with the Exclusions
        //    //// LHS = RHS (Right Hand Side)
        //    //if (!ExcludeName)
        //    //{
        //    //    oInsured.Name = oRhs._name;
        //    //}
        //    //if (!ExcludeName2)
        //    //{
        //    //    oInsured.Name2 = oRhs._name2;
        //    //}
        //    //if (!ExcludeStreetLine1)
        //    //{
        //    //    oInsured.StreetLine1 = oRhs._streetLine1;
        //    //}
        //    //if (!ExcludeStreetLine2)
        //    //{
        //    //    oInsured.StreetLine2 = oRhs._streetLine2;
        //    //}
        //    //if (!ExcludeStreetLine3)
        //    //{
        //    //    oInsured.StreetLine3 = oRhs._streetLine3;
        //    //}
        //    //if (!ExcludeCity)
        //    //{
        //    //    oInsured.City = oRhs._city;
        //    //}
        //    //if (!ExcludeState)
        //    //{
        //    //    oInsured.StateCode = oRhs._stateCode;
        //    //}
        //    //if (!ExcludeZip)
        //    //{
        //    //    oInsured.ZipCode = oRhs._zipCode;
        //    //}
        //    //if (!ExcludeBusiness)
        //    //{
        //    //    oInsured.BusinessOperations = oRhs._businessOperations;
        //    //}
        //    //if (oRhs._sicCode != null && oRhs._sicCode.Length > 0 && !ExcludeSIC)
        //    //{
        //    //    oInsured.SICCode = String.Format("{0:0000}", Convert.ToInt32(oRhs._sicCode));
        //    //}
        //    //if (!ExcludeUnderWriter)
        //    //{
        //    //    oInsured.Underwriter = oRhs._underwriter;
        //    //}

        //    //always set the current account status
        //    oLhs.NonrenewedDate = oRhs.NonrenewedDate;

        //    return oLhs;
        //}
        #endregion
    }
}