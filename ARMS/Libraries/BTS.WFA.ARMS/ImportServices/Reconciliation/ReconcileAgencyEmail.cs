﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class ReconcileAgencyEmail : IReconcile<AgencyEmail>
    {

        private ReconcileObject<AgencyEmail> rObject;
        private CompanyDetail moCompany;
        private List<AgencyEmail> moAgencyEmailList;
        private IArmsDb moDb;

        public ReconcileAgencyEmail(IArmsDb moDb, CompanyDetail oCompany, List<AgencyEmail> oEmailList)
        {
            moCompany = oCompany;
            moAgencyEmailList = oEmailList;
            rObject = new ReconcileObject<AgencyEmail>(moDb);
        }

        #region Members
        public AgencyEmail Entity
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public ReconcileObject<AgencyEmail> Object
        {
            get { return rObject; }
        }
        #endregion

        #region Methods
        public AgencyEmail GetExisting(string[] rstrForeignKey)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public AgencyEmail Update(AgencyEmail oNewEntity, AgencyEmail oExistingEntity)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public AgencyEmail Insert(AgencyEmail oNewEntity)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void Delete(AgencyEmail oEntity)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void Reconcile()
        {
            using (moDb = Application.GetDatabaseInstance())
            {
                foreach (AgencyEmail oAgencyEmail in moAgencyEmailList)
                {
                    if (oAgencyEmail.EmailAddress.Length > 0)
                    {
                        AgencyEmail oEntity = moDb.AgencyEmails.Where(a => a.EmailAddress == oAgencyEmail.EmailAddress && a.AgencyId == oAgencyEmail.AgencyId).FirstOrDefault();
                        if (oEntity == null)
                        {
                            AgencyEmail oNewAgencyEmail;
                            oNewAgencyEmail = oAgencyEmail;
                            moDb.AgencyEmails.Add(oNewAgencyEmail);
                        }
                    }
                }
            }
        }

        public void AddQueued()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void UpdateQueued()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void DeleteQueued()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool Equal(AgencyEmail oLhs, AgencyEmail oRhs)
        {
            return oLhs.EmailAddress.ToLower() == oRhs.EmailAddress.ToLower();
        }

        public void Clear()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion
    }
}

