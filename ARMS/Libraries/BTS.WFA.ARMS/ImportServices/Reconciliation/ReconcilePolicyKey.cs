﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class ReconcilePolicyKey
    {
        private string mstrNumber = "";
        private string mstrMod = "";
        
        public ReconcilePolicyKey(string strNumber, string strMod)
        {
            mstrNumber = strNumber;
            mstrMod = strMod;
        }

        public ReconcilePolicyKey(string[] rstrPieces)
        {
            if (mstrNumber.Length > 7 && mstrNumber[7] == '-')
            {
                mstrNumber = rstrPieces[0].Substring(7);
            }
            else
            {
                mstrNumber = rstrPieces[0];
            }
            mstrMod = rstrPieces[1];
        }

        public string PolicyNumber
        {
            get { return mstrNumber; }
            set { mstrNumber = value; }
        }

        public string PolicyMod
        {
            get { return mstrMod; }
            set { mstrMod = value; }
        }

        public bool IsEqual(string strNumber, string strMod)
        {
            return mstrNumber == strNumber && mstrMod == strMod;
        }

        public string[] ToArray()
        {
            return new string[]{ mstrNumber, mstrMod };
        }

        public override string ToString()
        {
            return mstrNumber + "|" + mstrMod;
        }
    }
}
