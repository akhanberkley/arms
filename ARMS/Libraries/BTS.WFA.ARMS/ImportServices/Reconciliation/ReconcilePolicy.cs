﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class ReconcilePolicy : IReconcile<Policy>
    {
        private ReconcileObject<Policy> rObject;
        private InsuredBO moInsuredBO;
        private List<Policy> moNewPolicyList = new List<Policy>();
        private List<Policy> moUpdatePolicyList = new List<Policy>();
        private List<Policy> moInactivePolicyList = new List<Policy>();
        private List<Claim> moNewClaimList = new List<Claim>();
        private List<RateModificationFactor> moNewRateModFactorList = new List<RateModificationFactor>();
        private Dictionary<string, Policy> moExistingPolicyTable = new Dictionary<string, Policy>();
        private IArmsDb moDb;

        // Deletes
        private List<Policy> moPolicyDeletes = new List<Policy>();
        private List<Claim> moClaimDeletes = new List<Claim>();
        private List<RateModificationFactor> moRateModFactorDeletes = new List<RateModificationFactor>();

        public ReconcilePolicy(IArmsDb db, InsuredBO oInsuredBO)
        {
            moInsuredBO = oInsuredBO;
            moDb = db;
            rObject = new ReconcileObject<Policy>(moDb);
        }

        #region Members
        public Policy[] NewPolicies
        {
            get { return moNewPolicyList.ToArray(); }
        }

        public Policy Entity
        {
            get { return rObject.Entity; }
        }

        public ReconcileObject<Policy> Object
        {
            get { return rObject; }
        }
        #endregion

        #region Methods
        public void Reconcile()
        {
            rObject.Reconcile();
        }

        public void Clear()
        {
            moNewPolicyList.Clear();
            moUpdatePolicyList.Clear();
            moNewClaimList.Clear();
            moNewRateModFactorList.Clear();
            moExistingPolicyTable.Clear();
        }

        public Policy GetNew(string[] rstrForeignKey)
        {
            ReconcilePolicyKey oKey = new ReconcilePolicyKey(rstrForeignKey);
            foreach (Policy oPolicy in moNewPolicyList)
            {
                if (oPolicy.PolicyNumber == oKey.PolicyNumber && Convert.ToString(oPolicy.PolicyMod) == oKey.PolicyMod)
                {
                    return oPolicy;
                }
                //else if (oPolicy.Number == oKey.Number && oPolicy.Mod == int.MinValue && oKey.Mod.Length == 0)
                //{
                //	return oPolicy;
                //}
            }
            return null;
        }

        /// <summary>
        /// Returns either a new or existing policy
        /// </summary>
        /// <returns></returns>
        public Policy GetPolicy(string[] rstrForeignKey)
        {
            Policy oReturn = GetNew(rstrForeignKey);
            if (oReturn == null)
            {
                oReturn = GetExisting(rstrForeignKey);
            }
            if (oReturn == null)
            {
                throw new Exception(String.Format("Requested Policy number {0} version {1} could not be found.", rstrForeignKey[0], rstrForeignKey[1]));
            }
            return oReturn;
        }

        public Policy GetExisting(string[] rstrForeignKey)
        {
            // Cache the policies for the Coverage
            Policy oExistingInsured = null;
            ReconcilePolicyKey oKey = new ReconcilePolicyKey(rstrForeignKey);

            if (moExistingPolicyTable.ContainsKey(oKey.ToString()))
            {
                return moExistingPolicyTable[oKey.ToString()];
            }
            else
            {
                // SPLIT the Pipe Delimited string here
                if (oKey.PolicyMod != null && oKey.PolicyMod.Length > 0)
                {
                    //oExistingInsured = Policy.GetOne("Number = ? AND Mod = ?", oKey.PolicyNumber, oKey.PolicyMod);
                    oExistingInsured = moDb.Policies.Where(p => p.PolicyNumber == oKey.PolicyNumber && p.PolicyMod.ToString() == oKey.PolicyMod).FirstOrDefault();
                }
                else
                {
                    //MAE oExistingInsured = Policy.GetOne("Number = ?", oKey.Number);
                }
                if (oExistingInsured != null)
                {
                    moExistingPolicyTable.Add(oKey.ToString(), oExistingInsured);
                }
            }
            return oExistingInsured;
        }

        public Policy Update(Policy oNewEntity, Policy oExistingEntity)
        {
            // No foreign key for the claim; remove the existing
            //Claim[] roClaims = Claim.GetArray("PolicyID = ?", oExistingEntity.ID);
            Claim[] roClaims = moDb.Claims.Where(c => c.PolicyId == oExistingEntity.PolicyId).ToArray();
            foreach (Claim oClaim in roClaims)
            {
                moClaimDeletes.Add(oClaim);
            }

            // No foreign key for the rate mod factors; remove the existing
            //RateModificationFactor[] roFactors = RateModificationFactor.GetArray("PolicyID = ?", oExistingEntity.ID);
            RateModificationFactor[] roFactors = moDb.RateModificationFactors.Where(r => r.PolicyId == oExistingEntity.PolicyId).ToArray();
            foreach (RateModificationFactor oFactor in roFactors)
            {
                moRateModFactorDeletes.Add(oFactor);
            }

            //if (oExistingEntity.IsReadOnly)
            //{
            //    oExistingEntity = oExistingEntity.GetWritableInstance();
            //}
            //if (oNewEntity.IsReadOnly)
            //{
            //    oNewEntity = oNewEntity.GetWritableInstance();
            //}

            oNewEntity.InsuredId = moInsuredBO.InsuredEntity.InsuredId;
            //oExistingEntity = Mapper.Map(oNewEntity, oExistingEntity);
            moUpdatePolicyList.Add(oExistingEntity);

            // Add the claims for this policy
            foreach (ClaimBO oClaimBO in moInsuredBO.ClaimList)
            {
                //if (oBlcClaim.PolicyNumber == oExistingEntity.Number && oBlcClaim.PolicyMod == oExistingEntity.Mod)
                if (oClaimBO.PolicyNumber == oExistingEntity.PolicyNumber)
                {
                    oClaimBO.Entity.PolicyId = oExistingEntity.PolicyId;
                    moNewClaimList.Add(oClaimBO.Entity);
                }
            }

            // Add the rate mod factors for this policy
            foreach (RateModFactorBO oRateModFactorBO in moInsuredBO.RateModFactorList)
            {
                //if (oBlcRateModFactor.PolicyNumber == oExistingEntity.Number && oBlcClaim.PolicyMod == oExistingEntity.Mod)
                if (oRateModFactorBO.PolicyNumber == oExistingEntity.PolicyNumber)
                {
                    oRateModFactorBO.Entity.PolicyId = oExistingEntity.PolicyId;
                    moNewRateModFactorList.Add(oRateModFactorBO.Entity);
                }
            }
            return oExistingEntity;
        }

        public Policy Insert(Policy oNewEntity)
        {
            //if (oNewEntity.IsReadOnly)
            //{
            //    oNewEntity = oNewEntity.GetWritableInstance();
            //}
            oNewEntity.InsuredId = moInsuredBO.InsuredEntity.InsuredId;
            moNewPolicyList.Add(oNewEntity);

            // Add the claims for this policy
            foreach (ClaimBO oClaimBO in moInsuredBO.ClaimList)
            {
                //if (oBlcClaim.PolicyNumber == oExistingEntity.Number && oBlcClaim.PolicyMod == oExistingEntity.Mod)
                if (oClaimBO.PolicyNumber == oNewEntity.PolicyNumber)
                {
                    oClaimBO.Entity.PolicyId = oNewEntity.PolicyId;
                    moNewClaimList.Add(oClaimBO.Entity);
                }
            }

            // Add the rate mod factors for this policy
            foreach (RateModFactorBO oRateModFactorBO in moInsuredBO.RateModFactorList)
            {
                //if (oBlcRateModFactor.PolicyNumber == oExistingEntity.Number && oBlcClaim.PolicyMod == oExistingEntity.Mod)
                if (oRateModFactorBO.PolicyNumber == oNewEntity.PolicyNumber)
                {
                    oRateModFactorBO.Entity.PolicyId = oNewEntity.PolicyId;
                    moNewRateModFactorList.Add(oRateModFactorBO.Entity);
                }
            }
            return oNewEntity;
        }

        public void Delete(Policy oEntity)
        {
            moPolicyDeletes.Add(oEntity);

            //Claim[] roClaims = Claim.GetArray("PolicyID = ?", oEntity.ID);
            Claim[] roClaims = moDb.Claims.Where(c => c.PolicyId == oEntity.PolicyId).ToArray();
            foreach (Claim oClaim in roClaims)
            {
                moClaimDeletes.Add(oClaim);
            }

            //RateModificationFactor[] roFactors = RateModificationFactor.GetArray("PolicyID = ?", oEntity.ID);
            RateModificationFactor[] roFactors = moDb.RateModificationFactors.Where(r => r.PolicyId == oEntity.PolicyId).ToArray();
            foreach (RateModificationFactor oFactor in roFactors)
            {
                moRateModFactorDeletes.Add(oFactor);
            }
        }

        public bool Equal(Policy oLhs, Policy oRhs)
        {
            return oLhs.PolicyNumber == oRhs.PolicyNumber && oLhs.PolicyMod == oRhs.PolicyMod;
            // Use if changed to match the policy by the expire date
            //return BlcPolicy.Equals(oLhs, oRhs);
        }

        public void DeleteQueued()
        {
            foreach (Claim oClaim in moClaimDeletes)
            {
                moDb.Claims.Remove(oClaim);
                //oClaim.Delete(moActor.DbTransaction);
            }

            foreach (RateModificationFactor oFactor in moRateModFactorDeletes)
            {
                moDb.RateModificationFactors.Remove(oFactor);
                //oFactor.Delete(moActor.DbTransaction);
            }

            // A PolicyID foreign key now exists in the Coverages.  The commented code below would need to take this into consideration in order to be functional.
            // Do not delete the Policy
            #region Delete Code
            //SurveyLocationPolicyCoverageName[] roSLPCN;
            //SurveyLocationPolicyExclusion[] roSPE;
            //SurveyLocationPolicyOptionalReport[] roSLPOR;

            //foreach (Policy oEntity in moPolicyDeletes)
            //{
            //    roSLPCN = SurveyLocationPolicyCoverageName.GetArray("PolicyID = ?", oEntity.ID);
            //    roSPE = SurveyLocationPolicyExclusion.GetArray("PolicyID = ?", oEntity.ID);
            //    roSLPOR = SurveyLocationPolicyOptionalReport.GetArray("PolicyID = ?", oEntity.ID);;

            //    if(roSLPCN != null)
            //    {
            //        foreach(SurveyLocationPolicyCoverageName oSlpcEntity in roSLPCN)
            //        {
            //            oSlpcEntity.Delete(moActor.DbTransaction);
            //        }
            //    }

            //    if (roSPE != null)
            //    {
            //        foreach (SurveyLocationPolicyExclusion oSpe in roSPE)
            //        {
            //            oSpe.Delete(moActor.DbTransaction);
            //        }
            //    }

            //    if (roSLPOR != null)
            //    {
            //        foreach (SurveyLocationPolicyOptionalReport oSlpor in roSLPOR)
            //        {
            //            oSlpor.Delete(moActor.DbTransaction);
            //        }
            //    }
            //    oEntity.Delete(moActor.DbTransaction);
            //}
            #endregion
        }

        public void AddQueued()
        {
            //Claim oClaimToInsert = null;
            //RateModificationFactor oFactorToInsert = null;

            foreach (Policy oPolicy in moNewPolicyList)
            {
                oPolicy.IsActive = true;
                moDb.Policies.Add(oPolicy);
                //oPolicy.Save(moActor.DbTransaction);
            }

            foreach (Claim oClaim in moNewClaimList)
            {
                moDb.Claims.Add(oClaim);
                //oClaimToInsert.Save(moActor.DbTransaction);
            }

            foreach (RateModificationFactor oFactor in moNewRateModFactorList)
            {
                moDb.RateModificationFactors.Add(oFactor);
                //oFactorToInsert.Save(moActor.DbTransaction);
            }
        }

        public void UpdateQueued()
        {
            // Not needed - unfinished work that changes the migrated policy mod numbers of zero using a best guess
            //BlcPolicyList oBlcPolicyList = new BlcPolicyList(moUpdatePolicyList, moActor.DbTransaction);
            //oBlcPolicyList.FixModDates();
            foreach (Policy oPolicy in moUpdatePolicyList)
            {
                oPolicy.IsActive = true;
                moDb.Policies.Add(oPolicy);
                //oPolicy.Save(moActor.DbTransaction);
            }
        }
        #endregion
    }
}
