﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class ReconcileBuilding : IReconcile<Building>
    {
        private ReconcileObject<Building> rObject;
        private List<Building> moBuildingDeletes = new List<Building>();
        private List<Building> moBuildingInserts = new List<Building>();
        private List<Building> moBuildingUpdates = new List<Building>();
        private Location moLocation = null;
        private IArmsDb moDb;

        public ReconcileBuilding(IArmsDb db)
        {
            moDb = db;
            rObject = new ReconcileObject<Building>(moDb);
        }

        /// <summary>
        /// If the building counts are different between the existing and incoming buildings,
        /// remove all buildings for this location and add them.
        /// </summary>
        /// <param name="roExistingBuildings"></param>
        /// <param name="roNewBuildings"></param>
        public void Reconcile(Building[] roExistingBuildings, Building[] roNewBuildings)
        {
            //if (roExistingBuildings.Length != roNewBuildings.Length && roExistingBuildings.Length > 0)
            //{
            //    // Remove all as there could be multiple buildings with the same building number
            //    DB.Engine.ExecuteDelete(typeof(Building), "LocationID = '" + moLocation.LocationId + "'");
            //    roExistingBuildings = new Building[0];
            //}
            //Object.Reconcile(roExistingBuildings, roNewBuildings, this);
        }

        #region Members
        public Location BuildingLocation
        {
            get { return moLocation; }
            set { moLocation = value; }
        }

        public ReconcileObject<Building> Object
        {
            get { return rObject; }
        }

        public Building Entity
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }
        #endregion

        #region Methods
        public void Reconcile()
        {
            rObject.Reconcile();
        }

        public void Clear()
        {
            moBuildingDeletes.Clear();
            moBuildingInserts.Clear();
            moBuildingUpdates.Clear();
        }

        public Building GetExisting(string[] rstrForeignKey)
        {
            string key = rstrForeignKey[0];
            Building oEntity = moDb.Buildings.Where(b => b.BuildingNumber.ToString() == key && b.LocationId == moLocation.LocationId).FirstOrDefault();
            return oEntity;
        }

        public Building[] GetExisting(Guid oLocationId)
        {
            Building[] roEntity = moDb.Buildings.Where(b => b.LocationId == oLocationId).ToArray();
            return roEntity;
        }

        public Building Update(Building oNewEntity, Building oExistingEntity)
        {
            //oExistingEntity = Mapper.Map(oNewEntity, oExistingEntity);
            oExistingEntity.LocationId = BuildingLocation.LocationId;
            moBuildingUpdates.Add(oExistingEntity);
            return oExistingEntity;
        }

        public Building Insert(Building oNewEntity)
        {
            oNewEntity.LocationId = BuildingLocation.LocationId;
            moBuildingInserts.Add(oNewEntity);
            return oNewEntity;
        }

        public void Delete(Building oEntity)
        {
            moBuildingDeletes.Add(oEntity);
        }

        public bool Equal(Building oLhs, Building oRhs)
        {
            // Use the Foreign Key for identification between systems
            return oLhs.BuildingNumber == oRhs.BuildingNumber;
        }

        public void DeleteQueued()
        {
            // Intentionally not implemented
        }

        public void AddQueued()
        {
            foreach (Building oBuilding in moBuildingInserts)
            {
                moDb.Buildings.Add(oBuilding);
            }
        }

        public void UpdateQueued()
        {
            foreach (Building oBuilding in moBuildingUpdates)
            {
                moDb.Buildings.Add(oBuilding);
            }
        }
        #endregion
    }
}
