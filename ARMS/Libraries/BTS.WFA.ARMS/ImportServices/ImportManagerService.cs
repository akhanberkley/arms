﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ARMS.ViewModels;
using Int = BTS.WFA.Integration;
using BTS.WFA.Integration.Models;
//using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.ImportServices
{
    public class ImportManagerService
    {
        private static CompanyDetail _company;
        private static RequestViewModel _request;
        private DateTime _started;
        private DateTime _ended;
        
        // Manages the data (import) processing and the async calls to integrated services to populate ARMS with the appropriate data to continue
        // the survey request process.
        // - should have a execution method that is provided the Request to process.
        // - should be provided the company and user for following the correct rule sets

        //public ImportManagerService(CompanyDetail company, RequestViewModel request)
        //{
        //    if (company == null)
        //    {
        //        throw new ArgumentNullException("company");
        //    }

        //    _company = company;
        //    _request = request;
        //}

        //public ImportManagerService(CompanyDetail company, RequestViewModel request, SurveyViewModel survey)
        //{
        //    if (company == null)
        //    {
        //        throw new ArgumentNullException("company");
        //    }

        //    _company = company;
        //    _request = request;
        //    _survey = survey;
        //}

        #region snippets from 2013 project
        /* Add the Company parameter and null check here.
        public ImportFactory(Company company)
        {
            if (company == null)
            {
                throw new ArgumentNullException("company");
            }
            _company = company; ;
        }
        */
        #endregion

        // Have either a ProcessRequest, ProcessProspect/Quote .. or within ProcessRequest, we determine the appropriate tasks for 
        // the specific scenario ... So incoming parameters, need to be the Request object to work on.
        // then as part of that Request object we would have the Request.InputType (SN submission number, PN policy number, CI client id)
        //
        //public SurveyViewModel ProcessRequest(SurveyViewModel survey)
        public static ImportManagerServiceResult ProcessRequest(CompanyDetail company, RequestViewModel request)
        {
            _company = company;
            _request = request;

            ImportManagerServiceResult _importResult = new ImportManagerServiceResult();

            // Define task and system integrations that are to be called based on company configuration?

            // Action TaskName = () => {};
            Action callPolicyImport = () =>
            {
                queryPolicyImport();
            };

            Action callClearanceImport = () =>
            {
                queryClearanceImport();
            };

            var tasks = new Task[] {
                // register tasks
                Task.Factory.StartNew(callPolicyImport),
                Task.Factory.StartNew(callClearanceImport),
            };

            #region example from bcs
            /* An example from BCS.Services.AgencyService line 147

                Data.Models.Agency dbAgency = null;
                Integration.Models.AgencySearchResult agencyRequest = null;
             
                //Get the agency from APS and from the clearance DB by agency code
                var tasks = new Task[] {
                    Task.Factory.StartNew(() => { dbAgency = getAgency(db.Agency.Where(a => a.AgencyNumber == agencyCode)); }),
                    Task.Factory.StartNew(() => { agencyRequest = Integration.Services.AgencyService.GetAgency(cn.SystemSettings.ApsServiceUrl, agencyCode, useParentLicensedStates, personnelRoles); })
                };
                Task.WaitAll(tasks);
                
                if (!agencyRequest.Succeeded)
                {
                    result.Succeeded = false;
                    result.Exception = agencyRequest.Exception;
                    result.Message = agencyRequest.Exception.Message;
                }
                else if (agencyRequest.Agency == null)
                {
                    result.Succeeded = false;
                    result.Message = "Agency was not found";
                }
                else
                {
                    if (agencyRequest.SecondaryExceptions.Count > 0)
                        result.Exception = new AggregateException(agencyRequest.SecondaryExceptions);

                    var apsAgency = agencyRequest.Agency;
                    result.ApsId = apsAgency.Id.ToString();

                    if (dbAgency == null)
                        dbAgency = getAgency(db.Agency.Where(a => a.ApsId == apsAgency.Id));
                    if (dbAgency == null)
                        dbAgency = db.Agency.Add(new Data.Models.Agency() { CompanyNumberId = cn.CompanyNumberId, EntryBy = userName });

                    ...and so on
                }
            */
            #endregion

            Task.WaitAll(tasks);

            // update result object
            _importResult.IsComplete = true;

            return _importResult;
        }

        // TODO: Handle sub tasks to multiple policy systems if in use by the company
        private static void queryPolicyImport()
        {
            //------------------------------------ web service call
            // TODO: Implement the call into the Integration project for the PolicyStar Batch|Transactional Service here.
            //AIC PolicyStar/Genesys Transactional (has a PolicyStarEndPointUrl in DB)
            string transactionUrl = _company.PolicyStarEndPointUrl;
            string AICTransactionalUrl = "http://aic-prd/PolicySTAR_WebServices/services/PremiumAuditService";
            //AIC PolicyStar/Genesys PDR (has a PDREndPointUrl in DB)
            string batchUrl = _company.PDREndPointUrl;
            string AICBatchUrl = "http://aicbatch-prd/BTS_PStar_SupportUtilitiesWeb/services/PremiumAuditService";
            //AIC A+
            //AIC APS
            string apsUrl = _company.APSEndPointUrl;


            // Integration Service Call
            var policyImportResults = Int.Services.PolicyStarTransactionalService.GetPolicyByPolicyNumber(AICTransactionalUrl, _request.InputValue, _company.Summary.CompanyNumber);
            
            // isQuote set to false, but this is determined by a check of their loss control history configuration.
            BuildRequestFromImport(policyImportResults, false);
        }

        // task for BuildPolicyImport
        // Works over the data return from all of the import processes and creats the composite Insured business object with all of it's
        // subordinate data. 
        // The next step, reconciliation of the objects works to persist (insert, update, and delete) data in the db.
        private static void BuildRequestFromImport(PolicySystemResults importResults, bool IsQuote)
        {
            //Short circuit if there was an issue with the service call
            if (!importResults.Succeeded)
            {
                throw new OperationCanceledException("Cancelled request building because policyResults succeeded was false");
            }
            //Short circuit if no policies are returned
            if (importResults.Policies.Count == 0)
            {
                throw new OperationCanceledException("Cancelled because policies found were 0");
            }

            // From CreateSurveySearch ... create these, all the import factory and then reconcile
            ReconcileInsured oReconcileInsured;
            InsuredBO oInsuredBO;
            
            // 2013 - checks survey types, and then 
            // if the survey request was made by the insured name
                    //...
            // else if the survey type was a not-written type OR the curr.user.company Creates Service Plans from Prospects && I have submission number
                    //...
            // else
                    // trim pn
                    // check survey reason requirements (Will no do that here in 2014)
                    // Bring up the import factory

            // Spin up the appropriate builder to populate the database
            AICTransactionalImportBuilder aicImportBuilder = new AICTransactionalImportBuilder(_request.InputValue, _company);
            oInsuredBO = aicImportBuilder.Build(importResults, false);

                    // check policy system count
                        // use selected company factory for companies who use multiple company
                        // else 
                            // Call the ImportInsuredbyPolicy
                    //Import Agency if its null

            #region from 2013 project
            /*
            AICPolicyStarImportBuilder aicImportBuilder = new AICPolicyStarImportBuilder(policyNumber);
            result = aicImportBuilder.BuildRequestFromPolicy();

            // Need to also check for policies still in A+Plus
            AICImportBuilder aicImportBuilder2 = new AICImportBuilder(result.InsuredEntity.ClientID, string.Empty, string.Empty);
            BlcInsured aPlusResult = aicImportBuilder2.BuildForPolicies();

            // Grab any A+Plus policies, locations, buildings that were returned and add them to the P* result set
            result.PolicyList.AddRange(aPlusResult.PolicyList);
            result.CoverageList.AddRange(aPlusResult.CoverageList);
            result.PolicyLocationList.AddRange(aPlusResult.PolicyLocationList);
            result.BuildingList.AddRange(aPlusResult.BuildingList);
            */
            #endregion

            // Reconcile the results
            oReconcileInsured = new ReconcileInsured(oInsuredBO, _company);
            oReconcileInsured.NewInsert();

            //
            // The survey request has been made, import called and reconciled ... Now they want to have the survey created / completed
            //

            // Check if this survey is a duplicate?
            // Survey, get one with insured id
            // if found, you'll be creating a duplicate
            
            //Survey dupSurvey = Survey.GetOne("InsuredID = ?", gInsuredID);
            //if (dupSurvey != null)
            //{
            //    JavaScript.ShowMessage(this, "You will be creating a duplicate survey request.  Please either use or delete the incomplete survey request below for this account before continuing.");
            //    return false;
            //}

            //commit changes to the Survey record previously created
            //Recording the now found InsuredID after persistance
            using (var db = Application.GetDatabaseInstance())
            {
                // Check if any policies exist
                //Policy[] policies = Policy.GetArray("InsuredID = ?", gInsuredID);
                Data.Models.Policy[] policies = db.Policies.Where(p => p.InsuredId == oReconcileInsured.InsuredEntity.InsuredId).ToArray();

                var dbSurvey = db.Surveys.Where(s => s.SurveyNumber == _request.Survey.Number).FirstOrDefault();
                dbSurvey.InsuredId = oReconcileInsured.InsuredEntity.InsuredId;

                // If/When the company policy system drop down is active and populated, record that in the survey
                //if (cboPolicySystem.SelectedValue != "")
                //{
                //    eSurvey.PolicySystemID = new Guid(cboPolicySystem.SelectedValue);
                //}
                //else
                //{
                //    eSurvey.PolicySystemID = this.CurrentUser.Company.ID;
                //}
                if (!string.IsNullOrEmpty(_request.PolicySystemCode))
                {
                    dbSurvey.PolicySystemId = new Guid(_request.PolicySystemCode);
                }
                else
                {
                    dbSurvey.PolicySystemId = _company.CompanyId;
                }
                

                Data.Models.Policy primaryPolicy = null;
                //if (!SurveyTypeType.IsNotWrittenBusiness(eSurveyType))
                //{
                    //if (policies.Length == 0)
                    //{
                        //JavaScript.ShowMessage(this, "Unable to create a survey request due to no policies being returned for this account.  Please contact your Loss Control Department if you have any questions.");
                        //return false;
                    //}
                    primaryPolicy = db.Policies.Where(p => p.PolicyNumber == _request.InputValue && p.InsuredId == oReconcileInsured.InsuredEntity.InsuredId).FirstOrDefault();
                    dbSurvey.PrimaryPolicyId = (primaryPolicy != null) ? primaryPolicy.PolicyId : db.Policies.Where(p => p.InsuredId == oReconcileInsured.InsuredEntity.InsuredId).Select(p => p.PolicyId).FirstOrDefault();
                //}
                
                //eSurvey.TypeID = SurveyType.GetOne(eSurveyType.Code, this.CurrentUser.Company);
                //eSurvey.TransactionTypeID = (cboTransactionType.Items.Count > 1) ? Guid.Parse(cboTransactionType.SelectedValue) : Guid.Empty;
                //eSurvey.CreateByInternalUserID = (!UserIdentity.Current.IsUnderwriter) ? CurrentUser.ID : Guid.Empty;
                //eSurvey.CreateByExternalUserName = (UserIdentity.Current.IsUnderwriter) ? UserIdentity.Current.Name : string.Empty;
                //eSurvey.DueDate = (CurrentUser.Company.SurveyDaysUntilDue > 0) ? DateTime.Today.AddDays(this.CurrentUser.Company.SurveyDaysUntilDue) : DateTime.MinValue;
                //eSurvey.WorkflowSubmissionNumber = (SurveyTypeType.IsNotWrittenBusiness(eSurveyType) || (CurrentUser.Company.CreateServicePlansFromProspects && numSubmissionNumber.Text.Trim().Length > 0)) ? numSubmissionNumber.Text : string.Empty;
                //eSurvey.UnderwriterCode = eInsured.Underwriter;

                dbSurvey.RequestedDate = (_request.CreateDate != null) ? _request.CreateDate : null;

                // Save these values
                db.SaveChanges();

                //if (_isCWGSurvey)
                //{
                //    SurveyReason surveyReason = new SurveyReason(new Guid(cboSurveyReasonType.SelectedValue), eSurvey.ID);
                //    surveyReason.Save(trans);
                //}
                //else
                //{
                //    //save any reasons checked
                //    //save any reasons checked
                //    foreach (ListItem item in chkListSurveyReasons.Items)
                //    {
                //        if (item.Selected)
                //        {
                //            SurveyReason surveyReason = new SurveyReason(new Guid(item.Value), eSurvey.ID);
                //            surveyReason.Save(trans);
                //        }
                //    }
                //}

                //if (SurveyTypeType.IsNotWrittenBusiness(eSurveyType) || 
                    //(CurrentUser.Company.CreateServicePlansFromProspects && 
                    //  numSubmissionNumber.Text.Trim().Length > 0))
                //{
                //    foreach (Policy policy in policies)
                //    {
                //        if (policy != null)
                //        {
                //            if (policy.LineOfBusiness != null)
                //            {
                //                //stub out the coverages for the newly created lob
                //                ImportFactory factory = new ImportFactory(CurrentUser.Company);
                //                BlcInsured oBlcInsured = factory.GetEmptyCoverages(policy.LineOfBusiness);

                //                foreach (BlcCoverage blcCoverage in oBlcInsured.CoverageList)
                //                {
                //                    Coverage eCoverage = new Coverage(new Guid());
                //                    eCoverage.InsuredID = eSurvey.InsuredID;
                //                    eCoverage.PolicyID = policy.ID;
                //                    eCoverage.TypeCode = blcCoverage.CoverageValueType;

                //                    //get the coverage name
                //                    CoverageNameTable tbl = new CoverageNameTable();
                //                    tbl.Load(this.CurrentUser.Company);
                //                    eCoverage.NameID = tbl.GetId(blcCoverage);

                //                    eCoverage.Save(trans);
                //                }
                //            }
                //            else
                //            {
                //                JavaScript.ShowMessage(this, "Unable to create a survey request from this submission number.  Please select a policy symbol for all of the submissions for this client in the Clearance system and retry generating this request.");
                //                return false;
                //            }
                //        }
                //    }
                //}
            }
            
            //return OnBuildRequestFromImportComplete();
        }

        public void OnBuildRequestFromImportComplete()
        {
            //
        }

        public void OnProcessRequestComplete()
        {
            // stub - no op
            // call Reconcile...  if this is the appropriate place for this 
        }

        private static void queryClearanceImport()
        {

            /******************/
            //var taskStart = DateTime.Now;
            //await Task.Delay(5000); //.NET 4.5 only

            //Delay(12000);

            //Replace above line with call to get data WITH AWAIT here.
            //Put logic to store/return result here
            //var taskEnd = DateTime.Now;
            //var taskDuration = (taskEnd - taskStart);

            //Debug.WriteLine(string.Format("[Task] queryClearanceImport: Started: {0} Completed: {1} Duration {2}", taskStart, taskEnd, taskDuration));
            Debug.WriteLine(string.Format("[Task] queryClearanceImport called"));

        }

        public static void ReconcileRequest()
        {
            // stub - no op
            //  if this is the appropriate place for this 
        }

        private Task Delay(double milliseconds)
        {
            var tcs = new TaskCompletionSource<bool>();
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Elapsed += (obj, args) =>
            {
                tcs.TrySetResult(true);
            };
            timer.Interval = milliseconds;
            timer.AutoReset = false;
            timer.Start();
            return tcs.Task;
        }

        //public async Task TestAsyncProcess()
        //{
        //    this.start = DateTime.Now;
            
        //    Console.WriteLine("TestAsyncProcess started: " + this.start);

        //    //Tasks are started when they are created here. They will run in parallel.
        //    Task task1 = CallAsyncPolicyImport();
        //    Task task2 = CallAsyncClearanceImport();

        //    //Awaiting the tasks' completion here will hold until each one has completed.
        //    //Individual tasks complete when they do.
        //    //The entire group will not complete until all of these tasks are done.
        //    await task1;
        //    await task2;

        //    this.end = DateTime.Now;

        //    Console.WriteLine("TestAsyncProcess ended: " + this.end);

        //    result = true;
        //}

        //async Task CallAsyncPolicyImport()
        //{
        //    var taskStart = DateTime.Now;
        //    await Task.Delay(5000);
        //    //Replace above line with call to get data WITH AWAIT here.
        //    //Put logic to store/return result here
        //    var taskEnd = DateTime.Now;
        //    var taskDuration = (taskEnd - taskStart);

        //    Console.WriteLine(string.Format("[Task] CallAsyncPolicyImport: Started: {0} Completed: {1} Duration {2}", taskStart, taskEnd, taskDuration));

        //}

        //async Task CallAsyncClearanceImport()
        //{
        //    var taskStart = DateTime.Now;
        //    await Task.Delay(12000);
        //    //Replace above line with call to get data WITH AWAIT here.
        //    //Put logic to store/return result here
        //    var taskEnd = DateTime.Now;
        //    var taskDuration = (taskEnd - taskStart);

        //    Console.WriteLine(string.Format("[Task] CallAsyncClearanceImport: Started: {0} Completed: {1} Duration {2}", taskStart, taskEnd, taskDuration));

        //}

        #region Methods for the ImportBuilders

        #endregion
    }

    public class ImportManagerServiceResult
    {
        public bool GenesysPolicyImportComplete = false;
        public bool APlusPolicyImportComplete = false;
        public bool ClearanceImportComplete = false;
        public bool AgencyPortalImportComplete = false;
        public bool BUSImportComplete = false;
        public bool IsComplete = false;

        public ImportManagerServiceResult() { }
    }
}
