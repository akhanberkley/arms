﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using LinqKit;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.Mapping
{
    public class DataToViewModel
    {
        public static Expression<Func<Data.Models.Company, CompanyDetail>> Company()
        {
            return (i => new CompanyDetail()
            {
                CompanyId = i.CompanyId,
                Summary = new CompanyViewModel
                {
                    CompanyName = i.CompanyName,
                    CompanyNumber = i.CompanyNumber,
                    Abbreviation = i.CompanyAbbreviation,
                    LongAbbreviation = i.CompanyLongAbbreviation,
                },
                PrimaryDomainName = i.PrimaryDomainName,
                Dsik = i.Dsik,
                Phone = i.CompanyPhone,
                Fax = i.CompanyFax,
                FeeCompanyUserRoleID = i.FeeCompanyUserRoleId,
                UnderwriterUserRoleID = i.UnderwriterUserRoleId,
                PolicyStarEndPointUrl = i.PolicyStarEndPointUrl,
                PDREndPointUrl = i.PdrEndPointUrl,
                APSEndPointUrl = i.ApsEndPointUrl,
            });
        }

        public static Expression<Func<Data.Models.RequestQueue, RequestViewModel>> Request()
        {
            var surveyMapper = Survey();

            return (i => new RequestViewModel()
            {
                InputTypeCode = i.InputTypeCode,
                InputValue = i.InputValue,
                StatusCode = i.StatusCode,
                CreateDate = i.CreateDate,
                LastModifiedDate = i.LastModifiedDate,
                TrashDate = i.TrashDate,
                CompanyId = i.CompanyId,
                CreateUser = i.CreateUser,
                LastModifiedUser = i.LastModifiedUser,
                LastRetryDate = i.LastRetryDate,
                LastRetryCount = i.LastRetryCount,
                DataAPlus = i.DataAPlus,
                DataAPS = i.DataAPS,
                DataBCS = i.DataBCS,
                DataBUS = i.DataBUS,
                DataClientCore = i.DataClientCore,
                DataGenesys = i.DataGenesys,
                SurveyDbId = i.SurveyId,
                Survey = (i.Survey != null) ? surveyMapper.Invoke(i.Survey) : null,
                //PolicySystemCode =
            });
        }

        public static Expression<Func<Data.Models.User, UserViewModel>> User()
        {
            return (i => new UserViewModel()
            {
                CompanyId = i.CompanyId,
                AccountDisabled = i.AccountDisabled,
                Name = i.Name,
                UserName = i.DomainName + "\\" + i.Username,
                WorkPhone = i.WorkPhone,
                HomePhone = i.HomePhone,
                FaxNumber = i.FaxNumber,
                MobilePhone = i.MobilePhone,
                EmailAddress = i.EmailAddress,
                Address = new AddressViewModel
                {
                    Address1 = i.StreetLine1,
                    Address2 = i.StreetLine2,
                    City = i.City,
                    State = i.StateCode,
                    PostalCode = i.ZipCode,
                },
                IsFeeCompany = i.IsFeeCompany,
                IsUnderwriter = i.IsUnderwriter,
                RoleName = i.UserRole.Name
            });
        }
        public static Expression<Func<Data.Models.UserRole, UserRoleViewModel>> UserRole()
        {
            return (i => new UserRoleViewModel()
            {
                Name = i.Name,
                Description = i.Description,
                PermissionSetId = i.PermissionSetId,
                GeneralPermissions = new UserRoleGeneralPermissions
                {
                    ViewCompanySummary = i.PermissionSet.CanViewCompanySummary,
                    SearchSurveys = i.PermissionSet.CanSearchSurveys,
                    CreateSurveys = i.PermissionSet.CanCreateSurveys,
                    CreateServicePlans = i.PermissionSet.CanCreateServicePlans,
                    ViewSurveyReviewQueues = i.PermissionSet.CanViewSurveyReviewQueues,
                    ViewLinks = i.PermissionSet.CanViewLinks,
                    ViewSurveyHistory = i.PermissionSet.CanViewSurveyHistory,
                    RequireSavedSearch = i.PermissionSet.CanRequireSavedSearch,
                    AdminUserRoles = i.PermissionSet.CanAdminUserRoles,
                    AdminUserAccounts = i.PermissionSet.CanAdminUserAccounts,
                    AdminFeeCompanies = i.PermissionSet.CanAdminFeeCompanies,
                    AdminTerritories = i.PermissionSet.CanAdminTerritories,
                    AdminRules = i.PermissionSet.CanAdminRules,
                    AdminLetters = i.PermissionSet.CanAdminLetters,
                    AdminReports = i.PermissionSet.CanAdminReports,
                    AdminServicePlanTemplates = i.PermissionSet.CanAdminServicePlanFields,
                    AdminResultsDisplay = i.PermissionSet.CanAdminResultsDisplay,
                    AdminRecommendations = i.PermissionSet.CanAdminRecommendations,
                    AdminLinks = i.PermissionSet.CanAdminLinks,
                    AdminUnderwriters = i.PermissionSet.CanAdminUnderwriters,
                    AdminClients = i.PermissionSet.CanAdminClients,
                    AdminActivityTimes = i.PermissionSet.CanAdminActivityTimes,
                    AdminHelpfulHints = i.PermissionSet.CanAdminHelpfulHints,
                    //WorkSurveys = i.PermissionSet.CanWorkSurveys,
                    //WorkReviews = i.PermissionSet.CanWorkReviews,
                    //WorkServicePlans = i.PermissionSet.CanWorkServicePlans,
                    //CanAdminDocuments = i.PermissionSet.CanAdminDocuments
                }
            });
        }

        public static Expression<Func<Data.Models.Agency, AgencyViewModel>> Agency()
        {
            return (i => new AgencyViewModel()
            {
                Code = i.AgencyNumber,
                Name = i.AgencyName,
                Phone = i.PhoneNumber,
                Fax = i.FaxNumber,
                Address = new AddressViewModel
                {
                    Address1 = i.StreetLine1,
                    Address2 = i.StreetLine2,
                    City = i.City,
                    State = i.StateCode,
                    PostalCode = i.ZipCode,
                }
            });
        }

        public static Expression<Func<Data.Models.Insured, InsuredViewModel>> Insured()
        {
            var agencyMapper = Agency();

            return (i => new InsuredViewModel()
            {
                Id = i.ClientId,
                Name = i.Name,
                AdditionalName = i.Name2,
                PrimaryContact = new ContactViewModel { Name = i.InsuredContactName, Email = i.InsuredContactEmail, Phone = i.InsuredContactPhoneNumber },
                Address = new AddressViewModel { Address1 = i.StreetLine1, Address2 = i.StreetLine2, City = i.City, State = i.StateCode, PostalCode = i.ZipCode },
                Agency = (i.Agency != null) ? agencyMapper.Invoke(i.Agency) : null,
                Agent = new ContactViewModel { Name = i.AgentContactName, Email = i.AgentEmail.EmailAddress, Phone = i.AgentContactPhoneNumber }
            });
        }
        public static Expression<Func<Data.Models.Location, LocationViewModel>> Location()
        {
            return (i => new LocationViewModel()
            {
                LocationDbId = i.LocationId,
                Number = i.LocationNumber,
                Description = i.Description,
                Address = new AddressViewModel() { Address1 = i.StreetLine1, Address2 = i.StreetLine2, City = i.City, State = i.StateCode, PostalCode = i.ZipCode },
                Buildings = i.Buildings.OrderBy(b => b.BuildingNumber).Select(b => new LocationBuildingViewModel()
                {
                    Number = b.BuildingNumber,
                    YearBuilt = b.YearBuilt,
                    Sprinkler = (b.HasSprinklerSystem == 1),
                    SprinklerCredit = (b.HasSprinklerSystemCredit == 1),
                    PublicProtection = (b.PublicProtection > 0),
                    Value = b.BuildingValue
                }).ToList(),
                Contacts = i.LocationContacts.OrderBy(c => c.Name)
                                             .Select(c => new LocationContactViewModel()
                                             {
                                                 Name = c.Name,
                                                 Phone = c.PhoneNumber,
                                                 Email = c.EmailAddress,
                                                 Title = c.Title,
                                                 Number = c.PriorityIndex
                                             })
                                             .ToList()
            });
        }
        public static Expression<Func<Data.Models.Policy, PolicyViewModel>> Policy()
        {
            return (i => new PolicyViewModel()
            {
                PolicyDbId = i.PolicyId,
                Number = i.PolicyNumber + "." + i.PolicyMod,
                EffectiveDate = i.EffectiveDate,
                ExpirationDate = i.ExpireDate,
                LineOfBusiness = new CodeListItemViewModel() { Code = i.LineOfBusiness.LineOfBusinessCode, Description = i.LineOfBusiness.LineOfBusinessName },
                PolicySymbol = i.PolicySymbol,
                Premium = i.Premium,
                Carrier = i.Carrier,
                HazardGrade = i.HazardGrade,
                BranchCode = i.BranchCode,
                PrimaryState = i.PolicyStateCode,
                ProfitCenter = (i.ProfitCenter == null) ? null : new CodeListItemViewModel() { Code = i.ProfitCenter.ProfitCenterCode, Description = i.ProfitCenter.ProfitCenterName },
                Coverages = i.Coverages.GroupBy(c => c.CoverageName)
                                       .OrderBy(g => g.Key.DisplayOrder)
                                       .Select(g => new PolicyCoverageViewModel()
                                       {
                                           Name = new CodeListItemViewModel() { Code = g.Key.CoverageNameType.CoverageNameTypeCode, Description = g.Key.CoverageNameType.CoverageNameTypeName },
                                           Items = g.OrderBy(c => c.CoverageType.DisplayOrder)
                                                    .Select(c => new PolicyCoverageItemViewModel() { Type = new CodeListItemViewModel { Code = c.CoverageType.CoverageTypeCode, Description = c.CoverageType.CoverageTypeName }, Value = c.CoverageValue })
                                                    .ToList()
                                       }).ToList()
            });
        }

        public static Expression<Func<Data.Models.Survey, SurveyViewModel>> Survey()
        {
            var insuredMapper = Insured();
            //var policyMapper = Policy();

            return (i => new SurveyViewModel()
            {
                CompanyDbId = i.CompanyId,
                SurveyDbId = i.SurveyId,
                InsuredDbId = i.InsuredId,
                Number = i.SurveyNumber,
                DueDate = i.DueDate,
                Insured = (i.Insured != null) ? insuredMapper.Invoke(i.Insured) : null,
                UnderwriterCode = i.UnderwriterCode,
                SurveyReasons = i.SurveyReasonTypes.OrderBy(t => t.DisplayOrder).Select(t => t.SurveyReasonTypeName).ToList(),
                //PrimaryPolicy = (i.Policy != null) ? policyMapper.Invoke(i.Policy) : null,
            });
        }

        public static Expression<Func<Data.Models.SurveyStatusType, SurveyStatusTypeViewModel>> SurveyStatusType()
        {
            return (i => new SurveyStatusTypeViewModel()
            {
                Code = i.SurveyStatusTypeCode,
                Name = i.SurveyStatusTypeName

            });
        }
        public static Expression<Func<Data.Models.SurveyDocument, SurveyDocumentViewModel>> SurveyDocument()
        {
            return (i => new SurveyDocumentViewModel()
            {
                DocumentType = i.DocTypeCode,
                FileName = i.DocTypeCode,
                EntryDate = i.UploadedOn,
                Number = i.FileNetReference
            });
        }

        //public static Expression<Func<Data.Models.ServicePlan, ServicePlanViewModel>> ServicePlan()
        //{

        //    var ServicePlanTypeMapper = ServicePlanType();
        //    //var InsuredMapper = Insured();
        //    var UserMapper = User();
        //    var PolicyMapper = Policy();
        //    var ServicePlanStatusMapper = ServicePlanStatus();
        //    var ServicePlanNoteMapper = ServicePlanNote();
        //    var ServicePlanHistoryMapper = ServicePlanHistory();
        //    var ServicePlanAttributeMapper = ServicePlanAttribute();
        //    var ServicePlanDocumentMapper = ServicePlanDocument();
        //    //var SurveyMapper = Survey();
        //    return (i => new ServicePlanViewModel()
        //    {
        //        ID = i.ServicePlanId,
        //        Number = i.ServicePlanNumber,
        //        Type = (i.ServicePlanType == null) ? null : ServicePlanTypeMapper.Invoke(i.ServicePlanType),
        //        //Insured = (i.Insured == null) ? null : InsuredMapper.Invoke(i.Insured),
        //        AssignedUser = (i.User_AssignedUserId == null) ? null : UserMapper.Invoke(i.User_AssignedUserId),
        //        CreatedByUser = (i.User_CreatedByUserId == null) ? null : UserMapper.Invoke(i.User_CreatedByUserId),
        //        PrimaryPolicy = (i.Policy == null) ? null : PolicyMapper.Invoke(i.Policy),
        //        ServicePlanStatus = (i.ServicePlanStatus == null) ? null : ServicePlanStatusMapper.Invoke(i.ServicePlanStatus),
        //        ServicePlanNote = i.ServicePlanNotes.Select(n => ServicePlanNoteMapper.Invoke(n)).ToList(),
        //        ServicePlanHistory = i.ServicePlanHistories.Select(h => ServicePlanHistoryMapper.Invoke(h)).ToList(),
        //        CompleteDate = i.CompleteDate,
        //        LastModifiedOn = i.LastModifiedOn,
        //        ServicePlanAttribute = i.ServicePlanAttributes.OrderBy(s => s.CompanyAttribute.DisplayOrder).Select(h => ServicePlanAttributeMapper.Invoke(h)).ToList(),
        //        ServicePlanDocument = i.ServicePlanDocuments.Select(d => ServicePlanDocumentMapper.Invoke(d)).ToList(),
        //        //Survey = (i.Survey == null) ? null : SurveyMapper.Invoke(i.Survey)


        //    });
        //}
        //public static Expression<Func<Data.Models.ServicePlanHistory, ServicePlanHistoryViewModel>> ServicePlanHistory()
        //{

        //    return (i => new ServicePlanHistoryViewModel()
        //    {
        //        EntryDate = i.EntryDate,
        //        EntryText = i.EntryText
        //    });
        //}
        //public static Expression<Func<Data.Models.ServicePlanNote, ServicePlanNoteViewModel>> ServicePlanNote()
        //{
        //    var UserMapper = User();

        //    return (i => new ServicePlanNoteViewModel()
        //    {
        //        Comment = i.Comment,
        //        EntryDate = i.EntryDate,
        //        EntryBy = (i.User == null) ? null : UserMapper.Invoke(i.User)

        //    });
        //}
        //public static Expression<Func<Data.Models.ServicePlanStatus, ServicePlanStatusViewModel>> ServicePlanStatus()
        //{

        //    return (i => new ServicePlanStatusViewModel()
        //    {
        //        Code = i.ServicePlanStatusCode,
        //        Name = i.ServicePlanStatusName

        //    });
        //}
        //public static Expression<Func<Data.Models.ServicePlanType, ServicePlanTypeViewModel>> ServicePlanType()
        //{

        //    return (i => new ServicePlanTypeViewModel()
        //    {
        //        Name = i.ServicePlanTypeName
        //    });
        //}
        //public static Expression<Func<Data.Models.ServicePlanGridColumnFilterCompany, ServicePlanGridColumnFilterCompanyViewModel>> ServicePlanGridColumnFilterCompany()
        //{
        //    var ServicePlanGridColumnFilterMapper = ServicePlanGridColumnFilter();
        //    return (i => new ServicePlanGridColumnFilterCompanyViewModel()
        //    {
        //        ServicePlanGridColumnFilterID = i.ServicePlanGridColumnFilterId,
        //        Display = i.Display,
        //        Required = i.Required,
        //        GridColumnFilter = (i.ServicePlanGridColumnFilter == null) ? null : ServicePlanGridColumnFilterMapper.Invoke(i.ServicePlanGridColumnFilter)


        //    });
        //}
        //public static Expression<Func<Data.Models.ServicePlanGridColumnFilter, ServicePlanGridColumnFilterViewModel>> ServicePlanGridColumnFilter()
        //{
        //    return (i => new ServicePlanGridColumnFilterViewModel()
        //    {
        //        ID = i.ServicePlanGridColumnFilterId,
        //        Name = i.Name,
        //        FilterExpression = i.FilterExpression,
        //        SortExpression = i.SortExpression,
        //        DotNetDataType = i.DotNetDataType
        //    });
        //}
        //public static Expression<Func<Data.Models.ServicePlanDocument, ServicePlanDocumentViewModel>> ServicePlanDocument()
        //{
        //    var UserMapper = User();

        //    return (i => new ServicePlanDocumentViewModel()
        //    {
        //        DocumentName = i.DocumentName,
        //        FileNetReference = i.FileNetReference,
        //        ID = i.ServicePlanDocumentId,
        //        SizeInBytes = i.SizeInBytes,
        //        UploadedOn = i.UploadedOn,
        //        UploadedUser = (i.User == null) ? null : UserMapper.Invoke(i.User)

        //    });
        //}
        //public static Expression<Func<Data.Models.ServicePlanAttribute, ServicePlanAttributeViewModel>> ServicePlanAttribute()
        //{
        //    var CompanyAttributeMapper = CompanyAttribute();

        //    return (i => new ServicePlanAttributeViewModel()
        //    {
        //        CompanyAttribute = (i.CompanyAttribute == null) ? null : CompanyAttributeMapper.Invoke(i.CompanyAttribute),
        //        AttributeValue = i.AttributeValue
        //    });
        //}

        public static Expression<Func<Data.Models.MergeDocument, DocumentViewModel>> Document()
        {
            return (i => new DocumentViewModel()
            {
                ID = i.MergeDocumentId,
                Name = i.MergeDocumentName,
                Disabled = i.Disabled,
                DaysUntilReminder = i.DaysUntilReminder,
                DocumentVersions = i.MergeDocumentVersions.Select(v => new DocumentVersionsViewModel()
                {
                    VersionNumber = v.VersionNumber,
                    DocumentName = v.DocumentName,
                    FileNetReference = v.FileNetReference,
                    UploadedOn = v.UploadedOn,
                    IsActive = v.IsActive
                }).ToList()

            });
        }

        #region Other Mappers
        public static Expression<Func<Data.Models.Territory, TerritoryViewModel>> Territory()
        {
            var UserMapper = User();
            var SurveyStatusTypeMapper = SurveyStatusType();
            return (i => new TerritoryViewModel()
           {
               ID = i.TerritoryId,
               Name = i.TerritoryName,
               Description = i.TerritoryDescription,
               InHouseSelection = i.InHouseSelection,
               SurveyStatusType = (i.SurveyStatusType == null) ? null : SurveyStatusTypeMapper.Invoke(i.SurveyStatusType),
               UserTerritories = i.UserTerritories.Select(ut => new UserTerritoriesViewModel()
               {
                   User = UserMapper.Invoke(ut.User),
                   PercentAllocated = ut.PercentAllocated,
                   MaxPremiumAmount = ut.MaxPremiumAmount,
                   MinPremiumAmount = ut.MinPremiumAmount
               }).ToList()

           });
        }
        public static Expression<Func<Data.Models.GridColumnFilterCompany, GridColumnFilterCompanyViewModel>> GridColumnFilterCompany()
        {
            return (i => new GridColumnFilterCompanyViewModel()
            {
                GridColumnFilterID = i.GridColumnFilterId,
                Display = i.Display,
                Required = i.Required,
                IsExternal = i.IsExternal,
                GridColumnFilter = (i.GridColumnFilter == null) ? null : new GridColumnFilterViewModel()
                {
                    ID = i.GridColumnFilter.GridColumnFilterId,
                    Name = i.GridColumnFilter.Name,
                    FilterExpression = i.GridColumnFilter.FilterExpression,
                    SortExpression = i.GridColumnFilter.SortExpression,
                    DotNetDataType = i.GridColumnFilter.DotNetDataType
                }
            });
        }
        public static Expression<Func<Data.Models.ActivityType, ActivityTypeViewModel>> ActivityType()
        {

            return (i => new ActivityTypeViewModel()
            {
                ActivityTypeID = i.ActivityTypeId,
                ActivityTypeCode = i.ActivityTypeLevelCode,
                ActivityTypeName = i.ActivityTypeName

            });
        }
        public static Expression<Func<Data.Models.AssignmentRule, AssignmentRuleViewModel>> AssignmentRule()
        {

            var AssignmentRuleActionMapper = AssignmentRuleAction();
            var SurveyStatusTypeMapper = SurveyStatusType();
            var FilterMapper = Filter();

            return (i => new AssignmentRuleViewModel()
            {
                ID = i.AssignmentRuleId,
                PriorityIndex = i.PriorityIndex,
                AssignmentRuleAction = (i.AssignmentRuleAction == null) ? null : AssignmentRuleActionMapper.Invoke(i.AssignmentRuleAction),
                SurveyStatusType = (i.SurveyStatusType == null) ? null : SurveyStatusTypeMapper.Invoke(i.SurveyStatusType),
                Filter = (i.Filter == null) ? null : FilterMapper.Invoke(i.Filter)
            });
        }
        public static Expression<Func<Data.Models.Filter, FilterViewModel>> Filter()
        {
            var FilterConditionMapper = FilterCondition();
            return (i => new FilterViewModel()
            {
                FilterId = i.FilterId,
                FilterCondition = i.FilterConditions.Select(f => FilterConditionMapper.Invoke(f)).ToList()

            });
        }
        public static Expression<Func<Data.Models.Link, LinkViewModel>> Link()
        {
            return (l => new LinkViewModel()
            {
                ID = l.LinkId,
                Description = l.LinkDescription,
                URL = l.LinkUrl,
                DisplayOrder = l.PriorityIndex
            });

        }
        public static Expression<Func<Data.Models.FilterCondition, FilterConditionViewModel>> FilterCondition()
        {
            var FilterFieldMapper = FilterField();
            var FilterOperatorMapper = FilterOperator();
            return (i => new FilterConditionViewModel()
            {
                CompareValue = i.CompareValue,
                DisplayValue = i.DisplayValue,
                FilterField = (i.FilterField == null) ? null : FilterFieldMapper.Invoke(i.FilterField),
                FilterOperator = (i.FilterOperator == null) ? null : FilterOperatorMapper.Invoke(i.FilterOperator),

            });
        }
        public static Expression<Func<Data.Models.AssignmentRuleAction, AssignmentRuleActionViewModel>> AssignmentRuleAction()
        {

            var AssignmentRuleActionTypeMapper = AssignmentRuleActionType();

            return (i => new AssignmentRuleActionViewModel()
            {
                Code = i.AssignmentRuleActionCode,
                DisplayName = i.DisplayName,
                Type = (i.AssignmentRuleActionType == null) ? null : AssignmentRuleActionTypeMapper.Invoke(i.AssignmentRuleActionType)

            });
        }
        public static Expression<Func<Data.Models.FilterField, FilterFieldViewModel>> FilterField()
        {

            return (i => new FilterFieldViewModel()
            {
                Code = i.FilterFieldCode,
                Name = i.FilterFieldName,
                SourceColumn = i.SourceColumn,
                SourceTable = i.SourceTable,
                SurveyStatusTypeCode = i.SurveyStatusTypeCode,
                ComboEntity = i.ComboEntity


            });
        }
        public static Expression<Func<Data.Models.FilterOperator, FilterOperatorViewModel>> FilterOperator()
        {

            return (i => new FilterOperatorViewModel()
            {
                Code = i.FilterOperatorCode,
                Name = i.FilterOperatorName,
                DisplayName = i.DisplayName,
                DisplayShortName = i.DisplayShortName,
                ForStringsOnly = i.ForStringsOnly

            });
        }
        public static Expression<Func<Data.Models.AssignmentRuleActionType, AssignmentRuleActionTypeViewModel>> AssignmentRuleActionType()
        {


            return (i => new AssignmentRuleActionTypeViewModel()
            {
                Code = i.AssignmentRuleActionTypeCode,
                Name = i.AssignmentRuleActionTypeName

            });
        }


        //public static Expression<Func<Data.Models.RateModificationFactor, RateModificationFactorViewModel>> RateModificationFactor()
        //{

        //    var RateModificationFactorTypeMapper = RateModificationFactorType();

        //    return (i => new RateModificationFactorViewModel()
        //    {
        //        ID = i.RateModificationFactorId,
        //        Rate = i.Rate,
        //        StateCode = i.StateCode,
        //        RateModificationFactorType = (i.RateModificationFactorType == null) ? null : RateModificationFactorTypeMapper.Invoke(i.RateModificationFactorType),

        //    });
        //}
        #endregion
    }
}


