﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ViewModels
{
    public class DocumentPageViewModel
    {
        public List<DocumentViewModel> Documents { get; set; }        
        public List<DocumentTypesViewModel> DocumentTypes { get; set; }
        public DocumentViewModel NewDocument { get; set; }

    }

    public class DocumentViewModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public bool Disabled { get; set; }
        public bool IsLetter { get; set; }
        public int? DaysUntilReminder { get; set; }
        public string ReminderDateTypeCode { get; set; }
        public int PriorityIndex { get; set; }
        public int Type { get; set; }
        public bool IsCritical { get; set; }
        public List<DocumentVersionsViewModel> DocumentVersions { get; set; }

    }

    public class DocumentVersionsViewModel
    {
        public int VersionNumber { get; set; }
        public bool IsActive { get; set; }
        public string DocumentName { get; set; }
        public string MimeType { get; set; }
        public long SizeInBytes { get; set; }
        public string FileNetReference { get; set; }
        public DateTime UploadedOn { get; set; }

    }

    public class DocumentTypesViewModel
    {
        public string Name { get; set; }

    }
}
