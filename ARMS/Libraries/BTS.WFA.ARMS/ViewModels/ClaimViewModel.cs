﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ViewModels
{
    public class ClaimViewModel
    {     
        public string PolicyNumber { get; set; }
        public int? PolicyMod { get; set; }
        public string PolicySymbol { get; set; }
        public DateTime? PolicyExpireDate { get; set; }
        public string Number { get; set; }
        public DateTime LossDate { get; set; }
        public string Claimant { get; set; }
        public decimal Amount { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public string LossType { get; set; }        

    }
}
