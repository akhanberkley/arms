﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class ActivityViewModel : BaseViewModel
    {
        public Guid ID { get; set; }
        public DateTime Date { get; set; }
        public decimal? Hours { get; set; }
        public decimal? Days { get; set; }
        public decimal? CallCount { get; set; }
        public string Comments { get; set; }
        public ActivityTypeViewModel ActivityType { get; set; }
        public UserViewModel AllocatedTo { get; set; }
     
    }

    public class ActivityTypeViewModel
    {
        public Guid ActivityTypeID { get; set; }
        public string ActivityTypeCode { get; set; }
        public string ActivityTypeName { get; set; }
    }
}
