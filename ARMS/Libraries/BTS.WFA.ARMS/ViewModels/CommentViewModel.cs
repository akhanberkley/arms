﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ViewModels
{
    public class CommentViewModel
    {
        public string Author { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
    }
    public class SurveyCommentViewModel : CommentViewModel
    {
        public Guid? Id { get; set; }
        public bool Internal { get; set; }
    }
}
