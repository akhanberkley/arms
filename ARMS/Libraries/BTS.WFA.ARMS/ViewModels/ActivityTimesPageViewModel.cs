﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;


namespace BTS.WFA.ARMS.ViewModels
{



    public class ActivityTimesPageViewModel : BaseViewModel 
    {

        public List<ActivityViewModel> Activities { get; set; }
        public List<UserViewModel> Users { get; set; }
        public ActivityViewModel NewActivityTime { get; set; }
        public List<ActivityTypeViewModel> ActivityTypes { get; set; }
        public List<CodeListItemViewModel> Years { get; set; }
        public List<CodeListItemViewModel> Months { get; set; }
    
    }
}
