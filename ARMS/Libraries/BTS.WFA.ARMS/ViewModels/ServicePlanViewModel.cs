﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BTS.WFA.ARMS.ViewModels
{
    public class ServicePlanPageViewModel
    {
        public List<ServicePlanViewModel> ServicePlans { get; set; }
        public List<ServicePlanStatusViewModel> ServicePlanStatus { get; set; }
        public List<UserViewModel> Users { get; set; }
        public List<ServicePlanNoteViewModel> ServicePlanNote { get; set; }
    }

    public class ServicePlanViewModel
    {
        public Guid ID { get; set; }
        public int Number { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateStateCode { get; set; }
        public DateTime? CompleteDate { get; set; }
        public DateTime LastUnderwriterReportDate { get; set; }
        public DateTime NextFullReportDueDate { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public UserViewModel AssignedUser { get; set; }
        public UserViewModel CreatedByUser { get; set; }
        public InsuredViewModel Insured { get; set; }
        public ServicePlanViewModel PreviousServicePlan { get; set; }
        public PolicyViewModel PrimaryPolicy { get; set; }
        public string ServicePlanStatusCode { get; set; }
        public SurveyGradingViewModel SurveyGrading { get; set; }
        public ServicePlanTypeViewModel Type { get; set; }
        public List<ServicePlanNoteViewModel> ServicePlanNote { get; set; }
        public List<ServicePlanHistoryViewModel> ServicePlanHistory { get; set; }
        public List<ServicePlanAttributeViewModel> ServicePlanAttribute { get; set; }
        public List<ServicePlanSurveyViewModel> ServicePlanSurvey { get; set; }
        public ServicePlanStatusViewModel ServicePlanStatus { get; set; }
        public List<ServicePlanDocumentViewModel> ServicePlanDocument { get; set; }
        public SurveyViewModel Survey { get; set; }

    }
    public class ServicePlanSurveyViewModel
    {
        public Guid ServicePlanID { get; set; }
        public Guid SurveyID { get; set; }
        public SurveyViewModel Survey { get; set; }
    }

    public class ServicePlanDocumentViewModel
    {
        public Guid ID { get; set; }
        public string DocumentName { get; set; }
        public long SizeInBytes { get; set; }
        public string FileNetReference { get; set; }
        public DateTime UploadedOn { get; set; }
        public UserViewModel UploadedUser { get; set; }


    }
    public class CompanyAttributeViewModel
    {
        public Guid ID { get; set; }
        public string Label { get; set; }
        public string AttributeDataTypeCode { get; set; }


    }
    public class ServicePlanAttributeViewModel
    {
        public Guid ID { get; set; }
        public CompanyAttributeViewModel CompanyAttribute { get; set; }
        public string AttributeValue { get; set; }

    }
    public class ServicePlanHistoryViewModel
    {
        public DateTime EntryDate { get; set; }
        public string EntryText { get; set; }
    }


    public class ServicePlanNoteViewModel
    {
        public DateTime EntryDate { get; set; }
        public string Comment { get; set; }
        public UserViewModel EntryBy { get; set; }
    }

    public class ServicePlanStatusViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }


    public class SurveyGradingViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class ServicePlanTypeViewModel
    {
        public string Name { get; set; }
        public bool Disabled { get; set; }
    }
}
