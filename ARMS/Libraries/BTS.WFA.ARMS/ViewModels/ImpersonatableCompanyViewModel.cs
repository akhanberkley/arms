﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class ImpersonatableCompanyViewModel
    {
        public ImpersonatableCompanyViewModel()
        {
            Users = new List<CodeListItemViewModel>();
        }

        [IgnoreDataMember]
        public Guid? CompanyId { get; set; }
        public CompanyViewModel Company { get { return Application.CompanyKeyMap.Values.FirstOrDefault(c => c.CompanyId == CompanyId).Summary; } }

        public List<CodeListItemViewModel> Users { get; set; }
    }
}
