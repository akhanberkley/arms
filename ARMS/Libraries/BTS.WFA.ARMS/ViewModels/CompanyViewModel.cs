﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class CompanyViewModel
    {
        public string CompanyNumber { get; set; }
        public string CompanyName { get; set; }
        public string Abbreviation { get; set; }
        public string LongAbbreviation { get; set; }
    }

    [TypeConverter(typeof(CompanyDetailModelConverter))]
    public class CompanyDetail
    {
        public CompanyDetail()
        {
            UserRoles = new Dictionary<string, UserRoleViewModel>();
            LinesOfBusiness = new List<LineOfBusinessViewModel>();
        }

        public Guid CompanyId { get; set; }
        public CompanyViewModel Summary { get; set; }
        public string PrimaryDomainName { get; set; }
        public int? Dsik { get; set; }

        public string Phone { get; set; }
        public string Fax { get; set; }
        public int SurveyDaysUntilDue { get; set; }
        public int SurveyNearDue { get; set; }
        public int ReviewDaysUntilDue { get; set; }
        public int ReviewNearDue { get; set; }
        public string EmailFromAddress { get; set; }
        public int NextFullReportDaysUntilDue { get; set; }
        public int YearsOfLossHistory { get; set; }
        public Guid FeeCompanyUserRoleID { get; set; }
        public Guid? UnderwriterUserRoleID { get; set; }

        public string PolicyStarEndPointUrl { get; set; }
        public string PDREndPointUrl { get; set; }
        public string APSEndPointUrl { get; set; }

        public Dictionary<string, UserRoleViewModel> UserRoles { get; set; }
        public List<LineOfBusinessViewModel> LinesOfBusiness { get; set; }
    }

    public class CompanyDetailModelConverter : TypeConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return false;
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            var key = value as string;
            if (!String.IsNullOrEmpty(key))
            {
                key = key.ToUpper();
                if (Application.CompanyKeyMap.ContainsKey(key))
                    return Application.CompanyKeyMap[key];
            }

            return base.ConvertFrom(context, culture, value);
        }
    }
}
