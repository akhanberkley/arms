﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{

    public class RecsPageViewModel 
    { 
        public List<RecViewModel> Recs { get; set; }
        public RecViewModel NewRec { get; set; }
    
    }
    public class RecViewModel : BaseViewModel
    {
        public Guid ID { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Disabled { get; set; }   

    }

    public class RecClassificationViewModel
    {
        public Guid ID { get; set; }      
        public bool RequiredOnSurveyCompletion { get; set; }
        public CodeListItemViewModel Type { get; set; }

    }

    public class RecStatusViewModel
    {
        public Guid ID { get; set; }     
        public CodeListItemViewModel Type { get; set; }
    }
}
