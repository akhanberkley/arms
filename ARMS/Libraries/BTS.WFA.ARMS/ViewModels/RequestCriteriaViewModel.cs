﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ViewModels
{
    public class RequestCriteriaViewModel
    {
        public string InputTypeCode { get; set; }
        public string PolicySystemCode { get; set; }
        //public string CompanyId { get; set; }
        public string InputValue { get; set; }
        //public string UserId { get; set; }
    }
}
