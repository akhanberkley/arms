﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BTS.WFA.ARMS.ViewModels
{
    public class LocationViewModel
    {
        public LocationViewModel()
        {
            Buildings = new List<LocationBuildingViewModel>();
            Comments = new List<CommentViewModel>();
            Contacts = new List<LocationContactViewModel>();
        }

        public bool Included { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public AddressViewModel Address { get; set; }

        public List<LocationContactViewModel> Contacts { get; set; }
        public List<LocationBuildingViewModel> Buildings { get; set; }
        public List<CommentViewModel> Comments { get; set; }

        [IgnoreDataMember]
        public Guid LocationDbId { get; set; }
    }

    public class LocationBuildingViewModel
    {
        public bool Included { get; set; }
        public short? Number { get; set; }
        public int? YearBuilt { get; set; }
        public bool Sprinkler { get; set; }
        public bool SprinklerCredit { get; set; }
        public bool PublicProtection { get; set; }
        public decimal? Value { get; set; }
    }
}
