﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ViewModels
{
    public class StateViewModel
    {
        public string StateName { get; set; }
        public string Abbreviation { get; set; }
    }
}
