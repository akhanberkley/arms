﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class ObjectiveViewModel
    {

        public Guid ID { get; set; }
        public int Number { get; set; }        
        public string Text { get; set; }
        public bool IsPrint { get; set; }
        public string CommentsText { get; set; }
        public DateTime TargetDate { get; set; }
        public string StatusCode { get; set; }
        public CodeListItemViewModel Status { get; set; }

    }       

}
