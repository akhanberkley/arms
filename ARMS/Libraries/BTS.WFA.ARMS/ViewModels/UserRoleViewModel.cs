﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class UserRoleViewModel : BaseViewModel
    {
        public UserRoleViewModel()
        {
            SurveyStatusPermissions = new List<SurveyStatusPermissions>();
            ServicePlanStatusPermissions = new List<ServicePlanStatusPermissions>();
        }

        public string Name { get; set; }
        public string Description { get; set; }

        public UserRoleGeneralPermissions GeneralPermissions { get; set; }
        public List<SurveyStatusPermissions> SurveyStatusPermissions { get; set; }
        public List<ServicePlanStatusPermissions> ServicePlanStatusPermissions { get; set; }

        [IgnoreDataMember]
        public Guid PermissionSetId { get; set; }
    }

    public class UserRoleGeneralPermissions
    {
        public bool ViewCompanySummary { get; set; }
        public bool SearchSurveys { get; set; }
        public bool CreateSurveys { get; set; }
        public bool CreateServicePlans { get; set; }
        public bool ViewSurveyReviewQueues { get; set; }
        public bool ViewLinks { get; set; }
        public bool ViewSurveyHistory { get; set; }
        public bool RequireSavedSearch { get; set; }
        public bool AdminUserRoles { get; set; }
        public bool AdminUserAccounts { get; set; }
        public bool AdminFeeCompanies { get; set; }
        public bool AdminTerritories { get; set; }
        public bool AdminRules { get; set; }
        public bool AdminLetters { get; set; }
        public bool AdminReports { get; set; }
        public bool AdminServicePlanTemplates { get; set; }
        public bool AdminResultsDisplay { get; set; }
        public bool AdminRecommendations { get; set; }
        public bool AdminLinks { get; set; }
        public bool AdminUnderwriters { get; set; }
        public bool AdminClients { get; set; }
        public bool AdminActivityTimes { get; set; }
        public bool AdminHelpfulHints { get; set; }
        //public bool WorkSurveys { get; set; }
        //public bool WorkReviews { get; set; }
        //public bool WorkServicePlans { get; set; }
        //public bool CanAdminDocuments { get; set; }
        //public bool CanWorkAgencyVisits { get; set; }
    }

    public class SurveyStatusPermissions
    {
        public SurveyStatusPermissions()
        {
            Actions = new List<SurveyActionPermissionViewModel>();
        }

        public CodeListItemViewModel Status { get; set; }
        public List<SurveyActionPermissionViewModel> Actions { get; set; }
    }

    public class ServicePlanStatusPermissions
    {
        public ServicePlanStatusPermissions()
        {
            Actions = new List<ServicePlanActionPermissionViewModel>();
        }

        public CodeListItemViewModel Status { get; set; }
        public List<ServicePlanActionPermissionViewModel> Actions { get; set; }
    }

    public class PermissionViewModel
    {
        private static int[] selfSettings = new[] { 1, 4, 6, 7 };
        private static int[] employeeSettings = new[] { 2, 4, 5, 7 };
        private static int[] vendorSettings = new[] { 3, 5, 6, 7 };

        public PermissionViewModel(int setting)
        {
            Self = (selfSettings.Contains(setting));
            Employee = (employeeSettings.Contains(setting));
            Vendor = (vendorSettings.Contains(setting));
        }

        public bool Self { get; set; }
        public bool Employee { get; set; }
        public bool Vendor { get; set; }
    }
    public class SurveyActionPermissionViewModel
    {
        public CodeListItemViewModel Action { get; set; }
        public PermissionViewModel Survey { get; set; }
        public PermissionViewModel ServiceVisit { get; set; }
    }
    public class ServicePlanActionPermissionViewModel
    {
        public CodeListItemViewModel Action { get; set; }
        public PermissionViewModel Permission { get; set; }
    }
}
