﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ViewModels
{
    public class TerritoryPageViewModel
    {

        public List<UserViewModel> User { get; set; }
        public List<TerritoryViewModel> Territory { get; set; }      

    }
    
    public class TerritoryViewModel
    {
        public TerritoryViewModel()
        {
            UserTerritories = new List<UserTerritoriesViewModel>();
        }
        public Guid ID { get; set; }            
        public string Name { get; set; }
        public string Description { get; set; }
        public bool InHouseSelection { get; set; }        
        public List<UserTerritoriesViewModel> UserTerritories { get; set; }
        public SurveyStatusTypeViewModel SurveyStatusType { get; set; }

    }

    public class UserTerritoriesViewModel
    {

        public UserViewModel User { get; set; }      
        public int? PercentAllocated { get; set; }
        public decimal? MinPremiumAmount { get; set; }
        public decimal? MaxPremiumAmount { get; set; }       

    }


}
