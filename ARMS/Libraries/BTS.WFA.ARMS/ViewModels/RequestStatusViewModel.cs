﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ViewModels
{
    public class RequestStatusViewModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string IconHtmlName { get; set; }
    }
}
