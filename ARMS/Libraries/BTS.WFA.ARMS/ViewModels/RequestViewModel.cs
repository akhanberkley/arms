﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class RequestViewModel : BaseViewModel
    {
        [IgnoreDataMember]
        public Guid? CompanyId { get; set; }
        public CompanyViewModel Company { get { return Application.CompanyKeyMap.Values.FirstOrDefault(c => c.CompanyId == CompanyId).Summary; } }

        public string InputTypeCode { get; set; }
        public string InputValue { get; set; }
        public string PolicySystemCode { get; set; }
        public string StatusCode { get; set; }

        public string CreateUser { get; set; }
        public string LastModifiedUser { get; set; }

        public DateTime? CreateDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? TrashDate { get; set; }
        public DateTime? LastRetryDate { get; set; }
        public string LastRetryCount { get; set; }

        public string DataClientCore { get; set; }
        public string DataBCS { get; set; }
        public string DataAPS { get; set; }
        public string DataAPlus { get; set; }
        public string DataBUS { get; set; }
        public string DataGenesys { get; set; }

        public SurveyViewModel Survey { get; set; }

        [IgnoreDataMember]
        public Guid? SurveyDbId { get; set; }
    }
}
