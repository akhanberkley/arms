﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class CoverageViewModel
    {          
        public string Value { get; set; }
        public CoverageNameViewModel Name { get; set; }
        public CodeListItemViewModel Type { get; set; }
        public CodeListItemViewModel NameType { get; set; }        
        public CodeListItemViewModel CoverageType { get; set; }
        public List<CoverageDetailViewModel> CoverageDetail { get; set; }

    }

    public class CoverageNameViewModel
    {   
        public string LineOfBusinessCode { get; set; }
        public string SubLineOfBusinessCode { get; set; }
        public string TypeCode { get; set; }
        public CodeListItemViewModel CoverageNameType { get; set; }
        public CodeListItemViewModel LineOfBusiness { get; set; } 
    }

    public class CoverageDetailViewModel
    {
      
        public string DetailValue { get; set; }
        public DateTime DateCreated { get; set; }
    }

}
