﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ViewModels
{
    public class AgencyViewModel
    {    
        public string Code { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }

        public AddressViewModel Address { get; set; }
    }
}
