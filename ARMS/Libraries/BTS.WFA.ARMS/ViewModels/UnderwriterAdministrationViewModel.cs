﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Services
{
    public class UnderwriterAdministrationViewModel
    {

         public UnderwriterAdministrationViewModel()
        {
            Underwriters = new List<UnderwriterViewModel>();
            ProfitCenters = new List<CodeListItemViewModel>();
        }

        public UnderwriterViewModel NewUnderwriter { get; set; }
        
        public List<UnderwriterViewModel> Underwriters { get; set; }
        public List<CodeListItemViewModel> ProfitCenters { get; set; }
        
    }
     
}
