﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class WebPageHelpfulHintViewModel : BaseViewModel
    {
        public string WebPagePath  { get; set; }
        public string WebPageDescription  { get; set; }
        public string HelpfulHintText  { get; set; }
        public bool AlwaysVisible  { get; set; }
    }
}
