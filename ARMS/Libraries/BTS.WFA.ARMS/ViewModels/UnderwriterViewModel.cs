﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;


namespace BTS.WFA.ARMS.ViewModels
{
    public class UnderwriterAdministrationViewModel
    {

        public UnderwriterAdministrationViewModel()
        {
            Underwriters = new List<UnderwriterViewModel>();
            ProfitCenters = new List<CodeListItemViewModel>();
        }

        public UnderwriterViewModel NewUnderwriter { get; set; }

        public List<UnderwriterViewModel> Underwriters { get; set; }
        public List<CodeListItemViewModel> ProfitCenters { get; set; }

    }
    
    public class UnderwriterViewModel : BaseViewModel
    {

        public string Code { get; set; }     
        public string Name { get; set; }
        public string DomainName { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneExt { get; set; }
        public string EmailAddress { get; set; }
        public string Title { get; set; }         
        public bool Disabled { get; set; }
        public CodeListItemViewModel ProfitCenter { get; set; }     
     
    }
}
