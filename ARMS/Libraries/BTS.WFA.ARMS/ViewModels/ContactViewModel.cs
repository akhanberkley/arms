﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ViewModels
{
    public class ContactViewModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class LocationContactViewModel : ContactViewModel
    {
        public int Number { get; set; }
        public string Title { get; set; }
    }
}
