﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class UserViewModel : BaseViewModel
    {
        [IgnoreDataMember]
        public Guid? CompanyId { get; set; }
        public CompanyViewModel Company { get { return Application.CompanyKeyMap.Values.FirstOrDefault(c => c.CompanyId == CompanyId).Summary; } }

        public bool AccountDisabled { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }

        public string EmailAddress { get; set; }
        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string FaxNumber { get; set; }
        public AddressViewModel Address { get; set; }

        public bool IsSystemAdministrator { get { return Application.SystemAdministratorUserNames.Any(u => u.Equals(UserName, StringComparison.CurrentCultureIgnoreCase)); } }
        public bool IsFeeCompany { get; set; }
        public bool IsUnderwriter { get; set; }

        [IgnoreDataMember]
        public string RoleName { get; set; }
        public UserRoleViewModel Role { get { return Application.CompanyKeyMap[Company.LongAbbreviation].UserRoles[RoleName]; } }
    }

    //public class UserSearchViewModel
    //{
    //    public string Name { get; set; }

    //}
}
