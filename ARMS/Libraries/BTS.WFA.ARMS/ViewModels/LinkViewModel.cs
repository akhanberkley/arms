﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class LinkViewModel : BaseViewModel
    {
        public Guid ID { get; set; }       
        public string Description { get; set; }
        public string URL { get; set; }
        public Int32 DisplayOrder { get; set; }
    }
}
