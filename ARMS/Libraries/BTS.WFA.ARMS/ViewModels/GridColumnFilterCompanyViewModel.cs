﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ViewModels
{
    public class GridColumnFilterCompanyViewModel
    {
        public Guid GridColumnFilterID { get; set; }    
        public bool Display { get; set; }
        public bool Required { get; set; }
        public bool IsExternal { get; set; }        
        public GridColumnFilterViewModel GridColumnFilter { get; set; }
    }

    public class GridColumnFilterViewModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string FilterExpression { get; set; }
        public string SortExpression { get; set; }
        public string DotNetDataType { get; set; }
      
    }

    public class ServicePlanGridColumnFilterCompanyViewModel
    {
        public Guid ServicePlanGridColumnFilterID { get; set; }      
        public bool Display { get; set; }
        public bool Required { get; set; }
        public bool IsExternal { get; set; }        
        public ServicePlanGridColumnFilterViewModel GridColumnFilter { get; set; }
    }

    public class ServicePlanGridColumnFilterViewModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string FilterExpression { get; set; }
        public string SortExpression { get; set; }
        public string DotNetDataType { get; set; }

    }


}
