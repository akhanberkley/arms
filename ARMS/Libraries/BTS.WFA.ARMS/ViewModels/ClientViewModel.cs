﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.ViewModels
{
    public class ClientViewModel
    {      
        public string AttributeValue { get; set; }
        public DateTime DateModified { get; set; }
        public ClientAttributeViewModel Attribute { get; set; }
        public ClientHistoryViewModel ClientHistory { get; set; }
    }

    public class ClientAttributeViewModel
    {   
        public string TypeCode { get; set; }  
        public ClientAttributeTypeViewModel Type { get; set; }

    }

    public class ClientAttributeTypeViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string ClientCoreGroupName { get; set; }
        public string DotNetDataType { get; set; }
        public string SourceTable { get; set; }
        public string FilterExpression { get; set; }
        public string SortExpression { get; set; }

    }
    public class ClientHistoryViewModel
    {  
        public DateTime EntryDate { get; set; }
        public string EntryText { get; set; }
    }
}
