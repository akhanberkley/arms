﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class SurveyViewModel : BaseViewModel
    {
        public SurveyViewModel()
        {
            SurveyReasons = new List<string>();
            Documents = new List<SurveyDocumentViewModel>();
            Comments = new List<SurveyCommentViewModel>();
            Policies = new List<PolicyViewModel>();
            Locations = new List<LocationViewModel>();
        }

        public int Number { get; set; }
        public DateTime? DueDate { get; set; }
        public InsuredViewModel Insured { get; set; }
        public ContactViewModel Underwriter { get; set; }

        public List<string> SurveyReasons { get; set; }
        public List<SurveyCommentViewModel> Comments { get; set; }
        public List<SurveyDocumentViewModel> Documents { get; set; }
        public PolicyViewModel PrimaryPolicy { get; set; }
        public List<PolicyViewModel> Policies { get; set; }
        public List<LocationViewModel> Locations { get; set; }

        [IgnoreDataMember]
        public Guid CompanyDbId { get; set; }
        [IgnoreDataMember]
        public Guid SurveyDbId { get; set; }
        [IgnoreDataMember]
        public Guid? InsuredDbId { get; set; }
        [IgnoreDataMember]
        public string UnderwriterCode { get; set; }
    }

    public class SurveyDocumentViewModel
    {
        public string DocumentType { get; set; }
        public string FileName { get; set; }
        public DateTime EntryDate { get; set; }
        public string Number { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicyType { get; set; }
        public string Remarks { get; set; }
    }
}


