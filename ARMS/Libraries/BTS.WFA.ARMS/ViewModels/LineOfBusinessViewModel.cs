﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class LineOfBusinessViewModel : CodeListItemViewModel
    {
        public LineOfBusinessViewModel()
        {
            PolicySymbols = new List<string>();
        }
        public List<string> PolicySymbols { get; set; }

        [IgnoreDataMember]
        public string PolicySymbolsFromDb
        {
            set
            {
                PolicySymbols = value.Split('|').ToList();
            }
        }
    }
}
