﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BTS.WFA.ARMS.ViewModels
{
    public class AssignmentRulePageViewModel
    {

        public List<AssignmentRuleViewModel> AssignmentRule { get; set; }
        public List<AssignmentRuleActionViewModel> AssignmentRuleAction { get; set; }
        public List<SurveyStatusTypeViewModel> SurveyStatusType { get; set; }
        public List<FilterFieldViewModel> FilterField { get; set; }
        public List<FilterOperatorViewModel> FilterOperator { get; set; }
        public AssignmentRuleViewModel NewRule { get; set; }
    }

    public class FilterViewModel
    {
        public Guid FilterId { get; set; }
        public List<FilterConditionViewModel> FilterCondition { get; set; }
    }

    public class FilterConditionViewModel
    {
        public FilterFieldViewModel FilterField { get; set; }
        public FilterOperatorViewModel FilterOperator { get; set; }
        public string CompareValue { get; set; }
        public string DisplayValue { get; set; }
    }

    public class FilterFieldViewModel
    {
        public string Code { get; set; }
        public string SurveyStatusTypeCode { get; set; }
        public string Name { get; set; }
        public string SourceTable { get; set; }
        public string SourceColumn { get; set; }
        public string DisplayValue { get; set; }
        public string ComboEntity { get; set; }
        public string FilterExpression { get; set; }
        public string DotNetDataType { get; set; }
    }
    public class FilterOperatorViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool ForStringsOnly { get; set; }
        public string DisplayName { get; set; }
        public string DisplayShortName { get; set; }
    }


    public class AssignmentRuleViewModel
    {
        public Guid ID { get; set; }
        public int PriorityIndex { get; set; }
        public AssignmentRuleActionViewModel AssignmentRuleAction { get; set; }
        public SurveyStatusTypeViewModel SurveyStatusType { get; set; }
        public FilterViewModel Filter { get; set; }
    }

    public class AssignmentRuleActionViewModel
    {
        public string Code { get; set; }
        public string DisplayName { get; set; }
        public AssignmentRuleActionTypeViewModel Type { get; set; }

    }

    public class SurveyStatusTypeViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }


    public class AssignmentRuleActionTypeViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}

