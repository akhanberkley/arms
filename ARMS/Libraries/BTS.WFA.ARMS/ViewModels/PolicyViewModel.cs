﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class PolicyViewModel
    {
        public PolicyViewModel()
        {
            Coverages = new List<PolicyCoverageViewModel>();
        }

        public CodeListItemViewModel LineOfBusiness { get; set; }
        public string PolicySymbol { get; set; }
        public string Number { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string BranchCode { get; set; }
        public CodeListItemViewModel ProfitCenter { get; set; }
        public string Carrier { get; set; }
        public string PrimaryState { get; set; }
        public decimal? Premium { get; set; }
        public string HazardGrade { get; set; }

        public List<PolicyCoverageViewModel> Coverages { get; set; }

        [IgnoreDataMember]
        public Guid PolicyDbId { get; set; }
    }

    public class PolicyCoverageViewModel
    {
        public PolicyCoverageViewModel()
        {
            Items = new List<PolicyCoverageItemViewModel>();
        }

        public bool Included { get; set; }
        public CodeListItemViewModel Name { get; set; }
        public List<PolicyCoverageItemViewModel> Items { get; set; }

    }
    public class PolicyCoverageItemViewModel
    {
        public CodeListItemViewModel Type { get; set; }
        public string Value { get; set; }
    }
}
