﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.ViewModels
{
    public class InsuredViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string AdditionalName { get; set; }
        public AddressViewModel Address { get; set; }
        public ContactViewModel PrimaryContact { get; set; }

        public AgencyViewModel Agency { get; set; }
        public ContactViewModel Agent { get; set; }
    }
}
