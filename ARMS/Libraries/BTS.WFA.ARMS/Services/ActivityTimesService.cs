﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ViewModels;
using System.Globalization;
using System.Data.Entity.Infrastructure;
using LinqKit;

namespace BTS.WFA.ARMS.Services
{
    public class ActivityTimesService
    {

        public static ActivityTimesPageViewModel GetViewInformation(CompanyDetail company)
        {
            var page = new ActivityTimesPageViewModel();

            using (var db = Application.GetDatabaseInstance())
            {

                //page.Activities = db.Activities.AsExpandable().Select(DataToViewModel.Activity()).ToList();

                //page.Users = db.Users.AsExpandable().Where(u => u.CompanyId == company.CompanyId && u.IsUnderwriter == false && u.IsFeeCompany == false && u.AccountDisabled == false)
                //                                  .Select(DataToViewModel.User()).ToList();

                //page.ActivityTypes = db.ActivityTypes.Where(u => u.CompanyId == company.CompanyId && u.Disabled == false).Select(DataToViewModel.ActivityType()).ToList();
                //page.NewActivityTime = new ActivityViewModel();
            }

            return page;
        }


        public static ActivityViewModel SaveActivityTime(ActivityViewModel activity, CompanyDetail company)
        {

            if (activity.Hours.Value <= 0)
            {
                activity.AddValidation("Hours", "The number of hours or days must be entered.");
            }

            if (activity.ValidationResults.Count == 0)
            {
                using (var db = Application.GetDatabaseInstance())
                {

                    var dbActivityTime = db.Activities.FirstOrDefault(a => a.ActivityId == activity.ID);


                    if (dbActivityTime == null)
                    {
                        dbActivityTime = db.Activities.Add(new Data.Models.Activity() { ActivityId = new Guid() });
                    }

                    //dbActivityTime.AllocatedTo = activity.AllocatedTo.ID;
                    dbActivityTime.ActivityTypeId = activity.ActivityType.ActivityTypeID;                    
                    dbActivityTime.ActivityDate = activity.Date;
                    dbActivityTime.ActivityHours = activity.Hours;
                    dbActivityTime.ActivityDays = activity.Days;
                    dbActivityTime.CallCount = activity.CallCount;
                    dbActivityTime.Comments = activity.Comments;

                    db.SaveChanges();
                }

            }

            return activity;
        }

        public static List<ValidationResult> DeleteActivityTime(Guid ID, CompanyDetail company)
        {
            var validations = new BaseViewModel();
            using (var db = Application.GetDatabaseInstance())
            {
                try
                {
                    var dbAcitivityTime = db.Activities.Where(u => u.ActivityId == ID)
                        .FirstOrDefault();

                    db.Activities.Remove(dbAcitivityTime);

                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex is DbUpdateException)
                    {
                        validations.AddValidation("General", "This Activity time could not be deleted.");
                        Application.HandleException(ex, new { Company = company.Summary, ActivityId = ID });
                    }
                    else
                        throw ex;
                }

            }

            return validations.ValidationResults;
        }

    }
}
