﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ViewModels;
using LinqKit;

namespace BTS.WFA.ARMS.Services
{
    public class UnderwriterService
    {
        public static UnderwriterViewModel SaveUnderwriter(UnderwriterViewModel underwriter, CompanyDetail company)
        {
            if (String.IsNullOrEmpty(underwriter.Name))
                underwriter.AddValidation("Name", "Name is required");
            if (String.IsNullOrEmpty(underwriter.DomainName))
                underwriter.AddValidation("DomainName", "Domain Name is required");
            if (String.IsNullOrEmpty(underwriter.UserName))
                underwriter.AddValidation("UserName", "Username is required");
            if (String.IsNullOrEmpty(underwriter.PhoneNumber))
                underwriter.AddValidation("PhoneNumber", "Phone Number is required");
            if (String.IsNullOrEmpty(underwriter.Title))
                underwriter.AddValidation("Title", "Title is required");
            
            if (underwriter.ValidationResults.Count == 0)
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    var dbUnderwriter = db.Underwriters.FirstOrDefault(c => c.CompanyId == company.CompanyId && c.UnderwriterCode == underwriter.Code);
                    if (dbUnderwriter == null)
                        dbUnderwriter = db.Underwriters.Add(new Data.Models.Underwriter() { CompanyId = company.CompanyId });

                    dbUnderwriter.UnderwriterCode = underwriter.Code;
                    dbUnderwriter.UnderwriterName = underwriter.Name;
                    dbUnderwriter.Username = underwriter.UserName;
                    dbUnderwriter.PhoneNumber = underwriter.PhoneNumber;
                    dbUnderwriter.PhoneExt = underwriter.PhoneExt;
                    dbUnderwriter.EmailAddress = underwriter.EmailAddress;
                    dbUnderwriter.DomainName = underwriter.DomainName;
                    dbUnderwriter.Disabled = underwriter.Disabled;
                    dbUnderwriter.Title = underwriter.Title;                   
                    db.SaveChanges();
                }
            }

            return underwriter;
        }

        public static Underwriter GetUnderwriterByUsername(string username, Guid companyId)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Underwriters.AsExpandable().Where(u => u.Username == username && u.CompanyId == companyId).FirstOrDefault();
            }
        }

        public static Underwriter GetUnderwriterByCode(string code, Guid companyId)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Underwriters.AsExpandable().Where(u => u.UnderwriterCode == code && u.CompanyId == companyId).FirstOrDefault();
            }
        }
    }
}
