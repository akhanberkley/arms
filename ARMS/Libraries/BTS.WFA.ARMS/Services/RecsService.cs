﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Services
{
    public class RecsService
    {
        public static RecsPageViewModel GetRecs(CompanyDetail company)
        {
            RecsPageViewModel page = new RecsPageViewModel();

            using (var db = Application.GetDatabaseInstance())
            {
                page.Recs = db.Recommendations.Where(r => r.CompanyId == company.CompanyId)
                                                  .Select(r => new RecViewModel()
                                                  {
                                                      ID = r.RecommendationId,
                                                      Code = r.Code,
                                                      Title = r.Title,
                                                      Description = r.Description,
                                                      Disabled = r.Disabled
                                                  }).ToList();

                page.NewRec = new RecViewModel();
            }

            return page;
        }

        public static RecViewModel SaveRec(RecViewModel rec, CompanyDetail company)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbRec = db.Recommendations.FirstOrDefault(r => r.RecommendationId == rec.ID && r.CompanyId == company.CompanyId);
                if (dbRec == null)
                {
                    dbRec = db.Recommendations.Add(new Data.Models.Recommendation() { RecommendationId = new Guid(), CompanyId = company.CompanyId });
                }

                dbRec.Description = rec.Description;
                dbRec.Code = rec.Code;
                dbRec.Title = rec.Title;
                dbRec.Disabled = rec.Disabled;
                db.SaveChanges();
            }
            return rec;
        }
    }
}
