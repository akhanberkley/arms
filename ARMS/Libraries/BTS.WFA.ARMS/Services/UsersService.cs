﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity.Infrastructure;
using LinqKit;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.Services
{
    public class UsersService
    {
        public static UserViewModel GetActiveUser(string userName)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Users.Where(u => u.AccountDisabled == false && u.DomainName + "\\" + u.Username == userName)
                              .Select(Mapping.DataToViewModel.User())
                              .FirstOrDefault();
            }
        }

        public static List<ValidationResult> DeleteUser(Guid ID, CompanyDetail company)
        {
            var validations = new BaseViewModel();
            using (var db = Application.GetDatabaseInstance())
            {
                try
                {
                    var dbUser = db.Users.Where(u => u.CompanyId == company.CompanyId && u.UserId == ID)
                        .FirstOrDefault();

                    db.Users.Remove(dbUser);

                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex is DbUpdateException)
                    {
                        validations.AddValidation("General", "This user could not be deleted.");
                        Application.HandleException(ex, new { Company = company.Summary, UserId = ID });
                    }
                    else
                        throw ex;
                }

            }

            return validations.ValidationResults;
        }

        public static UserViewModel SaveUser(UserViewModel user, CompanyDetail company)
        {
            if (user.IsFeeCompany)
            {
                string randomDigits = Guid.NewGuid().ToString().Substring(0, 8).ToUpper();
                user.UserName = string.Format("FC_{0}", randomDigits);
                //user.DomainName = company.PrimaryDomainName;
                //user.RoleID = company.FeeCompanyUserRoleID;
            }


            using (var db = Application.GetDatabaseInstance())
            {

                if (String.IsNullOrEmpty(user.Name))
                    user.AddValidation("Name", "Name is required");
                if (String.IsNullOrEmpty(user.UserName))
                    user.AddValidation("UserName", "Username is required");
                if (String.IsNullOrEmpty(user.WorkPhone))
                    user.AddValidation("WorkPhone", "Work Phone is required");

                if (String.IsNullOrEmpty(user.EmailAddress))
                    user.AddValidation("EmailAddress", "Email Address is required");


                if (user.ValidationResults.Count == 0)
                {

                    var dbUser = db.Users.FirstOrDefault(r => r.DomainName + "\\" + r.Username == user.UserName && r.CompanyId == company.CompanyId);
                    if (dbUser == null)
                    {
                        dbUser = db.Users.Add(new Data.Models.User() { UserId = Guid.NewGuid(), CompanyId = company.CompanyId });
                    }


                    dbUser.Name = user.Name;
                    dbUser.CompanyId = company.CompanyId;
                    //dbUser.Username = user.UserName;
                    //dbUser.DomainName = user.DomainName;
                    //dbUser.Title = user.Title;
                    //dbUser.Certifications = user.Certifications;
                    dbUser.EmailAddress = user.EmailAddress;
                    dbUser.WorkPhone = user.WorkPhone;
                    dbUser.HomePhone = user.HomePhone;
                    dbUser.MobilePhone = user.MobilePhone;
                    dbUser.FaxNumber = user.FaxNumber;
                    dbUser.StreetLine1 = user.Address.Address1;
                    dbUser.StreetLine2 = user.Address.Address2;
                    dbUser.City = user.Address.City;
                    dbUser.StateCode = user.Address.State;
                    dbUser.ZipCode = user.Address.PostalCode;
                    dbUser.AccountDisabled = user.AccountDisabled;
                    //dbUser.ManagerId = user.ManagerID;
                    //dbUser.UserRoleId = user.RoleID;
                    dbUser.TimeZoneCode = "US/Eastern";

                    db.SaveChanges();

                    //user = db.Users.AsExpandable()
                    //                       .Where(u => u.UserId == user.ID)
                    //                       .Select(DataMappers.User())
                    //                       .FirstOrDefault();
                }

            }

            return user;
        }
    }
}
