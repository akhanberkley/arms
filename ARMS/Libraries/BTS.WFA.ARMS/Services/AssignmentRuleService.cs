﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ViewModels;
using System.Data.Entity.Infrastructure;
using LinqKit;

namespace BTS.WFA.ARMS.Services
{
    public class AssignmentRuleService
    {
        public static List<CodeListItemViewModel> GetFieldValues(string fieldcode, CompanyDetail company)
        {
            var FieldValues = new List<CodeListItemViewModel>();
            var FilterField = new FilterFieldViewModel();

            using (var db = Application.GetDatabaseInstance())
            {
                FilterField = db.FilterFields.Where(f => f.FilterFieldCode == fieldcode).Select(Mapping.DataToViewModel.FilterField()).FirstOrDefault();

                switch (FilterField.ComboEntity)
                {
                    case "User":
                        FieldValues = db.Users.Where(u => u.AccountDisabled == false && u.IsUnderwriter == false && u.CompanyId == company.CompanyId)
                            .Select(i => new CodeListItemViewModel()
                            {
                                Code = i.Username,
                                Description = i.Name
                            }).ToList();
                        break;
                    case "State":
                        FieldValues = db.States
                           .Select(i => new CodeListItemViewModel()
                           {
                               Code = i.StateCode,
                               Description = i.StateName
                           }).ToList();
                        break;
                    case "SIC":
                        FieldValues = db.Sics
                           .Select(i => new CodeListItemViewModel()
                           {
                               Code = i.SicCode,
                               Description = i.SicCode
                           }).ToList();
                        break;
                    case "LineOfBusiness":
                        FieldValues = db.LineOfBusinesses
                          .Select(i => new CodeListItemViewModel()
                          {
                              Code = i.LineOfBusinessCode,
                              Description = i.LineOfBusinessName
                          }).ToList();
                        break;
                        
                }




            }
            return FieldValues;

        }

        public static AssignmentRuleViewModel SaveAssignmentRule(AssignmentRuleViewModel assignmentRule, CompanyDetail company)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbAssignmentRule = db.AssignmentRules.FirstOrDefault(t => t.AssignmentRuleId == assignmentRule.ID && t.CompanyId == company.CompanyId);
                if (dbAssignmentRule == null)
                {
                    dbAssignmentRule = db.AssignmentRules.Add(new Data.Models.AssignmentRule() { AssignmentRuleId = new Guid(), CompanyId = company.CompanyId });
                }
                dbAssignmentRule.PriorityIndex = assignmentRule.PriorityIndex;
                dbAssignmentRule.CompanyId = company.CompanyId;
                dbAssignmentRule.AssignmentRuleActionCode = assignmentRule.AssignmentRuleAction.Code;
                dbAssignmentRule.Filter.FilterId = assignmentRule.Filter.FilterId;
                db.SaveChanges();
            }
            return assignmentRule;
        }

        public static List<ValidationResult> DeleteAssignmentRule(Guid ID, CompanyDetail company)
        {
            var validations = new BaseViewModel();
            using (var db = Application.GetDatabaseInstance())
            {
                try
                {
                    var dbAssignmentRule = db.AssignmentRules.Where(u => u.CompanyId == company.CompanyId && u.AssignmentRuleId == ID)
                        .FirstOrDefault();

                    db.AssignmentRules.Remove(dbAssignmentRule);

                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex is DbUpdateException)
                    {
                        validations.AddValidation("General", "This AssignmentRule could not be deleted.");
                        Application.HandleException(ex, new { Company = company.Summary, AssignmentRuleId = ID });
                    }
                    else
                        throw ex;
                }

            }

            return validations.ValidationResults;
        }
    }

}
