﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Services
{
    public class FeeCompaniesService
    {
        public static List<UserViewModel> GetFeeCompanies(CompanyViewModel company)
        {
            List<UserViewModel> feecompanies = new List<UserViewModel>();

            using (var db = Application.GetDatabaseInstance())
            {
                feecompanies = db.Users.Where(u => u.CompanyId == company.Id && u.IsFeeCompany == true )
                                                  .Select(u => new UserViewModel()
                                                  {
                                                      ID = u.UserId,
                                                      Name = u.Name,
                                                      City =u.City,
                                                      StateCode = u.StateCode,                                                      
                                                      WorkPhone = u.WorkPhone,                                                      
                                                      EmailAddress = u.EmailAddress,   
                                                      AccountDisabled = u.AccountDisabled
                                                  }).ToList();


            }



            return feecompanies;
        }

        public static UserViewModel SaveFeeCompany(UserViewModel feecompany, CompanyViewModel company)
        {
            using (var db = Application.GetDatabaseInstance())
            {

                if (String.IsNullOrEmpty(feecompany.Name))
                    feecompany.AddValidation("Name", "Name is required");
              
                if (String.IsNullOrEmpty(feecompany.WorkPhone))
                    feecompany.AddValidation("WorkPhone", "Work Phone is required");

                if (String.IsNullOrEmpty(feecompany.EmailAddress))
                    feecompany.AddValidation("EmailAddress", "Email Address is required");


                if (feecompany.ValidationResults.Count == 0)
                {

                    var dbFeeCompany = db.Users.FirstOrDefault(r => r.UserId == feecompany.ID && r.CompanyId == company.Id);
                    if (dbFeeCompany == null)
                    {
                        dbFeeCompany = db.Users.Add(new Data.Models.User() { UserId = new Guid(), CompanyId = company.Id });
                    }


                    List<ProfitCenterViewModel> profitcenters = new List<ProfitCenterViewModel>();
                    profitcenters = db.ProfitCenters.Where(pc => pc.CompanyId == company.Id)
                                                     .Select(pc => new ProfitCenterViewModel()
                                                     {
                                                         Code = pc.ProfitCenterCode,
                                                         Name = pc.ProfitCenterCode
                                                     }).ToList();

                    List<UserViewModel> managers = new List<UserViewModel>();
                    managers = db.Users.Where(u => u.CompanyId == company.Id && u.AccountDisabled == false && u.IsFeeCompany == false && u.IsUnderwriter == false)
                                                   .Select(u => new UserViewModel()
                                                   {
                                                       ID = u.UserId,
                                                       Name = u.Name
                                                   }).ToList();

                    List<UserRoleViewModel> userroles = new List<UserRoleViewModel>();
                    userroles = db.UserRoles.Where(u => u.CompanyId == company.Id)
                                                   .Select(u => new UserRoleViewModel()
                                                   {
                                                       ID = u.UserRoleId,
                                                       Name = u.Name,
                                                       Description = u.Description                                                    
                                                   }).ToList();



                    dbFeeCompany.Name = feecompany.Name;
                    dbFeeCompany.DomainName = feecompany.DomainName;
                    dbFeeCompany.Title = feecompany.Title;
                    dbFeeCompany.Certifications = feecompany.Certifications;
                    dbFeeCompany.EmailAddress = feecompany.EmailAddress;
                    dbFeeCompany.WorkPhone = feecompany.WorkPhone;
                    dbFeeCompany.HomePhone = feecompany.HomePhone;
                    dbFeeCompany.MobilePhone = feecompany.MobilePhone;
                    dbFeeCompany.FaxNumber = feecompany.FaxNumber; 
                    dbFeeCompany.StreetLine1 = feecompany.StreetLine1;
                    dbFeeCompany.StreetLine2 = feecompany.StreetLine2;
                    dbFeeCompany.City = feecompany.City;
                    dbFeeCompany.StateCode = feecompany.StateCode;
                    dbFeeCompany.ZipCode = feecompany.ZipCode;
                    dbFeeCompany.AccountDisabled = feecompany.AccountDisabled;                 
                    db.SaveChanges();
                }

            }

            return feecompany;
        }
    }
}
