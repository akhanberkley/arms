﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;
using BTS.WFA.ARMS.ViewModels;
using LinqKit;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.Services
{
    public class CompanyService
    {
        public static Dictionary<string, CompanyDetail> GetCompanyKeyMap()
        {
            var map = new Dictionary<string, CompanyDetail>();
            using (var db = Application.GetDatabaseInstance())
            {
                //This will be where we do initial company settings setup
                var cks = db.Companies.Select(Mapping.DataToViewModel.Company());

                foreach (var ck in cks)
                    map.Add(ck.Summary.LongAbbreviation, ck);

                PopulateUserRoles(db, map);

                //PopulateLinesOfBusiness(db, map);
                var lobs = db.CompanyLineOfBusinesses.GroupBy(c => c.Company)
                                                    .Select(g => new {
                                                        CompanyKey = g.Key.CompanyLongAbbreviation,
                                                        LineOfBusinesses = g.OrderBy(c => c.DisplayOrder)
                                                                        .Select(c => new LineOfBusinessViewModel()
                                                                                {
                                                                                    Code = c.LineOfBusiness.LineOfBusinessCode,
                                                                                    Description = c.LineOfBusiness.LineOfBusinessName,
                                                                                    PolicySymbolsFromDb = c.PolicySymbols
                                                                                })
                                                    }).ToList();

                foreach (var l in lobs)
                {
                    map[l.CompanyKey].LinesOfBusiness = l.LineOfBusinesses.ToList();
                }
                
                //PopulateCoverages(db, map);
                //var coverages = db.
            }

            return map;
        }

        public static void PopulateUserRoles(Data.IArmsDb db, Dictionary<string, CompanyDetail> map)
        {
            //Roles
            var userRoleMapper = Mapping.DataToViewModel.UserRole();
            var userRoles = db.UserRoles.AsExpandable().Select(ur => new { CompanyKey = ur.Company.CompanyLongAbbreviation, Role = userRoleMapper.Invoke(ur) }).ToList();

            var surveyActions = db.SurveyActions.GroupBy(sa => new { sa.Company.CompanyLongAbbreviation, sa.SurveyStatus })
                                                .OrderBy(g => g.Key.SurveyStatus.DisplayOrder)
                                                .Select(g => new
                                                {
                                                    CompanyKey = g.Key.CompanyLongAbbreviation,
                                                    Status = new CodeListItemViewModel { Code = g.Key.SurveyStatus.SurveyStatusCode, Description = g.Key.SurveyStatus.SurveyStatusName },
                                                    Actions = g.OrderBy(sa => sa.DisplayOrder)
                                                                .Select(sa => new
                                                                {
                                                                    Action = new CodeListItemViewModel { Code = sa.SurveyActionType.SurveyActionTypeCode.Trim(), Description = sa.SurveyActionType.ActionName },
                                                                    Permissions = sa.SurveyActionPermissions.Select(sap => new
                                                                    {
                                                                        PermissionSetId = sap.PermissionSetId,
                                                                        Code = sap.ServiceTypeCode ?? "R",
                                                                        Setting = sap.Setting
                                                                    }).ToList()
                                                                }).ToList()
                                                }).ToList();

            var servicePlanActions = db.ServicePlanActions.GroupBy(sa => new { sa.Company.CompanyLongAbbreviation, sa.ServicePlanStatus })
                                                          .OrderBy(g => g.Key.ServicePlanStatus.DisplayOrder)
                                                          .Select(g => new
                                                          {
                                                              CompanyKey = g.Key.CompanyLongAbbreviation,
                                                              Status = new CodeListItemViewModel { Code = g.Key.ServicePlanStatus.ServicePlanStatusCode.Trim(), Description = g.Key.ServicePlanStatus.ServicePlanStatusName },
                                                              Actions = g.OrderBy(spa => spa.DisplayOrder)
                                                                         .Select(spa => new
                                                                         {
                                                                             Action = new CodeListItemViewModel { Code = spa.ServicePlanActionType.ServicePlanActionTypeCode.Trim(), Description = spa.ServicePlanActionType.ActionName },
                                                                             Permissions = spa.ServicePlanActionPermissions.Select(sap => new
                                                                             {
                                                                                 PermissionSetId = sap.PermissionSetId,
                                                                                 Setting = sap.Setting
                                                                             }).ToList()
                                                                         }).ToList()
                                                          }).ToList();

            var noServiceSurveyStatuses = new[] { Domains.SurveyStatusCode.UnassignedSurvey, Domains.SurveyStatusCode.OnHoldSurvey, Domains.SurveyStatusCode.CanceledSurvey };
            foreach (var ur in userRoles)
            {
                //TODO Fix DB data (duplicate roles) instead of this work around
                if (map[ur.CompanyKey].UserRoles.ContainsKey(ur.Role.Name))
                    continue;

                map[ur.CompanyKey].UserRoles.Add(ur.Role.Name, ur.Role);

                foreach (var sa in surveyActions.Where(sa => sa.CompanyKey == ur.CompanyKey))
                {
                    var ssp = new SurveyStatusPermissions() { Status = sa.Status };
                    foreach (var a in sa.Actions)
                    {
                        var ap = new SurveyActionPermissionViewModel() { Action = a.Action };
                        foreach (var permission in a.Permissions.Where(p => p.PermissionSetId == ur.Role.PermissionSetId))
                        {
                            if (permission.Code == "R")
                                ap.Survey = new PermissionViewModel(permission.Setting);
                            else if (!noServiceSurveyStatuses.Contains(sa.Status.Code))
                                ap.ServiceVisit = new PermissionViewModel(permission.Setting);
                        }
                        ap.Survey = ap.Survey ?? new PermissionViewModel(0);
                        if (!noServiceSurveyStatuses.Contains(sa.Status.Code) && ap.ServiceVisit == null)
                            ap.ServiceVisit = new PermissionViewModel(0);

                        ssp.Actions.Add(ap);
                    }

                    ur.Role.SurveyStatusPermissions.Add(ssp);
                }

                foreach (var spa in servicePlanActions.Where(spa => spa.CompanyKey == ur.CompanyKey))
                {
                    var spsp = new ServicePlanStatusPermissions() { Status = spa.Status };
                    foreach (var a in spa.Actions)
                    {
                        var ap = new ServicePlanActionPermissionViewModel() { Action = a.Action };
                        foreach (var permission in a.Permissions.Where(p => p.PermissionSetId == ur.Role.PermissionSetId))
                            ap.Permission = new PermissionViewModel(permission.Setting);

                        ap.Permission = ap.Permission ?? new PermissionViewModel(0);
                        spsp.Actions.Add(ap);
                    }

                    ur.Role.ServicePlanStatusPermissions.Add(spsp);
                }
            }
        }

        public static void PopulateLinesOfBusiness(Data.IArmsDb db, Dictionary<string, CompanyDetail> map)
        {
            // no-op
        }

        public static Company GetCompanyById(Guid companyId)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Companies.AsExpandable().Where(c => c.CompanyId == companyId).FirstOrDefault();
            }
        }
    }
}