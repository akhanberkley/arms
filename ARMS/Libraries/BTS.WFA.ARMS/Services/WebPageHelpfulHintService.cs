﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.Services
{
    public class WebPageHelpfulHintService
    {
        public static List<WebPageHelpfulHintViewModel> GetHelpfulHint(CompanyDetail company)
        {
            List<WebPageHelpfulHintViewModel> hints = new List<WebPageHelpfulHintViewModel>();

            using (var db = Application.GetDatabaseInstance())
            {
                hints = db.WebPageHelpfulHints.Where(h => h.CompanyId == company.CompanyId)
                                                  .Select(h => new WebPageHelpfulHintViewModel() { 
                                                      WebPagePath = h.WebPagePath, 
                                                      WebPageDescription = h.WebPageDescription,
                                                      HelpfulHintText = h.HelpfulHintText,
                                                      AlwaysVisible = h.AlwaysVisible
                                                  }).ToList();
            }

            return hints;
        }

        public static void UpdateWebPageHelpfulHint(WebPageHelpfulHintViewModel webPageHelpfulHint, CompanyDetail company)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbHint = db.WebPageHelpfulHints.FirstOrDefault(u => u.WebPagePath == webPageHelpfulHint.WebPagePath && u.CompanyId == company.CompanyId);
                dbHint.HelpfulHintText = webPageHelpfulHint.HelpfulHintText;
                dbHint.AlwaysVisible = webPageHelpfulHint.AlwaysVisible;

                db.SaveChanges();               
            }
        }
    }
}
