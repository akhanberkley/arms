﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqKit;
using BTS.WFA.ViewModels;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.Services
{
    public class SurveyService
    {
        public static SurveyViewModel Save(CompanyDetail cd, SurveyViewModel survey)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                try
                {
                    // Validations

                    if (survey.ValidationResults.Count == 0)
                    {
                        var dbSurvey = db.Surveys.FirstOrDefault(s => s.CompanyId == cd.CompanyId && s.SurveyNumber == survey.Number);
                        var isNew = (dbSurvey == null);
                        if (isNew)
                        {
                            // Create a new survey record
                            dbSurvey = db.Surveys.Add(new Data.Models.Survey());
                            dbSurvey.SurveyId = Guid.NewGuid();
                            dbSurvey.CompanyId = cd.CompanyId;
                            dbSurvey.SurveyTypeId = new Guid("E5D57E1E-D49E-4B4D-811C-54D25E7474FC"); // HACK: 063015 - AIC's Service survey type id, needs to be replaced
                            dbSurvey.ServiceTypeCode = "R"; // R = Survey
                            dbSurvey.SurveyStatusCode = "IP"; // IP = In Progress
                            dbSurvey.CreateStateCode = "I"; // I = In Progress
                            dbSurvey.CreateDate = DateTime.Now;
                        }
                        else
                        {
                            // Update an existing request
                            //dbSurvey...
                        }

                        if (dbSurvey == null)
                        {
                            survey.AddValidation("Survey", "Survey {0} could not be found", survey.Number);
                        }
                        else
                        {
                            //Save request and send a fresh copy from the database
                            if (survey.ValidationResults.Count == 0)
                            {
                                dbSurvey.LastModifiedOn = DateTime.Now;
                                
                                db.SaveChanges();

                                survey = SurveyQueryService.Get(cd, dbSurvey.SurveyNumber);
                                survey.ItemCreated = isNew;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return survey;
        }


























        //public static SurveyIdViewModel Save(CompanyViewModel company, SurveyViewModel survey)
        //{
        //    SurveyIdViewModel result = survey;

        //    //if (!options.IgnoreValidations)
        //        Validate(company, survey); 
            
        //    if (survey.ValidationResults.Count == 0)
        //    {
        //        using (var db = Application.GetDatabaseInstance())
        //        {
        //            //using (var trans = db.GetTransaction())
        //            //{
        //                try
        //                {
        //                    Data.Models.Survey dbSurvey = null;
        //                    var now = DateTime.Now;
        //                    var isNew = (survey.Number == default(int));
        //                    if (isNew)
        //                    {
        //                        dbSurvey = db.Surveys.Add(new Data.Models.Survey());
        //                        dbSurvey.CompanyId = company.CompanyId;
        //                        dbSurvey.CreateDate = now;
        //                        dbSurvey.CreateStateCode = "I"; //In Progress
        //                        dbSurvey.SurveyStatusCode = "SU"; //Survey Unassigned
        //                        dbSurvey.ServiceTypeCode = "R";
        //                        dbSurvey.InHouseSelection = false;
                                
        //                        db.SaveChanges();
        //                    }
        //                    else
        //                    {
        //                        // Query for the existing survey
        //                        var surveyQuery = db.Surveys//.Include
        //                                                .Where(s => s.CompanyId == company.CompanyId&& s.SurveyId == survey.ID);
        //                        dbSurvey = surveyQuery.FirstOrDefault();
        //                    }

        //                    if (dbSurvey == null)
        //                        survey.AddValidation("Survey", "Survey {0} could not be found", survey.ID);
        //                    else
        //                    {
        //                        // Update the existing survey

        //                        // for managing multiple actions during this update...
        //                        /*
        //                        List<Action> actions = new List<Action>();
        //                        //These steps either don't use the database, or all need the same connection/transaction
        //                        actions.Add(() =>
        //                        {
        //                        }
        //                        Thread safe steps
        //                        action.Add ...

        //                        //Threaded work
        //                        foreach (var a in actions)
        //                            threadedTasks.Add(Task.Factory.StartNew(a));

        //                        //Nonthreaded (for debugging/performance tuning)
        //                        //foreach (var a in actions)
        //                        //    a();

        //                        //Wait for everything to finish before committing
        //                        Task.WaitAll(threadedTasks.ToArray());
        //                        */

        //                        if (survey.ValidationResults.Count == 0)
        //                        {
        //                            //dbSurvey.LastModifiedBy = updateUser;
        //                            dbSurvey.LastModifiedOn = now;

        //                            db.SaveChanges();
        //                            //trans.Commit();

        //                            /*
        //                            var savedNumber = new SubmissionNumberViewModel() { Number = dbSubmission.SubmissionNumber, Sequence = dbSubmission.Sequence };
        //                            if (options.ReturnFullSubmission)
        //                                result = SubmissionQueryService.GetByNumber(cn, savedNumber, null);
        //                            else
        //                            */
        //                            result = new SurveyIdViewModel() { ID = dbSurvey.SurveyId, Number = dbSurvey.SurveyNumber };
                                    
        //                            result.ItemCreated = isNew;
        //                        }
        //                        //else
        //                            //trans.Rollback();
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    //trans.Rollback();
        //                    throw ex;
        //                }
        //            //}
        //        }
        //    }

        //    return result;
        //}

        public static void Validate(CompanyDetail company, SurveyViewModel survey)
        {
            //SubmissionValidator.Validate(cn, submissionNumber, submission, generatePolicyNumber);
        }

    }
}
