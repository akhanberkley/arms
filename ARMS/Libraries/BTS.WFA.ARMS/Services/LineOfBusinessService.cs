﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;
using BTS.WFA.ARMS.ViewModels;
using LinqKit;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.Services
{
    public class LineOfBusinessService
    {
        public static LineOfBusiness GetLobByCode(string code)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.LineOfBusinesses.AsExpandable().Where(c => c.LineOfBusinessCode == code).FirstOrDefault();
            }
        }
    }
}
