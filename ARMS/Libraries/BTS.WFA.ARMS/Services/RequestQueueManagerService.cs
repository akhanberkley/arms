﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using BTS.WFA.ARMS.ViewModels;
using LinqKit;

namespace BTS.WFA.ARMS.Services
{
    public class RequestQueueManagerService
    {
        public static List<RequestViewModel> Search(CompanyDetail cd, Func<IQueryable, IQueryable> odataFilter)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.RequestQueue.Where(a => a.CompanyId == cd.CompanyId);
                query = odataFilter(query) as IQueryable<Data.Models.RequestQueue>;
                var requests = query.AsExpandable().Select(Mapping.DataToViewModel.Request()).ToList();
                
                var surveyIds = requests.Select(r => r.SurveyDbId).ToArray();
                var surveys = SurveyQueryService.GetByQuery(db.Surveys.Where(s => surveyIds.Contains(s.SurveyId)), null).ToDictionary(s => s.SurveyDbId, s => s);
                foreach (var r in requests)
                    r.Survey = surveys[r.SurveyDbId.Value];

                return requests;
            }
        }

        public static RequestViewModel CreateRequest(CompanyDetail cd, string typeCode, string value)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbSurvey = db.Surveys.Add(new Data.Models.Survey());
                dbSurvey.SurveyId = Guid.NewGuid();
                dbSurvey.CompanyId = cd.CompanyId;
                dbSurvey.SurveyTypeId = new Guid("E5D57E1E-D49E-4B4D-811C-54D25E7474FC"); // HACK: 063015 - AIC's Service survey type id, needs to be replaced
                dbSurvey.ServiceTypeCode = "R"; // R = Survey
                dbSurvey.SurveyStatusCode = "IP"; // IP = In Progress
                dbSurvey.CreateStateCode = "I"; // I = In Progress
                
                var dbRequest = db.RequestQueue.Add(new Data.Models.RequestQueue());
                dbRequest.CompanyId = cd.CompanyId;
                dbRequest.InputTypeCode = typeCode;
                dbRequest.InputValue = value;
                dbRequest.StatusCode = "LD";

                var date = DateTime.Now;
                dbSurvey.LastModifiedOn = dbSurvey.CreateDate = date;
                dbRequest.LastModifiedDate = dbRequest.CreateDate = date;

                dbRequest.Survey = dbSurvey;
                db.SaveChanges();

                return Mapping.DataToViewModel.Request().Invoke(dbRequest);
            }
        }
    }
}
