﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;
using LinqKit;

namespace BTS.WFA.ARMS.Services
{
    public class UserRolesService
    {
        public static List<UserRoleViewModel> GetAll(CompanyDetail company)
        {
            return company.UserRoles.Values.ToList();
        }

        public static UserRoleViewModel SaveUserRole(UserRoleViewModel userrole, CompanyDetail company)
        {
            //if (String.IsNullOrEmpty(userrole.Name))
            //    userrole.AddValidation("Name", "Name is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanViewCompanySummary.ToString()))
            //    userrole.AddValidation("CanViewCompanySummary", "Can View Company Summary is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanSearchSurveys.ToString()))
            //    userrole.AddValidation("CanSearchSurveys", "Can Search Surveys is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanCreateSurveys.ToString()))
            //    userrole.AddValidation("CanCreateSurveys", "Can Create Surveys is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanViewSurveyReviewQueues.ToString()))
            //    userrole.AddValidation("CanViewSurveyReviewQueues", "Can View Survey Review is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanViewLinks.ToString()))
            //    userrole.AddValidation("CanViewLinks", "Can View Links is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanViewSurveyHistory.ToString()))
            //    userrole.AddValidation("CanViewSurveyHistory", "Can View Survey History is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanRequireSavedSearch.ToString()))
            //    userrole.AddValidation("CanRequireSavedSearch", "Can Require Saved Search is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminUserRoles.ToString()))
            //    userrole.AddValidation("CanAdminUserRoles", "Can Admin User Roles is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminUserAccounts.ToString()))
            //    userrole.AddValidation("CanAdminUserAccounts", "Can Admin User Accounts is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminFeeCompanies.ToString()))
            //    userrole.AddValidation("CanAdminFeeCompanies", "Can Admin Fee Companies is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminTerritories.ToString()))
            //    userrole.AddValidation("CanAdminTerritories", "Can Admin Territories is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminRules.ToString()))
            //    userrole.AddValidation("CanAdminRules", "Can Admin Rules is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminLetters.ToString()))
            //    userrole.AddValidation("CanAdminLetters", "Can Admin Letters is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminReports.ToString()))
            //    userrole.AddValidation("CanAdminReports", "Can Admin Reports is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminServicePlanFields.ToString()))
            //    userrole.AddValidation("CanAdminServicePlanFields", "Can Admin Service Plan Templates is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminResultsDisplay.ToString()))
            //    userrole.AddValidation("CanAdminResultsDisplay", "Can Admin Results Display is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminRecommendations.ToString()))
            //    userrole.AddValidation("CanAdminRecommendations", "Can Admin Recommendations is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminLinks.ToString()))
            //    userrole.AddValidation("CanAdminLinks", "Can Admin Links is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminUnderwriters.ToString()))
            //    userrole.AddValidation("CanAdminUnderwriters", "Can Admin Underwriters is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminClients.ToString()))
            //    userrole.AddValidation("CanAdminClients", "Can Admin Clients is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminActivityTimes.ToString()))
            //    userrole.AddValidation("CanAdminActivityTimes", "Can Admin Activity Times is required");
            //if (String.IsNullOrEmpty(userrole.PermissionSet.CanAdminHelpfulHints.ToString()))
            //    userrole.AddValidation("CanAdminHelpfulHints", "Can Admin Helpful Hints is required");



            if (userrole.ValidationResults.Count == 0)
            {


                using (var db = Application.GetDatabaseInstance())
                {
                    var dbUserRole = db.UserRoles.FirstOrDefault(t => t.CompanyId == company.CompanyId);
                    if (dbUserRole == null)
                    {
                        dbUserRole = db.UserRoles.Add(new Data.Models.UserRole() { UserRoleId = new Guid(), CompanyId = company.CompanyId });
                    }

                    
                    dbUserRole.Name = userrole.Name;
                    dbUserRole.Description = userrole.Description;                             
                    dbUserRole.PermissionSet.CanAdminActivityTimes = userrole.GeneralPermissions.AdminActivityTimes;
                    //dbUserRole.PermissionSet.CanWorkSurveys = userrole.GeneralPermissions.WorkSurveys;
                    //dbUserRole.PermissionSet.CanWorkReviews = userrole.GeneralPermissions.WorkReviews;
                    //dbUserRole.PermissionSet.CanWorkServicePlans = userrole.GeneralPermissions.WorkServicePlans;
                    dbUserRole.PermissionSet.CanViewCompanySummary = userrole.GeneralPermissions.ViewCompanySummary;
                    dbUserRole.PermissionSet.CanViewSurveyReviewQueues = userrole.GeneralPermissions.ViewSurveyReviewQueues;
                    dbUserRole.PermissionSet.CanRequireSavedSearch = userrole.GeneralPermissions.RequireSavedSearch;
                    dbUserRole.PermissionSet.CanSearchSurveys = userrole.GeneralPermissions.SearchSurveys;
                    dbUserRole.PermissionSet.CanCreateSurveys = userrole.GeneralPermissions.CreateSurveys;
                    dbUserRole.PermissionSet.CanCreateServicePlans = userrole.GeneralPermissions.CreateServicePlans;
                    dbUserRole.PermissionSet.CanViewLinks = userrole.GeneralPermissions.ViewLinks;
                    dbUserRole.PermissionSet.CanViewSurveyHistory = userrole.GeneralPermissions.ViewSurveyHistory;
                    dbUserRole.PermissionSet.CanAdminUserRoles = userrole.GeneralPermissions.AdminUserRoles;
                    dbUserRole.PermissionSet.CanAdminUserAccounts = userrole.GeneralPermissions.AdminUserAccounts;
                    dbUserRole.PermissionSet.CanAdminFeeCompanies = userrole.GeneralPermissions.AdminFeeCompanies;
                    dbUserRole.PermissionSet.CanAdminTerritories = userrole.GeneralPermissions.AdminTerritories;
                    dbUserRole.PermissionSet.CanAdminRules = userrole.GeneralPermissions.AdminRules;
                    dbUserRole.PermissionSet.CanAdminLetters = userrole.GeneralPermissions.AdminLetters;
                    dbUserRole.PermissionSet.CanAdminReports = userrole.GeneralPermissions.AdminReports;
                    dbUserRole.PermissionSet.CanAdminResultsDisplay = userrole.GeneralPermissions.AdminResultsDisplay;
                    dbUserRole.PermissionSet.CanAdminRecommendations = userrole.GeneralPermissions.AdminRecommendations;
                    //dbUserRole.PermissionSet.CanAdminServicePlanFields = userrole.GeneralPermissions.AdminServicePlanFields;
                    dbUserRole.PermissionSet.CanAdminLinks = userrole.GeneralPermissions.AdminLinks;
                    dbUserRole.PermissionSet.CanAdminUnderwriters = userrole.GeneralPermissions.AdminUnderwriters;
                    dbUserRole.PermissionSet.CanAdminClients = userrole.GeneralPermissions.AdminClients;
                    dbUserRole.PermissionSet.CanAdminActivityTimes = userrole.GeneralPermissions.AdminActivityTimes;
                    dbUserRole.PermissionSet.CanAdminHelpfulHints = userrole.GeneralPermissions.AdminHelpfulHints;
                    //dbUserRole.PermissionSet.CanAdminDocuments = userrole.GeneralPermissions.AdminDocuments;
                    db.SaveChanges();
                }              
            }
            return userrole;
        }
    }
}
