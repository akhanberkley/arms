﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Data.Entity;
using LinqKit;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ViewModels;
//using Int = BTS.WFA.Integration;
//using BTS.WFA.BCS.Mapping;

namespace BTS.WFA.ARMS.Services
{
    public class DomainService
    {
        public static List<ImpersonatableCompanyViewModel> ImpersonatableCompanies()
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Companies.OrderBy(c => c.CompanyName)
                                   .Select(c => new ImpersonatableCompanyViewModel
                                                {
                                                    CompanyId = c.CompanyId,
                                                    Users = c.Users.Where(u => !u.AccountDisabled && !u.IsFeeCompany)
                                                                   .OrderBy(u => u.Name)
                                                                   .Select(u => new CodeListItemViewModel { Code = u.DomainName + "\\" + u.Username, Description = u.Name })
                                                                   .ToList()
                                                })
                                   .ToList();
            }
        }

        public static List<StateViewModel> FullStateList()
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.States.OrderBy(s => s.StateCode)
                               .Select(state => new StateViewModel
                               {
                                   Abbreviation = state.StateCode,
                                   StateName = state.StateName
                               })
                               .ToList();
            }
        }

        public static List<string> SurveyReasons(CompanyDetail company)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.SurveyReasonTypes.Where(sr => sr.CompanyId == company.CompanyId)
                                           .OrderBy(sr => sr.DisplayOrder)
                                           .Select(sr => sr.SurveyReasonTypeName)
                                           .ToList();
            }
        }

        public static List<ContactViewModel> Underwriters(CompanyDetail company)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Underwriters.Where(u => u.CompanyId == company.CompanyId && !u.Disabled)
                                           .Select(u => new ContactViewModel { Name = u.UnderwriterName, Phone = u.PhoneNumber, Email = u.EmailAddress })
                                           .Union(db.Insureds.Where(i => i.CompanyId == company.CompanyId && i.Underwriter != null && !db.Underwriters.Any(u => u.CompanyId == company.CompanyId && u.UnderwriterCode == i.Underwriter))
                                                             .Select(i => new ContactViewModel { Name = i.Underwriter, Phone = "", Email = "" }))
                                           .Distinct()
                                           .OrderBy(u => u.Name)
                                           .ToList();
            }
        }

        public static List<LineOfBusinessViewModel> LinesOfBusiness(CompanyDetail company)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.CompanyLineOfBusinesses.Where(c => c.CompanyId == company.CompanyId)
                                                 .OrderBy(c => c.LineOfBusiness.DisplayOrder)
                                                 .Select(c => new LineOfBusinessViewModel()
                                                 {
                                                     Code = c.LineOfBusiness.LineOfBusinessCode,
                                                     Description = c.LineOfBusiness.LineOfBusinessName,
                                                     PolicySymbolsFromDb = c.PolicySymbols
                                                 }).ToList();
            }
        }

        public static List<CodeListItemViewModel> ProfitCenters(CompanyDetail company)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.ProfitCenters.Where(pc => pc.CompanyId == company.CompanyId)
                                       .OrderBy(pc => pc.ProfitCenterCode)
                                       .Select(pc => new CodeListItemViewModel() { Code = pc.ProfitCenterCode, Description = pc.ProfitCenterName })
                                       .ToList();
            }
        }
    }
}
