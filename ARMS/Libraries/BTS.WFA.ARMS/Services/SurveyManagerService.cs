﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Data.Entity;
//using System.Runtime.Caching;
//using System.Text.RegularExpressions;
//using LinqKit;
//using BTS.WFA.ViewModels;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data.Models;
//using BTS.WFA.BCS.Validation;
//using Int = BTS.WFA.Integration;

namespace BTS.WFA.ARMS.Services
{
    // Formerlly the SurveyManager base class in ARMS 2013
    // to be invoked by the subclass for the specific company, making this abstract and then having a AICSurveyService.cs for example
    public abstract class SurveyManagerService
    {
        //private const long DatabaseTimeResolution = 5 * TimeSpan.TicksPerMillisecond;
        //private static DateTime _lastHistoryEntryTime = DateTime.MinValue; // used to prevent dup time entries in history
        //protected static Random _randomNumbers = new Random((int)DateTime.Now.Ticks); // used in user/territory selection

        private CompanyDetail _company;
        private SurveyViewModel _survey;
        //private User _currentUser;
        //private bool _surveyOwnedByUser;
        //private bool _surveyOwnedByVendor;
        private bool _overridePermissions;


        // GetInstance(survey, user, overridePermissions)
        // GetInstance(survey, user)

        /// <summary>
        /// Instantiates a company-specific SurveyManagerService instance that can be used to preform
        /// survey management functions on behalf of a user.
        /// </summary>
        /// <param name="company">Company to define new instance for.</param>
        /// <param name="survey">Survey to be used/modified by this new instance.</param>
        /// <param name="currentUser">User to record as the current user for actions taken on the Survey. Use 'null' to indicate the system is taking action.</param>
        /// <returns>Company-specific instance of SurveyManager.</returns>
        public static SurveyManagerService GetInstance(CompanyDetail company)
        {
            switch (company.Summary.Abbreviation)
            {
                case "AIC":
                    return new CompanySpecific.AICSurveyManagerService(company, false);
                default:
                    throw new NotSupportedException("Company " + company.Summary.Abbreviation + " is not supported.");
            }
        }

        /// <summary>
        /// Create a new instance of this class to manage the specified Survey.
        /// </summary>
        /// <param name="survey">Survey to be used/modified by this new instance.</param>
        /// <param name="currentUser">User to record as the current user for actions taken on the Survey. Use 'null' to indicate the system is taking action.</param>
        /// <param name="overridePermissions">Flag to override permission checks for this user.</param>
        //protected SurveyManagerService(Survey survey, User currentUser, bool overridePermissions)
        protected SurveyManagerService(CompanyDetail company, bool overridePermissions)
        {
            if (company == null) throw new ArgumentNullException("company");

            _company = company;
            _overridePermissions = overridePermissions;

            //_survey = (survey == null) ? new SurveyViewModel() : survey;
            //_currentUser = currentUser;
            //this.Survey = survey;
        }

        #region Properties
        /// <summary>
        /// Gets or sets the current instance of the survey being managed by this class.
        /// NOTE: This instance is updated each time a change is made using the methods in this class.
        /// </summary>
        public SurveyViewModel Survey
        {
            get { return _survey; }
            set
            {
                if (value == null) throw new ArgumentNullException("value");
                _survey = value;
                //_surveyOwnedByUser = (_currentUser != null && _currentUser.ID == value.AssignedUserID);
                //_surveyOwnedByVendor = (value.AssignedUser != null && value.AssignedUser.IsFeeCompany);
            }
        }
        /// <summary>
        /// Gets the company being managed by this class.
        /// </summary>
        public CompanyDetail Company
        {
            get { return _company; }
        }
        /*
        /// <summary>
        /// Gets the user for which this manager will preforms actions.
        /// Null indicates no current users and the 'system' is assumed to be preforming actions.
        /// </summary>
        public User CurrentUser
        {
            get { return _currentUser; }
        }
        /// <summary>
        /// Returns the workflow helper for the company.
        /// </summary>
        //protected abstract WorkflowHelper Workflow { get; }      --- Return to this WorkflowHelper ??
        /// <summary>
        /// Gets of flag value that indicates if the survey is owned by the current users.
        /// </summary>
        protected bool SurveyOwnedByUser
        {
            //get { return _surveyOwnedByUser; }
        }
        /// <summary>
        /// Gets a of flag value that indicates if the survey owner is a fee company.
        /// </summary>
        protected bool SurveyOwnedByVendor
        {
            //get { return _surveyOwnedByVendor; }
        }
        */
        #endregion

        #region Methods
        // GetExternalAvailableAction --- How about a ActionsService?
        // GetAvailableActions
        // GetCurrentUserName
        // VerifyUserCanTakeAction
        // VerifyUserCanTakeThisAction


        // Pattern for Service Manager Methods

        // TODO: 063015 - Reimplement support for BPM Integration
        /// <summary>
        /// Creates a new survey for a insured location or locations.  Only used for creating workflow submissions.
        /// </summary>
        /// <returns>Survey created by this method.</returns>
        //public virtual Survey CreateSurvey() {}
        public virtual SurveyViewModel CreateSurvey(RequestViewModel request)
        {
            SurveyViewModel survey = new SurveyViewModel();

            OnCreateSurvey(survey);
            survey = CreateSurvey(request, survey); //, dueDate, oneSurvey, manuallyCreated, priority);
            OnCreateSurveyComplete(survey);

            return survey;
        }

        // performas the company-agnostic portion of the operation
        protected virtual SurveyViewModel CreateSurvey(RequestViewModel request, SurveyViewModel survey) 
        {
            var newSurvey = SurveyService.Save(_company, survey);

            //if (newSurvey != null)
            //{
                // Initiate the Import Manager Service to proceed with populating the requisite data to populate the survey request
                //ImportServices.ImportManagerService importManager = new ImportServices.ImportManagerService(_company, request, newSurvey);
                //importManager.ProcessRequest();
            //}
            
            return newSurvey;
        }

        // Performs additional custom processing before this operation is preformed.
        protected virtual void OnCreateSurvey(SurveyViewModel survey) 
        { }

         // Performs additional custom processing after this operation is preformed.
        protected virtual SurveyViewModel OnCreateSurveyComplete(SurveyViewModel survey) 
        {
            return survey;
        }
        
        // Survey CreateVisit
        // void CreateVisit
        // void OnCreateVisit
        // void OnCreateVisitComplete
        
        // Survey CreateSurveyFromExistingSurvey ( CopySurvey? )
        // Survey CreateSurveyFromExistingSurvey
        // void OnCreateSurveyFromExistingSurvey
        // void OnCreateSurveyFromExistingSurveyComplete

        // void NotifyUnderwriter
        // void NotifyUnderwriter
        // void OnNotifyUnderwriter
        // void OnNotifyUnderwriterComplete

        // void EmailUnderwriter ?    -- can we reduce and simplify
        // void EmailUnderwriter
        // void OnEmailUnderwriter
        // void OnEmailUnderwriterComplete
        // void EmailUser ?    -- can we reduce and simplify
        // void OnEmailUser

        // void Assign
        // void Assign
        // void OnAssign
        // void OnAssignComplete

        // bool AssignBasedOnTerritory
        // bool AssignBasedOnTerritory
        // void OnAssignBasedOnTerritory
        // void OnAssignBasedOnTerritoryComplete

        // void ReturnForCorrection
        // void ReturnForCorrection
        // void OnReturnForCorrection
        // void OnReturnForCorrectionComplete

        // TakeOwnership
        // ...

        // Accept
        // ...

        // AcceptByUnderwriter    -- can we reduce and simplify
        // ...

        // ChangeSubmissionNumber
        // ChangePrimaryPolicy
        // ...

        // ReleaseOwnership
        // ...

        // ReturnToCompany
        // ...

        // SendForReview
        // ...

        // ChangeSurveyType
        // ...

        // SetSurveyDetails
        // ...

        // Reopen
        // ...

        // SetMailSentDate
        // ...

        // SetMailReceivedDate
        // ...

        // ToggleValue
        // ...

        // AttachDocument
        // OnAttachDocumentIndex
        // ...

        // RemoveDocument
        // ...

        // WriteHistoryEntry
        // RemoveHistoryEntry
        // ...

        // AddNote
        // ...

        // RemoveNote
        // ...

        // Delete
        // ...

        // SurveyReview
        // ...

        // Cancel
        // ...

        // Close
        // ...

        // CloseAndNotify
        // ...

        // RecManagement
        // ...

        // AddPlanObjective
        // ObjectiveManagement
        // ...

        // ActivityManagement
        // ...

        // SetQualityAssessment
        // ...

        // ChangeDueDate
        // ...

        // ApproveDenyDueDate
        // ...

        // ChangeAssignedDate
        // ...

        // ChangeAcceptedDate
        // ...

        // ChangeReportSubmittedDate
        // ...

        // ChangeCompleteDate
        // ...

        // SaveSurvey
        // ...

        // PickUserForSurvey
        // ...

        // ChangeSurveyPriority
        // ...
        #endregion
    }
}
