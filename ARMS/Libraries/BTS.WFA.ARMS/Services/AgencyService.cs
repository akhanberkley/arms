﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ViewModels;
using BTS.WFA.ARMS.ViewModels;
using LinqKit;
using BTS.WFA.ARMS.Data.Models;
namespace BTS.WFA.ARMS.Services
{
    public class AgencyService
    {
        public static Agency GetAgencyByAgencyNumber(string agencyNumber, Guid companyId)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Agencies.AsExpandable().Where(a => a.AgencyNumber == agencyNumber && a.CompanyId == companyId).FirstOrDefault();
            }
        }

        public static List<AgencyViewModel> Search(CompanyDetail cd, Func<IQueryable, IQueryable> odataFilter)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var query = db.Agencies.Where(a => a.CompanyId == cd.CompanyId);
                query = odataFilter(query) as IQueryable<Data.Models.Agency>;

                return query.Select(Mapping.DataToViewModel.Agency()).ToList();
            }
        }

        public static List<ContactViewModel> GetAgents(CompanyDetail cd, string agencyCode)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                return db.Insureds.Where(i => i.CompanyId == cd.CompanyId && i.Agency.AgencyNumber == agencyCode && !String.IsNullOrEmpty(i.AgentContactName))
                                  .GroupBy(i => i.AgentContactName)
                                  .Select(g => g.OrderByDescending(i => (!String.IsNullOrEmpty(i.AgentContactPhoneNumber) ? 1 : 0) + ((i.AgentEmailId != null) ? 1 : 0)).FirstOrDefault())
                                  .Select(i => new ContactViewModel() { Name = i.AgentContactName, Phone = i.AgentContactPhoneNumber, Email = i.AgentEmail.EmailAddress.Trim() })
                                  .ToList();
            }
        }
    }
}
