﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;
using LinqKit;

namespace BTS.WFA.ARMS.Services
{
    public class DocumentsService
    {
        public static List<DocumentViewModel> GetLetters(CompanyDetail company)
        {
            List<DocumentViewModel> documents = new List<DocumentViewModel>();
            List<DocumentVersionsViewModel> documentversions = new List<DocumentVersionsViewModel>();
            List<DocumentTypesViewModel> documenttypes = new List<DocumentTypesViewModel>();

            using (var db = Application.GetDatabaseInstance())
            {
                documents = db.MergeDocuments.AsExpandable().Where(d => d.CompanyId == company.CompanyId && d.Type == 2)
                                                  .Select(Mapping.DataToViewModel.Document()).ToList();

                documenttypes = db.MergeDocumentTypes.Select(r => new DocumentTypesViewModel()
                {
                    Name = r.MergeDocumentTypeName
                }).ToList();
            }

            return documents;
        }

        public static List<DocumentViewModel> GetReports(CompanyDetail company)
        {
            List<DocumentViewModel> documents = new List<DocumentViewModel>();
            List<DocumentVersionsViewModel> documentversions = new List<DocumentVersionsViewModel>();
            List<DocumentTypesViewModel> documenttypes = new List<DocumentTypesViewModel>();

            using (var db = Application.GetDatabaseInstance())
            {
                documents = db.MergeDocuments.AsExpandable().Where(d => d.CompanyId == company.CompanyId && d.Type == 1)
                                                  .Select(Mapping.DataToViewModel.Document()).ToList();

                documenttypes = db.MergeDocumentTypes.Select(r => new DocumentTypesViewModel()
                {
                    Name = r.MergeDocumentTypeName
                }).ToList();
            }

            return documents;
        }



        public static DocumentViewModel SaveDocument(DocumentViewModel document, CompanyDetail company)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbDocument = db.MergeDocuments.FirstOrDefault(d => d.MergeDocumentId == document.ID && d.CompanyId == company.CompanyId);
                if (dbDocument == null)
                {
                    dbDocument = db.MergeDocuments.Add(new Data.Models.MergeDocument() { MergeDocumentId = new Guid(), CompanyId = company.CompanyId });
                }

                dbDocument.MergeDocumentName = document.Name;
                dbDocument.Disabled = document.Disabled;
                dbDocument.DaysUntilReminder = document.DaysUntilReminder;    
                db.SaveChanges();
            }
            return document;
        }
         
    }
}
