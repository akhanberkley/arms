﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ViewModels;
using LinqKit;

namespace BTS.WFA.ARMS.Services
{
    public class SurveyQueryService
    {
        public static SurveyViewModel Get(CompanyDetail cd, int number)
        {
            using (var db = Application.GetDatabaseInstance())
                return GetByQuery(db.Surveys.Where(s => s.CompanyId == cd.CompanyId && s.SurveyNumber == number)).FirstOrDefault();
        }

        public static List<SurveyViewModel> GetByQuery(IQueryable<Data.Models.Survey> query, Func<IQueryable, IQueryable> odataFilter = null)
        {
            if (odataFilter != null)
                query = odataFilter(query) as IQueryable<Data.Models.Survey>;

            var surveys = query.AsExpandable().Select(Mapping.DataToViewModel.Survey()).ToList();
            var surveyMap = surveys.ToDictionary(s => s.SurveyDbId);

            var surveyIds = surveyMap.Keys.ToArray();
            var insuredIds = surveys.Where(s => s.Insured != null).Select(s => s.InsuredDbId).ToArray();
            if (surveyIds.Length > 0)
            {
                List<Action> additionalQueries = new List<Action>();
                //Underwriters
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        var x = PredicateBuilder.False<Data.Models.Underwriter>();
                        var surveysWithUnderwriters = surveys.Where(s => !String.IsNullOrEmpty(s.UnderwriterCode)).Distinct().ToArray();
                        if (surveysWithUnderwriters.Length > 0)
                        {
                            foreach (var s in surveysWithUnderwriters)
                                x = x.Or(u => u.CompanyId == s.CompanyDbId && u.UnderwriterCode == s.UnderwriterCode);

                            var data = db.Underwriters.AsExpandable().Where(x)
                                                                     .ToDictionary(u => new Tuple<Guid, string>(u.CompanyId, u.UnderwriterCode),
                                                                                   u => new ContactViewModel() { Name = u.UnderwriterName, Email = u.EmailAddress, Phone = u.PhoneNumber });
                            foreach (var s in surveys)
                            {
                                var key = new Tuple<Guid, string>(s.CompanyDbId, s.UnderwriterCode);
                                if (data.ContainsKey(key))
                                    s.Underwriter = data[key];
                                else
                                    s.Underwriter = new ContactViewModel() { Name = s.UnderwriterCode };
                            }
                        }
                    }
                });

                //Comments
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        var data = db.SurveyNotes.AsExpandable().Where(n => surveyIds.Contains(n.SurveyId))
                                                                .OrderBy(n => n.SurveyId)
                                                                .ThenByDescending(n => n.EntryDate)
                                                                .Select(n => new
                                                                {
                                                                    SurveyId = n.SurveyId,
                                                                    Note = new SurveyCommentViewModel()
                                                                    {
                                                                        Id = n.SurveyNoteId,
                                                                        Date = n.EntryDate,
                                                                        Author = n.UserName ?? n.User.Name,
                                                                        Text = n.Comment,
                                                                        Internal = n.InternalOnly
                                                                    }
                                                                }).ToList();
                        foreach (var i in data)
                            surveyMap[i.SurveyId].Comments.Add(i.Note);
                    }
                });

                //Location Comments
                Dictionary<Tuple<Guid, Guid>, List<CommentViewModel>> locationComments = null;
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        locationComments = db.SurveyLocationNotes.AsExpandable().Where(n => surveyIds.Contains(n.SurveyId))
                                                                                .GroupBy(n => new { n.SurveyId, n.LocationId })
                                                                                .Select(g => new
                                                                                {
                                                                                    SurveyId = g.Key.SurveyId,
                                                                                    LocationId = g.Key.LocationId,
                                                                                    Note = g.OrderByDescending(n => n.EntryDate).Select(n => new CommentViewModel()
                                                                                    {
                                                                                        Date = n.EntryDate,
                                                                                        Author = n.UserName ?? n.User.Name,
                                                                                        Text = n.Comment
                                                                                    }).ToList()
                                                                                }).ToDictionary((n) => new Tuple<Guid, Guid>(n.SurveyId, n.LocationId), (n) => n.Note);
                    }
                });

                //Documents
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        var documentMapper = Mapping.DataToViewModel.SurveyDocument();
                        var data = db.SurveyDocuments.AsExpandable().Where(d => surveyIds.Contains(d.SurveyId))
                                                                    .OrderBy(d => d.SurveyId)
                                                                    .ThenByDescending(d => d.UploadedOn)
                                                                    .Select(d => new { SurveyId = d.SurveyId, Document = documentMapper.Invoke(d) })
                                                                    .ToList();
                        foreach (var i in data)
                            surveyMap[i.SurveyId].Documents.Add(i.Document);
                    }
                });

                //Locations
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        var locationMapper = Mapping.DataToViewModel.Location();
                        var data = db.Locations.AsExpandable().Where(l => insuredIds.Contains(l.InsuredId))
                                                              .OrderBy(l => l.LocationNumber)
                                                              .Select(l => new
                                                              {
                                                                  InsuredId = l.InsuredId,
                                                                  Location = locationMapper.Invoke(l)
                                                              }).ToList();
                        foreach (var i in data)
                            surveys.Where(s => s.InsuredDbId == i.InsuredId).ForEach(s => s.Locations.Add(i.Location));
                    }
                });

                //Policies
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        var policyMapper = Mapping.DataToViewModel.Policy();
                        var data = db.Policies.AsExpandable().Where(p => insuredIds.Contains(p.InsuredId))
                                                             .OrderByDescending(p => p.EffectiveDate)
                                                             .Select(p => new { InsuredId = p.InsuredId, Policy = policyMapper.Invoke(p) })
                                                             .ToList();
                        foreach (var i in data)
                            surveys.Where(s => s.InsuredDbId == i.InsuredId).ForEach(s => s.Policies.Add(i.Policy));
                    }
                });

                //Determine which coverages/locations are included
                Dictionary<Tuple<Guid, Guid>, List<Tuple<Guid, string>>> surveyAssociations = null;
                additionalQueries.Add(() =>
                {
                    using (var db = Application.GetDatabaseInstance())
                    {
                        surveyAssociations = db.SurveyPolicyCoverages.GroupBy(c => new { c.SurveyId, c.PolicyId })
                                                                     .Where(g => surveyIds.Contains(g.Key.SurveyId))// && g.Any(c => c.Active == true))
                                                                     .Select(g => new
                                                                     {
                                                                         SurveyId = g.Key.SurveyId,
                                                                         PolicyId = g.Key.PolicyId,
                                                                         LocationCoverages = g.Where(c => c.Active == true && c.ReportTypeCode == "R")
                                                                                              .Select(c => new { c.LocationId, c.CoverageName.CoverageNameTypeCode })
                                                                                              .ToList()
                                                                     })
                                                                     .ToDictionary(p => new Tuple<Guid, Guid>(p.SurveyId, p.PolicyId), p => p.LocationCoverages.Select(lc => new Tuple<Guid, string>(lc.LocationId, lc.CoverageNameTypeCode)).ToList());
                    }
                });

                //Threaded work
                Task[] threads = new Task[additionalQueries.Count];
                for (var i = 0; i < additionalQueries.Count; i++)
                    threads[i] = (Task.Factory.StartNew(additionalQueries[i]));
                Task.WaitAll(threads);

                //Nonthreaded (for debugging/performance tuning)
                //foreach (var q in additionalQueries)
                //    q();

                foreach (var s in surveys)
                {
                    //foreach (var p in s.Policies.ToArray())
                    //{
                    //    var key = new Tuple<Guid, Guid>(s.SurveyDbId, p.PolicyDbId);
                    //    if (!surveyAssociations.ContainsKey(key))
                    //        s.Policies.Remove(p);
                    //    else
                    //    {
                    //        var locationCoverages = surveyAssociations[key];
                    //        foreach (var c in p.Coverages.Where(c => locationCoverages.Any(lc => lc.Item2 == c.Name.Code)))
                    //            c.Included = true;
                    //        foreach (var l in s.Locations.Where(l => locationCoverages.Any(lc => lc.Item1 == l.LocationDbId)))
                    //            l.Included = true;
                    //    }
                    //}

                    foreach(var l in s.Locations)
                    {
                        var key = new Tuple<Guid, Guid>(s.SurveyDbId, l.LocationDbId);
                        if (locationComments.ContainsKey(key))
                            l.Comments = locationComments[key];
                    }
                }
            }

            return surveys;
        }
    }
}
