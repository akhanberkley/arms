﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.Services.CompanySpecific
{
    /// <summary>
    /// Concrete class that implements SurveyManager functions for AIC.
    /// </summary>
    public class AICSurveyManagerService : SurveyManagerService
    {
        //private static readonly WorkflowHelper workflow;
        //private static readonly string docRemarks;

        static AICSurveyManagerService()
        {
            //if (Company.AcadiaInsuranceCompany.MilestoneDate == DateTime.MinValue || Company.AcadiaInsuranceCompany.MilestoneDate > DateTime.Today)
            //{
            //    workflow = new WorkflowHelper(Company.AcadiaInsuranceCompany, Berkley.Workflow.WorkflowSystemType.ALBPM_5);
            //    docRemarks = "ARMS: Remark from LC ";
            //}
            //else
            //{
            //    workflow = new WorkflowHelper(Company.AcadiaInsuranceCompany, Berkley.Workflow.WorkflowSystemType.ALBPM_6);
            //    docRemarks = "ARMS";
            //}
        }

        //internal AICSurveyManagerService(Survey survey, User currentUser, bool overridePermissions)
        internal AICSurveyManagerService(CompanyDetail company, bool overridePermissions)
            : base(company, overridePermissions)
        {
        }

        #region Workflow
        #endregion

        #region Base class overrides
        #endregion
    }
}
