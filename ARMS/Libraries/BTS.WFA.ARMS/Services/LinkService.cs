﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.Services
{
    public class LinkService
    {
        public static List<LinkViewModel> GetAll(CompanyDetail company)
        {
            List<LinkViewModel> links = new List<LinkViewModel>();

            using (var db = Application.GetDatabaseInstance())
            {
                links = db.Links.Where(l => l.CompanyId == company.CompanyId)
                                    .OrderBy(l => l.PriorityIndex)
                                   .Select(Mapping.DataToViewModel.Link()).ToList();
            }

            return links;
        }

        public static LinkViewModel SaveLink(LinkViewModel link, CompanyDetail company)
        {
            if (String.IsNullOrEmpty(link.URL))
                link.AddValidation("URL", "A URL is required");

            // Check for and prohibit duplicate links, if the URL is already present?

            if (link.ValidationResults.Count == 0)
            {
                using (var db = Application.GetDatabaseInstance())
                {
                    var dbLink = db.Links.FirstOrDefault(l => l.LinkId == link.ID);
                    if (dbLink == null)
                    {
                        dbLink = db.Links.Add(new Data.Models.Link() { LinkId = new Guid(), CompanyId = company.CompanyId });
                        dbLink.PriorityIndex = db.Links.OrderByDescending(i => i.PriorityIndex).Select(i => i.PriorityIndex).First() + 1;
                    }

                    dbLink.LinkDescription = link.Description;
                    dbLink.LinkUrl = link.URL;

                    db.SaveChanges();
                    
                }
              
            }
            return link;
        }

        public static List<ValidationResult> DeleteLink(Guid ID, CompanyDetail company)
        {
            var validations = new BaseViewModel();
            using (var db = Application.GetDatabaseInstance())
            {
                try
                {
                    var dbLink = db.Links.Where(u => u.CompanyId == company.CompanyId && u.LinkId == ID)
                        .FirstOrDefault();

                    db.Links.Remove(dbLink);

                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    if (ex is DbUpdateException)
                    {
                        validations.AddValidation("General", "This Link could not be deleted.");
                        Application.HandleException(ex, new { Company = company.Summary, LinkId = ID });
                    }
                    else
                        throw ex;
                }

            }

            return validations.ValidationResults;
        }

    }
}