﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.ViewModels;
using LinqKit;

namespace BTS.WFA.ARMS.Services
{
    public class TerritoryService
    {
        public static List<TerritoryViewModel> GetTerritories(CompanyDetail company)
        {
            List<TerritoryViewModel> territories = new List<TerritoryViewModel>();
            List<UserTerritoriesViewModel> userterritories = new List<UserTerritoriesViewModel>();

            using (var db = Application.GetDatabaseInstance())
            {
                territories = db.Territories.AsExpandable().Where(t => t.CompanyId == company.CompanyId)
                                                  .Select(Mapping.DataToViewModel.Territory()).ToList();                        

            }

            return territories;
        }

        public static TerritoryViewModel SaveTerritory(TerritoryViewModel territory, CompanyDetail company)
        {
            using (var db = Application.GetDatabaseInstance())
            {
                var dbTerritory = db.Territories.FirstOrDefault(t => t.TerritoryId == territory.ID && t.CompanyId == company.CompanyId);
                if (dbTerritory == null)
                {
                    dbTerritory = db.Territories.Add(new Data.Models.Territory() { TerritoryId = new Guid(), CompanyId = company.CompanyId });
                }

                dbTerritory.TerritoryDescription = territory.Description;
                dbTerritory.TerritoryName = territory.Name;
                dbTerritory.InHouseSelection = territory.InHouseSelection;             
                
                db.SaveChanges();
            }
            return territory;
        }
    }
}
