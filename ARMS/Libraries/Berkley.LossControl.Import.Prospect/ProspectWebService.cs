using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using System.Data;
using System.IO;

//using Berkley.BLC.Core;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS;
using BTS.WFA.ARMS.Data.Models;

namespace Berkley.BLC.Import.Prospect
{
    public class ProspectWebService
    {
        private Berkley.BLC.Import.Prospect.SubmissionProxy.Submission _submissionService;
        private Berkley.BLC.Import.Prospect.ClientProxy.Client _clientService;
        private Berkley.BLC.Import.Prospect.Lookup.Lookup _lookupService;
        
        public ProspectWebService()
        {
            _submissionService = new Berkley.BLC.Import.Prospect.SubmissionProxy.Submission();
            _clientService = new Berkley.BLC.Import.Prospect.ClientProxy.Client();
            _lookupService = new Lookup.Lookup();

            _submissionService.Timeout = 100000;
            _clientService.Timeout = 100000;
            _lookupService.Timeout = 100000;

            /*
            switch (CoreSettings.ConfigurationType)
            {
                case ConfigurationType.Production:
                    _submissionService.Url = "http://bcs-services:3406/Submission.asmx";
                    //_clientService.Url = "http://bcs-services:3406/Client.asmx";
                    _lookupService.Url = "http://bcs-services:3406/Lookup.asmx";

                    _clientService.Url = "http://btsdebcs02:9191/client.asmx";

                    break;
                case ConfigurationType.Test:
                    _submissionService.Url = "http://btsdetstwfa01:3406/Submission.asmx";
                    _clientService.Url = "http://btsdetstwfa01:3406/Client.asmx";
                    _lookupService.Url = "http://btsdetstwfa01:3406/Lookup.asmx";
                    break;
                case ConfigurationType.Integration:
                    _submissionService.Url = "http://btsdeintwfa01:3406/Submission.asmx";
                    _clientService.Url = "http://btsdeintwfa01:3406/Client.asmx";
                    _lookupService.Url = "http://btsdeintwfa01:3406/Lookup.asmx";
                    break;
                case ConfigurationType.Development:
                    //_submissionService.Url = "http://btsdedevwfa01:3406/Submission.asmx";
                    //_clientService.Url = "http://btsdedevwfa01:3406/Client.asmx";

                    _submissionService.Url = "http://btsdetstwfa01:3406/Submission.asmx";
                    _clientService.Url = "http://btsdetstwfa01:3406/Client.asmx";
                    _lookupService.Url = "http://btsdetstwfa01:3406/Lookup.asmx";
                    break;
                default:

                    throw new NotSupportedException("Expecting an environment to be set.");
            }
            */
            _submissionService.Url = "http://btsdetstwfa01:3406/Submission.asmx";
            _clientService.Url = "http://btsdetstwfa01:3406/Client.asmx";
            _lookupService.Url = "http://btsdetstwfa01:3406/Lookup.asmx";
        }

        /// <summary>
        /// Returns a flag indicating if a submission exist or not in Clearance given a policy number.
        /// </summary>
        /// <param name="policyNumber">The policy number to query.</param>
        /// <param name="company">The Berkley company.</param>
        public bool DoesSubmissionExist(string policyNumber, Company company)
        {
            bool result = false;
            string xml;

            try
            {
                xml = _submissionService.GetSubmissionsByPolicyNumberAndCompanyNumber(policyNumber, company.CompanyNumber);

                // Validation
                if (xml == null || xml.Length == 0)
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Prospect web service not available: {0}", ex.Message));
            }

            // Check if any submissions exist in the xml
            DataSet ds = Deserialize(xml);
            if (ds.Tables["submissions"] != null && ds.Tables["submissions"].Rows.Count > 0)
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Returns a flag indicating if a submission exist or not in Clearance given a submission number.
        /// </summary>
        /// <param name="submissionNumber">The submission number to query.</param>
        /// <param name="company">The Berkley company.</param>
        public bool DoesSubmissionExistBySubmissionNumber(string submissionNumber, Company company)
        {
            try
            {
                string xml = _submissionService.GetSubmissionBySubmissionNumberAndCompanyNumber(submissionNumber, company.CompanyNumber, "");

                // Validation
                if (xml == null || xml.Length == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Prospect web service not available: {0}", ex.Message));
            }
        }

        /// <summary>
        /// Returns a dataset of submission(s) from Clearance given the submission number.
        /// </summary>
        /// <param name="submissionNumber">The submission number to query.</param>
        /// <param name="company">The Berkley company.</param>
        public DataSet GetSubmissionFromSubmissionNumber(string submissionNumber, Company company)
        {
            string xml;
            try
            {
                xml = _submissionService.GetSubmissionBySubmissionNumberAndCompanyNumber(submissionNumber, company.CompanyNumber, String.Empty);
            }
            catch (Exception ex)
            {
                throw new BlcProspectWebServiceException(string.Format("Prospect web service not available: {0}", ex.Message));
            }

            //validation
            if (xml == null || xml.Length == 0)
            {
                throw new BlcProspectWebServiceException(string.Format("Submission number {0} not found in Clearance.", submissionNumber));
            }

            return Deserialize(xml);
        }


        public Underwriter GetAssignedUnderwriter(string policyNumber, Company company)
        {
            string xml;
            Underwriter underwriter = null;
            try
            {
                try
                {
                    xml = _submissionService.GetSubmissionsByPolicyNumberAndCompanyNumber(policyNumber, company.CompanyNumber);
                }
                catch (Exception ex)
                {
                    throw new BlcProspectWebServiceException(string.Format("Prospect web service not available: {0}", ex.Message));
                }

                //validation
                if (xml == null || xml.Length == 0)
                {
                    throw new BlcProspectWebServiceException(string.Format("Policy number {0} not found in Clearance.", policyNumber));
                }

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(xml);

                XmlNode nodeUWUsername = xmlDoc.SelectSingleNode("/submission_list/submissions/submission/underwriter_name");
                if (nodeUWUsername != null && !string.IsNullOrEmpty(nodeUWUsername.InnerXml))
                {
                    //does this UW already exist in ARMS?
                    string userName = nodeUWUsername.InnerXml;
                    
                    //Underwriter existingUnderwriter = Underwriter.GetOne("Username = ? && CompanyID = ?", userName, company.CompanyId);
                    IQueryable<Underwriter> existingUnderwriter = null;
                    using (var db = Application.GetDatabaseInstance())
                    {
                        existingUnderwriter = from uw in db.Underwriters
                                              where uw.Username == userName 
                                                && uw.CompanyId == company.CompanyId
                                              select uw;
                    }
                    
                    if (existingUnderwriter != null)
                    {
                        return existingUnderwriter.FirstOrDefault();
                    }
                    else
                    {
                        //populate some underwriter details
                        XmlNode nodeUWInitials = xmlDoc.SelectSingleNode("/submission_list/submissions/submission/underwriter_initials");
                        XmlNode nodeUWFullName = xmlDoc.SelectSingleNode("/submission_list/submissions/submission/underwriter_full_name");
                        //underwriter = new Underwriter();
                        //underwriter.CompanyId = company.CompanyId;
                        //underwriter.DomainName = "WRBTS";
                        //underwriter.Username = userName;
                        //underwriter.Title = "UW";
                        //underwriter.Disabled = false;
                        //underwriter.PhoneNumber = "(none)";

                        //XmlNode nodeUWFullName = xmlDoc.SelectSingleNode("/submission_list/submissions/submission/underwriter_full_name");
                        //underwriter.UnderwriterName = nodeUWFullName.InnerXml;

                        //underwriter.Save();

                        using (var db = Application.GetDatabaseInstance())
                        {
                            // Save the new Underwriter
                            var dbUnderwriter = db.Underwriters.Add(new Underwriter() { CompanyId = company.CompanyId });

                            dbUnderwriter.UnderwriterCode = nodeUWInitials.ToString();
                            dbUnderwriter.UnderwriterName = nodeUWFullName.ToString();
                            dbUnderwriter.Username = userName;
                            dbUnderwriter.PhoneNumber = "(none)";
                            dbUnderwriter.PhoneExt = "";
                            dbUnderwriter.EmailAddress = "";
                            dbUnderwriter.DomainName = "WRBTS";
                            dbUnderwriter.Disabled = false;
                            dbUnderwriter.Title = "UW";
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string test = ex.Message;
                
                //swallow any exception and return a null underwriter object
            }

            return underwriter;
        }

        /// <summary>
        /// Returns a dataset of submission(s) from Clearance given the client id.
        /// </summary>
        /// <param name="submissionNumber">The client id to query.</param>
        /// <param name="company">The Berkley company.</param>
        public DataSet CallSubmissionFromClientId(string clientID, Company company)
        {
            string xml;
            try
            {
                xml = _submissionService.GetSubmissionsByClientId(company.CompanyNumber, clientID);
            }
            catch (Exception ex)
            {
                throw new BlcProspectWebServiceException(string.Format("Prospect web service not available: {0}", ex.Message));
            }

            //validation
            if (xml == null || xml.Length == 0)
            {
                throw new BlcProspectWebServiceException(string.Format("Client ID {0} not found in Clearance or no submissions exist for this client.", clientID));
            }

            return Deserialize(xml);
        }

        /// <summary>
        /// Returns a dataset of client information from Client Core given the client id.
        /// </summary>
        /// <param name="submissionNumber">The client id to query.</param>
        /// <param name="company">The Berkley company.</param>
        public DataSet CallClientWebService(string clientID, Company company)
        {
            string xml;
            try
            {
                xml = _clientService.GetClientById(company.CompanyNumber, clientID);
            }
            catch (Exception ex)
            {
                throw new BlcProspectWebServiceException(string.Format("Prospect client web service not available: {0}", ex.Message));
            }

            //validation
            if (xml == null || xml.Length == 0)
            {
                throw new BlcProspectWebServiceException(string.Format("Client ID {0} not found in Clearance.", clientID));
            }

            return Deserialize(xml);
        }

        /// <summary>
        /// Returns the status of the submission.
        /// </summary>
        /// <param name="submissionNumber">The submission number.</param>
        /// <param name="company">The Berkley company.</param>
        public string GetSubmissionStatus(string submissionNumber, Company company)
        {
            string result = string.Empty;

            try
            {
                DataSet ds = GetSubmissionFromSubmissionNumber(submissionNumber, company);
                foreach (DataTable table in ds.Tables)
                {
                    if (table.Rows.Count > 0)
                    {
                        if (table.TableName == "submission_status")
                        {
                            result = (table.Rows[0]["status_code"] != null) ? table.Rows[0]["status_code"].ToString() : string.Empty;
                            break;
                        }
                    }
                }
            }
            catch (BlcProspectWebServiceException)
            {
                //swallow this exception and return the empty string as a default
            }

            return result;
        }

        /// <summary>
        /// Returns the agency details.
        /// </summary>
        /// <param name="companyNumber">The company number.</param>
        /// <param name="agencyNumber">The agency number.</param>
        public DataSet GetAgencyDetails(int bcsCompanyNumberID, string agencyNumber)
        {
            string xml = string.Empty;

            try
            {
                int agencyID = _lookupService.GetAgencyIdByCompanyNumberIdAgencyNumber(bcsCompanyNumberID, agencyNumber);
                xml = _lookupService.GetAgencyById(agencyID);
            }
            catch (BlcProspectWebServiceException)
            {
                //swallow this exception and return the empty string as a default
            }

            //validation
            if (xml == null || xml.Length == 0)
            {
                throw new BlcProspectWebServiceException(string.Format("Agency Number '{0}' was not found in Clearance.", agencyNumber));
            }

            return Deserialize(xml);
        }

        public void SubmitClientMembershipInfo(Company company, string clientID, string membershipGroup, string primaryMember)
        {
            try
            {
                string xmlResponse = _clientService.ModifyClientMembershipInfo(company.CompanyNumber, clientID, (primaryMember == "1") ? membershipGroup : string.Empty);
                if (xmlResponse.Contains("ERROR"))
                {
                    throw new Exception(string.Format("Error updating client {0}. {1}", clientID, xmlResponse));
                }
            }
            catch (Exception ex)
            {
                throw new BlcProspectWebServiceException("Issue communicating with the Client Core.  An administrator has been notified", ex);
            }
        }

        public string GetClientMembershipInfo(Company company, string clientID, string membershipGroup, string defaultReturnValue)
        {
            string xml;
            try
            {
                xml = _clientService.GetClientById(company.CompanyNumber, clientID);
            }
            catch (Exception ex)
            {
                throw new BlcProspectWebServiceException("Issue communicating with the Client Core.  An administrator has been notified.", ex);
            }

            //validation
            if (xml == null || xml.Length == 0 || !xml.Contains("<client_id>"))
            {
                throw new BlcProspectWebServiceException(string.Format("Client ID {0} was not found in Client Core.  An administrator has been notified.", clientID));
            }

            XmlDocument doc = new XmlDocument();
            StringReader stringReader = new StringReader(xml);
            doc.Load(stringReader);

            XmlNode parentNode = doc.SelectSingleNode("/ClearanceClient/clientcore_client/client/membership-group");

            string result = defaultReturnValue;
            if (parentNode != null && parentNode.InnerXml == membershipGroup)
            {
                //if the group name is found then return a true/1
                result = "1";
            }

            return result;
        }


        private DataSet Deserialize(string xml)
        {
            StringReader stringReader = new StringReader(xml);
            XmlTextReader xmlReader = new XmlTextReader(stringReader);

            DataSet ds = new DataSet();
            ds.ReadXml(xmlReader);

            return ds;
        }
    }
}
