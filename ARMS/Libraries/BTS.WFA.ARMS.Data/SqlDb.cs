using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Reflection;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.Data
{
    public class SqlDb : BTS.WFA.Data.BaseDbContext, IArmsDb
    {
        public DbSet<Activity> Activities { get; set; }
        public DbSet<ActivityDocument> ActivityDocuments { get; set; }
        public DbSet<ActivityType> ActivityTypes { get; set; }
        public DbSet<ActivityTypeLevel> ActivityTypeLevels { get; set; }
        public DbSet<Agency> Agencies { get; set; }
        public DbSet<AgencyEmail> AgencyEmails { get; set; }
        public DbSet<AssignmentRule> AssignmentRules { get; set; }
        public DbSet<AssignmentRuleAction> AssignmentRuleActions { get; set; }
        public DbSet<AssignmentRuleActionType> AssignmentRuleActionTypes { get; set; }
        public DbSet<AttributeDataType> AttributeDataTypes { get; set; }
        public DbSet<Building> Buildings { get; set; }
        public DbSet<BuildingValuationType> BuildingValuationTypes { get; set; }
        public DbSet<Claim> Claims { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientAttribute> ClientAttributes { get; set; }
        public DbSet<ClientAttributeType> ClientAttributeTypes { get; set; }
        public DbSet<ClientHistory> ClientHistories { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyAttribute> CompanyAttributes { get; set; }
        public DbSet<CompanyCounter> CompanyCounters { get; set; }
        public DbSet<CompanyCounterType> CompanyCounterTypes { get; set; }
        public DbSet<CompanyLineOfBusiness> CompanyLineOfBusinesses { get; set; }
        public DbSet<CompanyParameter> CompanyParameters { get; set; }
        public DbSet<CompanyPolicySystem> CompanyPolicySystems { get; set; }
        public DbSet<ContactReasonType> ContactReasonTypes { get; set; }
        public DbSet<Coverage> Coverages { get; set; }
        public DbSet<CoverageDetail> CoverageDetails { get; set; }
        public DbSet<CoverageName> CoverageNames { get; set; }
        public DbSet<CoverageNameType> CoverageNameTypes { get; set; }
        public DbSet<CoverageType> CoverageTypes { get; set; }
        public DbSet<CreateState> CreateStates { get; set; }
        public DbSet<CreateSurveyType> CreateSurveyTypes { get; set; }
        public DbSet<DocType> DocTypes { get; set; }
        public DbSet<DueDateHistory> DueDateHistories { get; set; }
        public DbSet<DynamicField> DynamicFields { get; set; }
        public DbSet<EntityType> EntityTypes { get; set; }
        public DbSet<ExceptionEmailFilterString> ExceptionEmailFilterStrings { get; set; }
        public DbSet<ExceptionLog> ExceptionLogs { get; set; }
        public DbSet<ExceptionReturnMessage> ExceptionReturnMessages { get; set; }
        public DbSet<FeeUser> FeeUsers { get; set; }
        public DbSet<Filter> Filters { get; set; }
        public DbSet<FilterCondition> FilterConditions { get; set; }
        public DbSet<FilterField> FilterFields { get; set; }
        public DbSet<FilterOperator> FilterOperators { get; set; }
        public DbSet<FormFieldDataType> FormFieldDataTypes { get; set; }
        public DbSet<GeographicArea> GeographicAreas { get; set; }
        public DbSet<GeographicUnit> GeographicUnits { get; set; }
        public DbSet<GridColumnFilter> GridColumnFilters { get; set; }
        public DbSet<GridColumnFilterCompany> GridColumnFilterCompanies { get; set; }
        public DbSet<Insured> Insureds { get; set; }
        public DbSet<LineOfBusiness> LineOfBusinesses { get; set; }
        public DbSet<Link> Links { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<LocationContact> LocationContacts { get; set; }
        public DbSet<LoginHistory> LoginHistories { get; set; }
        public DbSet<MergeDocument> MergeDocuments { get; set; }
        public DbSet<MergeDocumentField> MergeDocumentFields { get; set; }
        public DbSet<MergeDocumentType> MergeDocumentTypes { get; set; }
        public DbSet<MergeDocumentVersion> MergeDocumentVersions { get; set; }
        public DbSet<MimeType> MimeTypes { get; set; }
        public DbSet<Naic> Naics { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Objective> Objectives { get; set; }
        public DbSet<ObjectiveAttribute> ObjectiveAttributes { get; set; }
        public DbSet<ObjectiveStatus> ObjectiveStatus { get; set; }
        public DbSet<Parameter> Parameters { get; set; }
        public DbSet<PermissionSet> PermissionSets { get; set; }
        public DbSet<Policy> Policies { get; set; }
        public DbSet<PriorityRating> PriorityRatings { get; set; }
        public DbSet<ProfitCenter> ProfitCenters { get; set; }
        public DbSet<RateModificationFactor> RateModificationFactors { get; set; }
        public DbSet<RateModificationFactorType> RateModificationFactorTypes { get; set; }
        public DbSet<RecClassification> RecClassifications { get; set; }
        public DbSet<RecClassificationType> RecClassificationTypes { get; set; }
        public DbSet<Recommendation> Recommendations { get; set; }
        public DbSet<RecStatus> RecStatus { get; set; }
        public DbSet<RecStatusType> RecStatusTypes { get; set; }
        public DbSet<ReportType> ReportTypes { get; set; }
        public DbSet<ReportTypeCompany> ReportTypeCompanies { get; set; }
        public DbSet<RequestQueue> RequestQueue { get; set; }               //WA-141 Doug Bruce 2015-03-26 - RequestQueue DB Structure
        public DbSet<RequestQueueStatus> RequestQueueStatuses { get; set; }   //WA-141 Doug Bruce 2015-03-26 - RequestQueue DB Structure
        public DbSet<RequestQueueSearch> RequestQueueSearches { get; set; }   //WA-141 Doug Bruce 2015-03-26 - RequestQueue DB Structure
        public DbSet<ReviewCategoryType> ReviewCategoryTypes { get; set; }
        public DbSet<ReviewerType> ReviewerTypes { get; set; }
        public DbSet<ReviewRatingType> ReviewRatingTypes { get; set; }
        public DbSet<ServicePlan> ServicePlans { get; set; }
        public DbSet<ServicePlanAction> ServicePlanActions { get; set; }
        public DbSet<ServicePlanActionPermission> ServicePlanActionPermissions { get; set; }
        public DbSet<ServicePlanActionType> ServicePlanActionTypes { get; set; }
        public DbSet<ServicePlanActivity> ServicePlanActivities { get; set; }
        public DbSet<ServicePlanAttribute> ServicePlanAttributes { get; set; }
        public DbSet<ServicePlanDocument> ServicePlanDocuments { get; set; }
        public DbSet<ServicePlanDynamicField> ServicePlanDynamicFields { get; set; }
        public DbSet<ServicePlanGridColumnFilter> ServicePlanGridColumnFilters { get; set; }
        public DbSet<ServicePlanGridColumnFilterCompany> ServicePlanGridColumnFilterCompanies { get; set; }
        public DbSet<ServicePlanHistory> ServicePlanHistories { get; set; }
        public DbSet<ServicePlanNote> ServicePlanNotes { get; set; }
        public DbSet<ServicePlanStatus> ServicePlanStatus { get; set; }
        public DbSet<ServicePlanType> ServicePlanTypes { get; set; }
        public DbSet<ServiceType> ServiceTypes { get; set; }
        public DbSet<SeverityRating> SeverityRatings { get; set; }
        public DbSet<Sic> Sics { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<SurveyAction> SurveyActions { get; set; }
        public DbSet<SurveyActionPermission> SurveyActionPermissions { get; set; }
        public DbSet<SurveyActionType> SurveyActionTypes { get; set; }
        public DbSet<SurveyActivity> SurveyActivities { get; set; }
        public DbSet<SurveyAttribute> SurveyAttributes { get; set; }
        public DbSet<SurveyDocument> SurveyDocuments { get; set; }
        public DbSet<SurveyGrading> SurveyGradings { get; set; }
        public DbSet<SurveyGradingCompany> SurveyGradingCompanies { get; set; }
        public DbSet<SurveyHistory> SurveyHistories { get; set; }
        public DbSet<SurveyLetter> SurveyLetters { get; set; }
        public DbSet<SurveyLocationNote> SurveyLocationNotes { get; set; }
        public DbSet<SurveyLocationPolicyCoverageName> SurveyLocationPolicyCoverageNames { get; set; }
        public DbSet<SurveyLocationPolicyExclusion> SurveyLocationPolicyExclusions { get; set; }
        public DbSet<SurveyMergeDocumentField> SurveyMergeDocumentFields { get; set; }
        public DbSet<SurveyNote> SurveyNotes { get; set; }
        public DbSet<SurveyPolicyCoverage> SurveyPolicyCoverages { get; set; }
        public DbSet<SurveyQuality> SurveyQualities { get; set; }
        public DbSet<SurveyQualityType> SurveyQualityTypes { get; set; }
        public DbSet<SurveyReasonType> SurveyReasonTypes { get; set; }
        public DbSet<SurveyRecommendation> SurveyRecommendations { get; set; }
        public DbSet<SurveyReleaseOwnership> SurveyReleaseOwnerships { get; set; }
        public DbSet<SurveyReleaseOwnershipType> SurveyReleaseOwnershipTypes { get; set; }
        public DbSet<SurveyReview> SurveyReviews { get; set; }
        public DbSet<SurveyReviewQueue> SurveyReviewQueues { get; set; }
        public DbSet<SurveyReviewRating> SurveyReviewRatings { get; set; }
        public DbSet<SurveyStatus> SurveyStatus { get; set; }
        public DbSet<SurveyStatusType> SurveyStatusTypes { get; set; }
        public DbSet<SurveyType> SurveyTypes { get; set; }
        public DbSet<SurveyTypeType> SurveyTypeTypes { get; set; }
        public DbSet<Territory> Territories { get; set; }
        public DbSet<TransactionType> TransactionTypes { get; set; }
        public DbSet<Underwriter> Underwriters { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<UserSearch> UserSearches { get; set; }
        public DbSet<UserSetting> UserSettings { get; set; }
        public DbSet<UserTerritory> UserTerritories { get; set; }
        public DbSet<WebPage> WebPages { get; set; }
        public DbSet<WebPageHelpfulHint> WebPageHelpfulHints { get; set; }
        public DbSet<Zip> Zips { get; set; }

        static SqlDb()
        {
            Database.SetInitializer<SqlDb>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
