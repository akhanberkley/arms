using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyAction
    {
        public Guid SurveyActionId { get; set; }
        public Guid CompanyId { get; set; }
        public string SurveyStatusCode { get; set; }
        public string SurveyActionTypeCode { get; set; }
        public string DisplayName { get; set; }
        public int DisplayOrder { get; set; }
        public string ConfirmMessage { get; set; }

        public virtual ICollection<SurveyActionPermission> SurveyActionPermissions { get; set; }

        public virtual Company Company { get; set; }
        public virtual SurveyActionType SurveyActionType { get; set; }
        public virtual SurveyStatus SurveyStatus { get; set; }

        public SurveyAction()
        {
            SurveyActionPermissions = new List<SurveyActionPermission>();
        }
    }

    internal class SurveyActionConfiguration : EntityTypeConfiguration<SurveyAction>
    {
        public SurveyActionConfiguration()
        {
            ToTable("dbo.SurveyAction");
            HasKey(x => x.SurveyActionId);

            Property(x => x.SurveyActionId).HasColumnName("SurveyActionID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.SurveyStatusCode).HasColumnName("SurveyStatusCode").IsRequired().HasMaxLength(2);
            Property(x => x.SurveyActionTypeCode).HasColumnName("SurveyActionTypeCode").IsRequired().HasMaxLength(10);
            Property(x => x.DisplayName).HasColumnName("DisplayName").IsRequired().HasMaxLength(60);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.ConfirmMessage).HasColumnName("ConfirmMessage").IsOptional().HasMaxLength(500);

            HasRequired(a => a.Company).WithMany().HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.SurveyStatus).WithMany(b => b.SurveyActions).HasForeignKey(c => c.SurveyStatusCode);
            HasRequired(a => a.SurveyActionType).WithMany(b => b.SurveyActions).HasForeignKey(c => c.SurveyActionTypeCode);
        }
    }
}
