using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class CompanyParameter
    {
        public Guid CompanyId { get; set; }
        public int ParameterId { get; set; }
        public Guid? DivisionId { get; set; }
        public string Value { get; set; }
        public DateTime CreateDateTime { get; set; }
        public int SortOrder { get; set; }
        public DateTime? EffectiveDate { get; set; }

        public virtual Company Company { get; set; }
        public virtual Parameter Parameter { get; set; }
    }

    internal class CompanyParameterConfiguration : EntityTypeConfiguration<CompanyParameter>
    {
        public CompanyParameterConfiguration()
        {
            ToTable("dbo.CompanyParameter");
            HasKey(x => new { x.CompanyId, x.ParameterId, x.Value });

            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.ParameterId).HasColumnName("ParameterID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.DivisionId).HasColumnName("DivisionID").IsOptional();
            Property(x => x.Value).HasColumnName("Value").IsRequired().HasMaxLength(400).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CreateDateTime).HasColumnName("CreateDateTime").IsRequired();
            Property(x => x.SortOrder).HasColumnName("SortOrder").IsRequired();
            Property(x => x.EffectiveDate).HasColumnName("EffectiveDate").IsOptional();

            HasRequired(a => a.Company).WithMany(b => b.CompanyParameters).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.Parameter).WithMany(b => b.CompanyParameters).HasForeignKey(c => c.ParameterId);
        }
    }
}
