using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Company
    {
        public Guid CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAbbreviation { get; set; }
        public string CompanyLongAbbreviation { get; set; }
        public string CompanyNumber { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyFax { get; set; }
        public string PrimaryDomainName { get; set; }
        public string PolicyStarEndPointUrl { get; set; }
        public string PdrEndPointUrl { get; set; }
        public int? Dsik { get; set; }
        public string ApsEndPointUrl { get; set; }
        public string FileNetDocClass { get; set; }
        public string FileNetUsername { get; set; }
        public string FileNetPassword { get; set; }
        public string FileNetClientIdIndexName { get; set; }
        public string FileNetClientIdIndexNameP8 { get; set; } //WA-697 Doug Bruce 2015-05-21 - Add field for P8 so ClientID search will work correctly for all companies.
        public bool SupportsFileNetIndexPolicySymbol { get; set; }
        public bool SupportsFileNetIndexAgencyNumber { get; set; }
        public bool SupportsFileNetIndexSourceSystem { get; set; }
        public int SurveyDaysUntilDue { get; set; }
        public int SurveyNearDue { get; set; }
        public int ReviewDaysUntilDue { get; set; }
        public int ReviewNearDue { get; set; }
        public string EmailFromAddress { get; set; }
        public Guid FeeCompanyUserRoleId { get; set; }
        public Guid? UnderwriterUserRoleId { get; set; }
        public int NextFullReportDaysUntilDue { get; set; }
        public bool AgentDataIsRequied { get; set; }
        public int YearsOfLossHistory { get; set; }
        public bool ImportRateModFactors { get; set; }
        public bool DefaultLocationsToOneSurvey { get; set; }
        public bool OptionToReviewIsVisible { get; set; }
        public bool ImportQuotes { get; set; }
        public string NonRequiredLobHazardGrades { get; set; }
        public bool SicSortedByAlpha { get; set; }
        public bool SupportsOverallGrading { get; set; }
        public bool SupportsCallCount { get; set; }
        public string CallCountLabel { get; set; }
        public bool SupportsSeverityRating { get; set; }
        public bool SupportsSimplifiedAcctMgmt { get; set; }
        public bool RequireVendorCost { get; set; }
        public bool SupportsVendorRecReminder { get; set; }
        public bool SupportsInvoiceFields { get; set; }
        public bool SupportsBusinessCategory { get; set; }
        public bool SupportsNaics { get; set; }
        public bool SupportsPortfolioId { get; set; }
        public bool RequireDocTypeOnCreateSurvey { get; set; }
        public bool RequireCommentOnCreateSurvey { get; set; }
        public bool SupportsSurveysDirectToFieldReps { get; set; }
        public bool SupportsBuildingValuation { get; set; }
        public string CreateSurveyTypeCode { get; set; }
        public bool SupportsDotNumber { get; set; }
        public int? DaysToAutoRefreshSurvey { get; set; }
        public bool RequireSicCode { get; set; }
        public bool RequireDocRemarks { get; set; }
        public bool SupportsDocRemarks { get; set; }
        public bool CreateServicePlansFromProspects { get; set; }
        public bool RequireDocumentUpload { get; set; }
        public bool RequireServiceVisitObjective { get; set; }
        public bool SupportsBuildings { get; set; }
        public bool SupportsNonDisclosure { get; set; }
        public bool RequirePolicyPremium { get; set; }
        public bool DisplayPolicyPremium { get; set; }
        public bool DisplayClaimsByDefault { get; set; }
        public string DefaultInsuredContact { get; set; }
        public string PolicyBranchCodeLabel { get; set; }
        public string BuildingSprinklerSystemLabel { get; set; }
        public bool SupportsExtractableDocuments { get; set; }
        public DateTime? MilestoneDate { get; set; }
        public bool HideServicePlanVisitsByDefault { get; set; }
        public bool AllowUnderwritersToEditRecs { get; set; }
        public bool DisplayRecsOnSurveyScreen { get; set; }
        public bool SupportsQuickSurveyCreation { get; set; }
        public int SurveyDaysCount { get; set; }

        public virtual ICollection<ActivityType> ActivityTypes { get; set; }
        public virtual ICollection<Agency> Agencies { get; set; }
        public virtual ICollection<AssignmentRule> AssignmentRules { get; set; }
        public virtual ICollection<ClientAttribute> ClientAttributes { get; set; }
        public virtual ICollection<CompanyAttribute> CompanyAttributes { get; set; }
        public virtual ICollection<CompanyCounter> CompanyCounters { get; set; }
        public virtual ICollection<CompanyLineOfBusiness> CompanyLineOfBusinesses { get; set; }
        public virtual ICollection<CompanyParameter> CompanyParameters { get; set; }
        public virtual ICollection<CompanyPolicySystem> CompanyPolicySystems_CompanyId { get; set; }
        public virtual ICollection<CompanyPolicySystem> CompanyPolicySystems_PolicySystemCompanyId { get; set; }
        public virtual ICollection<ContactReasonType> ContactReasonTypes { get; set; }
        public virtual ICollection<CoverageName> CoverageNames { get; set; }
        public virtual ICollection<DocType> DocTypes { get; set; }
        public virtual ICollection<DueDateHistory> DueDateHistories { get; set; }
        public virtual ICollection<DynamicField> DynamicFields { get; set; }
        public virtual ICollection<GridColumnFilterCompany> GridColumnFilterCompanies { get; set; }
        public virtual ICollection<Insured> Insureds { get; set; }
        public virtual ICollection<Link> Links { get; set; }
        public virtual ICollection<MergeDocument> MergeDocuments { get; set; }
        public virtual ICollection<PriorityRating> PriorityRatings { get; set; }
        public virtual ICollection<ProfitCenter> ProfitCenters { get; set; }
        public virtual ICollection<RecClassification> RecClassifications { get; set; }
        public virtual ICollection<Recommendation> Recommendations { get; set; }
        public virtual ICollection<RecStatus> RecStatus { get; set; }
        public virtual ICollection<ReviewCategoryType> ReviewCategoryTypes { get; set; }
        public virtual ICollection<ReviewRatingType> ReviewRatingTypes { get; set; }
        public virtual ICollection<ServicePlan> ServicePlans { get; set; }
        public virtual ICollection<ServicePlanAction> ServicePlanActions { get; set; }
        public virtual ICollection<ServicePlanGridColumnFilterCompany> ServicePlanGridColumnFilterCompanies { get; set; }
        public virtual ICollection<ServicePlanType> ServicePlanTypes { get; set; }
        public virtual ICollection<SeverityRating> SeverityRatings { get; set; }
        public virtual ICollection<Survey> Surveys { get; set; }
        public virtual ICollection<SurveyGradingCompany> SurveyGradingCompanies { get; set; }
        public virtual ICollection<SurveyQuality> SurveyQualities { get; set; }
        public virtual ICollection<SurveyReasonType> SurveyReasonTypes { get; set; }
        public virtual ICollection<SurveyReleaseOwnershipType> SurveyReleaseOwnershipTypes { get; set; }
        public virtual ICollection<SurveyReviewQueue> SurveyReviewQueues { get; set; }
        public virtual ICollection<SurveyType> SurveyTypes { get; set; }
        public virtual ICollection<Territory> Territories { get; set; }
        public virtual ICollection<TransactionType> TransactionTypes { get; set; }
        public virtual ICollection<Underwriter> Underwriters { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<WebPageHelpfulHint> WebPageHelpfulHints { get; set; }

        public virtual CreateSurveyType CreateSurveyType { get; set; }

        public Company()
        {
            ActivityTypes = new List<ActivityType>();
            Agencies = new List<Agency>();
            AssignmentRules = new List<AssignmentRule>();
            ClientAttributes = new List<ClientAttribute>();
            CompanyAttributes = new List<CompanyAttribute>();
            CompanyCounters = new List<CompanyCounter>();
            CompanyLineOfBusinesses = new List<CompanyLineOfBusiness>();
            CompanyParameters = new List<CompanyParameter>();
            CompanyPolicySystems_CompanyId = new List<CompanyPolicySystem>();
            CompanyPolicySystems_PolicySystemCompanyId = new List<CompanyPolicySystem>();
            ContactReasonTypes = new List<ContactReasonType>();
            CoverageNames = new List<CoverageName>();
            DocTypes = new List<DocType>();
            DueDateHistories = new List<DueDateHistory>();
            DynamicFields = new List<DynamicField>();
            GridColumnFilterCompanies = new List<GridColumnFilterCompany>();
            Insureds = new List<Insured>();
            Links = new List<Link>();
            MergeDocuments = new List<MergeDocument>();
            PriorityRatings = new List<PriorityRating>();
            ProfitCenters = new List<ProfitCenter>();
            RecClassifications = new List<RecClassification>();
            Recommendations = new List<Recommendation>();
            RecStatus = new List<RecStatus>();
            ReviewCategoryTypes = new List<ReviewCategoryType>();
            ReviewRatingTypes = new List<ReviewRatingType>();
            ServicePlans = new List<ServicePlan>();
            ServicePlanActions = new List<ServicePlanAction>();
            ServicePlanGridColumnFilterCompanies = new List<ServicePlanGridColumnFilterCompany>();
            ServicePlanTypes = new List<ServicePlanType>();
            SeverityRatings = new List<SeverityRating>();
            Surveys = new List<Survey>();
            SurveyGradingCompanies = new List<SurveyGradingCompany>();
            SurveyQualities = new List<SurveyQuality>();
            SurveyReasonTypes = new List<SurveyReasonType>();
            SurveyReleaseOwnershipTypes = new List<SurveyReleaseOwnershipType>();
            SurveyReviewQueues = new List<SurveyReviewQueue>();
            SurveyTypes = new List<SurveyType>();
            Territories = new List<Territory>();
            TransactionTypes = new List<TransactionType>();
            Underwriters = new List<Underwriter>();
            Users = new List<User>();
            UserRoles = new List<UserRole>();
            WebPageHelpfulHints = new List<WebPageHelpfulHint>();
        }
    }

    internal class CompanyConfiguration : EntityTypeConfiguration<Company>
    {
        public CompanyConfiguration()
        {
            ToTable("dbo.Company");
            HasKey(x => x.CompanyId);

            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.CompanyName).HasColumnName("CompanyName").IsRequired().HasMaxLength(50);
            Property(x => x.CompanyAbbreviation).HasColumnName("CompanyAbbreviation").IsRequired().HasMaxLength(10);
            Property(x => x.CompanyLongAbbreviation).HasColumnName("CompanyLongAbbreviation").IsRequired().HasMaxLength(10);
            Property(x => x.CompanyNumber).HasColumnName("CompanyNumber").IsRequired().HasMaxLength(10);
            Property(x => x.CompanyPhone).HasColumnName("CompanyPhone").IsOptional().HasMaxLength(20);
            Property(x => x.CompanyFax).HasColumnName("CompanyFax").IsOptional().HasMaxLength(20);
            Property(x => x.PrimaryDomainName).HasColumnName("PrimaryDomainName").IsRequired().HasMaxLength(20);
            Property(x => x.PolicyStarEndPointUrl).HasColumnName("PolicyStarEndPointUrl").IsOptional().HasMaxLength(500);
            Property(x => x.PdrEndPointUrl).HasColumnName("PDREndPointUrl").IsOptional().HasMaxLength(500);
            Property(x => x.Dsik).HasColumnName("DSIK").IsOptional();
            Property(x => x.ApsEndPointUrl).HasColumnName("APSEndPointUrl").IsOptional().HasMaxLength(500);
            Property(x => x.FileNetDocClass).HasColumnName("FileNetDocClass").IsRequired().HasMaxLength(30);
            Property(x => x.FileNetUsername).HasColumnName("FileNetUsername").IsRequired().HasMaxLength(30);
            Property(x => x.FileNetPassword).HasColumnName("FileNetPassword").IsRequired().HasMaxLength(30);
            Property(x => x.FileNetClientIdIndexName).HasColumnName("FileNetClientIDIndexName").IsRequired().HasMaxLength(30);
            Property(x => x.FileNetClientIdIndexNameP8).HasColumnName("FileNetClientIDIndexNameP8").IsRequired().HasMaxLength(30); //WA-697 Doug Bruce 2015-05-21 - Add field for P8 so ClientID search will work correctly for all companies.
            Property(x => x.SupportsFileNetIndexPolicySymbol).HasColumnName("SupportsFileNetIndexPolicySymbol").IsRequired();
            Property(x => x.SupportsFileNetIndexAgencyNumber).HasColumnName("SupportsFileNetIndexAgencyNumber").IsRequired();
            Property(x => x.SupportsFileNetIndexSourceSystem).HasColumnName("SupportsFileNetIndexSourceSystem").IsRequired();
            Property(x => x.SurveyDaysUntilDue).HasColumnName("SurveyDaysUntilDue").IsRequired();
            Property(x => x.SurveyNearDue).HasColumnName("SurveyNearDue").IsRequired();
            Property(x => x.ReviewDaysUntilDue).HasColumnName("ReviewDaysUntilDue").IsRequired();
            Property(x => x.ReviewNearDue).HasColumnName("ReviewNearDue").IsRequired();
            Property(x => x.EmailFromAddress).HasColumnName("EmailFromAddress").IsRequired().HasMaxLength(100);
            Property(x => x.FeeCompanyUserRoleId).HasColumnName("FeeCompanyUserRoleID").IsRequired();
            Property(x => x.UnderwriterUserRoleId).HasColumnName("UnderwriterUserRoleID").IsOptional();
            Property(x => x.NextFullReportDaysUntilDue).HasColumnName("NextFullReportDaysUntilDue").IsRequired();
            Property(x => x.AgentDataIsRequied).HasColumnName("AgentDataIsRequied").IsRequired();
            Property(x => x.YearsOfLossHistory).HasColumnName("YearsOfLossHistory").IsRequired();
            Property(x => x.ImportRateModFactors).HasColumnName("ImportRateModFactors").IsRequired();
            Property(x => x.DefaultLocationsToOneSurvey).HasColumnName("DefaultLocationsToOneSurvey").IsRequired();
            Property(x => x.OptionToReviewIsVisible).HasColumnName("OptionToReviewIsVisible").IsRequired();
            Property(x => x.ImportQuotes).HasColumnName("ImportQuotes").IsRequired();
            Property(x => x.NonRequiredLobHazardGrades).HasColumnName("NonRequiredLOBHazardGrades").IsOptional().HasMaxLength(50);
            Property(x => x.SicSortedByAlpha).HasColumnName("SICSortedByAlpha").IsRequired();
            Property(x => x.SupportsOverallGrading).HasColumnName("SupportsOverallGrading").IsRequired();
            Property(x => x.SupportsCallCount).HasColumnName("SupportsCallCount").IsRequired();
            Property(x => x.CallCountLabel).HasColumnName("CallCountLabel").IsRequired().HasMaxLength(50);
            Property(x => x.SupportsSeverityRating).HasColumnName("SupportsSeverityRating").IsRequired();
            Property(x => x.SupportsSimplifiedAcctMgmt).HasColumnName("SupportsSimplifiedAcctMgmt").IsRequired();
            Property(x => x.RequireVendorCost).HasColumnName("RequireVendorCost").IsRequired();
            Property(x => x.SupportsVendorRecReminder).HasColumnName("SupportsVendorRecReminder").IsRequired();
            Property(x => x.SupportsInvoiceFields).HasColumnName("SupportsInvoiceFields").IsRequired();
            Property(x => x.SupportsBusinessCategory).HasColumnName("SupportsBusinessCategory").IsRequired();
            Property(x => x.SupportsNaics).HasColumnName("SupportsNAICS").IsRequired();
            Property(x => x.SupportsPortfolioId).HasColumnName("SupportsPortfolioID").IsRequired();
            Property(x => x.RequireDocTypeOnCreateSurvey).HasColumnName("RequireDocTypeOnCreateSurvey").IsRequired();
            Property(x => x.RequireCommentOnCreateSurvey).HasColumnName("RequireCommentOnCreateSurvey").IsRequired();
            Property(x => x.SupportsSurveysDirectToFieldReps).HasColumnName("SupportsSurveysDirectToFieldReps").IsRequired();
            Property(x => x.SupportsBuildingValuation).HasColumnName("SupportsBuildingValuation").IsRequired();
            Property(x => x.CreateSurveyTypeCode).HasColumnName("CreateSurveyTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.SupportsDotNumber).HasColumnName("SupportsDOTNumber").IsRequired();
            Property(x => x.DaysToAutoRefreshSurvey).HasColumnName("DaysToAutoRefreshSurvey").IsOptional();
            Property(x => x.RequireSicCode).HasColumnName("RequireSICCode").IsRequired();
            Property(x => x.RequireDocRemarks).HasColumnName("RequireDocRemarks").IsRequired();
            Property(x => x.SupportsDocRemarks).HasColumnName("SupportsDocRemarks").IsRequired();
            Property(x => x.CreateServicePlansFromProspects).HasColumnName("CreateServicePlansFromProspects").IsRequired();
            Property(x => x.RequireDocumentUpload).HasColumnName("RequireDocumentUpload").IsRequired();
            Property(x => x.RequireServiceVisitObjective).HasColumnName("RequireServiceVisitObjective").IsRequired();
            Property(x => x.SupportsBuildings).HasColumnName("SupportsBuildings").IsRequired();
            Property(x => x.SupportsNonDisclosure).HasColumnName("SupportsNonDisclosure").IsRequired();
            Property(x => x.RequirePolicyPremium).HasColumnName("RequirePolicyPremium").IsRequired();
            Property(x => x.DisplayPolicyPremium).HasColumnName("DisplayPolicyPremium").IsRequired();
            Property(x => x.DisplayClaimsByDefault).HasColumnName("DisplayClaimsByDefault").IsRequired();
            Property(x => x.DefaultInsuredContact).HasColumnName("DefaultInsuredContact").IsOptional().HasMaxLength(225);
            Property(x => x.PolicyBranchCodeLabel).HasColumnName("PolicyBranchCodeLabel").IsRequired().HasMaxLength(30);
            Property(x => x.BuildingSprinklerSystemLabel).HasColumnName("BuildingSprinklerSystemLabel").IsRequired().HasMaxLength(30);
            Property(x => x.SupportsExtractableDocuments).HasColumnName("SupportsExtractableDocuments").IsRequired();
            Property(x => x.MilestoneDate).HasColumnName("MilestoneDate").IsOptional();
            Property(x => x.HideServicePlanVisitsByDefault).HasColumnName("HideServicePlanVisitsByDefault").IsRequired();
            Property(x => x.AllowUnderwritersToEditRecs).HasColumnName("AllowUnderwritersToEditRecs").IsRequired();
            Property(x => x.DisplayRecsOnSurveyScreen).HasColumnName("DisplayRecsOnSurveyScreen").IsRequired();
            Property(x => x.SupportsQuickSurveyCreation).HasColumnName("SupportsQuickSurveyCreation").IsRequired();
            Property(x => x.SurveyDaysCount).HasColumnName("SurveyDaysCount").IsRequired();

            HasRequired(a => a.CreateSurveyType).WithMany(b => b.Companies).HasForeignKey(c => c.CreateSurveyTypeCode);
        }
    }
}
