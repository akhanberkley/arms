using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyMergeDocumentField
    {
        public Guid SurveyId { get; set; }
        public Guid MergeDocumentId { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }

        public virtual MergeDocumentField MergeDocumentField { get; set; }
        public virtual Survey Survey { get; set; }
    }

    internal class SurveyMergeDocumentFieldConfiguration : EntityTypeConfiguration<SurveyMergeDocumentField>
    {
        public SurveyMergeDocumentFieldConfiguration()
        {
            ToTable("dbo.Survey_MergeDocumentField");
            HasKey(x => new { x.SurveyId, x.MergeDocumentId, x.FieldName });

            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.MergeDocumentId).HasColumnName("MergeDocumentID").IsRequired();
            Property(x => x.FieldName).HasColumnName("FieldName").IsRequired().HasMaxLength(500).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.FieldValue).HasColumnName("FieldValue").IsOptional().HasMaxLength(2000);

            HasRequired(a => a.Survey).WithMany(b => b.SurveyMergeDocumentFields).HasForeignKey(c => c.SurveyId);
            HasRequired(a => a.MergeDocumentField).WithMany(b => b.SurveyMergeDocumentFields).HasForeignKey(c => new { c.MergeDocumentId, c.FieldName });
        }
    }
}
