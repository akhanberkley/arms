using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class FilterOperator
    {
        public string FilterOperatorCode { get; set; }
        public string FilterOperatorName { get; set; }
        public bool ForStringsOnly { get; set; }
        public string DisplayName { get; set; }
        public string DisplayShortName { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<FilterCondition> FilterConditions { get; set; }

        public FilterOperator()
        {
            FilterConditions = new List<FilterCondition>();
        }
    }

    internal class FilterOperatorConfiguration : EntityTypeConfiguration<FilterOperator>
    {
        public FilterOperatorConfiguration()
        {
            ToTable("dbo.FilterOperator");
            HasKey(x => x.FilterOperatorCode);

            Property(x => x.FilterOperatorCode).HasColumnName("FilterOperatorCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.FilterOperatorName).HasColumnName("FilterOperatorName").IsRequired().HasMaxLength(50);
            Property(x => x.ForStringsOnly).HasColumnName("ForStringsOnly").IsRequired();
            Property(x => x.DisplayName).HasColumnName("DisplayName").IsRequired().HasMaxLength(50);
            Property(x => x.DisplayShortName).HasColumnName("DisplayShortName").IsRequired().HasMaxLength(20);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
