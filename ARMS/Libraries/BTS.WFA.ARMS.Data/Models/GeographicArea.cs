using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class GeographicArea
    {
        public Guid GeographicAreaId { get; set; }
        public Guid TerritoryId { get; set; }
        public string GeographicUnitCode { get; set; }
        public string AreaValue { get; set; }

        public virtual GeographicUnit GeographicUnit { get; set; }
        public virtual Territory Territory { get; set; }
    }

    internal class GeographicAreaConfiguration : EntityTypeConfiguration<GeographicArea>
    {
        public GeographicAreaConfiguration()
        {
            ToTable("dbo.GeographicArea");
            HasKey(x => x.GeographicAreaId);

            Property(x => x.GeographicAreaId).HasColumnName("GeographicAreaID").IsRequired();
            Property(x => x.TerritoryId).HasColumnName("TerritoryID").IsRequired();
            Property(x => x.GeographicUnitCode).HasColumnName("GeographicUnitCode").IsRequired().HasMaxLength(2);
            Property(x => x.AreaValue).HasColumnName("AreaValue").IsRequired().HasMaxLength(30);

            HasRequired(a => a.Territory).WithMany(b => b.GeographicAreas).HasForeignKey(c => c.TerritoryId);
            HasRequired(a => a.GeographicUnit).WithMany(b => b.GeographicAreas).HasForeignKey(c => c.GeographicUnitCode);
        }
    }
}
