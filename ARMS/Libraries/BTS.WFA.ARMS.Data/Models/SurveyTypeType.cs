using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyTypeType
    {
        public string SurveyTypeTypeCode { get; set; }
        public string SurveyTypeTypeName { get; set; }

        public virtual ICollection<SurveyType> SurveyTypes { get; set; }

        public SurveyTypeType()
        {
            SurveyTypes = new List<SurveyType>();
        }
    }
    internal class SurveyTypeTypeConfiguration : EntityTypeConfiguration<SurveyTypeType>
    {
        public SurveyTypeTypeConfiguration()
        {
            ToTable("dbo.SurveyTypeType");
            HasKey(x => x.SurveyTypeTypeCode);

            Property(x => x.SurveyTypeTypeCode).HasColumnName("SurveyTypeTypeCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SurveyTypeTypeName).HasColumnName("SurveyTypeTypeName").IsRequired().HasMaxLength(50);
        }
    }
}
