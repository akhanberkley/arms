using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanAttribute
    {
        public Guid ServicePlanId { get; set; }
        public Guid CompanyAttributeId { get; set; }
        public string AttributeValue { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }

        public virtual CompanyAttribute CompanyAttribute { get; set; }
        public virtual ServicePlan ServicePlan { get; set; }
    }

    internal class ServicePlanAttributeConfiguration : EntityTypeConfiguration<ServicePlanAttribute>
    {
        public ServicePlanAttributeConfiguration()
        {
            ToTable("dbo.ServicePlanAttribute");
            HasKey(x => new { x.ServicePlanId, x.CompanyAttributeId });

            Property(x => x.ServicePlanId).HasColumnName("ServicePlanID").IsRequired();
            Property(x => x.CompanyAttributeId).HasColumnName("CompanyAttributeID").IsRequired();
            Property(x => x.AttributeValue).HasColumnName("AttributeValue").IsOptional().HasMaxLength(3000);
            Property(x => x.LastModifiedBy).HasColumnName("LastModifiedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastModifiedDate).HasColumnName("LastModifiedDate").IsOptional();

            HasRequired(a => a.ServicePlan).WithMany(b => b.ServicePlanAttributes).HasForeignKey(c => c.ServicePlanId);
            HasRequired(a => a.CompanyAttribute).WithMany(b => b.ServicePlanAttributes).HasForeignKey(c => c.CompanyAttributeId);
        }
    }
}
