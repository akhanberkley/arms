using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyLocationPolicyExclusion
    {
        public Guid SurveyId { get; set; }
        public Guid LocationId { get; set; }
        public Guid PolicyId { get; set; }
        public bool LocationExcluded { get; set; }
        public bool PolicyExcluded { get; set; }
        public bool PolicyViewed { get; set; }
    }

    internal class SurveyLocationPolicyExclusionConfiguration : EntityTypeConfiguration<SurveyLocationPolicyExclusion>
    {
        public SurveyLocationPolicyExclusionConfiguration()
        {
            ToTable("dbo.Survey_LocationPolicyExclusion");
            HasKey(x => new { x.SurveyId, x.LocationId, x.PolicyId });

            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.LocationId).HasColumnName("LocationID").IsRequired();
            Property(x => x.PolicyId).HasColumnName("PolicyID").IsRequired();
            Property(x => x.LocationExcluded).HasColumnName("LocationExcluded").IsRequired();
            Property(x => x.PolicyExcluded).HasColumnName("PolicyExcluded").IsRequired();
            Property(x => x.PolicyViewed).HasColumnName("PolicyViewed").IsRequired();
        }
    }
}
