using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ActivityType
    {
        public Guid ActivityTypeId { get; set; }
        public Guid CompanyId { get; set; }
        public string ActivityTypeName { get; set; }
        public string ActivityTypeLevelCode { get; set; }
        public bool Disabled { get; set; }
        public bool IsProductive { get; set; }
        public int PriorityIndex { get; set; }

        public virtual ICollection<Activity> Activities { get; set; }
        public virtual ICollection<ServicePlanActivity> ServicePlanActivities { get; set; }
        public virtual ICollection<SurveyActivity> SurveyActivities { get; set; }

        public virtual ActivityTypeLevel ActivityTypeLevel { get; set; }
        public virtual Company Company { get; set; }

        public ActivityType()
        {
            Activities = new List<Activity>();
            ServicePlanActivities = new List<ServicePlanActivity>();
            SurveyActivities = new List<SurveyActivity>();
        }
    }

    internal class ActivityTypeConfiguration : EntityTypeConfiguration<ActivityType>
    {
        public ActivityTypeConfiguration()
        {
            ToTable("dbo.ActivityType");
            HasKey(x => x.ActivityTypeId);

            Property(x => x.ActivityTypeId).HasColumnName("ActivityTypeID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.ActivityTypeName).HasColumnName("ActivityTypeName").IsRequired().HasMaxLength(30);
            Property(x => x.ActivityTypeLevelCode).HasColumnName("ActivityTypeLevelCode").IsRequired().HasMaxLength(1);
            Property(x => x.Disabled).HasColumnName("Disabled").IsRequired();
            Property(x => x.IsProductive).HasColumnName("IsProductive").IsRequired();
            Property(x => x.PriorityIndex).HasColumnName("PriorityIndex").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.ActivityTypes).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.ActivityTypeLevel).WithMany(b => b.ActivityTypes).HasForeignKey(c => c.ActivityTypeLevelCode);
        }
    }
}
