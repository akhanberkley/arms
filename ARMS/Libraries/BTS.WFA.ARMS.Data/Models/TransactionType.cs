using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class TransactionType
    {
        public Guid TransactionTypeId { get; set; }
        public Guid CompanyId { get; set; }
        public string TransactionTypeDescription { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<Survey> Surveys { get; set; }

        public virtual Company Company { get; set; }

        public TransactionType()
        {
            Surveys = new List<Survey>();
        }
    }

    internal class TransactionTypeConfiguration : EntityTypeConfiguration<TransactionType>
    {
        public TransactionTypeConfiguration()
        {
            ToTable("dbo.TransactionType");
            HasKey(x => x.TransactionTypeId);

            Property(x => x.TransactionTypeId).HasColumnName("TransactionTypeID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.TransactionTypeDescription).HasColumnName("TransactionTypeDescription").IsRequired().HasMaxLength(50);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.TransactionTypes).HasForeignKey(c => c.CompanyId);
        }
    }
}
