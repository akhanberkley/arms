using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class FilterField
    {
        public string FilterFieldCode { get; set; }
        public string SurveyStatusTypeCode { get; set; }
        public string FilterFieldName { get; set; }
        public string SourceTable { get; set; }
        public string SourceColumn { get; set; }
        public string DotNetDataType { get; set; }
        public bool NullAllowed { get; set; }
        public string ComboEntity { get; set; }
        public string TextField { get; set; }
        public string ValueField { get; set; }
        public string FilterExpression { get; set; }
        public string SortExpression { get; set; }
        public int DisplayOrder { get; set; }
        public int? FlexibleDisplayWidth { get; set; }

        public virtual ICollection<FilterCondition> FilterConditions { get; set; }

        public virtual SurveyStatusType SurveyStatusType { get; set; }

        public FilterField()
        {
            FilterConditions = new List<FilterCondition>();
        }
    }

    internal class FilterFieldConfiguration : EntityTypeConfiguration<FilterField>
    {
        public FilterFieldConfiguration()
        {
            ToTable("dbo.FilterField");
            HasKey(x => x.FilterFieldCode);

            Property(x => x.FilterFieldCode).HasColumnName("FilterFieldCode").IsRequired().HasMaxLength(4).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SurveyStatusTypeCode).HasColumnName("SurveyStatusTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.FilterFieldName).HasColumnName("FilterFieldName").IsRequired().HasMaxLength(50);
            Property(x => x.SourceTable).HasColumnName("SourceTable").IsRequired().HasMaxLength(30);
            Property(x => x.SourceColumn).HasColumnName("SourceColumn").IsRequired().HasMaxLength(30);
            Property(x => x.DotNetDataType).HasColumnName("DotNetDataType").IsRequired().HasMaxLength(10);
            Property(x => x.NullAllowed).HasColumnName("NullAllowed").IsRequired();
            Property(x => x.ComboEntity).HasColumnName("ComboEntity").IsOptional().HasMaxLength(30);
            Property(x => x.TextField).HasColumnName("TextField").IsOptional().HasMaxLength(40);
            Property(x => x.ValueField).HasColumnName("ValueField").IsOptional().HasMaxLength(30);
            Property(x => x.FilterExpression).HasColumnName("FilterExpression").IsOptional().HasMaxLength(100);
            Property(x => x.SortExpression).HasColumnName("SortExpression").IsOptional().HasMaxLength(100);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.FlexibleDisplayWidth).HasColumnName("FlexibleDisplayWidth").IsOptional();

            HasRequired(a => a.SurveyStatusType).WithMany(b => b.FilterFields).HasForeignKey(c => c.SurveyStatusTypeCode);
        }
    }
}
