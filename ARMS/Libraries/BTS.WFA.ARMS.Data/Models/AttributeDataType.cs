using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class AttributeDataType
    {
        public string AttributeDataTypeCode { get; set; }
        public string AttributeDataTypeName { get; set; }

        public virtual ICollection<CompanyAttribute> CompanyAttributes { get; set; }

        public AttributeDataType()
        {
            CompanyAttributes = new List<CompanyAttribute>();
        }
    }

    internal class AttributeDataTypeConfiguration : EntityTypeConfiguration<AttributeDataType>
    {
        public AttributeDataTypeConfiguration()
        {
            ToTable("dbo.AttributeDataType");
            HasKey(x => x.AttributeDataTypeCode);

            Property(x => x.AttributeDataTypeCode).HasColumnName("AttributeDataTypeCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.AttributeDataTypeName).HasColumnName("AttributeDataTypeName").IsRequired().HasMaxLength(20);
        }
    }
}
