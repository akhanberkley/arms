using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanHistory
    {
        public Guid ServicePlanHistoryId { get; set; }
        public Guid ServicePlanId { get; set; }
        public DateTime EntryDate { get; set; }
        public string EntryText { get; set; }

        public virtual ServicePlan ServicePlan { get; set; }
    }

    internal class ServicePlanHistoryConfiguration : EntityTypeConfiguration<ServicePlanHistory>
    {
        public ServicePlanHistoryConfiguration()
        {
            ToTable("dbo.ServicePlanHistory");
            HasKey(x => x.ServicePlanHistoryId);

            Property(x => x.ServicePlanHistoryId).HasColumnName("ServicePlanHistoryID").IsRequired();
            Property(x => x.ServicePlanId).HasColumnName("ServicePlanID").IsRequired();
            Property(x => x.EntryDate).HasColumnName("EntryDate").IsRequired();
            Property(x => x.EntryText).HasColumnName("EntryText").IsRequired().HasMaxLength(8000);

            HasRequired(a => a.ServicePlan).WithMany(b => b.ServicePlanHistories).HasForeignKey(c => c.ServicePlanId);
        }
    }
}
