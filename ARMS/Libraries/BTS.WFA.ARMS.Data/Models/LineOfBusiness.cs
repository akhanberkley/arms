using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class LineOfBusiness
    {
        public string LineOfBusinessCode { get; set; }
        public string LineOfBusinessName { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<CompanyLineOfBusiness> CompanyLineOfBusinesses { get; set; }
        public virtual ICollection<CoverageName> CoverageNames_LineOfBusinessCode { get; set; }
        public virtual ICollection<CoverageName> CoverageNames_SubLineOfBusinessCode { get; set; }
        public virtual ICollection<Policy> Policies { get; set; }

        public LineOfBusiness()
        {
            CompanyLineOfBusinesses = new List<CompanyLineOfBusiness>();
            CoverageNames_LineOfBusinessCode = new List<CoverageName>();
            CoverageNames_SubLineOfBusinessCode = new List<CoverageName>();
            Policies = new List<Policy>();
        }
    }

    internal class LineOfBusinessConfiguration : EntityTypeConfiguration<LineOfBusiness>
    {
        public LineOfBusinessConfiguration()
        {
            ToTable("dbo.LineOfBusiness");
            HasKey(x => x.LineOfBusinessCode);

            Property(x => x.LineOfBusinessCode).HasColumnName("LineOfBusinessCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.LineOfBusinessName).HasColumnName("LineOfBusinessName").IsRequired().HasMaxLength(50);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
