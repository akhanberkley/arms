using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class CoverageDetail
    {
        public Guid CoverageDetailId { get; set; }
        public Guid CoverageId { get; set; }
        public Guid? InsuredId { get; set; }
        public Guid? PolicyId { get; set; }
        public string CoverageDetailValue { get; set; }
        public DateTime DateCreated { get; set; }

        public virtual Coverage Coverage { get; set; }
    }

    internal class CoverageDetailConfiguration : EntityTypeConfiguration<CoverageDetail>
    {
        public CoverageDetailConfiguration()
        {
            ToTable("dbo.CoverageDetail");
            HasKey(x => x.CoverageDetailId);

            Property(x => x.CoverageDetailId).HasColumnName("CoverageDetailID").IsRequired();
            Property(x => x.CoverageId).HasColumnName("CoverageID").IsRequired();
            Property(x => x.InsuredId).HasColumnName("InsuredID").IsRequired();
            Property(x => x.PolicyId).HasColumnName("PolicyID").IsRequired();
            Property(x => x.CoverageDetailValue).HasColumnName("CoverageDetailValue").IsOptional().HasMaxLength(500);
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsRequired();

            HasRequired(a => a.Coverage).WithMany(b => b.CoverageDetails).HasForeignKey(c => c.CoverageId);
        }
    }
}
