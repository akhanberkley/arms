using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class State
    {
        public string StateCode { get; set; }
        public string StateName { get; set; }

        public virtual ICollection<Agency> Agencies { get; set; }
        public virtual ICollection<Insured> Insureds { get; set; }
        public virtual ICollection<Location> Locations { get; set; }
        public virtual ICollection<LocationContact> LocationContacts { get; set; }
        public virtual ICollection<Policy> Policies { get; set; }
        public virtual ICollection<RateModificationFactor> RateModificationFactors { get; set; }
        public virtual ICollection<Zip> Zips { get; set; }

        public State()
        {
            Agencies = new List<Agency>();
            Insureds = new List<Insured>();
            Locations = new List<Location>();
            LocationContacts = new List<LocationContact>();
            Policies = new List<Policy>();
            RateModificationFactors = new List<RateModificationFactor>();
            Zips = new List<Zip>();
        }
    }

    internal class StateConfiguration : EntityTypeConfiguration<State>
    {
        public StateConfiguration()
        {
            ToTable("dbo.State");
            HasKey(x => x.StateCode);

            Property(x => x.StateCode).HasColumnName("StateCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.StateName).HasColumnName("StateName").IsRequired().HasMaxLength(50);
        }
    }
}
