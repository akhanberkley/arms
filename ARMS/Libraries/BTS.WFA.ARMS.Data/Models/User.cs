using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class User
    {
        public Guid UserId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid? ManagerId { get; set; }
        public string DomainName { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Certifications { get; set; }
        public string ProfitCenterCode { get; set; }
        public string EmailAddress { get; set; }
        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string FaxNumber { get; set; }
        public string StreetLine1 { get; set; }
        public string StreetLine2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string ZipCode { get; set; }
        public string TimeZoneCode { get; set; }
        public bool AccountDisabled { get; set; }
        public Guid UserRoleId { get; set; }
        public Guid? PermissionSetId { get; set; }
        public bool IsFeeCompany { get; set; }
        public bool IsUnderwriter { get; set; }
        public string RemoteAgencyNumbers { get; set; }

        public virtual ICollection<Activity> Activities { get; set; }
        public virtual ICollection<ActivityDocument> ActivityDocuments { get; set; }
        public virtual ICollection<DueDateHistory> DueDateHistories_ApprovalByUserId { get; set; }
        public virtual ICollection<DueDateHistory> DueDateHistories_RequestedByUserId { get; set; }
        public virtual ICollection<FeeUser> FeeUsers { get; set; }
        public virtual ICollection<ServicePlan> ServicePlans_AssignedUserId { get; set; }
        public virtual ICollection<ServicePlan> ServicePlans_CreatedByUserId { get; set; }
        public virtual ICollection<ServicePlanActivity> ServicePlanActivities { get; set; }
        public virtual ICollection<ServicePlanDocument> ServicePlanDocuments { get; set; }
        public virtual ICollection<ServicePlanNote> ServicePlanNotes { get; set; }
        public virtual ICollection<Survey> Surveys_AssignedUserId { get; set; }
        public virtual ICollection<Survey> Surveys_ConsultantUserId { get; set; }
        public virtual ICollection<Survey> Surveys_RecommendedUserId { get; set; }
        public virtual ICollection<Survey> Surveys_ReviewerUserId { get; set; }
        public virtual ICollection<Survey> Surveys_CreatedByUserId { get; set; }
        public virtual ICollection<SurveyActivity> SurveyActivities { get; set; }
        public virtual ICollection<SurveyDocument> SurveyDocuments { get; set; }
        public virtual ICollection<SurveyLocationNote> SurveyLocationNotes { get; set; }
        public virtual ICollection<SurveyNote> SurveyNotes { get; set; }
        public virtual ICollection<SurveyReleaseOwnership> SurveyReleaseOwnerships { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<UserSearch> UserSearches { get; set; }
        public virtual ICollection<UserSetting> UserSettings { get; set; }
        public virtual ICollection<UserTerritory> UserTerritories { get; set; }

        public virtual Company Company { get; set; }
        public virtual ProfitCenter ProfitCenter { get; set; }
        public virtual User User_ManagerId { get; set; }
        public virtual UserRole UserRole { get; set; }

        public User()
        {
            Activities = new List<Activity>();
            ActivityDocuments = new List<ActivityDocument>();
            DueDateHistories_ApprovalByUserId = new List<DueDateHistory>();
            DueDateHistories_RequestedByUserId = new List<DueDateHistory>();
            FeeUsers = new List<FeeUser>();
            ServicePlans_AssignedUserId = new List<ServicePlan>();
            ServicePlans_CreatedByUserId = new List<ServicePlan>();
            ServicePlanActivities = new List<ServicePlanActivity>();
            ServicePlanDocuments = new List<ServicePlanDocument>();
            ServicePlanNotes = new List<ServicePlanNote>();
            Surveys_AssignedUserId = new List<Survey>();
            Surveys_ConsultantUserId = new List<Survey>();
            Surveys_RecommendedUserId = new List<Survey>();
            Surveys_ReviewerUserId = new List<Survey>();
            SurveyLocationNotes = new List<SurveyLocationNote>();
            SurveyActivities = new List<SurveyActivity>();
            SurveyDocuments = new List<SurveyDocument>();
            SurveyNotes = new List<SurveyNote>();
            SurveyReleaseOwnerships = new List<SurveyReleaseOwnership>();
            Users = new List<User>();
            UserTerritories = new List<UserTerritory>();
            UserSearches = new List<UserSearch>();
            UserSettings = new List<UserSetting>();
        }
    }

    internal class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("dbo.User");
            HasKey(x => x.UserId);

            Property(x => x.UserId).HasColumnName("UserID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.ManagerId).HasColumnName("ManagerID").IsOptional();
            Property(x => x.DomainName).HasColumnName("DomainName").IsRequired().HasMaxLength(20);
            Property(x => x.Username).HasColumnName("Username").IsRequired().HasMaxLength(30);
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(60);
            Property(x => x.Title).HasColumnName("Title").IsOptional().HasMaxLength(80);
            Property(x => x.Certifications).HasColumnName("Certifications").IsOptional().HasMaxLength(60);
            Property(x => x.ProfitCenterCode).HasColumnName("ProfitCenterCode").IsOptional().HasMaxLength(10);
            Property(x => x.EmailAddress).HasColumnName("EmailAddress").IsRequired().HasMaxLength(100);
            Property(x => x.WorkPhone).HasColumnName("WorkPhone").IsRequired().HasMaxLength(25);
            Property(x => x.HomePhone).HasColumnName("HomePhone").IsOptional().HasMaxLength(25);
            Property(x => x.MobilePhone).HasColumnName("MobilePhone").IsOptional().HasMaxLength(25);
            Property(x => x.FaxNumber).HasColumnName("FaxNumber").IsOptional().HasMaxLength(25);
            Property(x => x.StreetLine1).HasColumnName("StreetLine1").IsOptional().HasMaxLength(100);
            Property(x => x.StreetLine2).HasColumnName("StreetLine2").IsOptional().HasMaxLength(100);
            Property(x => x.City).HasColumnName("City").IsOptional().HasMaxLength(30);
            Property(x => x.StateCode).HasColumnName("StateCode").IsOptional().HasMaxLength(2);
            Property(x => x.ZipCode).HasColumnName("ZipCode").IsOptional().HasMaxLength(11);
            Property(x => x.TimeZoneCode).HasColumnName("TimeZoneCode").IsRequired().HasMaxLength(25);
            Property(x => x.AccountDisabled).HasColumnName("AccountDisabled").IsRequired();
            Property(x => x.UserRoleId).HasColumnName("UserRoleID").IsRequired();
            Property(x => x.PermissionSetId).HasColumnName("PermissionSetID").IsOptional();
            Property(x => x.IsFeeCompany).HasColumnName("IsFeeCompany").IsRequired();
            Property(x => x.IsUnderwriter).HasColumnName("IsUnderwriter").IsRequired();
            Property(x => x.RemoteAgencyNumbers).HasColumnName("RemoteAgencyNumbers").IsOptional().HasMaxLength(100);

            HasRequired(a => a.Company).WithMany(b => b.Users).HasForeignKey(c => c.CompanyId);
            HasOptional(a => a.User_ManagerId).WithMany(b => b.Users).HasForeignKey(c => c.ManagerId);
            HasOptional(a => a.ProfitCenter).WithMany(b => b.Users).HasForeignKey(c => c.ProfitCenterCode);
            HasRequired(a => a.UserRole).WithMany(b => b.Users).HasForeignKey(c => c.UserRoleId);
        }
    }
}
