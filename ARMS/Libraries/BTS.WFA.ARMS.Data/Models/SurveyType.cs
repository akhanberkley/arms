using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyType
    {
        public Guid SurveyTypeId { get; set; }
        public Guid CompanyId { get; set; }
        public string SurveyTypeTypeCode { get; set; }
        public int DisplayOrder { get; set; }
        public string ServiceTypeCode { get; set; }
        public int Setting { get; set; }
        public bool Disabled { get; set; }

        public virtual ICollection<Survey> Surveys_FutureVisitSurveyTypeId { get; set; }
        public virtual ICollection<Survey> Surveys_SurveyTypeId { get; set; }

        public virtual Company Company { get; set; }
        public virtual ServiceType ServiceType { get; set; }
        public virtual SurveyTypeType SurveyTypeType { get; set; }

        public SurveyType()
        {
            Surveys_FutureVisitSurveyTypeId = new List<Survey>();
            Surveys_SurveyTypeId = new List<Survey>();
        }
    }

    internal class SurveyTypeConfiguration : EntityTypeConfiguration<SurveyType>
    {
        public SurveyTypeConfiguration()
        {
            ToTable("dbo.SurveyType");
            HasKey(x => x.SurveyTypeId);

            Property(x => x.SurveyTypeId).HasColumnName("SurveyTypeID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.SurveyTypeTypeCode).HasColumnName("SurveyTypeTypeCode").IsRequired().HasMaxLength(2);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.ServiceTypeCode).HasColumnName("ServiceTypeCode").IsOptional().HasMaxLength(1);
            Property(x => x.Setting).HasColumnName("Setting").IsRequired();
            Property(x => x.Disabled).HasColumnName("Disabled").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.SurveyTypes).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.SurveyTypeType).WithMany(b => b.SurveyTypes).HasForeignKey(c => c.SurveyTypeTypeCode);
            HasOptional(a => a.ServiceType).WithMany(b => b.SurveyTypes).HasForeignKey(c => c.ServiceTypeCode);
        }
    }
}
