using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ContactReasonType
    {
        public string ContactReasonTypeCode { get; set; }
        public Guid CompanyId { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<LocationContact> LocationContacts { get; set; }

        public virtual Company Company { get; set; }

        public ContactReasonType()
        {
            LocationContacts = new List<LocationContact>();
        }
    }

    internal class ContactReasonTypeConfiguration : EntityTypeConfiguration<ContactReasonType>
    {
        public ContactReasonTypeConfiguration()
        {
            ToTable("dbo.ContactReasonType");
            HasKey(x => x.ContactReasonTypeCode);

            Property(x => x.ContactReasonTypeCode).HasColumnName("ContactReasonTypeCode").IsRequired().HasMaxLength(20).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.ShortDescription).HasColumnName("ShortDescription").IsRequired().HasMaxLength(50);
            Property(x => x.LongDescription).HasColumnName("LongDescription").IsRequired().HasMaxLength(200);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.ContactReasonTypes).HasForeignKey(c => c.CompanyId);
        }
    }
}
