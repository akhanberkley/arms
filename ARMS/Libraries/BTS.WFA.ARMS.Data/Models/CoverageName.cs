using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class CoverageName
    {
        public Guid CoverageNameId { get; set; }
        public Guid CompanyId { get; set; }
        public string LineOfBusinessCode { get; set; }
        public string SubLineOfBusinessCode { get; set; }
        public string CoverageNameTypeCode { get; set; }
        public bool IsRequired { get; set; }
        public bool MigrationOnly { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<Coverage> Coverages { get; set; }
        public virtual ICollection<SurveyLocationPolicyCoverageName> SurveyLocationPolicyCoverageNames { get; set; }
        public virtual ICollection<SurveyPolicyCoverage> SurveyPolicyCoverages { get; set; }

        public virtual Company Company { get; set; }
        public virtual CoverageNameType CoverageNameType { get; set; }
        public virtual LineOfBusiness LineOfBusiness_LineOfBusinessCode { get; set; }
        public virtual LineOfBusiness LineOfBusiness_SubLineOfBusinessCode { get; set; }

        public CoverageName()
        {
            Coverages = new List<Coverage>();
            SurveyLocationPolicyCoverageNames = new List<SurveyLocationPolicyCoverageName>();
            SurveyPolicyCoverages = new List<SurveyPolicyCoverage>();
        }
    }

    internal class CoverageNameConfiguration : EntityTypeConfiguration<CoverageName>
    {
        public CoverageNameConfiguration()
        {
            ToTable("dbo.CoverageName");
            HasKey(x => x.CoverageNameId);

            Property(x => x.CoverageNameId).HasColumnName("CoverageNameID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.LineOfBusinessCode).HasColumnName("LineOfBusinessCode").IsRequired().HasMaxLength(2);
            Property(x => x.SubLineOfBusinessCode).HasColumnName("SubLineOfBusinessCode").IsOptional().HasMaxLength(2);
            Property(x => x.CoverageNameTypeCode).HasColumnName("CoverageNameTypeCode").IsRequired().HasMaxLength(10);
            Property(x => x.IsRequired).HasColumnName("IsRequired").IsRequired();
            Property(x => x.MigrationOnly).HasColumnName("MigrationOnly").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.CoverageNames).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.LineOfBusiness_LineOfBusinessCode).WithMany(b => b.CoverageNames_LineOfBusinessCode).HasForeignKey(c => c.LineOfBusinessCode);
            HasOptional(a => a.LineOfBusiness_SubLineOfBusinessCode).WithMany(b => b.CoverageNames_SubLineOfBusinessCode).HasForeignKey(c => c.SubLineOfBusinessCode);
            HasRequired(a => a.CoverageNameType).WithMany(b => b.CoverageNames).HasForeignKey(c => c.CoverageNameTypeCode);
        }
    }
}
