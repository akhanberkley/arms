using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanActionType
    {
        public string ServicePlanActionTypeCode { get; set; }
        public string ActionName { get; set; }

        public virtual ICollection<ServicePlanAction> ServicePlanActions { get; set; }

        public ServicePlanActionType()
        {
            ServicePlanActions = new List<ServicePlanAction>();
        }
    }

    internal class ServicePlanActionTypeConfiguration : EntityTypeConfiguration<ServicePlanActionType>
    {
        public ServicePlanActionTypeConfiguration()
        {
            ToTable("dbo.ServicePlanActionType");
            HasKey(x => x.ServicePlanActionTypeCode);

            Property(x => x.ServicePlanActionTypeCode).HasColumnName("ServicePlanActionTypeCode").IsRequired().HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ActionName).HasColumnName("ActionName").IsRequired().HasMaxLength(50);
        }
    }
}
