using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class MergeDocumentVersion
    {
        public Guid MergeDocumentVersionId { get; set; }
        public Guid MergeDocumentId { get; set; }
        public int VersionNumber { get; set; }
        public bool IsActive { get; set; }
        public string DocumentName { get; set; }
        public string MimeType { get; set; }
        public long SizeInBytes { get; set; }
        public string FileNetReference { get; set; }
        public DateTime UploadedOn { get; set; }

        public virtual MergeDocument MergeDocument { get; set; }
    }

    internal class MergeDocumentVersionConfiguration : EntityTypeConfiguration<MergeDocumentVersion>
    {
        public MergeDocumentVersionConfiguration()
        {
            ToTable("dbo.MergeDocumentVersion");
            HasKey(x => x.MergeDocumentVersionId);

            Property(x => x.MergeDocumentVersionId).HasColumnName("MergeDocumentVersionID").IsRequired();
            Property(x => x.MergeDocumentId).HasColumnName("MergeDocumentID").IsRequired();
            Property(x => x.VersionNumber).HasColumnName("VersionNumber").IsRequired();
            Property(x => x.IsActive).HasColumnName("IsActive").IsRequired();
            Property(x => x.DocumentName).HasColumnName("DocumentName").IsRequired().HasMaxLength(100);
            Property(x => x.MimeType).HasColumnName("MimeType").IsRequired().HasMaxLength(100);
            Property(x => x.SizeInBytes).HasColumnName("SizeInBytes").IsRequired();
            Property(x => x.FileNetReference).HasColumnName("FileNetReference").IsRequired().HasMaxLength(100);
            Property(x => x.UploadedOn).HasColumnName("UploadedOn").IsRequired();

            HasRequired(a => a.MergeDocument).WithMany(b => b.MergeDocumentVersions).HasForeignKey(c => c.MergeDocumentId);
        }
    }
}
