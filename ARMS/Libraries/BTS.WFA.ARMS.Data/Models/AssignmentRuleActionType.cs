using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class AssignmentRuleActionType
    {
        public string AssignmentRuleActionTypeCode { get; set; }
        public string AssignmentRuleActionTypeName { get; set; }

        public virtual ICollection<AssignmentRuleAction> AssignmentRuleActions { get; set; }

        public AssignmentRuleActionType()
        {
            AssignmentRuleActions = new List<AssignmentRuleAction>();
        }
    }

    internal class AssignmentRuleActionTypeConfiguration : EntityTypeConfiguration<AssignmentRuleActionType>
    {
        public AssignmentRuleActionTypeConfiguration()
        {
            ToTable("dbo.AssignmentRuleActionType");
            HasKey(x => x.AssignmentRuleActionTypeCode);

            Property(x => x.AssignmentRuleActionTypeCode).HasColumnName("AssignmentRuleActionTypeCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.AssignmentRuleActionTypeName).HasColumnName("AssignmentRuleActionTypeName").IsRequired().HasMaxLength(8);
        }
    }
}
