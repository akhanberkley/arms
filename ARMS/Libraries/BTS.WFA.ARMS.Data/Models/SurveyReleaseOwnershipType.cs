using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyReleaseOwnershipType
    {
        public Guid SurveyReleaseOwnershipTypeId { get; set; }
        public Guid CompanyId { get; set; }
        public string SurveyReleaseOwnershipTypeName { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<SurveyReleaseOwnership> SurveyReleaseOwnerships { get; set; }

        public virtual Company Company { get; set; }

        public SurveyReleaseOwnershipType()
        {
            SurveyReleaseOwnerships = new List<SurveyReleaseOwnership>();
        }
    }

    internal class SurveyReleaseOwnershipTypeConfiguration : EntityTypeConfiguration<SurveyReleaseOwnershipType>
    {
        public SurveyReleaseOwnershipTypeConfiguration()
        {
            ToTable("dbo.SurveyReleaseOwnershipType");
            HasKey(x => x.SurveyReleaseOwnershipTypeId);

            Property(x => x.SurveyReleaseOwnershipTypeId).HasColumnName("SurveyReleaseOwnershipTypeID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.SurveyReleaseOwnershipTypeName).HasColumnName("SurveyReleaseOwnershipTypeName").IsRequired().HasMaxLength(100);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.SurveyReleaseOwnershipTypes).HasForeignKey(c => c.CompanyId);
        }
    }
}
