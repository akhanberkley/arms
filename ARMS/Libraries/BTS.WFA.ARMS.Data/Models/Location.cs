using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Location
    {
        public Guid LocationId { get; set; }
        public Guid? InsuredId { get; set; }
        public string PolicySystemKey { get; set; }
        public string PolicySymbol { get; set; }
        public int LocationNumber { get; set; }
        public string LocationNumberForDisplay { get; set; }
        public string StreetLine1 { get; set; }
        public string StreetLine2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string ZipCode { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<Building> Buildings { get; set; }
        public virtual ICollection<Survey> Surveys { get; set; }
        public virtual ICollection<SurveyLocationNote> SurveyLocationNotes { get; set; }
        public virtual ICollection<SurveyLocationPolicyCoverageName> SurveyLocationPolicyCoverageNames { get; set; }
        public virtual ICollection<SurveyPolicyCoverage> SurveyPolicyCoverages { get; set; }
        public virtual ICollection<LocationContact> LocationContacts { get; set; }

        public virtual Insured Insured { get; set; }
        public virtual State State { get; set; }

        public Location()
        {
            Buildings = new List<Building>();
            Surveys = new List<Survey>();
            SurveyLocationNotes = new List<SurveyLocationNote>();
            SurveyLocationPolicyCoverageNames = new List<SurveyLocationPolicyCoverageName>();
            SurveyPolicyCoverages = new List<SurveyPolicyCoverage>();
            LocationContacts = new List<LocationContact>();
        }

        /// <summary>
        /// Gets or sets the public protection for the location that will apply to the buildings at that location.
        /// </summary>
        private int _publicProtection = Int32.MinValue;
        [NotMapped]
        public int PublicProtection
        {
            get
            {
                return _publicProtection;
            }
            set
            {
                _publicProtection = value;
            }
        }

        /// <summary>
        /// Gets or sets the has sprinklery system flag for the location that will apply to the buildings at that location.
        /// </summary>
        private short _hasSprinklerSystem = Int16.MinValue;
        [NotMapped]
        public short HasSprinklerSystem
        {
            get
            {
                return _hasSprinklerSystem;
            }
            set
            {
                _hasSprinklerSystem = value;
            }
        }
    }

    internal class LocationConfiguration : EntityTypeConfiguration<Location>
    {
        public LocationConfiguration()
        {
            ToTable("dbo.Location");
            HasKey(x => x.LocationId);

            Property(x => x.LocationId).HasColumnName("LocationID").IsRequired();
            Property(x => x.InsuredId).HasColumnName("InsuredID").IsOptional();
            Property(x => x.PolicySystemKey).HasColumnName("PolicySystemKey").IsOptional().HasMaxLength(50);
            Property(x => x.PolicySymbol).HasColumnName("PolicySymbol").IsOptional().HasMaxLength(3);
            Property(x => x.LocationNumber).HasColumnName("LocationNumber").IsOptional();
            Property(x => x.LocationNumberForDisplay).HasColumnName("LocationNumberForDisplay").IsOptional().HasMaxLength(20);
            Property(x => x.StreetLine1).HasColumnName("StreetLine1").IsOptional().HasMaxLength(200);
            Property(x => x.StreetLine2).HasColumnName("StreetLine2").IsOptional().HasMaxLength(150);
            Property(x => x.City).HasColumnName("City").IsOptional().HasMaxLength(30);
            Property(x => x.StateCode).HasColumnName("StateCode").IsOptional().HasMaxLength(2);
            Property(x => x.ZipCode).HasColumnName("ZipCode").IsOptional().HasMaxLength(11);
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasMaxLength(300);
            Property(x => x.IsActive).HasColumnName("IsActive").IsRequired();

            HasOptional(a => a.Insured).WithMany(b => b.Locations).HasForeignKey(c => c.InsuredId);
            HasOptional(a => a.State).WithMany(b => b.Locations).HasForeignKey(c => c.StateCode);
        }
    }
}
