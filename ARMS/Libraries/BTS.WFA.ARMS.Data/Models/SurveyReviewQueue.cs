using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyReviewQueue
    {
        public Guid SurveyReviewQueueId { get; set; }
        public Guid CompanyId { get; set; }
        public string SurveyReviewQueueName { get; set; }
        public string CountQuery { get; set; }
        public string ResultSetQuery { get; set; }
        public int DisplayOrder { get; set; }

        public virtual Company Company { get; set; }
    }

    internal class SurveyReviewQueueConfiguration : EntityTypeConfiguration<SurveyReviewQueue>
    {
        public SurveyReviewQueueConfiguration()
        {
            ToTable("dbo.SurveyReviewQueue");
            HasKey(x => x.SurveyReviewQueueId);

            Property(x => x.SurveyReviewQueueId).HasColumnName("SurveyReviewQueueID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.SurveyReviewQueueName).HasColumnName("SurveyReviewQueueName").IsOptional().HasMaxLength(100);
            Property(x => x.CountQuery).HasColumnName("CountQuery").IsOptional().HasMaxLength(1000);
            Property(x => x.ResultSetQuery).HasColumnName("ResultSetQuery").IsOptional().HasMaxLength(1000);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.SurveyReviewQueues).HasForeignKey(c => c.CompanyId);
        }
    }
}
