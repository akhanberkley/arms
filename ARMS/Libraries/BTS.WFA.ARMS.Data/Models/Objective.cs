using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Objective
    {
        public Guid ObjectiveId { get; set; }
        public int ObjectiveNumber { get; set; }
        public Guid? SurveyId { get; set; }
        public string ObjectiveText { get; set; }
        public string CommentsText { get; set; }
        public DateTime TargetDate { get; set; }
        public string ObjectiveStatusCode { get; set; }
        public Guid? ServicePlanId { get; set; }

        public virtual ICollection<ObjectiveAttribute> ObjectiveAttributes { get; set; }

        public virtual ObjectiveStatus ObjectiveStatus { get; set; }
        public virtual ServicePlan ServicePlan { get; set; }
        public virtual Survey Survey { get; set; }

        public Objective()
        {
            ObjectiveAttributes = new List<ObjectiveAttribute>();
        }
    }

    internal class ObjectiveConfiguration : EntityTypeConfiguration<Objective>
    {
        public ObjectiveConfiguration()
        {
            ToTable("dbo.Objective");
            HasKey(x => x.ObjectiveId);

            Property(x => x.ObjectiveId).HasColumnName("ObjectiveID").IsRequired();
            Property(x => x.ObjectiveNumber).HasColumnName("ObjectiveNumber").IsRequired();
            Property(x => x.SurveyId).HasColumnName("SurveyID").IsOptional();
            Property(x => x.ObjectiveText).HasColumnName("ObjectiveText").IsRequired().HasMaxLength(2000);
            Property(x => x.CommentsText).HasColumnName("CommentsText").IsOptional().HasMaxLength(2000);
            Property(x => x.TargetDate).HasColumnName("TargetDate").IsRequired();
            Property(x => x.ObjectiveStatusCode).HasColumnName("ObjectiveStatusCode").IsRequired().HasMaxLength(1);
            Property(x => x.ServicePlanId).HasColumnName("ServicePlanID").IsOptional();

            HasOptional(a => a.Survey).WithMany(b => b.Objectives).HasForeignKey(c => c.SurveyId);
            HasRequired(a => a.ObjectiveStatus).WithMany(b => b.Objectives).HasForeignKey(c => c.ObjectiveStatusCode);
            HasOptional(a => a.ServicePlan).WithMany(b => b.Objectives).HasForeignKey(c => c.ServicePlanId);
        }
    }
}
