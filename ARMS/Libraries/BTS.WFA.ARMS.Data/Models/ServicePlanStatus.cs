using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanStatus
    {
        public string ServicePlanStatusCode { get; set; }
        public string ServicePlanStatusName { get; set; }
        public string DisplayOrder { get; set; }

        public virtual ICollection<ServicePlan> ServicePlans { get; set; }
        public virtual ICollection<ServicePlanAction> ServicePlanActions { get; set; }

        public ServicePlanStatus()
        {
            ServicePlans = new List<ServicePlan>();
            ServicePlanActions = new List<ServicePlanAction>();
        }
    }

    internal class ServicePlanStatusConfiguration : EntityTypeConfiguration<ServicePlanStatus>
    {
        public ServicePlanStatusConfiguration()
        {
            ToTable("dbo.ServicePlanStatus");
            HasKey(x => x.ServicePlanStatusCode);

            Property(x => x.ServicePlanStatusCode).HasColumnName("ServicePlanStatusCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ServicePlanStatusName).HasColumnName("ServicePlanStatusName").IsRequired().HasMaxLength(30);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired().HasMaxLength(10);
        }
    }
}
