using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class CompanyAttribute
    {
        public Guid CompanyAttributeId { get; set; }
        public Guid CompanyId { get; set; }
        public string AttributeDataTypeCode { get; set; }
        public string EntityTypeCode { get; set; }
        public string Label { get; set; }
        public bool IsRequired { get; set; }
        public int DisplayOrder { get; set; }
        public string ControlAttributes { get; set; }
        public int? ControlColumnCount { get; set; }
        public int? ControlRowCount { get; set; }
        public bool DisplayInGrid { get; set; }
        public int? MinWidthInGrid { get; set; }
        public string MappedField { get; set; }
        public bool RequireOnCompletion { get; set; }
        public DateTime? ExpirationDate { get; set; }

        public virtual ICollection<ObjectiveAttribute> ObjectiveAttributes { get; set; }
        public virtual ICollection<ServicePlanAttribute> ServicePlanAttributes { get; set; }
        public virtual ICollection<SurveyAttribute> SurveyAttributes { get; set; }

        public virtual AttributeDataType AttributeDataType { get; set; }
        public virtual Company Company { get; set; }
        public virtual EntityType EntityType { get; set; }

        public CompanyAttribute()
        {
            ObjectiveAttributes = new List<ObjectiveAttribute>();
            ServicePlanAttributes = new List<ServicePlanAttribute>();
            SurveyAttributes = new List<SurveyAttribute>();
        }
    }

    internal class CompanyAttributeConfiguration : EntityTypeConfiguration<CompanyAttribute>
    {
        public CompanyAttributeConfiguration()
        {
            ToTable("dbo.CompanyAttribute");
            HasKey(x => x.CompanyAttributeId);

            Property(x => x.CompanyAttributeId).HasColumnName("CompanyAttributeID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.AttributeDataTypeCode).HasColumnName("AttributeDataTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.EntityTypeCode).HasColumnName("EntityTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.Label).HasColumnName("Label").IsRequired().HasMaxLength(100);
            Property(x => x.IsRequired).HasColumnName("IsRequired").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.ControlAttributes).HasColumnName("ControlAttributes").IsOptional().HasMaxLength(200);
            Property(x => x.ControlColumnCount).HasColumnName("ControlColumnCount").IsOptional();
            Property(x => x.ControlRowCount).HasColumnName("ControlRowCount").IsOptional();
            Property(x => x.DisplayInGrid).HasColumnName("DisplayInGrid").IsRequired();
            Property(x => x.MinWidthInGrid).HasColumnName("MinWidthInGrid").IsOptional();
            Property(x => x.MappedField).HasColumnName("MappedField").IsOptional().HasMaxLength(70);
            Property(x => x.RequireOnCompletion).HasColumnName("RequireOnCompletion").IsRequired();
            Property(x => x.ExpirationDate).HasColumnName("ExpirationDate").IsOptional();

            HasRequired(a => a.Company).WithMany(b => b.CompanyAttributes).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.AttributeDataType).WithMany(b => b.CompanyAttributes).HasForeignKey(c => c.AttributeDataTypeCode);
            HasRequired(a => a.EntityType).WithMany(b => b.CompanyAttributes).HasForeignKey(c => c.EntityTypeCode);
        }
    }
}
