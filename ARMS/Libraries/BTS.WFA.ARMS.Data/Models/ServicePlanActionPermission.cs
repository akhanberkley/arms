using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanActionPermission
    {
        public Guid ServicePlanActionPermissionId { get; set; }
        public Guid PermissionSetId { get; set; }
        public Guid ServicePlanActionId { get; set; }
        public int Setting { get; set; }

        public virtual PermissionSet PermissionSet { get; set; }
        public virtual ServicePlanAction ServicePlanAction { get; set; }
    }

    internal class ServicePlanActionPermissionConfiguration : EntityTypeConfiguration<ServicePlanActionPermission>
    {
        public ServicePlanActionPermissionConfiguration()
        {
            ToTable("dbo.ServicePlanActionPermission");
            HasKey(x => x.ServicePlanActionPermissionId);

            Property(x => x.ServicePlanActionPermissionId).HasColumnName("ServicePlanActionPermissionID").IsRequired();
            Property(x => x.PermissionSetId).HasColumnName("PermissionSetID").IsRequired();
            Property(x => x.ServicePlanActionId).HasColumnName("ServicePlanActionID").IsRequired();
            Property(x => x.Setting).HasColumnName("Setting").IsRequired();

            HasRequired(a => a.PermissionSet).WithMany(b => b.ServicePlanActionPermissions).HasForeignKey(c => c.PermissionSetId);
            HasRequired(a => a.ServicePlanAction).WithMany(b => b.ServicePlanActionPermissions).HasForeignKey(c => c.ServicePlanActionId);
        }
    }
}
