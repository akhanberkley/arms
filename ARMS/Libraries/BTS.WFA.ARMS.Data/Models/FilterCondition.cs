using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class FilterCondition
    {
        public Guid FilterConditionId { get; set; }
        public Guid FilterId { get; set; }
        public string FilterFieldCode { get; set; }
        public string FilterOperatorCode { get; set; }
        public string CompareValue { get; set; }
        public string DisplayValue { get; set; }
        public int OrderIndex { get; set; }

        public virtual Filter Filter { get; set; }
        public virtual FilterField FilterField { get; set; }
        public virtual FilterOperator FilterOperator { get; set; }
    }

    internal class FilterConditionConfiguration : EntityTypeConfiguration<FilterCondition>
    {
        public FilterConditionConfiguration()
        {
            ToTable("dbo.FilterCondition");
            HasKey(x => x.FilterConditionId);

            Property(x => x.FilterConditionId).HasColumnName("FilterConditionID").IsRequired();
            Property(x => x.FilterId).HasColumnName("FilterID").IsRequired();
            Property(x => x.FilterFieldCode).HasColumnName("FilterFieldCode").IsRequired().HasMaxLength(4);
            Property(x => x.FilterOperatorCode).HasColumnName("FilterOperatorCode").IsRequired().HasMaxLength(2);
            Property(x => x.CompareValue).HasColumnName("CompareValue").IsOptional().HasMaxLength(100);
            Property(x => x.DisplayValue).HasColumnName("DisplayValue").IsRequired().HasMaxLength(100);
            Property(x => x.OrderIndex).HasColumnName("OrderIndex").IsRequired();

            HasRequired(a => a.Filter).WithMany(b => b.FilterConditions).HasForeignKey(c => c.FilterId);
            HasRequired(a => a.FilterField).WithMany(b => b.FilterConditions).HasForeignKey(c => c.FilterFieldCode);
            HasRequired(a => a.FilterOperator).WithMany(b => b.FilterConditions).HasForeignKey(c => c.FilterOperatorCode);
        }
    }
}
