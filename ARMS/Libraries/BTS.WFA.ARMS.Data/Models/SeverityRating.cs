using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SeverityRating
    {
        public string SeverityRatingCode { get; set; }
        public Guid CompanyId { get; set; }
        public string SeverityRatingName { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<Survey> Surveys { get; set; }

        public virtual Company Company { get; set; }

        public SeverityRating()
        {
            Surveys = new List<Survey>();
        }
    }

    internal class SeverityRatingConfiguration : EntityTypeConfiguration<SeverityRating>
    {
        public SeverityRatingConfiguration()
        {
            ToTable("dbo.SeverityRating");
            HasKey(x => x.SeverityRatingCode);

            Property(x => x.SeverityRatingCode).HasColumnName("SeverityRatingCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.SeverityRatingName).HasColumnName("SeverityRatingName").IsRequired().HasMaxLength(50);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.SeverityRatings).HasForeignKey(c => c.CompanyId);
        }
    }
}
