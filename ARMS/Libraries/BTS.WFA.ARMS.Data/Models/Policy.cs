using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Policy
    {
        public Guid PolicyId { get; set; }
        public Guid InsuredId { get; set; }
        public string PolicyNumber { get; set; }
        public int PolicyMod { get; set; }
        public string PolicySymbol { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public string LineOfBusinessCode { get; set; }
        public decimal? Premium { get; set; }
        public string Carrier { get; set; }
        public string HazardGrade { get; set; }
        public string BranchCode { get; set; }
        public string ProfitCenterCode { get; set; }
        public string RatingCompany { get; set; }
        public string PolicyStateCode { get; set; }
        public bool IsActive { get; set; }
        [NotMapped]
        public string PolicySystemKey { get; set; }

        public virtual ICollection<Claim> Claims { get; set; }
        public virtual ICollection<Coverage> Coverages { get; set; }
        public virtual ICollection<RateModificationFactor> RateModificationFactors { get; set; }
        public virtual ICollection<ServicePlan> ServicePlans { get; set; }
        public virtual ICollection<Survey> Surveys { get; set; }
        public virtual ICollection<SurveyLocationPolicyCoverageName> SurveyLocationPolicyCoverageNames { get; set; }
        public virtual ICollection<SurveyPolicyCoverage> SurveyPolicyCoverages { get; set; }

        public virtual Insured Insured { get; set; }
        public virtual LineOfBusiness LineOfBusiness { get; set; }
        public virtual State State { get; set; }
        public virtual ProfitCenter ProfitCenter { get; set; }

        public Policy()
        {
            Claims = new List<Claim>();
            Coverages = new List<Coverage>();
            RateModificationFactors = new List<RateModificationFactor>();
            ServicePlans = new List<ServicePlan>();
            Surveys = new List<Survey>();
            SurveyLocationPolicyCoverageNames = new List<SurveyLocationPolicyCoverageName>();
            SurveyPolicyCoverages = new List<SurveyPolicyCoverage>();
        }
    }

    internal class PolicyConfiguration : EntityTypeConfiguration<Policy>
    {
        public PolicyConfiguration()
        {
            ToTable("dbo.Policy");
            HasKey(x => x.PolicyId);

            Property(x => x.PolicyId).HasColumnName("PolicyID").IsRequired();
            Property(x => x.InsuredId).HasColumnName("InsuredID").IsRequired();
            Property(x => x.PolicyNumber).HasColumnName("PolicyNumber").IsOptional().HasMaxLength(50);
            Property(x => x.PolicyMod).HasColumnName("PolicyMod").IsOptional();
            Property(x => x.PolicySymbol).HasColumnName("PolicySymbol").IsOptional().HasMaxLength(3);
            Property(x => x.EffectiveDate).HasColumnName("EffectiveDate").IsOptional();
            Property(x => x.ExpireDate).HasColumnName("ExpireDate").IsOptional();
            Property(x => x.LineOfBusinessCode).HasColumnName("LineOfBusinessCode").IsOptional().HasMaxLength(2);
            Property(x => x.Premium).HasColumnName("Premium").IsOptional().HasPrecision(19, 4);
            Property(x => x.Carrier).HasColumnName("Carrier").IsOptional().HasMaxLength(100);
            Property(x => x.HazardGrade).HasColumnName("HazardGrade").IsOptional().HasMaxLength(50);
            Property(x => x.BranchCode).HasColumnName("BranchCode").IsOptional().HasMaxLength(50);
            Property(x => x.ProfitCenterCode).HasColumnName("ProfitCenter").IsOptional().HasMaxLength(50);
            Property(x => x.RatingCompany).HasColumnName("RatingCompany").IsOptional().HasMaxLength(50);
            Property(x => x.PolicyStateCode).HasColumnName("PolicyStateCode").IsOptional().HasMaxLength(2);
            Property(x => x.IsActive).HasColumnName("IsActive").IsRequired();

            HasRequired(a => a.Insured).WithMany(b => b.Policies).HasForeignKey(c => c.InsuredId);
            HasOptional(a => a.LineOfBusiness).WithMany(b => b.Policies).HasForeignKey(c => c.LineOfBusinessCode);
            HasOptional(a => a.State).WithMany(b => b.Policies).HasForeignKey(c => c.PolicyStateCode);
            HasOptional(a => a.ProfitCenter).WithMany().HasForeignKey(c => c.ProfitCenterCode);
        }
    }
}
