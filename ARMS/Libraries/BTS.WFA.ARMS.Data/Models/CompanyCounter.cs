using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class CompanyCounter
    {
        public Guid CompanyCounterId { get; set; }
        public Guid CompanyId { get; set; }
        public string CompanyCounterTypeCode { get; set; }
        public int CurrentValue { get; set; }
        public int? MaxValue { get; set; }

        public virtual Company Company { get; set; }
        public virtual CompanyCounterType CompanyCounterType { get; set; }
    }

    internal class CompanyCounterConfiguration : EntityTypeConfiguration<CompanyCounter>
    {
        public CompanyCounterConfiguration()
        {
            ToTable("dbo.CompanyCounter");
            HasKey(x => x.CompanyCounterId);

            Property(x => x.CompanyCounterId).HasColumnName("CompanyCounterID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.CompanyCounterTypeCode).HasColumnName("CompanyCounterTypeCode").IsRequired().HasMaxLength(2);
            Property(x => x.CurrentValue).HasColumnName("CurrentValue").IsRequired();
            Property(x => x.MaxValue).HasColumnName("MaxValue").IsOptional();

            HasRequired(a => a.Company).WithMany(b => b.CompanyCounters).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.CompanyCounterType).WithMany(b => b.CompanyCounters).HasForeignKey(c => c.CompanyCounterTypeCode);
        }
    }
}
