using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class UserTerritory
    {
        public Guid UserId { get; set; }
        public Guid TerritoryId { get; set; }
        public int? PercentAllocated { get; set; }
        public decimal? MinPremiumAmount { get; set; }
        public decimal? MaxPremiumAmount { get; set; }

        public virtual Territory Territory { get; set; }
        public virtual User User { get; set; }
    }

    internal class UserTerritoryConfiguration : EntityTypeConfiguration<UserTerritory>
    {
        public UserTerritoryConfiguration()
        {
            ToTable("dbo.User_Territory");
            HasKey(x => new { x.UserId, x.TerritoryId });

            Property(x => x.UserId).HasColumnName("UserID").IsRequired();
            Property(x => x.TerritoryId).HasColumnName("TerritoryID").IsRequired();
            Property(x => x.PercentAllocated).HasColumnName("PercentAllocated").IsOptional();
            Property(x => x.MinPremiumAmount).HasColumnName("MinPremiumAmount").IsOptional().HasPrecision(19, 4);
            Property(x => x.MaxPremiumAmount).HasColumnName("MaxPremiumAmount").IsOptional().HasPrecision(19, 4);

            HasRequired(a => a.User).WithMany(b => b.UserTerritories).HasForeignKey(c => c.UserId);
            HasRequired(a => a.Territory).WithMany(b => b.UserTerritories).HasForeignKey(c => c.TerritoryId);
        }
    }
}
