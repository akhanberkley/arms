using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class RecClassification
    {
        public Guid RecClassificationId { get; set; }
        public Guid CompanyId { get; set; }
        public string RecClassificationTypeCode { get; set; }
        public bool RequiredOnSurveyCompletion { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<SurveyRecommendation> SurveyRecommendations { get; set; }

        public virtual Company Company { get; set; }
        public virtual RecClassificationType RecClassificationType { get; set; }

        public RecClassification()
        {
            SurveyRecommendations = new List<SurveyRecommendation>();
        }
    }

    internal class RecClassificationConfiguration : EntityTypeConfiguration<RecClassification>
    {
        public RecClassificationConfiguration()
        {
            ToTable("dbo.RecClassification");
            HasKey(x => x.RecClassificationId);

            Property(x => x.RecClassificationId).HasColumnName("RecClassificationID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.RecClassificationTypeCode).HasColumnName("RecClassificationTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.RequiredOnSurveyCompletion).HasColumnName("RequiredOnSurveyCompletion").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.RecClassifications).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.RecClassificationType).WithMany(b => b.RecClassifications).HasForeignKey(c => c.RecClassificationTypeCode);
        }
    }
}
