using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Survey
    {
        public Guid SurveyId { get; set; }
        public int SurveyNumber { get; set; }
        public Guid CompanyId { get; set; }
        public Guid? InsuredId { get; set; }
        public Guid? LocationId { get; set; }
        public Guid? PrimaryPolicyId { get; set; }
        public Guid? PreviousSurveyId { get; set; }
        public Guid SurveyTypeId { get; set; }
        public Guid? TransactionTypeId { get; set; }
        public Guid? SurveyQualityId { get; set; }
        public string ServiceTypeCode { get; set; }
        public string SurveyStatusCode { get; set; }
        public Guid? AssignedByUserId { get; set; }
        public Guid? AssignedUserId { get; set; }
        public Guid? RecommendedUserId { get; set; }
        public string CreateStateCode { get; set; }
        public DateTime CreateDate { get; set; }
        public Guid? CreateByInternalUserId { get; set; }
        public string CreateByExternalUserName { get; set; }
        public DateTime? AssignDate { get; set; }
        public DateTime? ReassignDate { get; set; }
        public DateTime? AcceptDate { get; set; }
        public DateTime? SurveyedDate { get; set; }
        public DateTime? ReportCompleteDate { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? CompleteDate { get; set; }
        public DateTime? CanceledDate { get; set; }
        public DateTime? UnderwritingAcceptDate { get; set; }
        public DateTime? ReminderDate { get; set; }
        public string ReminderComment { get; set; }
        public DateTime? MailReceivedDate { get; set; }
        public bool? Correction { get; set; }
        public bool? NonProductive { get; set; }
        public bool? Review { get; set; }
        public bool? NonDisclosureRequired { get; set; }
        public decimal? SurveyHours { get; set; }
        public decimal? CallCount { get; set; }
        public string SurveyGradingCode { get; set; }
        public string SeverityRatingCode { get; set; }
        public bool? RecsRequired { get; set; }
        public bool? DisplayClaims { get; set; }
        public decimal? FeeConsultantCost { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public bool? FutureVisitNeeded { get; set; }
        public Guid? FutureVisitSurveyTypeId { get; set; }
        public DateTime? FutureVisitDueDate { get; set; }
        public Guid? ConsultantUserId { get; set; }
        public Guid? ReviewerUserId { get; set; }
        public string WorkflowSubmissionNumber { get; set; }
        public string WorkflowReference { get; set; }
        public bool? UnderwriterNotified { get; set; }
        public string UnderwriterCode { get; set; }
        public string LegacySystemId { get; set; }
        public DateTime? LastRefreshDate { get; set; }
        public DateTime? CancelRefreshDate { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public string Priority { get; set; }
        public DateTime? ReviewerCompleteDate { get; set; }
        public string ApprovalCode { get; set; }
        public bool InHouseSelection { get; set; }
        public bool? ContainsCriticalRec { get; set; }
        public DateTime? RequestedDate { get; set; }
        public Guid? PolicySystemId { get; set; }
        public string Underwriter2Code { get; set; }
        public bool? ContainsOpenCriticalRec { get; set; }
        public bool? UWNotified { get; set; }

        public virtual ICollection<DueDateHistory> DueDateHistories { get; set; }
        public virtual ICollection<Objective> Objectives { get; set; }
        public virtual ICollection<ServicePlan> ServicePlans_PrimarySurveyId { get; set; }
        public virtual ICollection<ServicePlan> ServicePlans_ServicePlanId { get; set; }
        public virtual ICollection<Survey> Surveys { get; set; }
        public virtual ICollection<SurveyActivity> SurveyActivities { get; set; }
        public virtual ICollection<SurveyAttribute> SurveyAttributes { get; set; }
        public virtual ICollection<SurveyDocument> SurveyDocuments { get; set; }
        public virtual ICollection<SurveyHistory> SurveyHistories { get; set; }
        public virtual ICollection<SurveyLetter> SurveyLetters { get; set; }
        public virtual ICollection<SurveyLocationNote> SurveyLocationNotes { get; set; }
        public virtual ICollection<SurveyLocationPolicyCoverageName> SurveyLocationPolicyCoverageNames { get; set; }
        public virtual ICollection<SurveyMergeDocumentField> SurveyMergeDocumentFields { get; set; }
        public virtual ICollection<SurveyNote> SurveyNotes { get; set; }
        public virtual ICollection<SurveyPolicyCoverage> SurveyPolicyCoverages { get; set; }
        public virtual ICollection<SurveyReasonType> SurveyReasonTypes { get; set; }
        public virtual ICollection<SurveyRecommendation> SurveyRecommendations { get; set; }
        public virtual ICollection<SurveyReleaseOwnership> SurveyReleaseOwnerships { get; set; }
        public virtual ICollection<SurveyReview> SurveyReviews { get; set; }

        public virtual Company Company { get; set; }
        public virtual CreateState CreateState { get; set; }
        public virtual Insured Insured { get; set; }
        public virtual Location Location { get; set; }
        public virtual Policy Policy { get; set; }
        public virtual ServiceType ServiceType { get; set; }
        public virtual SeverityRating SeverityRating { get; set; }
        public virtual Survey Survey_PreviousSurveyId { get; set; }
        public virtual SurveyGrading SurveyGrading { get; set; }
        public virtual SurveyQuality SurveyQuality { get; set; }
        public virtual SurveyStatus SurveyStatus { get; set; }
        public virtual SurveyType SurveyType_FutureVisitSurveyTypeId { get; set; }
        public virtual SurveyType SurveyType_SurveyTypeId { get; set; }
        public virtual TransactionType TransactionType { get; set; }
        public virtual User User_AssignedUserId { get; set; }
        public virtual User User_ConsultantUserId { get; set; }
        public virtual User User_RecommendedUserId { get; set; }
        public virtual User User_ReviewerUserId { get; set; }
        public virtual User User_CreatedByUserId { get; set; }

        public Survey()
        {
            DueDateHistories = new List<DueDateHistory>();
            Objectives = new List<Objective>();
            ServicePlans_PrimarySurveyId = new List<ServicePlan>();
            Surveys = new List<Survey>();
            SurveyLocationNotes = new List<SurveyLocationNote>();
            SurveyLocationPolicyCoverageNames = new List<SurveyLocationPolicyCoverageName>();
            SurveyMergeDocumentFields = new List<SurveyMergeDocumentField>();
            SurveyPolicyCoverages = new List<SurveyPolicyCoverage>();
            SurveyRecommendations = new List<SurveyRecommendation>();
            SurveyActivities = new List<SurveyActivity>();
            SurveyAttributes = new List<SurveyAttribute>();
            SurveyDocuments = new List<SurveyDocument>();
            SurveyHistories = new List<SurveyHistory>();
            SurveyLetters = new List<SurveyLetter>();
            SurveyNotes = new List<SurveyNote>();
            SurveyReleaseOwnerships = new List<SurveyReleaseOwnership>();
            SurveyReviews = new List<SurveyReview>();
            ServicePlans_ServicePlanId = new List<ServicePlan>();
            SurveyReasonTypes = new List<SurveyReasonType>();
        }
    }

    internal class SurveyConfiguration : EntityTypeConfiguration<Survey>
    {
        public SurveyConfiguration()
        {
            ToTable("dbo.Survey");
            HasKey(x => x.SurveyId);

            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.SurveyNumber).HasColumnName("SurveyNumber").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.InsuredId).HasColumnName("InsuredID").IsOptional();
            Property(x => x.LocationId).HasColumnName("LocationID").IsOptional();
            Property(x => x.PrimaryPolicyId).HasColumnName("PrimaryPolicyID").IsOptional();
            Property(x => x.PreviousSurveyId).HasColumnName("PreviousSurveyID").IsOptional();
            Property(x => x.SurveyTypeId).HasColumnName("SurveyTypeID").IsRequired();
            Property(x => x.TransactionTypeId).HasColumnName("TransactionTypeID").IsOptional();
            Property(x => x.SurveyQualityId).HasColumnName("SurveyQualityID").IsOptional();
            Property(x => x.ServiceTypeCode).HasColumnName("ServiceTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.SurveyStatusCode).HasColumnName("SurveyStatusCode").IsRequired().HasMaxLength(2);
            Property(x => x.AssignedByUserId).HasColumnName("AssignedByUserID").IsOptional();
            Property(x => x.AssignedUserId).HasColumnName("AssignedUserID").IsOptional();
            Property(x => x.RecommendedUserId).HasColumnName("RecommendedUserID").IsOptional();
            Property(x => x.CreateStateCode).HasColumnName("CreateStateCode").IsRequired().HasMaxLength(1);
            Property(x => x.CreateDate).HasColumnName("CreateDate").IsRequired();
            Property(x => x.CreateByInternalUserId).HasColumnName("CreateByInternalUserID").IsOptional();
            Property(x => x.CreateByExternalUserName).HasColumnName("CreateByExternalUserName").IsOptional().HasMaxLength(30);
            Property(x => x.AssignDate).HasColumnName("AssignDate").IsOptional();
            Property(x => x.ReassignDate).HasColumnName("ReassignDate").IsOptional();
            Property(x => x.AcceptDate).HasColumnName("AcceptDate").IsOptional();
            Property(x => x.SurveyedDate).HasColumnName("SurveyedDate").IsOptional();
            Property(x => x.ReportCompleteDate).HasColumnName("ReportCompleteDate").IsOptional();
            Property(x => x.DueDate).HasColumnName("DueDate").IsOptional();
            Property(x => x.CompleteDate).HasColumnName("CompleteDate").IsOptional();
            Property(x => x.CanceledDate).HasColumnName("CanceledDate").IsOptional();
            Property(x => x.UnderwritingAcceptDate).HasColumnName("UnderwritingAcceptDate").IsOptional();
            Property(x => x.ReminderDate).HasColumnName("ReminderDate").IsOptional();
            Property(x => x.ReminderComment).HasColumnName("ReminderComment").IsOptional().HasMaxLength(500);
            Property(x => x.MailReceivedDate).HasColumnName("MailReceivedDate").IsOptional();
            Property(x => x.Correction).HasColumnName("Correction").IsOptional();
            Property(x => x.NonProductive).HasColumnName("NonProductive").IsOptional();
            Property(x => x.Review).HasColumnName("Review").IsOptional();
            Property(x => x.NonDisclosureRequired).HasColumnName("NonDisclosureRequired").IsOptional();
            Property(x => x.SurveyHours).HasColumnName("SurveyHours").IsOptional().HasPrecision(9, 2);
            Property(x => x.CallCount).HasColumnName("CallCount").IsOptional().HasPrecision(9, 2);
            Property(x => x.SurveyGradingCode).HasColumnName("SurveyGradingCode").IsOptional().HasMaxLength(1);
            Property(x => x.SeverityRatingCode).HasColumnName("SeverityRatingCode").IsOptional().HasMaxLength(2);
            Property(x => x.RecsRequired).HasColumnName("RecsRequired").IsOptional();
            Property(x => x.DisplayClaims).HasColumnName("DisplayClaims").IsOptional();
            Property(x => x.FeeConsultantCost).HasColumnName("FeeConsultantCost").IsOptional().HasPrecision(9, 2);
            Property(x => x.InvoiceNumber).HasColumnName("InvoiceNumber").IsOptional().HasMaxLength(30);
            Property(x => x.InvoiceDate).HasColumnName("InvoiceDate").IsOptional();
            Property(x => x.FutureVisitNeeded).HasColumnName("FutureVisitNeeded").IsOptional();
            Property(x => x.FutureVisitSurveyTypeId).HasColumnName("FutureVisitSurveyTypeID").IsOptional();
            Property(x => x.FutureVisitDueDate).HasColumnName("FutureVisitDueDate").IsOptional();
            Property(x => x.ConsultantUserId).HasColumnName("ConsultantUserID").IsOptional();
            Property(x => x.ReviewerUserId).HasColumnName("ReviewerUserID").IsOptional();
            Property(x => x.WorkflowSubmissionNumber).HasColumnName("WorkflowSubmissionNumber").IsOptional().HasMaxLength(50);
            Property(x => x.WorkflowReference).HasColumnName("WorkflowReference").IsOptional().HasMaxLength(50);
            Property(x => x.UnderwriterNotified).HasColumnName("UnderwriterNotified").IsOptional();
            Property(x => x.UnderwriterCode).HasColumnName("UnderwriterCode").IsOptional().HasMaxLength(3);
            Property(x => x.LegacySystemId).HasColumnName("LegacySystemID").IsOptional().HasMaxLength(100);
            Property(x => x.LastRefreshDate).HasColumnName("LastRefreshDate").IsOptional();
            Property(x => x.CancelRefreshDate).HasColumnName("CancelRefreshDate").IsOptional();
            Property(x => x.LastModifiedOn).HasColumnName("LastModifiedOn").IsOptional();
            Property(x => x.Priority).HasColumnName("Priority").IsOptional().HasMaxLength(500);
            Property(x => x.ReviewerCompleteDate).HasColumnName("ReviewerCompleteDate").IsOptional();
            Property(x => x.ApprovalCode).HasColumnName("ApprovalCode").IsOptional().HasMaxLength(10);
            Property(x => x.InHouseSelection).HasColumnName("InHouseSelection").IsRequired();
            Property(x => x.ContainsCriticalRec).HasColumnName("ContainsCriticalRec").IsOptional();
            Property(x => x.RequestedDate).HasColumnName("RequestedDate").IsOptional();
            Property(x => x.PolicySystemId).HasColumnName("PolicySystemID").IsOptional();
            Property(x => x.Underwriter2Code).HasColumnName("Underwriter2Code").IsOptional();
            Property(x => x.ContainsOpenCriticalRec).HasColumnName("ContainsOpenCriticalRec").IsOptional();
            Property(x => x.UWNotified).HasColumnName("UWNotified").IsOptional();

            HasRequired(a => a.Company).WithMany(b => b.Surveys).HasForeignKey(c => c.CompanyId);
            HasOptional(a => a.Insured).WithMany(b => b.Surveys).HasForeignKey(c => c.InsuredId);
            HasOptional(a => a.Location).WithMany(b => b.Surveys).HasForeignKey(c => c.LocationId);
            HasOptional(a => a.Policy).WithMany(b => b.Surveys).HasForeignKey(c => c.PrimaryPolicyId);
            HasOptional(a => a.Survey_PreviousSurveyId).WithMany(b => b.Surveys).HasForeignKey(c => c.PreviousSurveyId);
            HasRequired(a => a.SurveyType_SurveyTypeId).WithMany(b => b.Surveys_SurveyTypeId).HasForeignKey(c => c.SurveyTypeId);
            HasOptional(a => a.TransactionType).WithMany(b => b.Surveys).HasForeignKey(c => c.TransactionTypeId);
            HasOptional(a => a.SurveyQuality).WithMany(b => b.Surveys).HasForeignKey(c => c.SurveyQualityId);
            HasRequired(a => a.ServiceType).WithMany(b => b.Surveys).HasForeignKey(c => c.ServiceTypeCode);
            HasRequired(a => a.SurveyStatus).WithMany(b => b.Surveys).HasForeignKey(c => c.SurveyStatusCode);
            HasOptional(a => a.User_AssignedUserId).WithMany(b => b.Surveys_AssignedUserId).HasForeignKey(c => c.AssignedUserId);
            HasOptional(a => a.User_RecommendedUserId).WithMany(b => b.Surveys_RecommendedUserId).HasForeignKey(c => c.RecommendedUserId);
            HasRequired(a => a.CreateState).WithMany(b => b.Surveys).HasForeignKey(c => c.CreateStateCode);
            HasOptional(a => a.SurveyGrading).WithMany(b => b.Surveys).HasForeignKey(c => c.SurveyGradingCode);
            HasOptional(a => a.SeverityRating).WithMany(b => b.Surveys).HasForeignKey(c => c.SeverityRatingCode);
            HasOptional(a => a.SurveyType_FutureVisitSurveyTypeId).WithMany(b => b.Surveys_FutureVisitSurveyTypeId).HasForeignKey(c => c.FutureVisitSurveyTypeId);
            HasOptional(a => a.User_ConsultantUserId).WithMany(b => b.Surveys_ConsultantUserId).HasForeignKey(c => c.ConsultantUserId);
            HasOptional(a => a.User_ReviewerUserId).WithMany(b => b.Surveys_ReviewerUserId).HasForeignKey(c => c.ReviewerUserId);
            HasOptional(a => a.User_CreatedByUserId).WithMany(b => b.Surveys_CreatedByUserId).HasForeignKey(c => c.CreateByInternalUserId);
            HasMany(t => t.SurveyReasonTypes).WithMany(t => t.Surveys).Map(m =>
            {
                m.ToTable("Survey_Reason");
                m.MapLeftKey("SurveyID");
                m.MapRightKey("SurveyReasonTypeID");
            });
        }
    }
}
