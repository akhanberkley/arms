using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Underwriter
    {
        public string UnderwriterCode { get; set; }
        public Guid CompanyId { get; set; }
        public string UnderwriterName { get; set; }
        public string DomainName { get; set; }
        public string Username { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneExt { get; set; }
        public string EmailAddress { get; set; }
        public string Title { get; set; }
        public string ProfitCenterCode { get; set; }
        public bool Disabled { get; set; }

        public virtual Company Company { get; set; }
        public virtual ProfitCenter ProfitCenter { get; set; }
    }

    internal class UnderwriterConfiguration : EntityTypeConfiguration<Underwriter>
    {
        public UnderwriterConfiguration()
        {
            ToTable("dbo.Underwriter");
            HasKey(x => new { x.UnderwriterCode, x.CompanyId });

            Property(x => x.UnderwriterCode).HasColumnName("UnderwriterCode").IsRequired().HasMaxLength(3).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.UnderwriterName).HasColumnName("UnderwriterName").IsRequired().HasMaxLength(30);
            Property(x => x.DomainName).HasColumnName("DomainName").IsRequired().HasMaxLength(20);
            Property(x => x.Username).HasColumnName("Username").IsRequired().HasMaxLength(30);
            Property(x => x.PhoneNumber).HasColumnName("PhoneNumber").IsRequired().HasMaxLength(30);
            Property(x => x.PhoneExt).HasColumnName("PhoneExt").IsOptional().HasMaxLength(10);
            Property(x => x.EmailAddress).HasColumnName("EmailAddress").IsOptional().HasMaxLength(100);
            Property(x => x.Title).HasColumnName("Title").IsRequired().HasMaxLength(50);
            Property(x => x.ProfitCenterCode).HasColumnName("ProfitCenterCode").IsOptional().HasMaxLength(10);
            Property(x => x.Disabled).HasColumnName("Disabled").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.Underwriters).HasForeignKey(c => c.CompanyId);
            HasOptional(a => a.ProfitCenter).WithMany(b => b.Underwriters).HasForeignKey(c => c.ProfitCenterCode);
        }
    }
}
