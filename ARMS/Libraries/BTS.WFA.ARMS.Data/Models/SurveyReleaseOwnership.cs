using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyReleaseOwnership
    {
        public Guid SurveyReleaseOwnershipId { get; set; }
        public Guid SurveyId { get; set; }
        public Guid SurveyReleaseOwnershipTypeId { get; set; }
        public Guid UserId { get; set; }
        public DateTime ReleasedDate { get; set; }

        public virtual Survey Survey { get; set; }
        public virtual SurveyReleaseOwnershipType SurveyReleaseOwnershipType { get; set; }
        public virtual User User { get; set; }
    }

    internal class SurveyReleaseOwnershipConfiguration : EntityTypeConfiguration<SurveyReleaseOwnership>
    {
        public SurveyReleaseOwnershipConfiguration()
        {
            ToTable("dbo.SurveyReleaseOwnership");
            HasKey(x => x.SurveyReleaseOwnershipId);

            Property(x => x.SurveyReleaseOwnershipId).HasColumnName("SurveyReleaseOwnershipID").IsRequired();
            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.SurveyReleaseOwnershipTypeId).HasColumnName("SurveyReleaseOwnershipTypeID").IsRequired();
            Property(x => x.UserId).HasColumnName("UserID").IsRequired();
            Property(x => x.ReleasedDate).HasColumnName("ReleasedDate").IsRequired();

            HasRequired(a => a.Survey).WithMany(b => b.SurveyReleaseOwnerships).HasForeignKey(c => c.SurveyId);
            HasRequired(a => a.SurveyReleaseOwnershipType).WithMany(b => b.SurveyReleaseOwnerships).HasForeignKey(c => c.SurveyReleaseOwnershipTypeId);
            HasRequired(a => a.User).WithMany(b => b.SurveyReleaseOwnerships).HasForeignKey(c => c.UserId);
        }
    }
}
