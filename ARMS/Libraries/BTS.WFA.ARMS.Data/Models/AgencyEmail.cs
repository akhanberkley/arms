using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class AgencyEmail
    {
        public Guid AgencyEmailId { get; set; }
        public Guid AgencyId { get; set; }
        public string EmailAddress { get; set; }

        public virtual ICollection<Insured> Insureds { get; set; }

        public virtual Agency Agency { get; set; }

        public AgencyEmail()
        {
            Insureds = new List<Insured>();
        }
    }

    internal class AgencyEmailConfiguration : EntityTypeConfiguration<AgencyEmail>
    {
        public AgencyEmailConfiguration()
        {
            ToTable("dbo.AgencyEmail");
            HasKey(x => x.AgencyEmailId);

            Property(x => x.AgencyEmailId).HasColumnName("AgencyEmailID").IsRequired();
            Property(x => x.AgencyId).HasColumnName("AgencyID").IsRequired();
            Property(x => x.EmailAddress).HasColumnName("EmailAddress").IsRequired().HasMaxLength(100);

            HasRequired(a => a.Agency).WithMany(b => b.AgencyEmails).HasForeignKey(c => c.AgencyId);
        }
    }
}
