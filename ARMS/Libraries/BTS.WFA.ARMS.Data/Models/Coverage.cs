using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Coverage
    {
        public Guid CoverageId { get; set; }
        public Guid InsuredId { get; set; }
        public Guid? PolicyId { get; set; }
        public Guid CoverageNameId { get; set; }
        public string CoverageTypeCode { get; set; }
        public string CoverageValue { get; set; }

        public virtual ICollection<CoverageDetail> CoverageDetails { get; set; }

        public virtual CoverageName CoverageName { get; set; }
        public virtual CoverageType CoverageType { get; set; }
        public virtual Insured Insured { get; set; }
        public virtual Policy Policy { get; set; }

        public Coverage()
        {
            CoverageDetails = new List<CoverageDetail>();
        }
    }

    internal class CoverageConfiguration : EntityTypeConfiguration<Coverage>
    {
        public CoverageConfiguration()
        {
            ToTable("dbo.Coverage");
            HasKey(x => x.CoverageId);

            Property(x => x.CoverageId).HasColumnName("CoverageID").IsRequired();
            Property(x => x.InsuredId).HasColumnName("InsuredID").IsRequired();
            Property(x => x.PolicyId).HasColumnName("PolicyID").IsOptional();
            Property(x => x.CoverageNameId).HasColumnName("CoverageNameID").IsRequired();
            Property(x => x.CoverageTypeCode).HasColumnName("CoverageTypeCode").IsRequired().HasMaxLength(2);
            Property(x => x.CoverageValue).HasColumnName("CoverageValue").IsOptional().HasMaxLength(300);

            HasRequired(a => a.Insured).WithMany(b => b.Coverages).HasForeignKey(c => c.InsuredId);
            HasOptional(a => a.Policy).WithMany(b => b.Coverages).HasForeignKey(c => c.PolicyId);
            HasRequired(a => a.CoverageName).WithMany(b => b.Coverages).HasForeignKey(c => c.CoverageNameId);
            HasRequired(a => a.CoverageType).WithMany(b => b.Coverages).HasForeignKey(c => c.CoverageTypeCode);
        }
    }
}
