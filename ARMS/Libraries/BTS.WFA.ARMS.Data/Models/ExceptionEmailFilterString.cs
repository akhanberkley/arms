using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ExceptionEmailFilterString
    {
        public int Id { get; set; }
        public string FilterString { get; set; }
        public bool Active { get; set; }
        public string CorrectiveInstructions { get; set; }
    }

    internal class ExceptionEmailFilterStringConfiguration : EntityTypeConfiguration<ExceptionEmailFilterString>
    {
        public ExceptionEmailFilterStringConfiguration()
        {
            ToTable("dbo.ExceptionEmailFilterString");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.FilterString).HasColumnName("FilterString").IsRequired().HasMaxLength(200);
            Property(x => x.Active).HasColumnName("Active").IsRequired();
            Property(x => x.CorrectiveInstructions).HasColumnName("CorrectiveInstructions").IsOptional().HasMaxLength(500);
        }
    }
}
