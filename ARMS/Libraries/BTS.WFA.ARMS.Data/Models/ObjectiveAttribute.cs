using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ObjectiveAttribute
    {
        public Guid ObjectiveId { get; set; }
        public Guid CompanyAttributeId { get; set; }
        public string AttributeValue { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }

        public virtual CompanyAttribute CompanyAttribute { get; set; }
        public virtual Objective Objective { get; set; }
    }

    internal class ObjectiveAttributeConfiguration : EntityTypeConfiguration<ObjectiveAttribute>
    {
        public ObjectiveAttributeConfiguration()
        {
            ToTable("dbo.ObjectiveAttribute");
            HasKey(x => new { x.ObjectiveId, x.CompanyAttributeId });

            Property(x => x.ObjectiveId).HasColumnName("ObjectiveID").IsRequired();
            Property(x => x.CompanyAttributeId).HasColumnName("CompanyAttributeID").IsRequired();
            Property(x => x.AttributeValue).HasColumnName("AttributeValue").IsOptional().HasMaxLength(3000);
            Property(x => x.LastModifiedBy).HasColumnName("LastModifiedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastModifiedDate).HasColumnName("LastModifiedDate").IsOptional();

            HasRequired(a => a.Objective).WithMany(b => b.ObjectiveAttributes).HasForeignKey(c => c.ObjectiveId);
            HasRequired(a => a.CompanyAttribute).WithMany(b => b.ObjectiveAttributes).HasForeignKey(c => c.CompanyAttributeId);
        }
    }
}
