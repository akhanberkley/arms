using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Recommendation
    {
        public Guid RecommendationId { get; set; }
        public Guid CompanyId { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Disabled { get; set; }

        public virtual ICollection<SurveyRecommendation> SurveyRecommendations { get; set; }

        public virtual Company Company { get; set; }

        public Recommendation()
        {
            SurveyRecommendations = new List<SurveyRecommendation>();
        }
    }

    internal class RecommendationConfiguration : EntityTypeConfiguration<Recommendation>
    {
        public RecommendationConfiguration()
        {
            ToTable("dbo.Recommendation");
            HasKey(x => x.RecommendationId);

            Property(x => x.RecommendationId).HasColumnName("RecommendationID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.Code).HasColumnName("Code").IsRequired().HasMaxLength(50);
            Property(x => x.Title).HasColumnName("Title").IsRequired().HasMaxLength(100);
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasMaxLength(2147483647);
            Property(x => x.Disabled).HasColumnName("Disabled").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.Recommendations).HasForeignKey(c => c.CompanyId);
        }
    }
}
