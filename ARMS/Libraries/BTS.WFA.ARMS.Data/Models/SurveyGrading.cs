using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyGrading
    {
        public string SurveyGradingCode { get; set; }
        public string SurveyGradingName { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<ServicePlan> ServicePlans { get; set; }
        public virtual ICollection<Survey> Surveys { get; set; }
        public virtual ICollection<SurveyGradingCompany> SurveyGradingCompanies { get; set; }

        public SurveyGrading()
        {
            ServicePlans = new List<ServicePlan>();
            Surveys = new List<Survey>();
            SurveyGradingCompanies = new List<SurveyGradingCompany>();
        }
    }

    internal class SurveyGradingConfiguration : EntityTypeConfiguration<SurveyGrading>
    {
        public SurveyGradingConfiguration()
        {
            ToTable("dbo.SurveyGrading");
            HasKey(x => x.SurveyGradingCode);

            Property(x => x.SurveyGradingCode).HasColumnName("SurveyGradingCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SurveyGradingName).HasColumnName("SurveyGradingName").IsRequired().HasMaxLength(40);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
