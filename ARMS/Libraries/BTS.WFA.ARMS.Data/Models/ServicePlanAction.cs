using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanAction
    {
        public Guid ServicePlanActionId { get; set; }
        public Guid CompanyId { get; set; }
        public string ServicePlanStatusCode { get; set; }
        public string ServicePlanActionTypeCode { get; set; }
        public string DisplayName { get; set; }
        public int DisplayOrder { get; set; }
        public string ConfirmMessage { get; set; }

        public virtual ICollection<ServicePlanActionPermission> ServicePlanActionPermissions { get; set; }

        public virtual Company Company { get; set; }
        public virtual ServicePlanActionType ServicePlanActionType { get; set; }
        public virtual ServicePlanStatus ServicePlanStatus { get; set; }

        public ServicePlanAction()
        {
            ServicePlanActionPermissions = new List<ServicePlanActionPermission>();
        }
    }

    internal class ServicePlanActionConfiguration : EntityTypeConfiguration<ServicePlanAction>
    {
        public ServicePlanActionConfiguration()
        {
            ToTable("dbo.ServicePlanAction");
            HasKey(x => x.ServicePlanActionId);

            Property(x => x.ServicePlanActionId).HasColumnName("ServicePlanActionID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.ServicePlanStatusCode).HasColumnName("ServicePlanStatusCode").IsRequired().HasMaxLength(1);
            Property(x => x.ServicePlanActionTypeCode).HasColumnName("ServicePlanActionTypeCode").IsRequired().HasMaxLength(10);
            Property(x => x.DisplayName).HasColumnName("DisplayName").IsRequired().HasMaxLength(60);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.ConfirmMessage).HasColumnName("ConfirmMessage").IsOptional().HasMaxLength(500);

            HasRequired(a => a.Company).WithMany(b => b.ServicePlanActions).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.ServicePlanStatus).WithMany(b => b.ServicePlanActions).HasForeignKey(c => c.ServicePlanStatusCode);
            HasRequired(a => a.ServicePlanActionType).WithMany(b => b.ServicePlanActions).HasForeignKey(c => c.ServicePlanActionTypeCode);
        }
    }
}
