using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class RateModificationFactorType
    {
        public string RateModificationFactorTypeCode { get; set; }
        public string RateModificationFactorTypeDescription { get; set; }

        public virtual ICollection<RateModificationFactor> RateModificationFactors { get; set; }

        public RateModificationFactorType()
        {
            RateModificationFactors = new List<RateModificationFactor>();
        }
    }

    internal class RateModificationFactorTypeConfiguration : EntityTypeConfiguration<RateModificationFactorType>
    {
        public RateModificationFactorTypeConfiguration()
        {
            ToTable("dbo.RateModificationFactorType");
            HasKey(x => x.RateModificationFactorTypeCode);

            Property(x => x.RateModificationFactorTypeCode).HasColumnName("RateModificationFactorTypeCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RateModificationFactorTypeDescription).HasColumnName("RateModificationFactorTypeDescription").IsRequired().HasMaxLength(50);
        }
    }
}
