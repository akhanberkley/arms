using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class CompanyPolicySystem
    {
        public Guid PolicySystemCompanyId { get; set; }
        public Guid CompanyId { get; set; }
        public int DisplayOrder { get; set; }

        public virtual Company Company_CompanyId { get; set; }
        public virtual Company Company_PolicySystemCompanyId { get; set; }
    }

    internal class CompanyPolicySystemConfiguration : EntityTypeConfiguration<CompanyPolicySystem>
    {
        public CompanyPolicySystemConfiguration()
        {
            ToTable("dbo.CompanyPolicySystem");
            HasKey(x => new { x.PolicySystemCompanyId, x.CompanyId });

            Property(x => x.PolicySystemCompanyId).HasColumnName("PolicySystemCompanyID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company_PolicySystemCompanyId).WithMany(b => b.CompanyPolicySystems_PolicySystemCompanyId).HasForeignKey(c => c.PolicySystemCompanyId);
            HasRequired(a => a.Company_CompanyId).WithMany(b => b.CompanyPolicySystems_CompanyId).HasForeignKey(c => c.CompanyId);
        }
    }
}
