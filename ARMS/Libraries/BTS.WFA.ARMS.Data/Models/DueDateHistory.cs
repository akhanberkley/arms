using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class DueDateHistory
    {
        public Guid Id { get; set; }
        public Guid CompanyId { get; set; }
        public Guid SurveyId { get; set; }
        public Guid RequestedByUserId { get; set; }
        public DateTime CurrentDueDate { get; set; }
        public DateTime UpdatedDueDate { get; set; }
        public bool Approved { get; set; }
        public Guid? ApprovalByUserId { get; set; }
        public DateTime CreateDateTime { get; set; }

        public virtual Company Company { get; set; }
        public virtual Survey Survey { get; set; }
        public virtual User User_ApprovalByUserId { get; set; }
        public virtual User User_RequestedByUserId { get; set; }
    }

    internal class DueDateHistoryConfiguration : EntityTypeConfiguration<DueDateHistory>
    {
        public DueDateHistoryConfiguration()
        {
            ToTable("dbo.DueDateHistory");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.RequestedByUserId).HasColumnName("RequestedByUserID").IsRequired();
            Property(x => x.CurrentDueDate).HasColumnName("CurrentDueDate").IsRequired();
            Property(x => x.UpdatedDueDate).HasColumnName("UpdatedDueDate").IsRequired();
            Property(x => x.Approved).HasColumnName("Approved").IsRequired();
            Property(x => x.ApprovalByUserId).HasColumnName("ApprovalByUserID").IsOptional();
            Property(x => x.CreateDateTime).HasColumnName("CreateDateTime").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.DueDateHistories).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.Survey).WithMany(b => b.DueDateHistories).HasForeignKey(c => c.SurveyId);
            HasRequired(a => a.User_RequestedByUserId).WithMany(b => b.DueDateHistories_RequestedByUserId).HasForeignKey(c => c.RequestedByUserId);
            HasOptional(a => a.User_ApprovalByUserId).WithMany(b => b.DueDateHistories_ApprovalByUserId).HasForeignKey(c => c.ApprovalByUserId);
        }
    }
}
