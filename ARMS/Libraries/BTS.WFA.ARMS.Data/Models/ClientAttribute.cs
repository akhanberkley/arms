using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ClientAttribute
    {
        public Guid ClientAttributeId { get; set; }
        public string ClientAttributeTypeCode { get; set; }
        public Guid CompanyId { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<Client> Clients { get; set; }

        public virtual ClientAttributeType ClientAttributeType { get; set; }
        public virtual Company Company { get; set; }

        public ClientAttribute()
        {
            Clients = new List<Client>();
        }
    }

    internal class ClientAttributeConfiguration : EntityTypeConfiguration<ClientAttribute>
    {
        public ClientAttributeConfiguration()
        {
            ToTable("dbo.ClientAttribute");
            HasKey(x => x.ClientAttributeId);

            Property(x => x.ClientAttributeId).HasColumnName("ClientAttributeID").IsRequired();
            Property(x => x.ClientAttributeTypeCode).HasColumnName("ClientAttributeTypeCode").IsRequired().HasMaxLength(2);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.ClientAttributeType).WithMany(b => b.ClientAttributes).HasForeignKey(c => c.ClientAttributeTypeCode);
            HasRequired(a => a.Company).WithMany(b => b.ClientAttributes).HasForeignKey(c => c.CompanyId);
        }
    }
}
