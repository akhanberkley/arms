using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyStatusType
    {
        public string SurveyStatusTypeCode { get; set; }
        public string SurveyStatusTypeName { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<AssignmentRule> AssignmentRules { get; set; }
        public virtual ICollection<FilterField> FilterFields { get; set; }
        public virtual ICollection<SurveyStatus> SurveyStatus { get; set; }
        public virtual ICollection<Territory> Territories { get; set; }

        public SurveyStatusType()
        {
            AssignmentRules = new List<AssignmentRule>();
            FilterFields = new List<FilterField>();
            SurveyStatus = new List<SurveyStatus>();
            Territories = new List<Territory>();
        }
    }

    internal class SurveyStatusTypeConfiguration : EntityTypeConfiguration<SurveyStatusType>
    {
        public SurveyStatusTypeConfiguration()
        {
            ToTable("dbo.SurveyStatusType");
            HasKey(x => x.SurveyStatusTypeCode);

            Property(x => x.SurveyStatusTypeCode).HasColumnName("SurveyStatusTypeCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SurveyStatusTypeName).HasColumnName("SurveyStatusTypeName").IsRequired().HasMaxLength(30);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
