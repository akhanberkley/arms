using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Filter
    {
        public Guid FilterId { get; set; }

        public virtual ICollection<AssignmentRule> AssignmentRules { get; set; }
        public virtual ICollection<FilterCondition> FilterConditions { get; set; }

        public Filter()
        {
            AssignmentRules = new List<AssignmentRule>();
            FilterConditions = new List<FilterCondition>();
        }
    }

    internal class FilterConfiguration : EntityTypeConfiguration<Filter>
    {
        public FilterConfiguration()
        {
            ToTable("dbo.Filter");
            HasKey(x => x.FilterId);

            Property(x => x.FilterId).HasColumnName("FilterID").IsRequired();
        }
    }
}
