using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyGradingCompany
    {
        public string SurveyGradingCode { get; set; }
        public Guid CompanyId { get; set; }
        public int DisplayOrder { get; set; }

        public virtual Company Company { get; set; }
        public virtual SurveyGrading SurveyGrading { get; set; }
    }

    internal class SurveyGradingCompanyConfiguration : EntityTypeConfiguration<SurveyGradingCompany>
    {
        public SurveyGradingCompanyConfiguration()
        {
            ToTable("dbo.SurveyGrading_Company");
            HasKey(x => new { x.SurveyGradingCode, x.CompanyId });

            Property(x => x.SurveyGradingCode).HasColumnName("SurveyGradingCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.SurveyGrading).WithMany(b => b.SurveyGradingCompanies).HasForeignKey(c => c.SurveyGradingCode);
            HasRequired(a => a.Company).WithMany(b => b.SurveyGradingCompanies).HasForeignKey(c => c.CompanyId);
        }
    }
}
