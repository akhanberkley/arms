using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ClientHistory
    {
        public Guid ClientHistoryId { get; set; }
        public string ClientId { get; set; }
        public DateTime EntryDate { get; set; }
        public string EntryText { get; set; }
    }

    internal class ClientHistoryConfiguration : EntityTypeConfiguration<ClientHistory>
    {
        public ClientHistoryConfiguration()
        {
            ToTable("dbo.ClientHistory");
            HasKey(x => x.ClientHistoryId);

            Property(x => x.ClientHistoryId).HasColumnName("ClientHistoryID").IsRequired();
            Property(x => x.ClientId).HasColumnName("ClientID").IsRequired().HasMaxLength(50);
            Property(x => x.EntryDate).HasColumnName("EntryDate").IsRequired();
            Property(x => x.EntryText).HasColumnName("EntryText").IsRequired().HasMaxLength(8000);
        }
    }
}
