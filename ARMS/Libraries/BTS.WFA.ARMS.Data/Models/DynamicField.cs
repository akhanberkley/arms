using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class DynamicField
    {
        public Guid DynamicFieldId { get; set; }
        public Guid CompanyId { get; set; }
        public string Description { get; set; }
        public bool IsRequired { get; set; }
        public bool Disabled { get; set; }
        public int PriorityIndex { get; set; }

        public virtual ICollection<ServicePlanDynamicField> ServicePlanDynamicFields { get; set; }

        public virtual Company Company { get; set; }

        public DynamicField()
        {
            ServicePlanDynamicFields = new List<ServicePlanDynamicField>();
        }
    }

    internal class DynamicFieldConfiguration : EntityTypeConfiguration<DynamicField>
    {
        public DynamicFieldConfiguration()
        {
            ToTable("dbo.DynamicField");
            HasKey(x => x.DynamicFieldId);

            Property(x => x.DynamicFieldId).HasColumnName("DynamicFieldID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.Description).HasColumnName("Description").IsRequired().HasMaxLength(50);
            Property(x => x.IsRequired).HasColumnName("IsRequired").IsRequired();
            Property(x => x.Disabled).HasColumnName("Disabled").IsRequired();
            Property(x => x.PriorityIndex).HasColumnName("PriorityIndex").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.DynamicFields).HasForeignKey(c => c.CompanyId);
        }
    }
}
