using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ActivityTypeLevel
    {
        public string ActivityTypeLevelCode { get; set; }
        public string ActivityTypeLevelName { get; set; }

        public virtual ICollection<ActivityType> ActivityTypes { get; set; }

        public ActivityTypeLevel()
        {
            ActivityTypes = new List<ActivityType>();
        }
    }

    internal class ActivityTypeLevelConfiguration : EntityTypeConfiguration<ActivityTypeLevel>
    {
        public ActivityTypeLevelConfiguration()
        {
            ToTable("dbo.ActivityTypeLevel");
            HasKey(x => x.ActivityTypeLevelCode);

            Property(x => x.ActivityTypeLevelCode).HasColumnName("ActivityTypeLevelCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ActivityTypeLevelName).HasColumnName("ActivityTypeLevelName").IsRequired().HasMaxLength(30);
        }
    }
}
