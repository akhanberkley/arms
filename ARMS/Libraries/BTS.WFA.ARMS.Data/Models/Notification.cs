using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Notification
    {
        public Guid NotificationId { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Link { get; set; }
        public int? Type { get; set; }
        public int? Severity { get; set; }
    }

    internal class NotificationConfiguration : EntityTypeConfiguration<Notification>
    {
        public NotificationConfiguration()
        {
            ToTable("dbo.Notification");
            HasKey(x => x.NotificationId);

            Property(x => x.NotificationId).HasColumnName("NotificationID").IsRequired();
            Property(x => x.Description).HasColumnName("Description").IsRequired().HasMaxLength(1000);
            Property(x => x.StartDate).HasColumnName("StartDate").IsRequired();
            Property(x => x.EndDate).HasColumnName("EndDate").IsRequired();
            Property(x => x.Link).HasColumnName("Link").IsOptional().HasMaxLength(200);
            Property(x => x.Type).HasColumnName("Type").IsOptional();
            Property(x => x.Severity).HasColumnName("Severity").IsOptional();
        }
    }
}
