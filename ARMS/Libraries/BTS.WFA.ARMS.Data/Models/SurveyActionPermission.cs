using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyActionPermission
    {
        public Guid SurveyActionPermissionId { get; set; }
        public Guid PermissionSetId { get; set; }
        public Guid SurveyActionId { get; set; }
        public string ServiceTypeCode { get; set; }
        public int Setting { get; set; }

        public virtual PermissionSet PermissionSet { get; set; }
        public virtual ServiceType ServiceType { get; set; }
        public virtual SurveyAction SurveyAction { get; set; }
    }

    internal class SurveyActionPermissionConfiguration : EntityTypeConfiguration<SurveyActionPermission>
    {
        public SurveyActionPermissionConfiguration()
        {
            ToTable("dbo.SurveyActionPermission");
            HasKey(x => x.SurveyActionPermissionId);

            Property(x => x.SurveyActionPermissionId).HasColumnName("SurveyActionPermissionID").IsRequired();
            Property(x => x.PermissionSetId).HasColumnName("PermissionSetID").IsRequired();
            Property(x => x.SurveyActionId).HasColumnName("SurveyActionID").IsRequired();
            Property(x => x.ServiceTypeCode).HasColumnName("ServiceTypeCode").IsOptional().HasMaxLength(1);
            Property(x => x.Setting).HasColumnName("Setting").IsRequired();

            HasRequired(a => a.PermissionSet).WithMany(b => b.SurveyActionPermissions).HasForeignKey(c => c.PermissionSetId);
            HasRequired(a => a.SurveyAction).WithMany(b => b.SurveyActionPermissions).HasForeignKey(c => c.SurveyActionId);
            HasOptional(a => a.ServiceType).WithMany(b => b.SurveyActionPermissions).HasForeignKey(c => c.ServiceTypeCode);
        }
    }
}
