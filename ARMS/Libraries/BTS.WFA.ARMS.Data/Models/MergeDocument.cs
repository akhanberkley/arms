using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class MergeDocument
    {
        public Guid MergeDocumentId { get; set; }
        public Guid CompanyId { get; set; }
        public string MergeDocumentName { get; set; }
        public bool Disabled { get; set; }
        public bool IsLetter { get; set; }
        public int? DaysUntilReminder { get; set; }
        public string ReminderDateTypeCode { get; set; }
        public int PriorityIndex { get; set; }
        public int? Type { get; set; }
        public bool? IsCritical { get; set; }

        public virtual ICollection<MergeDocumentField> MergeDocumentFields { get; set; }
        public virtual ICollection<MergeDocumentVersion> MergeDocumentVersions { get; set; }
        public virtual ICollection<SurveyLetter> SurveyLetters { get; set; }

        public virtual Company Company { get; set; }
        public virtual MergeDocumentType MergeDocumentType { get; set; }

        public MergeDocument()
        {
            MergeDocumentFields = new List<MergeDocumentField>();
            MergeDocumentVersions = new List<MergeDocumentVersion>();
            SurveyLetters = new List<SurveyLetter>();
        }
    }

    internal class MergeDocumentConfiguration : EntityTypeConfiguration<MergeDocument>
    {
        public MergeDocumentConfiguration()
        {
            ToTable("dbo.MergeDocument");
            HasKey(x => x.MergeDocumentId);

            Property(x => x.MergeDocumentId).HasColumnName("MergeDocumentID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.MergeDocumentName).HasColumnName("MergeDocumentName").IsRequired().HasMaxLength(40);
            Property(x => x.Disabled).HasColumnName("Disabled").IsRequired();
            Property(x => x.IsLetter).HasColumnName("IsLetter").IsRequired();
            Property(x => x.DaysUntilReminder).HasColumnName("DaysUntilReminder").IsOptional();
            Property(x => x.ReminderDateTypeCode).HasColumnName("ReminderDateTypeCode").IsOptional().HasMaxLength(1);
            Property(x => x.PriorityIndex).HasColumnName("PriorityIndex").IsRequired();
            Property(x => x.Type).HasColumnName("Type").IsOptional();
            Property(x => x.IsCritical).HasColumnName("IsCritical").IsOptional();

            HasRequired(a => a.Company).WithMany(b => b.MergeDocuments).HasForeignKey(c => c.CompanyId);
            HasOptional(a => a.MergeDocumentType).WithMany(b => b.MergeDocuments).HasForeignKey(c => c.Type);
        }
    }
}
