using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyLetter
    {
        public Guid SurveyLetterId { get; set; }
        public Guid SurveyId { get; set; }
        public Guid MergeDocumentId { get; set; }
        public DateTime SentDate { get; set; }

        public virtual MergeDocument MergeDocument { get; set; }
        public virtual Survey Survey { get; set; }
    }

    internal class SurveyLetterConfiguration : EntityTypeConfiguration<SurveyLetter>
    {
        public SurveyLetterConfiguration()
        {
            ToTable("dbo.SurveyLetter");
            HasKey(x => x.SurveyLetterId);

            Property(x => x.SurveyLetterId).HasColumnName("SurveyLetterID").IsRequired();
            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.MergeDocumentId).HasColumnName("MergeDocumentID").IsRequired();
            Property(x => x.SentDate).HasColumnName("SentDate").IsRequired();

            HasRequired(a => a.Survey).WithMany(b => b.SurveyLetters).HasForeignKey(c => c.SurveyId);
            HasRequired(a => a.MergeDocument).WithMany(b => b.SurveyLetters).HasForeignKey(c => c.MergeDocumentId);
        }
    }
}
