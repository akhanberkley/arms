using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class LoginHistory
    {
        public int LoginHistoryId { get; set; }
        public DateTime AttemptedOn { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
        public bool WasSuccessful { get; set; }
    }

    internal class LoginHistoryConfiguration : EntityTypeConfiguration<LoginHistory>
    {
        public LoginHistoryConfiguration()
        {
            ToTable("dbo.LoginHistory");
            HasKey(x => x.LoginHistoryId);

            Property(x => x.LoginHistoryId).HasColumnName("LoginHistoryID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.AttemptedOn).HasColumnName("AttemptedOn").IsRequired();
            Property(x => x.Username).HasColumnName("Username").IsRequired().HasMaxLength(100);
            Property(x => x.IpAddress).HasColumnName("IPAddress").IsRequired().HasMaxLength(15);
            Property(x => x.WasSuccessful).HasColumnName("WasSuccessful").IsRequired();
        }
    }
}
