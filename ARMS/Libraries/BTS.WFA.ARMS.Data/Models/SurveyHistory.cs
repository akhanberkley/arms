using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyHistory
    {
        public Guid SurveyHistoryId { get; set; }
        public Guid SurveyId { get; set; }
        public DateTime EntryDate { get; set; }
        public string EntryText { get; set; }
        public bool IsRemoved { get; set; }

        public virtual Survey Survey { get; set; }
    }

    internal class SurveyHistoryConfiguration : EntityTypeConfiguration<SurveyHistory>
    {
        public SurveyHistoryConfiguration()
        {
            ToTable("dbo.SurveyHistory");
            HasKey(x => x.SurveyHistoryId);

            Property(x => x.SurveyHistoryId).HasColumnName("SurveyHistoryID").IsRequired();
            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.EntryDate).HasColumnName("EntryDate").IsRequired();
            Property(x => x.EntryText).HasColumnName("EntryText").IsRequired().HasMaxLength(8000);
            Property(x => x.IsRemoved).HasColumnName("IsRemoved").IsRequired();

            HasRequired(a => a.Survey).WithMany(b => b.SurveyHistories).HasForeignKey(c => c.SurveyId);
        }
    }
}
