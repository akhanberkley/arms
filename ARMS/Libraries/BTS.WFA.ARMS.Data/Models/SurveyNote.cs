using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyNote
    {
        public Guid SurveyNoteId { get; set; }
        public Guid SurveyId { get; set; }
        public Guid? UserId { get; set; }
        public string UserName { get; set; }
        public DateTime EntryDate { get; set; }
        public bool InternalOnly { get; set; }
        public string Comment { get; set; }
        public bool NoteBySurveyCreator { get; set; }
        public bool NoteByConsultant { get; set; }

        public virtual Survey Survey { get; set; }
        public virtual User User { get; set; }
    }

    internal class SurveyNoteConfiguration : EntityTypeConfiguration<SurveyNote>
    {
        public SurveyNoteConfiguration()
        {
            ToTable("dbo.SurveyNote");
            HasKey(x => x.SurveyNoteId);

            Property(x => x.SurveyNoteId).HasColumnName("SurveyNoteID").IsRequired();
            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.UserId).HasColumnName("UserID").IsOptional();
            Property(x => x.UserName).HasColumnName("UserName").IsOptional().HasMaxLength(50);
            Property(x => x.EntryDate).HasColumnName("EntryDate").IsRequired();
            Property(x => x.InternalOnly).HasColumnName("InternalOnly").IsRequired();
            Property(x => x.Comment).HasColumnName("Comment").IsRequired().HasMaxLength(8000);
            Property(x => x.NoteBySurveyCreator).HasColumnName("NoteBySurveyCreator").IsRequired();
            Property(x => x.NoteByConsultant).HasColumnName("NoteByConsultant").IsRequired();

            HasRequired(a => a.Survey).WithMany(b => b.SurveyNotes).HasForeignKey(c => c.SurveyId);
            HasOptional(a => a.User).WithMany(b => b.SurveyNotes).HasForeignKey(c => c.UserId);
        }
    }
}
