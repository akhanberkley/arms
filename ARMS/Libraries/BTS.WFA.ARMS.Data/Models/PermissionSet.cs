using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class PermissionSet
    {
        //public bool CanWorkAgencyVisits { get; set; }
        public Guid PermissionSetId { get; set; }
        public bool CanWorkSurveys { get; set; }
        public bool CanWorkReviews { get; set; }
        public bool CanWorkServicePlans { get; set; }
        public bool CanViewCompanySummary { get; set; }
        public bool CanViewSurveyReviewQueues { get; set; }
        public bool CanRequireSavedSearch { get; set; }
        public bool CanSearchSurveys { get; set; }
        public bool CanCreateSurveys { get; set; }
        public bool CanCreateServicePlans { get; set; }
        public bool CanViewLinks { get; set; }
        public bool CanAdminUserRoles { get; set; }
        public bool CanAdminUserAccounts { get; set; }
        public bool CanAdminFeeCompanies { get; set; }
        public bool CanAdminTerritories { get; set; }
        public bool CanAdminRules { get; set; }
        public bool CanAdminLetters { get; set; }
        public bool CanAdminReports { get; set; }
        public bool CanAdminResultsDisplay { get; set; }
        public bool CanAdminRecommendations { get; set; }
        public bool CanAdminServicePlanFields { get; set; }
        public bool CanAdminLinks { get; set; }
        public bool CanAdminUnderwriters { get; set; }
        public bool CanAdminClients { get; set; }
        public bool CanAdminActivityTimes { get; set; }
        public bool CanAdminHelpfulHints { get; set; }
        public bool CanAdminDocuments { get; set; }
        public bool CanViewSurveyHistory { get; set; }

        public virtual ICollection<ServicePlanActionPermission> ServicePlanActionPermissions { get; set; }
        public virtual ICollection<SurveyActionPermission> SurveyActionPermissions { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }

        public PermissionSet()
        {
            ServicePlanActionPermissions = new List<ServicePlanActionPermission>();
            SurveyActionPermissions = new List<SurveyActionPermission>();
            UserRoles = new List<UserRole>();
        }
    }

    internal class PermissionSetConfiguration : EntityTypeConfiguration<PermissionSet>
    {
        public PermissionSetConfiguration()
        {
            ToTable("dbo.PermissionSet");
            HasKey(x => x.PermissionSetId);

            Property(x => x.PermissionSetId).HasColumnName("PermissionSetID").IsRequired();
            Property(x => x.CanWorkSurveys).HasColumnName("CanWorkSurveys").IsRequired();
            Property(x => x.CanWorkReviews).HasColumnName("CanWorkReviews").IsRequired();
            Property(x => x.CanWorkServicePlans).HasColumnName("CanWorkServicePlans").IsRequired();
            Property(x => x.CanViewCompanySummary).HasColumnName("CanViewCompanySummary").IsRequired();
            Property(x => x.CanViewSurveyReviewQueues).HasColumnName("CanViewSurveyReviewQueues").IsRequired();
            Property(x => x.CanRequireSavedSearch).HasColumnName("CanRequireSavedSearch").IsRequired();
            Property(x => x.CanSearchSurveys).HasColumnName("CanSearchSurveys").IsRequired();
            Property(x => x.CanCreateSurveys).HasColumnName("CanCreateSurveys").IsRequired();
            Property(x => x.CanCreateServicePlans).HasColumnName("CanCreateServicePlans").IsRequired();
            Property(x => x.CanViewLinks).HasColumnName("CanViewLinks").IsRequired();
            Property(x => x.CanAdminUserRoles).HasColumnName("CanAdminUserRoles").IsRequired();
            Property(x => x.CanAdminUserAccounts).HasColumnName("CanAdminUserAccounts").IsRequired();
            Property(x => x.CanAdminFeeCompanies).HasColumnName("CanAdminFeeCompanies").IsRequired();
            Property(x => x.CanAdminTerritories).HasColumnName("CanAdminTerritories").IsRequired();
            Property(x => x.CanAdminRules).HasColumnName("CanAdminRules").IsRequired();
            Property(x => x.CanAdminLetters).HasColumnName("CanAdminLetters").IsRequired();
            Property(x => x.CanAdminReports).HasColumnName("CanAdminReports").IsRequired();
            Property(x => x.CanAdminResultsDisplay).HasColumnName("CanAdminResultsDisplay").IsRequired();
            Property(x => x.CanAdminRecommendations).HasColumnName("CanAdminRecommendations").IsRequired();
            Property(x => x.CanAdminServicePlanFields).HasColumnName("CanAdminServicePlanFields").IsRequired();
            Property(x => x.CanAdminLinks).HasColumnName("CanAdminLinks").IsRequired();
            Property(x => x.CanAdminUnderwriters).HasColumnName("CanAdminUnderwriters").IsRequired();
            Property(x => x.CanAdminClients).HasColumnName("CanAdminClients").IsRequired();
            Property(x => x.CanAdminActivityTimes).HasColumnName("CanAdminActivityTimes").IsRequired();
            Property(x => x.CanAdminHelpfulHints).HasColumnName("CanAdminHelpfulHints").IsRequired();
            Property(x => x.CanAdminDocuments).HasColumnName("CanAdminDocuments").IsRequired();
            Property(x => x.CanViewSurveyHistory).HasColumnName("CanViewSurveyHistory").IsRequired();
            //Property(x => x.CanWorkAgencyVisits).HasColumnName("CanWorkAgencyVisits").IsRequired();
            
        }
    }
}
