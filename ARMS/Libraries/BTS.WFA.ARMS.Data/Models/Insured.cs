using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Linq;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Insured
    {
        public Guid InsuredId { get; set; }
        public string ClientId { get; set; }
        public string PortfolioId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid? AgencyId { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public string StreetLine1 { get; set; }
        public string StreetLine2 { get; set; }
        public string StreetLine3 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string ZipCode { get; set; }
        public string BusinessOperations { get; set; }
        public string BusinessCategory { get; set; }
        public string DotNumber { get; set; }
        public string SicCode { get; set; }
        public string NaicsCode { get; set; }
        public string Underwriter { get; set; }
        public string CompanyWebsite { get; set; }
        public string AgentContactName { get; set; }
        public string AgentContactPhoneNumber { get; set; }
        public Guid? AgentEmailId { get; set; }
        public DateTime? NonrenewedDate { get; set; }
        public int ExclusionBit { get; set; }
        public string InsuredContactName { get; set; }
        public string InsuredContactPhoneNumber { get; set; }
        public string InsuredContactEmail { get; set; }

        public virtual ICollection<Coverage> Coverages { get; set; }
        public virtual ICollection<Location> Locations { get; set; }
        public virtual ICollection<Policy> Policies { get; set; }
        public virtual ICollection<ServicePlan> ServicePlans { get; set; }
        public virtual ICollection<Survey> Surveys { get; set; }

        public virtual Agency Agency { get; set; }
        public virtual AgencyEmail AgentEmail { get; set; }
        public virtual Company Company { get; set; }
        public virtual Sic Sic { get; set; }
        public virtual State State { get; set; }

        public Insured()
        {
            Coverages = new List<Coverage>();
            Locations = new List<Location>();
            Policies = new List<Policy>();
            ServicePlans = new List<ServicePlan>();
            Surveys = new List<Survey>();
        }
    }

    internal class InsuredConfiguration : EntityTypeConfiguration<Insured>
    {
        public InsuredConfiguration()
        {
            ToTable("dbo.Insured");
            HasKey(x => x.InsuredId);

            Property(x => x.InsuredId).HasColumnName("InsuredID").IsRequired();
            Property(x => x.ClientId).HasColumnName("ClientID").IsOptional().HasMaxLength(50);
            Property(x => x.PortfolioId).HasColumnName("PortfolioID").IsOptional().HasMaxLength(50);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.AgencyId).HasColumnName("AgencyID").IsOptional();
            Property(x => x.Name).HasColumnName("Name").IsOptional().HasMaxLength(200);
            Property(x => x.Name2).HasColumnName("Name2").IsOptional().HasMaxLength(200);
            Property(x => x.StreetLine1).HasColumnName("StreetLine1").IsOptional().HasMaxLength(150);
            Property(x => x.StreetLine2).HasColumnName("StreetLine2").IsOptional().HasMaxLength(150);
            Property(x => x.StreetLine3).HasColumnName("StreetLine3").IsOptional().HasMaxLength(150);
            Property(x => x.City).HasColumnName("City").IsOptional().HasMaxLength(30);
            Property(x => x.StateCode).HasColumnName("StateCode").IsOptional().HasMaxLength(2);
            Property(x => x.ZipCode).HasColumnName("ZipCode").IsOptional().HasMaxLength(11);
            Property(x => x.BusinessOperations).HasColumnName("BusinessOperations").IsOptional().HasMaxLength(500);
            Property(x => x.BusinessCategory).HasColumnName("BusinessCategory").IsOptional().HasMaxLength(100);
            Property(x => x.DotNumber).HasColumnName("DOTNumber").IsOptional().HasMaxLength(30);
            Property(x => x.SicCode).HasColumnName("SICCode").IsOptional().HasMaxLength(4);
            Property(x => x.NaicsCode).HasColumnName("NAICSCode").IsOptional().HasMaxLength(6);
            Property(x => x.Underwriter).HasColumnName("Underwriter").IsOptional().HasMaxLength(30);
            Property(x => x.CompanyWebsite).HasColumnName("CompanyWebsite").IsOptional().HasMaxLength(500);
            Property(x => x.AgentContactName).HasColumnName("AgentContactName").IsOptional().HasMaxLength(50);
            Property(x => x.AgentContactPhoneNumber).HasColumnName("AgentContactPhoneNumber").IsOptional().HasMaxLength(30);
            Property(x => x.AgentEmailId).HasColumnName("AgentEmailID").IsOptional();
            Property(x => x.NonrenewedDate).HasColumnName("NonrenewedDate").IsOptional();
            Property(x => x.ExclusionBit).HasColumnName("ExclusionBit").IsRequired();
            Property(x => x.InsuredContactName).HasColumnName("InsuredContactName").IsOptional().HasMaxLength(50);
            Property(x => x.InsuredContactPhoneNumber).HasColumnName("InsuredContactPhoneNumber").IsOptional().HasMaxLength(30);
            Property(x => x.InsuredContactEmail).HasColumnName("InsuredContactEmail").IsOptional().HasMaxLength(100);

            HasRequired(a => a.Company).WithMany(b => b.Insureds).HasForeignKey(c => c.CompanyId);
            HasOptional(a => a.Agency).WithMany(b => b.Insureds).HasForeignKey(c => c.AgencyId);
            HasOptional(a => a.State).WithMany(b => b.Insureds).HasForeignKey(c => c.StateCode);
            HasOptional(a => a.Sic).WithMany(b => b.Insureds).HasForeignKey(c => c.SicCode);
            HasOptional(a => a.AgentEmail).WithMany(b => b.Insureds).HasForeignKey(c => c.AgentEmailId);
        }
    }
}
