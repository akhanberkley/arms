using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyLocationPolicyCoverageName
    {
        public Guid SurveyId { get; set; }
        public Guid LocationId { get; set; }
        public Guid PolicyId { get; set; }
        public Guid CoverageNameId { get; set; }
        public string ReportTypeCode { get; set; }

        public virtual CoverageName CoverageName { get; set; }
        public virtual Location Location { get; set; }
        public virtual Policy Policy { get; set; }
        public virtual ReportType ReportType { get; set; }
        public virtual Survey Survey { get; set; }
    }

    internal class SurveyLocationPolicyCoverageNameConfiguration : EntityTypeConfiguration<SurveyLocationPolicyCoverageName>
    {
        public SurveyLocationPolicyCoverageNameConfiguration()
        {
            ToTable("dbo.Survey_LocationPolicyCoverageName");
            HasKey(x => new { x.SurveyId, x.LocationId, x.PolicyId, x.CoverageNameId });

            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.LocationId).HasColumnName("LocationID").IsRequired();
            Property(x => x.PolicyId).HasColumnName("PolicyID").IsRequired();
            Property(x => x.CoverageNameId).HasColumnName("CoverageNameID").IsRequired();
            Property(x => x.ReportTypeCode).HasColumnName("ReportTypeCode").IsOptional().HasMaxLength(1);

            HasRequired(a => a.Survey).WithMany(b => b.SurveyLocationPolicyCoverageNames).HasForeignKey(c => c.SurveyId);
            HasRequired(a => a.Location).WithMany(b => b.SurveyLocationPolicyCoverageNames).HasForeignKey(c => c.LocationId);
            HasRequired(a => a.Policy).WithMany(b => b.SurveyLocationPolicyCoverageNames).HasForeignKey(c => c.PolicyId);
            HasRequired(a => a.CoverageName).WithMany(b => b.SurveyLocationPolicyCoverageNames).HasForeignKey(c => c.CoverageNameId);
            HasOptional(a => a.ReportType).WithMany(b => b.SurveyLocationPolicyCoverageNames).HasForeignKey(c => c.ReportTypeCode);
        }
    }
}
