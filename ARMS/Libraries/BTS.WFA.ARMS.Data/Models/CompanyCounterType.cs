using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class CompanyCounterType
    {
        public string CompanyCounterTypeCode { get; set; }
        public string CompanyCounterTypeDescription { get; set; }

        public virtual ICollection<CompanyCounter> CompanyCounters { get; set; }

        public CompanyCounterType()
        {
            CompanyCounters = new List<CompanyCounter>();
        }
    }

    internal class CompanyCounterTypeConfiguration : EntityTypeConfiguration<CompanyCounterType>
    {
        public CompanyCounterTypeConfiguration()
        {
            ToTable("dbo.CompanyCounterType");
            HasKey(x => x.CompanyCounterTypeCode);

            Property(x => x.CompanyCounterTypeCode).HasColumnName("CompanyCounterTypeCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CompanyCounterTypeDescription).HasColumnName("CompanyCounterTypeDescription").IsRequired().HasMaxLength(100);
        }
    }
}
