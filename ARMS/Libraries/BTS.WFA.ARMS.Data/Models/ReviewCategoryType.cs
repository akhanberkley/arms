using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ReviewCategoryType
    {
        public Guid ReviewCategoryTypeId { get; set; }
        public Guid CompanyId { get; set; }
        public string ReviewerTypeCode { get; set; }
        public string Description { get; set; }
        public bool IsRequired { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<SurveyReviewRating> SurveyReviewRatings { get; set; }

        public virtual Company Company { get; set; }
        public virtual ReviewerType ReviewerType { get; set; }

        public ReviewCategoryType()
        {
            SurveyReviewRatings = new List<SurveyReviewRating>();
        }
    }

    internal class ReviewCategoryTypeConfiguration : EntityTypeConfiguration<ReviewCategoryType>
    {
        public ReviewCategoryTypeConfiguration()
        {
            ToTable("dbo.ReviewCategoryType");
            HasKey(x => x.ReviewCategoryTypeId);

            Property(x => x.ReviewCategoryTypeId).HasColumnName("ReviewCategoryTypeID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.ReviewerTypeCode).HasColumnName("ReviewerTypeCode").IsRequired().HasMaxLength(2);
            Property(x => x.Description).HasColumnName("Description").IsRequired().HasMaxLength(1000);
            Property(x => x.IsRequired).HasColumnName("IsRequired").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.ReviewCategoryTypes).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.ReviewerType).WithMany(b => b.ReviewCategoryTypes).HasForeignKey(c => c.ReviewerTypeCode);
        }
    }
}
