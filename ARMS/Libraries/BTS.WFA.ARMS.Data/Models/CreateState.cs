using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class CreateState
    {
        public string CreateStateCode { get; set; }
        public string CreateStateName { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<ServicePlan> ServicePlans { get; set; }
        public virtual ICollection<Survey> Surveys { get; set; }

        public CreateState()
        {
            ServicePlans = new List<ServicePlan>();
            Surveys = new List<Survey>();
        }
    }

    internal class CreateStateConfiguration : EntityTypeConfiguration<CreateState>
    {
        public CreateStateConfiguration()
        {
            ToTable("dbo.CreateState");
            HasKey(x => x.CreateStateCode);

            Property(x => x.CreateStateCode).HasColumnName("CreateStateCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CreateStateName).HasColumnName("CreateStateName").IsRequired().HasMaxLength(30);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
