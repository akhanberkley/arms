using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Agency
    {
        public Guid AgencyId { get; set; }
        public Guid CompanyId { get; set; }
        public string AgencyNumber { get; set; }
        public string AgencySubNumber { get; set; }
        public string AgencyName { get; set; }
        public string StreetLine1 { get; set; }
        public string StreetLine2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }

        public virtual ICollection<AgencyEmail> AgencyEmails { get; set; }
        public virtual ICollection<Insured> Insureds { get; set; }

        public virtual Company Company { get; set; }
        public virtual State State { get; set; }

        public Agency()
        {
            AgencyEmails = new List<AgencyEmail>();
            Insureds = new List<Insured>();
        }
    }

    internal class AgencyConfiguration : EntityTypeConfiguration<Agency>
    {
        public AgencyConfiguration()
        {
            ToTable("dbo.Agency");
            HasKey(x => x.AgencyId);

            Property(x => x.AgencyId).HasColumnName("AgencyID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.AgencyNumber).HasColumnName("AgencyNumber").IsOptional().HasMaxLength(20);
            Property(x => x.AgencySubNumber).HasColumnName("AgencySubNumber").IsOptional().HasMaxLength(20);
            Property(x => x.AgencyName).HasColumnName("AgencyName").IsOptional().HasMaxLength(100);
            Property(x => x.StreetLine1).HasColumnName("StreetLine1").IsOptional().HasMaxLength(150);
            Property(x => x.StreetLine2).HasColumnName("StreetLine2").IsOptional().HasMaxLength(150);
            Property(x => x.City).HasColumnName("City").IsOptional().HasMaxLength(30);
            Property(x => x.StateCode).HasColumnName("StateCode").IsOptional().HasMaxLength(2);
            Property(x => x.ZipCode).HasColumnName("ZipCode").IsOptional().HasMaxLength(11);
            Property(x => x.PhoneNumber).HasColumnName("PhoneNumber").IsOptional().HasMaxLength(30);
            Property(x => x.FaxNumber).HasColumnName("FaxNumber").IsOptional().HasMaxLength(30);

            HasRequired(a => a.Company).WithMany(b => b.Agencies).HasForeignKey(c => c.CompanyId);
            HasOptional(a => a.State).WithMany(b => b.Agencies).HasForeignKey(c => c.StateCode);
        }
    }
}
