using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanGridColumnFilterCompany
    {
        public Guid ServicePlanGridColumnFilterId { get; set; }
        public Guid CompanyId { get; set; }
        public int DisplayOrder { get; set; }
        public bool Display { get; set; }
        public bool Required { get; set; }

        public virtual Company Company { get; set; }
        public virtual ServicePlanGridColumnFilter ServicePlanGridColumnFilter { get; set; }
    }

    internal class ServicePlanGridColumnFilterCompanyConfiguration : EntityTypeConfiguration<ServicePlanGridColumnFilterCompany>
    {
        public ServicePlanGridColumnFilterCompanyConfiguration()
        {
            ToTable("dbo.ServicePlanGridColumnFilter_Company");
            HasKey(x => new { x.ServicePlanGridColumnFilterId, x.CompanyId });

            Property(x => x.ServicePlanGridColumnFilterId).HasColumnName("ServicePlanGridColumnFilterID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.Display).HasColumnName("Display").IsRequired();
            Property(x => x.Required).HasColumnName("Required").IsRequired();

            HasRequired(a => a.ServicePlanGridColumnFilter).WithMany(b => b.ServicePlanGridColumnFilterCompanies).HasForeignKey(c => c.ServicePlanGridColumnFilterId);
            HasRequired(a => a.Company).WithMany(b => b.ServicePlanGridColumnFilterCompanies).HasForeignKey(c => c.CompanyId);
        }
    }
}
