﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class RequestQueueSearch     //WA-141 Doug Bruce 2015-03-27 - RequestQueue DB Structure
    {
        public string RequestQueueSearchCode { get; set; }
        public string Description { get; set; }
    }

    internal class RequestQueueSearchConfiguration : EntityTypeConfiguration<RequestQueueSearch>
    {
        public RequestQueueSearchConfiguration()
        {
            ToTable("dbo.RequestQueueSearch");
            HasKey(x => x.RequestQueueSearchCode);

            Property(x => x.RequestQueueSearchCode).HasColumnName("RequestQueueSearchCode").IsRequired();
            Property(x => x.Description).HasColumnName("Description").IsOptional();
        }
    }
}
