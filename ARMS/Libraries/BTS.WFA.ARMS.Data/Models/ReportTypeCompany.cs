using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ReportTypeCompany
    {
        public Guid CompanyId { get; set; }
        public string ReportTypeCode { get; set; }
        public string ReportTypeName { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ReportType ReportType { get; set; }
    }

    internal class ReportTypeCompanyConfiguration : EntityTypeConfiguration<ReportTypeCompany>
    {
        public ReportTypeCompanyConfiguration()
        {
            ToTable("dbo.ReportType_Company");
            HasKey(x => new { x.CompanyId, x.ReportTypeCode });

            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.ReportTypeCode).HasColumnName("ReportTypeCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ReportTypeName).HasColumnName("ReportTypeName").IsRequired().HasMaxLength(10);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.ReportType).WithMany(b => b.ReportTypeCompanies).HasForeignKey(c => c.ReportTypeCode);
        }
    }
}
