using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class FormFieldDataType
    {
        public string FormFieldDataTypeCode { get; set; }
        public string FormFieldDataTypeDisplayName { get; set; }
        public string DotNetDataType { get; set; }
        public bool IsBooleanField { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<MergeDocumentField> MergeDocumentFields { get; set; }

        public FormFieldDataType()
        {
            MergeDocumentFields = new List<MergeDocumentField>();
        }
    }

    internal class FormFieldDataTypeConfiguration : EntityTypeConfiguration<FormFieldDataType>
    {
        public FormFieldDataTypeConfiguration()
        {
            ToTable("dbo.FormFieldDataType");
            HasKey(x => x.FormFieldDataTypeCode);

            Property(x => x.FormFieldDataTypeCode).HasColumnName("FormFieldDataTypeCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.FormFieldDataTypeDisplayName).HasColumnName("FormFieldDataTypeDisplayName").IsRequired().HasMaxLength(50);
            Property(x => x.DotNetDataType).HasColumnName("DotNetDataType").IsRequired().HasMaxLength(10);
            Property(x => x.IsBooleanField).HasColumnName("IsBooleanField").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
