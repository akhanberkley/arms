using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class RecClassificationType
    {
        public string RecClassificationTypeCode { get; set; }
        public string RecClassificationTypeName { get; set; }

        public virtual ICollection<RecClassification> RecClassifications { get; set; }

        public RecClassificationType()
        {
            RecClassifications = new List<RecClassification>();
        }
    }

    internal class RecClassificationTypeConfiguration : EntityTypeConfiguration<RecClassificationType>
    {
        public RecClassificationTypeConfiguration()
        {
            ToTable("dbo.RecClassificationType");
            HasKey(x => x.RecClassificationTypeCode);

            Property(x => x.RecClassificationTypeCode).HasColumnName("RecClassificationTypeCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecClassificationTypeName).HasColumnName("RecClassificationTypeName").IsRequired().HasMaxLength(50);
        }
    }
}
