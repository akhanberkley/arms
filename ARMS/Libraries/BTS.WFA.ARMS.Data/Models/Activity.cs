using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Activity
    {
        public Guid ActivityId { get; set; }
        public Guid ActivityTypeId { get; set; }
        public Guid AllocatedTo { get; set; }
        public DateTime ActivityDate { get; set; }
        public decimal? ActivityHours { get; set; }
        public decimal? ActivityDays { get; set; }
        public decimal? CallCount { get; set; }
        public string Comments { get; set; }
        public Guid? AgencyVisitedId { get; set; }

        public virtual ICollection<ActivityDocument> ActivityDocuments { get; set; }

        public virtual ActivityType ActivityType { get; set; }
        public virtual User User { get; set; }

        public Activity()
        {
            ActivityDocuments = new List<ActivityDocument>();
        }
    }

    internal class ActivityConfiguration : EntityTypeConfiguration<Activity>
    {
        public ActivityConfiguration()
        {
            ToTable("dbo.Activity");
            HasKey(x => x.ActivityId);

            Property(x => x.ActivityId).HasColumnName("ActivityID").IsRequired();
            Property(x => x.ActivityTypeId).HasColumnName("ActivityTypeID").IsRequired();
            Property(x => x.AllocatedTo).HasColumnName("AllocatedTo").IsRequired();
            Property(x => x.ActivityDate).HasColumnName("ActivityDate").IsRequired();
            Property(x => x.ActivityHours).HasColumnName("ActivityHours").IsOptional().HasPrecision(9, 2);
            Property(x => x.ActivityDays).HasColumnName("ActivityDays").IsOptional().HasPrecision(9, 2);
            Property(x => x.CallCount).HasColumnName("CallCount").IsOptional().HasPrecision(9, 2);
            Property(x => x.Comments).HasColumnName("Comments").IsOptional().HasMaxLength(6000);
            Property(x => x.AgencyVisitedId).HasColumnName("AgencyVisitedID").IsOptional();

            HasRequired(a => a.ActivityType).WithMany(b => b.Activities).HasForeignKey(c => c.ActivityTypeId);
            HasRequired(a => a.User).WithMany(b => b.Activities).HasForeignKey(c => c.AllocatedTo);
        }
    }
}
