using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyReasonType
    {
        public Guid SurveyReasonTypeId { get; set; }
        public Guid CompanyId { get; set; }
        public string SurveyReasonTypeName { get; set; }
        public int? DisplayOrder { get; set; }

        public virtual ICollection<Survey> Surveys { get; set; }

        public virtual Company Company { get; set; }

        public SurveyReasonType()
        {
            Surveys = new List<Survey>();
        }
    }

    internal class SurveyReasonTypeConfiguration : EntityTypeConfiguration<SurveyReasonType>
    {
        public SurveyReasonTypeConfiguration()
        {
            ToTable("dbo.SurveyReasonType");
            HasKey(x => x.SurveyReasonTypeId);

            Property(x => x.SurveyReasonTypeId).HasColumnName("SurveyReasonTypeID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.SurveyReasonTypeName).HasColumnName("SurveyReasonTypeName").IsOptional().HasMaxLength(50);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsOptional();

            HasRequired(a => a.Company).WithMany(b => b.SurveyReasonTypes).HasForeignKey(c => c.CompanyId);
        }
    }
}
