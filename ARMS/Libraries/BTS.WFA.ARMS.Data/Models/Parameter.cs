using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Parameter
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int DotNetDataTypeId { get; set; }
        public bool CompanyEditable { get; set; }
        public DateTime CreateDateTime { get; set; }

        public virtual ICollection<CompanyParameter> CompanyParameters { get; set; }

        public Parameter()
        {
            CompanyParameters = new List<CompanyParameter>();
        }
    }

    internal class ParameterConfiguration : EntityTypeConfiguration<Parameter>
    {
        public ParameterConfiguration()
        {
            ToTable("dbo.Parameter");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Description).HasColumnName("Description").IsRequired().HasMaxLength(30);
            Property(x => x.DotNetDataTypeId).HasColumnName("DotNetDataTypeID").IsRequired();
            Property(x => x.CompanyEditable).HasColumnName("CompanyEditable").IsRequired();
            Property(x => x.CreateDateTime).HasColumnName("CreateDateTime").IsRequired();
        }
    }
}
