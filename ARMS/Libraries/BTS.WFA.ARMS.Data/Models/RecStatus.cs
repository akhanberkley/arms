using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class RecStatus
    {
        public Guid RecStatusId { get; set; }
        public Guid CompanyId { get; set; }
        public string RecStatusTypeCode { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<SurveyRecommendation> SurveyRecommendations { get; set; }

        public virtual Company Company { get; set; }
        public virtual RecStatusType RecStatusType { get; set; }

        public RecStatus()
        {
            SurveyRecommendations = new List<SurveyRecommendation>();
        }
    }

    internal class RecStatusConfiguration : EntityTypeConfiguration<RecStatus>
    {
        public RecStatusConfiguration()
        {
            ToTable("dbo.RecStatus");
            HasKey(x => x.RecStatusId);

            Property(x => x.RecStatusId).HasColumnName("RecStatusID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.RecStatusTypeCode).HasColumnName("RecStatusTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.RecStatus).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.RecStatusType).WithMany(b => b.RecStatus).HasForeignKey(c => c.RecStatusTypeCode);
        }
    }
}
