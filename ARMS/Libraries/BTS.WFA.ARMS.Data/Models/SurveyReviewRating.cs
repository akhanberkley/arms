using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyReviewRating
    {
        public Guid SurveyReviewRatingId { get; set; }
        public Guid SurveyReviewId { get; set; }
        public Guid ReviewCategoryTypeId { get; set; }
        public Guid? ReviewRatingTypeId { get; set; }
        public string Comments { get; set; }

        public virtual ReviewCategoryType ReviewCategoryType { get; set; }
        public virtual ReviewRatingType ReviewRatingType { get; set; }
        public virtual SurveyReview SurveyReview { get; set; }
    }

    internal class SurveyReviewRatingConfiguration : EntityTypeConfiguration<SurveyReviewRating>
    {
        public SurveyReviewRatingConfiguration()
        {
            ToTable("dbo.SurveyReviewRating");
            HasKey(x => x.SurveyReviewRatingId);

            Property(x => x.SurveyReviewRatingId).HasColumnName("SurveyReviewRatingID").IsRequired();
            Property(x => x.SurveyReviewId).HasColumnName("SurveyReviewID").IsRequired();
            Property(x => x.ReviewCategoryTypeId).HasColumnName("ReviewCategoryTypeID").IsRequired();
            Property(x => x.ReviewRatingTypeId).HasColumnName("ReviewRatingTypeID").IsOptional();
            Property(x => x.Comments).HasColumnName("Comments").IsOptional().HasMaxLength(2000);

            HasRequired(a => a.SurveyReview).WithMany(b => b.SurveyReviewRatings).HasForeignKey(c => c.SurveyReviewId);
            HasRequired(a => a.ReviewCategoryType).WithMany(b => b.SurveyReviewRatings).HasForeignKey(c => c.ReviewCategoryTypeId);
            HasOptional(a => a.ReviewRatingType).WithMany(b => b.SurveyReviewRatings).HasForeignKey(c => c.ReviewRatingTypeId);
        }
    }
}
