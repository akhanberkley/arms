using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class EntityType
    {
        public string EntityTypeCode { get; set; }
        public string EntityTypeName { get; set; }

        public virtual ICollection<CompanyAttribute> CompanyAttributes { get; set; }

        public EntityType()
        {
            CompanyAttributes = new List<CompanyAttribute>();
        }
    }

    internal class EntityTypeConfiguration : EntityTypeConfiguration<EntityType>
    {
        public EntityTypeConfiguration()
        {
            ToTable("dbo.EntityType");
            HasKey(x => x.EntityTypeCode);

            Property(x => x.EntityTypeCode).HasColumnName("EntityTypeCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.EntityTypeName).HasColumnName("EntityTypeName").IsRequired().HasMaxLength(30);
        }
    }
}
