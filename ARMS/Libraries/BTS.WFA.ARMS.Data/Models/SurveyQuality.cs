using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyQuality
    {
        public Guid SurveyQualityId { get; set; }
        public Guid CompanyId { get; set; }
        public string SurveyQualityTypeCode { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<Survey> Surveys { get; set; }

        public virtual Company Company { get; set; }
        public virtual SurveyQualityType SurveyQualityType { get; set; }

        public SurveyQuality()
        {
            Surveys = new List<Survey>();
        }
    }

    internal class SurveyQualityConfiguration : EntityTypeConfiguration<SurveyQuality>
    {
        public SurveyQualityConfiguration()
        {
            ToTable("dbo.SurveyQuality");
            HasKey(x => x.SurveyQualityId);

            Property(x => x.SurveyQualityId).HasColumnName("SurveyQualityID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.SurveyQualityTypeCode).HasColumnName("SurveyQualityTypeCode").IsRequired().HasMaxLength(2);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.SurveyQualities).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.SurveyQualityType).WithMany(b => b.SurveyQualities).HasForeignKey(c => c.SurveyQualityTypeCode);
        }
    }
}
