using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanDocument
    {
        public Guid ServicePlanDocumentId { get; set; }
        public Guid ServicePlanId { get; set; }
        public string DocumentName { get; set; }
        public string MimeType { get; set; }
        public long SizeInBytes { get; set; }
        public string FileNetReference { get; set; }
        public DateTime UploadedOn { get; set; }
        public Guid? UploadedUserId { get; set; }
        public bool IsRemoved { get; set; }

        public virtual ServicePlan ServicePlan { get; set; }
        public virtual User User { get; set; }
    }

    internal class ServicePlanDocumentConfiguration : EntityTypeConfiguration<ServicePlanDocument>
    {
        public ServicePlanDocumentConfiguration()
        {
            ToTable("dbo.ServicePlanDocument");
            HasKey(x => x.ServicePlanDocumentId);

            Property(x => x.ServicePlanDocumentId).HasColumnName("ServicePlanDocumentID").IsRequired();
            Property(x => x.ServicePlanId).HasColumnName("ServicePlanID").IsRequired();
            Property(x => x.DocumentName).HasColumnName("DocumentName").IsRequired().HasMaxLength(200);
            Property(x => x.MimeType).HasColumnName("MimeType").IsRequired().HasMaxLength(100);
            Property(x => x.SizeInBytes).HasColumnName("SizeInBytes").IsRequired();
            Property(x => x.FileNetReference).HasColumnName("FileNetReference").IsRequired().HasMaxLength(100);
            Property(x => x.UploadedOn).HasColumnName("UploadedOn").IsRequired();
            Property(x => x.UploadedUserId).HasColumnName("UploadedUserID").IsOptional();
            Property(x => x.IsRemoved).HasColumnName("IsRemoved").IsRequired();

            HasRequired(a => a.ServicePlan).WithMany(b => b.ServicePlanDocuments).HasForeignKey(c => c.ServicePlanId);
            HasOptional(a => a.User).WithMany(b => b.ServicePlanDocuments).HasForeignKey(c => c.UploadedUserId);
        }
    }
}
