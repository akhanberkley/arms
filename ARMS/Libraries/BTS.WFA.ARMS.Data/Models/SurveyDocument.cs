using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyDocument
    {
        public Guid SurveyDocumentId { get; set; }
        public Guid SurveyId { get; set; }
        public string DocumentName { get; set; }
        public string MimeType { get; set; }
        public long SizeInBytes { get; set; }
        public string FileNetReference { get; set; }
        public DateTime UploadedOn { get; set; }
        public Guid? UploadUserId { get; set; }
        public string UploadUserName { get; set; }
        public string DocTypeCode { get; set; }
        public bool FileNetRemovable { get; set; }
        public bool IsRemoved { get; set; }

        public virtual Survey Survey { get; set; }
        public virtual User User { get; set; }
    }

    internal class SurveyDocumentConfiguration : EntityTypeConfiguration<SurveyDocument>
    {
        public SurveyDocumentConfiguration()
        {
            ToTable("dbo.SurveyDocument");
            HasKey(x => x.SurveyDocumentId);

            Property(x => x.SurveyDocumentId).HasColumnName("SurveyDocumentID").IsRequired();
            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.DocumentName).HasColumnName("DocumentName").IsRequired().HasMaxLength(200);
            Property(x => x.MimeType).HasColumnName("MimeType").IsRequired().HasMaxLength(100);
            Property(x => x.SizeInBytes).HasColumnName("SizeInBytes").IsRequired();
            Property(x => x.FileNetReference).HasColumnName("FileNetReference").IsRequired().HasMaxLength(100);
            Property(x => x.UploadedOn).HasColumnName("UploadedOn").IsRequired();
            Property(x => x.UploadUserId).HasColumnName("UploadUserID").IsOptional();
            Property(x => x.UploadUserName).HasColumnName("UploadUserName").IsOptional().HasMaxLength(50);
            Property(x => x.DocTypeCode).HasColumnName("DocTypeCode").IsOptional().HasMaxLength(50);
            Property(x => x.FileNetRemovable).HasColumnName("FileNetRemovable").IsRequired();
            Property(x => x.IsRemoved).HasColumnName("IsRemoved").IsRequired();

            HasRequired(a => a.Survey).WithMany(b => b.SurveyDocuments).HasForeignKey(c => c.SurveyId);
            HasOptional(a => a.User).WithMany(b => b.SurveyDocuments).HasForeignKey(c => c.UploadUserId);
        }
    }
}
