using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyStatus
    {
        public string SurveyStatusCode { get; set; }
        public string SurveyStatusName { get; set; }
        public string SurveyStatusTypeCode { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<Survey> Surveys { get; set; }
        public virtual ICollection<SurveyAction> SurveyActions { get; set; }

        public virtual SurveyStatusType SurveyStatusType { get; set; }

        public SurveyStatus()
        {
            Surveys = new List<Survey>();
            SurveyActions = new List<SurveyAction>();
        }
    }

    internal class SurveyStatusConfiguration : EntityTypeConfiguration<SurveyStatus>
    {
        public SurveyStatusConfiguration()
        {
            ToTable("dbo.SurveyStatus");
            HasKey(x => x.SurveyStatusCode);

            Property(x => x.SurveyStatusCode).HasColumnName("SurveyStatusCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SurveyStatusName).HasColumnName("SurveyStatusName").IsRequired().HasMaxLength(30);
            Property(x => x.SurveyStatusTypeCode).HasColumnName("SurveyStatusTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.SurveyStatusType).WithMany(b => b.SurveyStatus).HasForeignKey(c => c.SurveyStatusTypeCode);
        }
    }
}
