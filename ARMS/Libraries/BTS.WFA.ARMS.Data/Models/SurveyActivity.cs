using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyActivity
    {
        public Guid SurveyActivityId { get; set; }
        public Guid SurveyId { get; set; }
        public Guid ActivityTypeId { get; set; }
        public Guid AllocatedTo { get; set; }
        public DateTime ActivityDate { get; set; }
        public decimal ActivityHours { get; set; }
        public string Comments { get; set; }

        public virtual ActivityType ActivityType { get; set; }
        public virtual Survey Survey { get; set; }
        public virtual User User { get; set; }
    }

    internal class SurveyActivityConfiguration : EntityTypeConfiguration<SurveyActivity>
    {
        public SurveyActivityConfiguration()
        {
            ToTable("dbo.SurveyActivity");
            HasKey(x => x.SurveyActivityId);

            Property(x => x.SurveyActivityId).HasColumnName("SurveyActivityID").IsRequired();
            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.ActivityTypeId).HasColumnName("ActivityTypeID").IsRequired();
            Property(x => x.AllocatedTo).HasColumnName("AllocatedTo").IsRequired();
            Property(x => x.ActivityDate).HasColumnName("ActivityDate").IsRequired();
            Property(x => x.ActivityHours).HasColumnName("ActivityHours").IsRequired().HasPrecision(9, 2);
            Property(x => x.Comments).HasColumnName("Comments").IsOptional().HasMaxLength(6000);

            HasRequired(a => a.Survey).WithMany(b => b.SurveyActivities).HasForeignKey(c => c.SurveyId);
            HasRequired(a => a.ActivityType).WithMany(b => b.SurveyActivities).HasForeignKey(c => c.ActivityTypeId);
            HasRequired(a => a.User).WithMany(b => b.SurveyActivities).HasForeignKey(c => c.AllocatedTo);
        }
    }
}
