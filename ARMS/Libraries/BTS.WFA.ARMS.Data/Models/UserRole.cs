using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class UserRole
    {
        public Guid UserRoleId { get; set; }
        public Guid CompanyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid PermissionSetId { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public virtual Company Company { get; set; }
        public virtual PermissionSet PermissionSet { get; set; }

        public UserRole()
        {
            Users = new List<User>();
        }
    }

    internal class UserRoleConfiguration : EntityTypeConfiguration<UserRole>
    {
        public UserRoleConfiguration()
        {
            ToTable("dbo.UserRole");
            HasKey(x => x.UserRoleId);

            Property(x => x.UserRoleId).HasColumnName("UserRoleID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(20);
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasMaxLength(1000);
            Property(x => x.PermissionSetId).HasColumnName("PermissionSetID").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.UserRoles).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.PermissionSet).WithMany(b => b.UserRoles).HasForeignKey(c => c.PermissionSetId);
        }
    }
}
