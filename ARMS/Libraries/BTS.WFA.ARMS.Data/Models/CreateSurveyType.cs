using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class CreateSurveyType
    {
        public string CreateSurveyTypeCode { get; set; }
        public string CreateSurveyTypeDescription { get; set; }

        public virtual ICollection<Company> Companies { get; set; }

        public CreateSurveyType()
        {
            Companies = new List<Company>();
        }
    }

    internal class CreateSurveyTypeConfiguration : EntityTypeConfiguration<CreateSurveyType>
    {
        public CreateSurveyTypeConfiguration()
        {
            ToTable("dbo.CreateSurveyType");
            HasKey(x => x.CreateSurveyTypeCode);

            Property(x => x.CreateSurveyTypeCode).HasColumnName("CreateSurveyTypeCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CreateSurveyTypeDescription).HasColumnName("CreateSurveyTypeDescription").IsRequired().HasMaxLength(50);
        }
    }
}
