using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class BuildingValuationType
    {
        public string BuildingValuationTypeCode { get; set; }
        public string BuildingValuationTypeDescription { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<Building> Buildings { get; set; }

        public BuildingValuationType()
        {
            Buildings = new List<Building>();
        }
    }

    internal class BuildingValuationTypeConfiguration : EntityTypeConfiguration<BuildingValuationType>
    {
        public BuildingValuationTypeConfiguration()
        {
            ToTable("dbo.BuildingValuationType");
            HasKey(x => x.BuildingValuationTypeCode);

            Property(x => x.BuildingValuationTypeCode).HasColumnName("BuildingValuationTypeCode").IsRequired().HasMaxLength(3).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.BuildingValuationTypeDescription).HasColumnName("BuildingValuationTypeDescription").IsRequired().HasMaxLength(100);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
