using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Claim
    {
        public Guid ClaimId { get; set; }
        public Guid PolicyId { get; set; }
        public string PolicyNumber { get; set; }
        public int? PolicyMod { get; set; }
        public string PolicySymbol { get; set; }
        public DateTime? PolicyExpireDate { get; set; }
        public string ClaimNumber { get; set; }
        public DateTime LossDate { get; set; }
        public string Claimant { get; set; }
        public decimal ClaimAmount { get; set; }
        public string ClaimComment { get; set; }
        public string ClaimStatus { get; set; }
        public string LossType { get; set; }

        public virtual Policy Policy { get; set; }
    }

    internal class ClaimConfiguration : EntityTypeConfiguration<Claim>
    {
        public ClaimConfiguration()
        {
            ToTable("dbo.Claim");
            HasKey(x => x.ClaimId);

            Property(x => x.ClaimId).HasColumnName("ClaimID").IsRequired();
            Property(x => x.PolicyId).HasColumnName("PolicyID").IsRequired();
            Property(x => x.PolicyNumber).HasColumnName("PolicyNumber").IsOptional().HasMaxLength(50);
            Property(x => x.PolicyMod).HasColumnName("PolicyMod").IsOptional();
            Property(x => x.PolicySymbol).HasColumnName("PolicySymbol").IsOptional().HasMaxLength(3);
            Property(x => x.PolicyExpireDate).HasColumnName("PolicyExpireDate").IsOptional();
            Property(x => x.ClaimNumber).HasColumnName("ClaimNumber").IsRequired().HasMaxLength(30);
            Property(x => x.LossDate).HasColumnName("LossDate").IsRequired();
            Property(x => x.Claimant).HasColumnName("Claimant").IsRequired().HasMaxLength(100);
            Property(x => x.ClaimAmount).HasColumnName("ClaimAmount").IsRequired().HasPrecision(19, 4);
            Property(x => x.ClaimComment).HasColumnName("ClaimComment").IsRequired().HasMaxLength(2000);
            Property(x => x.ClaimStatus).HasColumnName("ClaimStatus").IsOptional().HasMaxLength(10);
            Property(x => x.LossType).HasColumnName("LossType").IsOptional().HasMaxLength(70);

            HasRequired(a => a.Policy).WithMany(b => b.Claims).HasForeignKey(c => c.PolicyId);
        }
    }
}
