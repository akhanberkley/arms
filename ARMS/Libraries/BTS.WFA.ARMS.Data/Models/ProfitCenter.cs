using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ProfitCenter
    {
        public string ProfitCenterCode { get; set; }
        public Guid CompanyId { get; set; }
        public string ProfitCenterName { get; set; }

        public virtual ICollection<Underwriter> Underwriters { get; set; }
        public virtual ICollection<User> Users { get; set; }

        public virtual Company Company { get; set; }

        public ProfitCenter()
        {
            Underwriters = new List<Underwriter>();
            Users = new List<User>();
        }
    }

    internal class ProfitCenterConfiguration : EntityTypeConfiguration<ProfitCenter>
    {
        public ProfitCenterConfiguration()
        {
            ToTable("dbo.ProfitCenter");
            HasKey(x => x.ProfitCenterCode);

            Property(x => x.ProfitCenterCode).HasColumnName("ProfitCenterCode").IsRequired().HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.ProfitCenterName).HasColumnName("ProfitCenterName").IsRequired().HasMaxLength(50);

            HasRequired(a => a.Company).WithMany(b => b.ProfitCenters).HasForeignKey(c => c.CompanyId);
        }
    }
}
