﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace BTS.WFA.ARMS.Data.Models
{
    public class RequestQueue   //WA-141 Doug Bruce 2015-03-26 - RequestQueue DB Structure
    {
        public int RequestQueueID { get; set; }
        public Guid? SurveyId { get; set; }
        public Guid? CompanyId { get; set; }
        public string StatusCode { get; set; }
        public string InputTypeCode { get; set; }
        public string InputValue { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateUser { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public string LastModifiedUser { get; set; }
        public DateTime? LastRetryDate { get; set; }
        public string LastRetryCount { get; set; }
        public DateTime? TrashDate { get; set; }
        public string DataClientCore { get; set; }
        public string DataBCS { get; set; }
        public string DataAPS { get; set; }
        public string DataAPlus { get; set; }
        public string DataBUS { get; set; }
        public string DataGenesys { get; set; }

        // Foreign keys
        public virtual Survey Survey { get; set; } //  FK_Request_Survey
    }

    internal class RequestQueueConfiguration : EntityTypeConfiguration<RequestQueue>
    {
        public RequestQueueConfiguration()
        {
            ToTable("dbo.RequestQueue");
            HasKey(x => x.RequestQueueID);

            Property(x => x.RequestQueueID).HasColumnName("RequestQueueID").IsRequired();
            Property(x => x.SurveyId).HasColumnName("SurveyID").IsOptional();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsOptional();
            Property(x => x.StatusCode).HasColumnName("StatusCode").IsOptional().HasMaxLength(2);
            Property(x => x.InputTypeCode).HasColumnName("InputTypeCode").IsOptional().HasMaxLength(2);
            Property(x => x.InputValue).HasColumnName("InputValue").IsOptional();
            Property(x => x.CreateDate).HasColumnName("CreateDate").IsOptional();
            Property(x => x.CreateUser).HasColumnName("CreateUser").IsOptional();
            Property(x => x.LastModifiedDate).HasColumnName("LastModifiedDate").IsOptional();
            Property(x => x.LastModifiedUser).HasColumnName("LastModifiedUser").IsOptional();
            Property(x => x.LastRetryDate).HasColumnName("LastRetryDate").IsOptional();
            Property(x => x.LastRetryCount).HasColumnName("LastRetryCount").IsOptional();
            Property(x => x.TrashDate).HasColumnName("TrashDate").IsOptional();
            Property(x => x.DataClientCore).HasColumnName("DataClientCore").IsOptional();
            Property(x => x.DataBCS).HasColumnName("DataBCS").IsOptional();
            Property(x => x.DataAPS).HasColumnName("DataAPS").IsOptional();
            Property(x => x.DataAPlus).HasColumnName("DataAPlus").IsOptional();
            Property(x => x.DataGenesys).HasColumnName("DataGenesys").IsOptional();

            // Foreign keys
            HasOptional(a => a.Survey).WithMany().HasForeignKey(c => c.SurveyId); // FK_Request_Survey
        }
    }
}
