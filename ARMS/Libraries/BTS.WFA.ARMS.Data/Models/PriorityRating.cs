using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class PriorityRating
    {
        public Guid PriorityRatingId { get; set; }
        public Guid CompanyId { get; set; }
        public string PriorityRatingName { get; set; }
        public int DisplayOrder { get; set; }
        public string Description { get; set; }

        public virtual Company Company { get; set; }
    }

    internal class PriorityRatingConfiguration : EntityTypeConfiguration<PriorityRating>
    {
        public PriorityRatingConfiguration()
        {
            ToTable("dbo.PriorityRating");
            HasKey(x => x.PriorityRatingId);

            Property(x => x.PriorityRatingId).HasColumnName("PriorityRatingID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.PriorityRatingName).HasColumnName("PriorityRatingName").IsRequired().HasMaxLength(500);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasMaxLength(50);

            HasRequired(a => a.Company).WithMany(b => b.PriorityRatings).HasForeignKey(c => c.CompanyId);
        }
    }
}
