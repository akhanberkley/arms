using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyRecommendation
    {
        public Guid SurveyRecommendationId { get; set; }
        public Guid SurveyId { get; set; }
        public Guid RecommendationId { get; set; }
        public string RecommendationNumber { get; set; }
        public string EntryText { get; set; }
        public string Comments { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateCompleted { get; set; }
        public Guid? RecClassificationId { get; set; }
        public Guid? RecStatusId { get; set; }
        public int PriorityIndex { get; set; }

        public virtual RecClassification RecClassification { get; set; }
        public virtual Recommendation Recommendation { get; set; }
        public virtual RecStatus RecStatus { get; set; }
        public virtual Survey Survey { get; set; }
    }

    internal class SurveyRecommendationConfiguration : EntityTypeConfiguration<SurveyRecommendation>
    {
        public SurveyRecommendationConfiguration()
        {
            ToTable("dbo.Survey_Recommendation");
            HasKey(x => x.SurveyRecommendationId);

            Property(x => x.SurveyRecommendationId).HasColumnName("SurveyRecommendationID").IsRequired();
            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.RecommendationId).HasColumnName("RecommendationID").IsRequired();
            Property(x => x.RecommendationNumber).HasColumnName("RecommendationNumber").IsOptional().HasMaxLength(30);
            Property(x => x.EntryText).HasColumnName("EntryText").IsOptional().HasMaxLength(2147483647);
            Property(x => x.Comments).HasColumnName("Comments").IsOptional().HasMaxLength(4000);
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsOptional();
            Property(x => x.DateCompleted).HasColumnName("DateCompleted").IsOptional();
            Property(x => x.RecClassificationId).HasColumnName("RecClassificationID").IsOptional();
            Property(x => x.RecStatusId).HasColumnName("RecStatusID").IsOptional();
            Property(x => x.PriorityIndex).HasColumnName("PriorityIndex").IsRequired();

            HasRequired(a => a.Survey).WithMany(b => b.SurveyRecommendations).HasForeignKey(c => c.SurveyId);
            HasRequired(a => a.Recommendation).WithMany(b => b.SurveyRecommendations).HasForeignKey(c => c.RecommendationId);
            HasOptional(a => a.RecClassification).WithMany(b => b.SurveyRecommendations).HasForeignKey(c => c.RecClassificationId);
            HasOptional(a => a.RecStatus).WithMany(b => b.SurveyRecommendations).HasForeignKey(c => c.RecStatusId);
        }
    }
}
