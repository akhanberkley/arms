using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class WebPageHelpfulHint
    {
        public string WebPagePath { get; set; }
        public Guid CompanyId { get; set; }
        public string WebPageDescription { get; set; }
        public string HelpfulHintText { get; set; }
        public bool AlwaysVisible { get; set; }

        public virtual Company Company { get; set; }
        public virtual WebPage WebPage { get; set; }
    }

    internal class WebPageHelpfulHintConfiguration : EntityTypeConfiguration<WebPageHelpfulHint>
    {
        public WebPageHelpfulHintConfiguration()
        {
            ToTable("dbo.WebPageHelpfulHint");
            HasKey(x => new { x.WebPagePath, x.CompanyId });

            Property(x => x.WebPagePath).HasColumnName("WebPagePath").IsRequired().HasMaxLength(100).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.WebPageDescription).HasColumnName("WebPageDescription").IsRequired().HasMaxLength(100);
            Property(x => x.HelpfulHintText).HasColumnName("HelpfulHintText").IsOptional().HasMaxLength(5000);
            Property(x => x.AlwaysVisible).HasColumnName("AlwaysVisible").IsRequired();

            HasRequired(a => a.WebPage).WithMany(b => b.WebPageHelpfulHints).HasForeignKey(c => c.WebPagePath);
            HasRequired(a => a.Company).WithMany(b => b.WebPageHelpfulHints).HasForeignKey(c => c.CompanyId);
        }
    }
}
