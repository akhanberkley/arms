using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class CoverageNameType
    {
        public string CoverageNameTypeCode { get; set; }
        public string CoverageNameTypeName { get; set; }

        public virtual ICollection<CoverageName> CoverageNames { get; set; }

        public CoverageNameType()
        {
            CoverageNames = new List<CoverageName>();
        }
    }

    internal class CoverageNameTypeConfiguration : EntityTypeConfiguration<CoverageNameType>
    {
        public CoverageNameTypeConfiguration()
        {
            ToTable("dbo.CoverageNameType");
            HasKey(x => x.CoverageNameTypeCode);

            Property(x => x.CoverageNameTypeCode).HasColumnName("CoverageNameTypeCode").IsRequired().HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CoverageNameTypeName).HasColumnName("CoverageNameTypeName").IsRequired().HasMaxLength(60);
        }
    }
}
