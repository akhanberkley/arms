using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class AssignmentRuleAction
    {
        public string AssignmentRuleActionCode { get; set; }
        public string AssignmentRuleActionTypeCode { get; set; }
        public string DisplayName { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<AssignmentRule> AssignmentRules { get; set; }

        public virtual AssignmentRuleActionType AssignmentRuleActionType { get; set; }

        public AssignmentRuleAction()
        {
            AssignmentRules = new List<AssignmentRule>();
        }
    }

    internal class AssignmentRuleActionConfiguration : EntityTypeConfiguration<AssignmentRuleAction>
    {
        public AssignmentRuleActionConfiguration()
        {
            ToTable("dbo.AssignmentRuleAction");
            HasKey(x => x.AssignmentRuleActionCode);

            Property(x => x.AssignmentRuleActionCode).HasColumnName("AssignmentRuleActionCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.AssignmentRuleActionTypeCode).HasColumnName("AssignmentRuleActionTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.DisplayName).HasColumnName("DisplayName").IsRequired().HasMaxLength(50);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.AssignmentRuleActionType).WithMany(b => b.AssignmentRuleActions).HasForeignKey(c => c.AssignmentRuleActionTypeCode);
        }
    }
}
