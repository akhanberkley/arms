using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Territory
    {
        public Guid TerritoryId { get; set; }
        public Guid CompanyId { get; set; }
        public string SurveyStatusTypeCode { get; set; }
        public string TerritoryName { get; set; }
        public string TerritoryDescription { get; set; }
        public bool InHouseSelection { get; set; }

        public virtual ICollection<GeographicArea> GeographicAreas { get; set; }
        public virtual ICollection<UserTerritory> UserTerritories { get; set; }

        public virtual Company Company { get; set; }
        public virtual SurveyStatusType SurveyStatusType { get; set; }

        public Territory()
        {
            GeographicAreas = new List<GeographicArea>();
            UserTerritories = new List<UserTerritory>();
        }
    }

    internal class TerritoryConfiguration : EntityTypeConfiguration<Territory>
    {
        public TerritoryConfiguration()
        {
            ToTable("dbo.Territory");
            HasKey(x => x.TerritoryId);

            Property(x => x.TerritoryId).HasColumnName("TerritoryID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.SurveyStatusTypeCode).HasColumnName("SurveyStatusTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.TerritoryName).HasColumnName("TerritoryName").IsRequired().HasMaxLength(200);
            Property(x => x.TerritoryDescription).HasColumnName("TerritoryDescription").IsOptional().HasMaxLength(1000);
            Property(x => x.InHouseSelection).HasColumnName("InHouseSelection").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.Territories).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.SurveyStatusType).WithMany(b => b.Territories).HasForeignKey(c => c.SurveyStatusTypeCode);
        }
    }
}
