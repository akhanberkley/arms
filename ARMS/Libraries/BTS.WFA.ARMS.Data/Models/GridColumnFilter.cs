using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class GridColumnFilter
    {
        public Guid GridColumnFilterId { get; set; }
        public string Name { get; set; }
        public string FilterExpression { get; set; }
        public string SortExpression { get; set; }
        public string DotNetDataType { get; set; }

        public virtual ICollection<GridColumnFilterCompany> GridColumnFilterCompanies { get; set; }

        public GridColumnFilter()
        {
            GridColumnFilterCompanies = new List<GridColumnFilterCompany>();
        }
    }

    internal class GridColumnFilterConfiguration : EntityTypeConfiguration<GridColumnFilter>
    {
        public GridColumnFilterConfiguration()
        {
            ToTable("dbo.GridColumnFilter");
            HasKey(x => x.GridColumnFilterId);

            Property(x => x.GridColumnFilterId).HasColumnName("GridColumnFilterID").IsRequired();
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(50);
            Property(x => x.FilterExpression).HasColumnName("FilterExpression").IsRequired().HasMaxLength(50);
            Property(x => x.SortExpression).HasColumnName("SortExpression").IsRequired().HasMaxLength(100);
            Property(x => x.DotNetDataType).HasColumnName("DotNetDataType").IsRequired().HasMaxLength(50);
        }
    }
}
