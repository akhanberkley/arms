using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ClientAttributeType
    {
        public string ClientAttributeTypeCode { get; set; }
        public string ClientAttributeTypeName { get; set; }
        public string ClientCoreGroupName { get; set; }
        public string DotNetDataType { get; set; }
        public string SourceTable { get; set; }
        public string SourceColumn { get; set; }
        public string FilterExpression { get; set; }
        public string SortExpression { get; set; }

        public virtual ICollection<ClientAttribute> ClientAttributes { get; set; }

        public ClientAttributeType()
        {
            ClientAttributes = new List<ClientAttribute>();
        }
    }

    internal class ClientAttributeTypeConfiguration : EntityTypeConfiguration<ClientAttributeType>
    {
        public ClientAttributeTypeConfiguration()
        {
            ToTable("dbo.ClientAttributeType");
            HasKey(x => x.ClientAttributeTypeCode);

            Property(x => x.ClientAttributeTypeCode).HasColumnName("ClientAttributeTypeCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ClientAttributeTypeName).HasColumnName("ClientAttributeTypeName").IsRequired().HasMaxLength(50);
            Property(x => x.ClientCoreGroupName).HasColumnName("ClientCoreGroupName").IsOptional().HasMaxLength(50);
            Property(x => x.DotNetDataType).HasColumnName("DotNetDataType").IsRequired().HasMaxLength(50);
            Property(x => x.SourceTable).HasColumnName("SourceTable").IsOptional().HasMaxLength(30);
            Property(x => x.SourceColumn).HasColumnName("SourceColumn").IsOptional().HasMaxLength(30);
            Property(x => x.FilterExpression).HasColumnName("FilterExpression").IsOptional().HasMaxLength(300);
            Property(x => x.SortExpression).HasColumnName("SortExpression").IsOptional().HasMaxLength(100);
        }
    }
}
