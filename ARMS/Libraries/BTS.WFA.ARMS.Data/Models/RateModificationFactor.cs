using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class RateModificationFactor
    {
        public Guid RateModificationFactorId { get; set; }
        public Guid PolicyId { get; set; }
        public string StateCode { get; set; }
        public string RateModificationFactorTypeCode { get; set; }
        public decimal Rate { get; set; }

        public virtual Policy Policy { get; set; }
        public virtual RateModificationFactorType RateModificationFactorType { get; set; }
        public virtual State State { get; set; }
    }

    internal class RateModificationFactorConfiguration : EntityTypeConfiguration<RateModificationFactor>
    {
        public RateModificationFactorConfiguration()
        {
            ToTable("dbo.RateModificationFactor");
            HasKey(x => x.RateModificationFactorId);

            Property(x => x.RateModificationFactorId).HasColumnName("RateModificationFactorID").IsRequired();
            Property(x => x.PolicyId).HasColumnName("PolicyID").IsRequired();
            Property(x => x.StateCode).HasColumnName("StateCode").IsRequired().HasMaxLength(2);
            Property(x => x.RateModificationFactorTypeCode).HasColumnName("RateModificationFactorTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.Rate).HasColumnName("Rate").IsRequired().HasPrecision(9, 3);

            HasRequired(a => a.Policy).WithMany(b => b.RateModificationFactors).HasForeignKey(c => c.PolicyId);
            HasRequired(a => a.State).WithMany(b => b.RateModificationFactors).HasForeignKey(c => c.StateCode);
            HasRequired(a => a.RateModificationFactorType).WithMany(b => b.RateModificationFactors).HasForeignKey(c => c.RateModificationFactorTypeCode);
        }
    }
}
