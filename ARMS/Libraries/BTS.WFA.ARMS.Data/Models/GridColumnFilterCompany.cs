using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class GridColumnFilterCompany
    {
        public Guid GridColumnFilterId { get; set; }
        public Guid CompanyId { get; set; }
        public int DisplayOrder { get; set; }
        public bool Display { get; set; }
        public bool Required { get; set; }
        public bool IsExternal { get; set; }

        public virtual Company Company { get; set; }
        public virtual GridColumnFilter GridColumnFilter { get; set; }
    }

    internal class GridColumnFilterCompanyConfiguration : EntityTypeConfiguration<GridColumnFilterCompany>
    {
        public GridColumnFilterCompanyConfiguration()
        {
            ToTable("dbo.GridColumnFilter_Company");
            HasKey(x => new { x.GridColumnFilterId, x.CompanyId, x.IsExternal });

            Property(x => x.GridColumnFilterId).HasColumnName("GridColumnFilterID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.Display).HasColumnName("Display").IsRequired();
            Property(x => x.Required).HasColumnName("Required").IsRequired();
            Property(x => x.IsExternal).HasColumnName("IsExternal").IsRequired();

            HasRequired(a => a.GridColumnFilter).WithMany(b => b.GridColumnFilterCompanies).HasForeignKey(c => c.GridColumnFilterId);
            HasRequired(a => a.Company).WithMany(b => b.GridColumnFilterCompanies).HasForeignKey(c => c.CompanyId);
        }
    }
}
