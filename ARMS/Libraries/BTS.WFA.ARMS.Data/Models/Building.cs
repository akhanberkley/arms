using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Building
    {
        public Guid BuildingId { get; set; }
        public Guid LocationId { get; set; }
        public short? BuildingNumber { get; set; }
        public int? YearBuilt { get; set; }
        public short? HasSprinklerSystem { get; set; }
        public short? HasSprinklerSystemCredit { get; set; }
        public int? PublicProtection { get; set; }
        public decimal? StockValues { get; set; }
        public decimal? Contents { get; set; }
        public decimal? BuildingValue { get; set; }
        public decimal? BusinessInterruption { get; set; }
        public string LocationOccupancy { get; set; }
        public string BuildingValuationTypeCode { get; set; }
        public decimal? CoinsurancePercentage { get; set; }
        public decimal? ChangeInEnvironmentalControl { get; set; }
        public decimal? ScientificAnimals { get; set; }
        public decimal? Contamination { get; set; }
        public decimal? RadioActiveContamination { get; set; }
        public decimal? Flood { get; set; }
        public decimal? Earthquake { get; set; }
        public short? Itv { get; set; }

        public virtual BuildingValuationType BuildingValuationType { get; set; }
        public virtual Location Location { get; set; }
    }

    internal class BuildingConfiguration : EntityTypeConfiguration<Building>
    {
        public BuildingConfiguration()
        {
            ToTable("dbo.Building");
            HasKey(x => x.BuildingId);

            Property(x => x.BuildingId).HasColumnName("BuildingID").IsRequired();
            Property(x => x.LocationId).HasColumnName("LocationID").IsRequired();
            Property(x => x.BuildingNumber).HasColumnName("BuildingNumber").IsOptional();
            Property(x => x.YearBuilt).HasColumnName("YearBuilt").IsOptional();
            Property(x => x.HasSprinklerSystem).HasColumnName("HasSprinklerSystem").IsOptional();
            Property(x => x.HasSprinklerSystemCredit).HasColumnName("HasSprinklerSystemCredit").IsOptional();
            Property(x => x.PublicProtection).HasColumnName("PublicProtection").IsOptional();
            Property(x => x.StockValues).HasColumnName("StockValues").IsOptional();
            Property(x => x.Contents).HasColumnName("Contents").IsOptional();
            Property(x => x.BuildingValue).HasColumnName("BuildingValue").IsOptional();
            Property(x => x.BusinessInterruption).HasColumnName("BusinessInterruption").IsOptional();
            Property(x => x.LocationOccupancy).HasColumnName("LocationOccupancy").IsOptional().HasMaxLength(500);
            Property(x => x.BuildingValuationTypeCode).HasColumnName("BuildingValuationTypeCode").IsOptional().HasMaxLength(3);
            Property(x => x.CoinsurancePercentage).HasColumnName("CoinsurancePercentage").IsOptional().HasPrecision(9, 2);
            Property(x => x.ChangeInEnvironmentalControl).HasColumnName("ChangeInEnvironmentalControl").IsOptional();
            Property(x => x.ScientificAnimals).HasColumnName("ScientificAnimals").IsOptional();
            Property(x => x.Contamination).HasColumnName("Contamination").IsOptional();
            Property(x => x.RadioActiveContamination).HasColumnName("RadioActiveContamination").IsOptional();
            Property(x => x.Flood).HasColumnName("Flood").IsOptional();
            Property(x => x.Earthquake).HasColumnName("Earthquake").IsOptional();
            Property(x => x.Itv).HasColumnName("ITV").IsOptional();

            HasRequired(a => a.Location).WithMany(b => b.Buildings).HasForeignKey(c => c.LocationId);
            HasOptional(a => a.BuildingValuationType).WithMany(b => b.Buildings).HasForeignKey(c => c.BuildingValuationTypeCode);
        }
    }
}
