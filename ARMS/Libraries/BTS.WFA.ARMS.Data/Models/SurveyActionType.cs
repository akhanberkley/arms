using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyActionType
    {
        public string SurveyActionTypeCode { get; set; }
        public string ActionName { get; set; }

        public virtual ICollection<SurveyAction> SurveyActions { get; set; }

        public SurveyActionType()
        {
            SurveyActions = new List<SurveyAction>();
        }
    }

    internal class SurveyActionTypeConfiguration : EntityTypeConfiguration<SurveyActionType>
    {
        public SurveyActionTypeConfiguration()
        {
            ToTable("dbo.SurveyActionType");
            HasKey(x => x.SurveyActionTypeCode);

            Property(x => x.SurveyActionTypeCode).HasColumnName("SurveyActionTypeCode").IsRequired().HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ActionName).HasColumnName("ActionName").IsRequired().HasMaxLength(50);
        }
    }
}
