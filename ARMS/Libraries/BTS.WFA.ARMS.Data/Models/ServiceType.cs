using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServiceType
    {
        public string ServiceTypeCode { get; set; }
        public string ServiceTypeName { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<Survey> Surveys { get; set; }
        public virtual ICollection<SurveyActionPermission> SurveyActionPermissions { get; set; }
        public virtual ICollection<SurveyType> SurveyTypes { get; set; }

        public ServiceType()
        {
            Surveys = new List<Survey>();
            SurveyActionPermissions = new List<SurveyActionPermission>();
            SurveyTypes = new List<SurveyType>();
        }
    }

    internal class ServiceTypeConfiguration : EntityTypeConfiguration<ServiceType>
    {
        public ServiceTypeConfiguration()
        {
            ToTable("dbo.ServiceType");
            HasKey(x => x.ServiceTypeCode);

            Property(x => x.ServiceTypeCode).HasColumnName("ServiceTypeCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ServiceTypeName).HasColumnName("ServiceTypeName").IsRequired().HasMaxLength(50);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
