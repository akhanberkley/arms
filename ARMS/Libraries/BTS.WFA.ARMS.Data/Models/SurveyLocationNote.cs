using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyLocationNote
    {
        public Guid SurveyId { get; set; }
        public Guid LocationId { get; set; }
        public Guid? UserId { get; set; }
        public string UserName { get; set; }
        public DateTime EntryDate { get; set; }
        public string Comment { get; set; }

        public virtual Location Location { get; set; }
        public virtual Survey Survey { get; set; }
        public virtual User User { get; set; }
    }

    internal class SurveyLocationNoteConfiguration : EntityTypeConfiguration<SurveyLocationNote>
    {
        public SurveyLocationNoteConfiguration()
        {
            ToTable("dbo.Survey_LocationNote");
            HasKey(x => new { x.SurveyId, x.LocationId });

            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.LocationId).HasColumnName("LocationID").IsRequired();
            Property(x => x.UserId).HasColumnName("UserID").IsOptional();
            Property(x => x.UserName).HasColumnName("UserName").IsOptional().HasMaxLength(50);
            Property(x => x.EntryDate).HasColumnName("EntryDate").IsRequired();
            Property(x => x.Comment).HasColumnName("Comment").IsRequired().HasMaxLength(8000);

            HasRequired(a => a.Survey).WithMany(b => b.SurveyLocationNotes).HasForeignKey(c => c.SurveyId);
            HasRequired(a => a.Location).WithMany(b => b.SurveyLocationNotes).HasForeignKey(c => c.LocationId);
            HasOptional(a => a.User).WithMany(b => b.SurveyLocationNotes).HasForeignKey(c => c.UserId);
        }
    }
}
