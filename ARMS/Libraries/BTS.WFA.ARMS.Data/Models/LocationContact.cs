using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class LocationContact
    {
        public Guid LocationContactId { get; set; }
        public Guid LocationId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string PhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string EmailAddress { get; set; }
        public string ContactReasonTypeCode { get; set; }
        public string CompanyName { get; set; }
        public string StreetLine1 { get; set; }
        public string StreetLine2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string ZipCode { get; set; }
        public int PriorityIndex { get; set; }
        public bool Modified { get; set; }
        public string PolicySystemKey { get; set; }

        public virtual ContactReasonType ContactReasonType { get; set; }
        public virtual State State { get; set; }
    }

    internal class LocationContactConfiguration : EntityTypeConfiguration<LocationContact>
    {
        public LocationContactConfiguration()
        {
            ToTable("dbo.LocationContact");
            HasKey(x => x.LocationContactId);

            Property(x => x.LocationContactId).HasColumnName("LocationContactID").IsRequired();
            Property(x => x.LocationId).HasColumnName("LocationID").IsRequired();
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(225);
            Property(x => x.Title).HasColumnName("Title").IsOptional().HasMaxLength(70);
            Property(x => x.PhoneNumber).HasColumnName("PhoneNumber").IsOptional().HasMaxLength(40);
            Property(x => x.AlternatePhoneNumber).HasColumnName("AlternatePhoneNumber").IsOptional().HasMaxLength(30);
            Property(x => x.FaxNumber).HasColumnName("FaxNumber").IsOptional().HasMaxLength(30);
            Property(x => x.EmailAddress).HasColumnName("EmailAddress").IsOptional().HasMaxLength(100);
            Property(x => x.ContactReasonTypeCode).HasColumnName("ContactReasonTypeCode").IsOptional().HasMaxLength(20);
            Property(x => x.CompanyName).HasColumnName("CompanyName").IsOptional().HasMaxLength(200);
            Property(x => x.StreetLine1).HasColumnName("StreetLine1").IsOptional().HasMaxLength(150);
            Property(x => x.StreetLine2).HasColumnName("StreetLine2").IsOptional().HasMaxLength(150);
            Property(x => x.City).HasColumnName("City").IsOptional().HasMaxLength(30);
            Property(x => x.StateCode).HasColumnName("StateCode").IsOptional().HasMaxLength(2);
            Property(x => x.ZipCode).HasColumnName("ZipCode").IsOptional().HasMaxLength(11);
            Property(x => x.PriorityIndex).HasColumnName("PriorityIndex").IsRequired();
            Property(x => x.Modified).HasColumnName("Modified").IsRequired();

            HasOptional(a => a.ContactReasonType).WithMany(b => b.LocationContacts).HasForeignKey(c => c.ContactReasonTypeCode);
            HasOptional(a => a.State).WithMany(b => b.LocationContacts).HasForeignKey(c => c.StateCode);
        }
    }
}
