using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class MergeDocumentType
    {
        public int MergeDocumentTypeId { get; set; }
        public string MergeDocumentTypeName { get; set; }

        public virtual ICollection<MergeDocument> MergeDocuments { get; set; }

        public MergeDocumentType()
        {
            MergeDocuments = new List<MergeDocument>();
        }
    }

    internal class MergeDocumentTypeConfiguration : EntityTypeConfiguration<MergeDocumentType>
    {
        public MergeDocumentTypeConfiguration()
        {
            ToTable("dbo.MergeDocumentType");
            HasKey(x => x.MergeDocumentTypeId);

            Property(x => x.MergeDocumentTypeId).HasColumnName("MergeDocumentTypeID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.MergeDocumentTypeName).HasColumnName("MergeDocumentTypeName").IsRequired().HasMaxLength(50);
        }
    }
}
