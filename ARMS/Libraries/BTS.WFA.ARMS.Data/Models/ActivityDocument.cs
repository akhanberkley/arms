using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ActivityDocument
    {
        public Guid ActivityDocumentId { get; set; }
        public Guid ActivityId { get; set; }
        public string DocumentName { get; set; }
        public string MimeType { get; set; }
        public long SizeInBytes { get; set; }
        public string FileNetReference { get; set; }
        public DateTime UploadedOn { get; set; }
        public Guid UploadUserId { get; set; }

        public virtual Activity Activity { get; set; }
        public virtual User User { get; set; }
    }

    internal class ActivityDocumentConfiguration : EntityTypeConfiguration<ActivityDocument>
    {
        public ActivityDocumentConfiguration()
        {
            ToTable("dbo.ActivityDocument");
            HasKey(x => x.ActivityDocumentId);

            Property(x => x.ActivityDocumentId).HasColumnName("ActivityDocumentID").IsRequired();
            Property(x => x.ActivityId).HasColumnName("ActivityID").IsRequired();
            Property(x => x.DocumentName).HasColumnName("DocumentName").IsRequired().HasMaxLength(200);
            Property(x => x.MimeType).HasColumnName("MimeType").IsRequired().HasMaxLength(100);
            Property(x => x.SizeInBytes).HasColumnName("SizeInBytes").IsRequired();
            Property(x => x.FileNetReference).HasColumnName("FileNetReference").IsRequired().HasMaxLength(100);
            Property(x => x.UploadedOn).HasColumnName("UploadedOn").IsRequired();
            Property(x => x.UploadUserId).HasColumnName("UploadUserID").IsRequired();

            HasRequired(a => a.Activity).WithMany(b => b.ActivityDocuments).HasForeignKey(c => c.ActivityId);
            HasRequired(a => a.User).WithMany(b => b.ActivityDocuments).HasForeignKey(c => c.UploadUserId);
        }
    }
}
