using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanNote
    {
        public Guid ServicePlanNoteId { get; set; }
        public Guid ServicePlanId { get; set; }
        public Guid UserId { get; set; }
        public DateTime EntryDate { get; set; }
        public string Comment { get; set; }

        public virtual ServicePlan ServicePlan { get; set; }
        public virtual User User { get; set; }
    }

    internal class ServicePlanNoteConfiguration : EntityTypeConfiguration<ServicePlanNote>
    {
        public ServicePlanNoteConfiguration()
        {
            ToTable("dbo.ServicePlanNote");
            HasKey(x => x.ServicePlanNoteId);

            Property(x => x.ServicePlanNoteId).HasColumnName("ServicePlanNoteID").IsRequired();
            Property(x => x.ServicePlanId).HasColumnName("ServicePlanID").IsRequired();
            Property(x => x.UserId).HasColumnName("UserID").IsRequired();
            Property(x => x.EntryDate).HasColumnName("EntryDate").IsRequired();
            Property(x => x.Comment).HasColumnName("Comment").IsRequired().HasMaxLength(8000);

            HasRequired(a => a.ServicePlan).WithMany(b => b.ServicePlanNotes).HasForeignKey(c => c.ServicePlanId);
            HasRequired(a => a.User).WithMany(b => b.ServicePlanNotes).HasForeignKey(c => c.UserId);
        }
    }
}
