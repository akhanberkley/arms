using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class DocType
    {
        public string DocTypeCode { get; set; }
        public Guid CompanyId { get; set; }
        public string DocTypeDescription { get; set; }
        public int DisplayOrder { get; set; }

        public virtual Company Company { get; set; }
    }

    internal class DocTypeConfiguration : EntityTypeConfiguration<DocType>
    {
        public DocTypeConfiguration()
        {
            ToTable("dbo.DocType");
            HasKey(x => new { x.DocTypeCode, x.CompanyId });

            Property(x => x.DocTypeCode).HasColumnName("DocTypeCode").IsRequired().HasMaxLength(50).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.DocTypeDescription).HasColumnName("DocTypeDescription").IsRequired().HasMaxLength(50);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.DocTypes).HasForeignKey(c => c.CompanyId);
        }
    }
}
