using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Client
    {
        public string ClientId { get; set; }
        public Guid ClientAttributeId { get; set; }
        public string ClientAttributeValue { get; set; }
        public DateTime? DateModified { get; set; }

        public virtual ClientAttribute ClientAttribute { get; set; }
    }

    internal class ClientConfiguration : EntityTypeConfiguration<Client>
    {
        public ClientConfiguration()
        {
            ToTable("dbo.Client");
            HasKey(x => new { x.ClientId, x.ClientAttributeId });

            Property(x => x.ClientId).HasColumnName("ClientID").IsRequired().HasMaxLength(50).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ClientAttributeId).HasColumnName("ClientAttributeID").IsRequired();
            Property(x => x.ClientAttributeValue).HasColumnName("ClientAttributeValue").IsOptional().HasMaxLength(100);
            Property(x => x.DateModified).HasColumnName("DateModified").IsOptional();

            HasRequired(a => a.ClientAttribute).WithMany(b => b.Clients).HasForeignKey(c => c.ClientAttributeId);
        }
    }
}
