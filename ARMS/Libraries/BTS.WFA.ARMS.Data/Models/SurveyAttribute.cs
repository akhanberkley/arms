using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyAttribute
    {
        public Guid SurveyId { get; set; }
        public Guid CompanyAttributeId { get; set; }
        public string AttributeValue { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }

        public virtual CompanyAttribute CompanyAttribute { get; set; }
        public virtual Survey Survey { get; set; }
    }

    internal class SurveyAttributeConfiguration : EntityTypeConfiguration<SurveyAttribute>
    {
        public SurveyAttributeConfiguration()
        {
            ToTable("dbo.SurveyAttribute");
            HasKey(x => new { x.SurveyId, x.CompanyAttributeId });

            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.CompanyAttributeId).HasColumnName("CompanyAttributeID").IsRequired();
            Property(x => x.AttributeValue).HasColumnName("AttributeValue").IsOptional().HasMaxLength(3000);
            Property(x => x.LastModifiedBy).HasColumnName("LastModifiedBy").IsOptional().HasMaxLength(50);
            Property(x => x.LastModifiedDate).HasColumnName("LastModifiedDate").IsOptional();

            HasRequired(a => a.Survey).WithMany(b => b.SurveyAttributes).HasForeignKey(c => c.SurveyId);
            HasRequired(a => a.CompanyAttribute).WithMany(b => b.SurveyAttributes).HasForeignKey(c => c.CompanyAttributeId);
        }
    }
}
