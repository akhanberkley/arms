using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ExceptionLog
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Server { get; set; }
        public string Message { get; set; }
        public string Method { get; set; }
        public string File { get; set; }
        public string User { get; set; }
        public string StackTrace { get; set; }
        public string Url { get; set; }
    }

    internal class ExceptionLogConfiguration : EntityTypeConfiguration<ExceptionLog>
    {
        public ExceptionLogConfiguration()
        {
            ToTable("dbo.ExceptionLog");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.DateCreated).HasColumnName("DateCreated").IsRequired();
            Property(x => x.Server).HasColumnName("Server").IsOptional().HasMaxLength(50);
            Property(x => x.Message).HasColumnName("Message").IsOptional().HasMaxLength(500);
            Property(x => x.Method).HasColumnName("Method").IsOptional().HasMaxLength(200);
            Property(x => x.File).HasColumnName("File").IsOptional().HasMaxLength(500);
            Property(x => x.User).HasColumnName("User").IsOptional().HasMaxLength(100);
            Property(x => x.StackTrace).HasColumnName("StackTrace").IsOptional().HasMaxLength(6000);
            Property(x => x.Url).HasColumnName("URL").IsOptional().HasMaxLength(500);
        }
    }
}
