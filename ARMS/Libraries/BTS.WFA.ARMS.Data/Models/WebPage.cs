using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class WebPage
    {
        public string WebPagePath { get; set; }
        public string WebPageName { get; set; }
        public bool IsLoginRequired { get; set; }
        public bool IsAddEditPage { get; set; }
        public bool NoCachingOnBrowser { get; set; }

        public virtual ICollection<WebPageHelpfulHint> WebPageHelpfulHints { get; set; }

        public WebPage()
        {
            WebPageHelpfulHints = new List<WebPageHelpfulHint>();
        }
    }

    internal class WebPageConfiguration : EntityTypeConfiguration<WebPage>
    {
        public WebPageConfiguration()
        {
            ToTable("dbo.WebPage");
            HasKey(x => x.WebPagePath);

            Property(x => x.WebPagePath).HasColumnName("WebPagePath").IsRequired().HasMaxLength(100).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.WebPageName).HasColumnName("WebPageName").IsRequired().HasMaxLength(100);
            Property(x => x.IsLoginRequired).HasColumnName("IsLoginRequired").IsRequired();
            Property(x => x.IsAddEditPage).HasColumnName("IsAddEditPage").IsRequired();
            Property(x => x.NoCachingOnBrowser).HasColumnName("NoCachingOnBrowser").IsRequired();
        }
    }
}
