using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanDynamicField
    {
        public Guid ServicePlanId { get; set; }
        public Guid DynamicFieldId { get; set; }
        public string EntryText { get; set; }

        public virtual DynamicField DynamicField { get; set; }
        public virtual ServicePlan ServicePlan { get; set; }
    }

    internal class ServicePlanDynamicFieldConfiguration : EntityTypeConfiguration<ServicePlanDynamicField>
    {
        public ServicePlanDynamicFieldConfiguration()
        {
            ToTable("dbo.ServicePlan_DynamicField");
            HasKey(x => new { x.ServicePlanId, x.DynamicFieldId });

            Property(x => x.ServicePlanId).HasColumnName("ServicePlanID").IsRequired();
            Property(x => x.DynamicFieldId).HasColumnName("DynamicFieldID").IsRequired();
            Property(x => x.EntryText).HasColumnName("EntryText").IsRequired().HasMaxLength(8000);

            HasRequired(a => a.ServicePlan).WithMany(b => b.ServicePlanDynamicFields).HasForeignKey(c => c.ServicePlanId);
            HasRequired(a => a.DynamicField).WithMany(b => b.ServicePlanDynamicFields).HasForeignKey(c => c.DynamicFieldId);
        }
    }
}
