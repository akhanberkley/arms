using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlan
    {
        public Guid ServicePlanId { get; set; }
        public int ServicePlanNumber { get; set; }
        public Guid CompanyId { get; set; }
        public Guid InsuredId { get; set; }
        public Guid? PrimarySurveyId { get; set; }
        public Guid? PrimaryPolicyId { get; set; }
        public string WorkflowSubmissionNumber { get; set; }
        public Guid? PreviousServicePlanId { get; set; }
        public Guid? ServicePlanTypeId { get; set; }
        public string ServicePlanStatusCode { get; set; }
        public Guid? AssignedUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public Guid CreatedByUserId { get; set; }
        public string CreateStateCode { get; set; }
        public DateTime? CompleteDate { get; set; }
        public DateTime? LastUnderwriterReportDate { get; set; }
        public DateTime? NextFullReportDueDate { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public string ServicePlanGradingCode { get; set; }

        public virtual ICollection<Objective> Objectives { get; set; }
        public virtual ICollection<ServicePlan> ServicePlans { get; set; }
        public virtual ICollection<ServicePlanActivity> ServicePlanActivities { get; set; }
        public virtual ICollection<ServicePlanAttribute> ServicePlanAttributes { get; set; }
        public virtual ICollection<ServicePlanDocument> ServicePlanDocuments { get; set; }
        public virtual ICollection<ServicePlanDynamicField> ServicePlanDynamicFields { get; set; }
        public virtual ICollection<ServicePlanHistory> ServicePlanHistories { get; set; }
        public virtual ICollection<ServicePlanNote> ServicePlanNotes { get; set; }
        public virtual ICollection<Survey> Surveys { get; set; }

        public virtual Company Company { get; set; }
        public virtual CreateState CreateState { get; set; }
        public virtual Insured Insured { get; set; }
        public virtual Policy Policy { get; set; }
        public virtual ServicePlan ServicePlan_PreviousServicePlanId { get; set; }
        public virtual ServicePlanStatus ServicePlanStatus { get; set; }
        public virtual ServicePlanType ServicePlanType { get; set; }
        public virtual Survey Survey { get; set; }
        public virtual SurveyGrading SurveyGrading { get; set; }
        public virtual User User_AssignedUserId { get; set; }
        public virtual User User_CreatedByUserId { get; set; }

        public ServicePlan()
        {
            Objectives = new List<Objective>();
            ServicePlans = new List<ServicePlan>();
            ServicePlanDynamicFields = new List<ServicePlanDynamicField>();
            ServicePlanActivities = new List<ServicePlanActivity>();
            ServicePlanAttributes = new List<ServicePlanAttribute>();
            ServicePlanDocuments = new List<ServicePlanDocument>();
            ServicePlanHistories = new List<ServicePlanHistory>();
            ServicePlanNotes = new List<ServicePlanNote>();
            Surveys = new List<Survey>();
        }
    }

    internal class ServicePlanConfiguration : EntityTypeConfiguration<ServicePlan>
    {
        public ServicePlanConfiguration()
        {
            ToTable("dbo.ServicePlan");
            HasKey(x => x.ServicePlanId);

            Property(x => x.ServicePlanId).HasColumnName("ServicePlanID").IsRequired();
            Property(x => x.ServicePlanNumber).HasColumnName("ServicePlanNumber").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.InsuredId).HasColumnName("InsuredID").IsRequired();
            Property(x => x.PrimarySurveyId).HasColumnName("PrimarySurveyID").IsOptional();
            Property(x => x.PrimaryPolicyId).HasColumnName("PrimaryPolicyID").IsOptional();
            Property(x => x.WorkflowSubmissionNumber).HasColumnName("WorkflowSubmissionNumber").IsOptional().HasMaxLength(50);
            Property(x => x.PreviousServicePlanId).HasColumnName("PreviousServicePlanID").IsOptional();
            Property(x => x.ServicePlanTypeId).HasColumnName("ServicePlanTypeID").IsOptional();
            Property(x => x.ServicePlanStatusCode).HasColumnName("ServicePlanStatusCode").IsRequired().HasMaxLength(1);
            Property(x => x.AssignedUserId).HasColumnName("AssignedUserID").IsOptional();
            Property(x => x.CreateDate).HasColumnName("CreateDate").IsRequired();
            Property(x => x.CreatedByUserId).HasColumnName("CreatedByUserID").IsRequired();
            Property(x => x.CreateStateCode).HasColumnName("CreateStateCode").IsRequired().HasMaxLength(1);
            Property(x => x.CompleteDate).HasColumnName("CompleteDate").IsOptional();
            Property(x => x.LastUnderwriterReportDate).HasColumnName("LastUnderwriterReportDate").IsOptional();
            Property(x => x.NextFullReportDueDate).HasColumnName("NextFullReportDueDate").IsOptional();
            Property(x => x.LastModifiedOn).HasColumnName("LastModifiedOn").IsOptional();
            Property(x => x.ServicePlanGradingCode).HasColumnName("ServicePlanGradingCode").IsOptional().HasMaxLength(1);

            HasRequired(a => a.Company).WithMany(b => b.ServicePlans).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.Insured).WithMany(b => b.ServicePlans).HasForeignKey(c => c.InsuredId);
            HasOptional(a => a.Survey).WithMany(b => b.ServicePlans_PrimarySurveyId).HasForeignKey(c => c.PrimarySurveyId);
            HasOptional(a => a.Policy).WithMany(b => b.ServicePlans).HasForeignKey(c => c.PrimaryPolicyId);
            HasOptional(a => a.ServicePlan_PreviousServicePlanId).WithMany(b => b.ServicePlans).HasForeignKey(c => c.PreviousServicePlanId);
            HasOptional(a => a.ServicePlanType).WithMany(b => b.ServicePlans).HasForeignKey(c => c.ServicePlanTypeId);
            HasRequired(a => a.ServicePlanStatus).WithMany(b => b.ServicePlans).HasForeignKey(c => c.ServicePlanStatusCode);
            HasOptional(a => a.User_AssignedUserId).WithMany(b => b.ServicePlans_AssignedUserId).HasForeignKey(c => c.AssignedUserId);
            HasRequired(a => a.User_CreatedByUserId).WithMany(b => b.ServicePlans_CreatedByUserId).HasForeignKey(c => c.CreatedByUserId);
            HasRequired(a => a.CreateState).WithMany(b => b.ServicePlans).HasForeignKey(c => c.CreateStateCode);
            HasOptional(a => a.SurveyGrading).WithMany(b => b.ServicePlans).HasForeignKey(c => c.ServicePlanGradingCode);
            HasMany(t => t.Surveys).WithMany().Map(m =>
            {
                m.ToTable("ServicePlan_Survey");
                m.MapLeftKey("ServicePlanID");
                m.MapRightKey("SurveyID");
            });
        }
    }
}
