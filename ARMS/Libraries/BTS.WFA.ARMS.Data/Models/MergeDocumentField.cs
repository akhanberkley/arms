using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class MergeDocumentField
    {
        public Guid MergeDocumentId { get; set; }
        public string FieldName { get; set; }
        public string FieldType { get; set; }
        public string DisplayName { get; set; }
        public string FormFieldDataTypeCode { get; set; }
        public bool IsReported { get; set; }
        public bool IsRequired { get; set; }
        public DateTime LastReportedOn { get; set; }

        public virtual ICollection<SurveyMergeDocumentField> SurveyMergeDocumentFields { get; set; }

        public virtual FormFieldDataType FormFieldDataType { get; set; }
        public virtual MergeDocument MergeDocument { get; set; }

        public MergeDocumentField()
        {
            SurveyMergeDocumentFields = new List<SurveyMergeDocumentField>();
        }
    }

    internal class MergeDocumentFieldConfiguration : EntityTypeConfiguration<MergeDocumentField>
    {
        public MergeDocumentFieldConfiguration()
        {
            ToTable("dbo.MergeDocumentField");
            HasKey(x => new { x.MergeDocumentId, x.FieldName });

            Property(x => x.MergeDocumentId).HasColumnName("MergeDocumentID").IsRequired();
            Property(x => x.FieldName).HasColumnName("FieldName").IsRequired().HasMaxLength(500).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.FieldType).HasColumnName("FieldType").IsRequired().HasMaxLength(100);
            Property(x => x.DisplayName).HasColumnName("DisplayName").IsRequired().HasMaxLength(500);
            Property(x => x.FormFieldDataTypeCode).HasColumnName("FormFieldDataTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.IsReported).HasColumnName("IsReported").IsRequired();
            Property(x => x.IsRequired).HasColumnName("IsRequired").IsRequired();
            Property(x => x.LastReportedOn).HasColumnName("LastReportedOn").IsRequired();

            HasRequired(a => a.MergeDocument).WithMany(b => b.MergeDocumentFields).HasForeignKey(c => c.MergeDocumentId);
            HasRequired(a => a.FormFieldDataType).WithMany(b => b.MergeDocumentFields).HasForeignKey(c => c.FormFieldDataTypeCode);
        }
    }
}
