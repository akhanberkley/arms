using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class UserSearch
    {
        public Guid UserSearchId { get; set; }
        public Guid UserId { get; set; }
        public string UserSearchName { get; set; }
        public string FilterExpression { get; set; }
        public string FilterParameters { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsRequired { get; set; }

        public virtual User User { get; set; }
    }

    internal class UserSearchConfiguration : EntityTypeConfiguration<UserSearch>
    {
        public UserSearchConfiguration()
        {
            ToTable("dbo.UserSearch");
            HasKey(x => x.UserSearchId);

            Property(x => x.UserSearchId).HasColumnName("UserSearchID").IsRequired();
            Property(x => x.UserId).HasColumnName("UserID").IsRequired();
            Property(x => x.UserSearchName).HasColumnName("UserSearchName").IsRequired().HasMaxLength(100);
            Property(x => x.FilterExpression).HasColumnName("FilterExpression").IsRequired().HasMaxLength(1000);
            Property(x => x.FilterParameters).HasColumnName("FilterParameters").IsRequired().HasMaxLength(1000);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.IsRequired).HasColumnName("IsRequired").IsRequired();

            HasRequired(a => a.User).WithMany(b => b.UserSearches).HasForeignKey(c => c.UserId);
        }
    }
}
