using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanActivity
    {
        public Guid ServicePlanActivityId { get; set; }
        public Guid ServicePlanId { get; set; }
        public Guid ActivityTypeId { get; set; }
        public Guid AllocatedTo { get; set; }
        public DateTime ActivityDate { get; set; }
        public decimal ActivityHours { get; set; }
        public string Comments { get; set; }

        public virtual ActivityType ActivityType { get; set; }
        public virtual ServicePlan ServicePlan { get; set; }
        public virtual User User { get; set; }
    }

    internal class ServicePlanActivityConfiguration : EntityTypeConfiguration<ServicePlanActivity>
    {
        public ServicePlanActivityConfiguration()
        {
            ToTable("dbo.ServicePlanActivity");
            HasKey(x => x.ServicePlanActivityId);

            Property(x => x.ServicePlanActivityId).HasColumnName("ServicePlanActivityID").IsRequired();
            Property(x => x.ServicePlanId).HasColumnName("ServicePlanID").IsRequired();
            Property(x => x.ActivityTypeId).HasColumnName("ActivityTypeID").IsRequired();
            Property(x => x.AllocatedTo).HasColumnName("AllocatedTo").IsRequired();
            Property(x => x.ActivityDate).HasColumnName("ActivityDate").IsRequired();
            Property(x => x.ActivityHours).HasColumnName("ActivityHours").IsRequired().HasPrecision(9, 2);
            Property(x => x.Comments).HasColumnName("Comments").IsOptional().HasMaxLength(6000);

            HasRequired(a => a.ServicePlan).WithMany(b => b.ServicePlanActivities).HasForeignKey(c => c.ServicePlanId);
            HasRequired(a => a.ActivityType).WithMany(b => b.ServicePlanActivities).HasForeignKey(c => c.ActivityTypeId);
            HasRequired(a => a.User).WithMany(b => b.ServicePlanActivities).HasForeignKey(c => c.AllocatedTo);
        }
    }
}
