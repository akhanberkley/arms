using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class UserSetting
    {
        public Guid UserId { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
        public int ValueType { get; set; }

        public virtual User User { get; set; }
    }

    internal class UserSettingConfiguration : EntityTypeConfiguration<UserSetting>
    {
        public UserSettingConfiguration()
        {
            ToTable("dbo.UserSetting");
            HasKey(x => new { x.UserId, x.SettingName });

            Property(x => x.UserId).HasColumnName("UserID").IsRequired();
            Property(x => x.SettingName).HasColumnName("SettingName").IsRequired().HasMaxLength(200).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SettingValue).HasColumnName("SettingValue").IsRequired().HasMaxLength(4000);
            Property(x => x.ValueType).HasColumnName("ValueType").IsRequired();

            HasRequired(a => a.User).WithMany(b => b.UserSettings).HasForeignKey(c => c.UserId);
        }
    }
}
