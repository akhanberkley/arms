using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ReviewerType
    {
        public string ReviewerTypeCode { get; set; }
        public string ReviewerTypeName { get; set; }

        public virtual ICollection<ReviewCategoryType> ReviewCategoryTypes { get; set; }
        public virtual ICollection<ReviewRatingType> ReviewRatingTypes { get; set; }
        public virtual ICollection<SurveyReview> SurveyReviews { get; set; }

        public ReviewerType()
        {
            ReviewCategoryTypes = new List<ReviewCategoryType>();
            ReviewRatingTypes = new List<ReviewRatingType>();
            SurveyReviews = new List<SurveyReview>();
        }
    }

    internal class ReviewerTypeConfiguration : EntityTypeConfiguration<ReviewerType>
    {
        public ReviewerTypeConfiguration()
        {
            ToTable("dbo.ReviewerType");
            HasKey(x => x.ReviewerTypeCode);

            Property(x => x.ReviewerTypeCode).HasColumnName("ReviewerTypeCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ReviewerTypeName).HasColumnName("ReviewerTypeName").IsRequired().HasMaxLength(100);
        }
    }
}
