using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Zip
    {
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string StateCode { get; set; }

        public virtual State State { get; set; }
    }

    internal class ZipConfiguration : EntityTypeConfiguration<Zip>
    {
        public ZipConfiguration()
        {
            ToTable("dbo.Zip");
            HasKey(x => x.ZipCode);

            Property(x => x.ZipCode).HasColumnName("ZipCode").IsRequired().HasMaxLength(5).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.City).HasColumnName("City").IsRequired().HasMaxLength(30);
            Property(x => x.County).HasColumnName("County").IsOptional().HasMaxLength(50);
            Property(x => x.StateCode).HasColumnName("StateCode").IsRequired().HasMaxLength(2);

            HasRequired(a => a.State).WithMany(b => b.Zips).HasForeignKey(c => c.StateCode);
        }
    }
}
