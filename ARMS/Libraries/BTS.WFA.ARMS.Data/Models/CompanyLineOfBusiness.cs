using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class CompanyLineOfBusiness
    {
        public Guid CompanyId { get; set; }
        public string LineOfBusinessCode { get; set; }
        public string PolicySymbols { get; set; }
        public int DisplayOrder { get; set; }

        public virtual Company Company { get; set; }
        public virtual LineOfBusiness LineOfBusiness { get; set; }
    }

    internal class CompanyLineOfBusinessConfiguration : EntityTypeConfiguration<CompanyLineOfBusiness>
    {
        public CompanyLineOfBusinessConfiguration()
        {
            ToTable("dbo.CompanyLineOfBusiness");
            HasKey(x => new { x.CompanyId, x.LineOfBusinessCode });

            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.LineOfBusinessCode).HasColumnName("LineOfBusinessCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.PolicySymbols).HasColumnName("PolicySymbols").IsRequired().HasMaxLength(200);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.CompanyLineOfBusinesses).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.LineOfBusiness).WithMany(b => b.CompanyLineOfBusinesses).HasForeignKey(c => c.LineOfBusinessCode);
        }
    }
}
