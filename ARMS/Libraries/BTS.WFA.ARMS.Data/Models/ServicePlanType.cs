using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanType
    {
        public Guid ServicePlanTypeId { get; set; }
        public Guid CompanyId { get; set; }
        public string ServicePlanTypeName { get; set; }
        public int DisplayOrder { get; set; }
        public bool Disabled { get; set; }

        public virtual ICollection<ServicePlan> ServicePlans { get; set; }

        public virtual Company Company { get; set; }

        public ServicePlanType()
        {
            ServicePlans = new List<ServicePlan>();
        }
    }

    internal class ServicePlanTypeConfiguration : EntityTypeConfiguration<ServicePlanType>
    {
        public ServicePlanTypeConfiguration()
        {
            ToTable("dbo.ServicePlanType");
            HasKey(x => x.ServicePlanTypeId);

            Property(x => x.ServicePlanTypeId).HasColumnName("ServicePlanTypeID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.ServicePlanTypeName).HasColumnName("ServicePlanTypeName").IsRequired().HasMaxLength(50);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
            Property(x => x.Disabled).HasColumnName("Disabled").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.ServicePlanTypes).HasForeignKey(c => c.CompanyId);
        }
    }
}
