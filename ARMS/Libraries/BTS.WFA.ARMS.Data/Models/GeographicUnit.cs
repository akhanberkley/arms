using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class GeographicUnit
    {
        public string GeographicUnitCode { get; set; }
        public string GeographicUnitName { get; set; }
        public int PriorityIndex { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<GeographicArea> GeographicAreas { get; set; }

        public GeographicUnit()
        {
            GeographicAreas = new List<GeographicArea>();
        }
    }

    internal class GeographicUnitConfiguration : EntityTypeConfiguration<GeographicUnit>
    {
        public GeographicUnitConfiguration()
        {
            ToTable("dbo.GeographicUnit");
            HasKey(x => x.GeographicUnitCode);

            Property(x => x.GeographicUnitCode).HasColumnName("GeographicUnitCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.GeographicUnitName).HasColumnName("GeographicUnitName").IsRequired().HasMaxLength(30);
            Property(x => x.PriorityIndex).HasColumnName("PriorityIndex").IsRequired();
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
