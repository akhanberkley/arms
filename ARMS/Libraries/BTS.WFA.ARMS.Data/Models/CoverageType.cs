using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class CoverageType
    {
        public string CoverageTypeCode { get; set; }
        public string CoverageTypeName { get; set; }
        public string DotNetDataType { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<Coverage> Coverages { get; set; }

        public CoverageType()
        {
            Coverages = new List<Coverage>();
        }
    }

    internal class CoverageTypeConfiguration : EntityTypeConfiguration<CoverageType>
    {
        public CoverageTypeConfiguration()
        {
            ToTable("dbo.CoverageType");
            HasKey(x => x.CoverageTypeCode);

            Property(x => x.CoverageTypeCode).HasColumnName("CoverageTypeCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CoverageTypeName).HasColumnName("CoverageTypeName").IsRequired().HasMaxLength(50);
            Property(x => x.DotNetDataType).HasColumnName("DotNetDataType").IsRequired().HasMaxLength(10);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
