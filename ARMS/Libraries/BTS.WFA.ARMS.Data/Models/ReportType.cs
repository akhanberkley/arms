using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ReportType
    {
        public string ReportTypeCode { get; set; }
        public string ReportTypeName { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<ReportTypeCompany> ReportTypeCompanies { get; set; }
        public virtual ICollection<SurveyLocationPolicyCoverageName> SurveyLocationPolicyCoverageNames { get; set; }
        public virtual ICollection<SurveyPolicyCoverage> SurveyPolicyCoverages { get; set; }

        public ReportType()
        {
            ReportTypeCompanies = new List<ReportTypeCompany>();
            SurveyLocationPolicyCoverageNames = new List<SurveyLocationPolicyCoverageName>();
            SurveyPolicyCoverages = new List<SurveyPolicyCoverage>();
        }
    }

    internal class ReportTypeConfiguration : EntityTypeConfiguration<ReportType>
    {
        public ReportTypeConfiguration()
        {
            ToTable("dbo.ReportType");
            HasKey(x => x.ReportTypeCode);

            Property(x => x.ReportTypeCode).HasColumnName("ReportTypeCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ReportTypeName).HasColumnName("ReportTypeName").IsRequired().HasMaxLength(10);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
