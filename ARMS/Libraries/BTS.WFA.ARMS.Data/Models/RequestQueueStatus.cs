﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class RequestQueueStatus     //WA-141 Doug Bruce 2015-03-27 - RequestQueue DB Structure
    {
        public string RequestQueueStatusCode { get; set; }
        public string Description { get; set; }
        public string IconHtmlName { get; set; }
    }

    internal class RequestQueueStatusConfiguration : EntityTypeConfiguration<RequestQueueStatus>
    {
        public RequestQueueStatusConfiguration()
        {
            ToTable("dbo.RequestQueueStatus");
            HasKey(x => x.RequestQueueStatusCode);

            Property(x => x.RequestQueueStatusCode).HasColumnName("RequestQueueStatusCode").IsRequired();
            Property(x => x.Description).HasColumnName("Description").IsOptional();
            Property(x => x.IconHtmlName).HasColumnName("IconHtmlName").IsOptional();
        }
    }
}
