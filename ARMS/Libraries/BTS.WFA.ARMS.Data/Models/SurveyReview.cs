using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyReview
    {
        public Guid SurveyReviewId { get; set; }
        public Guid SurveyId { get; set; }
        public string ReviewerTypeCode { get; set; }
        public DateTime CreateDate { get; set; }
        public string ReviewedBy { get; set; }
        public DateTime? ReviewedDate { get; set; }

        public virtual ICollection<SurveyReviewRating> SurveyReviewRatings { get; set; }

        public virtual ReviewerType ReviewerType { get; set; }
        public virtual Survey Survey { get; set; }

        public SurveyReview()
        {
            SurveyReviewRatings = new List<SurveyReviewRating>();
        }
    }

    internal class SurveyReviewConfiguration : EntityTypeConfiguration<SurveyReview>
    {
        public SurveyReviewConfiguration()
        {
            ToTable("dbo.SurveyReview");
            HasKey(x => x.SurveyReviewId);

            Property(x => x.SurveyReviewId).HasColumnName("SurveyReviewID").IsRequired();
            Property(x => x.SurveyId).HasColumnName("SurveyID").IsRequired();
            Property(x => x.ReviewerTypeCode).HasColumnName("ReviewerTypeCode").IsRequired().HasMaxLength(2);
            Property(x => x.CreateDate).HasColumnName("CreateDate").IsRequired();
            Property(x => x.ReviewedBy).HasColumnName("ReviewedBy").IsOptional().HasMaxLength(30);
            Property(x => x.ReviewedDate).HasColumnName("ReviewedDate").IsOptional();

            HasRequired(a => a.Survey).WithMany(b => b.SurveyReviews).HasForeignKey(c => c.SurveyId);
            HasRequired(a => a.ReviewerType).WithMany(b => b.SurveyReviews).HasForeignKey(c => c.ReviewerTypeCode);
        }
    }
}
