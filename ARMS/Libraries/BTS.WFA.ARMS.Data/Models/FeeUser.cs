using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class FeeUser
    {
        public Guid FeeUserId { get; set; }
        public Guid FeeCompanyUserId { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string AlternatePhoneNumber { get; set; }
        public bool AccountDisabled { get; set; }
        public string PasswordHash { get; set; }
        public int FailedLoginAttempts { get; set; }
        public DateTime? LockedOutUntil { get; set; }

        public virtual User User { get; set; }
    }

    internal class FeeUserConfiguration : EntityTypeConfiguration<FeeUser>
    {
        public FeeUserConfiguration()
        {
            ToTable("dbo.FeeUser");
            HasKey(x => x.FeeUserId);

            Property(x => x.FeeUserId).HasColumnName("FeeUserID").IsRequired();
            Property(x => x.FeeCompanyUserId).HasColumnName("FeeCompanyUserID").IsRequired();
            Property(x => x.Username).HasColumnName("Username").IsRequired().HasMaxLength(30);
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(60);
            Property(x => x.EmailAddress).HasColumnName("EmailAddress").IsOptional().HasMaxLength(100);
            Property(x => x.PhoneNumber).HasColumnName("PhoneNumber").IsOptional().HasMaxLength(25);
            Property(x => x.AlternatePhoneNumber).HasColumnName("AlternatePhoneNumber").IsOptional().HasMaxLength(25);
            Property(x => x.AccountDisabled).HasColumnName("AccountDisabled").IsRequired();
            Property(x => x.PasswordHash).HasColumnName("PasswordHash").IsRequired().HasMaxLength(100);
            Property(x => x.FailedLoginAttempts).HasColumnName("FailedLoginAttempts").IsRequired();
            Property(x => x.LockedOutUntil).HasColumnName("LockedOutUntil").IsOptional();

            HasRequired(a => a.User).WithMany(b => b.FeeUsers).HasForeignKey(c => c.FeeCompanyUserId);
        }
    }
}
