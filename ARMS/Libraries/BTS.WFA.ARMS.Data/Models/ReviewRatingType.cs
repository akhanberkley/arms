using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ReviewRatingType
    {
        public Guid ReviewRatingTypeId { get; set; }
        public Guid CompanyId { get; set; }
        public string ReviewerTypeCode { get; set; }
        public int RatingNumber { get; set; }
        public string RatingName { get; set; }
        public string RatingDescription { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<SurveyReviewRating> SurveyReviewRatings { get; set; }

        public virtual Company Company { get; set; }
        public virtual ReviewerType ReviewerType { get; set; }

        public ReviewRatingType()
        {
            SurveyReviewRatings = new List<SurveyReviewRating>();
        }
    }

    internal class ReviewRatingTypeConfiguration : EntityTypeConfiguration<ReviewRatingType>
    {
        public ReviewRatingTypeConfiguration()
        {
            ToTable("dbo.ReviewRatingType");
            HasKey(x => x.ReviewRatingTypeId);

            Property(x => x.ReviewRatingTypeId).HasColumnName("ReviewRatingTypeID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.ReviewerTypeCode).HasColumnName("ReviewerTypeCode").IsRequired().HasMaxLength(2);
            Property(x => x.RatingNumber).HasColumnName("RatingNumber").IsRequired();
            Property(x => x.RatingName).HasColumnName("RatingName").IsRequired().HasMaxLength(50);
            Property(x => x.RatingDescription).HasColumnName("RatingDescription").IsRequired().HasMaxLength(1000);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.ReviewRatingTypes).HasForeignKey(c => c.CompanyId);
            HasRequired(a => a.ReviewerType).WithMany(b => b.ReviewRatingTypes).HasForeignKey(c => c.ReviewerTypeCode);
        }
    }
}
