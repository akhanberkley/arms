using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ObjectiveStatus
    {
        public string ObjectiveStatusCode { get; set; }
        public string ObjectiveStatusName { get; set; }
        public int DisplayOrder { get; set; }

        public virtual ICollection<Objective> Objectives { get; set; }

        public ObjectiveStatus()
        {
            Objectives = new List<Objective>();
        }
    }

    internal class ObjectiveStatusConfiguration : EntityTypeConfiguration<ObjectiveStatus>
    {
        public ObjectiveStatusConfiguration()
        {
            ToTable("dbo.ObjectiveStatus");
            HasKey(x => x.ObjectiveStatusCode);

            Property(x => x.ObjectiveStatusCode).HasColumnName("ObjectiveStatusCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ObjectiveStatusName).HasColumnName("ObjectiveStatusName").IsRequired().HasMaxLength(20);
            Property(x => x.DisplayOrder).HasColumnName("DisplayOrder").IsRequired();
        }
    }
}
