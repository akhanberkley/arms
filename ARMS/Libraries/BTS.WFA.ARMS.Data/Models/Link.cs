using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Link
    {
        public Guid LinkId { get; set; }
        public Guid CompanyId { get; set; }
        public string LinkDescription { get; set; }
        public string LinkUrl { get; set; }
        public int PriorityIndex { get; set; }

        public virtual Company Company { get; set; }
    }

    internal class LinkConfiguration : EntityTypeConfiguration<Link>
    {
        public LinkConfiguration()
        {
            ToTable("dbo.Link");
            HasKey(x => x.LinkId);

            Property(x => x.LinkId).HasColumnName("LinkID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.LinkDescription).HasColumnName("LinkDescription").IsRequired().HasMaxLength(200);
            Property(x => x.LinkUrl).HasColumnName("LinkURL").IsRequired().HasMaxLength(200);
            Property(x => x.PriorityIndex).HasColumnName("PriorityIndex").IsRequired();

            HasRequired(a => a.Company).WithMany(b => b.Links).HasForeignKey(c => c.CompanyId);
        }
    }
}
