using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class ServicePlanGridColumnFilter
    {
        public Guid ServicePlanGridColumnFilterId { get; set; }
        public string Name { get; set; }
        public string FilterExpression { get; set; }
        public string SortExpression { get; set; }
        public string DotNetDataType { get; set; }

        public virtual ICollection<ServicePlanGridColumnFilterCompany> ServicePlanGridColumnFilterCompanies { get; set; }

        public ServicePlanGridColumnFilter()
        {
            ServicePlanGridColumnFilterCompanies = new List<ServicePlanGridColumnFilterCompany>();
        }
    }

    internal class ServicePlanGridColumnFilterConfiguration : EntityTypeConfiguration<ServicePlanGridColumnFilter>
    {
        public ServicePlanGridColumnFilterConfiguration()
        {
            ToTable("dbo.ServicePlanGridColumnFilter");
            HasKey(x => x.ServicePlanGridColumnFilterId);

            Property(x => x.ServicePlanGridColumnFilterId).HasColumnName("ServicePlanGridColumnFilterID").IsRequired();
            Property(x => x.Name).HasColumnName("Name").IsRequired().HasMaxLength(50);
            Property(x => x.FilterExpression).HasColumnName("FilterExpression").IsRequired().HasMaxLength(50);
            Property(x => x.SortExpression).HasColumnName("SortExpression").IsRequired().HasMaxLength(100);
            Property(x => x.DotNetDataType).HasColumnName("DotNetDataType").IsRequired().HasMaxLength(50);
        }
    }
}
