using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class RecStatusType
    {
        public string RecStatusTypeCode { get; set; }
        public string RecStatusTypeName { get; set; }

        public virtual ICollection<RecStatus> RecStatus { get; set; }

        public RecStatusType()
        {
            RecStatus = new List<RecStatus>();
        }
    }

    internal class RecStatusTypeConfiguration : EntityTypeConfiguration<RecStatusType>
    {
        public RecStatusTypeConfiguration()
        {
            ToTable("dbo.RecStatusType");
            HasKey(x => x.RecStatusTypeCode);

            Property(x => x.RecStatusTypeCode).HasColumnName("RecStatusTypeCode").IsRequired().HasMaxLength(1).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecStatusTypeName).HasColumnName("RecStatusTypeName").IsRequired().HasMaxLength(50);
        }
    }
}
