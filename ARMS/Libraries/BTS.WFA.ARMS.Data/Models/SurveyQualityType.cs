using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class SurveyQualityType
    {
        public string SurveyQualityTypeCode { get; set; }
        public string SurveyQualityTypeName { get; set; }

        public virtual ICollection<SurveyQuality> SurveyQualities { get; set; }

        public SurveyQualityType()
        {
            SurveyQualities = new List<SurveyQuality>();
        }
    }

    internal class SurveyQualityTypeConfiguration : EntityTypeConfiguration<SurveyQualityType>
    {
        public SurveyQualityTypeConfiguration()
        {
            ToTable("dbo.SurveyQualityType");
            HasKey(x => x.SurveyQualityTypeCode);

            Property(x => x.SurveyQualityTypeCode).HasColumnName("SurveyQualityTypeCode").IsRequired().HasMaxLength(2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SurveyQualityTypeName).HasColumnName("SurveyQualityTypeName").IsRequired().HasMaxLength(100);
        }
    }
}
