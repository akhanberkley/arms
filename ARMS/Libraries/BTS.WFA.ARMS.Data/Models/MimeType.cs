using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class MimeType
    {
        public Guid MimeTypeId { get; set; }
        public string MimeTypeName { get; set; }
        public string DocumentExtension { get; set; }
    }

    internal class MimeTypeConfiguration : EntityTypeConfiguration<MimeType>
    {
        public MimeTypeConfiguration()
        {
            ToTable("dbo.MimeType");
            HasKey(x => x.MimeTypeId);

            Property(x => x.MimeTypeId).HasColumnName("MimeTypeID").IsRequired();
            Property(x => x.MimeTypeName).HasColumnName("MimeTypeName").IsRequired().HasMaxLength(100);
            Property(x => x.DocumentExtension).HasColumnName("DocumentExtension").IsRequired().HasMaxLength(10);
        }
    }
}
