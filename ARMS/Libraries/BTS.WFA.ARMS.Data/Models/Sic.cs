using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Sic
    {
        public string SicCode { get; set; }
        public string SicDescription { get; set; }

        public virtual ICollection<Insured> Insureds { get; set; }

        public Sic()
        {
            Insureds = new List<Insured>();
        }
    }

    internal class SicConfiguration : EntityTypeConfiguration<Sic>
    {
        public SicConfiguration()
        {
            ToTable("dbo.SIC");
            HasKey(x => x.SicCode);

            Property(x => x.SicCode).HasColumnName("SICCode").IsRequired().HasMaxLength(4).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.SicDescription).HasColumnName("SICDescription").IsRequired().HasMaxLength(150);
        }
    }
}
