using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class Naic
    {
        public string NaicCode { get; set; }
        public string NaicDescription { get; set; }
    }

    internal class NaicConfiguration : EntityTypeConfiguration<Naic>
    {
        public NaicConfiguration()
        {
            ToTable("dbo.NAIC");
            HasKey(x => x.NaicCode);

            Property(x => x.NaicCode).HasColumnName("NAICCode").IsRequired().HasMaxLength(6).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.NaicDescription).HasColumnName("NAICDescription").IsRequired().HasMaxLength(150);
        }
    }
}
