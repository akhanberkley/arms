using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace BTS.WFA.ARMS.Data.Models
{
    public class AssignmentRule
    {
        public Guid AssignmentRuleId { get; set; }
        public Guid CompanyId { get; set; }
        public Guid? FilterId { get; set; }
        public string SurveyStatusTypeCode { get; set; }
        public int PriorityIndex { get; set; }
        public string AssignmentRuleActionCode { get; set; }

        public virtual AssignmentRuleAction AssignmentRuleAction { get; set; }
        public virtual Company Company { get; set; }
        public virtual Filter Filter { get; set; }
        public virtual SurveyStatusType SurveyStatusType { get; set; }
    }

    internal class AssignmentRuleConfiguration : EntityTypeConfiguration<AssignmentRule>
    {
        public AssignmentRuleConfiguration()
        {
            ToTable("dbo.AssignmentRule");
            HasKey(x => x.AssignmentRuleId);

            Property(x => x.AssignmentRuleId).HasColumnName("AssignmentRuleID").IsRequired();
            Property(x => x.CompanyId).HasColumnName("CompanyID").IsRequired();
            Property(x => x.FilterId).HasColumnName("FilterID").IsOptional();
            Property(x => x.SurveyStatusTypeCode).HasColumnName("SurveyStatusTypeCode").IsRequired().HasMaxLength(1);
            Property(x => x.PriorityIndex).HasColumnName("PriorityIndex").IsRequired();
            Property(x => x.AssignmentRuleActionCode).HasColumnName("AssignmentRuleActionCode").IsRequired().HasMaxLength(2);

            HasRequired(a => a.Company).WithMany(b => b.AssignmentRules).HasForeignKey(c => c.CompanyId);
            HasOptional(a => a.Filter).WithMany(b => b.AssignmentRules).HasForeignKey(c => c.FilterId);
            HasRequired(a => a.SurveyStatusType).WithMany(b => b.AssignmentRules).HasForeignKey(c => c.SurveyStatusTypeCode);
            HasRequired(a => a.AssignmentRuleAction).WithMany(b => b.AssignmentRules).HasForeignKey(c => c.AssignmentRuleActionCode);
        }
    }
}
