﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using BTS.WFA.Data;

namespace BTS.WFA.ARMS.Data
{
    public class MockDb : BTS.WFA.Data.BaseMockContext, IArmsDb
    {
        static MockDb()
        {
            LoadStandardDomainData();
        }

        public DbSet<Models.Activity> Activities { get; set; }
        public DbSet<Models.ActivityDocument> ActivityDocuments { get; set; }
        public DbSet<Models.ActivityType> ActivityTypes { get; set; }
        public DbSet<Models.ActivityTypeLevel> ActivityTypeLevels { get; set; }
        public DbSet<Models.Agency> Agencies { get; set; }
        public DbSet<Models.AgencyEmail> AgencyEmails { get; set; }
        public DbSet<Models.AssignmentRule> AssignmentRules { get; set; }
        public DbSet<Models.AssignmentRuleAction> AssignmentRuleActions { get; set; }
        public DbSet<Models.AssignmentRuleActionType> AssignmentRuleActionTypes { get; set; }
        public DbSet<Models.AttributeDataType> AttributeDataTypes { get; set; }
        public DbSet<Models.Building> Buildings { get; set; }
        public DbSet<Models.BuildingValuationType> BuildingValuationTypes { get; set; }
        public DbSet<Models.Claim> Claims { get; set; }
        public DbSet<Models.Client> Clients { get; set; }
        public DbSet<Models.ClientAttribute> ClientAttributes { get; set; }
        public DbSet<Models.ClientAttributeType> ClientAttributeTypes { get; set; }
        public DbSet<Models.ClientHistory> ClientHistories { get; set; }
        public DbSet<Models.Company> Companies { get; set; }
        public DbSet<Models.CompanyAttribute> CompanyAttributes { get; set; }
        public DbSet<Models.CompanyCounter> CompanyCounters { get; set; }
        public DbSet<Models.CompanyCounterType> CompanyCounterTypes { get; set; }
        public DbSet<Models.CompanyLineOfBusiness> CompanyLineOfBusinesses { get; set; }
        public DbSet<Models.CompanyParameter> CompanyParameters { get; set; }
        public DbSet<Models.CompanyPolicySystem> CompanyPolicySystems { get; set; }
        public DbSet<Models.ContactReasonType> ContactReasonTypes { get; set; }
        public DbSet<Models.Coverage> Coverages { get; set; }
        public DbSet<Models.CoverageDetail> CoverageDetails { get; set; }
        public DbSet<Models.CoverageName> CoverageNames { get; set; }
        public DbSet<Models.CoverageNameType> CoverageNameTypes { get; set; }
        public DbSet<Models.CoverageType> CoverageTypes { get; set; }
        public DbSet<Models.CreateState> CreateStates { get; set; }
        public DbSet<Models.CreateSurveyType> CreateSurveyTypes { get; set; }
        public DbSet<Models.DocType> DocTypes { get; set; }
        public DbSet<Models.DueDateHistory> DueDateHistories { get; set; }
        public DbSet<Models.DynamicField> DynamicFields { get; set; }
        public DbSet<Models.EntityType> EntityTypes { get; set; }
        public DbSet<Models.ExceptionEmailFilterString> ExceptionEmailFilterStrings { get; set; }
        public DbSet<Models.ExceptionLog> ExceptionLogs { get; set; }
        public DbSet<Models.ExceptionReturnMessage> ExceptionReturnMessages { get; set; }
        public DbSet<Models.FeeUser> FeeUsers { get; set; }
        public DbSet<Models.Filter> Filters { get; set; }
        public DbSet<Models.FilterCondition> FilterConditions { get; set; }
        public DbSet<Models.FilterField> FilterFields { get; set; }
        public DbSet<Models.FilterOperator> FilterOperators { get; set; }
        public DbSet<Models.FormFieldDataType> FormFieldDataTypes { get; set; }
        public DbSet<Models.GeographicArea> GeographicAreas { get; set; }
        public DbSet<Models.GeographicUnit> GeographicUnits { get; set; }
        public DbSet<Models.GridColumnFilter> GridColumnFilters { get; set; }
        public DbSet<Models.GridColumnFilterCompany> GridColumnFilterCompanies { get; set; }
        public DbSet<Models.Insured> Insureds { get; set; }
        public DbSet<Models.LineOfBusiness> LineOfBusinesses { get; set; }
        public DbSet<Models.Link> Links { get; set; }
        public DbSet<Models.Location> Locations { get; set; }
        public DbSet<Models.LocationContact> LocationContacts { get; set; }
        public DbSet<Models.LoginHistory> LoginHistories { get; set; }
        public DbSet<Models.MergeDocument> MergeDocuments { get; set; }
        public DbSet<Models.MergeDocumentField> MergeDocumentFields { get; set; }
        public DbSet<Models.MergeDocumentType> MergeDocumentTypes { get; set; }
        public DbSet<Models.MergeDocumentVersion> MergeDocumentVersions { get; set; }
        public DbSet<Models.MimeType> MimeTypes { get; set; }
        public DbSet<Models.Naic> Naics { get; set; }
        public DbSet<Models.Notification> Notifications { get; set; }
        public DbSet<Models.Objective> Objectives { get; set; }
        public DbSet<Models.ObjectiveAttribute> ObjectiveAttributes { get; set; }
        public DbSet<Models.ObjectiveStatus> ObjectiveStatus { get; set; }
        public DbSet<Models.Parameter> Parameters { get; set; }
        public DbSet<Models.PermissionSet> PermissionSets { get; set; }
        public DbSet<Models.Policy> Policies { get; set; }
        public DbSet<Models.PriorityRating> PriorityRatings { get; set; }
        public DbSet<Models.ProfitCenter> ProfitCenters { get; set; }
        public DbSet<Models.RateModificationFactor> RateModificationFactors { get; set; }
        public DbSet<Models.RateModificationFactorType> RateModificationFactorTypes { get; set; }
        public DbSet<Models.RecClassification> RecClassifications { get; set; }
        public DbSet<Models.RecClassificationType> RecClassificationTypes { get; set; }
        public DbSet<Models.Recommendation> Recommendations { get; set; }
        public DbSet<Models.RecStatus> RecStatus { get; set; }
        public DbSet<Models.RecStatusType> RecStatusTypes { get; set; }
        public DbSet<Models.ReportType> ReportTypes { get; set; }
        public DbSet<Models.ReportTypeCompany> ReportTypeCompanies { get; set; }
        public DbSet<Models.RequestQueue> RequestQueue { get; set; }               //WA-141 Doug Bruce 2015-03-26 - RequestQueue DB Structure
        public DbSet<Models.RequestQueueStatus> RequestQueueStatuses { get; set; }   //WA-141 Doug Bruce 2015-03-26 - RequestQueue DB Structure
        public DbSet<Models.RequestQueueSearch> RequestQueueSearches { get; set; }   //WA-141 Doug Bruce 2015-03-26 - RequestQueue DB Structure
        public DbSet<Models.ReviewCategoryType> ReviewCategoryTypes { get; set; }
        public DbSet<Models.ReviewerType> ReviewerTypes { get; set; }
        public DbSet<Models.ReviewRatingType> ReviewRatingTypes { get; set; }
        public DbSet<Models.ServicePlan> ServicePlans { get; set; }
        public DbSet<Models.ServicePlanAction> ServicePlanActions { get; set; }
        public DbSet<Models.ServicePlanActionPermission> ServicePlanActionPermissions { get; set; }
        public DbSet<Models.ServicePlanActionType> ServicePlanActionTypes { get; set; }
        public DbSet<Models.ServicePlanActivity> ServicePlanActivities { get; set; }
        public DbSet<Models.ServicePlanAttribute> ServicePlanAttributes { get; set; }
        public DbSet<Models.ServicePlanDocument> ServicePlanDocuments { get; set; }
        public DbSet<Models.ServicePlanDynamicField> ServicePlanDynamicFields { get; set; }
        public DbSet<Models.ServicePlanGridColumnFilter> ServicePlanGridColumnFilters { get; set; }
        public DbSet<Models.ServicePlanGridColumnFilterCompany> ServicePlanGridColumnFilterCompanies { get; set; }
        public DbSet<Models.ServicePlanHistory> ServicePlanHistories { get; set; }
        public DbSet<Models.ServicePlanNote> ServicePlanNotes { get; set; }
        public DbSet<Models.ServicePlanStatus> ServicePlanStatus { get; set; }
        public DbSet<Models.ServicePlanType> ServicePlanTypes { get; set; }
        public DbSet<Models.ServiceType> ServiceTypes { get; set; }
        public DbSet<Models.SeverityRating> SeverityRatings { get; set; }
        public DbSet<Models.Sic> Sics { get; set; }
        public DbSet<Models.State> States { get; set; }
        public DbSet<Models.Survey> Surveys { get; set; }
        public DbSet<Models.SurveyAction> SurveyActions { get; set; }
        public DbSet<Models.SurveyActionPermission> SurveyActionPermissions { get; set; }
        public DbSet<Models.SurveyActionType> SurveyActionTypes { get; set; }
        public DbSet<Models.SurveyActivity> SurveyActivities { get; set; }
        public DbSet<Models.SurveyAttribute> SurveyAttributes { get; set; }
        public DbSet<Models.SurveyDocument> SurveyDocuments { get; set; }
        public DbSet<Models.SurveyGrading> SurveyGradings { get; set; }
        public DbSet<Models.SurveyGradingCompany> SurveyGradingCompanies { get; set; }
        public DbSet<Models.SurveyHistory> SurveyHistories { get; set; }
        public DbSet<Models.SurveyLetter> SurveyLetters { get; set; }
        public DbSet<Models.SurveyLocationNote> SurveyLocationNotes { get; set; }
        public DbSet<Models.SurveyLocationPolicyCoverageName> SurveyLocationPolicyCoverageNames { get; set; }
        public DbSet<Models.SurveyLocationPolicyExclusion> SurveyLocationPolicyExclusions { get; set; }
        public DbSet<Models.SurveyMergeDocumentField> SurveyMergeDocumentFields { get; set; }
        public DbSet<Models.SurveyNote> SurveyNotes { get; set; }
        public DbSet<Models.SurveyPolicyCoverage> SurveyPolicyCoverages { get; set; }
        public DbSet<Models.SurveyQuality> SurveyQualities { get; set; }
        public DbSet<Models.SurveyQualityType> SurveyQualityTypes { get; set; }
        public DbSet<Models.SurveyReasonType> SurveyReasonTypes { get; set; }
        public DbSet<Models.SurveyRecommendation> SurveyRecommendations { get; set; }
        public DbSet<Models.SurveyReleaseOwnership> SurveyReleaseOwnerships { get; set; }
        public DbSet<Models.SurveyReleaseOwnershipType> SurveyReleaseOwnershipTypes { get; set; }
        public DbSet<Models.SurveyReview> SurveyReviews { get; set; }
        public DbSet<Models.SurveyReviewQueue> SurveyReviewQueues { get; set; }
        public DbSet<Models.SurveyReviewRating> SurveyReviewRatings { get; set; }
        public DbSet<Models.SurveyStatus> SurveyStatus { get; set; }
        public DbSet<Models.SurveyStatusType> SurveyStatusTypes { get; set; }
        public DbSet<Models.SurveyType> SurveyTypes { get; set; }
        public DbSet<Models.SurveyTypeType> SurveyTypeTypes { get; set; }
        public DbSet<Models.Territory> Territories { get; set; }
        public DbSet<Models.TransactionType> TransactionTypes { get; set; }
        public DbSet<Models.Underwriter> Underwriters { get; set; }
        public DbSet<Models.User> Users { get; set; }
        public DbSet<Models.UserRole> UserRoles { get; set; }
        public DbSet<Models.UserSearch> UserSearches { get; set; }
        public DbSet<Models.UserSetting> UserSettings { get; set; }
        public DbSet<Models.UserTerritory> UserTerritories { get; set; }
        public DbSet<Models.WebPage> WebPages { get; set; }
        public DbSet<Models.WebPageHelpfulHint> WebPageHelpfulHints { get; set; }
        public DbSet<Models.Zip> Zips { get; set; }

        private static void LoadStandardDomainData()
        {
            var db = new MockDb();
        }
    }
}
