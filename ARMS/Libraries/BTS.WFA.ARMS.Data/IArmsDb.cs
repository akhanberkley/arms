using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using BTS.WFA.ARMS.Data.Models;

namespace BTS.WFA.ARMS.Data
{
    public interface IArmsDb : IDisposable
    {
        DbSet<Activity> Activities { get; set; }
        DbSet<ActivityDocument> ActivityDocuments { get; set; }
        DbSet<ActivityType> ActivityTypes { get; set; }
        DbSet<ActivityTypeLevel> ActivityTypeLevels { get; set; }
        DbSet<Agency> Agencies { get; set; }
        DbSet<AgencyEmail> AgencyEmails { get; set; }
        DbSet<AssignmentRule> AssignmentRules { get; set; }
        DbSet<AssignmentRuleAction> AssignmentRuleActions { get; set; }
        DbSet<AssignmentRuleActionType> AssignmentRuleActionTypes { get; set; }
        DbSet<AttributeDataType> AttributeDataTypes { get; set; }
        DbSet<Building> Buildings { get; set; }
        DbSet<BuildingValuationType> BuildingValuationTypes { get; set; }
        DbSet<Claim> Claims { get; set; }
        DbSet<Client> Clients { get; set; }
        DbSet<ClientAttribute> ClientAttributes { get; set; }
        DbSet<ClientAttributeType> ClientAttributeTypes { get; set; }
        DbSet<ClientHistory> ClientHistories { get; set; }
        DbSet<Company> Companies { get; set; }
        DbSet<CompanyAttribute> CompanyAttributes { get; set; }
        DbSet<CompanyCounter> CompanyCounters { get; set; }
        DbSet<CompanyCounterType> CompanyCounterTypes { get; set; }
        DbSet<CompanyLineOfBusiness> CompanyLineOfBusinesses { get; set; }
        DbSet<CompanyParameter> CompanyParameters { get; set; }
        DbSet<CompanyPolicySystem> CompanyPolicySystems { get; set; }
        DbSet<ContactReasonType> ContactReasonTypes { get; set; }
        DbSet<Coverage> Coverages { get; set; }
        DbSet<CoverageDetail> CoverageDetails { get; set; }
        DbSet<CoverageName> CoverageNames { get; set; }
        DbSet<CoverageNameType> CoverageNameTypes { get; set; }
        DbSet<CoverageType> CoverageTypes { get; set; }
        DbSet<CreateState> CreateStates { get; set; }
        DbSet<CreateSurveyType> CreateSurveyTypes { get; set; }
        DbSet<DocType> DocTypes { get; set; }
        DbSet<DueDateHistory> DueDateHistories { get; set; }
        DbSet<DynamicField> DynamicFields { get; set; }
        DbSet<EntityType> EntityTypes { get; set; }
        DbSet<ExceptionEmailFilterString> ExceptionEmailFilterStrings { get; set; }
        DbSet<ExceptionLog> ExceptionLogs { get; set; }
        DbSet<ExceptionReturnMessage> ExceptionReturnMessages { get; set; }
        DbSet<FeeUser> FeeUsers { get; set; }
        DbSet<Filter> Filters { get; set; }
        DbSet<FilterCondition> FilterConditions { get; set; }
        DbSet<FilterField> FilterFields { get; set; }
        DbSet<FilterOperator> FilterOperators { get; set; }
        DbSet<FormFieldDataType> FormFieldDataTypes { get; set; }
        DbSet<GeographicArea> GeographicAreas { get; set; }
        DbSet<GeographicUnit> GeographicUnits { get; set; }
        DbSet<GridColumnFilter> GridColumnFilters { get; set; }
        DbSet<GridColumnFilterCompany> GridColumnFilterCompanies { get; set; }
        DbSet<Insured> Insureds { get; set; }
        DbSet<LineOfBusiness> LineOfBusinesses { get; set; }
        DbSet<Link> Links { get; set; }
        DbSet<Location> Locations { get; set; }
        DbSet<LocationContact> LocationContacts { get; set; }
        DbSet<LoginHistory> LoginHistories { get; set; }
        DbSet<MergeDocument> MergeDocuments { get; set; }
        DbSet<MergeDocumentField> MergeDocumentFields { get; set; }
        DbSet<MergeDocumentType> MergeDocumentTypes { get; set; }
        DbSet<MergeDocumentVersion> MergeDocumentVersions { get; set; }
        DbSet<MimeType> MimeTypes { get; set; }
        DbSet<Naic> Naics { get; set; }
        DbSet<Notification> Notifications { get; set; }
        DbSet<Objective> Objectives { get; set; }
        DbSet<ObjectiveAttribute> ObjectiveAttributes { get; set; }
        DbSet<ObjectiveStatus> ObjectiveStatus { get; set; }
        DbSet<Parameter> Parameters { get; set; }
        DbSet<PermissionSet> PermissionSets { get; set; }
        DbSet<Policy> Policies { get; set; }
        DbSet<PriorityRating> PriorityRatings { get; set; }
        DbSet<ProfitCenter> ProfitCenters { get; set; }
        DbSet<RateModificationFactor> RateModificationFactors { get; set; }
        DbSet<RateModificationFactorType> RateModificationFactorTypes { get; set; }
        DbSet<RecClassification> RecClassifications { get; set; }
        DbSet<RecClassificationType> RecClassificationTypes { get; set; }
        DbSet<Recommendation> Recommendations { get; set; }
        DbSet<RecStatus> RecStatus { get; set; }
        DbSet<RecStatusType> RecStatusTypes { get; set; }
        DbSet<ReportType> ReportTypes { get; set; }
        DbSet<ReportTypeCompany> ReportTypeCompanies { get; set; }
        DbSet<RequestQueue> RequestQueue { get; set; }                     //WA-141 Doug Bruce 2015-03-26 - Queue DB Structure
        DbSet<RequestQueueStatus> RequestQueueStatuses { get; set; }         //WA-141 Doug Bruce 2015-03-26 - Queue DB Structure
        DbSet<RequestQueueSearch> RequestQueueSearches { get; set; }         //WA-141 Doug Bruce 2015-03-26 - Queue DB Structure
        DbSet<ReviewCategoryType> ReviewCategoryTypes { get; set; }
        DbSet<ReviewerType> ReviewerTypes { get; set; }
        DbSet<ReviewRatingType> ReviewRatingTypes { get; set; }
        DbSet<ServicePlan> ServicePlans { get; set; }
        DbSet<ServicePlanAction> ServicePlanActions { get; set; }
        DbSet<ServicePlanActionPermission> ServicePlanActionPermissions { get; set; }
        DbSet<ServicePlanActionType> ServicePlanActionTypes { get; set; }
        DbSet<ServicePlanActivity> ServicePlanActivities { get; set; }
        DbSet<ServicePlanAttribute> ServicePlanAttributes { get; set; }
        DbSet<ServicePlanDocument> ServicePlanDocuments { get; set; }
        DbSet<ServicePlanDynamicField> ServicePlanDynamicFields { get; set; }
        DbSet<ServicePlanGridColumnFilter> ServicePlanGridColumnFilters { get; set; }
        DbSet<ServicePlanGridColumnFilterCompany> ServicePlanGridColumnFilterCompanies { get; set; }
        DbSet<ServicePlanHistory> ServicePlanHistories { get; set; }
        DbSet<ServicePlanNote> ServicePlanNotes { get; set; }
        DbSet<ServicePlanStatus> ServicePlanStatus { get; set; }
        DbSet<ServicePlanType> ServicePlanTypes { get; set; }
        DbSet<ServiceType> ServiceTypes { get; set; }
        DbSet<SeverityRating> SeverityRatings { get; set; }
        DbSet<Sic> Sics { get; set; }
        DbSet<State> States { get; set; }
        DbSet<Survey> Surveys { get; set; }
        DbSet<SurveyAction> SurveyActions { get; set; }
        DbSet<SurveyActionPermission> SurveyActionPermissions { get; set; }
        DbSet<SurveyActionType> SurveyActionTypes { get; set; }
        DbSet<SurveyActivity> SurveyActivities { get; set; }
        DbSet<SurveyAttribute> SurveyAttributes { get; set; }
        DbSet<SurveyDocument> SurveyDocuments { get; set; }
        DbSet<SurveyGrading> SurveyGradings { get; set; }
        DbSet<SurveyGradingCompany> SurveyGradingCompanies { get; set; }
        DbSet<SurveyHistory> SurveyHistories { get; set; }
        DbSet<SurveyLetter> SurveyLetters { get; set; }
        DbSet<SurveyLocationNote> SurveyLocationNotes { get; set; }
        DbSet<SurveyLocationPolicyCoverageName> SurveyLocationPolicyCoverageNames { get; set; }
        DbSet<SurveyLocationPolicyExclusion> SurveyLocationPolicyExclusions { get; set; }
        DbSet<SurveyMergeDocumentField> SurveyMergeDocumentFields { get; set; }
        DbSet<SurveyNote> SurveyNotes { get; set; }
        DbSet<SurveyPolicyCoverage> SurveyPolicyCoverages { get; set; }
        DbSet<SurveyQuality> SurveyQualities { get; set; }
        DbSet<SurveyQualityType> SurveyQualityTypes { get; set; }
        DbSet<SurveyReasonType> SurveyReasonTypes { get; set; }
        DbSet<SurveyRecommendation> SurveyRecommendations { get; set; }
        DbSet<SurveyReleaseOwnership> SurveyReleaseOwnerships { get; set; }
        DbSet<SurveyReleaseOwnershipType> SurveyReleaseOwnershipTypes { get; set; }
        DbSet<SurveyReview> SurveyReviews { get; set; }
        DbSet<SurveyReviewQueue> SurveyReviewQueues { get; set; }
        DbSet<SurveyReviewRating> SurveyReviewRatings { get; set; }
        DbSet<SurveyStatus> SurveyStatus { get; set; }
        DbSet<SurveyStatusType> SurveyStatusTypes { get; set; }
        DbSet<SurveyType> SurveyTypes { get; set; }
        DbSet<SurveyTypeType> SurveyTypeTypes { get; set; }
        DbSet<Territory> Territories { get; set; }
        DbSet<TransactionType> TransactionTypes { get; set; }
        DbSet<Underwriter> Underwriters { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<UserRole> UserRoles { get; set; }
        DbSet<UserSearch> UserSearches { get; set; }
        DbSet<UserSetting> UserSettings { get; set; }
        DbSet<UserTerritory> UserTerritories { get; set; }
        DbSet<WebPage> WebPages { get; set; }
        DbSet<WebPageHelpfulHint> WebPageHelpfulHints { get; set; }
        DbSet<Zip> Zips { get; set; }

        DbContextTransaction GetTransaction(System.Data.IsolationLevel isoLevel = System.Data.IsolationLevel.ReadCommitted);
        int SaveChanges();
    }

}
