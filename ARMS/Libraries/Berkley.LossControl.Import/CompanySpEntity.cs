using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Berkley.BLC.Import
{
    public class CompanySpEntity
    {
        #region Type Enum
        public enum SpType
        {
            /// <summary>
            /// A stored procedure returning the insured information.
            /// </summary>
            Insured = 0,
            /// <summary>
            /// A stored procedure returning the agency information.
            /// </summary>
            Agency = 1,
            /// <summary>
            /// A stored procedure returning the location information.
            /// </summary>
            Location = 2,
            /// <summary>
            /// A stored procedure returning the building information of a location.
            /// </summary>
            Building = 3,
            /// <summary>
            /// A stored procedure returning the policy for a location.
            /// </summary>
            Policy = 4,
            /// <summary>
            /// A stored procedure returning the coverage for a policy.
            /// </summary>
            Coverage = 5,
            /// <summary>
            /// A stored procedure returning the claim against an insured.
            /// </summary>
            Claim = 6,
            /// <summary>
            /// A stored procedure returning the rate mod factors of a policy for an insured.
            /// </summary>
            RateModFactor = 7,
        }
        #endregion

        private SpType meType;
        private string strType;
        private string mstrName;
        private string mstrConnectionString;
        private string mstrClientId;
        private string mstrPolicyNumber;
        private string mstrAgentNumber;

        public CompanySpEntity()
        {
        }

        public CompanySpEntity(SpType eType, string strName, string strConnectionString, string strClientId, string strPolicyNumber, string strAgentNumber)
        {
            meType = eType;
            mstrName = strName;
            mstrConnectionString = strConnectionString;
            mstrClientId = strClientId;
            mstrPolicyNumber = strPolicyNumber;
            mstrAgentNumber = strAgentNumber;
        }

        public CompanySpEntity(string strEntityType, string strName, string strConnectionString, string strClientId, string strPolicyNumber, string strAgentNumber)
        {
            meType = ConvertToEnum(strEntityType);
            mstrName = strName;
            mstrConnectionString = strConnectionString;
            mstrClientId = strClientId;
            mstrPolicyNumber = strPolicyNumber;
            mstrAgentNumber = strAgentNumber;
        }

        public string Name
        {
            get { return mstrName; }
            set { mstrName = value; }
        }

        //[XmlElement("Key")]
        public string TypeAsString
        {
            get
            {
                return strType;
            }
            set
            {
                strType = value;
                meType = ConvertToEnum(strType);
            }
        }

        public SpType CompanySpType
        {
            get { return meType; }
        }

        public string ConnectionString
        {
            get { return mstrConnectionString; }
        }

        public string ClientId
        {
            get { return mstrClientId; }
        }

        public string PolicyNumber
        {
            get { return mstrPolicyNumber; }
        }

        public string AgentNumber
        {
            get { return mstrAgentNumber; }
        }

        private SpType ConvertToEnum(string strEnumName)
        {
            switch (strEnumName)
            {
                case "Insured":
                    return SpType.Insured;
                case "Agency":
                    return SpType.Agency;
                case "Location":
                    return SpType.Location;
                case "Building":
                    return SpType.Building;
                case "Policy":
                    return SpType.Policy;
                case "Coverage":
                    return SpType.Coverage;
                case "Claim":
                    return SpType.Claim;
                case "RateModFactor":
                    return SpType.RateModFactor;

                default:
                    throw new MissingFieldException("CompanySpEntity.ConvertToEnum", "Unknown EntityType: " + strEnumName);
            }
        }


    }
}
