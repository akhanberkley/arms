using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public interface INamedTypeEntity
	{
		string this[int iKey] { get; set; }
		Dictionary<int, string> NameDictionary { get; }
	}
}
