using System;
using System.Text.RegularExpressions;
//using BTS.WFA;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;
//using Berkley.BLC.Business;
using Berkley.BLC.Import.Prospect;

namespace Berkley.BLC.Import
{
    public class ImportFactory
    {
        private static Company _company;
        
        public ImportFactory(Company company)
        {
            if (company == null)
            {
                throw new ArgumentNullException("company");
            }

            _company = company; ;
        }

        /// <summary>
        /// Builds up a BlcInsured object from the data returned from the policy system and CWS.
        /// </summary>
        /// <param name="policyNumber">The policy number to query the policy system.</param>
        public BlcInsured ImportInsuredByPolicy(string policyNumber, bool importClaims)
        {
            BlcInsured result = new BlcInsured();

            switch (_company.CompanyAbbreviation)
            {
                case "BMG":
                    try
                    {
                        if (policyNumber.Length > 7)
                            policyNumber = policyNumber.Substring(2, policyNumber.Length - 2);
                        //if ((Berkley.BLC.Business.Utility.GetCompanyParameter("PolicyStarWAS8", _company).ToUpper() == "TRUE"))
                        //{
                        //    BMAGPolicyStarImportBuilderWAS8 bmagImportBuilder = new BMAGPolicyStarImportBuilderWAS8(policyNumber.Remove(0, 2));
                        //    result = bmagImportBuilder.BuildRequestFromPolicy();
                        //}
                        //else
                        //{
                        BMAGPolicyStarImportBuilder bmagImportBuilder = new BMAGPolicyStarImportBuilder(policyNumber);
                        result = bmagImportBuilder.BuildRequestFromPolicy();
                        //}

                        //BMAGPolicyStarImportBuilder bmagImportBuilder = new BMAGPolicyStarImportBuilder(policyNumber);
                        //result = bmagImportBuilder.BuildRequestFromPolicy();

                        // 10/27/2009: Need to also check for policies still in A+Plus for BMAG (Inland Marine)
                        BMAGImportBuilder bmagImportBuilder2 = new BMAGImportBuilder(result.InsuredEntity.ClientId, string.Empty, string.Empty);
                        BlcInsured aPlusResult = bmagImportBuilder2.BuildForPolicies();

                        // Grab any A+Plus policies, locations, buildings that were returned and add them to the P* result set
                        result.PolicyList.AddRange(aPlusResult.PolicyList);
                        result.CoverageList.AddRange(aPlusResult.CoverageList);
                        result.PolicyLocationList.AddRange(aPlusResult.PolicyLocationList);
                        result.BuildingList.AddRange(aPlusResult.BuildingList);
                    }
                    catch (BlcNoInsuredException)
                    {
                        try
                        {
                            BMAGImportBuilder bmagImportBuilder = new BMAGImportBuilder(string.Empty, _company.CompanyNumber + policyNumber, string.Empty);
                            result = bmagImportBuilder.Build(0);
                        }
                        catch (BlcNoInsuredException)
                        {
                            throw new BlcNoInsuredException(string.Format("Insured not found for Policy Number: {0}", policyNumber));
                        }
                    }
                    break;

                case "AIC":
                    try
                    {
                        AICPolicyStarImportBuilder aicImportBuilder = new AICPolicyStarImportBuilder(policyNumber);
                        result = aicImportBuilder.BuildRequestFromPolicy();

                        // Need to also check for policies still in A+Plus
                        AICImportBuilder aicImportBuilder2 = new AICImportBuilder(result.InsuredEntity.ClientId, string.Empty, string.Empty);
                        BlcInsured aPlusResult = aicImportBuilder2.BuildForPolicies();

                        // Grab any A+Plus policies, locations, buildings that were returned and add them to the P* result set
                        result.PolicyList.AddRange(aPlusResult.PolicyList);
                        result.CoverageList.AddRange(aPlusResult.CoverageList);
                        result.PolicyLocationList.AddRange(aPlusResult.PolicyLocationList);
                        result.BuildingList.AddRange(aPlusResult.BuildingList);
                    }
                    catch (BlcNoInsuredException)
                    {
                        try
                        {
                            AICImportBuilder aicImportBuilder = new AICImportBuilder(string.Empty, _company.CompanyNumber + policyNumber, string.Empty);
                            result = aicImportBuilder.Build(0);
                        }
                        catch (BlcNoInsuredException)
                        {
                            throw new BlcNoInsuredException(string.Format("Insured not found for Policy Number: {0}", policyNumber));
                        }
                    }
                    break;

                case "CWG":
                    try
                    {
                        //if ((Berkley.BLC.Business.Utility.GetCompanyParameter("PolicyStarWAS8", _company).ToUpper() == "TRUE"))
                        //{
                        //    CWGPolicyStarImportBuilderWAS8 cwgImportBuilder = new CWGPolicyStarImportBuilderWAS8(policyNumber.Remove(0, 2));
                        //    result = cwgImportBuilder.BuildRequestFromPolicy();
                        //}
                        //else
                        //{
                        CWGPolicyStarImportBuilder cwgImportBuilder = new CWGPolicyStarImportBuilder(policyNumber);
                        result = cwgImportBuilder.BuildRequestFromPolicy();
                        //}


                        //CWGPolicyStarImportBuilder cwgImportBuilder = new CWGPolicyStarImportBuilder(policyNumber);
                        //result = cwgImportBuilder.BuildRequestFromPolicy();

                        // Grab any A+Plus policies, locations, buildings that were returned and add them to the P* result set
                        CWGImportBuilder cwgImportBuilder2 = new CWGImportBuilder(result.InsuredEntity.ClientId, string.Empty, string.Empty);
                        BlcInsured aPlusResult = cwgImportBuilder2.BuildForPolicies();

                        // Grab any A+Plus policies, locations, buildings that were returned and add them to the P* result set
                        result.PolicyList.AddRange(aPlusResult.PolicyList);
                        result.CoverageList.AddRange(aPlusResult.CoverageList);
                        result.PolicyLocationList.AddRange(aPlusResult.PolicyLocationList);
                        result.BuildingList.AddRange(aPlusResult.BuildingList);
                    }
                    catch (BlcNoInsuredException)
                    {
                        try
                        {
                            CWGImportBuilder cwgImportBuilder = new CWGImportBuilder(string.Empty, _company.CompanyNumber + policyNumber, string.Empty);
                            result = cwgImportBuilder.Build(0);
                        }
                        catch (BlcNoInsuredException)
                        {
                            throw new BlcNoInsuredException(string.Format("Insured not found for Policy Number: {0}", policyNumber));
                        }
                    }
                    break;
                case "BARS":

                    try
                    {
                       
                        //if ((Berkley.BLC.Business.Utility.GetCompanyParameter("PolicyStarWAS8", _company).ToUpper() == "TRUE"))
                        //{
                        //    BARSPolicyStarImportBuilderWAS8 barsImportBuilder = new BARSPolicyStarImportBuilderWAS8(policyNumber.Remove(0, 2));
                        //    result = barsImportBuilder.BuildRequestFromPolicy();
                        //}
                        //else
                        //{
                            BARSPolicyStarImportBuilder barsImportBuilder = new BARSPolicyStarImportBuilder(policyNumber.Remove(0, 2));
                            result = barsImportBuilder.BuildRequestFromPolicy();
                        //}                  
                        
                        
                        // 01/16/2013: Need to also check for policies still in A+Plus
                        //_company = _company.GetWritableInstance();
                        _company.CompanyNumber = "90";
                        if (policyNumber.Substring(0, 2) == "21")
                            policyNumber = "90" + policyNumber.Substring(2, policyNumber.Length - 2);

                        BARSImportBuilder barsImportBuilder2 = new BARSImportBuilder(string.Empty, policyNumber, string.Empty);
                        BlcInsured aPlusResult = barsImportBuilder2.BuildForPolicies();
                        
                        // Grab any A+Plus policies, locations, buildings that were returned and add them to the P* result set
                        result.PolicyList.AddRange(aPlusResult.PolicyList);
                        result.CoverageList.AddRange(aPlusResult.CoverageList);
                        result.PolicyLocationList.AddRange(aPlusResult.PolicyLocationList);
                        result.BuildingList.AddRange(aPlusResult.BuildingList);

                        ////Finally check CWG - Hack - need to remove
                        //_company.Number = "40";
                        //CWGImportBuilder cwgImportBuilder2 = new CWGImportBuilder(result.InsuredEntity.ClientId, string.Empty, string.Empty);
                        //aPlusResult = cwgImportBuilder2.BuildForPolicies();

                        //// Grab any A+Plus policies, locations, buildings that were returned and add them to the P* result set
                        //result.PolicyList.AddRange(aPlusResult.PolicyList);
                        //result.CoverageList.AddRange(aPlusResult.CoverageList);
                        //result.PolicyLocationList.AddRange(aPlusResult.PolicyLocationList);
                        //result.BuildingList.AddRange(aPlusResult.BuildingList);


                        break;
                    }
                    catch (BlcNoInsuredException)
                    {
                        try
                        {
                            //_company = _company.GetWritableInstance();
                            _company.CompanyNumber = "90";
                            if (policyNumber.Substring(0, 2) == "21")
                                policyNumber = "90" + policyNumber.Substring(2, policyNumber.Length - 2);
                            BARSImportBuilder barsImportBuilder = new BARSImportBuilder(string.Empty, policyNumber, string.Empty);
                            result = barsImportBuilder.Build(0);
                            break;
                        }
                        catch (BlcNoInsuredException)
                        {
                            try
                            {
                                //USIGPolicyStarImportBuilder usigImportBuilder1 = new USIGPolicyStarImportBuilder(policyNumber.Remove(0, 2));
                                //result = usigImportBuilder1.BuildRequestFromPolicy();
                                _company.CompanyNumber = "40";
                                policyNumber = "40" + policyNumber.Substring(2, policyNumber.Length - 2);
                                CWGImportBuilder cwgImportBuilder2 = new CWGImportBuilder(string.Empty, policyNumber, string.Empty);
                                result = cwgImportBuilder2.BuildForPolicies();
                            }
                            catch (BlcNoInsuredException)
                            {
                                try
                                {
                                    //_company = _company.GetWritableInstance();
                                    _company.CompanyNumber = "80";
                                    if (policyNumber.Substring(0, 2) == "21")
                                        policyNumber = "80" + policyNumber.Substring(2, policyNumber.Length - 2);
                                    USIGImportBuilder usigImportBuilder1 = new USIGImportBuilder(string.Empty, policyNumber, string.Empty);
                                    result = usigImportBuilder1.Build(0);
                                    break;
                                }
                                catch (BlcNoInsuredException)
                                {
                                    try
                                    {
                                        if (policyNumber.Length == 7)
                                        {
                                            //this is used for refreshes only for BARS policies
                                            BARSImportBuilder barsImportBuilder = new BARSImportBuilder(string.Empty, "90" + policyNumber, string.Empty);
                                            result = barsImportBuilder.Build(0);

                                            ImportFactory agencyFactory = new ImportFactory(BlcCompany.BerkleyAgribusinessRiskSpecialists);
                                            string agencyNumber = (result.AgencyEntity != null) ? result.AgencyEntity.AgencyNumber : string.Empty;
                                            result.AgencyEntity = agencyFactory.ImportAgency(agencyNumber, policyNumber);
                                            break;
                                        }
                                        else
                                        {
                                            throw new BlcNoInsuredException(string.Format("Insured not found for Policy Number: {0}", policyNumber));
                                        }
                                    }
                                    catch (BlcNoInsuredException)
                                    {
                                        if (policyNumber.Length == 7)
                                        {
                                            //this is used for refreshes only for BARS/CWG policies
                                            BARSImportBuilder barsImportBuilder = new BARSImportBuilder(string.Empty, "40" + policyNumber, string.Empty);
                                            result = barsImportBuilder.Build(0);

                                            ImportFactory agencyFactory = new ImportFactory(BlcCompany.ContinentalWesternGroup);
                                            string agencyNumber = (result.AgencyEntity != null) ? result.AgencyEntity.AgencyNumber : string.Empty;
                                            result.AgencyEntity = agencyFactory.ImportAgency(agencyNumber, policyNumber);
                                            break;
                                        }
                                        else
                                        {
                                            throw new BlcNoInsuredException(string.Format("Insured not found for Policy Number: {0}", policyNumber));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                case "USG":
                                                      
                    if (policyNumber.Length > 7)
                        policyNumber = policyNumber.Substring(2, policyNumber.Length - 2);

                    try
                    {
                    USIGPolicyStarImportBuilder usigImportBuilder = new USIGPolicyStarImportBuilder(policyNumber);
                    result = usigImportBuilder.BuildRequestFromPolicy();
                    }
                    catch (BlcNoInsuredException)
                    {
                        if ((Utility.GetCompanyParameter("USIGBSIGLookup", _company).ToUpper() == "TRUE"))
                        {
                            try
                            {
                                USIGPolicyStarImportBuilder usigImportBuilder = new USIGPolicyStarImportBuilder(policyNumber);
                                result = usigImportBuilder.BuildRequestFromPolicy(BlcCompany.UnionStandardInsuranceGroup, "16");
                            }
                            catch (BlcNoInsuredException)
                            {
                                throw new BlcNoInsuredException(string.Format("Insured not found for Policy Number: {0}", policyNumber));
                            }
                        }
                    }

                    //UNDO ME : Vilas Meshram : 05/30/2013
                    //// 10/27/2009: Need to also check for policies still in A+Plus for USIG 
                    //USIGImportBuilder usigImportBuilder2 = new USIGImportBuilder(result.InsuredEntity.ClientID, string.Empty, string.Empty);
                    //BlcInsured aPlusResultusig = usigImportBuilder2.BuildForPolicies();

                    //// Grab any A+Plus policies, locations, buildings that were returned and add them to the P* result set
                    //result.PolicyList.AddRange(aPlusResultusig.PolicyList);
                    //result.CoverageList.AddRange(aPlusResultusig.CoverageList);
                    //result.PolicyLocationList.AddRange(aPlusResultusig.PolicyLocationList);
                    //result.BuildingList.AddRange(aPlusResultusig.BuildingList);

                    //UNDO ME : Vilas Meshram : 05/30/2013
                    break;
                case "BNPG":
                     try
                    {
                        BNPGImportBuilder bnpgImportBuilder = new BNPGImportBuilder(string.Empty, policyNumber, string.Empty);
                        result = bnpgImportBuilder.Build(0);
                        break;       
                    }
                    catch (BlcNoInsuredException)
                    {
                        try
                        {
                            if ((Utility.GetCompanyParameter("BNPGGenesys", _company).ToUpper() == "TRUE"))
                            {
                                if (policyNumber.Length > 7)
                                    policyNumber = policyNumber.Substring(1, policyNumber.Length - 1);
                                BNPGPolicyStarImportBuilder bnpgImportBuilder = new BNPGPolicyStarImportBuilder(policyNumber);
                                result = bnpgImportBuilder.BuildRequestFromPolicy();

                                // Need to also check for policies still in A+Plus
                                BNPGImportBuilder bnpgImportBuilder2 = new BNPGImportBuilder(result.InsuredEntity.ClientId, string.Empty, string.Empty);
                                BlcInsured aPlusResult = bnpgImportBuilder2.BuildForPolicies();

                                // Grab any A+Plus policies, locations, buildings that were returned and add them to the P* result set
                                result.PolicyList.AddRange(aPlusResult.PolicyList);
                                result.CoverageList.AddRange(aPlusResult.CoverageList);
                                result.PolicyLocationList.AddRange(aPlusResult.PolicyLocationList);
                                result.BuildingList.AddRange(aPlusResult.BuildingList);
                            }
                            else
                                throw new BlcNoInsuredException(string.Format("Insured not found for Policy Number: {0}", policyNumber));
                        }
                        catch (BlcNoInsuredException)
                        {
                            throw new BlcNoInsuredException(string.Format("Insured not found for Policy Number: {0}", policyNumber));
                        }
                    }
                    break;
                    //try
                    //{
                    //    //BNPGImportBuilder bnpgImportBuilder = new BNPGImportBuilder(string.Empty, policyNumber, string.Empty);
                    //    //result = bnpgImportBuilder.Build(0);
                    //    BNPGPolicyStarImportBuilder bnpgImportBuilder = new BNPGPolicyStarImportBuilder(policyNumber);
                    //    result = bnpgImportBuilder.BuildRequestFromPolicy();
                     
                    //    break;
                    //}
                    //catch (BlcNoInsuredException)
                    //{
                    //    if (policyNumber.Length == 7)
                    //    {
                    //        //this is used for refreshes only for BNP/CWG policies
                    //        BNPGImportBuilder bnpgImportBuilder = new BNPGImportBuilder(string.Empty, "09" + policyNumber, string.Empty);
                    //        result = bnpgImportBuilder.Build(0);

                    //        ImportFactory agencyFactory = new ImportFactory(Company.BerkleyNorthPacificGroup);
                    //        result.AgencyEntity = agencyFactory.ImportAgency(string.Empty, policyNumber);
                    //        break;
                    //    }
                    //    else
                    //    {
                    //        throw new BlcNoInsuredException(string.Format("Insured not found for Policy Number: {0}", policyNumber));
                    //    }
                    //}

                case "CCI":

                    try
                    {
                        CCICPolicyStarImportBuilder ccicImportBuilder = new CCICPolicyStarImportBuilder(policyNumber);
                        result = ccicImportBuilder.BuildRequestFromPolicy();
                    }
                    catch (BlcNoInsuredException)
                    {
                        if (policyNumber.Length > 7)
                            policyNumber = policyNumber.Substring(2, policyNumber.Length - 2);

                        string CCICurl = Utility.GetCompanyParameter("CCICGenesysURL", _company).ToString();
                        if (!string.IsNullOrWhiteSpace(CCICurl))
                        {
                            CCICPolicyStarImportBuilder ccicImportBuilderG = new CCICPolicyStarImportBuilder(policyNumber);
                            result = ccicImportBuilderG.BuildRequestFromPolicy(CCICurl);
                            break;
                        }
                        else
                            throw new BlcNoInsuredException("Insured not found");
                       
                     }
 
                    break;

                case "BLS":
                   
                    //if ((Berkley.BLC.Business.Utility.GetCompanyParameter("PolicyStarWAS8", _company).ToUpper() == "TRUE"))
                    //{
                    //    BLSPolicyStarImportBuilderWAS8 blsImportBuilder = new BLSPolicyStarImportBuilderWAS8(policyNumber);
                    //    result = blsImportBuilder.BuildRequestFromPolicy();
                    //}
                    //else
                    //{
                        BLSPolicyStarImportBuilder blsImportBuilder = new BLSPolicyStarImportBuilder(policyNumber);
                        result = blsImportBuilder.BuildRequestFromPolicy();
                    //}

                        //BLSPolicyStarImportBuilder blsImportBuilder = new BLSPolicyStarImportBuilder(policyNumber);
                        //result = blsImportBuilder.BuildRequestFromPolicy();
                    
                    break;

                case "BOG":
                    BOGImportBuilder bogImportBuilder = new BOGImportBuilder();
                    result = bogImportBuilder.BuildRequestFromPolicy(policyNumber);
                    break;

                case "BSUM":
                    try
                    {
                        BSUMPolicyStarImportBuilder bsumImportBuilder = new BSUMPolicyStarImportBuilder(policyNumber);
                        result = bsumImportBuilder.BuildRequestFromPolicy();

                        // Grab any A+Plus policies, locations, buildings that were returned and add them to the P* result set
                        BSUMImportBuilder bsumImportBuilder2 = new BSUMImportBuilder(result.InsuredEntity.ClientId, string.Empty, string.Empty);
                        BlcInsured aPlusResult = bsumImportBuilder2.BuildForPolicies();

                        // Grab any A+Plus policies, locations, buildings that were returned and add them to the P* result set
                        result.PolicyList.AddRange(aPlusResult.PolicyList);
                        result.CoverageList.AddRange(aPlusResult.CoverageList);
                        result.PolicyLocationList.AddRange(aPlusResult.PolicyLocationList);
                        result.BuildingList.AddRange(aPlusResult.BuildingList);
                    }
                    catch (BlcNoInsuredException)
                    {
                        try
                        {
                            BSUMImportBuilder BSUMImportBuilder = new BSUMImportBuilder(string.Empty, "23" + TrimPolicySymbol(policyNumber), string.Empty);
                            result = BSUMImportBuilder.Build(0);
                        }
                        catch (BlcNoInsuredException)
                        {
                            throw new BlcNoInsuredException(string.Format("Insured not found for Policy Number: {0}", policyNumber));
                        }
                    }
                    break;
                case "BFM":
                    if (policyNumber.Length > 7)
                        policyNumber = policyNumber.Substring(2, policyNumber.Length - 2);
                    BFMPolicyStarImportBuilder bfmImportBuilder = new BFMPolicyStarImportBuilder(policyNumber);
                    result = bfmImportBuilder.BuildRequestFromPolicy(BlcCompany.BerkleyFireMarine);
                    break;

                case "BSIG":
                    if (policyNumber.Length > 7)
                        policyNumber = policyNumber.Substring(2, policyNumber.Length - 2);
                    BSIGPolicyStarImportBuilder bsigImportBuilder = new BSIGPolicyStarImportBuilder(policyNumber);
                    result = bsigImportBuilder.BuildRequestFromPolicy(BlcCompany.BerkleySouthEast);
                    _company = BlcCompany.BerkleyMidAtlanticGroup;
                    break;

                case "BTU":
                    if (policyNumber.Length > 7)
                        policyNumber = policyNumber.Substring(2, policyNumber.Length - 2);
                    BTUPolicyStarImportBuilder btuImportBuilder = new BTUPolicyStarImportBuilder(policyNumber);
                    result = btuImportBuilder.BuildRequestFromPolicy(BlcCompany.BerkleyTechnologyUnderwriters);
                    break;

                case "RIC":
                    if (policyNumber.Length > 7)
                        policyNumber = policyNumber.Substring(2, policyNumber.Length - 2);
                    RICPolicyStarImportBuilder ricImportBuilder = new RICPolicyStarImportBuilder(policyNumber);
                    result = ricImportBuilder.BuildRequestFromPolicy(BlcCompany.RiverportInsuranceCompany);
                    break;

                default:
                    throw new NotSupportedException(string.Format("Was not expecting company '{0}' to be passed.", _company.CompanyAbbreviation));
            }

            if (importClaims && _company.YearsOfLossHistory > 0)
            {
                //get the claims and attach to the BlcInsured
                ClaimWebService service = new ClaimWebService();
                service.GetClaims(ref result, _company);
            }

            return result;
        }

        /// <summary>
        /// Trims the policy symbol from the policy number
        /// </summary>
        /// <param name="strPolicyNumber">The policy number</param>
        protected string TrimPolicySymbol(string strPolicyNumber)
        {
            // any non-digit character match at the front of the string
            string pattern = @"^\D+";
            Regex rx = new Regex(pattern);
            return rx.Replace(strPolicyNumber, "");
        }

        /// <summary>
        /// Builds up a a list of claims from the passed policies.
        /// </summary>
        /// <param name="policies">The list of policies to get claims.</param>
        public BlcClaim[] ImportClaims(Policy[] policies)
        {
            ClaimWebService service = new ClaimWebService();
            return service.GetClaims(policies, _company);
        }

        /// <summary>
        /// Builds up a BlcInsured object from the data returned from the policy system by a quote.
        /// </summary>
        /// <param name="quoteNumber">The quote number to query the policy system.</param>
        public BlcInsured ImportInsuredByQuote(string quoteNumber)
        {
            BlcInsured result = new BlcInsured();

            switch (_company.CompanyAbbreviation)
            {
                case "BMG":

                    //if ((Berkley.BLC.Business.Utility.GetCompanyParameter("PolicyStarWAS8", _company).ToUpper() == "TRUE"))
                    //{
                    //    BMAGPolicyStarImportBuilderWAS8 bmagImportBuilder = new BMAGPolicyStarImportBuilderWAS8(quoteNumber);
                    //    result = bmagImportBuilder.BuildRequestFromQuote();
                    //}
                    //else
                    //{
                    BMAGPolicyStarImportBuilder bmagImportBuilder = new BMAGPolicyStarImportBuilder(quoteNumber);
                    result = bmagImportBuilder.BuildRequestFromQuote();
                    //}

                    //BMAGPolicyStarImportBuilder bmagImportBuilder = new BMAGPolicyStarImportBuilder(quoteNumber);
                    //result = bmagImportBuilder.BuildRequestFromQuote();
                    break;

                case "AIC":
                    AICPDRImportBuilder aicImportBuilder = new AICPDRImportBuilder(quoteNumber);
                    result = aicImportBuilder.BuildRequestFromQuote();
                    break;

                default:
                    throw new NotSupportedException(string.Format("Was not expecting company '{0}' to be passed.", _company.CompanyAbbreviation));
            }

            return result;
        }

        /// <summary>
        /// Determines whether the polciy number passed is valid.
        /// </summary>
        /// <param name="policyNumber">The policy number to query the policy system.</param>
        public bool ValidPolicy(string policyNumber)
        {
            bool valid = false;

            switch (_company.CompanyAbbreviation)
            {
                case "BMG":

                    //if ((Berkley.BLC.Business.Utility.GetCompanyParameter("PolicyStarWAS8", _company).ToUpper() == "TRUE"))
                    //{
                    //    BMAGPolicyStarImportBuilderWAS8 bmagImportBuilder = new BMAGPolicyStarImportBuilderWAS8(policyNumber);
                    //    valid = bmagImportBuilder.PolicyExists();
                    //}
                    //else
                    //{
                    BMAGPolicyStarImportBuilder bmagImportBuilder = new BMAGPolicyStarImportBuilder(policyNumber);
                    valid = bmagImportBuilder.PolicyExists();
                    //}

                    //BMAGPolicyStarImportBuilder bmagImportBuilder = new BMAGPolicyStarImportBuilder(policyNumber);
                    //valid = bmagImportBuilder.PolicyExists();

                    // check A+Plus
                    if (!valid)
                    {
                        BMAGImportBuilder bmagImportBuilder2 = new BMAGImportBuilder(string.Empty, _company.CompanyNumber + policyNumber, string.Empty);
                        Insured insured = bmagImportBuilder2.BuildForAccountStatus();

                        valid = (insured != null);
                    }
                    break;

                case "AIC":
                    AICPolicyStarImportBuilder aicImportBuilder = new AICPolicyStarImportBuilder(policyNumber);
                    valid = aicImportBuilder.PolicyExists();

                    // check A+Plus
                    if (!valid)
                    {
                        AICImportBuilder aicImportBuilder2 = new AICImportBuilder(string.Empty, _company.CompanyNumber + policyNumber, string.Empty);
                        Insured insured = aicImportBuilder2.BuildForAccountStatus();

                        valid = (insured != null);
                    }
                    break;

                case "CCI":

                    //if ((Berkley.BLC.Business.Utility.GetCompanyParameter("PolicyStarWAS8", _company).ToUpper() == "TRUE"))
                    //{
                    //    CCICPolicyStarImportBuilderWAS8 ccicImportBuilder = new CCICPolicyStarImportBuilderWAS8(policyNumber);
                    //    valid = ccicImportBuilder.PolicyExists();
                    //}
                    //else
                    //{
                    CCICPolicyStarImportBuilder ccicImportBuilder = new CCICPolicyStarImportBuilder(policyNumber);
                    valid = ccicImportBuilder.PolicyExists();
                    //}

                    //CCICPolicyStarImportBuilder ccicImportBuilder = new CCICPolicyStarImportBuilder(policyNumber);
                    //valid = ccicImportBuilder.PolicyExists();
                    break;

                default:
                    throw new NotSupportedException(string.Format("Was not expecting company '{0}' to be passed.", _company.CompanyAbbreviation));
            }

            return valid;
        }

        /// <summary>
        /// Builds up an Agency object from the data returned from the policy system.
        /// </summary>
        /// <param name="agencyNumber">The agency number to query the policy system.</param>
        public Agency ImportAgency(string agencyNumber, string policyNumber)
        {
            Agency result = null;

            policyNumber = (policyNumber.Length > 0) ? _company.CompanyNumber + policyNumber : string.Empty;

            switch (_company.CompanyAbbreviation)
            {
                case "BMG":
                    //if ((Berkley.BLC.Business.Utility.GetCompanyParameter("PolicyStarWAS8", _company).ToUpper() == "TRUE"))
                    //{
                    //    BMAGPolicyStarImportBuilderWAS8 bmagImportBuilder = new BMAGPolicyStarImportBuilderWAS8(string.Empty);
                    //    result = bmagImportBuilder.BuildForAgency(agencyNumber);
                    //}
                    //else
                    //{
                    BMAGPolicyStarImportBuilder bmagImportBuilder = new BMAGPolicyStarImportBuilder(string.Empty);
                    result = bmagImportBuilder.BuildForAgency(agencyNumber);
                    //}
                    break;

                case "AIC":
                    AICPolicyStarImportBuilder aicImportBuilder = new AICPolicyStarImportBuilder(string.Empty);
                    result = aicImportBuilder.BuildForAgency(agencyNumber);
                    break;

                case "CWG":
                    //if ((Berkley.BLC.Business.Utility.GetCompanyParameter("PolicyStarWAS8", _company).ToUpper() == "TRUE"))
                    //{
                    //    CWGPolicyStarImportBuilderWAS8 cwgImportBuilder = new CWGPolicyStarImportBuilderWAS8(string.Empty);
                    //    result = cwgImportBuilder.BuildForAgency(agencyNumber);
                    //}
                    //else
                    //{
                    CWGPolicyStarImportBuilder cwgImportBuilder = new CWGPolicyStarImportBuilder(string.Empty);
                    result = cwgImportBuilder.BuildForAgency(agencyNumber);
                    //}
                    break;

                case "BARS":
                     BARSPolicyStarImportBuilder barsImportBuilder = new BARSPolicyStarImportBuilder(string.Empty);
                        result = barsImportBuilder.BuildForAgency(agencyNumber);

                    //BARSImportBuilder barsImportBuilder = new BARSImportBuilder(string.Empty, policyNumber, agencyNumber);
                    //result = barsImportBuilder.BuildForAgency();
                    break;

                case "USG":
                    USIGPolicyStarImportBuilder usigImportBuilder = new USIGPolicyStarImportBuilder(string.Empty);
                    result = usigImportBuilder.BuildForAgency(agencyNumber);
                    break;

                case "CCI":
                    ProspectWebService cciService = new ProspectWebService();
                    System.Data.DataSet cciDS = cciService.GetAgencyDetails(16, agencyNumber);

                    ProspectBuilder cciImportBuilder = new ProspectBuilder();
                    result = cciImportBuilder.BuildAgency(cciDS);
                    break;

                case "BLS":
                    ProspectWebService blsService = new ProspectWebService();
                    System.Data.DataSet blsDS = blsService.GetAgencyDetails(30, agencyNumber);

                    ProspectBuilder blsImportBuilder = new ProspectBuilder();
                    result = blsImportBuilder.BuildAgency(blsDS);
                    break;

                case "BNPG":
                    //BNPGImportBuilder bnpgImportBuilder = new BNPGImportBuilder(string.Empty, string.Empty, string.Empty);
                    //result = bnpgImportBuilder.BuildForAgency(agencyNumber);
                    BNPGPolicyStarImportBuilder bnpgImportBuilder = new BNPGPolicyStarImportBuilder(string.Empty);
                      result = bnpgImportBuilder.BuildForAgency(agencyNumber);
                    break;

                case "BOG":
                    BOGImportBuilder bogImportBuilder = new BOGImportBuilder();
                    result = bogImportBuilder.BuildForAgency(agencyNumber);
                    break;

                case "BSUM":
                    //BSUMImportBuilder bsumImportBuilder = new BSUMImportBuilder();
                    //result = bsumImportBuilder.BuildForAgency(agencyNumber);
                    break;

                case "BTU":
                    BTUPolicyStarImportBuilder btuImportBuilder = new BTUPolicyStarImportBuilder(string.Empty);
                    result = btuImportBuilder.BuildForAgency(agencyNumber);
                    break;

                case "RIC":
                    RICPolicyStarImportBuilder ricImportBuilder = new RICPolicyStarImportBuilder(string.Empty);
                    result = ricImportBuilder.BuildForAgency(agencyNumber);
                    break;

            }

            return result;
        }

        /// <summary>
        /// Builds up an empty BlcInsured object with the default coverages added per line of business.
        /// </summary>
        /// <param name="policyNumber">The policy number.</param>
        /// <param name="policyMod">The policy mod.</param>
        /// <param name="lineOfBusiness">The line of business.</param>
        public BlcInsured GetEmptyCoverages(BTS.WFA.ARMS.Data.Models.LineOfBusiness lineOfBusiness)
        {
            BlcInsured result = new BlcInsured();

            switch (_company.CompanyAbbreviation)
            {
                case "BMG":
                    BMAGImportBuilder bmagImportBuilder = new BMAGImportBuilder(string.Empty, string.Empty, string.Empty);
                    result = bmagImportBuilder.BuildEmptyCoverages(lineOfBusiness.LineOfBusinessCode);
                    break;

                case "AIC":
                    AICImportBuilder aicImportBuilder = new AICImportBuilder(string.Empty, string.Empty, string.Empty);
                    result = aicImportBuilder.BuildEmptyCoverages(lineOfBusiness.LineOfBusinessCode);
                    break;

                case "CWG":
                    CWGImportBuilder cwgImportBuilder = new CWGImportBuilder(string.Empty, string.Empty, string.Empty);
                    result = cwgImportBuilder.BuildEmptyCoverages(lineOfBusiness.LineOfBusinessCode);
                    break;

                case "BARS":
                    BARSImportBuilder barsImportBuilder = new BARSImportBuilder(string.Empty, string.Empty, string.Empty);
                    result = barsImportBuilder.BuildEmptyCoverages(lineOfBusiness.LineOfBusinessCode);
                    break;

                case "USG":
                    USIGImportBuilder usigImportBuilder = new USIGImportBuilder(string.Empty, string.Empty, string.Empty);
                    result = usigImportBuilder.BuildEmptyCoverages(lineOfBusiness.LineOfBusinessCode);
                    break;

                case "BNPG":
                    BNPGImportBuilder bnpgImportBuilder = new BNPGImportBuilder(string.Empty, string.Empty, string.Empty);
                    result = bnpgImportBuilder.BuildEmptyCoverages(lineOfBusiness.LineOfBusinessCode);
                    break;

                case "CCI":
                    CCICPolicyStarImportBuilder ccicImportBuilder = new CCICPolicyStarImportBuilder(string.Empty);
                    result = ccicImportBuilder.BuildEmptyCoverages(lineOfBusiness.LineOfBusinessCode);
                    break;

                case "BLS":
                    BLSPolicyStarImportBuilder blsImportBuilder = new BLSPolicyStarImportBuilder(string.Empty);
                    result = blsImportBuilder.BuildEmptyCoverages(lineOfBusiness.LineOfBusinessCode);
                    break;

                case "BOG":
                    BOGImportBuilder bogImportBuilder = new BOGImportBuilder();
                    result = bogImportBuilder.BuildEmptyCoverages(lineOfBusiness.LineOfBusinessCode);
                    break;

                case "BSUM":
                    BSUMImportBuilder bsumImportBuilder = new BSUMImportBuilder(string.Empty, string.Empty, string.Empty);
                    result = bsumImportBuilder.BuildEmptyCoverages(lineOfBusiness.LineOfBusinessCode);
                    break;

                case "BTU":
                    BTUPolicyStarImportBuilder btuImportBuilder = new BTUPolicyStarImportBuilder(string.Empty);
                    result = btuImportBuilder.BuildEmptyCoverages(lineOfBusiness.LineOfBusinessCode);
                    break;

                case "RIC":
                    RICPolicyStarImportBuilder ricImportBuilder = new RICPolicyStarImportBuilder(string.Empty);
                    result = ricImportBuilder.BuildEmptyCoverages(lineOfBusiness.LineOfBusinessCode);
                    break;

                default:
                    throw new NotSupportedException(string.Format("Was not expecting company '{0}' to be passed.", _company.CompanyAbbreviation));
            }

            return result;
        }

        /// <summary>
        /// Populates an Insured object with the non-renewed status from the data returned from the policy system.
        /// </summary>
        /// <param name="policyNumber">The policy number to query the policy system.</param>
        public Insured ImportOnlyInsuredForAccountStatus(string policyNumber, string clientId)
        {
            Insured result = null;

            switch (_company.CompanyAbbreviation)
            {
                case "BMG":
                    try
                    {
                        //if ((Berkley.BLC.Business.Utility.GetCompanyParameter("PolicyStarWAS8", _company).ToUpper() == "TRUE"))
                        //{
                        //    BMAGPolicyStarImportBuilderWAS8 bmagImportBuilder = new BMAGPolicyStarImportBuilderWAS8(clientID);
                        //    result = bmagImportBuilder.BuildForAccountStatus(clientID);
                        //}
                        //else
                        //{
                        BMAGPolicyStarImportBuilder bmagImportBuilder = new BMAGPolicyStarImportBuilder(clientId);
                        result = bmagImportBuilder.BuildForAccountStatus(clientId);
                        //}
                    }
                    catch (BlcNoInsuredException)
                    {
                        BMAGImportBuilder bmagImportBuilder = new BMAGImportBuilder(string.Empty, _company.CompanyNumber + policyNumber, string.Empty);
                        result = bmagImportBuilder.BuildForAccountStatus();
                    }
                    break;

                case "AIC":
                    try
                    {
                        AICPolicyStarImportBuilder aicImportBuilder = new AICPolicyStarImportBuilder(clientId);
                        result = aicImportBuilder.BuildForAccountStatus(clientId);
                    }
                    catch (BlcNoInsuredException)
                    {
                        AICImportBuilder aicImportBuilder = new AICImportBuilder(string.Empty, _company.CompanyNumber + policyNumber, string.Empty);
                        result = aicImportBuilder.BuildForAccountStatus();
                    }
                    break;

                case "CWG":
                    try
                    {
                        //if ((Berkley.BLC.Business.Utility.GetCompanyParameter("PolicyStarWAS8", _company).ToUpper() == "TRUE"))
                        //{
                        //    CWGPolicyStarImportBuilderWAS8 cwgImportBuilder = new CWGPolicyStarImportBuilderWAS8(clientID);
                        //    result = cwgImportBuilder.BuildForAccountStatus(clientID);
                        //}
                        //else
                        //{
                        CWGPolicyStarImportBuilder cwgImportBuilder = new CWGPolicyStarImportBuilder(clientId);
                        result = cwgImportBuilder.BuildForAccountStatus(clientId);
                        //}
                    }
                    catch (BlcNoInsuredException)
                    {
                        CWGImportBuilder cwgImportBuilder = new CWGImportBuilder(string.Empty, BlcCompany.ContinentalWesternGroup.CompanyNumber + policyNumber, string.Empty);
                        result = cwgImportBuilder.BuildForAccountStatus();
                    }
                    break;

                case "BNPG":
                    BNPGImportBuilder bnpgImportBuilder = new BNPGImportBuilder(string.Empty, BlcCompany.BerkleyNorthPacificGroup.CompanyNumber + policyNumber, string.Empty);
                    result = bnpgImportBuilder.BuildForAccountStatus();
                    break;

                case "BARS":
                    BARSImportBuilder barsImportBuilder = new BARSImportBuilder(string.Empty, _company.CompanyNumber + policyNumber, string.Empty);
                    result = barsImportBuilder.BuildForAccountStatus();
                    break;

                case "USG":
                    USIGImportBuilder usigImportBuilder = new USIGImportBuilder(string.Empty, _company.CompanyNumber + policyNumber, string.Empty);
                    result = usigImportBuilder.BuildForAccountStatus();
                    break;

                case "CCI":
                    //if ((Berkley.BLC.Business.Utility.GetCompanyParameter("PolicyStarWAS8", _company).ToUpper() == "TRUE"))
                    //{
                    //    CCICPolicyStarImportBuilderWAS8 ccicImportBuilder = new CCICPolicyStarImportBuilderWAS8(clientID);
                    //    result = ccicImportBuilder.BuildForAccountStatus(clientID);
                    //}
                    //else
                    //{
                    CCICPolicyStarImportBuilder ccicImportBuilder = new CCICPolicyStarImportBuilder(clientId);
                    result = ccicImportBuilder.BuildForAccountStatus(clientId);
                    //}
                    break;

                case "BLS":
                    BLSPolicyStarImportBuilder blsImportBuilder = new BLSPolicyStarImportBuilder(clientId);
                    result = blsImportBuilder.BuildForAccountStatus(clientId);
                    break;

                case "BOG":
                    BOGImportBuilder bogImportBuilder = new BOGImportBuilder();
                    result = new Insured();
                    result.NonrenewedDate = bogImportBuilder.BuildForAccountStatus(clientId);
                    break;

                case "BTU":
                    BTUPolicyStarImportBuilder btuImportBuilder = new BTUPolicyStarImportBuilder(clientId);
                    result = btuImportBuilder.BuildForAccountStatus(clientId);
                    break;

                case "RIC":
                    RICPolicyStarImportBuilder ricImportBuilder = new RICPolicyStarImportBuilder(clientId);
                    result = ricImportBuilder.BuildForAccountStatus(clientId);
                    break;

                default:
                    throw new NotSupportedException(string.Format("Was not expecting company '{0}' to be passed.", _company.CompanyAbbreviation));
            }
            return result;
        }

        /// <summary>
        /// The line of business code.
        /// </summary>
        /// <param name="policySymbol">The policy symbol from which to derive the lob.</param>
        public string DetermineLOB(string policySymbol)
        {
            LineOfBusiness.LineOfBusinessType lobType;
            string lobCode = string.Empty;

            if (_company.CompanyId == BlcCompany.BerkleyMidAtlanticGroup.CompanyId)
            {
                BMAGLineOfBusiness lineOfBusiness = new BMAGLineOfBusiness();
                lobType = lineOfBusiness.GetType(policySymbol);
                lobCode = lineOfBusiness.GetBlcName(lobType);
            }
            else if (_company.CompanyId == BlcCompany.AcadiaInsuranceCompany.CompanyId)
            {
                AICLineOfBusiness lineOfBusiness = new AICLineOfBusiness();
                lobType = lineOfBusiness.GetType(policySymbol);
                lobCode = lineOfBusiness.GetBlcName(lobType);
            }
            else if (_company.CompanyId == BlcCompany.ContinentalWesternGroup.CompanyId)
            {
                CWGLineOfBusiness lineOfBusiness = new CWGLineOfBusiness();
                lobType = lineOfBusiness.GetType(policySymbol);
                lobCode = lineOfBusiness.GetBlcName(lobType);
            }
            else if (_company.CompanyId == BlcCompany.CarolinaCasualtyInsuranceGroup.CompanyId)
            {
                CCICLineOfBusiness lineOfBusiness = new CCICLineOfBusiness();
                lobType = lineOfBusiness.GetType(policySymbol);
                lobCode = lineOfBusiness.GetBlcName(lobType);
            }
            else if (_company.CompanyId == BlcCompany.BerkleyLifeSciences.CompanyId)
            {
                BLSLineOfBusiness lineOfBusiness = new BLSLineOfBusiness();
                lobType = lineOfBusiness.GetType(policySymbol);
                lobCode = lineOfBusiness.GetBlcName(lobType);
            }
            else if (_company.CompanyId == BlcCompany.BerkleyAgribusinessRiskSpecialists.CompanyId || _company.CompanyId.ToString() == "a56ebe75-f5a8-47a2-b8cf-55979dbbaafb")
            {
                BARSLineOfBusiness lineOfBusiness = new BARSLineOfBusiness();
                lobType = lineOfBusiness.GetType(policySymbol);
                lobCode = lineOfBusiness.GetBlcName(lobType);
            }
            else if (_company.CompanyId == BlcCompany.BerkleyNorthPacificGroup.CompanyId)
            {
                BNPGLineOfBusiness lineOfBusiness = new BNPGLineOfBusiness();
                lobType = lineOfBusiness.GetType(policySymbol);
                lobCode = lineOfBusiness.GetBlcName(lobType);
            }
            else if (_company.CompanyId == BlcCompany.BerkleyOilGas.CompanyId)
            {
                BOGLineOfBusiness lineOfBusiness = new BOGLineOfBusiness();
                lobType = lineOfBusiness.GetType(policySymbol);
                lobCode = lineOfBusiness.GetBlcName(lobType);
            }
            else if (_company.CompanyId == BlcCompany.BerkleyTechnologyUnderwriters.CompanyId)
            {
                BTULineOfBusiness lineOfBusiness = new BTULineOfBusiness();
                lobType = lineOfBusiness.GetType(policySymbol);
                lobCode = lineOfBusiness.GetBlcName(lobType);
            }
            else if (_company.CompanyId == BlcCompany.RiverportInsuranceCompany.CompanyId)
            {
                RICLineOfBusiness lineOfBusiness = new RICLineOfBusiness();
                lobType = lineOfBusiness.GetType(policySymbol);
                lobCode = lineOfBusiness.GetBlcName(lobType);
            }
            else
            {
                throw new NotSupportedException(string.Format("Was not expecting company '{0}' to be passed.", _company.CompanyAbbreviation));
            }

            return lobCode;
        }

        /// <summary>
        /// Returns the submission status from the Clearance system.
        /// </summary>
        /// <param name="submissionNumber">The submission number to query the Clearance system.</param>
        public string ImportSubmissionStatus(string submissionNumber)
        {
            ProspectWebService service = new ProspectWebService();
            return service.GetSubmissionStatus(submissionNumber, _company);
        }

        /// <summary>
        /// Returns the submission status from the Clearance system.
        /// </summary>
        /// <param name="submissionNumber">The submission number to query the Clearance system.</param>
        public bool ValidSubmission(string submissionNumber)
        {
            ProspectWebService service = new ProspectWebService();
            return service.DoesSubmissionExistBySubmissionNumber(submissionNumber, _company);
        }
    }
}
