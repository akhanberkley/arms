﻿//using Berkley.BLC.Business;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ARMS.Services;
using System.Collections.Generic;
using System.Linq;

namespace Berkley.BLC.Import
{
    public class RICPolicyStarImportBuilder : TransactionalPolicyStarBaseImportBuilder, IImportBuilder
    {
        private CompanyPolicyEntity moPolicyFieldEntity = new CompanyPolicyEntity();
        private CompanyInsuredEntity moInsuredFieldEntity = new CompanyInsuredEntity();
        private CompanyAgencyEntity moAgencyFieldEntity = new CompanyAgencyEntity();
        private CompanyLocationEntity moLocationFieldEntity = new CompanyLocationEntity();
        private CompanyBuildingEntity moBuildingFieldEntity = new CompanyBuildingEntity();
        private CompanyClaimEntity moCompanyClaimEntity = new CompanyClaimEntity();
        private CompanyRateModFactorEntity moCompanyRateModFactorEntity = new CompanyRateModFactorEntity();
        private RICLineOfBusiness moLineOfBusiness = new RICLineOfBusiness();

        #region Line Of Business / Coverage
        private CompanyCoverageEntity moCoverageFieldEntity = new CompanyCoverageEntity();
        private RICCoverageDictionary moCoverageDictionary = new RICCoverageDictionary();
        #endregion

        public RICPolicyStarImportBuilder(string strPolicyNumber)
        {
            base.Initialize(this, strPolicyNumber, BlcCompany.RiverportInsuranceCompany);
            // Rearrange fields in moPolicyFieldEntity as necessary
        }

        #region IImportBuilder Members

        public BlcStoredProcedure GetStoredProcedure()
        {
            return null;
        }

        public BlcStoredProcedure GetStoredProcedure(CompanySpEntity.SpType eType)
        {
            return null;
        }

        public BlcInsured BuildComplete(BlcInsured oBlcInsured)
        {
            // The BlcDataSet has been filled out.  Do the Insert into the BLC DB
            // If all of the company (derived) classes do the same thing, put the basic insert/update
            // in BaseImportBuilder
            Policy oPolicy;
            RICLineOfBusiness oLineOfBusiness = new RICLineOfBusiness();
            LineOfBusiness.LineOfBusinessType eType;
            List<Policy> oPolicyList = oBlcInsured.PolicyList;

            oPolicyList = oPolicyList.OrderByDescending(pol => pol.PolicyMod).ToList();

            //**************************************************************
            //Only take the most recent policy mod - Hack for P* issue
            List<Policy> filteredPolicies = new List<Policy>();

            foreach (Policy p in oPolicyList)
            {
                if (filteredPolicies == null || filteredPolicies.Where(c => c.PolicyNumber == p.PolicyNumber).Count() == 0)
                {
                    filteredPolicies.Add(p);
                }
                else
                {
                    oBlcInsured.PolicyList.Remove(p);
                }
            }

            if (filteredPolicies.Count > 0)
                oPolicyList = filteredPolicies;
            //***************************************************************

            List<Location> locations = oBlcInsured.PolicyLocationList;
            List<Location> filteredLocation = new List<Location>();

            locations = locations.OrderBy(loc => loc.PolicySymbol).ToList();
            foreach (Location l in locations)
            {
                if ((filteredLocation == null || filteredLocation.Where(c => c.StreetLine1 == l.StreetLine1).Count() == 0) && oPolicyList.Where(p => p.PolicySymbol == l.PolicySymbol).Count() > 0)
                {
                    filteredLocation.Add(l);
                }
                else
                {
                    oBlcInsured.PolicyLocationList.Remove(l);
                }
            }

            //Buildings
            List<BlcBuilding> buildings = oBlcInsured.BuildingList;
            List<BlcBuilding> filteredBuilding = new List<BlcBuilding>();

            buildings = buildings.OrderBy(b => b.LocationId).ToList();
            foreach (BlcBuilding b in buildings)
            {
                if ((filteredBuilding == null || filteredBuilding.Where(c => c.Contents == b.Contents && c.BuildingNumber == b.BuildingNumber && c.LocationAsInt == b.LocationAsInt).Count() == 0))
                {
                    filteredBuilding.Add(b);
                }
                else
                {
                    oBlcInsured.BuildingList.Remove(b);
                }
            }

            //Coverages
            List<BlcCoverage> coverages = oBlcInsured.CoverageList;
            List<BlcCoverage> filteredCoverages = new List<BlcCoverage>();

            coverages = coverages.OrderBy(b => b.PolicyMod).ToList();
            foreach (BlcCoverage cov in coverages)
            {
                if (filteredPolicies.Where(c => c.PolicyNumber == cov.PolicyNumber && c.PolicyMod.ToString() == cov.PolicyMod).Count() == 0)
                {
                    oBlcInsured.CoverageList.Remove(cov);
                }
            }

            for (int i = 0; i < oPolicyList.Count; i++)
            {
                oPolicy = oPolicyList[i];
                eType = oLineOfBusiness.GetType(oPolicy.PolicySymbol);
                oPolicy.LineOfBusinessCode = oLineOfBusiness.GetBlcName(eType);
                oPolicyList[i] = oPolicy;
            }

            //convert the underwriter
            if (!UnderwriterExist(oBlcInsured.InsuredEntity.Underwriter))
            {
                //attempt to get the assigned underwriter from BCS
                Prospect.ProspectWebService service = new Prospect.ProspectWebService();
                Underwriter underwriter = service.GetAssignedUnderwriter(base.PolicyNumber, base.BerkleyCompany);
                oBlcInsured.InsuredEntity.Underwriter = (underwriter != null) ? underwriter.UnderwriterCode : string.Empty;
            }

            return oBlcInsured;
        }

        private bool UnderwriterExist(string initials)
        {
            bool result = false;

            if (initials.Length > 0)
            {
                //Underwriter underwriter = Underwriter.GetOne("Code = ? && CompanyID = ?", initials, Company.RiverportInsuranceCompany.ID);
                Underwriter underwriter = UnderwriterService.GetUnderwriterByUsername(initials, BlcCompany.RiverportInsuranceCompany.CompanyId);

                result = (underwriter != null) ? true : false;
            }

            return result;
        }

        public Agency BuildForAgency(string agencyNumber)
        {
            Berkley.BLC.Import.APSWebService service = new Berkley.BLC.Import.APSWebService(BlcCompany.RiverportInsuranceCompany.ApsEndPointUrl);
            APSProxy.agency apsAgency = service.GetAgency(agencyNumber);

            return base.BuildAgency(apsAgency);
        }

        public CompanyCoverageEntity CoverageFieldEntity
        {
            get { return moCoverageFieldEntity; }
        }

        public List<CompanyCoverageColumn> GetCoverage(LineOfBusiness.LineOfBusinessType eType)
        {
            return moCoverageDictionary.GetColumnList(eType);
        }

        public int GetMaxCoverageColumns()
        {
            return moCoverageDictionary.GetColumnCount();
        }

        public int GetMaxColumnNumber()
        {
            return moCoverageDictionary.GetMaxColumnNumber();
        }

        // The derived classes can change the field mapping
        public CompanyPolicyEntity PolicyFieldEntity
        {
            get { return moPolicyFieldEntity; }
        }

        // The derived classes can change the field mapping
        public CompanyInsuredEntity InsuredFieldEntity
        {
            get { return moInsuredFieldEntity; }
        }

        public CompanyAgencyEntity AgencyFieldEntity
        {
            get { return moAgencyFieldEntity; }
        }

        public CompanyLocationEntity LocationFieldEntity
        {
            get { return moLocationFieldEntity; }
        }

        public CompanyBuildingEntity BuildingFieldEntity
        {
            get { return moBuildingFieldEntity; }
        }

        public LineOfBusiness LineOfBusinessFieldEntity
        {
            get { return moLineOfBusiness; }
        }

        public CompanyClaimEntity ClaimFieldEntity
        {
            get { return moCompanyClaimEntity; }
        }

        public CompanyRateModFactorEntity RateModFactorFieldEntity
        {
            get { return moCompanyRateModFactorEntity; }
        }

        public string CompanyAbbreviation
        {
            get { return "RIC"; }
        }


        #endregion
    }
}