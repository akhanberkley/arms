//using Berkley.BLC.Business;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ARMS.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Berkley.BLC.Import
{
    public class BSUMPolicyStarImportBuilder : PolicyStarBaseImportBuilder, IImportBuilder
    {
        private CompanyPolicyEntity moPolicyFieldEntity = new CompanyPolicyEntity();
        private CompanyInsuredEntity moInsuredFieldEntity = new CompanyInsuredEntity();
        private CompanyAgencyEntity moAgencyFieldEntity = new CompanyAgencyEntity();
        private CompanyLocationEntity moLocationFieldEntity = new CompanyLocationEntity();
        private CompanyBuildingEntity moBuildingFieldEntity = new CompanyBuildingEntity();
        private CompanyClaimEntity moCompanyClaimEntity = new CompanyClaimEntity();
        private CompanyRateModFactorEntity moCompanyRateModFactorEntity = new CompanyRateModFactorEntity();
        private BSUMLineOfBusiness moLineOfBusiness = new BSUMLineOfBusiness();

        private List<string> PolicySymbolExclusions = new List<string> { "CUA" };

        #region Line Of Business / Coverage
        private CompanyCoverageEntity moCoverageFieldEntity = new CompanyCoverageEntity();
        private BSUMCoverageDictionary moCoverageDictionary = new BSUMCoverageDictionary();
        #endregion

        public BSUMPolicyStarImportBuilder(string strPolicyNumber)
        {
            base.Initialize(this, strPolicyNumber, BlcCompany.BerkleySpecialtyUnderwritingManagers);
            // Rearrange fields in moPolicyFieldEntity as necessary
        }

        #region IImportBuilder Members

        public BlcStoredProcedure GetStoredProcedure()
        {
            return null;
        }

        public BlcStoredProcedure GetStoredProcedure(CompanySpEntity.SpType eType)
        {
            return null;
        }

        public BlcInsured BuildComplete(BlcInsured oBlcInsured)
        {
            // The BlcDataSet has been filled out.  Do the Insert into the BLC DB
            // If all of the company (derived) classes do the same thing, put the basic insert/update
            // in BaseImportBuilder
            Policy oPolicy;
            BSUMLineOfBusiness oLineOfBusiness = new BSUMLineOfBusiness();
            LineOfBusiness.LineOfBusinessType eType;

            // Exclude specified Policy Symbols from results
            // Need to record the policy numbers excluded for next removing them from the CoverageList
            List<Policy> excludedPolicies = new List<Policy>();

            for (int i = 0; i < PolicySymbolExclusions.Count; i++)
            {
                foreach (Policy p in oBlcInsured.PolicyList.ToList())
                {
                    if (p.PolicySymbol == PolicySymbolExclusions[i])
                    {
                        excludedPolicies.Add(p);
                        oBlcInsured.PolicyList.Remove(p);
                    }
                }
            }

            List<Policy> oPolicyList = oBlcInsured.PolicyList;

            // Filter Policies by Mod
            // Only take the most recent policy mod - Hack for P* issue
            List<Policy> filteredPolicies = new List<Policy>();
            oPolicyList = oPolicyList.OrderByDescending(pol => pol.PolicyMod).ToList();

            foreach (Policy p in oPolicyList)
            {
                if (filteredPolicies == null || filteredPolicies.Where(c => c.PolicyNumber == p.PolicyNumber).Count() == 0)
                {
                    filteredPolicies.Add(p);
                }
                else
                {
                    oBlcInsured.PolicyList.Remove(p);
                }
            }

            if (filteredPolicies.Count > 0)
                oPolicyList = filteredPolicies;

            // Filter Locations
            List<Location> locations = oBlcInsured.PolicyLocationList;
            List<Location> filteredLocation = new List<Location>();

            locations = locations.OrderBy(loc => loc.PolicySymbol).ToList();
            foreach (Location l in locations)
            {
                if ((filteredLocation == null || filteredLocation.Where(c => c.StreetLine1 == l.StreetLine1).Count() == 0) && oPolicyList.Where(p => p.PolicySymbol == l.PolicySymbol).Count() > 0)
                {
                    filteredLocation.Add(l);
                }
                else
                {
                    oBlcInsured.PolicyLocationList.Remove(l);
                }
            }

            // Filter Buildings
            List<BlcBuilding> buildings = oBlcInsured.BuildingList;
            List<BlcBuilding> filteredBuilding = new List<BlcBuilding>();

            buildings = buildings.OrderBy(b => b.LocationId).ToList();
            foreach (BlcBuilding b in buildings)
            {
                if ((filteredBuilding == null || filteredBuilding.Where(c => c.Contents == b.Contents && c.BuildingNumber == b.BuildingNumber && c.LocationAsInt == b.LocationAsInt).Count() == 0))
                {
                    filteredBuilding.Add(b);
                }
                else
                {
                    oBlcInsured.BuildingList.Remove(b);
                }
            }

            // Filter Coverages
            List<BlcCoverage> coverages = oBlcInsured.CoverageList;
            List<BlcCoverage> filteredCoverages = new List<BlcCoverage>();

            coverages = coverages.OrderBy(b => b.PolicyMod).ToList();
            foreach (BlcCoverage cov in coverages)
            {
                if (filteredPolicies.Where(c => c.PolicyNumber == cov.PolicyNumber && c.PolicyMod.ToString() == cov.PolicyMod).Count() == 0)
                {
                    oBlcInsured.CoverageList.Remove(cov);
                }
            }

            // For each of the policies to be excluded
            // check the CoverageList and remove them by policy number and mod if present
            if (excludedPolicies.Count > 0)
            {
                foreach (Policy p in excludedPolicies)
                {
                    var policyNumber = (p.PolicyNumber.Length > 7) ? p.PolicyNumber.Substring(p.PolicyNumber.Length - 7) : p.PolicyNumber;

                    // Remove Policy Exclusions from Locations
                    foreach (Location loc in locations.ToList())
                    {
                        var policySystemKey = loc.PolicySystemKey.Substring(loc.PolicySystemKey.Length - 7);
                        if (policySystemKey == policyNumber)
                        {
                            oBlcInsured.PolicyLocationList.Remove(loc);
                        }
                    }

                    // Remove Policy Exclusions from Coverages
                    foreach (BlcCoverage cov in coverages.ToList())
                    {
                        if (cov.PolicyNumber == policyNumber && cov.PolicyMod == p.PolicyMod.ToString())
                        {
                            oBlcInsured.CoverageList.Remove(cov);
                        }
                    }
                }
            }

            for (int i = 0; i < oPolicyList.Count; i++)
            {
                oPolicy = oPolicyList[i];
                eType = oLineOfBusiness.GetType(oPolicy.PolicySymbol);
                oPolicy.LineOfBusinessCode = oLineOfBusiness.GetBlcName(eType);
                oPolicyList[i] = oPolicy;
            }

            //convert the underwriter
            if (!UnderwriterExist(oBlcInsured.InsuredEntity.Underwriter))
            {
                //attempt to get the assigned underwriter from BCS
                Prospect.ProspectWebService service = new Prospect.ProspectWebService();
                Underwriter underwriter = service.GetAssignedUnderwriter(base.PolicyNumber, base.BerkleyCompany);
                oBlcInsured.InsuredEntity.Underwriter = (underwriter != null) ? underwriter.UnderwriterCode : string.Empty;
            }

            return oBlcInsured;
        }

        private bool UnderwriterExist(string initials)
        {
            bool result = false;

            if (initials.Length > 0)
            {
                //Underwriter underwriter = Underwriter.GetOne("Code = ? && CompanyID = ?", initials, BlcCompany.BerkleySpecialtyUnderwritingManagers.CompanyId);
                Underwriter underwriter = UnderwriterService.GetUnderwriterByCode(initials, BlcCompany.BerkleySpecialtyUnderwritingManagers.CompanyId);
                result = (underwriter != null) ? true : false;
            }

            return result;
        }

        public Agency BuildForAgency(string agencyNumber)
        {
            Berkley.BLC.Import.APSWebService service = new Berkley.BLC.Import.APSWebService(BlcCompany.BerkleySpecialtyUnderwritingManagers.ApsEndPointUrl);
            APSProxy.agency apsAgency = service.GetAgency(agencyNumber);

            return base.BuildAgency(apsAgency);
        }

        public CompanyCoverageEntity CoverageFieldEntity
        {
            get { return moCoverageFieldEntity; }
        }

        public List<CompanyCoverageColumn> GetCoverage(LineOfBusiness.LineOfBusinessType eType)
        {
            return moCoverageDictionary.GetColumnList(eType);
        }

        public int GetMaxCoverageColumns()
        {
            return moCoverageDictionary.GetColumnCount();
        }

        public int GetMaxColumnNumber()
        {
            return moCoverageDictionary.GetMaxColumnNumber();
        }

        // The derived classes can change the field mapping
        public CompanyPolicyEntity PolicyFieldEntity
        {
            get { return moPolicyFieldEntity; }
        }

        // The derived classes can change the field mapping
        public CompanyInsuredEntity InsuredFieldEntity
        {
            get { return moInsuredFieldEntity; }
        }

        public CompanyAgencyEntity AgencyFieldEntity
        {
            get { return moAgencyFieldEntity; }
        }

        public CompanyLocationEntity LocationFieldEntity
        {
            get { return moLocationFieldEntity; }
        }

        public CompanyBuildingEntity BuildingFieldEntity
        {
            get { return moBuildingFieldEntity; }
        }

        public LineOfBusiness LineOfBusinessFieldEntity
        {
            get { return moLineOfBusiness; }
        }

        public CompanyClaimEntity ClaimFieldEntity
        {
            get { return moCompanyClaimEntity; }
        }

        public CompanyRateModFactorEntity RateModFactorFieldEntity
        {
            get { return moCompanyRateModFactorEntity; }
        }

        public string CompanyAbbreviation
        {
            get { return "BSD"; }
        }

        /// <summary>
        /// Parses policy number and symbol from a PolicyGeneric Info.
        /// </summary>
        /// <param name="policy">The P* PolicyAuditDO</param>
        /// <param name="policyNumber">The PATS policy number.</param>
        /// <param name="policySymbol">The PATS policy symbol.</param>
        public void ParsePolicySymbol(string pNumber, out string policyNumber, out string policySymbol)
        {
            policyNumber = string.Empty;
            policySymbol = string.Empty;

            if (IsMeaningful(pNumber))
                policyNumber = pNumber;

            if (IsMeaningful(GetSymbolFromFullPolicyNumber(pNumber.Trim())))
                policySymbol = GetSymbolFromFullPolicyNumber(pNumber.Trim());

            //Always store policy number without symbol
            if (policySymbol != string.Empty)
                policyNumber = policyNumber.Replace(policySymbol, "");

        }

        /// <summary>
        /// Parses policy symbol from full alphanummeric policy number.
        /// </summary>
        /// <param name="policySymbol">Full policy number.</param>
        /// <returns>A string policy symbol.</returns>
        private string GetSymbolFromFullPolicyNumber(string fullPolicyNumber)
        {
            string result = string.Empty;
            foreach (Char c in fullPolicyNumber)
            {
                try
                {
                    string s = new string(c, 1);
                    int i = int.Parse(s);
                }
                catch (FormatException)
                {
                    result = String.Concat(result, new string(c, 1));
                }
                catch (Exception)
                {
                    //swallow
                }
            }
            return result;
        }

        public override string ParsePolicySymbol(string pNumber)
        {
            string policySymbol = string.Empty;
            string policyNumber = string.Empty;
            ParsePolicySymbol(pNumber, out policyNumber, out policySymbol);
            return policySymbol;
        }

        #endregion
    }
}
