using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;
//using Berkley.BLC.Business;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ARMS.Services;

namespace Berkley.BLC.Import
{
    /// <summary>
    /// This class 1. Contains common functions for use in DB Access
    ///            2. Defines the Entities/classes that make up a Survey Insert into the ARMS Database
    /// </summary>
    public class StagingBaseImportBuilder
    {
        private IImportBuilder moImportBuilder;
        private BlcInsured moBlcInsured = new BlcInsured();
        private string _connectionInfo;
        private List<string> policyList = new List<string>();
        private Company _berkleyCompany;

        public StagingBaseImportBuilder()
        {
        }

        public void Initialize(IImportBuilder oImportBuilder, string connectionInfo, Company company)
        {
            moImportBuilder = oImportBuilder;
            _connectionInfo = connectionInfo;
            _berkleyCompany = company;
        }

        #region Properties

        protected string ConnectionInfo
        {
            get { return _connectionInfo; }
        }

        protected Company BerkleyCompany
        {
            get { return _berkleyCompany; }
        }

        #endregion

        public Agency BuildForAgency(string agencyNumber)
        {
            DataSet ds = BlcStoredProcedure.SQLDbImportSprocs("Agency_Get", ConnectionInfo, agencyNumber);
            if (ds.Tables[0].Rows.Count <= 0)
            {
                throw new BlcImportException("No agency found for #" + agencyNumber);
            }

            moBlcInsured.AgencyEntity = BuildAgency(ds.Tables[0].Rows[0]);
            return moBlcInsured.AgencyEntity;
        }

        public DateTime BuildForAccountStatus(string clientId)
        {
            DataSet ds = BlcStoredProcedure.SQLDbImportSprocs("PolicyNonRenewedStatus_Get", ConnectionInfo, clientId);

            //Set the account status based on the statuses of all the insured's policies
            //Only if all policies are non-renewed, is the client set as non-renewed
            int activePolicyCount = Int32.Parse(ds.Tables[0].Rows[0][0].ToString());

            return (activePolicyCount == 0) ? DateTime.Now : DateTime.MinValue;
        }

        /// <summary>
        /// Call all of stored procedures and return the aggregate data.
        /// </summary>
        /// <returns>A BlcInsured filled out with data from all of the stored procedures</returns>
        public BlcInsured BuildRequestFromPolicy(string policyNumber)
        {
            DataSet ds = BlcStoredProcedure.SQLDbImportSprocs("CompleteSurveyDetails_Get", ConnectionInfo, policyNumber);

            //first, build the insured
            DataTable dtInsured = ds.Tables[(int)StagingSpEntity.SpType.Insured];
            if (dtInsured.Rows.Count > 0)
            {
                BuildInsured(dtInsured.Rows[0], ref moBlcInsured);
            }
            else
            {
                throw new BlcNoInsuredException("No insured found for policy #" + policyNumber);
            }

            //Next, build the agency
            DataTable dtAgency = ds.Tables[(int)StagingSpEntity.SpType.Agency];
            if (dtAgency.Rows.Count > 0)
            {
                moBlcInsured.AgencyEntity = BuildAgency(dtAgency.Rows[0]);
            }
            else
            {
                throw new BlcImportException("No agency found for policy #" + policyNumber);
            }

            //Next, build the policies
            DataTable dtPolicy = ds.Tables[(int)StagingSpEntity.SpType.Policy];
            foreach (DataRow dr in dtPolicy.Rows)
            {
                moBlcInsured.PolicyList.Add(BuildPolicy(dr));
            }

            //Next, build the buildings
            DataTable dtBuilding = ds.Tables[(int)StagingSpEntity.SpType.Building];
            foreach (DataRow dr in dtBuilding.Rows)
            {
                moBlcInsured.BuildingList.Add(BuildBuilding(dr));
            }

            //Next, build the locations
            DataTable dtLocation = ds.Tables[(int)StagingSpEntity.SpType.Location];
            foreach (DataRow dr in dtLocation.Rows)
            {
                moBlcInsured.PolicyLocationList.Add(BuildLocation(dr));
            }

            // if not locations are found then default the first location to the insured address with a default contact
            if (moBlcInsured.PolicyLocationList.Count <= 0)
            {
                Location location = BuildLocation(moBlcInsured.InsuredEntity);
                moBlcInsured.PolicyLocationList.Add(location);

                moBlcInsured.LocationContactList.Add(BuildLocationContact(location));
            }

            //Next, build the location contacts
            DataTable dtLocationContact = ds.Tables[(int)StagingSpEntity.SpType.LocationContact];
            foreach (DataRow dr in dtLocationContact.Rows)
            {
                moBlcInsured.LocationContactList.Add(BuildLocationContact(dr));
            }

            //Next, build the coverages by policy
            foreach (Policy policy in moBlcInsured.PolicyList)
            {
                DataSet dsCoverages = BlcStoredProcedure.SQLDbImportSprocs("PolicyCoverage_Get", ConnectionInfo, policy.PolicySystemKey);
                moBlcInsured.CoverageList.AddRange(BuildCoverages(dsCoverages, policy));
            }

            //Next, build the claims
            DataTable dtClaim = ds.Tables[(int)StagingSpEntity.SpType.Claim];
            foreach (DataRow dr in dtClaim.Rows)
            {
                moBlcInsured.ClaimList.Add(BuildClaim(dr));
            }

            //Next, build the rate mods
            DataTable dtRateModFactor = ds.Tables[(int)StagingSpEntity.SpType.RateModFactor];
            foreach (DataRow dr in dtRateModFactor.Rows)
            {
                moBlcInsured.RateModFactorList.Add(BuildRateModFactor(dr));
            }

            return moImportBuilder.BuildComplete(moBlcInsured);
        }

        public BlcInsured BuildEmptyCoverages(string strBlcLineOfBusiness)
        {
            LineOfBusiness oLineOfBusiness = moImportBuilder.LineOfBusinessFieldEntity;

            Policy policy = new Policy();
            policy.PolicySymbol = oLineOfBusiness.GetCompanyLob(strBlcLineOfBusiness);

            moBlcInsured.CoverageList.AddRange(BuildCoverages(null, policy));

            return moBlcInsured;
        }

        private BlcCoverage[] BuildCoverages(DataSet ds, Policy policy)
        {
            List<BlcCoverage> moReturnList = new List<BlcCoverage>();

            try
            {
                // Field Mapping for the Coverage/Line of Business
                CompanyCoverageEntity coverageFields = moImportBuilder.CoverageFieldEntity;

                // Defines each LOB
                LineOfBusiness lineOfBusiness = moImportBuilder.LineOfBusinessFieldEntity;
                
                // Convert that to a Typed name
                LineOfBusiness.LineOfBusinessType eType = lineOfBusiness.GetType(policy.PolicySymbol);

                // Switch Policy Symbol and get the field mapping for this LOB
                List<CompanyCoverageColumn> companyColumnList = moImportBuilder.GetCoverage(eType);

                // Iterage Rows in Coverage
                bool matchFound = false;
                List<String> coveragesNotFound = new List<string>();
                foreach (CompanyCoverageColumn companyCoverageColumn in companyColumnList)
                {
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (companyCoverageColumn.StagingCoverageValueType == ReadString(dr, (int)StagingPolicyCoverageEntity.FieldColumns.CoverageCode))
                            {
                                moReturnList.Add(BuildCoverage(companyCoverageColumn, dr, policy, lineOfBusiness));
                                matchFound = true;
                                break;
                            }
                        }
                    }

                    if (!matchFound)
                    {
                        moReturnList.Add(BuildCoverage(companyCoverageColumn, null, policy, lineOfBusiness));
                    }
                }
            }
            catch (Exception exception)
            {
                Exception returnEx = new Exception(string.Format(exception.Message + ". Policy {0}", policy.PolicyNumber), exception.InnerException);
                throw returnEx;
            }
            return moReturnList.ToArray();
        }

        private BlcCoverage BuildCoverage(CompanyCoverageColumn companyCoverageColumn, DataRow dr, Policy policy, LineOfBusiness lineOfBusiness)
        {
            BlcCoverage blcCoverage = new BlcCoverage();
            
            // Set the Coverage members
            blcCoverage.PolicyNumber = policy.PolicyNumber;
            blcCoverage.PolicyMod = (policy.PolicyMod != Int32.MinValue) ? policy.PolicyMod.ToString() : string.Empty;
            blcCoverage.LineOfBusinessAsString = lineOfBusiness.GetBlcName(companyCoverageColumn.LineOfBusiness);
            blcCoverage.CompanyAbbreviation = moImportBuilder.CompanyAbbreviation;

            //class codes have multiple fields for a value
            if (dr != null && !string.IsNullOrEmpty(ReadString(dr, (int)StagingPolicyCoverageEntity.FieldColumns.ClassCodeDescription))
                && ReadString(dr, (int)StagingPolicyCoverageEntity.FieldColumns.CoverageCode) == "CLASS")
            {
                blcCoverage.Value = string.Format("{0} - {1}|{2}|{3}",
                    ReadString(dr, (int)StagingPolicyCoverageEntity.FieldColumns.CoverageValue),
                    ReadString(dr, (int)StagingPolicyCoverageEntity.FieldColumns.ClassCodeDescription),
                    ReadString(dr, (int)StagingPolicyCoverageEntity.FieldColumns.ClassCodePayroll),
                    ReadString(dr, (int)StagingPolicyCoverageEntity.FieldColumns.ClassCodeEmployeeCount));
            }
            else
            {
                blcCoverage.Value = (dr != null) ? ReadString(dr, (int)StagingPolicyCoverageEntity.FieldColumns.CoverageValue) : string.Empty;
            }

            // The name of the coverage i.e. General Liability
            blcCoverage.CoverageNameType = companyCoverageColumn.ColumnGroupTypeAsString;

            // A name given to the value itself i.e. Limits
            blcCoverage.CoverageValueType = companyCoverageColumn.ColumnValueTypeAsString;

            if (companyCoverageColumn.SubLineOfBusiness != LineOfBusiness.LineOfBusinessType.Unknown)
            {
                blcCoverage.SubLineOfBusinessAsString = lineOfBusiness.GetBlcName(companyCoverageColumn.SubLineOfBusiness);
            }

            return blcCoverage;
        }

        private BlcBuilding BuildBuilding(DataRow dr)
        {
            BlcBuilding building = new BlcBuilding(Guid.NewGuid());

            building.PolicySystemKey = ReadString(dr, (int)StagingBuildingEntity.BuildingColumn.LocationId);
            building.LocationAsInt = ReadInteger(dr, (int)StagingBuildingEntity.BuildingColumn.LocationNumber);
            building.BuildingNumber = (short)ReadInteger(dr, (int)StagingBuildingEntity.BuildingColumn.BuildingNumber);
            building.YearBuilt = ReadInteger(dr, (int)StagingBuildingEntity.BuildingColumn.YearBuilt);
            building.HasSprinklerSystem = (short)ReadInteger(dr, (int)StagingBuildingEntity.BuildingColumn.HasSprinklerSystem);
            building.HasSprinklerSystemCredit = (short)ReadInteger(dr, (int)StagingBuildingEntity.BuildingColumn.HasSprinklerSystemCredit);
            building.PublicProtection = ReadInteger(dr, (int)StagingBuildingEntity.BuildingColumn.PublicProtection);
            building.StockValues = ReadDecimal(dr, (int)StagingBuildingEntity.BuildingColumn.StockValues);
            building.Contents = ReadDecimal(dr, (int)StagingBuildingEntity.BuildingColumn.BuildingContents);
            building.BuildingValue = ReadDecimal(dr, (int)StagingBuildingEntity.BuildingColumn.BuildingValue);
            building.BusinessInterruption = ReadDecimal(dr, (int)StagingBuildingEntity.BuildingColumn.BusinessInterruption);
            building.LocationOccupancy = ReadString(dr, (int)StagingBuildingEntity.BuildingColumn.LocationOccupancy);
            
            try {building.BuildingValuationTypeCode = ReadValuationType(dr, (int)StagingBuildingEntity.BuildingColumn.BuildingValuationType);} catch (IndexOutOfRangeException) { }
            try {building.CoinsurancePercentage = ReadPercentage(dr, (int)StagingBuildingEntity.BuildingColumn.CoinsurancePercentage);} catch (IndexOutOfRangeException) { }

            return building;
        }

        private BlcClaim BuildClaim(DataRow dr)
        {
            BlcClaim oBlcClaim = new BlcClaim();
            Claim claim = new Claim();

            claim.ClaimNumber = ReadString(dr, (int)StagingClaimEntity.ClaimColumn.ClaimNumber);
            claim.LossDate = ReadDateTime(dr, (int)StagingClaimEntity.ClaimColumn.LossDate);
            claim.Claimant = ReadString(dr, (int)StagingClaimEntity.ClaimColumn.Claimant);
            claim.ClaimAmount = ReadDecimal(dr, (int)StagingClaimEntity.ClaimColumn.ClaimAmount);
            claim.ClaimComment = ReadString(dr, (int)StagingClaimEntity.ClaimColumn.ClaimComment);
            claim.ClaimStatus = ReadString(dr, (int)StagingClaimEntity.ClaimColumn.ClaimStatus);
            

            oBlcClaim.Entity = claim;
            oBlcClaim.PolicyMod = ReadInteger(dr, (int)StagingClaimEntity.ClaimColumn.PolicyVersion);
            oBlcClaim.PolicyNumber = ReadString(dr, (int)StagingClaimEntity.ClaimColumn.PolicyNumber);
            return oBlcClaim;
        }

        private BlcRateModFactor BuildRateModFactor(DataRow dr)
        {
            BlcRateModFactor oBlcRateModFactor = new BlcRateModFactor();
            RateModificationFactor rateModFactor = new RateModificationFactor();

            rateModFactor.StateCode = ReadState(dr, (int)StagingRateModFactorEntity.RateModFactorColumn.StateCode);
            rateModFactor.Rate = ReadDecimal(dr, (int)StagingRateModFactorEntity.RateModFactorColumn.Rate);
            rateModFactor.RateModificationFactorTypeCode = ReadRateModType(dr, (int)StagingRateModFactorEntity.RateModFactorColumn.FactorTypeCode);

            oBlcRateModFactor.Entity = rateModFactor;
            oBlcRateModFactor.PolicyNumber = ReadString(dr, (int)StagingRateModFactorEntity.RateModFactorColumn.PolicyNumber);
            return oBlcRateModFactor;
        }

        private Location BuildLocation(DataRow dr)
        {
            Location location = new Location();

            location.PolicySystemKey = ReadString(dr, (int)StagingLocationEntity.LocationColumn.LocationId);
            location.LocationNumber = (ReadInteger(dr, (int)StagingLocationEntity.LocationColumn.LocationNumber) != Int32.MinValue) ? ReadInteger(dr, (int)StagingLocationEntity.LocationColumn.LocationNumber) : 0;
            location.StreetLine1 = ReadString(dr, (int)StagingLocationEntity.LocationColumn.StreetLine1);
            location.StreetLine2 = ReadString(dr, (int)StagingLocationEntity.LocationColumn.StreetLine2);
            location.City = ReadString(dr, (int)StagingLocationEntity.LocationColumn.City);
            location.StateCode = ReadState(dr, (int)StagingLocationEntity.LocationColumn.StateCode);
            location.ZipCode = ReadString(dr, (int)StagingLocationEntity.LocationColumn.ZipCode);
            location.Description = ReadString(dr, (int)StagingLocationEntity.LocationColumn.LocationDescription);
            location.IsActive = true;

            return location;
        }

        private Location BuildLocation(Insured insured)
        {
            Location location = new Location();

            location.PolicySystemKey = Guid.NewGuid().ToString();
            location.LocationNumber = 1;
            location.StreetLine1 = insured.StreetLine1;
            location.StreetLine2 = insured.StreetLine2;
            location.City = insured.City;
            location.StateCode = insured.StateCode;
            location.ZipCode = insured.ZipCode;
            location.Description = string.Empty;
            location.IsActive = true;

            return location;
        }

        private LocationContact BuildLocationContact(DataRow dr)
        {
            LocationContact contact = new LocationContact();

            contact.PolicySystemKey = ReadString(dr, (int)StagingLocationContactEntity.LocationColumn.LocationId);
            contact.Name = ReadString(dr, (int)StagingLocationContactEntity.LocationColumn.Name);
            contact.Title = ReadString(dr, (int)StagingLocationContactEntity.LocationColumn.Title);
            contact.PhoneNumber = ReadString(dr, (int)StagingLocationContactEntity.LocationColumn.PhoneNumber);
            contact.AlternatePhoneNumber = ReadString(dr, (int)StagingLocationContactEntity.LocationColumn.AlternatePhoneNumber);
            contact.FaxNumber = ReadString(dr, (int)StagingLocationContactEntity.LocationColumn.FaxNumber);
            contact.EmailAddress = ReadString(dr, (int)StagingLocationContactEntity.LocationColumn.EmailAddress);
            contact.CompanyName = ReadString(dr, (int)StagingLocationContactEntity.LocationColumn.CompanyName);
            contact.StreetLine1 = ReadString(dr, (int)StagingLocationContactEntity.LocationColumn.StreetLine1);
            contact.StreetLine2 = ReadString(dr, (int)StagingLocationContactEntity.LocationColumn.StreetLine2);
            contact.City = ReadString(dr, (int)StagingLocationContactEntity.LocationColumn.City);
            contact.StateCode = ReadState(dr, (int)StagingLocationContactEntity.LocationColumn.StateCode);
            contact.ZipCode = ReadString(dr, (int)StagingLocationContactEntity.LocationColumn.ZipCode);
            contact.PriorityIndex = ReadInteger(dr, (int)StagingLocationContactEntity.LocationColumn.PriorityIndex);

            return contact;
        }

        private LocationContact BuildLocationContact(Location location)
        {
            LocationContact contact = new LocationContact();

            contact.PolicySystemKey = location.PolicySystemKey;
            contact.Name = BerkleyCompany.DefaultInsuredContact;
            contact.PriorityIndex = 1;

            return contact;
        }

        private Agency BuildAgency(DataRow dr)
        {
            Agency agency = new Agency();

            agency.AgencyNumber = ReadString(dr, (int)StagingAgencyEntity.FieldColumns.Number);
            agency.AgencyName = ReadString(dr, (int)StagingAgencyEntity.FieldColumns.Name);
            agency.StreetLine1 = ReadString(dr, (int)StagingAgencyEntity.FieldColumns.StreetLine1);
            agency.StreetLine2 = ReadString(dr, (int)StagingAgencyEntity.FieldColumns.StreetLine2);
            agency.City = ReadString(dr, (int)StagingAgencyEntity.FieldColumns.City);
            agency.StateCode = ReadState(dr, (int)StagingAgencyEntity.FieldColumns.State);
            agency.ZipCode = ReadString(dr, (int)StagingAgencyEntity.FieldColumns.ZipCode);
            agency.PhoneNumber = ReadString(dr, (int)StagingAgencyEntity.FieldColumns.PhoneNumber);
            agency.FaxNumber = ReadString(dr, (int)StagingAgencyEntity.FieldColumns.FaxNumber);

            return agency;
        }

        /// <summary>
        /// Builds an <see cref="Insured"/> for a given row in the 
        /// insured file.
        /// </summary>
        /// <param name="row">The row from which to build the <see cref="Insured"/>.</param>
        /// <returns>The Insured Entity <see cref="Insured"/>.</returns>
        private Insured BuildInsured(DataRow dr, ref BlcInsured oBlcInsured)
        {
            Insured insured = new Insured();

            insured.ClientId = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.ClientID);
            insured.Name = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.InsuredName1);
            insured.Name2 = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.InsuredName2);
            insured.StreetLine1 = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.StreetLine1);
            insured.StreetLine2 = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.StreetLine2);
            insured.StreetLine3 = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.StreetLine3);
            insured.City = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.City);
            insured.StateCode = ReadState(dr, (int)StagingInsuredEntity.InsuredColumn.StateCode);
            insured.ZipCode = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.ZipCode);
            insured.BusinessOperations = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.BusinessOperations);
            insured.BusinessCategory = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.BusinessCategory);

            //pad the SICCode so that there is always 4 digits
            string strSIC = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.SICCode);
            insured.SicCode = (strSIC != string.Empty) ? strSIC.PadLeft(4, '0') : string.Empty;
            insured.NaicsCode = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.NAICSCode);
            insured.DotNumber = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.DOTNumber);
            insured.Underwriter = ReadUnderwriter(ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.UnderwriterUsername));
            insured.CompanyWebsite = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.CompanyWebsite);
            insured.AgentContactName = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.AgentContactName);
            insured.AgentContactPhoneNumber = ReadString(dr, (int)StagingInsuredEntity.InsuredColumn.AgentContactPhone);
            insured.NonrenewedDate = BuildForAccountStatus(insured.ClientId);

            oBlcInsured.InsuredEntity = insured;
            return insured;
        }

        /// <summary>
        /// Builds an <see cref="Policy"/> for a given row in the 
        /// policy file.
        /// </summary>
        /// <param name="row">The row from which to build the <see cref="Policy"/>.</param>
        /// <param name="policyStatusList">List of all the policy statuses for the insrued.</param>
        /// <returns>A <see cref="Policy"/>.</returns>
        private Policy BuildPolicy(DataRow dr)
        {
            Policy policy = new Policy();

            policy.PolicySystemKey = ReadString(dr, (int)StagingPolicyEntity.PolicyColumn.PolicyId);
            policy.PolicyNumber = ReadString(dr, (int)StagingPolicyEntity.PolicyColumn.PolicyNumber);
            policy.PolicyMod = ReadInteger(dr, (int)StagingPolicyEntity.PolicyColumn.PolicyVersion);
            policy.PolicySymbol = ReadString(dr, (int)StagingPolicyEntity.PolicyColumn.PolicySymbol);
            policy.EffectiveDate = ReadDateTime(dr, (int)StagingPolicyEntity.PolicyColumn.EffectiveDate);
            policy.ExpireDate = ReadDateTime(dr, (int)StagingPolicyEntity.PolicyColumn.ExpirationDate);
            policy.Premium = ReadDecimal(dr, (int)StagingPolicyEntity.PolicyColumn.TotalEstimatedPremium);
            policy.Carrier = ReadString(dr, (int)StagingPolicyEntity.PolicyColumn.Carrier);
            policy.PolicyStateCode = ReadState(dr, (int)StagingPolicyEntity.PolicyColumn.StateAbbreviation);
            policy.BranchCode = ReadString(dr, (int)StagingPolicyEntity.PolicyColumn.BranchCode);
            policy.HazardGrade = ReadString(dr, (int)StagingPolicyEntity.PolicyColumn.HazardGrade);
            policy.ProfitCenterCode = ReadString(dr, (int)StagingPolicyEntity.PolicyColumn.ProfitCenter);
            policy.RatingCompany = ReadString(dr, (int)StagingPolicyEntity.PolicyColumn.RatingCompany);

            return policy;
        }



        #region Read Routines

        private string ReadString(DataRow dr, int columnNumber)
        {
            return SafeRead(dr[columnNumber]);
        }

        private bool ReadBoolean(DataRow dr, int columnNumber)
        {
            // Call the ICompanyEntity indexer[] which returns the mapped column number
            return Convert.ToBoolean(SafeRead(dr[columnNumber]));

        }

        private DateTime ReadDateTime(DataRow dr, int columnNumber)
        {
            return (ParseDateTime(SafeRead(dr[columnNumber])));
        }

        /// <summary>
        /// Parses a date/time string from the input files and converts it 
        /// into a representative <see cref="DateTime"/> object.
        /// </summary>
        /// <param name="dateTime">The string to parse.</param>
        /// <returns>A DateTime representative of the dateTime.</returns>
        private DateTime ParseDateTime(string dateTime)
        {
            DateTime result = DateTime.MinValue;

            if (IsMeaningful(dateTime))
            {
                try
                {
                    result = Convert.ToDateTime(dateTime, FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // swallow the error and return DateTime.MinValue
                }
                catch (FormatException)
                {
                    // swallow the error and return DateTime.MinValue
                }
            }

            return result;
        }
        /// <summary>
        /// Parses a date string from the input files and converts it 
        /// into a representative <see cref="DateTime"/> object.
        /// </summary>
        /// <param name="date">The string to parse.</param>
        /// <returns>A DateTime representative of the date.</returns>
        private DateTime ParseDate(string date)
        {
            DateTime result = DateTime.MinValue;

            if (IsMeaningful(date))
            {
                try
                {
                    result = DateTime.ParseExact(date, "MM/dd/yyyy", FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // swallow the error and return DateTime.MinValue
                }
                catch (FormatException)
                {
                    // swallow the error and return DateTime.MinValue
                }
            }

            return result;
        }

        private int ReadInteger(DataRow dr, int columnNumber)
        {
            return SafeReadInteger(dr[columnNumber].ToString());
        }

        private decimal ReadDecimal(DataRow dr, int columnNumber)
        {
            // Call the ICompanyEntity indexer[] which returns the mapped column number
            return SafeReadDecimal(dr[columnNumber].ToString());
        }

        /// <summary>
        /// Returns an value from the object specified.
        /// </summary>
        /// <param name="amount">The object from which to find the value.</param>
        /// <returns>The value.</returns>
        protected decimal SafeReadDecimal(object amount)
        {
            decimal result = decimal.MinValue;

            try
            {
                if (amount != null)
                    result = decimal.Parse(amount.ToString(), FormatProvider);
            }
            catch (ArgumentNullException)
            {
                // return decimal.MinValue
            }
            catch (FormatException)
            {
                // return decimal.MinValue
            }
            catch (OverflowException)
            {
                // return decimal.MinValue
            }

            return result;
        }

        /// <summary>
        /// Returns a format provider useful for string parsing.
        /// </summary>
        protected static IFormatProvider FormatProvider
        {
            get
            {
                return System.Globalization.CultureInfo.InvariantCulture;
            }
        }

        /// <summary>
        /// Determines whether or not a string has anything in it.
        /// </summary>
        /// <param name="text">The string to test.</param>
        /// <returns>True if the string has content, false otherwise.</returns>
        protected bool IsMeaningful(string text)
        {
            return text != null && text.Trim().Length > 0;
        }
        /// <summary>
        /// Determines whether or not a string has anything in it.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>True if the value has content, false otherwise.</returns>
        protected bool IsMeaningful(int value)
        {
            return value > int.MinValue && value < int.MaxValue;
        }
        /// <summary>
        /// Determines whether or not a decimal has anything in it.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>True if the value has content, false otherwise.</returns>
        protected bool IsMeaningful(decimal value)
        {
            return value > decimal.MinValue && value < decimal.MaxValue;
        }
        /// <summary>
        /// Determines whether or not a value has any meaningful content.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>True if the value has content, false otherwise.</returns>
        protected bool IsMeaningful(Array value)
        {
            return value != null && value.Length > 0;
        }

        /// <summary>
        /// Returns an value from the object specified.
        /// </summary>
        /// <param name="amount">The object from which to find the value.</param>
        /// <param name="nullValue">The default value if null.</param>
        /// <returns>The value.</returns>
        protected decimal SafeReadDecimal(object amount, decimal nullValue)
        {
            decimal result = nullValue;

            try
            {
                if (amount != null)
                    result = decimal.Parse(amount.ToString(), FormatProvider);
            }
            catch (ArgumentNullException)
            {
                // return decimal.MinValue
            }
            catch (FormatException)
            {
                // return decimal.MinValue
            }
            catch (OverflowException)
            {
                // return decimal.MinValue
            }

            return result;
        }

        /// <summary>
        /// Returns an value from the array, row, and column specified.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The value.</returns>
        protected int SafeReadInteger(string value)
        {
            int result = int.MinValue;

            if (IsMeaningful(value))
            {
                try
                {
                    result = int.Parse(value, FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // return decimal.MinValue
                }
                catch (FormatException)
                {
                    // return decimal.MinValue
                }
                catch (OverflowException)
                {
                    // return decimal.MinValue
                }
            }

            return result;
        }

        /// <summary>
        /// Returns an value from the object in the datatable.
        /// </summary>
        /// <param name="data">The object from which to find the value.</param>
        /// <returns>The value.</returns>
        protected static string SafeRead(object data)
        {
            string result = string.Empty;

            if (data != null && data.ToString().Trim().Length > 0)
                result = data.ToString().Trim();

            return result;
        }

        private decimal ReadPercentage(DataRow dr, int columnNumber)
        {
            decimal result = ReadDecimal(dr, columnNumber);
            return (result != Decimal.MinValue) ? result / 100 : Decimal.MinValue;
        }

        private string ReadState(DataRow dr, int columnNumber)
        {
            // Only allow valid state codes
            string strState = ReadString(dr, columnNumber);

            //return (State.GetOne("Code = ?", strState) != null) ? strState : string.Empty; // -- bypassed for the refresh build only
            return strState;
        }

        private string ReadValuationType(DataRow dr, int columnNumber)
        {
            // Only allow valid state codes
            string result = ReadString(dr, columnNumber);

            //return (BuildingValuationType.GetOne("Code = ?", result) != null) ? result : string.Empty; // -- bypassed for the refresh build only
            return result;
        }

        private string ReadUnderwriter(string userName)
        {
            DataSet ds = BlcStoredProcedure.SQLDbImportSprocs("Underwriter_Get", ConnectionInfo, userName);
            if (ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
            {
                throw new NotSupportedException(string.Format("No underwriter found for username {0}", userName));
            }

            //if the underwriter doesn't already exist in ARMS, then add it to the db
            //Underwriter underwriter = Underwriter.GetOne("Username = ? && CompanyID = ?", userName, BerkleyCompany.ID);
            Underwriter underwriter = UnderwriterService.GetUnderwriterByUsername(userName, BerkleyCompany.CompanyId);
            
            if (underwriter == null)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                
                //get a random new code first
                string underwriterCode = Guid.NewGuid().ToString().Substring(0, 3).ToUpper();

                underwriter = new Underwriter();
                underwriter.UnderwriterCode = underwriterCode;
                underwriter.CompanyId = BerkleyCompany.CompanyId;
                underwriter.UnderwriterName = ReadString(dr, (int)StagingUnderwriterEntity.UnderwriterColumn.Name);
                underwriter.DomainName = "WRBTS";
                underwriter.Username = userName;
                underwriter.PhoneNumber = ReadString(dr, (int)StagingUnderwriterEntity.UnderwriterColumn.PhoneNumber);
                underwriter.PhoneExt = ReadString(dr, (int)StagingUnderwriterEntity.UnderwriterColumn.PhoneExt);
                underwriter.EmailAddress = ReadString(dr, (int)StagingUnderwriterEntity.UnderwriterColumn.EmailAddress);
                underwriter.Title = ReadString(dr, (int)StagingUnderwriterEntity.UnderwriterColumn.Title);

                //underwriter.Save();
                throw new NotImplementedException();
            }

            return underwriter.UnderwriterCode;
        }

        private string ReadRateModType(DataRow dr, int columnNumber)
        {
            string strRateModType = ReadString(dr, columnNumber);

            // Only allow valid rate mod types
            if (strRateModType != null && strRateModType.ToUpper().Contains("C"))
            {
                //return RateModificationFactorType.Construction.Code;
                return "C";
            }
            else if (strRateModType != null && strRateModType.ToUpper().Contains("E"))
            {
                //return RateModificationFactorType.Experience.Code;
                return "E";
            }
            else
            {
                //return RateModificationFactorType.Unknown.Code;
                return "U";
            }
        }

        #endregion

    }
}
