using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
//using Berkley.BLC.Business;
//using Berkley.BLC.Core;
using Berkley.BLC.Import.Properties;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;

namespace Berkley.BLC.Import
{
    public class BNPGImportBuilder : BaseImportBuilder, IImportBuilder
    {
        private BlcStoredProcedure moStoredProcedure = null;
        private CompanyPolicyEntity moPolicyFieldEntity = new CompanyPolicyEntity();
        private CompanyInsuredEntity moInsuredFieldEntity = new CompanyInsuredEntity();
        private CompanyAgencyEntity moAgencyFieldEntity = new CompanyAgencyEntity();
        private CompanyLocationEntity moLocationFieldEntity = new CompanyLocationEntity();
        private CompanyBuildingEntity moBuildingFieldEntity = new CompanyBuildingEntity();
        private CompanyClaimEntity moCompanyClaimEntity = new CompanyClaimEntity();
        private CompanyRateModFactorEntity moCompanyRateModFactorEntity = new CompanyRateModFactorEntity();
        private BNPGLineOfBusiness moLineOfBusiness = new BNPGLineOfBusiness();

        #region Line Of Business / Coverage
        private CompanyCoverageEntity moCoverageFieldEntity = new CompanyCoverageEntity();
        private BNPGCoverageDictionary moCoverageDictionary = new BNPGCoverageDictionary();
        #endregion

        public BNPGImportBuilder(string strClientId, string strPolicyNumber, string strAgentNumber)
        {
            base.Initialize(this, strClientId, strPolicyNumber, strAgentNumber);
            // Rearrange fields in moPolicyFieldEntity as necessary
        }

        #region IImportBuilder Members
        public BlcStoredProcedure GetStoredProcedure()
        {
                      
            if (moStoredProcedure == null)
            {
                //string strConnection = CoreSettings.BNPConnectionString;
                string strConnection = ConfigurationManager.AppSettings["BNPConnectionString"];

                CompanySpEntityList oList = new CompanySpEntityList(strConnection, base.ClientId, base.PolicyNumber, base.AgentNumber);
                oList.Load(Settings.Default.NewBNPGImportFileList);
                moStoredProcedure = new BlcStoredProcedure(oList);
            }

            if (moStoredProcedure == null || moStoredProcedure.DataTables[CompanySpEntity.SpType.Policy].Rows.Count <= 0)
            {
                //string strConnection = CoreSettings.NationalAccountsConnectionString;
                string strConnection = ConfigurationManager.AppSettings["NationalAccountsConnectionString"];

                CompanySpEntityList oList = new CompanySpEntityList(strConnection, base.ClientId, base.PolicyNumber, base.AgentNumber);
                oList.Load(Settings.Default.BNPGImportFileList);
                moStoredProcedure = new BlcStoredProcedure(oList);
            }
            
            //check if any records were return for the insured sp, if not point to CWG DSS
            if (moStoredProcedure == null || moStoredProcedure.DataTables[CompanySpEntity.SpType.Policy].Rows.Count <= 0)
            {
                //string strConnection = CoreSettings.CWGConnectionString;
                string strConnection = ConfigurationManager.AppSettings["CWGConnectionString"];

                CompanySpEntityList oList2 = new CompanySpEntityList(strConnection, base.ClientId, base.PolicyNumber, base.AgentNumber);
                oList2.Load(Settings.Default.CWGImportFileList);
                moStoredProcedure = new BlcStoredProcedure(oList2);
            }
           

            return moStoredProcedure;
        }

        public BlcStoredProcedure GetStoredProcedure(CompanySpEntity.SpType eType)
        {
            if (BlcCompany.BerkleyNorthPacificGroup.MilestoneDate == DateTime.MinValue || BlcCompany.BerkleyNorthPacificGroup.MilestoneDate > DateTime.Today)
            {
                //string strConnection = CoreSettings.NationalAccountsConnectionString;
                string strConnection = ConfigurationManager.AppSettings["NationalAccountsConnectionString"];

                CompanySpEntityList oList = new CompanySpEntityList(strConnection, base.ClientId, base.PolicyNumber, base.AgentNumber);
                oList.Load(Settings.Default.BNPGImportFileList);
                moStoredProcedure = new BlcStoredProcedure(oList, eType);

            }
            else
            {
                //string strConnection = CoreSettings.BNPConnectionString;
                string strConnection = ConfigurationManager.AppSettings["BNPConnectionString"];

                CompanySpEntityList oList = new CompanySpEntityList(strConnection, base.ClientId, base.PolicyNumber, base.AgentNumber);
                oList.Load(Settings.Default.NewBNPGImportFileList);
                moStoredProcedure = new BlcStoredProcedure(oList, eType);
            }

            return moStoredProcedure;
        }

        private string TrimPolicyNumber(string strPolicyNumber)
        {
            if (strPolicyNumber.Length > 2)
            {
                if (base.PolicyNumber.StartsWith("9"))
                {
                    return strPolicyNumber.Substring(1, strPolicyNumber.Length - 1);
                }
                else
                {
                    return strPolicyNumber.Substring(2, strPolicyNumber.Length - 2);
                }
            }
            else
            {
                return strPolicyNumber;
            }
        }

        public BlcInsured BuildComplete(BlcInsured oBlcInsured)
        {
            // The BlcDataSet has been filled out.  Do the Insert into the BLC DB
            // If all of the company (derived) classes do the same thing, put the basic insert/update
            // in BaseImportBuilder
            Policy oPolicy;
            BNPGLineOfBusiness oLineOfBusiness = new BNPGLineOfBusiness();
            LineOfBusiness.LineOfBusinessType eType;
            List<Policy> oPolicyList = oBlcInsured.PolicyList;
            for (int i = 0; i < oPolicyList.Count; i++)
            {
                oPolicy = oPolicyList[i];
                eType = oLineOfBusiness.GetType(oPolicy.PolicySymbol);
                oPolicy.LineOfBusinessCode = oLineOfBusiness.GetBlcName(eType);
                oPolicy.PolicyNumber = TrimPolicyNumber(oPolicy.PolicyNumber);
                oPolicyList[i] = oPolicy;
            }
            //Trim the policies for the rate mods
            for (int j = 0; j < oBlcInsured.RateModFactorList.Count; j++)
            {
                oBlcInsured.RateModFactorList[j].PolicyNumber = TrimPolicyNumber(oBlcInsured.RateModFactorList[j].PolicyNumber);
            }
            return oBlcInsured;
        }

        public Agency BuildForAgency(string agencyNumber)
        {
            Berkley.BLC.Import.APSWebService service = new Berkley.BLC.Import.APSWebService(BlcCompany.BerkleyNorthPacificGroup.ApsEndPointUrl);
            APSProxy.agency apsAgency = service.GetAgency(agencyNumber);

            return base.BuildAgency(apsAgency);
        }

        public CompanyCoverageEntity CoverageFieldEntity
        {
            get { return moCoverageFieldEntity; }
        }

        public List<CompanyCoverageColumn> GetCoverage(LineOfBusiness.LineOfBusinessType eType)
        {
            return moCoverageDictionary.GetColumnList(eType);
        }

        public int GetMaxCoverageColumns()
        {
            return moCoverageDictionary.GetColumnCount();
        }

        public int GetMaxColumnNumber()
        {
            return moCoverageDictionary.GetMaxColumnNumber();
        }

        // The derived classes can change the field mapping
        public CompanyPolicyEntity PolicyFieldEntity
        {
            get { return moPolicyFieldEntity; }
        }

        // The derived classes can change the field mapping
        public CompanyInsuredEntity InsuredFieldEntity
        {
            get { return moInsuredFieldEntity; }
        }

        public CompanyAgencyEntity AgencyFieldEntity
        {
            get { return moAgencyFieldEntity; }
        }

        public CompanyLocationEntity LocationFieldEntity
        {
            get { return moLocationFieldEntity; }
        }

        public CompanyBuildingEntity BuildingFieldEntity
        {
            get { return moBuildingFieldEntity; }
        }

        public LineOfBusiness LineOfBusinessFieldEntity
        {
            get { return moLineOfBusiness; }
        }

        public CompanyClaimEntity ClaimFieldEntity
        {
            get { return moCompanyClaimEntity; }
        }

        public CompanyRateModFactorEntity RateModFactorFieldEntity
        {
            get { return moCompanyRateModFactorEntity; }
        }

        public string CompanyAbbreviation
        {
            get { return "BNPG"; }
        }
        #endregion
    }
}
