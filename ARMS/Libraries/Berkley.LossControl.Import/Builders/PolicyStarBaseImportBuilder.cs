using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
//using Berkley.BLC.Business;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;
using Berkley.BLC.Import.PolicyStar;
using Berkley.Import.PolicyStar;
using APSProxy = Berkley.BLC.Import.APSProxy;
//using Berkley.BLC.Core.Extensions;
using System.Linq;

namespace Berkley.BLC.Import
{
    /// <summary>
    /// This class 1. Contains common functions for use in DB Access
    ///            2. Defines the Entities/classes that make up a Survey Insert into the ARMS Database from PDR
    /// </summary>
    public class PolicyStarBaseImportBuilder
    {
        private IImportBuilder moImportBuilder;
        private BlcInsured moBlcInsured = new BlcInsured();
        private string mstrPolicyNumber;
        private Company berkleyCompany;
        private List<string> policyStatusList = new List<string>();
        private List<string> policyCancelPendingAuditList = new List<string>();
        private List<string> moProperCaseExclusions = new List<string>();
        private string usesVehicleDetails;
        private string usesIMSchedules;
        private bool usesAPSUnderwriter;

        public PolicyStarBaseImportBuilder()
        {
        }

        public void Initialize(IImportBuilder oImportBuilder, string strPolicyNumber, Company company)
        {
            moImportBuilder = oImportBuilder;
            mstrPolicyNumber = strPolicyNumber;
            berkleyCompany = company;
            usesVehicleDetails = Utility.GetCompanyParameter("UsesVehicleDetail", BerkleyCompany).ToLower();
            usesIMSchedules = Utility.GetCompanyParameter("UsesIMSchedules", BerkleyCompany).ToLower();            
            usesAPSUnderwriter = (Utility.GetCompanyParameter("UsesAPSUnderwriter", company).ToUpper() == "TRUE") ? true : false;
        }

        protected string PolicyNumber
        {
            get { return mstrPolicyNumber; }
        }

        protected Company BerkleyCompany
        {
            get { return berkleyCompany; }
        }

        protected string SearchParameters
        {
            get
            {
                string strReturn = "";
                if (mstrPolicyNumber != null && mstrPolicyNumber.Length > 0)
                {
                    strReturn = "Number: " + mstrPolicyNumber + " ";
                }
                return strReturn;
            }
        }

        /// <summary>
        /// Calls the Policy Star service and returns the aggregate data from a quote.
        /// </summary>
        /// <returns>A BlcInsured filled out with data from all of the policy star service.</returns>
        public BlcInsured BuildRequestFromQuote()
        {
            BLCPolicyStarWebService service = new BLCPolicyStarWebService(BerkleyCompany.PolicyStarEndPointUrl);
            return Build(service.GetQuotes(PolicyNumber, BerkleyCompany.CompanyNumber), true, BerkleyCompany);
        }

        /// <summary>
        /// Calls the Policy Star service and returns the aggregate data from a quote.
        /// </summary>
        /// <returns>A BlcInsured filled out with data from all of the policy star service.</returns>
        public BlcInsured BuildRequestFromPolicy()
        {
            BLCPolicyStarWebService service = new BLCPolicyStarWebService(BerkleyCompany.PolicyStarEndPointUrl);
            return Build(service.GetPolicies(PolicyNumber, BerkleyCompany.CompanyNumber), false, BerkleyCompany);
        }

        /// <summary>
        /// Calls the Policy Star service and returns the aggregate data from a quote.
        /// </summary>
        /// <returns>A BlcInsured filled out with data from all of the policy star service.</returns>
        public BlcInsured BuildRequestFromPolicy(Company company)
        {
            BLCPolicyStarWebService service = new BLCPolicyStarWebService(company.PolicyStarEndPointUrl);
            return Build(service.GetPolicies(PolicyNumber, BerkleyCompany.CompanyNumber), false, company);
        }

        /// <summary>
        /// Calls the Policy Star service and returns the aggregate data from a quote.
        /// </summary>
        /// <returns>A BlcInsured filled out with data from all of the policy star service.</returns>
        public BlcInsured BuildRequestFromPolicy(Company company, string accountNumber)
        {
            BLCPolicyStarWebService service = new BLCPolicyStarWebService(company.PolicyStarEndPointUrl);
            return Build(service.GetPolicies(PolicyNumber, accountNumber), false, company);
        }

        /// <summary>
        /// Calls the Policy Star service and returns a flag of whether or not the policy is valid.
        /// </summary>
        /// <returns>A flag.</returns>
        public bool PolicyExists()
        {
            BLCPolicyStarWebService service = new BLCPolicyStarWebService(BerkleyCompany.PolicyStarEndPointUrl);
            PremiumAuditLossControlDO ds = service.GetPolicies(PolicyNumber, BerkleyCompany.CompanyNumber);

            //Check if a valid dataset was returned from the service
            if (ds == null || ds.policyAuditDO == null || ds.policyAuditDO[0] == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Calls the Policy Star service and returns the aggregate data from a policy.
        /// </summary>
        /// <returns>A BlcInsured filled out with data from all of the policy star service.</returns>
        internal BlcInsured Build(PremiumAuditLossControlDO ds, bool isQuote, Company company)
        {
            //Short circuit if no policies are returned
            if (ds == null || ds.policyAuditDO == null || ds.policyAuditDO[0] == null)
            {
                throw new BlcNoInsuredException("Insured not found for " + SearchParameters);
            }

            //Traverse the products of the policies looking for unique locations
            SortedList locationList = new SortedList();

            //Check if the policy number exist in the returned results
            bool policyNumberMatched = false;
            foreach (PolicyAuditDO policyDataSet in ds.policyAuditDO)
            {
                if (ReadString(policyDataSet.policyNbr) == PolicyNumber && policyDataSet.namedInsured != null)
                {
                    foreach (PDRNamedInsuredDO namedInsured in policyDataSet.namedInsured)
                    {
                        if (namedInsured != null)
                        {
                            policyNumberMatched = true;
                        }
                    }
                }
            }

            foreach (PolicyAuditDO policyDataSet in ds.policyAuditDO)
            {
                if (moBlcInsured.InsuredEntity == null &&
                    ((ReadString(policyDataSet.policyNbr) == PolicyNumber && policyNumberMatched) || 
                    ((ReadString(policyDataSet.policyNbr) != PolicyNumber && !policyNumberMatched) ||
                    isQuote)))
                {
                    //Build the insured
                    if (policyDataSet.namedInsured != null)
                    {
                        BuildInsured(policyDataSet, ref moBlcInsured);
                    }
                    else
                    {
                        throw new BlcNoInsuredException("Insured not found for " + SearchParameters);
                    }

                    //Build the agency
                    if (policyDataSet.agency != null)
                    {
                        //determine if the company uses APS or not
                        if (!string.IsNullOrEmpty(company.ApsEndPointUrl))
                        {
                            try
                            {
                                //APS
                                APSWebService service = new APSWebService(company.ApsEndPointUrl);
                                APSProxy.agency apsAgency = service.GetAgency(policyDataSet.agency.agencyNumber);

                                moBlcInsured.AgencyEntity = BuildAgency(apsAgency);
                            }
                            catch
                            {
                                moBlcInsured.AgencyEntity = BuildAgency(policyDataSet.agency);
                            }
                        }
                        else
                        {
                            //P*
                            moBlcInsured.AgencyEntity = BuildAgency(policyDataSet.agency);
                        }
                    }
                    else
                    {
                        throw new BlcImportException("Agency not found for " + SearchParameters);
                    }
                }

                //Build the policy
                Policy policy = BuildPolicy(policyDataSet);
                if (policy.PolicyNumber != string.Empty && policy.PolicyMod != Int32.MinValue && policyDataSet.products != null && ReadString(policyDataSet.policyStatus) == "ACTIVE")
                {
                    moBlcInsured.PolicyList.Add(policy);

                    //Build the coverages
                    BlcCoverage[] coverages = BuildCoverages(policyDataSet.products, policy);
                    foreach (BlcCoverage coverage in coverages)
                    {
                        moBlcInsured.CoverageList.Add(new CoverageKey(coverage), coverage);
                    }

                    if (policyDataSet.products != null)
                    {

                        //Build the rate mods
                        foreach (SecondaryProductDO product in policyDataSet.products)
                        {
                            if (product.rateMod != null)
                            {
                                foreach (RateModFactor rateMod in product.rateMod)
                                {
                                    moBlcInsured.RateModFactorList.Add(BuildRateModFactor(rateMod, policy));
                                }
                            }
                        }

                        foreach (SecondaryProductDO product in policyDataSet.products)
                        {
                            if (product.units != null) //Locations
                            {
                                foreach (PDRUnitDO locationUnit in product.units)
                                {
                                    if (ReadString(locationUnit.entityName).ToUpper() == "LOCATION")
                                    {
                                        // build and add location 
                                        Location location;
                                        if (!locationList.Contains(BuildLocationKey(locationUnit)))
                                        {
                                            location = BuildLocation(locationUnit, policy);
                                            moBlcInsured.PolicyLocationList.Add(location);

                                            //retain location for uniqueness
                                            locationList.Add(BuildLocationKey(locationUnit), location);
                                        }
                                        else
                                        {
                                            //use previous built location
                                            location = locationList[BuildLocationKey(locationUnit)] as Location;

                                            //even though we can reuse the previous built location, we need to check if this current one has a protection class entered
                                            int publicProtection = StripNonNumericChars(locationUnit.protectionClass);
                                            if (location.PublicProtection == Int32.MinValue && publicProtection != Int32.MinValue)
                                            {
                                                location.PublicProtection = publicProtection;
                                            }
                                        }

                                        if (locationUnit.unit != null) //Buildings
                                        {
                                            foreach (PDRUnitDO buildingUnit in locationUnit.unit)
                                            {
                                                if (ReadString(buildingUnit.entityName).ToUpper() == "BUILDING")
                                                {
                                                    //Discovered on 5/24/10 that the spinkler indicator is at the location level in p*
                                                    //if (ReadString(locationUnit.protectiveSafeGrd) != string.Empty)
                                                    //{
                                                    //    if (ReadString(locationUnit.protectiveSafeGrd) == "P-1")
                                                    //    {
                                                    //        location.HasSprinklerSystem = 1;
                                                    //    }
                                                    //    else
                                                    //    {
                                                    //        location.HasSprinklerSystem = 0;
                                                    //    }
                                                    //}
                                                    
                                                    // build and add building 
                                                    moBlcInsured.BuildingList.Add(BuildBuilding(buildingUnit, location));
                                                }
                                            }
                                        }

                                        if (locationUnit.coverage != null)
                                        {
                                            foreach (PDRCoverageDO buildingUnit in locationUnit.coverage)
                                            {
                                                if (ReadString(buildingUnit.coverageCode).ToUpper() == "SCHD_BLDG")
                                                {
                                                    // build and add building 
                                                    moBlcInsured.BuildingList.Add(BuildBuilding(buildingUnit, locationUnit.coverage, location));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Check if any policies were found
            if (moBlcInsured.PolicyList.Count <= 0)
            {
                throw new BlcNoInsuredException("No policies were found for " + SearchParameters);
            }

            //Default a location
            if (moBlcInsured.PolicyLocationList.Count <= 0)
            {
                //If no locations, add the insured address as the default location
                moBlcInsured.PolicyLocationList.Add(BuildLocation(moBlcInsured.InsuredEntity));
            }

            //Set the account status based on the statuses of all the insured's policies
            //Only if all policies are non-renewed, is the client set as non-renewed
            moBlcInsured.InsuredEntity.NonrenewedDate = (!policyStatusList.Contains("N")) ? DateTime.Now : DateTime.MinValue;

            return moImportBuilder.BuildComplete(moBlcInsured);
        }

        /// <summary>
        /// Builds an <see cref="Insured"/> for a given PolicyAuditDO in the PolicyStar xml file.
        /// </summary>
        /// <returns>The Insured Entity <see cref="Insured"/>.</returns>
        public Insured BuildForAccountStatus(string ClientID)
        {
            Insured insured = new Insured();
            BLCPolicyStarWebService service = new BLCPolicyStarWebService(BerkleyCompany.PdrEndPointUrl);
            PolicyDataDO[] ds = service.GetAccountStatusByPolicy(ClientID, BerkleyCompany.CompanyNumber);

            //short circuit if these objects are not found
            if (ds == null || ds.Length == 0)
            {
                throw new BlcNoInsuredException("Insured not found for " + SearchParameters);
            }

            //traverse policies getting their statuses
            foreach (PolicyDataDO policyDataObject in ds)
            {
                policyStatusList.Add(ReadString(policyDataObject.policyStatus) + "|" + ReadString(policyDataObject.nonRenewableFlag));
            }

            //Set the account status based on the statuses of all the insured's policies
            //Only if all policies are not active, is the client set as non-renewed
            insured.NonrenewedDate = (!policyStatusList.Contains("ACTIVE|N")) ? DateTime.Now : DateTime.MinValue;

            return insured;
        }

        ///// <summary>
        ///// Builds an <see cref="Insured"/> for a given PolicyAuditDO in the PolicyStar xml file.
        ///// </summary>
        ///// <returns>The Insured Entity <see cref="Insured"/>.</returns>
        //public Insured BuildForAccountStatus()
        //{
        //    Insured insured = new Insured(Guid.NewGuid());
        //    BLCPolicyStarWebService service = new BLCPolicyStarWebService(BerkleyCompany.PolicyStarEndPointUrl);
        //    bool isNonRenewed = service.IsAccountNonRenewed(PolicyNumber, BerkleyCompany.CompanyNumber);

        //    //Set the account status based on the statuses of all the insured's policies
        //    //Only if all policies are non-renewed, is the client set as non-renewed
        //    insured.NonrenewedDate = (isNonRenewed) ? DateTime.Now : DateTime.MinValue;

        //    return insured;
        //}

        /// <summary>
        /// Builds an <see cref="Insured"/> for a given PolicyAuditDO in the PolicyStar xml file.
        /// </summary>
        /// <param name="starsInsured">The insured PolicyAuditDO data object.</param>
        /// <param name="oBlcInsured">The blcInsured object that will hook up with the insured object.</param>
        /// <returns>The Insured Entity <see cref="Insured"/>.</returns>
        private Insured BuildInsured(PolicyAuditDO pdrPolicy, ref BlcInsured oBlcInsured)
        {
            Insured insured = new Insured();

            insured.BusinessOperations = ReadString(pdrPolicy.businessDesc);
            if (usesAPSUnderwriter)
            {
                Prospect.ProspectWebService service = new Prospect.ProspectWebService();
                Underwriter underwriter = service.GetAssignedUnderwriter(pdrPolicy.policyNbr, BerkleyCompany);
                insured.Underwriter = (underwriter != null) ? underwriter.UnderwriterCode : string.Empty;
            }
            else
            {
                insured.Underwriter = ReadString(pdrPolicy.underwriterCode);
            }
               
            insured.PortfolioId = ReadString(pdrPolicy.portfolioId);

            //Get the SIC code from the custom details
            if (pdrPolicy.customDetails != null)
            {
                foreach (PDRCustomDetailDO customDetail in pdrPolicy.customDetails)
                {
                    if (ReadString(customDetail.customDetailName).ToUpper() == "SIC_CD")
                    {
                        insured.SicCode = (ReadString(customDetail.customDetailValue) != string.Empty) ? ReadString(customDetail.customDetailValue).PadLeft(4, '0') : string.Empty;
                    }
                }
            }

            //get the insured's primary address
            bool primaryAddressFound = false;
            for (int i = 0; i < pdrPolicy.namedInsured.Length; i++)
            {
                if (pdrPolicy.namedInsured[i] != null)
                {
                    if (pdrPolicy.namedInsured[i].insAddress != null)
                    {
                        for (int j = 0; j < pdrPolicy.namedInsured[i].insAddress.Length; j++)
                        {
                            if (primaryAddressFound == false &&
                                ReadString(pdrPolicy.namedInsured[i].insAddress[j].primaryFLG) == "Y")
                            {
                                PDRAddressDO address = pdrPolicy.namedInsured[i].insAddress[j];
                                insured.StreetLine1 = ReadStreetAddress(address.houseNbr, address.address1);
                                insured.StreetLine2 = ReadString(address.address2);
                                insured.City = ReadString(address.city);
                                insured.StateCode = ReadState(address.stateProvCd);
                                insured.ZipCode = ReadString(address.postalCode);
                                primaryAddressFound = true;
                                break;
                            }
                        }
                    }
                }
            }

            //If a primary address is not returned, then just grab the first address
            if (primaryAddressFound == false && pdrPolicy.namedInsured[0] != null && 
                pdrPolicy.namedInsured[0].insAddress != null && pdrPolicy.namedInsured[0].insAddress.Length > 0)
            {
                PDRAddressDO address = pdrPolicy.namedInsured[0].insAddress[0];
                insured.StreetLine1 = ReadStreetAddress(address.houseNbr, address.address1);
                insured.StreetLine2 = ReadString(address.address2);
                insured.City = ReadString(address.city);
                insured.StateCode = ReadState(address.stateProvCd);
                insured.ZipCode = ReadString(address.postalCode);
            }

            //check if there is any primary named insured.  If not, then grab the first named insured.
            bool primaryNameExist = false;
            for (int i = 0; i < pdrPolicy.namedInsured.Length; i++)
            {
                if (pdrPolicy.namedInsured[i] != null)
                {
                    if (ReadString(pdrPolicy.namedInsured[i].primaryNmInsF) == "Y") //Primary Insured Name
                    {
                        primaryNameExist = true;
                    }
                }
            }

            for (int i = 0; i < pdrPolicy.namedInsured.Length; i++)
            {
                if (pdrPolicy.namedInsured[i] != null)
                {
                    if (ReadString(pdrPolicy.namedInsured[i].primaryNmInsF) == "Y" || !primaryNameExist) //Primary Insured Name
                    {
                        insured.ClientId = ReadString(pdrPolicy.namedInsured[i].insName[0].clientId);
                        insured.Name = ReadString(pdrPolicy.namedInsured[i].insName[0].nameBusiness);

                        //get the owner's info
                        if (pdrPolicy.namedInsured[i].insName[0].communications != null)
                        {
                            foreach (PDRCommunicationDO communication in pdrPolicy.namedInsured[i].insName[0].communications)
                            {
                                if (ReadString(communication.communicationType).ToUpper() == "CTWORKPHN")
                                {
                                    oBlcInsured.OwnerPhone = ReadString(communication.communicationValue);
                                }
                                else if (ReadString(communication.communicationType).ToUpper() == "CTHOMEPHN" && oBlcInsured.OwnerAltPhone == string.Empty)
                                {
                                    oBlcInsured.OwnerAltPhone = ReadString(communication.communicationValue);
                                }
                                else if (ReadString(communication.communicationType).ToUpper() == "CTCELLPHN" && oBlcInsured.OwnerAltPhone == string.Empty)
                                {
                                    oBlcInsured.OwnerAltPhone = ReadString(communication.communicationValue);
                                }
                                else if (ReadString(communication.communicationType).ToUpper() == "CTSFDIRPHN" && oBlcInsured.OwnerAltPhone == string.Empty)
                                {
                                    oBlcInsured.OwnerAltPhone = ReadString(communication.communicationValue);
                                }
                                else if (ReadString(communication.communicationType).ToUpper() == "CTSFDIRPHN" && oBlcInsured.OwnerAltPhone == string.Empty)
                                {
                                    oBlcInsured.OwnerFax = ReadString(communication.communicationValue);
                                }
                                else if (ReadString(communication.communicationType).ToUpper() == "CTEMAIL")
                                {
                                    oBlcInsured.OwnerEmail = ReadString(communication.communicationValue);
                                }
                            }
                        }

                        //owner name
                        oBlcInsured.Owner = ReadString(pdrPolicy.ownerLegalName);

                        break;
                    }
                    else // Second Insured Name
                    {
                        insured.Name2 = pdrPolicy.namedInsured[i].insName[0].nameBusiness;
                    }
                }
            }

            // -- bypassed for the refresh project to build only
            //Since the p* service is unreliable in always returning a client address, this hack
            //has been put in place to pull in previous client address information by client id
            //if (insured.StreetLine1 == String.Empty && insured.City == String.Empty && insured.StateCode == String.Empty && insured.ZipCode == String.Empty)
            //{
            //    Insured pastInsured = Insured.GetOne("ClientID = ? && !ISNULL(ZipCode) && ZipCode != '' && CompanyID = ?", insured.ClientId, berkleyCompany.CompanyId);
            //    if (pastInsured != null)
            //    {
            //        insured.StreetLine1 = pastInsured.StreetLine1;
            //        insured.StreetLine2 = pastInsured.StreetLine2;
            //        insured.City = pastInsured.City;
            //        insured.StateCode = pastInsured.StateCode;
            //        insured.ZipCode = pastInsured.ZipCode;
            //    }
            //}

            oBlcInsured.InsuredEntity = insured;
            return insured;
        }

        /// <summary>
        /// Builds an <see cref="Agency"/> for a given AgencyDO in the PolicyStar xml file.
        /// </summary>
        /// <param name="starAgency">The AgencyDO data object.</param>
        /// <returns>The Agency Entity <see cref="Agency"/>.</returns>
        internal Agency BuildAgency(APSProxy.agency apsAgency)
        {
            Agency agency = new Agency();

            //name & number
            agency.AgencyName = ReadString(apsAgency.dbaName);
            agency.AgencyNumber = ReadString(apsAgency.agencyCode);

            //address
            foreach (APSProxy.location location in apsAgency.locations)
            {
                if (location.isPrimaryLocation)
                {
                    agency.StreetLine1 = location.line1;
                    agency.StreetLine2 = location.line2;
                    agency.City = location.city;
                    agency.StateCode = (location.usState != null) ? location.usState.description : string.Empty;
                    agency.ZipCode = location.zip;

                    //contact info
                    foreach (APSProxy.locationCommunication communication in location.locationCommunications)
                    {
                        if (communication.communicationType.label == "Phone Number")
                        {
                            agency.PhoneNumber = communication.communicationValue;
                        }

                        if (communication.communicationType.label == "Fax Number")
                        {
                            agency.FaxNumber = communication.communicationValue;
                        }
                    }
                }
            }
            return agency;
        }

        /// <summary>
        /// Builds an <see cref="Agency"/> for a given AgencyDO in the PolicyStar xml file.
        /// </summary>
        /// <param name="starAgency">The AgencyDO data object.</param>
        /// <returns>The Agency Entity <see cref="Agency"/>.</returns>
        private Agency BuildAgency(AgencyDO pdrAgency)
        {
            Agency agency = new Agency();

            //name & number
            agency.AgencyName = ReadString(pdrAgency.agencyName);
            agency.AgencyNumber = ReadString(pdrAgency.agencyNumber);

            //address
            if (pdrAgency.address != null)
            {
                agency.StreetLine1 = ReadStreetAddress(pdrAgency.address.houseNbr, pdrAgency.address.address1);
                agency.StreetLine2 = ReadString(pdrAgency.address.address2);
                agency.City = ReadString(pdrAgency.address.city);
                agency.StateCode = ReadState(pdrAgency.address.stateProvCd);
                agency.ZipCode = ReadString(pdrAgency.address.postalCode);
            }

            //contact info
            agency.PhoneNumber = ReadString(pdrAgency.agencyPhone);
            //agency.FaxNumber = ReadString(pdrAgency.agencyFax);

            return agency;
        }

        /// <summary>
        /// Builds a <see cref="Policy"/> for a given PolicyAuditDO in the PolicyStar xml file.
        /// </summary>
        /// <param name="starsPolicy">The policy PolicyAuditDO data object.</param>
        /// <returns>The Policy Entity <see cref="Policy"/>.</returns>
        private Policy BuildPolicy(PolicyAuditDO pdrPolicy)
        {
            Policy policy = new Policy();

            //Get the profit center from the custom details
            //if (pdrPolicy.customDetails != null)
            //{
            //    foreach (PDRCustomDetailDO customDetail in pdrPolicy.customDetails)
            //    {
            //        if (ReadString(customDetail.customDetailName).ToUpper() == "PCC")
            //        {
            //            policy.ProfitCenter = ReadString(customDetail.customDetailValue);
            //        }
            //    }
            //}

            //discovered on 1/14/11 that the profit center is stored in the branch code field in P*
            policy.ProfitCenterCode = ReadString(pdrPolicy.branchCode);

            //find the branch code
            if (pdrPolicy.customDetails != null)
            {
                foreach (PDRCustomDetailDO customDetail in pdrPolicy.customDetails)
                {
                    if (ReadString(customDetail.customDetailName).ToUpper() == "BUS_GRP")
                    {
                        policy.BranchCode = ReadString(customDetail.customDetailValue);
                    }
                }
            }

            policy.Carrier = ReadString(pdrPolicy.companyName);
            policy.EffectiveDate = ReadDateTime(pdrPolicy.effectiveDate);
            policy.ExpireDate = ReadDateTime(pdrPolicy.expirationDate);
            policy.PolicyMod = ReadInteger(pdrPolicy.policySeqNbr);
            policy.PolicyNumber = ReadString(pdrPolicy.policyNbr);
            if (berkleyCompany.CompanyId == BlcCompany.BerkleySpecialtyUnderwritingManagers.CompanyId)
            {
                policy.PolicySymbol = ParsePolicySymbol(policy.PolicyNumber);
            }
            else
                policy.PolicySymbol = ReadString(pdrPolicy.policySymbol);
                policy.Premium = ReadDecimal(pdrPolicy.totalEstimatedPremium);
                policy.PolicyStateCode = ReadState(pdrPolicy.primaryProductState);

            //find the hazard grade
            if (pdrPolicy.products != null)
            {
                foreach (SecondaryProductDO product in pdrPolicy.products)
                {
                    if (!String.IsNullOrEmpty(ReadString(product.lobHazardDescription)))
                    {
                        int hazardGrade = StripNonNumericChars(product.lobHazardDescription);
                        policy.HazardGrade = (hazardGrade != Int32.MinValue) ? hazardGrade.ToString() : String.Empty;
                    }
                }
            }

            // -- bypassed for the refresh project to build only
            //try and find past hazard grade for the policy
            //if (policy.HazardGrade == string.Empty)
            //{
            //    Policy pastPolicy = Policy.GetOne("Number = ? && !ISNULL(HazardGrade) && HazardGrade != '' && Insured.CompanyID = ?", policy.PolicyNumber, berkleyCompany.CompanyId);
            //    policy.HazardGrade = (pastPolicy != null) ? pastPolicy.HazardGrade : "";
            //}

            //Add the policy status to the list for setting the client status later
            policyStatusList.Add(ReadString(pdrPolicy.nonRenewableFlag));

            return policy;
        }

        /// <summary>
        /// Builds a <see cref="Location"/> for a given PDRUnitDO in the PolicyStar xml file.
        /// </summary>
        /// <param name="starsLocation">The PDRUnitDO data object.</param>
        /// <param name="policy">The policy this loaction was extracted from.</param>
        /// <returns>The Location Entity <see cref="Location"/>.</returns>
        private Location BuildLocation(PDRUnitDO pdrLocation, Policy policy)
        {
            Location location = new Location();

            if (pdrLocation != null && pdrLocation.address != null)
            {
                location.LocationNumber = ReadInteger(ReadString(pdrLocation.attachNbrI));
                location.PolicySymbol = policy.PolicySymbol;
                location.PolicySystemKey = string.Format("{0}-{1}", location.LocationNumber, policy.PolicyNumber);
                location.StreetLine1 = ReadStreetAddress(pdrLocation.address.houseNbr, pdrLocation.address.address1);
                location.StreetLine2 = ReadString(pdrLocation.address.address2);
                location.City = ReadString(pdrLocation.address.city);
                location.StateCode = ReadState(pdrLocation.address.stateProvCd);
                location.ZipCode = ReadString(pdrLocation.address.postalCode);
                location.Description = ReadString(pdrLocation.unitDesc);
                location.IsActive = true;

                //Discovered on 3/29/10 that the public protection is at the location level in p*
                location.PublicProtection = StripNonNumericChars(pdrLocation.protectionClass);
            }

            return location;
        }

        /// <summary>
        /// Builds a <see cref="Location"/> for a given PDRUnitDO in the PolicyStar xml file.
        /// </summary>
        /// <param name="starsLocation">The PDRUnitDO data object.</param>
        /// <param name="policy">The policy this loaction was extracted from.</param>
        /// <returns>The Location Entity <see cref="Location"/>.</returns>
        private Location BuildLocation(Insured insured)
        {
            Location location = new Location();

            location.PolicySystemKey = string.Format("1-{0}", PolicyNumber);
            location.LocationNumber = 1;
            location.IsActive = true;
            location.StreetLine1 = moBlcInsured.InsuredEntity.StreetLine1;
            location.StreetLine2 = moBlcInsured.InsuredEntity.StreetLine2;
            location.City = moBlcInsured.InsuredEntity.City;
            location.StateCode = moBlcInsured.InsuredEntity.StateCode;
            location.ZipCode = moBlcInsured.InsuredEntity.ZipCode;

            return location;
        }

        /// <summary>
        /// Builds an identifier for a given location
        /// </summary>
        /// <param name="pdrLocation">The PDRUnitDO data object.</param>
        /// <returns>The a key genereted from the locatino info.</returns>
        private string BuildLocationKey(PDRUnitDO pdrLocation)
        {
            StringBuilder sb = new StringBuilder();

            if (pdrLocation != null && pdrLocation.address != null)
            {
                sb.Append(ReadInteger(ReadString(pdrLocation.attachNbrI)));
                sb.Append("|");
                sb.Append(ReadString(pdrLocation.address.houseNbr));
                sb.Append("|");
                sb.Append(ReadString(pdrLocation.address.city));
                sb.Append("|");
                sb.Append(ReadState(pdrLocation.address.stateProvCd));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Builds a <see cref="Building"/> for a given PDRUnitDO in the PolicyStar xml file.
        /// </summary>
        /// <param name="pdrBuilding">The PDRUnitDO data object.</param>
        /// <param name="location">The location that the building will be tied to.</param>
        /// <returns>The Building Entity <see cref="Building"/>.</returns>
        private BlcBuilding BuildBuilding(PDRUnitDO pdrBuilding, Location location)
        {
            BlcBuilding building = new BlcBuilding(Guid.NewGuid());

            building.BuildingNumber = ReadShort(ReadString(pdrBuilding.attachNbrI));
            building.PolicySystemKey = location.PolicySystemKey;
            building.LocationAsInt = location.LocationNumber;

            //the following values are actually coverages in PDR
            if (pdrBuilding.coverage != null)
            {
                foreach (PDRCoverageDO coverage in pdrBuilding.coverage)
                {
                    switch (ReadString(coverage.coverageCode).ToUpper())
                    {
                        case "BPP":
                        case "SCHD_CNTS":
                            building.Contents = ReadDecimal(coverage.limitAmt1);
                            break;
                        case "BLDG":
                        case "SCHD_BLDG":
                            building.BuildingValue = ReadDecimal(coverage.limitAmt1);
                            break;
                        case "STK":
                            building.StockValues = ReadDecimal(coverage.limitAmt1);
                            break;
                        case "TE":
                        case "SCHD_BII":
                            building.BusinessInterruption = ReadDecimal(coverage.limitAmt1);
                            break;
                    }

                    //find the sprinkler indicator
                    if (coverage.customDetails != null && coverage.customDetails.Length > 0)
                    {
                        foreach (PDRCustomDtlDO customDetail in coverage.customDetails)
                        {
                            if (ReadString(customDetail.customDetailName).ToUpper() == "RATE_ID")
                            {
                                //sprinkler exist
                                if (ReadString(customDetail.customDetailValue).ToUpper() == "BL_SP_SCH")
                                {
                                    building.HasSprinklerSystem = 1;
                                }
                                else if (ReadString(customDetail.customDetailValue).ToUpper() == "NBL_SP_SCH")
                                {
                                    building.HasSprinklerSystem = 1;
                                    building.HasSprinklerSystemCredit = 1;
                                    break;
                                }
                                else
                                {
                                    building.HasSprinklerSystem = (building.HasSprinklerSystem == 1) ? (short)1 : (short)0;
                                    building.HasSprinklerSystemCredit = 0;
                                }
                            }
                        }
                    }
                }
            }

            building.LocationOccupancy = ReadString(pdrBuilding.unitDesc);
            building.PublicProtection = location.PublicProtection;
            //building.HasSprinklerSystem = location.HasSprinklerSystem;
            building.YearBuilt = ReadInteger(ReadString(pdrBuilding.unitYear));

            return building;
        }

        /// <summary>
        /// Builds a <see cref="Building"/> for a given PDRCoverageDO in the PolicyStar xml file.
        /// </summary>
        /// <param name="pdrBuilding">The PDRCoverageDO data object.</param>
        /// <param name="location">The location that the building will be tied to.</param>
        /// <returns>The Building Entity <see cref="Building"/>.</returns>
        private BlcBuilding BuildBuilding(PDRCoverageDO pdrBuilding, PDRCoverageDO[] pdrBuildingCoverages, Location location)
        {
            BlcBuilding building = new BlcBuilding(Guid.NewGuid());

            building.BuildingNumber = 1;
            building.PolicySystemKey = location.PolicySystemKey;
            building.LocationAsInt = location.LocationNumber;

            //the following values are actually coverages in PDR
            if (pdrBuildingCoverages != null)
            {
                foreach (PDRCoverageDO coverage in pdrBuildingCoverages)
                {
                    //business income
                    if (ReadString(coverage.coverageCode).ToUpper() == "SCHD_BII" && coverage.coverage != null)
                    {
                        building.BusinessInterruption = 0;
                        foreach (PDRCoverageDO subCoverage in coverage.coverage)
                        {
                            building.BusinessInterruption += ReadDecimal(subCoverage.limitAmt1);
                        }
                    }
                }
            }

            //find the sprinkler indicator
            if (pdrBuilding.customDetails != null && pdrBuilding.customDetails.Length > 0)
            {
                foreach (PDRCustomDtlDO customDetail in pdrBuilding.customDetails)
                {
                    if (ReadString(customDetail.customDetailName).ToUpper() == "RATE_ID")
                    {
                        //sprinkler exist
                        if (ReadString(customDetail.customDetailValue).ToUpper() == "BL_SP_SCH")
                        {
                            building.HasSprinklerSystem = 1;
                        }
                        else if (ReadString(customDetail.customDetailValue).ToUpper() == "NBL_SP_SCH")
                        {
                            building.HasSprinklerSystem = 1;
                            building.HasSprinklerSystemCredit = 1;
                            break;
                        }
                        else
                        {
                            building.HasSprinklerSystem = (building.HasSprinklerSystem == 1) ? (short)1 : (short)0;
                            building.HasSprinklerSystemCredit = 0;
                        }
                    }
                }
            }

            building.BuildingValue = ReadDecimal(pdrBuilding.limitAmt1);
            building.PublicProtection = location.PublicProtection;
            //building.HasSprinklerSystem = location.HasSprinklerSystem;

            return building;
        }

        /// <summary>
        /// Builds a <see cref="RateModificationFactor"/> for a given RateModFactor in the PolicyStar xml file.
        /// </summary>
        /// <param name="policyStarRateMod">The RateModFactor data object.</param>
        /// <param name="policy">The policy to tie the rate mod factor to.</param>
        /// <returns>The RateModificationFactor Entity <see cref="RateModificationFactor"/>.</returns>
        private BlcRateModFactor BuildRateModFactor(RateModFactor pdrRateMod, Policy policy)
        {
            BlcRateModFactor blcRateModFactor = new BlcRateModFactor();
            RateModificationFactor factor = new RateModificationFactor();

            factor.StateCode = ReadState(pdrRateMod.rtModState);
            factor.Rate = ReadDecimal(pdrRateMod.rtModFactor);
            factor.RateModificationFactorTypeCode = ReadRateModType(pdrRateMod.rtModCode);

            blcRateModFactor.Entity = factor;
            blcRateModFactor.PolicyMod = policy.PolicyMod;
            blcRateModFactor.PolicyNumber = policy.PolicyNumber;
            return blcRateModFactor;
        }

        /// <summary>
        /// Builds an of <see cref="Coverage"/> entities for a given list of PDRCoverageDOs in the PolicyStar xml file.
        /// </summary>
        /// <param name="oRow">The PDRCoverageDO data object.</param>
        /// <returns>An array of Coverage Entitities.</returns>
        private BlcCoverage[] BuildCoverages(SecondaryProductDO[] products, Policy policy)
        {

            List<BlcCoverage> list = new List<BlcCoverage>();

            // Field Mapping for the Coverage/Line of Business
            CompanyCoverageEntity coverageFields = moImportBuilder.CoverageFieldEntity;

            // Defines each LOB
            LineOfBusiness lineOfBusiness = moImportBuilder.LineOfBusinessFieldEntity;

            // Convert that to a Typed name
            LineOfBusiness.LineOfBusinessType eType = lineOfBusiness.GetType(policy.PolicySymbol);

            // Switch Policy Symbol and get the field mapping for this LOB
            List<CompanyCoverageColumn> companyColumnList = moImportBuilder.GetCoverage(eType);

            //Defines coverage Details (vehicles)
            List<string> listVeh = new List<string>();
            List<string> listIMSchedule = new List<string>();

            try
            {
                if (companyColumnList != null)
                {
                    // Iterage Rows in Coverage - Ignore the column numbers defined in CompanyCoverageEntity
                    foreach (CompanyCoverageColumn coverageColumn in companyColumnList)
                    {
                        bool matchFound = false;

                        BlcCoverage blcCoverage = null;

                        if (coverageColumn.ColumnValueType == ColumnValueType.Enum.ClassCodePayrollEmployees)
                        {
                            foreach (SecondaryProductDO product in products)
                            {
                                if (product.units != null)
                                {
                                    foreach (PDRUnitDO unit in product.units)
                                    {
                                        if (unit.coverage != null)
                                        {
                                            foreach (PDRCoverageDO unitCoverage in unit.coverage)
                                            {
                                                if (IsMeaningful(unitCoverage.classCodeCov))
                                                {
                                                    blcCoverage = BuildCoverage(unitCoverage, coverageColumn, policy, lineOfBusiness);
                                                    list.Add(blcCoverage);
                                                    matchFound = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (coverageColumn.ColumnValueType == ColumnValueType.Enum.NumberOfVehicles)
                        {
                            int vehicleCount = 0;
                            foreach (SecondaryProductDO product in products)
                            {
                                if (product.units != null)
                                {
                                    foreach (PDRUnitDO unit in product.units)
                                    {
                                        if (ReadString(unit.entityName).ToUpper() == "VEHICLE")
                                        {
                                            vehicleCount = vehicleCount + 1;
                                            matchFound = true;
                                            if (usesVehicleDetails == "true")
                                                listVeh.Add(string.Format("{0} {1} {2} ({3})", unit.unitYear.ToSafeString(), unit.makeT.ToSafeString(), unit.modelT.ToSafeString(), unit.identificationTxt.ToSafeString()));
                                        }
                                    }
                                }
                            }

                            PDRCoverageDO coverage = new PDRCoverageDO();
                            coverage.limitAmt1 = vehicleCount;

                            blcCoverage = BuildCoverage(coverage, coverageColumn, policy, lineOfBusiness, listVeh);
                            list.Add(blcCoverage);
                        }
                        else if (coverageColumn.ColumnValueType == ColumnValueType.Enum.Exposures)
                        {
                            double exposureAmount = 0;
                            foreach (SecondaryProductDO product in products)
                            {
                                if (product.units != null)
                                {
                                    foreach (PDRUnitDO unit in product.units)
                                    {
                                        if (unit.coverage != null)
                                        {
                                            foreach (PDRCoverageDO unitCoverage in unit.coverage)
                                            {
                                                if (coverageColumn.CoverageCodesAsStringArray.Contains(ReadString(unitCoverage.coverageCode)))
                                                {
                                                    exposureAmount = exposureAmount + unitCoverage.estExposureAmt;
                                                    matchFound = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            PDRCoverageDO coverage = new PDRCoverageDO();
                            coverage.limitAmt1 = exposureAmount;

                            blcCoverage = BuildCoverage(coverage, coverageColumn, policy, lineOfBusiness);
                            list.Add(blcCoverage);
                        }
                        else if (coverageColumn.ColumnValueType == ColumnValueType.Enum.Equipment & usesIMSchedules == "true")
                        {
                            decimal IMScheduleLimit = 0;
                            foreach (SecondaryProductDO product in products)
                            {
                                if (product.units != null)
                                {
                                    foreach (PDRUnitDO unit in product.units)
                                    {
                                        if (ReadString(unit.entityName).ToUpper() == "IM SCHEDULED ITEMS" && unit.contractorsEquipmentCoverageRows != null)
                                        {
                                            foreach (ContractorsEquipmentCoverageRow c in unit.contractorsEquipmentCoverageRows)
                                            {
                                                BlcIMSchedule schedule = new BlcIMSchedule();
                                                foreach (ContractorsEquipmentCoverageField cf in c.coverageFields)
                                                {
                                                    switch (ReadString(cf.field).ToUpper())
                                                    {
                                                        case "SEQ #":
                                                            schedule.Number = cf.value.ToSafeString();
                                                            break;
                                                        case "ENTER A DESCRIPTION OF THE COVERED PROPERTY":
                                                            schedule.Equipment = cf.value.ToSafeString();
                                                            break;
                                                        case "SCHEDULE ITEM YEAR":
                                                            schedule.Year = cf.value.ToSafeString();
                                                            break;
                                                        case "SCHEDULE ITEM SERIAL NUMBER":
                                                            schedule.SerialNumber = cf.value.ToSafeString();
                                                            break;
                                                        case "LIMIT":
                                                            schedule.Limit = cf.value.ToDecimal();
                                                            break;
                                                    }
                                                }

                                                IMScheduleLimit += schedule.Limit;
                                                listIMSchedule.Add(string.Format("{0} {1} {2} {3}", schedule.Year.ToSafeString(), schedule.Equipment.ToSafeString(), string.IsNullOrWhiteSpace(schedule.SerialNumber) ? string.Empty : string.Format("({0})", schedule.SerialNumber.ToSafeString()) , string.Format("{0:C}",schedule.Limit)));
                                            }
                                        }
                                    }
                                }
                            }
                            PDRCoverageDO coverage = new PDRCoverageDO();

                            coverage.limitAmt1 = (double)IMScheduleLimit;

                            //Create new coverage for Equipment only
                            //CompanyCoverageColumn newColumn = new CompanyCoverageColumn();
                            //newColumn = companyColumnList.Where(c => c.ColumnValueType == ColumnValueType.Enum.Equipment).FirstOrDefault();
                            blcCoverage = BuildCoverage(coverage, coverageColumn, policy, lineOfBusiness, listIMSchedule);
                            list.Add(blcCoverage);
                        }       
                        else
                        {                            
                            foreach (SecondaryProductDO product in products)
                            {
                                if (product.coverages != null)
                                {
                                    foreach (PDRCoverageDO parentCoverage in product.coverages)
                                    {
                                        if (coverageColumn.CoverageCodesAsStringArray.Contains(ReadString(parentCoverage.coverageCode)) &&
                                            ReadString(parentCoverage.limitAmt1) != string.Empty &&
                                            ReadString(parentCoverage.limitAmt1) != "0" &&
                                            matchFound == false)
                                        {
                                            blcCoverage = BuildCoverage(parentCoverage, coverageColumn, policy, lineOfBusiness);
                                            list.Add(blcCoverage);
                                            matchFound = true;
                                            break;
                                        }

                                        if (parentCoverage.coverage != null)
                                        {
                                            foreach (PDRCoverageDO childCoverage in parentCoverage.coverage)
                                            {
                                                if (coverageColumn.CoverageCodesAsStringArray.Contains(ReadString(childCoverage.coverageCode)) &&
                                                    ReadString(childCoverage.limitAmt1) != string.Empty &&
                                                    ReadString(childCoverage.limitAmt1) != "0" &&
                                                    matchFound == false)
                                                {
                                                    blcCoverage = BuildCoverage(childCoverage, coverageColumn, policy, lineOfBusiness);
                                                    list.Add(blcCoverage);
                                                    matchFound = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!matchFound && product.units != null)
                                {
                                    foreach (PDRUnitDO unit in product.units)
                                    {
                                        if (unit.coverage != null)
                                        {
                                            foreach (PDRCoverageDO parentCoverage in unit.coverage)
                                            {
                                                if (coverageColumn.CoverageCodesAsStringArray.Contains(ReadString(parentCoverage.coverageCode)) &&
                                                    ReadString(parentCoverage.limitAmt1) != string.Empty &&
                                                    ReadString(parentCoverage.limitAmt1) != "0" &&
                                                    matchFound == false)
                                                {
                                                    blcCoverage = BuildCoverage(parentCoverage, coverageColumn, policy, lineOfBusiness);
                                                    list.Add(blcCoverage);
                                                    matchFound = true;
                                                    break;
                                                }

                                                if (parentCoverage.coverage != null)
                                                {
                                                    foreach (PDRCoverageDO childCoverage in parentCoverage.coverage)
                                                    {
                                                        if (coverageColumn.CoverageCodesAsStringArray.Contains(ReadString(childCoverage.coverageCode)) &&
                                                            ReadString(childCoverage.limitAmt1) != string.Empty &&
                                                            ReadString(childCoverage.limitAmt1) != "0" &&
                                                            matchFound == false)
                                                        {
                                                            blcCoverage = BuildCoverage(childCoverage, coverageColumn, policy, lineOfBusiness);
                                                            list.Add(blcCoverage);
                                                            matchFound = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (blcCoverage == null) //Coverage not found in P*, add an empty coverage
                        {
                            list.Add(BuildCoverage(null, coverageColumn, policy, lineOfBusiness));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exception newEx = new Exception(string.Format(ex.Message + ". Policy {0}", policy.PolicyNumber), ex.InnerException);
                throw newEx;
            }

            return list.ToArray();
        }

        private BlcCoverage BuildCoverage(PDRCoverageDO coverage, CompanyCoverageColumn column, Policy policy, LineOfBusiness lineOfBusiness)
        {
            BlcCoverage blcCoverage = new BlcCoverage();
            blcCoverage.PolicyNumber = policy.PolicyNumber;
            blcCoverage.PolicyMod = (policy.PolicyMod != int.MinValue) ? policy.PolicyMod.ToString() : string.Empty;

            //class codes have multiple fields for a value
            if (column.ColumnValueType == ColumnValueType.Enum.ClassCodePayrollEmployees && coverage != null)
            {
                blcCoverage.Value = string.Format("{0} - {1}|{2}|{3}",
                    ReadString(coverage.classCodeCov),
                    ReadString(coverage.classCodeDesc),
                    ReadString(""),
                    ReadString(""));
            }
            else
            {
                blcCoverage.Value = (coverage != null) ? ReadString(coverage.limitAmt1) : string.Empty;
            }

            blcCoverage.LineOfBusinessAsString = lineOfBusiness.GetBlcName(column.LineOfBusiness);
            blcCoverage.CompanyAbbreviation = moImportBuilder.CompanyAbbreviation;

            // The name of the coverage i.e. General Liability
            blcCoverage.CoverageNameType = column.ColumnGroupTypeAsString;

            // A name given to the value itself i.e. Limits
            blcCoverage.CoverageValueType = column.ColumnValueTypeAsString;

            if (column.SubLineOfBusiness != LineOfBusiness.LineOfBusinessType.Unknown)
            {
                blcCoverage.SubLineOfBusinessAsString = lineOfBusiness.GetBlcName(column.SubLineOfBusiness);
            }

            return blcCoverage;
        }

        private BlcCoverage BuildCoverage(PDRCoverageDO coverage, CompanyCoverageColumn column, Policy policy, LineOfBusiness lineOfBusiness, List<string> details)
        {
            BlcCoverage blcCoverage = new BlcCoverage();
            blcCoverage.PolicyNumber = policy.PolicyNumber;
            blcCoverage.PolicyMod = (policy.PolicyMod != int.MinValue) ? policy.PolicyMod.ToString() : string.Empty;

            //class codes have multiple fields for a value
            if (column.ColumnValueType == ColumnValueType.Enum.ClassCodePayrollEmployees && coverage != null)
            {
                blcCoverage.Value = string.Format("{0} - {1}|{2}|{3}",
                    ReadString(coverage.classCodeCov),
                    ReadString(coverage.classCodeDesc),
                    ReadString(""),
                    ReadString(""));
            }
            else
            {
                blcCoverage.Value = (coverage != null) ? ReadString(coverage.limitAmt1) : string.Empty;
            }

            blcCoverage.LineOfBusinessAsString = lineOfBusiness.GetBlcName(column.LineOfBusiness);
            blcCoverage.CompanyAbbreviation = moImportBuilder.CompanyAbbreviation;

            // The name of the coverage i.e. General Liability
            blcCoverage.CoverageNameType = column.ColumnGroupTypeAsString;

            // A name given to the value itself i.e. Limits
            blcCoverage.CoverageValueType = column.ColumnValueTypeAsString;

            if (column.SubLineOfBusiness != LineOfBusiness.LineOfBusinessType.Unknown)
            {
                blcCoverage.SubLineOfBusinessAsString = lineOfBusiness.GetBlcName(column.SubLineOfBusiness);
            }

            //Coverage Details
            if (details != null && details.Count > 0)
            {
                List<CoverageDetail> lscd = new List<CoverageDetail>();
                foreach (string s in details)
                {
                    CoverageDetail cd = new CoverageDetail();
                    cd.CoverageDetailValue = s.ToString();

                    lscd.Add(cd);
                }

                blcCoverage.CoverageDetails = lscd;
            }

            return blcCoverage;
        }


        #region Read Routines

        private string ReadString(object obj)
        {
            return (obj != null) ? obj.ToString().Trim() : string.Empty;
        }

        /// <summary>
        /// Returns an value from the object specified.
        /// </summary>
        /// <param name="amount">The object from which to find the value.</param>
        /// <returns>The value.</returns>
        protected decimal ReadDecimal(object amount)
        {
            decimal result = decimal.MinValue;

            try
            {
                if (amount != null)
                    result = decimal.Parse(amount.ToString(), FormatProvider);
            }
            catch (ArgumentNullException)
            {
                // return decimal.MinValue
            }
            catch (FormatException)
            {
                // return decimal.MinValue
            }
            catch (OverflowException)
            {
                // return decimal.MinValue
            }

            return result;
        }
        /// <summary>
        /// Returns an value from the array, row, and column specified.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The value.</returns>
        protected int ReadInteger(string value)
        {
            int result = int.MinValue;

            if (IsMeaningful(value))
            {
                try
                {
                    result = int.Parse(value, FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // return decimal.MinValue
                }
                catch (FormatException)
                {
                    // return decimal.MinValue
                }
                catch (OverflowException)
                {
                    // return decimal.MinValue
                }
            }

            return result;
        }

        /// <summary>
        /// Returns an value from the array, row, and column specified.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The value.</returns>
        protected Int16 ReadShort(string value)
        {
            Int16 result = Int16.MinValue;

            if (IsMeaningful(value))
            {
                try
                {
                    result = Int16.Parse(value, FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // return Int16.MinValue;
                }
                catch (FormatException)
                {
                    // return Int16.MinValue;
                }
                catch (OverflowException)
                {
                    // return Int16.MinValue;
                }
            }

            return result;
        }

        /// <summary>
        /// Parses a date/time string from the input files and converts it 
        /// into a representative <see cref="DateTime"/> object.
        /// </summary>
        /// <param name="dateTime">The string to parse.</param>
        /// <returns>A DateTime representative of the dateTime.</returns>
        private DateTime ReadDateTime(string dateTime)
        {
            DateTime result = DateTime.MinValue;

            if (IsMeaningful(dateTime))
            {
                try
                {
                    result = Convert.ToDateTime(dateTime, FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // swallow the error and return DateTime.MinValue
                }
                catch (FormatException)
                {
                    // swallow the error and return DateTime.MinValue
                }
            }

            return result;
        }
        /// <summary>
        /// Parses a date string from the input files and converts it 
        /// into a representative <see cref="DateTime"/> object.
        /// </summary>
        /// <param name="date">The string to parse.</param>
        /// <returns>A DateTime representative of the date.</returns>
        private DateTime ReadDate(string date)
        {
            DateTime result = DateTime.MinValue;

            if (IsMeaningful(date))
            {
                try
                {
                    result = DateTime.ParseExact(date, "MM/dd/yyyy", FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // swallow the error and return DateTime.MinValue
                }
                catch (FormatException)
                {
                    // swallow the error and return DateTime.MinValue
                }
            }

            return result;
        }

        //private string ReadUnderwriter(string fullName)
        //{
        //    Underwriter underwriter = Underwriter.GetOne("Name LIKE ? && CompanyID = ?", "%" + fullName + "%", berkleyCompany.ID);

        //    ////try one more time with a broader wild card search in cases of mispellings
        //    //if (underwriter == null)
        //    //{

        //    //}

        //    return (underwriter != null) ? underwriter.Code : fullName;
        //}

        private string ReadStreetAddress(string streetNumber, string streetAddress)
        {
            string result = string.Empty;

            streetNumber = ReadString(streetNumber);
            streetAddress = ReadString(streetAddress);

            //check for a street number
            if (streetNumber.Length > 0)
            {
                result += streetNumber + " ";
            }

            //check for a street address
            if (streetAddress.Length > 0)
            {
                result += streetAddress;
            }

            return result;
        }

        public static string TrimZipCode(string strZipCode)
        {
            if (strZipCode.Length > 5)
            {
                for (int i = strZipCode.Length - 1; i >= strZipCode.Length - 4; i--)
                {
                    if (strZipCode[i] != '0')
                    {
                        return strZipCode;
                    }
                }
                return strZipCode.Substring(0, 5);
            }
            else
            {
                return strZipCode;
            }
        }

        private string ReadState(string strState)
        {
            strState = ReadString(strState);

            // Only allow valid state codes from the PDR
            //if (strState != string.Empty && State.GetOne("Code = ?", strState) != null) // -- bypassed on to get refresh project to build
            if (strState != string.Empty)
            {
                return strState;
            }
            else
            {
                return string.Empty;
            }
        }

        private string ReadRateModType(string rateModType)
        {
            if (rateModType == null)
                return string.Empty;

            // Only allow valid rate mod types
            if (rateModType.ToUpper().Contains("CNTRCT"))
            {
                //return RateModificationFactorType.Construction.Code;
                return "C";
            }
            else if (rateModType.ToUpper().Contains("EXPER"))
            {
                //return RateModificationFactorType.Experience.Code;
                return "E";
            }
            else
            {
                //return RateModificationFactorType.Unknown.Code;
                return "U";
            }
        }

        /// <summary>
        /// Determines whether or not a string has anything in it.
        /// </summary>
        /// <param name="text">The string to test.</param>
        /// <returns>True if the string has content, false otherwise.</returns>
        protected bool IsMeaningful(string text)
        {
            return text != null && text.Trim().Length > 0;
        }
        /// <summary>
        /// Determines whether or not a string has anything in it.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>True if the value has content, false otherwise.</returns>
        protected bool IsMeaningful(int value)
        {
            return value > int.MinValue && value < int.MaxValue;
        }
        /// <summary>
        /// Determines whether or not a decimal has anything in it.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>True if the value has content, false otherwise.</returns>
        protected bool IsMeaningful(decimal value)
        {
            return value > decimal.MinValue && value < decimal.MaxValue;
        }
        /// <summary>
        /// Determines whether or not a value has any meaningful content.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>True if the value has content, false otherwise.</returns>
        protected bool IsMeaningful(Array value)
        {
            return value != null && value.Length > 0;
        }
        /// <summary>
        /// Returns a format provider useful for string parsing.
        /// </summary>
        protected static IFormatProvider FormatProvider
        {
            get
            {
                return System.Globalization.CultureInfo.InvariantCulture;
            }
        }

        private int StripNonNumericChars(string number)
        {
            string result = string.Empty;

            char[] characters = number.ToCharArray();
            foreach (char character in characters)
            {
                if (Char.IsDigit(character))
                {
                    result += character;
                }
            }

            return !string.IsNullOrEmpty(result) ? Convert.ToInt32(result) : Int32.MinValue;
        }

        public virtual string ParsePolicySymbol(string pNumber)
        {
            return pNumber; 
        }
        #endregion
    }
}
