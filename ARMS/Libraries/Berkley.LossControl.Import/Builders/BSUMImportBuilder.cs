//using Berkley.BLC.Business;
//using Berkley.BLC.Core;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;
using Berkley.BLC.Import.Properties;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;

namespace Berkley.BLC.Import
{
    public class BSUMImportBuilder : BaseImportBuilder, IImportBuilder
    {
        private BlcStoredProcedure moStoredProcedure = null;
        private CompanyPolicyEntity moPolicyFieldEntity = new CompanyPolicyEntity();
        private CompanyInsuredEntity moInsuredFieldEntity = new CompanyInsuredEntity();
        private CompanyAgencyEntity moAgencyFieldEntity = new CompanyAgencyEntity();
        private CompanyLocationEntity moLocationFieldEntity = new CompanyLocationEntity();
        private CompanyBuildingEntity moBuildingFieldEntity = new CompanyBuildingEntity();
        private CompanyClaimEntity moCompanyClaimEntity = new CompanyClaimEntity();
        private CompanyRateModFactorEntity moCompanyRateModFactorEntity = new CompanyRateModFactorEntity();
        private BSUMLineOfBusiness moLineOfBusiness = new BSUMLineOfBusiness();

        private List<string> PolicySymbolExclusions = new List<string> { "CUA" };

        #region Line Of Business / Coverage
        private CompanyCoverageEntity moCoverageFieldEntity = new CompanyCoverageEntity();
        private BSUMCoverageDictionary moCoverageDictionary = new BSUMCoverageDictionary();
        #endregion

        public BSUMImportBuilder(string strClientId, string strPolicyNumber, string strAgentNumber)
        {
            base.Initialize(this, strClientId, strPolicyNumber, strAgentNumber);
            // Rearrange fields in moPolicyFieldEntity as necessary
        }

        #region IImportBuilder Members
        public BlcStoredProcedure GetStoredProcedure()
        {
            if (moStoredProcedure == null)
            {
                //string strConnection = CoreSettings.BSDConnectionString;
                string strConnection = ConfigurationManager.AppSettings["BSDConnectionString"];

                CompanySpEntityList oList = new CompanySpEntityList(strConnection, base.ClientId, base.PolicyNumber, base.AgentNumber);
                oList.Load(Settings.Default.BSUMImportFileList);
                moStoredProcedure = new BlcStoredProcedure(oList);
            }
            return moStoredProcedure;
        }

        public BlcStoredProcedure GetStoredProcedure(CompanySpEntity.SpType eType)
        {
            if (moStoredProcedure == null)
            {
                //string strConnection = CoreSettings.BSDConnectionString;
                string strConnection = ConfigurationManager.AppSettings["BSDConnectionString"];

                CompanySpEntityList oList = new CompanySpEntityList(strConnection, base.ClientId, base.PolicyNumber, base.AgentNumber);
                oList.Load(Settings.Default.BSUMImportFileList);
                moStoredProcedure = new BlcStoredProcedure(oList, eType);
            }
            return moStoredProcedure;
        }

        /// <summary>
        /// Trims two leading characters (company number) from the policy number
        /// </summary>
        /// <param name="strPolicyNumber">The policy number</param>
        private string TrimPolicyNumber(string strPolicyNumber)
        {
            if (strPolicyNumber.Length > 2)
            {
                string strReturn = strPolicyNumber.Substring(2, strPolicyNumber.Length - 2);
                return strReturn;
            }
            else
            {
                return strPolicyNumber;
            }
        }

        public BlcInsured BuildComplete(BlcInsured oBlcInsured)
        {
            // The BlcDataSet has been filled out.  Do the Insert into the BLC DB
            // If all of the company (derived) classes do the same thing, put the basic insert/update
            // in BaseImportBuilder
            Policy oPolicy;
            BSUMLineOfBusiness oLineOfBusiness = new BSUMLineOfBusiness();
            LineOfBusiness.LineOfBusinessType eType;

            // Exclude specified Policy Symbols from results
            // Need to record the policy numbers excluded for next removing them from the CoverageList
            List<Policy> excludedPolicies = new List<Policy>();

            for (int i = 0; i < PolicySymbolExclusions.Count; i++)
            {
                foreach (Policy p in oBlcInsured.PolicyList.ToList())
                {
                    if (p.PolicySymbol == PolicySymbolExclusions[i])
                    {
                        excludedPolicies.Add(p);
                        oBlcInsured.PolicyList.Remove(p);
                    }
                }
            }

            List<Policy> oPolicyList = oBlcInsured.PolicyList;

            // For each of the policies to be excluded
            // check the Locations, Buildings, and Coverage list and remove if present
            if (excludedPolicies.Count > 0)
            {
                foreach (Policy p in excludedPolicies)
                {
                    List<Location> locations = oBlcInsured.PolicyLocationList;
                    List<BlcCoverage> coverages = oBlcInsured.CoverageList;

                    var policyNumber = (p.PolicyNumber.Length > 7) ? p.PolicyNumber.Substring(p.PolicyNumber.Length - 7) : p.PolicyNumber;

                    //Locations
                    foreach (Location loc in locations.ToList())
                    {
                        var policySystemKey = loc.PolicySystemKey.Substring(loc.PolicySystemKey.Length - 7);
                        if (policySystemKey == policyNumber)
                        {
                            oBlcInsured.PolicyLocationList.Remove(loc);
                        }
                    }

                    //Coverages
                    foreach (BlcCoverage cov in coverages.ToList())
                    {
                        if (cov.PolicyNumber == policyNumber && cov.PolicyMod == p.PolicyMod.ToString())
                        {
                            oBlcInsured.CoverageList.Remove(cov);
                        }
                    }

                }
            }

            for (int i = 0; i < oPolicyList.Count; i++)
            {
                oPolicy = oPolicyList[i];
                eType = oLineOfBusiness.GetType(oPolicy.PolicySymbol);
                oPolicy.LineOfBusinessCode = oLineOfBusiness.GetBlcName(eType);
                oPolicy.PolicyNumber = TrimPolicyNumber(oPolicy.PolicyNumber);
                oPolicyList[i] = oPolicy;

                //the mapping is incorrect for BSUM, the profit center is coming in as the branch code
                oPolicy.ProfitCenterCode = oPolicy.BranchCode;
            }
            //Trim the policies for the rate mods
            for (int j = 0; j < oBlcInsured.RateModFactorList.Count; j++)
            {
                oBlcInsured.RateModFactorList[j].PolicyNumber = TrimPolicyNumber(oBlcInsured.RateModFactorList[j].PolicyNumber);
            }
            return oBlcInsured;
        }

        public CompanyCoverageEntity CoverageFieldEntity
        {
            get { return moCoverageFieldEntity; }
        }

        public List<CompanyCoverageColumn> GetCoverage(LineOfBusiness.LineOfBusinessType eType)
        {
            return moCoverageDictionary.GetColumnList(eType);
        }

        public int GetMaxCoverageColumns()
        {
            return moCoverageDictionary.GetColumnCount();
        }

        public int GetMaxColumnNumber()
        {
            return moCoverageDictionary.GetMaxColumnNumber();
        }

        // The derived classes can change the field mapping
        public CompanyPolicyEntity PolicyFieldEntity
        {
            get { return moPolicyFieldEntity; }
        }

        // The derived classes can change the field mapping
        public CompanyInsuredEntity InsuredFieldEntity
        {
            get { return moInsuredFieldEntity; }
        }

        public CompanyAgencyEntity AgencyFieldEntity
        {
            get { return moAgencyFieldEntity; }
        }

        public CompanyLocationEntity LocationFieldEntity
        {
            get { return moLocationFieldEntity; }
        }

        public CompanyBuildingEntity BuildingFieldEntity
        {
            get { return moBuildingFieldEntity; }
        }

        public LineOfBusiness LineOfBusinessFieldEntity
        {
            get { return moLineOfBusiness; }
        }

        public CompanyClaimEntity ClaimFieldEntity
        {
            get { return moCompanyClaimEntity; }
        }

        public CompanyRateModFactorEntity RateModFactorFieldEntity
        {
            get { return moCompanyRateModFactorEntity; }
        }

        public string CompanyAbbreviation
        {
            get { return "BSD"; }
        }
        #endregion
    }
}
