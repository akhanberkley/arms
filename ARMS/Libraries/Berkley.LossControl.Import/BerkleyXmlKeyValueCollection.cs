﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Xml;

namespace Berkley.BLC.Import
{
    public class BerkleyXmlKeyValueCollection
    {
        private NameValueCollection moKeyValues = new NameValueCollection();

        public BerkleyXmlKeyValueCollection(string strXmlNodeList)
        {
            Build(strXmlNodeList);
        }

        public NameValueCollection KeyValues
        {
            get { return moKeyValues; }
        }

        private void Build(string strXmlNodeList)
        {
            XmlDocument oDoc = new XmlDocument();
            oDoc.LoadXml("<root>" + strXmlNodeList + "</root>");

            XmlNodeList nodeList = oDoc.GetElementsByTagName("add");
            foreach (XmlNode node in nodeList)
            {
                if (node.Attributes["key"] == null || node.Attributes["value"] == null)
                {
                    throw new MissingFieldException("BerkleyXmlKeyValueCollection.Build", "key or value not present in Section " + strXmlNodeList);
                }
                moKeyValues.Add(node.Attributes["key"].Value, node.Attributes["value"].Value);
            }

        }
    }
}
