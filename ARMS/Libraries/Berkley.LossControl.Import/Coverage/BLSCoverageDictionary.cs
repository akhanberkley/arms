using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class BLSCoverageDictionary : CoverageDictionary
    {
        public BLSCoverageDictionary()
        {
        }

        #region Workers Comp
        protected override List<CompanyCoverageColumn> CreateWorkersComp()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.WcExposure],
                                                    LineOfBusiness.LineOfBusinessType.WorkersComp,
                                                    ColumnGroupType.Enum.Exposure,
                                                    ColumnValueType.Enum.ClassCodePayrollEmployees,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            return oColumns;
        }
        #endregion

        #region Commercial Auto
        protected override List<CompanyCoverageColumn> CreateCommercialAuto()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.PhysicalDamageLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.COMP }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaVehicleCount],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.NumberOfVehicles,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.NonOwnerAuto,
                                                    ColumnValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.NonOwnerAuto,
                                                    ColumnValueType.Enum.PhysicalDamageLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 4));

            return oColumns;
        }
        #endregion

        #region Commercial Umbrella
        protected override List<CompanyCoverageColumn> CreateCommercialUmbrella()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialUmbrella,
                                                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialUmbrella,
                                                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 1));

            return oColumns;
        }
        #endregion

        #region General Liablility
        protected override List<CompanyCoverageColumn> CreateGeneralLiability()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR, PolicyStarCoverageValueType.Enum.LIAB_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_AGG, PolicyStarCoverageValueType.Enum.LIAB_AGG }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.Exposures,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL1 }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.ProductsOperations,
                                                    ColumnValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROD, PolicyStarCoverageValueType.Enum.PROD_CPT }, 3));

            return oColumns;
        }
        #endregion

        #region Inland Marine
        protected override List<CompanyCoverageColumn> CreateInlandMarine()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.MotorTruckCargo,
                                                    ColumnValueType.Enum.LegalLiabilityLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CRGO_CAT }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.MotorTruckCargo,
                                                    ColumnValueType.Enum.VehicleLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROP_VEH }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.ConstructionLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.BLDRS_CAT }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.ExistingBuildingLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CSE_CONS }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilitySpoilageLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.WarehouseLiability,
                                                    ColumnValueType.Enum.SpoilageLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilityColdStorageLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.WarehouseLiability,
                                                    ColumnValueType.Enum.ColdStorageLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImJewelersBlockPremisesLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.JewelersBlock,
                                                    ColumnValueType.Enum.YourPremisesLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImComputerHardwareLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ComputerEquipment,
                                                    ColumnValueType.Enum.HardwareLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EDP_HDWR }, 7));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImTransitTransportationLimit],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.TransitCoverage,
                                                    ColumnValueType.Enum.TransportationLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TRNP }, 8));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImTransitTransportationScheduleLimit],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.TransitCoverage,
                                                    ColumnValueType.Enum.TransportationScheduleLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TRNP_SCHED }, 9));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImInstallFloaterInstallationCatostrophic],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.InstallationFloater,
                                                    ColumnValueType.Enum.InstallationCatostrophic,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.INST_CAT }, 10));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImInstallFloaterJobsiteSchedule],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.InstallationFloater,
                                                    ColumnValueType.Enum.JobsiteSchedule,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.INST_JSITE }, 11));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImInstallFloaterInTransit],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.InstallationFloater,
                                                    ColumnValueType.Enum.InTransit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TRNS_IN }, 12));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImInstallFloaterTemporaryLocations],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.InstallationFloater,
                                                    ColumnValueType.Enum.TemporaryLocations,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TEMP_STO }, 13));

            return oColumns;
        }
        #endregion

        #region Package

        protected override List<CompanyCoverageColumn> CreatePackages()
        {
            List<CompanyCoverageColumn> oPackageList = new List<CompanyCoverageColumn>();

            #region Commercial Auto

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB }, 0));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.PhysicalDamageLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.COMP }, 1));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaVehicleCount],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.NumberOfVehicles,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.NonOwnerAuto,
                                ColumnValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 3));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.NonOwnerAuto,
                                ColumnValueType.Enum.PhysicalDamageLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 4));

            #endregion

            #region Inland Marine

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.BuildersRisk,
                    ColumnValueType.Enum.ConstructionLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.BuildersRisk,
                    ColumnValueType.Enum.ExistingBuildingLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImComputerHardwareLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ComputerEquipment,
                    ColumnValueType.Enum.HardwareLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EDP_HDWR }, 7));

            #endregion

            #region Commercial Umbrella
            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.CommercialUmbrella,
                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                    ColumnValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 8));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CuAggregateLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.CommercialUmbrella,
                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                    ColumnValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 9));
            #endregion

            #region General Liability
            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR, PolicyStarCoverageValueType.Enum.LIAB_OCCR }, 10));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_AGG, PolicyStarCoverageValueType.Enum.LIAB_AGG }, 11));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.Exposures,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL1 }, 12));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.ProductsOperationsLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROD, PolicyStarCoverageValueType.Enum.PROD_CPT }, 13));

            #endregion

            #region Property

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    ColumnGroupType.Enum.BusinessIncome,
                    ColumnValueType.Enum.Limits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 14));

            #endregion

            return oPackageList;
        }

        #endregion

        public override int GetMaxColumnNumber()
        {
            //largest column number plus one
            return 87;
        }

        protected override void BuildCoverageMap()
        {
            // Commercial Umbrella
            moCoverageDictionary[CoverageMap.CuOccurrenceLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.CuAggregateLimits] = int.MinValue;

            // Workers Comp
            moCoverageDictionary[CoverageMap.WcExposure] = int.MinValue;

            // Commercial Auto
            moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit] = int.MinValue;
            moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit] = int.MinValue;
            moCoverageDictionary[CoverageMap.CaVehicleCount] = int.MinValue;
            moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit] = int.MinValue;
            moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit] = int.MinValue;
            moCoverageDictionary[CoverageMap.CaNonOwnerAutoLiability] = int.MinValue;

            // Commercial Crime
            moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits] = int.MinValue;

            // General Liability
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures] = int.MinValue;
            moCoverageDictionary[CoverageMap.GlProductsOperationsLimits] = int.MinValue;

            // Commercial Property
            moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits] = int.MinValue;

            // Inland Marine
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilitySpoilageLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilityColdStorageLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImJewelersBlockPremisesLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImComputerHardwareLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImTransitTransportationLimit] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImTransitTransportationScheduleLimit] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImInstallFloaterInstallationCatostrophic] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImInstallFloaterJobsiteSchedule] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImInstallFloaterInTransit] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImInstallFloaterTemporaryLocations] = int.MinValue;
        }
    }
}
