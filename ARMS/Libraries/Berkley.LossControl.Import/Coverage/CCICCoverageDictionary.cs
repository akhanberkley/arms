using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class CCICCoverageDictionary : CoverageDictionary
    {
        public CCICCoverageDictionary()
        {
        }

        #region Commercial Auto
        protected override List<CompanyCoverageColumn> CreateCommercialAuto()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.PhysicalDamageLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.COMP }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonownedHiredAutoLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.NonownedHiredAutoLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaGaragekeepersLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.Garagekeeper,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaTractors],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.Tractors,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaTrailers],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.Trailers,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaServiceUnits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.ServiceUnits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaCommercialUnits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.CommercialUnits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 7));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaActivePlates],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.ActivePlates,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 8));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaInactivePlates],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.InactivePlates,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 9));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaPPTsExHeavy],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.PPTsExHeavy,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 10));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaPPTsHeavy],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.PPTsHeavy,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 11));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaPPTsMedium],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.PPTsMedium,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 12));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaPPTsLight],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.PPTsLight,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 13));


            return oColumns;
        }
        #endregion

        #region General Liablility
        protected override List<CompanyCoverageColumn> CreateGeneralLiability()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR, PolicyStarCoverageValueType.Enum.LIAB_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_AGG, PolicyStarCoverageValueType.Enum.LIAB_AGG }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.Exposures,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL1 }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.ProductsOperationsLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROD, PolicyStarCoverageValueType.Enum.PROD_CPT }, 3));

            return oColumns;
        }
        #endregion

        #region Inland Marine
        protected override List<CompanyCoverageColumn> CreateInlandMarine()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.MotorTruckCargo,
                                                    ColumnValueType.Enum.LegalLiabilityLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CRGO_CAT }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.MotorTruckCargo,
                                                    ColumnValueType.Enum.VehicleLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROP_VEH }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoTerminalCoverage],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.MotorTruckCargo,
                                                    ColumnValueType.Enum.TerminalCoverage,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoTerminalLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.MotorTruckCargo,
                                                    ColumnValueType.Enum.TerminalLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                                                    ColumnValueType.Enum.ToOthers,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TO_OTH }, 4));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                                                    ColumnValueType.Enum.FromOthers,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.FROM_OTH }, 5));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImTransitCoverageHiredCarrier],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.TransitCoverage,
                                                    ColumnValueType.Enum.HiredCarrier,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImTransitCoverageOwnedVehicle],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.TransitCoverage,
                                                    ColumnValueType.Enum.OwnedVehicle,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 7));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImToolFloaterLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ToolFloater,
                                                    ColumnValueType.Enum.YourTools,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NEW_EQUIP }, 8));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ToolFloater,
                                                    ColumnValueType.Enum.EmployeeTools,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EMP_TOOL }, 9));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImEDPEquipmentHardwareLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.EDPEquipment,
                                                    ColumnValueType.Enum.HardwareLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 10));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImEDPEquipmentSoftwareLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.EDPEquipment,
                                                    ColumnValueType.Enum.SoftwareMedia,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 11));

            return oColumns;
        }
        #endregion

        public override int GetMaxColumnNumber()
        {
            //largest column number plus one
            return 66;
        }

        protected override void BuildCoverageMap()
        {

            // Commercial Auto
            moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit] = 13;
            moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit] = 14;
            moCoverageDictionary[CoverageMap.CaNonownedHiredAutoLimits] = 15;
            moCoverageDictionary[CoverageMap.CaGaragekeepersLimits] = 16;
            moCoverageDictionary[CoverageMap.CaTractors] = 17;
            moCoverageDictionary[CoverageMap.CaTrailers] = 18;
            moCoverageDictionary[CoverageMap.CaServiceUnits] = 19;
            moCoverageDictionary[CoverageMap.CaCommercialUnits] = 20;
            moCoverageDictionary[CoverageMap.CaActivePlates] = 21;
            moCoverageDictionary[CoverageMap.CaInactivePlates] = 22;
            moCoverageDictionary[CoverageMap.CaPPTsExHeavy] = 23;
            moCoverageDictionary[CoverageMap.CaPPTsHeavy] = 24;
            moCoverageDictionary[CoverageMap.CaPPTsMedium] = 25;
            moCoverageDictionary[CoverageMap.CaPPTsLight] = 26;

            // General Liability
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits] = 27;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits] = 28;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures] = 29;
            moCoverageDictionary[CoverageMap.GlProductsOperationsLimits] = 30;

            // Inland Marine
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits] = 31;
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits] = 32;
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoTerminalCoverage] = 33;
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoTerminalLimits] = 34;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits] = 35;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits] = 36;
            moCoverageDictionary[CoverageMap.ImTransitCoverageHiredCarrier] = 37;
            moCoverageDictionary[CoverageMap.ImTransitCoverageOwnedVehicle] = 38;
            moCoverageDictionary[CoverageMap.ImToolFloaterLimits] = 39;
            moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits] = 40;
            moCoverageDictionary[CoverageMap.ImEDPEquipmentHardwareLimits] = 41;
            moCoverageDictionary[CoverageMap.ImEDPEquipmentSoftwareLimits] = 42;
        }

    }
}
