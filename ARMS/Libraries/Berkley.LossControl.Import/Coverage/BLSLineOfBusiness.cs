using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class BLSLineOfBusiness : LineOfBusiness
    {
        public BLSLineOfBusiness()
        {
        }

        protected override void DerivedBuildMap(ref Dictionary<int, string> oDictionary)
        {
            // Pipe delimit multiple symbols for a LOB
            oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(BlcCompany.BerkleyLifeSciences, BlcLineOfBusiness.CommercialAuto));
            oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(BlcCompany.BerkleyLifeSciences, BlcLineOfBusiness.GeneralLiability));
            oDictionary.Add((int)LineOfBusinessType.CommercialUmbrella, GetCompanyPolicySymbols(BlcCompany.BerkleyLifeSciences, BlcLineOfBusiness.CommercialUmbrella));
            oDictionary.Add((int)LineOfBusinessType.InlandMarine, GetCompanyPolicySymbols(BlcCompany.BerkleyLifeSciences, BlcLineOfBusiness.InlandMarine));
            oDictionary.Add((int)LineOfBusinessType.Package, GetCompanyPolicySymbols(BlcCompany.BerkleyLifeSciences, BlcLineOfBusiness.Package));
            oDictionary.Add((int)LineOfBusinessType.WorkersComp, GetCompanyPolicySymbols(BlcCompany.BerkleyLifeSciences, BlcLineOfBusiness.WorkersCompensation));

            moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, BlcLineOfBusiness.CommercialAuto.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, BlcLineOfBusiness.GeneralLiability.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialUmbrella, BlcLineOfBusiness.CommercialUmbrella.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, BlcLineOfBusiness.InlandMarine.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.Package, BlcLineOfBusiness.Package.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.WorkersComp, BlcLineOfBusiness.WorkersCompensation.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialProperty, BlcLineOfBusiness.CommercialProperty.LineOfBusinessCode);
        }

    }
}
