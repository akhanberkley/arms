using System.Collections.Generic;

namespace Berkley.BLC.Import
{
    public class BSUMCoverageDictionary : CoverageDictionary
    {
        public BSUMCoverageDictionary()
        {
        }

        #region Business Owner
        protected override List<CompanyCoverageColumn> CreateBusinessOwners()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            #region Business Owner

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.BoGeneralLiabilityOccurrenceLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.GeneralLiability,
                                                        ColumnValueType.Enum.OccuranceLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.BoGeneralLiabilityAggregateLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.GeneralLiability,
                                                        ColumnValueType.Enum.AggregateLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB_AGG }, 1));
            #endregion

            #region Commercial Auto

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                        ColumnGroupType.Enum.CommercialAuto,
                                                        ColumnValueType.Enum.BodilyInjuryLimit,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaVehicleCount],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                        ColumnGroupType.Enum.CommercialAuto,
                                                        ColumnValueType.Enum.NumberOfVehicles,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));

            #endregion


            return oColumns;
        }
        #endregion

        #region Workers Comp
        protected override List<CompanyCoverageColumn> CreateWorkersComp()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.WcExposure],
                                                    LineOfBusiness.LineOfBusinessType.WorkersComp,
                                                    ColumnGroupType.Enum.Exposure,
                                                    ColumnValueType.Enum.ClassCodePayrollEmployees,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            return oColumns;
        }
        #endregion

        #region Commercial Auto
        protected override List<CompanyCoverageColumn> CreateCommercialAuto()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaVehicleCount],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.NumberOfVehicles,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.NonOwnerAuto,
                                                    ColumnValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.NonOwnerAuto,
                                                    ColumnValueType.Enum.PhysicalDamageLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 3));

            return oColumns;
        }
        #endregion

        #region Commercial Umbrella
        /*
        protected override List<CompanyCoverageColumn> CreateCommercialUmbrella()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialUmbrella,
                                                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialUmbrella,
                                                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 1));

            return oColumns;
        }
        */
        #endregion

        #region Garage
        protected override List<CompanyCoverageColumn> CreateGarage()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaGaragekeepersLimits],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    ColumnGroupType.Enum.Garagekeeper,
                                                    ColumnValueType.Enum.Garagekeeper,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            return oColumns;
        }
        #endregion

        #region General Liablility
        protected override List<CompanyCoverageColumn> CreateGeneralLiability()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_AGG }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.Exposures,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.ProductsOperations,
                                                    ColumnValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROD }, 3));

            return oColumns;
        }
        #endregion

        #region Commercial Property
        protected override List<CompanyCoverageColumn> CreateCommercialProperty()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            return oColumns;
        }
        #endregion

        #region Inland Marine
        protected override List<CompanyCoverageColumn> CreateInlandMarine()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.ConstructionLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CSE_CONS }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.ExistingBuildingLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.JSITE_SCHD }, 1));

            return oColumns;
        }
        #endregion

        #region Package

        protected override List<CompanyCoverageColumn> CreatePackages()
        {
            List<CompanyCoverageColumn> oPackageList = new List<CompanyCoverageColumn>();

            #region Commercial Auto

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaVehicleCount],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.NumberOfVehicles,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.NonOwnerAuto,
                                ColumnValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.NonOwnerAuto,
                                ColumnValueType.Enum.PhysicalDamageLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));

            #endregion

            #region General Liability
            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.Exposures,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.ProductsOperations,
                    ColumnValueType.Enum.Limits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 7));

            #endregion

            #region Commercial Property

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    ColumnGroupType.Enum.BusinessIncome,
                    ColumnValueType.Enum.Limits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 23));

            #endregion


            return oPackageList;
        }

        #endregion

        public override int GetMaxColumnNumber()
        {
            //largest column number plus one
            return 88;
        }

        protected override void BuildCoverageMap()
        {
            // Business Owner
            moCoverageDictionary[CoverageMap.BoGeneralLiabilityOccurrenceLimits] = 4;
            moCoverageDictionary[CoverageMap.BoGeneralLiabilityAggregateLimits] = 5;
            moCoverageDictionary[CoverageMap.BoBusinessIncomeLimits] = 6;
            moCoverageDictionary[CoverageMap.BoBusinessIncomeFromDependentProperties] = 7;

            // Commercial Output
            moCoverageDictionary[CoverageMap.CoBuildingLimit] = 9;
            moCoverageDictionary[CoverageMap.CoBuildingCatastropheLimit] = 10;
            moCoverageDictionary[CoverageMap.CoPersonalPropertyLimit] = 11;

            // Commercial Umbrella
            //moCoverageDictionary[CoverageMap.CuOccurrenceLimits] = 39;
            //moCoverageDictionary[CoverageMap.CuAggregateLimits] = 40;

            // Workers Comp
            moCoverageDictionary[CoverageMap.WcExposure] = 12;

            // Commercial Auto
            moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit] = 13;
            moCoverageDictionary[CoverageMap.CaVehicleCount] = 15;
            moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit] = 16;
            moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit] = 17;
            moCoverageDictionary[CoverageMap.CaNonOwnerAutoLiability] = 8;
            moCoverageDictionary[CoverageMap.CaGaragekeepersLimits] = 14;

            // Commercial Crime
            moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits] = 41;
            moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits] = 42;
            moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits] = 44;
            moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits] = 45;

            // Garage
            moCoverageDictionary[CoverageMap.GaComprehensiveDeductable] = 68;
            moCoverageDictionary[CoverageMap.GaComprehensiveLimits] = 73;
            moCoverageDictionary[CoverageMap.GaSpecialCausesOfLossDeductable] = 69;
            moCoverageDictionary[CoverageMap.GaSpecialCausesOfLossLimits] = 72;
            moCoverageDictionary[CoverageMap.GaCollisionDeductable] = 70;
            moCoverageDictionary[CoverageMap.GaCollisionLimits] = 71;
            moCoverageDictionary[CoverageMap.GaGaragekeepersLimits] = 14;

            moCoverageDictionary[CoverageMap.GaDealerClassOfOpRegularOPFurnishedAutos] = 56;
            moCoverageDictionary[CoverageMap.GaDealerClassOfOpRegularOPNonFurnishedAutos] = 57;
            moCoverageDictionary[CoverageMap.GaDealerClassOfOpEmployeesNonRegularOp] = 58;
            moCoverageDictionary[CoverageMap.GaDealerClassOfOpNonEmployeesUnder25] = 59;
            moCoverageDictionary[CoverageMap.GaDealerClassOfOpNonEmployeesOver25] = 60;
            moCoverageDictionary[CoverageMap.GaDealerPhysicalDamageComprehensiveLimits] = 61;
            moCoverageDictionary[CoverageMap.GaDealerPhysicalDamageCollisionLimits] = 62;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionVehicleCount] = 63;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionTripCount] = 64;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionPriceNew] = 65;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionMileage] = 66;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionDeductable] = 67;

            // General Liability
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits] = 21;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits] = 22;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures] = 23;
            moCoverageDictionary[CoverageMap.GlProductsOperationsLimits] = 24;
            moCoverageDictionary[CoverageMap.GlFRPLiabilityLimits] = 87;

            // Commercial Property
            moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits] = 43;

            // Inland Marine
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits] = 25;
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits] = 26;
            moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits] = 27;
            moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits] = 28;
            moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilitySpoilageLimits] = 29;
            moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilityColdStorageLimits] = 30;
            moCoverageDictionary[CoverageMap.ImJewelersBlockPremisesLimits] = 31;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment] = 46;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits] = 32;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits] = 33;
            moCoverageDictionary[CoverageMap.ImToolFloaterLimits] = 34;
            moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits] = 35;
            moCoverageDictionary[CoverageMap.ImComputerHardwareLimits] = 36;
            moCoverageDictionary[CoverageMap.ImComputerSoftwareMediaLimits] = 86;
            moCoverageDictionary[CoverageMap.ImSignsScheduledLimits] = 37;
            moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits] = 38;
            moCoverageDictionary[CoverageMap.ImAccountsReceivableInsuredPremises1] = 74;
            moCoverageDictionary[CoverageMap.ImAccountsReceivableInsuredPremises2] = 75;
            moCoverageDictionary[CoverageMap.ImValuablePapersProperty1] = 76;
            moCoverageDictionary[CoverageMap.ImValuablePapersProperty2] = 77;
            moCoverageDictionary[CoverageMap.ImValuablePapersProperty3] = 78;
            moCoverageDictionary[CoverageMap.ImValuablePapersAllOtherCoveredProperty] = 79;
            moCoverageDictionary[CoverageMap.ImFineArtsFloaterOnPremises] = 80;
            moCoverageDictionary[CoverageMap.ImFineArtsFloaterTransportation] = 81;
            moCoverageDictionary[CoverageMap.ImBaileesCoveragePremises] = 82;
            moCoverageDictionary[CoverageMap.ImBaileesCoverageTransit] = 83;
            moCoverageDictionary[CoverageMap.ImTransitCoverageHiredCarrier] = 84;
            moCoverageDictionary[CoverageMap.ImTransitCoverageOwnedVehicle] = 85;
            moCoverageDictionary[CoverageMap.ImMobileEquipmentLimits] = 43;

            // Ocean Marine
            moCoverageDictionary[CoverageMap.OmPhysicalDamageVesselLimits] = 47;
            moCoverageDictionary[CoverageMap.OmPhysicalDamageExcessCollisionLimits] = 48;
            moCoverageDictionary[CoverageMap.OmPhysicalDamageBreachOfWarrantyLimits] = 49;
            moCoverageDictionary[CoverageMap.OmLegalLiabilityLimits] = 50;
            moCoverageDictionary[CoverageMap.OmLegalLiabilityMedicalPaymentsLimits] = 51;
            moCoverageDictionary[CoverageMap.OmBoatDealersCoverageDLimits] = 52;
            moCoverageDictionary[CoverageMap.OmBoatDealersCoverageALimits] = 55;
            moCoverageDictionary[CoverageMap.OmBuildersRiskAmountInsured] = 53;
            moCoverageDictionary[CoverageMap.OmBuildersRiskAgreedValue] = 54;
        }
    }
}
