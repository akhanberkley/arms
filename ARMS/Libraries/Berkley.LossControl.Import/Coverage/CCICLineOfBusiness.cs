using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class CCICLineOfBusiness : LineOfBusiness
    {
        public CCICLineOfBusiness()
        { 
        }

        protected override void DerivedBuildMap(ref Dictionary<int, string> oDictionary)
        {
            oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(BlcCompany.CarolinaCasualtyInsuranceGroup, BlcLineOfBusiness.CommercialAuto));
            oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(BlcCompany.CarolinaCasualtyInsuranceGroup, BlcLineOfBusiness.GeneralLiability));
            //oDictionary.Add((int)LineOfBusinessType.InlandMarine, "ARP");

            moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, BlcLineOfBusiness.CommercialAuto.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, BlcLineOfBusiness.GeneralLiability.LineOfBusinessCode);
            //moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, "IM");
        }
    }
}
