using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class AICLineOfBusiness : LineOfBusiness
    {
        public AICLineOfBusiness()
        {
        }

        protected override void DerivedBuildMap(ref Dictionary<int, string> oDictionary)
        {
            // Pipe delimit multiple symbols for a LOB
            oDictionary.Add((int)LineOfBusinessType.BusinessOwner, GetCompanyPolicySymbols(BlcCompany.AcadiaInsuranceCompany, BlcLineOfBusiness.BusinessOwner));
            oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(BlcCompany.AcadiaInsuranceCompany, BlcLineOfBusiness.CommercialAuto));
            oDictionary.Add((int)LineOfBusinessType.CommercialCrime, GetCompanyPolicySymbols(BlcCompany.AcadiaInsuranceCompany, BlcLineOfBusiness.CommercialCrime));
            oDictionary.Add((int)LineOfBusinessType.CommercialProperty, GetCompanyPolicySymbols(BlcCompany.AcadiaInsuranceCompany, BlcLineOfBusiness.CommercialProperty));
            oDictionary.Add((int)LineOfBusinessType.Garage, GetCompanyPolicySymbols(BlcCompany.AcadiaInsuranceCompany, BlcLineOfBusiness.Garage));
            oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(BlcCompany.AcadiaInsuranceCompany, BlcLineOfBusiness.GeneralLiability));
            oDictionary.Add((int)LineOfBusinessType.CommercialUmbrella, GetCompanyPolicySymbols(BlcCompany.AcadiaInsuranceCompany, BlcLineOfBusiness.CommercialUmbrella));
            oDictionary.Add((int)LineOfBusinessType.InlandMarine, GetCompanyPolicySymbols(BlcCompany.AcadiaInsuranceCompany, BlcLineOfBusiness.InlandMarine));
            oDictionary.Add((int)LineOfBusinessType.OceanMarine, GetCompanyPolicySymbols(BlcCompany.AcadiaInsuranceCompany, BlcLineOfBusiness.OceanMarine));
            oDictionary.Add((int)LineOfBusinessType.Package, GetCompanyPolicySymbols(BlcCompany.AcadiaInsuranceCompany, BlcLineOfBusiness.Package));
            oDictionary.Add((int)LineOfBusinessType.WorkersComp, GetCompanyPolicySymbols(BlcCompany.AcadiaInsuranceCompany, BlcLineOfBusiness.WorkersCompensation));

            moBlcLobDictionary.Add(LineOfBusinessType.BusinessOwner, BlcLineOfBusiness.BusinessOwner.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, BlcLineOfBusiness.CommercialAuto.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialCrime, BlcLineOfBusiness.CommercialCrime.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialProperty, BlcLineOfBusiness.CommercialProperty.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialUmbrella, BlcLineOfBusiness.CommercialUmbrella.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.Dealer, BlcLineOfBusiness.Dealer.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.Garage, BlcLineOfBusiness.Garage.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, BlcLineOfBusiness.GeneralLiability.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, BlcLineOfBusiness.InlandMarine.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.OceanMarine, BlcLineOfBusiness.OceanMarine.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.Package, BlcLineOfBusiness.Package.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.WorkersComp, BlcLineOfBusiness.WorkersCompensation.LineOfBusinessCode);
        }

    }
}
