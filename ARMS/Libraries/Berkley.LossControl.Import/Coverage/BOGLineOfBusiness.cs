using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class BOGLineOfBusiness : LineOfBusiness
    {
        public BOGLineOfBusiness()
        {
        }

        protected override void DerivedBuildMap(ref Dictionary<int, string> oDictionary)
        {
            // Pipe delimit multiple symbols for a LOB
            oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(BlcCompany.BerkleyOilGas, BlcLineOfBusiness.CommercialAuto));
            oDictionary.Add((int)LineOfBusinessType.CommercialProperty, GetCompanyPolicySymbols(BlcCompany.BerkleyOilGas, BlcLineOfBusiness.CommercialProperty));
            oDictionary.Add((int)LineOfBusinessType.CommercialUmbrella, GetCompanyPolicySymbols(BlcCompany.BerkleyOilGas, BlcLineOfBusiness.CommercialUmbrella));
            oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(BlcCompany.BerkleyOilGas, BlcLineOfBusiness.GeneralLiability));
            oDictionary.Add((int)LineOfBusinessType.InlandMarine, GetCompanyPolicySymbols(BlcCompany.BerkleyOilGas, BlcLineOfBusiness.InlandMarine));
            oDictionary.Add((int)LineOfBusinessType.WorkersComp, GetCompanyPolicySymbols(BlcCompany.BerkleyOilGas, BlcLineOfBusiness.WorkersCompensation));
            oDictionary.Add((int)LineOfBusinessType.CommercialCrime, GetCompanyPolicySymbols(BlcCompany.BerkleyOilGas, BlcLineOfBusiness.CommercialCrime));

            moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, BlcLineOfBusiness.CommercialAuto.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialCrime, BlcLineOfBusiness.CommercialCrime.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialProperty, BlcLineOfBusiness.CommercialProperty.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialUmbrella, BlcLineOfBusiness.CommercialUmbrella.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, BlcLineOfBusiness.GeneralLiability.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, BlcLineOfBusiness.InlandMarine.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.WorkersComp, BlcLineOfBusiness.WorkersCompensation.LineOfBusinessCode);
        }

    }
}
