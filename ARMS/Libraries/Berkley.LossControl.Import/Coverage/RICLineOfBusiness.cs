﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class RICLineOfBusiness : LineOfBusiness
    {
        public RICLineOfBusiness()
        {
        }

        protected override void DerivedBuildMap(ref Dictionary<int, string> oDictionary)
        {
            // Pipe delimit multiple symbols for a LOB
            oDictionary.Add((int)LineOfBusinessType.BusinessOwner, GetCompanyPolicySymbols(BlcCompany.RiverportInsuranceCompany, BlcLineOfBusiness.BusinessOwner));
            oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(BlcCompany.RiverportInsuranceCompany, BlcLineOfBusiness.CommercialAuto));
            oDictionary.Add((int)LineOfBusinessType.CommercialProperty, GetCompanyPolicySymbols(BlcCompany.RiverportInsuranceCompany, BlcLineOfBusiness.CommercialProperty));
            oDictionary.Add((int)LineOfBusinessType.CommercialUmbrella, GetCompanyPolicySymbols(BlcCompany.RiverportInsuranceCompany, BlcLineOfBusiness.CommercialUmbrella));
            oDictionary.Add((int)LineOfBusinessType.Garage, GetCompanyPolicySymbols(BlcCompany.RiverportInsuranceCompany, BlcLineOfBusiness.Garage));
            oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(BlcCompany.RiverportInsuranceCompany, BlcLineOfBusiness.GeneralLiability));
            oDictionary.Add((int)LineOfBusinessType.InlandMarine, GetCompanyPolicySymbols(BlcCompany.RiverportInsuranceCompany, BlcLineOfBusiness.InlandMarine));
            oDictionary.Add((int)LineOfBusinessType.Package, GetCompanyPolicySymbols(BlcCompany.RiverportInsuranceCompany, BlcLineOfBusiness.Package));
            oDictionary.Add((int)LineOfBusinessType.WorkersComp, GetCompanyPolicySymbols(BlcCompany.RiverportInsuranceCompany, BlcLineOfBusiness.WorkersCompensation));

            moBlcLobDictionary.Add(LineOfBusinessType.BusinessOwner, BlcLineOfBusiness.BusinessOwner.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, BlcLineOfBusiness.CommercialAuto.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialCrime, BlcLineOfBusiness.CommercialCrime.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialProperty, BlcLineOfBusiness.CommercialProperty.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialUmbrella, BlcLineOfBusiness.CommercialUmbrella.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.Dealer, BlcLineOfBusiness.Dealer.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.Garage, BlcLineOfBusiness.Garage.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, BlcLineOfBusiness.GeneralLiability.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, BlcLineOfBusiness.InlandMarine.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.OceanMarine, BlcLineOfBusiness.OceanMarine.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.Package, BlcLineOfBusiness.Package.LineOfBusinessCode);
            moBlcLobDictionary.Add(LineOfBusinessType.WorkersComp, BlcLineOfBusiness.WorkersCompensation.LineOfBusinessCode);
            
        }

    }
}
