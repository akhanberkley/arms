using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class CompanyAgencyEntity : BaseCompanyEntity
	{
		#region Enum
		/// <summary>
		/// Translates the meaning of each column in the policy system's policy
		/// file.
		/// </summary>
		public enum FieldColumns
		{
			Unknown = -1,
			/// <summary>
			/// The agency number.
			/// </summary>
			Number = 0,
			/// <summary>
			/// The agency's name.
			/// </summary>
			Name,
			/// <summary>
			/// The agency's street address.
			/// </summary>
			StreetAddress,
			/// <summary>
			/// The agency's street address.
			/// </summary>
			StreetAddress2,
			/// <summary>
			/// The agency's city.
			/// </summary>
			City,
			/// <summary>
			/// The agency's state.
			/// </summary>
			State,
			/// <summary>
			/// The agency's postal code.
			/// </summary>
			PostalCode,
			/// <summary>
			/// The agency's telephone number.
			/// </summary>
			TelephoneNumber,
			/// <summary>
			/// The agency's facsimile number.
			/// </summary>
			FacsimileNumber
		}
		#endregion

		public CompanyAgencyEntity()
		{
		}

		protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
		{
			oColumnMapDictionary.Add((int)FieldColumns.Number, 0);
			oColumnMapDictionary.Add((int)FieldColumns.Name, 1);
			oColumnMapDictionary.Add((int)FieldColumns.StreetAddress, 2);
			oColumnMapDictionary.Add((int)FieldColumns.StreetAddress2, 3);
			oColumnMapDictionary.Add((int)FieldColumns.City, 4);
			oColumnMapDictionary.Add((int)FieldColumns.State, 5);
			oColumnMapDictionary.Add((int)FieldColumns.PostalCode, 6);
			oColumnMapDictionary.Add((int)FieldColumns.TelephoneNumber, 7);
			oColumnMapDictionary.Add((int)FieldColumns.FacsimileNumber, 8);
		}
	}
}
