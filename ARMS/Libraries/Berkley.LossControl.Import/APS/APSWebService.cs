using APSProxy = Berkley.BLC.Import.APSProxy;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;
//using Berkley.BLC.Import;
//using Berkley.BLC.Core;
using System;
using System.Text;
using QCI.ExceptionManagement;

namespace Berkley.BLC.Import
{
    public class APSWebService
    {
        APSProxy.APSService service;

        public APSWebService(string endPointUrl)
        {
            service = new APSProxy.APSService();
            service.Timeout = 10000000;

            // set the URL based on the environment and company
            service.Url = endPointUrl;
        }

        /// <summary>
        /// Returns an APS agency object for the specified agency number.
        /// </summary>
        /// <param name="policyNumber">The agency number or code.</param>
        /// <returns>An APS agency object.</returns>
        public APSProxy.agency GetAgency(string agencyNumber)
        {
            try
            {
                return service.getAgencyByCode(agencyNumber);
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(new APSWebServiceException(string.Format("An exception occurred communicating with the APS service: Method=getAgencyByCode, AgencyNumber={0}.", agencyNumber), ex));
                throw new BlcNoInsuredException("Unable to import the details from APS for the agency of this account");
            }
        }

        /// <summary>
        /// Returns an APS agency object for the specified agency number.
        /// </summary>
        /// <param name="policyNumber">The agency number or code.</param>
        /// <returns>An APS agency object.</returns>
        public APSProxy.personnelAssigned[] GetAssignedUnderwriter(string agencyNumber, string underwriterRoleCode, string underwritingUnitCode, bool isPrimary)
        {
            try
            {
                return service.getPersonnelAssignmentsByAgencyCodeUWCodeRoleCodePrimary(agencyNumber, underwriterRoleCode, underwritingUnitCode, isPrimary ? 1 : 0);
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(new APSWebServiceException(string.Format("An exception occurred communicating with the APS service: Method=getPersonnelAssignmentsByAgencyCodeUWCodeRoleCodePrimary, AgencyNumber={0}, underwriterRoleCode={1}, underwritingUnitCode={2}, isPrimary={3}.", agencyNumber, underwriterRoleCode, underwritingUnitCode, isPrimary), ex));
                throw new BlcNoInsuredException("Unable to import the details from APS for the agency of this account");
            }
        }
    }
}
