using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class StagingBuildingEntity : BaseCompanyEntity
	{
		#region Enum
		/// <summary>
		/// Translates the meaning of each column in the policy system's policy
		/// file.
		/// </summary>
		public enum BuildingColumn
		{
            LocationId,
			LocationNumber,
			BuildingNumber,
			YearBuilt,
            HasSprinklerSystem,
            HasSprinklerSystemCredit,
			PublicProtection,
			StockValues,
			BuildingContents,
			BuildingValue,
			BusinessInterruption,
            LocationOccupancy,
            BuildingValuationType,
            CoinsurancePercentage
            
		}
		#endregion

        public StagingBuildingEntity()
		{
		}

		protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
		{
            oColumnMapDictionary.Add((int)BuildingColumn.LocationId, 0);
            oColumnMapDictionary.Add((int)BuildingColumn.LocationNumber, 1);
			oColumnMapDictionary.Add((int)BuildingColumn.BuildingNumber, 2);
			oColumnMapDictionary.Add((int)BuildingColumn.YearBuilt, 3);
            oColumnMapDictionary.Add((int)BuildingColumn.HasSprinklerSystem, 4);
            oColumnMapDictionary.Add((int)BuildingColumn.HasSprinklerSystemCredit, 5);
			oColumnMapDictionary.Add((int)BuildingColumn.PublicProtection, 6);
			oColumnMapDictionary.Add((int)BuildingColumn.StockValues, 7);
			oColumnMapDictionary.Add((int)BuildingColumn.BuildingContents, 8);
			oColumnMapDictionary.Add((int)BuildingColumn.BuildingValue, 9);
			oColumnMapDictionary.Add((int)BuildingColumn.BusinessInterruption, 10);
            oColumnMapDictionary.Add((int)BuildingColumn.LocationOccupancy, 11);
            oColumnMapDictionary.Add((int)BuildingColumn.BuildingValuationType, 12);
            oColumnMapDictionary.Add((int)BuildingColumn.CoinsurancePercentage, 13);
            
		}
	}
}
