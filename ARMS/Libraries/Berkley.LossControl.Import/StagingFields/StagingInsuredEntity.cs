using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class StagingInsuredEntity : BaseCompanyEntity
	{
		#region Enum
		/// <summary>
		/// Translates the meaning of each column in the policy system's policy
		/// file.
		/// </summary>
		public enum InsuredColumn
		{
			/// <summary>
			/// The client identifier from the policy system.
			/// </summary>
			ClientID = 0,
			/// <summary>
			/// The agency number related to the insured.
			/// </summary>
			AgencyNumber,
			/// <summary>
			/// The name of the insured.
			/// </summary>
			InsuredName1,
			InsuredName2,
			/// <summary>
			/// The street address of the insured.
			/// </summary>
            StreetLine1,
            StreetLine2,
            StreetLine3,
			/// <summary>
			/// The city of the insured.
			/// </summary>
			City,
			/// <summary>
			/// The state of the insured.
			/// </summary>
			StateCode,
			/// <summary>
			/// The zip code of the insured.
			/// </summary>
			ZipCode,
			/// <summary>
			/// The insured's business operations.
			/// </summary>
			BusinessOperations,
            /// <summary>
            /// The business category.
            /// </summary>
            BusinessCategory,
            /// <summary>
            /// The business category.
            /// </summary>
            DOTNumber,
			/// <summary>
			/// The SIC Code of the business type of the insured.
			/// </summary>
			SICCode,
            /// <summary>
            /// The NAICS code.
            /// </summary>
            NAICSCode,
            /// <summary>
            /// The assigned underwriter.
            /// </summary>
            UnderwriterUsername,
            /// <summary>
            /// The assigned underwriter.
            /// </summary>
            CompanyWebsite,
            /// <summary>
            /// The assigned underwriter.
            /// </summary>
            AgentContactName,
            /// <summary>
            /// The assigned underwriter.
            /// </summary>
            AgentContactPhone,
            
		}
		#endregion

        public StagingInsuredEntity()
		{
		}

		protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
		{
			oColumnMapDictionary.Add((int)InsuredColumn.ClientID, 0);
			oColumnMapDictionary.Add((int)InsuredColumn.AgencyNumber, 1);
			oColumnMapDictionary.Add((int)InsuredColumn.InsuredName1, 2);
			oColumnMapDictionary.Add((int)InsuredColumn.InsuredName2, 3);
            oColumnMapDictionary.Add((int)InsuredColumn.StreetLine1, 4);
            oColumnMapDictionary.Add((int)InsuredColumn.StreetLine2, 5);
            oColumnMapDictionary.Add((int)InsuredColumn.StreetLine3, 6);
			oColumnMapDictionary.Add((int)InsuredColumn.City, 7);
			oColumnMapDictionary.Add((int)InsuredColumn.StateCode, 8);
			oColumnMapDictionary.Add((int)InsuredColumn.ZipCode, 9);
			oColumnMapDictionary.Add((int)InsuredColumn.BusinessOperations, 10);
            oColumnMapDictionary.Add((int)InsuredColumn.BusinessCategory, 11);
            oColumnMapDictionary.Add((int)InsuredColumn.DOTNumber, 12);
			oColumnMapDictionary.Add((int)InsuredColumn.SICCode, 13);
            oColumnMapDictionary.Add((int)InsuredColumn.NAICSCode, 14);
            oColumnMapDictionary.Add((int)InsuredColumn.UnderwriterUsername, 15);
            oColumnMapDictionary.Add((int)InsuredColumn.CompanyWebsite, 16);
            oColumnMapDictionary.Add((int)InsuredColumn.AgentContactName, 17);
            oColumnMapDictionary.Add((int)InsuredColumn.AgentContactPhone, 18);
			
		}
	}
}
