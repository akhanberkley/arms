using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class StagingUnderwriterEntity : BaseCompanyEntity
	{
		#region Enum
		/// <summary>
		/// Translates the meaning of each column in the policy system's policy
		/// file.
		/// </summary>
		public enum UnderwriterColumn
		{
            Username = 0,
            Name = 1,
			PhoneNumber = 2,
			PhoneExt = 3,
            EmailAddress = 4,
            Title = 5,
            ProfitCenter = 6
		}
		#endregion

        public StagingUnderwriterEntity()
		{
		}

		protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
		{
            oColumnMapDictionary.Add((int)UnderwriterColumn.Username, 0);
            oColumnMapDictionary.Add((int)UnderwriterColumn.Name, 1);
            oColumnMapDictionary.Add((int)UnderwriterColumn.PhoneNumber, 2);
            oColumnMapDictionary.Add((int)UnderwriterColumn.PhoneExt, 3);
            oColumnMapDictionary.Add((int)UnderwriterColumn.EmailAddress, 4);
            oColumnMapDictionary.Add((int)UnderwriterColumn.Title, 5);
            oColumnMapDictionary.Add((int)UnderwriterColumn.ProfitCenter, 6);
		}
	}
}
