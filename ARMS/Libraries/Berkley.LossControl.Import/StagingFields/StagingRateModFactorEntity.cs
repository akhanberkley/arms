using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class StagingRateModFactorEntity : BaseCompanyEntity
	{
		#region Enum
		/// <summary>
		/// Translates the meaning of each column in the policy system's policy
		/// file.
		/// </summary>
		public enum RateModFactorColumn
		{
            /// <summary>
            /// The client id of the insured.
            /// </summary>
            ClientID = 0,
            /// <summary>
			/// The policy number to which the rate mod is applicable.
			/// </summary>
			PolicyNumber = 1,
			/// <summary>
			/// The policy version to which the rate mod is applicable.
			/// </summary>
			PolicyVersion = 2,
            /// <summary>
            /// The policy id to which the rate mod is applicable.
            /// </summary>
            PolicyId = 3,
            StateCode = 4,
            FactorTypeCode = 5,
			Rate = 6
		}
		#endregion

        public StagingRateModFactorEntity()
		{
		}

		protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
		{
            oColumnMapDictionary.Add((int)RateModFactorColumn.ClientID, 0);
            oColumnMapDictionary.Add((int)RateModFactorColumn.PolicyNumber, 1);
            oColumnMapDictionary.Add((int)RateModFactorColumn.PolicyVersion, 2);
            oColumnMapDictionary.Add((int)RateModFactorColumn.PolicyId, 3);
            oColumnMapDictionary.Add((int)RateModFactorColumn.StateCode, 4);
            oColumnMapDictionary.Add((int)RateModFactorColumn.FactorTypeCode, 5);
            oColumnMapDictionary.Add((int)RateModFactorColumn.Rate, 6);
		}
	}
}
