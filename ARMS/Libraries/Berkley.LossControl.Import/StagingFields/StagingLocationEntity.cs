using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class StagingLocationEntity : BaseCompanyEntity
	{
		#region Enum
		/// <summary>
		/// Translates the meaning of each column in the policy system's policy
		/// file.
		/// </summary>
		public enum LocationColumn
		{
            LocationId = 0,
            ClientId,
            LocationNumber,
			StreetLine1,
			StreetLine2,
            City,
			StateCode,
			ZipCode,
            LocationDescription
		}
		#endregion

        public StagingLocationEntity()
		{
		}

		protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
		{
            oColumnMapDictionary.Add((int)LocationColumn.LocationId, 0);
            oColumnMapDictionary.Add((int)LocationColumn.ClientId, 1);
            oColumnMapDictionary.Add((int)LocationColumn.LocationNumber, 2);
            oColumnMapDictionary.Add((int)LocationColumn.StreetLine1, 3);
            oColumnMapDictionary.Add((int)LocationColumn.StreetLine2, 4);
			oColumnMapDictionary.Add((int)LocationColumn.City, 5);
			oColumnMapDictionary.Add((int)LocationColumn.StateCode, 6);
			oColumnMapDictionary.Add((int)LocationColumn.ZipCode, 7);
            oColumnMapDictionary.Add((int)LocationColumn.LocationDescription, 8);
		}
	}
}
