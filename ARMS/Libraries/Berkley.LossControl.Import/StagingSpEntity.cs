using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Berkley.BLC.Import
{
    public class StagingSpEntity
    {
        #region Type Enum
        public enum SpType
        {
            /// <summary>
            /// A stored procedure returning the insured information.
            /// </summary>
            Insured = 0,
            /// <summary>
            /// A stored procedure returning the agency information.
            /// </summary>
            Agency = 1,
            /// <summary>
            /// A stored procedure returning the policy for a location.
            /// </summary>
            Policy = 2,
            /// <summary>
            /// A stored procedure returning the building information of a location.
            /// </summary>
            Building = 3,
            /// <summary>
            /// A stored procedure returning the location information.
            /// </summary>
            Location = 4,
            /// <summary>
            /// A stored procedure returning the location contacts information.
            /// </summary>
            LocationContact = 5,
            /// <summary>
            /// A stored procedure returning the claim against an insured.
            /// </summary>
            Claim = 6,
            /// <summary>
            /// A stored procedure returning the rate mod factors of a policy for an insured.
            /// </summary>
            RateModFactor = 7
        }
        #endregion

        public StagingSpEntity()
        {
        }
    }
}
