using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;
//using Berkley.BLC.Business;
//using Berkley.BLC.Core;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;

namespace Berkley.BLC.Import
{
    public class ClaimWebService
    {
        public ClaimWebService()
        {
        }

        public BlcInsured GetClaims(ref BlcInsured blcInsured, Company company)
        {
            ClaimTransBLC[] roClaimTransBLC = CallWebService(blcInsured.PolicyList.ToArray(), company);
            if (roClaimTransBLC != null)
            {
                BuildClaimEntities(ref blcInsured, roClaimTransBLC);
            }
            return blcInsured;
        }

        public BlcClaim[] GetClaims(Policy[] policies, Company company)
        {
            ClaimTransBLC[] roClaimTransBLC = CallWebService(policies, company);
            BlcInsured blcInsured = new BlcInsured();
            if (roClaimTransBLC != null)
            {
                BuildClaimEntities(ref blcInsured, roClaimTransBLC);
            }
            return blcInsured.ClaimList.ToArray();
        }

        private void BuildClaimEntities(ref BlcInsured oBlcInsured, ClaimTransBLC[] roClaimTransBLC)
        {
            Claim oClaimEntity;
            BlcClaim oBlcClaim;
            string strFirstName;
            string strLastName;
            string[] rstrDateParts;

            foreach (ClaimTransBLC oClaimTransBLC in roClaimTransBLC)
            {
                // Iterate the Claims
                if (oClaimTransBLC.Claims != null)
                {
                    foreach (ClaimTransBLCClaims oWebClaim in oClaimTransBLC.Claims)
                    {
                        oBlcClaim = new BlcClaim();
                        oClaimEntity = new Claim();

                        oBlcClaim.Entity = oClaimEntity;
                        oBlcClaim.PolicyMod = Convert.ToInt32(oWebClaim.PolicyMod.Trim());
                        oBlcClaim.PolicyNumber = oWebClaim.PolicyNumber.Trim();

                        oClaimEntity.PolicyNumber = oWebClaim.PolicyNumber.Trim();
                        oClaimEntity.PolicyMod = Convert.ToInt32(oWebClaim.PolicyMod.Trim());
                        oClaimEntity.PolicySymbol = oWebClaim.PolicyPrefix.Trim();
                        oClaimEntity.PolicyExpireDate = Convert.ToDateTime(oWebClaim.PolicyExpDate.Trim());

                        oClaimEntity.ClaimAmount = 0;
                        oClaimEntity.ClaimNumber = oWebClaim.ClaimNumber.Trim();
                        strFirstName = oWebClaim.ClaimantFirstName.Trim();
                        strLastName = oWebClaim.ClaimantLastName.Trim();
                        if (strFirstName.Length > 0)
                        {
                            oClaimEntity.Claimant = oWebClaim.ClaimantLastName.Trim() + ", " + oWebClaim.ClaimantFirstName.Trim();
                        }
                        else
                        {
                            oClaimEntity.Claimant = oWebClaim.ClaimantLastName.Trim();
                        }
                        oClaimEntity.ClaimComment = oWebClaim.LossDescription.Trim();
                        oClaimEntity.ClaimStatus = oWebClaim.ClaimStatus.Trim();
                        oClaimEntity.LossType = oWebClaim.TypeOfLoss.Trim();
                        oWebClaim.ClaimNumber = oWebClaim.ClaimNumber.Trim();

                        rstrDateParts = oWebClaim.DateOfLoss.Split('T');
                        if (rstrDateParts.Length > 0)
                        {
                            // Only add the date - When the time is added, the date can roll back to the previous day
                            oClaimEntity.LossDate = DateTime.Parse(rstrDateParts[0]);
                        }
                        else
                        {
                            throw new BlcClaimsWebServiceException("Invalid Loss Date in Claim: " + oWebClaim.ClaimNumber);
                        }

                        if (oClaimTransBLC.Trans != null)
                        {
                            // Add up the Trans
                            foreach (ClaimTransBLCTrans oWebTrans in oClaimTransBLC.Trans)
                            {
                                if (oWebTrans.ClaimNumber.Trim() == oWebClaim.ClaimNumber)
                                {
                                    //New Biz rule, 6/10/2009:  recoveries need to be subtracted
                                    if (oWebTrans.TranType.Contains("RECOVERY"))
                                    {
                                        oClaimEntity.ClaimAmount -= oWebTrans.Amount;
                                    }
                                    else
                                    {
                                        oClaimEntity.ClaimAmount += oWebTrans.Amount;
                                    }


                                    oClaimEntity.ClaimAmount += oWebTrans.Reserve;
                                }
                            }
                        }

                        oBlcInsured.ClaimList.Add(oBlcClaim);
                    }
                }
            }
        }

        public ClaimTransBLC CreateClaimInquiryCWS(string strXml)
        {
            StringReader oStringReader = null;
            XmlTextReader oXmlReader = null;
            try
            {
                // serialise to object
                XmlSerializer oXmlSerializer = new XmlSerializer(typeof(ClaimTransBLC));
                oStringReader = new StringReader(strXml); // read xml data
                oXmlReader = new XmlTextReader(oStringReader); // create reader
                // covert reader to object
                return (ClaimTransBLC)oXmlSerializer.Deserialize(oXmlReader);
            }
            catch (Exception oException)
            {
                throw new BlcClaimsWebServiceException("Error deserializing XML data from Claims web service. " + oException.Message);
            }
            finally
            {
                if (oStringReader != null)
                {
                    oStringReader.Close();
                }
                if (oXmlReader != null)
                {
                    oXmlReader.Close();
                }
            }
        }

        /// <summary>
        /// This function wraps the Claims web service call.
        /// 
        /// The BtsClaims.xsd and BtsClaims.cs were created using the XSD command line utility.
        /// 1. Grag the XML returned from the web service using the debugger and QuickWatch
        /// 2. Save the XML to a file and run the xsd.exe tool using the xml as an input.
        /// 
        /// </summary>
        /// <param name="policies"></param>
        /// <param name="oCompany"></param>
        /// <returns></returns>
        private ClaimTransBLC[] CallWebService(Policy[] policies, Company company)
        {
            CWS.ClaimTrans claimInquiry = new Berkley.BLC.Import.CWS.ClaimTrans();
            claimInquiry.Timeout = 100000;

            //switch (CoreSettings.ConfigurationType)
            //{
            //    case ConfigurationType.Production:
            //        //claimInquiry.Url = "http://btseprdev/claimsinquiry/claimtrans.asmx";
            //        claimInquiry.Url = "http://portalwebservices:8088/claims/claimtrans.asmx";
            //        break;
            //    case ConfigurationType.Test:
            //        claimInquiry.Url = "http://portalwebservices:8088/claims/claimtrans.asmx";
            //        break;
            //    case ConfigurationType.Integration:
            //    case ConfigurationType.Development:
            //        claimInquiry.Url = "http://tst-portalwebservices:8088/claims/claimtrans.asmx";
            //        break;
            //    default:
            //        throw new NotSupportedException("Expecting an environment to be set.");
            //}

            claimInquiry.Url = "http://tst-portalwebservices:8088/claims/claimtrans.asmx";

            List<ClaimTransBLC> claimTransBLCList = new List<ClaimTransBLC>();
            ClaimTransBLC claimTransBLC;
            XmlNode xmlNode = null;

            var distinctPolicyNumbers = policies.Select(p => p.PolicyNumber).Distinct();
            var distinctPolicies = new List<Policy>();
            foreach (var pn in distinctPolicyNumbers)
            {
                var policy = (from p in policies
                              where p.PolicyNumber == pn
                              orderby p.PolicyNumber, p.PolicyMod descending
                              select p).First();
                distinctPolicies.Add(policy);
            }

            foreach (Policy policy in distinctPolicies)
            {
                StringWriter stringWriter = new StringWriter();
                XmlTextWriter writer = new XmlTextWriter(stringWriter);
                DateTime startDate;
                DateTime endDate = policy.ExpireDate.GetValueOrDefault();
                string strEndDate;
                string strStartDate;

                // WARNING: If the SCHEMA Changes, convert all DATE members to STRING data types
                // In the BtsClaims.cs class.
                try
                {
                    if (company.YearsOfLossHistory <= 0)
                    {
                        return null;
                    }
                    else
                    {
                        startDate = endDate.AddYears(-company.YearsOfLossHistory);
                    }
                    strStartDate = startDate.ToShortDateString();
                    strEndDate = endDate.ToShortDateString();

                    // No input MOD.  The result will be all claims in the given date range
                    xmlNode = claimInquiry.BLCNoMod("BTSBLC00"/*CoreSettings.ClaimsWebServiceId*/, Convert.ToInt32(company.CompanyNumber), policy.PolicyNumber, strStartDate, strEndDate);
                }
                catch (SoapException oSoapException)
                {
                    StringBuilder oBuilder = new StringBuilder("The Claims web service returned the following error:");
                    oBuilder.Append(" {0}\r\n");
                    oBuilder.Append("The Claims system was passed the following information: \r\n");
                    oBuilder.Append("DSIK: {1}\r\n");
                    oBuilder.Append("Corporation ID: {2}\r\n");
                    oBuilder.Append("Policy Number: {3}\r\n");
                    oBuilder.Append("Policy Mod: {4}\r\n");

                    throw new BlcClaimsWebServiceException(String.Format(oBuilder.ToString(), oSoapException.Message, "BTSBLC00"/*CoreSettings.ClaimsWebServiceId*/, company.CompanyNumber, policy.PolicyNumber, policy.PolicyMod));
                }
                catch (Exception oException)
                {
                    throw new BlcClaimsWebServiceException("General Claims web service error: " + oException.Message);
                }
                xmlNode.WriteTo(writer);

                claimTransBLC = CreateClaimInquiryCWS(stringWriter.GetStringBuilder().ToString());
                if (claimTransBLC != null)
                {
                    claimTransBLCList.Add(claimTransBLC);
                }
            }
            return claimTransBLCList.ToArray();
        }


    }
}
