using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class BlcImportException : Exception
	{
		public BlcImportException(string strText)
			: base(strText)
		{
		}
	}
}
