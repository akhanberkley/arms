using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class PolicySTARWebServiceException : Exception
    {
        public PolicySTARWebServiceException(string strText, Exception innerException)
            : base(strText, innerException)
        {

        }
    }
}

