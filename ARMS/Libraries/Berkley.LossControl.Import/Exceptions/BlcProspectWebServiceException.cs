using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class BlcProspectWebServiceException : Exception
	{
		public BlcProspectWebServiceException(string strText)
			: base(strText)
		{

		}

	}
}
