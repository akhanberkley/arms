using System;
using System.Collections.Generic;
using System.Text;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;

namespace Berkley.BLC.Import
{
	public class BlcServicePlan
	{
		private ServicePlan moServicePlan;
		private List<ServicePlanNote> moServicePlanNotes = new List<ServicePlanNote>();
		private List<SurveyRecommendation> moSurveyRecommendationList = new List<SurveyRecommendation>();

		public BlcServicePlan()
		{
		}

		public ServicePlan ServicePlanEntity
		{
			get { return moServicePlan; }
			set { moServicePlan = value; }
		}

		public List<ServicePlanNote> ServicePlanNoteList
		{
			get { return moServicePlanNotes; }
		}

		public List<SurveyRecommendation> SurveyRecommendationList
		{
			get { return moSurveyRecommendationList; }
		}


	}
}
