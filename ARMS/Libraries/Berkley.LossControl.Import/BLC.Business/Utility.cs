﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using BTS.WFA.ARMS.Data.Models;

namespace Berkley.BLC.Import
{
    public class Utility
    {
        public static string GetCompanyParameter(string parameter, Company company)
        {
            /*
            Parameter param = null;
            try
            {
                List<Parameter> parameters = Cache.Get<List<Parameter>>(Cache.Keys.Parameters);

#if DEBUG
                parameters = null;
#endif

                if (parameters == null)
                {
                    parameters = Parameter.GetArray("ID > ?", 0).ToList();
                    Cache.Set(Cache.Keys.Parameters, parameters);
                }

                param = parameters.Where(p => p.Description == parameter).FirstOrDefault();
            }
            catch
            {
                //non http processes maybe using this
                Parameter pmeter = Parameter.GetOne("Description = ?", parameter);
                if (pmeter != null)
                {
                    CompanyParameter cParam = CompanyParameter.GetOne("CompanyID = ? && ParameterID = ?", company.CompanyId, pmeter.Id);
                    if (cParam != null)
                        return cParam.Value.ToString();
                }
                return string.Empty;

            }

            if (param != null)
            {
                List<CompanyParameter> companyparameters = Cache.Get<List<CompanyParameter>>(Cache.Keys.CompanyParameters, company.CompanyNumber.ToInt());

#if DEBUG
                companyparameters = null;
#endif
                if (companyparameters == null)
                {
                    companyparameters = CompanyParameter.GetArray("CompanyID = ?", company.CompanyId).ToList();
                    Cache.Set(Cache.Keys.CompanyParameters, company.CompanyNumber.ToInt(), companyparameters);
                }

                CompanyParameter cParam = companyparameters.Where(cp => cp.ParameterId == param.Id).FirstOrDefault();


                if (cParam != null)
                    return cParam.Value.ToString();
            }
            */
            return string.Empty;

        }

        public static void CopyPropertyValues<T>(ref T e1, T e2)
        {
            PropertyInfo[] properties = e1.GetType().GetProperties();

            foreach (PropertyInfo property in properties)
            {
                if (property.Name == "ID" || property.Name == "IsReadOnly")
                    continue;

                //if (property.PropertyType.IsGenericType)
                //    continue;

                if (property.PropertyType.BaseType == null || property.PropertyType.BaseType.Name.Contains("Entity") || property.PropertyType.Name.Contains("Entity"))
                    continue;

                try
                {
                    e1.GetType().GetProperty(property.Name).SetValue(e1, e2.GetType().GetProperty(property.Name).GetValue(e2, null), null);
                }
                catch
                {
                    continue;
                }
            }
        }
    }
}
