﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ARMS;
using BTS.WFA.ARMS.Services;
using BTS.WFA.ARMS.ViewModels;

namespace Berkley.BLC.Import
{
    public class BlcCompany
    {
        public static readonly Company AcadiaInsuranceCompany = CompanyService.GetCompanyById(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"));
        public static readonly Company BerkleyAgribusinessRiskSpecialists = CompanyService.GetCompanyById(new Guid("982BAF6D-A134-4136-8865-C4653A14CA8F"));
        public static readonly Company BerkleyMidAtlanticGroup = CompanyService.GetCompanyById(new Guid("77C82BD6-0BD9-4D79-92FD-C133788A3B0E"));
        public static readonly Company ContinentalWesternGroup = CompanyService.GetCompanyById(new Guid("30E6C82E-2DF1-44B8-82B7-3B34654CF282"));
        public static readonly Company UnionStandardInsuranceGroup = CompanyService.GetCompanyById(new Guid("25f08c73-e8a3-4475-af1a-91882abcdb76"));
        public static readonly Company WRBerkleyCorporation = CompanyService.GetCompanyById(new Guid("5e7b1b53-da4c-4d80-af93-e3948911b2ea"));
        public static readonly Company BerkleyNorthPacificGroup = CompanyService.GetCompanyById(new Guid("4E482502-80CD-43CC-9632-E579180560C0"));
        public static readonly Company CarolinaCasualtyInsuranceGroup = CompanyService.GetCompanyById(new Guid("02F6BF97-7F88-4D44-9449-27A31B89E7C3"));
        public static readonly Company BerkleyLifeSciences = CompanyService.GetCompanyById(new Guid("7F521E36-C296-47C7-9012-B47EE76A09D1"));
        public static readonly Company BerkleyOilGas = CompanyService.GetCompanyById(new Guid("E0BEA76A-B9D0-4A42-8ECF-FCA0ECC793EE"));
        public static readonly Company BerkleySpecialtyUnderwritingManagers = CompanyService.GetCompanyById(new Guid("02e15646-1058-40ed-add1-00197fb4c93a"));
        public static readonly Company BerkleyFireMarine = CompanyService.GetCompanyById(new Guid("35f08c73-e8a3-4475-af1a-91882abcdb89"));
        public static readonly Company BerkleySouthEast = CompanyService.GetCompanyById(new Guid("87c82bd6-0bd9-4d79-92fd-c133788a3b02"));
        public static readonly Company BerkleyTechnologyUnderwriters = CompanyService.GetCompanyById(new Guid("EC0203CE-18A8-4444-B0E1-D98FFC469E3F"));
        public static readonly Company RiverportInsuranceCompany = CompanyService.GetCompanyById(new Guid("00E140A3-F45C-4687-8232-28A86FA5DFAE"));
    }
}
