using System;
using System.Collections.Generic;
using System.Text;

using System.Linq;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ViewModels;
using BTS.WFA.ARMS;

namespace Berkley.BLC.Import
{
    public class CoverageNameTable
    {
        private Dictionary<string, Guid> moCoverage = new Dictionary<string,Guid>();

        public CoverageNameTable()
        {
        }

        public void Clear()
        {
            moCoverage.Clear();
        }

        public Guid GetId(BlcCoverage oCoverage)
        {
            string strKey = new CoverageNameKey(oCoverage).Key;
            if (moCoverage.ContainsKey(strKey))
            {
                return moCoverage[strKey];
            }
            else
            {
                throw new Exception("CoverageName not found" + oCoverage.ToString());
            }
        }

        public void Load(Company oCompany)
        {
            //CoverageName[] roCoverageNames = CoverageName.GetArray("CompanyID = ?", oCompany.ID);

            using (var db = Application.GetDatabaseInstance())
            {
                CoverageName[] roCoverageNames = db.CoverageNames.Where(c=>c.CompanyId == oCompany.CompanyId).ToArray(); 
            
                foreach(CoverageName oCoverageName in roCoverageNames)
                {
                    if (oCoverageName.SubLineOfBusinessCode.Length > 0)
                    {
                        moCoverage.Add(new CoverageNameKey(oCoverageName.LineOfBusinessCode, oCoverageName.SubLineOfBusinessCode, oCoverageName.CoverageNameTypeCode).Key, oCoverageName.CoverageNameId);
                    }
                    else
                    {
                        moCoverage.Add(new CoverageNameKey(oCoverageName.LineOfBusinessCode, oCoverageName.CoverageNameTypeCode).Key, oCoverageName.CoverageNameId);
                    }
                }
            }
        }
    }
}
