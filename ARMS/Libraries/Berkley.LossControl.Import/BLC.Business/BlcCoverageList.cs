using System;
using System.Collections.Generic;
using System.Text;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;


namespace Berkley.BLC.Import
{
	public class BlcCoverageList<T> : List<T>
	{
		private Dictionary<string, List<T>> moDictionary = new Dictionary<string, List<T>>();

		public BlcCoverageList()
		{
		}

		public void Add(CoverageKey oCoverageKey, T oClass)
		{
			string strKey = oCoverageKey.Key;

			base.Add(oClass);
			if (moDictionary.ContainsKey(strKey))
			{
				List<T> oList  = moDictionary[strKey];
				oList.Add(oClass);
				moDictionary[strKey] = oList;
			}
			else
			{
				List<T> oList = new List<T>();

				oList.Add(oClass);
				moDictionary[strKey] = oList;
			}
		}

		public List<T> Get(CoverageKey oCoverageKey)
		{
			if(moDictionary.ContainsKey(oCoverageKey.Key))
			{
				return moDictionary[oCoverageKey.Key];
			}
			else
			{
				return null;
			}
		}

	}
}
