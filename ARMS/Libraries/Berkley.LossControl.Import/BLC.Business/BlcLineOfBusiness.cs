﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ARMS;
using BTS.WFA.ARMS.Services;
using BTS.WFA.ARMS.ViewModels;

namespace Berkley.BLC.Import
{
    public class BlcLineOfBusiness
    {
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness WorkersCompensation = LineOfBusinessService.GetLobByCode("WC");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness GeneralLiability = LineOfBusinessService.GetLobByCode("GL");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness CommercialAuto = LineOfBusinessService.GetLobByCode("CA");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness CommercialOutput = LineOfBusinessService.GetLobByCode("CO");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness CommercialProperty = LineOfBusinessService.GetLobByCode("CP");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness BusinessOwner = LineOfBusinessService.GetLobByCode("BO");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness InlandMarine = LineOfBusinessService.GetLobByCode("IM");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness OceanMarine = LineOfBusinessService.GetLobByCode("OM");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness Package = LineOfBusinessService.GetLobByCode("PK");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness CommercialUmbrella = LineOfBusinessService.GetLobByCode("CU");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness CommercialCrime = LineOfBusinessService.GetLobByCode("CR");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness Garage = LineOfBusinessService.GetLobByCode("GA");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness Dealer = LineOfBusinessService.GetLobByCode("DR");
        public static readonly BTS.WFA.ARMS.Data.Models.LineOfBusiness Transportation = LineOfBusinessService.GetLobByCode("TR");
    }
}
