using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using BTS.WFA.ARMS.Data.Models;
using BTS.WFA.ViewModels;
using BTS.WFA.ARMS;

namespace Berkley.BLC.Import
{
    public class BlcUser
    {
        private static Dictionary<string, User> moUserDictionary = null;
        private string mstrName = "";
        private static Company moCompany;
        
        public BlcUser(string strNotesUser)
        {
            mstrName = ParseCn(strNotesUser);
        }

        public static Company Company
        {
            get { return moCompany; }
            set { moCompany = value; }
        }

        public string Name
        {
            get { return mstrName; }
        }

        private string ParseCn(string strText)
        {
            int iNameStart = strText.IndexOf("CN=");
            int iNameEnd = strText.IndexOf("/");

            if (iNameStart >= 0 && iNameEnd > iNameStart)
            {
                iNameStart += 3;
                return strText.Substring(iNameStart, iNameEnd - iNameStart);
            }
            else if (iNameEnd > 0)
            {
                return strText.Substring(0, iNameEnd).Trim();
            }
            else
            {
                return strText.Trim();
            }
        }

        private static void FillDictionary()
        {
            if (moUserDictionary == null)
            {
                moUserDictionary = new Dictionary<string, User>();

                //User[] roUsers = User.GetArray("CompanyID = ?", moCompany.ID);

                using (var db = Application.GetDatabaseInstance())
                {
                    User[] roUsers = db.Users.Where(u => u.CompanyId == moCompany.CompanyId).ToArray();
                    foreach (User oUser in roUsers)
                    {
                        moUserDictionary[oUser.Name] = oUser;
                    }
                }
            }
        }

        public static User GetUserByCn(string strNotesUser)
        {
            BlcUser oReturn = new BlcUser(strNotesUser);
            if (oReturn.Name.Length > 0)
            {
                FillDictionary();
                if (moUserDictionary.ContainsKey(oReturn.Name))
                {
                    return moUserDictionary[oReturn.Name];
                }
                else
                {
                    oReturn.ValidateUser(oReturn.Name);
                    return moUserDictionary[oReturn.Name];
                }
            }
            return null;
        }

        public static User GetUserByName(string strNotesUser)
        {
            FillDictionary();

            if (moUserDictionary.ContainsKey(strNotesUser))
            {
                return moUserDictionary[strNotesUser];
            }
            return null;
        }

        public void ValidateUser(string strName)
        {
            FillDictionary();

            if (strName.Length > 0)
            {
                if (!moUserDictionary.ContainsKey(strName))
                {
                    //User oUser = new User();
                    //oUser.AccountDisabled = true;
                    ////oUser.Certifications = ;
                    ////oUser.City = ;
                    ////oUser.FaxNumber = ;
                    ////oUser.HomePhone = ;
                    ////oUser.HtmlAddress = ;
                    ////oUser.MobilePhone = ;
                    ////oUser.PermissionSetID = new Guid("E949FAAF-144D-4CEC-981E-B79F5D338A9E");
                    ////oUser.ProfitCenter = ;
                    ////oUser.StateCode = ;
                    ////oUser.StreetLine1 = ;
                    ////oUser.StreetLine2 = ;
                    ////oUser.Title = ;
                    ////oUser.ZipCode = ;

                    //oUser.CompanyId = moCompany.CompanyId;
                    //oUser.DomainName = "WRBTS";
                    //oUser.EmailAddress = "unknown";
                    //oUser.IsFeeCompany = false;
                    //oUser.IsUnderwriter = false;
                    //oUser.Name = strName;
                    //oUser.UserRoleId = new Guid("D5F621CA-2C21-49C7-80A8-906CC2648298");
                    //oUser.TimeZoneCode = "US/Eastern";
                    //if(strName.Length > 30)
                    //{
                    //    oUser.Username = strName.Substring(0, 30).Replace(" ", String.Empty);
                    //}
                    //else
                    //{
                    //    oUser.Username = strName.Replace(" ", String.Empty);
                    //}
                    //oUser.WorkPhone = "9999999999";
                    //moUserDictionary[strName] = oUser;
                    //oUser.Save();

                    throw new NotImplementedException();
                }
            }
        }
    }
}
