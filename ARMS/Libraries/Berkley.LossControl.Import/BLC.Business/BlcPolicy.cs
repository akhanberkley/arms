using System;
using System.Collections.Generic;
using System.Text;


//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;

namespace Berkley.BLC.Import
{
	public class BlcPolicy
	{
        private string mstrPolicySystemKey = "";

		public static bool Equals(Policy oLhs, Policy oRhs)
		{
            if (oRhs.PolicyMod != 0 && oLhs.PolicyMod != 0)
			{
                return oLhs.PolicyNumber == oRhs.PolicyNumber && oLhs.PolicyMod == oRhs.PolicyMod;
			}
			else
			{
                string strRhsPolicy = oRhs.PolicyNumber.Substring(0, 7);
                string strLhsPolicy = oLhs.PolicyNumber.Substring(0, 7);
				if (strRhsPolicy == strLhsPolicy)
				{
                    //Do we need to allow for day variance (Talk to Chris)
                    if (oLhs.ExpireDate == oRhs.ExpireDate)
					{
                        
                        //if (oLhs.ExpireDate.Month == oRhs.ExpireDate.Month)
                        //{
							return true;
                        //}
                        //else if (oLhs.ExpireDate.Month == oRhs.ExpireDate.Month + 1)
                        //{
                        //    return true;
                        //}
                        //else if (oLhs.ExpireDate.Month == oRhs.ExpireDate.Month - 1)
                        //{
                        //    return true;
                        //}
					}
				}
				return false;
			}
		}

        public string PolicySystemKey
        {
            get { return mstrPolicySystemKey; }
            set { mstrPolicySystemKey = value; }
        }
	}
}
