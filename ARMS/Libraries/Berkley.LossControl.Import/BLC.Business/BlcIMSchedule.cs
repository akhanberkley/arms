﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Berkley.BLC.Import
{
    public class BlcIMSchedule
    {
        private string imNumber = string.Empty;
        private string imEquipment = string.Empty;
        private string imYear = string.Empty;
        private string imSerialNumber = string.Empty;
        private decimal imLimit = 0;


        public BlcIMSchedule()
        {
        }

       
        public string Number
        {
            get { return imNumber; }
            set { imNumber = value; }
        }

        // Equipment
        public string Equipment
        {
            get { return imEquipment; }
            set { imEquipment = value; }
        }

       //Year
        public string Year
        {
            get { return imYear; }
            set { imYear = value; }
        }

        //SerialNumber
        public string SerialNumber
        {
            get { return imSerialNumber; }
            set { imSerialNumber = value; }
        }

        //Limit
        public decimal Limit
        {
            get { return imLimit; }
            set { imLimit = value; }
        }

    }
}
