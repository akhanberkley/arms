using System;
using System.Collections.Generic;
using System.Text;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;

namespace Berkley.BLC.Import
{
	public class BlcClaim
	{
		private Claim moClaim;
		private string mstrPolicyNumber;
		private int miPolicyMod;
		
		public BlcClaim()
		{
		}

		public Claim Entity
		{
			get { return moClaim; }
			set { moClaim = value; }
		}

		public string PolicyNumber
		{
			get { return mstrPolicyNumber; }
			set { mstrPolicyNumber = value; }
		}

		public int PolicyMod
		{
			get { return miPolicyMod; }
			set { miPolicyMod = value; }
		}

	}
}
