using System;
using System.Collections.Generic;
using System.Text;
//using Berkley.BLC.Entities;
using BTS.WFA.ARMS.Data.Models;

namespace Berkley.BLC.Import
{
    public class BlcInsured
    {
        private string mstrOwner = string.Empty;
        private string mstrOwnerPhone = string.Empty;
        private string mstrOwnerAltPhone = string.Empty;
        private string mstrOwnerFax = string.Empty;
        private string mstrOwnerEmail = string.Empty;
        private string mstrOwnerTitle = string.Empty;
        private Insured moInsured;
        private Agency moAgency;
        private string mstrAgencyError = string.Empty;
        private string mstrPreviousClientId = string.Empty;

        // Used by Migration
        private bool mfFoundInDb = false;

        private List<Location> moPolicyLocationList = new List<Location>();
        private List<LocationContact> moLocationContactList = new List<LocationContact>();
        private List<Policy> moPolicyList = new List<Policy>();
        private List<BlcBuilding> moBuildingList = new List<BlcBuilding>();
        private List<BlcClaim> moClaimList = new List<BlcClaim>();
        private List<BlcRateModFactor> moRateModFactorList = new List<BlcRateModFactor>();
        private BlcCoverage moBlcCoverage = new BlcCoverage();
        private BlcCoverageList<BlcCoverage> moCoverageList = new BlcCoverageList<BlcCoverage>();
        private List<AgencyEmail> moAgencyEmailList = new List<AgencyEmail>();

        private Policy[] mroPolicies = null;
        private Location[] mroLocations = null;

        public BlcInsured()
        {
        }

        public void SetInsuredId(Guid oInsuredId)
        {
            moInsured = new Insured();
        }

        public string AgencyError
        {
            get { return mstrAgencyError; }
            set { mstrAgencyError = value; }
        }

        /// <summary>
        /// Set the PreviousClientId to instruct Reconcile to update the ClientId
        /// </summary>
        public string PreviousClientId
        {
            get { return mstrPreviousClientId; }
            set { mstrPreviousClientId = value; }
        }

        #region Existing Entities
        public Policy[] ExistingPolicies
        {
            get { return mroPolicies; }
            set { mroPolicies = value; }
        }

        public Location[] ExistingLocations
        {
            get { return mroLocations; }
            set { mroLocations = value; }
        }

        #endregion

        /// <summary>
        /// Returned from the Insured Stored Procedure - goes in the LocationContact BLC Table
        /// </summary>
        public string Owner
        {
            get { return mstrOwner; }
            set { mstrOwner = value; }
        }

        // Migrated
        public string OwnerTitle
        {
            get { return mstrOwnerTitle; }
            set { mstrOwnerTitle = value; }
        }

        /// <summary>
        /// Returned from the Insured Stored Procedure - goes in the LocationContact BLC Table
        /// </summary>
        public string OwnerPhone
        {
            get { return mstrOwnerPhone; }
            set { mstrOwnerPhone = value; }
        }

        /// <summary>
        /// Returned from the Insured Stored Procedure - goes in the LocationContact BLC Table
        /// </summary>
        public string OwnerAltPhone
        {
            get { return mstrOwnerAltPhone; }
            set { mstrOwnerAltPhone = value; }
        }

        /// <summary>
        /// Returned from the Insured Stored Procedure - goes in the LocationContact BLC Table
        /// </summary>
        public string OwnerFax
        {
            get { return mstrOwnerFax; }
            set { mstrOwnerFax = value; }
        }

        /// <summary>
        /// Returned from the Insured Stored Procedure - goes in the LocationContact BLC Table
        /// </summary>
        public string OwnerEmail 
        {
            get { return mstrOwnerEmail; }
            set { mstrOwnerEmail = value; }
        }

        public Insured InsuredEntity
        {
            get { return moInsured; }
            set { moInsured = value; }
        }

        public Agency AgencyEntity
        {
            get { return moAgency; }
            set { moAgency = value; }
        }

        public List<AgencyEmail> AgencyEmailList
        {
            get { return moAgencyEmailList; }
        }

        public List<Location> PolicyLocationList
        {
            get { return moPolicyLocationList; }
        }

        public List<LocationContact> LocationContactList
        {
            get { return moLocationContactList; }
        }

        public List<Policy> PolicyList
        {
            get { return moPolicyList; }
        }

        public List<BlcBuilding> BuildingList
        {
            get { return moBuildingList; }
        }

        // BLC Buildings set by the Base Builder
        public List<BlcBuilding> GetBuildingListForLocation(Location oLocation)
        {
            List<BlcBuilding> oReturnList = new List<BlcBuilding>();

            foreach (BlcBuilding oBuilding in moBuildingList)
            {
                //company specif hack for A+Plus
                //if (this.InsuredEntity.CompanyID == Company.ContinentalWesternGroup.ID)
                //{
                //    if (oLocation.Number == oBuilding.LocationAsInt)
                //    {
                //        oReturnList.Add(oBuilding);
                //    }
                //}
                //else
                //{
                    // Use the Policy KEY to find the Buildings for a Policy
                    if (oLocation.PolicySystemKey == oBuilding.PolicySystemKey)
                    {
                        oReturnList.Add(oBuilding);
                    }
                //}
            }
            return oReturnList;
        }

        public BlcCoverageList<BlcCoverage> CoverageList
        {
            get { return moCoverageList; }
        }

        public List<BlcClaim> ClaimList
        {
            get { return moClaimList; }
        }

        public List<BlcRateModFactor> RateModFactorList
        {
            get { return moRateModFactorList; }
        }

        // For Migration
        public bool FoundInDb
        {
            get { return mfFoundInDb; }
            set { mfFoundInDb = value; }
        }

    }
}
