using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using System.Xml.Serialization;
using System.Xml;
using System.IO;

using Berkley.BLC.Import.Properties;
//using Berkley.BLC.Core;

namespace Berkley.BLC.Import
{
    /// <summary>
    /// Contains a List of Stored Procedures specific for a Company
    /// The definition of the list is held in the Config file in XML
    /// CompanySpEntityList also holds the Connection String and KEY for the DATA to Import from the Company DB 
    /// </summary>
    public class CompanySpEntityList : List<CompanySpEntity>
    {
        private string mstrConnectionString;
        private string mstrClientId;
        private string mstrPolicyNumber;
        private string mstrAgentNumber;

        public CompanySpEntityList(string strConnectionString, string strClientId, string strPolicyNumber, string strAgentNumber)
        {
            mstrConnectionString = strConnectionString;
            mstrClientId = strClientId;
            mstrPolicyNumber = strPolicyNumber;
            mstrAgentNumber = strAgentNumber;
        }

        public void Load(string strConfigSection)
        {
            NameValueCollection oRows = new BerkleyXmlKeyValueCollection(strConfigSection).KeyValues;
            foreach (string strKey in oRows.Keys) 
            {
                this.Add(new CompanySpEntity(strKey, oRows[strKey], mstrConnectionString, mstrClientId, mstrPolicyNumber, mstrAgentNumber));
            }
        }

    }
}
