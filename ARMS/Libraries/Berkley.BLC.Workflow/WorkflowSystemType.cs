using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.Workflow
{
    /// <summary>
    /// Enum for the different types of workflow products integrated to ARMS.
    /// </summary>
    public enum WorkflowSystemType
    {
        /// <summary>
        /// VA3i legacy workflow system.
        /// </summary>
        VA3I,
        /// <summary>
        /// ALBPM 5.5 version.
        /// </summary>
        ALBPM_5,
        /// <summary>
        /// ALBPM 6.0 version.
        /// </summary>
        ALBPM_6,
        /// <summary>
        /// ALBPM 6.0 version with P*.
        /// </summary>
        ALBPM_6_WITH_PStar,
    }
}