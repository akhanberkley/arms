/*===========================================================================
  File:			CloseSuspendedSurveyInstance.cs
  
  Summary:		Closes a suspended survey instance in the ALBPM workflow system.

  Classes:		CloseSuspendedAuditInstance
  
  Enumerations:	None
  
  Interfaces:	None
  
  Origin:		Loss Control Tracking System(September, 2007)
  
=============================================================================
  Copyright (c) 2007 Berkley Technology Services, LLC. All rights reserved.
 =============================================================================*/

using System;
using System.Xml;
using Berkley.BLC.Core;
using Berkley.BLC.Entities;

namespace Berkley.Workflow
{
    /// <summary>
    /// Closes a suspended audit transaction in the ALBPM workflow system.
    /// </summary>
    public class CloseSuspendedSurveyInstance : VA3iBusinessProcess
    {
        #region fields
        private IPolicy policy;
        private string transactionType;
        private string status;
        private string policyNote;
        private string instanceNote;
        private string submissionNumber;
        private string referenceNumber;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="requestor">The <see cref="IContact"/> that requested
        /// the transaction to be opened.</param>
        /// <param name="system">The <see cref="ISoftwareApplication"/> that
        /// requested the transaction to be opened.</param>
        /// <param name="company">The company to whom the transaction is
        /// associated.</param>
        /// <param name="systemType">The workflow system and version.</param>
        /// <param name="policy">The insurance policy to which the transaction
        /// is associated.</param>
        /// <param name="transactionType">The type of transaction.</param>
        /// <param name="status">The status of the transaction.</param>
        /// <param name="policyNote">A policy note.</param>
        /// <param name="instanceNote">An instance note.</param>
        /// <param name="configuration">The software configuration in which the process will execute.</param>
        /// <param name="submissionNumber">The submission number.</param>
        /// <param name="referenceNumber">The worflow reference number.</param>
        public CloseSuspendedSurveyInstance(
            IContact requestor, ISoftwareApplication system,
            Company company, WorkflowSystemType systemType, IPolicy policy,
            string transactionType, string status, string policyNote, string instanceNote, ConfigurationType configuration, string submissionNumber, string referenceNumber)
            : base(requestor, system, company, systemType, configuration)
        {
            if (policy != null)
            {
                this.policy = policy;
            }
            else if (submissionNumber != null && submissionNumber.Trim().Length > 0)
            {
                this.submissionNumber = submissionNumber;
            }
            else
            {
                throw new InvalidParameterException();
            }

            if (status != null && status.Trim().Length > 0)
                this.status = status;
            else
                throw new InvalidParameterException();

            this.transactionType = transactionType;
            this.policyNote = policyNote;
            this.instanceNote = instanceNote;
            this.referenceNumber = referenceNumber;
        }
        #endregion

        #region properties
        #endregion

        #region methods
        /// <summary>
        /// Overridden. Builds a open transaction element.
        /// </summary>
        /// <param name="document">The document to which the transaction will be
        /// appended.</param>
        /// <returns>An <see cref="XmlElement"/> containing the transaction 
        /// information.</returns>
        protected override XmlNode BuildTransaction(XmlDocument document)
        {
            /*
			 * short circuit if the document is invalid
			 */
            if (document == null || (this.policy == null && this.submissionNumber == null))
                return null;

            XmlElement transaction = document.CreateElement("Transaction");
            XmlElement attributes = document.CreateElement("TranAttributes");

            XmlElement policyData = document.CreateElement("PolicyAttrData");
            XmlAttribute attribute = null;
            if (this.policy != null) //policy
            {
                //policy number
                attribute = document.CreateAttribute("PolicyNumber");
                attribute.Value = this.policy.Number.Replace("_P", "").Replace("_", "");
                policyData.Attributes.Append(attribute);

                //policy effective date
                attribute = BuildEffectiveDate(document, policy);
                policyData.Attributes.Append(attribute);
            }
            else if (this.submissionNumber != null) //submission number
            {
                attribute = document.CreateAttribute("SubmissionNum");
                attribute.Value = this.submissionNumber;
                policyData.Attributes.Append(attribute);
            }
            else
            {
                return null;
            }

            attributes.AppendChild(policyData);

            /*
             * set the transactionData details.
             */
            XmlElement transactionData = document.CreateElement("TranAttrData");
            attribute = document.CreateAttribute("TranType");
            attribute.Value = this.transactionType;
            transactionData.Attributes.Append(attribute);

            attribute = document.CreateAttribute("TranNo");
            attribute.Value = base.DetermineTransactionNumber(this.referenceNumber);
            transactionData.Attributes.Append(attribute);

            attribute = document.CreateAttribute("SuspenseCode");
            attribute.Value = (SystemType == WorkflowSystemType.ALBPM_6 || SystemType == WorkflowSystemType.ALBPM_6_WITH_PStar) ? "SuspendWorkflow" : "SuspendForLCSurvey";
            transactionData.Attributes.Append(attribute);

            attribute = document.CreateAttribute((SystemType == WorkflowSystemType.ALBPM_6 || SystemType == WorkflowSystemType.ALBPM_6_WITH_PStar) ? "ProcessStatus" : "Status");
            attribute.Value = this.status;
            transactionData.Attributes.Append(attribute);

            attributes.AppendChild(transactionData);

            transaction.AppendChild(attributes);

            /*
             * add the transaction note
             */
            if (this.instanceNote != null && this.instanceNote.Length > 0)
            {
                XmlNode noteRecord = BuildNoteElement(document, NoteScope.Transaction, this.instanceNote, VA3iBusinessProcess.account);
                transaction.AppendChild(noteRecord);
            }

            return transaction;
        }

        /// <summary>
        /// Overridden. Not used.
        /// </summary>
        /// <param name="document">The document to which the note will be
        /// appended.</param>
        /// <returns>An <see cref="XmlElement"/> containing the note
        /// information.</returns>
        protected override XmlNode BuildNote(XmlDocument document)
        {
            /*
             * short circuit if the document is invalid
             */
            if (document == null || this.policyNote == null || this.policyNote.Trim().Length == 0)
                return null;

            /*
             * build the note element
             */
            XmlNode noteRecord = BuildNoteElement(document, NoteScope.Policy, this.policyNote, VA3iBusinessProcess.account);

            /*
			 * set the policy or submission
			 */
            XmlElement policyData = document.CreateElement("PolicyAttrData");
            XmlAttribute attribute = null;
            if (this.policy != null) //policy
            {
                attribute = document.CreateAttribute("PolicyNumber");
                attribute.Value = this.policy.Number.Replace("_P", "").Replace("_", "");
                policyData.Attributes.Append(attribute);
            }
            else if (this.submissionNumber != null)
            {
                attribute = document.CreateAttribute("SubmissionNum");
                attribute.Value = this.submissionNumber;
                policyData.Attributes.Append(attribute);
            }
            else
            {
                return null;
            }

            noteRecord.AppendChild(policyData);

            return noteRecord;
        }
        #endregion
    }
}