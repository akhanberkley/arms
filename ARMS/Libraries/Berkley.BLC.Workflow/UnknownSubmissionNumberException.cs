/*===========================================================================
  File:			UnknownSubmissionNumberException.cs
  
  Summary:		Thrown when an invalid or otherwise unknown submission number is 
				provided to a process.
			
  Classes:		UnknownSubmissionNumberException
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace Berkley.Workflow
{
	/// <summary>
	/// Thrown when an invalid or otherwise unknown submission number is 
	/// provided to a process.
	/// </summary>
	[Serializable()]
	public class UnknownSubmissionNumberException : WorkflowException
	{
		#region constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="UnknownSubmissionNumberException"/> 
		/// class.
		/// </summary>
		public UnknownSubmissionNumberException() : base(Resource.GetString("UnknownSubmissionNumberExceptionMessage"))
		{
			// stub. use base implmentation.
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="UnknownSubmissionNumberException"/>
		/// class with a specified error message.
		/// </summary>
		/// <param name="message">The message that describes the error.</param>
		public UnknownSubmissionNumberException(string message): base(message) 
		{
			// stub. use base implmentation.
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="UnknownSubmissionNumberException"/> 
		/// class with a specified error message and a reference to the inner 
		/// exception that is the cause of this exception.
		/// </summary>
		/// <param name="message">The error message that explains the reason for 
		/// the exception.</param>
		/// <param name="innerException">The exception that is the cause of the 
		/// current exception. If the innerException parameter is not a null 
		/// reference, the current exception is raised in a catch block that 
		/// handles the inner exception.</param>
		public UnknownSubmissionNumberException(string message, Exception innerException): base (message, innerException)
		{
			// stub. use base implmentation.
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="UnknownSubmissionNumberException"/>
		/// class with serialized data.
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> that holds 
		/// the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="StreamingContext"/> that 
		/// contains contextual information about the source or destination.</param>
		protected UnknownSubmissionNumberException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// stub. use base implmentation.
		}
		#endregion

		#region methods
		#endregion
	}
}
