/*===========================================================================
  File:			VA3iBusinessProcess.cs
  
  Summary:		Abstract base class for business processes.
			
  Classes:		VA3iBusinessProcess
  
  Enumerations:	None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.Configuration;
using System.Globalization;
using System.Xml;
using Berkley.BLC.Core;
using Berkley.BLC.Entities;
using Debug = System.Diagnostics.Debug;
using VA3i = Berkley.Workflow.VA3iService.DWImport;

namespace Berkley.Workflow
{
	/// <summary>
	/// Abstract base class for VA3i business processes.
	/// </summary>
	public abstract class VA3iBusinessProcess : BusinessProcess
	{
		#region fields
		private IContact requestor;
		private ISoftwareApplication system;
		private Company company;
        private WorkflowSystemType systemType;
		private string submissionNumber;

        /// <summary>
        /// The account name for notes.
        /// </summary>
        public const string account = "ARMS";
		#endregion

		#region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="requestor">The <see cref="IContact"/> that requested
		/// the transaction to be closed.</param>
		/// <param name="system">The <see cref="ISoftwareApplication"/> that
		/// requested the transaction to be closed.</param>
		/// <param name="company">The company to whom the transaction is
		/// associated.</param>
        /// <param name="systemType">The workflow system and version.</param>
		/// <param name="configuration">The software configuration in which the process will execute.</param>
		protected VA3iBusinessProcess(IContact requestor, ISoftwareApplication system, Company company, WorkflowSystemType systemType, ConfigurationType configuration) : base(configuration)
		{
			if (requestor == null || system == null || company == null)
				throw new InvalidParameterException();
			else
			{
				this.requestor = requestor;
				this.system = system;
				this.company = company;
                this.systemType = systemType;
				this.submissionNumber = string.Empty;
			}
		}
		#endregion

		#region properties
		/// <summary>
		/// Returns the submission number generated for the transaction by 
		/// the VA3i workflow system.
		/// </summary>
		public string Result
		{
			get
			{
				return this.submissionNumber;
			}
		}
		/// <summary>
		/// The <see cref="IContact"/> that requested
		/// the transaction to be closed.
		/// </summary>
		protected IContact Requestor
		{
			get
			{
				return this.requestor;
			}
		}
		/// <summary>
		/// The <see cref="ISoftwareApplication"/> that
		/// requested the transaction to be closed.
		/// </summary>
		protected ISoftwareApplication SoftwareApplication
		{
			get
			{
				return this.system;
			}
		}
		/// <summary>
		/// The company to whom the transaction is
		/// associated.
		/// </summary>
		protected Company Company
		{
			get
			{
				return this.company;
			}
		}
        /// <summary>
        /// The workflow system type and version.
        /// </summary>
        protected WorkflowSystemType SystemType
        {
            get
            {
                return this.systemType;
            }
        }
		/// <summary>
		/// A reference to the VA3i web service.
		/// </summary>
		protected VA3i Service
		{
			get
			{
				VA3i service = new VA3i();


				switch (Configuration)
				{
					case ConfigurationType.Production:
                        service.Url = "http://genie2-prd/GenieService/Import";
						break;
					case ConfigurationType.Development:
					case ConfigurationType.Integration:
					case ConfigurationType.Test:
					default:
                        service.Url = "http://genie2-tst/GenieService/Import";
						break;
				}

				return service;
			}
		}
		#endregion

		#region methods
		/// <summary>
		/// Executes the business process.
		/// </summary>
		public override void Run()
		{
			/*
			 * build the message
			 */
			XmlDocument message = BuildMessage();

			/*
			 * call the VA3i web service and hold the result
			 */
			try
			{
				using (VA3i service = Service)
				{
					string result = service.Submit(message.DocumentElement);
					this.submissionNumber = ParseSubmissionNumber(result);
				}
			}
			catch(System.Web.Services.Protocols.SoapException soapex)
			{
				throw new ConnectivityException(soapex);
			}
		}
		/// <summary>
		/// Builds the transaction close message.
		/// </summary>
		/// <returns>The transaction close message.</returns>
		protected virtual XmlDocument BuildMessage()
		{
			/*
			 * create the default namespace
			 */
			XmlNameTable nameTable = new NameTable();
			XmlDocument document = new XmlDocument(nameTable);

			/*
			 * create the document
			 */
			XmlNode root = BuildDocumentElement(document);
			document.AppendChild(root);

			/*
			 * add the request element
			 */
			root.AppendChild(BuildRequestElement(document));

			/*
			 * add the batch
			 */
			root.AppendChild(BuildBatch(document));

			/*
			 * return the result
			 */
			return document;
		}
		/// <summary>
		/// Builds the root element of the transaction message.
		/// </summary>
		/// <param name="document">The <see cref="XmlDocument"/> to which the
		/// request will be appended.</param>
		/// <returns>The root element for the transaction message.</returns>
		protected virtual XmlNode BuildDocumentElement(XmlDocument document)
		{
			XmlNode root = document.CreateElement("VA3iImport");
			XmlAttribute attribute;

			attribute = document.CreateAttribute("xmlns");
			attribute.Value = "http://intranet.bts.wrbc/DWServices/DWImport";
			root.Attributes.Append(attribute);

			/*
			 * get the most up to date schema. note that if a breaking change
			 * is made to the wsdl, this process will also break. however,
			 * it will have broken anyway when the final call is made because
			 * the service's expectations have changed.
			 */
			string uri;
			using (VA3i service = Service)
			{
				try
				{
					uri = service.GetSchemaURL(Company.Number, company.FileNetDocClass);
				}
				catch(System.Web.Services.Protocols.SoapException)
				{
					/*
					 * the url could not be determined, so take our best guess.
					 */
                    uri = service.Url.Substring(0, service.Url.LastIndexOf('/')) + "/GENIe_" + Company.Number + "_" + company.FileNetDocClass + ".xsd";
				}
			}

			attribute = document.CreateAttribute("xsi", "schemaLocation", "http://www.w3.org/2001/XMLSchema-instance");
			attribute.Value = uri;
			root.Attributes.Append(attribute);

			attribute = document.CreateAttribute("DocCreateDate");
			attribute.Value = DetermineCurrentDate();
			root.Attributes.Append(attribute);
			
			attribute = document.CreateAttribute("DocClass");
            attribute.Value = company.FileNetDocClass;
			root.Attributes.Append(attribute);

			attribute = document.CreateAttribute("CompanyNumber");
			attribute.Value = company.Number;
			root.Attributes.Append(attribute);

			return root;
		}

		/// <summary>
		/// Builds the request element for the transaction message.
		/// </summary>
		/// <param name="document">The <see cref="XmlDocument"/> to which the
		/// request will be appended.</param>
		/// <returns>The request element for the transaction message.</returns>
		protected virtual XmlNode BuildRequestElement(XmlDocument document)
		{
			/*
			 * create the request
			 */
			XmlElement result = document.CreateElement("RequestInfo");
			XmlAttribute attribute;

			attribute = document.CreateAttribute("RequestCreateDate");
			attribute.Value = DetermineCurrentDate();
			result.Attributes.Append(attribute);
			
			/*
			 * add the requestor
			 */
			XmlElement requestorElement = document.CreateElement("Requestor");
			requestorElement.AppendChild(BuildContactElement(document, requestor));
			result.AppendChild(requestorElement);
			
			/*
			 * add the action succeeded contact
			 */
			XmlElement element = document.CreateElement("NotificationSuccess");
			attribute = document.CreateAttribute("RequestNotification");
			attribute.Value = "0";
			element.Attributes.Append(attribute);
			element.AppendChild(BuildContactElement(document, system.BusinessContact));
			result.AppendChild(element);

			/*
			 * add the action failed contacts
			 */
			element = document.CreateElement("NotificationFail");

			/*
			 * add the business contact
			 */
			XmlElement businessContactElement = document.CreateElement("BusinessContact");
			businessContactElement.AppendChild(BuildContactElement(document, system.BusinessContact));
			element.AppendChild(businessContactElement);

			/*
			 * add the technical contact
			 */
			XmlElement technicalContactElement = document.CreateElement("TechnicalContact");
			technicalContactElement.AppendChild(BuildContactElement(document, system.TechnicalContact));
			element.AppendChild(technicalContactElement);

			result.AppendChild(element);

			/*
			 * add the application that created the request
			 */
			element = document.CreateElement("RequestCreateType");

			XmlElement applicationElement = document.CreateElement("ApplicationName");
			applicationElement.InnerText = system.Name;
			element.AppendChild(applicationElement);
			
			result.AppendChild(element);

			return result;
		}
		/// <summary>
		/// Builds a contact element.
		/// </summary>
		/// <param name="document">The document to which the contact will be
		/// appended.</param>
		/// <param name="contact">The contact.</param>
		/// <returns>An <see cref="XmlElement"/> containing the contact
		/// information.</returns>
		protected virtual XmlNode BuildContactElement(XmlDocument document, IContact contact)
		{
			XmlNode result = document.CreateElement("Contact");
			XmlAttribute attribute;

			/*
			 * set the name, email, and phone. if the contact is null,
			 * the contact element will be returned, but the attributes
			 * will be empty. otherwise, they will contain the state of
			 * the contact that was provided.
			 */
			string name = string.Empty;
			string email = string.Empty;
			string phone = string.Empty;

			if (contact != null)
			{
				name = contact.Name;
				email = contact.EmailAddress;
				phone = contact.TelephoneNumber;
			}

			/*
			 * add the contact's name
			 */
			attribute = document.CreateAttribute("Name");
			attribute.Value = name;
			result.Attributes.Append(attribute);
			
			/*
			 * add the contact's email address
			 */
			attribute = document.CreateAttribute("Email");
			attribute.Value = email;
			result.Attributes.Append(attribute);

			/*
			 * add the contact's telephone number
			 */
			attribute = document.CreateAttribute("Phone");
			attribute.Value = phone;
			result.Attributes.Append(attribute);

			/*
			 * return the results
			 */
			return result;
		}
		/// <summary>
		/// Builds the batch.
		/// </summary>
		/// <param name="document">The document to which the batch will be
		/// appended.</param>
		/// <returns>An <see cref="XmlElement"/> containing the batch 
		/// information.</returns>
		protected virtual XmlNode BuildBatch(XmlDocument document)
		{
			XmlElement result = document.CreateElement("BatchInfo");

			XmlNode transaction = BuildTransaction(document);
			if (transaction != null)
				result.AppendChild(transaction);

			XmlNode note = BuildNote(document);
			if (note != null)
				result.AppendChild(note);

			return result;
		}
		/// <summary>
		/// Constructs a note element.
		/// </summary>
		/// <param name="document">The <see cref="XmlDocument"/> to which the note
		/// will be applied.</param>
		/// <param name="scope">The area of the document to which the note will
		/// be applied.</param>
		/// <param name="text">The text of the note.</param>
		/// <param name="noteAccount">The account of the note.</param>
		/// <returns>A note element.</returns>
		protected XmlNode BuildNoteElement(XmlDocument document, NoteScope scope, string text, string noteAccount)
		{
			XmlElement noteRecord = document.CreateElement("NoteRecord");

			XmlAttribute attribute = document.CreateAttribute("NoteDateTime");
			attribute.Value =  DateTime.Now.ToString("s");
			noteRecord.Attributes.Append(attribute);

			attribute = document.CreateAttribute("NoteScope");

			switch(scope)
			{
				case NoteScope.Account:
					attribute.Value = "Account";
					break;
				case NoteScope.Document:
					attribute.Value = "Document";
					break;
				case NoteScope.Policy:
					attribute.Value = "Policy";
					break;
				case NoteScope.Transaction:
					attribute.Value = "Transaction";
					break;
			}
			noteRecord.Attributes.Append(attribute);

			attribute = document.CreateAttribute("NoteType");
			attribute.Value = "UNW";
			noteRecord.Attributes.Append(attribute);

			attribute = document.CreateAttribute("NoteUser");
			attribute.Value = noteAccount;
			noteRecord.Attributes.Append(attribute);

			attribute = document.CreateAttribute("NoteValue");
			attribute.Value = text;
			noteRecord.Attributes.Append(attribute);

			return noteRecord;
		}
		/// <summary>
		/// Abstract. Builds a transaction element.
		/// </summary>
		/// <param name="document">The document to which the transaction will be
		/// appended.</param>
		/// <returns>An <see cref="XmlElement"/> containing the transaction 
		/// information.</returns>
		protected abstract XmlNode BuildTransaction(XmlDocument document);
		/// <summary>
		/// Abstract. Builds a note element.
		/// </summary>
		/// <param name="document">The document to which the note will be
		/// appended.</param>
		/// <returns>An <see cref="XmlElement"/> containing the note
		/// information.</returns>
		protected abstract  XmlNode BuildNote(XmlDocument document);
		/// <summary>
		/// Returns the current date formatted according to the xsd:date
		/// specification.
		/// </summary>
		/// <returns>The current date formatted according to the xsd:date
		/// specification.</returns>
		protected static string DetermineCurrentDate()
		{
			return DateTime.Now.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
		}
		/// <summary>
		/// Removes the extraneous characters from the Submit() web service 
		/// result.
		/// </summary>
		/// <param name="submissionNumber">The submission number to parse.</param>
		/// <returns>A purified submission number.</returns>
		private static string ParseSubmissionNumber(string submissionNumber)
		{
			/*
			 * when a submission number is returned from the service, it is
			 * formatted more for human consumption than machine. to parse the
			 * actual submission number, the prefix and suffix information 
			 * is removed.
			 */
			string result = submissionNumber.Replace("Accepted Submission #", string.Empty);
			result = result.Substring(0, result.IndexOf(':'));

			return result;
		}
		/// <summary>
		/// Converts a VA3i or ALBPM submission number into its related transaction number.
		/// </summary>
		/// <param name="submissionNumber">A valid submission number.</param>
		/// <returns>The transaction number associated with the provided submission
		/// number.</returns>
		protected string DetermineTransactionNumber(string submissionNumber)
		{
			string result = string.Empty;

			try
			{
				XmlNode node = null;
				using (VA3i service = this.Service)
				{
					node = service.GetSubmissionStatus(submissionNumber);
				}

				/*
				 * GetSubmissionStatus returns xml with details about the submission.
				 * the only node of concern to this activity is the ID attribute 
				 * of the Transaction element.
				 */
				if (node != null)
				{
					XmlNode transaction = node.SelectSingleNode(@"Transaction/@ID");
					if (transaction != null)
						result = transaction.InnerText;
				}
			}
			catch(System.Web.Services.Protocols.SoapException soapex)
			{
				/*
				 * either the submission number was not found or the service 
				 * is down or othewise unresponsive.
				 * 
				 * HACK: parse the SoapException message. If it uses the word
				 * 'Submission', treat it like an UnknownSubmissionNumberException.
				 * Otherwise, treat it as a connectivity issue.
				 */
				if (soapex.Message.IndexOf("Submission", 0, soapex.Message.Length) >= 0)
					throw new UnknownSubmissionNumberException("See inner exception for details.", soapex);
				else
					throw new ConnectivityException(soapex);
			}

			if (result.Length > 0)
			{
                try
                {
                    //sample: "/BTS/Regional/BMAG/BMAGAudit#Default-1.2/23516/0"
                    
                    //remove the characters after and including the last slash
                    int length = result.Length - result.LastIndexOf("/");
                    result = result.Remove((result.Length - length), length);

                    //remove all the characters till the last remaining slash
                    result = result.Remove(0, (result.LastIndexOf("/") + 1));
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Submission number ({0}) corresponded to an invalid ALBPM transaction number to parse.", submissionNumber), ex);
                }
			}

			return result;
		}

		/// <summary>
		/// Builds the document type element.
		/// </summary>
		/// <param name="document">The <see cref="XmlDocument"/> from which to 
		/// construct the document type.</param>
		/// <param name="docType">The document type code.</param>
		/// <returns>An <see cref="XmlAttribute"/> representing the document type.</returns>
		protected XmlAttribute BuildDocumentType(XmlDocument document, string docType)
		{
			XmlAttribute attribute = document.CreateAttribute("DocType");
			attribute.Value = docType;
			
			return attribute;
		}

        /// <summary>
        /// Builds the document remarks element.
        /// </summary>
        /// <param name="document">The <see cref="XmlDocument"/> from which to 
        /// construct the document type.</param>
        /// <param name="docRemarks">The document type code.</param>
        /// <returns>An <see cref="XmlAttribute"/> representing the document type.</returns>
        protected XmlAttribute BuildDocumentRemarks(XmlDocument document, string docRemarks)
        {
            XmlAttribute attribute = document.CreateAttribute("DocRemarks");
            attribute.Value = docRemarks;

            return attribute;
        }

        /// <summary>
        /// Builds the effective date element.
        /// </summary>
        /// <param name="document">The <see cref="XmlDocument"/> from which to 
        /// construct the document type.</param>
        /// <param name="policy">The policy.</param>
        /// <returns>An <see cref="XmlAttribute"/> representing the effective date.</returns>
        protected XmlAttribute BuildEffectiveDate(XmlDocument document, IPolicy policy)
        {
            XmlAttribute attribute = document.CreateAttribute("EffectiveDate");
            attribute.Value = policy.EffectiveDate.ToString("yyyy-MM-dd");
            return attribute;
        }

        /// <summary>
        /// Builds the expiration date element.
        /// </summary>
        /// <param name="document">The <see cref="XmlDocument"/> from which to 
        /// construct the document type.</param>
        /// <param name="policy">The policy.</param>
        /// <returns>An <see cref="XmlAttribute"/> representing the effective date.</returns>
        protected XmlAttribute BuildExpirationDate(XmlDocument document, Policy policy)
        {
            XmlAttribute attribute = document.CreateAttribute("ExpDate");
            attribute.Value = policy.ExpireDate.ToString("yyyy-MM-dd");
            return attribute;
        }
		#endregion
	}
}