using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Permissions;

/*
 * description
 */
[assembly: AssemblyTitle("Berkley.Workflow.dll")]
[assembly: AssemblyDescription("Berkley.Workflow.dll")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Berkley Technology Services, LLC.")]
[assembly: AssemblyProduct("Berkley Technology Services Workflow Library")]
[assembly: AssemblyCopyright("Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

/*
 * interop
 */
[assembly: CLSCompliant(false)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]

/*
 * security
 */
/*
[assembly: SecurityPermission(SecurityAction.RequestOptional, Execution = true)]
[assembly: PermissionSet(SecurityAction.RequestOptional, Name = "Nothing")]
*/

/*
 * version
 */
[assembly: AssemblyVersion("1.0.0")]

/*
 * signature
 */
[assembly: AssemblyDelaySign(false)]
//[assembly: AssemblyKeyFile(@"..\..\AssemblyKey.snk")]
[assembly: AssemblyKeyName("")]