/*===========================================================================
  File:			BusinessProcess.cs
  
  Summary:		Abstract base class for business processes.
			
  Classes:		BusinessProcess
  
  Enumerations:	None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using Berkley.BLC.Core;

namespace Berkley.Workflow
{
    /// <summary>
    /// Abstract base class for business processes.
    /// </summary>
    public abstract class BusinessProcess
    {
        #region fields
        ConfigurationType configuration = ConfigurationType.Development;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        protected BusinessProcess(ConfigurationType configuration)
        {
            this.configuration = configuration;
        }
        #endregion

        #region properties
        /// <summary>
        /// The software configuration in which the process will execute.
        /// </summary>
        public ConfigurationType Configuration
        {
            get
            {
                return configuration;
            }
        }
        #endregion

        #region methods
        /// <summary>
        /// Executes the business process.
        /// </summary>
        public abstract void Run();
        #endregion
    }
}