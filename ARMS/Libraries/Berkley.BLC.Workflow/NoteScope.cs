using System;

namespace Berkley.Workflow
{
	/// <summary>
	/// A list of the areas of a process document to which a note may be
	/// applied.
	/// </summary>
	public enum NoteScope
	{
		/// <summary>
		/// The account area of the process document.
		/// </summary>
		Account,
		/// <summary>
		/// The document area of the process document.
		/// </summary>
		Document,
		/// <summary>
		/// The policy area of the process document.
		/// </summary>
		Policy,
		/// <summary>
		/// The transaction area of the process document.
		/// </summary>
		Transaction
	}
}