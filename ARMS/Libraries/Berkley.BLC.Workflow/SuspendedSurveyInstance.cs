/*===========================================================================
  File:			SuspendedSurveyInstance.cs
  
  Summary:		Creates a suspended survey instance in the ALBPM workflow system.

  Classes:		SuspendedSurveyInstance
  
  Enumerations:	None
  
  Interfaces:	None
  
  Origin:		Loss Control Tracking System (September, 2007)
  
=============================================================================
  Copyright (c) 2007 Berkley Technology Services, LLC. All rights reserved.
 =============================================================================*/

using System;
using System.Xml;
using Berkley.BLC.Core;
using Berkley.BLC.Entities;

namespace Berkley.Workflow
{
    /// <summary>
    /// Creates a suspended survey instance in the ALBPM workflow system.
    /// </summary>
    public class SuspendedSurveyInstance : VA3iBusinessProcess
    {
        #region fields
        private Policy policy;
        private byte[] image;
        private string docType;
        private string transactionType;
        private string processType;
        private string status;
        private string fileExtension;
        private string policyNote;
        private string instanceNote;
        private string submissionNumber;
        private string docRemarks;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="requestor">The <see cref="IContact"/> that requested
        /// the transaction to be opened.</param>
        /// <param name="system">The <see cref="ISoftwareApplication"/> that
        /// requested the transaction to be opened.</param>
        /// <param name="company">The company to whom the transaction is
        /// associated.</param>
        /// <param name="systemType">The workflow system and version.</param>
        /// <param name="policy">The insurance policy to which the transaction
        /// is associated.</param>
        /// <param name="image">The image to attach.</param>
        /// <param name="docType">The document type code that is expected by the workflow system.</param>
        /// <param name="transactionType">The type of transaction.</param>
        /// <param name="processType">The process type for the transaction.</param>
        /// <param name="status">The status of the transaction.</param>
        /// <param name="fileExtension">The file extension of the image.</param>
        /// <param name="policyNote">A policy note.</param>
        /// <param name="instanceNote">A transaction note.</param>
        /// <param name="configuration">The software configuration in which the process will execute.</param>
        /// <param name="submissionNumber">The submission number.</param>
        /// <param name="docRemarks">The doc remarks.</param>
        public SuspendedSurveyInstance(
            IContact requestor, ISoftwareApplication system,
            Company company, WorkflowSystemType systemType, Policy policy,
            byte[] image, string docType,
            string transactionType, string processType, string status, string fileExtension, string policyNote, string instanceNote, ConfigurationType configuration, string submissionNumber, string docRemarks)
            : base(requestor, system, company, systemType, configuration)
        {
            if (policy != null)
            {
                this.policy = policy;
            }
            else if (submissionNumber != null && submissionNumber.Trim().Length > 0)
            {
                this.submissionNumber = submissionNumber;
            }
            else
            {
                throw new InvalidParameterException();
            }

            if (image == null || image.Length == 0)
                throw new InvalidParameterException("image");
            else
                this.image = image;

            if (status != null && status.Trim().Length > 0)
                this.status = status;
            else
                throw new InvalidParameterException();

            this.processType = processType;
            this.docType = docType;
            this.transactionType = transactionType;
            this.fileExtension = fileExtension;
            this.policyNote = policyNote;
            this.instanceNote = instanceNote;
            this.docRemarks = docRemarks;
        }
        #endregion

        #region properties
        #endregion

        #region methods
        /// <summary>
        /// Overridden. Builds a open transaction element.
        /// </summary>
        /// <param name="document">The document to which the transaction will be
        /// appended.</param>
        /// <returns>An <see cref="XmlElement"/> containing the transaction 
        /// information.</returns>
        protected override XmlNode BuildTransaction(XmlDocument document)
        {
            /*
			 * short circuit if the document is invalid
			 */
            if (document == null || (this.policy == null && this.submissionNumber == null))
                return null;

            XmlElement transaction = document.CreateElement("Transaction");
            XmlElement attributes = document.CreateElement("TranAttributes");

            XmlElement policyData = document.CreateElement("PolicyAttrData");
            XmlAttribute attribute = null;
            if (this.policy != null) //policy
            {
                //policy number
                attribute = document.CreateAttribute("PolicyNumber");
                attribute.Value = this.policy.Number.Replace("_P", "").Replace("_", "");
                policyData.Attributes.Append(attribute);

                //policy effective date
                attribute = BuildEffectiveDate(document, policy);
                policyData.Attributes.Append(attribute);

                //a hack to put in place since p* company needs more data elements passed
                if (SystemType == WorkflowSystemType.ALBPM_6_WITH_PStar)
                {
                    //policy expiration date
                    attribute = BuildExpirationDate(document, policy);
                    policyData.Attributes.Append(attribute);

                    attribute = document.CreateAttribute("InsuredName");
                    attribute.Value = this.policy.Insured.Name;
                    policyData.Attributes.Append(attribute);

                    attribute = document.CreateAttribute("Account");
                    attribute.Value = this.policy.Insured.ClientID;
                    policyData.Attributes.Append(attribute);

                    attribute = document.CreateAttribute("AgencyNumber");
                    attribute.Value = (policy.Insured.Agency != null) ? policy.Insured.Agency.Number : string.Empty;
                    policyData.Attributes.Append(attribute);

                    attribute = document.CreateAttribute("AgencyName");
                    attribute.Value = (policy.Insured.Agency != null) ? policy.Insured.Agency.Name : string.Empty;
                    policyData.Attributes.Append(attribute);
                }
            }
            else if (this.submissionNumber != null) //submission number
            {
                attribute = document.CreateAttribute("SubmissionNum");
                attribute.Value = this.submissionNumber;
                policyData.Attributes.Append(attribute);
            }
            else
            {
                return null;
            }

            attributes.AppendChild(policyData);

            /*
             * set the type of document to which this transaction applies.
             */
            XmlElement transactionData = document.CreateElement("TranAttrData");
            attribute = document.CreateAttribute("TranType");
            attribute.Value = this.transactionType;
            transactionData.Attributes.Append(attribute);

            if (SystemType == WorkflowSystemType.ALBPM_6 || SystemType == WorkflowSystemType.ALBPM_6_WITH_PStar)
            {
                attribute = document.CreateAttribute("ProcessType");
                attribute.Value = this.processType;
                transactionData.Attributes.Append(attribute);

                attribute = document.CreateAttribute("ProcessStatus");
                attribute.Value = this.status;
                transactionData.Attributes.Append(attribute);
            }
            else
            {
                attribute = document.CreateAttribute("Status");
                attribute.Value = this.status;
                transactionData.Attributes.Append(attribute);
            }

            attributes.AppendChild(transactionData);

            transaction.AppendChild(attributes);

            /*
             * add the transaction note
             */
            if (this.instanceNote != null && this.instanceNote.Length > 0)
            {
                XmlNode noteRecord = BuildNoteElement(document, NoteScope.Transaction, this.instanceNote, VA3iBusinessProcess.account);
                transaction.AppendChild(noteRecord);
            }

            /*
             * set the document information
             */
            if (this.image != null && this.image.Length > 0)
            {
                XmlElement documentRecord = document.CreateElement("DocumentRecord");

                attribute = document.CreateAttribute("FileExtension");
                attribute.Value = this.fileExtension;
                documentRecord.Attributes.Append(attribute);

                attribute = document.CreateAttribute("Primary");
                attribute.Value = "1";
                documentRecord.Attributes.Append(attribute);

                attribute = document.CreateAttribute("Zipped");
                attribute.Value = "0";
                documentRecord.Attributes.Append(attribute);

                attribute = document.CreateAttribute("xsi", "type", "http://www.w3.org/2001/XMLSchema-instance");
                attribute.Value = "Internal";
                documentRecord.Attributes.Append(attribute);

                /*
                 * set the document type and remarks
                 */
                XmlElement documentAttributeData = document.CreateElement("DocAttrData");

                attribute = BuildDocumentType(document, this.docType);
                documentAttributeData.Attributes.Append(attribute);

                if (docRemarks != null && docRemarks.Trim().Length > 0)
                {
                    attribute = BuildDocumentRemarks(document, docRemarks);
                    documentAttributeData.Attributes.Append(attribute);
                }

                documentRecord.AppendChild(documentAttributeData);

                /*
                 * add the image
                 */
                XmlElement documentData = document.CreateElement("DocumentData");
                documentData.InnerText = Convert.ToBase64String(this.image);
                documentRecord.AppendChild(documentData);

                transaction.AppendChild(documentRecord);
            }

            return transaction;
        }

        /// <summary>
        /// Overridden. Not used.
        /// </summary>
        /// <param name="document">The document to which the note will be
        /// appended.</param>
        /// <returns>An <see cref="XmlElement"/> containing the note
        /// information.</returns>
        protected override XmlNode BuildNote(XmlDocument document)
        {
            /*
             * short circuit if the document is invalid
             */
            if (document == null || this.policyNote == null || this.policyNote.Trim().Length == 0)
                return null;

            /*
             * build the note element
             */
            XmlNode noteRecord = BuildNoteElement(document, NoteScope.Policy, this.policyNote, VA3iBusinessProcess.account);

            /*
			 * set the policy or submission
			 */
            XmlElement policyData = document.CreateElement("PolicyAttrData");
            XmlAttribute attribute = null;
            if (this.policy != null) //policy
            {
                attribute = document.CreateAttribute("PolicyNumber");
                attribute.Value = this.policy.Number.Replace("_P", "").Replace("_", "");
                policyData.Attributes.Append(attribute);
            }
            else if (this.submissionNumber != null)
            {
                attribute = document.CreateAttribute("SubmissionNum");
                attribute.Value = this.submissionNumber;
                policyData.Attributes.Append(attribute);
            }
            else
            {
                return null;
            }

            noteRecord.AppendChild(policyData);

            return noteRecord;
        }
        #endregion
    }
}