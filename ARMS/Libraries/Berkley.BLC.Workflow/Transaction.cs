/*===========================================================================
  File:			Transaction.cs
  
  Summary:		A class for getting transaction information.

  Classes:		Transaction
  
  Enumerations:	None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (January, 2007)
  
=============================================================================
  Copyright (c) 2006 Berkley Technology Services, LLC. All rights reserved.
 =============================================================================*/

using System;
using System.Xml;
using Berkley.BLC.Core;
using Berkley.BLC.Entities;

namespace Berkley.Workflow
{
	/// <summary>
	/// A class for getting transaction information.
	/// </summary>
	public class Transaction : VA3iBusinessProcess
	{
		#region fields
		private string id;
		#endregion

		#region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		/// <param name="requestor">The <see cref="IContact"/> that requested
		/// the transaction to be closed.</param>
		/// <param name="system">The <see cref="ISoftwareApplication"/> that
		/// requested the transaction to be closed.</param>
		/// <param name="company">The company to whom the transaction is
		/// associated.</param>
        /// <param name="systemType">The workflow system and version.</param>
		/// <param name="submissionNumber">The submission number of a VA3i 
		/// transaction to which the document should be attached.</param>
		/// <param name="configuration">The software configuration in which the process will execute.</param>
		public Transaction(
			IContact requestor, ISoftwareApplication system, Company company, WorkflowSystemType systemType, 
			string submissionNumber,ConfigurationType configuration) 
			: base(requestor, system, company, systemType, configuration)
		{
			if (submissionNumber == null || submissionNumber.Trim().Length == 0)
				throw new ArgumentException("A submission number is required.", "submissionNumber");

			this.id = base.DetermineTransactionNumber(submissionNumber);
		}
		#endregion

		#region properties
		/// <summary>
		/// Gets the transaction id from the submission number.
		/// </summary>
		public string ID
		{
			get
			{
				return this.id;
			}
		}

		#endregion

		#region methods
		/// <summary>
		/// Overridden. Builds a close transaction element.
		/// </summary>
		/// <param name="document">The document to which the transaction will be
		/// appended.</param>
		/// <returns>An <see cref="XmlElement"/> containing the transaction 
		/// information.</returns>
		protected override XmlNode BuildTransaction(XmlDocument document)
		{
			return null;
		}
		/// <summary>
		/// Overridden. Not used.
		/// </summary>
		/// <param name="document">The document to which the note will be
		/// appended.</param>
		/// <returns>An <see cref="XmlElement"/> containing the note
		/// information.</returns>
		protected override XmlNode BuildNote(XmlDocument document)
		{
			return null;
		}
		#endregion
	}
}