using System;
using Berkley.BLC.Entities;
using Wilson.ORMapper;
using System.IO;

namespace Berkley.BLC.Business.CompanySpecific
{
    /// <summary>
    /// Concrete class that implements SurveyManager functions for BNPG.
    /// </summary>
    public class SurveyManagerBNPG: SurveyManager
    {
        private static readonly WorkflowHelper workflow;

        static SurveyManagerBNPG()
        {
            workflow = new WorkflowHelper(Company.BerkleyAgribusinessRiskSpecialists, Berkley.Workflow.WorkflowSystemType.ALBPM_5);
        }

        internal SurveyManagerBNPG(Survey survey, User currentUser, bool overridePermissions)
            : base(survey, currentUser, overridePermissions)
        {
        }

        /// <summary>
        /// Overridden. Returns the workflow provider.
        /// </summary>
        protected override WorkflowHelper Workflow
        {
            get
            {
                return workflow;
            }
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnCloseAndNotifyComplete(Transaction trans, Survey survey, string explainText, User currentUser, string recRount)
        {
            Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

            EmailHelper email = new EmailHelper(survey, currentUser);
            email.SendEmailToUnderwriter(EmailHelper.NotificationType.ClosedSurvey, underwriter, string.Empty, explainText, recRount);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCancelComplete(Transaction trans, Survey survey, string explainText, User currentUser)
        {
            Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

            EmailHelper email = new EmailHelper(survey, currentUser);

            if (survey.ConsultantUser != null || survey.AssignedByUser != null)
            {
                email.SendEmailToConsultant(EmailHelper.NotificationType.CancelledSurvey, explainText);
            }

            email.SendEmailToUnderwriter(EmailHelper.NotificationType.CancelledSurvey, underwriter, "planderson@berkleynpac.com", explainText, string.Empty);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnSurveyReviewComplete(Transaction trans, Survey survey, User currentUser)
        {
            EmailHelper email = new EmailHelper(survey, currentUser);
            email.SendEmailToConsultant(EmailHelper.NotificationType.CompletedQCR, string.Empty);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override string OnAttachDocument(Transaction trans, Survey survey, string fileName, string mimeType, Stream contentStream, DocType docType, string docRemarks)
        {
            string fileNetRef = string.Empty;
            try
            {
                // remove any pathing info from the specified filename
                fileName = Path.GetFileName(fileName);

                // determine which policy to use
                Policy policy = null;
                if (survey.PrimaryPolicy != null && survey.PrimaryPolicy.ExpireDate != DateTime.MinValue)
                {
                    policy = survey.PrimaryPolicy;
                }
                else //prospect survey
                {
                    Policy[] policies = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && Active = true]", "ExpireDate DESC", survey.ID);
                    if (policies.Length > 0 && policies[0].ExpireDate != DateTime.MinValue)
                    {
                        policy = policies[0];
                    }
                }

                // save the doc to the imaging server and get the FileNet reference
                Company company = this.Survey.Company.GetWritableInstance();
                if (this.Survey.Company.MilestoneDate == DateTime.MinValue || this.Survey.Company.MilestoneDate > DateTime.Today)
                {
                    company.Number = "40";
                }

                string docTypeDescription = (docType != null) ? docType.Description : String.Empty;
                ImagingHelper imaging = new ImagingHelper(company);
                fileNetRef = imaging.StoreFileImage(fileName, mimeType, contentStream, "LCR", docTypeDescription, (survey.SurveyedDate != DateTime.MinValue) ? survey.SurveyedDate : DateTime.Today, (policy != null) ? policy.ExpireDate : DateTime.MinValue, DateTime.Now, survey.Insured.ClientID, policy, survey.WorkflowSubmissionNumber);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save file '" + fileName + "' of type '" + mimeType + "' to FileNet.  See inner exception for details.", ex);
            }

            return fileNetRef;
        }

        protected override void OnAttachDocumentComplete(Survey survey, string documentReference, DocType docType,User currentUser)
        {
           Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

            EmailHelper email = new EmailHelper(survey, currentUser);
            if(docType.Code == "RECS")
            {
                string text = string.Format("NEW CRITICAL Recommendations # {0}.", survey.Number);
                string ccEmailAddresses = "imaging@berkleynpac.com";

                email.SendEmailToUnderwriter(EmailHelper.NotificationType.AttachDocument, underwriter, ccEmailAddresses, text, string.Empty);
            }
        }

        #region SendForReview - BNPG has their field reps do all of the review work (i.e. send letters).

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected override Survey SendForReview(Transaction trans, Survey survey, bool futureVisitNeeded, bool reviewNeeded, SurveyType futureSurveyType, User futureUser, DateTime futureDate, string futurePurpose)
        {
            string historyEntry = string.Empty;
            survey.ConsultantUserID = survey.AssignedUserID;
            survey.StatusCode = SurveyStatus.ClosedSurvey.Code;
            survey.CompleteDate = DateTime.Today;
            historyEntry += "Report has been completed, underwriting has been notified, and the survey has been closed.";

            //New Future Visit
            Survey newSurvey = null;
            if (futureVisitNeeded && !survey.FutureVisitNeeded)
            {
                survey.FutureVisitNeeded = futureVisitNeeded;
                survey.FutureVisitSurveyTypeID = futureSurveyType.ID;
                survey.FutureVisitDueDate = futureDate;

                //Append to the prior history entry
                historyEntry += string.Format(" Future survey created in ARMS with a due date of {0}.", futureDate.ToShortDateString());

                //Create new survey
                newSurvey = new Survey(new Guid());
                newSurvey.CompanyID = survey.CompanyID;
                newSurvey.InsuredID = survey.InsuredID;
                newSurvey.PrimaryPolicyID = survey.PrimaryPolicyID;
                newSurvey.LocationID = survey.LocationID;
                newSurvey.PreviousSurveyID = survey.ID;
                newSurvey.TypeID = futureSurveyType.ID;
                newSurvey.AssignedUserID = futureUser.ID;
                newSurvey.ServiceTypeCode = survey.ServiceTypeCode;
                newSurvey.StatusCode = SurveyStatus.AssignedSurvey.Code;
                newSurvey.CreateStateCode = CreateState.AssignedToConsultant.Code;
                newSurvey.CreateByInternalUserID = survey.AssignedUserID;
                newSurvey.CreateDate = DateTime.Now;
                newSurvey.AssignDate = DateTime.Now;
                newSurvey.AcceptDate = DateTime.Now;
                newSurvey.DueDate = futureDate;
                newSurvey.RecsRequired = !SurveyTypeType.IsNotWrittenBusiness(futureSurveyType.Type);
                newSurvey.WorkflowSubmissionNumber = survey.WorkflowSubmissionNumber;
                newSurvey.UnderwriterCode = survey.UnderwriterCode;
                newSurvey.Priority = survey.Priority;
                newSurvey.Save(trans);

                //Include all the Policies/Coverages from the original survey
                SurveyPolicyCoverage[] oldCoverages = SurveyPolicyCoverage.GetArray("SurveyID = ?", survey.ID);
                foreach (SurveyPolicyCoverage oldCoverage in oldCoverages)
                {
                    SurveyPolicyCoverage newCoverage = new SurveyPolicyCoverage(newSurvey.ID, oldCoverage.LocationID, oldCoverage.PolicyID, oldCoverage.CoverageNameID);
                    newCoverage.ReportTypeCode = oldCoverage.ReportTypeCode;
                    newCoverage.Active = oldCoverage.Active;
                    newCoverage.Save(trans);
                }

                //Create history entry
                string newEntry = string.Format("Survey created in ARMS by {0} from survey #{1}.", GetCurrentUserName(), survey.Number);
                WriteHistoryEntry(trans, newSurvey, newEntry);

                //Add the purpose as a comment
                if (futurePurpose != string.Empty)
                {
                    SurveyNote note = new SurveyNote(new Guid());
                    note.SurveyID = newSurvey.ID;
                    note.UserID = CurrentUser.ID;
                    note.EntryDate = DateTime.Now;
                    note.InternalOnly = false;
                    note.AdminOnly = false;
                    note.Comment = string.Format("Purpose of Future Survey: {0}", futurePurpose);

                    note.Save(trans);
                }
            }

            //set the report complete date
            //Business Rule:  Do not reset the report complete date if this is a correction (don't want to add extra time).
            if (!survey.Correction && survey.ReportCompleteDate == DateTime.MinValue)
            {
                survey.ReportCompleteDate = DateTime.Now;
            }

            // save our changes
            SaveSurvey(trans, survey, historyEntry);

            return newSurvey;
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnSendForReviewComplete(Transaction trans, Survey currentSurvey, Survey futureSurvey, bool isNewVisit, bool reviewNeeded, string uwInstructions)
        {
            Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", currentSurvey.CompanyID, currentSurvey.UnderwriterCode);

            EmailHelper email = new EmailHelper(currentSurvey, currentSurvey.AssignedUser);
            email.SendEmailToUnderwriter(EmailHelper.NotificationType.CompletedSurvey, underwriter, string.Empty, string.Empty, string.Empty);
        }

        #endregion

    }
}