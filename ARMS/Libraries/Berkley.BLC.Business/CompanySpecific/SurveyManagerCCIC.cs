using System;
using Berkley.BLC.Entities;
using Wilson.ORMapper;
using System.IO;

namespace Berkley.BLC.Business.CompanySpecific
{
    /// <summary>
    /// Concrete class that implements SurveyManager functions for CCIC.
    /// </summary>
    public class SurveyManagerCCIC : SurveyManager
    {
        private static readonly WorkflowHelper workflow;
        private static readonly string docRemarks;

        static SurveyManagerCCIC()
        {
            workflow = new WorkflowHelper(Company.CarolinaCasualtyInsuranceGroup,  Berkley.Workflow.WorkflowSystemType.ALBPM_6_WITH_PStar);
            docRemarks = "ARMS";
        }

        internal SurveyManagerCCIC(Survey survey, User currentUser, bool overridePermissions)
            : base(survey, currentUser, overridePermissions)
        {
        }

        /// <summary>
        /// Overridden. Returns the workflow provider.
        /// </summary>
        protected override WorkflowHelper Workflow
        {
            get
            {
                return workflow;
            }
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override Survey OnCreateSurveyComplete(Transaction trans, Survey survey)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            // notify the workflow system we have this survey by creating a new suspended instance
            // note: this will record the reference number in our survey for future communication with workflow
            Workflow.CreateSuspendedInstance(survey, "LOSS PREVENTION - REPORT", "CCICUWWorkflow", "Loss Prevention", "Waiting for Report", null, NewSurveyNoteText, docRemarks);
            return survey;
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCreateVisitComplete(Transaction trans, Survey survey)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            // notify the workflow system we have this survey by creating a new suspended instance
            // note: this will record the reference number in our survey for future communication with workflow
            Workflow.CreateSuspendedInstance(survey, "LOSS PREVENTION - REPORT", "CCICUWWorkflow", "Loss Prevention", "Waiting for Report", null, NewSurveyNoteText, docRemarks);
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCreateSurveyFromExistingSurveyComplete(Transaction trans, Survey survey)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            // notify the workflow system we have this survey by creating a new suspended instance
            // note: this will record the reference number in our survey for future communication with workflow
            Workflow.CreateSuspendedInstance(survey, "LOSS PREVENTION - REPORT", "CCICUWWorkflow", "Loss Prevention", "Waiting for Report", null, NewSurveyNoteText, docRemarks);
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected override Survey SendForReview(Transaction trans, Survey survey, bool futureVisitNeeded, bool reviewNeeded, SurveyType futureSurveyType, User futureUser, DateTime futureDate, string futurePurpose)
        {
            string historyEntry = string.Empty;
            survey.ConsultantUserID = survey.AssignedUserID;
            survey.StatusCode = SurveyStatus.ClosedSurvey.Code;
            survey.CompleteDate = DateTime.Today;
            historyEntry += "Report has been completed and the survey has been closed.";

            //New Future Visit
            Survey newSurvey = null;
            if (futureVisitNeeded && !survey.FutureVisitNeeded)
            {
                survey.FutureVisitNeeded = futureVisitNeeded;
                survey.FutureVisitSurveyTypeID = futureSurveyType.ID;
                survey.FutureVisitDueDate = futureDate;

                //Append to the prior history entry
                historyEntry += string.Format(" Future survey created in ARMS with a due date of {0}.", futureDate.ToShortDateString());

                //Create new survey
                newSurvey = new Survey(new Guid());
                newSurvey.CompanyID = survey.CompanyID;
                newSurvey.InsuredID = survey.InsuredID;
                newSurvey.PrimaryPolicyID = survey.PrimaryPolicyID;
                newSurvey.LocationID = survey.LocationID;
                newSurvey.PreviousSurveyID = survey.ID;
                newSurvey.TypeID = futureSurveyType.ID;
                newSurvey.AssignedUserID = futureUser.ID;
                newSurvey.ServiceTypeCode = survey.ServiceTypeCode;
                newSurvey.StatusCode = SurveyStatus.AssignedSurvey.Code;
                newSurvey.CreateStateCode = CreateState.AssignedToConsultant.Code;
                newSurvey.CreateByInternalUserID = survey.AssignedUserID;
                newSurvey.CreateDate = DateTime.Now;
                newSurvey.AssignDate = DateTime.Now;
                newSurvey.AcceptDate = DateTime.Now;
                newSurvey.DueDate = futureDate;
                newSurvey.RecsRequired = !SurveyTypeType.IsNotWrittenBusiness(futureSurveyType.Type);
                newSurvey.WorkflowSubmissionNumber = survey.WorkflowSubmissionNumber;
                newSurvey.UnderwriterCode = survey.UnderwriterCode;
                newSurvey.Save(trans);

                //Include all the Policies/Coverages from the original survey
                SurveyPolicyCoverage[] oldCoverages = SurveyPolicyCoverage.GetArray("SurveyID = ?", survey.ID);
                foreach (SurveyPolicyCoverage oldCoverage in oldCoverages)
                {
                    SurveyPolicyCoverage newCoverage = new SurveyPolicyCoverage(newSurvey.ID, oldCoverage.LocationID, oldCoverage.PolicyID, oldCoverage.CoverageNameID);
                    newCoverage.ReportTypeCode = oldCoverage.ReportTypeCode;
                    newCoverage.Active = oldCoverage.Active;
                    newCoverage.Save(trans);
                }

                //Create history entry
                string newEntry = string.Format("Survey created in ARMS by {0} from survey #{1}.", GetCurrentUserName(), survey.Number);
                WriteHistoryEntry(trans, newSurvey, newEntry);

                //Add the purpose as a comment
                if (futurePurpose != string.Empty)
                {
                    SurveyNote note = new SurveyNote(new Guid());
                    note.SurveyID = newSurvey.ID;
                    note.UserID = CurrentUser.ID;
                    note.EntryDate = DateTime.Now;
                    note.InternalOnly = false;
                    note.AdminOnly = false;
                    note.Comment = string.Format("Purpose of Future Survey: {0}", futurePurpose);

                    note.Save(trans);
                }
            }

            //set the report complete date
            //Business Rule:  Do not reset the report complete date if this is a correction (don't want to add extra time).
            if (!survey.Correction && survey.ReportCompleteDate == DateTime.MinValue)
            {
                survey.ReportCompleteDate = DateTime.Now;
            }

            // save our changes
            SaveSurvey(trans, survey, historyEntry);

            return newSurvey;
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnSendForReviewComplete(Transaction trans, Survey currentSurvey, Survey futureSurvey, bool isNewVisit, bool reviewNeeded, string uwInstructions)
        {
            if (isNewVisit && futureSurvey != null)
            {
                futureSurvey = (futureSurvey.IsReadOnly) ? futureSurvey.GetWritableInstance() : futureSurvey;

                // notify the workflow system we have this survey by creating a new suspended instance
                // note: this will record the reference number in our survey for future communication with workflow
                Workflow.CreateSuspendedInstance(futureSurvey, "LOSS PREVENTION - REPORT", "CCICUWWorkflow", "Loss Prevention", "Waiting for Report", null, NewSurveyNoteText, docRemarks);

                base.SaveSurvey(trans, futureSurvey, null);
            }

            //Route the existing survey in BPM
            currentSurvey = (currentSurvey.IsReadOnly) ? currentSurvey.GetWritableInstance() : currentSurvey;

            //For the UW notification, state the attached docs
            string uwNote = string.Empty;
            SurveyDocument[] docs = SurveyDocument.GetSortedArray("SurveyID = ? && !ISNULL(DocTypeCode)", "UploadedOn DESC", currentSurvey.ID);
            for (int i = 0; i < docs.Length; i++)
            {
                DocType docType = DocType.GetOne("Code = ? && CompanyID = ?", docs[i].DocTypeCode, CurrentUser.CompanyID);
                string docTypeName = (docType != null) ? docType.Description : docs[i].DocTypeCode;

                if (i == 0)
                {
                    uwNote += string.Format("The following documents have been attached to the survey: {0}", docTypeName);
                }
                else
                {
                    uwNote += ", " + docTypeName;
                }

                if (i == docs.Length - 1)
                {
                    uwNote += ".";
                }
            }

            string note = (uwNote != null && uwNote.Length > 0) ? uwNote : CompletedNewSurveyNoteText;

            Workflow.CloseSuspendedInstance(currentSurvey, "CCICUWWorkflow", "Report Completed", null, note);
            base.SaveSurvey(trans, currentSurvey, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnReopenComplete(Transaction trans, Survey survey, User consultant, bool wasCanceled)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            string noteText = (wasCanceled) ? ReOpenedCanceledSurveyNoteText : ReOpenedSurveyNoteText;

            Workflow.CreateSuspendedInstance(survey, "LOSS PREVENTION - REPORT", "CCICUWWorkflow", "Loss Prevention", "Waiting for Report", null, noteText, docRemarks);
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnCloseAndNotifyComplete(Transaction trans, Survey survey, string explainText, User currentUser, string recRount)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            string note = (explainText.Length > 0) ? string.Format(ClosedNewSurveyAndNotifyNoteText, currentUser.Name, explainText) : ClosedNewSurveyNoteText;

            Workflow.CloseSuspendedInstance(survey, "CCICUWWorkflow", "Report Completed", null, note);
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnNotifyUWComplete(Transaction trans, Survey survey, string explainText, User currentUser)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            string note = (explainText.Length > 0) ? string.Format(CompletedNewSurveyAndNotifyNoteText, currentUser.Name, explainText) : CompletedNewSurveyNoteText;

            Workflow.CloseSuspendedInstance(survey, "CCICUWWorkflow", "Report Completed", null, note);
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCancelComplete(Transaction trans, Survey survey, string explainText, User currentUser)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            //add the reason for cancellation and user
            string note = string.Format(CanceledSurveyNoteText, currentUser.Name, explainText);

            Workflow.CloseSuspendedInstance(survey, "CCICUWWorkflow", "Cancelled Survey", null, note);
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override string OnAttachDocument(Transaction trans, Survey survey, string fileName, string mimeType, Stream contentStream, DocType docType, string docRemarks)
        {
            string fileNetRef = string.Empty;
            try
            {
                // remove any pathing info from the specified filename
                fileName = Path.GetFileName(fileName);

                // determine which policy to use
                Policy policy = null;
                if (survey.PrimaryPolicy != null && survey.PrimaryPolicy.ExpireDate != DateTime.MinValue)
                {
                    policy = survey.PrimaryPolicy;
                }
                else //prospect survey
                {
                    //First check if a package of BOP line of business exist
                    Policy[] policies = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && Active = true] && (LineOfBusinessCode = ? || LineOfBusinessCode = ?)", "ExpireDate DESC", survey.ID, LineOfBusiness.Package.Code, LineOfBusiness.BusinessOwner.Code);
                    if (policies.Length > 0 && policies[0].ExpireDate != DateTime.MinValue)
                    {
                        policy = policies[0];
                    }

                    if (policy == null)
                    {
                        Policy[] policies2 = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && Active = true]", "ExpireDate DESC", survey.ID);
                        if (policies2.Length > 0 && policies2[0].ExpireDate != DateTime.MinValue)
                        {
                            policy = policies2[0];
                        }
                    }
                }

                //biz rule:  CCIC wants to ensure the policy symbol is attached to the policy number
                if ( policy != null)
                { 
                    if (!String.IsNullOrEmpty(policy.Number) && !String.IsNullOrEmpty(policy.Symbol))
                    {
                        policy = policy.GetWritableInstance();

                        if (char.IsDigit(policy.Number, 0))
                        {
                            policy.Number = policy.Symbol + policy.Number;
                        }
                        else 
                        {
                            policy.Number = policy.Number.Replace("_", "").TrimEnd('P');
                        }
                    }
                }

                // save the doc to the imaging server and get the FileNet reference
                string docTypeCode = (docType != null) ? docType.Code : String.Empty;
                ImagingHelper imaging = new ImagingHelper(this.Survey.Company);
                fileNetRef = imaging.StoreFileImage(fileName, mimeType, contentStream, docTypeCode, docRemarks, survey.SurveyedDate, (policy != null) ? policy.ExpireDate : DateTime.MinValue, DateTime.MinValue, survey.Insured.ClientID, policy, survey.WorkflowSubmissionNumber);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save file '" + fileName + "' of type '" + mimeType + "' to FileNet.  See inner exception for details.", ex);
            }

            return fileNetRef;
        }
    }
}