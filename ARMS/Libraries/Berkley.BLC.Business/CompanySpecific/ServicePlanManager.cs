using System;
using System.Collections;
using System.IO;
using Wilson.ORMapper;
using System.Data.SqlTypes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Berkley.BLC.Entities;


namespace Berkley.BLC.Business
{
    public class ServicePlanManager
    {
        private const long DatabaseTimeResolution = 5 * TimeSpan.TicksPerMillisecond;
        private static DateTime _lastHistoryEntryTime = DateTime.MinValue; // used to prevent dup time entries in history

        private Company _company;
        private ServicePlan _servicePlan;
        private User _currentUser;
        private bool _servicePlanOwnedByUser;
        private bool _override;

        /// <summary>
        /// Create a new instance of this class to manage the specified service plan.
        /// </summary>
        /// <param name="servicePlan">Service plan to be used/modified by this new instance.</param>
        /// <param name="currentUser">User to record as the current user for actions taken on the service plan. Use 'null' to indicate the system is taking action.</param>
        /// <param name="overridePermissions">Flag to override permission checks for this user.</param>
        public ServicePlanManager(ServicePlan servicePlan, User currentUser, bool overridePermissions)
        {
            if (servicePlan == null) throw new ArgumentNullException("servicePlan");

            _company = servicePlan.Company;
            _currentUser = currentUser;
            _override = overridePermissions;

            this.ServicePlan = servicePlan;
        }

        /// <summary>
        /// Gets the company being managed by this class.
        /// </summary>
        public Company Company
        {
            get { return _company; }
        }
        /// <summary>
        /// Gets the user for which this manager will preforms actions.
        /// Null indicates no current users and the 'system' is assumed to be preforming actions.
        /// </summary>
        public User CurrentUser
        {
            get { return _currentUser; }
        }

        /// <summary>
        /// Gets or sets the current instance of the service plan being managed by this class.
        /// NOTE: This instance is updated each time a change is made using the methods in this class.
        /// </summary>
        public ServicePlan ServicePlan
        {
            get { return _servicePlan; }
            set
            {
                if (value == null) throw new ArgumentNullException("value");

                _servicePlan = value;
                _servicePlanOwnedByUser = (_currentUser != null && _currentUser.ID == value.AssignedUserID);
            }
        }

        /// <summary>
        /// Gets of flag value that indicates if the survey is owned by the current users.
        /// </summary>
        protected bool ServicePlanOwnedByUser
        {
            get { return _servicePlanOwnedByUser; }
        }

        /// <summary>
        /// Gets the current user's full name, or "System" if there is none.
        /// </summary>
        protected string GetCurrentUserName()
        {
            return (this.CurrentUser != null) ? this.CurrentUser.Name : "System";
        }

        /// <summary>
        /// Verifies the current user can preform the specified action on the current service plan, 
        /// based on the company's defined actions and user permissions.
        /// </summary>
        /// <param name="actionType">Action to be preformed by the current user.</param>
        protected void VerifyUserCanTakeAction(ServicePlanActionType actionType)
        {
            //Bypass all of this if this is a System Admin User
            if (!_override)
            {
                // verify the company allows the specified action to be preformed
                if (!this.Company.IsActionAllowed(actionType, this.ServicePlan.Status))
                {
                    // build and throw a detailed exception
                    string msg = string.Format("Action '{0}' cannot be taken on service plan {1}.  Company '{2}' does not define this action for service plans with status '{3}'.",
                        actionType.ActionName, this.ServicePlan.Number, this.Company.Abbreviation, this.ServicePlan.StatusCode);

                    throw new InvalidOperationException(msg);
                }

                // verify the current user has permission to take the specified action
                // note: this system user has not permission restrictions
                if (this.CurrentUser != null)
                {
                    PermissionSet permissionSet = this.CurrentUser.Permissions;
                    if (!permissionSet.CanTakeAction(actionType, this.ServicePlan, this.CurrentUser))
                    {
                        // build and throw a detailed exception
                        string msg = string.Format("Action '{0}' cannot be taken on service plan {1} with status of '{2}'.  User '{3}' with company '{4}' does not sufficient permission.",
                            actionType.ActionName, this.ServicePlan.Number, this.ServicePlan.StatusCode, this.CurrentUser.Name, this.Company.Abbreviation);

                        throw new InvalidOperationException(msg);
                    }
                }
            }
        }

        /// <summary>
        /// Builds a list of actions that this user can preform on the service plan.
        /// </summary>
        /// <returns>Array of ServicePlanActions.</returns>
        public ServicePlanAction[] GetAvailableActions()
        {
            if (this.CurrentUser == null)
            {
                throw new InvalidOperationException("A current user must be defined and cannot be system.");
            }

            // get all actions defined in the DB for this company
            ServicePlanAction[] availableActions = ServicePlanAction.GetSortedArray("CompanyID = ? && ServicePlanStatusCode = ?",
                "DisplayOrder ASC", this.Company.ID, this.ServicePlan.StatusCode);

            // build another array with only actions the user has permission to preform
            ServicePlanAction[] actions = new ServicePlanAction[availableActions.Length];
            int actionCount = 0;
            PermissionSet permissions = this.CurrentUser.Permissions;
            foreach (ServicePlanAction action in availableActions)
            {
                bool allowed = permissions.CanTakeAction(action, this.ServicePlanOwnedByUser);
                if (allowed)
                {
                    actions[actionCount++] = action;
                }
            }

            // trim array to length
            ServicePlanAction[] result = new ServicePlanAction[actionCount];
            Array.Copy(actions, result, actionCount);

            return result;
        }

        /// <summary>
        /// Creates a new service plan.
        /// </summary>
        /// <returns>Service plan created by this method.</returns>
        public ServicePlan CreatePlan()
        {
            ServicePlan servicePlan = this.ServicePlan;

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                if (servicePlan.IsReadOnly)
                    servicePlan = servicePlan.GetWritableInstance();

                CreatePlan(trans, servicePlan);

                servicePlan.Save(trans);

                trans.Commit();
            }

            return servicePlan;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected void CreatePlan(Transaction trans, ServicePlan servicePlan)
        {
            servicePlan.StatusCode = ServicePlanStatus.AssignedPlan.Code;
            servicePlan.AssignedUserID = this.CurrentUser.ID;
            servicePlan.CreateStateCode = CreateState.AssignedToConsultant.Code;

            //Create history entry
            string historyEntry = string.Format("Service plan created in ARMS by {0}.", GetCurrentUserName());
            WriteHistoryEntry(trans, servicePlan, historyEntry);

            //Change the creation state for all the visits
            Survey[] surveys = Survey.GetArray("CompanyID = ? && ServicePlanSurveys[ServicePlanID = ?]", this.Company.ID, servicePlan.ID);
            foreach (Survey survey in surveys)
            {
                SurveyManager manager = SurveyManager.GetInstance(survey, this.CurrentUser);
                manager.CreateVisit();
            }
        }

        /// <summary>
        /// Creates a new service plan.
        /// </summary>
        /// <returns>Service plan created by this method.</returns>
        public ServicePlan AddVisitToExistingPlan(Survey survey)
        {
            ServicePlan servicePlan = this.ServicePlan;

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                if (servicePlan.IsReadOnly)
                    servicePlan = servicePlan.GetWritableInstance();

                AddVisitToExistingPlan(trans, servicePlan, survey);

                trans.Commit();
            }

            return servicePlan;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected void AddVisitToExistingPlan(Transaction trans, ServicePlan servicePlan, Survey survey)
        {
            SurveyManager manager = SurveyManager.GetInstance(survey, this.CurrentUser);
            manager.CreateVisit();

            string historyEntry;

            if (survey.Number > 0)
            {
                historyEntry = string.Format("{0} created service visit {1} and assigned it to {2}.", GetCurrentUserName(), survey.Number, survey.AssignedUser.Name);
            }
            else
            {
                historyEntry = string.Format("{0} created service visit with a due date of {1} and assigned it to {2}.", GetCurrentUserName(), survey.DueDate.ToShortDateString(), survey.AssignedUser.Name);
            }

            // save our changes
            SaveServicePlan(trans, servicePlan, historyEntry);
        }

        /// <summary>
        /// Assigns or reassigns this service plan to a new user.
        /// </summary>
        /// <param name="newOwner">User to which this service plan should be assigned.</param>
        public void Assign(User newOwner)
        {
            if (newOwner == null) throw new ArgumentNullException("newOwner");

            VerifyUserCanTakeAction(ServicePlanActionType.Assign);

            ServicePlan servicePlan = this.ServicePlan.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                Assign(trans, servicePlan, newOwner);

                trans.Commit();
            }

            this.ServicePlan = servicePlan;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected void Assign(Transaction trans, ServicePlan servicePlan, User newOwner)
        {
            servicePlan.StatusCode = ServicePlanStatus.AssignedPlan.Code;

            // build the history entry
            string historyEntry;
            if (servicePlan.AssignedUserID == Guid.Empty)
            {
                historyEntry = string.Format("{0} assigned this service plan to {1}.", GetCurrentUserName(), newOwner.Name);
            }
            else // re-assignment
            {
                historyEntry = string.Format("{0} reassigned this service plan to {1}.", GetCurrentUserName(), newOwner.Name);
            }

            // now assign the new owner
            servicePlan.AssignedUserID = newOwner.ID;

            // save our changes
            SaveServicePlan(trans, servicePlan, historyEntry);
        }

        /// <summary>
        /// Begins the process of reopening a service plan.
        /// </summary>
        public void Reopen(User newOwner)
        {
            if (newOwner == null) throw new ArgumentNullException("newOwner");
            if (this.CurrentUser == null) throw new InvalidOperationException("This action requires a current user to be defined.");

            VerifyUserCanTakeAction(ServicePlanActionType.Reopen);

            ServicePlan servicePlan = this.ServicePlan.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                Reopen(trans, servicePlan, newOwner);
                trans.Commit();
            }

            this.ServicePlan = servicePlan;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected void Reopen(Transaction trans, ServicePlan servicePlan, User newOwner)
        {
            // status must be completed or canceled
            if (servicePlan.Status != ServicePlanStatus.ClosedPlan && servicePlan.Status != ServicePlanStatus.CanceledPlan)
            {
                throw new InvalidOperationException("This action cannot be called for service plans with a status of '" + servicePlan.Status.Name + "'.");
            }

            servicePlan.StatusCode = ServicePlanStatus.AssignedPlan.Code;
            servicePlan.AssignedUserID = newOwner.ID;
            servicePlan.CompleteDate = DateTime.MinValue;

            // create history entry
            string historyEntry = string.Format("{0} reopened service plan and assigned it to {1}.", GetCurrentUserName(), newOwner.Name);

            // save our changes
            SaveServicePlan(trans, servicePlan, historyEntry);
        }

        /// <summary>
        /// Releases the currently assigned owner of this service plan and reassigns it to the appropriate party.
        /// </summary>
        public virtual void ReleaseOwnership()
        {
            // a user must be assigned to the service plan
            if (this.ServicePlan.AssignedUser == null)
            {
                throw new InvalidOperationException("This action cannot be called when no user is assigned to the service plan.");
            }

            VerifyUserCanTakeAction(ServicePlanActionType.ReleaseOwnership);

            ServicePlan servicePlan = this.ServicePlan.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                ReleaseOwnership(trans, servicePlan);
                trans.Commit();
            }

            this.ServicePlan = servicePlan;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ReleaseOwnership(Transaction trans, ServicePlan servicePlan)
        {
            User priorOwner = servicePlan.AssignedUser;

            // survey must be "in progress"
            if (servicePlan.Status != ServicePlanStatus.AssignedPlan)
            {
                throw new InvalidOperationException("This action cannot be called when the service plan has a status of '" + servicePlan.Status.Name + "'.");
            }

            // unassign the service plan
            servicePlan.AssignedUserID = Guid.Empty;
            servicePlan.StatusCode = ServicePlanStatus.UnassignedPlan.Code;

            string historyEntry;
            if (this.CurrentUser != null && priorOwner.ID == this.CurrentUser.ID) // owner
            {
                historyEntry = string.Format("{0} released ownership of this service plan.", GetCurrentUserName());
            }
            else // not the owner
            {
                historyEntry = string.Format("{0} released ownership of this service plan from {1}.", GetCurrentUserName(), priorOwner.Name);
            }

            // save our changes
            SaveServicePlan(trans, servicePlan, historyEntry);
        }

        /// <summary>
        /// Changes the service plan type.
        /// </summary>
        public virtual void ChangeServicePlanType(ServicePlanType type)
        {
            VerifyUserCanTakeAction(ServicePlanActionType.ChangePlanType);

            ServicePlan servicePlan = this.ServicePlan.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                ChangeServicePlanType(trans, servicePlan, type);
                trans.Commit();
            }

            this.ServicePlan = servicePlan;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ChangeServicePlanType(Transaction trans, ServicePlan servicePlan, ServicePlanType type)
        {
            
            //set the history entry
            string historyEntry;
            if (servicePlan.Type != null)
            {
                historyEntry = string.Format("{0} changed the service plan type from '{1}' to '{2}'.", GetCurrentUserName(), servicePlan.Type.Name, type.Name);
            }
            else
            {
                historyEntry = string.Format("{0} changed the service plan type from being blank to '{1}'.", GetCurrentUserName(), type.Name);
            }

            servicePlan.TypeID = type.ID;

            // save our changes
            SaveServicePlan(trans, servicePlan, historyEntry);
        }

        /// <summary>
        /// Finalizes the service plan and closes it out.
        /// </summary>
        public virtual ServicePlan Close(bool futurePlanNeeded, User futureUser, string gradingCode)
        {
            VerifyUserCanTakeAction(ServicePlanActionType.Close);

            ServicePlan servicePlan = this.ServicePlan.GetWritableInstance();

            ServicePlan newPlan;
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                newPlan = Close(trans, servicePlan, futurePlanNeeded, futureUser, gradingCode);
                trans.Commit();
            }

            this.ServicePlan = servicePlan;

            return newPlan;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual ServicePlan Close(Transaction trans, ServicePlan servicePlan, bool futurePlanNeeded, User futureUser, string gradingCode)
        {
            string historyEntry = string.Empty;
            servicePlan.StatusCode = ServicePlanStatus.ClosedPlan.Code;
            servicePlan.CompleteDate = DateTime.Today;

            if ((Utility.GetCompanyParameter("UsesPlanGrading", CurrentUser.Company).ToUpper() == "TRUE") ? true : false && gradingCode != servicePlan.GradingCode)
            {
                servicePlan.GradingCode = gradingCode;
                string msgValue = (servicePlan.GradingCode != string.Empty) ? servicePlan.SurveyGrading.Name : "nothing";

                historyEntry = string.Format("{0} set the overall grading to {1}.", GetCurrentUserName(), msgValue);
                WriteHistoryEntry(trans, servicePlan, historyEntry);
            }

            ServicePlan newPlan = null;
            if (futurePlanNeeded)
            {
                //Create new service plan
                newPlan = new ServicePlan(new Guid());
                newPlan.CompanyID = servicePlan.CompanyID;
                newPlan.InsuredID = servicePlan.InsuredID;
                newPlan.PrimarySurveyID = servicePlan.PrimarySurveyID;
                newPlan.PrimaryPolicyID = servicePlan.PrimaryPolicyID;
                newPlan.PreviousServicePlanID = servicePlan.ID;
                newPlan.StatusCode = ServicePlanStatus.AssignedPlan.Code;
                newPlan.AssignedUserID = futureUser.ID;
                newPlan.CreateStateCode = CreateState.AssignedToConsultant.Code;
                newPlan.CreateDate = DateTime.Now;
                newPlan.CreatedByUserID = CurrentUser.ID;
                newPlan.Save(trans);

                //copy over any plan attributes
                ServicePlanAttribute[] oldPlanAttributes = ServicePlanAttribute.GetArray("ServicePlanID = ?", servicePlan.ID);
                foreach (ServicePlanAttribute oldPlanAttribute in oldPlanAttributes)
                {
                    ServicePlanAttribute newPlanAttribute = new ServicePlanAttribute(newPlan.ID, oldPlanAttribute.CompanyAttributeID);
                    newPlanAttribute.AttributeValue = oldPlanAttribute.AttributeValue;
                    newPlanAttribute.LastModifiedBy = oldPlanAttribute.LastModifiedBy;
                    newPlanAttribute.LastModifiedDate = oldPlanAttribute.LastModifiedDate;
                    newPlanAttribute.Save(trans);
                }

                //Create history entry
                string newEntry = string.Format("Service plan created from previous service plan #{0} in ARMS by {1}.", servicePlan.Number, GetCurrentUserName());
                WriteHistoryEntry(trans, newPlan, newEntry);
            }

            historyEntry = string.Format("Service plan closed by {0}.", GetCurrentUserName());

            // save our changes
            SaveServicePlan(trans, servicePlan, historyEntry);

            return newPlan;
        }

        /// <summary>
        /// Finalizes the service plan and cancels.
        /// </summary>
        public virtual void Cancel()
        {
            VerifyUserCanTakeAction(ServicePlanActionType.Cancel);

            ServicePlan servicePlan = this.ServicePlan.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                Cancel(trans, servicePlan);
                trans.Commit();
            }

            this.ServicePlan = servicePlan;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void Cancel(Transaction trans, ServicePlan servicePlan)
        {
            servicePlan.StatusCode = ServicePlanStatus.CanceledPlan.Code;
            servicePlan.CompleteDate = DateTime.Today;
            string historyEntry = string.Format("Service plan canceled by {0}.", GetCurrentUserName());

            //Cancel any outstanding service visits for this plan
            Survey[] visits = Survey.GetArray("ServicePlanSurveys[ServicePlanID = ?] && Status.TypeCode != ?", servicePlan.ID, SurveyStatusType.Completed.Code);
            foreach (Survey visit in visits)
            {
                SurveyManager manager = SurveyManager.GetInstance(visit, CurrentUser, true);
                manager.Cancel("The service plan for this service visit was canceled.");
            }

            // save our changes
            SaveServicePlan(trans, servicePlan, historyEntry);
        }

        /// <summary>
        /// Attaches a document to this Service Plan and stores it in FileNet for later retrieval.
        /// </summary>
        /// <param name="fileName">Filename of the file being attached.</param>
        /// <param name="mimeType">MIME Type of the file beign attached.</param>
        /// <param name="contentStream">Stream of the file content.</param>
        /// <param name="docType">The document type if this document will be attached to a Workflow transaction, default is null.</param>
        /// <param name="docRemarks">Remarks attached to the document in the imaging system.</param>
        public string AttachDocument(string fileName, string mimeType, Stream contentStream, string docType, string docRemarks)
        {
            if (fileName == null) throw new ArgumentNullException("fileName");
            if (mimeType == null) throw new ArgumentNullException("mimeType");
            if (contentStream == null) throw new ArgumentNullException("contentStream");
            if (contentStream.Length == 0) throw new ArgumentException("Content stream cannot be zero length.");

            VerifyUserCanTakeAction(ServicePlanActionType.AddDocument);

            ServicePlan servicePlan = this.ServicePlan.GetWritableInstance();
            string fileNetRef;

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                fileNetRef = OnAttachDocument(trans, servicePlan, fileName, mimeType, contentStream, docType, docRemarks);
                AttachDocument(trans, servicePlan, fileName, mimeType, contentStream, fileNetRef);

                trans.Commit();
            }

            this.ServicePlan = servicePlan;

            return fileNetRef;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected void AttachDocument(Transaction trans, ServicePlan servicePlan, string fileName, string mimeType, Stream contentStream, string fileNetRef)
        {
            // remove any pathing info from the specified filename
            fileName = Path.GetFileName(fileName);

            // create a record of this file with the audit and record this action in history
            ServicePlanDocument doc = new ServicePlanDocument(Guid.NewGuid());
            doc.ServicePlanID = servicePlan.ID;
            doc.DocumentName = fileName;
            doc.MimeType = mimeType;
            doc.SizeInBytes = contentStream.Length;
            doc.FileNetReference = fileNetRef;
            doc.UploadedUserID = (this.CurrentUser != null) ? this.CurrentUser.ID : Guid.Empty;
            doc.UploadedOn = DateTime.Now;
            doc.IsRemoved = false;
            doc.Save(trans);

            string historyEntry = string.Format("{0} attached file '{1}'.", GetCurrentUserName(), fileName);
            WriteHistoryEntry(trans, this.ServicePlan, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected string OnAttachDocument(Transaction trans, ServicePlan servicePlan, string fileName, string mimeType, Stream contentStream, string docType, string docRemarks)
        {
            string fileNetRef = string.Empty;
            try
            {
                // remove any pathing info from the specified filename 
                fileName = Path.GetFileName(fileName);

                // save the doc to the imaging server and get the FileNet reference
                ImagingHelper imaging = new ImagingHelper(this.ServicePlan.Company);
                fileNetRef = imaging.StoreFileImage(fileName, mimeType, contentStream, docType, docRemarks, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, servicePlan.Insured.ClientID, null, servicePlan.WorkflowSubmissionNumber);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save file '" + fileName + "' of type '" + mimeType + "' to FileNet.  See inner exception for details.", ex);
            }

            return fileNetRef;
        }

        /// <summary>
        /// Removes the specified service plan document.
        /// </summary>
        public void RemoveDocument(ServicePlanDocument doc)
        {
            VerifyUserCanTakeAction(ServicePlanActionType.RemoveDocument);

            ServicePlan servicePlan = this.ServicePlan.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                RemoveDocument(trans, servicePlan, doc);
                trans.Commit();
            }

            this.ServicePlan = servicePlan;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected void RemoveDocument(Transaction trans, ServicePlan servicePlan, ServicePlanDocument doc)
        {
            string historyEntry = string.Format("{0} removed file '{1}'.", GetCurrentUserName(), doc.DocumentName);

            //try and remove the document from FileNet
            ImagingHelper imaging = new ImagingHelper(Company);
            imaging.RemoveFileImage(doc.FileNetReference);

            // mark the doc as deleted in ARMS
            ServicePlanDocument removedDoc = doc.GetWritableInstance();
            removedDoc.IsRemoved = true;
            removedDoc.Save(trans);

            //write the history entry
            WriteHistoryEntry(trans, servicePlan, historyEntry);
        }

        /// <summary>
        /// Attaches a comment from a user to a ServicePlan as a note.
        /// </summary>
        /// <param name="comment">The comment to be added as a note to the service plan.</param>
        /// <param name="commentDate">The timestamp of the entry.</param>
        public void AddNote(string comment, DateTime commentDate)
        {
            if (comment == null || comment.Length == 0) throw new ArgumentNullException("comment");

            VerifyUserCanTakeAction(ServicePlanActionType.AddNote);

            ServicePlan servicePlan = this.ServicePlan.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                AddNote(trans, servicePlan, comment, commentDate);
                trans.Commit();
            }

            this.ServicePlan = servicePlan;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void AddNote(Transaction trans, ServicePlan servicePlan, string comment, DateTime commentDate)
        {
            // build and save the comment
            ServicePlanNote note = new ServicePlanNote(Guid.NewGuid());
            note.ServicePlanID = servicePlan.ID;
            note.UserID = this.CurrentUser.ID;
            note.EntryDate = commentDate;
            note.Comment = comment;
            note.Save(trans);

            // Set the last modified date
            servicePlan.LastModifiedOn = DateTime.Now;
            servicePlan.Save(trans);
        }

        /// <summary>
        /// Removes the specified service plan note.
        /// </summary>
        public void RemoveNote(ServicePlanNote note)
        {
            VerifyUserCanTakeAction(ServicePlanActionType.RemoveNote);

            ServicePlan servicePlan = this.ServicePlan.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                RemoveDocument(trans, servicePlan, note);
                trans.Commit();
            }

            this.ServicePlan = servicePlan;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected void RemoveDocument(Transaction trans, ServicePlan servicePlan, ServicePlanNote note)
        {
            string historyEntry = string.Format("{0} removed a comment that was entered on {1} by {2}.", GetCurrentUserName(), note.EntryDate.ToString("g"), note.User.Name);

            //delete the note
            note.Delete(trans);

            //write the history entry
            WriteHistoryEntry(trans, servicePlan, historyEntry);
        }

        #region UI Action Enums
        /// <summary>
        /// The UI action type.
        /// </summary>
        public enum UIAction
        {
            /// <summary>
            /// Updating a record in an entity.
            /// </summary>
            Update,
            /// <summary>
            /// Adding a record to an entity.
            /// </summary>
            Add,
            /// <summary>
            /// Removing a record from an entity.
            /// </summary>
            Remove
        }
        #endregion

        /// <summary>
        /// Updates the history regarding rec changes.
        /// </summary>
        public void ActivityManagement(ServicePlanActivity activity, UIAction action)
        {
            VerifyUserCanTakeAction(ServicePlanActionType.ManageAccount);

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                ActivityManagement(trans, ServicePlan, activity, action);
                trans.Commit();
            }
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected void ActivityManagement(Transaction trans, ServicePlan servicePlan, ServicePlanActivity activity, UIAction action)
        {
            //Create history entry
            string historyEntry;
            if (action == UIAction.Update)
            {
                historyEntry = string.Format("{0} updated the {1} {2} activity time.", GetCurrentUserName(), activity.ActivityDate.ToShortDateString(), activity.ActivityType.Name);
            }
            else if (action == UIAction.Add)
            {
                historyEntry = string.Format("{0} entered a new {1} activity time.", GetCurrentUserName(), activity.ActivityType.Name);
            }
            else if (action == UIAction.Remove)
            {
                historyEntry = string.Format("{0} removed the {1} {2} activity time.", GetCurrentUserName(), activity.ActivityDate.ToShortDateString(), activity.ActivityType.Name);
            }
            else
            {
                throw new NotSupportedException("Not a supported UI action taken.");
            }

            WriteHistoryEntry(trans, servicePlan, historyEntry);
        }

        /// <summary>
        /// Writes a history entry for a Service Plan.
        /// </summary>
        /// <param name="trans">Transaction underwhich this operation will be preformed.</param>
        /// <param name="servicePlan">Service plan associated to this new entry.</param>
        /// <param name="entryText">Text to be saved in history.</param>
        protected static void WriteHistoryEntry(Transaction trans, ServicePlan servicePlan, string entryText)
        {
            // try to avoid duplicate history times in database (DateTime.Now resolution is ~10 ms)
            //
            // From SQL Server Books Online:
            //   Date and time data from January 1, 1753 through December 31, 9999, to an accuracy of one 
            //   three-hundredth of a second (equivalent to 3.33 milliseconds or 0.00333 seconds).
            //   Values are rounded to increments of .000, .003, or .007 seconds, as shown in the table.
            //
            DateTime entryTime = DateTime.Now;
            if (entryTime < _lastHistoryEntryTime.AddTicks(DatabaseTimeResolution))
            {
                entryTime = _lastHistoryEntryTime.AddTicks(DatabaseTimeResolution);
            }

            ServicePlanHistory history = new ServicePlanHistory(Guid.NewGuid());
            history.ServicePlanID = servicePlan.ID;
            history.EntryDate = entryTime;
            history.EntryText = entryText;
            history.Save(trans);

            _lastHistoryEntryTime = entryTime;
        }

        /// <summary>
        /// Saves a servicePlan, records any change in status and writes an entry to history.
        /// </summary>
        /// <param name="trans">Transaction underwhich this operation will be preformed.</param>
        /// <param name="servicePlan">ServicePlan to save.</param>
        /// <param name="historyEntry">Entry to be added to the servicePlan's history.</param>
        protected void SaveServicePlan(Transaction trans, ServicePlan servicePlan, string historyEntry)
        {
            SaveServicePlan(trans, servicePlan, historyEntry, null);
        }

        /// <summary>
        /// Saves a servicePlan, records any change in status and writes an entry to history.
        /// </summary>
        /// <param name="trans">Transaction underwhich this operation will be preformed.</param>
        /// <param name="servicePlan">ServicePlan to save.</param>
        /// <param name="historyEntry">Entry to be added to the servicePlan's history.</param>
        /// <param name="actionUser">User that performed the action.</param>
        protected void SaveServicePlan(Transaction trans, ServicePlan servicePlan, string historyEntry, User actionUser)
        {
            // write the passed string to history
            if (historyEntry != null && historyEntry.Length > 0)
            {
                WriteHistoryEntry(trans, servicePlan, historyEntry);
            }

            servicePlan.LastModifiedOn = DateTime.Now;

            // save the service plan
            servicePlan.Save(trans);
        }

        public bool CheckServicePlanPolicyforExpiration(ServicePlan servicePlan)
        {
            //Check service plan primary policy to see if any are past the expiration date
            Policy[] policies;

            if (_servicePlan.PrimaryPolicyID != null && _servicePlan.PrimaryPolicyID != Guid.Empty)
                policies = Policy.GetArray("ID = ? && ExpireDate < ? && IsActive = true", _servicePlan.PrimaryPolicyID, DateTime.Now.ToShortDateString());
            else
                policies = Policy.GetArray("InsuredID = ? && ExpireDate < ? && IsActive = true", _servicePlan.InsuredID, DateTime.Now.ToShortDateString());

            if (policies != null && policies.Length > 0)
                return true;

            return false;

        }

        public bool CheckServicePlanforRecs(ServicePlan servicePlan)
        {
            //Check service plan primary policy to see if any are past the expiration date
            Policy[] policies;

            if (_servicePlan.PrimaryPolicyID != null && _servicePlan.PrimaryPolicyID != Guid.Empty)
                policies = Policy.GetArray("ID = ? && ExpireDate < ?", _servicePlan.PrimaryPolicyID, DateTime.Now.ToShortDateString());
            else
                policies = Policy.GetArray("InsuredID = ? && ExpireDate < ?", _servicePlan.InsuredID, DateTime.Now.ToShortDateString());

            if (policies != null && policies.Length > 0)
                return true;

            return false;

        }

        //Copy open objectives from previous service plan to new
        public void CopyActivePlanObjectives(ServicePlan oldServicePlan, ServicePlan newServicePlan)
        {
            Objective[] objs = Objective.GetArray("isNull(SurveyID) && ServicePlanID = ? && StatusCode != ?", oldServicePlan.ID, "C");

            foreach (Objective oj in objs)
            {
                using (Transaction trans = DB.Engine.BeginTransaction())
                {                    
                    //New Objective
                    Objective objective = new Objective(Guid.NewGuid());

                    Business.Utility.CopyPropertyValues(ref objective, oj);

                    //get the highest number and incriment by one
                    Objective[] objectives = Objective.GetSortedArray("ServicePlanID = ?", "Number DESC", newServicePlan.ID);
                    objective.Number = (objectives.Length > 0) ? objectives[0].Number + 1 : 1;
                    objective.ServicePlanID = newServicePlan.ID;
                        
                    objective.Save(trans);

                    //Create any attributes
                    List<ObjectiveAttribute> planAttributes = ObjectiveAttribute.GetArray("ObjectiveID = ?", oldServicePlan.ID).ToList();

                    foreach (ObjectiveAttribute a in planAttributes)
                    {
                        ObjectiveAttribute newAttribute = new ObjectiveAttribute(objective.ID, a.CompanyAttributeID);

                        Business.Utility.CopyPropertyValues(ref newAttribute, a);

                        newAttribute.Save(trans);
                    }

                    trans.Commit();
                }
            }              
        }

        public void UpdatePlanGrading(string gradingCode)
        {
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                string historyEntry = string.Empty;

                ServicePlan servicePlan = this.ServicePlan.GetWritableInstance();

                servicePlan.GradingCode = gradingCode;
                string msgValue = (servicePlan.GradingCode != string.Empty) ? servicePlan.SurveyGrading.Name : "nothing";

                historyEntry = string.Format("{0} set the overall grading to {1}.", GetCurrentUserName(), msgValue);
                WriteHistoryEntry(trans, servicePlan, historyEntry);

                servicePlan.Save(trans);

                trans.Commit();
            }
        }
    }
}
