using System;
using Berkley.BLC.Entities;
using Wilson.ORMapper;
using System.IO;

namespace Berkley.BLC.Business.CompanySpecific
{
    /// <summary>
    /// Concrete class that implements SurveyManager functions for BLS.
    /// </summary>
    public class SurveyManagerBLS : SurveyManager
    {
        private static readonly WorkflowHelper workflow;
        private static readonly string docRemarks;

        static SurveyManagerBLS()
        {
            workflow = new WorkflowHelper(Company.BerkleyMidAtlanticGroup, Berkley.Workflow.WorkflowSystemType.ALBPM_5);
            docRemarks = "READ-ONLY SCREEN IN ARMS";
        }

        internal SurveyManagerBLS(Survey survey, User currentUser, bool overridePermissions)
            : base(survey, currentUser, overridePermissions)
        {
        }

        /// <summary>
        /// Overridden. Returns the workflow provider.
        /// </summary>
        protected override WorkflowHelper Workflow
        {
            get
            {
                return workflow;
            }
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override Survey OnCreateSurveyComplete(Transaction trans, Survey survey)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            //// notify the workflow system we have this survey by creating a new suspended instance
            //// note: this will record the reference number in our survey for future communication with workflow
            //Workflow.CreateSuspendedInstance(survey, "LCR", "BLSSurvey", "Active", null, NewSurveyNoteText, docRemarks);
            return survey;
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCreateVisitComplete(Transaction trans, Survey survey)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            // notify the workflow system we have this survey by creating a new suspended instance
            // note: this will record the reference number in our survey for future communication with workflow
            //Workflow.CreateSuspendedInstance(survey, "LCR", "BLSSurvey", "Active", null, NewSurveyNoteText, docRemarks);
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCreateSurveyFromExistingSurveyComplete(Transaction trans, Survey survey)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            // notify the workflow system we have this survey by creating a new suspended instance
            // note: this will record the reference number in our survey for future communication with workflow
            //Workflow.CreateSuspendedInstance(survey, "LCR", "BLSSurvey", "Active", null, NewSurveyNoteText, docRemarks);
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnSendForReviewComplete(Transaction trans, Survey currentSurvey, Survey futureSurvey, bool isNewVisit, bool reviewNeeded, string uwInstructions)
        {
            if (isNewVisit && futureSurvey != null)
            {
                futureSurvey = (futureSurvey.IsReadOnly) ? futureSurvey.GetWritableInstance() : futureSurvey;

                // notify the workflow system we have this survey by creating a new suspended instance
                // note: this will record the reference number in our survey for future communication with workflow
                //Workflow.CreateSuspendedInstance(futureSurvey, "LCR", "BLSSurvey", "Active", null, NewSurveyNoteText, docRemarks);

                base.SaveSurvey(trans, futureSurvey, null);
            }

            //Route the existing survey in BPM
            currentSurvey = (currentSurvey.IsReadOnly) ? currentSurvey.GetWritableInstance() : currentSurvey;

            //For the UW notification, state the attached docs
            string uwNote = string.Empty;
            SurveyDocument[] docs = SurveyDocument.GetSortedArray("SurveyID = ? && !ISNULL(DocTypeCode)", "UploadedOn DESC", currentSurvey.ID);
            for (int i = 0; i < docs.Length; i++)
            {
                DocType docType = DocType.GetOne("Code = ? && CompanyID = ?", docs[i].DocTypeCode, CurrentUser.CompanyID);
                string docTypeName = (docType != null) ? docType.Description : docs[i].DocTypeCode;

                if (i == 0)
                {
                    uwNote += string.Format("The following documents have been attached to the survey: {0}", docTypeName);
                }
                else
                {
                    uwNote += ", " + docTypeName;
                }

                if (i == docs.Length - 1)
                {
                    uwNote += ".";
                }
            }

            string note = (uwNote != null && uwNote.Length > 0) ? uwNote : CompletedNewSurveyNoteText;

            //Workflow.CloseSuspendedInstance(currentSurvey, "BLSSurvey", "Complete", null, note);
            base.SaveSurvey(trans, currentSurvey, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnReopenComplete(Transaction trans, Survey survey, User consultant, bool wasCanceled)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            string noteText = (wasCanceled) ? ReOpenedCanceledSurveyNoteText : ReOpenedSurveyNoteText;

            //Workflow.CreateSuspendedInstance(survey, "LCR", "BLSSurvey", "Active", null, noteText, docRemarks);
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnCloseAndNotifyComplete(Transaction trans, Survey survey, string explainText, User currentUser, string recRount)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            string note = (explainText.Length > 0) ? string.Format(ClosedNewSurveyAndNotifyNoteText, currentUser.Name, explainText) : ClosedNewSurveyNoteText;

            //Workflow.CloseSuspendedInstance(survey, "BLSSurvey", "Complete", null, note);
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnNotifyUWComplete(Transaction trans, Survey survey, string explainText, User currentUser)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            string note = (explainText.Length > 0) ? string.Format(CompletedNewSurveyAndNotifyNoteText, currentUser.Name, explainText) : CompletedNewSurveyNoteText;

            //Workflow.CloseSuspendedInstance(survey, "BLSSurvey", "Complete", null, note);
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCancelComplete(Transaction trans, Survey survey, string explainText, User currentUser)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            //add the reason for cancellation and user
            string note = string.Format(CanceledSurveyNoteText, currentUser.Name, explainText);

            //Workflow.CloseSuspendedInstance(survey, "BLSSurvey", "Canceled", null, note);
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override string OnAttachDocument(Transaction trans, Survey survey, string fileName, string mimeType, Stream contentStream, DocType docType, string docRemarks)
        {
            string fileNetRef = string.Empty;
            try
            {
                // remove any pathing info from the specified filename
                fileName = Path.GetFileName(fileName);

                // determine which policy to use
                Policy policy = null;
                if (survey.PrimaryPolicy != null && survey.PrimaryPolicy.ExpireDate != DateTime.MinValue)
                {
                    policy = survey.PrimaryPolicy;
                }
                else //prospect survey
                {
                    //First check if a package of BOP line of business exist
                    Policy[] policies = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && Active = true] && (LineOfBusinessCode = ? || LineOfBusinessCode = ?)", "ExpireDate DESC", survey.ID, LineOfBusiness.Package.Code, LineOfBusiness.BusinessOwner.Code);
                    if (policies.Length > 0 && policies[0].ExpireDate != DateTime.MinValue)
                    {
                        policy = policies[0];
                    }

                    if (policy == null)
                    {
                        Policy[] policies2 = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && Active = true]", "ExpireDate DESC", survey.ID);
                        if (policies2.Length > 0 && policies2[0].ExpireDate != DateTime.MinValue)
                        {
                            policy = policies2[0];
                        }
                    }
                }

                // save the doc to the imaging server and get the FileNet reference
                string docTypeCode = (docType != null) ? docType.Code : String.Empty;
                ImagingHelper imaging = new ImagingHelper(this.Survey.Company);
                fileNetRef = imaging.StoreFileImage(fileName, mimeType, contentStream, docTypeCode, docRemarks, (survey.SurveyedDate != DateTime.MinValue) ? survey.SurveyedDate : DateTime.Today, DateTime.MinValue, DateTime.Now, survey.Insured.ClientID, policy, survey.WorkflowSubmissionNumber);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save file '" + fileName + "' of type '" + mimeType + "' to FileNet.  See inner exception for details.", ex);
            }

            return fileNetRef;
        }
    }
}