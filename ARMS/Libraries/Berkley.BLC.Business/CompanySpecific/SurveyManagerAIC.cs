using System;
using Berkley.BLC.Entities;
using Wilson.ORMapper;
using System.IO;
using System.Collections;

namespace Berkley.BLC.Business.CompanySpecific
{
    /// <summary>
    /// Concrete class that implements SurveyManager functions for AIC.
    /// </summary>
    public class SurveyManagerAIC : SurveyManager
    {
        private static readonly WorkflowHelper workflow;
        private static readonly string docRemarks;

        static SurveyManagerAIC()
        {

            if (Company.AcadiaInsuranceCompany.MilestoneDate == DateTime.MinValue || Company.AcadiaInsuranceCompany.MilestoneDate > DateTime.Today)
            {
                workflow = new WorkflowHelper(Company.AcadiaInsuranceCompany, Berkley.Workflow.WorkflowSystemType.ALBPM_5);
                docRemarks = "ARMS: Remark from LC ";
            }
            else
            {
                workflow = new WorkflowHelper(Company.AcadiaInsuranceCompany, Berkley.Workflow.WorkflowSystemType.ALBPM_6);
                docRemarks = "ARMS";
            }
        }

        internal SurveyManagerAIC(Survey survey, User currentUser, bool overridePermissions)
            : base(survey, currentUser, overridePermissions)
        {
        }

        /// <summary>
        /// Overridden. Returns the workflow provider.
        /// </summary>
        protected override WorkflowHelper Workflow
        {
            get
            {
                return workflow;
            }
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override Survey OnCreateSurveyComplete(Transaction trans, Survey survey)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            
            return survey;
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCreateVisitComplete(Transaction trans, Survey survey)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

           
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCreateSurveyFromExistingSurveyComplete(Transaction trans, Survey survey)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

           
            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnSendForReviewComplete(Transaction trans, Survey currentSurvey, Survey futureSurvey, bool isNewVisit, bool reviewNeeded, string uwInstructions)
        {
            //Create an instance for the future visit
            if (isNewVisit && futureSurvey != null)
            {
                futureSurvey = (futureSurvey.IsReadOnly) ? futureSurvey.GetWritableInstance() : futureSurvey;

               
                base.SaveSurvey(trans, futureSurvey, null);
            }

            //Route the existing survey in BPM
            currentSurvey = (currentSurvey.IsReadOnly) ? currentSurvey.GetWritableInstance() : currentSurvey;

            string note;
            if (!string.IsNullOrEmpty(uwInstructions))
            {
                note = string.Format(NewSurveyAndNotifyNoteTextCompletedClosed, CurrentUser.Name, uwInstructions);
            }
            else
            {
                note = CompletedClosedNewSurveyNoteText;
            }

            if (Company.AcadiaInsuranceCompany.MilestoneDate == DateTime.MinValue || Company.AcadiaInsuranceCompany.MilestoneDate > DateTime.Today)
            {
                Workflow.CreateSuspendedInstance(currentSurvey, "LCR", "AICLossControlReport", "Active", null, note, docRemarks);
            }
            else
            {
                Workflow.CreateSuspendedInstance(currentSurvey, "LCS", "AICUWWorkflow", "Loss Control Report", "UW Review", null, note, string.Format("ARMS - Remark from LC: {0}", uwInstructions));
            }

            if (Company.AcadiaInsuranceCompany.MilestoneDate == DateTime.MinValue || Company.AcadiaInsuranceCompany.MilestoneDate > DateTime.Today)
            {
                //Do nothing
            }
            else
            {
                if (!reviewNeeded)  //Required for the AIC BPM Project 32341
                {
                    //Add the note to the uw as a comment
                    if (uwInstructions != null && uwInstructions.Trim().Length > 0)
                    {
                        SurveyNote UWnote = new SurveyNote(new Guid());
                        UWnote.SurveyID = currentSurvey.ID;
                        UWnote.UserID = CurrentUser.ID;
                        UWnote.EntryDate = DateTime.Now;
                        UWnote.InternalOnly = false;
                        UWnote.AdminOnly = false;
                        UWnote.Comment = string.Format("Instructions to UW upon closing {0}: {1}", currentSurvey.ServiceType.Name.ToLower(), uwInstructions);

                        UWnote.Save(trans);
                    }
                }
            }

            base.SaveSurvey(trans, currentSurvey, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnReopenComplete(Transaction trans, Survey survey, User consultant, bool wasCanceled)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            

            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnCloseAndNotifyComplete(Transaction trans, Survey survey, string explainText, User currentUser, string recRount)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;
            string note = (explainText != null && explainText.Length > 0) ? string.Format(NewSurveyAndNotifyNoteTextClosed, currentUser.Name, explainText) : ClosedNewSurveyNoteText;

            if (Company.AcadiaInsuranceCompany.MilestoneDate == DateTime.MinValue || Company.AcadiaInsuranceCompany.MilestoneDate > DateTime.Today)
            {
                Workflow.CreateSuspendedInstance(survey, "LCR", "AICLossControlReport", "Active", null, note, docRemarks);
            }
            else
            {
                Workflow.CreateSuspendedInstance(survey, "LCS", "AICUWWorkflow", "Loss Control Report", "UW Review", null, note, string.Format("ARMS - Remark from LC: {0}", explainText));
            }

            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnNotifyUWComplete(Transaction trans, Survey survey, string explainText, User currentUser)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;
            string note = (explainText != null && explainText.Length > 0) ? string.Format(NewSurveyAndNotifyNoteTextCompleted, currentUser.Name, explainText) : CompletedNewSurveyNoteText;

            if (Company.AcadiaInsuranceCompany.MilestoneDate == DateTime.MinValue || Company.AcadiaInsuranceCompany.MilestoneDate > DateTime.Today)
            {
                Workflow.CreateSuspendedInstance(survey, "LCR", "AICLossControlReport", "Active", null, note, docRemarks);
            }
            else
            {
                Workflow.CreateSuspendedInstance(survey, "LCS", "AICUWWorkflow", "Loss Control Report", "UW Review", null, note, string.Format("ARMS - Remark from LC: {0}", explainText));
            }

            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCancelComplete(Transaction trans, Survey survey, string explainText, User currentUser)
        {
            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;

            base.SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override string OnAttachDocument(Transaction trans, Survey survey, string fileName, string mimeType, Stream contentStream, DocType docType, string docRemarks)
        {
            string fileNetRef = string.Empty;
            try
            {
                // remove any pathing info from the specified filename
                fileName = Path.GetFileName(fileName);

                // determine which policy to use
                Policy policy = null;
                if (survey.PrimaryPolicy != null && survey.PrimaryPolicy.ExpireDate != DateTime.MinValue)
                {
                    policy = survey.PrimaryPolicy;
                }
                else //prospect survey
                {
                    //First check if a package of BOP line of business exist
                    Policy[] policies = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && Active = true] && (LineOfBusinessCode = ? || LineOfBusinessCode = ?)", "ExpireDate DESC", survey.ID, LineOfBusiness.Package.Code, LineOfBusiness.BusinessOwner.Code);
                    if (policies.Length > 0 && policies[0].ExpireDate != DateTime.MinValue)
                    {
                        policy = policies[0];
                    }

                    if (policy == null)
                    {
                        Policy[] policies2 = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && Active = true]", "ExpireDate DESC", survey.ID);
                        if (policies2.Length > 0 && policies2[0].ExpireDate != DateTime.MinValue)
                        {
                            policy = policies2[0];
                        }
                    }
                }

                if (Company.AcadiaInsuranceCompany.MilestoneDate == DateTime.MinValue || Company.AcadiaInsuranceCompany.MilestoneDate > DateTime.Today)
                {
                    //Do nothing
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(docRemarks))
                        docRemarks = "ARMS - Remark from LC: (none)";
                    else
                        docRemarks = string.Format("ARMS - Remark from LC: {0}", docRemarks);
                }
                // save the doc to the imaging server and get the FileNet reference
                string docTypeCode = (docType != null) ? docType.Code : String.Empty;
                ImagingHelper imaging = new ImagingHelper(this.Survey.Company);
                //WA-881 Doug Bruce 2015-08-31 - Survey Date was being sent for Effective Date; corrected to send policy.EffectiveDate if policy is not null.
                fileNetRef = imaging.StoreFileImage(fileName, mimeType, contentStream, docTypeCode, docRemarks, (policy != null) ? policy.EffectiveDate : DateTime.MinValue, (policy != null) ? policy.ExpireDate : DateTime.MinValue, DateTime.Now, survey.Insured.ClientID, policy, survey.WorkflowSubmissionNumber);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save file '" + fileName + "' of type '" + mimeType + "' to FileNet.  See inner exception for details.", ex);
            }

            return fileNetRef;
        }

        #region PickUserForSurvey - AIC goes by Agency State
        /// <summary>
        /// Picks a UserTerritory based on territory assignment rules setup for the company.
        /// If more than one user is assigned to the matching territory, one is picked at random
        /// using the assignment weight assigned to the users.
        /// </summary>
        /// <param name="survey">Survey forwhich to pick the user.</param>
        /// <param name="surveyType">Mode the survey will be in after assignment.</param>
        /// <returns>A UserTerritory matching the constraints provided, or null if no match could be found.</returns>
        protected override UserTerritory PickUserForSurvey(Survey survey, SurveyStatusType surveyType)
        {
            // build a query to find matching user/territory assignments for the type of territory being requested
            // and where the user would not be outside any sepecified premium limit (and are not diabled).
            string filter = "Territory[SurveyStatusTypeCode = ?].GeographicAreas[GeographicUnitCode = ? && AreaValue = ?]"
                + " && User[CompanyID = ? && AccountDisabled = false]";
            string sort = "PercentAllocated DESC";

            // compile the query (we might need to run it three times in a row)
            CompiledQuery query = DB.Engine.Compile(new OPathQuery(typeof(UserTerritory), filter, sort));

            //Get the assigned user's profit center
            string profitCenter = (survey.AssignedUser != null ? survey.AssignedUser.ProfitCenterCode.ToUpper() : string.Empty);

            // get state territory assignment matches
            UserTerritory[] matches;

            // look for profit center assignment matches
            matches = (UserTerritory[])DB.FetchArray(query,
                surveyType.Code, GeographicUnit.ProfitCenter.Code, profitCenter,
                survey.CompanyID);

            if (matches.Length == 0)
            {
                // look for 5-digit zip assignment matches
                matches = (UserTerritory[])DB.FetchArray(query,
                    surveyType.Code, GeographicUnit.Zip5.Code, survey.Location.ZipCode,
                    survey.CompanyID);
            }

            if (matches.Length == 0)
            {
                // look for 3-digit zip assignment matches
                matches = (UserTerritory[])DB.FetchArray(query,
                    surveyType.Code, GeographicUnit.Zip3.Code, (survey.Location.ZipCode.Length >= 3 ? survey.Location.ZipCode.Substring(0, 3) : string.Empty),
                    survey.CompanyID);
            }

            if (matches.Length == 0)
            {
                // look for state assignment matches			
                matches = (UserTerritory[])DB.FetchArray(query,
                    surveyType.Code, GeographicUnit.State.Code, survey.Insured.Agency.StateCode,
                    survey.CompanyID);
            }

            // we're done if no match was found
            if (matches.Length == 0)
            {
                return null;
            }

            //determine if allocation is based on percentage or premium amounts
            if (matches[0].PercentAllocated != int.MinValue)
            {
                // extract the allocations and determine sum
                double total = 0;
                double[] allocations = new double[matches.Length];
                for (int i = 0; i < matches.Length; i++)
                {
                    double alloc = matches[i].PercentAllocated;
                    allocations[i] = alloc;
                    total += alloc;
                }

                // we're done if total allocation is zero
                if (total <= 0)
                {
                    return null;
                }

                // generate a random number between 0 and our total (e.g., 0 <= value < total)
                double rnd = _randomNumbers.NextDouble() * total;

                // find the user with allocation falling in the range of the generated random number
                double low = 0;
                for (int i = 0; i < allocations.Length; i++)
                {
                    double high = low + allocations[i];
                    if (low <= rnd && rnd < high) // this is our match
                    {
                        return matches[i];
                    }
                    low = high;
                }

                // we couldn't find a user for some reason (algorithm problem)
                throw new Exception("No user was selected for Survey " + survey.Number + ".  The random number is " + rnd + " and the user count is " + matches.Length);
            }
            else
            {
                decimal premium = 0;

                Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                foreach (Policy policy in policies)
                {
                    premium = (policy.Premium != decimal.MinValue) ? policy.Premium + premium : premium;
                }

                foreach (UserTerritory match in matches)
                {
                    if (premium >= match.MinPremiumAmount && premium <= match.MaxPremiumAmount)
                        return match;
                }

                //if we got this far than there were no matches
                return null;
            }
        }
        #endregion

        /// <summary>
        /// Gets the list of available actions for vendors based on the survey status.
        /// </summary>
        public override SurveyActionType[] GetExternalAvailableActions()
        {
            ArrayList list = new ArrayList();
            if (this.Survey.StatusCode == SurveyStatus.ReturningToCompany.Code)
            {
                list.Add(SurveyActionType.ReturnToCompany);
            }
            else if (this.Survey.StatusCode == SurveyStatus.AwaitingAcceptance.Code)
            {
                list.Add(SurveyActionType.AcceptSurvey);
                list.Add(SurveyActionType.ReturnToCompany);
                list.Add(SurveyActionType.AddNote);
            }
            else if (this.Survey.StatusCode == SurveyStatus.AssignedSurvey.Code)
            {
                list.Add(SurveyActionType.ReturnToCompany);
                list.Add(SurveyActionType.SetSurveyDetails);
                list.Add(SurveyActionType.DownloadReport);
                list.Add(SurveyActionType.ManageRecs);
                list.Add(SurveyActionType.RemoveRec);
                list.Add(SurveyActionType.SendForReview);
                list.Add(SurveyActionType.AddNote);
                list.Add(SurveyActionType.AddDocument);
            }

            return list.ToArray(typeof(SurveyActionType)) as SurveyActionType[];
        }
    }
}