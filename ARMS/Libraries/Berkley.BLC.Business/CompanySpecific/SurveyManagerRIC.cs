﻿using System;
using Berkley.BLC.Entities;
using Wilson.ORMapper;
using System.IO;

namespace Berkley.BLC.Business.CompanySpecific
{
    /// <summary>
    /// Concrete class that implements SurveyManager functions for RIC.
    /// </summary>
    public class SurveyManagerRIC: SurveyManager
    {
        private static readonly WorkflowHelper workflow;

        static SurveyManagerRIC()
        {
            workflow = new WorkflowHelper(Company.RiverportInsuranceCompany, Berkley.Workflow.WorkflowSystemType.ALBPM_5);
        }

        internal SurveyManagerRIC(Survey survey, User currentUser, bool overridePermissions)
            : base(survey, currentUser, overridePermissions)
        {
        }

        /// <summary>
        /// Overridden. Returns the workflow provider.
        /// </summary>
        protected override WorkflowHelper Workflow
        {
            get
            {
                return workflow;
            }
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnCloseAndNotifyComplete(Transaction trans, Survey survey, string explainText, User currentUser, string recRount)
        {
            Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

            string ccEmail = (survey.ConsultantUser != null && !survey.ConsultantUser.IsFeeCompany) ? survey.ConsultantUser.EmailAddress : string.Empty;
            
            EmailHelper email = new EmailHelper(survey, currentUser);
            email.SendEmailToUnderwriter(EmailHelper.NotificationType.ClosedSurvey, underwriter, ccEmail, explainText, recRount);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCancelComplete(Transaction trans, Survey survey, string explainText, User currentUser)
        {
            Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

            string ccEmail = (survey.ConsultantUser != null && !survey.ConsultantUser.IsFeeCompany) ? survey.ConsultantUser.EmailAddress : string.Empty;

            EmailHelper email = new EmailHelper(survey, currentUser);
            email.SendEmailToUnderwriter(EmailHelper.NotificationType.CancelledSurvey, underwriter, ccEmail, explainText, string.Empty);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnSurveyReviewComplete(Transaction trans, Survey survey, User currentUser)
        {
            EmailHelper email = new EmailHelper(survey, currentUser);
            email.SendEmailToConsultant(EmailHelper.NotificationType.CompletedQCR, string.Empty);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnSendForReviewComplete(Transaction trans, Survey currentSurvey, Survey futureSurvey, bool isNewVisit, bool reviewNeeded, string uwInstructions)
        {
            Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", currentSurvey.CompanyID, currentSurvey.UnderwriterCode);

            string ccEmail = (currentSurvey.ConsultantUser != null && !currentSurvey.ConsultantUser.IsFeeCompany) ? currentSurvey.ConsultantUser.EmailAddress : string.Empty;

            EmailHelper email = new EmailHelper(currentSurvey, currentSurvey.AssignedUser);
            email.SendEmailToUnderwriter(EmailHelper.NotificationType.CompletedSurvey, underwriter, ccEmail, string.Empty, string.Empty);
        }
    }
}
