using Berkley.BLC.Entities;
using System;
using System.IO;
using Wilson.ORMapper;

namespace Berkley.BLC.Business.CompanySpecific
{
    /// <summary>
    /// Concrete class that implements SurveyManager functions for BOG.
    /// </summary>
    public class SurveyManagerBOG : SurveyManager
    {
        private static readonly WorkflowHelper workflow;

        //Parameter configurations
        protected string _sendEmailNotificationsTo;

        static SurveyManagerBOG()
        {
            workflow = new WorkflowHelper(Company.BerkleyOilGas, Berkley.Workflow.WorkflowSystemType.ALBPM_5);
        }

        internal SurveyManagerBOG(Survey survey, User currentUser, bool overridePermissions)
            : base(survey, currentUser, overridePermissions)
        {
        }

        /// <summary>
        /// Overridden. Returns the workflow provider.
        /// </summary>
        protected override WorkflowHelper Workflow
        {
            get
            {
                return workflow;
            }
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnSendForReviewComplete(Transaction trans, Survey currentSurvey, Survey futureSurvey, bool isNewVisit, bool reviewNeeded, string uwInstructions)
        {
            //Per Squish 21546 - No email to UW for this action
            //string ccList = string.IsNullOrEmpty(BLC.Core.CoreSettings.BOGEmailNotification) ? string.Empty : BLC.Core.CoreSettings.BOGEmailNotification;
            //if (!currentSurvey.UnderwriterNotified)
            //{
            //    Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", currentSurvey.CompanyID, currentSurvey.UnderwriterCode);

            //    EmailHelper email = new EmailHelper(currentSurvey, CurrentUser);
            //    email.SendEmailToUnderwriter(EmailHelper.NotificationType.CompletedSurvey, underwriter, ccList, string.Empty, string.Empty);

            //    Survey editSurvey = (currentSurvey.IsReadOnly) ? currentSurvey.GetWritableInstance() : currentSurvey;
            //    editSurvey.UnderwriterNotified = true;

            //    base.SaveSurvey(trans, editSurvey, null);
            //}

            //Get listof email addresses from configuration
            //string emailAddress = Utility.GetCompanyParameter("SendEmailNotificationsTo", CurrentUser.Company).ToLower();

            //if (!string.IsNullOrWhiteSpace(emailAddress))
            //{
            //    EmailHelper email = new EmailHelper(currentSurvey, CurrentUser);
            //    email.SendEmailNotification(EmailHelper.NotificationType.ReviewSurvey, string.Empty, emailAddress);
            //}
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnCloseAndNotifyComplete(Transaction trans, Survey survey, string explainText, User currentUser, string recRount)
        {
            Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);          

            string emailAddress = Utility.GetCompanyParameter("SendEmailNotificationsTo", CurrentUser.Company).ToLower();

            if (survey.ConsultantUser != null && !string.IsNullOrWhiteSpace(emailAddress))
                emailAddress = string.Format("{0};{1}", emailAddress, survey.ConsultantUser.EmailAddress);

            EmailHelper email = new EmailHelper(survey, currentUser);
            email.SendEmailToUnderwriter(EmailHelper.NotificationType.ClosedSurvey, underwriter, emailAddress, explainText, recRount);
            
             if (survey.Underwriter2Code != null && survey.Underwriter2Code != string.Empty)
             { 
                Underwriter underwriter2 = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.Underwriter2Code);
                email.SendEmailToUnderwriter(EmailHelper.NotificationType.ClosedSurvey, underwriter2, emailAddress, explainText, recRount);
             }

        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCancelComplete(Transaction trans, Survey survey, string explainText, User currentUser)
        {

            string emailAddress = Utility.GetCompanyParameter("SendEmailNotificationsTo", CurrentUser.Company).ToLower();

            //if (survey.ConsultantUser != null && !string.IsNullOrWhiteSpace(emailAddress))
            //    emailAddress = string.Format("{0};{1}", emailAddress, survey.ConsultantUser.EmailAddress);

            //Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

            //EmailHelper email = new EmailHelper(survey, currentUser);
            //email.SendEmailToUnderwriter(EmailHelper.NotificationType.CancelledSurvey, underwriter, emailAddress, explainText, string.Empty);

            if (!string.IsNullOrWhiteSpace(emailAddress))
            {
                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailNotification(EmailHelper.NotificationType.CancelledSurvey, string.Empty, emailAddress);
            }

        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override Survey OnCreateSurveyComplete(Transaction trans, Survey survey)
        {
            //Get listof email addresses from configuration
            string emailAddress = Utility.GetCompanyParameter("SendEmailNotificationsTo", CurrentUser.Company).ToLower();
            
            if (!string.IsNullOrWhiteSpace(emailAddress))
            {
                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailNotification(EmailHelper.NotificationType.CreateSurvey, string.Empty, emailAddress);
            }

            //Notify Consultant
            if (survey.AssignedUser != null)
            {
                OnAssignComplete(trans, survey, survey.AssignedUser);
            }

            //Notify UW
            if (survey.UnderwriterCode != null)
            {
                Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);               

                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailToUnderwriter(EmailHelper.NotificationType.CreateSurvey, underwriter, string.Empty, string.Empty, string.Empty);
            }

            if (survey.Underwriter2Code != null && survey.Underwriter2Code != string.Empty)
            {
                Underwriter underwriter2 = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.Underwriter2Code);

                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailToUnderwriter(EmailHelper.NotificationType.CreateSurvey, underwriter2, string.Empty, string.Empty, string.Empty);
            }
            return survey;
        }

        protected override void OnCreateVisitComplete(Transaction trans, Survey survey)
        {
            //Get listof email addresses from configuration
            string emailAddress = Utility.GetCompanyParameter("SendEmailNotificationsTo", CurrentUser.Company).ToLower();

            if (!string.IsNullOrWhiteSpace(emailAddress))
            {
                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailNotification(EmailHelper.NotificationType.CreateSurvey, string.Empty, emailAddress);
            }

            //Notify Consultant
            if (survey.AssignedUser != null)
            {
                OnAssignComplete(trans, survey, survey.AssignedUser);
            }

            //Notify UW
            if (survey.UnderwriterCode != null)
            {
                Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailToUnderwriter(EmailHelper.NotificationType.CreateSurvey, underwriter, string.Empty, string.Empty, string.Empty);
            }

            if (survey.Underwriter2Code != null && survey.Underwriter2Code != string.Empty)
            {
                Underwriter underwriter2 = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.Underwriter2Code);

                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailToUnderwriter(EmailHelper.NotificationType.CreateSurvey, underwriter2, string.Empty, string.Empty, string.Empty);
            }
        }

        protected override void OnAssignComplete(Transaction trans, Survey survey, User newUser)
        {
            //Get listof email addresses from configuration
            string emailAddress = newUser.EmailAddress;

            if (!string.IsNullOrWhiteSpace(emailAddress))
            {
                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailNotification(EmailHelper.NotificationType.AcceptSurvey, string.Empty, emailAddress);
            }
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnReturnToCompanyComplete(Transaction trans, Survey survey)
        {
            //Get listof email addresses from configuration
            string emailAddress = Utility.GetCompanyParameter("SendEmailNotificationsTo", CurrentUser.Company).ToLower();

            if (!string.IsNullOrWhiteSpace(emailAddress))
            {
                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailNotification(EmailHelper.NotificationType.ReturnSurvey, string.Empty, emailAddress);
            }
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override string OnAttachDocument(Transaction trans, Survey survey, string fileName, string mimeType, Stream contentStream, DocType docType, string docRemarks)
        {
            string fileNetRef = string.Empty;
            try
            {
                // remove any pathing info from the specified filename
                fileName = Path.GetFileName(fileName);

                // determine which policy to use
                Policy policy = null;
                if (survey.PrimaryPolicy != null && survey.PrimaryPolicy.ExpireDate != DateTime.MinValue)
                {
                    policy = survey.PrimaryPolicy;
                }
                else //prospect survey
                {
                    Policy[] policies = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && Active = true]", "ExpireDate DESC", survey.ID);
                    if (policies.Length > 0 && policies[0].ExpireDate != DateTime.MinValue)
                    {
                        policy = policies[0];
                    }
                }

                // save the doc to the imaging server and get the FileNet reference
                ImagingHelper imaging = new ImagingHelper(this.Survey.Company);
                fileNetRef = imaging.StoreFileImage(fileName, mimeType, contentStream, "LCR", docRemarks, (survey.SurveyedDate != DateTime.MinValue) ? survey.SurveyedDate : DateTime.Today, (policy != null) ? policy.ExpireDate : DateTime.MinValue, DateTime.Now, survey.Insured.ClientID, policy, survey.WorkflowSubmissionNumber);

                // if the report is attached, then notify that UW that the survey report is ready
                _sendEmailNotificationsTo = Utility.GetCompanyParameter("SendEmailNotificationsTo", CurrentUser.Company).ToLower();
                string emailAddress = _sendEmailNotificationsTo;

                //BOG wants the email to be sent to Company Personnel as well in addition to UW and Consultant
                // so adding the BOG personnel to CC List
                string ccList = emailAddress;
                if (fileName.Contains("RER") || fileName.Contains("SVR"))
                {
                    Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

                    string emailBody = "Attached document is a " + (fileName.Contains("RER") ? "Risk Evaluation Report" : "Service Letter");

                    EmailHelper email = new EmailHelper(survey, CurrentUser);
                    email.SendEmailToUnderwriter(EmailHelper.NotificationType.CompletedSurvey, underwriter, ccList, emailBody, string.Empty);

                    if (survey.Underwriter2Code != null && survey.Underwriter2Code != string.Empty)
                    {
                        Underwriter underwriter2 = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.Underwriter2Code);
                        email.SendEmailToUnderwriter(EmailHelper.NotificationType.CompletedSurvey, underwriter2, ccList, emailBody, string.Empty);
                    }
                    Survey editSurvey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;
                    editSurvey.UnderwriterNotified = true;

                    string history = "Notified underwiter that the report is ready for review.";
                    base.SaveSurvey(trans, editSurvey, history);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save file '" + fileName + "' of type '" + mimeType + "' to FileNet.  See inner exception for details.", ex);
            }

            return fileNetRef;
        }
        
        public override void SendForReview(bool futureVisitNeeded, bool reviewNeeded, SurveyType futureSurveyType, User futureUser, DateTime futureDate, string futurePurpose, string uwInstructions)
        {
            reviewNeeded = false;   //BOG would only like a review to be done for Vendor audits

            VerifyUserCanTakeAction(SurveyActionType.SendForReview);

            Survey survey = (this.Survey.IsReadOnly) ? this.Survey.GetWritableInstance() : this.Survey;
            bool isNewVisit = (futureVisitNeeded && !survey.FutureVisitNeeded);

            // cost and hours must be specified if being sent by a fee consultant
            if (survey.AssignedUser.IsFeeCompany)
            {
                if (survey.Hours == decimal.MinValue)
                {
                    throw new InvalidOperationException("Survey Hours properties must be specified.");
                }

                // round the hours and cost to 2 decimal places
                survey.Hours = decimal.Round(survey.Hours, 2);
                survey.FeeConsultantCost = (survey.FeeConsultantCost != decimal.MinValue) ? decimal.Round(survey.FeeConsultantCost, 2) : decimal.MinValue;
                reviewNeeded = true;
            }

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnSendForReview(trans, survey);
                Survey futureSurvey = SendForReview(trans, survey, futureVisitNeeded, reviewNeeded, futureSurveyType, futureUser, futureDate, futurePurpose);

                if (survey.ConsultantUser != null && !survey.Correction)
                {
                    survey = survey.GetWritableInstance();
                    survey.UnderwriterNotified = true;
                    OnSendForReviewComplete(trans, survey, futureSurvey, isNewVisit, reviewNeeded, uwInstructions);
                }

                if (!reviewNeeded)
                    OnCloseAndNotifyComplete(trans, survey, "Review not needed", CurrentUser, "");

                trans.Commit();
            }

            this.Survey = survey;
        }

        protected override void OnChangeDueDateComplete(Transaction trans, Survey survey, DateTime newDate, string strReason, ReleaseOwnershipType releaseType)
        {
            string emailAddress = Utility.GetCompanyParameter("SendEmailNotificationsTo", CurrentUser.Company).ToLower();
            
            if (!string.IsNullOrWhiteSpace(emailAddress))
            {
                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailNotification(EmailHelper.NotificationType.DueDateChanged, string.Empty, emailAddress);
            }

            //Notify Consultant
            if (survey.AssignedUser != null)
            {
                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailNotification(EmailHelper.NotificationType.DueDateChanged, string.Empty, survey.AssignedUser.EmailAddress);
            }
        }
    }
}