using System;
using Berkley.BLC.Entities;
using Wilson.ORMapper;
using System.IO;

namespace Berkley.BLC.Business.CompanySpecific
{
    /// <summary>
    /// Concrete class that implements SurveyManager functions for USIG.
    /// </summary>
    public class SurveyManagerUSIG : SurveyManager
    {
        internal SurveyManagerUSIG(Survey survey, User currentUser, bool overridePermissions)
            : base(survey, currentUser, overridePermissions)
        {
        }

        /// <summary>
        /// Overridden. Returns the workflow provider.
        /// </summary>
        protected override WorkflowHelper Workflow
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override string OnAttachDocument(Transaction trans, Survey survey, string fileName, string mimeType, Stream contentStream, DocType docType, string docRemarks)
        {
            string fileNetRef = string.Empty;
            try
            {
                // remove any pathing info from the specified filename
                fileName = Path.GetFileName(fileName);

                //Until USG integrates with BPM, they will not save any doc indexes when saving their docs to 
                //FileNet (so they are not retrievable by outside systems).
                //They want the techs to manually attach the documents to the BPM instance.

                // save the doc to the imaging server and get the FileNet reference
                ImagingHelper imaging = new ImagingHelper(this.Survey.Company);
                fileNetRef = imaging.StoreFileImage(fileName, mimeType, contentStream, string.Empty, string.Empty, DateTime.MinValue, DateTime.MinValue, DateTime.Now, string.Empty, null, string.Empty);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save file '" + fileName + "' of type '" + mimeType + "' to FileNet.  See inner exception for details.", ex);
            }

            return fileNetRef;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        /*
        protected override void AddNote(Transaction trans, Survey survey, string comment, bool internalOnly, DateTime commentDate, string underwriterUsername)
        {
            // build and save the comment
            if (CurrentUser.IsUnderwriter || CurrentUser.Role.IsConsultant || CurrentUser.IsFeeCompany)
            {
                SurveyNote note = new SurveyNote(Guid.NewGuid());
                note.SurveyID = survey.ID;
                note.UserID = this.CurrentUser.ID;
                note.UserName = (this.CurrentUser.IsUnderwriter && !String.IsNullOrEmpty(underwriterUsername)) ? underwriterUsername : string.Empty;
                note.EntryDate = commentDate;
                note.InternalOnly = internalOnly;
                note.Comment = "Instructions from consultant: " + comment;
                note.NoteByConsultant = this.CurrentUser.Role.IsConsultant;
                note.Save(trans);

                // Set the last modified date for the field systems
                survey.LastModifiedOn = DateTime.Now;
                survey.Save(trans);
            }
            else
            {
                string historyEntry = string.Format("Internal comment by {0}: '{1}'.", GetCurrentUserName(), comment);
                SaveSurvey(trans, survey, historyEntry);
            }
        }
        */

        /// <summary>
        /// Moves the survey to the review queue.
        /// </summary>

        public override void SendForReview(bool futureVisitNeeded, bool reviewNeeded, SurveyType futureSurveyType, User futureUser, DateTime futureDate, string futurePurpose, string uwInstructions)
        {
            string historyEntry;
            VerifyUserCanTakeAction(SurveyActionType.SendForReview);
            Survey survey = (this.Survey.IsReadOnly) ? this.Survey.GetWritableInstance() : this.Survey;
            User actionUser = survey.AssignedUser;
            bool isNewVisit = (futureVisitNeeded && !survey.FutureVisitNeeded);

            if (survey.ServiceType.Code == ServiceType.ServiceVisit.Code)
            {
                actionUser = survey.ReviewerUser;

                survey.ConsultantUserID = survey.AssignedUserID;
                ServicePlanSurvey servicePlanSurvey = ServicePlanSurvey.GetOne("SurveyID = ?", survey.ID);

                //Check serviceplan object
                //check valid id assigned
                if (servicePlanSurvey.ServicePlan.AssignedUser == null)
                {
                    base.SendForReview(futureVisitNeeded, reviewNeeded, futureSurveyType, futureUser, futureDate, futurePurpose, uwInstructions);
                }

                survey.AssignedUserID = servicePlanSurvey.ServicePlan.AssignedUserID;
                survey.StatusCode = SurveyStatus.AssignedReview.Code;

                survey.Save();
                Transaction trans = DB.Engine.BeginTransaction();
                //History entry
                historyEntry = string.Format("Survey completed and closed by {0}.", GetCurrentUserName());
                WriteHistoryEntry(trans,survey, historyEntry);
                trans.Commit();
            }
            else
            {
                base.SendForReview(futureVisitNeeded, reviewNeeded, futureSurveyType, futureUser, futureDate, futurePurpose, uwInstructions);
            }

            this.Survey = survey;
          
        }                      

    }
}