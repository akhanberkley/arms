using Berkley.BLC.Entities;
using System;
using System.Collections;
using System.IO;
using Wilson.ORMapper;

namespace Berkley.BLC.Business.CompanySpecific
{
    /// <summary>
    /// Concrete class that implements SurveyManager functions for BOG.
    /// </summary>
    public class SurveyManagerBSUM : SurveyManager
    {
        private static readonly WorkflowHelper workflow;

        //Parameter configurations
        protected string _sendEmailNotificationsTo;

        static SurveyManagerBSUM()
        {
            workflow = new WorkflowHelper(Company.BerkleySpecialtyUnderwritingManagers, Berkley.Workflow.WorkflowSystemType.ALBPM_5);
        }

        internal SurveyManagerBSUM(Survey survey, User currentUser, bool overridePermissions)
            : base(survey, currentUser, overridePermissions)
        {
        }

        /// <summary>
        /// Overridden. Returns the workflow provider.
        /// </summary>
        protected override WorkflowHelper Workflow
        {
            get
            {
                return workflow;
            }
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnSendForReviewComplete(Transaction trans, Survey survey, Survey futureSurvey, bool isNewVisit, bool reviewNeeded, string uwInstructions)
        {
            //Get list of email addresses from configuration
            string ccEmailAddresses = string.Empty;
            ccEmailAddresses = Utility.GetCompanyParameter("SendEmailNotificationsTo", CurrentUser.Company).ToLower();

            Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

            EmailHelper email = new EmailHelper(survey, CurrentUser);
            email.SendEmailToUnderwriter(EmailHelper.NotificationType.ClosedSurvey, underwriter, ccEmailAddresses, string.Empty, string.Empty);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnCloseAndNotifyComplete(Transaction trans, Survey survey, string explainText, User currentUser, string recRount)
        {
            //Get list of email addresses from configuration
            string ccEmailAddresses = string.Empty;
            ccEmailAddresses = Utility.GetCompanyParameter("SendEmailNotificationsTo", CurrentUser.Company).ToLower();

            Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

            EmailHelper email = new EmailHelper(survey, currentUser);
            email.SendEmailToUnderwriter(EmailHelper.NotificationType.ClosedSurvey, underwriter, ccEmailAddresses, explainText, recRount);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override Survey OnCreateSurveyComplete(Transaction trans, Survey survey)
        {
            //Get list of email addresses from configuration
            string ccEmailAddresses = string.Empty;
            ccEmailAddresses = Utility.GetCompanyParameter("SendEmailNotificationsTo", CurrentUser.Company).ToLower();

            if (survey.UnderwriterCode != null)
            {
                Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailToUnderwriter(EmailHelper.NotificationType.CreateSurvey, underwriter, ccEmailAddresses, string.Empty, string.Empty);
            }

            return survey;
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnAssignComplete(Transaction trans, Survey survey, User newUser)
        {
            string emailAddress = newUser.EmailAddress;

            if (!string.IsNullOrWhiteSpace(emailAddress))
            {
                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailNotification(EmailHelper.NotificationType.AcceptSurvey, string.Empty, emailAddress);
            }
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnReturnToCompanyComplete(Transaction trans, Survey survey)
        {
            //Get list of email addresses from configuration
            string ccEmailAddresses = string.Empty;
            ccEmailAddresses = Utility.GetCompanyParameter("SendEmailNotificationsTo", CurrentUser.Company).ToLower();

            if (survey.UnderwriterCode != null)
            {
                Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

                EmailHelper email = new EmailHelper(survey, CurrentUser);
                email.SendEmailToUnderwriter(EmailHelper.NotificationType.ReturnSurvey, underwriter, ccEmailAddresses, string.Empty, string.Empty);
            }

        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override string OnAttachDocument(Transaction trans, Survey survey, string fileName, string mimeType, Stream contentStream, DocType docType, string docRemarks)
        {
            string fileNetRef = string.Empty;
            try
            {
                // remove any pathing info from the specified filename
                fileName = Path.GetFileName(fileName);

                // determine which policy to use
                Policy policy = null;
                if (survey.PrimaryPolicy != null && survey.PrimaryPolicy.ExpireDate != DateTime.MinValue)
                {
                    policy = survey.PrimaryPolicy;
                }
                else //prospect survey
                {
                    Policy[] policies = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && Active = true]", "ExpireDate DESC", survey.ID);
                    if (policies.Length > 0 && policies[0].ExpireDate != DateTime.MinValue)
                    {
                        policy = policies[0];
                    }
                }

                // save the doc to the imaging server and get the FileNet reference
                ImagingHelper imaging = new ImagingHelper(this.Survey.Company);
                fileNetRef = imaging.StoreFileImage(fileName, mimeType, contentStream, "LCR", docRemarks, (survey.SurveyedDate != DateTime.MinValue) ? survey.SurveyedDate : DateTime.Today, (policy != null) ? policy.ExpireDate : DateTime.MinValue, DateTime.Now, survey.Insured.ClientID, policy, survey.WorkflowSubmissionNumber);

                //// if the report is attached, then notify that UW that the survey report is ready
                //if (!survey.UnderwriterNotified && fileName.Contains("RER"))
                //{
                //    Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

                //    EmailHelper email = new EmailHelper(survey, CurrentUser);
                //    email.SendEmailToUnderwriter(EmailHelper.NotificationType.CompletedSurvey, underwriter, string.Empty, string.Empty, string.Empty);

                //    Survey editSurvey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;
                //    editSurvey.UnderwriterNotified = true;

                //    string history = "Notified underwiter that the report is ready for review.";
                //    base.SaveSurvey(trans, editSurvey, history);
                //}
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save file '" + fileName + "' of type '" + mimeType + "' to FileNet.  See inner exception for details.", ex);
            }

            return fileNetRef;
        }

        /// <summary>
        /// Gets the list of available actions for vendors based on the survey status.
        /// </summary>
        public override SurveyActionType[] GetExternalAvailableActions()
        {
            ArrayList list = new ArrayList();
            if (this.Survey.StatusCode == SurveyStatus.ReturningToCompany.Code)
            {
                list.Add(SurveyActionType.ReturnToCompany);
            }
            else if (this.Survey.StatusCode == SurveyStatus.AwaitingAcceptance.Code)
            {
                list.Add(SurveyActionType.AcceptSurvey);
                list.Add(SurveyActionType.ReturnToCompany);
                list.Add(SurveyActionType.AddNote);
            }
            else if (this.Survey.StatusCode == SurveyStatus.AssignedSurvey.Code)
            {
                list.Add(SurveyActionType.ReturnToCompany);
                list.Add(SurveyActionType.ManageRecs);
                list.Add(SurveyActionType.RemoveRec);
                list.Add(SurveyActionType.SetSurveyDetails);
                list.Add(SurveyActionType.SendForReview);
                list.Add(SurveyActionType.AddNote);
                list.Add(SurveyActionType.AddDocument);
            }

            return list.ToArray(typeof(SurveyActionType)) as SurveyActionType[];
        }
    }
}