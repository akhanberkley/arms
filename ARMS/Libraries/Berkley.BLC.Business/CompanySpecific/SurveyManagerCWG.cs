using System;
using Berkley.BLC.Entities;
using Wilson.ORMapper;
using System.IO;

namespace Berkley.BLC.Business.CompanySpecific
{
    /// <summary>
    /// Concrete class that implements SurveyManager functions for CWG.
    /// </summary>
    public class SurveyManagerCWG: SurveyManager
    {
        private static readonly WorkflowHelper workflow;

        static SurveyManagerCWG()
        {
            workflow = new WorkflowHelper(Company.ContinentalWesternGroup, Berkley.Workflow.WorkflowSystemType.ALBPM_5);
        }

        internal SurveyManagerCWG(Survey survey, User currentUser, bool overridePermissions)
            : base(survey, currentUser, overridePermissions)
        {
        }

        /// <summary>
        /// Overridden. Returns the workflow provider.
        /// </summary>
        protected override WorkflowHelper Workflow
        {
            get
            {
                return workflow;
            }
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnSetMailSentDateComplete(Transaction trans, Survey survey, SurveyLetter surveyLetter, User currentUser)
        {
            if (surveyLetter.MergeDocument.Name.Contains("First") && !survey.UnderwriterNotified)
            {
                Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

                EmailHelper email = new EmailHelper(survey, currentUser);
                email.SendEmailToUnderwriter(EmailHelper.NotificationType.CompletedSurvey, underwriter, string.Empty, string.Empty, string.Empty);

                //Determine if a UW questionnaire needs to be sent out
                bool sendQuestionnaire = CompanyCounter.IncrimentCounter(survey.Company, CompanyCounterType.UWQuestionnaire, true);
                if (sendQuestionnaire)
                {
                    //Stub out a new survey review record
                    SurveyReview surveyReview = new SurveyReview(Guid.NewGuid());
                    surveyReview.SurveyID = survey.ID;
                    surveyReview.ReviewerTypeCode = ReviewerType.Underwriter.Code;
                    surveyReview.CreateDate = DateTime.Now;
                    surveyReview.Save(trans);
                    
                    EmailHelper emailQuestionnaire = new EmailHelper(survey, currentUser);
                    emailQuestionnaire.SendEmailToUnderwriter(EmailHelper.NotificationType.Questionnaire, underwriter, string.Empty, string.Empty, string.Empty);
                }

                Survey editSurvey = survey.GetWritableInstance();
                editSurvey.UnderwriterNotified = true;

                base.SaveSurvey(trans, editSurvey, null);
            }
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected override void OnCloseAndNotifyComplete(Transaction trans, Survey survey, string explainText, User currentUser, string recRount)
        {
            Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);
            explainText = explainText + "<br><br><b>Recomendation Status Details :</b>";
            SurveyRecommendation[] eRecs = SurveyRecommendation.GetSortedArray("SurveyID = ?", "PriorityIndex DESC", survey.ID);
            for (int i = 0; i < eRecs.Length; i++)
            {
                explainText += "<br><b>Title:</b> " + eRecs[eRecs.Length - i - 1].Recommendation.Title + "<b>Status:</b> " + eRecs[eRecs.Length - i - 1].RecStatus.Type.Name;
               
            }

            EmailHelper email = new EmailHelper(survey, currentUser);
            email.SendEmailToUnderwriter(EmailHelper.NotificationType.ClosedSurvey, underwriter, string.Empty, explainText, recRount);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnCancelComplete(Transaction trans, Survey survey, string explainText, User currentUser)
        {
            Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Code = ?", survey.CompanyID, survey.UnderwriterCode);

            EmailHelper email = new EmailHelper(survey, currentUser);
            email.SendEmailToUnderwriter(EmailHelper.NotificationType.CancelledSurvey, underwriter, string.Empty, explainText, string.Empty);
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override void OnSurveyReviewComplete(Transaction trans, Survey survey, User currentUser)
        {
            if (survey.ConsultantUser != null && !survey.ConsultantUser.IsFeeCompany)
            {
                EmailHelper email = new EmailHelper(survey, currentUser);
                email.SendEmailToConsultant(EmailHelper.NotificationType.CompletedQCR, string.Empty);
            }
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected override string OnAttachDocument(Transaction trans, Survey survey, string fileName, string mimeType, Stream contentStream, DocType docType, string docRemarks)
        {
            string fileNetRef = string.Empty;
            try
            {
                // remove any pathing info from the specified filename
                fileName = Path.GetFileName(fileName);

                // determine which policy to use
                Policy policy = null;
                if (survey.PrimaryPolicy != null && survey.PrimaryPolicy.ExpireDate != DateTime.MinValue)
                {
                    policy = survey.PrimaryPolicy;
                }
                else //prospect survey
                {
                    Policy[] policies = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && Active = true]", "ExpireDate DESC", survey.ID);
                    if (policies.Length > 0 && policies[0].ExpireDate != DateTime.MinValue)
                    {
                        policy = policies[0];
                    }
                }

                // save the doc to the imaging server and get the FileNet reference
                string docTypeDescription = (docType != null) ? docType.Description : String.Empty;
                ImagingHelper imaging = new ImagingHelper(this.Survey.Company);
                fileNetRef = imaging.StoreFileImage(fileName, mimeType, contentStream, "LCR", docTypeDescription, (survey.SurveyedDate != DateTime.MinValue) ? survey.SurveyedDate : DateTime.Today, (policy != null) ? policy.ExpireDate : DateTime.MinValue, DateTime.Now, survey.Insured.ClientID, policy, survey.WorkflowSubmissionNumber);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save file '" + fileName + "' of type '" + mimeType + "' to FileNet.  See inner exception for details.", ex);
            }

            return fileNetRef;
        }

        #region SendForReview - CWG reviews all surveys.  Need to determine if they go to supervisor or admin assistan.

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected override Survey SendForReview(Transaction trans, Survey survey, bool futureVisitNeeded, bool reviewNeeded, SurveyType futureSurveyType, User futureUser, DateTime futureDate, string futurePurpose)
        {
            string historyEntry;
            User actionUser = survey.AssignedUser;

            // assign survey to last reviewer if one is listed
            if (survey.ReviewerUserID != Guid.Empty)
            {
                actionUser = survey.ReviewerUser;

                survey.ConsultantUserID = survey.AssignedUserID;
                survey.AssignedUserID = survey.ReviewerUserID;
                survey.StatusCode = SurveyStatus.AssignedReview.Code;

                historyEntry = string.Format("{0} sent survey back to {1} for review.", GetCurrentUserName(), survey.AssignedUser.Name);
            }
            else // no reviewer listed
            {
                historyEntry = string.Format("{0} completed the request. ", GetCurrentUserName());

                // try to find a reviewer to assign based on territory
                UserTerritory assignment = PickUserForSurvey(survey, SurveyStatusType.Review);

                if (survey.AssignedUser.IsFeeCompany && survey.AssignedByUser != null)
                {
                    //assign back to the person that assigned the survey to the vendor
                    survey.ConsultantUserID = survey.AssignedUserID;
                    survey.AssignedUserID = survey.AssignedByUserID;
                    survey.StatusCode = SurveyStatus.AssignedReview.Code;
                    historyEntry += string.Format("Survey assigned to {0} for review.", survey.AssignedByUser.Name);
                }
                else if (survey.Review && actionUser.Manager != null)
                {
                    //assign to supervisor
                    survey.ConsultantUserID = survey.AssignedUserID;
                    survey.AssignedUserID = actionUser.ManagerID;
                    survey.StatusCode = SurveyStatus.AssignedReview.Code;
                    historyEntry += string.Format("Survey assigned to {0} since the survey was flagged for review.", actionUser.Manager.Name);
                }
                else if (assignment != null)
                {
                    User targetUser = assignment.User;

                    survey.ConsultantUserID = survey.AssignedUserID;
                    survey.AssignedUserID = targetUser.ID;
                    survey.StatusCode = SurveyStatus.AssignedReview.Code;
                    historyEntry += string.Format("Survey assigned to {0} based on territory '{1}'.",
                        targetUser.Name, assignment.Territory.Name);
                }
                else
                {
                    survey.ConsultantUserID = survey.AssignedUserID;
                    survey.AssignedUserID = Guid.Empty;
                    survey.StatusCode = SurveyStatus.UnassignedReview.Code;
                    historyEntry += "Survey could not be assigned to a reviewer based on the existing territories.";
                }
            }

            //New Future Visit
            Survey newSurvey = null;
            if (futureVisitNeeded && !survey.FutureVisitNeeded)
            {
                survey.FutureVisitNeeded = futureVisitNeeded;
                survey.FutureVisitSurveyTypeID = futureSurveyType.ID;
                survey.FutureVisitDueDate = futureDate;

                //Append to the prior history entry
                historyEntry += string.Format(" Future survey created in ARMS with a due date of {0}.", futureDate.ToShortDateString());

                //Create new survey
                newSurvey = new Survey(new Guid());
                newSurvey.CompanyID = survey.CompanyID;
                newSurvey.InsuredID = survey.InsuredID;
                newSurvey.PrimaryPolicyID = survey.PrimaryPolicyID;
                newSurvey.LocationID = survey.LocationID;
                newSurvey.PreviousSurveyID = survey.ID;
                newSurvey.TypeID = futureSurveyType.ID;
                newSurvey.AssignedUserID = futureUser.ID;
                newSurvey.ServiceTypeCode = survey.ServiceTypeCode;
                newSurvey.StatusCode = SurveyStatus.AssignedSurvey.Code;
                newSurvey.CreateStateCode = CreateState.AssignedToConsultant.Code;
                newSurvey.CreateByInternalUserID = survey.AssignedUserID;
                newSurvey.CreateDate = DateTime.Now;
                newSurvey.AssignDate = DateTime.Now;
                newSurvey.AcceptDate = DateTime.Now;
                newSurvey.DueDate = futureDate;
                newSurvey.RecsRequired = !SurveyTypeType.IsNotWrittenBusiness(futureSurveyType.Type);
                newSurvey.WorkflowSubmissionNumber = survey.WorkflowSubmissionNumber;
                newSurvey.UnderwriterCode = survey.UnderwriterCode;
                newSurvey.Priority = survey.Priority;
                newSurvey.Save(trans);

                //Include all the Policies/Coverages from the original survey
                SurveyPolicyCoverage[] oldCoverages = SurveyPolicyCoverage.GetArray("SurveyID = ?", survey.ID);
                foreach (SurveyPolicyCoverage oldCoverage in oldCoverages)
                {
                    SurveyPolicyCoverage newCoverage = new SurveyPolicyCoverage(newSurvey.ID, oldCoverage.LocationID, oldCoverage.PolicyID, oldCoverage.CoverageNameID);
                    newCoverage.ReportTypeCode = oldCoverage.ReportTypeCode;
                    newCoverage.Active = oldCoverage.Active;
                    newCoverage.Save(trans);
                }

                //Create history entry
                string newEntry = string.Format("Survey created in ARMS by {0} from survey #{1}.", GetCurrentUserName(), survey.Number);
                WriteHistoryEntry(trans, newSurvey, newEntry);

                //Add the purpose as a comment
                if (futurePurpose != string.Empty)
                {
                    SurveyNote note = new SurveyNote(new Guid());
                    note.SurveyID = newSurvey.ID;
                    note.UserID = CurrentUser.ID;
                    note.EntryDate = DateTime.Now;
                    note.InternalOnly = false;
                    note.AdminOnly = false;
                    note.Comment = string.Format("Purpose of Future Survey: {0}", futurePurpose);

                    note.Save(trans);
                }
            }

            //set the report complete date
            //Business Rule:  Do not reset the report complete date if this is a correction (don't want to add extra time).
            if (!survey.Correction && survey.ReportCompleteDate == DateTime.MinValue)
            {
                survey.ReportCompleteDate = DateTime.Now;
            }

            // save our changes
            SaveSurvey(trans, survey, historyEntry, actionUser);

            return newSurvey;
        }

        #endregion

        #region PickUserForSurvey - CWG goes by both Insured State and Policy Symbol
        /// <summary>
        /// Picks a UserTerritory based on territory assignment rules setup for the company.
        /// If more than one user is assigned to the matching territory, one is picked at random
        /// using the assignment weight assigned to the users.
        /// </summary>
        /// <param name="survey">Survey forwhich to pick the user.</param>
        /// <param name="surveyType">Mode the survey will be in after assignment.</param>
        /// <returns>A UserTerritory matching the constraints provided, or null if no match could be found.</returns>
        protected override UserTerritory PickUserForSurvey(Survey survey, SurveyStatusType surveyType)
        {
            // build a query to find matching user/territory assignments for the type of territory being requested
            // and where the user would not be outside any sepecified premium limit (and are not diabled).
            string filter = "Territory[SurveyStatusTypeCode = ?].GeographicAreas[GeographicUnitCode = ? && AreaValue = ?]"
                + " && User[CompanyID = ? && AccountDisabled = false]";
            string sort = "PercentAllocated DESC";

            // compile the query (we might need to run it three times in a row)
            CompiledQuery query = DB.Engine.Compile(new OPathQuery(typeof(UserTerritory), filter, sort));

            //Get the primary policy symbol
            string policySymbol = (survey.PrimaryPolicy != null ? survey.PrimaryPolicy.Symbol.ToUpper() : string.Empty);

            // get state territory assignment matches
            UserTerritory[] matches;

            // look for policy symbol assignment matches
            matches = (UserTerritory[])DB.FetchArray(query,
                surveyType.Code, GeographicUnit.ProfitCenter.Code, policySymbol,
                survey.CompanyID);

            if (matches.Length == 0)
            {
                // look for 5-digit zip assignment matches
                matches = (UserTerritory[])DB.FetchArray(query,
                    surveyType.Code, GeographicUnit.Zip5.Code, survey.Location.ZipCode,
                    survey.CompanyID);
            }

            if (matches.Length == 0)
            {
                // look for 3-digit zip assignment matches
                matches = (UserTerritory[])DB.FetchArray(query,
                    surveyType.Code, GeographicUnit.Zip3.Code, (survey.Location.ZipCode.Length >= 3 ? survey.Location.ZipCode.Substring(0, 3) : string.Empty),
                    survey.CompanyID);
            }

            if (matches.Length == 0)
            {
                // look for state assignment matches			
                matches = (UserTerritory[])DB.FetchArray(query,
                    surveyType.Code, GeographicUnit.State.Code, survey.Insured.StateCode,
                    survey.CompanyID);
            }

            // we're done if no match was found
            if (matches.Length == 0)
            {
                return null;
            }

            //determine if allocation is based on percentage or premium amounts
            if (matches[0].PercentAllocated != int.MinValue)
            {
                // extract the allocations and determine sum
                double total = 0;
                double[] allocations = new double[matches.Length];
                for (int i = 0; i < matches.Length; i++)
                {
                    double alloc = matches[i].PercentAllocated;
                    allocations[i] = alloc;
                    total += alloc;
                }

                // we're done if total allocation is zero
                if (total <= 0)
                {
                    return null;
                }

                // generate a random number between 0 and our total (e.g., 0 <= value < total)
                double rnd = _randomNumbers.NextDouble() * total;

                // find the user with allocation falling in the range of the generated random number
                double low = 0;
                for (int i = 0; i < allocations.Length; i++)
                {
                    double high = low + allocations[i];
                    if (low <= rnd && rnd < high) // this is our match
                    {
                        return matches[i];
                    }
                    low = high;
                }

                // we couldn't find a user for some reason (algorithm problem)
                throw new Exception("No user was selected for Survey " + survey.Number + ".  The random number is " + rnd + " and the user count is " + matches.Length);
            }
            else
            {
                decimal premium = 0;

                Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                foreach (Policy policy in policies)
                {
                    premium = (policy.Premium != decimal.MinValue) ? policy.Premium + premium : premium;
                }

                foreach (UserTerritory match in matches)
                {
                    if (premium >= match.MinPremiumAmount && premium <= match.MaxPremiumAmount)
                        return match;
                }

                //if we got this far than there were no matches
                return null;
            }
        }
        #endregion
    }
}