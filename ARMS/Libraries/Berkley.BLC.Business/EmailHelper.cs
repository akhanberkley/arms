using Berkley.BLC.Core;
using Berkley.BLC.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;


namespace Berkley.BLC.Business
{
    /// <summary>
    /// Contains utility methods to help emailing tasks details.
    /// </summary>
    public class EmailHelper
    {
        private Survey _survey;
        private User _currentUser;
        private string _policyNumberList;

        #region Enum
        /// <summary>
        /// The different types of mail notifications.
        /// </summary>
        public enum NotificationType
        {
            ClosedSurvey,
            CancelledSurvey,
            CompletedSurvey,
            EmailUnderwriter,
            EmailUser,
            Questionnaire,
            CompletedQCR,
            CreateSurvey,
            AcceptSurvey,
            ReturnSurvey,
            AttachDocument,
            DueDateChanged,
            ReviewSurvey
        }
        #endregion

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        public EmailHelper(Survey survey, User currentUser)
        {
            _survey = survey;
            _currentUser = currentUser;
            _policyNumberList = DB.Engine.ExecuteScalar(string.Format("SELECT dbo.GetSurveyPolicyList('{0}')", _survey.ID)).ToString();
        }

        /// <summary>
        /// Sends an email to an underwriter.
        /// </summary>
        /// <param name="type">The notification type.</param>
        public void SendEmailToUnderwriter(NotificationType type, Underwriter underwriter, string ccEmail, string bodyText, string recCount)
        {
            //make sure we have a valid UW and email
            if (underwriter == null)
            {
                throw new QCI.Web.Validation.FieldValidationException("There is no underwriter assigned to the account of this survey request.  Please correct and try again.");
            }

            if (!EmailRegex(underwriter.EmailAddress))
            {
                throw new QCI.Web.Validation.FieldValidationException("The email address on record for this underwriter is not valid.  Please contact an administrator to correct.");
            }

            MailAddress messageFrom = new MailAddress(_survey.Company.EmailFromAddress);
            MailAddress messageTo;

            //Don't send emails to the actual UW unless we are in production
            if (CoreSettings.ConfigurationType == ConfigurationType.Production)
            {
                messageTo = new MailAddress(underwriter.EmailAddress);
            }
            else
            {
                messageTo = new MailAddress(_currentUser.EmailAddress);
            }

            // Create a message and set up the recipients.
            MailMessage message = new MailMessage(messageFrom, messageTo);
            SmtpClient client = new SmtpClient(Berkley.BLC.Core.CoreSettings.SmtpServer);

            // Check if anyone needs to be CC'd
            if (!string.IsNullOrEmpty(ccEmail))
            {
                List<string> CCAddresses = ccEmail.Split(';').ToList();
                foreach (string address in CCAddresses)
                {
                    message.CC.Add(new MailAddress(address));
                }

            }

            message.IsBodyHtml = true;

            //build the start of the email body
            StringBuilder sb = new StringBuilder();
            sb.Append("<font size='2' face='Arial'>");
            if (_survey.Number > 0)
                sb.AppendFormat("<b>Survey Number:</b> {0}<br>", _survey.Number);
            sb.AppendFormat("<b>Policy Number(s):</b> {0}<br>", _policyNumberList);
            sb.AppendFormat("<b>Insured Name:</b> {0}<br><br>", _survey.Insured.Name);

            string emailBody = string.Empty;
            if (type == NotificationType.CompletedSurvey)
            {
                message.Subject = string.Format("Survey #{0} ({1}) Completed in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyCompletedText(sb, bodyText);
            }
            else if (type == NotificationType.ClosedSurvey)
            {
                message.Subject = string.Format("Survey #{0} ({1}) Closed in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyClosedText(sb, bodyText, recCount);
            }
            else if (type == NotificationType.CancelledSurvey)
            {   
                message.Subject = string.Format("Survey #{0} ({1}) Cancelled in ARMS", _survey.Number, _survey.Insured.Name);                
                emailBody = SurveyCancelledText(sb, bodyText);
            }
            else if (type == NotificationType.EmailUnderwriter)
            {
                message.Subject = string.Format("Comment about Survey #{0} ({1}) in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyEmailText(sb, bodyText);
            }
            else if (type == NotificationType.Questionnaire)
            {
                message.Subject = string.Format("Questionnaire for Survey #{0} ({1}) in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = QuestionnaireEmailText(sb, bodyText);
            }
            else if (type == NotificationType.AttachDocument)
            {
                if (bodyText.Contains("CRITICAL"))
                {
                    message.Subject = string.Format("A new document for Survey #{0} ({1}) in ARMS was attached with NEW CRITICAL RECS", _survey.Number, _survey.Insured.Name);
                }
                else
                {
                    message.Subject = string.Format("A new document for Survey #{0} ({1}) in ARMS was attached", _survey.Number, _survey.Insured.Name);
                }                
                emailBody = SurveyAttachDocumentText(sb, bodyText);
            }
            else if (type == NotificationType.CreateSurvey)
            {
                message.Subject = string.Format("New Survey ({0}) Created in ARMS", _survey.Insured.Name);
                emailBody = SurveyCreatedText(sb, bodyText);
            }
            else if (type == NotificationType.ReviewSurvey)
            {
                message.Subject = string.Format("Review for Survey ({0}) has been completed in ARMS", _survey.Insured.Name);
                emailBody = SurveyReviewText(sb, bodyText);
            }
            else
            {
                emailBody = "";
            }

            //set the body
            message.Body = emailBody;

            client.Send(message);
        }


        /// <summary>
        /// Sends an email to an user.
        /// </summary>
        /// <param name="type">The notification type.</param>
        public void SendEmailToUser(NotificationType type, User user, string ccEmail, string bodyText, string recCount)
        {
            //make sure we have a valid UW and email
            if (user == null)
            {
                throw new QCI.Web.Validation.FieldValidationException("There is no user assigned to the account of this survey request.  Please correct and try again.");
            }

            if (!EmailRegex(user.EmailAddress))
            {
                throw new QCI.Web.Validation.FieldValidationException("The email address on record for this user is not valid.  Please contact an administrator to correct.");
            }

            MailAddress messageFrom = new MailAddress(_survey.Company.EmailFromAddress);
            MailAddress messageTo;

            //Don't send emails to the actual UW unless we are in production
            if (CoreSettings.ConfigurationType == ConfigurationType.Production)
            {
                messageTo = new MailAddress(user.EmailAddress);
            }
            else
            {
                messageTo = new MailAddress(_currentUser.EmailAddress);
            }

            // Create a message and set up the recipients.
            MailMessage message = new MailMessage(messageFrom, messageTo);
            SmtpClient client = new SmtpClient(Berkley.BLC.Core.CoreSettings.SmtpServer);

            // Check if anyone needs to be CC'd
            if (!string.IsNullOrEmpty(ccEmail))
            {
                List<string> CCAddresses = ccEmail.Split(';').ToList();
                foreach (string address in CCAddresses)
                {
                    message.CC.Add(new MailAddress(address));
                }

            }

            message.IsBodyHtml = true;

            //build the start of the email body
            StringBuilder sb = new StringBuilder();
            sb.Append("<font size='2' face='Arial'>");
            if (_survey.Number > 0)
                sb.AppendFormat("<b>Survey Number:</b> {0}<br>", _survey.Number);
            sb.AppendFormat("<b>Policy Number(s):</b> {0}<br>", _policyNumberList);
            sb.AppendFormat("<b>Insured Name:</b> {0}<br><br>", _survey.Insured.Name);

            string emailBody = string.Empty;
            if (type == NotificationType.CompletedSurvey)
            {
                message.Subject = string.Format("Survey #{0} ({1}) Completed in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyCompletedText(sb, bodyText);
            }
            else if (type == NotificationType.ClosedSurvey)
            {
                message.Subject = string.Format("Survey #{0} ({1}) Closed in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyClosedText(sb, bodyText, recCount);
            }
            else if (type == NotificationType.CancelledSurvey)
            {
                message.Subject = string.Format("Survey #{0} ({1}) Cancelled in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyCancelledText(sb, bodyText);
            }
            else if (type == NotificationType.EmailUser)
            {
                message.Subject = string.Format("Comment about Survey #{0} ({1}) in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyEmailText(sb, bodyText);
            }
            else if (type == NotificationType.Questionnaire)
            {
                message.Subject = string.Format("Questionnaire for Survey #{0} ({1}) in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = QuestionnaireEmailText(sb, bodyText);
            }
            else if (type == NotificationType.AttachDocument)
            {
                message.Subject = string.Format("A new document for Survey #{0} ({1}) in ARMS was attached", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyAttachDocumentText(sb, bodyText);
            }
            else if (type == NotificationType.CreateSurvey)
            {
                message.Subject = string.Format("New Survey ({0}) Created in ARMS", _survey.Insured.Name);
                emailBody = SurveyCreatedText(sb, bodyText);
            }
            else
            {
                emailBody = "";
            }

            //set the body
            message.Body = emailBody;

            client.Send(message);
        }
        /// <summary>
        /// Sends an email to a consultant.
        /// </summary>
        /// <param name="type">The notification type.</param>
        public void SendEmailToConsultant(NotificationType type, string commentsToConsultant)
        {
            //make sure we have a valid UW and email
            User consultant = (_survey.ConsultantUser != null) ? _survey.ConsultantUser : _survey.AssignedUser;

            if (consultant == null || consultant.IsFeeCompany)
            {
                throw new QCI.Web.Validation.FieldValidationException("A company consultant has not completed or has not been assigned this survey.  Please correct and try again.");
            }

            if (!EmailRegex(consultant.EmailAddress))
            {
                throw new QCI.Web.Validation.FieldValidationException("The email address on record for the consultant who completed or was assigned this request is not valid.  Please correct and try again.");
            }

            MailAddress messageFrom = new MailAddress(_survey.Company.EmailFromAddress);
            MailAddress messageTo;

            //Don't send emails to the actual consultant unless we are in production
            if (CoreSettings.ConfigurationType == ConfigurationType.Production)
            {
                messageTo = new MailAddress(consultant.EmailAddress);
            }
            else
            {
                messageTo = new MailAddress(_currentUser.EmailAddress);
            }

            // Create a message and set up the recipients.
            MailMessage message = new MailMessage(messageFrom, messageTo);
            SmtpClient client = new SmtpClient(Berkley.BLC.Core.CoreSettings.SmtpServer);

            message.IsBodyHtml = true;

            //build the start of the email body
            StringBuilder sb = new StringBuilder();
            sb.Append("<font size='2' face='Arial'>");
            if (_survey.Number > 0)
                sb.AppendFormat("<b>Survey Number:</b> {0}<br>", _survey.Number);
            sb.AppendFormat("<b>Policy Number(s):</b> {0}<br>", _policyNumberList);
            sb.AppendFormat("<b>Insured Name:</b> {0}<br><br>", _survey.Insured.Name);

            string emailBody = string.Empty;
            if (type == NotificationType.CompletedQCR)
            {
                message.Subject = string.Format("QCR completed for Survey #{0} ({1}) in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = QCRCompletedText(sb);
            }
            else if (type == NotificationType.CancelledSurvey)
            {
                message.Subject = string.Format("Survey #{0} ({1}) Cancelled in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyCancelledText(sb, commentsToConsultant);
            }
            else
            {
                emailBody = "";
            }

            //set the body
            message.Body = emailBody;

            client.Send(message);
        }

        /// <summary>
        /// Sends an email to a consultant.
        /// </summary>
        /// <param name="type">The notification type.</param>
        public void SendEmailNotification(NotificationType type, string comments, string To)
        {
            List<string> ToAddresses = To.Split(';').ToList();
            MailAddress messageFrom = new MailAddress(_survey.Company.EmailFromAddress);
            MailAddress messageTo;


            if (ToAddresses != null && ToAddresses.Count > 1)
                messageTo = new MailAddress(ToAddresses[0]);
            else
                messageTo = new MailAddress(To);

            //Don't send emails to the actual consultant unless we are in production
            if (CoreSettings.ConfigurationType != ConfigurationType.Production)
            {
                messageTo = new MailAddress(_currentUser.EmailAddress);
            }


            // Create a message and set up the recipients.
            MailMessage message = new MailMessage(messageFrom, messageTo);
            if (CoreSettings.ConfigurationType == ConfigurationType.Production)
            {
                foreach (string address in ToAddresses)
                {
                    if (address != messageTo.Address)
                        message.To.Add(new MailAddress(address));
                }
            }
            SmtpClient client = new SmtpClient(Berkley.BLC.Core.CoreSettings.SmtpServer);

            message.IsBodyHtml = true;

            //build the start of the email body
            StringBuilder sb = new StringBuilder();
            sb.Append("<font size='2' face='Arial'>");
            if (_survey.Number > 0)
                sb.AppendFormat("<b>Survey Number:</b> {0}<br>", _survey.Number);
            if (_policyNumberList != null && _policyNumberList.Length > 0)
                sb.AppendFormat("<b>Policy Number(s):</b> {0}<br>", _policyNumberList);
            sb.AppendFormat("<b>Insured Name:</b> {0}<br><br>", _survey.Insured.Name);

            string emailBody = string.Empty;
            if (type == NotificationType.CompletedQCR)
            {
                message.Subject = string.Format("QCR completed for Survey #{0} ({1}) in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = QCRCompletedText(sb);
            }
            else if (type == NotificationType.CancelledSurvey)
            {
                message.Subject = string.Format("Survey #{0} ({1}) Cancelled in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyCancelledText(sb, comments);
            }
            else if (type == NotificationType.CreateSurvey)
            {
                message.Subject = string.Format("New Survey #{0} ({1}) Created in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyCreatedText(sb, comments);
            }
            else if (type == NotificationType.AcceptSurvey)
            {
                message.Subject = string.Format("Survey #{0} ({1}) is assgined to you in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyAcceptText(sb, comments);
            }
            else if (type == NotificationType.ReturnSurvey)
            {
                message.Subject = string.Format("Survey #{0} ({1}) has been returned/rejected in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyReturnedText(sb, comments);
            }
            else if (type == NotificationType.ClosedSurvey)
            {
                message.Subject = string.Format("Survey ({0}) Closed in ARMS", _survey.Insured.Name);
                emailBody = SurveyClosedText(sb, comments, string.Empty);
            }
            else if(type == NotificationType.DueDateChanged)
            {
                message.Subject = string.Format("Survey #{0} ({1}) due date has changed in ARMS", _survey.Number, _survey.Insured.Name);
                emailBody = SurveyDueDateChangedText(sb, string.Empty);
            }
            else
            {
                emailBody = "";
            }

            //set the body
            message.Body = emailBody;

            client.Send(message);
        }

        private string QCRCompletedText(StringBuilder sb)
        {
            sb.AppendFormat("A QCR for this survey has been completed by {0} in ARMS.<br><br>", _currentUser.Name);
            sb.AppendFormat("Click on the following URL to view the QCR: " + SurveyReviewPage + "&reviewertype={1}", _survey.ID, ReviewerType.Manager.Code);
            sb.AppendFormat("</font>");

            return sb.ToString();
        }

        private string SurveyClosedText(StringBuilder sb, string text, string recCount)
        {
            if (!string.IsNullOrEmpty(recCount) && recCount != "0/0")
            {
                sb.AppendFormat("This survey has been closed by {0} in ARMS with {1} suggestions remaining open.<br><br>", _currentUser.Name, recCount);
            }
            else
            {
                sb.AppendFormat("This survey has been closed by {0} in ARMS with no suggestions needed.<br><br>", _currentUser.Name);
            }

            if (!String.IsNullOrEmpty(text))
            {
                sb.AppendFormat("{0} has included the following remarks: {1}<br><br>", _currentUser.Name, text);
            }

            sb.AppendFormat("Click on the following URL to view the survey: " + SurveyPage + "&CompanyNumber={1}", _survey.ID, _survey.Company.Number);
            sb.AppendFormat("</font>");

            return sb.ToString();
        }

        private string SurveyCompletedText(StringBuilder sb, string text)
        {
            sb.AppendFormat("This survey report has been completed by {0} in ARMS and is ready for your review.<br><br>", (_survey.ConsultantUser != null) ? _survey.ConsultantUser.Name : _survey.AssignedUser.Name);

            if (!String.IsNullOrEmpty(text))
            {
                sb.AppendFormat("{0} has included the following remarks: {1}<br><br>", _currentUser.Name, text);
            }

            sb.AppendFormat("Click on the following URL to view the survey: " + SurveyPage + "&CompanyNumber={1}", _survey.ID, _survey.Company.Number);
            sb.AppendFormat("</font>");

            return sb.ToString();
        }

        private string SurveyCreatedText(StringBuilder sb, string text)
        {
            sb.AppendFormat("This survey has been Created by {0} in ARMS and is ready for assignment.<br><br>", (_survey.CreateByInternalUser != null) ? _survey.CreateByInternalUser.Name : _currentUser.Name);

            if (!String.IsNullOrEmpty(text))
            {
                sb.AppendFormat("{0} has included the following remarks: {1}<br><br>", _currentUser.Name, text);
            }

            sb.AppendFormat("Click on the following URL to view the survey: " + SurveyPage + "&CompanyNumber={1}", _survey.ID, _survey.Company.Number);
            sb.AppendFormat("</font>");

            return sb.ToString();
        }

        private string SurveyReviewText(StringBuilder sb, string text)
        {
            sb.AppendFormat("This survey has been reviewed by {0} in ARMS.<br><br>", (_survey.ReviewerUser != null) ? _survey.ReviewerUser.Name : _currentUser.Name);

            if (!String.IsNullOrEmpty(text))
            {
                sb.AppendFormat("{0} has included the following remarks: {1}<br><br>", _currentUser.Name, text);
            }

            sb.AppendFormat("Click on the following URL to view the survey: " + SurveyPage + "&CompanyNumber={1}", _survey.ID, _survey.Company.Number);
            sb.AppendFormat("</font>");

            return sb.ToString();
        }
        private string SurveyAcceptText(StringBuilder sb, string text)
        {
            sb.AppendFormat("This survey  has been Assigned by {0} in ARMS and is ready for your acceptance.<br><br>", (_survey.AssignedByUser != null) ? _survey.AssignedByUser.Name : _currentUser.Name);

            if (!String.IsNullOrEmpty(text))
            {
                sb.AppendFormat("{0} has included the following remarks: {1}<br><br>", _currentUser.Name, text);
            }

            sb.AppendFormat("Click on the following URL to view the survey: " + SurveyPage + "&CompanyNumber={1}", _survey.ID, _survey.Company.Number);
            sb.AppendFormat("</font>");

            return sb.ToString();
        }

        private string SurveyReturnedText(StringBuilder sb, string text)
        {
            sb.AppendFormat("This survey has been Returned by {0} in ARMS and is ready for your review/reassignment.<br><br>", (_survey.AssignedUser != null) ? _survey.AssignedUser.Name : _currentUser.Name);

            if (!String.IsNullOrEmpty(text))
            {
                sb.AppendFormat("{0} has included the following remarks: {1}<br><br>", _currentUser.Name, text);
            }

            sb.AppendFormat("Click on the following URL to view the survey: " + SurveyPage + "&CompanyNumber={1}", _survey.ID, _survey.Company.Number);
            sb.AppendFormat("</font>");

            return sb.ToString();
        }

        private string SurveyCancelledText(StringBuilder sb, string text)
        {
            sb.AppendFormat("This survey has been cancelled by {0} in ARMS.<br><br>", _currentUser.Name);

            if (!String.IsNullOrEmpty(text))
            {
                sb.AppendFormat("{0} has included the following remarks: {1}<br><br>", _currentUser.Name, text);
            }

            sb.AppendFormat("Click on the following URL to view the survey: " + SurveyPage + "&CompanyNumber={1}", _survey.ID, _survey.Company.Number);
            sb.AppendFormat("</font>");

            return sb.ToString();
        }

        private string SurveyAttachDocumentText(StringBuilder sb, string text)
        {
            sb.AppendFormat("A new document has been attached by {0} in ARMS.<br><br>", _currentUser.Name);

            if (!String.IsNullOrEmpty(text))
            {
                sb.AppendFormat("{0} has included the following remarks: {1}<br><br>", _currentUser.Name, text);
            }

            sb.AppendFormat("Click on the following URL to view the survey: " + SurveyPage + "&CompanyNumber={1}", _survey.ID, _survey.Company.Number);
            sb.AppendFormat("</font>");

            return sb.ToString();
        }

        private string SurveyEmailText(StringBuilder sb, string text)
        {
            sb.AppendFormat("{0} has made the following comment in regards to this survey in ARMS:<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{1}<br><br>", _currentUser.Name, text);
            sb.AppendFormat("Click on the following URL to view the survey: " + SurveyPage + "&CompanyNumber={1}", _survey.ID, _survey.Company.Number);
            sb.AppendFormat("</font>");

            return sb.ToString();
        }

        private string QuestionnaireEmailText(StringBuilder sb, string text)
        {
            sb.AppendFormat("You have been asked to fill out a Loss Control Quality Questionnaire for a recent survey request completed by {0} for this survey in ARMS.<br><br>", _survey.ConsultantUser.Name);
            sb.AppendFormat("Click on the following URL to complete the questionnaire: " + SurveyReviewPage + "&reviewertype={1}" + "&CompanyNumber={2}", _survey.ID, ReviewerType.Underwriter.Code, _survey.Company.Number);
            sb.AppendFormat("</font>");

            return sb.ToString();
        }

        private string SurveyDueDateChangedText(StringBuilder sb, string text)
        {
            sb.AppendFormat("This survey's due date has changed to {0} in ARMS.<br><br>", _survey.DueDate.ToShortDateString());

            if (!String.IsNullOrEmpty(text))
            {
                sb.AppendFormat("{0} has included the following remarks: {1}<br><br>", _currentUser.Name, text);
            }

            sb.AppendFormat("Click on the following URL to view the survey: " + SurveyPage + "&CompanyNumber={1}", _survey.ID, _survey.Company.Number);
            sb.AppendFormat("</font>");

            return sb.ToString();
        }

        private static bool EmailRegex(string emailAddress)
        {
            string patternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
                  + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                  + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                  + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                  + @"[a-zA-Z]{2,}))$";
            Regex reg = new Regex(patternStrict);

            return reg.IsMatch(emailAddress);
        }

        private string SurveyPage
        {
            get
            {
                return CoreSettings.SurveyPage.Replace("ReadOnly", string.Empty);
            }
        }

        private string SurveyReviewPage
        {
            get
            {
                return CoreSettings.SurveyPage.Replace("ReadOnlySurvey", "SurveyReview");
            }
        }
    }
}
