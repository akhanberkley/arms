using System;
using System.Configuration;
using System.Text;
using System.Data;

using Berkley.Workflow;
using Berkley.BLC.Core;
using Berkley.BLC.Entities;
using Berkley.BLC.Import.Prospect;

namespace Berkley.BLC.Business
{
    /// <summary>
    /// Provides utility methods to manage company workflow systems.
    /// </summary>
    public class WorkflowHelper
    {
        private Company _company;
        private WorkflowSystemType _systemType;
        private Contact _contact;
        private SoftwareApplication _application;
        private bool _workflowEnabled;

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        public WorkflowHelper(Company company, WorkflowSystemType systemType)
        {
            _company = company;
            _systemType = systemType;

            string contactName = ConfigurationManager.AppSettings["VA3i.ContactName"];
            string contactEmail = ConfigurationManager.AppSettings["VA3i.ContactEmail"];
            string contactPhone = ConfigurationManager.AppSettings["VA3i.ContactPhone"];

            _contact = new Contact(contactName, contactEmail, contactPhone);
            _application = new SoftwareApplication("Account Reporting Management System", "ARMS", _contact, _contact);
            _workflowEnabled = (ConfigurationManager.AppSettings["WorkflowEnabled"] != "0");
        }

        /// <summary>
        /// Creates a suspended survey instance in the ALBPM workflow system.
        /// The passed survey is updated to include the reference number returned from workflow system.
        /// </summary>
        /// <param name="survey">A writable survey whos instance should be closed.</param>
        /// <param name="docType">The type of document.</param>
        /// <param name="instanceType">The type of instance.</param>
        /// <param name="status">The status of the instance.</param>
        /// <param name="clientNote">A client note.</param>
        /// <param name="instanceNote">An instance note.</param>
        /// <param name="docRemarks">The doc remarks.</param>
        public void CreateSuspendedInstance(Survey survey, string docType, string instanceType, string status, string clientNote, string instanceNote, string docRemarks)
        {
            CreateSuspendedInstance(survey, docType, instanceType, null, status, clientNote, instanceNote, docRemarks);
        }


        /// <summary>
        /// Creates a suspended survey instance in the ALBPM workflow system.
        /// The passed survey is updated to include the reference number returned from workflow system.
        /// </summary>
        /// <param name="survey">A writable survey whos instance should be closed.</param>
        /// <param name="docType">The type of document.</param>
        /// <param name="instanceType">The type of instance.</param>
        /// <param name="processType">The process type of the instance.</param>
        /// <param name="status">The status of the instance.</param>
        /// <param name="clientNote">A client note.</param>
        /// <param name="instanceNote">An instance note.</param>
        /// <param name="docRemarks">The doc remarks.</param>
        public void CreateSuspendedInstance(Survey survey, string docType, string instanceType, string processType, string status, string clientNote, string instanceNote, string docRemarks)
        {
            if (survey.IsReadOnly) throw new ArgumentException("Survey cannot be read-only.", "survey");

            // don't do anything if workflow has been disabled or if this is a migrated survey
            if (!_workflowEnabled || survey.CreateState == CreateState.Migrated)
            {
                return;
            }

            try
            {
                byte[] image = BuildLinkImage(survey);

                // run the process
                Workflow.SuspendedSurveyInstance process =
                    new SuspendedSurveyInstance(_contact, _application, _company, _systemType,
                    survey.PrimaryPolicy, image, docType, instanceType, processType, status, "HTML", clientNote, instanceNote, 
                    CoreSettings.ConfigurationType, survey.WorkflowSubmissionNumber, docRemarks);

                process.Run();

                // check and return the result (which is the submission number)
                string refNumber = process.Result;
                if (refNumber == null || refNumber.Length == 0)
                {
                    throw new Exception("A workflow reference number was not returned.");
                }

                // set the workflow reference
                survey.WorkflowReference = refNumber;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to create suspended instance for survey '" + survey.ID + "'. See inner exception for details.", ex);
            }
        }

        /// <summary>
        /// Closes a suspended survey instance in the ALBPM workflow system.
        /// The passed survey is updated to include the reference number returned from workflow system.
        /// </summary>
        /// <param name="survey">A writable survey whos instance should be closed.</param>
        /// <param name="instanceType">The type of instance.</param>
        /// <param name="status">The status of the instance.</param>
        /// <param name="clientNote">A client note.</param>
        /// <param name="instanceNote">A instance note.</param>
        public void CloseSuspendedInstance(Survey survey, string instanceType, string status, string clientNote, string instanceNote)
        {
            if (survey.IsReadOnly) throw new ArgumentException("Survey cannot be read-only.", "survey");

            // don't do anything if workflow has been disabled or if this is a migrated survey
            if (!_workflowEnabled || survey.CreateState == CreateState.Migrated)
            {
                return;
            }

            // we can't do anything if no submission number exists
            string workflowReference = survey.WorkflowReference;
            if (workflowReference == null || workflowReference.Length == 0)
            {
                return;
            }

            // check if the instance is ready
            Workflow.Transaction instance = new Transaction(_contact, _application, _company, _systemType, workflowReference, CoreSettings.ConfigurationType);
            if (instance == null || instance.ID == null || instance.ID.Length <= 0)
            {
                throw new QCI.Web.Validation.FieldValidationException("The suspended instance in ALBPM is not ready to be closed.  Please try again at a later time.");
            }

            try
            {
                // run the process
                Workflow.CloseSuspendedSurveyInstance process =
                    new CloseSuspendedSurveyInstance(_contact, _application, _company, _systemType,
                    survey.PrimaryPolicy, instanceType, status, clientNote, instanceNote, CoreSettings.ConfigurationType, survey.WorkflowSubmissionNumber, workflowReference);

                process.Run();

                // check and return the result (which is the submission number)
                string refNumber = process.Result;
                if (refNumber == null || refNumber.Length == 0)
                {
                    throw new Exception("A workflow reference number was not returned.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to close instance for survey '" + survey.ID + "'. See inner exception for details.", ex);
            }
        }
        /// <summary>
        /// Constructs an image of an HTML page that serves to visually link
        /// the workflow system with the page in the ARMS web that shows
        /// the respective survey.
        /// </summary>
        /// <param name="survey">The survey to link.</param>
        /// <returns>A UTF-8 encoded byte array that contains the HTML.</returns>
        private byte[] BuildLinkImage(Survey survey)
        {
            StringBuilder html = new StringBuilder();

            string url = Berkley.BLC.Core.CoreSettings.SurveyPage;
            if (url != null && url.Trim().Length > 0)
                url = string.Format(url, survey.ID.ToString());
            else
                throw new Exception("A software configuration setting (SurveyPage) was not found while executing Berkley.BLC.Business.WorkflowHelper.");

            html.Append("<html>\r\n");
            html.Append("<head>\r\n");
            html.AppendFormat("\t<title>ARMS Survey Status for #{0}</title>\r\n", survey.Number.ToString());
            html.Append("</head>\r\n");
            html.Append("<frameset>\r\n");
            html.AppendFormat("\t<frame frameborder=\"0\" src=\"{0}\" />", url);
            html.Append("</frameset>\r\n");
            html.Append("</html>\r\n");

            UTF8Encoding encoding = new UTF8Encoding();
            return encoding.GetBytes(html.ToString());
        }

        //private Policy DeterminePolicy(Survey survey)
        //{
        //    Policy result = null;

        //    // only return a policy if there is no submission number
        //    if (survey.PrimaryPolicy != null && survey.WorkflowSubmissionNumber.Length <= 0)
        //    {
        //        ProspectWebService prospectService = new ProspectWebService();
        //        bool exist = prospectService.DoesSubmissionExist(survey.PrimaryPolicy.Number, _company);

        //        //Had to use a solution outside of Wilson for this query (ORMapper was locking)
        //        DataTable dt = QCI.DataAccess.SqlHelper.ExecuteDataTable(DB.ConnectionString, CommandType.Text, string.Format("SELECT DISTINCT PolicyID FROM Survey_PolicyCoverage WHERE Active = 1 AND SurveyID = '283dec9c-d861-4622-bdd8-0063cacc067f' AND PolicyID != '6de36ec0-a262-4f83-b5e9-5ff72b03e32f'"));

        //        // check if any other policies attached to the survey exist in Clearance
        //        if (!exist)
        //        {
        //            Policy[] policies = null;
        //            foreach (Policy policy in policies)
        //            {
        //                exist = prospectService.DoesSubmissionExist(policy.Number, _company);
        //                if (exist)
        //                {
        //                    result = policy;
        //                    break;
        //                }
        //            }

        //            //if we got this far without finding a valid policy then inform the user
        //            throw new Exception("Valid policy was not found.");
        //        }
        //        else //primary policy does exist
        //        {
        //            result = survey.PrimaryPolicy;
        //        }
        //    }

        //    return result;
        //}
    }
}
