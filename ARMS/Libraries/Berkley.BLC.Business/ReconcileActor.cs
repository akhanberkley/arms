using System;
using System.Collections.Generic;
using System.Text;
using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
	public class ReconcileActor<T>
	{
		private T moEntity;
		private bool mfAlreadyExists = false;
		private Transaction moTransaction = null;
		private T moNewEntity;
		private IReconcile<T> moIReconcile = null;

		/// <summary>
		/// Use this constructor along with the Reconcile() call.
		/// Use when entities need to be deleted from the DB during Reconciliation.
		/// </summary>
		/// <param name="oTransaction"></param>
		public ReconcileActor(Transaction oTransaction)
		{
			moTransaction = oTransaction;
		}

		/// <summary>
		/// Performs an Update or Insert based on the Entity's presence in the DB
		/// This constructor will not take into account deleting a DB entity
		/// </summary>
		/// <param name="strForeignKey"></param>
		/// <param name="oNewEntity"></param>
		/// <param name="oIReconcile"></param>
		/// <param name="oTransaction"></param>
		public ReconcileActor(string[] rstrForeignKey, T oNewEntity, IReconcile<T> oIReconcile, Transaction oTransaction)
		{
			moTransaction = oTransaction;
			moNewEntity = oNewEntity;
			moIReconcile = oIReconcile;
			moEntity = oIReconcile.GetExisting(rstrForeignKey);
		}

        /// <summary>
        /// Performs an Update or Insert based on the Entity's presence in the DB
        /// This constructor will not take into account deleting a DB entity
        /// </summary>
        /// <param name="strForeignKey"></param>
        /// <param name="oNewEntity"></param>
        /// <param name="oIReconcile"></param>
        /// <param name="oTransaction"></param>
        public ReconcileActor(T oNewEntity, IReconcile<T> oIReconcile, Transaction oTransaction)
        {
            moTransaction = oTransaction;
            moNewEntity = oNewEntity;
            moIReconcile = oIReconcile;
        }

		public void Reconcile()
		{
			if (moEntity != null)
			{
				mfAlreadyExists = true;
				moEntity = moIReconcile.Update(moNewEntity, moEntity);
			}
			else
			{
				moEntity = moIReconcile.Insert(moNewEntity);
			}
		}

		public Transaction DbTransaction
		{
			get { return moTransaction; }
		}

		public void Reconcile(T[] roExisting, T[] roNew, IReconcile<T> oIReconcile)
		{
			bool fFound = false;
			foreach (T oExistingLocation in roExisting)
			{
				fFound = false;
				foreach (T oNewLocation in roNew)
				{
					if (oIReconcile.Equal(oExistingLocation, oNewLocation))
					{
						fFound = true;
						oIReconcile.Update(oNewLocation, oExistingLocation);
						break;
					}
				}
				if (!fFound)
				{
					oIReconcile.Delete(oExistingLocation);
				}
			}

			foreach (T oNewEntity in roNew)
			{
				fFound = false;
				foreach (T oExistingLocation in roExisting)
				{
					if (oIReconcile.Equal(oExistingLocation, oNewEntity))
					{
						fFound = true;
						break;
					}
				}
				if (!fFound)
				{
					oIReconcile.Insert(oNewEntity);
				}
			}
		}

		public State GetExisting(string strForeignKey)
		{
			State oState = State.GetOne("StateCode = ?", strForeignKey);
			return oState;
		}

		public T Entity
		{
			get { return moEntity; }
			set { moEntity = value; }
		}

		public bool AlreadyExists
		{
			get { return mfAlreadyExists; }
		}


	}
}
