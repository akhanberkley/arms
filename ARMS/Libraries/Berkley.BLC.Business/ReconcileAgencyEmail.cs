using System;
using System.Collections.Generic;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
	public class ReconcileAgencyEmail : IReconcile<AgencyEmail>
	{
		private ReconcileActor<AgencyEmail> moActor;
		private Company moCompany;
		private List<AgencyEmail> moAgencyEmailList;

		public ReconcileAgencyEmail(Company oCompany, List<AgencyEmail> oEmailList, Transaction oTransaction)
		{
			moCompany = oCompany;
			moAgencyEmailList = oEmailList;
			moActor = new ReconcileActor<AgencyEmail>(oTransaction);
		}

		#region IReconcile<AgencyEmail> Members

		public AgencyEmail GetExisting(string[] rstrForeignKey)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public AgencyEmail Entity
		{
			get { throw new Exception("The method or operation is not implemented."); }
		}

		public AgencyEmail Update(AgencyEmail oNewEntity, AgencyEmail oExistingEntity)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public AgencyEmail Insert(AgencyEmail oNewEntity)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public void Delete(AgencyEmail oEntity)
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public bool Equal(AgencyEmail oLhs, AgencyEmail oRhs)
		{
			return oLhs.EmailAddress.ToLower() == oRhs.EmailAddress.ToLower();
		}

		public ReconcileActor<AgencyEmail> Actor
		{
			get { return moActor; }
		}

		public void Reconcile()
		{
			foreach (AgencyEmail oAgencyEmail in moAgencyEmailList)
			{
				if (oAgencyEmail.EmailAddress.Length > 0)
				{
					AgencyEmail oEntity = AgencyEmail.GetOne("AgencyID = ? AND EmailAddress = ?", oAgencyEmail.ID, oAgencyEmail.EmailAddress);
					if (oEntity == null)
					{
						AgencyEmail oNewAgencyEmail;
						if (oAgencyEmail.IsReadOnly)
						{
							oNewAgencyEmail = oAgencyEmail.GetWritableInstance();
						}
						else
						{
							oNewAgencyEmail = oAgencyEmail;
						}
						oNewAgencyEmail.Save(moActor.DbTransaction);
					}
				}
			}
		}

		public void DeleteQueued()
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public void AddQueued()
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public void UpdateQueued()
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public void Clear()
		{
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion
	}
}
