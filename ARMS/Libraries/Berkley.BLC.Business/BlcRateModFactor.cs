using System;
using System.Collections.Generic;
using System.Text;
using Berkley.BLC.Entities;

namespace Berkley.BLC.Business
{
	public class BlcRateModFactor
	{
        private RateModificationFactor moRateModFactor;
		private string mstrPolicyNumber;
		private int miPolicyMod;

        public BlcRateModFactor()
		{
		}

		public RateModificationFactor Entity
		{
            get { return moRateModFactor; }
            set { moRateModFactor = value; }
		}

		public string PolicyNumber
		{
			get { return mstrPolicyNumber; }
			set { mstrPolicyNumber = value; }
		}

		public int PolicyMod
		{
			get { return miPolicyMod; }
			set { miPolicyMod = value; }
		}

	}
}
