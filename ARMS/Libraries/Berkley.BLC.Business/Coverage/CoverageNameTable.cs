using System;
using System.Collections.Generic;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
	public class CoverageNameTable
	{
		private Dictionary<string, Guid> moCoverage = new Dictionary<string,Guid>();

		public CoverageNameTable()
		{
		}

		public void Clear()
		{
			moCoverage.Clear();
		}

		public Guid GetId(BlcCoverage oCoverage)
		{
			string strKey = new CoverageNameKey(oCoverage).Key;
			if (moCoverage.ContainsKey(strKey))
			{
				return moCoverage[strKey];
			}
			else
			{
				throw new Exception("CoverageName not found" + oCoverage.ToString());
			}
		}

		public void Load(Company oCompany)
		{
			CoverageName[] roCoverageNames = CoverageName.GetArray("CompanyID = ?", oCompany.ID);

			foreach(CoverageName oCoverageName in roCoverageNames)
			{
				if (oCoverageName.SubLineOfBusinessCode.Length > 0)
				{
					moCoverage.Add(new CoverageNameKey(oCoverageName.LineOfBusinessCode, oCoverageName.SubLineOfBusinessCode, oCoverageName.TypeCode).Key, oCoverageName.ID);
				}
				else
				{
					moCoverage.Add(new CoverageNameKey(oCoverageName.LineOfBusinessCode, oCoverageName.TypeCode).Key, oCoverageName.ID);
				}

			}
		}
	}
}
