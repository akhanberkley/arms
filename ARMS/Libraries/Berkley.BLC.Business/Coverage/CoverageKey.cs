using System;
using System.Collections.Generic;
using System.Text;
using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
	public class CoverageKey
	{
		private string mstrKey;

		public CoverageKey(BlcCoverage oCoverage)
		{
			CoverageNameKey oCoverageNameKey = new CoverageNameKey(oCoverage);
			StringBuilder oBuilder = new StringBuilder();
			oBuilder.Append(oCoverageNameKey.Key);
			oBuilder.Append("!");
			oBuilder.Append(oCoverage.CoverageValueType.Trim());
			mstrKey = oBuilder.ToString();
		}

		public CoverageKey(VWCoverage oCoverage)
		{
			CoverageNameKey oCoverageNameKey;

			if (oCoverage.SubLineOfBusinessCode.Length > 0)
			{
				oCoverageNameKey = new CoverageNameKey(oCoverage.LineOfBusinessCode, oCoverage.SubLineOfBusinessCode, oCoverage.CoverageNameTypeCode);
			}
			else
			{
				oCoverageNameKey = new CoverageNameKey(oCoverage.LineOfBusinessCode, oCoverage.CoverageNameTypeCode);
			}
			StringBuilder oBuilder = new StringBuilder();
			oBuilder.Append(oCoverageNameKey.Key);
			oBuilder.Append("!");
			oBuilder.Append(oCoverage.CoverageTypeCode.Trim());
			mstrKey = oBuilder.ToString();
		}

		public string Key
		{
			get { return mstrKey; }
		}

	}
}
