using System;
using System.Collections.Generic;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
	public class BlcPolicy
	{

		public static bool Equals(Policy oLhs, Policy oRhs)
		{
			if (oRhs.Mod != 0 && oLhs.Mod != 0)
			{
				return oLhs.Number == oRhs.Number && oLhs.Mod == oRhs.Mod;
			}
			else
			{
				string strRhsPolicy = oRhs.Number.Substring(0, 7);
				string strLhsPolicy = oLhs.Number.Substring(0, 7);
				if (strRhsPolicy == strLhsPolicy)
				{
					if (oLhs.ExpireDate.Year == oRhs.ExpireDate.Year)
					{
						if (oLhs.ExpireDate.Month == oRhs.ExpireDate.Month)
						{
							return true;
						}
						else if (oLhs.ExpireDate.Month == oRhs.ExpireDate.Month + 1)
						{
							return true;
						}
						else if (oLhs.ExpireDate.Month == oRhs.ExpireDate.Month - 1)
						{
							return true;
						}
					}
				}
				return false;
			}
		}
	}
}
