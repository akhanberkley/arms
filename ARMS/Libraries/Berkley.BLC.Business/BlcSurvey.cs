using System;
using System.Collections.Generic;
using System.Text;

using Berkley.BLC.Entities;

namespace Berkley.BLC.Business
{
	public class BlcSurvey
	{
		private BlcInsured moInsured;
		private Survey moSurvey = null;
		private BlcServicePlan moServicePlan = new BlcServicePlan();
		private List<SurveyNote> moSurveyNoteList = new List<SurveyNote>();
		private List<SurveyHistory> moSurveyHistoryList = new List<SurveyHistory>();
		private List<SurveyRecommendation> moSurveyRecommendationList = new List<SurveyRecommendation>();
		private Dictionary<string, SurveyType> moTypeDictionary = null;
		private List<Location> moLocations = new List<Location>(); // Used by Service Plan migration
		private List<LocationContact> moLocationContacts = new List<LocationContact>(); // Used by Service Plan migration
		private List<Objective> moObjectives = new List<Objective>(); // Used by Service Plan migration
		
		public BlcSurvey()
		{
		}

		public BlcInsured Insured
		{
			get { return moInsured; }
			set { moInsured = value; }
		}

		public Survey SurveyEntity
		{
			get { return moSurvey; }
			set { moSurvey = value; }
		}

		public BlcServicePlan ServicePlan
		{
			get { return moServicePlan; }
			set { moServicePlan = value; }
		}

		public List<SurveyNote> SurveyNoteList
		{
			get { return moSurveyNoteList; }
		}

		public List<SurveyRecommendation> SurveyRecommendationList
		{
			get { return moSurveyRecommendationList; }
			set { moSurveyRecommendationList = value; }
		}

		public List<SurveyHistory> SurveyHistoryList
		{
			get { return moSurveyHistoryList; }
		}

		public List<Location> Locations
		{
			get { return moLocations; }
		}
		public List<LocationContact> LocationContacts
		{
			get { return moLocationContacts; }
		}
		public List<Objective> ObjectiveList
		{
			get { return moObjectives; }
		}

		private void BuildTypes()
		{
			SurveyType[] roSurveyTypes = SurveyType.GetArray("*");

			moTypeDictionary = new Dictionary<string, SurveyType>();
			foreach (SurveyType oType in roSurveyTypes)
			{
				moTypeDictionary[oType.TypeCode] = oType;
			}
		}

		public Guid GetTypeGuid(string strCode)
		{
			if (moTypeDictionary == null)
			{
				BuildTypes();
			}
			if (moTypeDictionary.ContainsKey(strCode))
			{
				return moTypeDictionary[strCode].ID;
			}
			else
			{
				return moTypeDictionary["OT"].ID; // Other
			}
		}


	}
}
