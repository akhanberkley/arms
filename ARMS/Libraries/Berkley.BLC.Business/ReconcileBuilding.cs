	using System;
using System.Collections.Generic;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
	public class ReconcileBuilding : IReconcile<Building>
	{
		private ReconcileActor<Building> moActor;
		private List<Building> moBuildingDeletes = new List<Building>();
		private List<Building> moBuildingInserts = new List<Building>();
		private List<Building> moBuildingUpdates = new List<Building>();
		private Location moLocation = null;

		public ReconcileBuilding(Transaction oTransaction)
		{ 
			moActor = new ReconcileActor<Building>(oTransaction);
		}

		public Location BuildingLocation
		{
			get { return moLocation; }
			set { moLocation = value; }
		}
		/// <summary>
		/// If the building counts are different between the existing and incoming buildings,
		/// remove all buildings for this location and add them.
		/// </summary>
		/// <param name="roExistingBuildings"></param>
		/// <param name="roNewBuildings"></param>
		public void Reconcile(Building[] roExistingBuildings, Building[] roNewBuildings)
		{
			if (roExistingBuildings.Length != roNewBuildings.Length && roExistingBuildings.Length > 0)
			{
				// Remove all as there could be multiple buildings with the same building number
				DB.Engine.ExecuteDelete(typeof(Building), "LocationID = '" + moLocation.ID + "'");
				roExistingBuildings = new Building[0];
			}
			Actor.Reconcile(roExistingBuildings, roNewBuildings, this);
		}

		#region IReconcile<Building> Members
		public void Clear()
		{
			moBuildingDeletes.Clear();
			moBuildingInserts.Clear();
			moBuildingUpdates.Clear();
		}

		public void Reconcile()
		{
			moActor.Reconcile();
		}

		public Building GetExisting(string[] rstrForeignKey)
		{
			Building oEntity = Building.GetOne("BuildingNumber = ? AND LocationID = ?", rstrForeignKey[0], moLocation.ID);
			return oEntity;
		}

		public Building[] GetExisting(Guid oLocationId)
		{
			Building[] roEntity = Building.GetArray("LocationID = ?", oLocationId);
			return roEntity;
		}

		public Building Entity
		{
			get { throw new Exception("The method or operation is not implemented."); }
		}

		public Building Update(Building oNewEntity, Building oExistingEntity)
		{
			if (oExistingEntity.IsReadOnly)
			{
				oExistingEntity = oExistingEntity.GetWritableInstance();
			}
			oExistingEntity = oExistingEntity.Assign(oNewEntity);
			oExistingEntity.LocationID = BuildingLocation.ID;
			moBuildingUpdates.Add(oExistingEntity);
			return oExistingEntity;
		}

		public Building Insert(Building oNewEntity)
		{
			if (oNewEntity.IsReadOnly)
			{
				oNewEntity = oNewEntity.GetWritableInstance();
			}
			oNewEntity.LocationID = BuildingLocation.ID;
			moBuildingInserts.Add(oNewEntity);
			return oNewEntity;
		}

		public void Delete(Building oEntity)
		{
			moBuildingDeletes.Add(oEntity);
		}

		public bool Equal(Building oLhs, Building oRhs)
		{
			// Use the Foreign Key for identification between systems
			return oLhs.Number == oRhs.Number;
		}

		public ReconcileActor<Building> Actor
		{
			get { return moActor; }
		}
		
		public void DeleteQueued()
		{
			#region Deletes are done in Reconcile
			//foreach(Building oEntity in moBuildingDeletes)
			//{
			//	oEntity.Delete(moActor.DbTransaction);
			//}
			#endregion
		}

		public void AddQueued()
		{
			Building oNewBuilding;
			foreach (Building oBuilding in moBuildingInserts)
			{
				if (oBuilding.IsReadOnly)
				{
					oNewBuilding = oBuilding.GetWritableInstance();
				}
				else
				{
					oNewBuilding = oBuilding;
				}
				oNewBuilding.Save(moActor.DbTransaction);
			}
		}

		public void UpdateQueued()
		{
			foreach (Building oBuilding in moBuildingUpdates)
			{
				oBuilding.Save(moActor.DbTransaction);
			}
		}
		#endregion
	}
}
