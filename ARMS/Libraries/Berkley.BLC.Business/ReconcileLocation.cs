using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
    public class ReconcileLocation : IReconcile<Location>
    {
        ReconcileBuilding moReconcileBuilding = null;
        private ReconcileActor<Location> moActor;
        private BlcInsured moBlcInsured;
        private List<Location> moNewLocationList = new List<Location>();
        private List<Location> moUpdateLocationList = new List<Location>();
        // We need to keep track of the deletes and do them after the updates
        private List<Location> moLocationDeleteList = new List<Location>();
        private List<LocationContact> moLocationContactInsertList = new List<LocationContact>();

        public ReconcileLocation(BlcInsured oBlcInsured, Transaction oTransaction)
        {
            moActor = new ReconcileActor<Location>(oTransaction);
            moReconcileBuilding = new ReconcileBuilding(oTransaction);
            moBlcInsured = oBlcInsured;
        }

        public Location[] NewLocations
        {
            get { return moNewLocationList.ToArray(); }
        }

        #region IReconcile<Location> Members
        public void Clear()
        {
            moNewLocationList.Clear();
            moUpdateLocationList.Clear();
            moLocationDeleteList.Clear();
            moLocationContactInsertList.Clear();
            moReconcileBuilding.Clear();
        }

        public void Reconcile()
        {
            moActor.Reconcile();
        }

        public Location GetExisting(string[] rstrForeignKey)
        {
            Location oEntity = Location.GetOne("PolicySystemKey = ?", rstrForeignKey[0]);
            return oEntity;
        }

        public Location Entity
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        public Location Update(Location oNewEntity, Location oExistingEntity)
        {
            Building[] roExistingBuilding = Building.GetArray("LocationID = ?", oExistingEntity.ID);
            Building[] roNewBuildings = moBlcInsured.GetBuildingListForLocation(oNewEntity).ToArray();

            // Assign the new data to to the existing DB Entity
            if (oExistingEntity.IsReadOnly)
            {
                oExistingEntity = oExistingEntity.GetWritableInstance();
            }
            oExistingEntity.Assign(oNewEntity);
            moUpdateLocationList.Add(oExistingEntity);

            // Do the Inserts, Updates, and Deletes for the Buildings
            moReconcileBuilding.BuildingLocation = oExistingEntity;
            moReconcileBuilding.Reconcile(roExistingBuilding, roNewBuildings);

            return oNewEntity;
        }

        public Location Insert(Location oNewLocation)
        {
            Building oNewBuilding;
            Building[] roNewBuildings = moBlcInsured.GetBuildingListForLocation(oNewLocation).ToArray();

            //see if we can pull contact(s) from a previous survey request for this client/location combo
            string command = string.Format("EXEC CreateSurvey_GetPastContact @ClientID = '{0}', @CompanyID = '{1}', @LocationNumber = '{2}'", moBlcInsured.InsuredEntity.ClientID, moBlcInsured.InsuredEntity.CompanyID, oNewLocation.Number);
            DataTable dt = DB.Engine.GetDataSet(command).Tables[0];

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    LocationContact pastContact = LocationContact.Get(new Guid((dr[0]).ToString()));
                    LocationContact newContact = new LocationContact(Guid.NewGuid());
                    newContact.LocationID = oNewLocation.ID;
                    newContact.Name = pastContact.Name;
                    newContact.Title = pastContact.Title;
                    newContact.PhoneNumber = pastContact.PhoneNumber;
                    newContact.AlternatePhoneNumber = pastContact.AlternatePhoneNumber;
                    newContact.FaxNumber = pastContact.FaxNumber;
                    newContact.EmailAddress = pastContact.EmailAddress;
                    newContact.ContactReasonTypeCode = pastContact.ContactReasonTypeCode;
                    newContact.CompanyName = pastContact.CompanyName;
                    newContact.StreetLine1 = pastContact.StreetLine1;
                    newContact.StreetLine2 = pastContact.StreetLine2;
                    newContact.City = pastContact.City;
                    newContact.StateCode = pastContact.StateCode;
                    newContact.ZipCode = pastContact.ZipCode;
                    newContact.PriorityIndex = pastContact.PriorityIndex;
                    newContact.Modified = false;

                    moLocationContactInsertList.Add(newContact);
                }
            }
            else
            {
                // Only do this in the Insert as there is no way to maintain the LocationContact rows
                if ((moBlcInsured.Owner != null && moBlcInsured.Owner.Length > 0) || 
                    (moBlcInsured.OwnerPhone != null && moBlcInsured.OwnerPhone.Length > 0))
                {
                    LocationContact oLocationContact = new LocationContact(Guid.NewGuid());

                    // Insert the "Owner" name in the LocationContact table
                    oLocationContact.LocationID = oNewLocation.ID;
                    oLocationContact.Name = moBlcInsured.Owner;
                    oLocationContact.PhoneNumber = moBlcInsured.OwnerPhone;
                    oLocationContact.AlternatePhoneNumber = moBlcInsured.OwnerAltPhone;
                    oLocationContact.FaxNumber = moBlcInsured.OwnerFax;
                    oLocationContact.EmailAddress = moBlcInsured.OwnerEmail;
                    oLocationContact.Title = moBlcInsured.OwnerTitle;
                    oLocationContact.PriorityIndex = 1;
                    moLocationContactInsertList.Add(oLocationContact);
                }

                foreach (LocationContact contact in moBlcInsured.LocationContactList)
                {
                    if (contact.PolicySystemKey == oNewLocation.PolicySystemKey)
                    {
                        contact.LocationID = oNewLocation.ID;
                        moLocationContactInsertList.Add(contact);
                    }
                }
            }

            if (oNewLocation.IsReadOnly)
            {
                oNewLocation = oNewLocation.GetWritableInstance();
            }
            oNewLocation.InsuredID = moBlcInsured.InsuredEntity.ID;
            moNewLocationList.Add(oNewLocation);

            foreach (Building oBuilding in roNewBuildings)
            {
                if (oBuilding.IsReadOnly)
                {
                    oNewBuilding = oBuilding.GetWritableInstance();
                }
                else
                {
                    oNewBuilding = oBuilding;
                }
                moReconcileBuilding.BuildingLocation = oNewLocation;
                moReconcileBuilding.Insert(oNewBuilding);
            }

            return oNewLocation;
        }

        public void Delete(Location oEntity)
        {
            // This code just sets the Insured ID to null as the Survey tables have foreign keys
            // back into the Insured. A nighly batch will removed the Locations that are not tied to a Survey.
            if (oEntity.IsReadOnly)
            {
                oEntity = oEntity.GetWritableInstance();
            }
            oEntity.IsActive = false;
            moUpdateLocationList.Add(oEntity);
        }

        public bool Equal(Location oLhs, Location oRhs)
        {
            return oLhs.PolicySystemKey == oRhs.PolicySystemKey;
        }

        public ReconcileActor<Location> Actor
        {
            get { return moActor; }
        }

        public void DeleteQueued()
        {
            Location oWriteLocation;

            // The entity isn't actually deleted, it's marked as inactive
            //moReconcileBuilding.DeleteQueued();

            foreach (Location oLocation in moLocationDeleteList)
            {
                oWriteLocation = oLocation;
                if (oWriteLocation.IsReadOnly)
                {
                    oWriteLocation = oLocation.GetWritableInstance();
                }
                oWriteLocation.IsActive = false;
                oLocation.Save(moActor.DbTransaction);
            }
        }

        public void AddQueued()
        {
            LocationContact oWriteLocationContact;
            Location oWriteLocation;

            foreach (LocationContact oLocationContact in moLocationContactInsertList)
            {
                oWriteLocationContact = oLocationContact;
                if (oWriteLocationContact.IsReadOnly)
                {
                    oWriteLocationContact = oLocationContact.GetWritableInstance();
                }
                oWriteLocationContact.Save(moActor.DbTransaction);
            }

            foreach (Location oLocation in moNewLocationList)
            {
                oWriteLocation = oLocation;
                if (oWriteLocation.IsReadOnly)
                {
                    oWriteLocation = oLocation.GetWritableInstance();
                }
                oWriteLocation.Save(moActor.DbTransaction);
            }
            moReconcileBuilding.AddQueued();
        }

        public void UpdateQueued()
        {
            foreach (Location oLocation in moUpdateLocationList)
            {
                // An exception is thrown if the Location is saved with the InsuredID set to Guid.Empty
                if (oLocation.InsuredID == Guid.Empty)
                {
                    moActor.DbTransaction.ExecuteScalar(String.Format("Update Location set InsuredID=null, PolicySystemKey=null where LocationID='{0}'", oLocation.ID));
                }
                else
                {
                    oLocation.Save(moActor.DbTransaction);
                }
            }
            moReconcileBuilding.UpdateQueued();
        }
        #endregion

    }
}
