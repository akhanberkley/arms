using System;
using System.Collections.Generic;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
	public class BlcCoverage
	{
		private string mstrLineOfBusiness = "";
		private string mstrSubLineOfBusiness = "";
		private string mstrCompanyAbbreviation = "";
		private string mstrCoverageValueType = "";
		private string mstrCoverageNameType = "";
		private string mstrPolicyNumber = "";
		private string mstrPolicyMod = "";
		private string mstrValue = "";
		private bool mfReport = false;
        private List<CoverageDetail> mlsCoverageDetail = null;

		public BlcCoverage()
		{
		}

		public Coverage CreateCoverage(Guid oCoverageId, Guid oInsuredId, Guid oCoverageNameId, Guid oPolicyId)
		{
			Coverage oNewCoverage = new Coverage(oCoverageId);
			oNewCoverage.InsuredID = oInsuredId;
			oNewCoverage.NameID = oCoverageNameId;
			oNewCoverage.TypeCode = mstrCoverageValueType;
			oNewCoverage.PolicyID = oPolicyId;
			oNewCoverage.Value = mstrValue;

			return oNewCoverage;
		}

        public List<CoverageDetail> CreateCoverageDetail(Guid oCoverageId, Guid oInsuredId, Guid oPolicyId, List<CoverageDetail> oDetails)
        {
            List<CoverageDetail> cdList = new List<CoverageDetail>();

            foreach (CoverageDetail cd in oDetails)
            {
                CoverageDetail oNewCoverageDetail = new CoverageDetail(new Guid());
                oNewCoverageDetail.CoverageID = oCoverageId;
                oNewCoverageDetail.InsuredID = oInsuredId;
                oNewCoverageDetail.PolicyID = oPolicyId;
                oNewCoverageDetail.DateCreated = DateTime.Now;
                oNewCoverageDetail.Value = cd.Value;

                cdList.Add(oNewCoverageDetail);
            }

            return cdList;
        }

		public string Value
		{
			get { return mstrValue; }
			set { mstrValue = value; }
		}

		// Used by the Migration code
		public bool Report
		{
			get { return mfReport; }
			set { mfReport = value; }
		}

		public string LineOfBusinessAsString
		{
			get { return mstrLineOfBusiness; }
			set { mstrLineOfBusiness = value; }
		}

		public string SubLineOfBusinessAsString
		{
			get { return mstrSubLineOfBusiness; }
			set { mstrSubLineOfBusiness = value; }
		}

		public string CompanyAbbreviation
		{
			get { return mstrCompanyAbbreviation; }
			set { mstrCompanyAbbreviation = value; }
		}

		public string CoverageValueType
		{
			get { return mstrCoverageValueType; }
			set { mstrCoverageValueType = value; }
		}

		public string CoverageNameType
		{
			get { return mstrCoverageNameType; }
			set { mstrCoverageNameType = value; }
		}

		public string PolicyNumber
		{
			get { return mstrPolicyNumber; }
			set { mstrPolicyNumber = value; }
		}

		public string PolicyMod
		{
			get { return mstrPolicyMod; }
			set { mstrPolicyMod = value; }
		}

		public override string ToString()
		{
			StringBuilder oBuilder = new StringBuilder();

			oBuilder.Append("Line of business=");
			oBuilder.Append(mstrLineOfBusiness);
			oBuilder.Append(" SubLine of business=");
			oBuilder.Append(mstrSubLineOfBusiness);

			oBuilder.Append(" Company=");
			oBuilder.Append(mstrCompanyAbbreviation);
			oBuilder.Append(" Coverage value type=");
			oBuilder.Append(mstrCoverageValueType);
			oBuilder.Append(" Coverage name type=");
			oBuilder.Append(mstrCoverageNameType);

			return oBuilder.ToString();
		}

        public List<CoverageDetail> CoverageDetails
        {
            get { return mlsCoverageDetail; }
            set { mlsCoverageDetail = value; }
        }

	}
}
