using System;
using System.Collections.Generic;
using System.Text;
using Berkley.BLC.Entities;

namespace Berkley.BLC.Business
{
	public class BlcClaim
	{
		private Claim moClaim;
		private string mstrPolicyNumber;
		private int miPolicyMod;
		
		public BlcClaim()
		{
		}

		public Claim Entity
		{
			get { return moClaim; }
			set { moClaim = value; }
		}

		public string PolicyNumber
		{
			get { return mstrPolicyNumber; }
			set { mstrPolicyNumber = value; }
		}

		public int PolicyMod
		{
			get { return miPolicyMod; }
			set { miPolicyMod = value; }
		}

	}
}
