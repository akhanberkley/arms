using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Business
{
	public interface IReconcile<T>
	{
		T GetExisting(string[] rstrForeignKey);
		T Entity { get; }
		T Update(T oNewEntity, T oExistingEntity);
		T Insert(T oNewEntity);
		void Delete(T oEntity);
		bool Equal(T oLhs, T oRhs);
		ReconcileActor<T> Actor { get; }
		void Reconcile();
		void DeleteQueued();
		void AddQueued();
		void UpdateQueued();
		void Clear();
	}
}
