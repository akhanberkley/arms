using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
	public class QueryBuilder
	{
		public QueryBuilder()
		{
		}

		/// <summary>
		/// Using reflection to convert the multi-dimensional Guid[] to an object[] 
		/// in order to call the Entity's GetArray() function.
		/// </summary>
		/// <param name="oEntityType"></param>
		/// <param name="strQuery"></param>
		/// <param name="roParameters"></param>
		/// <returns></returns>
		public static object GetArray(Type oEntityType, string strQuery, Guid[] roParameters)
		{
			List<object> moArgumentList = new List<object>();
			List<object> moGuidList = new List<object>();
			MethodInfo oMethodInfo;

			// Create the Query
			OPathQuery oQuery = new OPathQuery(oEntityType, strQuery);
			moArgumentList.Add(oQuery);

			// Here is where we flatten out the Guid[] into the argument array
			foreach (Guid oGuid in roParameters)
			{
				moGuidList.Add(oGuid);
			}
			moArgumentList.Add(moGuidList.ToArray());

			// Call the Entity function
			oMethodInfo = oEntityType.GetMethod("GetArray", new Type[] { typeof(OPathQuery), typeof(object[]) });
			return oMethodInfo.Invoke(null, moArgumentList.ToArray());
		}

		public static string BuildOrQuery(int iLength, string strEntityName)
		{
			StringBuilder oBuilder = new StringBuilder();
			bool fFirst = true;

			for (int i = 0; i < iLength; i++)
			{
				if (fFirst)
				{
					fFirst = false;
					oBuilder.Append(strEntityName);
					oBuilder.Append(" = ?");
				}
				else
				{
					oBuilder.Append(" OR ");
					oBuilder.Append(strEntityName);
					oBuilder.Append(" = ?");
				}
			}
			return oBuilder.ToString();
		}

	}
}
