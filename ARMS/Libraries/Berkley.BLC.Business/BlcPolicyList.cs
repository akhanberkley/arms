using System;
using System.Collections.Generic;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
	public class BlcPolicyList
	{
		private List<Policy> moUpdatePolicyList;
		private Transaction moTransaction;

		public BlcPolicyList(List<Policy> oUpdatePolicyList, Transaction oTransaction)
		{
			moUpdatePolicyList = oUpdatePolicyList;
			moTransaction = oTransaction;
		}

		public Policy[] LoadExisting(string strPolicyNumber)
		{
			return Policy.GetArray("Number Like ?", string.Format("{0}%", strPolicyNumber));
		}

		public void FixModDates()
		{
			SortedDictionary<DateTime, Policy> oSortedPolicies = new SortedDictionary<DateTime, Policy>();
			List<Policy> oPolicyList = new List<Policy>();
			SortedDictionary<int, int> oModPositions = new SortedDictionary<int, int>();
			int iCount = 0;
			int iDistance = 0;
			bool fAdded = false;

			// Sort the policies on the Expire date - the Policy.Mod should be in the same order
			foreach (Policy oPolicy in moUpdatePolicyList)
			{
				oSortedPolicies.Clear();
				oPolicyList.Clear();
				Policy[] roPolicy = LoadExisting(oPolicy.Number);
				foreach (Policy oExistingPolicy in roPolicy)
				{
					if (BlcPolicy.Equals(oPolicy, oExistingPolicy))
					{
						oSortedPolicies[oPolicy.ExpireDate] = oPolicy;
					}
					else
					{
						oSortedPolicies[oExistingPolicy.ExpireDate] = oExistingPolicy;
					}
				}

				// Figure out the Policy position based on the sorted Expire Date
				foreach (Policy oSortedPolicy in oSortedPolicies.Values)
				{
					if (oSortedPolicy.Mod > 0)
					{
						oModPositions[iCount] = iCount;
					}
					iCount++;
				}
				iCount = 0;

				foreach (Policy oSortedPolicy in oSortedPolicies.Values)
				{
					fAdded = false;
					// The policy has a mod
					if (oModPositions.ContainsKey(iCount))
					{
						oPolicyList.Add(oSortedPolicy);
						fAdded = true;
					}
					else
					{
						iDistance = 1;
						for (int i = iCount + 1; i < moUpdatePolicyList.Count; i++)
						{
							if (oModPositions.ContainsKey(i))
							{
								oSortedPolicy.Mod = oModPositions[i] - iDistance;
								oPolicyList.Add(oSortedPolicy);
								fAdded = true;
								break;
							}
							iDistance++;
						}
					}
					iCount++;
					if (!fAdded)
					{
						oPolicyList.Add(oSortedPolicy);
					}
				}

				foreach (Policy oFixedPolicy in oPolicyList)
				{
					oFixedPolicy.Save(moTransaction);
				}
			}

		}
	}
}
