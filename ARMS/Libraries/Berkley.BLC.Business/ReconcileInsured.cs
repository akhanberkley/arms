using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
    public class ReconcileInsured : IReconcile<Insured>
    {
        private ReconcileActor<Insured> moActor;
        private BlcInsured moBlcInsured;
        private Company moCompany;
        private Survey moSurvey;
        private Transaction moTransaction;
        private List<Insured> moDeleteInsuredList = new List<Insured>();
        private List<Insured> moInsertInsuredList = new List<Insured>();
        private List<Insured> moUpdateInsuredList = new List<Insured>();

        public ReconcileInsured(BlcInsured oBlcInsured, Company oCompany)
        {
            moBlcInsured = oBlcInsured;
            moCompany = oCompany;
        }

        public ReconcileInsured(BlcInsured oBlcInsured, Survey oSurvey, Company oCompany)
        {
            moBlcInsured = oBlcInsured;
            moCompany = oCompany;
            moSurvey = oSurvey;
        }

        public void Reconcile()
        {
            using (moTransaction = DB.Engine.BeginTransaction())
            {
                Reconcile(moTransaction, false);
                moActor.DbTransaction.Commit();
            }
            moTransaction = null;
        }

        public void NewInsert()
        {
            using (moTransaction = DB.Engine.BeginTransaction())
            {
                Reconcile(moTransaction, true);
                moActor.DbTransaction.Commit();
            }
            moTransaction = null;
        }

        //public void ReconcileCoverages()
        //{
        //    using (moTransaction = DB.Engine.BeginTransaction())
        //    {
        //        ReconcileCoverage oReconcileCoverage = new ReconcileCoverage(moTransaction, moCompany, moBlcInsured);
        //        ReconcilePolicy oReconcilePolicy = new ReconcilePolicy(moBlcInsured, moTransaction);
                
        //        oReconcileCoverage.Reconcile(moBlcInsured.CoverageList, oReconcilePolicy);
        //        oReconcileCoverage.DeleteQueued();
        //        oReconcileCoverage.AddQueued();

        //        moTransaction.Commit();
        //    }
        //    moTransaction = null;
        //}

        private void Reconcile(Transaction oTransaction, bool newInsert)
        {
            Exception oCaughtException = null;
            ReconcileCoverage oReconcileCoverage = null;
            ReconcileAgency oReconcileAgency = null;
            ReconcilePolicy oReconcilePolicy = null;
            ReconcileLocation oReconcileLocation = null;
            //Insured oInsuredFromPolicy = null;
            string strClientId = String.Empty;

            try
            {
                oReconcileCoverage = new ReconcileCoverage(oTransaction, moCompany, moBlcInsured);
                Location[] roBuiltLocations = moBlcInsured.PolicyLocationList.ToArray();
                Policy[] roBuiltPolicies = moBlcInsured.PolicyList.ToArray();

                // -- Agency -- //
                oReconcileAgency = new ReconcileAgency(moCompany, moBlcInsured.AgencyEntity, moBlcInsured.AgencyEmailList, oTransaction);
                oReconcileAgency.Reconcile();
                moBlcInsured.AgencyEntity = oReconcileAgency.Entity;

                // -- Insured -- //
                if (moBlcInsured.PreviousClientId != String.Empty)
                {
                    strClientId = moBlcInsured.PreviousClientId;
                }
                else
                {
                    strClientId = moBlcInsured.InsuredEntity.ClientID;
                }

                if (!newInsert)
                {
                    if (moSurvey == null) 
                        throw new ArgumentNullException("survey");
                    else
                        moActor = new ReconcileActor<Insured>(new string[] { moSurvey.InsuredID.ToString() }, moBlcInsured.InsuredEntity, this, oTransaction);
                }
                else
                {
                    moActor = new ReconcileActor<Insured>(moBlcInsured.InsuredEntity, this, oTransaction);
                }
                //Run the reconcile
                moActor.Reconcile();
                moBlcInsured.InsuredEntity = moActor.Entity;
                
                // Set all policies for this insured to NOT Active
                DB.Engine.ExecuteScalar(string.Format("UPDATE Policy set IsActive=0 where InsuredID='{0}'", moBlcInsured.InsuredEntity.ID));

                // Get the existing Locations & Polocies in the DB for this Insured
                moBlcInsured.ExistingLocations = Location.GetArray("InsuredID = ?", moBlcInsured.InsuredEntity.ID);
                moBlcInsured.ExistingPolicies = Policy.GetArray("InsuredID = ?", moBlcInsured.InsuredEntity.ID);

                Hashtable existingPolicies = new Hashtable(moBlcInsured.ExistingPolicies.Length);
                foreach (Policy policy in moBlcInsured.ExistingPolicies)
                {
                    string key = string.Format("{0}-{1}", policy.Number, policy.Mod);
                    if (!existingPolicies.ContainsKey(key))
                    {
                        existingPolicies.Add(key, policy.ID);
                    }
                }

                // -- Policies -- // Do before the Locations - The ReconcilePolicy takes care of the Claim and Rate Mods
                oReconcilePolicy = new ReconcilePolicy(moBlcInsured, moActor.DbTransaction);
                oReconcilePolicy.Actor.Reconcile(moBlcInsured.ExistingPolicies, roBuiltPolicies, oReconcilePolicy);

                // -- Locations -- //  ReconcileLocation takes care of Building
                oReconcileLocation = new ReconcileLocation(moBlcInsured, moActor.DbTransaction);
                // Do the Inserts, Updates, and Deletes for the Locations
                oReconcileLocation.Actor.Reconcile(moBlcInsured.ExistingLocations, roBuiltLocations, oReconcileLocation);

                // -- Coverage -- //
                oReconcileCoverage.Reconcile(moBlcInsured.CoverageList, existingPolicies, oReconcilePolicy);

                oReconcilePolicy.DeleteQueued();
                oReconcileLocation.DeleteQueued();
                oReconcileCoverage.DeleteQueued();

                oReconcileAgency.AddQueued();
                AddQueued();
                oReconcilePolicy.AddQueued();
                oReconcileLocation.AddQueued();
                oReconcileCoverage.AddQueued();

                oReconcileAgency.UpdateQueued();
                UpdateQueued();
                oReconcilePolicy.UpdateQueued();
                oReconcileLocation.UpdateQueued();
            }
            catch (Exception oException)
            {
                oCaughtException = oException;
            }
            finally
            {
                try
                {
                    Clear();
                    if (oReconcileAgency != null)
                    {
                        oReconcileAgency.Clear();
                    }
                    if (oReconcilePolicy != null)
                    {
                        oReconcilePolicy.Clear();
                    }
                    if (oReconcileLocation != null)
                    {
                        oReconcileLocation.Clear();
                    }
                    if (oReconcileCoverage != null)
                    {
                        oReconcileCoverage.Clear();
                    }
                }
                catch(Exception oClearException)
                {
                    if (oCaughtException == null)
                    {
                        oCaughtException = oClearException;
                    }
                    else
                    {
                        oCaughtException.Data.Add("Clear Exception"," ALSO Clear() Exception!!!: " + oClearException.Message);
                    }
                }
                if (oCaughtException != null)
                {
                    throw oCaughtException;
                }
            }
        }

        public Insured GetInsuredFromIncomingPolicies()
        {
            string strSelect;
            Guid oGuid;
            Insured oInsured;

            foreach (Policy oPolicy in moBlcInsured.PolicyList)
            {
                strSelect = String.Format("SELECT dbo.Insured.InsuredID FROM dbo.Insured INNER JOIN dbo.Policy ON dbo.Insured.InsuredID = dbo.Policy.InsuredID INNER JOIN dbo.Company ON dbo.Insured.CompanyID = dbo.Company.CompanyID WHERE (dbo.Policy.PolicyNumber LIKE '{0}%') AND (dbo.Company.CompanyID = '{1}')", oPolicy.Number, moCompany.ID);
                object o = DB.Engine.ExecuteScalar(strSelect);
                if (o != null)
                {
                    oGuid = (Guid)o;
                    oInsured = Insured.GetOne("ID = ?", (Guid)o);
                    return oInsured;
                }
            }
            return null;
        }

        public Insured GetInsuredFromExistingSurvey()
        {
            string strSelect;
            Guid oGuid;
            Insured oInsured;

            foreach (Policy oPolicy in moBlcInsured.PolicyList)
            {
                strSelect = String.Format("SELECT dbo.Insured.InsuredID FROM dbo.Insured INNER JOIN dbo.Policy ON dbo.Insured.InsuredID = dbo.Policy.InsuredID INNER JOIN dbo.Company ON dbo.Insured.CompanyID = dbo.Company.CompanyID WHERE (dbo.Policy.PolicyNumber LIKE '{0}%') AND (dbo.Company.CompanyID = '{1}')", oPolicy.Number, moCompany.ID);
                object o = DB.Engine.ExecuteScalar(strSelect);
                if (o != null)
                {
                    oGuid = (Guid)o;
                    oInsured = Insured.GetOne("ID = ?", (Guid)o);
                    return oInsured;
                }
            }
            return null;
        }

        #region IReconcile<Insured> Members
        public void Clear()
        {
            moDeleteInsuredList.Clear();
            moInsertInsuredList.Clear();
            moUpdateInsuredList.Clear();
        }

        Insured IReconcile<Insured>.GetExisting(string[] rstrForeignKey)
        {
            Insured oExistingInsured = Insured.GetOne("ID = ?", rstrForeignKey[0]);
            return oExistingInsured;
        }

        public Insured InsuredEntity
        {
            get { return moActor.Entity; }
        }

        Insured IReconcile<Insured>.Entity
        {
            get { return moActor.Entity; }
        }

        private string VerifySicCode(string strSicCode)
        {
            // Verify that the code exists in the BLC Table
            SIC oSic = SIC.GetOne("Code = ?", strSicCode);
            if (oSic == null)
            {
                // If it doesn't exist, remove the code
                return "";
            }
            else
            {
                return strSicCode;
            }
        }

        Insured IReconcile<Insured>.Update(Insured oNewEntity, Insured oExistingEntity)
        {
            if (oExistingEntity.IsReadOnly)
            {
                oExistingEntity = oExistingEntity.GetWritableInstance();
            }
            oNewEntity.CompanyID = moCompany.ID;
            oNewEntity.AgencyID = moBlcInsured.AgencyEntity.ID;
            oNewEntity.SICCode = VerifySicCode(oNewEntity.SICCode);
            oExistingEntity = oExistingEntity.Assign(oNewEntity);
            moUpdateInsuredList.Add(oExistingEntity);
            return oExistingEntity;
        }

        Insured IReconcile<Insured>.Insert(Insured oNewEntity)
        {
            if (oNewEntity.IsReadOnly)
            {
                oNewEntity = oNewEntity.GetWritableInstance();
            }
            oNewEntity.CompanyID = moCompany.ID;
            oNewEntity.AgencyID = moBlcInsured.AgencyEntity.ID;
            oNewEntity.SICCode = VerifySicCode(oNewEntity.SICCode);
            moInsertInsuredList.Add(oNewEntity);
            return oNewEntity;
        }

        public void Delete(Insured oEntity)
        {
            moDeleteInsuredList.Add(oEntity);
        }

        public bool Equal(Insured oLhs, Insured oRhs)
        {
            return oLhs.ClientID == oRhs.ClientID;
        }

        public ReconcileActor<Insured> Actor
        {
            get { return moActor; }
        }

        public void DeleteQueued()
        {
            foreach (Insured oInsured in moDeleteInsuredList)
            {
                oInsured.Delete(moActor.DbTransaction);
            }
        }

        public void AddQueued()
        {
            foreach (Insured oInsured in moInsertInsuredList)
            {
                oInsured.Save(moTransaction);
            }
        }
        public void UpdateQueued()
        {
            Insured oCurrentInsured;
            
            foreach (Insured oInsured in moUpdateInsuredList)
            {
                oCurrentInsured = oInsured;
                if (oCurrentInsured.IsReadOnly)
                {
                    oCurrentInsured = oCurrentInsured.GetWritableInstance();
                }
                oCurrentInsured.Save(moTransaction);
            }
        }
        #endregion
    }
}
