using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Berkley.BLC.Entities;
using Berkley.Imaging;
using Berkley.Imaging.Server.FileNet;
using System.Configuration;     //WA-905 Doug Bruce 2015-09-22 - Dynamic FileNet Uri
using System.Drawing.Imaging;   //WA-905 Doug Bruce 2015-09-22 - TIFF merging
using System.Drawing;           //WA-905 Doug Bruce 2015-09-22 - TIFF merging

namespace Berkley.BLC.Business
{
    /// <summary>
    /// Contains utility methods to help abstract imaging tasks details from the caller.
    /// </summary>
    public class ImagingHelper
    {
        private Company _company;
        private string fileNetVersion = null;
        private IImagingServer _serverIS = null;
        private IImagingServerP8 _serverP8 = null;
        //private string ValidFileExtsP8 = ".mp3.mp4.wma.rar.zip.msg.eml.tiff.tif.jpg.jpeg.gif.png.bmp.psd.ai.svg.doc.dot.docx.dotx.docm.dotm.rtf.txt.xls.xlsx.xlsm.csv.one.ppt.pps.pptx.pptm.ppsx.ppsm.mpp.vsd.vsdx.vsdm.pdf.avi.mov.mp4.wmv.vcf.htm.html.xhtml.mhtml.mht.xml.";

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="company">The company whos imaging system should be used.</param>
        public ImagingHelper(Company company)
        {
            // get the configuration type from the resources file
            Berkley.BLC.Core.ConfigurationType configEnvironment = Berkley.BLC.Core.CoreSettings.ConfigurationType;
            fileNetVersion = company.FileNetVersion;    //WA-621 Doug Bruce 2015-04-30 - Make IS vs P8 setting per-company
            if (fileNetVersion == String.Empty)
            {
                fileNetVersion = "IS";
            }

            // get an imaging server instance for the passed company.  Company includes information for setting FileNet username, password, and docclass.
            // WA-621 Doug Bruce 2015-04-30 - Open server for the version of FileNet this company is set up for.
            if (fileNetVersion == "IS")
            {
                _serverIS = ServerFactory.GetServer(company, configEnvironment);
                _serverIS.SetUserP8(company.FileNetUsernameP8, company.FileNetPasswordP8, company.FileNetDocClassP8);   //WA-703 Doug Bruce 2015-05-21 - for backwards compatibility - to get the P8 doc number
            }
            else
            {
                _serverP8 = ServerFactoryP8.GetServer(company, configEnvironment);
                _serverP8.SetUser(company.FileNetUsernameP8, company.FileNetPasswordP8, company.FileNetDocClassP8, company.Number);
            }
            _company = company;
       }

        //=============================================================================================================
        // driver split methods
        //=============================================================================================================
        #region driver split methods
        /// <summary>
        /// Stores a file image in a company's imaging system.
        /// </summary>
        /// <param name="fileName">The name of the file being imaged.</param>
        /// <param name="mimeType">MIME type of the file being imaged.</param>
        /// <param name="contentStream">The content of the image to be stored.</param>
        /// <returns>The FileNet reference number be used when retreiving the image.</returns>
        public string StoreFileImage(string fileName, string mimeType, Stream contentStream)
        {
            //This will be split inside this called method
            return StoreFileImage(fileName, mimeType, contentStream, null, null, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, string.Empty, null, string.Empty);
        }

        /// <summary>
        /// Stores a file image in a company's imaging system.
        /// </summary>
        /// <param name="fileName">The name of the file being imaged.</param>
        /// <param name="mimeType">MIME type of the file being imaged.</param>
        /// <param name="contentStream">The content of the image to be stored.</param>
        /// <param name="docType">The document type, default is null.</param>
        /// <param name="docRemarks">Remarks tied to the document.</param>
        /// <param name="docEffectiveDate">The effective date of the document.</param>
        /// <param name="docExpirationDate">The expiration date of the document.</param>
        /// <param name="scanDate">The date the document was uploaded into ARMS and sent to FileNet.</param>
        /// <param name="clientID">The client id.</param>
        /// <param name="policy">Optionl policy object.</param>
        /// <param name="policy">Optionl submission number.</param>
        /// <returns>The FileNet reference number be used when retreiving the image.</returns>
        public string StoreFileImage(string fileName, string mimeType, Stream contentStream, string docType, string docRemarks, DateTime docEffectiveDate, DateTime docExpirationDate, DateTime scanDate, string clientID, Policy policy, string submissionNumber)
        {
            string result = null;
            if (fileNetVersion == "P8")
            {
                result = StoreFileImageP8(fileName, mimeType, contentStream, docType, docRemarks, docEffectiveDate, docExpirationDate, scanDate, clientID, policy, submissionNumber);
            }
            else
            {
                result = StoreFileImageIS(fileName, mimeType, contentStream, docType, docRemarks, docEffectiveDate, docExpirationDate, scanDate, clientID, policy, submissionNumber);
            }
            return result;
        }

        /// <summary>
        /// Retrieve a file image from a company's imaging server.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <returns>The file image contents as a stream.</returns>
        public Stream RetrieveFileImage(string fileNetReference)
        {
            Stream result = new MemoryStream();
            if (fileNetVersion == "P8")
            {
                result = RetrieveFileImageP8(fileNetReference);
            }
            else
            {
                result = RetrieveFileImageIS(fileNetReference);
            }
            return result;
        }

        /// <summary>
        /// Retrieve a file image from a company's imaging server.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <param name="pageCount">The number of pages to return.</param>
        /// <returns>The file image contents as an array of byte[] arrays containing the file contents.</returns>
        public IFile[] RetrieveFile(string fileNetReference, int pageCount)
        {
            List<IFile> result = new List<IFile>();
            if (fileNetVersion == "P8")
            {
                result = RetrieveFileP8(fileNetReference, pageCount);
            }
            else
            {
                result = RetrieveFileIS(fileNetReference, pageCount);
            }
            return result.ToArray();
        }

        /// <summary>
        /// Retrieve a list of indexes for the queried documents from a company's imaging server.
        /// </summary>
        /// <param name="policyNumbers">The policy number(s) filter for the list of indexes</param>
        /// <param name="clientID">The client id filter for the list of indexes</param>
        /// <param name="docID">The document id filter for the list of indexes</param>
        /// <param name="docTypes">The doc type filter for the list of indexes</param>
        /// <param name="entryStartDate">The entry start date filter for the list of indexes</param>
        /// <param name="entryEndDate">The entry end date filter for the list of indexes</param>
        /// <returns>The list of indexes for the queried documents.</returns>
        public IDocument[] RetrieveFileIndexes(string[] policyNumbers, string clientID, string docID, string[] docTypes, DateTime entryStartDate, DateTime entryEndDate)
        {
            IDocument[] result = null;
            if (fileNetVersion == "P8")
            {
                result = RetrieveFileIndexesP8(policyNumbers, clientID, docID, docTypes, entryStartDate, entryEndDate);
            }
            else
            {
                result = RetrieveFileIndexesIS(policyNumbers, clientID, docID, docTypes, entryStartDate, entryEndDate);
            }
            return result;
        }

        /// <summary>
        /// Retrieve the PageCount Index in order to view multi-page tiffs.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <returns>The page count.</returns>
        public Int32 RetrievePageCount(string fileNetReference)
        {
            Int32 result = 0;
            if (fileNetVersion == "P8")
            {
                result = RetrievePageCountP8(fileNetReference);
            }
            else
            {
                result = RetrievePageCountIS(fileNetReference);
            }
            return result;
        }

        /// <summary>
        /// Update a file image from a company's imaging server.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <param name="docRemarks">The Doc Remarks index to update.</param>
        public void UpdateFileImage(string fileNetReference, string docRemarks)
        {
            if (fileNetVersion == "P8")
            {
                UpdateFileImageP8(fileNetReference, docRemarks);
            }
            else
            {
                UpdateFileImageIS(fileNetReference, docRemarks);
            }
        }

        /// <summary>
        /// Remove a file image from a company's imaging server.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <returns>The return string from FileNet.</returns>
        public void RemoveFileImage(string fileNetReference)
        {
            if (fileNetVersion == "P8")
            {
                RemoveFileImageP8(fileNetReference);
            }
            else
            {
                RemoveFileImageIS(fileNetReference);
            }
        }
        #endregion driver split methods

        //=============================================================================================================
        // P8 methods
        //=============================================================================================================
        #region P8 methods
        /// <summary>
        /// Stores a file image in a company's imaging system.
        /// </summary>
        /// <param name="fileName">The name of the file being imaged.</param>
        /// <param name="mimeType">MIME type of the file being imaged.</param>
        /// <param name="contentStream">The content of the image to be stored.</param>
        /// <returns>The FileNet reference number be used when retreiving the image.</returns>
        public string StoreFileImageP8(string fileName, string mimeType, Stream contentStream)
        {
            return StoreFileImageP8(fileName, mimeType, contentStream, null, null, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, string.Empty, null, string.Empty);
        }

        /// <summary>
        /// Stores a file image in a company's imaging system.
        /// </summary>
        /// <param name="fileName">The name of the file being imaged.</param>
        /// <param name="mimeType">MIME type of the file being imaged.</param>
        /// <param name="contentStream">The content of the image to be stored.</param>
        /// <param name="docType">The document type, default P8 null.</param>
        /// <param name="docRemarks">Remarks tied to the document.</param>
        /// <param name="docEffectiveDate">The effective date of the document.</param>
        /// <param name="docExpirationDate">The expiration date of the document.</param>
        /// <param name="scanDate">The date the document was uploaded into ARMS and sent to FileNet.</param>
        /// <param name="clientID">The client id.</param>
        /// <param name="policy">Optionl policy object.</param>
        /// <param name="policy">Optionl submission number.</param>
        /// <returns>The FileNet reference number be used when retreiving the image.</returns>
        public string StoreFileImageP8(string fileName, string mimeType, Stream contentStream, string docType, string docRemarks, DateTime docEffectiveDate, DateTime docExpirationDate, DateTime scanDate, string clientID, Policy policy, string submissionNumber)
        {
            if (contentStream == null) throw new ArgumentNullException("contentStream");
            if (contentStream.Length == 0) throw new ArgumentException("Content stream cannot be zero length.");

            string fileNetRef;
            try
            {
                // create the FileNet document
                DocumentP8 docP8 = (DocumentP8)_serverP8.CreateDocument();
                docP8.Company = _company;
                docP8.FileName = fileName;
                docP8.Extension = Path.GetExtension(fileName);
                docP8.MimeType = mimeType;
                docP8.DocClass = _company.FileNetDocClassP8;

                // create a file, attach it to the doc, and save everything
                ContentP8 fileP8 = (ContentP8)_serverP8.CreateFile();
                fileP8.Content = contentStream;
                fileP8.FileName = fileName;
                fileP8.MimeType = mimeType;

                docP8.ContentList.Add(fileP8);
                docP8.Type = DocumentType.Underwriting;

                //add doc type index
                if (docType != null && docType.Length > 0)
                {
                    PropertyP8 propP8 = new FileNetPropertyP8();
                    propP8.Name = FileNetPropertyP8.DocType;
                    propP8.Value = docType;
                    docP8.Properties.Add(propP8);
                }
                else
                {
                    PropertyP8 propP8 = new FileNetPropertyP8();
                    propP8.Name = FileNetPropertyP8.DocType;
                    propP8.Value = "ARMS";
                    docP8.Properties.Add(propP8);
                }

                //add doc remarks propP8
                if (docRemarks != null && docRemarks.Length > 0)
                {
                    PropertyP8 propP8 = new FileNetPropertyP8();
                    propP8.Name = FileNetPropertyP8.DocRemarks;
                    propP8.Value = docRemarks;
                    docP8.Properties.Add(propP8);
                }

                //add effective date propP8
                if (docEffectiveDate != DateTime.MinValue)
                {
                    PropertyP8 propP8 = new FileNetPropertyP8();
                    propP8.Name = FileNetPropertyP8.EffectiveDate;
                    propP8.Value = docEffectiveDate.ToShortDateString();
                    docP8.Properties.Add(propP8);
                }

                //add expiration date propP8
                if (docExpirationDate != DateTime.MinValue)
                {
                    PropertyP8 propP8 = new FileNetPropertyP8();
                    propP8.Name = FileNetPropertyP8.ExpirationDate;
                    propP8.Value = docExpirationDate.ToShortDateString();
                    docP8.Properties.Add(propP8);
                }

                //add scan date propP8
                if (scanDate != DateTime.MinValue)
                {
                    PropertyP8 propP8 = new FileNetPropertyP8();
                    propP8.Name = FileNetPropertyP8.ScanDate;
                    propP8.Value = scanDate.ToShortDateString();
                    docP8.Properties.Add(propP8);
                }

                //add client id
                if (clientID != null && clientID.Length > 0)
                {
                    PropertyP8 propP8 = new FileNetPropertyP8();
                    propP8.Name = FileNetPropertyP8.ClientID;
                    propP8.Value = clientID;
                    docP8.Properties.Add(propP8);
                }

                //add policy symbol
                if (_company.SupportsFileNetIndexPolicySymbol)
                {
                    if (policy != null && policy.Symbol.Trim().Length > 0)
                    {
                        PropertyP8 propP8 = new FileNetPropertyP8();
                        propP8.Name = FileNetPropertyP8.PolicySymbol;
                        propP8.Value = policy.Symbol.Trim();
                        docP8.Properties.Add(propP8);
                    }
                }

                //add policy number
                if (policy != null && policy.Number.Trim().Length > 0)
                {
                    PropertyP8 propP8 = new FileNetPropertyP8();
                    propP8.Name = FileNetPropertyP8.PolicyNumber;
                    propP8.Value = policy.Number.Trim();
                    docP8.Properties.Add(propP8);
                }

                //add the insured's name
                if (policy != null && policy.Insured != null && policy.Insured.Name.Trim().Length > 0)
                {
                    PropertyP8 propP8 = new FileNetPropertyP8();
                    propP8.Name = FileNetPropertyP8.InsuredName;
                    propP8.Value = policy.Insured.Name.Trim().ToUpper();
                    docP8.Properties.Add(propP8);
                }
                else
                {
                    PropertyP8 propP8 = new FileNetPropertyP8();
                    propP8.Name = FileNetPropertyP8.InsuredName;
                    propP8.Value = "ARMS";
                    docP8.Properties.Add(propP8);
                }

                //add the submission number
                if (submissionNumber != null && submissionNumber.Trim().Length > 0)
                {
                    PropertyP8 propP8 = new FileNetPropertyP8();
                    propP8.Name = FileNetPropertyP8.SubmissionNumber;
                    propP8.Value = submissionNumber.Trim();
                    docP8.Properties.Add(propP8);
                }

                //add agency number
                if (_company.SupportsFileNetIndexAgencyNumber)
                {
                    if (policy != null && policy.Insured.Agency != null && policy.Insured.Agency.Number.Trim().Length > 0)
                    {
                        PropertyP8 propP8 = new FileNetPropertyP8();
                        propP8.Name = FileNetPropertyP8.AgencyNumber;
                        propP8.Value = policy.Insured.Agency.Number.Trim();
                        docP8.Properties.Add(propP8);
                    }
                    else
                    {
                        PropertyP8 propP8 = new FileNetPropertyP8();
                        propP8.Name = FileNetPropertyP8.AgencyNumber;
                        propP8.Value = "ARMS";
                        docP8.Properties.Add(propP8);
                    }
                }

                //add the source system
                if (_company.SupportsFileNetIndexSourceSystem)
                {
                    PropertyP8 propP8 = new FileNetPropertyP8();
                    propP8.Name = FileNetPropertyP8.SourceSystem;
                    propP8.Value = "ARMS";
                    docP8.Properties.Add(propP8);
                }

                // save the doc to the imaging server and get the FileNet reference
                fileNetRef = _serverP8.Add(docP8);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save file image to imaging system.  See inner exception for details.", ex);
            }

            // return the FileNet reference
            return fileNetRef;
        }

        /// <summary>
        /// Retrieve a file image from a company's imaging server.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <param name="pageCount">The number of pages to return.</param>
        /// <returns>The file image contents as a stream.</returns>
        public List<IFile> RetrieveFileP8(string fileNetReference, int pageCount)
        {
            List<IFile> fileList = new List<IFile>();       //Return value (list of File images/ContentList items)

            string notFoundClasses = _company.Abbreviation; //default to company abbreviation in error message if not found

            try
            {
                //-------------------------------------------------------------------------
                //WA-905 Doug Bruce 2015-09-22 - Determine if related companies exist.
                //
                //  It is possible that a document might be stored in P8 under another
                //  DocClass than the default for the current company.  If related 
                //  companies exist, they will be in CompanyParameters.  If more than one,
                //  try hitting FileNet with each DocClass listed until one is found.  If 
                //  none are found after that, then throw the error, showing where you looked.
                //-------------------------------------------------------------------------

                //--------------------------------------------------------------------------------------------------------
                // Setup
                //--------------------------------------------------------------------------------------------------------
                List<DocClassCredentials> docClasses = GetRelatedDocClasses();   //List of potential DocClasses to search in (default empty)

                if (docClasses.Count > 1)
                {
                    notFoundClasses = String.Empty;   //Since this is now related company DocClasses; restart error list.
                }


                //--------------------------------------------------------------------------------------------------------
                // Search
                //--------------------------------------------------------------------------------------------------------

                //Save the original default DocClass and put it back when you're done
                string restoreDocClass = _serverP8.FileNetDocClass;
                string restoreCompanyNo = _serverP8.FileNetCompanyNo;

                //If there are not multiple Doc Classes, this will only be executed ONCE for the company's default Doc Class.
                foreach (DocClassCredentials docClass in docClasses)
                {
                    try 
                    {
                        _serverP8.FileNetDocClass = docClass.FileNetDocClassP8;   //Set the one RetrieveDocument will use to search in
                        _serverP8.FileNetCompanyNo = docClass.FileNetCompanyNo;    
                        //======================================================================================
                        // retrieve the document from FileNet
                        DocumentP8 document = _serverP8.RetrieveDocument(fileNetReference, 0, 0);
                        //======================================================================================
                        if (document != null)
                        {
                            //--------------------------------------------------------------------------------
                            //Check CompanyParameters to see if multiple content should be merged into one (default yes)
                            //--------------------------------------------------------------------------------
                            bool doMergeFiles = !(Berkley.BLC.Business.Utility.GetCompanyParameter("P8MergeMultipleContentToOne", _company).ToLower() == "false");  //default is DO MERGE if company parameter not found

                            //======================================================================================
                            if (doMergeFiles)
                            {
                                //------------------------------------------------------
                                //This is option for returning ONE merged file to caller
                                //------------------------------------------------------
                                //Collect all the ContentList items into a single stream for return
                                Stream singleFileImageStream = new MemoryStream();
                                if (document.ContentList.Count > 1)
                                {
                                    //throw new Exception("The document contains " + document.ContentList.Count + " files.  Exactly one was expected.");
                                    singleFileImageStream = MergeMultipleContentListItems(document);
                                    singleFileImageStream.Position = 0;    //Reset position so it can be read properly by caller
                                }
                                else
                                {
                                    singleFileImageStream = document.ContentList[0].Content;    //just return the first and only one
                                }
                                IFile thisfile = new FileIS();
                                thisfile.Content = singleFileImageStream;
                                fileList.Add(thisfile);
                            }
                            else
                            {
                                //------------------------------------------------------
                                //This is option for returning MULTIPLE files to caller
                                // (NOT recommended)
                                //-----------------------------------------------------
                                //Collect all the ContentList items for return
                                for (int i = 0; i < document.ContentList.Count; i++)
                                {
                                    IFile thisfile = new FileIS();
                                    thisfile.Content = document.ContentList[i].Content;
                                    fileList.Add(thisfile);
                                }
                            }
                            //======================================================================================

                            break;  //When you find it, stop looking through Doc Classes
                        }
                    }
                    catch (Exception)
                    {
                        notFoundClasses = notFoundClasses + docClass + " ";
                        //ignore and move on to next class
                    }
                }

                _serverP8.FileNetDocClass = restoreDocClass;
                _serverP8.FileNetCompanyNo = restoreCompanyNo;    

                //--------------------------------------------------------------------------------------------------------
                // Return
                //--------------------------------------------------------------------------------------------------------

                return fileList;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Failed to retrieve document with reference '{0}' from imaging system for the following related companies: [{1}]. See inner exception for details. RetrieveFileImageP8()", fileNetReference, notFoundClasses.Trim()), ex);
            }
        }

        /// <summary>
        /// Retrieve a file image from a company's imaging server.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <returns>The file image contents as a stream.</returns>
        public Stream RetrieveFileImageP8(string fileNetReference)
        {
            Stream singleFileImageStream = new MemoryStream();  //Return value (stream of a single file image)

            string notFoundClasses = _company.Abbreviation;     //default to company abbreviation in error message if not found
            
            try
            {
                //-------------------------------------------------------------------------
                //WA-905 Doug Bruce 2015-09-22 - Determine if related companies exist.
                //
                //  It is possible that a document might be stored in P8 under another
                //  DocClass than the default for the current company.  If related 
                //  companies exist, they will be in CompanyParameters.  If more than one,
                //  try hitting FileNet with each DocClass listed until one is found.  If 
                //  none are found after that, then throw the error, showing where you looked.
                //-------------------------------------------------------------------------

                //--------------------------------------------------------------------------------------------------------
                // Setup
                //--------------------------------------------------------------------------------------------------------
                List<DocClassCredentials> docClasses = GetRelatedDocClasses();   //List of potential DocClasses to search in (default empty)

                if (docClasses.Count > 1)
                {
                    notFoundClasses = String.Empty;   //Since this is now related company DocClasses; restart error list.
                }

                //--------------------------------------------------------------------------------------------------------
                // Search
                //--------------------------------------------------------------------------------------------------------

                //Save the original default DocClass and put it back when you're done
                string restoreDocClass = _serverP8.FileNetDocClass;
                string restoreCompanyNo = _serverP8.FileNetCompanyNo;

                //If there are not multiple Doc Classes, this will only be executed ONCE for the company's default Doc Class.
                foreach (DocClassCredentials docClass in docClasses)
                {
                    try
                    {
                        _serverP8.FileNetDocClass = docClass.FileNetDocClassP8;   //Set the one RetrieveDocument will use to search in
                        _serverP8.FileNetCompanyNo = docClass.FileNetCompanyNo;    

                        //======================================================================================
                        //Retrieve the document from FileNet
                        DocumentP8 document = (DocumentP8)_serverP8.RetrieveDocument(fileNetReference);
                        //======================================================================================
                        if (document != null)
                        {
                            //======================================================================================
                            // IMPORTANT: This DOES NOT consider company parameter P8MergeMultipleContentToOne. If
                            //            set to 'false', it could result in only the first of multiple pages being
                            //            returned to the user.  Multiple pages will ALWAYS be merged.
                            //======================================================================================
                            //If multiple ContentList items exist, merge them together. Per Imaging team, SHOULD ALWAYS BE TIFs!
                            if (document.ContentList.Count > 1)
                            {
                                //throw new Exception("The document contains " + document.ContentList.Count + " files.  Exactly one was expected.");
                                singleFileImageStream = MergeMultipleContentListItems(document);
                                singleFileImageStream.Position = 0;    //Reset position so it can be read properly by caller
                            }
                            else
                            {
                                singleFileImageStream = document.ContentList[0].Content;    //just return the first and only one
                            }
                            //======================================================================================

                            break;  //When you find it, stop looking through classes
                        }
                    }
                    catch (Exception)
                    {
                        notFoundClasses = notFoundClasses + docClass + " ";
                        //ignore and move on to next class
                    }
                }

                _serverP8.FileNetDocClass = restoreDocClass;
                _serverP8.FileNetCompanyNo = restoreCompanyNo;    

                //--------------------------------------------------------------------------------------------------------
                // Return
                //--------------------------------------------------------------------------------------------------------

                return singleFileImageStream;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Failed to retrieve document with reference '{0}' from imaging system for the following related companies: [{1}]. See inner exception for details. RetrieveFileImageP8()", fileNetReference, notFoundClasses.Trim()), ex);
            }
        }

            
        /// <summary>
        /// Retrieve a list of indexes for the queried documents from a company's imaging server.
        /// </summary>
        /// <param name="policyNumbers">The policy number(s) filter for the list of indexes</param>
        /// <param name="clientID">The client id filter for the list of indexes</param>
        /// <param name="docID">The document id filter for the list of indexes</param>
        /// <param name="docTypes">The doc type filter for the list of indexes</param>
        /// <param name="entryStartDate">The entry start date filter for the list of indexes</param>
        /// <param name="entryEndDate">The entry end date filter for the list of indexes</param>
        /// <returns>The list of indexes for the queried documents.</returns>
        public IDocument[] RetrieveFileIndexesP8(string[] policyNumbers, string clientID, string docID, string[] docTypes, DateTime entryStartDate, DateTime entryEndDate)
        {
            IDocument[] docList = new IDocument[0] { };     //Return value, array of IS format documents

            string notFoundClasses = _company.Abbreviation; //default to company abbreviation in error message if not found

            try
            {
                //-------------------------------------------------------------------------
                //WA-905 Doug Bruce 2015-09-22 - Determine if related companies exist.
                //
                //  It is possible that a document might be stored in P8 under another
                //  DocClass than the default for the current company.  If related 
                //  companies exist, they will be in CompanyParameters.  If more than one,
                //  try hitting FileNet with each DocClass listed until one is found.  If 
                //  none are found after that, then throw the error, showing where you looked.
                //-------------------------------------------------------------------------

                //--------------------------------------------------------------------------------------------------------
                // Setup
                //--------------------------------------------------------------------------------------------------------
                List<DocClassCredentials> docClasses = GetRelatedDocClasses();   //List of potential DocClasses to search in (default empty)

                if (docClasses.Count > 1)
                {
                    notFoundClasses = String.Empty;   //Since this is now related company DocClasses; restart error list.
                }

                //--------------------------------------------------------------------------------------------------------
                // Search
                //--------------------------------------------------------------------------------------------------------

                //Save the original default DocClass and put it back when you're done
                string restoreDocClass = _serverP8.FileNetDocClass;
                string restoreCompanyNo = _serverP8.FileNetCompanyNo;

                //If there are not multiple Doc Classes, this will only be executed ONCE for the company's default Doc Class.
                foreach (DocClassCredentials docClass in docClasses)
                {
                    try
                    {
                        _serverP8.FileNetDocClass = docClass.FileNetDocClassP8;   //Use within RetrieveIndexes() -> BuildFilters() to call the right CompanyNo.
                        _serverP8.FileNetCompanyNo = docClass.FileNetCompanyNo;    

                        //======================================================================================
                        // retrieve the indexes for the document
                        
                        // create the FileNet document so we can load the company
                        DocumentP8 documentP8 = (DocumentP8)_serverP8.CreateDocument();
                        documentP8.Company = _company;

                        // retrieve the indexes for all the documents from FileNet
                        List<DocumentP8> docsP8 = new List<DocumentP8>();
                        docsP8 = (List<DocumentP8>)_serverP8.RetrieveIndexes(documentP8, policyNumbers, clientID, docID, docTypes, entryStartDate, entryEndDate, 0, _company.SupportsFileNetIndexPolicySymbol);
                        //======================================================================================

                        if (docsP8 == null || docsP8.Count == 0)
                        {
                            //If not found, RetrieveIndexes() does not throw an error - it just returns no document.
                            // So we must explicitly throw the error to move on to the next DocClass.
                            throw new Exception(String.Format("No documents found for {0} in DocClass {1}. RetrieveFileIndexesP8()", docID, docClass));
                        }
                        else
                        {
                            //======================================================================================
                            // transfer to IS format list
                            List<DocumentIS> docsIS = new List<DocumentIS>();
                            foreach (DocumentP8 docP8 in docsP8)
                            {
                                DocumentIS d = (DocumentIS)TranslateDocumentToIS(docP8);    //this includes property->index translation
                                docsIS.Add(d);
                            }
                            // convert to IS format array for return
                            docList = docsIS.ToArray();
                            //======================================================================================

                            break;  //When you find it, stop searching DocClasses.
                        }
                    }
                    catch (Exception)
                    {
                        notFoundClasses = notFoundClasses + docClass + " ";
                        //ignore and move on to next class
                    }
                }

                _serverP8.FileNetDocClass = restoreDocClass;
                _serverP8.FileNetCompanyNo = restoreCompanyNo;    

                //--------------------------------------------------------------------------------------------------------
                // Return
                //--------------------------------------------------------------------------------------------------------

                return docList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Failed to fetch indexes with the following filters: '{0}', '{1}', '{2}', '{3}' from imaging system. Searched related companies: [{4}]. See inner exception for details. RetrieveFileIndexesP8()", policyNumbers.ToString(), docTypes.ToString(), entryStartDate.ToShortDateString(), entryEndDate.ToShortDateString(), notFoundClasses.Trim()), ex);
            }
        }

        /// <summary>
        /// Retrieve the PageCount Index in order to view multi-page tiffs.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <returns>The page count.</returns>
        public Int32 RetrievePageCountP8(string fileNetReference)
        {
            Int32 pageCount = 0;                            //Return value (page count for this document)

            string notFoundClasses = _company.Abbreviation; //default to company abbreviation in error message if not found

            try
            {
                //-------------------------------------------------------------------------
                //WA-905 Doug Bruce 2015-09-22 - Determine if related companies exist.
                //
                //  It is possible that a document might be stored in P8 under another
                //  DocClass than the default for the current company.  If related 
                //  companies exist, they will be in CompanyParameters.  If more than one,
                //  try hitting FileNet with each DocClass listed until one is found.  If 
                //  none are found after that, then throw the error, showing where you looked.
                //-------------------------------------------------------------------------

                //--------------------------------------------------------------------------------------------------------
                // Setup
                //--------------------------------------------------------------------------------------------------------
                List<DocClassCredentials> docClasses = GetRelatedDocClasses();   //List of potential DocClasses to search in (default empty)

                if (docClasses.Count > 1)
                {
                    notFoundClasses = String.Empty;   //Since this is now related company DocClasses; restart error list.
                }

                //--------------------------------------------------------------------------------------------------------
                // Search
                //--------------------------------------------------------------------------------------------------------
                //Save the original default DocClass and put it back when you're done
                string restoreDocClass = _serverP8.FileNetDocClass;
                string restoreCompanyNo = _serverP8.FileNetCompanyNo;

                //If there are not multiple Doc Classes, this will only be executed ONCE for the company's default Doc Class.
                foreach (DocClassCredentials docClass in docClasses)
                {
                    try
                    {
                        _serverP8.FileNetDocClass = docClass.FileNetDocClassP8;   //Set the one RetrieveDocument will use to search in
                        _serverP8.FileNetCompanyNo = docClass.FileNetCompanyNo;    

                        //======================================================================================
                        //Retrieve the page count from FileNet
                        pageCount = _serverP8.RetrievePageCount(fileNetReference);
                        //======================================================================================
                        if (pageCount > 0)
                        {
                            break;  //When you find it, stop searching DocClasses.
                        }
                    }
                    catch (Exception)
                    {
                        notFoundClasses = notFoundClasses + docClass + " ";
                        //ignore and move on to next class
                    }
                }

                _serverP8.FileNetDocClass = restoreDocClass;
                _serverP8.FileNetCompanyNo = restoreCompanyNo;    

                //--------------------------------------------------------------------------------------------------------
                // Return
                //--------------------------------------------------------------------------------------------------------
                return pageCount;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Failed to retrieve page count with reference '{0}' from imaging system for the following related companies: [{1}].  RetrievePageCountP8()", fileNetReference, notFoundClasses.Trim()), ex);
            }
        }

        /// <summary>
        /// Update a file image from a company's imaging server.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <param name="docRemarks">The Doc Remarks index to update.</param>
        public void UpdateFileImageP8(string fileNetReference, string docRemarks)
        {
            string notFoundClasses = _company.Abbreviation;    //default to company abbreviation in error message if not found

            try
            {
                //-------------------------------------------------------------------------
                //WA-905 Doug Bruce 2015-09-22 - Determine if related companies exist.
                //
                //  It is possible that a document might be stored in P8 under another
                //  DocClass than the default for the current company.  If related 
                //  companies exist, they will be in CompanyParameters.  If more than one,
                //  try hitting FileNet with each DocClass listed until one is found.  If 
                //  none are found after that, then throw the error, showing where you looked.
                //-------------------------------------------------------------------------

                //--------------------------------------------------------------------------------------------------------
                // Setup
                //--------------------------------------------------------------------------------------------------------
                List<DocClassCredentials> docClasses = GetRelatedDocClasses();   //List of potential DocClasses to search in (default empty)

                if (docClasses.Count > 1)
                {
                    notFoundClasses = String.Empty;   //Since this is now related company DocClasses; restart error list.
                }

                //--------------------------------------------------------------------------------------------------------
                // Search
                //--------------------------------------------------------------------------------------------------------
                //Save the original default DocClass and put it back when you're done
                string restoreDocClass = _serverP8.FileNetDocClass;
                string restoreCompanyNo = _serverP8.FileNetCompanyNo;

                //If there are not multiple Doc Classes, this will only be executed ONCE for the company's default Doc Class.
                foreach (DocClassCredentials docClass in docClasses)
                {
                    try
                    {
                        _serverP8.FileNetDocClass = docClass.FileNetDocClassP8;   //Set the one RetrieveDocument will use to search in
                        _serverP8.FileNetCompanyNo = docClass.FileNetCompanyNo;    

                        //======================================================================================
                        // retrieve the document from FileNet
                        DocumentP8 document = (DocumentP8)_serverP8.RetrieveDocument(fileNetReference);
                        //======================================================================================

                        if (document != null)
                        {
                            //======================================================================================
                            document.DocNumber = fileNetReference;

                            /*
                             * In P8, to update a property of this document, you only need to send in the one
                             * you're updating, NOT the whole list (if you do, you will get errors for trying to
                             * update read-only parameters).  So just build a parameter list of the ones you want
                             * updated, and attach it to this document.
                             * */                            
                            List<PropertyP8> updatedProperties = new List<PropertyP8>();

                            PropertyP8 prop = new FileNetPropertyP8();
                            prop.Name = FileNetPropertyP8.DocRemarks;
                            prop.Value = docRemarks;
                            updatedProperties.Add(prop);

                            _serverP8.UpdateProperties(document.Id, updatedProperties);
                            //======================================================================================

                            break;  //If there was no error, it will get here; stop searching DocClasses.
                        }
                    }
                    catch (Exception)
                    {
                        notFoundClasses = notFoundClasses + docClass + " ";
                        //ignore and move on to next class
                    }
                }

                _serverP8.FileNetDocClass = restoreDocClass;
                _serverP8.FileNetCompanyNo = restoreCompanyNo;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Failed to update document with reference '{0}', remarks '{2}' from imaging system for the following related companies: [{1}].  UpdateFileImageP8()", fileNetReference, notFoundClasses.Trim(), docRemarks), ex);
            }
        }

        /// <summary>
        /// Remove a file image from a company's imaging server.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <returns>The return string from FileNet.</returns>
        public void RemoveFileImageP8(string fileNetReference)
        {
            string notFoundClasses = _company.Abbreviation;    //default to company abbreviation in error message if not found

            try
            {
                //-------------------------------------------------------------------------
                //WA-905 Doug Bruce 2015-09-22 - Determine if related companies exist.
                //
                //  It is possible that a document might be stored in P8 under another
                //  DocClass than the default for the current company.  If related 
                //  companies exist, they will be in CompanyParameters.  If more than one,
                //  try hitting FileNet with each DocClass listed until one is found.  If 
                //  none are found after that, then throw the error, showing where you looked.
                //-------------------------------------------------------------------------

                //--------------------------------------------------------------------------------------------------------
                // Setup
                //--------------------------------------------------------------------------------------------------------
                List<DocClassCredentials> docClasses = GetRelatedDocClasses();   //List of potential DocClasses to search in (default empty)

                if (docClasses.Count > 1)
                {
                    notFoundClasses = String.Empty;   //Since this is now related company DocClasses; restart error list.
                }

                //--------------------------------------------------------------------------------------------------------
                // Search
                //--------------------------------------------------------------------------------------------------------
                //Save the original default DocClass and put it back when you're done
                string restoreDocClass = _serverP8.FileNetDocClass;
                string restoreCompanyNo = _serverP8.FileNetCompanyNo;

                //If there are not multiple Doc Classes, this will only be executed ONCE for the company's default Doc Class.
                foreach (DocClassCredentials docClass in docClasses)
                {
                    try
                    {
                        _serverP8.FileNetDocClass = docClass.FileNetDocClassP8;   //Set the one RetrieveDocument will use to search in
                        _serverP8.FileNetCompanyNo = docClass.FileNetCompanyNo;    

                        //======================================================================================
                        _serverP8.RemoveDocument(fileNetReference);
                        //======================================================================================

                        break;  //If there was no error, it will get here; stop searching DocClasses.
                    }
                    catch (Exception)
                    {
                        notFoundClasses = notFoundClasses + docClass + " ";
                        //ignore and move on to next class
                    }
                }

                _serverP8.FileNetDocClass = restoreDocClass;
                _serverP8.FileNetCompanyNo = restoreCompanyNo;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Failed to remove document with reference '{0}' from imaging system for the following related companies: [{1}].  RemoveFileImageP8()", fileNetReference, notFoundClasses.Trim()), ex);
            }
        }
        #endregion P8 methods

        //=============================================================================================================
        // IS methods
        //=============================================================================================================
        #region IS methods

        /// <summary>
        /// Stores a file image in a company's imaging system.
        /// </summary>
        /// <param name="fileName">The name of the file being imaged.</param>
        /// <param name="mimeType">MIME type of the file being imaged.</param>
        /// <param name="contentStream">The content of the image to be stored.</param>
        /// <returns>The FileNet reference number be used when retreiving the image.</returns>
        public string StoreFileImageIS(string fileName, string mimeType, Stream contentStream)
        {
            return StoreFileImageIS(fileName, mimeType, contentStream, null, null, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, string.Empty, null, string.Empty);
        }

        /// <summary>
        /// Stores a file image in a company's imaging system.
        /// </summary>
        /// <param name="fileName">The name of the file being imaged.</param>
        /// <param name="mimeType">MIME type of the file being imaged.</param>
        /// <param name="contentStream">The content of the image to be stored.</param>
        /// <param name="docType">The document type, default is null.</param>
        /// <param name="docRemarks">Remarks tied to the document.</param>
        /// <param name="docEffectiveDate">The effective date of the document.</param>
        /// <param name="docExpirationDate">The expiration date of the document.</param>
        /// <param name="scanDate">The date the document was uploaded into ARMS and sent to FileNet.</param>
        /// <param name="clientID">The client id.</param>
        /// <param name="policy">Optionl policy object.</param>
        /// <param name="policy">Optionl submission number.</param>
        /// <returns>The FileNet reference number be used when retreiving the image.</returns>
        public string StoreFileImageIS(string fileName, string mimeType, Stream contentStream, string docType, string docRemarks, DateTime docEffectiveDate, DateTime docExpirationDate, DateTime scanDate, string clientID, Policy policy, string submissionNumber)
        {
            if (contentStream == null) throw new ArgumentNullException("contentStream");
            if (contentStream.Length == 0) throw new ArgumentException("Content stream cannot be zero length.");

            string fileNetRef;
            try
            {
                // create the FileNet document
                IDocument document = _serverIS.CreateDocument();
                document.Company = _company;
                document.FileName = fileName;
                document.Extension = Path.GetExtension(fileName);

                // create a file, attach it to the doc, and save everything
                IFile file = _serverIS.CreateFile();
                file.Content = contentStream;

                document.Files.Add(file);
                document.MimeType = mimeType;
                document.Type = DocumentType.Underwriting;

                //add doc type index
                if (docType != null && docType.Length > 0)
                {
                    IIndex index = new FileNetIndex();
                    index.Name = FileNetIndex.DocType;
                    index.Value = docType;
                    document.Indexes.Add(index);
                }
                else
                {
                    IIndex index = new FileNetIndex();
                    index.Name = FileNetIndex.DocType;
                    index.Value = "ARMS";
                    document.Indexes.Add(index);
                }

                //add doc remarks index
                if (docRemarks != null && docRemarks.Length > 0)
                {
                    IIndex index = new FileNetIndex();
                    index.Name = FileNetIndex.DocRemarks;
                    index.Value = docRemarks;
                    document.Indexes.Add(index);
                }

                //add effective date index
                if (docEffectiveDate != DateTime.MinValue)
                {
                    IIndex index = new FileNetIndex();
                    index.Name = FileNetIndex.EffectiveDate;
                    index.Value = docEffectiveDate.ToShortDateString();
                    document.Indexes.Add(index);
                }

                //add expiration date index
                if (docExpirationDate != DateTime.MinValue)
                {
                    IIndex index = new FileNetIndex();
                    index.Name = FileNetIndex.ExpirationDate;
                    index.Value = docExpirationDate.ToShortDateString();
                    document.Indexes.Add(index);
                }

                //add scan date index
                if (scanDate != DateTime.MinValue)
                {
                    IIndex index = new FileNetIndex();
                    index.Name = FileNetIndex.ScanDate;
                    index.Value = scanDate.ToShortDateString();
                    document.Indexes.Add(index);
                }

                //add client id
                if (clientID != null && clientID.Length > 0)
                {
                    IIndex index = new FileNetIndex();
                    index.Name = _company.FileNetClientIDIndexName;
                    index.Value = clientID;
                    document.Indexes.Add(index);
                }

                //add policy symbol
                if (_company.SupportsFileNetIndexPolicySymbol)
                {
                    if (policy != null && policy.Symbol.Trim().Length > 0)
                    {
                        IIndex index = new FileNetIndex();
                        index.Name = FileNetIndex.PolicySymbol;
                        index.Value = policy.Symbol.Trim();
                        document.Indexes.Add(index);
                    }
                }

                //add policy number
                if (policy != null && policy.Number.Trim().Length > 0)
                {
                    IIndex index = new FileNetIndex();
                    index.Name = FileNetIndex.PolicyNumber;
                    index.Value = policy.Number.Trim();
                    document.Indexes.Add(index);
                }

                //add the insured's name
                if (policy != null && policy.Insured != null && policy.Insured.Name.Trim().Length > 0)
                {
                    IIndex index = new FileNetIndex();
                    index.Name = FileNetIndex.InsuredName;
                    index.Value = policy.Insured.Name.Trim().ToUpper();
                    document.Indexes.Add(index);
                }
                else
                {
                    IIndex index = new FileNetIndex();
                    index.Name = FileNetIndex.InsuredName;
                    index.Value = "ARMS";
                    document.Indexes.Add(index);
                }

                //add the submission number
                if (submissionNumber != null && submissionNumber.Trim().Length > 0)
                {
                    IIndex index = new FileNetIndex();
                    index.Name = FileNetIndex.SubmissionNumber;
                    index.Value = submissionNumber.Trim();
                    document.Indexes.Add(index);
                }

                //add agency number
                if (_company.SupportsFileNetIndexAgencyNumber)
                {
                    if (policy != null && policy.Insured.Agency != null && policy.Insured.Agency.Number.Trim().Length > 0)
                    {
                        IIndex index = new FileNetIndex();
                        index.Name = FileNetIndex.AgencyNumber;
                        index.Value = policy.Insured.Agency.Number.Trim();
                        document.Indexes.Add(index);
                    }
                    else
                    {
                        IIndex index = new FileNetIndex();
                        index.Name = FileNetIndex.AgencyNumber;
                        index.Value = "ARMS";
                        document.Indexes.Add(index);
                    }
                }

                //add the source system
                if (_company.SupportsFileNetIndexSourceSystem)
                {
                    IIndex index = new FileNetIndex();
                    index.Name = FileNetIndex.SourceSystem;
                    index.Value = "ARMS";
                    document.Indexes.Add(index);
                }

                // save the doc to the imaging server and get the FileNet reference
                fileNetRef = _serverIS.Add(document, _company.FileNetUsername, _company.FileNetPassword);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save file image to imaging system.  See inner exception for details.", ex);
            }

            // return the FileNet reference
            return fileNetRef;
        }

        /// <summary>
        /// Retrieve a file image from a company's imaging server.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <returns>The file image contents as a stream.</returns>
        public Stream RetrieveFileImageIS(string fileNetReference)
        {
            try
            {
                // retrieve the document
                IDocument document = _serverIS.RetrieveDocument(fileNetReference, _company.FileNetUsername, _company.FileNetPassword);

                // verify exactly one file exists in the doc
                if (document.Files.Count != 1)
                {
                    throw new Exception("The document contains " + document.Files.Count + " files.  Exactly one was expected.");
                }
                return document.Files[0].Content;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to fetch file image with reference '" + fileNetReference + "' from imaging system.  See inner exception for details.", ex);
            }
        }

        /// <summary>
        /// Retrieve a file image from a company's imaging server.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <param name="pageCount">The number of pages to return.</param>
        /// <returns>The file image contents as a stream.</returns>
        public List<IFile> RetrieveFileIS(string fileNetReference, int pageCount)
        {
            try
            {
                // retrieve the files
                List<IFile> list = new List<IFile>(pageCount);
                if (pageCount > 0)
                {
                    for (int i = 1; i < pageCount + 1; i++)
                    {
                        list.Add(_serverIS.RetrieveDocument(fileNetReference, i, 0, _company.FileNetUsername, _company.FileNetPassword).Files[0]);
                    }
                }
                else
                {
                    list.Add(_serverIS.RetrieveDocument(fileNetReference, 0, 0, _company.FileNetUsername, _company.FileNetPassword).Files[0]);
                }

                return list;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to fetch file image with reference '" + fileNetReference + "' from imaging system.  See inner exception for details.", ex);
            }
        }

        /// <summary>
        /// Retrieve a list of indexes for the queried documents from a company's imaging server.
        /// </summary>
        /// <param name="policyNumbers">The policy number(s) filter for the list of indexes</param>
        /// <param name="clientID">The client id filter for the list of indexes</param>
        /// <param name="docID">The document id filter for the list of indexes</param>
        /// <param name="docTypes">The doc type filter for the list of indexes</param>
        /// <param name="entryStartDate">The entry start date filter for the list of indexes</param>
        /// <param name="entryEndDate">The entry end date filter for the list of indexes</param>
        /// <returns>The list of indexes for the queried documents.</returns>
        public IDocument[] RetrieveFileIndexesIS(string[] policyNumbers, string clientID, string docID, string[] docTypes, DateTime entryStartDate, DateTime entryEndDate)
        {
            try
            {
                // create the FileNet document so we can load the company
                IDocument document = _serverIS.CreateDocument();
                document.Company = _company;

                // retrieve the indexes for all the documents
                return _serverIS.RetrieveIndexes(document, policyNumbers, clientID, docID, docTypes, entryStartDate, entryEndDate, 0, _company.FileNetUsername, _company.FileNetPassword);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Failed to fetch indexes with the following filters: '{0}', '{1}', '{2}', '{3}' from imaging system.  See inner exception for details.", policyNumbers.ToString(), docTypes.ToString(), entryStartDate.ToShortDateString(), entryEndDate.ToShortDateString()), ex);
            }
        }

        /// <summary>
        /// Retrieve the PageCount Index in order to view multi-page tiffs.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <returns>The page count.</returns>
        public Int32 RetrievePageCountIS(string fileNetReference)
        {
            try
            {
                return _serverIS.RetrievePageCount(fileNetReference, _company.FileNetUsername, _company.FileNetPassword);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to fetch file image with reference '" + fileNetReference + "' from imaging system.  See inner exception for details.", ex);
            }
        }

        /// <summary>
        /// Update a file image from a company's imaging server.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <param name="docRemarks">The Doc Remarks index to update.</param>
        public void UpdateFileImageIS(string fileNetReference, string docRemarks)
        {
            try
            {
                // retrieve the document
                IDocument document = _serverIS.RetrieveDocument(fileNetReference, _company.FileNetUsername, _company.FileNetPassword);
                document.DocNumber = fileNetReference;

                // make any update to indexes here
                if (docRemarks != null && docRemarks.Length > 0)
                {
                    if (document.Indexes.Contains(FileNetIndex.DocRemarks))
                    {
                        // remove the existing index
                        document.Indexes[FileNetIndex.DocRemarks].Value = docRemarks;
                        //document.Indexes.RemoveAt(document.Indexes.IndexOf(FileNetIndex.DocRemarks));
                    }
                    else
                    {
                        IIndex index = new FileNetIndex();
                        index.Name = FileNetIndex.DocRemarks;
                        index.Value = docRemarks;
                        document.Indexes.Add(index);
                    }
                }

                _serverIS.Update(document, _company.FileNetUsername, _company.FileNetPassword);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to fetch & update file image with reference '" + fileNetReference + "' from imaging system.  See inner exception for details.", ex);
            }
        }

        /// <summary>
        /// Remove a file image from a company's imaging server.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number of the image.</param>
        /// <returns>The return string from FileNet.</returns>
        public void RemoveFileImageIS(string fileNetReference)
        {
            try
            {
                // retrieve the document
                _serverIS.RemoveDocument(fileNetReference, _company.FileNetUsername, _company.FileNetPassword);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to remove file image with reference '" + fileNetReference + "' from imaging system.  See inner exception for details.", ex);
            }
        }
        #endregion IS methods

        //=============================================================================================================
        // support methods
        //=============================================================================================================
        #region Support methods

        /// <summary>
        /// Obscures the details of getting a company parameter for getting a potential list of doc classes back.
        /// </summary>
        /// <returns>The list of DocClasses to search (only the current company's if coparam not found)</returns>
        protected List<DocClassCredentials> GetRelatedDocClasses()
        {
            List<DocClassCredentials> docClasses = new List<DocClassCredentials>();   //List of potential DocClasses to search in (default empty)

            //These are set as a Parameter/CompanyParameter per company
            Parameter param = Parameter.GetOne("Description = ?", "RelatedDocClasses");
            if (param == null)
            {
                //No related companies, so use default DocClass for this company
                DocClassCredentials dcc = new DocClassCredentials();
                dcc.FileNetDocClassP8 = _company.FileNetDocClassP8;
                dcc.FileNetCompanyNo = _company.Number;
                docClasses.Add(dcc);     //So use default DocClass for this company
            }
            else
            {
                //It exists; check CompanyParameters for this company to see if there are related companies
                CompanyParameter[] cParam = CompanyParameter.GetSortedArray("CompanyID = ? && ParameterID = ?", "SortOrder ASC", _company.ID, param.ID);

                if (cParam == null || cParam.Length == 0)
                {
                    //No related companies, so use default DocClass for this company
                    DocClassCredentials dcc = new DocClassCredentials();
                    dcc.FileNetDocClassP8 = _company.FileNetDocClassP8;
                    dcc.FileNetCompanyNo = _company.Number;
                    docClasses.Add(dcc);     //
                }
                else
                {
                    //Related companies found (at least one).  Build list of DocClasses for each to search in
                    for (int i = 0; i < cParam.Length; i++){
                        string[] dc = cParam[i].Value.Split(',');
                        DocClassCredentials dcc = new DocClassCredentials();
                        dcc.FileNetDocClassP8 = dc[0];  //DocClass
                        dcc.FileNetCompanyNo = dc[1];   //CompanyNumber for this class
                        docClasses.Add(dcc);
                    }
                }
            }

            return docClasses;
        }

        //WA-905 Doug Bruce 2015-09-15 - Manage merging different types of contentList items into one stream.
        /// <summary>
        /// Merge multiple file stream together into one file stream
        /// </summary>
        /// <param name="document">document with the ContentList array of files to be merged into one file stream</param>
        /// <returns>The single merged file stream</returns>
        protected Stream MergeMultipleContentListItems(DocumentP8 document)
        {
            Stream singleFileImageStream = new MemoryStream();  //Return value (stream of a single file image)
            string mimeType = document.ContentList[0].MimeType;
            string fileExtension = "." + System.IO.Path.GetExtension(document.ContentList[0].FileName).ToLower();

            if (document.ContentList.Count > 0)
            {
                if (mimeType == "image/tiff")
                {
                    //tif files
                    singleFileImageStream = mergeMultipleToSingleTIFF(document);
                }
                else if (fileExtension.Contains(".png") || fileExtension.Contains(".bmp") || fileExtension.Contains(".gif") || fileExtension.Contains(".jp"))
                {
                    //graphics files
                    singleFileImageStream = mergeMultipleToSingleGraphicsFile(document);
                }
                else
                {
                    //other files
                    singleFileImageStream = mergeMultipleToSingleOtherFile(document);
                }
            }
            else
            {
                //Only one ContentList item was sent in.
                document.ContentList[0].Content.CopyTo(singleFileImageStream);
            }

            singleFileImageStream.Position = 0;    //Reset position so it can be read properly by caller
            return singleFileImageStream;
        }

        //WA-905 Doug Bruce 2015-09-15 - Merge TIF files into one.
        //http://stackoverflow.com/questions/5861060/merging-multiple-tiff-files-in-one-tiff-file, adapted.
        /// <summary>
        /// Merge multiple TIF files into one (in the form of a stream).
        /// </summary>
        /// <param name="document">ContentList collection of file image streams retrieved from FileNet</param>
        /// <returns>The merged file image contents as a stream.</returns>
        public Stream mergeMultipleToSingleTIFF(DocumentP8 document)
        {
            if (document == null)
                throw new ArgumentNullException("docIn");

            IContentCollectionP8 contentListIn = document.ContentList;

            if (document.ContentList == null)
                throw new ArgumentNullException("contentListIn");

            //Setup memorystream array elements to hold the ContentList items.
            MemoryStream[] streamsToMerge = new MemoryStream[contentListIn.Count];

            //Get file extension and MIME type of first item; assume the rest are the same.
            string fileExtension = Path.GetExtension(contentListIn[0].FileName);
            string mimeType = contentListIn[0].MimeType;

            //if (mimeType != "image/tiff")
            //{
            //    throw new Exception(String.Format("mergeMultipleToSingleTIFF(): Unable to merge content of MIME type [{0}], document [{1}] (containing {2} ContentList items).", mimeType, document.Id, contentListIn.Count));
            //}

            //Copy ContentList bytes to array of memory streams
            for (int i = 0; i < contentListIn.Count; i++)
            {
                streamsToMerge[i] = new MemoryStream(contentListIn[i].ContentBytes);
            }

            //Set up encoding
            System.Drawing.Imaging.Encoder enc = System.Drawing.Imaging.Encoder.SaveFlag;

            //Set MIME types (sent in from the doc the ContentList came from)
            ImageCodecInfo info = null;
            foreach (ImageCodecInfo ice in ImageCodecInfo.GetImageEncoders())
                if (ice.MimeType == mimeType)
                    info = ice;

            //Merge them into one stream
            EncoderParameters ep = new EncoderParameters(1);
            ep.Param[0] = new EncoderParameter(enc, (long)EncoderValue.MultiFrame);
            MemoryStream streamOut = new MemoryStream();
            Bitmap bitmapOut = new Bitmap(streamsToMerge[0]);
            bitmapOut.Save(streamOut, info, ep);

            ep.Param[0] = new EncoderParameter(enc, (long)EncoderValue.FrameDimensionPage);
            for (int x = 1; x < streamsToMerge.Length; x++)
            {
                bitmapOut.SaveAdd(Image.FromStream(streamsToMerge[x]), ep);
            }

            ep.Param[0] = new EncoderParameter(enc, (long)EncoderValue.Flush);
            bitmapOut.SaveAdd(ep);

            return streamOut;
        }

        //WA-905 Doug Bruce 2015-09-15 - Merge various graphics files into one.
        // Using streams and bitmaps
        //http://stackoverflow.com/questions/6383123/merge-two-images-to-create-a-single-image-in-c-net
        // Writing to files and concatenating them, like MakePDF does it
        //https://social.msdn.microsoft.com/forums/vstudio/en-US/815d5586-2587-4a17-bee1-c6d06000ad68/getfiles-all-picture-files
        //http://www.codeproject.com/Articles/502249/Combineplusseveralplusimagesplustoplusformplusaplu
        /// <summary>
        /// Merge multiple graphics files into one (in the form of a stream).
        /// </summary>
        /// <param name="document">ContentList collection of file image streams retrieved from FileNet</param>
        /// <returns>The merged file image contents as a stream.</returns>
        public Stream mergeMultipleToSingleGraphicsFile(DocumentP8 document)
        {
            //EXPAND OUT USING ONE OF THE ABOVE METHODS!
            Stream singleFileImageStream = new MemoryStream();  //Return value (stream of a single file image)
            
            for (int i = 0; i < document.ContentList.Count; i++)
                document.ContentList[i].Content.CopyTo(singleFileImageStream);

            singleFileImageStream.Position = 0;
            return singleFileImageStream;
        }

        //WA-905 Doug Bruce 2015-09-15 - Merge other files into one.
        // Here is code for ANY file type to be concatenated:
        //http://stackoverflow.com/questions/444309/what-would-be-the-fastest-way-to-concatenate-three-files-in-c
        //OR SEE StreamDocument - that writes out files for MakePDF to merge... replace MakePDF with a file concat.
        /// <summary>
        /// Merge multiple files into one (in the form of a stream).
        /// </summary>
        /// <param name="document">ContentList collection of file image streams retrieved from FileNet</param>
        /// <returns>The merged file image contents as a stream.</returns>
        public Stream mergeMultipleToSingleOtherFile(DocumentP8 document)
        {
            //TEST FIRST - IF NOT WORKING, EXPAND OUT USING ONE OF THE ABOVE METHODS!
            Stream singleFileImageStream = new MemoryStream();  //Return value (stream of a single file image)

            for (int i = 0; i < document.ContentList.Count; i++)
                document.ContentList[i].Content.CopyTo(singleFileImageStream);

            singleFileImageStream.Position = 0;
            return singleFileImageStream;
        }

        private IFile[] TranslateContentListToIS(List<ContentP8> filesP8)
        {
            List<IFile> filesIS = new List<IFile>();
            for (int i = 0; i < filesP8.Count; i++)
            {
                FileIS fileIS = new FileIS();
                fileIS.Content = filesP8[i].Content;
                filesIS.Add(fileIS);
            }
            //Move some fields to Document level here?
            return filesIS.ToArray();
        }

        private IIndexCollection TranslatePropertiesToIS(IPropertyCollectionP8 propP8)
        {
            IIndexCollection indexesIS = new IIndexCollection();
            for (int i = 0; i < propP8.Count; i++)
            {
                if (propP8[i].Name != null)
                {
                    //Put in test to see if they are indexes vs other properties?
                    IndexIS idxIS = new IndexIS();
                    idxIS.Name = propP8[i].Name;
                    idxIS.Value = propP8[i].Value;
                    indexesIS.Add(idxIS);
                }
            }
            //Move some fields to Document level here?
            return indexesIS;
        }

        //This ONLY transfers the Doc level properties and Properties collection to IS format
        //It does NOT transfer any file content.
        public IDocument TranslateDocumentToIS(DocumentP8 docP8)
        {
            DocumentIS docIS = new DocumentIS();

            //Document properties have already been retrieved from the Properties[] collection
            // in FileNetServerP8.ImportFromFileNet().  No need to get them from Properties[] again.
            // Just moved them over from the Document level.

            docIS.DocNumber = docP8.DocNumber;
            docIS.DocRemarks = docP8.DocRemarks;
            docIS.DocType = docP8.DocType;
            docIS.EntryDate = docP8.EntryDate;
            docIS.Extension = docP8.Extension;
            docIS.FileName = docP8.FileName;
            docIS.Id = docP8.Id;
            docIS.MimeType = docP8.MimeType;
            docIS.PageCount = docP8.PageCount;
            docIS.PolicyNumber = docP8.PolicyNumber;
            docIS.PolicySymbol = docP8.PolicySymbol;
            docIS.Type = docP8.Type;

            docIS.Indexes = TranslatePropertiesToIS(docP8.Properties);
            return docIS;
        }

        ////Utility functions to translate from P8 to IS
        //private DocumentIS TranslateP8PropsToISDoc(DocumentP8 docP8)
        //{
        //    DocumentIS docIS = new DocumentIS();

        //    //docIS.FileName = docP8.FileName;
        //    //docIS.Extension = docP8.Extension;

        //    foreach (PropertyP8 p in docP8.Properties)
        //    {
        //        switch (p.Name)
        //        {
        //            case "Id":
        //                {
        //                    docIS.Id = p.Value;
        //                    break;
        //                }
        //            case "Filename":    //This will override the docP8.FileName if found.
        //                {
        //                    docIS.FileName = p.Value;
        //                    docIS.Extension = Path.GetExtension(docIS.FileName);
        //                    break;
        //                }
        //            case FileNetPropertyP8.MimeType:
        //                {
        //                    docIS.MimeType = p.Value;
        //                    break;
        //                }
        //            case FileNetPropertyP8.DocNumber:
        //                {
        //                    docIS.DocNumber = p.Value;
        //                    break;
        //                }
        //            case FileNetPropertyP8.DocRemarks:
        //                {
        //                    docIS.DocRemarks = p.Value;
        //                    break;
        //                }
        //            case FileNetPropertyP8.DocType:
        //                {
        //                    docIS.DocType = p.Value;
        //                    break;
        //                }
        //            case FileNetPropertyP8.PolicyNumber:
        //                {
        //                    docIS.PolicyNumber = p.Value;
        //                    break;
        //                }
        //            case FileNetPropertyP8.PolicySymbol:
        //                {
        //                    docIS.PolicySymbol = p.Value;
        //                    break;
        //                }
        //            case FileNetPropertyP8.EntryDate:
        //                {
        //                    DateTime dValue;        //Date comes back from P8 in this format: "WED APR 29 08:00:00 EST 2015".
        //                    if (DateTime.TryParse(p.Value.Substring(4, 6) + ", " + p.Value.Substring(24, 4), out dValue))
        //                    {
        //                        docIS.EntryDate = dValue.ToString("yyyy-MM-dd");
        //                    }
        //                    else
        //                    {
        //                        docIS.EntryDate = p.Value;
        //                    }
        //                    break;
        //                }
        //        }
        //    }
        //    docIS.Indexes = TranslateIndexesToIS(docP8.Properties);
        //    return docIS;
        //}

        public DocumentP8 TranslateDocumentToP8(IDocument docIS)
        {
            DocumentP8 docP8 = new DocumentP8();

            docP8.Company = docIS.Company;
            docP8.DocNumber = docIS.DocNumber;
            docP8.DocRemarks = docIS.DocRemarks;
            docP8.DocType = docIS.DocType;
            docP8.EntryDate = docIS.EntryDate;
            docP8.Extension = docIS.Extension;
            docP8.FileName = docIS.FileName;
            docP8.Id = docIS.Id;
            docP8.MimeType = docIS.MimeType;
            docP8.PageCount = docIS.PageCount;
            docP8.PolicyNumber = docIS.PolicyNumber;
            docP8.PolicySymbol = docIS.PolicySymbol;
            docP8.Type = docIS.Type;
            docP8.ContentList = TranslateFilesToP8(docIS.Files, docIS.FileName, docIS.MimeType);
            docP8.Properties = TranslateIndexesToP8(docIS.Indexes);
            return docP8;
        }

        private IContentCollectionP8 TranslateFilesToP8(IFileCollection filesIS, string fileName, string mimeType)
        {
            IContentCollectionP8 filesP8 = new IContentCollectionP8();
            for (int i = 0; i < filesIS.Count; i++)
            {
                ContentP8 fileP8 = new ContentP8();
                fileP8.FileName = fileName;
                fileP8.MimeType = mimeType;
                fileP8.Content = filesIS[i].Content;
                filesP8.Add(fileP8);
            }
            //Move some fields to Document level here?
            return filesP8;
        }

        private IPropertyCollectionP8 TranslateIndexesToP8(IIndexCollection idxIS)
        {
            IPropertyCollectionP8 indexesP8 = new IPropertyCollectionP8();
            for (int i = 0; i < idxIS.Count; i++)
            {
                if (idxIS[i].Name != null)
                {
                    //Put in test to see if they are indexes vs other properties?
                    PropertyP8 idxP8 = new PropertyP8();
                    idxP8.Name = idxIS[i].Name;
                    idxP8.Value = idxIS[i].Value;
                    indexesP8.Add(idxP8);
                }
            }
            //Move some fields to Document level here?
            return indexesP8;
        }
        #endregion Support methods
    
        #region Unit Test methods
        public List<TestResponse> TestImaging(List<TestResponse> trl, string fileNetReference, string additionalDescription)
        {
            try
            {
                TestResponse oneTest = new TestResponse();

                IFile[] fileIS = new IFile[] { };
                Stream fileImage = new MemoryStream();
                IDocument[] docs = new DocumentIS[] { };
                string[] nothing = new string[] { };
                string mimeType = "";
                Int32 pageCount = 0;

                oneTest = new TestResponse("RetrieveFileIndexes() " + additionalDescription,
                                            "RetrieveFileIndexes('','','" + fileNetReference + "','',MinValue,MaxValue)", 
                                            "IDocument[]");
                try
                {
                    docs = RetrieveFileIndexes(nothing, "", fileNetReference, nothing, DateTime.MinValue, DateTime.MaxValue);
                    if (docs.Length == 0) throw new Exception(String.Format("No documents (indexes) returned for {0} {1}", _company.FileNetVersion, fileNetReference));
                    mimeType = GetIndexByName(docs[0].Indexes, "MimeType");
                    oneTest.ContentType = mimeType;
                    oneTest.IsSuccessful = true;
                    oneTest.Message = "Ok";
                    oneTest.InternalResponse = String.Format("{0} doc(s) with indexes returned.", docs.Length);
                }
                catch (Exception ex)
                {
                    oneTest.Error = ex;
                    oneTest.Message = String.Format("Errors processing {0} {1}.", _company.FileNetVersion, fileNetReference);
                }
                trl.Add(oneTest);
                oneTest = null;

                oneTest = new TestResponse("RetrieveFile() " + additionalDescription, "RetrieveFile('" + fileNetReference + "',0)", "IFile[]");
                try
                {
                    fileIS = RetrieveFile(fileNetReference, 0);
                    oneTest.ResponseStream = fileIS[0].Content;
                    oneTest.ContentType = mimeType;
                    oneTest.IsSuccessful = true;
                    oneTest.Message = "Ok";
                    oneTest.InternalResponse = String.Format("Length of stream returned: {0}.", fileIS[0].Content.Length);
                }
                catch (Exception ex)
                {
                    oneTest.Error = ex;
                    oneTest.Message = String.Format("Errors processing {0} {1}.", _company.FileNetVersion, fileNetReference);
                }
                trl.Add(oneTest);
                oneTest = null;

                oneTest = new TestResponse("RetrieveFileImage() " + additionalDescription, "RetrieveFileImage('" + fileNetReference + "')", "Stream");
                try
                {
                    fileImage = RetrieveFileImage(fileNetReference);
                    if (fileImage == null || fileImage.Length == 0)
                    {
                        throw new Exception("No image returned from RetrieveFileImage().");
                    }
                    oneTest.ResponseStream = fileImage;
                    oneTest.ContentType = mimeType;
                    oneTest.IsSuccessful = true;
                    oneTest.Message = "Ok";
                    oneTest.InternalResponse = String.Format("Length of stream returned: {0}.", fileImage.Length);
                }
                catch (Exception ex)
                {
                    oneTest.Error = ex;
                    oneTest.Message = String.Format("Errors processing {0} {1}.", _company.FileNetVersion, fileNetReference);
                }
                trl.Add(oneTest);
                oneTest = null;

                oneTest = new TestResponse("RetrievePageCount() " + additionalDescription, "RetrieveDocumentIndexed('" + fileNetReference + "')", "IDocument");
                try
                {
                    pageCount = RetrievePageCount(fileNetReference);
                    if (pageCount == 0)
                    {
                        throw new Exception("pageCount of 0 returned from RetrievePageCount().");
                    }
                    oneTest.ResponseStream = null;
                    oneTest.ContentType = null;
                    oneTest.IsSuccessful = true;
                    oneTest.Message = "Ok";
                    oneTest.InternalResponse = String.Format("pageCount: {0}.", pageCount);
                }
                catch (Exception ex)
                {
                    oneTest.Error = ex;
                    oneTest.Message = String.Format("Errors processing {0} {1}.", _company.FileNetVersion, fileNetReference);
                }
                trl.Add(oneTest);
                oneTest = null;

                string newValue = Guid.NewGuid().ToString();
                oneTest = new TestResponse(String.Format("UpdateFileImage('{0}','{1}') {2}", fileNetReference, newValue, additionalDescription), String.Format("UpdateFileImage('{0}','{1}')", fileNetReference, newValue), "void");
                try
                {
                    UpdateFileImage(fileNetReference, newValue);
                    docs = RetrieveFileIndexes(nothing, "", fileNetReference, nothing, DateTime.MinValue, DateTime.MaxValue);
                    if (docs != null)
                    {
                        string existingValue = GetIndexByName(docs[0].Indexes, "DocRemarks");
                        if (existingValue == newValue)
                        {
                            oneTest.IsSuccessful = true;
                            oneTest.InternalResponse = String.Format("docRemarks property successfully updated and verified as {0}.", existingValue);
                        }
                        else
                        {
                            oneTest.IsSuccessful = false;
                            oneTest.InternalResponse = String.Format("Update failed: FN value {0} was not updated with {1}", existingValue, newValue);
                        }
                    }
                    oneTest.ResponseStream = null;
                    oneTest.ContentType = null;
                    oneTest.Message = "Ok";
                }
                catch (Exception ex)
                {
                    oneTest.Error = ex;
                    oneTest.Message = String.Format("Errors processing {0} {1}.", _company.FileNetVersion, fileNetReference);
                }
                trl.Add(oneTest);
                oneTest = null;
            }
            catch (Exception)
            {
                //ignore an carry on
            }
            return trl;
        }

        private string GetIndexByName(IIndexCollection indexes, string paramName)
        {
            //NOTE: Finds ONLY the FIRST one.  (Sometimes multiple TIFF files have multiple filename entries.)
            string value = null;   //In case it can't find an index with this name
            foreach (IIndex idx in indexes)
            {
                if (idx.Name!=null && idx.Name.ToLower() == paramName.ToLower())
                {
                    value = idx.Value;
                    break;
                }
            }
            //Move some fields to Document level here?
            return value;
        }

        public string GetMimeType(string fileNetReference)
        {
            string[] nothing = new string[] { };
            IDocument[] docs = RetrieveFileIndexes(nothing, "", fileNetReference, nothing, DateTime.MinValue, DateTime.MaxValue);
            if (docs.Length > 0)
            {
                return GetIndexByName(docs[0].Indexes, "MimeType");
            }
            else
            {
                return "";
            }
        }

        public Stream GetStream(string fileNetReference, int which)
        {
            switch (which)
            {
                case (2):
                    {
                        IFile[] fileIS = RetrieveFile(fileNetReference, 0);
                        return fileIS[0].Content;
                    }
                default:
                    {
                        Stream fileImage = RetrieveFileImage(fileNetReference);
                        return fileImage;
                    }
            }
        }

        private void LogException(string description, Exception thisEx)
        {
            try
            {
                string filePath = ConfigurationManager.AppSettings["TempDocPath"];
                string fileOut = filePath + "ARMS.log";

                using (StreamWriter writer = new StreamWriter(fileOut, true))
                {
                    writer.Write(DateTime.Now);
                    writer.WriteLine("");
                    writer.WriteLine("Description: " + description);
                    while (thisEx != null)
                    {
                        writer.WriteLine(thisEx.Message);
                        thisEx = thisEx.InnerException;
                    }

                    writer.WriteLine("");
                    writer.WriteLine("=======================================================================================================================");
                    writer.WriteLine("");
                }
            }
            catch (Exception)
            {
                //Absorb any errors that occur here.
            }
        }

        #endregion Unit Test methods
    }

    //WA-3060 Doug Bruce 2015-10-06 - ARMS needs both DocClass and CompanyNo to find docs.  User/PW are the same (so far).
    public class DocClassCredentials
    {
        public string FileNetDocClassP8 { get; set; }
        public string FileNetCompanyNo { get; set; }
    }

    //Needed to create these so new objects can be instantiated from them (can't do that with abstract classes or interfaces).
    class DocumentIS : IDocument
    {
        public IFileCollection Files { get; set; }
        public IIndexCollection Indexes { get; set; }
        public string Id { get; set; }
        public Company Company { get; set; }
        public DocumentType Type { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public string Extension { get; set; }
        public string DocNumber { get; set; }
        public string PolicyNumber { get; set; }
        public string PolicySymbol { get; set; }
        public string DocType { get; set; }
        public string EntryDate { get; set; }
        public string DocRemarks { get; set; }
        public int PageCount { get; set; }
    }

    class IndexIS : IIndex
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    class FileIS : IFile
    {
        public System.IO.Stream Content { get; set; }
    }
}
