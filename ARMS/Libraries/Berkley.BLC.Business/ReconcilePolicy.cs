using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
	public class ReconcilePolicy : IReconcile<Policy>
	{
		private ReconcileActor<Policy> moActor;
		private BlcInsured moBlcInsured;
		private List<Policy> moNewPolicyList = new List<Policy>();
		private List<Policy> moUpdatePolicyList = new List<Policy>();
		private List<Policy> moInactivePolicyList = new List<Policy>();
		private List<Claim> moNewClaimList = new List<Claim>();
        private List<RateModificationFactor> moNewRateModFactorList = new List<RateModificationFactor>();
		private Dictionary<string, Policy> moExistingPolicyTable = new Dictionary<string, Policy>();

		// Deletes
		private List<Policy> moPolicyDeletes = new List<Policy>();
		private List<Claim> moClaimDeletes = new List<Claim>();
        private List<RateModificationFactor> moRateModFactorDeletes = new List<RateModificationFactor>();

		public ReconcilePolicy(BlcInsured oBlcInsured, Transaction oTransaction)
		{
			moBlcInsured = oBlcInsured;
			moActor = new ReconcileActor<Policy>(oTransaction);
		}

		public Policy[] NewPolicies
		{
			get { return moNewPolicyList.ToArray(); }
		}

		public Policy GetNew(string[] rstrForeignKey)
		{
			ReconcilePolicyKey oKey = new ReconcilePolicyKey(rstrForeignKey);
			foreach (Policy oPolicy in moNewPolicyList)
			{
				if (oPolicy.Number == oKey.Number && Convert.ToString(oPolicy.Mod) == oKey.Mod)
				{
					return oPolicy;
				}
				//else if (oPolicy.Number == oKey.Number && oPolicy.Mod == int.MinValue && oKey.Mod.Length == 0)
				//{
				//	return oPolicy;
				//}
			}
			return null;
		}

		/// <summary>
		/// Returns either a new or existing policy
		/// </summary>
		/// <returns></returns>
		public Policy GetPolicy(string[] rstrForeignKey)
		{
			Policy oReturn = GetNew(rstrForeignKey);
			if (oReturn == null)
			{
				oReturn = GetExisting(rstrForeignKey);
			}
			if (oReturn == null)
			{
				throw new Exception(String.Format("Requested Policy number {0} version {1} could not be found.", rstrForeignKey[0], rstrForeignKey[1]));
			}
			return oReturn;
		}

		#region IReconcile<Policy> Members
		public void Clear()
		{
			moNewPolicyList.Clear();
			moUpdatePolicyList.Clear();
			moNewClaimList.Clear();
            moNewRateModFactorList.Clear();
			moExistingPolicyTable.Clear();
		}

		public void Reconcile()
		{
			moActor.Reconcile();
		}

		public Policy GetExisting(string[] rstrForeignKey)
		{
			// Cache the policies for the Coverage
			Policy oExistingInsured = null;
			ReconcilePolicyKey oKey = new ReconcilePolicyKey(rstrForeignKey);

			if (moExistingPolicyTable.ContainsKey(oKey.ToString()))
			{
				return moExistingPolicyTable[oKey.ToString()];
			}
			else
			{
				// SPLIT the Pipe Delimited string here
				if (oKey.Mod != null && oKey.Mod.Length > 0)
				{
					oExistingInsured = Policy.GetOne("Number = ? AND Mod = ?", oKey.Number, oKey.Mod);
				}
				else
				{
					//MAE oExistingInsured = Policy.GetOne("Number = ?", oKey.Number);
				}
				if (oExistingInsured != null)
				{
					moExistingPolicyTable.Add(oKey.ToString(), oExistingInsured);
				}
			}
			return oExistingInsured;
		}

		public Policy Entity
		{
			get { return moActor.Entity; }
		}

		public Policy Update(Policy oNewEntity, Policy oExistingEntity)
		{
			// No foreign key for the claim; remove the existing
			Claim[] roClaims = Claim.GetArray("PolicyID = ?", oExistingEntity.ID);
			foreach (Claim oClaim in roClaims)
			{
				moClaimDeletes.Add(oClaim);
			}

            // No foreign key for the rate mod factors; remove the existing
            RateModificationFactor[] roFactors = RateModificationFactor.GetArray("PolicyID = ?", oExistingEntity.ID);
            foreach (RateModificationFactor oFactor in roFactors)
            {
                moRateModFactorDeletes.Add(oFactor);
            }

			if (oExistingEntity.IsReadOnly)
			{
				oExistingEntity = oExistingEntity.GetWritableInstance();
			}
			if (oNewEntity.IsReadOnly)
			{
				oNewEntity = oNewEntity.GetWritableInstance();
			}

			oNewEntity.InsuredID = moBlcInsured.InsuredEntity.ID;
			oExistingEntity = oExistingEntity.Assign(oNewEntity);
			moUpdatePolicyList.Add(oExistingEntity);

			// Add the claims for this policy
			foreach (BlcClaim oBlcClaim in moBlcInsured.ClaimList)
			{
				if (oBlcClaim.PolicyNumber == oExistingEntity.Number /* && oBlcClaim.PolicyMod == oExistingEntity.Mod*/)
				{
					oBlcClaim.Entity.PolicyID = oExistingEntity.ID;
					moNewClaimList.Add(oBlcClaim.Entity);
				}
			}

            // Add the rate mod factors for this policy
            foreach (BlcRateModFactor oBlcRateModFactor in moBlcInsured.RateModFactorList)
            {
                if (oBlcRateModFactor.PolicyNumber == oExistingEntity.Number /* && oBlcClaim.PolicyMod == oExistingEntity.Mod*/)
                {
                    oBlcRateModFactor.Entity.PolicyID = oExistingEntity.ID;
                    moNewRateModFactorList.Add(oBlcRateModFactor.Entity);
                }
            }
			return oExistingEntity;
		}

		public Policy Insert(Policy oNewEntity)
		{
			if (oNewEntity.IsReadOnly)
			{
				oNewEntity = oNewEntity.GetWritableInstance();
			}
			oNewEntity.InsuredID = moBlcInsured.InsuredEntity.ID;
			moNewPolicyList.Add(oNewEntity);

			// Add the claims for this policy
			foreach (BlcClaim oBlcClaim in moBlcInsured.ClaimList)
			{
				if (oBlcClaim.PolicyNumber == oNewEntity.Number /* && oBlcClaim.PolicyMod == oNewEntity.Mod*/)
				{
					oBlcClaim.Entity.PolicyID = oNewEntity.ID;
					moNewClaimList.Add(oBlcClaim.Entity);
				}
			}

            // Add the rate mod factors for this policy
            foreach (BlcRateModFactor oBlcRateModFactor in moBlcInsured.RateModFactorList)
            {
                if (oBlcRateModFactor.PolicyNumber == oNewEntity.Number /* && oBlcClaim.PolicyMod == oExistingEntity.Mod*/)
                {
                    oBlcRateModFactor.Entity.PolicyID = oNewEntity.ID;
                    moNewRateModFactorList.Add(oBlcRateModFactor.Entity);
                }
            }
			return oNewEntity;
		}

		public void Delete(Policy oEntity)
		{
			moPolicyDeletes.Add(oEntity);

			Claim[] roClaims = Claim.GetArray("PolicyID = ?", oEntity.ID);
			foreach (Claim oClaim in roClaims)
			{
				moClaimDeletes.Add(oClaim);
			}

            RateModificationFactor[] roFactors = RateModificationFactor.GetArray("PolicyID = ?", oEntity.ID);
            foreach (RateModificationFactor oFactor in roFactors)
            {
                moRateModFactorDeletes.Add(oFactor);
            }
		}

		public bool Equal(Policy oLhs, Policy oRhs)
		{
			return oLhs.Number == oRhs.Number && oLhs.Mod == oRhs.Mod;
			// Use if changed to match the policy by the expire date
			//return BlcPolicy.Equals(oLhs, oRhs);
		}

		public ReconcileActor<Policy> Actor
		{
			get { return moActor; }
		}

		public void DeleteQueued()
		{
			foreach (Claim oClaim in moClaimDeletes)
			{
				oClaim.Delete(moActor.DbTransaction);
			}

            foreach (RateModificationFactor oFactor in moRateModFactorDeletes)
            {
                oFactor.Delete(moActor.DbTransaction);
            }

			// A PolicyID foreign key now exists in the Coverages.  The commented code below would need to take this into consideration in order to be functional.
			// Do not delete the Policy
			#region Delete Code
			/*
			SurveyLocationPolicyCoverageName[] roSLPCN;
			SurveyLocationPolicyExclusion[] roSPE;
			SurveyLocationPolicyOptionalReport[] roSLPOR;

			foreach (Policy oEntity in moPolicyDeletes)
			{
				roSLPCN = SurveyLocationPolicyCoverageName.GetArray("PolicyID = ?", oEntity.ID);
				roSPE = SurveyLocationPolicyExclusion.GetArray("PolicyID = ?", oEntity.ID);
				roSLPOR = SurveyLocationPolicyOptionalReport.GetArray("PolicyID = ?", oEntity.ID);;

				if(roSLPCN != null)
				{
					foreach(SurveyLocationPolicyCoverageName oSlpcEntity in roSLPCN)
					{
						oSlpcEntity.Delete(moActor.DbTransaction);
					}
				}

				if (roSPE != null)
				{
					foreach (SurveyLocationPolicyExclusion oSpe in roSPE)
					{
						oSpe.Delete(moActor.DbTransaction);
					}
				}

				if (roSLPOR != null)
				{
					foreach (SurveyLocationPolicyOptionalReport oSlpor in roSLPOR)
					{
						oSlpor.Delete(moActor.DbTransaction);
					}
				}
				oEntity.Delete(moActor.DbTransaction);
			}
			 * */
			#endregion
		}
		public void AddQueued()
		{
			Claim oClaimToInsert = null;
            RateModificationFactor oFactorToInsert = null;

			foreach (Policy oPolicy in moNewPolicyList)
			{
				oPolicy.IsActive = true;
				oPolicy.Save(moActor.DbTransaction);
			}
			foreach (Claim oClaim in moNewClaimList)
			{
				if (oClaim.IsReadOnly)
				{
					oClaimToInsert = oClaim.GetWritableInstance();
				}
				else
				{
					oClaimToInsert = oClaim;
				}
				oClaimToInsert.Save(moActor.DbTransaction);
			}
            foreach (RateModificationFactor oFactor in moNewRateModFactorList)
            {
                if (oFactor.IsReadOnly)
                {
                    oFactorToInsert = oFactor.GetWritableInstance();
                }
                else
                {
                    oFactorToInsert = oFactor;
                }
                oFactorToInsert.Save(moActor.DbTransaction);
            }
		}

		public void UpdateQueued()
		{
			// Not needed - unfinished work that changes the migrated policy mod numbers of zero using a best guess
			//BlcPolicyList oBlcPolicyList = new BlcPolicyList(moUpdatePolicyList, moActor.DbTransaction);
			//oBlcPolicyList.FixModDates();
			foreach (Policy oPolicy in moUpdatePolicyList)
			{
				oPolicy.IsActive = true;
				oPolicy.Save(moActor.DbTransaction);
			}
		}
		#endregion
	}
}
