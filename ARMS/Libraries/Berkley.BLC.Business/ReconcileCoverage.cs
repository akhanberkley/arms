using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Reflection;
using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
	public class ReconcileCoverage 
    {
        // The Hybrid Approach tried to only update, delete, and add the Coverages as appropriate
		// The problem was that some (as it turns out most) coverages share their LOB, Sub-LOB, and Coverage Type Name
		// and therefore need to be deleted and re-added.  It turns out that it is faster to 
		// do a delete by InsuredId and then re-add all of the coverages.
		private Transaction moTransaction;
        private Company moCompany;
		private BlcInsured moBlcInsured;
		private CoverageNameTable moCoverageNameTable;
        private List<Guid> policiesToDelete = new List<Guid>();

		private List<Coverage> moAddList = new List<Coverage>();
        private List<CoverageDetail> moCovDetailList = new List<CoverageDetail>();
		#region Hybrid Approach
		/*
		private Dictionary<int, BlcCoverage> moAddBlcCoverages = new Dictionary<int, BlcCoverage>();
		private Dictionary<Guid, VWCoverage> moDeleteList = new Dictionary<Guid, VWCoverage>();
		private List<Coverage> moUpdateList = new List<Coverage>();
		private BlcCoverageList<VWCoverage> moFoundBlcCoverageList = new BlcCoverageList<VWCoverage>();
		*/
		#endregion

		public ReconcileCoverage(Transaction oTransaction, Company oCompany, BlcInsured oBlcInsured)
        {
			moCoverageNameTable = new CoverageNameTable();
			moTransaction = oTransaction;
            moCompany = oCompany;
			moBlcInsured = oBlcInsured;
		}

		public void Clear()
		{
			if (moCoverageNameTable != null)
			{
				moCoverageNameTable.Clear();
			}
		}

		#region Hybrid Approach
		/*
		private bool RemoveCoveragesInTheSameGroup(CoverageKey oCoverageKey, VWCoverage oVwCoverage)
		{
			bool fReturn = false;
			List<VWCoverage> oExistingList;

			moFoundBlcCoverageList.Add(oCoverageKey, oVwCoverage);
			oExistingList = moFoundBlcCoverageList.Get(oCoverageKey);
			if (oExistingList.Count > 1)
			{
				foreach(VWCoverage oCoverageToDelete in moFoundBlcCoverageList)
				{
					moDeleteList[oCoverageToDelete.CoverageID] = oCoverageToDelete;
					fReturn = true;
				}
			}
			return fReturn;
		}
		 */
		#endregion

        public void Reconcile(BlcCoverageList<BlcCoverage> oRhsBlcCoverageList, Hashtable existingPolicies, ReconcilePolicy oReconcilePolicy)
        {
			Guid oInsuredId = moBlcInsured.InsuredEntity.ID;
			moCoverageNameTable.Load(moCompany);
			bool fFound;
			List<BlcCoverage> oFoundList = new List<BlcCoverage>();
			List<BlcCoverage> oStartList = new List<BlcCoverage>();
			BlcCoverage oBlcCoverage;
			VWCoverage[] roCoverages = VWCoverage.GetArray("InsuredID = ?", oInsuredId);

			// Convert the Coverage View rows (read only) into BlcCoverage so we can Trim()
			foreach(VWCoverage oCoverage in roCoverages)
			{
				oBlcCoverage = new BlcCoverage();
				oBlcCoverage.CoverageNameType = oCoverage.CoverageNameTypeCode == null? String.Empty : oCoverage.CoverageNameTypeCode.Trim();
				oBlcCoverage.CoverageValueType = oCoverage.CoverageTypeCode == null ? String.Empty : oCoverage.CoverageTypeCode.Trim();
				oBlcCoverage.LineOfBusinessAsString = oCoverage.LineOfBusinessCode == null ? String.Empty : oCoverage.LineOfBusinessCode.Trim();
				oBlcCoverage.SubLineOfBusinessAsString = oCoverage.SubLineOfBusinessCode == null ? String.Empty : oCoverage.SubLineOfBusinessCode.Trim();
				oBlcCoverage.PolicyNumber = oCoverage.PolicyNumber;
				oBlcCoverage.PolicyMod = Convert.ToString(oCoverage.PolicyMod);
				oBlcCoverage.Value = oCoverage.CoverageValue == null ? String.Empty : oCoverage.CoverageValue.Trim();
				oStartList.Add(oBlcCoverage);
			}
			// If we have the same number of rows coming in as we do in the DB
			// and all of criteria are the same, skip touching the DB
			foreach (BlcCoverage oRhsBlcCoverage in oRhsBlcCoverageList)
			{
				fFound = false;
				foreach (BlcCoverage oIteratedCoverage in oStartList.ToArray())
				{
					if (oRhsBlcCoverage.CoverageNameType == oIteratedCoverage.CoverageNameType)
					{
						if (oRhsBlcCoverage.CoverageValueType == oIteratedCoverage.CoverageValueType)
						{
							if (oRhsBlcCoverage.LineOfBusinessAsString == oIteratedCoverage.LineOfBusinessAsString)
							{
								if (oRhsBlcCoverage.SubLineOfBusinessAsString == oIteratedCoverage.SubLineOfBusinessAsString)
								{
									if (oRhsBlcCoverage.PolicyNumber == oIteratedCoverage.PolicyNumber)
									{
										if (oRhsBlcCoverage.PolicyMod == oIteratedCoverage.PolicyMod)
										{
											if (oRhsBlcCoverage.Value == oIteratedCoverage.Value)
											{
												// Remove the item from the list of BlcCoverage that we started with
												oStartList.Remove(oIteratedCoverage);
												// The found list should end up with all of the BlcCoverage converted from the VWCoverage DB items
												oFoundList.Add(oRhsBlcCoverage);
												fFound = true;
												break;
											}
										}
									}
								}
							}
						}
					}
				}
				if (!fFound)
				{
					break;
				}
			}

			#region Hybrid Approach
			/*
			List<BlcCoverage> oBlcCoverages;
			VWCoverage[] roCoverages = VWCoverage.GetArray("InsuredID = ?", oInsuredId);
			CoverageKey oCoverageKey;
			bool fRemoveExisting;

			if (roCoverages.Length > 0)
			{
				foreach (VWCoverage oVwCoverage in roCoverages)
				{
					oCoverageKey = new CoverageKey(oVwCoverage);

					// If we end up with more than one entry in a coverage "group", we need to delete and insert
					fRemoveExisting = RemoveCoveragesInTheSameGroup(oCoverageKey, oVwCoverage);

					// If the count if different, clear these entries out and start over
					// This is critical as the LOB, Sub-LOB, ValueType, and NameType can be reused.
					oBlcCoverages = oRhsBlcCoverageList.Get(oCoverageKey);
					if (oBlcCoverages != null)
					{
						if(fRemoveExisting)
						{
							// The old have been removed, add the new for sure
							foreach (BlcCoverage oBlcCoverage in oBlcCoverages)
							{
								// Only add it once - could duplicate if CoverageKey.Key is duplicated in DB Coverage table which can happen
								moAddBlcCoverages[oBlcCoverage.GetHashCode()] = oBlcCoverage; 
							}
						}
						else if(oBlcCoverages.Count > 1)
						{
							foreach (BlcCoverage oBlcCoverage in oBlcCoverages)
							{
								// Only add it once - could duplicate if CoverageKey.Key is duplicated in DB Coverage table which can happen
								moAddBlcCoverages[oBlcCoverage.GetHashCode()] = oBlcCoverage; 
							}
							moDeleteList[oVwCoverage.CoverageID] = oVwCoverage;
						}
						else if(oBlcCoverages.Count == 1)
						{
							if (oBlcCoverages[0].Value != oVwCoverage.CoverageValue)
							{
								moUpdateList.Add(oBlcCoverages[0].CreateCoverage(oVwCoverage.CoverageID, oInsuredId, oVwCoverage.CoverageNameID));
							}
						}
						else
						{
							moDeleteList[oVwCoverage.CoverageID] = oVwCoverage;
						}
					}
					else
					{
						moDeleteList[oVwCoverage.CoverageID] = oVwCoverage;
					}
				}
			}
			else
			*/
			#endregion

            // Accounts for missing, new and different items
            // If all of the items are not the same, nuke the DB and add the new items
            if (oStartList.Count != 0 || oFoundList.Count != oRhsBlcCoverageList.Count)
            {
                // Add them all
                foreach (BlcCoverage oInboundCoverage in oRhsBlcCoverageList)
                {
                    Guid policyID;
                    string key = string.Format("{0}-{1}", oInboundCoverage.PolicyNumber, oInboundCoverage.PolicyMod);

                    if (existingPolicies.Contains(key)) //existing
                    {
                        policyID = new Guid(existingPolicies[key].ToString());
                    }
                    else //new
                    {
                        ReconcilePolicyKey oPolicyKey = new ReconcilePolicyKey(oInboundCoverage.PolicyNumber, oInboundCoverage.PolicyMod);
                        policyID = oReconcilePolicy.GetPolicy(oPolicyKey.ToArray()).ID;
                    }

                    //determine which policiy ids for coverages that will need to be deleted so they can be added back
                    if (!policiesToDelete.Contains(policyID))
                    {
                        policiesToDelete.Add(policyID);
                    }

                    Coverage cov = oInboundCoverage.CreateCoverage(Guid.NewGuid(), oInsuredId, moCoverageNameTable.GetId(oInboundCoverage), policyID);
                    moAddList.Add(cov);

                    //Coverage Details
                    if (oInboundCoverage.CoverageDetails != null && oInboundCoverage.CoverageDetails.Count > 0)
                    {
                        moCovDetailList.AddRange(oInboundCoverage.CreateCoverageDetail(cov.ID, cov.InsuredID, cov.PolicyID, oInboundCoverage.CoverageDetails));
                    }
                }
            }
        }

        public void DeleteQueued()
        {
            try
            {
                foreach(Guid policyID in policiesToDelete.ToArray())
                {
                    // It's a lot faster to delete all of the rows at once
                    DB.Engine.ExecuteDelete(typeof(CoverageDetail), string.Format("InsuredId = '{0}' AND PolicyID = '{1}'", moBlcInsured.InsuredEntity.ID, policyID));
                    DB.Engine.ExecuteDelete(typeof(Coverage), string.Format("InsuredId = '{0}' AND PolicyID = '{1}'", moBlcInsured.InsuredEntity.ID, policyID));
                }
            }
            catch (Exception oException)
            {
                throw new Exception("Exception during Coverage delete: " + oException.Message);
            }

            #region Hybrid Approach
            /*
            Coverage oCoverage;
            foreach (VWCoverage oEntity in moDeleteList.Values)
            {
                oCoverage = new Coverage(oEntity.CoverageID);
                DB.Engine.StartTracking(oCoverage, InitialState.Updated);
                oCoverage.Delete(moTransaction);
            }
            */
            #endregion
        }

        public void AddQueued()
        {
			Coverage oNewCoverage;
			foreach (Coverage oEntity in moAddList)
			{
				if (oEntity.IsReadOnly)
				{
					oNewCoverage = oEntity.GetWritableInstance();
				}
				else
				{
					oNewCoverage = oEntity;
				}
				oNewCoverage.Save(moTransaction);
			}

            if (moCovDetailList != null && moCovDetailList.Count > 0)
            {
                CoverageDetail oNewCovDetail;
                foreach (CoverageDetail oDEntity in moCovDetailList)
                {
                    if (oDEntity.IsReadOnly)
                    {
                        oNewCovDetail = oDEntity.GetWritableInstance();
                    }
                    else
                    {
                        oNewCovDetail = oDEntity;
                    }
                    oNewCovDetail.Save(moTransaction);
                }
            }
			#region Hybrid Approach
			/*
			foreach(BlcCoverage oBlcCoverage in moAddBlcCoverages.Values)
			{
				oNewCoverage = oBlcCoverage.CreateCoverage(Guid.NewGuid(), moBlcInsured.InsuredEntity.ID, moCoverageNameTable.GetId(oBlcCoverage));
				if (oNewCoverage.IsReadOnly)
				{
					oNewCoverage = oNewCoverage.GetWritableInstance();
				}
				oNewCoverage.Save(moTransaction);
			}

			foreach (Coverage oCoverage in moUpdateList)
			{
				DB.Engine.StartTracking(oCoverage, InitialState.Updated);
				oCoverage.Save(moTransaction);
			}
			*/
			#endregion
		}

	}
}
