using System;
using System.Collections;
using System.IO;
using Wilson.ORMapper;
using System.Data.SqlTypes;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Berkley.BLC.Entities;

namespace Berkley.BLC.Business
{
    public abstract class SurveyManager
    {
        private const long DatabaseTimeResolution = 5 * TimeSpan.TicksPerMillisecond;
        private static DateTime _lastHistoryEntryTime = DateTime.MinValue; // used to prevent dup time entries in history
        protected static Random _randomNumbers = new Random((int)DateTime.Now.Ticks); // used in user/territory selection

        private Company _company;
        private Survey _survey;
        private User _currentUser;
        private bool _surveyOwnedByUser;
        private bool _surveyOwnedByVendor;
        private bool _override;

        #region workflow constants

        /// <summary>
        /// The note text provided to the workflow system for new surveys.
        /// </summary>
        protected const string NewSurveyNoteText = "Survey request created in ARMS.";
        /// <summary>
        /// The note text provided to the workflow system for reopened surveys.
        /// </summary>
        protected const string ReOpenedSurveyNoteText = "Completed survey request reopened in ARMS.";
        /// <summary>
        /// The note text provided to the workflow system for canceled surveys that are reopened.
        /// </summary>
        protected const string ReOpenedCanceledSurveyNoteText = "Canceled survey request reopened in ARMS.";
        /// <summary>
        /// The note text provided to the workflow system for completed surveys.
        /// </summary>
        protected const string ClosedNewSurveyNoteText = "Survey request closed in ARMS.";
        /// <summary>
        /// The note text provided to the workflow system for completed and closed surveys.
        /// </summary>
        protected const string CompletedClosedNewSurveyNoteText = "Survey request completed and closed in ARMS.";
        /// <summary>
        /// The note text provided to the workflow system for completed surveys.
        /// </summary>
        protected const string CompletedNewSurveyNoteText = "Survey request completed in ARMS.";
        /// <summary>
        /// The note text provided to the workflow system for completed surveys.
        /// </summary>
        protected const string CompletedClosedNewSurveyAndNotifyNoteText = "Survey request completed and closed in ARMS. ARMS user {0} states the following: {1}";
        /// <summary>
        /// The note text provided to the workflow system for completed surveys.
        /// </summary>
        protected const string CompletedNewSurveyAndNotifyNoteText = "Survey request completed in ARMS. ARMS user {0} states the following: {1}";
        /// <summary>
        /// The note text provided to the workflow system for completed surveys.
        /// </summary>
        protected const string ClosedNewSurveyAndNotifyNoteText = "Survey request closed in ARMS. ARMS user {0} states the following: {1}";
        /// <summary>
        /// The note text provided to the workflow system for canceled surveys.
        /// </summary>
        protected const string CanceledSurveyNoteText = "Survey request canceled in ARMS. Reason for cancellation provided by {0}: {1}";

        #endregion

        /// <summary>
        /// Builds and returns a company-specific SurveyManager instance that can be used to preform
        /// survey management functions on behalf of a user.
        /// </summary>
        /// <param name="survey">Survey to be used/modified by this new instance.</param>
        /// <param name="currentUser">User to record as the current user for actions taken on the Survey. Use 'null' to indicate the system is taking action.</param>
        /// <returns>Company-specific instance of SurveyManager.</returns>
        public static SurveyManager GetInstance(Survey survey, User currentUser)
        {
            switch (survey.Company.Abbreviation)
            {
                case "BARS":
                    return new CompanySpecific.SurveyManagerBARS(survey, currentUser, false);
                case "BMG":
                    return new CompanySpecific.SurveyManagerBMAG(survey, currentUser, false);
                case "AIC":
                    return new CompanySpecific.SurveyManagerAIC(survey, currentUser, false);
                case "CWG":
                    return new CompanySpecific.SurveyManagerCWG(survey, currentUser, false);
                case "USG":
                    return new CompanySpecific.SurveyManagerUSIG(survey, currentUser, false);
                case "CCI":
                    return new CompanySpecific.SurveyManagerCCIC(survey, currentUser, false);
                case "BNPG":
                    return new CompanySpecific.SurveyManagerBNPG(survey, currentUser, false);
                case "BLS":
                    return new CompanySpecific.SurveyManagerBLS(survey, currentUser, false);
                case "BOG":
                    return new CompanySpecific.SurveyManagerBOG(survey, currentUser, false);
                default:
                    throw new NotSupportedException("Company '" + survey.Company.Abbreviation + "' is not supported.");
            }
        }

        /// <summary>
        /// Builds and returns a company-specific SurveyManager instance that can be used to preform
        /// survey management functions on behalf of a user.
        /// </summary>
        /// <param name="survey">Survey to be used/modified by this new instance.</param>
        /// <param name="currentUser">User to record as the current user for actions taken on the Survey. Use 'null' to indicate the system is taking action.</param>
        /// <param name="overridePermissions">Flag to override permission checks for this user.</param>
        /// <returns>Company-specific instance of SurveyManager.</returns>
        public static SurveyManager GetInstance(Survey survey, User currentUser, bool overridePermissions)
        {
            switch (survey.Company.Abbreviation)
            {
                case "BARS":
                    return new CompanySpecific.SurveyManagerBARS(survey, currentUser, overridePermissions);
                case "BMG":
                    return new CompanySpecific.SurveyManagerBMAG(survey, currentUser, overridePermissions);
                case "AIC":
                    return new CompanySpecific.SurveyManagerAIC(survey, currentUser, overridePermissions);
                case "CWG":
                    return new CompanySpecific.SurveyManagerCWG(survey, currentUser, overridePermissions);
                case "USG":
                    return new CompanySpecific.SurveyManagerUSIG(survey, currentUser, overridePermissions);
                case "CCI":
                    return new CompanySpecific.SurveyManagerCCIC(survey, currentUser, overridePermissions);
                case "BNPG":
                    return new CompanySpecific.SurveyManagerBNPG(survey, currentUser, overridePermissions);
                case "BLS":
                    return new CompanySpecific.SurveyManagerBLS(survey, currentUser, overridePermissions);
                case "BOG":
                    return new CompanySpecific.SurveyManagerBOG(survey, currentUser, overridePermissions);
                default:
                    throw new NotSupportedException("Company '" + survey.Company.Abbreviation + "' is not supported.");
            }
        }

        /// <summary>
        /// Create a new instance of this class to manage the specified Survey.
        /// </summary>
        /// <param name="survey">Survey to be used/modified by this new instance.</param>
        /// <param name="currentUser">User to record as the current user for actions taken on the Survey. Use 'null' to indicate the system is taking action.</param>
        /// <param name="overridePermissions">Flag to override permission checks for this user.</param>
        protected SurveyManager(Survey survey, User currentUser, bool overridePermissions)
        {
            if (survey == null) throw new ArgumentNullException("survey");

            _company = survey.Company;
            _currentUser = currentUser;
            _override = overridePermissions;

            this.Survey = survey;
        }

        /// <summary>
        /// Returns the workflow helper for the company.
        /// </summary>
        protected abstract WorkflowHelper Workflow { get; }

        /// <summary>
        /// Gets the company being managed by this class.
        /// </summary>
        public Company Company
        {
            get { return _company; }
        }
        /// <summary>
        /// Gets the user for which this manager will preforms actions.
        /// Null indicates no current users and the 'system' is assumed to be preforming actions.
        /// </summary>
        public User CurrentUser
        {
            get { return _currentUser; }
        }

        /// <summary>
        /// Gets or sets the current instance of the survey being managed by this class.
        /// NOTE: This instance is updated each time a change is made using the methods in this class.
        /// </summary>
        public Survey Survey
        {
            get { return _survey; }
            set
            {
                if (value == null) throw new ArgumentNullException("value");

                _survey = value;
                _surveyOwnedByUser = (_currentUser != null && _currentUser.ID == value.AssignedUserID);
                _surveyOwnedByVendor = (value.AssignedUser != null && value.AssignedUser.IsFeeCompany);
            }
        }

        /// <summary>
        /// Gets of flag value that indicates if the survey is owned by the current users.
        /// </summary>
        protected bool SurveyOwnedByUser
        {
            get { return _surveyOwnedByUser; }
        }

        /// <summary>
        /// Gets a of flag value that indicates if the survey owner is a fee company.
        /// </summary>
        protected bool SurveyOwnedByVendor
        {
            get { return _surveyOwnedByVendor; }
        }

        /// <summary>
        /// Gets the list of available actions for vendors based on the survey status.
        /// </summary>
        public virtual SurveyActionType[] GetExternalAvailableActions()
        {
            ArrayList list = new ArrayList();
            if (this.Survey.StatusCode == SurveyStatus.ReturningToCompany.Code)
            {
                list.Add(SurveyActionType.ReturnToCompany);
            }
            else if (this.Survey.StatusCode == SurveyStatus.AwaitingAcceptance.Code)
            {
                list.Add(SurveyActionType.AcceptSurvey);
                list.Add(SurveyActionType.ReturnToCompany);
                list.Add(SurveyActionType.AddNote);
            }
            else if (this.Survey.StatusCode == SurveyStatus.AssignedSurvey.Code)
            {
                list.Add(SurveyActionType.ReturnToCompany);
                list.Add(SurveyActionType.SetSurveyDetails);
                list.Add(SurveyActionType.SendForReview);
                list.Add(SurveyActionType.AddNote);
                list.Add(SurveyActionType.AddDocument);
            }

            return list.ToArray(typeof(SurveyActionType)) as SurveyActionType[];
        }

        /// <summary>
        /// Builds a list of actions that this user can preform on the survey.
        /// </summary>
        /// <returns>Array of SurveyActions.</returns>
        public virtual SurveyAction[] GetAvailableActions()
        {
            if (this.CurrentUser == null)
            {
                throw new InvalidOperationException("A current user must be defined and cannot be system.");
            }

            // get all actions defined in the DB for this company and where the actionlist flag is set to false
            SurveyAction[] availableActions = SurveyAction.GetSortedArray("CompanyID = ? && SurveyStatusCode = ?",
                "DisplayOrder ASC", this.Company.ID, this.Survey.StatusCode);

            // build another array with only actions the user has permission to preform
            SurveyAction[] actions = new SurveyAction[availableActions.Length];
            int actionCount = 0;
            PermissionSet permissions = this.CurrentUser.Permissions;
            foreach (SurveyAction action in availableActions)
            {
                bool allowed = permissions.CanTakeAction(action, this.Survey.ServiceType, this.SurveyOwnedByUser, this.SurveyOwnedByVendor);
                if (allowed)
                {
                    actions[actionCount++] = action;
                }
            }

            // trim array to length
            SurveyAction[] result = new SurveyAction[actionCount];
            Array.Copy(actions, result, actionCount);

            return result;
        }

        /// <summary>
        /// Creates a new survey for a insured location or locations.  Only used for creating workflow submissions.
        /// </summary>
        /// <returns>Survey created by this method.</returns>
        public virtual Survey CreateSurvey()
        {
            Survey survey = this.Survey;

            survey = OnCreateSurveyComplete(null, survey);

            survey = (survey.IsReadOnly) ? survey.GetWritableInstance() : survey;
            survey.CreateStateCode = CreateState.OnHold.Code;

            return survey;
        }

        /// <summary>
        /// Creates a new survey for a insured location or locations.
        /// </summary>
        /// <param name="dueDate">The due date of the survey request.</param>
        /// <param name="oneSurvey">Flag determining if all the locations should be on one survey.</param>
        /// <returns>Survey created by this method.</returns>
        public virtual Survey CreateSurvey(DateTime dueDate, bool oneSurvey, bool manuallyCreated, string priority)
        {
            Survey survey = this.Survey;

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnCreateSurvey(trans, survey);
                CreateSurvey(trans, survey, dueDate, oneSurvey, manuallyCreated, priority);
                //OnCreateSurveyComplete(trans, survey);

                if (survey.IsReadOnly)
                    survey = survey.GetWritableInstance();

                survey.Save(trans);

                trans.Commit();
            }

            return survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void CreateSurvey(Transaction trans, Survey survey, DateTime dueDate, bool oneSurvey, bool manuallyCreated, string priority)
        {
            //Seperate out each selected location into its own Survey Request
            VWLocation[] locations = VWLocation.GetSortedArray("InsuredID = ? && ContactCount > 0 && LocationExclusionCount = 0 && ActivePolicyCount > 0 && ActivePolicyCount != PolicyExclusionCount && ActivePolicyCount = (PolicyExclusionCount + PolicyBeenViewedCount)", "LocationNumber ASC", survey.InsuredID);
            Policy[] allPolicies = Policy.GetArray("InsuredID = ? && IsActive = true", survey.InsuredID);
            AssignmentRule[] rules = AssignmentRule.GetSortedArray("CompanyID = ? && SurveyStatusTypeCode = ?", "PriorityIndex ASC", _company.ID, SurveyStatusType.Survey.Code);

            //Get the attached docs and notes and reasons
            SurveyReason[] reasons = SurveyReason.GetArray("SurveyID = ?", survey.ID);
            SurveyDocument[] documents = SurveyDocument.GetArray("SurveyID = ?", survey.ID);
            SurveyNote[] notes = SurveyNote.GetArray("SurveyID = ?", survey.ID);
            
            //initialize
            bool firstPass = true;
            //Location[] locations = list.ToArray(typeof(Location)) as Location[];
            Survey newSurvey = null;
            string historyEntry = null;

            foreach (VWLocation location in locations)
            {
                //Create new survey
                if (!oneSurvey || firstPass)
                {
                    newSurvey = new Survey(new Guid());
                    newSurvey.CompanyID = survey.CompanyID;
                    newSurvey.InsuredID = survey.InsuredID;
                    newSurvey.PrimaryPolicyID = survey.PrimaryPolicyID;
                    newSurvey.LocationID = location.LocationID;
                    newSurvey.TypeID = survey.TypeID;
                    newSurvey.ServiceTypeCode = survey.ServiceTypeCode;
                    newSurvey.TransactionTypeID = survey.TransactionTypeID;
                    newSurvey.StatusCode = survey.StatusCode;
                    newSurvey.CreateStateCode = (manuallyCreated) ? CreateState.Migrated.Code : survey.CreateStateCode;
                    newSurvey.CreateByInternalUserID = survey.CreateByInternalUserID;
                    newSurvey.CreateByExternalUserName = survey.CreateByExternalUserName;
                    newSurvey.CreateDate = DateTime.Now;
                    newSurvey.DueDate = dueDate;
                    newSurvey.NonDisclosureRequired = survey.NonDisclosureRequired;
                    newSurvey.RecsRequired = !SurveyTypeType.IsNotWrittenBusiness(survey.Type.Type);
                    newSurvey.WorkflowSubmissionNumber = survey.WorkflowSubmissionNumber;
                    newSurvey.UnderwriterCode = survey.UnderwriterCode;
                    newSurvey.Priority = priority;
                    newSurvey.InHouseSelection = survey.InHouseSelection;
                    newSurvey.Save(trans);

                    //Create history entry
                    historyEntry = string.Format("Survey created in ARMS by {0}.", GetCurrentUserName());
                    WriteHistoryEntry(trans, newSurvey, historyEntry);

                    //Add any survey reasons
                    foreach (SurveyReason tempreason in reasons)
                    {
                        // build and save the document
                        SurveyReason reason = new SurveyReason(tempreason.TypeID, newSurvey.ID);
                        reason.Save(trans);
                    }

                    //Add any documents that were attached 
                    foreach (SurveyDocument tempDoc in documents)
                    {
                        // build and save the document
                        SurveyDocument doc = new SurveyDocument(Guid.NewGuid());
                        doc.SurveyID = newSurvey.ID;
                        doc.DocumentName = tempDoc.DocumentName;
                        doc.MimeType = tempDoc.MimeType;
                        doc.SizeInBytes = tempDoc.SizeInBytes;
                        doc.FileNetReference = tempDoc.FileNetReference;
                        doc.UploadedOn = tempDoc.UploadedOn;
                        doc.UploadUserID = tempDoc.UploadUserID;
                        doc.UploadUserName = tempDoc.UploadUserName;
                        doc.FileNetRemovable = tempDoc.FileNetRemovable;
                        doc.IsRemoved = false;
                        doc.Save(trans);
                    }

                    //Add any "insured" specific comments that were entered
                    foreach (SurveyNote tempNote in notes)
                    {
                        if (tempNote.Comment != null && tempNote.Comment.Trim().Length > 0)
                        {
                            // build and save the comment
                            SurveyNote note = new SurveyNote(Guid.NewGuid());
                            note.SurveyID = newSurvey.ID;
                            note.UserID = tempNote.UserID;
                            note.UserName = tempNote.UserName;
                            note.EntryDate = tempNote.EntryDate;
                            note.InternalOnly = tempNote.InternalOnly;
                            note.Comment = "Instructions from survey creator: " + tempNote.Comment;
                            note.NoteBySurveyCreator = tempNote.NoteBySurveyCreator;
                            note.Save(trans);
                        }
                    }
                }

                //Save the policies and coverage references that were selected and reported
                SurveyLocationPolicyCoverageName[] selectedCoverages = SurveyLocationPolicyCoverageName.GetArray("SurveyID = ? && LocationID = ?", survey.ID, location.LocationID);
                foreach (SurveyLocationPolicyCoverageName selectedCoverage in selectedCoverages)
                {
                    SurveyLocationPolicyExclusion exclusion = SurveyLocationPolicyExclusion.GetOne("SurveyID = ? && LocationID = ? && PolicyID = ?", survey.ID, selectedCoverage.LocationID, selectedCoverage.PolicyID);
                    if (!exclusion.PolicyExcluded && exclusion.Policy.LineOfBusiness != LineOfBusiness.CommercialUmbrella)
                    {
                        SurveyPolicyCoverage coverage = new SurveyPolicyCoverage(newSurvey.ID, selectedCoverage.LocationID, selectedCoverage.PolicyID, selectedCoverage.CoverageNameID);
                        coverage.ReportTypeCode = (coverage.CoverageName.Type == CoverageNameType.CommercialUmbrella) ? ReportType.Report.Code : selectedCoverage.ReportTypeCode;
                        coverage.Active = true;

                        coverage.Save(trans);
                    }
                }

                //Now save the the policies with its coverages that were not included 
                Policy[] excludedPolicies = Policy.GetArray("SurveyLocationPolicyExclusions[SurveyID = ? && LocationID = ? && PolicyExcluded = True] || (InsuredID = ? && LineOfBusinessCode = ? && IsActive = true)", survey.ID, location.LocationID, survey.InsuredID, LineOfBusiness.CommercialUmbrella.Code);
                foreach (Policy excludedPolicy in excludedPolicies)
                {
                    CoverageName[] coverageNames = CoverageName.GetArray("LineOfBusinessCode = ? && CompanyID = ? && (IsRequired = True || Coverages[!ISNULL(Value)])", excludedPolicy.LineOfBusinessCode, CurrentUser.CompanyID);
                    foreach (CoverageName coverageName in coverageNames)
                    {
                        SurveyPolicyCoverage coverage = new SurveyPolicyCoverage(newSurvey.ID, location.LocationID, excludedPolicy.ID, coverageName.ID);
                        coverage.ReportTypeCode = (coverageName.Type == CoverageNameType.CommercialUmbrella) ? ReportType.Report.Code : ReportType.NoReport.Code;
                        coverage.Active = (excludedPolicy.LineOfBusiness == LineOfBusiness.CommercialUmbrella);

                        coverage.Save(trans);
                    }
                }

                //Add any "location" specific comments that were entered
                SurveyLocationNote locationNote = SurveyLocationNote.GetOne("SurveyID = ? && LocationID = ?", survey.ID, location.LocationID);
                if (locationNote != null && locationNote.Comment != null && locationNote.Comment.Trim().Length > 0)
                {
                    // build and save the comment
                    SurveyNote note = new SurveyNote(Guid.NewGuid());
                    note.SurveyID = newSurvey.ID;
                    note.UserID = locationNote.UserID;
                    note.UserName = locationNote.UserName;
                    note.EntryDate = locationNote.EntryDate;
                    note.InternalOnly = false;
                    note.Comment = locationNote.Comment;
                    note.Save(trans);
                }

                //check if this survey needs to go directly to the field rep
                if (_company.SupportsSurveysDirectToFieldReps && (CurrentUser.ID == newSurvey.CreateByInternalUserID && !CurrentUser.Role.PermissionSet.CanWorkReviews))
                {
                    newSurvey = newSurvey.GetWritableInstance();
                    newSurvey.CreateStateCode = (manuallyCreated) ? CreateState.Migrated.Code : CreateState.AssignedToConsultant.Code;
                    newSurvey.AssignedUserID = CurrentUser.ID;
                    newSurvey.StatusCode = SurveyStatus.AssignedSurvey.Code;
                    newSurvey.AssignDate = DateTime.Today;
                    newSurvey.AcceptDate = DateTime.Now;

                    historyEntry = string.Format("Survey assigned to and accepted by {0} based on the survey being created by {0}.", CurrentUser.Name);

                    OnCreateSurveyComplete(trans, newSurvey);
                    SaveSurvey(trans, newSurvey, historyEntry);
                }
                else
                {
                    if (!oneSurvey || firstPass)
                    {
                        //flag
                        firstPass = false;

                        #region --- Hold Evaluation ---

                        // see if this survey should be put on hold
                        bool putOnHold = false;
                        historyEntry = null;
                        newSurvey = newSurvey.GetWritableInstance();
                        // see if this survey matches any one of the hold rules
                        foreach (AssignmentRule rule in rules)
                        {
                            if (rule.Filter.MatchesSurvey(newSurvey)) // rule match
                            {
                                if (rule.ActionCode == "HI")
                                {
                                    newSurvey.InHouseSelection = true;
                                    historyEntry = string.Format("Survey assigned to Internal Staff based on assignment rule {{{0}}}.", rule.Filter.ToString());
                                }
                                else if (rule.ActionCode == "HV")
                                {
                                    newSurvey.InHouseSelection = false;
                                    historyEntry = string.Format("Survey assigned to vendor based on assignment rule {{{0}}}.", rule.Filter.ToString());
                                }
                                else
                                {
                                    putOnHold = true;
                                    historyEntry = string.Format("Survey put on hold based on assignment rule {{{0}}}.", rule.Filter.ToString());
                                }
                                break;
                            }
                        }
                        if (putOnHold)
                        {
                            // put survey in hold queue
                            newSurvey.AssignedUserID = Guid.Empty;
                            newSurvey.StatusCode = SurveyStatus.OnHoldSurvey.Code;
                            newSurvey.CreateStateCode = (manuallyCreated) ? CreateState.Migrated.Code : CreateState.OnHold.Code;

                            UserTerritory holdAssignment = PickUserForSurvey(newSurvey, SurveyStatusType.Survey);
                            if (holdAssignment != null) // consultant found for recommendation
                            {
                                newSurvey.RecommendedUserID = holdAssignment.UserID;
                            }

                            OnCreateSurveyComplete(trans, newSurvey);
                            SaveSurvey(trans, newSurvey, historyEntry);

                            // go to next location
                            continue;
                        }

                        #endregion

                        #region --- Consultant Assignment ---

                        // if we get here then the survey is not on hold

                        // try to assign the survey to a consultant
                        User targetConsultant = null;

                        // pick the consulant who should be assigned this survey, based on territory assignments and allocation
                        historyEntry = null;
                        UserTerritory assignment = PickUserForSurvey(newSurvey, SurveyStatusType.Survey);
                        if (assignment != null) // eligable consultant found
                        {
                            targetConsultant = assignment.User;

                            // build the history entry to include details of this match
                            historyEntry = string.Format("Survey assigned to {0} based on territory '{1}'.",
                                targetConsultant.Name, assignment.Territory.Name);
                        }
                        else // no consulant found
                        {
                            historyEntry = "ARMS could not assign this survey using territory assignments.";
                        }

                        if (targetConsultant != null)
                        {
                            newSurvey.AssignedUserID = targetConsultant.ID;
                            newSurvey.StatusCode = SurveyStatus.AwaitingAcceptance.Code;
                            newSurvey.AssignDate = DateTime.Today;
                            if (!targetConsultant.IsFeeCompany)
                            {
                                newSurvey.CreateStateCode = (manuallyCreated) ? CreateState.Migrated.Code : CreateState.AssignedToConsultant.Code;
                            }
                            else // vendor
                            {
                                newSurvey.CreateStateCode = (manuallyCreated) ? CreateState.Migrated.Code : CreateState.AssignedToVendor.Code;
                            }
                        }
                        else // no consultant
                        {
                            newSurvey.AssignedUserID = Guid.Empty;
                            newSurvey.StatusCode = SurveyStatus.UnassignedSurvey.Code;
                            newSurvey.CreateStateCode = (manuallyCreated) ? CreateState.Migrated.Code : CreateState.NotAssigned.Code;
                        }

                        OnCreateSurveyComplete(trans, newSurvey);
                        SaveSurvey(trans, newSurvey, historyEntry);

                        #endregion
                    }
                }
            }
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnCreateSurvey(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual Survey OnCreateSurveyComplete(Transaction trans, Survey survey)
        {
            return survey;
        }

        /// <summary>
        /// Creates a new service visit.
        /// </summary>
        /// <returns>Service visit created by this method.</returns>
        public virtual Survey CreateVisit()
        {
            Survey survey = this.Survey;

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                if (survey.IsReadOnly)
                    survey = survey.GetWritableInstance();

                OnCreateVisit(trans, survey);
                CreateVisit(trans, survey);
                OnCreateVisitComplete(trans, survey);

                trans.Commit();
            }

            return survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void CreateVisit(Transaction trans, Survey survey)
        {
            survey.CreateStateCode = CreateState.AssignedToConsultant.Code;
            survey.AssignDate = DateTime.Now;
            survey.AcceptDate = DateTime.Now;
            survey.UnderwriterCode = survey.Insured.Underwriter;

            //if this visit is assigned to someone other than the creator, change this to awaiting acceptance
            if (CurrentUser.ID != survey.AssignedUserID)
            {
                survey.StatusCode = SurveyStatus.AwaitingAcceptance.Code;
                survey.AcceptDate = DateTime.MinValue;
            }

            survey.Save(trans);

            //Create history entry
            string historyEntry = string.Format("Service visit created in ARMS by {0}.", GetCurrentUserName());
            WriteHistoryEntry(trans, survey, historyEntry);

            ////Retain all the possible policies and coverages tied to the insured
            //Policy[] policies = Policy.GetArray("InsuredID = ? && IsActive = true", survey.InsuredID);
            //foreach (Policy policy in policies)
            //{
            //    CoverageName[] coverageNames = CoverageName.GetArray("Coverages[InsuredID = ? && PolicyID = ?] || (IsRequired = true && CompanyID = ? && LineOfBusinessCode = ?)", survey.InsuredID, policy.ID, CurrentUser.CompanyID, policy.LineOfBusinessCode);
            //    foreach (CoverageName coverageName in coverageNames)
            //    {
            //        //Check if the any of the coverage have a value, if so then report on it
            //        Coverage[] coverages = Coverage.GetArray("InsuredID = ? && PolicyID = ? && NameID = ? && !ISNULL(Value) && Value != '' && Value != '0' ", survey.InsuredID, policy.ID, coverageName.ID);

            //        SurveyPolicyCoverage newCoverage = new SurveyPolicyCoverage(survey.ID, survey.LocationID, policy.ID, coverageName.ID);
            //        newCoverage.ReportTypeCode = (coverages.Length > 0) ? ReportType.Report.Code : ReportType.NoReport.Code;
            //        newCoverage.Active = true;
            //        newCoverage.Save(trans);
            //    }
            //}


            //Retain all the possible policies and coverages tied to the insured
            Policy[] policies = Policy.GetArray("InsuredID = ? && IsActive = true", survey.InsuredID);
            foreach (Policy policy in policies)
            {
                DataTable dt = StoredProcedures.CreateServiceVisit_GetPolicyCoverages(
                        survey.InsuredID,
                        policy.ID,
                        CurrentUser.CompanyID,
                        policy.LineOfBusinessCode);

                foreach (DataRow dr in dt.Rows)
                {
                    Guid coverageNameID = new Guid(dr["CoverageNameID"].ToString());
                    int coveragesWithValuesCount = Convert.ToInt32(dr["CoveragesWithValuesCount"]);

                    SurveyPolicyCoverage newCoverage = new SurveyPolicyCoverage(survey.ID, survey.LocationID, policy.ID, coverageNameID);
                    newCoverage.ReportTypeCode = (coveragesWithValuesCount > 0) ? ReportType.Report.Code : ReportType.NoReport.Code;
                    newCoverage.Active = true;
                    newCoverage.Save(trans);
                }
            }
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnCreateVisit(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnCreateVisitComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Creates a new survey from an existing survey.
        /// </summary>
        /// <returns>Service visit created by this method.</returns>
        public virtual Survey CreateSurveyFromExistingSurvey(SurveyType surveyType, User assignedUser, DateTime dueDate)
        {
            VerifyUserCanTakeAction(SurveyActionType.CreateNewSurvey);

            Survey survey = this.Survey;
            Survey newSurvey = null;

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                if (survey.IsReadOnly)
                    survey = survey.GetWritableInstance();

                OnCreateSurveyFromExistingSurvey(trans, survey);
                newSurvey = CreateSurveyFromExistingSurvey(trans, survey, surveyType, assignedUser, dueDate);
                OnCreateSurveyFromExistingSurveyComplete(trans, newSurvey);

                trans.Commit();
            }

            return newSurvey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual Survey CreateSurveyFromExistingSurvey(Transaction trans, Survey oldSurvey, SurveyType surveyType, User assignedUser, DateTime dueDate)
        {
            Survey newSurvey = new Survey(Guid.NewGuid());
            newSurvey.CompanyID = oldSurvey.CompanyID;
            newSurvey.InsuredID = oldSurvey.InsuredID;
            newSurvey.LocationID = oldSurvey.LocationID;
            newSurvey.PrimaryPolicyID = oldSurvey.PrimaryPolicyID;
            newSurvey.PreviousSurveyID = oldSurvey.ID;
            newSurvey.ServiceTypeCode = ServiceType.SurveyRequest.Code;
            newSurvey.TransactionTypeID = oldSurvey.TransactionTypeID;
            newSurvey.TypeID = surveyType.ID;
            newSurvey.StatusCode = SurveyStatus.AwaitingAcceptance.Code;
            newSurvey.AssignedUserID = assignedUser.ID;
            newSurvey.CreateStateCode = oldSurvey.CreateStateCode;
            newSurvey.CreateDate = DateTime.Now;
            newSurvey.CreateByInternalUserID = CurrentUser.ID;
            newSurvey.AssignDate = DateTime.Now;
            newSurvey.DueDate = dueDate.Date;
            newSurvey.WorkflowSubmissionNumber = oldSurvey.WorkflowSubmissionNumber;
            newSurvey.UnderwriterCode = oldSurvey.UnderwriterCode;
            newSurvey.Priority = oldSurvey.Priority;
            newSurvey.Save(trans);

            //Create history entry
            string historyEntry = string.Format("Survey created in ARMS by {0} from previous survey #{1}.", GetCurrentUserName(), oldSurvey.Number);
            WriteHistoryEntry(trans, newSurvey, historyEntry);

            //Copy over the previous survey data elements
            SurveyPolicyCoverage[] coverages = SurveyPolicyCoverage.GetArray("SurveyID = ?", oldSurvey.ID);
            foreach (SurveyPolicyCoverage oldCoverage in coverages)
            {
                SurveyPolicyCoverage newCoverage = new SurveyPolicyCoverage(newSurvey.ID, oldCoverage.LocationID, oldCoverage.PolicyID, oldCoverage.CoverageNameID);
                newCoverage.ReportTypeCode = oldCoverage.ReportTypeCode;
                newCoverage.Active = oldCoverage.Active;
                newCoverage.Save(trans);
            }

            return newSurvey;
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnCreateSurveyFromExistingSurvey(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnCreateSurveyFromExistingSurveyComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Notifies the UW of the completed survey without actually closing it in ARMS.
        /// </summary>
        public virtual void NotifyUW(string explainText)
        {
            VerifyUserCanTakeAction(SurveyActionType.NotifyUnderwriting);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnNotifyUW(trans, survey);
                NotifyUW(trans, survey);
                OnNotifyUWComplete(trans, survey, explainText, CurrentUser);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void NotifyUW(Transaction trans, Survey survey)
        {
            survey.UnderwriterNotified = true;

            //Create history entry
            string historyEntry = string.Format("{0} notified underwriting.", GetCurrentUserName());
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnNotifyUW(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnNotifyUWComplete(Transaction trans, Survey survey, string explainText, User currentUser)
        {
        }

        /// <summary>
        /// Emails the UW.
        /// </summary>
        public virtual void EmailUW(Underwriter underwriter, string text)
        {
            VerifyUserCanTakeAction(SurveyActionType.EmailUW);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnEmailUW(trans, survey);
                EmailUW(trans, survey, underwriter, text);
                OnEmailUWComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void EmailUW(Transaction trans, Survey survey, Underwriter underwriter, string text)
        {
            EmailHelper email = new EmailHelper(survey, CurrentUser);
            email.SendEmailToUnderwriter(EmailHelper.NotificationType.EmailUnderwriter, underwriter, string.Empty, text, string.Empty);

            SurveyNote note = new SurveyNote(Guid.NewGuid());
            note.SurveyID = survey.ID;
            note.UserID = CurrentUser.ID;
            note.EntryDate = DateTime.Now;
            note.InternalOnly = false;
            note.Comment = string.Format("To {0}: {1}", underwriter.Name, text);
            note.Save(trans);

            //Create history entry
            string historyEntry = string.Format("{0} sent an email to {1}.", GetCurrentUserName(), underwriter.Name);
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnEmailUW(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnEmailUWComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Assigns or reassigns this Survey to a new user.
        /// </summary>
        /// <param name="newOwner">User to which this survey should be assigned.</param>
        public virtual void Assign(User newOwner)
        {
            if (newOwner == null) throw new ArgumentNullException("newOwner");

            VerifyUserCanTakeAction(SurveyActionType.AssignSurvey);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnAssign(trans, survey, newOwner);
                Assign(trans, survey, newOwner);
                OnAssignComplete(trans, survey, newOwner);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void Assign(Transaction trans, Survey survey, User newOwner)
        {
            Guid originalOwnerID = survey.AssignedUserID;

            if (survey.Status.IsInList(SurveyStatus.UnassignedSurvey, SurveyStatus.AssignedSurvey, SurveyStatus.OnHoldSurvey, SurveyStatus.AwaitingAcceptance))
            {
                if (!newOwner.IsFeeCompany && !newOwner.Permissions.CanWorkSurveys)
                {
                    throw new InvalidOperationException("User '" + newOwner.ID + "' cannot work surveys.");
                }

                survey.AssignedByUserID = CurrentUser.ID;
                survey.StatusCode = SurveyStatus.AwaitingAcceptance.Code;
                survey.AssignDate = DateTime.Now;
            }
            else if (survey.Status.IsInList(SurveyStatus.UnassignedReview, SurveyStatus.AssignedReview))
            {
                if (!newOwner.Permissions.CanWorkReviews)
                {
                    throw new InvalidOperationException("User '" + newOwner.ID + "' cannot work reviews.");
                }

                survey.StatusCode = SurveyStatus.AssignedReview.Code;
            }
            else
            {
                throw new InvalidOperationException("This action cannot be called when the survey has a status of '" + survey.StatusCode + "'.");
            }

            // build the history entry
            string historyEntry;
            if (survey.AssignedUserID == Guid.Empty)
            {
                historyEntry = string.Format("{0} assigned this survey to {1}.", GetCurrentUserName(), newOwner.Name);
            }
            else // re-assignment
            {
                survey.ReassignDate = DateTime.Now;
                historyEntry = string.Format("{0} reassigned this survey to {1}.", GetCurrentUserName(), newOwner.Name);
            }

            // now assign the new owner
            survey.RecommendedUserID = Guid.Empty;
            survey.AssignedUserID = newOwner.ID;

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnAssign(Transaction trans, Survey survey, User newOwner)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnAssignComplete(Transaction trans, Survey survey, User newOwner)
        {
        }

        /// <summary>
        /// Returns the survey to the last owner (consultant) for correction.
        /// </summary>
        /// <param name="newOwner">User to which the returned survey should be assigned.</param>
        public virtual void ReturnForCorrection(User newOwner)
        {
            if (newOwner == null) throw new ArgumentNullException("newOwner");

            VerifyUserCanTakeAction(SurveyActionType.Reject);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnReturnForCorrection(trans, survey, newOwner);
                ReturnForCorrection(trans, survey, newOwner);
                OnReturnForCorrectionComplete(trans, survey, newOwner);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ReturnForCorrection(Transaction trans, Survey survey, User newOwner)
        {
            User recorder = newOwner;

            // new owner must be a consultant
            if (!newOwner.IsFeeCompany && !newOwner.Permissions.CanWorkSurveys)
            {
                throw new InvalidOperationException("User '" + newOwner.ID + "' cannot work surveys.");
            }

            if (survey.Status == SurveyStatus.AssignedReview)
            {
                // record the current technician
                survey.ReviewerUserID = survey.AssignedUserID;
            }
            else if (survey.Status == SurveyStatus.UnassignedReview)
            {
                // update the status
                recorder = this.CurrentUser;
            }
            else // invalid status
            {
                throw new InvalidOperationException("This method cannot be called when the survey has a status of '" + survey.StatusCode + "'.");
            }

            // assign the new user
            survey.ConsultantUserID = Guid.Empty;
            survey.StatusCode = SurveyStatus.AssignedSurvey.Code;
            survey.AssignedUserID = newOwner.ID;
            survey.Correction = true;
            string historyEntry = string.Format("{0} returned {1} to {2} for correction.", GetCurrentUserName(), survey.ServiceType.Name.ToLower(), newOwner.Name);

            // save our changes
            SaveSurvey(trans, survey, historyEntry, recorder);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnReturnForCorrection(Transaction trans, Survey survey, User newOwner)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnReturnForCorrectionComplete(Transaction trans, Survey survey, User newOwner)
        {
        }

        /// <summary>
        /// Assigns an and survey based on the survey/review territory
        /// </summary>
        public bool AssignBasedOnTerritory()
        {
            VerifyUserCanTakeAction(SurveyActionType.AssignSurveyTerritory);

            Survey survey = this.Survey.GetWritableInstance();

            bool assigned;

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnAssignBasedOnTerritory(trans, survey);
                assigned = AssignBasedOnTerritory(trans, survey);
                OnAssignBasedOnTerritoryComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;

            return assigned;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual bool AssignBasedOnTerritory(Transaction trans, Survey survey)
        {
            // pick the consultnat who should be assigned this survey, based on territory assignments and allocation
            UserTerritory assignment;
            string historyEntry = string.Empty;

            if (survey.StatusCode == SurveyStatus.UnassignedSurvey.Code || survey.StatusCode == SurveyStatus.OnHoldSurvey.Code)
            {
                assignment = PickUserForSurvey(survey, SurveyStatusType.Survey);
                survey.StatusCode = SurveyStatus.AwaitingAcceptance.Code;
                survey.AssignDate = (survey.AssignDate == DateTime.MinValue) ? DateTime.Today : survey.AssignDate;
            }
            else if (survey.StatusCode == SurveyStatus.UnassignedReview.Code)
            {
                assignment = PickUserForSurvey(survey, SurveyStatusType.Review);
                survey.StatusCode = SurveyStatus.AssignedReview.Code;
            }
            else
                throw new InvalidOperationException("This action cannot be called when the survey has a status of '" + survey.StatusCode + "'.");

            if (assignment != null) // eligable consultant found
            {

                if (_override)
                {
                    historyEntry = string.Format("ARMS assigned this survey to {0} based on territory '{1}'.",
                        assignment.User.Name, assignment.Territory.Name);
                }
                else if (survey.AssignedUserID == Guid.Empty)
                {
                    historyEntry = string.Format("{0} assigned this survey to {1} based on territory '{2}'.",
                        CurrentUser.Name, assignment.User.Name, assignment.Territory.Name);
                }
                else // re-assignment
                {
                    historyEntry = string.Format("{0} reassigned this survey to {1} based on territory '{2}'.",
                        CurrentUser.Name, assignment.User.Name, assignment.Territory.Name);
                }

                survey.AssignedUserID = assignment.User.ID;
                survey.RecommendedUserID = Guid.Empty;
                SaveSurvey(trans, survey, historyEntry);
            }
            else // no consultant
            {
                //return false to let the user know that the survey could not 
                //be assigned based on the territory
                return false;
            }

            return true;
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnAssignBasedOnTerritory(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnAssignBasedOnTerritoryComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Changes ownership of this survey to the user specified.
        /// </summary>
        /// <param name="newOwner">User requesting ownership of this survey.</param>
        public virtual void TakeOwnership(User newOwner)
        {
            if (newOwner == null) throw new ArgumentNullException("newOwner");

            VerifyUserCanTakeAction(SurveyActionType.TakeOwnership);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnTakeOwnership(trans, survey, newOwner);
                TakeOwnership(trans, survey, newOwner);
                OnTakeOwnershipComplete(trans, survey, newOwner);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void TakeOwnership(Transaction trans, Survey survey, User newOwner)
        {
            Guid originalOwnerID = survey.AssignedUserID;

            // modify the status and build the history entry text
            string historyEntry;
            if (survey.Status == SurveyStatus.UnassignedReview)
            {
                // target owner must be a reviewer
                if (!newOwner.Permissions.CanWorkReviews)
                {
                    throw new InvalidOperationException("User '" + newOwner.ID + "' is not assigned the reviewer role.");
                }

                survey.StatusCode = SurveyStatus.AssignedReview.Code;
                historyEntry = string.Format("{0} has taken ownership for review.", newOwner.Name);
            }
            else
            {
                throw new InvalidOperationException("This action cannot be called when the survey has a status of '" + survey.StatusCode + "'.");
            }

            // assign the new owner
            survey.AssignedUserID = newOwner.ID;

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnTakeOwnership(Transaction trans, Survey survey, User newOwner)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnTakeOwnershipComplete(Transaction trans, Survey survey, User newOwner)
        {
        }

        /// <summary>
        /// User accepting an assigned survey.
        /// </summary>
        public virtual void Accept()
        {
            VerifyUserCanTakeAction(SurveyActionType.AcceptSurvey);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnAccept(trans, survey);
                Accept(trans, survey);
                OnAcceptComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void Accept(Transaction trans, Survey survey)
        {
            // Status must be in Pending Survey
            if (survey.Status != SurveyStatus.AwaitingAcceptance)
            {
                throw new InvalidOperationException("This action cannot be called when the survey has a status of '" + survey.StatusCode + "'.");
            }

            survey.StatusCode = SurveyStatus.AssignedSurvey.Code;
            survey.AcceptDate = DateTime.Now;

            string historyEntry = string.Format("{0} accepted this {1}.", GetCurrentUserName(), survey.ServiceType.Name.ToLower());

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnAccept(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnAcceptComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// UW accepting an assigned survey.
        /// </summary>
        public virtual void AcceptByUW(string userName)
        {
            VerifyUserCanTakeAction(SurveyActionType.AcknowledgeByUW);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnAcceptByUW(trans, survey);
                AcceptByUW(trans, survey, userName);
                OnAcceptByUWComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void AcceptByUW(Transaction trans, Survey survey, string userName)
        {
            // Status must be in Pending Survey
            if (survey.Status != SurveyStatus.AssignedReview && survey.Status != SurveyStatus.ClosedSurvey)
            {
                throw new InvalidOperationException("This action cannot be called when the survey has a status of '" + survey.StatusCode + "'.");
            }

            survey.UnderwritingAcceptDate = DateTime.Now;

            string historyEntry = string.Format("{0} acknowledged this {1} from underwriting.", (!string.IsNullOrEmpty(userName)) ? userName : GetCurrentUserName(), survey.ServiceType.Name.ToLower());

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnAcceptByUW(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnAcceptByUWComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// User changing the submission number used to create the survey request.
        /// </summary>
        public virtual void ChangeSubmissionNumber(string submissionNumber)
        {
            VerifyUserCanTakeAction(SurveyActionType.ChangePrimaryPolicy);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnChangeSubmissionNumber(trans, survey);
                ChangeSubmissionNumber(trans, survey, submissionNumber);
                OnChangeSubmissionNumberComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ChangeSubmissionNumber(Transaction trans, Survey survey, string submissionNumber)
        {
            // Previous primary policy must exist
            if (String.IsNullOrEmpty(survey.WorkflowSubmissionNumber))
            {
                throw new InvalidOperationException("This action cannot be called when the survey does not have a current submission number.");
            }

            string previousSubmissionNumber = _survey.WorkflowSubmissionNumber;

            survey.WorkflowSubmissionNumber = submissionNumber;

            string historyEntry = string.Format("{0} changed the submission number used to create this survey request from {1} to {2}.", GetCurrentUserName(), previousSubmissionNumber, submissionNumber);

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnChangeSubmissionNumber(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnChangeSubmissionNumberComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// User accepting an assigned survey.
        /// </summary>
        public virtual void ChangePrimaryPolicy(string policyNumber)
        {
            VerifyUserCanTakeAction(SurveyActionType.ChangePrimaryPolicy);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnChangePrimaryPolicy(trans, survey);
                ChangePrimaryPolicy(trans, survey, policyNumber);
                OnChangePrimaryPolicyComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ChangePrimaryPolicy(Transaction trans, Survey survey, string policyNumber)
        {
            // Previous primary policy must exist
            if (survey.PrimaryPolicy == null)
            {
                throw new InvalidOperationException("This action cannot be called when the survey does not have a current primary policy.");
            }

            string previousPolicyNumber = _survey.PrimaryPolicy.Number;

            //Check if that policy already exists in ARMS
            Policy policy = Policy.GetOne("Insured.CompanyID = ? && Number = ?", CurrentUser.CompanyID, policyNumber);
            if (policy != null)
            {
                survey.PrimaryPolicyID = policy.ID;
            }
            else
            {
                Policy newPolicy = new Policy(Guid.NewGuid());
                newPolicy.InsuredID = survey.InsuredID;
                newPolicy.Number = policyNumber;
                newPolicy.IsActive = false;
                newPolicy.Save(trans);

                survey.PrimaryPolicyID = newPolicy.ID;
            }

            string historyEntry = string.Format("{0} changed this survey's primary policy from {1} to {2}.", GetCurrentUserName(), previousPolicyNumber, policyNumber);

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnChangePrimaryPolicy(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnChangePrimaryPolicyComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Releases the currently assigned owner of this Survey and reassigns it to the appropriate party.
        /// </summary>
        public virtual void ReleaseOwnership()
        {
            ReleaseOwnership(null, null);
        }

        /// <summary>
        /// Releases the currently assigned owner of this Survey and reassigns it to the appropriate party.
        /// </summary>
        public virtual void ReleaseOwnership(SurveyReleaseOwnershipType releaseType, string txtReason)
        {
            // a user must be assigned to the survey
            if (this.Survey.AssignedUser == null)
            {
                throw new InvalidOperationException("This action cannot be called when no user is assigned to the survey.");
            }

            //VerifyUserCanTakeAction(SurveyActionType.ReleaseOwnership);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnReleaseOwnership(trans, survey);
                ReleaseOwnership(trans, survey, releaseType, txtReason);
                OnReleaseOwnershipComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ReleaseOwnership(Transaction trans, Survey survey, SurveyReleaseOwnershipType releaseType, string txtReason)
        {
            User priorOwner = survey.AssignedUser;

            // survey must be "in progress"
            if (!survey.Status.IsInList(SurveyStatus.AssignedSurvey, SurveyStatus.AwaitingAcceptance, SurveyStatus.AssignedReview))
            {
                throw new InvalidOperationException("This action cannot be called when the survey has a status of '" + survey.Status.Name + "'.");
            }

            // unassign the survey
            if (survey.Status == SurveyStatus.AssignedSurvey || survey.Status == SurveyStatus.AwaitingAcceptance)
            {
                if (survey.Status == SurveyStatus.AssignedSurvey && survey.AssignedUser.IsFeeCompany)
                {
                    survey.StatusCode = SurveyStatus.ReturningToCompany.Code;
                }
                else
                {
                    survey.RecommendedUserID = survey.AssignedUserID;
                    survey.AssignedUserID = Guid.Empty;
                    survey.StatusCode = SurveyStatus.UnassignedSurvey.Code;
                }
            }
            else if (survey.Status == SurveyStatus.AssignedReview)
            {
                survey.AssignedUserID = Guid.Empty;
                survey.StatusCode = SurveyStatus.UnassignedReview.Code;
            }

            //Add the reason if one was selected
            if (releaseType != null)
            {
                SurveyReleaseOwnership surveyReleaseOwnership = new SurveyReleaseOwnership(new Guid());
                surveyReleaseOwnership.SurveyID = survey.ID;
                surveyReleaseOwnership.TypeID = releaseType.ID;
                surveyReleaseOwnership.UserID = CurrentUser.ID;
                surveyReleaseOwnership.ReleasedDate = DateTime.Now;
                surveyReleaseOwnership.Save(trans);
            }

            //Add the comment if one is supplied
            if (txtReason != null && txtReason.Trim().Length > 0)
            {
                AddNote(trans, survey, txtReason, false, DateTime.Now, string.Empty);
                survey = survey.GetWritableInstance();
            }

            string historyEntry;
            if (this.CurrentUser != null && priorOwner.ID == this.CurrentUser.ID) // owner
            {
                historyEntry = string.Format("{0} released ownership of this survey.", GetCurrentUserName());
            }
            else // not the owner
            {
                historyEntry = string.Format("{0} released ownership of this survey from {1}.", GetCurrentUserName(), priorOwner.Name);
            }

            if (releaseType != null && releaseType.Name != null)
            {
                historyEntry += string.Format(" Reason for Releasing Ownership: {0}.", releaseType.Name);
            }

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnReleaseOwnership(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnReleaseOwnershipComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Releases the currently assigned vendor of this Survey and reassigns it to the appropriate party.
        /// </summary>
        public virtual void ReturnToCompany()
        {
            // a user must be assigned to the survey
            if (this.Survey.AssignedUser == null)
            {
                throw new InvalidOperationException("This action cannot be called when no user is assigned to the survey.");
            }

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnReturnToCompany(trans, survey);
                ReturnToCompany(trans, survey);
                OnReturnToCompanyComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ReturnToCompany(Transaction trans, Survey survey)
        {
            User priorOwner = survey.AssignedUser;

            // survey must be "in progress"
            if (!survey.Status.IsInList(SurveyStatus.AssignedSurvey, SurveyStatus.AwaitingAcceptance, SurveyStatus.ReturningToCompany))
            {
                throw new InvalidOperationException("This action cannot be called when the survey has a status of '" + survey.Status.Name + "'.");
            }

            // unassign the survey
            survey.AssignedUserID = Guid.Empty;
            survey.StatusCode = SurveyStatus.UnassignedSurvey.Code;

            string historyEntry = string.Format("{0} returned this {1}.", GetCurrentUserName(), survey.ServiceType.Name.ToLower());

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnReturnToCompany(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnReturnToCompanyComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Moves the survey to the review queue.
        /// </summary>
        public virtual void SendForReview(bool reviewNeeded)
        {
            SendForReview(false, reviewNeeded, null, null, DateTime.MinValue, null, null);
        }

        /// <summary>
        /// Moves the survey to the review queue.
        /// </summary>
        public virtual void SendForReview(bool futureVisitNeeded, bool reviewNeeded, SurveyType futureSurveyType, User futureUser, DateTime futureDate, string futurePurpose, string uwInstructions)
        {
            VerifyUserCanTakeAction(SurveyActionType.SendForReview);

            Survey survey = (this.Survey.IsReadOnly) ? this.Survey.GetWritableInstance() : this.Survey;
            bool isNewVisit = (futureVisitNeeded && !survey.FutureVisitNeeded);

            // cost and hours must be specified if being sent by a fee consultant
            if (survey.AssignedUser.IsFeeCompany)
            {
                if (survey.Hours == decimal.MinValue)
                {
                    throw new InvalidOperationException("Survey Hours properties must be specified.");
                }

                // round the hours and cost to 2 decimal places
                survey.Hours = decimal.Round(survey.Hours, 2);
                survey.FeeConsultantCost = (survey.FeeConsultantCost != decimal.MinValue) ? decimal.Round(survey.FeeConsultantCost, 2) : decimal.MinValue;
            }

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnSendForReview(trans, survey);
                Survey futureSurvey = SendForReview(trans, survey, futureVisitNeeded, reviewNeeded, futureSurveyType, futureUser, futureDate, futurePurpose);

                if (survey.ConsultantUser != null && !survey.Correction)
                {
                    survey = survey.GetWritableInstance();
                    survey.UnderwriterNotified = true;
                    OnSendForReviewComplete(trans, survey, futureSurvey, isNewVisit, reviewNeeded, uwInstructions);
                }

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual Survey SendForReview(Transaction trans, Survey survey, bool futureVisitNeeded, bool reviewNeeded, SurveyType futureSurveyType, User futureUser, DateTime futureDate, string futurePurpose)
        {
            string historyEntry;
            User actionUser = survey.AssignedUser;

            if (!reviewNeeded)
            {
                if (survey.Status == SurveyStatus.AssignedSurvey)
                {
                    if (survey.ReportCompleteDate == DateTime.MinValue)
                    {
                        survey.ReportCompleteDate = DateTime.Today;
                    }

                    survey.ConsultantUserID = survey.AssignedUserID;
                    survey.StatusCode = SurveyStatus.ClosedSurvey.Code;
                    survey.CompleteDate = DateTime.Today;
                    historyEntry = string.Format("Survey completed and closed by {0}.", GetCurrentUserName());
                }
                else
                {
                    throw new InvalidOperationException("This action cannot be called when the survey has a status of '" + survey.StatusCode + "'.");
                }
            }
            else
            {
                // assign survey to last reviewer if one is listed
                if (survey.ReviewerUserID != Guid.Empty)
                {
                    actionUser = survey.ReviewerUser;

                    survey.ConsultantUserID = survey.AssignedUserID;
                    survey.AssignedUserID = survey.ReviewerUserID;
                    survey.StatusCode = SurveyStatus.AssignedReview.Code;

                    historyEntry = string.Format("{0} sent survey back to {1} for review.", GetCurrentUserName(), survey.AssignedUser.Name);
                }
                else // no reviewer listed
                {
                    historyEntry = string.Format("{0} completed the request. ", GetCurrentUserName());

                    AssignmentRule[] rules = AssignmentRule.GetSortedArray("CompanyID = ? && SurveyStatusTypeCode = ?", "PriorityIndex ASC", _company.ID, SurveyStatusType.Review.Code);

                    // see if this survey should be assigned to a reviewer
                    bool reviewed = false;
                    // see if this survey matches any one of the assignment rules
                    foreach (AssignmentRule rule in rules)
                    {
                        if (rule.Filter.MatchesSurvey(survey)) // rule match
                        {
                            reviewed = true;
                            historyEntry += string.Format("Survey will be reviewed based on assignment rule {{{0}}}. ", rule.Filter.ToString());
                            if ((Berkley.BLC.Business.Utility.GetCompanyParameter("UsesInHouseSelection", CurrentUser.Company).ToUpper() == "TRUE") ? true : false && rule.ActionCode == "AI")
                            {
                                survey.InHouseSelection = true;
                            }
                            break;
                        }
                    }

                    // try to find a reviewer to assign based on territory
                    UserTerritory assignment = PickUserForSurvey(survey, SurveyStatusType.Review);
                    if (reviewed && assignment != null)
                    {
                        User targetUser = assignment.User;

                        survey.ConsultantUserID = survey.AssignedUserID;
                        survey.AssignedUserID = targetUser.ID;
                        survey.StatusCode = SurveyStatus.AssignedReview.Code;
                        historyEntry += string.Format("Survey assigned to {0} based on territory '{1}'.",
                            targetUser.Name, assignment.Territory.Name);
                    }
                    else
                    {
                        survey.ConsultantUserID = survey.AssignedUserID;
                        survey.AssignedUserID = Guid.Empty;
                        survey.StatusCode = SurveyStatus.UnassignedReview.Code;

                        if (reviewed)
                        {
                            historyEntry += "Survey could not be assigned to a reviewer based on the existing territories.";
                        }
                    }
                }
            }

            //New Future Visit
            Survey newSurvey = null;
            if (futureVisitNeeded && !survey.FutureVisitNeeded)
            {
                survey.FutureVisitNeeded = futureVisitNeeded;
                survey.FutureVisitSurveyTypeID = futureSurveyType.ID;
                survey.FutureVisitDueDate = futureDate;

                //Append to the prior history entry
                historyEntry += string.Format(" Future survey created in ARMS with a due date of {0}.", futureDate.ToShortDateString());

                //Create new survey
                newSurvey = new Survey(new Guid());
                newSurvey.CompanyID = survey.CompanyID;
                newSurvey.InsuredID = survey.InsuredID;
                newSurvey.PrimaryPolicyID = survey.PrimaryPolicyID;
                newSurvey.LocationID = survey.LocationID;
                newSurvey.PreviousSurveyID = survey.ID;
                newSurvey.TypeID = futureSurveyType.ID;
                newSurvey.AssignedUserID = futureUser.ID;
                newSurvey.ServiceTypeCode = survey.ServiceTypeCode;
                newSurvey.TransactionTypeID = survey.TransactionTypeID;
                newSurvey.StatusCode = SurveyStatus.AssignedSurvey.Code;
                newSurvey.CreateStateCode = CreateState.AssignedToConsultant.Code;
                newSurvey.CreateByInternalUserID = CurrentUser.ID;
                newSurvey.CreateDate = DateTime.Now;
                newSurvey.AssignDate = DateTime.Now;
                newSurvey.AcceptDate = DateTime.Now;
                newSurvey.DueDate = futureDate;
                newSurvey.RecsRequired = !SurveyTypeType.IsNotWrittenBusiness(futureSurveyType.Type);
                newSurvey.WorkflowSubmissionNumber = survey.WorkflowSubmissionNumber;
                newSurvey.UnderwriterCode = survey.UnderwriterCode;
                newSurvey.Priority = survey.Priority;
                newSurvey.Save(trans);

                //Include all the Policies/Coverages from the original survey
                SurveyPolicyCoverage[] oldCoverages = SurveyPolicyCoverage.GetArray("SurveyID = ?", survey.ID);
                foreach (SurveyPolicyCoverage oldCoverage in oldCoverages)
                {
                    SurveyPolicyCoverage newCoverage = new SurveyPolicyCoverage(newSurvey.ID, oldCoverage.LocationID, oldCoverage.PolicyID, oldCoverage.CoverageNameID);
                    newCoverage.ReportTypeCode = oldCoverage.ReportTypeCode;
                    newCoverage.Active = oldCoverage.Active;
                    newCoverage.Save(trans);
                }

                //Create history entry
                string newEntry = string.Format("Survey created in ARMS by {0} from survey #{1}.", GetCurrentUserName(), survey.Number);
                WriteHistoryEntry(trans, newSurvey, newEntry);

                //Add the purpose as a comment
                if (futurePurpose != string.Empty)
                {
                    SurveyNote note = new SurveyNote(new Guid());
                    note.SurveyID = newSurvey.ID;
                    note.UserID = CurrentUser.ID;
                    note.EntryDate = DateTime.Now;
                    note.InternalOnly = false;
                    note.Comment = string.Format("Purpose of Future Survey: {0}", futurePurpose);

                    note.Save(trans);
                }
            }

            //set the report complete date
            //Business Rule:  Do not reset the report complete date if this is a correction (don't want to add extra time).
            if (!survey.Correction && survey.ReportCompleteDate == DateTime.MinValue)
            {
                survey.ReportCompleteDate = DateTime.Now;
            }

            // save our changes
            SaveSurvey(trans, survey, historyEntry, actionUser);

            return newSurvey;
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnSendForReview(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnSendForReviewComplete(Transaction trans, Survey currentSurvey, Survey futureSurvey, bool isNewVisit, bool reviewNeeded, string uwInstructions)
        {
        }

        /// <summary>
        /// Changes the survey type.
        /// </summary>
        public virtual void ChangeSurveyType(SurveyTypeType surveyType)
        {
            VerifyUserCanTakeAction(SurveyActionType.ChangeSurveyType);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnChangeSurveyType(trans, survey);
                ChangeSurveyType(trans, survey, surveyType);
                OnChangeSurveyTypeComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ChangeSurveyType(Transaction trans, Survey survey, SurveyTypeType surveyType)
        {
            //set the history entry
            string historyEntry = string.Format("{0} changed the {1} type from {2} to {3}.", GetCurrentUserName(), survey.ServiceType.Name.ToLower(), survey.Type.Type.Name, surveyType.Name);

            survey.TypeID = SurveyType.GetOne(surveyType.Code, CurrentUser.Company);

            // save our changes
            SaveSurvey(trans, survey, historyEntry, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnChangeSurveyType(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnChangeSurveyTypeComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Consultant setting the details of the survey.
        /// </summary>
        public virtual void SetSurveyDetails(DateTime dateSurveyed, decimal hours, string gradingCode, string severityRatingCode, decimal calls, decimal cost, bool recsRequired, string invoiceNumber, DateTime invoiceDate)
        {
            VerifyUserCanTakeAction(SurveyActionType.SetSurveyDetails);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnSetSurveyDetails(trans, survey);
                SetSurveyDetails(trans, survey, dateSurveyed, hours, gradingCode, severityRatingCode, calls, cost, recsRequired, invoiceNumber, invoiceDate);
                OnSetSurveyDetailsComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void SetSurveyDetails(Transaction trans, Survey survey, DateTime dateSurveyed, decimal hours, string gradingCode, string severityRatingCode, decimal calls, decimal cost, bool recsRequired, string invoiceNumber, DateTime invoiceDate)
        {
            string historyEntry = string.Empty;
            string msgValue = string.Empty;

            //Determine what has been updated
            if (dateSurveyed != survey.SurveyedDate)
            {
                survey.SurveyedDate = dateSurveyed;
                msgValue = (survey.SurveyedDate != DateTime.MinValue) ? survey.SurveyedDate.ToShortDateString() : "nothing";

                historyEntry = string.Format("{0} set the date surveyed to {1}.", GetCurrentUserName(), msgValue);
                WriteHistoryEntry(trans, survey, historyEntry);
            }

            if (hours != survey.Hours)
            {
                survey.Hours = hours;
                msgValue = (survey.Hours != decimal.MinValue) ? survey.Hours.ToString() : "nothing";

                historyEntry = string.Format("{0} set the hours surveyed to {1}.", GetCurrentUserName(), msgValue);
                WriteHistoryEntry(trans, survey, historyEntry);
            }

            if (gradingCode != survey.GradingCode)
            {
                survey.GradingCode = gradingCode;
                msgValue = (survey.GradingCode != string.Empty) ? survey.Grading.Name : "nothing";

                historyEntry = string.Format("{0} set the overall grading to {1}.", GetCurrentUserName(), msgValue);
                WriteHistoryEntry(trans, survey, historyEntry);
            }

            if (severityRatingCode != survey.SeverityRatingCode)
            {
                survey.SeverityRatingCode = severityRatingCode;
                msgValue = (survey.SeverityRatingCode != string.Empty) ? survey.SeverityRating.Name : "nothing";

                historyEntry = string.Format("{0} set the severity rating to {1}.", GetCurrentUserName(), msgValue);
                WriteHistoryEntry(trans, survey, historyEntry);
            }

            if (calls != survey.CallCount)
            {
                survey.CallCount = calls;
                msgValue = (survey.CallCount != decimal.MinValue) ? survey.CallCount.ToString() : "nothing";

                historyEntry = string.Format("{0} set the {1} to {2}.", GetCurrentUserName(), Company.CallCountLabel.ToLower(), msgValue);
                WriteHistoryEntry(trans, survey, historyEntry);
            }

            if (cost != survey.FeeConsultantCost)
            {
                survey.FeeConsultantCost = cost;
                msgValue = (survey.FeeConsultantCost != decimal.MinValue) ? survey.FeeConsultantCost.ToString("C2") : "nothing";

                historyEntry = string.Format("{0} set the fee cost to {1}.", GetCurrentUserName(), msgValue);
                WriteHistoryEntry(trans, survey, historyEntry);
            }

            if (invoiceNumber != survey.InvoiceNumber)
            {
                survey.InvoiceNumber = invoiceNumber;
                msgValue = (survey.InvoiceNumber != string.Empty) ? survey.InvoiceNumber : "nothing";

                historyEntry = string.Format("{0} set the invoice number to {1}.", GetCurrentUserName(), msgValue);
                WriteHistoryEntry(trans, survey, historyEntry);
            }

            if (invoiceDate != survey.InvoiceDate)
            {
                survey.InvoiceDate = invoiceDate;
                msgValue = (survey.InvoiceDate != DateTime.MinValue) ? survey.InvoiceDate.ToShortDateString() : "nothing";

                historyEntry = string.Format("{0} set the invoice date to {1}.", GetCurrentUserName(), msgValue);
                WriteHistoryEntry(trans, survey, historyEntry);
            }

            survey.RecsRequired = recsRequired;

            // save our changes
            SaveSurvey(trans, survey, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnSetSurveyDetails(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnSetSurveyDetailsComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Begins the process of reopening a survey.
        /// </summary>
        /// <returns>A reference to the reopened survey.</returns>
        public virtual void Reopen(User newOwner)
        {
            if (newOwner == null) throw new ArgumentNullException("newOwner");
            if (this.CurrentUser == null) throw new InvalidOperationException("This action requires a current user to be defined.");

            VerifyUserCanTakeAction(SurveyActionType.ReopenSurvey);

            Survey survey = this.Survey.GetWritableInstance();

            //check if this survey was originally canceled
            bool wasCanceled = (survey.Status == SurveyStatus.CanceledSurvey);

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnReopen(trans, survey, newOwner);
                Reopen(trans, survey, newOwner);
                OnReopenComplete(trans, survey, newOwner, wasCanceled);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void Reopen(Transaction trans, Survey survey, User newOwner)
        {
            // status must be completed or canceled
            if (survey.Status != SurveyStatus.ClosedSurvey && survey.Status != SurveyStatus.CanceledSurvey)
            {
                throw new InvalidOperationException("This action cannot be called for surveys with a status of '" + survey.Status.Name + "'.");
            }

            //determine the survey status based on the hierachy of user's roles (survey, review)
            if (newOwner.Role.PermissionSet.CanWorkReviews)
            {
                survey.StatusCode = SurveyStatus.AssignedReview.Code;
            }
            else if (newOwner.Role.PermissionSet.CanWorkSurveys)
            {
                survey.StatusCode = SurveyStatus.AwaitingAcceptance.Code;
            }
            else
                throw new InvalidOperationException("This user does not have permission to work any type of survey.");

            // manually create a clone of this survey
            survey.AssignedUserID = newOwner.ID;
            survey.CompleteDate = DateTime.MinValue;
            survey.CanceledDate = DateTime.MinValue;
            survey.ReviewerCompleteDate = DateTime.MinValue;
            survey.UnderwriterNotified = false;

            //clear the consultant if not in review status
            if (survey.StatusCode != SurveyStatus.AssignedReview.Code)
            {
                survey.ConsultantUserID = Guid.Empty;
            }

            // create history entry
            string historyEntry = string.Format("{0} reopened {1} and assigned it to {2}.", GetCurrentUserName(), survey.ServiceType.Name.ToLower(), newOwner.Name);

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnReopen(Transaction trans, Survey survey, User consultant)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnReopenComplete(Transaction trans, Survey survey, User consultant, bool wasCanceled)
        {
        }

        /// <summary>
        /// Begins the process of setting the letter generation details.
        /// </summary>
        /// <returns>A reference to the reopened survey.</returns>
        public virtual void SetMailSentDate(MergeDocument doc, DateTime sentDate)
        {
            VerifyUserCanTakeAction(SurveyActionType.SetMailSent);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnSetMailSentDate(trans, survey);
                SurveyLetter surveyLetter = SetMailSentDate(trans, survey, doc, sentDate);
                OnSetMailSentDateComplete(trans, survey, surveyLetter, CurrentUser);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual SurveyLetter SetMailSentDate(Transaction trans, Survey survey, MergeDocument doc, DateTime sentDate)
        {
            //Check if a SurveyLetter object has already been created for this document
            SurveyLetter surveyLetter = SurveyLetter.GetOne("SurveyID = ? && MergeDocumentID = ?", survey.ID, doc.ID);

            string historyEntry;
            if (sentDate == DateTime.MinValue && surveyLetter != null)
            {
                //remove the entry
                surveyLetter.Delete(trans);
                historyEntry = string.Format("{0} removed the mail sent date for the {1}.", GetCurrentUserName(), doc.Name);
            }
            else if (surveyLetter == null)
            {
                surveyLetter = new SurveyLetter(Guid.NewGuid());
                surveyLetter.SurveyID = survey.ID;
                surveyLetter.MergeDocumentID = doc.ID;
                surveyLetter.SentDate = sentDate;

                surveyLetter.Save(trans);
                historyEntry = string.Format("{0} sent the {1} on {2:d}.", GetCurrentUserName(), doc.Name, sentDate);
            }
            else
            {
                surveyLetter = surveyLetter.GetWritableInstance();
                surveyLetter.SentDate = sentDate;

                surveyLetter.Save(trans);
                historyEntry = string.Format("{0} updated the {1} sent date to {2:d}.", GetCurrentUserName(), doc.Name, sentDate);
            }

            // save our changes
            SaveSurvey(trans, survey, historyEntry);

            return surveyLetter;
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnSetMailSentDate(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnSetMailSentDateComplete(Transaction trans, Survey survey, SurveyLetter surveyLetter, User currentUser)
        {
        }

        /// <summary>
        /// Begins the process of setting the letter generation details.
        /// </summary>
        /// <returns>A reference to the reopened survey.</returns>
        public virtual void SetMailReceivedDate(DateTime newDate)
        {
            VerifyUserCanTakeAction(SurveyActionType.SetMailReceived);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnSetMailReceivedDate(trans, survey);
                SetMailReceivedDate(trans, survey, newDate);
                OnSetMailReceivedDateComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void SetMailReceivedDate(Transaction trans, Survey survey, DateTime newDate)
        {
            // make the change
            survey.MailReceivedDate = newDate;

            string historyEntry;
            if (newDate.Date == DateTime.MinValue)
                historyEntry = string.Format("{0} removed the mail received date.", GetCurrentUserName());
            else
                historyEntry = string.Format("{0} set the mail received date to {1:d}.", GetCurrentUserName(), newDate);

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnSetMailReceivedDate(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnSetMailReceivedDateComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Toggles a boolean value.
        /// </summary>
        public virtual void ToggleValue(SurveyActionType actionType)
        {
            VerifyUserCanTakeAction(actionType);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnToggleValue(trans, survey);
                ToggleValue(trans, survey, actionType);
                OnToggleValueComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ToggleValue(Transaction trans, Survey survey, SurveyActionType actionType)
        {
            bool newValue;

            if (actionType == SurveyActionType.NonProductive)
            {
                newValue = !survey.NonProductive;
                survey.NonProductive = newValue;
            }
            else if (actionType == SurveyActionType.Review)
            {
                newValue = !survey.Review;
                survey.Review = newValue;
            }
            else
            {
                throw new InvalidOperationException("This action '" + actionType.ActionName + " cannot be changed'.");
            }

            string boolText = (newValue) ? "Yes" : "No";
            string historyEntry = string.Format("{0} {1} to '{2}'.", GetCurrentUserName(), actionType.ActionName, boolText);

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnToggleValue(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnToggleValueComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Takes an existing FileNet document and refernces it in ARMS.
        /// </summary>
        /// <param name="fileNetReference">The FileNet reference number.</param>
        /// <param name="fileName">Filename of the file being attached.</param>
        /// <param name="mimeType">MIME Type of the file beign attached.</param>
        /// <param name="contentStream">Stream of the file content.</param>
        public void AttachDocument(string fileNetReference, string fileName, string mimeType, Stream contentStream, string docRemarks)
        {
            if (fileNetReference == null) throw new ArgumentNullException("fileNetReference");
            if (fileName == null) throw new ArgumentNullException("fileName");
            if (mimeType == null) throw new ArgumentNullException("mimeType");
            if (contentStream == null) throw new ArgumentNullException("contentStream");
            if (contentStream.Length == 0) throw new ArgumentException("Content stream cannot be zero length.");

            VerifyUserCanTakeAction(SurveyActionType.AddDocument);
            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnAttachDocumentIndex(trans, survey, fileNetReference, docRemarks);
                AttachDocument(trans, survey, fileName, mimeType, contentStream, fileNetReference, false, null);
                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Attaches a document to this Survey and stores it in FileNet for later retrieval.
        /// </summary>
        /// <param name="fileName">Filename of the file being attached.</param>
        /// <param name="mimeType">MIME Type of the file beign attached.</param>
        /// <param name="contentStream">Stream of the file content.</param>
        /// <param name="docTypeCode">The document type if this document will be attached to a Workflow transaction, default is null.</param>
        /// <param name="docRemarks">Remarks tied to the document in the imaging system.</param>
        /// <param name="canRemoveFromFileNet">Flag indicating if the file can be removed from FileNet.</param>
        public string AttachDocument(string fileName, string mimeType, Stream contentStream, DocType docType, string docRemarks, bool canRemoveFromFileNet)
        {
            if (fileName == null) throw new ArgumentNullException("fileName");
            if (mimeType == null) throw new ArgumentNullException("mimeType");
            if (contentStream == null) throw new ArgumentNullException("contentStream");
            if (contentStream.Length == 0) throw new ArgumentException("Content stream cannot be zero length.");

            VerifyUserCanTakeAction(SurveyActionType.AddDocument);

            Survey survey = this.Survey.GetWritableInstance();
            string fileNetRef;

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                fileNetRef = OnAttachDocument(trans, survey, fileName, mimeType, contentStream, docType, docRemarks);
                AttachDocument(trans, survey, fileName, mimeType, contentStream, fileNetRef, canRemoveFromFileNet, docType);
                OnAttachDocumentComplete(survey, fileNetRef, docType);

                trans.Commit();
            }

            this.Survey = survey;

            return fileNetRef;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected void AttachDocument(Transaction trans, Survey survey, string fileName, string mimeType, Stream contentStream, string fileNetRef, bool canRemoveFromFileNet, DocType docType)
        {
            // remove any pathing info from the specified filename
            fileName = Path.GetFileName(fileName);

            // create a record of this file with the survey and record this action in history
            SurveyDocument doc = new SurveyDocument(Guid.NewGuid());
            doc.SurveyID = this.Survey.ID;
            doc.DocumentName = fileName;
            doc.MimeType = mimeType;
            doc.SizeInBytes = contentStream.Length;
            doc.FileNetReference = fileNetRef;
            doc.UploadUserID = (this.CurrentUser != null) ? this.CurrentUser.ID : Guid.Empty;
            //doc.UploadUserName = (this.CurrentUser != null && this.CurrentUser.IsUnderwriter) ? this.CurrentUser.Username : string.Empty;
            doc.UploadedOn = DateTime.Now;
            doc.DocTypeCode = (docType != null) ? docType.Code : String.Empty;
            doc.FileNetRemovable = canRemoveFromFileNet;
            doc.IsRemoved = false;
            doc.Save(trans);

            string historyEntry = string.Format("{0} attached file '{1}'.", GetCurrentUserName(), fileName);

            WriteHistoryEntry(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual string OnAttachDocument(Transaction trans, Survey survey, string fileName, string mimeType, Stream contentStream, DocType docType, string docRemarks)
        {
            string fileNetRef = string.Empty;
            try
            {
                // remove any pathing info from the specified filename
                fileName = Path.GetFileName(fileName);

                // save the doc to the imaging server and get the FileNet reference
                ImagingHelper imaging = new ImagingHelper(this.Survey.Company);
                fileNetRef = imaging.StoreFileImage(fileName, mimeType, contentStream, null, null, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, survey.Insured.ClientID, null, survey.WorkflowSubmissionNumber);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save file '" + fileName + "' of type '" + mimeType + "' to FileNet.  See inner exception for details.", ex);
            }

            return fileNetRef;
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnAttachDocumentIndex(Transaction trans, Survey survey, string fileNetReference, string docRemarks)
        {
            try
            {
                // save the doc to the imaging server and get the FileNet reference
                ImagingHelper imaging = new ImagingHelper(this.Survey.Company);
                imaging.UpdateFileImage(fileNetReference, docRemarks);
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to update index for document # '" + fileNetReference + "'.  See inner exception for details.", ex);
            }
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnAttachDocumentComplete(Survey survey, string documentReference, DocType docType)
        {
        }

        /// <summary>
        /// Removes the specified survey document.
        /// </summary>
        public virtual void RemoveDocument(SurveyDocument doc)
        {
            VerifyUserCanTakeAction(SurveyActionType.RemoveDocument);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnRemoveDocument(trans, survey);
                RemoveDocument(trans, survey, doc);
                OnRemoveDocumentComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void RemoveDocument(Transaction trans, Survey survey, SurveyDocument doc)
        {
            string historyEntry = string.Format("{0} removed file '{1}'.", GetCurrentUserName(), doc.DocumentName);

            //try and remove the document from FileNet
            if (doc.FileNetRemovable)
            {
                ImagingHelper imaging = new ImagingHelper(Company);
                imaging.RemoveFileImage(doc.FileNetReference);
            }

            // mark the doc as deleted in ARMS
            SurveyDocument removedDoc = doc.GetWritableInstance();
            removedDoc.IsRemoved = true;
            removedDoc.Save(trans);

            //write the history entry
            WriteHistoryEntry(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnRemoveDocument(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnRemoveDocumentComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Removes the specified survey document.
        /// </summary>
        public virtual void RemoveHistoryEntry(SurveyHistory surveyHistory)
        {
            VerifyUserCanTakeAction(SurveyActionType.RemoveDocument);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnRemoveHistoryEntry(trans, survey);
                RemoveHistoryEntry(trans, survey, surveyHistory);
                OnRemoveHistoryEntryComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void RemoveHistoryEntry(Transaction trans, Survey survey, SurveyHistory surveyHistory)
        {
            string historyEntry = string.Format("{0} removed history entry that was created on {1}.", GetCurrentUserName(), surveyHistory.EntryDate.ToShortDateString());

            // mark the doc as deleted in ARMS
            SurveyHistory removedHistory = surveyHistory.GetWritableInstance();
            removedHistory.IsRemoved = true;
            removedHistory.Save(trans);

            //write the history entry
            WriteHistoryEntry(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnRemoveHistoryEntry(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnRemoveHistoryEntryComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Attaches a comment from a user to an Survey as a note.
        /// </summary>
        /// <param name="comment">The comment to be added as a note to the survey.</param>
        /// <param name="internalOnly">Flag to determine if note is internal only.</param>
        /// <param name="commentDate">The timestamp of the entry.</param>
        /// <param name="underwriterUsername">The optional username of the underwriter, if added by a UW.</param>
        public void AddNote(string comment, bool internalOnly, DateTime commentDate, string underwriterUsername)
        {
            if (comment == null || comment.Length == 0) throw new ArgumentNullException("comment");

            VerifyUserCanTakeAction(SurveyActionType.AddNote);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnAddNote(trans, survey, comment);
                AddNote(trans, survey, comment, internalOnly, commentDate, underwriterUsername);
                OnAddNoteComplete(trans, survey, comment);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void AddNote(Transaction trans, Survey survey, string comment, bool internalOnly, DateTime commentDate, string underwriterUsername)
        {
            // build and save the comment
            SurveyNote note = new SurveyNote(Guid.NewGuid());
            note.SurveyID = survey.ID;
            note.UserID = this.CurrentUser.ID;
            note.UserName = (this.CurrentUser.IsUnderwriter && !String.IsNullOrEmpty(underwriterUsername)) ? underwriterUsername : string.Empty;
            note.EntryDate = commentDate;
            note.InternalOnly = internalOnly;
            note.Comment = comment;
            note.NoteByConsultant = this.CurrentUser.Role.IsConsultant;
            note.Save(trans);

            // Set the last modified date for the field systems
            survey.LastModifiedOn = DateTime.Now;
            survey.Save(trans);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnAddNote(Transaction trans, Survey survey, string comment)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnAddNoteComplete(Transaction trans, Survey survey, string comment)
        {
        }

        /// <summary>
        /// Removes the specified survey note.
        /// </summary>
        public virtual void RemoveNote(SurveyNote note)
        {
            VerifyUserCanTakeAction(SurveyActionType.RemoveNote);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnRemoveNote(trans, survey);
                RemoveNote(trans, survey, note);
                OnRemoveNoteComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void RemoveNote(Transaction trans, Survey survey, SurveyNote note)
        {
            string historyEntry = string.Format("{0} removed a comment that was entered on {1} by {2}.", GetCurrentUserName(), note.EntryDate.ToString("g"), (note.User != null) ? note.User.Name : survey.CreateByExternalUserName);

            //delete the note
            note.Delete(trans);

            //write the history entry
            WriteHistoryEntry(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnRemoveNote(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnRemoveNoteComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Deletes this survey (by calling the Delete method).
        /// </summary>
        public virtual void Delete()
        {
            VerifyUserCanTakeAction(SurveyActionType.DeleteSurvey);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnDelete(trans, survey);
                Delete(trans, survey);
                OnDeleteComplete(trans, survey, CurrentUser);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void Delete(Transaction trans, Survey survey)
        {
            survey.CreateStateCode = CreateState.InProgress.Code;

            // save our changes
            SaveSurvey(trans, survey, string.Format("Survey deleted by {0}.", GetCurrentUserName()));
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnDelete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnDeleteComplete(Transaction trans, Survey survey, User currentUser)
        {
        }

        /// <summary>
        /// Completing a survey review.
        /// </summary>
        public virtual void SurveyReview()
        {
            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnSurveyReview(trans, survey);
                SurveyReview(trans, survey);
                OnSurveyReviewComplete(trans, survey, CurrentUser);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void SurveyReview(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnSurveyReview(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnSurveyReviewComplete(Transaction trans, Survey survey, User currentUser)
        {
        }

        /// <summary>
        /// Cancels this survey (by calling the Cancel method).
        /// </summary>
        public virtual void Cancel(string explainText)
        {
            VerifyUserCanTakeAction(SurveyActionType.CancelSurvey);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnCancel(trans, survey);
                Cancel(trans, survey, explainText);
                OnCancelComplete(trans, survey, explainText, CurrentUser);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void Cancel(Transaction trans, Survey survey, string explainText)
        {
            if (survey.Status.Type == SurveyStatusType.Survey && survey.AssignedUser != null)
            {
                survey.ConsultantUserID = survey.AssignedUserID;
            }

            survey.AssignedUserID = this.CurrentUser.ID;
            survey.StatusCode = SurveyStatus.CanceledSurvey.Code;
            survey.CompleteDate = DateTime.Today;
            survey.CanceledDate = DateTime.Today;
            survey.UnderwriterNotified = true;

            string historyEntry = string.Format("{0} canceled by {1}.", survey.ServiceType.Name, GetCurrentUserName());

            //Add the explanation as a comment
            if (explainText != string.Empty)
            {
                SurveyNote note = new SurveyNote(new Guid());
                note.SurveyID = survey.ID;
                note.UserID = CurrentUser.ID;
                note.EntryDate = DateTime.Now;
                note.InternalOnly = false;
                note.Comment = string.Format("Reason for Cancellation: {0}", explainText);

                note.Save(trans);
            }

            // If this is a service visit, add a history entry to the service plan
            if (survey.ServiceType.Code == ServiceType.ServiceVisit.Code)
            {
                ServicePlanSurvey servicePlanSurvey = ServicePlanSurvey.GetOne("SurveyID = ?", survey.ID);

                ServicePlanHistory planHistory = new ServicePlanHistory(Guid.NewGuid());
                planHistory.ServicePlanID = servicePlanSurvey.ServicePlanID;
                planHistory.EntryText = string.Format("Service visit {0} canceled by {1}.", survey.Number, GetCurrentUserName());
                planHistory.EntryDate = DateTime.Now;
                planHistory.Save(trans);
            }

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnCancel(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnCancelComplete(Transaction trans, Survey survey, string explainText, User currentUser)
        {
        }

        /// <summary>
        /// Finalizes the survey and closes it out.
        /// </summary>
        public virtual void Close()
        {
            if (!this.Survey.UnderwriterNotified)
            {
                VerifyUserCanTakeAction(SurveyActionType.Close);
            }

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnClose(trans, survey);
                Close(trans, survey);
                OnCloseComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void Close(Transaction trans, Survey survey)
        {
            string historyEntry;
            if (survey.Status == SurveyStatus.AssignedReview)
            {
                survey.ReviewerUserID = survey.AssignedUserID;
                survey.StatusCode = SurveyStatus.ClosedSurvey.Code;
                survey.CompleteDate = DateTime.Today;
                survey.UnderwriterNotified = true;
                survey.ReviewerCompleteDate = DateTime.Today;
                historyEntry = string.Format("Review completed and survey closed by {0}.", GetCurrentUserName());
            }
            else if (survey.Status == SurveyStatus.AssignedSurvey)
            {
                if (survey.ReportCompleteDate == DateTime.MinValue)
                {
                    survey.ReportCompleteDate = DateTime.Today;
                }

                survey.ConsultantUserID = survey.AssignedUserID;
                survey.StatusCode = SurveyStatus.ClosedSurvey.Code;
                survey.CompleteDate = DateTime.Today;
                survey.UnderwriterNotified = true;
                historyEntry = string.Format("{0} completed and closed by {1}.", survey.ServiceType.Name, GetCurrentUserName());
            }
            else
            {
                throw new InvalidOperationException("This action cannot be called when the survey has a status of '" + survey.StatusCode + "'.");
            }

            // If this is a service visit, add a history entry to the service plan
            if (survey.ServiceType.Code == ServiceType.ServiceVisit.Code)
            {
                ServicePlanSurvey servicePlanSurvey = ServicePlanSurvey.GetOne("SurveyID = ?", survey.ID);

                ServicePlanHistory planHistory = new ServicePlanHistory(Guid.NewGuid());
                planHistory.ServicePlanID = servicePlanSurvey.ServicePlanID;
                planHistory.EntryText = string.Format("Service visit {0} closed by {1}.", survey.Number, GetCurrentUserName());
                planHistory.EntryDate = DateTime.Now;
                planHistory.Save(trans);
            }

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnClose(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnCloseComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Finalizes the survey and closes it out.
        /// </summary>
        public virtual void CloseAndNotify(string text)
        {
            VerifyUserCanTakeAction(SurveyActionType.CloseAndNotify);

            Survey survey = this.Survey.GetWritableInstance();

            //get the rec count
            string command = string.Format("EXEC Recs_GetCompletedCount @SurveyID = '{0}'", _survey.ID);
            string recRount = DB.Engine.GetDataSet(command).Tables[0].Rows[0][0].ToString();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnCloseAndNotify(trans, survey);
                CloseAndNotify(trans, survey, text);
                OnCloseAndNotifyComplete(trans, survey, text, CurrentUser, recRount);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void CloseAndNotify(Transaction trans, Survey survey, string text)
        {
            string historyEntry;
            if (survey.Status == SurveyStatus.AssignedReview)
            {
                survey.ReviewerUserID = survey.AssignedUserID;
                survey.StatusCode = SurveyStatus.ClosedSurvey.Code;
                survey.CompleteDate = DateTime.Today;
                survey.UnderwriterNotified = true;
                survey.ReviewerCompleteDate = DateTime.Today;
                historyEntry = string.Format("Review completed and {0} closed by {1}.", survey.ServiceType.Name.ToLower(), GetCurrentUserName());
            }
            else if (survey.Status == SurveyStatus.AssignedSurvey)
            {
                if (survey.ReportCompleteDate == DateTime.MinValue)
                {
                    survey.ReportCompleteDate = DateTime.Today;
                }

                survey.ConsultantUserID = survey.AssignedUserID;
                survey.StatusCode = SurveyStatus.ClosedSurvey.Code;
                survey.CompleteDate = DateTime.Today;
                survey.UnderwriterNotified = true;
                historyEntry = string.Format("{0} completed and closed by {1}.", survey.ServiceType.Name, GetCurrentUserName());
            }
            else
            {
                throw new InvalidOperationException("This action cannot be called when the survey has a status of '" + survey.StatusCode + "'.");
            }

            //Add the note to the uw as a comment
            if (text != null && text.Trim().Length > 0)
            {
                SurveyNote note = new SurveyNote(new Guid());
                note.SurveyID = survey.ID;
                note.UserID = CurrentUser.ID;
                note.EntryDate = DateTime.Now;
                note.InternalOnly = false;
                note.Comment = string.Format("Instructions to UW upon closing {0}: {1}", survey.ServiceType.Name.ToLower(), text);

                note.Save(trans);
            }

            // If this is a service visit, add a history entry to the service plan
            if (survey.ServiceType.Code == ServiceType.ServiceVisit.Code)
            {
                ServicePlanSurvey servicePlanSurvey = ServicePlanSurvey.GetOne("SurveyID = ?", survey.ID);

                ServicePlanHistory planHistory = new ServicePlanHistory(Guid.NewGuid());
                planHistory.ServicePlanID = servicePlanSurvey.ServicePlanID;
                planHistory.EntryText = string.Format("Service visit {0} closed by {1}.", survey.Number, GetCurrentUserName());
                planHistory.EntryDate = DateTime.Now;
                planHistory.Save(trans);
            }

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnCloseAndNotify(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnCloseAndNotifyComplete(Transaction trans, Survey survey, string explainText, User currentUser, string recRount)
        {
        }

        #region UI Action Enums
        /// <summary>
        /// The UI action type.
        /// </summary>
        public enum UIAction
        {
            /// <summary>
            /// Updating a record in an entity.
            /// </summary>
            Update,
            /// <summary>
            /// Adding a record to an entity.
            /// </summary>
            Add,
            /// <summary>
            /// Removing a record from an entity.
            /// </summary>
            Remove
        }
        #endregion

        //Added By Vilas Meshram: 02/20/2013: Squish #21347 :Due Date Extension
        #region UI AproveDeny Enums
        /// <summary>
        /// The UI action ApproveDeny type.
        /// </summary>
        public enum ApproveDeny
        {
            /// <summary>
            /// Approve the New Due date.
            /// </summary>
            Approve,
            /// <summary>
            /// Deny the New Due date.
            /// </summary>
            Deny,
            /// <summary>
            /// Approve the Due date to new Due date.
            /// </summary>
            NewDate
        }
        #endregion

        //Added By Vilas Meshram: 02/25/2013: Squish #21347 :Due Date Extension/Request Extension
        #region UI ReleaseOwnershipType Enums
        /// <summary>
        /// The UI action ReleaseOwnershipType type.
        /// </summary>
        public enum ReleaseOwnershipType
        {
            /// <summary>
            /// Request Due Date Change.
            /// </summary>
            RequestDueDateChange,
            /// <summary>
            /// Request Extension.
            /// </summary>
            RequestExtension,
            /// <summary>
            /// Request none.
            /// </summary>
            none

           }
        #endregion



        /// <summary>
        /// Updates the history regarding rec changes.
        /// </summary>
        public virtual void RecManagement(SurveyRecommendation rec, UIAction action)
        {
            VerifyUserCanTakeAction(SurveyActionType.ManageRecs);

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnRecManagement(trans, Survey);
                RecManagement(trans, Survey, rec, action);
                OnRecManagementComplete(trans, Survey);

                trans.Commit();
            }
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void RecManagement(Transaction trans, Survey survey, SurveyRecommendation rec, UIAction action)
        {
            //Create history entry
            string historyEntry;
            if (action == UIAction.Update)
            {
                historyEntry = string.Format("{0} updated rec {1} to '{2}'.", GetCurrentUserName(), rec.RecommendationNumber, rec.RecStatus.Type.Name);
            }
            else if (action == UIAction.Add)
            {
                historyEntry = string.Format("{0} added rec {1}.", GetCurrentUserName(), rec.RecommendationNumber);
            }
            else if (action == UIAction.Remove)
            {
                historyEntry = string.Format("{0} removed rec {1}.", GetCurrentUserName(), rec.RecommendationNumber);
            }
            else
            {
                throw new NotSupportedException("Not a supported UI action taken.");
            }

            WriteHistoryEntry(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnRecManagement(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnRecManagementComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Updates the history regarding rec changes.
        /// </summary>
        public virtual void ObjectiveManagement(Objective objective, UIAction action)
        {
            VerifyUserCanTakeAction(SurveyActionType.ManageObjectives);

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnObjectiveManagement(trans, Survey);
                ObjectiveManagement(trans, Survey, objective, action);
                OnObjectiveManagementComplete(trans, Survey);

                trans.Commit();
            }
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ObjectiveManagement(Transaction trans, Survey survey, Objective objective, UIAction action)
        {
            //Create history entry
            string historyEntry;
            if (action == UIAction.Update)
            {
                historyEntry = string.Format("{0} updated objective {1}.", GetCurrentUserName(), objective.Number);
            }
            else if (action == UIAction.Add)
            {
                historyEntry = string.Format("{0} added objective {1}.", GetCurrentUserName(), objective.Number);
            }
            else if (action == UIAction.Remove)
            {
                historyEntry = string.Format("{0} removed objective {1}.", GetCurrentUserName(), objective.Number);
            }
            else
            {
                throw new NotSupportedException("Not a supported UI action taken.");
            }

            WriteHistoryEntry(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnObjectiveManagement(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnObjectiveManagementComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Updates the history regarding activity changes.
        /// </summary>
        public virtual void ActivityManagement(SurveyActivity activity, UIAction action)
        {
            VerifyUserCanTakeAction(SurveyActionType.AccountManagement);

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnActivityManagement(trans, Survey);
                ActivityManagement(trans, Survey, activity, action);
                OnActivityManagementComplete(trans, Survey);

                trans.Commit();
            }
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ActivityManagement(Transaction trans, Survey survey, SurveyActivity activity, UIAction action)
        {
            //Create history entry
            string historyEntry;
            if (action == UIAction.Update)
            {
                historyEntry = string.Format("{0} updated the {1} {2} activity time.", GetCurrentUserName(), activity.ActivityDate.ToShortDateString(), activity.ActivityType.Name);
            }
            else if (action == UIAction.Add)
            {
                historyEntry = string.Format("{0} entered a new {1} activity time of {2} hours.", GetCurrentUserName(), activity.ActivityType.Name, activity.ActivityHours);
            }
            else if (action == UIAction.Remove)
            {
                historyEntry = string.Format("{0} removed the {1} {2} activity time.", GetCurrentUserName(), activity.ActivityDate.ToShortDateString(), activity.ActivityType.Name);
            }
            else
            {
                throw new NotSupportedException("Not a supported UI action taken.");
            }

            WriteHistoryEntry(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnActivityManagement(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnActivityManagementComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Updates the history and set the quality assessment.
        /// </summary>
        public virtual void SetQualityAssessment(SurveyQuality surveyQuality, string explainText)
        {
            VerifyUserCanTakeAction(SurveyActionType.QualityAssessment);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnSetQualityAssessment(trans, survey);
                SetQualityAssessment(trans, survey, surveyQuality, explainText);
                OnSetQualityAssessmentComplete(trans, survey);

                trans.Commit();
            }
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void SetQualityAssessment(Transaction trans, Survey survey, SurveyQuality surveyQuality, string explainText)
        {
            //Create history entry
            string historyEntry;
            if (survey.Quality != null && survey.QualityID != Guid.Empty)
            {
                historyEntry = string.Format("{0} updated the quality assessment from {1} to {2}", GetCurrentUserName(), survey.Quality.Type.Name, surveyQuality.Type.Name);
            }
            else
            {
                historyEntry = string.Format("{0} set the quality assessment to {1}.", GetCurrentUserName(), surveyQuality.Type.Name);
            }

            //set the QA
            survey.QualityID = surveyQuality.ID;

            //Add the explanation as a comment
            if (explainText != string.Empty)
            {
                SurveyNote note = new SurveyNote(new Guid());
                note.SurveyID = survey.ID;
                note.UserID = CurrentUser.ID;
                note.EntryDate = DateTime.Now;
                note.InternalOnly = true;
                note.Comment = string.Format("QA Comment: {0}", explainText);

                note.Save(trans);
            }

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnSetQualityAssessment(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnSetQualityAssessmentComplete(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Changes the due date of the survey and records the change in history.
        /// </summary>
        /// <param name="newDate">New due date for this survey.</param>
        public virtual void ChangeDueDate(DateTime newDate, string strReason, ReleaseOwnershipType releaseType)
        {
            if (newDate == DateTime.MinValue) throw new ArgumentNullException("newDate");

            VerifyUserCanTakeAction(SurveyActionType.EditDueDate);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnChangeDueDate(trans, survey, newDate, strReason, releaseType);
                ChangeDueDate(trans, survey, newDate, strReason, releaseType);
                OnChangeDueDateComplete(trans, survey, newDate, strReason, releaseType);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ChangeDueDate(Transaction trans, Survey survey, DateTime newDate, string txtReason, ReleaseOwnershipType releaseType)
        {
            string historyEntry;
            string strReleaseType = string.Empty;
            string strCommnets = string.Empty; 

            if ((Utility.GetCompanyParameter("UsesDueDateHistory", CurrentUser.Company).ToUpper() == "TRUE") ? true : false)
            {
                DueDateHistory hist = new DueDateHistory(Guid.NewGuid());
                hist.CompanyID = CurrentUser.CompanyID;
                hist.RequestedByUserID = CurrentUser.ID;
                hist.SurveyID = survey.ID;
                hist.CurrentDueDate = survey.DueDate;
                hist.UpdatedDueDate = newDate;
                hist.CreateDateTime = DateTime.Now;   
                hist.Save();
                
                if (releaseType == ReleaseOwnershipType.RequestDueDateChange)
                {
                    strReleaseType = "requested due date change ";
                    strCommnets = "Request Due Date Change Reason: ";
                }
                else if (releaseType == ReleaseOwnershipType.RequestExtension)
                {
                    strReleaseType = "requested extension change ";
                    strCommnets = "Request Extension Change Reason: ";
                }

                historyEntry = string.Format("{0} " + strReleaseType + " from {1:d} to {2:d}. ", GetCurrentUserName(), survey.DueDate, newDate);
                historyEntry += historyEntry + strCommnets + txtReason;
                 
                //historyEntry = string.Format("{0} requested due date change from {1:d} to {2:d}. ", GetCurrentUserName(), survey.DueDate, newDate);
                survey.ApprovalCode = "D";
            }
            else
            {
                historyEntry = string.Format("{0} changed the due date from {1:d} to {2:d}.", GetCurrentUserName(), survey.DueDate, newDate); 
            }

            //Add the comment if one is supplied
            if (txtReason != null && txtReason.Trim().Length > 0)
            {
                AddNote(trans, survey, txtReason, false, DateTime.Now, string.Empty);
                survey = survey.GetWritableInstance();
            }

            // make the change
            survey.DueDate = newDate.Date;
            // save our changes
            SaveSurvey(trans, survey, historyEntry);

        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnChangeDueDate(Transaction trans, Survey survey, DateTime newDate, string strReason, ReleaseOwnershipType releaseType)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
         protected virtual void OnChangeDueDateComplete(Transaction trans, Survey survey, DateTime newDate, string strReason, ReleaseOwnershipType releaseType)
        {
        }

         //Modified by Vilas Meshram: 02/23/2013 : Squish #21347 :Due Date Extension
         public virtual void ApproveDenyDueDate(DueDateHistory[] hist, ApproveDeny action, DateTime newDueDate, string strApprovDenyReason, ReleaseOwnershipType releaseType)
         //public virtual void ApproveDenyDueDate(DueDateHistory[] hist, bool bApprove)
        {
            string historyEntry;
            
            Survey survey = this.Survey.GetWritableInstance();
            bool fp = true;

            string strReleaseType = string.Empty;

            if (releaseType == ReleaseOwnershipType.RequestDueDateChange)
                strReleaseType = "requested due date change";
            else if (releaseType == ReleaseOwnershipType.RequestExtension)
                strReleaseType = "requested extension change";
            
                if (hist != null && hist.Length > 0)
                {
                    foreach (DueDateHistory h in hist)
                    {
                        using (Transaction trans = DB.Engine.BeginTransaction())
                        {
                            DueDateHistory wh = h.GetWritableInstance();
                            if (action == ApproveDeny.Approve)
                            {
                                wh.Approved = true;
                                wh.ApprovalByUserID = CurrentUser.ID;
                                if (fp)
                                {
                                    historyEntry = string.Format("{0} approved " + strReleaseType + " from {1:d} to {2:d}.", CurrentUser.Name, h.CurrentDueDate, h.UpdatedDueDate);
                                    //historyEntry = string.Format("{0} approved due date change from {1:d} to {2:d}.", CurrentUser.Name, h.CurrentDueDate, h.UpdatedDueDate);
                                    historyEntry += " Comments: " + strApprovDenyReason;
                                    survey.ApprovalCode = null;
                                    SaveSurvey(trans, survey, historyEntry);
                                }
                            }
                            else if (action == ApproveDeny.Deny)
                            {
                                wh.ApprovalByUserID = CurrentUser.ID;
                                if (fp)
                                {
                                    historyEntry = string.Format("{0} denied " + strReleaseType + " from {1:d} to {2:d}.  Reverting due date change", CurrentUser.Name, h.CurrentDueDate, h.UpdatedDueDate);
                                    //historyEntry = string.Format("{0} denied due date change from {1:d} to {2:d}.  Reverting due date change", CurrentUser.Name, h.CurrentDueDate, h.UpdatedDueDate);
                                    historyEntry += " Comments: " + strApprovDenyReason;
                                    survey.DueDate = h.CurrentDueDate;
                                    survey.ApprovalCode = null;
                                    SaveSurvey(trans, survey, historyEntry);
                                }
                            }
                            else if (action == ApproveDeny.NewDate)
                            {
                                wh.Approved = true;
                                wh.ApprovalByUserID = CurrentUser.ID;

                                if (fp)
                                {
                                    historyEntry = string.Format("{0} approved " + strReleaseType + " from {1:d} to {2:d}.", CurrentUser.Name, h.CurrentDueDate, newDueDate);
                                    //historyEntry = string.Format("{0} approved due date change from {1:d} to {2:d}.", CurrentUser.Name, h.CurrentDueDate, newDueDate);
                                    historyEntry += " Comments: " + strApprovDenyReason;
                                    survey.ApprovalCode = null;
                                    survey.DueDate = newDueDate;
                                    SaveSurvey(trans, survey, historyEntry);
                                }
                            }

                            wh.Save(trans);
                            fp = false;
                            trans.Commit();
                        }
                    }
                }
                 
        }

        /// <summary>
        /// Changes the assigned date of the survey and records the change in history.
        /// </summary>
        /// <param name="newDate">New assigned date for this survey.</param>
        public virtual void ChangeAssignedDate(DateTime newDate)
        {
            if (newDate == DateTime.MinValue) throw new ArgumentNullException("newDate");

            VerifyUserCanTakeAction(SurveyActionType.EditAssignedDate);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnChangeAssignedDate(trans, survey, newDate);
                ChangeAssignedDate(trans, survey, newDate);
                OnChangeAssignedDateComplete(trans, survey, newDate);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ChangeAssignedDate(Transaction trans, Survey survey, DateTime newDate)
        {
            // make the change
            string historyEntry = string.Format("{0} changed the assigned date from {1:d} to {2:d}.", GetCurrentUserName(), survey.AssignDate, newDate);
            survey.AssignDate = newDate.Date;

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnChangeAssignedDate(Transaction trans, Survey survey, DateTime newDate)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnChangeAssignedDateComplete(Transaction trans, Survey survey, DateTime newDate)
        {
        }

        /// <summary>
        /// Changes the accepted date of the survey and records the change in history.
        /// </summary>
        /// <param name="newDate">New accepted date for this survey.</param>
        public virtual void ChangeAcceptedDate(DateTime newDate)
        {
            if (newDate == DateTime.MinValue) throw new ArgumentNullException("newDate");

            VerifyUserCanTakeAction(SurveyActionType.EditAcceptedDate);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnChangeAcceptedDate(trans, survey, newDate);
                ChangeAcceptedDate(trans, survey, newDate);
                OnChangeAcceptedDateComplete(trans, survey, newDate);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ChangeAcceptedDate(Transaction trans, Survey survey, DateTime newDate)
        {
            // make the change
            string historyEntry = string.Format("{0} changed the accepted date from {1:d} to {2:d}.", GetCurrentUserName(), survey.AcceptDate, newDate);
            survey.AcceptDate = newDate.Date;

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnChangeAcceptedDate(Transaction trans, Survey survey, DateTime newDate)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnChangeAcceptedDateComplete(Transaction trans, Survey survey, DateTime newDate)
        {
        }

        /// <summary>
        /// Changes the due date of the survey and records the change in history.
        /// </summary>
        /// <param name="newDate">New due date for this survey.</param>
        public virtual void ChangeReportSubmittedDate(DateTime newDate)
        {
            if (newDate == DateTime.MinValue) throw new ArgumentNullException("newDate");

            VerifyUserCanTakeAction(SurveyActionType.EditReportSubmittedDate);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnChangeReportSubmittedDate(trans, survey, newDate);
                ChangeReportSubmittedDate(trans, survey, newDate);
                OnChangeReportSubmittedDateComplete(trans, survey, newDate);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ChangeReportSubmittedDate(Transaction trans, Survey survey, DateTime newDate)
        {
            // make the change
            string historyEntry = string.Format("{0} changed the report submitted date from {1:d} to {2:d}.", GetCurrentUserName(), survey.ReportCompleteDate, newDate);
            survey.ReportCompleteDate = newDate.Date;

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnChangeReportSubmittedDate(Transaction trans, Survey survey, DateTime newDate)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnChangeReportSubmittedDateComplete(Transaction trans, Survey survey, DateTime newDate)
        {
        }

        /// <summary>
        /// Changes the complete date of the survey and records the change in history.
        /// </summary>
        /// <param name="newDate">New complete date for this survey.</param>
        public virtual void ChangeCompleteDate(DateTime newDate)
        {
            if (newDate == DateTime.MinValue) throw new ArgumentNullException("newDate");

            VerifyUserCanTakeAction(SurveyActionType.EditCompleteDate);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnChangeCompleteDate(trans, survey, newDate);
                ChangeCompleteDate(trans, survey, newDate);
                OnChangeCompleteDateComplete(trans, survey, newDate);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ChangeCompleteDate(Transaction trans, Survey survey, DateTime newDate)
        {
            // make the change
            string historyEntry = string.Format("{0} changed the complete date from {1:d} to {2:d}.", GetCurrentUserName(), survey.CompleteDate, newDate);
            survey.CompleteDate = newDate.Date;

            // save our changes
            SaveSurvey(trans, survey, historyEntry);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnChangeCompleteDate(Transaction trans, Survey survey, DateTime newDate)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnChangeCompleteDateComplete(Transaction trans, Survey survey, DateTime newDate)
        {
        }

        /// <summary>
        /// Gets the current user's full name, or "System" if there is none.
        /// </summary>
        protected string GetCurrentUserName()
        {
            return (this.CurrentUser != null) ? this.CurrentUser.Name : "System";
        }

        /// <summary>
        /// Verifies the current user can preform the specified action on the current survey, 
        /// based on the company's defined actions and user permissions.
        /// </summary>
        /// <param name="actionType">Action to be preformed by the current user.</param>
        public bool VerifyUserCanTakeThisAction(SurveyActionType actionType)
        {
            //Bypass all of this if this is a System Admin User
            if (!_override)
            {
                // verify the company allows the specified action to be preformed
                if (!this.Company.IsActionAllowed(actionType, this.Survey.Status))
                {
                    return false;
                }

                // verify the current user has permission to take the specified action
                // note: this system user has not permission restrictions
                if (this.CurrentUser != null)
                {
                    PermissionSet permissionSet = this.CurrentUser.Permissions;
                    if (!permissionSet.CanTakeAction(actionType, this.Survey, this.CurrentUser))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Verifies the current user can preform the specified action on the current survey, 
        /// based on the company's defined actions and user permissions.
        /// </summary>
        /// <param name="actionType">Action to be preformed by the current user.</param>
        protected void VerifyUserCanTakeAction(SurveyActionType actionType)
        {
   
            //Bypass all of this if this is a System Admin User
            if (!_override)
            {
                // verify the company allows the specified action to be preformed
                if (!this.Company.IsActionAllowed(actionType, this.Survey.Status))
                {
                    // build and throw a detailed exception
                    string msg = string.Format("Action '{0}' cannot be taken on survey {1}.  Company '{2}' does not define this action for surveys with status '{3}'.",
                        actionType.ActionName, this.Survey.Number, this.Company.Abbreviation, this.Survey.StatusCode);

                    throw new InvalidOperationException(msg);
                }

                // verify the current user has permission to take the specified action
                // note: this system user has not permission restrictions
                if (this.CurrentUser != null)
                {
                    PermissionSet permissionSet = this.CurrentUser.Permissions;
                    if (!permissionSet.CanTakeAction(actionType, this.Survey, this.CurrentUser))
                    {
                        // build and throw a detailed exception
                        string msg = string.Format("Action '{0}' cannot be taken on survey {1} with status of '{2}'.  User '{3}' with company '{4}' does not sufficient permission.",
                            actionType.ActionName, this.Survey.Number, this.Survey.StatusCode, this.CurrentUser.Name, this.Company.Abbreviation);

                        throw new InvalidOperationException(msg);
                    }
                }
            }

        }

        /// <summary>
        /// Saves a Survey, records any change in status, notifies the field system of any ownership change if needed,
        /// and writes an entry to history.
        /// </summary>
        /// <param name="trans">Transaction underwhich this operation will be preformed.</param>
        /// <param name="survey">Survey to save.</param>
        /// <param name="historyEntry">Entry to be added to the surveys's history.</param>
        protected void SaveSurvey(Transaction trans, Survey survey, string historyEntry)
        {
            SaveSurvey(trans, survey, historyEntry, null);
        }

        /// <summary>
        /// Saves a Survey, records any change in status, notifies the field system of any ownership change if needed,
        /// and writes an entry to history.
        /// </summary>
        /// <param name="trans">Transaction underwhich this operation will be preformed.</param>
        /// <param name="survey">Survey to save.</param>
        /// <param name="historyEntry">Entry to be added to the Survey's history.</param>
        /// <param name="actionUser">User that performed the action.</param>
        protected void SaveSurvey(Transaction trans, Survey survey, string historyEntry, User actionUser)
        {
            // write the passed string to history
            if (historyEntry != null && historyEntry.Length > 0)
            {
                WriteHistoryEntry(trans, survey, historyEntry);
            }

            survey.LastModifiedOn = DateTime.Now;

            // save the survey
            survey.Save(trans);
        }

        /// <summary>
        /// Writes a history entry for a Survey.
        /// </summary>
        /// <param name="trans">Transaction underwhich this operation will be preformed.</param>
        /// <param name="survey">Survey associated to this new entry.</param>
        /// <param name="entryText">Text to be saved in history.</param>
        protected static void WriteHistoryEntry(Transaction trans, Survey survey, string entryText)
        {
            // try to avoid duplicate history times in database (DateTime.Now resolution is ~10 ms)
            //
            // From SQL Server Books Online:
            //   Date and time data from January 1, 1753 through December 31, 9999, to an accuracy of one 
            //   three-hundredth of a second (equivalent to 3.33 milliseconds or 0.00333 seconds).
            //   Values are rounded to increments of .000, .003, or .007 seconds, as shown in the table.
            //
            DateTime entryTime = DateTime.Now;
            if (entryTime < _lastHistoryEntryTime.AddTicks(DatabaseTimeResolution))
            {
                entryTime = _lastHistoryEntryTime.AddTicks(DatabaseTimeResolution);
            }

            SurveyHistory history = new SurveyHistory(Guid.NewGuid());
            history.SurveyID = survey.ID;
            history.EntryDate = entryTime;
            history.EntryText = entryText;
            history.Save(trans);

            _lastHistoryEntryTime = entryTime;
        }

        /// <summary>
        /// Picks a UserTerritory based on territory assignment rules setup for the company.
        /// If more than one user is assigned to the matching territory, one is picked at random
        /// using the assignment weight assigned to the users.
        /// </summary>
        /// <param name="survey">Survey forwhich to pick the user.</param>
        /// <param name="surveyType">Mode the survey will be in after assignment.</param>
        /// <returns>A UserTerritory matching the constraints provided, or null if no match could be found.</returns>
        protected virtual UserTerritory PickUserForSurvey(Survey survey, SurveyStatusType surveyType)
        {
            // build a query to find matching user/territory assignments for the type of territory being requested
            // and where the user would not be outside any sepecified premium limit (and are not diabled).
            string filter = "Territory[SurveyStatusTypeCode = ? && InHouseSelection = ?].GeographicAreas[GeographicUnitCode = ? && AreaValue = ?]"
                + " && User[CompanyID = ? && AccountDisabled = false]";
            string sort = "PercentAllocated DESC";

            // compile the query (we might need to run it three times in a row)
            CompiledQuery query = DB.Engine.Compile(new OPathQuery(typeof(UserTerritory), filter, sort));

            //Get the assigned user's profit center
            string profitCenter = String.Empty;
            if (survey.AssignedUser != null)
            {
                if (String.IsNullOrEmpty(survey.AssignedUser.ProfitCenterCode) && !String.IsNullOrEmpty(survey.Insured.Underwriter))
                {
                    Underwriter underwriter = Underwriter.GetOne("Code = ? && CompanyID = ?", survey.Insured.Underwriter, survey.CompanyID);
                    if (underwriter != null)
                    {
                        profitCenter = underwriter.ProfitCenterCode.ToUpper();
                    }
                }
                else
                {
                    profitCenter = survey.AssignedUser.ProfitCenterCode.ToUpper();
                }
            }

            // get state territory assignment matches
            UserTerritory[] matches;

            // look for profit center assignment matches
            matches = (UserTerritory[])DB.FetchArray(query,
                surveyType.Code, survey.InHouseSelection, GeographicUnit.ProfitCenter.Code, profitCenter,
                survey.CompanyID);

            if (matches.Length == 0)
            {
                // look for 5-digit zip assignment matches
                matches = (UserTerritory[])DB.FetchArray(query,
                    surveyType.Code, survey.InHouseSelection, GeographicUnit.Zip5.Code, (survey.Location.ZipCode.Length > 5) ? survey.Location.ZipCode.Substring(0, 5) : survey.Location.ZipCode,
                    survey.CompanyID);
            }

            if (matches.Length == 0)
            {
                // look for 3-digit zip assignment matches
                matches = (UserTerritory[])DB.FetchArray(query,
                    surveyType.Code, survey.InHouseSelection, GeographicUnit.Zip3.Code, (survey.Location.ZipCode.Length >= 3) ? survey.Location.ZipCode.Substring(0, 3) : string.Empty,
                    survey.CompanyID);
            }

            if (matches.Length == 0)
            {
                // look for state assignment matches			
                matches = (UserTerritory[])DB.FetchArray(query,
                    surveyType.Code, survey.InHouseSelection, GeographicUnit.State.Code, survey.Location.StateCode,
                    survey.CompanyID);
            }

            // we're done if no match was found
            if (matches.Length == 0)
            {
                return null;
            }

            //determine if allocation is based on percentage or premium amounts
            if (matches[0].PercentAllocated != int.MinValue)
            {
                // extract the allocations and determine sum
                double total = 0;
                double[] allocations = new double[matches.Length];
                for (int i = 0; i < matches.Length; i++)
                {
                    double alloc = matches[i].PercentAllocated;
                    allocations[i] = alloc;
                    total += alloc;
                }

                // we're done if total allocation is zero
                if (total <= 0)
                {
                    return null;
                }

                // generate a random number between 0 and our total (e.g., 0 <= value < total)
                double rnd = _randomNumbers.NextDouble() * total;

                // find the user with allocation falling in the range of the generated random number
                double low = 0;
                for (int i = 0; i < allocations.Length; i++)
                {
                    double high = low + allocations[i];
                    if (low <= rnd && rnd < high) // this is our match
                    {
                        return matches[i];
                    }
                    low = high;
                }

                // we couldn't find a user for some reason (algorithm problem)
                throw new Exception("No user was selected for Survey " + survey.Number + ".  The random number is " + rnd + " and the user count is " + matches.Length);
            }
            else
            {
                decimal premium = 0;

                Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                foreach (Policy policy in policies)
                {
                    premium = (policy.Premium != decimal.MinValue) ? policy.Premium + premium : premium;
                }

                foreach (UserTerritory match in matches)
                {
                    if (premium >= match.MinPremiumAmount && premium <= match.MaxPremiumAmount)
                        return match;
                }

                //if we got this far than there were no matches
                return null;
            }
        }

        /// <summary>
        /// Changes the survey type.
        /// </summary>
        public virtual void ChangeSurveyPriority(string severity)
        {
            VerifyUserCanTakeAction(SurveyActionType.ChangeSurveyPriority);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                OnChangeSurveyPriority(trans, survey);
                ChangeSurveyPriority(trans, survey, severity);
                OnChangeSurveyPriorityComplete(trans, survey);

                trans.Commit();
            }

            this.Survey = survey;
        }

        /// <summary>
        /// Performs the company-agnostic part of this operation.
        /// </summary>
        protected virtual void ChangeSurveyPriority(Transaction trans, Survey survey, string priority)
        {
            //set the history entry
            string historyEntry = string.Format("{0} changed the survey priority from {1} to {2}.", GetCurrentUserName(), survey.Priority, priority);

            survey.Priority = priority;
            // save our changes
            SaveSurvey(trans, survey, historyEntry, null);
        }

        /// <summary>
        /// Performs additional custom processing before this operation is preformed.
        /// </summary>
        protected virtual void OnChangeSurveyPriority(Transaction trans, Survey survey)
        {
        }

        /// <summary>
        /// Performs additional custom processing after this operation is preformed.
        /// </summary>
        protected virtual void OnChangeSurveyPriorityComplete(Transaction trans, Survey survey)
        {
        }
  
        public void AddPlanObjective(string planObjectiveId)
        {
            //if (comment == null || comment.Length == 0) throw new ArgumentNullException("comment");

            //VerifyUserCanTakeAction(SurveyActionType.AddNote);

            Survey survey = this.Survey.GetWritableInstance();

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //Get plan objective
                Objective planObjective = Objective.GetOne("ID = ?", planObjectiveId);

                    //New Objective
                    Objective objective = new Objective(Guid.NewGuid());

                    Business.Utility.CopyPropertyValues(ref objective, planObjective);

                    if (survey != null)
                    {
                        objective.SurveyID = survey.ID;

                        //get the highest number and incriment by one
                        Objective[] objectives = Objective.GetSortedArray("SurveyID = ?", "Number DESC", survey.ID);
                        objective.Number = (objectives.Length > 0) ? objectives[0].Number + 1 : 1;
                        objective.ServicePlanID = planObjective.ServicePlanID;
                    }

                    objective.Save(trans);

                //Create any attributes
                    List<ObjectiveAttribute> planAttributes = ObjectiveAttribute.GetArray("ObjectiveID = ?", planObjective.ID).ToList();

                    foreach (ObjectiveAttribute a in planAttributes)
                    {
                        ObjectiveAttribute newAttribute = new ObjectiveAttribute(objective.ID, a.CompanyAttributeID);

                        Business.Utility.CopyPropertyValues(ref newAttribute, a);

                        newAttribute.Save(trans);
                    }

                trans.Commit();
            }

            this.Survey = survey;
        }
    }
}
