using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents an Activity entity, which maps to table 'Activity' in the database.
	/// </summary>
    [Serializable]
	public class Activity : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private Activity()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this Activity.</param>
		public Activity(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


        #region --- Generated Members ---

        private Guid _id;
        private Guid _typeID;
        private Guid _allocatedTo;
        private DateTime _date;
        private decimal _hours = Decimal.MinValue;
        private decimal _days = Decimal.MinValue;
        private decimal _callCount = Decimal.MinValue;
        private string _comments = String.Empty;
        private ObjectHolder _activityTypeHolder = null;
        private ObjectHolder _allocatedToUserHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Activity ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Activity Type.
        /// </summary>
        public Guid TypeID
        {
            get { return _typeID; }
            set
            {
                VerifyWritable();
                _typeID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Allocated To.
        /// </summary>
        public Guid AllocatedTo
        {
            get { return _allocatedTo; }
            set
            {
                VerifyWritable();
                _allocatedTo = value;
            }
        }

        /// <summary>
        /// Gets or sets the Activity Date.
        /// </summary>
        public DateTime Date
        {
            get { return _date; }
            set
            {
                VerifyWritable();
                _date = value;
            }
        }

        /// <summary>
        /// Gets or sets the Activity Hours. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal Hours
        {
            get { return _hours; }
            set
            {
                VerifyWritable();
                _hours = value;
            }
        }

        /// <summary>
        /// Gets or sets the Activity Days. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal Days
        {
            get { return _days; }
            set
            {
                VerifyWritable();
                _days = value;
            }
        }

        /// <summary>
        /// Gets or sets the Call Count. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal CallCount
        {
            get { return _callCount; }
            set
            {
                VerifyWritable();
                _callCount = value;
            }
        }

        /// <summary>
        /// Gets or sets the Comments. Null value is 'String.Empty'.
        /// </summary>
        public string Comments
        {
            get { return _comments; }
            set
            {
                VerifyWritable();
                _comments = value;
            }
        }

        /// <summary>
        /// Gets the instance of a ActivityType object related to this entity.
        /// </summary>
        public ActivityType Type
        {
            get
            {
                _activityTypeHolder.Key = _typeID;
                return (ActivityType)_activityTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User User
        {
            get
            {
                _allocatedToUserHolder.Key = _allocatedTo;
                return (User)_allocatedToUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_typeID": return _typeID;
                    case "_allocatedTo": return _allocatedTo;
                    case "_date": return _date;
                    case "_hours": return _hours;
                    case "_days": return _days;
                    case "_callCount": return _callCount;
                    case "_comments": return _comments;
                    case "_activityTypeHolder": return _activityTypeHolder;
                    case "_allocatedToUserHolder": return _allocatedToUserHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_typeID": _typeID = (Guid)value; break;
                    case "_allocatedTo": _allocatedTo = (Guid)value; break;
                    case "_date": _date = (DateTime)value; break;
                    case "_hours": _hours = (decimal)value; break;
                    case "_days": _days = (decimal)value; break;
                    case "_callCount": _callCount = (decimal)value; break;
                    case "_comments": _comments = (string)value; break;
                    case "_activityTypeHolder": _activityTypeHolder = (ObjectHolder)value; break;
                    case "_allocatedToUserHolder": _allocatedToUserHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static Activity Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(Activity), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch Activity entity with ID = '{0}'.", id));
            }
            return (Activity)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Activity GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Activity), opathExpression);
            return (Activity)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Activity GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Activity))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Activity)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Activity[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Activity), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Activity[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Activity))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Activity[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Activity[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Activity), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public Activity GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            Activity clone = (Activity)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.TypeID, _typeID);
            Validator.Validate(Field.AllocatedTo, _allocatedTo);
            Validator.Validate(Field.Date, _date);
            Validator.Validate(Field.Hours, _hours);
            Validator.Validate(Field.Days, _days);
            Validator.Validate(Field.CallCount, _callCount);
            Validator.Validate(Field.Comments, _comments);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _typeID = Guid.Empty;
            _allocatedTo = Guid.Empty;
            _date = DateTime.MinValue;
            _hours = Decimal.MinValue;
            _days = Decimal.MinValue;
            _callCount = Decimal.MinValue;
            _comments = null;
            _activityTypeHolder = null;
            _allocatedToUserHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the Activity entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the TypeID field.
            /// </summary>
            TypeID,
            /// <summary>
            /// Represents the AllocatedTo field.
            /// </summary>
            AllocatedTo,
            /// <summary>
            /// Represents the Date field.
            /// </summary>
            Date,
            /// <summary>
            /// Represents the Hours field.
            /// </summary>
            Hours,
            /// <summary>
            /// Represents the Days field.
            /// </summary>
            Days,
            /// <summary>
            /// Represents the CallCount field.
            /// </summary>
            CallCount,
            /// <summary>
            /// Represents the Comments field.
            /// </summary>
            Comments,
        }

        #endregion
    }
}