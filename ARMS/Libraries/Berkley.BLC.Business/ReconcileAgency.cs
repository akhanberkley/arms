using System;
using System.Collections.Generic;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;

namespace Berkley.BLC.Business
{
	public class ReconcileAgency : IReconcile<Agency>
	{
		private ReconcileActor<Agency> moActor;
		private List<Agency> moAgencyInsertList = new List<Agency>();
		private List<Agency> moAgencyUpdateList = new List<Agency>();
		private List<AgencyEmail> moAgencyEmailList;
		private Company moCompany;

		public ReconcileAgency(Company oCompany, Agency oBuiltAgency, List<AgencyEmail> oEmailList, Transaction oTransaction)
		{
			moCompany = oCompany;
			moAgencyEmailList = oEmailList;
			moActor = new ReconcileActor<Agency>(new string[] { oBuiltAgency.Number }, oBuiltAgency, this, oTransaction);
		}

		public ReconcileAgency(Company oCompany)
		{
			moCompany = oCompany;
		}

		public void Reconcile(Agency oBuiltAgency)
		{
			Transaction oTransaction;
			using (oTransaction = DB.Engine.BeginTransaction())
			{
				moActor = new ReconcileActor<Agency>(new string[] { oBuiltAgency.Number }, oBuiltAgency, this, oTransaction);
				moActor.Reconcile();
				AddQueued();
				UpdateQueued();
				oTransaction.Commit();
			}
		}

		#region IReconcile<Agency> Members
		public void Clear()
		{
			moAgencyInsertList.Clear();
			moAgencyUpdateList.Clear();
		}

		public void Reconcile()
		{
			moActor.Reconcile();
		}

		public Agency GetExisting(string[] rstrForeignKey)
		{
			Agency oAgency = Agency.GetOne("Number = ? AND CompanyID = ?", rstrForeignKey[0], moCompany.ID);
			return oAgency;
		}

		public Agency Entity
		{
			get { return moActor.Entity; }
		}

		public Agency Update(Agency oNewEntity, Agency oExistingEntity)
		{
			if (oExistingEntity.IsReadOnly)
			{
				oExistingEntity = oExistingEntity.GetWritableInstance();
			}
			oExistingEntity = oExistingEntity.Assign(oNewEntity);
			moAgencyUpdateList.Add(oExistingEntity);
			moActor.Entity = oExistingEntity;

			// Update the AgencyID in the AgencyEmail
            if (moAgencyEmailList != null && moAgencyEmailList.Count > 0)
			{
				for (int i = 0; i < moAgencyEmailList.Count; i++)
				{
					if (moAgencyEmailList[i].AgencyID == oNewEntity.ID)
					{
						moAgencyEmailList[i].AgencyID = oExistingEntity.ID;
					}
				}
			}
			return oExistingEntity;
		}

		public Agency Insert(Agency oNewEntity)
		{
			if (oNewEntity.IsReadOnly)
			{
				oNewEntity = oNewEntity.GetWritableInstance();
			}
			oNewEntity.CompanyID = moCompany.ID;
			moAgencyInsertList.Add(oNewEntity);
			return oNewEntity;
		}

		public void Delete(Agency oEntity)
		{
			// Intentionally not implemented
		}

		public bool Equal(Agency oLhs, Agency oRhs)
		{
			return oLhs.Number == oRhs.Number;
		}

		public ReconcileActor<Agency> Actor
		{
			get { return moActor; }
		}

		public void DeleteQueued()
		{
			// Not implemented
		}

		public void AddQueued()
		{
			foreach (Agency oAgency in moAgencyInsertList)
			{
				oAgency.Save(moActor.DbTransaction);
			}
		}
		public void UpdateQueued()
		{
			ReconcileAgencyEmail oReconcileAgencyEmail;

			foreach (Agency oAgency in moAgencyUpdateList)
			{
				oAgency.Save(moActor.DbTransaction);
			}

            if (moAgencyEmailList != null && moAgencyEmailList.Count > 0)
			{
				oReconcileAgencyEmail = new ReconcileAgencyEmail(moCompany, moAgencyEmailList, moActor.DbTransaction);
				oReconcileAgencyEmail.Reconcile();
			}

		}
		#endregion
	}
}
