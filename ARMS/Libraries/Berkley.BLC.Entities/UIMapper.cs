using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Xml;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// The UIMapper is a small engine that provides a bridge between field controls and the logic in the 
    /// presentation tier (i.e., the code-behind of each page).  It's responsible for providing support to 
    /// mapped field controls on each page (which it does by providing access to the data in the DB Map) and 
    /// marshalling data between them and actual business objects.
    /// </summary>
    public class UIMapper
    {
        private static UIMapper _instance; // singleton object
        private static Hashtable _nullValues = new Hashtable(100); // maps null values -> real values 

        private string _defaultNamespace = null;
        private Hashtable _fields; // maps field name -> fieldinfo

        /// <summary>
        /// Private constructor to prevent external object instantiation.
        /// </summary>
        private UIMapper()
        {
        }


        /// <summary>
        /// Initializes the default instance of UIMapper for use.
        /// </summary>
        /// <param name="dbMapName">Name of the XML mapping file embedded resource.</param>
        public static void Initialize(string dbMapName)
        {
            _instance = new UIMapper();
            _instance.Load(dbMapName);
        }


        /// <summary>
        /// Gets the singleton instance of this class.
        /// </summary>
        public static UIMapper Instance
        {
            get
            {
                if (_instance == null)
                {
                    throw new InvalidOperationException("Initialize method must be called before this property is used.");
                }
                return _instance;
            }
        }


        /// <summary>
        /// Returns the UIFieldInfo of a mapped entity field specified by the field enumerated member.
        /// </summary>
        /// <param name="enumValue">Enum value of the field.</param>
        /// <returns>Reference to the UIFieldInfo for the field specified.</returns>
        public UIFieldInfo GetFieldInfo(Enum enumValue)
        {
            string fieldName = UIMapper.GetFieldName(enumValue);
            return this.GetFieldInfo(fieldName);
        }

        /// <summary>
        /// Returns the UIFieldInfo of a mapped entity field specified by entity type and field name.
        /// </summary>
        /// <param name="fieldName">Fully-defined name of the enum value for the field.</param>
        /// <returns>Reference to the UIFieldInfo for the field specified.</returns>
        public UIFieldInfo GetFieldInfo(string fieldName)
        {
            return GetFieldInfo(fieldName, true);
        }

        /// <summary>
        /// Returns the UIFieldInfo of a mapped entity field specified by entity type and field name.
        /// </summary>
        /// <param name="fieldName">Fully-defined name of the enum value for the field.</param>
        /// <param name="mustBeFound">Value indicating if an exception should be thrown if the field is not found in the mapping file.</param>
        /// <returns>Reference to the UIFieldInfo for the field specified.</returns>
        public UIFieldInfo GetFieldInfo(string fieldName, bool mustBeFound)
        {
            UIFieldInfo result = (UIFieldInfo)_fields[fieldName];
            if (result == null && mustBeFound)
            {
                throw new Exception("Field '" + fieldName + "' not found in mapping file.");
            }
            return result;
        }


        /// <summary>
        /// Returns PropertyInfo for a mapped entity field specified by the field enumerated member.
        /// </summary>
        /// <param name="enumValue">Enum value of the field.</param>
        /// <returns>Reference to the PropertyInfo for the field specified.</returns>
        public PropertyInfo GetPropertyInfo(Enum enumValue)
        {
            string fieldName = UIMapper.GetFieldName(enumValue);
            return this.GetPropertyInfo(fieldName);
        }

        /// <summary>
        /// Returns the PropertyInfo of a property specified by entity type and field name.
        /// </summary>
        /// <param name="fieldName">Fully-defined name of the property to evaluate.</param>
        /// <returns>Reference to the PropertyInfo for the field specified.</returns>
        public PropertyInfo GetPropertyInfo(string fieldName)
        {
            UIFieldInfo fieldInfo = this.GetFieldInfo(fieldName, false);
            if (fieldInfo != null)
            {
                return fieldInfo.PropertyInfo;
            }
            else // not a mapped field see if we can find it using reflection
            {
                int index = fieldName.LastIndexOf('.');
                string typeName = GetFullTypeName(fieldName.Substring(0, index));
                string propertyName = fieldName.Substring(index + 1);

                // find the type (caller will throw an exception if it can't be found)
                //TODO: Consider a caching strategy for FindType - it's not exactly a high-performance method
                Type type = UIMapper.FindType(typeName);

                // try to get the property info from the type
                PropertyInfo propInfo = type.GetProperty(propertyName);
                if (propInfo == null)
                {
                    throw new ArgumentException("Type '" + type + "' does not contain a public property named '" + propertyName + "'.");
                }

                return propInfo;
            }
        }


        /// <summary>
        /// Returns the class type of an entity specified by name.
        /// </summary>
        /// <param name="typeName">Name of the entity class (without namespace).</param>
        /// <returns>Class type of the entity.</returns>
        public Type GetEntityType(string typeName)
        {
            typeName = GetFullTypeName(typeName);
            return FindType(typeName);
        }

        private string GetFullTypeName(string typeName)
        {
            if (_defaultNamespace != null && typeName.IndexOf('.') < 0)
            {
                return _defaultNamespace + "." + typeName;
            }
            else
            {
                return typeName;
            }
        }


        /// <summary>
        /// Returns the name of a mapped entity field specified by the field enumerated member.
        /// </summary>
        /// <param name="enumValue">Enum value of the field.</param>
        /// <returns>Name of the field specified.</returns>
        public static string GetFieldName(Enum enumValue)
        {
            Type entityType = enumValue.GetType().DeclaringType;
            return UIMapper.GetFieldName(entityType, enumValue.ToString());
        }

        /// <summary>
        /// Returns the name of a mapped entity field specified by entity type and field name.
        /// </summary>
        /// <param name="entityType">Type of entity to evaluate.</param>
        /// <param name="enumValueName">Name of the enum value for the field.</param>
        /// <returns>Name of the field specified.</returns>
        public static string GetFieldName(Type entityType, string enumValueName)
        {
            return (entityType.Name + "." + enumValueName);
        }



        private void Load(string mapName)
        {
            _fields = new Hashtable(2000);

            // process the db map, building a hashtable of fields
            XmlDocument mappings = LoadResourceXmlDoc(mapName);

            // get the root node
            XmlNode root = mappings.SelectSingleNode("mappings");
            if (root == null)
            {
                throw new Exception("Root mappings is Missing");
            }

            // check the mapping version
            string version = this.GetValue(root, "version", "4.2");
            if (version != "4.2")
            {
                throw new Exception("Version of mapping file is not compatible");
            }

            // get the default namespace for entities in the map
            _defaultNamespace = this.GetValue(root, "defaultNamespace", null);

            // process each entity node
            foreach (XmlNode entityNode in root.SelectNodes("entity"))
            {
                string typeName = this.GetValue(entityNode, "type");
                typeName = GetFullTypeName(typeName);

                Type entityType = FindType(typeName);
                Type entityEnumType = FindType(entityType.FullName + "+Field");

                // process each attribute node
                foreach (XmlNode attribNode in entityNode.SelectNodes("attribute"))
                {
                    string alias = this.GetValue(attribNode, "alias");
                    if (alias == null || alias.Length == 0)
                    {
                        throw new Exception("Entity " + entityType.ToString() + " has a field with no 'alias' attribute specified.");
                    }
                    Enum enumValue = (Enum)Enum.Parse(entityEnumType, alias);
                    string fieldName = GetFieldName(enumValue);

                    string dataType = this.GetValue(attribNode, "type");
                    if (dataType == null || dataType.Length == 0)
                    {
                        throw new Exception("Field '" + fieldName + "' is missing a 'type' attribute.");
                    }
                    Type fieldType = Type.GetType(dataType);

                    string displayName = this.GetValue(attribNode, "name", null);
                    if (displayName == null || displayName.Length == 0)
                    {
                        throw new Exception("Field '" + fieldName + "' is missing a 'name' attribute.");
                    }

                    int size = int.Parse(this.GetValue(attribNode, "size", "0"));
                    if (fieldType == typeof(string) && size == 0)
                    {
                        throw new Exception("Field '" + fieldName + "' is missing a 'size' attribute.");
                    }

                    object nullValue = this.GetValue(attribNode, "nullValue", null);
                    bool isNullable = (nullValue != null);
                    if (nullValue != null)
                    {
                        nullValue = ConvertNullValue((string)nullValue, fieldType);
                    }

                    //switch (this.GetValue(attribNode, "persistType", "").ToUpper())
                    //{
                    //	case "READONLY": //todo
                    //	case "CONCURRENT": //todo							
                    //	default: //todo:
                    //}

                    UIFieldInfo field = new UIFieldInfo(entityType, enumValue, fieldType);
                    field.DisplayName = displayName;
                    field.IsNullable = isNullable;
                    field.NullValue = nullValue;
                    field.IsRequired = !isNullable; //bool.Parse(isRequired);
                    field.MaxLength = size;
                    field.MinLength = 0; //int.Parse(minLength);
                    field.Columns = ConvertToColumns(size);
                    field.Rows = ConvertToRows(size);
                    field.Format = this.GetValue(attribNode, "format", null);
                    field.InputMask = this.GetValue(attribNode, "inputMask", null);

                    if (field.MinLength < 0)
                    {
                        throw new Exception("Field '" + fieldName + "': MinLength value cannot be negative.");
                    }
                    if (field.MaxLength < 0)
                    {
                        throw new Exception("Field '" + fieldName + "': MaxLength value cannot be negative.");
                    }
                    if (field.MaxLength < field.MinLength)
                    {
                        throw new Exception("Field '" + fieldName + "': MinLength value cannot be greater than MaxLength.");
                    }

                    _fields.Add(fieldName, field);
                }
            }
        }


        /// <summary>
        /// Loads the XML file embedded in the assembly as a resource.
        /// </summary>
        /// <param name="resourceName">Name of the resource to load.</param>
        /// <returns>Populated XmlDocument.</returns>
        public static XmlDocument LoadResourceXmlDoc(string resourceName)
        {
            using (Stream docStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(typeof(DB), resourceName))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(docStream);
                return doc;
            }
        }


        private string GetValue(XmlNode node, string name)
        {
            return this.GetValue(node, name, null);
        }

        private string GetValue(XmlNode node, string name, string defaultValue)
        {
            XmlNode attr = node.Attributes.GetNamedItem(name);
            return (attr != null) ? attr.Value : defaultValue;
        }

        private int ConvertToColumns(int maxLength)
        {
            if (maxLength < 5)
            {
                return 5;
            }
            else if (maxLength < 100)
            {
                return ((maxLength + 4) / 5) * 5;
            }
            else
            {
                return 100;
            }
        }

        private int ConvertToRows(int maxLength)
        {
            if (maxLength <= 255)
            {
                return 0;
            }
            else
            {
                int rows = (maxLength / 100) + 2;
                return (rows < 10) ? rows : 10;
            }
        }


        /// <summary>
        /// Gets the type class specifed by string name.
        /// </summary>
        /// <param name="typeName">Fully-qualified name of type to find.</param>
        /// <returns>Reference to Type specified.</returns>
        public static Type FindType(string typeName)
        {
            Type type = Type.GetType(typeName, false);
            if (type != null)
            {
                return type;
            }

            //TODO: consider re-design to cache previous types so we don't have to look it up everytime
            //TODO: don't think we need this as long as this class/method lives in the same project as the entities
            foreach (Assembly asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                type = asm.GetType(typeName);
                if (type != null)
                {
                    return type;
                }
            }

            throw new ArgumentException("Type named '" + typeName + "' could not be located in any loaded assembly.");
        }


        private static object GetNullValue(string value)
        {
            switch (value)
            {
                case "null": return null;

                case "": return string.Empty;
                case "String.Empty": return string.Empty;

                case "Guid.Empty": return Guid.Empty;

                case "Int32.MinValue": return Int32.MinValue;
                case "Int32.MaxValue": return Int32.MaxValue;
                case "Int16.MinValue": return Int16.MinValue;
                case "Int16.MaxValue": return Int16.MaxValue;
                case "Int64.MinValue": return Int64.MinValue;
                case "Int64.MaxValue": return Int64.MaxValue;

                case "UInt32.MinValue": return UInt32.MinValue;
                case "UInt32.MaxValue": return UInt32.MaxValue;
                case "UInt16.MinValue": return UInt16.MinValue;
                case "UInt16.MaxValue": return UInt16.MaxValue;
                case "UInt64.MinValue": return UInt64.MinValue;
                case "UInt64.MaxValue": return UInt64.MaxValue;

                case "Byte.MinValue": return Byte.MinValue;
                case "Byte.MaxValue": return Byte.MaxValue;
                case "SByte.MinValue": return SByte.MinValue;
                case "SByte.MaxValue": return SByte.MaxValue;

                case "Char.MinValue": return Char.MinValue;
                case "Char.MaxValue": return Char.MaxValue;

                case "Single.MinValue": return Single.MinValue;
                case "Single.MaxValue": return Single.MaxValue;
                case "Single.NaN": return Single.NaN;
                case "Single.NegativeInfinity": return Single.NegativeInfinity;
                case "Single.PositiveInfinity": return Single.PositiveInfinity;
                case "Single.Epsilon": return Single.Epsilon;

                case "Double.Epsilon": return Double.Epsilon;
                case "Double.MaxValue": return Double.MaxValue;
                case "Double.MinValue": return Double.MinValue;
                case "Double.NaN": return Double.NaN;
                case "Double.NegativeInfinity": return Double.NegativeInfinity;
                case "Double.PositiveInfinity": return Double.PositiveInfinity;

                case "Decimal.MinValue": return Decimal.MinValue;
                case "Decimal.MaxValue": return Decimal.MaxValue;
                case "Decimal.Zero": return Decimal.Zero;
                case "Decimal.One": return Decimal.One;
                case "Decimal.MinusOne": return Decimal.MinusOne;

                case "DateTime.MinValue": return DateTime.MinValue;
                case "DateTime.MaxValue": return DateTime.MaxValue;
            }

            throw new ArgumentException("Expression '" + value + "' could not be converted to a value.");
        }


        /// <summary>
        /// Converts a string value or expression to the specified data type.
        /// </summary>
        /// <param name="expression">Value or expression to be converted.</param>
        /// <param name="targetType">Data type to which the value or expression will be converted.</param>
        /// <returns>Value converted to the specified type.</returns>
        /// <remarks>
        /// Expression Examples:
        /// - null
        /// - MinValue
        /// - Int32.MinValue
        /// - System.Int32.MinValue
        /// - String.Empty
        /// - Guid.Empty
        /// - CustomNamespace.CustomType.MemberOrProperty
        /// </remarks>
        public static object ConvertNullValue(string expression, Type targetType) // Jeff Lanning (jefflanning@gmail.com): Added method to convert expressions to values.
        {
            if (expression == null) throw new ArgumentNullException("expression");

            // return the value if the type+expression combo is already in our cache
            string key = targetType.FullName + "\t" + expression;
            object value = _nullValues[key];
            if (value != null || _nullValues.ContainsKey(key))
            { //NOTE: have to check cache a second time since nulls are valid values
                return value;
            }

            Type type;
            string memberName;

            // try to get the type from the expression
            // default to the targetType if no type name is in the expression or could not convertable to a type
            int index = expression.LastIndexOf(Type.Delimiter);
            if (index > 0)
            { // type delimiter found
                string typeName = expression.Substring(0, index);
                type = Type.GetType(typeName);
                if (type == null)
                {
                    type = Type.GetType("System." + typeName);
                }

                if (type != null)
                {
                    memberName = expression.Substring(index + 1);
                }
                else
                { // could not find the type
                    type = targetType;
                    memberName = expression;
                }
            }
            else
            { // no type delimiter
                type = targetType;
                memberName = expression;
            }

            // see if the member name is a field
            FieldInfo field = type.GetField(memberName);
            if (field != null)
            {
                value = field.GetValue(type);
            }
            else
            { // not a field
                // see if the member name is a property
                PropertyInfo property = type.GetProperty(memberName);
                if (property != null)
                {
                    value = property.GetValue(null, null);
                }
                else
                { // not a property
                    // "null" and "nothing" converts to null value
                    switch (expression.ToLower())
                    {
                        case "null":
                        case "nothing":
                            {
                                if (targetType.IsValueType)
                                {
                                    throw new Exception("Null object cannot be converted to a value type.");
                                }
                                return null;
                            }
                        default:
                            {
                                // use the original expression as the value to be converted
                                value = expression;
                                break;
                            }
                    }
                }
            }

            // convert the value to the target type; special-case Guid and byte-array types
            if (targetType == typeof(Guid) && value is string)
            {
                value = new Guid((string)value);
            }
            else if (targetType == typeof(byte[]) && value is string)
            {
                value = new byte[0]; // Paul Welter (http://www.LoreSoft.com) -- support nullable byte array
            }
            else
            {
                value = Convert.ChangeType(value, targetType);
            }

            // add the final value to the cache
            _nullValues.Add(key, value);

            return value;
        }
    }
}

