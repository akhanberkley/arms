using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

using QCI.ExceptionManagement;
using QCI.Encryption;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Contains security related utility methods.
    /// </summary>
    public sealed class Security
    {
        private const string CRYPT_KEY_BASE64 = "e0AgGfZXmkeh0JMTMkxnilLuNETgDcVc";
        private const string CRYPT_IV_BASE64 = "ZPGaXfrSUk5=";

        /// <summary>
        /// Private constructor to prevent object instantiation of this class.
        /// </summary>
        private Security() { }


        /// <summary>
        /// Creates an authentication ticket and attaches it to a cookie in the outgoing response.
        /// Redirects the user to the page provided by the ReturnUrl query string variable, or the default specified.
        /// </summary>
        /// <param name="authedUser">User that has been authenticated.</param>
        /// <param name="persistentCookie">Flag that indicates if the auth cookie should be persistent.</param>
        /// <param name="defaultPageUrl">URL to redirect the user to if a ReturnUrl was not provided on the query string of the request.</param>
        public static void LoginUser(User authedUser, bool persistentCookie, string defaultPageUrl)
        {
            HttpContext current = HttpContext.Current;

            // create the forms auth cookie and add it to the outgoing response;
            // note: the name in the auth ticket must be the ID of the user NOT the username
            FormsAuthentication.SetAuthCookie(authedUser.ID.ToString(), persistentCookie);

            // redirect the user back to the originally page requested; or to the default if none
            string url = current.Request["ReturnUrl"];
            if (url == null || url.Length == 0)
            {
                url = defaultPageUrl;
            }

            current.Response.Redirect(url, true);
        }


        /// <summary>
        /// Generates a GenericPrincipal object, containing the IadaIdentity and role of the user,
        /// from the Forms Authentication cookie found in the request from the passed HttpContext.
        /// NOTE: The application cache is used to store GenericPrincipal objects, reducing the
        /// number of times they need to be re-created.
        /// </summary>
        /// <param name="currentContext">Current HttpContext from this instance of the application.</param>
        /// <returns></returns>
        public static void ReplaceUserPrincipal(HttpContext currentContext, HttpRequest request)
        {
            // get the Forms Auth cookie; bail if there is none
            HttpCookie authCookie = currentContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null) return;

            // build the cache key where there should be a cached version of the principal
            string cacheKey = "GenericPrincipal." + authCookie.Value;

            // try to get the GenericPrincipal from cache; create one if cache miss
            GenericPrincipal principal = (GenericPrincipal)currentContext.Cache[cacheKey];
            if (principal == null) // not in cache
            {
                // decrypt the auth ticket contained in the cookie and create a new identity
                // bail if ticket is invalid, decryption fails, or identity cannot be created
                UserIdentity identity;
                try
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    if (authTicket == null) return;

                    identity = new UserIdentity(authTicket, cacheKey, request);
                }
                catch (Exception ex) // ticket is not valid or failed to be decrypted, or identity could not be loaded
                {
                    // throw away the user's auth ticket - there could might something wrong with it
                    FormsAuthentication.SignOut();

                    // record this exception (possible security issue) and bail out
                    ExceptionManager.Publish(ex);
                    return;
                }

                // create a new prinpical object based on the identity and role
                //string role = ConvertFromSecurityRole(identity.Role);
                principal = new GenericPrincipal(identity, new string[0]);

                // cache the new prinpical object, set in to expire at the same time as the ticket
                currentContext.Cache.Insert(cacheKey, principal, null, identity.Ticket.Expiration, TimeSpan.Zero);
            }

            // replace the current principal with a new one for this user
            currentContext.User = principal;
        }



        /// <summary>
        /// Returns true if the current HTTP request was made using localhost (user is on the same machine the application is on).
        /// </summary>
        /// <returns>True if the current request was made using a local connection.</returns>
        public static bool IsHttpRequestLocal()
        {
            //
            // NOTE: code taken from HttpRequest.IsLocal property in ASP.NET 1.1
            //
            HttpContext current = HttpContext.Current;
            string userAddr = current.Request.UserHostAddress;
            if (userAddr == "127.0.0.1" || userAddr == "::1")
            {
                return true;
            }
            if (userAddr == null || userAddr.Length == 0)
            {
                return false;
            }
            if (userAddr == current.Request.ServerVariables["LOCAL_ADDR"])
            {
                return true;
            }
            return false;
        }


        /// <summary>
        /// Redirect the user to the login page with the currently requested page as the return url.
        /// </summary>
        public static void RedirectToLoginPage()
        {
            HttpContext current = HttpContext.Current;

            // get the current application path
            string appPath = current.Request.ApplicationPath;
            if (!appPath.EndsWith("/")) appPath += "/";

            // get the URL to the login page (can't access the Auth tag)
            string loginUrl = System.Configuration.ConfigurationManager.AppSettings["LoginUrl"];
            if (loginUrl == null) throw new Exception("Setting 'LoginUrl' must be specified in the application config file.");

            // redirect user to login page with this page as the return
            string url;
            if (!String.IsNullOrEmpty(current.Request.RawUrl) && current.Server.UrlEncode(current.Request.RawUrl) != "%2f")
            {
                url = appPath + loginUrl + "?ReturnUrl=" + current.Server.UrlEncode(current.Request.RawUrl);
            }
            else
            {
                url = appPath + loginUrl;
            }
            current.Response.Redirect(url, true);
        }


        /// <summary>
        /// Sends an HTTP status code 403 (Forbidden) to the client and ends the current response.
        /// </summary>
        public static void RequestForbidden()
        {
            HttpResponse response = HttpContext.Current.Response;

            // send a 403 (access forbidden) response to the client
            response.Clear();
            response.StatusCode = 403;

            response.Write("<html><head><title>HTTP 403 (Forbidden)</title></head><body>\r\n");
            response.Write("<h2>Forbidden</h2>\r\n");
            response.Write("You tried to access a URL for which you don't have permission.\r\n");
            response.Write("</body></html>\r\n");

            response.End();

            // publish this event as an exception (to notify admins) as this might be a security issue
            if (bool.Parse(ConfigurationManager.AppSettings["Publish403Errors"]))
            {
                ExceptionManager.Publish(new HttpException(403, "User was forbidden to view the requested URL."));
            }
        }

        /// <summary>
        /// Sends an HTTP status code 408 (Timeout) to the client and ends the current response.
        /// </summary>
        public static void RequestExpired()
        {
            HttpResponse response = HttpContext.Current.Response;

            // send a 408 response to the client
            response.Clear();
            response.StatusCode = 408;

            response.Write("<html><head><title>HTTP 408 (Expired)</title></head><body>\r\n");
            response.Write("<h2>Expired</h2>\r\n");
            response.Write("The page you are trying to access has expired.\r\n");
            response.Write("</body></html>\r\n");

            response.End();
        }


        /// <summary>
        /// Determines if the password is acceptable for use.
        /// </summary>
        /// <param name="password">Password to evaluate.</param>
        /// <param name="errorMessage">Message that can be displayed to the end-user to tell them why the password is not valid.</param>
        /// <returns>True if the password is strong; otherwise false.</returns>
        public static bool IsPasswordValid(string password, out string errorMessage)
        {
            errorMessage = null;
            if (password.Length < 8)
            {
                errorMessage = "Password must be at least 8 characters in length.";
                return false;
            }
            if (password.Length > 20)
            {
                errorMessage = "Password cannot be more than 20 characters in length.";
                return false;
            }

            return true;
        }



        /// <summary>
        /// Generates a cryptographically strong sequence of base-64 characters.
        /// </summary>
        /// <param name="length">Length of the string to generate.</param>
        /// <returns>String containing random characters.</returns>
        public static string CreateSaltString(int length)
        {
            if (length < 1) throw new ArgumentOutOfRangeException("length", "Length must be one or greater.");

            // generate enough random bytes so that we have enough after base-64 encoding them (but not too many extra)
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buffer = new byte[length * 6 / 8 + 1];
            rng.GetBytes(buffer);

            // encode the bytes a return the correct length of string
            return Convert.ToBase64String(buffer).Substring(0, length);
        }


        /// <summary>
        /// Encrypts a string using the application crypto key.
        /// The encrypted value is returned as a base-64 encoded string.
        /// </summary>
        /// <param name="value">String to encrypt.</param>
        /// <returns>The encrypted string, encoded as a base-64 string.</returns>
        public static string EncryptToBase64String(string value)
        {
            //TODO: consider making the encryptor a static member var
            Encryptor enc = new Encryptor(EncryptionAlgorithm.TripleDes, CRYPT_KEY_BASE64, CRYPT_IV_BASE64);
            return enc.EncryptToBase64String(value);
        }

        /// <summary>
        /// Encrypts a salted string using the application crypto key.
        /// The encrypted value is returned as a base-64 encoded string.
        /// </summary>
        /// <param name="value">String to encrypt.</param>
        /// <param name="salt">The salt value use.</param>
        /// <returns>The encrypted string, encoded as a base-64 string.</returns>
        public static string EncryptToBase64String(string value, string salt)
        {
            return EncryptToBase64String(salt + value);
        }


        /// <summary>
        /// Decrypts a base-64 encoded string using the application crypto key.
        /// </summary>
        /// <param name="value">String to decrypt (encoded as a base-64 string).</param>
        /// <returns>The decrypted string.</returns>
        public static string DecryptFromBase64String(string value)
        {
            //TODO: consider making the decryptor a static member var
            Decryptor dec = new Decryptor(EncryptionAlgorithm.TripleDes, CRYPT_KEY_BASE64, CRYPT_IV_BASE64);
            return dec.DecryptFromBase64String(value);
        }

        /// <summary>
        /// Decrypts a salted base-64 encoded string using the application crypto key.
        /// </summary>
        /// <param name="value">String to decrypt (encoded as a base-64 string).</param>
        /// <param name="salt">Salt value used to encrypt the string.</param>
        /// <returns>The decrypted string.</returns>
        public static string DecryptFromBase64String(string value, string salt)
        {
            string saltedValue = DecryptFromBase64String(value);
            if (saltedValue.Length < salt.Length || saltedValue.Substring(0, salt.Length) != salt)
            {
                throw new Exception("Salt value '" + salt + "' does not match the one found in '" + value + "'.  Decryption failed.");
            }

            return saltedValue.Substring(salt.Length);
        }


        /// <summary>
        /// Computes the hash value for the specified string.
        /// </summary>
        /// <param name="value">Value to be encoded.</param>
        /// <param name="salt">Salt value to add to string value to be encoded.</param>
        /// <returns>SHA1 hashed value with salt added.</returns>
        public static string CreateSha1Hash(string value, string salt)
        {
            string saltedValue = value + salt;
            byte[] data = System.Text.Encoding.ASCII.GetBytes(saltedValue);

            SHA1 sha = new SHA1Managed();
            byte[] hash = sha.ComputeHash(data);

            return Convert.ToBase64String(hash);
        }

    }
}
