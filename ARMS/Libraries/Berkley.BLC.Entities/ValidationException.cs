using System;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Exception that is throw when a mapped field fails validation.
	/// </summary>
	[Serializable()]
	public class ValidationException : Exception
	{
		private Enum _field;
		private string _message;
		private string _messageFormat;
		private Enum[] _messageFields;

		/// <summary>
		/// Initializes an new instance of this class with the specified error message.
		/// </summary>
		/// <param name="field">Field that failed validation.</param>
		/// <param name="message">A message that describes the validation exception, which can be presented to the end-user.</param>
		public ValidationException(Enum field, string message)
		{
			if (message == null ) throw new ArgumentNullException("message");

			_field = field;
			_message = message;
			_messageFormat = null;
			_messageFields = null;
		}

		/// <summary>
		/// Initializes an new instance of this class with the specified error message.
		/// </summary>
		/// <param name="field">Field that failed validation.</param>
		/// <param name="messageFormat">Formatted string containing a message that describes the validation exception, which can be presented to the end-user.</param>
		/// <param name="messageFields">Fields whose names will be included in the formatted string.</param>
		public ValidationException(Enum field, string messageFormat, params Enum[] messageFields)
		{
			if (messageFormat == null) throw new ArgumentNullException("messageFormat");
			if (messageFields == null) throw new ArgumentNullException("formatFields");
			if (messageFields.Length == 0) throw new ArgumentException("Array must contain at least one member.", "messageFields");

			_field = field;
			_message = null;
			_messageFormat = messageFormat;
			_messageFields = messageFields;
		}
		

		/// <summary>
		/// Gets the field that failed validation.
		/// </summary>
		public Enum Field
		{
			get { return _field; }
		}
		
		/// <summary>
		/// Gets the format string used to build the actual error message.
		/// </summary>
		public string MessageFormat
		{
			get { return _messageFormat; }
		}
		
		/// <summary>
		/// Gets the fields to be included in the error message.
		/// </summary>
		public Enum[] MessageFields
		{
			get { return _messageFields; }
		}
		
		/// <summary>
		/// Gets the error message, which can be presented to the end-user.
		/// </summary>
		public override string Message
		{
			get
			{
				if (_message == null)
				{
					_message = BuildMessageString();
				}
				return _message;
			}
		}


		private string BuildMessageString()
		{
			// build the array of field names (use the display name when available)
			string[] values = new string[_messageFields.Length];
			for (int i = 0; i < _messageFields.Length; i++)
			{
				UIFieldInfo fieldInfo = UIMapper.Instance.GetFieldInfo(_messageFields[i]);
				if (fieldInfo.DisplayName != null && fieldInfo.DisplayName.Length > 0)
				{
					values[i] = fieldInfo.DisplayName;
				}
				else // no display name
				{
					values[i] = fieldInfo.EnumValue.ToString();
				}
			}

			// build and return the final message string
			return string.Format(_messageFormat, values);
		}
	}
}