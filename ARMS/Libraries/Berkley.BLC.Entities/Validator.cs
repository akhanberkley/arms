using System;
using System.Text.RegularExpressions;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Validation engine used to validate values for field using the rules in the mapping file.
	/// </summary>
	public class Validator
	{
		private Validator()
		{
		}

		/// <summary>
		/// Validates the specified value against the definition of the field.
		/// </summary>
		/// <param name="enumValue">Field to use for validation.</param>
		/// <param name="value">Value to validate.</param>
		public static void Validate(Enum enumValue, object value)
		{
			UIFieldInfo field = UIMapper.Instance.GetFieldInfo(enumValue);
			if( field == null )
			{
				throw new ArgumentException("Could not find field info for field '" + UIMapper.GetFieldName(enumValue) + "'.");
			}

			// verify: value type matches the type of field
			if( value != null && !field.DataType.Equals(value.GetType()) )
			{
				throw new ArgumentException("Value type does not match the field type. Expected type '" + field.DataType.FullName + "'.", "value");
			}

			// see if (the null value for this field was specified
			bool isNullValue = false;
			if( field.IsNullable )
			{
				if( value == null )
				{
					if( field.NullValue == null )
					{
						isNullValue = true;
					}
				}
				else
				{
					if( value.Equals(field.NullValue) )
					{
						isNullValue = true;
					}
				}				
			}

			// verify: a value is specifed (if (field is required)
			if( field.IsRequired && isNullValue )
			{
				throw new ValidationException(enumValue, "{0} must be specified.", enumValue);
			}			

			// preform additional string validations
			if( value is String )
			{
				ValidateString(field, (string)value);
			}
		}

        private static void ValidateString(UIFieldInfo field, string value)
        {
            // nothing left to verify if (the value is blank
            if (value == null || value.Length == 0)
            {
                return;
            }

            // verify: text value is exact length (if (max and min are the same)
            if (field.MaxLength > 0)
            {
                if (field.MinLength == field.MaxLength && value.Length != field.MinLength)
                {
                    throw new ValidationException(field.EnumValue, "{0} must be " + field.MinLength + " characters in length.", field.EnumValue);
                }
            }

            // verify: text value not less than min length
            if (value.Length < field.MinLength)
            {
                throw new ValidationException(field.EnumValue, "{0} must be at least " + field.MinLength + " characters in length.", field.EnumValue);
            }

            // verify: text value not greater than the max length
            if (field.MaxLength > 0)
            {
                if (value.Length > field.MaxLength)
                {
                    throw new ValidationException(field.EnumValue, "{0} cannot exceed " + field.MaxLength + " characters in length.", field.EnumValue);
                }
            }

            // verify: text value matches the regex pattern (if one is specifed)
            if (field.Format != null && field.Format.Length > 0)
            {
                if (!Regex.IsMatch(value, field.Format))
                {
                    throw new ValidationException(field.EnumValue, "{0} is not formatted correctly.", field.EnumValue);
                }
            }
        }
	}
}