﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace Berkley.BLC.Entities.Extensions
{
    public static class EntityExtensions
    {
        //partial credit to http://www.singingeels.com/Blogs/Nullable/2008/03/26/Dynamic_LINQ_OrderBy_using_String_Names.aspx
        public static List<T> Sort<T>(this List<T> entity, string columnName)
        {
            var param = Expression.Parameter(typeof(T), typeof(T).Name);
            var sortExpression = Expression.Lambda<Func<T, object>>(Expression.Convert(Expression.Property(param, columnName), typeof(object)), param);

            return entity.AsQueryable().OrderBy(sortExpression).ToList();
        }

        public static List<T> SortDESC<T>(this List<T> entity, string columnName)
        {
            var param = Expression.Parameter(typeof(T), typeof(T).Name);
            var sortExpression = Expression.Lambda<Func<T, object>>(Expression.Convert(Expression.Property(param, columnName), typeof(object)), param);

            return entity.AsQueryable().OrderByDescending(sortExpression).ToList();
        }

        /// <summary>
        /// Filters an entity by the specified columnname and value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <param name="columnName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static List<T> Filter<T>(this List<T> entity, string columnName, object value)
        {
            var param = Expression.Parameter(typeof(T), typeof(T).Name);

            var filterExpression = Expression.Lambda<Func<T, bool>>(Expression.Equal(Expression.Property(param, columnName), Expression.Constant(value)), param);

            return entity.AsQueryable().Where(filterExpression).ToList();
        }
    }
}
