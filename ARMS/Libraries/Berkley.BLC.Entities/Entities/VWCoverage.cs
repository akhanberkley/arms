using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a VWCoverage entity, which maps to table 'VW_Coverage' in the database.
    /// </summary>
    public class VWCoverage : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private VWCoverage()
        {
        }


        #region --- Generated Members ---

        private Guid _coverageID;
        private string _coverageValue = String.Empty;
        private string _lineOfBusinessCode;
        private string _subLineOfBusinessCode = String.Empty;
        private string _coverageTypeCode;
        private string _coverageNameTypeCode;
        private string _coverageTypeName;
        private string _coverageNameTypeName;
        private int _displayOrder;
        private Guid _insuredID;
        private Guid _coverageNameID;
        private Guid _companyID;
        private Guid _policyID = Guid.Empty;
        private string _policyNumber = String.Empty;
        private int _policyMod = Int32.MinValue;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Coverage ID.
        /// </summary>
        public Guid CoverageID
        {
            get { return _coverageID; }
        }

        /// <summary>
        /// Gets the Coverage Value. Null value is 'String.Empty'.
        /// </summary>
        public string CoverageValue
        {
            get { return _coverageValue; }
        }

        /// <summary>
        /// Gets the Line of Business Code.
        /// </summary>
        public string LineOfBusinessCode
        {
            get { return _lineOfBusinessCode; }
        }

        /// <summary>
        /// Gets the Sub Line of Business Code. Null value is 'String.Empty'.
        /// </summary>
        public string SubLineOfBusinessCode
        {
            get { return _subLineOfBusinessCode; }
        }

        /// <summary>
        /// Gets the Coverage Type Code.
        /// </summary>
        public string CoverageTypeCode
        {
            get { return _coverageTypeCode; }
        }

        /// <summary>
        /// Gets the Coverage Name Type Code.
        /// </summary>
        public string CoverageNameTypeCode
        {
            get { return _coverageNameTypeCode; }
        }

        /// <summary>
        /// Gets the Coverage Type Name.
        /// </summary>
        public string CoverageTypeName
        {
            get { return _coverageTypeName; }
        }

        /// <summary>
        /// Gets the Coverage Name Type Name.
        /// </summary>
        public string CoverageNameTypeName
        {
            get { return _coverageNameTypeName; }
        }

        /// <summary>
        /// Gets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
        }

        /// <summary>
        /// Gets the Insured ID.
        /// </summary>
        public Guid InsuredID
        {
            get { return _insuredID; }
        }

        /// <summary>
        /// Gets the Coverage Name ID.
        /// </summary>
        public Guid CoverageNameID
        {
            get { return _coverageNameID; }
        }

        /// <summary>
        /// Gets the Company ID.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets the Policy ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid PolicyID
        {
            get { return _policyID; }
        }

        /// <summary>
        /// Gets the Policy Number. Null value is 'String.Empty'.
        /// </summary>
        public string PolicyNumber
        {
            get { return _policyNumber; }
        }

        /// <summary>
        /// Gets the Policy Mod. Null value is 'Int32.MinValue'.
        /// </summary>
        public int PolicyMod
        {
            get { return _policyMod; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_coverageID": return _coverageID;
                    case "_coverageValue": return _coverageValue;
                    case "_lineOfBusinessCode": return _lineOfBusinessCode;
                    case "_subLineOfBusinessCode": return _subLineOfBusinessCode;
                    case "_coverageTypeCode": return _coverageTypeCode;
                    case "_coverageNameTypeCode": return _coverageNameTypeCode;
                    case "_coverageTypeName": return _coverageTypeName;
                    case "_coverageNameTypeName": return _coverageNameTypeName;
                    case "_displayOrder": return _displayOrder;
                    case "_insuredID": return _insuredID;
                    case "_coverageNameID": return _coverageNameID;
                    case "_companyID": return _companyID;
                    case "_policyID": return _policyID;
                    case "_policyNumber": return _policyNumber;
                    case "_policyMod": return _policyMod;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_coverageID": _coverageID = (Guid)value; break;
                    case "_coverageValue": _coverageValue = (string)value; break;
                    case "_lineOfBusinessCode": _lineOfBusinessCode = (string)value; break;
                    case "_subLineOfBusinessCode": _subLineOfBusinessCode = (string)value; break;
                    case "_coverageTypeCode": _coverageTypeCode = (string)value; break;
                    case "_coverageNameTypeCode": _coverageNameTypeCode = (string)value; break;
                    case "_coverageTypeName": _coverageTypeName = (string)value; break;
                    case "_coverageNameTypeName": _coverageNameTypeName = (string)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    case "_insuredID": _insuredID = (Guid)value; break;
                    case "_coverageNameID": _coverageNameID = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_policyID": _policyID = (Guid)value; break;
                    case "_policyNumber": _policyNumber = (string)value; break;
                    case "_policyMod": _policyMod = (int)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="coverageID">CoverageID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static VWCoverage Get(Guid coverageID)
        {
            OPathQuery query = new OPathQuery(typeof(VWCoverage), "CoverageID = ?");
            object entity = DB.Engine.GetObject(query, coverageID);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch VWCoverage entity with CoverageID = '{0}'.", coverageID));
            }
            return (VWCoverage)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static VWCoverage GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWCoverage), opathExpression);
            return (VWCoverage)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static VWCoverage GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(VWCoverage))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (VWCoverage)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWCoverage[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWCoverage), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWCoverage[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(VWCoverage))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (VWCoverage[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWCoverage[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWCoverage), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _coverageID = Guid.Empty;
            _coverageValue = null;
            _lineOfBusinessCode = null;
            _subLineOfBusinessCode = null;
            _coverageTypeCode = null;
            _coverageNameTypeCode = null;
            _coverageTypeName = null;
            _coverageNameTypeName = null;
            _displayOrder = Int32.MinValue;
            _insuredID = Guid.Empty;
            _coverageNameID = Guid.Empty;
            _companyID = Guid.Empty;
            _policyID = Guid.Empty;
            _policyNumber = null;
            _policyMod = Int32.MinValue;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the VWCoverage entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the CoverageID field.
            /// </summary>
            CoverageID,
            /// <summary>
            /// Represents the CoverageValue field.
            /// </summary>
            CoverageValue,
            /// <summary>
            /// Represents the LineOfBusinessCode field.
            /// </summary>
            LineOfBusinessCode,
            /// <summary>
            /// Represents the SubLineOfBusinessCode field.
            /// </summary>
            SubLineOfBusinessCode,
            /// <summary>
            /// Represents the CoverageTypeCode field.
            /// </summary>
            CoverageTypeCode,
            /// <summary>
            /// Represents the CoverageNameTypeCode field.
            /// </summary>
            CoverageNameTypeCode,
            /// <summary>
            /// Represents the CoverageTypeName field.
            /// </summary>
            CoverageTypeName,
            /// <summary>
            /// Represents the CoverageNameTypeName field.
            /// </summary>
            CoverageNameTypeName,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
            /// <summary>
            /// Represents the InsuredID field.
            /// </summary>
            InsuredID,
            /// <summary>
            /// Represents the CoverageNameID field.
            /// </summary>
            CoverageNameID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the PolicyID field.
            /// </summary>
            PolicyID,
            /// <summary>
            /// Represents the PolicyNumber field.
            /// </summary>
            PolicyNumber,
            /// <summary>
            /// Represents the PolicyMod field.
            /// </summary>
            PolicyMod,
        }

        #endregion
    }
}