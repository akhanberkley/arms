using System;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a Company entity, which maps to table 'Company' in the database.
    /// </summary>
    public class Company : IObjectHelper
    {
        private static Hashtable _allowedActions = Hashtable.Synchronized(new Hashtable());

        #region Public Fields

        /// <summary>
        /// Represents Acadia Insurance Company.
        /// </summary>
        public static readonly Company AcadiaInsuranceCompany = Company.Get(new Guid("BC202DBE-E09B-4C8E-804C-7D90D3C40B46"));
        /// <summary>
        /// Represents Berkley Agribusiness Risk Specialists.
        /// </summary>
        public static readonly Company BerkleyAgribusinessRiskSpecialists = Company.Get(new Guid("982BAF6D-A134-4136-8865-C4653A14CA8F"));
        /// <summary>
        /// Represents Berkley Mid-Atlantic Group.
        /// </summary>
        public static readonly Company BerkleyMidAtlanticGroup = Company.Get(new Guid("77C82BD6-0BD9-4D79-92FD-C133788A3B0E"));
        /// <summary>
        /// Represents Continental Western Group.
        /// </summary>
        public static readonly Company ContinentalWesternGroup = Company.Get(new Guid("30E6C82E-2DF1-44B8-82B7-3B34654CF282"));
        /// <summary>
        /// Represents Union Standard Insurance Group.
        /// </summary>
        public static readonly Company UnionStandardInsuranceGroup = Company.Get(new Guid("25f08c73-e8a3-4475-af1a-91882abcdb76"));
        /// <summary>
        /// Represents WR Berkley Corporation.
        /// </summary>
        public static readonly Company WRBerkleyCorporation = Company.Get(new Guid("5e7b1b53-da4c-4d80-af93-e3948911b2ea"));
        /// <summary>
        /// Represents Berkley North Pacific Group.
        /// </summary>
        public static readonly Company BerkleyNorthPacificGroup = Company.Get(new Guid("4E482502-80CD-43CC-9632-E579180560C0"));
        /// <summary>
        /// Represents Carolina Casualty Insurance Group.
        /// </summary>
        public static readonly Company CarolinaCasualtyInsuranceGroup = Company.Get(new Guid("02F6BF97-7F88-4D44-9449-27A31B89E7C3"));
        /// <summary>
        /// Represents Berkley Life Sciences.
        /// </summary>
        public static readonly Company BerkleyLifeSciences = Company.Get(new Guid("7F521E36-C296-47C7-9012-B47EE76A09D1"));
        /// <summary>
        /// Represents Berkley Oil Gas.
        /// </summary>
        public static readonly Company BerkleyOilGas = Company.Get(new Guid("E0BEA76A-B9D0-4A42-8ECF-FCA0ECC793EE"));
        /// <summary>
        /// Represents Berkley Specialty Underwriting Managers.
        /// </summary>
        public static readonly Company BerkleySpecialtyUnderwritingManagers = Company.Get(new Guid("02e15646-1058-40ed-add1-00197fb4c93a"));
        /// <summary>
        /// Represents Berkley Fire Marine
        /// </summary>
        public static readonly Company BerkleyFireMarine = Company.Get(new Guid("35f08c73-e8a3-4475-af1a-91882abcdb89"));
        /// <summary>
        /// Represents Berkley Fire Marine
        /// </summary>
        public static readonly Company BerkleySouthEast = Company.Get(new Guid("87c82bd6-0bd9-4d79-92fd-c133788a3b02"));
        /// <summary>
        /// Represents Berkley Fire Marine
        /// </summary>
        public static readonly Company BerkleyTechnologyUnderwriters = Company.Get(new Guid("EC0203CE-18A8-4444-B0E1-D98FFC469E3F"));
        /// <summary>
        /// Represents Berkley Fire Marine
        /// </summary>
        public static readonly Company RiverportInsuranceCompany = Company.Get(new Guid("00E140A3-F45C-4687-8232-28A86FA5DFAE"));


        #endregion

        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private Company()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this Company.</param>
        public Company(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Checks if this company allows a given action to be taken on surveys with a given status.
        /// </summary>
        /// <param name="actionType">Type of action.</param>
        /// <param name="status">Status of survey.</param>
        /// <returns>True of this company allows the action; otherwise, false.</returns>
        public bool IsActionAllowed(SurveyActionType actionType, SurveyStatus status)
        {
            string key = this.ID + ":" + actionType.Code + ":" + status.Code;

            object flag = _allowedActions[key];
            if (flag == null) // cache miss
            {
                // see if this action is defined
                SurveyAction action = SurveyAction.GetOne("CompanyID = ? && SurveyStatusCode = ? && TypeCode = ?", this.ID, status.Code, actionType.Code);

                flag = (action != null);
                _allowedActions[key] = flag;
            }

            return (bool)flag;
        }

        /// <summary>
        /// Checks if this company allows a given action to be taken on service plans with a given status.
        /// </summary>
        /// <param name="actionType">Type of action.</param>
        /// <param name="status">Status of service plan.</param>
        /// <returns>True of this company allows the action; otherwise, false.</returns>
        public bool IsActionAllowed(ServicePlanActionType actionType, ServicePlanStatus status)
        {
            string key = this.ID + ":" + actionType.Code + ":" + status.Code;

            object flag = _allowedActions[key];
            if (flag == null) // cache miss
            {
                // see if this action is defined
                ServicePlanAction action = ServicePlanAction.GetOne("CompanyID = ? && ServicePlanStatusCode = ? && TypeCode = ?", this.ID, status.Code, actionType.Code);

                flag = (action != null);
                _allowedActions[key] = flag;
            }

            return (bool)flag;
        }

        /// <summary>
        /// Returns an array of users in this Company who have permission to work surveys
        /// and match the specified filter condition.
        /// </summary>
        /// <param name="filter">A User-based OPath filter expression.</param>
        /// <param name="parameters">Values for the parameters used in the filter expression.</param>
        /// <returns>An array of Users.</returns>
        public User[] GetConsultants(string filter, params object[] parameters)
        {
            string query = "CompanyID = ? && (Role.PermissionSet.CanWorkSurveys = true)";

            ArrayList args = new ArrayList();
            args.Add(this.ID);

            // add additional filter and parameters if provided
            if (filter != null && filter.Length > 0)
            {
                query += " && " + filter;
            }
            if (parameters != null && parameters.Length > 0)
            {
                args.AddRange(parameters);
            }

            return User.GetSortedArray(query, "Name ASC", args.ToArray());
        }

        /// <summary>
        /// Returns an array of users in this Company who have permission to work reviews
        /// and match the specified filter condition.
        /// </summary>
        /// <param name="filter">A User-based OPath filter expression.</param>
        /// <param name="parameters">Values for the parameters used in the filter expression.</param>
        /// <returns>An array of Users.</returns>
        public User[] GetReviewStaff(string filter, params object[] parameters)
        {
            string query = "CompanyID = ? && (Role.PermissionSet.CanWorkReviews = true)";

            ArrayList args = new ArrayList();
            args.Add(this.ID);

            // add additional filter and parameters if provided
            if (filter != null && filter.Length > 0)
            {
                query += " && " + filter;
            }
            if (parameters != null && parameters.Length > 0)
            {
                args.AddRange(parameters);
            }

            return User.GetSortedArray(query, "Name ASC", args.ToArray());
        }

        /// <summary>
        /// Returns an array of all active Users who have permission to work surveys or work reviews.
        /// </summary>
        /// <returns>Array of Users.</returns>
        public User[] GetActiveUsers()
        {
            string query = "CompanyID = ? && AccountDisabled = false && (Role.PermissionSet.CanWorkSurveys = true || Role.PermissionSet.CanWorkReviews = true)";

            ArrayList args = new ArrayList();
            args.Add(this.ID);

            return User.GetSortedArray(query, "Name ASC", args.ToArray());
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private string _name;
        private string _abbreviation;
        private string _longAbbreviation;
        private string _number;
        private string _phone = String.Empty;
        private string _fax = String.Empty;
        private string _primaryDomainName;
        private string _policyStarEndPointUrl = String.Empty;
        private string _pdrEndPointUrl = String.Empty;
        private int _dsik = Int32.MinValue;
        private string _apsEndPointUrl = String.Empty;
        private string _fileNetDocClass;
        private string _fileNetUsername;
        private string _fileNetPassword;
        private string _fileNetClientIDIndexName;
        private string _fileNetClientIDIndexNameP8; //WA-697 Doug Bruce 2015-05-21 - Add field for P8 so ClientID search will work correctly for all companies.
        private bool _supportsFileNetIndexPolicySymbol;
        private bool _supportsFileNetIndexAgencyNumber;
        private bool _supportsFileNetIndexSourceSystem;
        private int _surveyDaysUntilDue;
        private int _surveyNearDue;
        private int _reviewDaysUntilDue;
        private int _reviewNearDue;
        private string _emailFromAddress;
        private Guid _feeCompanyUserRoleID;
        private Guid _underwriterUserRoleID = Guid.Empty;
        private int _nextFullReportDaysUntilDue;
        private bool _agentDataIsRequied;
        private int _yearsOfLossHistory;
        private bool _importRateModFactors;
        private bool _defaultLocationsToOneSurvey;
        private bool _optionToReviewIsVisible;
        private bool _importQuotes;
        private string _nonRequiredLOBHazardGrades = String.Empty;
        private bool _sicSortedByAlpha;
        private bool _naicSortedByAlpha;
        private bool _supportsOverallGrading;
        private bool _supportsCallCount;
        private string _callCountLabel;
        private bool _supportsSeverityRating;
        private bool _supportsSimplifiedAcctMgmt;
        private bool _requireVendorCost;
        private bool _supportsVendorRecReminder;
        private bool _supportsInvoiceFields;
        private bool _supportsBusinessCategory;
        private bool _supportsNAICS;
        private bool _supportsPortfolioID;
        private bool _requireDocTypeOnCreateSurvey;
        private bool _requireCommentOnCreateSurvey;
        private bool _supportsSurveysDirectToFieldReps;
        private bool _supportsBuildingValuation;
        private string _createSurveyTypeCode;
        private bool _supportsDOTNumber;
        private int _daysToAutoRefreshSurvey = Int32.MinValue;
        private bool _requireSICCode;
        private bool _requireDocRemarks;
        private bool _supportsDocRemarks;
        private bool _createServicePlansFromProspects;
        private bool _requireDocumentUpload;
        private bool _requireServiceVisitObjective;
        private bool _supportsBuildings;
        private bool _supportsNonDisclosure;
        private bool _requirePolicyPremium;
        private bool _displayPolicyPremium;
        private bool _displayClaimsByDefault;
        private string _defaultInsuredContact = String.Empty;
        private string _policyBranchCodeLabel;
        private string _buildingSprinklerSystemLabel;
        private bool _supportsExtractableDocuments;
        private DateTime _milestoneDate = DateTime.MinValue;
        private bool _hideServicePlanVisitsByDefault;
        private bool _allowUnderwritersToEditRecs;
        private bool _displayRecsOnSurveyScreen;
        private bool _supportsQuickSurveyCreation;
        private int _surveyDaysCount;
        private ObjectHolder _createSurveyTypeHolder = null;
        private string _fileNetDocClassP8;                      //WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
        private string _fileNetUsernameP8;                      //WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
        private string _fileNetPasswordP8;                      //WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
        private string _fileNetVersion;                         //WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Company ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Company Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                VerifyWritable();
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Company Abbreviation.
        /// </summary>
        public string Abbreviation
        {
            get { return _abbreviation; }
            set
            {
                VerifyWritable();
                _abbreviation = value;
            }
        }

        /// <summary>
        /// Gets or sets the Company Long Abbreviation.
        /// </summary>
        public string LongAbbreviation
        {
            get { return _longAbbreviation; }
            set
            {
                VerifyWritable();
                _longAbbreviation = value;
            }
        }

        /// <summary>
        /// Gets or sets the Company Number.
        /// </summary>
        public string Number
        {
            get { return _number; }
            set
            {
                VerifyWritable();
                _number = value;
            }
        }

        /// <summary>
        /// Gets or sets the Company Phone. Null value is 'String.Empty'.
        /// </summary>
        public string Phone
        {
            get { return _phone; }
            set
            {
                VerifyWritable();
                _phone = value;
            }
        }

        /// <summary>
        /// Gets or sets the Company Fax. Null value is 'String.Empty'.
        /// </summary>
        public string Fax
        {
            get { return _fax; }
            set
            {
                VerifyWritable();
                _fax = value;
            }
        }

        /// <summary>
        /// Gets or sets the Primary Domain Name.
        /// </summary>
        public string PrimaryDomainName
        {
            get { return _primaryDomainName; }
            set
            {
                VerifyWritable();
                _primaryDomainName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Star End Point Url. Null value is 'String.Empty'.
        /// </summary>
        public string PolicyStarEndPointUrl
        {
            get { return _policyStarEndPointUrl; }
            set
            {
                VerifyWritable();
                _policyStarEndPointUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the PDR End Point Url. Null value is 'String.Empty'.
        /// </summary>
        public string PDREndPointUrl
        {
            get { return _pdrEndPointUrl; }
            set
            {
                VerifyWritable();
                _pdrEndPointUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the DSIK. Null value is 'Int32.MinValue'.
        /// </summary>
        public int DSIK
        {
            get { return _dsik; }
            set
            {
                VerifyWritable();
                _dsik = value;
            }
        }

        /// <summary>
        /// Gets or sets the APS End Point Url. Null value is 'String.Empty'.
        /// </summary>
        public string APSEndPointUrl
        {
            get { return _apsEndPointUrl; }
            set
            {
                VerifyWritable();
                _apsEndPointUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Doc Class.
        /// </summary>
        public string FileNetDocClass
        {
            get { return _fileNetDocClass; }
            set
            {
                VerifyWritable();
                _fileNetDocClass = value;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Username.
        /// </summary>
        public string FileNetUsername
        {
            get { return _fileNetUsername; }
            set
            {
                VerifyWritable();
                _fileNetUsername = value;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Password.
        /// </summary>
        public string FileNetPassword
        {
            get { return _fileNetPassword; }
            set
            {
                VerifyWritable();
                _fileNetPassword = value;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Client ID Index Name.
        /// </summary>
        public string FileNetClientIDIndexName
        {
            get { return _fileNetClientIDIndexName; }
            set
            {
                VerifyWritable();
                _fileNetClientIDIndexName = value;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Client ID Index Name.
        /// </summary>
        public string FileNetClientIDIndexNameP8 //WA-697 Doug Bruce 2015-05-21 - Add field for P8 so ClientID search will work correctly for all companies.
        {
            get { return _fileNetClientIDIndexNameP8; }
            set
            {
                VerifyWritable();
                _fileNetClientIDIndexNameP8 = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports File Net Index Policy Symbol.
        /// </summary>
        public bool SupportsFileNetIndexPolicySymbol
        {
            get { return _supportsFileNetIndexPolicySymbol; }
            set
            {
                VerifyWritable();
                _supportsFileNetIndexPolicySymbol = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports File Net Index Agency Number.
        /// </summary>
        public bool SupportsFileNetIndexAgencyNumber
        {
            get { return _supportsFileNetIndexAgencyNumber; }
            set
            {
                VerifyWritable();
                _supportsFileNetIndexAgencyNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports File Net Index Source System.
        /// </summary>
        public bool SupportsFileNetIndexSourceSystem
        {
            get { return _supportsFileNetIndexSourceSystem; }
            set
            {
                VerifyWritable();
                _supportsFileNetIndexSourceSystem = value;
            }
        }

        /// <summary>
        /// Gets or sets the Survey Days Until Due.
        /// </summary>
        public int SurveyDaysUntilDue
        {
            get { return _surveyDaysUntilDue; }
            set
            {
                VerifyWritable();
                _surveyDaysUntilDue = value;
            }
        }

        /// <summary>
        /// Gets or sets the Survey Near Due.
        /// </summary>
        public int SurveyNearDue
        {
            get { return _surveyNearDue; }
            set
            {
                VerifyWritable();
                _surveyNearDue = value;
            }
        }

        /// <summary>
        /// Gets or sets the Review Days Until Due.
        /// </summary>
        public int ReviewDaysUntilDue
        {
            get { return _reviewDaysUntilDue; }
            set
            {
                VerifyWritable();
                _reviewDaysUntilDue = value;
            }
        }

        /// <summary>
        /// Gets or sets the Review Near Due.
        /// </summary>
        public int ReviewNearDue
        {
            get { return _reviewNearDue; }
            set
            {
                VerifyWritable();
                _reviewNearDue = value;
            }
        }

        /// <summary>
        /// Gets or sets the Email From Address.
        /// </summary>
        public string EmailFromAddress
        {
            get { return _emailFromAddress; }
            set
            {
                VerifyWritable();
                _emailFromAddress = value;
            }
        }

        /// <summary>
        /// Gets or sets the Fee Company User Role ID.
        /// </summary>
        public Guid FeeCompanyUserRoleID
        {
            get { return _feeCompanyUserRoleID; }
            set
            {
                VerifyWritable();
                _feeCompanyUserRoleID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Underwriter User Role ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid UnderwriterUserRoleID
        {
            get { return _underwriterUserRoleID; }
            set
            {
                VerifyWritable();
                _underwriterUserRoleID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Next Full Report Days Until Due.
        /// </summary>
        public int NextFullReportDaysUntilDue
        {
            get { return _nextFullReportDaysUntilDue; }
            set
            {
                VerifyWritable();
                _nextFullReportDaysUntilDue = value;
            }
        }

        /// <summary>
        /// Gets or sets the Agent Data Is Requied.
        /// </summary>
        public bool AgentDataIsRequied
        {
            get { return _agentDataIsRequied; }
            set
            {
                VerifyWritable();
                _agentDataIsRequied = value;
            }
        }

        /// <summary>
        /// Gets or sets the Years of Loss History.
        /// </summary>
        public int YearsOfLossHistory
        {
            get { return _yearsOfLossHistory; }
            set
            {
                VerifyWritable();
                _yearsOfLossHistory = value;
            }
        }

        /// <summary>
        /// Gets or sets the Import Rate Mod Factors.
        /// </summary>
        public bool ImportRateModFactors
        {
            get { return _importRateModFactors; }
            set
            {
                VerifyWritable();
                _importRateModFactors = value;
            }
        }

        /// <summary>
        /// Gets or sets the Default Locations to One Survey.
        /// </summary>
        public bool DefaultLocationsToOneSurvey
        {
            get { return _defaultLocationsToOneSurvey; }
            set
            {
                VerifyWritable();
                _defaultLocationsToOneSurvey = value;
            }
        }

        /// <summary>
        /// Gets or sets the Option to Review Is Visible.
        /// </summary>
        public bool OptionToReviewIsVisible
        {
            get { return _optionToReviewIsVisible; }
            set
            {
                VerifyWritable();
                _optionToReviewIsVisible = value;
            }
        }

        /// <summary>
        /// Gets or sets the Import Quotes.
        /// </summary>
        public bool ImportQuotes
        {
            get { return _importQuotes; }
            set
            {
                VerifyWritable();
                _importQuotes = value;
            }
        }

        /// <summary>
        /// Gets or sets the Non Required LOB Hazard Grades. Null value is 'String.Empty'.
        /// </summary>
        public string NonRequiredLOBHazardGrades
        {
            get { return _nonRequiredLOBHazardGrades; }
            set
            {
                VerifyWritable();
                _nonRequiredLOBHazardGrades = value;
            }
        }

        /// <summary>
        /// Gets or sets the SIC Sorted By Alpha.
        /// </summary>
        public bool SICSortedByAlpha
        {
            get { return _sicSortedByAlpha; }
            set
            {
                VerifyWritable();
                _sicSortedByAlpha = value;
            }
        }

        /// <summary>
        /// Gets or sets the SIC Sorted By Alpha.
        /// </summary>
        public bool NAICSortedByAlpha
        {
            get { return _naicSortedByAlpha; }
            set
            {
                VerifyWritable();
                _naicSortedByAlpha = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Overall Grading.
        /// </summary>
        public bool SupportsOverallGrading
        {
            get { return _supportsOverallGrading; }
            set
            {
                VerifyWritable();
                _supportsOverallGrading = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Call Count.
        /// </summary>
        public bool SupportsCallCount
        {
            get { return _supportsCallCount; }
            set
            {
                VerifyWritable();
                _supportsCallCount = value;
            }
        }

        /// <summary>
        /// Gets or sets the Call Count Label.
        /// </summary>
        public string CallCountLabel
        {
            get { return _callCountLabel; }
            set
            {
                VerifyWritable();
                _callCountLabel = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Severity Rating.
        /// </summary>
        public bool SupportsSeverityRating
        {
            get { return _supportsSeverityRating; }
            set
            {
                VerifyWritable();
                _supportsSeverityRating = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Simplified Acct Mgmt.
        /// </summary>
        public bool SupportsSimplifiedAcctMgmt
        {
            get { return _supportsSimplifiedAcctMgmt; }
            set
            {
                VerifyWritable();
                _supportsSimplifiedAcctMgmt = value;
            }
        }

        /// <summary>
        /// Gets or sets the Require Vendor Cost.
        /// </summary>
        public bool RequireVendorCost
        {
            get { return _requireVendorCost; }
            set
            {
                VerifyWritable();
                _requireVendorCost = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Vendor Rec Reminder.
        /// </summary>
        public bool SupportsVendorRecReminder
        {
            get { return _supportsVendorRecReminder; }
            set
            {
                VerifyWritable();
                _supportsVendorRecReminder = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Invoice Fields.
        /// </summary>
        public bool SupportsInvoiceFields
        {
            get { return _supportsInvoiceFields; }
            set
            {
                VerifyWritable();
                _supportsInvoiceFields = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Business Category.
        /// </summary>
        public bool SupportsBusinessCategory
        {
            get { return _supportsBusinessCategory; }
            set
            {
                VerifyWritable();
                _supportsBusinessCategory = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports NAICS.
        /// </summary>
        public bool SupportsNAICS
        {
            get { return _supportsNAICS; }
            set
            {
                VerifyWritable();
                _supportsNAICS = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Portfolio ID.
        /// </summary>
        public bool SupportsPortfolioID
        {
            get { return _supportsPortfolioID; }
            set
            {
                VerifyWritable();
                _supportsPortfolioID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Require Doc Type On Create Survey.
        /// </summary>
        public bool RequireDocTypeOnCreateSurvey
        {
            get { return _requireDocTypeOnCreateSurvey; }
            set
            {
                VerifyWritable();
                _requireDocTypeOnCreateSurvey = value;
            }
        }

        /// <summary>
        /// Gets or sets the Require Comment On Create Survey.
        /// </summary>
        public bool RequireCommentOnCreateSurvey
        {
            get { return _requireCommentOnCreateSurvey; }
            set
            {
                VerifyWritable();
                _requireCommentOnCreateSurvey = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Surveys Direct to Field Reps.
        /// </summary>
        public bool SupportsSurveysDirectToFieldReps
        {
            get { return _supportsSurveysDirectToFieldReps; }
            set
            {
                VerifyWritable();
                _supportsSurveysDirectToFieldReps = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Building Valuation.
        /// </summary>
        public bool SupportsBuildingValuation
        {
            get { return _supportsBuildingValuation; }
            set
            {
                VerifyWritable();
                _supportsBuildingValuation = value;
            }
        }

        /// <summary>
        /// Gets or sets the Create Survey Type.
        /// </summary>
        public string CreateSurveyTypeCode
        {
            get { return _createSurveyTypeCode; }
            set
            {
                VerifyWritable();
                _createSurveyTypeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports DOT Number.
        /// </summary>
        public bool SupportsDOTNumber
        {
            get { return _supportsDOTNumber; }
            set
            {
                VerifyWritable();
                _supportsDOTNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Days to Auto Refresh Survey. Null value is 'Int32.MinValue'.
        /// </summary>
        public int DaysToAutoRefreshSurvey
        {
            get { return _daysToAutoRefreshSurvey; }
            set
            {
                VerifyWritable();
                _daysToAutoRefreshSurvey = value;
            }
        }

        /// <summary>
        /// Gets or sets the Require SIC Code.
        /// </summary>
        public bool RequireSICCode
        {
            get { return _requireSICCode; }
            set
            {
                VerifyWritable();
                _requireSICCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Require Doc Remarks.
        /// </summary>
        public bool RequireDocRemarks
        {
            get { return _requireDocRemarks; }
            set
            {
                VerifyWritable();
                _requireDocRemarks = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Doc Remarks.
        /// </summary>
        public bool SupportsDocRemarks
        {
            get { return _supportsDocRemarks; }
            set
            {
                VerifyWritable();
                _supportsDocRemarks = value;
            }
        }

        /// <summary>
        /// Gets or sets the Create Service Plans From Prospects.
        /// </summary>
        public bool CreateServicePlansFromProspects
        {
            get { return _createServicePlansFromProspects; }
            set
            {
                VerifyWritable();
                _createServicePlansFromProspects = value;
            }
        }

        /// <summary>
        /// Gets or sets the Require Document Upload.
        /// </summary>
        public bool RequireDocumentUpload
        {
            get { return _requireDocumentUpload; }
            set
            {
                VerifyWritable();
                _requireDocumentUpload = value;
            }
        }

        /// <summary>
        /// Gets or sets the Require Service Visit Objective.
        /// </summary>
        public bool RequireServiceVisitObjective
        {
            get { return _requireServiceVisitObjective; }
            set
            {
                VerifyWritable();
                _requireServiceVisitObjective = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Buildings.
        /// </summary>
        public bool SupportsBuildings
        {
            get { return _supportsBuildings; }
            set
            {
                VerifyWritable();
                _supportsBuildings = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Non Disclosure.
        /// </summary>
        public bool SupportsNonDisclosure
        {
            get { return _supportsNonDisclosure; }
            set
            {
                VerifyWritable();
                _supportsNonDisclosure = value;
            }
        }

        /// <summary>
        /// Gets or sets the Require Policy Premium.
        /// </summary>
        public bool RequirePolicyPremium
        {
            get { return _requirePolicyPremium; }
            set
            {
                VerifyWritable();
                _requirePolicyPremium = value;
            }
        }

        /// <summary>
        /// Gets or sets the Display Policy Premium.
        /// </summary>
        public bool DisplayPolicyPremium
        {
            get { return _displayPolicyPremium; }
            set
            {
                VerifyWritable();
                _displayPolicyPremium = value;
            }
        }

        /// <summary>
        /// Gets or sets the Display Claims By Default.
        /// </summary>
        public bool DisplayClaimsByDefault
        {
            get { return _displayClaimsByDefault; }
            set
            {
                VerifyWritable();
                _displayClaimsByDefault = value;
            }
        }

        /// <summary>
        /// Gets or sets the Default Insured Contact. Null value is 'String.Empty'.
        /// </summary>
        public string DefaultInsuredContact
        {
            get { return _defaultInsuredContact; }
            set
            {
                VerifyWritable();
                _defaultInsuredContact = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Branch Code Label.
        /// </summary>
        public string PolicyBranchCodeLabel
        {
            get { return _policyBranchCodeLabel; }
            set
            {
                VerifyWritable();
                _policyBranchCodeLabel = value;
            }
        }

        /// <summary>
        /// Gets or sets the Building Sprinkler System Label.
        /// </summary>
        public string BuildingSprinklerSystemLabel
        {
            get { return _buildingSprinklerSystemLabel; }
            set
            {
                VerifyWritable();
                _buildingSprinklerSystemLabel = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Extractable Documents.
        /// </summary>
        public bool SupportsExtractableDocuments
        {
            get { return _supportsExtractableDocuments; }
            set
            {
                VerifyWritable();
                _supportsExtractableDocuments = value;
            }
        }

        /// <summary>
        /// Gets or sets the Milestone Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime MilestoneDate
        {
            get { return _milestoneDate; }
            set
            {
                VerifyWritable();
                _milestoneDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Hide Service Plan Visits By Default.
        /// </summary>
        public bool HideServicePlanVisitsByDefault
        {
            get { return _hideServicePlanVisitsByDefault; }
            set
            {
                VerifyWritable();
                _hideServicePlanVisitsByDefault = value;
            }
        }

        /// <summary>
        /// Gets or sets the Allow Underwriters to Edit Recs.
        /// </summary>
        public bool AllowUnderwritersToEditRecs
        {
            get { return _allowUnderwritersToEditRecs; }
            set
            {
                VerifyWritable();
                _allowUnderwritersToEditRecs = value;
            }
        }

        /// <summary>
        /// Gets or sets the Display Recs On Survey Screen.
        /// </summary>
        public bool DisplayRecsOnSurveyScreen
        {
            get { return _displayRecsOnSurveyScreen; }
            set
            {
                VerifyWritable();
                _displayRecsOnSurveyScreen = value;
            }
        }

        /// <summary>
        /// Gets or sets the Supports Quick Survey Creation.
        /// </summary>
        public bool SupportsQuickSurveyCreation
        {
            get { return _supportsQuickSurveyCreation; }
            set
            {
                VerifyWritable();
                _supportsQuickSurveyCreation = value;
            }
        }
        /// <summary>
        /// Gets or sets the Survey Days Count.
        /// </summary>
        public int SurveyDaysCount
        {
            get { return _surveyDaysCount; }
            set
            {
                VerifyWritable();
                _surveyDaysCount = value;
            }
        }

        /// <summary>
        /// Gets the instance of a CreateSurveyType object related to this entity.
        /// </summary>
        public CreateSurveyType CreateSurveyType
        {
            get
            {
                _createSurveyTypeHolder.Key = _createSurveyTypeCode;
                return (CreateSurveyType)_createSurveyTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Doc Class.
        /// WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
        /// </summary>
        public string FileNetDocClassP8
        {
            get { return _fileNetDocClassP8; }
            set
            {
                VerifyWritable();
                _fileNetDocClassP8 = value;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Username.
        /// WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
        /// </summary>
        public string FileNetUsernameP8
        {
            get { return _fileNetUsernameP8; }
            set
            {
                VerifyWritable();
                _fileNetUsernameP8 = value;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Password.
        /// WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
        /// </summary>
        public string FileNetPasswordP8
        {
            get { return _fileNetPasswordP8; }
            set
            {
                VerifyWritable();
                _fileNetPasswordP8 = value;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Version.
        /// WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
        /// </summary>
        public string FileNetVersion
        {
            get { return _fileNetVersion; }
            set
            {
                VerifyWritable();
                _fileNetVersion = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_name": return _name;
                    case "_abbreviation": return _abbreviation;
                    case "_longAbbreviation": return _longAbbreviation;
                    case "_number": return _number;
                    case "_phone": return _phone;
                    case "_fax": return _fax;
                    case "_primaryDomainName": return _primaryDomainName;
                    case "_policyStarEndPointUrl": return _policyStarEndPointUrl;
                    case "_pdrEndPointUrl": return _pdrEndPointUrl;
                    case "_dsik": return _dsik;
                    case "_apsEndPointUrl": return _apsEndPointUrl;
                    case "_fileNetDocClass": return _fileNetDocClass;
                    case "_fileNetUsername": return _fileNetUsername;
                    case "_fileNetPassword": return _fileNetPassword;
                    case "_fileNetClientIDIndexName": return _fileNetClientIDIndexName;
                    case "_fileNetClientIDIndexNameP8": return _fileNetClientIDIndexNameP8; //WA-697 Doug Bruce 2015-05-21 - Add field for P8 so ClientID search will work correctly for all companies.
                    case "_supportsFileNetIndexPolicySymbol": return _supportsFileNetIndexPolicySymbol;
                    case "_supportsFileNetIndexAgencyNumber": return _supportsFileNetIndexAgencyNumber;
                    case "_supportsFileNetIndexSourceSystem": return _supportsFileNetIndexSourceSystem;
                    case "_surveyDaysUntilDue": return _surveyDaysUntilDue;
                    case "_surveyNearDue": return _surveyNearDue;
                    case "_reviewDaysUntilDue": return _reviewDaysUntilDue;
                    case "_reviewNearDue": return _reviewNearDue;
                    case "_emailFromAddress": return _emailFromAddress;
                    case "_feeCompanyUserRoleID": return _feeCompanyUserRoleID;
                    case "_underwriterUserRoleID": return _underwriterUserRoleID;
                    case "_nextFullReportDaysUntilDue": return _nextFullReportDaysUntilDue;
                    case "_agentDataIsRequied": return _agentDataIsRequied;
                    case "_yearsOfLossHistory": return _yearsOfLossHistory;
                    case "_importRateModFactors": return _importRateModFactors;
                    case "_defaultLocationsToOneSurvey": return _defaultLocationsToOneSurvey;
                    case "_optionToReviewIsVisible": return _optionToReviewIsVisible;
                    case "_importQuotes": return _importQuotes;
                    case "_nonRequiredLOBHazardGrades": return _nonRequiredLOBHazardGrades;
                    case "_sicSortedByAlpha": return _sicSortedByAlpha;
                    case "_supportsOverallGrading": return _supportsOverallGrading;
                    case "_supportsCallCount": return _supportsCallCount;
                    case "_callCountLabel": return _callCountLabel;
                    case "_supportsSeverityRating": return _supportsSeverityRating;
                    case "_supportsSimplifiedAcctMgmt": return _supportsSimplifiedAcctMgmt;
                    case "_requireVendorCost": return _requireVendorCost;
                    case "_supportsVendorRecReminder": return _supportsVendorRecReminder;
                    case "_supportsInvoiceFields": return _supportsInvoiceFields;
                    case "_supportsBusinessCategory": return _supportsBusinessCategory;
                    case "_supportsNAICS": return _supportsNAICS;
                    case "_supportsPortfolioID": return _supportsPortfolioID;
                    case "_requireDocTypeOnCreateSurvey": return _requireDocTypeOnCreateSurvey;
                    case "_requireCommentOnCreateSurvey": return _requireCommentOnCreateSurvey;
                    case "_supportsSurveysDirectToFieldReps": return _supportsSurveysDirectToFieldReps;
                    case "_supportsBuildingValuation": return _supportsBuildingValuation;
                    case "_createSurveyTypeCode": return _createSurveyTypeCode;
                    case "_supportsDOTNumber": return _supportsDOTNumber;
                    case "_daysToAutoRefreshSurvey": return _daysToAutoRefreshSurvey;
                    case "_requireSICCode": return _requireSICCode;
                    case "_requireDocRemarks": return _requireDocRemarks;
                    case "_supportsDocRemarks": return _supportsDocRemarks;
                    case "_createServicePlansFromProspects": return _createServicePlansFromProspects;
                    case "_requireDocumentUpload": return _requireDocumentUpload;
                    case "_requireServiceVisitObjective": return _requireServiceVisitObjective;
                    case "_supportsBuildings": return _supportsBuildings;
                    case "_supportsNonDisclosure": return _supportsNonDisclosure;
                    case "_requirePolicyPremium": return _requirePolicyPremium;
                    case "_displayPolicyPremium": return _displayPolicyPremium;
                    case "_displayClaimsByDefault": return _displayClaimsByDefault;
                    case "_defaultInsuredContact": return _defaultInsuredContact;
                    case "_policyBranchCodeLabel": return _policyBranchCodeLabel;
                    case "_buildingSprinklerSystemLabel": return _buildingSprinklerSystemLabel;
                    case "_supportsExtractableDocuments": return _supportsExtractableDocuments;
                    case "_milestoneDate": return _milestoneDate;
                    case "_hideServicePlanVisitsByDefault": return _hideServicePlanVisitsByDefault;
                    case "_allowUnderwritersToEditRecs": return _allowUnderwritersToEditRecs;
                    case "_displayRecsOnSurveyScreen": return _displayRecsOnSurveyScreen;
                    case "_supportsQuickSurveyCreation": return _supportsQuickSurveyCreation;
                    case "_surveyDaysCount": return _surveyDaysCount;
                    case "_createSurveyTypeHolder": return _createSurveyTypeHolder;
                    case "_fileNetDocClassP8": return _fileNetDocClassP8;           // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
                    case "_fileNetUsernameP8": return _fileNetUsernameP8;           // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
                    case "_fileNetPasswordP8": return _fileNetPasswordP8;           // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
                    case "_fileNetVersion": return _fileNetVersion;                 // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_name": _name = (string)value; break;
                    case "_abbreviation": _abbreviation = (string)value; break;
                    case "_longAbbreviation": _longAbbreviation = (string)value; break;
                    case "_number": _number = (string)value; break;
                    case "_phone": _phone = (string)value; break;
                    case "_fax": _fax = (string)value; break;
                    case "_primaryDomainName": _primaryDomainName = (string)value; break;
                    case "_policyStarEndPointUrl": _policyStarEndPointUrl = (string)value; break;
                    case "_pdrEndPointUrl": _pdrEndPointUrl = (string)value; break;
                    case "_dsik": _dsik = (int)value; break;
                    case "_apsEndPointUrl": _apsEndPointUrl = (string)value; break;
                    case "_fileNetDocClass": _fileNetDocClass = (string)value; break;
                    case "_fileNetUsername": _fileNetUsername = (string)value; break;
                    case "_fileNetPassword": _fileNetPassword = (string)value; break;
                    case "_fileNetClientIDIndexName": _fileNetClientIDIndexName = (string)value; break;
                    case "_fileNetClientIDIndexNameP8": _fileNetClientIDIndexNameP8 = (string)value; break; //WA-697 Doug Bruce 2015-05-21 - Add field for P8 so ClientID search will work correctly for all companies.
                    case "_supportsFileNetIndexPolicySymbol": _supportsFileNetIndexPolicySymbol = (bool)value; break;
                    case "_supportsFileNetIndexAgencyNumber": _supportsFileNetIndexAgencyNumber = (bool)value; break;
                    case "_supportsFileNetIndexSourceSystem": _supportsFileNetIndexSourceSystem = (bool)value; break;
                    case "_surveyDaysUntilDue": _surveyDaysUntilDue = (int)value; break;
                    case "_surveyNearDue": _surveyNearDue = (int)value; break;
                    case "_reviewDaysUntilDue": _reviewDaysUntilDue = (int)value; break;
                    case "_reviewNearDue": _reviewNearDue = (int)value; break;
                    case "_emailFromAddress": _emailFromAddress = (string)value; break;
                    case "_feeCompanyUserRoleID": _feeCompanyUserRoleID = (Guid)value; break;
                    case "_underwriterUserRoleID": _underwriterUserRoleID = (Guid)value; break;
                    case "_nextFullReportDaysUntilDue": _nextFullReportDaysUntilDue = (int)value; break;
                    case "_agentDataIsRequied": _agentDataIsRequied = (bool)value; break;
                    case "_yearsOfLossHistory": _yearsOfLossHistory = (int)value; break;
                    case "_importRateModFactors": _importRateModFactors = (bool)value; break;
                    case "_defaultLocationsToOneSurvey": _defaultLocationsToOneSurvey = (bool)value; break;
                    case "_optionToReviewIsVisible": _optionToReviewIsVisible = (bool)value; break;
                    case "_importQuotes": _importQuotes = (bool)value; break;
                    case "_nonRequiredLOBHazardGrades": _nonRequiredLOBHazardGrades = (string)value; break;
                    case "_sicSortedByAlpha": _sicSortedByAlpha = (bool)value; break;
                    case "_supportsOverallGrading": _supportsOverallGrading = (bool)value; break;
                    case "_supportsCallCount": _supportsCallCount = (bool)value; break;
                    case "_callCountLabel": _callCountLabel = (string)value; break;
                    case "_supportsSeverityRating": _supportsSeverityRating = (bool)value; break;
                    case "_supportsSimplifiedAcctMgmt": _supportsSimplifiedAcctMgmt = (bool)value; break;
                    case "_requireVendorCost": _requireVendorCost = (bool)value; break;
                    case "_supportsVendorRecReminder": _supportsVendorRecReminder = (bool)value; break;
                    case "_supportsInvoiceFields": _supportsInvoiceFields = (bool)value; break;
                    case "_supportsBusinessCategory": _supportsBusinessCategory = (bool)value; break;
                    case "_supportsNAICS": _supportsNAICS = (bool)value; break;
                    case "_supportsPortfolioID": _supportsPortfolioID = (bool)value; break;
                    case "_requireDocTypeOnCreateSurvey": _requireDocTypeOnCreateSurvey = (bool)value; break;
                    case "_requireCommentOnCreateSurvey": _requireCommentOnCreateSurvey = (bool)value; break;
                    case "_supportsSurveysDirectToFieldReps": _supportsSurveysDirectToFieldReps = (bool)value; break;
                    case "_supportsBuildingValuation": _supportsBuildingValuation = (bool)value; break;
                    case "_createSurveyTypeCode": _createSurveyTypeCode = (string)value; break;
                    case "_supportsDOTNumber": _supportsDOTNumber = (bool)value; break;
                    case "_daysToAutoRefreshSurvey": _daysToAutoRefreshSurvey = (int)value; break;
                    case "_requireSICCode": _requireSICCode = (bool)value; break;
                    case "_requireDocRemarks": _requireDocRemarks = (bool)value; break;
                    case "_supportsDocRemarks": _supportsDocRemarks = (bool)value; break;
                    case "_createServicePlansFromProspects": _createServicePlansFromProspects = (bool)value; break;
                    case "_requireDocumentUpload": _requireDocumentUpload = (bool)value; break;
                    case "_requireServiceVisitObjective": _requireServiceVisitObjective = (bool)value; break;
                    case "_supportsBuildings": _supportsBuildings = (bool)value; break;
                    case "_supportsNonDisclosure": _supportsNonDisclosure = (bool)value; break;
                    case "_requirePolicyPremium": _requirePolicyPremium = (bool)value; break;
                    case "_displayPolicyPremium": _displayPolicyPremium = (bool)value; break;
                    case "_displayClaimsByDefault": _displayClaimsByDefault = (bool)value; break;
                    case "_defaultInsuredContact": _defaultInsuredContact = (string)value; break;
                    case "_policyBranchCodeLabel": _policyBranchCodeLabel = (string)value; break;
                    case "_buildingSprinklerSystemLabel": _buildingSprinklerSystemLabel = (string)value; break;
                    case "_supportsExtractableDocuments": _supportsExtractableDocuments = (bool)value; break;
                    case "_milestoneDate": _milestoneDate = (DateTime)value; break;
                    case "_hideServicePlanVisitsByDefault": _hideServicePlanVisitsByDefault = (bool)value; break;
                    case "_allowUnderwritersToEditRecs": _allowUnderwritersToEditRecs = (bool)value; break;
                    case "_displayRecsOnSurveyScreen": _displayRecsOnSurveyScreen = (bool)value; break;
                    case "_supportsQuickSurveyCreation": _supportsQuickSurveyCreation = (bool)value; break;
                    case "_surveyDaysCount": _surveyDaysCount = (int)value; break;
                    case "_createSurveyTypeHolder": _createSurveyTypeHolder = (ObjectHolder)value; break;
                    case "_fileNetDocClassP8": _fileNetDocClassP8 = (string)value; break;       // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
                    case "_fileNetUsernameP8": _fileNetUsernameP8 = (string)value; break;       // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
                    case "_fileNetPasswordP8": _fileNetPasswordP8 = (string)value; break;       // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
                    case "_fileNetVersion": _fileNetVersion = (string)value; break;             // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static Company Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(Company), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch Company entity with ID = '{0}'.", id));
            }
            return (Company)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Company GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Company), opathExpression);
            return (Company)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Company GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Company))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Company)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Company[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Company), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Company[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Company))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Company[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Company[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Company), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public Company GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            Company clone = (Company)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.Name, _name);
            Validator.Validate(Field.Abbreviation, _abbreviation);
            Validator.Validate(Field.LongAbbreviation, _longAbbreviation);
            Validator.Validate(Field.Number, _number);
            Validator.Validate(Field.Phone, _phone);
            Validator.Validate(Field.Fax, _fax);
            Validator.Validate(Field.PrimaryDomainName, _primaryDomainName);
            Validator.Validate(Field.PolicyStarEndPointUrl, _policyStarEndPointUrl);
            Validator.Validate(Field.PDREndPointUrl, _pdrEndPointUrl);
            Validator.Validate(Field.DSIK, _dsik);
            Validator.Validate(Field.APSEndPointUrl, _apsEndPointUrl);
            Validator.Validate(Field.FileNetDocClass, _fileNetDocClass);
            Validator.Validate(Field.FileNetUsername, _fileNetUsername);
            Validator.Validate(Field.FileNetPassword, _fileNetPassword);
            Validator.Validate(Field.FileNetClientIDIndexName, _fileNetClientIDIndexName);
            Validator.Validate(Field.FileNetClientIDIndexNameP8, _fileNetClientIDIndexNameP8); //WA-697 Doug Bruce 2015-05-21 - Add field for P8 so ClientID search will work correctly for all companies.
            Validator.Validate(Field.SupportsFileNetIndexPolicySymbol, _supportsFileNetIndexPolicySymbol);
            Validator.Validate(Field.SupportsFileNetIndexAgencyNumber, _supportsFileNetIndexAgencyNumber);
            Validator.Validate(Field.SupportsFileNetIndexSourceSystem, _supportsFileNetIndexSourceSystem);
            Validator.Validate(Field.SurveyDaysUntilDue, _surveyDaysUntilDue);
            Validator.Validate(Field.SurveyNearDue, _surveyNearDue);
            Validator.Validate(Field.ReviewDaysUntilDue, _reviewDaysUntilDue);
            Validator.Validate(Field.ReviewNearDue, _reviewNearDue);
            Validator.Validate(Field.EmailFromAddress, _emailFromAddress);
            Validator.Validate(Field.FeeCompanyUserRoleID, _feeCompanyUserRoleID);
            Validator.Validate(Field.UnderwriterUserRoleID, _underwriterUserRoleID);
            Validator.Validate(Field.NextFullReportDaysUntilDue, _nextFullReportDaysUntilDue);
            Validator.Validate(Field.AgentDataIsRequied, _agentDataIsRequied);
            Validator.Validate(Field.YearsOfLossHistory, _yearsOfLossHistory);
            Validator.Validate(Field.ImportRateModFactors, _importRateModFactors);
            Validator.Validate(Field.DefaultLocationsToOneSurvey, _defaultLocationsToOneSurvey);
            Validator.Validate(Field.OptionToReviewIsVisible, _optionToReviewIsVisible);
            Validator.Validate(Field.ImportQuotes, _importQuotes);
            Validator.Validate(Field.NonRequiredLOBHazardGrades, _nonRequiredLOBHazardGrades);
            Validator.Validate(Field.SICSortedByAlpha, _sicSortedByAlpha);
            Validator.Validate(Field.SupportsOverallGrading, _supportsOverallGrading);
            Validator.Validate(Field.SupportsCallCount, _supportsCallCount);
            Validator.Validate(Field.CallCountLabel, _callCountLabel);
            Validator.Validate(Field.SupportsSeverityRating, _supportsSeverityRating);
            Validator.Validate(Field.SupportsSimplifiedAcctMgmt, _supportsSimplifiedAcctMgmt);
            Validator.Validate(Field.RequireVendorCost, _requireVendorCost);
            Validator.Validate(Field.SupportsVendorRecReminder, _supportsVendorRecReminder);
            Validator.Validate(Field.SupportsInvoiceFields, _supportsInvoiceFields);
            Validator.Validate(Field.SupportsBusinessCategory, _supportsBusinessCategory);
            Validator.Validate(Field.SupportsNAICS, _supportsNAICS);
            Validator.Validate(Field.SupportsPortfolioID, _supportsPortfolioID);
            Validator.Validate(Field.RequireDocTypeOnCreateSurvey, _requireDocTypeOnCreateSurvey);
            Validator.Validate(Field.RequireCommentOnCreateSurvey, _requireCommentOnCreateSurvey);
            Validator.Validate(Field.SupportsSurveysDirectToFieldReps, _supportsSurveysDirectToFieldReps);
            Validator.Validate(Field.SupportsBuildingValuation, _supportsBuildingValuation);
            Validator.Validate(Field.CreateSurveyTypeCode, _createSurveyTypeCode);
            Validator.Validate(Field.SupportsDOTNumber, _supportsDOTNumber);
            Validator.Validate(Field.DaysToAutoRefreshSurvey, _daysToAutoRefreshSurvey);
            Validator.Validate(Field.RequireSICCode, _requireSICCode);
            Validator.Validate(Field.RequireDocRemarks, _requireDocRemarks);
            Validator.Validate(Field.SupportsDocRemarks, _supportsDocRemarks);
            Validator.Validate(Field.CreateServicePlansFromProspects, _createServicePlansFromProspects);
            Validator.Validate(Field.RequireDocumentUpload, _requireDocumentUpload);
            Validator.Validate(Field.RequireServiceVisitObjective, _requireServiceVisitObjective);
            Validator.Validate(Field.SupportsBuildings, _supportsBuildings);
            Validator.Validate(Field.SupportsNonDisclosure, _supportsNonDisclosure);
            Validator.Validate(Field.RequirePolicyPremium, _requirePolicyPremium);
            Validator.Validate(Field.DisplayPolicyPremium, _displayPolicyPremium);
            Validator.Validate(Field.DisplayClaimsByDefault, _displayClaimsByDefault);
            Validator.Validate(Field.DefaultInsuredContact, _defaultInsuredContact);
            Validator.Validate(Field.PolicyBranchCodeLabel, _policyBranchCodeLabel);
            Validator.Validate(Field.BuildingSprinklerSystemLabel, _buildingSprinklerSystemLabel);
            Validator.Validate(Field.SupportsExtractableDocuments, _supportsExtractableDocuments);
            Validator.Validate(Field.MilestoneDate, _milestoneDate);
            Validator.Validate(Field.HideServicePlanVisitsByDefault, _hideServicePlanVisitsByDefault);
            Validator.Validate(Field.AllowUnderwritersToEditRecs, _allowUnderwritersToEditRecs);
            Validator.Validate(Field.DisplayRecsOnSurveyScreen, _displayRecsOnSurveyScreen);
            Validator.Validate(Field.SupportsQuickSurveyCreation, _supportsQuickSurveyCreation);
            Validator.Validate(Field.SurveyDaysCount, _surveyDaysCount);
            Validator.Validate(Field.FileNetDocClassP8, _fileNetDocClassP8);    // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
            Validator.Validate(Field.FileNetUsernameP8, _fileNetUsernameP8);    // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
            Validator.Validate(Field.FileNetPasswordP8, _fileNetPasswordP8);    // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
            Validator.Validate(Field.FileNetVersion, _fileNetVersion);          // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _name = null;
            _abbreviation = null;
            _longAbbreviation = null;
            _number = null;
            _phone = null;
            _fax = null;
            _primaryDomainName = null;
            _policyStarEndPointUrl = null;
            _pdrEndPointUrl = null;
            _dsik = Int32.MinValue;
            _apsEndPointUrl = null;
            _fileNetDocClass = null;
            _fileNetUsername = null;
            _fileNetPassword = null;
            _fileNetClientIDIndexName = null;
            _fileNetClientIDIndexNameP8 = null; //WA-697 Doug Bruce 2015-05-21 - Add field for P8 so ClientID search will work correctly for all companies.
            _supportsFileNetIndexPolicySymbol = false;
            _supportsFileNetIndexAgencyNumber = false;
            _supportsFileNetIndexSourceSystem = false;
            _surveyDaysUntilDue = Int32.MinValue;
            _surveyNearDue = Int32.MinValue;
            _reviewDaysUntilDue = Int32.MinValue;
            _reviewNearDue = Int32.MinValue;
            _emailFromAddress = null;
            _feeCompanyUserRoleID = Guid.Empty;
            _underwriterUserRoleID = Guid.Empty;
            _nextFullReportDaysUntilDue = Int32.MinValue;
            _agentDataIsRequied = false;
            _yearsOfLossHistory = Int32.MinValue;
            _importRateModFactors = false;
            _defaultLocationsToOneSurvey = false;
            _optionToReviewIsVisible = false;
            _importQuotes = false;
            _nonRequiredLOBHazardGrades = null;
            _sicSortedByAlpha = false;
            _supportsOverallGrading = false;
            _supportsCallCount = false;
            _callCountLabel = null;
            _supportsSeverityRating = false;
            _supportsSimplifiedAcctMgmt = false;
            _requireVendorCost = false;
            _supportsVendorRecReminder = false;
            _supportsInvoiceFields = false;
            _supportsBusinessCategory = false;
            _supportsNAICS = false;
            _supportsPortfolioID = false;
            _requireDocTypeOnCreateSurvey = false;
            _requireCommentOnCreateSurvey = false;
            _supportsSurveysDirectToFieldReps = false;
            _supportsBuildingValuation = false;
            _createSurveyTypeCode = null;
            _supportsDOTNumber = false;
            _daysToAutoRefreshSurvey = Int32.MinValue;
            _requireSICCode = false;
            _requireDocRemarks = false;
            _supportsDocRemarks = false;
            _createServicePlansFromProspects = false;
            _requireDocumentUpload = false;
            _requireServiceVisitObjective = false;
            _supportsBuildings = false;
            _supportsNonDisclosure = false;
            _requirePolicyPremium = false;
            _displayPolicyPremium = false;
            _displayClaimsByDefault = false;
            _defaultInsuredContact = null;
            _policyBranchCodeLabel = null;
            _buildingSprinklerSystemLabel = null;
            _supportsExtractableDocuments = false;
            _milestoneDate = DateTime.MinValue;
            _hideServicePlanVisitsByDefault = false;
            _allowUnderwritersToEditRecs = false;
            _displayRecsOnSurveyScreen = false;
            _supportsQuickSurveyCreation = false;
            _surveyDaysCount = Int32.MinValue;
            _createSurveyTypeHolder = null;
            _fileNetDocClassP8 = null;      // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
            _fileNetUsernameP8 = null;      // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
            _fileNetPasswordP8 = null;      // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
            _fileNetVersion = null;         // WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the Company entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the Abbreviation field.
            /// </summary>
            Abbreviation,
            /// <summary>
            /// Represents the LongAbbreviation field.
            /// </summary>
            LongAbbreviation,
            /// <summary>
            /// Represents the Number field.
            /// </summary>
            Number,
            /// <summary>
            /// Represents the Phone field.
            /// </summary>
            Phone,
            /// <summary>
            /// Represents the Fax field.
            /// </summary>
            Fax,
            /// <summary>
            /// Represents the PrimaryDomainName field.
            /// </summary>
            PrimaryDomainName,
            /// <summary>
            /// Represents the PolicyStarEndPointUrl field.
            /// </summary>
            PolicyStarEndPointUrl,
            /// <summary>
            /// Represents the PDREndPointUrl field.
            /// </summary>
            PDREndPointUrl,
            /// <summary>
            /// Represents the DSIK field.
            /// </summary>
            DSIK,
            /// <summary>
            /// Represents the APSEndPointUrl field.
            /// </summary>
            APSEndPointUrl,
            /// <summary>
            /// Represents the FileNetDocClass field.
            /// </summary>
            FileNetDocClass,
            /// <summary>
            /// Represents the FileNetUsername field.
            /// </summary>
            FileNetUsername,
            /// <summary>
            /// Represents the FileNetPassword field.
            /// </summary>
            FileNetPassword,
            /// <summary>
            /// Represents the FileNetClientIDIndexName field.
            /// </summary>
            FileNetClientIDIndexName,
            /// <summary>
            /// Represents the FileNetClientIDIndexNameP8 field.
            /// </summary>
            FileNetClientIDIndexNameP8, //WA-697 Doug Bruce 2015-05-21 - Add field for P8 so ClientID search will work correctly for all companies.
            /// <summary>
            /// Represents the SupportsFileNetIndexPolicySymbol field.
            /// </summary>
            SupportsFileNetIndexPolicySymbol,
            /// <summary>
            /// Represents the SupportsFileNetIndexAgencyNumber field.
            /// </summary>
            SupportsFileNetIndexAgencyNumber,
            /// <summary>
            /// Represents the SupportsFileNetIndexSourceSystem field.
            /// </summary>
            SupportsFileNetIndexSourceSystem,
            /// <summary>
            /// Represents the SurveyDaysUntilDue field.
            /// </summary>
            SurveyDaysUntilDue,
            /// <summary>
            /// Represents the SurveyNearDue field.
            /// </summary>
            SurveyNearDue,
            /// <summary>
            /// Represents the ReviewDaysUntilDue field.
            /// </summary>
            ReviewDaysUntilDue,
            /// <summary>
            /// Represents the ReviewNearDue field.
            /// </summary>
            ReviewNearDue,
            /// <summary>
            /// Represents the EmailFromAddress field.
            /// </summary>
            EmailFromAddress,
            /// <summary>
            /// Represents the FeeCompanyUserRoleID field.
            /// </summary>
            FeeCompanyUserRoleID,
            /// <summary>
            /// Represents the UnderwriterUserRoleID field.
            /// </summary>
            UnderwriterUserRoleID,
            /// <summary>
            /// Represents the NextFullReportDaysUntilDue field.
            /// </summary>
            NextFullReportDaysUntilDue,
            /// <summary>
            /// Represents the AgentDataIsRequied field.
            /// </summary>
            AgentDataIsRequied,
            /// <summary>
            /// Represents the YearsOfLossHistory field.
            /// </summary>
            YearsOfLossHistory,
            /// <summary>
            /// Represents the ImportRateModFactors field.
            /// </summary>
            ImportRateModFactors,
            /// <summary>
            /// Represents the DefaultLocationsToOneSurvey field.
            /// </summary>
            DefaultLocationsToOneSurvey,
            /// <summary>
            /// Represents the OptionToReviewIsVisible field.
            /// </summary>
            OptionToReviewIsVisible,
            /// <summary>
            /// Represents the ImportQuotes field.
            /// </summary>
            ImportQuotes,
            /// <summary>
            /// Represents the NonRequiredLOBHazardGrades field.
            /// </summary>
            NonRequiredLOBHazardGrades,
            /// <summary>
            /// Represents the SICSortedByAlpha field.
            /// </summary>
            SICSortedByAlpha,
            /// <summary>
            /// Represents the SupportsOverallGrading field.
            /// </summary>
            SupportsOverallGrading,
            /// <summary>
            /// Represents the SupportsCallCount field.
            /// </summary>
            SupportsCallCount,
            /// <summary>
            /// Represents the CallCountLabel field.
            /// </summary>
            CallCountLabel,
            /// <summary>
            /// Represents the SupportsSeverityRating field.
            /// </summary>
            SupportsSeverityRating,
            /// <summary>
            /// Represents the SupportsSimplifiedAcctMgmt field.
            /// </summary>
            SupportsSimplifiedAcctMgmt,
            /// <summary>
            /// Represents the RequireVendorCost field.
            /// </summary>
            RequireVendorCost,
            /// <summary>
            /// Represents the SupportsVendorRecReminder field.
            /// </summary>
            SupportsVendorRecReminder,
            /// <summary>
            /// Represents the SupportsInvoiceFields field.
            /// </summary>
            SupportsInvoiceFields,
            /// <summary>
            /// Represents the SupportsBusinessCategory field.
            /// </summary>
            SupportsBusinessCategory,
            /// <summary>
            /// Represents the SupportsNAICS field.
            /// </summary>
            SupportsNAICS,
            /// <summary>
            /// Represents the SupportsPortfolioID field.
            /// </summary>
            SupportsPortfolioID,
            /// <summary>
            /// Represents the RequireDocTypeOnCreateSurvey field.
            /// </summary>
            RequireDocTypeOnCreateSurvey,
            /// <summary>
            /// Represents the RequireCommentOnCreateSurvey field.
            /// </summary>
            RequireCommentOnCreateSurvey,
            /// <summary>
            /// Represents the SupportsSurveysDirectToFieldReps field.
            /// </summary>
            SupportsSurveysDirectToFieldReps,
            /// <summary>
            /// Represents the SupportsBuildingValuation field.
            /// </summary>
            SupportsBuildingValuation,
            /// <summary>
            /// Represents the CreateSurveyTypeCode field.
            /// </summary>
            CreateSurveyTypeCode,
            /// <summary>
            /// Represents the SupportsDOTNumber field.
            /// </summary>
            SupportsDOTNumber,
            /// <summary>
            /// Represents the DaysToAutoRefreshSurvey field.
            /// </summary>
            DaysToAutoRefreshSurvey,
            /// <summary>
            /// Represents the RequireSICCode field.
            /// </summary>
            RequireSICCode,
            /// <summary>
            /// Represents the RequireDocRemarks field.
            /// </summary>
            RequireDocRemarks,
            /// <summary>
            /// Represents the SupportsDocRemarks field.
            /// </summary>
            SupportsDocRemarks,
            /// <summary>
            /// Represents the CreateServicePlansFromProspects field.
            /// </summary>
            CreateServicePlansFromProspects,
            /// <summary>
            /// Represents the RequireDocumentUpload field.
            /// </summary>
            RequireDocumentUpload,
            /// <summary>
            /// Represents the RequireServiceVisitObjective field.
            /// </summary>
            RequireServiceVisitObjective,
            /// <summary>
            /// Represents the SupportsBuildings field.
            /// </summary>
            SupportsBuildings,
            /// <summary>
            /// Represents the SupportsNonDisclosure field.
            /// </summary>
            SupportsNonDisclosure,
            /// <summary>
            /// Represents the RequirePolicyPremium field.
            /// </summary>
            RequirePolicyPremium,
            /// <summary>
            /// Represents the DisplayPolicyPremium field.
            /// </summary>
            DisplayPolicyPremium,
            /// <summary>
            /// Represents the DisplayClaimsByDefault field.
            /// </summary>
            DisplayClaimsByDefault,
            /// <summary>
            /// Represents the DefaultInsuredContact field.
            /// </summary>
            DefaultInsuredContact,
            /// <summary>
            /// Represents the PolicyBranchCodeLabel field.
            /// </summary>
            PolicyBranchCodeLabel,
            /// <summary>
            /// Represents the BuildingSprinklerSystemLabel field.
            /// </summary>
            BuildingSprinklerSystemLabel,
            /// <summary>
            /// Represents the SupportsExtractableDocuments field.
            /// </summary>
            SupportsExtractableDocuments,
            /// <summary>
            /// Represents the MilestoneDate field.
            /// </summary>
            MilestoneDate,
            /// <summary>
            /// Represents the HideServicePlanVisitsByDefault field.
            /// </summary>
            HideServicePlanVisitsByDefault,
            /// <summary>
            /// Represents the AllowUnderwritersToEditRecs field.
            /// </summary>
            AllowUnderwritersToEditRecs,
            /// <summary>
            /// Represents the DisplayRecsOnSurveyScreen field.
            /// </summary>
            DisplayRecsOnSurveyScreen,
            /// <summary>
            /// Represents the SupportsQuickSurveyCreation field.
            /// </summary>
            SupportsQuickSurveyCreation,
            /// <summary>
            /// Represents the SurveyDaysCount field.
            /// </summary>
            SurveyDaysCount,
            /// <summary>
            /// Represents the FileNetDocClass field for P8.
            /// WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
            /// </summary>
            FileNetDocClassP8,
            /// <summary>
            /// Represents the FileNetUsername field for P8.
            /// WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
            /// </summary>
            FileNetUsernameP8,
            /// <summary>
            /// Represents the FileNetPassword field for P8.
            /// WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
            /// </summary>
            FileNetPasswordP8,
            /// <summary>
            /// Represents the FileNetVersion field (IS or P8).
            /// WA-621, Doug Bruce, 2015-04-30 - P8 Upgrade for ARMS
            /// </summary>
            FileNetVersion,
        }

        #endregion
    }
}