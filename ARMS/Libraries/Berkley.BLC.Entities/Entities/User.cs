using System;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents an User entity, which maps to table 'User' in the database.
    /// </summary>
    public class User : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private User()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this User.</param>
        public User(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Gets the address as an HTML-encoded multi-line string value.
        /// </summary>
        public string HtmlAddress
        {
            get
            {
                return Formatter.Address(this.StreetLine1, this.StreetLine2, this.City, this.StateCode, this.ZipCode);
            }
        }

        /// <summary>
        /// Gets the effective PermissionSet for this user, which is based on 
        /// their assigned Role and any user-specific permissions.
        /// </summary>
        public PermissionSet Permissions
        {
            get
            {
                return PermissionSetCache.Fetch(this);
            }
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _companyID;
        private Guid _managerID = Guid.Empty;
        private string _domainName;
        private string _username;
        private string _name;
        private string _title = String.Empty;
        private string _certifications = String.Empty;
        private string _profitCenterCode = String.Empty;
        private string _emailAddress;
        private string _workPhone;
        private string _homePhone = String.Empty;
        private string _mobilePhone = String.Empty;
        private string _faxNumber = String.Empty;
        private string _streetLine1 = String.Empty;
        private string _streetLine2 = String.Empty;
        private string _city = String.Empty;
        private string _stateCode = String.Empty;
        private string _zipCode = String.Empty;
        private string _timeZoneCode;
        private bool _accountDisabled;
        private Guid _roleID;
        private Guid _permissionSetID = Guid.Empty;
        private bool _isFeeCompany;
        private bool _isUnderwriter;
        private string _remoteAgencyNumbers = String.Empty;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _managerUserHolder = null;
        private ObjectHolder _profitCenterHolder = null;
        private ObjectHolder _userRoleHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the User ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
            set
            {
                VerifyWritable();
                _companyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Manager. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid ManagerID
        {
            get { return _managerID; }
            set
            {
                VerifyWritable();
                _managerID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Domain Name.
        /// </summary>
        public string DomainName
        {
            get { return _domainName; }
            set
            {
                VerifyWritable();
                _domainName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Username.
        /// </summary>
        public string Username
        {
            get { return _username; }
            set
            {
                VerifyWritable();
                _username = value;
            }
        }

        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                VerifyWritable();
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Title. Null value is 'String.Empty'.
        /// </summary>
        public string Title
        {
            get { return _title; }
            set
            {
                VerifyWritable();
                _title = value;
            }
        }

        /// <summary>
        /// Gets or sets the Certifications. Null value is 'String.Empty'.
        /// </summary>
        public string Certifications
        {
            get { return _certifications; }
            set
            {
                VerifyWritable();
                _certifications = value;
            }
        }

        /// <summary>
        /// Gets or sets the Profit Center. Null value is 'String.Empty'.
        /// </summary>
        public string ProfitCenterCode
        {
            get { return _profitCenterCode; }
            set
            {
                VerifyWritable();
                _profitCenterCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Email Address.
        /// </summary>
        public string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                VerifyWritable();
                _emailAddress = value;
            }
        }

        /// <summary>
        /// Gets or sets the Work Phone.
        /// </summary>
        public string WorkPhone
        {
            get { return _workPhone; }
            set
            {
                VerifyWritable();
                _workPhone = value;
            }
        }

        /// <summary>
        /// Gets or sets the Home Phone. Null value is 'String.Empty'.
        /// </summary>
        public string HomePhone
        {
            get { return _homePhone; }
            set
            {
                VerifyWritable();
                _homePhone = value;
            }
        }

        /// <summary>
        /// Gets or sets the Mobile Phone. Null value is 'String.Empty'.
        /// </summary>
        public string MobilePhone
        {
            get { return _mobilePhone; }
            set
            {
                VerifyWritable();
                _mobilePhone = value;
            }
        }

        /// <summary>
        /// Gets or sets the Fax Number. Null value is 'String.Empty'.
        /// </summary>
        public string FaxNumber
        {
            get { return _faxNumber; }
            set
            {
                VerifyWritable();
                _faxNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Street Line 1. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine1
        {
            get { return _streetLine1; }
            set
            {
                VerifyWritable();
                _streetLine1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the Street Line 2. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine2
        {
            get { return _streetLine2; }
            set
            {
                VerifyWritable();
                _streetLine2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the City. Null value is 'String.Empty'.
        /// </summary>
        public string City
        {
            get { return _city; }
            set
            {
                VerifyWritable();
                _city = value;
            }
        }

        /// <summary>
        /// Gets or sets the State Code. Null value is 'String.Empty'.
        /// </summary>
        public string StateCode
        {
            get { return _stateCode; }
            set
            {
                VerifyWritable();
                _stateCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Zip Code. Null value is 'String.Empty'.
        /// </summary>
        public string ZipCode
        {
            get { return _zipCode; }
            set
            {
                VerifyWritable();
                _zipCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Time Zone Code.
        /// </summary>
        public string TimeZoneCode
        {
            get { return _timeZoneCode; }
            set
            {
                VerifyWritable();
                _timeZoneCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Account Disabled.
        /// </summary>
        public bool AccountDisabled
        {
            get { return _accountDisabled; }
            set
            {
                VerifyWritable();
                _accountDisabled = value;
            }
        }

        /// <summary>
        /// Gets or sets the User Role.
        /// </summary>
        public Guid RoleID
        {
            get { return _roleID; }
            set
            {
                VerifyWritable();
                _roleID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Permission Set ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid PermissionSetID
        {
            get { return _permissionSetID; }
            set
            {
                VerifyWritable();
                _permissionSetID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Is Fee Company.
        /// </summary>
        public bool IsFeeCompany
        {
            get { return _isFeeCompany; }
            set
            {
                VerifyWritable();
                _isFeeCompany = value;
            }
        }

        /// <summary>
        /// Gets or sets the Is Underwriter.
        /// </summary>
        public bool IsUnderwriter
        {
            get { return _isUnderwriter; }
            set
            {
                VerifyWritable();
                _isUnderwriter = value;
            }
        }

        /// <summary>
        /// Gets or sets the Remote Agency Numbers. Null value is 'String.Empty'.
        /// </summary>
        public string RemoteAgencyNumbers
        {
            get { return _remoteAgencyNumbers; }
            set
            {
                VerifyWritable();
                _remoteAgencyNumbers = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User Manager
        {
            get
            {
                _managerUserHolder.Key = _managerID;
                return (User)_managerUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a ProfitCenter object related to this entity.
        /// </summary>
        public ProfitCenter ProfitCenter
        {
            get
            {
                _profitCenterHolder.Key = _profitCenterCode;
                return (ProfitCenter)_profitCenterHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a UserRole object related to this entity.
        /// </summary>
        public UserRole Role
        {
            get
            {
                _userRoleHolder.Key = _roleID;
                return (UserRole)_userRoleHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_companyID": return _companyID;
                    case "_managerID": return _managerID;
                    case "_domainName": return _domainName;
                    case "_username": return _username;
                    case "_name": return _name;
                    case "_title": return _title;
                    case "_certifications": return _certifications;
                    case "_profitCenterCode": return _profitCenterCode;
                    case "_emailAddress": return _emailAddress;
                    case "_workPhone": return _workPhone;
                    case "_homePhone": return _homePhone;
                    case "_mobilePhone": return _mobilePhone;
                    case "_faxNumber": return _faxNumber;
                    case "_streetLine1": return _streetLine1;
                    case "_streetLine2": return _streetLine2;
                    case "_city": return _city;
                    case "_stateCode": return _stateCode;
                    case "_zipCode": return _zipCode;
                    case "_timeZoneCode": return _timeZoneCode;
                    case "_accountDisabled": return _accountDisabled;
                    case "_roleID": return _roleID;
                    case "_permissionSetID": return _permissionSetID;
                    case "_isFeeCompany": return _isFeeCompany;
                    case "_isUnderwriter": return _isUnderwriter;
                    case "_remoteAgencyNumbers": return _remoteAgencyNumbers;
                    case "_companyHolder": return _companyHolder;
                    case "_managerUserHolder": return _managerUserHolder;
                    case "_profitCenterHolder": return _profitCenterHolder;
                    case "_userRoleHolder": return _userRoleHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_managerID": _managerID = (Guid)value; break;
                    case "_domainName": _domainName = (string)value; break;
                    case "_username": _username = (string)value; break;
                    case "_name": _name = (string)value; break;
                    case "_title": _title = (string)value; break;
                    case "_certifications": _certifications = (string)value; break;
                    case "_profitCenterCode": _profitCenterCode = (string)value; break;
                    case "_emailAddress": _emailAddress = (string)value; break;
                    case "_workPhone": _workPhone = (string)value; break;
                    case "_homePhone": _homePhone = (string)value; break;
                    case "_mobilePhone": _mobilePhone = (string)value; break;
                    case "_faxNumber": _faxNumber = (string)value; break;
                    case "_streetLine1": _streetLine1 = (string)value; break;
                    case "_streetLine2": _streetLine2 = (string)value; break;
                    case "_city": _city = (string)value; break;
                    case "_stateCode": _stateCode = (string)value; break;
                    case "_zipCode": _zipCode = (string)value; break;
                    case "_timeZoneCode": _timeZoneCode = (string)value; break;
                    case "_accountDisabled": _accountDisabled = (bool)value; break;
                    case "_roleID": _roleID = (Guid)value; break;
                    case "_permissionSetID": _permissionSetID = (Guid)value; break;
                    case "_isFeeCompany": _isFeeCompany = (bool)value; break;
                    case "_isUnderwriter": _isUnderwriter = (bool)value; break;
                    case "_remoteAgencyNumbers": _remoteAgencyNumbers = (string)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_managerUserHolder": _managerUserHolder = (ObjectHolder)value; break;
                    case "_profitCenterHolder": _profitCenterHolder = (ObjectHolder)value; break;
                    case "_userRoleHolder": _userRoleHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static User Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(User), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch User entity with ID = '{0}'.", id));
            }
            return (User)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static User GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(User), opathExpression);
            return (User)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static User GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(User))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (User)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static User[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(User), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static User[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(User))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (User[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static User[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(User), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public User GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            User clone = (User)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.ManagerID, _managerID);
            Validator.Validate(Field.DomainName, _domainName);
            Validator.Validate(Field.Username, _username);
            Validator.Validate(Field.Name, _name);
            Validator.Validate(Field.Title, _title);
            Validator.Validate(Field.Certifications, _certifications);
            Validator.Validate(Field.ProfitCenterCode, _profitCenterCode);
            Validator.Validate(Field.EmailAddress, _emailAddress);
            Validator.Validate(Field.WorkPhone, _workPhone);
            Validator.Validate(Field.HomePhone, _homePhone);
            Validator.Validate(Field.MobilePhone, _mobilePhone);
            Validator.Validate(Field.FaxNumber, _faxNumber);
            Validator.Validate(Field.StreetLine1, _streetLine1);
            Validator.Validate(Field.StreetLine2, _streetLine2);
            Validator.Validate(Field.City, _city);
            Validator.Validate(Field.StateCode, _stateCode);
            Validator.Validate(Field.ZipCode, _zipCode);
            Validator.Validate(Field.TimeZoneCode, _timeZoneCode);
            Validator.Validate(Field.AccountDisabled, _accountDisabled);
            Validator.Validate(Field.RoleID, _roleID);
            Validator.Validate(Field.PermissionSetID, _permissionSetID);
            Validator.Validate(Field.IsFeeCompany, _isFeeCompany);
            Validator.Validate(Field.IsUnderwriter, _isUnderwriter);
            Validator.Validate(Field.RemoteAgencyNumbers, _remoteAgencyNumbers);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _companyID = Guid.Empty;
            _managerID = Guid.Empty;
            _domainName = null;
            _username = null;
            _name = null;
            _title = null;
            _certifications = null;
            _profitCenterCode = null;
            _emailAddress = null;
            _workPhone = null;
            _homePhone = null;
            _mobilePhone = null;
            _faxNumber = null;
            _streetLine1 = null;
            _streetLine2 = null;
            _city = null;
            _stateCode = null;
            _zipCode = null;
            _timeZoneCode = null;
            _accountDisabled = false;
            _roleID = Guid.Empty;
            _permissionSetID = Guid.Empty;
            _isFeeCompany = false;
            _isUnderwriter = false;
            _remoteAgencyNumbers = null;
            _companyHolder = null;
            _managerUserHolder = null;
            _profitCenterHolder = null;
            _userRoleHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the User entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the ManagerID field.
            /// </summary>
            ManagerID,
            /// <summary>
            /// Represents the DomainName field.
            /// </summary>
            DomainName,
            /// <summary>
            /// Represents the Username field.
            /// </summary>
            Username,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the Title field.
            /// </summary>
            Title,
            /// <summary>
            /// Represents the Certifications field.
            /// </summary>
            Certifications,
            /// <summary>
            /// Represents the ProfitCenterCode field.
            /// </summary>
            ProfitCenterCode,
            /// <summary>
            /// Represents the EmailAddress field.
            /// </summary>
            EmailAddress,
            /// <summary>
            /// Represents the WorkPhone field.
            /// </summary>
            WorkPhone,
            /// <summary>
            /// Represents the HomePhone field.
            /// </summary>
            HomePhone,
            /// <summary>
            /// Represents the MobilePhone field.
            /// </summary>
            MobilePhone,
            /// <summary>
            /// Represents the FaxNumber field.
            /// </summary>
            FaxNumber,
            /// <summary>
            /// Represents the StreetLine1 field.
            /// </summary>
            StreetLine1,
            /// <summary>
            /// Represents the StreetLine2 field.
            /// </summary>
            StreetLine2,
            /// <summary>
            /// Represents the City field.
            /// </summary>
            City,
            /// <summary>
            /// Represents the StateCode field.
            /// </summary>
            StateCode,
            /// <summary>
            /// Represents the ZipCode field.
            /// </summary>
            ZipCode,
            /// <summary>
            /// Represents the TimeZoneCode field.
            /// </summary>
            TimeZoneCode,
            /// <summary>
            /// Represents the AccountDisabled field.
            /// </summary>
            AccountDisabled,
            /// <summary>
            /// Represents the RoleID field.
            /// </summary>
            RoleID,
            /// <summary>
            /// Represents the PermissionSetID field.
            /// </summary>
            PermissionSetID,
            /// <summary>
            /// Represents the IsFeeCompany field.
            /// </summary>
            IsFeeCompany,
            /// <summary>
            /// Represents the IsUnderwriter field.
            /// </summary>
            IsUnderwriter,
            /// <summary>
            /// Represents the RemoteAgencyNumbers field.
            /// </summary>
            RemoteAgencyNumbers,
        }

        #endregion
    }
}