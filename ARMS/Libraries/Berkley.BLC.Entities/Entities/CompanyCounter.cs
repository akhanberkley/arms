using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a CompanyCounter entity, which maps to table 'CompanyCounter' in the database.
	/// </summary>
	public class CompanyCounter : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private CompanyCounter()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this CompanyCounter.</param>
		public CompanyCounter(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}

        /// <summary>
        /// Updates the counter by a value of 1.
        /// </summary>
        /// <param name="company">The company.</param>
        /// <param name="counterType">The counter type.</param>
        /// <param name="enforceMax">Flag that indicates to enforce the maximum value.</param>
        /// <returns>True of the maximum value has been met otherwise, false.</returns>
        public static bool IncrimentCounter(Company company, CompanyCounterType counterType, bool enforceMax)
        {
            bool result = false;
            CompanyCounter companyCounter = CompanyCounter.GetOne("CompanyID = ? && TypeCode = ?", company.ID, counterType.Code).GetWritableInstance();

            if (enforceMax)
            {
                companyCounter.CurrentValue = companyCounter.CurrentValue + 1;
                result = (companyCounter.CurrentValue == companyCounter.MaxValue);

                if (result)
                {
                    //Max has been met, reset the value
                    companyCounter.CurrentValue = 1;
                }
            }
            else
            {
                companyCounter.CurrentValue = companyCounter.CurrentValue + 1;
            }

            companyCounter.Save();

            return result;
        }

		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _id;
		private Guid _companyID;
		private string _typeCode;
		private int _currentValue;
		private int _maxValue = Int32.MinValue;
		private ObjectHolder _companyCounterTypeHolder = null;
		private ObjectHolder _companyHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Company Counter ID.
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets or sets the Company.
		/// </summary>
		public Guid CompanyID
		{
			get { return _companyID; }
			set
			{
				VerifyWritable();
				_companyID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Company Counter Type.
		/// </summary>
		public string TypeCode
		{
			get { return _typeCode; }
			set
			{
				VerifyWritable();
				_typeCode = value;
			}
		}

		/// <summary>
		/// Gets or sets the Current Value.
		/// </summary>
		public int CurrentValue
		{
			get { return _currentValue; }
			set
			{
				VerifyWritable();
				_currentValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the Max Value. Null value is 'Int32.MinValue'.
		/// </summary>
		public int MaxValue
		{
			get { return _maxValue; }
			set
			{
				VerifyWritable();
				_maxValue = value;
			}
		}

		/// <summary>
		/// Gets the instance of a CompanyCounterType object related to this entity.
		/// </summary>
		public CompanyCounterType Type
		{
			get
			{
				_companyCounterTypeHolder.Key = _typeCode;
				return (CompanyCounterType)_companyCounterTypeHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Company object related to this entity.
		/// </summary>
		public Company Company
		{
			get
			{
				_companyHolder.Key = _companyID;
				return (Company)_companyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_companyID": return _companyID;
					case "_typeCode": return _typeCode;
					case "_currentValue": return _currentValue;
					case "_maxValue": return _maxValue;
					case "_companyCounterTypeHolder": return _companyCounterTypeHolder;
					case "_companyHolder": return _companyHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (Guid)value; break;
					case "_companyID": _companyID = (Guid)value; break;
					case "_typeCode": _typeCode = (string)value; break;
					case "_currentValue": _currentValue = (int)value; break;
					case "_maxValue": _maxValue = (int)value; break;
					case "_companyCounterTypeHolder": _companyCounterTypeHolder = (ObjectHolder)value; break;
					case "_companyHolder": _companyHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static CompanyCounter Get(Guid id)
		{
			OPathQuery query = new OPathQuery(typeof(CompanyCounter), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch CompanyCounter entity with ID = '{0}'.", id));
			}
			return (CompanyCounter)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static CompanyCounter GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CompanyCounter), opathExpression);
			return (CompanyCounter)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static CompanyCounter GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(CompanyCounter) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (CompanyCounter)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CompanyCounter[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CompanyCounter), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CompanyCounter[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(CompanyCounter) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (CompanyCounter[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CompanyCounter[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CompanyCounter), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public CompanyCounter GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			CompanyCounter clone = (CompanyCounter)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ID, _id);
			Validator.Validate(Field.CompanyID, _companyID);
			Validator.Validate(Field.TypeCode, _typeCode);
			Validator.Validate(Field.CurrentValue, _currentValue);
			Validator.Validate(Field.MaxValue, _maxValue);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Guid.Empty;
			_companyID = Guid.Empty;
			_typeCode = null;
			_currentValue = Int32.MinValue;
			_maxValue = Int32.MinValue;
			_companyCounterTypeHolder = null;
			_companyHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the CompanyCounter entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the CompanyID field.
			/// </summary>
			CompanyID,
			/// <summary>
			/// Represents the TypeCode field.
			/// </summary>
			TypeCode,
			/// <summary>
			/// Represents the CurrentValue field.
			/// </summary>
			CurrentValue,
			/// <summary>
			/// Represents the MaxValue field.
			/// </summary>
			MaxValue,
		}
		
		#endregion
	}
}