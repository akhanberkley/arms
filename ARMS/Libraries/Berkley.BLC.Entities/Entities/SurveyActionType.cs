using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyActionType entity, which maps to table 'SurveyActionType' in the database.
	/// </summary>
	public class SurveyActionType : IObjectHelper
	{
        #region Public Fields

        /// <summary>
        /// Represents a user editing the due date for a survey.
        /// </summary>
        public static readonly SurveyActionType EditDueDate = SurveyActionType.Get("DUEDATE");
        /// <summary>
        /// Represents a user editing the report submitted date for a survey.
        /// </summary>
        public static readonly SurveyActionType EditReportSubmittedDate = SurveyActionType.Get("RPTSUBDATE");
        /// <summary>
        /// Represents a user editing the complete date for a survey.
        /// </summary>
        public static readonly SurveyActionType EditCompleteDate = SurveyActionType.Get("COMPDATE");
        /// <summary>
        /// Represents assigning a survey to a user.
        /// </summary>
        public static readonly SurveyActionType AssignSurvey = SurveyActionType.Get("ASSIGN");
        /// <summary>
        /// Represents a user taking ownership of a survey.
        /// </summary>
        public static readonly SurveyActionType TakeOwnership = SurveyActionType.Get("TAKE");
        /// <summary>
        /// Represents a user releasing ownership of a survey.
        /// </summary>
        public static readonly SurveyActionType ReleaseOwnership = SurveyActionType.Get("RELEASE");
        /// <summary>
        /// Represents an user canceling a survey.
        /// </summary>
        public static readonly SurveyActionType CancelSurvey = SurveyActionType.Get("CANCEL");
        /// <summary>
        /// Represents an user closing and notifying a survey.
        /// </summary>
        public static readonly SurveyActionType CloseAndNotify = SurveyActionType.Get("CLSNOTIFY");
        /// <summary>
        /// Represents an user closing a survey.
        /// </summary>
        public static readonly SurveyActionType Close = SurveyActionType.Get("CLOSE");
        /// <summary>
        /// Represents an user reopening or completed survey.
        /// </summary>
        public static readonly SurveyActionType ReopenSurvey = SurveyActionType.Get("REOPEN");
        /// <summary>
        /// Represents an user adding a document to a survey.
        /// </summary>
        public static readonly SurveyActionType AddDocument = SurveyActionType.Get("DOCUMENT");
        /// <summary>
        /// Represents an user adding a comment to a survey.
        /// </summary>
        public static readonly SurveyActionType AddNote = SurveyActionType.Get("NOTE");
        /// <summary>
        /// Represents an user generating a letter for a survey.
        /// </summary>
        public static readonly SurveyActionType GenerateLetter = SurveyActionType.Get("LETTER");
        /// <summary>
        /// Represents an user downloading a report for a survey.
        /// </summary>
        public static readonly SurveyActionType DownloadReport = SurveyActionType.Get("REPORT");
        /// <summary>
        /// Represents an user setting the non-productive flag.
        /// </summary>
        public static readonly SurveyActionType NonProductive = SurveyActionType.Get("PRODUCTIVE");
        /// <summary>
        /// Represents an user setting the review flag.
        /// </summary>
        public static readonly SurveyActionType Review = SurveyActionType.Get("REVIEW");
        /// <summary>
        /// Represents an user assigning a survey based on the territory.
        /// </summary>
        public static readonly SurveyActionType AssignSurveyTerritory = SurveyActionType.Get("ASSIGNTERR");
        /// <summary>
        /// Represents an user sending a survey for review.
        /// </summary>
        public static readonly SurveyActionType SendForReview = SurveyActionType.Get("SENDFOL");
        /// <summary>
        /// Represents an user accepting a survey.
        /// </summary>
        public static readonly SurveyActionType AcceptSurvey = SurveyActionType.Get("ACCEPT");
        /// <summary>
        /// Represents an user setting the survey details.
        /// </summary>
        public static readonly SurveyActionType SetSurveyDetails = SurveyActionType.Get("SETDETAILS");
        /// <summary>
        /// Represents an user adding/editing recommendations.
        /// </summary>
        public static readonly SurveyActionType ManageRecs = SurveyActionType.Get("RECS");
        /// <summary>
        /// Represents an user adding/editing objectives.
        /// </summary>
        public static readonly SurveyActionType ManageObjectives = SurveyActionType.Get("OBJECTIVE");
        /// <summary>
        /// Represents a user returning the survey back to the consultant for correction.
        /// </summary>
        public static readonly SurveyActionType Reject = SurveyActionType.Get("REJECT");
        /// <summary>
        /// Represents an external user returning the survey back to the company uncompleted.
        /// </summary>
        public static readonly SurveyActionType ReturnToCompany = SurveyActionType.Get("RETURNCOMP");
        /// <summary>
        /// Represents an user extending the letter dates.
        /// </summary>
        public static readonly SurveyActionType ExtendLetterDates = SurveyActionType.Get("EXTENDLTR");
        /// <summary>
        /// Represents an user creating a service plan from the survey.
        /// </summary>
        public static readonly SurveyActionType CreateServicePlan = SurveyActionType.Get("CREATESERV");
        /// <summary>
        /// Represents an user setting the mail received date.
        /// </summary>
        public static readonly SurveyActionType SetMailReceived = SurveyActionType.Get("MAILREC");
        /// <summary>
        /// Represents an user setting the mail sent date.
        /// </summary>
        public static readonly SurveyActionType SetMailSent = SurveyActionType.Get("MAILSENT");
        /// <summary>
        /// Represents an user updating the insured details of a survey.
        /// </summary>
        public static readonly SurveyActionType EditInsured = SurveyActionType.Get("UPDATEINS");
        /// <summary>
        /// Represents an user updating the insured policy details of a survey.
        /// </summary>
        public static readonly SurveyActionType EditPolicy = SurveyActionType.Get("UPDATEPOL");
        /// <summary>
        /// Represents an user removing a document from a survey.
        /// </summary>
        public static readonly SurveyActionType RemoveDocument = SurveyActionType.Get("REMOVEDOC");
        /// <summary>
        /// Represents an user removing a history entry from a survey.
        /// </summary>
        public static readonly SurveyActionType RemoveHistory = SurveyActionType.Get("REMOVEHIST");
        /// <summary>
        /// Represents an user removing a rec from a survey.
        /// </summary>
        public static readonly SurveyActionType RemoveRec = SurveyActionType.Get("REMOVEREC");
        /// <summary>
        /// Represents an user changing the survey type.
        /// </summary>
        public static readonly SurveyActionType ChangeSurveyType = SurveyActionType.Get("CHANGETYPE");
        /// <summary>
        /// Represents an user adding/editing the activity times for a survey.
        /// </summary>
        public static readonly SurveyActionType AccountManagement = SurveyActionType.Get("HOURS");
        /// <summary>
        /// Represents an user managing the locations for a survey.
        /// </summary>
        public static readonly SurveyActionType ManageLocations = SurveyActionType.Get("LOCATIONS");
        /// <summary>
        /// Represents an user removing a note from a survey.
        /// </summary>
        public static readonly SurveyActionType RemoveNote = SurveyActionType.Get("REMOVENOTE");
        /// <summary>
        /// Represents an user peforming a Quality Assessment of a survey.
        /// </summary>
        public static readonly SurveyActionType QualityAssessment = SurveyActionType.Get("QUALITY");
        /// <summary>
        /// Represents an user editing the assigned date of a survey.
        /// </summary>
        public static readonly SurveyActionType EditAssignedDate = SurveyActionType.Get("EDITASSIGN");
        /// <summary>
        /// Represents an user editing the accepted date of a survey.
        /// </summary>
        public static readonly SurveyActionType EditAcceptedDate = SurveyActionType.Get("EDITACCEPT");
        /// <summary>
        /// Represents an user creating a new survey from an existing survey.
        /// </summary>
        public static readonly SurveyActionType CreateNewSurvey = SurveyActionType.Get("CREATESURV");
        /// <summary>
        /// Represents an user notifying the UW (routes the instance in BPM to the UW).
        /// </summary>
        public static readonly SurveyActionType NotifyUnderwriting = SurveyActionType.Get("NOTIFYUW");
        /// <summary>
        /// Represents an user deleting a survey request.
        /// </summary>
        public static readonly SurveyActionType DeleteSurvey = SurveyActionType.Get("REMOVESURV");
        /// <summary>
        /// Represents an user deleting a survey request.
        /// </summary>
        public static readonly SurveyActionType ChangePrimaryPolicy = SurveyActionType.Get("CHANGEPOL");
        /// <summary>
        /// Represents an user emailing an underwriter.
        /// </summary>
        public static readonly SurveyActionType EmailUW = SurveyActionType.Get("EMAILUW");
        /// Represents an user emailing an underwriter.
        /// </summary>
        public static readonly SurveyActionType EmailUser = SurveyActionType.Get("EMAILUW"); 
        /// <summary>
        /// Represents an user performing a QCR.
        /// </summary>
        public static readonly SurveyActionType PerformQCR = SurveyActionType.Get("QCR");
        /// <summary>
        /// Represents an user reviewing a QCR.
        /// </summary>
        public static readonly SurveyActionType ReviewQCR = SurveyActionType.Get("REVIEWQCR");
        /// <summary>
        /// Represents a UW acknowledging the survey.
        /// </summary>
        public static readonly SurveyActionType AcknowledgeByUW = SurveyActionType.Get("AKNOWLEDGE");
        /// <summary>
        /// Represents a UW Questionnaire.
        /// </summary>
        public static readonly SurveyActionType UWQuestionnaire = SurveyActionType.Get("UWQUEST");
        /// <summary>
        /// Represents a manage ratings.
        /// </summary>
        public static readonly SurveyActionType ManageRatings = SurveyActionType.Get("MGRRATINGS");
        /// <summary>
        /// Represents a manage ratings.
        /// </summary>
        public static readonly SurveyActionType ChangeSurveyPriority = SurveyActionType.Get("CHANGEPRI");
        /// <summary>
        /// Represents a Due Date Approval.
        /// </summary>
        public static readonly SurveyActionType ApproveDueDate = SurveyActionType.Get("ADDUEDATE");

        #endregion


        /// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyActionType()
		{
		}


		#region --- Generated Members ---

		private string _code;
		private string _actionName;
       

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Survey Action Type.
		/// </summary>
		public string Code
		{
			get { return _code; }
		}

		/// <summary>
		/// Gets the Action Name.
		/// </summary>
		public string ActionName
		{
			get { return _actionName; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_code": return _code;
					case "_actionName": return _actionName;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_code": _code = (string)value; break;
					case "_actionName": _actionName = (string)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="code">Code of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static SurveyActionType Get(string code)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyActionType), "Code = ?");
			object entity = DB.Engine.GetObject(query, code);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch SurveyActionType entity with Code = '{0}'.", code));
			}
			return (SurveyActionType)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyActionType GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyActionType), opathExpression);
			return (SurveyActionType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyActionType GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyActionType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyActionType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyActionType[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyActionType), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyActionType[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyActionType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyActionType[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyActionType[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyActionType), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Determines if two SurveyActionType objects have the same semantic value.
		/// </summary>
		/// <param name="one">The first object to compare.</param>
		/// <param name="two">The second object to compare.</param>
		/// <returns>True if they are semantically identical, false otherwise.</returns>
		public static bool operator == (SurveyActionType one, SurveyActionType two) 
		{
			if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
			{
				return true;
			}
			else if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
			{
				return false;
			}
			else
			{
				return (one.CompareTo(two) == 0);
			}
		}

		/// <summary>
		/// Determines if two SurveyActionType objects have different semantic values.
		/// </summary>
		/// <param name="one">The first object to compare.</param>
		/// <param name="two">The second object to compare.</param>
		/// <returns>True if they are semantically dissimilar, false otherwise.</returns>
		public static bool operator != (SurveyActionType one, SurveyActionType two) 
		{
			if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
			{
				return false;
			}
			if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
			{
				return true;
			}
			else
			{
				return (one.CompareTo(two) != 0);
			}
		}
		
		/// <summary>
		/// Compares the current instance with another object of the same type.
		/// </summary>
		/// <param name="value">An object to compare with this instance.</param>
		/// <returns>Less than zero if this instance is less than value. 
		/// Zero if this instance is equal to value.
		/// Greater than zero if this instance is greater than value.</returns>
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (value is SurveyActionType)
			{
				SurveyActionType obj = (SurveyActionType)value;
				return (this.Code.CompareTo(obj.Code));
			}
			throw new ArgumentException("Value is not the correct type.");
		}

		/// <summary>
		/// Returns the hash code for this instance.
		/// </summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		public override int GetHashCode()
		{
			return this.Code.GetHashCode();
		}

		/// <summary>
		/// Determines whether this instance is equal to the specified object.
		/// </summary>
		/// <param name="value">The object to compare with the current instance. </param>
		/// <returns>true if the specified object is equal to the current instance; otherwise, false.</returns>
		public override bool Equals(object value)
		{
			return (this == (value as SurveyActionType));
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_code = null;
			_actionName = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the SurveyActionType entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the Code field.
			/// </summary>
			Code,
			/// <summary>
			/// Represents the ActionName field.
			/// </summary>
			ActionName,
		}
		
		#endregion

        
    }
}