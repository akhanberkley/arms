using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a CoverageDetail entity, which maps to table 'CoverageDetail' in the database.
	/// </summary>
	public class CoverageDetail : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private CoverageDetail()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this CoverageDetail.</param>
		public CoverageDetail(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _id;
		private Guid _coverageID;
		private Guid _insuredID = Guid.Empty;
		private Guid _policyID = Guid.Empty;
		private string _value = String.Empty;
        private DateTime _dateCreated;
		private ObjectHolder _coverageHolder = null;
		private ObjectHolder _insuredHolder = null;
		private ObjectHolder _policyHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Coverage Detail ID.
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets or sets the Coverage.
		/// </summary>
		public Guid CoverageID
		{
			get { return _coverageID; }
			set
			{
				VerifyWritable();
				_coverageID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Insured. Null value is 'Guid.Empty'.
		/// </summary>
		public Guid InsuredID
		{
			get { return _insuredID; }
			set
			{
				VerifyWritable();
				_insuredID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Policy. Null value is 'Guid.Empty'.
		/// </summary>
		public Guid PolicyID
		{
			get { return _policyID; }
			set
			{
				VerifyWritable();
				_policyID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Coverage Detail Value. Null value is 'String.Empty'.
		/// </summary>
		public string Value
		{
			get { return _value; }
			set
			{
				VerifyWritable();
				_value = value;
			}
		}

		/// <summary>
		/// Gets or sets the Date Created.
		/// </summary>
        public DateTime DateCreated
		{
			get { return _dateCreated; }
			set
			{
				VerifyWritable();
				_dateCreated = value;
			}
		}

		/// <summary>
		/// Gets the instance of a Coverage object related to this entity.
		/// </summary>
		public Coverage Coverage
		{
			get
			{
				_coverageHolder.Key = _coverageID;
				return (Coverage)_coverageHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Insured object related to this entity.
		/// </summary>
		public Insured Insured
		{
			get
			{
				_insuredHolder.Key = _insuredID;
				return (Insured)_insuredHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Policy object related to this entity.
		/// </summary>
		public Policy Policy
		{
			get
			{
				_policyHolder.Key = _policyID;
				return (Policy)_policyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_coverageID": return _coverageID;
					case "_insuredID": return _insuredID;
					case "_policyID": return _policyID;
					case "_value": return _value;
					case "_dateCreated": return _dateCreated;
					case "_coverageHolder": return _coverageHolder;
					case "_insuredHolder": return _insuredHolder;
					case "_policyHolder": return _policyHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (Guid)value; break;
					case "_coverageID": _coverageID = (Guid)value; break;
					case "_insuredID": _insuredID = (Guid)value; break;
					case "_policyID": _policyID = (Guid)value; break;
					case "_value": _value = (string)value; break;
                    case "_dateCreated": _dateCreated = (DateTime)value; break;
					case "_coverageHolder": _coverageHolder = (ObjectHolder)value; break;
					case "_insuredHolder": _insuredHolder = (ObjectHolder)value; break;
					case "_policyHolder": _policyHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static CoverageDetail Get(Guid id)
		{
			OPathQuery query = new OPathQuery(typeof(CoverageDetail), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch CoverageDetail entity with ID = '{0}'.", id));
			}
			return (CoverageDetail)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static CoverageDetail GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CoverageDetail), opathExpression);
			return (CoverageDetail)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static CoverageDetail GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(CoverageDetail) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (CoverageDetail)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CoverageDetail[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CoverageDetail), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CoverageDetail[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(CoverageDetail) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (CoverageDetail[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CoverageDetail[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CoverageDetail), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public CoverageDetail GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			CoverageDetail clone = (CoverageDetail)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ID, _id);
			Validator.Validate(Field.CoverageID, _coverageID);
			Validator.Validate(Field.InsuredID, _insuredID);
			Validator.Validate(Field.PolicyID, _policyID);
			Validator.Validate(Field.Value, _value);
			Validator.Validate(Field.DateCreated, _dateCreated);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Guid.Empty;
			_coverageID = Guid.Empty;
			_insuredID = Guid.Empty;
			_policyID = Guid.Empty;
			_value = null;
			_dateCreated = DateTime.MinValue;
			_coverageHolder = null;
			_insuredHolder = null;
			_policyHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the CoverageDetail entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the CoverageID field.
			/// </summary>
			CoverageID,
			/// <summary>
			/// Represents the InsuredID field.
			/// </summary>
			InsuredID,
			/// <summary>
			/// Represents the PolicyID field.
			/// </summary>
			PolicyID,
			/// <summary>
			/// Represents the Value field.
			/// </summary>
			Value,
			/// <summary>
			/// Represents the DateCreated field.
			/// </summary>
			DateCreated,
		}
		
		#endregion
	}
}