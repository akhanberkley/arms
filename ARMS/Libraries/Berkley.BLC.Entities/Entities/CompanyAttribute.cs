using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a CompanyAttribute entity, which maps to table 'CompanyAttribute' in the database.
    /// </summary>
    public class CompanyAttribute : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private CompanyAttribute()
        {
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _companyID;
        private string _attributeDataTypeCode;
        private string _entityTypeCode;
        private string _label;
        private bool _isRequired;
        private int _displayOrder;
        private string _controlAttributes = String.Empty;
        private int _controlColumnCount = Int32.MinValue;
        private int _controlRowCount = Int32.MinValue;
        private bool _displayInGrid;
        private int _minWidthInGrid = Int32.MinValue;
        private string _mappedField = String.Empty;
        private bool _requireOnCompletion;
        private DateTime _expirationDate = DateTime.MinValue;
        private ObjectHolder _attributeDataTypeHolder = null;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _entityTypeHolder = null;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Company Attribute.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets the Attribute Data Type.
        /// </summary>
        public string AttributeDataTypeCode
        {
            get { return _attributeDataTypeCode; }
        }

        /// <summary>
        /// Gets the Entity Type.
        /// </summary>
        public string EntityTypeCode
        {
            get { return _entityTypeCode; }
        }

        /// <summary>
        /// Gets the Label.
        /// </summary>
        public string Label
        {
            get { return _label; }
        }

        /// <summary>
        /// Gets the Is Required.
        /// </summary>
        public bool IsRequired
        {
            get { return _isRequired; }
        }

        /// <summary>
        /// Gets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
        }

        /// <summary>
        /// Gets the Control Attributes. Null value is 'String.Empty'.
        /// </summary>
        public string ControlAttributes
        {
            get { return _controlAttributes; }
        }

        /// <summary>
        /// Gets the Control Column Count. Null value is 'Int32.MinValue'.
        /// </summary>
        public int ControlColumnCount
        {
            get { return _controlColumnCount; }
        }

        /// <summary>
        /// Gets the Control Row Count. Null value is 'Int32.MinValue'.
        /// </summary>
        public int ControlRowCount
        {
            get { return _controlRowCount; }
        }

        /// <summary>
        /// Gets the Display In Grid.
        /// </summary>
        public bool DisplayInGrid
        {
            get { return _displayInGrid; }
        }

        /// <summary>
        /// Gets the Min Width In Grid. Null value is 'Int32.MinValue'.
        /// </summary>
        public int MinWidthInGrid
        {
            get { return _minWidthInGrid; }
        }

        /// <summary>
        /// Gets the Mapped Field. Null value is 'String.Empty'.
        /// </summary>
        public string MappedField
        {
            get { return _mappedField; }
        }

        /// <summary>
        /// Gets the Require On Completion.
        /// </summary>
        public bool RequireOnCompletion
        {
            get { return _requireOnCompletion; }
        }

        /// <summary>
        /// Gets the Expiration Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime ExpirationDate
        {
            get { return _expirationDate; }
        }

        /// <summary>
        /// Gets the instance of a AttributeDataType object related to this entity.
        /// </summary>
        public AttributeDataType AttributeDataType
        {
            get
            {
                _attributeDataTypeHolder.Key = _attributeDataTypeCode;
                return (AttributeDataType)_attributeDataTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a EntityType object related to this entity.
        /// </summary>
        public EntityType EntityType
        {
            get
            {
                _entityTypeHolder.Key = _entityTypeCode;
                return (EntityType)_entityTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_companyID": return _companyID;
                    case "_attributeDataTypeCode": return _attributeDataTypeCode;
                    case "_entityTypeCode": return _entityTypeCode;
                    case "_label": return _label;
                    case "_isRequired": return _isRequired;
                    case "_displayOrder": return _displayOrder;
                    case "_controlAttributes": return _controlAttributes;
                    case "_controlColumnCount": return _controlColumnCount;
                    case "_controlRowCount": return _controlRowCount;
                    case "_displayInGrid": return _displayInGrid;
                    case "_minWidthInGrid": return _minWidthInGrid;
                    case "_mappedField": return _mappedField;
                    case "_requireOnCompletion": return _requireOnCompletion;
                    case "_expirationDate": return _expirationDate;
                    case "_attributeDataTypeHolder": return _attributeDataTypeHolder;
                    case "_companyHolder": return _companyHolder;
                    case "_entityTypeHolder": return _entityTypeHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_attributeDataTypeCode": _attributeDataTypeCode = (string)value; break;
                    case "_entityTypeCode": _entityTypeCode = (string)value; break;
                    case "_label": _label = (string)value; break;
                    case "_isRequired": _isRequired = (bool)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    case "_controlAttributes": _controlAttributes = (string)value; break;
                    case "_controlColumnCount": _controlColumnCount = (int)value; break;
                    case "_controlRowCount": _controlRowCount = (int)value; break;
                    case "_displayInGrid": _displayInGrid = (bool)value; break;
                    case "_minWidthInGrid": _minWidthInGrid = (int)value; break;
                    case "_mappedField": _mappedField = (string)value; break;
                    case "_requireOnCompletion": _requireOnCompletion = (bool)value; break;
                    case "_expirationDate": _expirationDate = (DateTime)value; break;
                    case "_attributeDataTypeHolder": _attributeDataTypeHolder = (ObjectHolder)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_entityTypeHolder": _entityTypeHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static CompanyAttribute Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(CompanyAttribute), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch CompanyAttribute entity with ID = '{0}'.", id));
            }
            return (CompanyAttribute)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static CompanyAttribute GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(CompanyAttribute), opathExpression);
            return (CompanyAttribute)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static CompanyAttribute GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(CompanyAttribute))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (CompanyAttribute)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static CompanyAttribute[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(CompanyAttribute), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static CompanyAttribute[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(CompanyAttribute))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (CompanyAttribute[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static CompanyAttribute[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(CompanyAttribute), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _companyID = Guid.Empty;
            _attributeDataTypeCode = null;
            _entityTypeCode = null;
            _label = null;
            _isRequired = false;
            _displayOrder = Int32.MinValue;
            _controlAttributes = null;
            _controlColumnCount = Int32.MinValue;
            _controlRowCount = Int32.MinValue;
            _displayInGrid = false;
            _minWidthInGrid = Int32.MinValue;
            _mappedField = null;
            _requireOnCompletion = false;
            _expirationDate = DateTime.MinValue;
            _attributeDataTypeHolder = null;
            _companyHolder = null;
            _entityTypeHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the CompanyAttribute entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the AttributeDataTypeCode field.
            /// </summary>
            AttributeDataTypeCode,
            /// <summary>
            /// Represents the EntityTypeCode field.
            /// </summary>
            EntityTypeCode,
            /// <summary>
            /// Represents the Label field.
            /// </summary>
            Label,
            /// <summary>
            /// Represents the IsRequired field.
            /// </summary>
            IsRequired,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
            /// <summary>
            /// Represents the ControlAttributes field.
            /// </summary>
            ControlAttributes,
            /// <summary>
            /// Represents the ControlColumnCount field.
            /// </summary>
            ControlColumnCount,
            /// <summary>
            /// Represents the ControlRowCount field.
            /// </summary>
            ControlRowCount,
            /// <summary>
            /// Represents the DisplayInGrid field.
            /// </summary>
            DisplayInGrid,
            /// <summary>
            /// Represents the MinWidthInGrid field.
            /// </summary>
            MinWidthInGrid,
            /// <summary>
            /// Represents the MappedField field.
            /// </summary>
            MappedField,
            /// <summary>
            /// Represents the RequireOnCompletion field.
            /// </summary>
            RequireOnCompletion,
            /// <summary>
            /// Represents the ExpirationDate field.
            /// </summary>
            ExpirationDate,
        }

        #endregion
    }
}