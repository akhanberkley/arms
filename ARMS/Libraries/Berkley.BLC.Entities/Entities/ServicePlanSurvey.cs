using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a ServicePlanSurvey entity, which maps to table 'ServicePlan_Survey' in the database.
	/// </summary>
	public class ServicePlanSurvey : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private ServicePlanSurvey()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="servicePlanID">ServicePlanID of this ServicePlanSurvey.</param>
		/// <param name="surveyID">SurveyID of this ServicePlanSurvey.</param>
		public ServicePlanSurvey(Guid servicePlanID, Guid surveyID)
		{
			_servicePlanID = servicePlanID;
			_surveyID = surveyID;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _servicePlanID;
		private Guid _surveyID;
		private ObjectHolder _servicePlanHolder = null;
		private ObjectHolder _surveyHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Service Plan.
		/// </summary>
		public Guid ServicePlanID
		{
			get { return _servicePlanID; }
		}

		/// <summary>
		/// Gets the Survey.
		/// </summary>
		public Guid SurveyID
		{
			get { return _surveyID; }
		}

		/// <summary>
		/// Gets the instance of a ServicePlan object related to this entity.
		/// </summary>
		public ServicePlan ServicePlan
		{
			get
			{
				_servicePlanHolder.Key = _servicePlanID;
				return (ServicePlan)_servicePlanHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Survey object related to this entity.
		/// </summary>
		public Survey Survey
		{
			get
			{
				_surveyHolder.Key = _surveyID;
				return (Survey)_surveyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_servicePlanID": return _servicePlanID;
					case "_surveyID": return _surveyID;
					case "_servicePlanHolder": return _servicePlanHolder;
					case "_surveyHolder": return _surveyHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_servicePlanID": _servicePlanID = (Guid)value; break;
					case "_surveyID": _surveyID = (Guid)value; break;
					case "_servicePlanHolder": _servicePlanHolder = (ObjectHolder)value; break;
					case "_surveyHolder": _surveyHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="servicePlanID">ServicePlanID of the entity to fetch.</param>
		/// <param name="surveyID">SurveyID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static ServicePlanSurvey Get(Guid servicePlanID, Guid surveyID)
		{
			OPathQuery query = new OPathQuery(typeof(ServicePlanSurvey), "ServicePlanID = ? && SurveyID = ?");
			object entity = DB.Engine.GetObject(query, servicePlanID, surveyID);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch ServicePlanSurvey entity with ServicePlanID = '{0}' and SurveyID = '{1}'.", servicePlanID, surveyID));
			}
			return (ServicePlanSurvey)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static ServicePlanSurvey GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ServicePlanSurvey), opathExpression);
			return (ServicePlanSurvey)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static ServicePlanSurvey GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(ServicePlanSurvey) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (ServicePlanSurvey)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ServicePlanSurvey[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ServicePlanSurvey), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ServicePlanSurvey[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(ServicePlanSurvey) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (ServicePlanSurvey[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ServicePlanSurvey[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ServicePlanSurvey), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public ServicePlanSurvey GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			ServicePlanSurvey clone = (ServicePlanSurvey)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ServicePlanID, _servicePlanID);
			Validator.Validate(Field.SurveyID, _surveyID);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_servicePlanID = Guid.Empty;
			_surveyID = Guid.Empty;
			_servicePlanHolder = null;
			_surveyHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the ServicePlanSurvey entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ServicePlanID field.
			/// </summary>
			ServicePlanID,
			/// <summary>
			/// Represents the SurveyID field.
			/// </summary>
			SurveyID,
		}
		
		#endregion
	}
}