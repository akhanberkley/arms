using System;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a ServicePlanActionType entity, which maps to table 'ServicePlanActionType' in the database.
    /// </summary>
    public class ServicePlanActionType : IObjectHelper
    {
        #region Public Fields

        /// <summary>
        /// Represents a user closing a service plan.
        /// </summary>
        public static readonly ServicePlanActionType ClosePlan = ServicePlanActionType.Get("CLOSE");
        /// <summary>
        /// Represents a user managing the plan account.
        /// </summary>
        public static readonly ServicePlanActionType ManageAccount = ServicePlanActionType.Get("MANAGE");
        /// <summary>
        /// Represents a user adding a service visit to the plan.
        /// </summary>
        public static readonly ServicePlanActionType AddVisit = ServicePlanActionType.Get("VISIT");
        /// <summary>
        /// Represents a user uploading a document and attaching it to a service plan.
        /// </summary>
        public static readonly ServicePlanActionType AddDocument = ServicePlanActionType.Get("DOCUMENT");
        /// <summary>
        /// Represents a user adding a comment/note to a service plan.
        /// </summary>
        public static readonly ServicePlanActionType AddNote = ServicePlanActionType.Get("NOTE");
        /// <summary>
        /// Represents a user reopening a closed service plan.
        /// </summary>
        public static readonly ServicePlanActionType Reopen = ServicePlanActionType.Get("REOPEN");
        /// <summary>
        /// Represents a user assigning a service plan.
        /// </summary>
        public static readonly ServicePlanActionType Assign = ServicePlanActionType.Get("ASSIGN");
        /// <summary>
        /// Represents a user releasing ownership of a serivce plan.
        /// </summary>
        public static readonly ServicePlanActionType ReleaseOwnership = ServicePlanActionType.Get("RELEASE");
        /// <summary>
        /// Represents a user closing a serivce plan.
        /// </summary>
        public static readonly ServicePlanActionType Close = ServicePlanActionType.Get("CLOSE");
        /// <summary>
        /// Represents a user canceling a serivce plan.
        /// </summary>
        public static readonly ServicePlanActionType Cancel = ServicePlanActionType.Get("CANCEL");
        /// <summary>
        /// Represents an user updating the insured details of a service plan.
        /// </summary>
        public static readonly ServicePlanActionType EditInsured = ServicePlanActionType.Get("UPDATEINS");
        /// <summary>
        /// Represents an user updating the insured policy details of a service plan.
        /// </summary>
        public static readonly ServicePlanActionType EditPolicy = ServicePlanActionType.Get("UPDATEPOL");
        /// <summary>
        /// Represents a user removing a document from a service plan.
        /// </summary>
        public static readonly ServicePlanActionType RemoveDocument = ServicePlanActionType.Get("REMOVEDOC");
        /// <summary>
        /// Represents a user removing a note from a service plan.
        /// </summary>
        public static readonly ServicePlanActionType RemoveNote = ServicePlanActionType.Get("REMOVENOTE");
        /// <summary>
        /// Represents a user removing a note from a service plan.
        /// </summary>
        public static readonly ServicePlanActionType ChangePlanType = ServicePlanActionType.Get("CHANGETYPE");
        /// <summary>
        /// Represents a user getting document reports from a service plan.
        /// </summary>
        public static readonly ServicePlanActionType DocumentReport = ServicePlanActionType.Get("DOCREPORT");
        /// <summary>
        /// Represents a user managing objectives in a service plan.
        /// </summary>
        public static readonly ServicePlanActionType ManagePlanObjectives = ServicePlanActionType.Get("OBJECTIVE");
        /// <summary>
        /// Represents a user managing objectives in a service plan.
        /// </summary>
        public static readonly ServicePlanActionType AssignPlanGrading = ServicePlanActionType.Get("PLANGRADE");

        #endregion

        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private ServicePlanActionType()
        {
        }

        #region --- Generated Members ---

        private string _code;
        private string _actionName;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Service Plan Action Type.
        /// </summary>
        public string Code
        {
            get { return _code; }
        }

        /// <summary>
        /// Gets the Action Name.
        /// </summary>
        public string ActionName
        {
            get { return _actionName; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_code": return _code;
                    case "_actionName": return _actionName;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_code": _code = (string)value; break;
                    case "_actionName": _actionName = (string)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="code">Code of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static ServicePlanActionType Get(string code)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanActionType), "Code = ?");
            object entity = DB.Engine.GetObject(query, code);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch ServicePlanActionType entity with Code = '{0}'.", code));
            }
            return (ServicePlanActionType)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ServicePlanActionType GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanActionType), opathExpression);
            return (ServicePlanActionType)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ServicePlanActionType GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ServicePlanActionType))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ServicePlanActionType)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlanActionType[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanActionType), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlanActionType[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ServicePlanActionType))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ServicePlanActionType[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlanActionType[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanActionType), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Determines if two ServicePlanActionType objects have the same semantic value.
        /// </summary>
        /// <param name="one">The first object to compare.</param>
        /// <param name="two">The second object to compare.</param>
        /// <returns>True if they are semantically identical, false otherwise.</returns>
        public static bool operator ==(ServicePlanActionType one, ServicePlanActionType two)
        {
            if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
            {
                return true;
            }
            else if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
            {
                return false;
            }
            else
            {
                return (one.CompareTo(two) == 0);
            }
        }

        /// <summary>
        /// Determines if two ServicePlanActionType objects have different semantic values.
        /// </summary>
        /// <param name="one">The first object to compare.</param>
        /// <param name="two">The second object to compare.</param>
        /// <returns>True if they are semantically dissimilar, false otherwise.</returns>
        public static bool operator !=(ServicePlanActionType one, ServicePlanActionType two)
        {
            if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
            {
                return false;
            }
            if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
            {
                return true;
            }
            else
            {
                return (one.CompareTo(two) != 0);
            }
        }

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="value">An object to compare with this instance.</param>
        /// <returns>Less than zero if this instance is less than value. 
        /// Zero if this instance is equal to value.
        /// Greater than zero if this instance is greater than value.</returns>
        public int CompareTo(object value)
        {
            if (value == null)
            {
                return 1;
            }
            if (value is ServicePlanActionType)
            {
                ServicePlanActionType obj = (ServicePlanActionType)value;
                return (this.Code.CompareTo(obj.Code));
            }
            throw new ArgumentException("Value is not the correct type.");
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode()
        {
            return this.Code.GetHashCode();
        }

        /// <summary>
        /// Determines whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="value">The object to compare with the current instance. </param>
        /// <returns>true if the specified object is equal to the current instance; otherwise, false.</returns>
        public override bool Equals(object value)
        {
            return (this == (value as ServicePlanActionType));
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _code = null;
            _actionName = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the ServicePlanActionType entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the Code field.
            /// </summary>
            Code,
            /// <summary>
            /// Represents the ActionName field.
            /// </summary>
            ActionName,
        }

        #endregion
    }
}