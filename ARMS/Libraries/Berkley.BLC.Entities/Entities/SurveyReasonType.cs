using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
 	/// <summary>
	/// Represents a SurveyReasonType entity, which maps to table 'SurveyReasonType' in the database.
	/// </summary>
	public class SurveyReasonType : IObjectHelper
	{

        /// <summary>
        /// None.
        /// </summary>
        public const int SETTING_ALL = 0;
        /// <summary>
        /// Survey Request.
        /// </summary>
        public const int SETTING_SURVEY_REQUEST = 1;
        /// <summary>
        /// Service Visit.
        /// </summary>
        public const int SETTING_SERVICE_VISIT = 2;
        /// <summary>
        /// Future Visit.
        /// </summary>
        public const int SETTING_FUTURE_VISIT = 3;
        /// <summary>
        /// Survey Request and Service Visit.
        /// </summary>
        public const int SETTING_SURVEY_REQUEST_AND_SERVICE_VISIT = 4;
        /// <summary>
        /// Survey Request and Future Visit.
        /// </summary>
        public const int SETTING_SURVEY_REQUEST_AND_FUTURE_VISIT = 5;
        /// <summary>
        /// Service Visit and Future Visit.
        /// </summary>
        public const int SETTING_SERVICE_VISIT_AND_FUTURE_VISIT = 6;

        /// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyReasonType()
		{
		}


		#region --- Generated Members ---

		private Guid _id;
		private Guid _companyID;
		private string _name = String.Empty;
		private int _displayOrder = Int32.MinValue;
        private int _setting = Int32.MinValue;
		private ObjectHolder _companyHolder = null;
		private IList _surveyList = null;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Survey Reason Type.
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets the Company.
		/// </summary>
		public Guid CompanyID
		{
			get { return _companyID; }
		}

		/// <summary>
		/// Gets the Survey Reason Type. Null value is 'String.Empty'.
		/// </summary>
		public string Name
		{
			get { return _name; }
		}

		/// <summary>
		/// Gets the Display Order. Null value is 'Int32.MinValue'.
		/// </summary>
		public int DisplayOrder
		{
			get { return _displayOrder; }
		}

        /// <summary>
        /// Gets the Setting.
        /// </summary>
        public int Setting
        {
            get { return _setting; }
        }

		/// <summary>
		/// Gets the instance of a Company object related to this entity.
		/// </summary>
		public Company Company
		{
			get
			{
				_companyHolder.Key = _companyID;
				return (Company)_companyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the collection of Survey objects related to this entity.
		/// </summary>
		public IList Surveys
		{
			get
			{
				return _surveyList;
			}
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_companyID": return _companyID;
					case "_name": return _name;
					case "_displayOrder": return _displayOrder;
                    case "_setting": return _setting;
					case "_companyHolder": return _companyHolder;
					case "_surveyList": return _surveyList;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (Guid)value; break;
					case "_companyID": _companyID = (Guid)value; break;
					case "_name": _name = (string)value; break;
					case "_displayOrder": _displayOrder = (int)value; break;
                    case "_setting": _setting = (int)value; break;
					case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
					case "_surveyList": _surveyList = (IList)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static SurveyReasonType Get(Guid id)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyReasonType), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch SurveyReasonType entity with ID = '{0}'.", id));
			}
			return (SurveyReasonType)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyReasonType GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyReasonType), opathExpression);
			return (SurveyReasonType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyReasonType GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyReasonType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyReasonType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyReasonType[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyReasonType), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyReasonType[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyReasonType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyReasonType[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyReasonType[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyReasonType), opathExpression, opathSort);
			return GetArray(query, parameters);
		}

        /// <summary>
        /// Returns an array of survey types for future visits.
        public static SurveyReasonType[] GetFutureSurveyReasonTypes(Company company)
        {
            ArrayList args = new ArrayList();
            args.Add(company.ID);
            args.Add(SETTING_ALL);
            args.Add(SETTING_FUTURE_VISIT);
            args.Add(SETTING_SURVEY_REQUEST_AND_FUTURE_VISIT);
            args.Add(SETTING_SERVICE_VISIT_AND_FUTURE_VISIT);

            return SurveyReasonType.GetSortedArray("CompanyID = ? && (Setting = ? || Setting = ? || Setting = ? || Setting = ?) ", "DisplayOrder ASC", args.ToArray());
        }

        /// <summary>
        /// Returns an array of survey types for service visits.
        public static SurveyReasonType[] GetServiceSurveyReasonTypes(Company company)
        {
            ArrayList args = new ArrayList();
            args.Add(company.ID);
            args.Add(SETTING_ALL);
            args.Add(SETTING_SERVICE_VISIT);
            args.Add(SETTING_SURVEY_REQUEST_AND_SERVICE_VISIT);
            args.Add(SETTING_SERVICE_VISIT_AND_FUTURE_VISIT);

            return SurveyReasonType.GetSortedArray("CompanyID = ? && (Setting = ? || Setting = ? || Setting = ? || Setting = ?)", "DisplayOrder ASC", args.ToArray());
        }

        /// <summary>
        /// Returns an array of survey types for survey requests.
        public static SurveyReasonType[] GetRequestSurveyReasonTypes(Company company)
        {
            ArrayList args = new ArrayList();
            args.Add(company.ID);
            args.Add(SETTING_ALL);
            args.Add(SETTING_SURVEY_REQUEST);
            args.Add(SETTING_SURVEY_REQUEST_AND_SERVICE_VISIT);
            args.Add(SETTING_SURVEY_REQUEST_AND_FUTURE_VISIT);

            return SurveyReasonType.GetSortedArray("CompanyID = ? && (Setting = ? || Setting = ? || Setting = ? || Setting = ?) ", "DisplayOrder ASC", args.ToArray());
        }


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Guid.Empty;
			_companyID = Guid.Empty;
			_name = null;
			_displayOrder = Int32.MinValue;
            _setting = Int32.MinValue;
			_companyHolder = null;
			_surveyList = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the SurveyReasonType entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the CompanyID field.
			/// </summary>
			CompanyID,
			/// <summary>
			/// Represents the Name field.
			/// </summary>
			Name,
			/// <summary>
			/// Represents the DisplayOrder field.
			/// </summary>
			DisplayOrder,
            /// <summary>
            /// Represents the Setting field.
            /// </summary>
            Setting,
		}
		
		#endregion
	}
}