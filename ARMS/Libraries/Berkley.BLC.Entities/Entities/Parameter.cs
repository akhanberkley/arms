using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a Parameter entity, which maps to table 'Parameter' in the database.
	/// </summary>
	public class Parameter : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private Parameter()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this Parameter.</param>
		public Parameter(int id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private int _id;
		private string _description;
		private int _dotNetDataTypeID;
		private bool _companyEditable;
		private DateTime _createDateTime;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the ID.
		/// </summary>
		public int ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets or sets the Description.
		/// </summary>
		public string Description
		{
			get { return _description; }
			set
			{
				VerifyWritable();
				_description = value;
			}
		}

		/// <summary>
		/// Gets or sets the Dot Net Data Type ID.
		/// </summary>
		public int DotNetDataTypeID
		{
			get { return _dotNetDataTypeID; }
			set
			{
				VerifyWritable();
				_dotNetDataTypeID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Company Editable.
		/// </summary>
		public bool CompanyEditable
		{
			get { return _companyEditable; }
			set
			{
				VerifyWritable();
				_companyEditable = value;
			}
		}

		/// <summary>
		/// Gets or sets the Create Date Time.
		/// </summary>
		public DateTime CreateDateTime
		{
			get { return _createDateTime; }
			set
			{
				VerifyWritable();
				_createDateTime = value;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_description": return _description;
					case "_dotNetDataTypeID": return _dotNetDataTypeID;
					case "_companyEditable": return _companyEditable;
					case "_createDateTime": return _createDateTime;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (int)value; break;
					case "_description": _description = (string)value; break;
					case "_dotNetDataTypeID": _dotNetDataTypeID = (int)value; break;
					case "_companyEditable": _companyEditable = (bool)value; break;
					case "_createDateTime": _createDateTime = (DateTime)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static Parameter Get(int id)
		{
			OPathQuery query = new OPathQuery(typeof(Parameter), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch Parameter entity with ID = '{0}'.", id));
			}
			return (Parameter)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static Parameter GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(Parameter), opathExpression);
			return (Parameter)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static Parameter GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(Parameter) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (Parameter)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static Parameter[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(Parameter), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static Parameter[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(Parameter) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (Parameter[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static Parameter[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(Parameter), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public Parameter GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			Parameter clone = (Parameter)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ID, _id);
			Validator.Validate(Field.Description, _description);
			Validator.Validate(Field.DotNetDataTypeID, _dotNetDataTypeID);
			Validator.Validate(Field.CompanyEditable, _companyEditable);
			Validator.Validate(Field.CreateDateTime, _createDateTime);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Int32.MinValue;
			_description = null;
			_dotNetDataTypeID = Int32.MinValue;
			_companyEditable = false;
			_createDateTime = DateTime.MinValue;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the Parameter entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the Description field.
			/// </summary>
			Description,
			/// <summary>
			/// Represents the DotNetDataTypeID field.
			/// </summary>
			DotNetDataTypeID,
			/// <summary>
			/// Represents the CompanyEditable field.
			/// </summary>
			CompanyEditable,
			/// <summary>
			/// Represents the CreateDateTime field.
			/// </summary>
			CreateDateTime,
		}
		
		#endregion
	}
}