using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a CompanyPolicySystem entity, which maps to table 'CompanyPolicySystem' in the database.
	/// </summary>
	public class CompanyPolicySystem : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private CompanyPolicySystem()
		{
		}


		#region --- Generated Members ---

		private Guid _policySystemCompanyID;
		private Guid _companyID;
		private int _displayOrder;
		private ObjectHolder _companyHolder = null;
		private ObjectHolder _policySystemCompanyHolder = null;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Policy System Company.
		/// </summary>
		public Guid PolicySystemCompanyID
		{
			get { return _policySystemCompanyID; }
		}

		/// <summary>
		/// Gets the Company.
		/// </summary>
		public Guid CompanyID
		{
			get { return _companyID; }
		}

		/// <summary>
		/// Gets the Display Order.
		/// </summary>
		public int DisplayOrder
		{
			get { return _displayOrder; }
		}

		/// <summary>
		/// Gets the instance of a Company object related to this entity.
		/// </summary>
		public Company Company
		{
			get
			{
				_companyHolder.Key = _companyID;
				return (Company)_companyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Company object related to this entity.
		/// </summary>
		public Company PolicySystemCompany
		{
			get
			{
				_policySystemCompanyHolder.Key = _policySystemCompanyID;
				return (Company)_policySystemCompanyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_policySystemCompanyID": return _policySystemCompanyID;
					case "_companyID": return _companyID;
					case "_displayOrder": return _displayOrder;
					case "_companyHolder": return _companyHolder;
					case "_policySystemCompanyHolder": return _policySystemCompanyHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_policySystemCompanyID": _policySystemCompanyID = (Guid)value; break;
					case "_companyID": _companyID = (Guid)value; break;
					case "_displayOrder": _displayOrder = (int)value; break;
					case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
					case "_policySystemCompanyHolder": _policySystemCompanyHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="policySystemCompanyID">PolicySystemCompanyID of the entity to fetch.</param>
		/// <param name="companyID">CompanyID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static CompanyPolicySystem Get(Guid policySystemCompanyID, Guid companyID)
		{
			OPathQuery query = new OPathQuery(typeof(CompanyPolicySystem), "PolicySystemCompanyID = ? && CompanyID = ?");
			object entity = DB.Engine.GetObject(query, policySystemCompanyID, companyID);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch CompanyPolicySystem entity with PolicySystemCompanyID = '{0}' and CompanyID = '{1}'.", policySystemCompanyID, companyID));
			}
			return (CompanyPolicySystem)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static CompanyPolicySystem GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CompanyPolicySystem), opathExpression);
			return (CompanyPolicySystem)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static CompanyPolicySystem GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(CompanyPolicySystem) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (CompanyPolicySystem)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CompanyPolicySystem[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CompanyPolicySystem), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CompanyPolicySystem[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(CompanyPolicySystem) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (CompanyPolicySystem[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CompanyPolicySystem[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CompanyPolicySystem), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_policySystemCompanyID = Guid.Empty;
			_companyID = Guid.Empty;
			_displayOrder = Int32.MinValue;
			_companyHolder = null;
			_policySystemCompanyHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the CompanyPolicySystem entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the PolicySystemCompanyID field.
			/// </summary>
			PolicySystemCompanyID,
			/// <summary>
			/// Represents the CompanyID field.
			/// </summary>
			CompanyID,
			/// <summary>
			/// Represents the DisplayOrder field.
			/// </summary>
			DisplayOrder,
		}
		
		#endregion
	}
}