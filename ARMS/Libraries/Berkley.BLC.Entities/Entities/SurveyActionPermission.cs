using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyActionPermission entity, which maps to table 'SurveyActionPermission' in the database.
	/// </summary>
	public class SurveyActionPermission : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyActionPermission()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this SurveyActionPermission.</param>
		public SurveyActionPermission(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


        #region --- Generated Members ---

        private Guid _id;
        private Guid _permissionSetID;
        private Guid _surveyActionID;
        private string _serviceTypeCode = String.Empty;
        private int _setting;
        private ObjectHolder _permissionSetHolder = null;
        private ObjectHolder _serviceTypeHolder = null;
        private ObjectHolder _surveyActionHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Survey Action Permission ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Permission Set.
        /// </summary>
        public Guid PermissionSetID
        {
            get { return _permissionSetID; }
            set
            {
                VerifyWritable();
                _permissionSetID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Survey Action.
        /// </summary>
        public Guid SurveyActionID
        {
            get { return _surveyActionID; }
            set
            {
                VerifyWritable();
                _surveyActionID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Service Type. Null value is 'String.Empty'.
        /// </summary>
        public string ServiceTypeCode
        {
            get { return _serviceTypeCode; }
            set
            {
                VerifyWritable();
                _serviceTypeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Setting.
        /// </summary>
        public int Setting
        {
            get { return _setting; }
            set
            {
                VerifyWritable();
                _setting = value;
            }
        }

        /// <summary>
        /// Gets the instance of a PermissionSet object related to this entity.
        /// </summary>
        public PermissionSet PermissionSet
        {
            get
            {
                _permissionSetHolder.Key = _permissionSetID;
                return (PermissionSet)_permissionSetHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a ServiceType object related to this entity.
        /// </summary>
        public ServiceType ServiceType
        {
            get
            {
                _serviceTypeHolder.Key = _serviceTypeCode;
                return (ServiceType)_serviceTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a SurveyAction object related to this entity.
        /// </summary>
        public SurveyAction SurveyAction
        {
            get
            {
                _surveyActionHolder.Key = _surveyActionID;
                return (SurveyAction)_surveyActionHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_permissionSetID": return _permissionSetID;
                    case "_surveyActionID": return _surveyActionID;
                    case "_serviceTypeCode": return _serviceTypeCode;
                    case "_setting": return _setting;
                    case "_permissionSetHolder": return _permissionSetHolder;
                    case "_serviceTypeHolder": return _serviceTypeHolder;
                    case "_surveyActionHolder": return _surveyActionHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_permissionSetID": _permissionSetID = (Guid)value; break;
                    case "_surveyActionID": _surveyActionID = (Guid)value; break;
                    case "_serviceTypeCode": _serviceTypeCode = (string)value; break;
                    case "_setting": _setting = (int)value; break;
                    case "_permissionSetHolder": _permissionSetHolder = (ObjectHolder)value; break;
                    case "_serviceTypeHolder": _serviceTypeHolder = (ObjectHolder)value; break;
                    case "_surveyActionHolder": _surveyActionHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static SurveyActionPermission Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyActionPermission), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch SurveyActionPermission entity with ID = '{0}'.", id));
            }
            return (SurveyActionPermission)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyActionPermission GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyActionPermission), opathExpression);
            return (SurveyActionPermission)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyActionPermission GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyActionPermission))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyActionPermission)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyActionPermission[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyActionPermission), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyActionPermission[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyActionPermission))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyActionPermission[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyActionPermission[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyActionPermission), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public SurveyActionPermission GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            SurveyActionPermission clone = (SurveyActionPermission)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.PermissionSetID, _permissionSetID);
            Validator.Validate(Field.SurveyActionID, _surveyActionID);
            Validator.Validate(Field.ServiceTypeCode, _serviceTypeCode);
            Validator.Validate(Field.Setting, _setting);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _permissionSetID = Guid.Empty;
            _surveyActionID = Guid.Empty;
            _serviceTypeCode = null;
            _setting = Int32.MinValue;
            _permissionSetHolder = null;
            _serviceTypeHolder = null;
            _surveyActionHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the SurveyActionPermission entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the PermissionSetID field.
            /// </summary>
            PermissionSetID,
            /// <summary>
            /// Represents the SurveyActionID field.
            /// </summary>
            SurveyActionID,
            /// <summary>
            /// Represents the ServiceTypeCode field.
            /// </summary>
            ServiceTypeCode,
            /// <summary>
            /// Represents the Setting field.
            /// </summary>
            Setting,
        }

        #endregion
    }
}