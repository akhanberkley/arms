using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyReviewQueue entity, which maps to table 'SurveyReviewQueue' in the database.
	/// </summary>
	public class SurveyReviewQueue : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyReviewQueue()
		{
		}


        #region --- Generated Members ---

        private Guid _id;
        private Guid _companyID;
        private string _name = String.Empty;
        private string _countQuery = String.Empty;
        private string _resultSetQuery = String.Empty;
        private int _displayOrder;
        private ObjectHolder _companyHolder = null;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Survey Review Queue.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets the Survey Review Queue. Null value is 'String.Empty'.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Gets the Count Query. Null value is 'String.Empty'.
        /// </summary>
        public string CountQuery
        {
            get { return _countQuery; }
        }

        /// <summary>
        /// Gets the Result Set Query. Null value is 'String.Empty'.
        /// </summary>
        public string ResultSetQuery
        {
            get { return _resultSetQuery; }
        }

        /// <summary>
        /// Gets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_companyID": return _companyID;
                    case "_name": return _name;
                    case "_countQuery": return _countQuery;
                    case "_resultSetQuery": return _resultSetQuery;
                    case "_displayOrder": return _displayOrder;
                    case "_companyHolder": return _companyHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_name": _name = (string)value; break;
                    case "_countQuery": _countQuery = (string)value; break;
                    case "_resultSetQuery": _resultSetQuery = (string)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static SurveyReviewQueue Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyReviewQueue), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch SurveyReviewQueue entity with ID = '{0}'.", id));
            }
            return (SurveyReviewQueue)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyReviewQueue GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyReviewQueue), opathExpression);
            return (SurveyReviewQueue)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyReviewQueue GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyReviewQueue))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyReviewQueue)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyReviewQueue[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyReviewQueue), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyReviewQueue[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyReviewQueue))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyReviewQueue[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyReviewQueue[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyReviewQueue), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _companyID = Guid.Empty;
            _name = null;
            _countQuery = null;
            _resultSetQuery = null;
            _displayOrder = Int32.MinValue;
            _companyHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the SurveyReviewQueue entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the CountQuery field.
            /// </summary>
            CountQuery,
            /// <summary>
            /// Represents the ResultSetQuery field.
            /// </summary>
            ResultSetQuery,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
        }

        #endregion
    }
}