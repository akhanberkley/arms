using System;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a VWSurvey entity, which maps to table 'VW_Survey' in the database.
    /// </summary>
    [Serializable]
    public class VWSurvey : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private VWSurvey()
        {
        }

        #region --- Generated Members ---

        private Guid _id = Guid.Empty;
        private Guid _surveyID;
        private Guid _policySystemID;
        private int _surveyNumber;
        private Guid _companyID;
        private string _companyAbbreviation;
        private Guid _surveyTypeID;
        private string _surveyTypeName;
        private string _surveyReasonTypes = String.Empty;
        private Guid _surveyQualityID = Guid.Empty;
        private string _surveyQualityTypeName = String.Empty;
        private string _serviceTypeCode;
        private string _serviceTypeName;
        private string _surveyStatusCode;
        private string _surveyStatusName;
        private string _surveyStatusTypeCode;
        private string _surveyStatusTypeName;
        private Guid _assignedUserID = Guid.Empty;
        private string _assignedUserName = String.Empty;
        private Guid _assignedUserManagerID = Guid.Empty;
        private Guid _recommendedUserID = Guid.Empty;
        private Guid _recommendedUserManagerID = Guid.Empty;
        private Guid _completedUserManagerID = Guid.Empty;
        private string _createStateCode;
        private DateTime _createDate;
        private Guid _createByInternalUserID = Guid.Empty;
        private string _createByExternalUserName = String.Empty;
        private DateTime _assignDate = DateTime.MinValue;
        private DateTime _reassignDate = DateTime.MinValue;
        private DateTime _acceptDate = DateTime.MinValue;
        private DateTime _surveyedDate = DateTime.MinValue;
        private DateTime _reportCompleteDate = DateTime.MinValue;
        private DateTime _dueDate = DateTime.MinValue;
        private DateTime _completeDate = DateTime.MinValue;
        private DateTime _canceledDate = DateTime.MinValue;
        private DateTime _underwritingAcceptDate = DateTime.MinValue;
        private DateTime _reminderDate = DateTime.MinValue;
        private string _reminderComment = String.Empty;
        private string _priority = String.Empty;
        private decimal _surveyHours = Decimal.MinValue;
        private decimal _callCount = Decimal.MinValue;
        private bool _correction = false;
        private bool _review = false;
        private string _surveyGradingCode = String.Empty;
        private string _surveyGradingName = String.Empty;
        private string _severityRatingCode = String.Empty;
        private string _severityRatingName = String.Empty;
        private DateTime _policyEffectiveDate = DateTime.MinValue;
        private DateTime _policyExpireDate = DateTime.MinValue;
        private DateTime _reviewerCompleteDate = DateTime.MinValue;
        private decimal _policyPremium = Decimal.MinValue;
        private string _profitCenter = String.Empty;
        private decimal _feeConsultantCost = Decimal.MinValue;
        private Guid _consultantUserID = Guid.Empty;
        private string _consultantUserName = String.Empty;
        private bool _consultantIsFeeCompany = false;
        private string _profitCenterByUser = String.Empty;
        private Guid _reviewerUserID = Guid.Empty;
        private string _reviewerUserName = String.Empty;
        private DateTime _lastModifiedOn = DateTime.MinValue;
        private Guid _insuredID = Guid.Empty;
        private string _clientID = String.Empty;
        private string _portfolioID = String.Empty;
        private string _insuredName = String.Empty;
        private string _insuredName2 = String.Empty;
        private string _insuredStreetLine1 = String.Empty;
        private string _insuredStreetLine2 = String.Empty;
        private string _insuredStreetLine3 = String.Empty;
        private string _insuredCity = String.Empty;
        private string _insuredStateCode = String.Empty;
        private string _insuredZipCode = String.Empty;
        private string _businessOperations = String.Empty;
        private string _sicCode = String.Empty;
        private string _underwriter = String.Empty;
        private string _underwriterCode = String.Empty;
        private string _underwriter2Code = String.Empty;
        private DateTime _insuredNonRenewedDate = DateTime.MinValue;
        private string _agencyNumber = String.Empty;
        private string _agencySubNumber = String.Empty;
        private string _agencyName = String.Empty;
        private string _agencyStreetLine1 = String.Empty;
        private string _agencyStreetLine2 = String.Empty;
        private string _agencyCity = String.Empty;
        private string _agencyStateCode = String.Empty;
        private string _agencyZipCode = String.Empty;
        private string _agencyPhoneNumber = String.Empty;
        private string _agencyFaxNumber = String.Empty;
        private Guid _locationID = Guid.Empty;
        private int _locationNumber = Int32.MinValue;
        private string _locationStreetLine1 = String.Empty;
        private string _locationStreetLine2 = String.Empty;
        private string _locationCity = String.Empty;
        private string _locationStateCode = String.Empty;
        private string _locationZipCode = String.Empty;
        private string _locationDescription = String.Empty;
        private int _locationCount = Int32.MinValue;
        private string _recCount = String.Empty;
        private string _paceAccount = String.Empty;
        private string _keyAccount = String.Empty;
        private string _lossSensitive = String.Empty;
        private string _rmsRep = String.Empty;
        private string _paceRep = String.Empty;
        private DateTime _todaysDate = DateTime.MinValue;
        private string _surveyLetters = String.Empty;
        private decimal _cwgDrivingTime = Decimal.MinValue;
        private decimal _cwgSurveyTime = Decimal.MinValue;
        private decimal _cwgReportWriteupTime = Decimal.MinValue;
        private decimal _techConsultingTime = Decimal.MinValue;
        private Guid _surveyReleaseOwnershipTypeID = Guid.Empty;
        private string _policyNumber;        
        private string _approvalCode = String.Empty;
        private bool _containsCriticalRec = false;
        private bool _containsOpenCriticalRec = false;
        private DateTime _requestedDate = DateTime.MinValue;
        private int _initialLetterCount = Int32.MinValue;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets the Survey ID.
        /// </summary>
        public Guid SurveyID
        {
            get { return _surveyID; }
        }

        /// <summary>
        /// Gets the PolicySystemID.
        /// </summary>
        public Guid PolicySystemID
        {
            get { return _policySystemID; }
        }

        /// <summary>
        /// Gets the Survey Number.
        /// </summary>
        public int SurveyNumber
        {
            get { return _surveyNumber; }
        }

        /// <summary>
        /// Gets the Company ID.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets the Company Abbreviation.
        /// </summary>
        public string CompanyAbbreviation
        {
            get { return _companyAbbreviation; }
        }

        /// <summary>
        /// Gets the Survey Type ID.
        /// </summary>
        public Guid SurveyTypeID
        {
            get { return _surveyTypeID; }
        }

        /// <summary>
        /// Gets the Survey Type Name.
        /// </summary>
        public string SurveyTypeName
        {
            get { return _surveyTypeName; }
        }

        /// <summary>
        /// Gets the Survey Reason Type(s).
        /// </summary>
        public string SurveyReasonTypes
        {
            get { return _surveyReasonTypes; }
        }

        /// <summary>
        /// Gets the Survey Quality ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid SurveyQualityID
        {
            get { return _surveyQualityID; }
        }

        /// <summary>
        /// Gets the Survey Quality Type Name. Null value is 'String.Empty'.
        /// </summary>
        public string SurveyQualityTypeName
        {
            get { return _surveyQualityTypeName; }
        }

        /// <summary>
        /// Gets the Service Type Code.
        /// </summary>
        public string ServiceTypeCode
        {
            get { return _serviceTypeCode; }
        }

        /// <summary>
        /// Gets the Service Type Name.
        /// </summary>
        public string ServiceTypeName
        {
            get { return _serviceTypeName; }
        }

        /// <summary>
        /// Gets the Survey Status Code.
        /// </summary>
        public string SurveyStatusCode
        {
            get { return _surveyStatusCode; }
        }

        /// <summary>
        /// Gets the Survey Status Name.
        /// </summary>
        public string SurveyStatusName
        {
            get { return _surveyStatusName; }
        }

        /// <summary>
        /// Gets the Survey Status Type Code.
        /// </summary>
        public string SurveyStatusTypeCode
        {
            get { return _surveyStatusTypeCode; }
        }

        /// <summary>
        /// Gets the Survey Status Type Name.
        /// </summary>
        public string SurveyStatusTypeName
        {
            get { return _surveyStatusTypeName; }
        }

        /// <summary>
        /// Gets the Assigned User ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid AssignedUserID
        {
            get { return _assignedUserID; }
        }

        /// <summary>
        /// Gets the Assigned User Name. Null value is 'String.Empty'.
        /// </summary>
        public string AssignedUserName
        {
            get { return _assignedUserName; }
        }

        /// <summary>
        /// Gets the Assigned User Manager ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid AssignedUserManagerID
        {
            get { return _assignedUserManagerID; }
        }

        /// <summary>
        /// Gets the Recommended User ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid RecommendedUserID
        {
            get { return _recommendedUserID; }
        }

        /// <summary>
        /// Gets the Recommended User Manager ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid RecommendedUserManagerID
        {
            get { return _recommendedUserManagerID; }
        }

        /// <summary>
        /// Gets the Completed User Manager ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid CompletedUserManagerID
        {
            get { return _completedUserManagerID; }
        }

        /// <summary>
        /// Gets the Create State Code.
        /// </summary>
        public string CreateStateCode
        {
            get { return _createStateCode; }
        }

        /// <summary>
        /// Gets the Create Date.
        /// </summary>
        public DateTime CreateDate
        {
            get { return _createDate; }
        }

        /// <summary>
        /// Gets the Tech Complete Date.
        /// </summary>
        public DateTime ReviewerCompleteDate
        {
            get { return _reviewerCompleteDate; }
        }

        /// <summary>
        /// Gets the Create By Internal User ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid CreateByInternalUserID
        {
            get { return _createByInternalUserID; }
        }

        /// <summary>
        /// Gets the Create By External User Name. Null value is 'String.Empty'.
        /// </summary>
        public string CreateByExternalUserName
        {
            get { return _createByExternalUserName; }
        }

        /// <summary>
        /// Gets the Assign Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime AssignDate
        {
            get { return _assignDate; }
        }

        /// <summary>
        /// Gets the Reassign Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime ReassignDate
        {
            get { return _reassignDate; }
        }

        /// <summary>
        /// Gets the Accept Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime AcceptDate
        {
            get { return _acceptDate; }
        }

        /// <summary>
        /// Gets the Surveyed Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime SurveyedDate
        {
            get { return _surveyedDate; }
        }

        /// <summary>
        /// Gets the Report Complete Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime ReportCompleteDate
        {
            get { return _reportCompleteDate; }
        }

        /// <summary>
        /// Gets the Due Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime DueDate
        {
            get { return _dueDate; }
        }

        /// <summary>
        /// Gets the Complete Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime CompleteDate
        {
            get { return _completeDate; }
        }

        /// <summary>
        /// Gets the Canceled Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime CanceledDate
        {
            get { return _canceledDate; }
        }

        /// <summary>
        /// Gets the Underwriting Accept Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime UnderwritingAcceptDate
        {
            get { return _underwritingAcceptDate; }
        }

        /// <summary>
        /// Gets the Reminder Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime ReminderDate
        {
            get { return _reminderDate; }
        }

        /// <summary>
        /// Gets the Reminder Comment. Null value is 'String.Empty'.
        /// </summary>
        public string ReminderComment
        {
            get { return _reminderComment; }
        }

        /// <summary>
        /// Gets the Priority. Null value is 'String.Empty'.
        /// </summary>
        public string Priority
        {
            get { return _priority; }
        }

        /// <summary>
        /// Gets the Survey Hours. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal SurveyHours
        {
            get { return _surveyHours; }
        }

        /// <summary>
        /// Gets the Call Count. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal CallCount
        {
            get { return _callCount; }
        }

        /// <summary>
        /// Gets the Correction. Null value is 'false'.
        /// </summary>
        public bool Correction
        {
            get { return _correction; }
        }

        /// <summary>
        /// Gets the Review. Null value is 'false'.
        /// </summary>
        public bool Review
        {
            get { return _review; }
        }

        /// <summary>
        /// Gets the Survey Grading Code. Null value is 'String.Empty'.
        /// </summary>
        public string SurveyGradingCode
        {
            get { return _surveyGradingCode; }
        }

        /// <summary>
        /// Gets the Survey Grading Name. Null value is 'String.Empty'.
        /// </summary>
        public string SurveyGradingName
        {
            get { return _surveyGradingName; }
        }

        /// <summary>
        /// Gets the Severity Rating Code. Null value is 'String.Empty'.
        /// </summary>
        public string SeverityRatingCode
        {
            get { return _severityRatingCode; }
        }

        /// <summary>
        /// Gets the Severity Rating Name. Null value is 'String.Empty'.
        /// </summary>
        public string SeverityRatingName
        {
            get { return _severityRatingName; }
        }

        /// <summary>
        /// Gets the Policy Effective Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime PolicyEffectiveDate
        {
            get { return _policyEffectiveDate; }
        }

        /// <summary>
        /// Gets the Policy Expire Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime PolicyExpireDate
        {
            get { return _policyExpireDate; }
        }

        /// <summary>
        /// Gets the Policy Premium. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal PolicyPremium
        {
            get { return _policyPremium; }
        }

        /// <summary>
        /// Gets the Profit Center. Null value is 'String.Empty'.
        /// </summary>
        public string ProfitCenter
        {
            get { return _profitCenter; }
        }

        /// <summary>
        /// Gets the Fee Consultant Cost. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal FeeConsultantCost
        {
            get { return _feeConsultantCost; }
        }

        /// <summary>
        /// Gets the Consultant User ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid ConsultantUserID
        {
            get { return _consultantUserID; }
        }

        /// <summary>
        /// Gets the Consultant User Name. Null value is 'String.Empty'.
        /// </summary>
        public string ConsultantUserName
        {
            get { return _consultantUserName; }
        }

        /// <summary>
        /// Gets the Consultant Is Fee Company. Null value is 'false'.
        /// </summary>
        public bool ConsultantIsFeeCompany
        {
            get { return _consultantIsFeeCompany; }
        }

        /// <summary>
        /// Gets the Profit Center By User. Null value is 'String.Empty'.
        /// </summary>
        public string ProfitCenterByUser
        {
            get { return _profitCenterByUser; }
        }

        /// <summary>
        /// Gets the Reviewer User ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid ReviewerUserID
        {
            get { return _reviewerUserID; }
        }

        /// <summary>
        /// Gets the Reviewer User Name. Null value is 'String.Empty'.
        /// </summary>
        public string ReviewerUserName
        {
            get { return _reviewerUserName; }
        }

        /// <summary>
        /// Gets the Last Modified On. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime LastModifiedOn
        {
            get { return _lastModifiedOn; }
        }

        /// <summary>
        /// Gets the Insured ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid InsuredID
        {
            get { return _insuredID; }
        }

        /// <summary>
        /// Gets the Client ID. Null value is 'String.Empty'.
        /// </summary>
        public string ClientID
        {
            get { return _clientID; }
        }

        /// <summary>
        /// Gets the Portfolio ID. Null value is 'String.Empty'.
        /// </summary>
        public string PortfolioID
        {
            get { return _portfolioID; }
        }

        /// <summary>
        /// Gets the Insured Name. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredName
        {
            get { return _insuredName; }
        }

        /// <summary>
        /// Gets the Insured Name 2. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredName2
        {
            get { return _insuredName2; }
        }

        /// <summary>
        /// Gets the Insured Street Line 1. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredStreetLine1
        {
            get { return _insuredStreetLine1; }
        }

        /// <summary>
        /// Gets the Insured Street Line 2. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredStreetLine2
        {
            get { return _insuredStreetLine2; }
        }

        /// <summary>
        /// Gets the Insured Street Line 3. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredStreetLine3
        {
            get { return _insuredStreetLine3; }
        }

        /// <summary>
        /// Gets the Insured City. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredCity
        {
            get { return _insuredCity; }
        }

        /// <summary>
        /// Gets the Insured State Code. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredStateCode
        {
            get { return _insuredStateCode; }
        }

        /// <summary>
        /// Gets the Insured Zip Code. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredZipCode
        {
            get { return _insuredZipCode; }
        }

        /// <summary>
        /// Gets the Business Operations. Null value is 'String.Empty'.
        /// </summary>
        public string BusinessOperations
        {
            get { return _businessOperations; }
        }

        /// <summary>
        /// Gets the SIC Code. Null value is 'String.Empty'.
        /// </summary>
        public string SICCode
        {
            get { return _sicCode; }
        }

        /// <summary>
        /// Gets the Underwriter. Null value is 'String.Empty'.
        /// </summary>
        public string Underwriter
        {
            get { return _underwriter; }
        }

        /// <summary>
        /// Gets the Underwriter Code. Null value is 'String.Empty'.
        /// </summary>
        public string UnderwriterCode
        {
            get { return _underwriterCode; }
        }

        /// <summary>
        /// Gets the Underwriter Code. Null value is 'String.Empty'.
        /// </summary>
        public string Underwriter2Code
        {
            get { return _underwriter2Code; }
        }
        /// <summary>
        /// Gets the Insured Non Renewed Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime InsuredNonRenewedDate
        {
            get { return _insuredNonRenewedDate; }
        }

        /// <summary>
        /// Gets the Agency Number. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyNumber
        {
            get { return _agencyNumber; }
        }

        /// <summary>
        /// Gets the Agency Sub Number. Null value is 'String.Empty'.
        /// </summary>
        public string AgencySubNumber
        {
            get { return _agencySubNumber; }
        }

        /// <summary>
        /// Gets the Agency Name. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyName
        {
            get { return _agencyName; }
        }

        /// <summary>
        /// Gets the Agency Street Line 1. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyStreetLine1
        {
            get { return _agencyStreetLine1; }
        }

        /// <summary>
        /// Gets the Agency Street Line 2. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyStreetLine2
        {
            get { return _agencyStreetLine2; }
        }

        /// <summary>
        /// Gets the Agency City. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyCity
        {
            get { return _agencyCity; }
        }

        /// <summary>
        /// Gets the Agency State Code. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyStateCode
        {
            get { return _agencyStateCode; }
        }

        /// <summary>
        /// Gets the Agency Zip Code. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyZipCode
        {
            get { return _agencyZipCode; }
        }

        /// <summary>
        /// Gets the Agency Phone Number. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyPhoneNumber
        {
            get { return _agencyPhoneNumber; }
        }

        /// <summary>
        /// Gets the Agency Fax Number. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyFaxNumber
        {
            get { return _agencyFaxNumber; }
        }

        /// <summary>
        /// Gets the Location ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid LocationID
        {
            get { return _locationID; }
        }

        /// <summary>
        /// Gets the Location Number. Null value is 'Int32.MinValue'.
        /// </summary>
        public int LocationNumber
        {
            get { return _locationNumber; }
        }

        /// <summary>
        /// Gets the Location Street Line 1. Null value is 'String.Empty'.
        /// </summary>
        public string LocationStreetLine1
        {
            get { return _locationStreetLine1; }
        }

        /// <summary>
        /// Gets the Location Street Line 2. Null value is 'String.Empty'.
        /// </summary>
        public string LocationStreetLine2
        {
            get { return _locationStreetLine2; }
        }

        /// <summary>
        /// Gets the Location City. Null value is 'String.Empty'.
        /// </summary>
        public string LocationCity
        {
            get { return _locationCity; }
        }

        /// <summary>
        /// Gets the Location State Code. Null value is 'String.Empty'.
        /// </summary>
        public string LocationStateCode
        {
            get { return _locationStateCode; }
        }

        /// <summary>
        /// Gets the Location Zip Code. Null value is 'String.Empty'.
        /// </summary>
        public string LocationZipCode
        {
            get { return _locationZipCode; }
        }

        /// <summary>
        /// Gets the Location Description. Null value is 'String.Empty'.
        /// </summary>
        public string LocationDescription
        {
            get { return _locationDescription; }
        }

        /// <summary>
        /// Gets the Location Count. Null value is 'Int32.MinValue'.
        /// </summary>
        public int LocationCount
        {
            get { return _locationCount; }
        }

        /// <summary>
        /// Gets the Rec Count. Null value is 'String.Empty'.
        /// </summary>
        public string RecCount
        {
            get { return _recCount; }
        }

        /// <summary>
        /// Gets the Pace Account. Null value is 'String.Empty'.
        /// </summary>
        public string PaceAccount
        {
            get { return _paceAccount; }
        }

        /// <summary>
        /// Gets the Key Account. Null value is 'String.Empty'.
        /// </summary>
        public string KeyAccount
        {
            get { return _keyAccount; }
        }

        /// <summary>
        /// Gets the Loss Sensitive. Null value is 'String.Empty'.
        /// </summary>
        public string LossSensitive
        {
            get { return _lossSensitive; }
        }

        /// <summary>
        /// Gets the RMS Rep. Null value is 'String.Empty'.
        /// </summary>
        public string RMSRep
        {
            get { return _rmsRep; }
        }

        /// <summary>
        /// Gets the Pace Rep. Null value is 'String.Empty'.
        /// </summary>
        public string PaceRep
        {
            get { return _paceRep; }
        }

        /// <summary>
        /// Gets the Todays Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime TodaysDate
        {
            get { return _todaysDate; }
        }

        /// <summary>
        /// Gets the Survey Letters. Null value is 'String.Empty'.
        /// </summary>
        public string SurveyLetters
        {
            get { return _surveyLetters; }
        }

        /// <summary>
        /// Gets the CWG Driving Time. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal CWGDrivingTime
        {
            get { return _cwgDrivingTime; }
        }

        /// <summary>
        /// Gets the CWG Survey Time. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal CWGSurveyTime
        {
            get { return _cwgSurveyTime; }
        }

        /// <summary>
        /// Gets the CWG Report Writeup Time. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal CWGReportWriteupTime
        {
            get { return _cwgReportWriteupTime; }
        }

        /// <summary>
        /// Gets the TechConsultingTime Time. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal TechConsultingTime
        {
            get { return _techConsultingTime; }
        }

        /// <summary>
        /// Gets the Survey Release Ownership Type ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid SurveyReleaseOwnershipTypeID
        {
            get { return _surveyReleaseOwnershipTypeID; }
        }

        /// <summary>
        /// Gets the Policy Number.
        /// </summary>
        public string PolicyNumber
        {
            get { return _policyNumber; }
        }
                
        /// <summary>
        /// Gets or sets the Approval Code. Null value is 'String.Empty'.
        /// </summary>
        public string ApprovalCode
        {
            get { return _approvalCode; }
            set
            {
                _approvalCode = value;
            }
        }

        /// <summary>
        /// Gets the Contains Critical Rec. Null value is 'false'.
        /// </summary>
        public bool ContainsCriticalRec
        {
            get { return _containsCriticalRec; }
        }

        /// <summary>
        /// Gets the Contains Critical Rec. Null value is 'false'.
        /// </summary>
        public bool ContainsOpenCriticalRec
        {
            get { return _containsOpenCriticalRec; }
        }

        /// <summary>
        /// Gets or sets the Requested Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime RequestedDate
        {
            get { return _requestedDate; }
        }

        /// <summary>
        /// Gets the Initial Letter Count. Null value is 'Int32.MinValue'.
        /// </summary>
        public int InitialLetterCount
        {
            get { return _initialLetterCount; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_surveyID": return _surveyID;
                    case "_policySystemID": return _policySystemID;
                    case "_surveyNumber": return _surveyNumber;
                    case "_companyID": return _companyID;
                    case "_companyAbbreviation": return _companyAbbreviation;
                    case "_surveyTypeID": return _surveyTypeID;
                    case "_surveyTypeName": return _surveyTypeName;
                    case "_surveyReasonTypes": return _surveyReasonTypes;
                    case "_surveyQualityID": return _surveyQualityID;
                    case "_surveyQualityTypeName": return _surveyQualityTypeName;
                    case "_serviceTypeCode": return _serviceTypeCode;
                    case "_serviceTypeName": return _serviceTypeName;
                    case "_surveyStatusCode": return _surveyStatusCode;
                    case "_surveyStatusName": return _surveyStatusName;
                    case "_surveyStatusTypeCode": return _surveyStatusTypeCode;
                    case "_surveyStatusTypeName": return _surveyStatusTypeName;
                    case "_assignedUserID": return _assignedUserID;
                    case "_assignedUserName": return _assignedUserName;
                    case "_assignedUserManagerID": return _assignedUserManagerID;
                    case "_recommendedUserID": return _recommendedUserID;
                    case "_recommendedUserManagerID": return _recommendedUserManagerID;
                    case "_completedUserManagerID": return _completedUserManagerID;
                    case "_createStateCode": return _createStateCode;
                    case "_createDate": return _createDate;
                    case "_reviewerCompleteDate": return _reviewerCompleteDate;
                    case "_createByInternalUserID": return _createByInternalUserID;
                    case "_createByExternalUserName": return _createByExternalUserName;
                    case "_assignDate": return _assignDate;
                    case "_reassignDate": return _reassignDate;
                    case "_acceptDate": return _acceptDate;
                    case "_surveyedDate": return _surveyedDate;
                    case "_reportCompleteDate": return _reportCompleteDate;
                    case "_dueDate": return _dueDate;
                    case "_completeDate": return _completeDate;
                    case "_canceledDate": return _canceledDate;
                    case "_underwritingAcceptDate": return _underwritingAcceptDate;
                    case "_reminderDate": return _reminderDate;
                    case "_reminderComment": return _reminderComment;
                    case "_priority": return _priority;
                    case "_surveyHours": return _surveyHours;
                    case "_callCount": return _callCount;
                    case "_correction": return _correction;
                    case "_review": return _review;
                    case "_surveyGradingCode": return _surveyGradingCode;
                    case "_surveyGradingName": return _surveyGradingName;
                    case "_severityRatingCode": return _severityRatingCode;
                    case "_severityRatingName": return _severityRatingName;
                    case "_policyEffectiveDate": return _policyEffectiveDate;
                    case "_policyExpireDate": return _policyExpireDate;
                    case "_policyPremium": return _policyPremium;
                    case "_profitCenter": return _profitCenter;
                    case "_feeConsultantCost": return _feeConsultantCost;
                    case "_consultantUserID": return _consultantUserID;
                    case "_consultantUserName": return _consultantUserName;
                    case "_consultantIsFeeCompany": return _consultantIsFeeCompany;
                    case "_profitCenterByUser": return _profitCenterByUser;
                    case "_reviewerUserID": return _reviewerUserID;
                    case "_reviewerUserName": return _reviewerUserName;
                    case "_lastModifiedOn": return _lastModifiedOn;
                    case "_insuredID": return _insuredID;
                    case "_clientID": return _clientID;
                    case "_portfolioID": return _portfolioID;
                    case "_insuredName": return _insuredName;
                    case "_insuredName2": return _insuredName2;
                    case "_insuredStreetLine1": return _insuredStreetLine1;
                    case "_insuredStreetLine2": return _insuredStreetLine2;
                    case "_insuredStreetLine3": return _insuredStreetLine3;
                    case "_insuredCity": return _insuredCity;
                    case "_insuredStateCode": return _insuredStateCode;
                    case "_insuredZipCode": return _insuredZipCode;
                    case "_businessOperations": return _businessOperations;
                    case "_sicCode": return _sicCode;
                    case "_underwriter": return _underwriter;
                    case "_underwriterCode": return _underwriterCode;
                    case "_underwriter2Code": return _underwriter2Code;
                    case "_insuredNonRenewedDate": return _insuredNonRenewedDate;
                    case "_agencyNumber": return _agencyNumber;
                    case "_agencySubNumber": return _agencySubNumber;
                    case "_agencyName": return _agencyName;
                    case "_agencyStreetLine1": return _agencyStreetLine1;
                    case "_agencyStreetLine2": return _agencyStreetLine2;
                    case "_agencyCity": return _agencyCity;
                    case "_agencyStateCode": return _agencyStateCode;
                    case "_agencyZipCode": return _agencyZipCode;
                    case "_agencyPhoneNumber": return _agencyPhoneNumber;
                    case "_agencyFaxNumber": return _agencyFaxNumber;
                    case "_locationID": return _locationID;
                    case "_locationNumber": return _locationNumber;
                    case "_locationStreetLine1": return _locationStreetLine1;
                    case "_locationStreetLine2": return _locationStreetLine2;
                    case "_locationCity": return _locationCity;
                    case "_locationStateCode": return _locationStateCode;
                    case "_locationZipCode": return _locationZipCode;
                    case "_locationDescription": return _locationDescription;
                    case "_locationCount": return _locationCount;
                    case "_recCount": return _recCount;
                    case "_paceAccount": return _paceAccount;
                    case "_keyAccount": return _keyAccount;
                    case "_lossSensitive": return _lossSensitive;
                    case "_rmsRep": return _rmsRep;
                    case "_paceRep": return _paceRep;
                    case "_todaysDate": return _todaysDate;
                    case "_surveyLetters": return _surveyLetters;
                    case "_cwgDrivingTime": return _cwgDrivingTime;
                    case "_cwgSurveyTime": return _cwgSurveyTime;
                    case "_cwgReportWriteupTime": return _cwgReportWriteupTime;
                    case "_techConsultingTime": return _techConsultingTime;
                    case "_surveyReleaseOwnershipTypeID": return _surveyReleaseOwnershipTypeID;
                    case "_policyNumber": return _policyNumber;                    
                    case "_approvalCode": return _approvalCode;
                    case "_containsCriticalRec": return _containsCriticalRec;
                    case "_containsOpenCriticalRec": return _containsOpenCriticalRec;
                    case "_requestedDate": return _requestedDate;
                    case "_initialLetterCount": return _initialLetterCount;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_surveyID": _surveyID = (Guid)value; break;
                    case "_policySystemID": _policySystemID = (Guid)value; break;
                    case "_surveyNumber": _surveyNumber = (int)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_companyAbbreviation": _companyAbbreviation = (string)value; break;
                    case "_surveyTypeID": _surveyTypeID = (Guid)value; break;
                    case "_surveyTypeName": _surveyTypeName = (string)value; break;
                    case "_surveyReasonTypes": _surveyReasonTypes = (string)value; break;
                    case "_surveyQualityID": _surveyQualityID = (Guid)value; break;
                    case "_surveyQualityTypeName": _surveyQualityTypeName = (string)value; break;
                    case "_serviceTypeCode": _serviceTypeCode = (string)value; break;
                    case "_serviceTypeName": _serviceTypeName = (string)value; break;
                    case "_surveyStatusCode": _surveyStatusCode = (string)value; break;
                    case "_surveyStatusName": _surveyStatusName = (string)value; break;
                    case "_surveyStatusTypeCode": _surveyStatusTypeCode = (string)value; break;
                    case "_surveyStatusTypeName": _surveyStatusTypeName = (string)value; break;
                    case "_assignedUserID": _assignedUserID = (Guid)value; break;
                    case "_assignedUserName": _assignedUserName = (string)value; break;
                    case "_assignedUserManagerID": _assignedUserManagerID = (Guid)value; break;
                    case "_recommendedUserID": _recommendedUserID = (Guid)value; break;
                    case "_recommendedUserManagerID": _recommendedUserManagerID = (Guid)value; break;
                    case "_completedUserManagerID": _completedUserManagerID = (Guid)value; break;
                    case "_createStateCode": _createStateCode = (string)value; break;
                    case "_createDate": _createDate = (DateTime)value; break;
                    case "_reviewerCompleteDate": _reviewerCompleteDate = (DateTime)value; break;
                    case "_createByInternalUserID": _createByInternalUserID = (Guid)value; break;
                    case "_createByExternalUserName": _createByExternalUserName = (string)value; break;
                    case "_assignDate": _assignDate = (DateTime)value; break;
                    case "_reassignDate": _reassignDate = (DateTime)value; break;
                    case "_acceptDate": _acceptDate = (DateTime)value; break;
                    case "_surveyedDate": _surveyedDate = (DateTime)value; break;
                    case "_reportCompleteDate": _reportCompleteDate = (DateTime)value; break;
                    case "_dueDate": _dueDate = (DateTime)value; break;
                    case "_completeDate": _completeDate = (DateTime)value; break;
                    case "_canceledDate": _canceledDate = (DateTime)value; break;
                    case "_underwritingAcceptDate": _underwritingAcceptDate = (DateTime)value; break;
                    case "_reminderDate": _reminderDate = (DateTime)value; break;
                    case "_reminderComment": _reminderComment = (string)value; break;
                    case "_priority": _priority = (string)value; break;
                    case "_surveyHours": _surveyHours = (decimal)value; break;
                    case "_callCount": _callCount = (decimal)value; break;
                    case "_correction": _correction = (bool)value; break;
                    case "_review": _review = (bool)value; break;
                    case "_surveyGradingCode": _surveyGradingCode = (string)value; break;
                    case "_surveyGradingName": _surveyGradingName = (string)value; break;
                    case "_severityRatingCode": _severityRatingCode = (string)value; break;
                    case "_severityRatingName": _severityRatingName = (string)value; break;
                    case "_policyEffectiveDate": _policyEffectiveDate = (DateTime)value; break;
                    case "_policyExpireDate": _policyExpireDate = (DateTime)value; break;
                    case "_policyPremium": _policyPremium = (decimal)value; break;
                    case "_profitCenter": _profitCenter = (string)value; break;
                    case "_feeConsultantCost": _feeConsultantCost = (decimal)value; break;
                    case "_consultantUserID": _consultantUserID = (Guid)value; break;
                    case "_consultantUserName": _consultantUserName = (string)value; break;
                    case "_consultantIsFeeCompany": _consultantIsFeeCompany = (bool)value; break;
                    case "_profitCenterByUser": _profitCenterByUser = (string)value; break;
                    case "_reviewerUserID": _reviewerUserID = (Guid)value; break;
                    case "_reviewerUserName": _reviewerUserName = (string)value; break;
                    case "_lastModifiedOn": _lastModifiedOn = (DateTime)value; break;
                    case "_insuredID": _insuredID = (Guid)value; break;
                    case "_clientID": _clientID = (string)value; break;
                    case "_portfolioID": _portfolioID = (string)value; break;
                    case "_insuredName": _insuredName = (string)value; break;
                    case "_insuredName2": _insuredName2 = (string)value; break;
                    case "_insuredStreetLine1": _insuredStreetLine1 = (string)value; break;
                    case "_insuredStreetLine2": _insuredStreetLine2 = (string)value; break;
                    case "_insuredStreetLine3": _insuredStreetLine3 = (string)value; break;
                    case "_insuredCity": _insuredCity = (string)value; break;
                    case "_insuredStateCode": _insuredStateCode = (string)value; break;
                    case "_insuredZipCode": _insuredZipCode = (string)value; break;
                    case "_businessOperations": _businessOperations = (string)value; break;
                    case "_sicCode": _sicCode = (string)value; break;
                    case "_underwriter": _underwriter = (string)value; break;
                    case "_underwriterCode": _underwriterCode = (string)value; break;
                    case "_underwriter2Code": _underwriter2Code = (string)value; break;
                    case "_insuredNonRenewedDate": _insuredNonRenewedDate = (DateTime)value; break;
                    case "_agencyNumber": _agencyNumber = (string)value; break;
                    case "_agencySubNumber": _agencySubNumber = (string)value; break;
                    case "_agencyName": _agencyName = (string)value; break;
                    case "_agencyStreetLine1": _agencyStreetLine1 = (string)value; break;
                    case "_agencyStreetLine2": _agencyStreetLine2 = (string)value; break;
                    case "_agencyCity": _agencyCity = (string)value; break;
                    case "_agencyStateCode": _agencyStateCode = (string)value; break;
                    case "_agencyZipCode": _agencyZipCode = (string)value; break;
                    case "_agencyPhoneNumber": _agencyPhoneNumber = (string)value; break;
                    case "_agencyFaxNumber": _agencyFaxNumber = (string)value; break;
                    case "_locationID": _locationID = (Guid)value; break;
                    case "_locationNumber": _locationNumber = (int)value; break;
                    case "_locationStreetLine1": _locationStreetLine1 = (string)value; break;
                    case "_locationStreetLine2": _locationStreetLine2 = (string)value; break;
                    case "_locationCity": _locationCity = (string)value; break;
                    case "_locationStateCode": _locationStateCode = (string)value; break;
                    case "_locationZipCode": _locationZipCode = (string)value; break;
                    case "_locationDescription": _locationDescription = (string)value; break;
                    case "_locationCount": _locationCount = (int)value; break;
                    case "_recCount": _recCount = (string)value; break;
                    case "_paceAccount": _paceAccount = (string)value; break;
                    case "_keyAccount": _keyAccount = (string)value; break;
                    case "_lossSensitive": _lossSensitive = (string)value; break;
                    case "_rmsRep": _rmsRep = (string)value; break;
                    case "_paceRep": _paceRep = (string)value; break;
                    case "_todaysDate": _todaysDate = (DateTime)value; break;
                    case "_surveyLetters": _surveyLetters = (string)value; break;
                    case "_cwgDrivingTime": _cwgDrivingTime = (decimal)value; break;
                    case "_cwgSurveyTime": _cwgSurveyTime = (decimal)value; break;
                    case "_cwgReportWriteupTime": _cwgReportWriteupTime = (decimal)value; break;
                    case "_techConsultingTime": _techConsultingTime = (decimal)value; break;
                    case "_surveyReleaseOwnershipTypeID": _surveyReleaseOwnershipTypeID = (Guid)value; break;
                    case "_policyNumber": _policyNumber = (string)value; break;                    
                    case "_approvalCode": _approvalCode = (string)value; break;
                    case "_containsCriticalRec": _containsCriticalRec = (bool)value; break;
                    case "_containsOpenCriticalRec": _containsOpenCriticalRec = (bool)value; break;
                    case "_requestedDate": _requestedDate = (DateTime)value; break;
                    case "_initialLetterCount": _initialLetterCount = (int)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static VWSurvey Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(VWSurvey), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch VWSurvey entity with ID = '{0}'.", id));
            }
            return (VWSurvey)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static VWSurvey GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWSurvey), opathExpression);
            return (VWSurvey)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static VWSurvey GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(VWSurvey))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (VWSurvey)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWSurvey[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWSurvey), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWSurvey[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(VWSurvey))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (VWSurvey[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWSurvey[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWSurvey), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _surveyID = Guid.Empty;
            _policySystemID = Guid.Empty;
            _surveyNumber = Int32.MinValue;
            _companyID = Guid.Empty;
            _companyAbbreviation = null;
            _surveyTypeID = Guid.Empty;
            _surveyTypeName = null;
            _surveyReasonTypes = null;
            _surveyQualityID = Guid.Empty;
            _surveyQualityTypeName = null;
            _serviceTypeCode = null;
            _serviceTypeName = null;
            _surveyStatusCode = null;
            _surveyStatusName = null;
            _surveyStatusTypeCode = null;
            _surveyStatusTypeName = null;
            _assignedUserID = Guid.Empty;
            _assignedUserName = null;
            _assignedUserManagerID = Guid.Empty;
            _recommendedUserID = Guid.Empty;
            _recommendedUserManagerID = Guid.Empty;
            _completedUserManagerID = Guid.Empty;
            _createStateCode = null;
            _createDate = DateTime.MinValue;
            _reviewerCompleteDate = DateTime.MinValue;
            _createByInternalUserID = Guid.Empty;
            _createByExternalUserName = null;
            _assignDate = DateTime.MinValue;
            _reassignDate = DateTime.MinValue;
            _acceptDate = DateTime.MinValue;
            _surveyedDate = DateTime.MinValue;
            _reportCompleteDate = DateTime.MinValue;
            _dueDate = DateTime.MinValue;
            _completeDate = DateTime.MinValue;
            _canceledDate = DateTime.MinValue;
            _underwritingAcceptDate = DateTime.MinValue;
            _reminderDate = DateTime.MinValue;
            _reminderComment = null;
            _priority = null;
            _surveyHours = Decimal.MinValue;
            _callCount = Decimal.MinValue;
            _correction = false;
            _review = false;
            _surveyGradingCode = null;
            _surveyGradingName = null;
            _severityRatingCode = null;
            _severityRatingName = null;
            _policyEffectiveDate = DateTime.MinValue;
            _policyExpireDate = DateTime.MinValue;
            _policyPremium = Decimal.MinValue;
            _profitCenter = null;
            _feeConsultantCost = Decimal.MinValue;
            _consultantUserID = Guid.Empty;
            _consultantUserName = null;
            _consultantIsFeeCompany = false;
            _profitCenterByUser = null;
            _reviewerUserID = Guid.Empty;
            _reviewerUserName = null;
            _lastModifiedOn = DateTime.MinValue;
            _insuredID = Guid.Empty;
            _clientID = null;
            _portfolioID = null;
            _insuredName = null;
            _insuredName2 = null;
            _insuredStreetLine1 = null;
            _insuredStreetLine2 = null;
            _insuredStreetLine3 = null;
            _insuredCity = null;
            _insuredStateCode = null;
            _insuredZipCode = null;
            _businessOperations = null;
            _sicCode = null;
            _underwriter = null;
            _underwriterCode = null;
            _underwriter2Code = null;
            _insuredNonRenewedDate = DateTime.MinValue;
            _agencyNumber = null;
            _agencySubNumber = null;
            _agencyName = null;
            _agencyStreetLine1 = null;
            _agencyStreetLine2 = null;
            _agencyCity = null;
            _agencyStateCode = null;
            _agencyZipCode = null;
            _agencyPhoneNumber = null;
            _agencyFaxNumber = null;
            _locationID = Guid.Empty;
            _locationNumber = Int32.MinValue;
            _locationStreetLine1 = null;
            _locationStreetLine2 = null;
            _locationCity = null;
            _locationStateCode = null;
            _locationZipCode = null;
            _locationDescription = null;
            _locationCount = Int32.MinValue;
            _recCount = null;
            _paceAccount = null;
            _keyAccount = null;
            _lossSensitive = null;
            _rmsRep = null;
            _paceRep = null;
            _todaysDate = DateTime.MinValue;
            _surveyLetters = null;
            _cwgDrivingTime = Decimal.MinValue;
            _cwgSurveyTime = Decimal.MinValue;
            _cwgReportWriteupTime = Decimal.MinValue;
            _techConsultingTime = Decimal.MinValue;
            _surveyReleaseOwnershipTypeID = Guid.Empty;
            _policyNumber = null;            
            _approvalCode = null;
            _containsCriticalRec = false;
            _containsOpenCriticalRec = false;
            _requestedDate = DateTime.MinValue;
            _initialLetterCount = Int32.MinValue;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the VWSurvey entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the SurveyID field.
            /// </summary>
            SurveyID,
            /// <summary>
            /// Represents the SurveyID field.
            /// </summary>
            PolicySystemID,
            /// <summary>
            /// Represents the SurveyNumber field.
            /// </summary>
            SurveyNumber,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the CompanyAbbreviation field.
            /// </summary>
            CompanyAbbreviation,
            /// <summary>
            /// Represents the SurveyTypeID field.
            /// </summary>
            SurveyTypeID,
            /// <summary>
            /// Represents the SurveyTypeName field.
            /// </summary>
            SurveyTypeName,
            /// <summary>
            /// Represents the SurveyReasonTypes field.
            /// </summary>
            SurveyReasonTypes,
            /// <summary>
            /// Represents the SurveyQualityID field.
            /// </summary>
            SurveyQualityID,
            /// <summary>
            /// Represents the SurveyQualityTypeName field.
            /// </summary>
            SurveyQualityTypeName,
            /// <summary>
            /// Represents the ServiceTypeCode field.
            /// </summary>
            ServiceTypeCode,
            /// <summary>
            /// Represents the ServiceTypeName field.
            /// </summary>
            ServiceTypeName,
            /// <summary>
            /// Represents the SurveyStatusCode field.
            /// </summary>
            SurveyStatusCode,
            /// <summary>
            /// Represents the SurveyStatusName field.
            /// </summary>
            SurveyStatusName,
            /// <summary>
            /// Represents the SurveyStatusTypeCode field.
            /// </summary>
            SurveyStatusTypeCode,
            /// <summary>
            /// Represents the SurveyStatusTypeName field.
            /// </summary>
            SurveyStatusTypeName,
            /// <summary>
            /// Represents the AssignedUserID field.
            /// </summary>
            AssignedUserID,
            /// <summary>
            /// Represents the AssignedUserName field.
            /// </summary>
            AssignedUserName,
            /// <summary>
            /// Represents the AssignedUserManagerID field.
            /// </summary>
            AssignedUserManagerID,
            /// <summary>
            /// Represents the RecommendedUserID field.
            /// </summary>
            RecommendedUserID,
            /// <summary>
            /// Represents the RecommendedUserManagerID field.
            /// </summary>
            RecommendedUserManagerID,
            /// <summary>
            /// Represents the CompletedUserManagerID field.
            /// </summary>
            CompletedUserManagerID,
            /// <summary>
            /// Represents the CreateStateCode field.
            /// </summary>
            CreateStateCode,
            /// <summary>
            /// Represents the CreateDate field.
            /// </summary>
            CreateDate,
            /// <summary>
            /// Represents the CreateByInternalUserID field.
            /// </summary>
            CreateByInternalUserID,
            /// <summary>
            /// Represents the CreateByExternalUserName field.
            /// </summary>
            CreateByExternalUserName,
            /// <summary>
            /// Represents the AssignDate field.
            /// </summary>
            AssignDate,
            /// <summary>
            /// Represents the ReassignDate field.
            /// </summary>
            ReassignDate,
            /// <summary>
            /// Represents the AcceptDate field.
            /// </summary>
            AcceptDate,
            /// <summary>
            /// Represents the SurveyedDate field.
            /// </summary>
            SurveyedDate,
            /// <summary>
            /// Represents the ReportCompleteDate field.
            /// </summary>
            ReportCompleteDate,
            /// <summary>
            /// Represents the DueDate field.
            /// </summary>
            DueDate,
            /// <summary>
            /// Represents the CompleteDate field.
            /// </summary>
            CompleteDate,
            /// <summary>
            /// Represents the CanceledDate field.
            /// </summary>
            CanceledDate,
            /// <summary>
            /// Represents the UnderwritingAcceptDate field.
            /// </summary>
            UnderwritingAcceptDate,
            /// <summary>
            /// Represents the ReminderDate field.
            /// </summary>
            ReminderDate,
            /// <summary>
            /// Represents the ReminderComment field.
            /// </summary>
            ReminderComment,
            /// <summary>
            /// Represents the Priority field.
            /// </summary>
            Priority,
            /// <summary>
            /// Represents the SurveyHours field.
            /// </summary>
            SurveyHours,
            /// <summary>
            /// Represents the CallCount field.
            /// </summary>
            CallCount,
            /// <summary>
            /// Represents the Correction field.
            /// </summary>
            Correction,
            /// <summary>
            /// Represents the Review field.
            /// </summary>
            Review,
            /// <summary>
            /// Represents the SurveyGradingCode field.
            /// </summary>
            SurveyGradingCode,
            /// <summary>
            /// Represents the SurveyGradingName field.
            /// </summary>
            SurveyGradingName,
            /// <summary>
            /// Represents the SeverityRatingCode field.
            /// </summary>
            SeverityRatingCode,
            /// <summary>
            /// Represents the SeverityRatingName field.
            /// </summary>
            SeverityRatingName,
            /// <summary>
            /// Represents the PolicyEffectiveDate field.
            /// </summary>
            PolicyEffectiveDate,
            /// <summary>
            /// Represents the PolicyExpireDate field.
            /// </summary>
            PolicyExpireDate,
            /// <summary>
            /// Represents the PolicyPremium field.
            /// </summary>
            PolicyPremium,
            /// <summary>
            /// Represents the ProfitCenter field.
            /// </summary>
            ProfitCenter,
            /// <summary>
            /// Represents the FeeConsultantCost field.
            /// </summary>
            FeeConsultantCost,
            /// <summary>
            /// Represents the ConsultantUserID field.
            /// </summary>
            ConsultantUserID,
            /// <summary>
            /// Represents the ConsultantUserName field.
            /// </summary>
            ConsultantUserName,
            /// <summary>
            /// Represents the ConsultantIsFeeCompany field.
            /// </summary>
            ConsultantIsFeeCompany,
            /// <summary>
            /// Represents the ProfitCenterByUser field.
            /// </summary>
            ProfitCenterByUser,
            /// <summary>
            /// Represents the ReviewerUserID field.
            /// </summary>
            ReviewerUserID,
            /// <summary>
            /// Represents the ReviewerUserName field.
            /// </summary>
            ReviewerUserName,
            /// <summary>
            /// Represents the LastModifiedOn field.
            /// </summary>
            LastModifiedOn,
            /// <summary>
            /// Represents the InsuredID field.
            /// </summary>
            InsuredID,
            /// <summary>
            /// Represents the ClientID field.
            /// </summary>
            ClientID,
            /// <summary>
            /// Represents the PortfolioID field.
            /// </summary>
            PortfolioID,
            /// <summary>
            /// Represents the InsuredName field.
            /// </summary>
            InsuredName,
            /// <summary>
            /// Represents the InsuredName2 field.
            /// </summary>
            InsuredName2,
            /// <summary>
            /// Represents the InsuredStreetLine1 field.
            /// </summary>
            InsuredStreetLine1,
            /// <summary>
            /// Represents the InsuredStreetLine2 field.
            /// </summary>
            InsuredStreetLine2,
            /// <summary>
            /// Represents the InsuredStreetLine3 field.
            /// </summary>
            InsuredStreetLine3,
            /// <summary>
            /// Represents the InsuredCity field.
            /// </summary>
            InsuredCity,
            /// <summary>
            /// Represents the InsuredStateCode field.
            /// </summary>
            InsuredStateCode,
            /// <summary>
            /// Represents the InsuredZipCode field.
            /// </summary>
            InsuredZipCode,
            /// <summary>
            /// Represents the BusinessOperations field.
            /// </summary>
            BusinessOperations,
            /// <summary>
            /// Represents the SICCode field.
            /// </summary>
            SICCode,
            /// <summary>
            /// Represents the Underwriter field.
            /// </summary>
            Underwriter,
            /// <summary>
            /// Represents the UnderwriterCode field.
            /// </summary>
            UnderwriterCode,
            /// Represents the UnderwriterCode field.
            /// </summary>
            Underwriter2Code,
            /// <summary>
            /// Represents the InsuredNonRenewedDate field.
            /// </summary>
            InsuredNonRenewedDate,
            /// <summary>
            /// Represents the AgencyNumber field.
            /// </summary>
            AgencyNumber,
            /// <summary>
            /// Represents the AgencySubNumber field.
            /// </summary>
            AgencySubNumber,
            /// <summary>
            /// Represents the AgencyName field.
            /// </summary>
            AgencyName,
            /// <summary>
            /// Represents the AgencyStreetLine1 field.
            /// </summary>
            AgencyStreetLine1,
            /// <summary>
            /// Represents the AgencyStreetLine2 field.
            /// </summary>
            AgencyStreetLine2,
            /// <summary>
            /// Represents the AgencyCity field.
            /// </summary>
            AgencyCity,
            /// <summary>
            /// Represents the AgencyStateCode field.
            /// </summary>
            AgencyStateCode,
            /// <summary>
            /// Represents the AgencyZipCode field.
            /// </summary>
            AgencyZipCode,
            /// <summary>
            /// Represents the AgencyPhoneNumber field.
            /// </summary>
            AgencyPhoneNumber,
            /// <summary>
            /// Represents the AgencyFaxNumber field.
            /// </summary>
            AgencyFaxNumber,
            /// <summary>
            /// Represents the LocationID field.
            /// </summary>
            LocationID,
            /// <summary>
            /// Represents the LocationNumber field.
            /// </summary>
            LocationNumber,
            /// <summary>
            /// Represents the LocationStreetLine1 field.
            /// </summary>
            LocationStreetLine1,
            /// <summary>
            /// Represents the LocationStreetLine2 field.
            /// </summary>
            LocationStreetLine2,
            /// <summary>
            /// Represents the LocationCity field.
            /// </summary>
            LocationCity,
            /// <summary>
            /// Represents the LocationStateCode field.
            /// </summary>
            LocationStateCode,
            /// <summary>
            /// Represents the LocationZipCode field.
            /// </summary>
            LocationZipCode,
            /// <summary>
            /// Represents the LocationDescription field.
            /// </summary>
            LocationDescription,
            /// <summary>
            /// Represents the LocationCount field.
            /// </summary>
            LocationCount,
            /// <summary>
            /// Represents the RecCount field.
            /// </summary>
            RecCount,
            /// <summary>
            /// Represents the PaceAccount field.
            /// </summary>
            PaceAccount,
            /// <summary>
            /// Represents the KeyAccount field.
            /// </summary>
            KeyAccount,
            /// <summary>
            /// Represents the LossSensitive field.
            /// </summary>
            LossSensitive,
            /// <summary>
            /// Represents the RMSRep field.
            /// </summary>
            RMSRep,
            /// <summary>
            /// Represents the PaceRep field.
            /// </summary>
            PaceRep,
            /// <summary>
            /// Represents the TodaysDate field.
            /// </summary>
            TodaysDate,
            /// <summary>
            /// Represents the SurveyLetters field.
            /// </summary>
            SurveyLetters,
            /// <summary>
            /// Represents the CWGDrivingTime field.
            /// </summary>
            CWGDrivingTime,
            /// <summary>
            /// Represents the CWGSurveyTime field.
            /// </summary>
            CWGSurveyTime,
            /// <summary>
            /// Represents the CWGReportWriteupTime field.
            /// </summary>
            CWGReportWriteupTime,
            /// Represents the TechConsultingTime field.
            /// </summary>
            TechConsultingTime,
            /// <summary>
            /// Represents the SurveyReleaseOwnershipTypeID field.
            /// </summary>
            SurveyReleaseOwnershipTypeID,
            /// <summary>
            /// Represents the PolicyNumber field.
            /// </summary>
            PolicyNumber,            
            /// <summary>
            /// Represents the TechCompleteDate field.
            /// </summary>
            ReviewerCompleteDate,
            /// <summary>
            /// Represents the ApprovalCode field.
            /// </summary>
            ApprovalCode,
            /// <summary>
            /// Represents the ContainsCriticalRec field.
            /// </summary>
            ContainsCriticalRec,
            /// Represents the ContainsCriticalRec field.
            /// </summary>
            ContainsOpenCriticalRec,
            /// <summary>
            /// Represents the RequestedDate field.
            /// </summary>
            RequestedDate,
            /// <summary>
            /// Represents the InitialLetter field.
            /// </summary>
            InitialLetterCount,
        }

        #endregion
    }
}