using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyTypeType entity, which maps to table 'SurveyTypeType' in the database.
	/// </summary>
	public class SurveyTypeType : IObjectHelper
	{
        #region Public Fields

        /// <summary>
        /// Represents an Agency Call survey type.
        /// </summary>
        public static readonly SurveyTypeType AgencyCall = SurveyTypeType.Get("AG");
        /// <summary>
        /// Represents a Consultative survey type.
        /// </summary>
        public static readonly SurveyTypeType Consultative = SurveyTypeType.Get("CO");
        /// <summary>
        /// Represents a New Business survey type.
        /// </summary>
        public static readonly SurveyTypeType NewBusiness = SurveyTypeType.Get("NB");
        /// <summary>
        /// Represents a New Business Bound survey type.
        /// </summary>
        public static readonly SurveyTypeType NewBusinessBound = SurveyTypeType.Get("ND");
        /// <summary>
        /// Represents a New Business Issued survey type.
        /// </summary>
        public static readonly SurveyTypeType NewBusinessIssued = SurveyTypeType.Get("NI");
        /// <summary>
        /// Represents a Phone survey type.
        /// </summary>
        public static readonly SurveyTypeType Phone = SurveyTypeType.Get("PH");
        /// <summary>
        /// Represents a Prospect survey type.
        /// </summary>
        public static readonly SurveyTypeType Prospect = SurveyTypeType.Get("PR");
        /// <summary>
        /// Represents a Rec Check survey type.
        /// </summary>
        public static readonly SurveyTypeType RecCheck = SurveyTypeType.Get("RC");
        /// <summary>
        /// Represents a Resurvey survey type.
        /// </summary>
        public static readonly SurveyTypeType Resurvey = SurveyTypeType.Get("RE");
        /// <summary>
        /// Represents a Renewal survey type.
        /// </summary>
        public static readonly SurveyTypeType Renewal = SurveyTypeType.Get("RN");
        /// <summary>
        /// Represents a Service survey type.
        /// </summary>
        public static readonly SurveyTypeType Service = SurveyTypeType.Get("SE");
        /// <summary>
        /// Represents a Training survey type.
        /// </summary>
        public static readonly SurveyTypeType Training = SurveyTypeType.Get("TR");
        /// <summary>
        /// Represents a Safety Committee Training survey type.
        /// </summary>
        public static readonly SurveyTypeType SafetyCommitteeTraining = SurveyTypeType.Get("ST");
        /// <summary>
        /// Represents an U/W Request survey type.
        /// </summary>
        public static readonly SurveyTypeType UWRequest = SurveyTypeType.Get("UW");
        /// <summary>
        /// Represents a Prospect Written survey type.
        /// </summary>
        public static readonly SurveyTypeType ProspectWritten = SurveyTypeType.Get("PW");
        /// <summary>
        /// Represents an Agency Call from a submission.
        /// </summary>
        public static readonly SurveyTypeType AgencyCallFromSubmission = SurveyTypeType.Get("AS");
        /// <summary>
        /// Represents an Quote survey type.
        /// </summary>
        public static readonly SurveyTypeType Quote = SurveyTypeType.Get("QT");
        /// <summary>
        /// Represents an Quote survey type.
        /// </summary>
        public static readonly SurveyTypeType WrittenBusiness = SurveyTypeType.Get("WB");
        /// <summary>
        /// Represents an PrePolicy survey type.
        /// </summary>
        public static readonly SurveyTypeType PrePolicy = SurveyTypeType.Get("PP");
        /// <summary>
        /// Represents a quote new business survey type.
        /// </summary>
        public static readonly SurveyTypeType QuoteNewBusiness = SurveyTypeType.Get("QN");
        
        #endregion
        
        /// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyTypeType()
		{
		}

        /// <summary>
        /// Determines if the survey type is not written business.
        /// </summary>
        public static bool IsNotWrittenBusiness(SurveyTypeType type)
        {
            return (type == SurveyTypeType.NewBusinessBound || 
                type == SurveyTypeType.Prospect || 
                type == SurveyTypeType.AgencyCallFromSubmission || 
                type == SurveyTypeType.Quote ||
                type == SurveyTypeType.PrePolicy ||
                type == SurveyTypeType.QuoteNewBusiness);
        }

		#region --- Generated Members ---

		private string _code;
		private string _name;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Survey Type Type.
		/// </summary>
		public string Code
		{
			get { return _code; }
		}

		/// <summary>
		/// Gets the Survey Type Type.
		/// </summary>
		public string Name
		{
			get { return _name; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_code": return _code;
					case "_name": return _name;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_code": _code = (string)value; break;
					case "_name": _name = (string)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="code">Code of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static SurveyTypeType Get(string code)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyTypeType), "Code = ?");
			object entity = DB.Engine.GetObject(query, code);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch SurveyTypeType entity with Code = '{0}'.", code));
			}
			return (SurveyTypeType)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyTypeType GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyTypeType), opathExpression);
			return (SurveyTypeType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyTypeType GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyTypeType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyTypeType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyTypeType[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyTypeType), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyTypeType[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyTypeType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyTypeType[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyTypeType[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyTypeType), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Determines if two SurveyTypeType objects have the same semantic value.
		/// </summary>
		/// <param name="one">The first object to compare.</param>
		/// <param name="two">The second object to compare.</param>
		/// <returns>True if they are semantically identical, false otherwise.</returns>
		public static bool operator == (SurveyTypeType one, SurveyTypeType two) 
		{
			if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
			{
				return true;
			}
			else if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
			{
				return false;
			}
			else
			{
				return (one.CompareTo(two) == 0);
			}
		}

		/// <summary>
		/// Determines if two SurveyTypeType objects have different semantic values.
		/// </summary>
		/// <param name="one">The first object to compare.</param>
		/// <param name="two">The second object to compare.</param>
		/// <returns>True if they are semantically dissimilar, false otherwise.</returns>
		public static bool operator != (SurveyTypeType one, SurveyTypeType two) 
		{
			if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
			{
				return false;
			}
			if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
			{
				return true;
			}
			else
			{
				return (one.CompareTo(two) != 0);
			}
		}
		
		/// <summary>
		/// Compares the current instance with another object of the same type.
		/// </summary>
		/// <param name="value">An object to compare with this instance.</param>
		/// <returns>Less than zero if this instance is less than value. 
		/// Zero if this instance is equal to value.
		/// Greater than zero if this instance is greater than value.</returns>
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (value is SurveyTypeType)
			{
				SurveyTypeType obj = (SurveyTypeType)value;
				return (this.Code.CompareTo(obj.Code));
			}
			throw new ArgumentException("Value is not the correct type.");
		}

		/// <summary>
		/// Returns the hash code for this instance.
		/// </summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		public override int GetHashCode()
		{
			return this.Code.GetHashCode();
		}

		/// <summary>
		/// Determines whether this instance is equal to the specified object.
		/// </summary>
		/// <param name="value">The object to compare with the current instance. </param>
		/// <returns>true if the specified object is equal to the current instance; otherwise, false.</returns>
		public override bool Equals(object value)
		{
			return (this == (value as SurveyTypeType));
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_code = null;
			_name = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the SurveyTypeType entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the Code field.
			/// </summary>
			Code,
			/// <summary>
			/// Represents the Name field.
			/// </summary>
			Name,
		}
		
		#endregion
	}
}