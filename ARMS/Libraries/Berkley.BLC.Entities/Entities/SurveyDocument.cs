using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyDocument entity, which maps to table 'SurveyDocument' in the database.
	/// </summary>
	public class SurveyDocument : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyDocument()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this SurveyDocument.</param>
		public SurveyDocument(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


        #region --- Generated Members ---

        private Guid _id;
        private Guid _surveyID;
        private string _documentName;
        private string _mimeType;
        private long _sizeInBytes;
        private string _fileNetReference;
        private DateTime _uploadedOn;
        private Guid _uploadUserID = Guid.Empty;
        private string _uploadUserName = String.Empty;
        private string _docTypeCode = String.Empty;
        private bool _fileNetRemovable;
        private bool _isRemoved;
        private ObjectHolder _surveyHolder = null;
        private ObjectHolder _uploadUserHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Survey Document ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Survey.
        /// </summary>
        public Guid SurveyID
        {
            get { return _surveyID; }
            set
            {
                VerifyWritable();
                _surveyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Document Name.
        /// </summary>
        public string DocumentName
        {
            get { return _documentName; }
            set
            {
                VerifyWritable();
                _documentName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Mime Type.
        /// </summary>
        public string MimeType
        {
            get { return _mimeType; }
            set
            {
                VerifyWritable();
                _mimeType = value;
            }
        }

        /// <summary>
        /// Gets or sets the Size In Bytes.
        /// </summary>
        public long SizeInBytes
        {
            get { return _sizeInBytes; }
            set
            {
                VerifyWritable();
                _sizeInBytes = value;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Reference.
        /// </summary>
        public string FileNetReference
        {
            get { return _fileNetReference; }
            set
            {
                VerifyWritable();
                _fileNetReference = value;
            }
        }

        /// <summary>
        /// Gets or sets the Uploaded On.
        /// </summary>
        public DateTime UploadedOn
        {
            get { return _uploadedOn; }
            set
            {
                VerifyWritable();
                _uploadedOn = value;
            }
        }

        /// <summary>
        /// Gets or sets the Upload User. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid UploadUserID
        {
            get { return _uploadUserID; }
            set
            {
                VerifyWritable();
                _uploadUserID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Upload User Name. Null value is 'String.Empty'.
        /// </summary>
        public string UploadUserName
        {
            get { return _uploadUserName; }
            set
            {
                VerifyWritable();
                _uploadUserName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Doc Type Code. Null value is 'String.Empty'.
        /// </summary>
        public string DocTypeCode
        {
            get { return _docTypeCode; }
            set
            {
                VerifyWritable();
                _docTypeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Removable.
        /// </summary>
        public bool FileNetRemovable
        {
            get { return _fileNetRemovable; }
            set
            {
                VerifyWritable();
                _fileNetRemovable = value;
            }
        }

        /// <summary>
        /// Gets or sets the Is Removed.
        /// </summary>
        public bool IsRemoved
        {
            get { return _isRemoved; }
            set
            {
                VerifyWritable();
                _isRemoved = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Survey object related to this entity.
        /// </summary>
        public Survey Survey
        {
            get
            {
                _surveyHolder.Key = _surveyID;
                return (Survey)_surveyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User UploadUser
        {
            get
            {
                _uploadUserHolder.Key = _uploadUserID;
                return (User)_uploadUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_surveyID": return _surveyID;
                    case "_documentName": return _documentName;
                    case "_mimeType": return _mimeType;
                    case "_sizeInBytes": return _sizeInBytes;
                    case "_fileNetReference": return _fileNetReference;
                    case "_uploadedOn": return _uploadedOn;
                    case "_uploadUserID": return _uploadUserID;
                    case "_uploadUserName": return _uploadUserName;
                    case "_docTypeCode": return _docTypeCode;
                    case "_fileNetRemovable": return _fileNetRemovable;
                    case "_isRemoved": return _isRemoved;
                    case "_surveyHolder": return _surveyHolder;
                    case "_uploadUserHolder": return _uploadUserHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_surveyID": _surveyID = (Guid)value; break;
                    case "_documentName": _documentName = (string)value; break;
                    case "_mimeType": _mimeType = (string)value; break;
                    case "_sizeInBytes": _sizeInBytes = (long)value; break;
                    case "_fileNetReference": _fileNetReference = (string)value; break;
                    case "_uploadedOn": _uploadedOn = (DateTime)value; break;
                    case "_uploadUserID": _uploadUserID = (Guid)value; break;
                    case "_uploadUserName": _uploadUserName = (string)value; break;
                    case "_docTypeCode": _docTypeCode = (string)value; break;
                    case "_fileNetRemovable": _fileNetRemovable = (bool)value; break;
                    case "_isRemoved": _isRemoved = (bool)value; break;
                    case "_surveyHolder": _surveyHolder = (ObjectHolder)value; break;
                    case "_uploadUserHolder": _uploadUserHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static SurveyDocument Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyDocument), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch SurveyDocument entity with ID = '{0}'.", id));
            }
            return (SurveyDocument)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyDocument GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyDocument), opathExpression);
            return (SurveyDocument)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyDocument GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyDocument))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyDocument)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyDocument[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyDocument), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyDocument[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyDocument))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyDocument[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyDocument[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyDocument), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public SurveyDocument GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            SurveyDocument clone = (SurveyDocument)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.SurveyID, _surveyID);
            Validator.Validate(Field.DocumentName, _documentName);
            Validator.Validate(Field.MimeType, _mimeType);
            Validator.Validate(Field.SizeInBytes, _sizeInBytes);
            Validator.Validate(Field.FileNetReference, _fileNetReference);
            Validator.Validate(Field.UploadedOn, _uploadedOn);
            Validator.Validate(Field.UploadUserID, _uploadUserID);
            Validator.Validate(Field.UploadUserName, _uploadUserName);
            Validator.Validate(Field.DocTypeCode, _docTypeCode);
            Validator.Validate(Field.FileNetRemovable, _fileNetRemovable);
            Validator.Validate(Field.IsRemoved, _isRemoved);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _surveyID = Guid.Empty;
            _documentName = null;
            _mimeType = null;
            _sizeInBytes = Int64.MinValue;
            _fileNetReference = null;
            _uploadedOn = DateTime.MinValue;
            _uploadUserID = Guid.Empty;
            _uploadUserName = null;
            _docTypeCode = null;
            _fileNetRemovable = false;
            _isRemoved = false;
            _surveyHolder = null;
            _uploadUserHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the SurveyDocument entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the SurveyID field.
            /// </summary>
            SurveyID,
            /// <summary>
            /// Represents the DocumentName field.
            /// </summary>
            DocumentName,
            /// <summary>
            /// Represents the MimeType field.
            /// </summary>
            MimeType,
            /// <summary>
            /// Represents the SizeInBytes field.
            /// </summary>
            SizeInBytes,
            /// <summary>
            /// Represents the FileNetReference field.
            /// </summary>
            FileNetReference,
            /// <summary>
            /// Represents the UploadedOn field.
            /// </summary>
            UploadedOn,
            /// <summary>
            /// Represents the UploadUserID field.
            /// </summary>
            UploadUserID,
            /// <summary>
            /// Represents the UploadUserName field.
            /// </summary>
            UploadUserName,
            /// <summary>
            /// Represents the DocTypeCode field.
            /// </summary>
            DocTypeCode,
            /// <summary>
            /// Represents the FileNetRemovable field.
            /// </summary>
            FileNetRemovable,
            /// <summary>
            /// Represents the IsRemoved field.
            /// </summary>
            IsRemoved,
        }

        #endregion
    }
}