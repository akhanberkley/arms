using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a SurveyLocationPolicyExclusion entity, which maps to table 'Survey_LocationPolicyExclusion' in the database.
    /// </summary>
    public class SurveyLocationPolicyExclusion : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private SurveyLocationPolicyExclusion()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="surveyID">SurveyID of this SurveyLocationPolicyExclusion.</param>
        /// <param name="locationID">LocationID of this SurveyLocationPolicyExclusion.</param>
        /// <param name="policyID">PolicyID of this SurveyLocationPolicyExclusion.</param>
        public SurveyLocationPolicyExclusion(Guid surveyID, Guid locationID, Guid policyID)
        {
            _surveyID = surveyID;
            _locationID = locationID;
            _policyID = policyID;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _surveyID;
        private Guid _locationID;
        private Guid _policyID;
        private bool _locationExcluded;
        private bool _policyExcluded;
        private bool _policyViewed;
        private ObjectHolder _locationHolder = null;
        private ObjectHolder _policyHolder = null;
        private ObjectHolder _surveyHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Survey.
        /// </summary>
        public Guid SurveyID
        {
            get { return _surveyID; }
        }

        /// <summary>
        /// Gets the Location.
        /// </summary>
        public Guid LocationID
        {
            get { return _locationID; }
        }

        /// <summary>
        /// Gets the Policy.
        /// </summary>
        public Guid PolicyID
        {
            get { return _policyID; }
        }

        /// <summary>
        /// Gets or sets the Location Excluded.
        /// </summary>
        public bool LocationExcluded
        {
            get { return _locationExcluded; }
            set
            {
                VerifyWritable();
                _locationExcluded = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Excluded.
        /// </summary>
        public bool PolicyExcluded
        {
            get { return _policyExcluded; }
            set
            {
                VerifyWritable();
                _policyExcluded = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Viewed.
        /// </summary>
        public bool PolicyViewed
        {
            get { return _policyViewed; }
            set
            {
                VerifyWritable();
                _policyViewed = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Location object related to this entity.
        /// </summary>
        public Location Location
        {
            get
            {
                _locationHolder.Key = _locationID;
                return (Location)_locationHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Policy object related to this entity.
        /// </summary>
        public Policy Policy
        {
            get
            {
                _policyHolder.Key = _policyID;
                return (Policy)_policyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Survey object related to this entity.
        /// </summary>
        public Survey Survey
        {
            get
            {
                _surveyHolder.Key = _surveyID;
                return (Survey)_surveyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_surveyID": return _surveyID;
                    case "_locationID": return _locationID;
                    case "_policyID": return _policyID;
                    case "_locationExcluded": return _locationExcluded;
                    case "_policyExcluded": return _policyExcluded;
                    case "_policyViewed": return _policyViewed;
                    case "_locationHolder": return _locationHolder;
                    case "_policyHolder": return _policyHolder;
                    case "_surveyHolder": return _surveyHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_surveyID": _surveyID = (Guid)value; break;
                    case "_locationID": _locationID = (Guid)value; break;
                    case "_policyID": _policyID = (Guid)value; break;
                    case "_locationExcluded": _locationExcluded = (bool)value; break;
                    case "_policyExcluded": _policyExcluded = (bool)value; break;
                    case "_policyViewed": _policyViewed = (bool)value; break;
                    case "_locationHolder": _locationHolder = (ObjectHolder)value; break;
                    case "_policyHolder": _policyHolder = (ObjectHolder)value; break;
                    case "_surveyHolder": _surveyHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="surveyID">SurveyID of the entity to fetch.</param>
        /// <param name="locationID">LocationID of the entity to fetch.</param>
        /// <param name="policyID">PolicyID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static SurveyLocationPolicyExclusion Get(Guid surveyID, Guid locationID, Guid policyID)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyLocationPolicyExclusion), "SurveyID = ? && LocationID = ? && PolicyID = ?");
            object entity = DB.Engine.GetObject(query, surveyID, locationID, policyID);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch SurveyLocationPolicyExclusion entity with SurveyID = '{0}' and LocationID = '{1}' and PolicyID = '{2}'.", surveyID, locationID, policyID));
            }
            return (SurveyLocationPolicyExclusion)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyLocationPolicyExclusion GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyLocationPolicyExclusion), opathExpression);
            return (SurveyLocationPolicyExclusion)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyLocationPolicyExclusion GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyLocationPolicyExclusion))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyLocationPolicyExclusion)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyLocationPolicyExclusion[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyLocationPolicyExclusion), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyLocationPolicyExclusion[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyLocationPolicyExclusion))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyLocationPolicyExclusion[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyLocationPolicyExclusion[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyLocationPolicyExclusion), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public SurveyLocationPolicyExclusion GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            SurveyLocationPolicyExclusion clone = (SurveyLocationPolicyExclusion)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.SurveyID, _surveyID);
            Validator.Validate(Field.LocationID, _locationID);
            Validator.Validate(Field.PolicyID, _policyID);
            Validator.Validate(Field.LocationExcluded, _locationExcluded);
            Validator.Validate(Field.PolicyExcluded, _policyExcluded);
            Validator.Validate(Field.PolicyViewed, _policyViewed);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _surveyID = Guid.Empty;
            _locationID = Guid.Empty;
            _policyID = Guid.Empty;
            _locationExcluded = false;
            _policyExcluded = false;
            _policyViewed = false;
            _locationHolder = null;
            _policyHolder = null;
            _surveyHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the SurveyLocationPolicyExclusion entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the SurveyID field.
            /// </summary>
            SurveyID,
            /// <summary>
            /// Represents the LocationID field.
            /// </summary>
            LocationID,
            /// <summary>
            /// Represents the PolicyID field.
            /// </summary>
            PolicyID,
            /// <summary>
            /// Represents the LocationExcluded field.
            /// </summary>
            LocationExcluded,
            /// <summary>
            /// Represents the PolicyExcluded field.
            /// </summary>
            PolicyExcluded,
            /// <summary>
            /// Represents the PolicyViewed field.
            /// </summary>
            PolicyViewed,
        }

        #endregion
    }
}