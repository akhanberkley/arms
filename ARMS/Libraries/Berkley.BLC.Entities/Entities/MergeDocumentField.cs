using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a MergeDocumentField entity, which maps to table 'MergeDocumentField' in the database.
	/// </summary>
	public class MergeDocumentField : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private MergeDocumentField()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="mergeDocumentID">MergeDocumentID of this MergeDocumentField.</param>
		/// <param name="fieldName">FieldName of this MergeDocumentField.</param>
		public MergeDocumentField(Guid mergeDocumentID, string fieldName)
		{
			_mergeDocumentID = mergeDocumentID;
			_fieldName = fieldName;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


        #region --- Generated Members ---

        private Guid _mergeDocumentID;
        private string _fieldName;
        private string _fieldType;
        private string _displayName;
        private string _formFieldDataTypeCode;
        private bool _isReported;
        private bool _isRequired;
        private DateTime _lastReportedOn;
        private ObjectHolder _formFieldDataTypeHolder = null;
        private ObjectHolder _mergeDocumentHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Merge Document.
        /// </summary>
        public Guid MergeDocumentID
        {
            get { return _mergeDocumentID; }
        }

        /// <summary>
        /// Gets the Field Name.
        /// </summary>
        public string FieldName
        {
            get { return _fieldName; }
        }

        /// <summary>
        /// Gets or sets the Field Type.
        /// </summary>
        public string FieldType
        {
            get { return _fieldType; }
            set
            {
                VerifyWritable();
                _fieldType = value;
            }
        }

        /// <summary>
        /// Gets or sets the Display Name.
        /// </summary>
        public string DisplayName
        {
            get { return _displayName; }
            set
            {
                VerifyWritable();
                _displayName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Form Field Data Type.
        /// </summary>
        public string FormFieldDataTypeCode
        {
            get { return _formFieldDataTypeCode; }
            set
            {
                VerifyWritable();
                _formFieldDataTypeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Is Reported.
        /// </summary>
        public bool IsReported
        {
            get { return _isReported; }
            set
            {
                VerifyWritable();
                _isReported = value;
            }
        }

        /// <summary>
        /// Gets or sets the Is Required.
        /// </summary>
        public bool IsRequired
        {
            get { return _isRequired; }
            set
            {
                VerifyWritable();
                _isRequired = value;
            }
        }

        /// <summary>
        /// Gets or sets the Last Reported On.
        /// </summary>
        public DateTime LastReportedOn
        {
            get { return _lastReportedOn; }
            set
            {
                VerifyWritable();
                _lastReportedOn = value;
            }
        }

        /// <summary>
        /// Gets the instance of a FormFieldDataType object related to this entity.
        /// </summary>
        public FormFieldDataType FormFieldDataType
        {
            get
            {
                _formFieldDataTypeHolder.Key = _formFieldDataTypeCode;
                return (FormFieldDataType)_formFieldDataTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a MergeDocument object related to this entity.
        /// </summary>
        public MergeDocument MergeDocument
        {
            get
            {
                _mergeDocumentHolder.Key = _mergeDocumentID;
                return (MergeDocument)_mergeDocumentHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_mergeDocumentID": return _mergeDocumentID;
                    case "_fieldName": return _fieldName;
                    case "_fieldType": return _fieldType;
                    case "_displayName": return _displayName;
                    case "_formFieldDataTypeCode": return _formFieldDataTypeCode;
                    case "_isReported": return _isReported;
                    case "_isRequired": return _isRequired;
                    case "_lastReportedOn": return _lastReportedOn;
                    case "_formFieldDataTypeHolder": return _formFieldDataTypeHolder;
                    case "_mergeDocumentHolder": return _mergeDocumentHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_mergeDocumentID": _mergeDocumentID = (Guid)value; break;
                    case "_fieldName": _fieldName = (string)value; break;
                    case "_fieldType": _fieldType = (string)value; break;
                    case "_displayName": _displayName = (string)value; break;
                    case "_formFieldDataTypeCode": _formFieldDataTypeCode = (string)value; break;
                    case "_isReported": _isReported = (bool)value; break;
                    case "_isRequired": _isRequired = (bool)value; break;
                    case "_lastReportedOn": _lastReportedOn = (DateTime)value; break;
                    case "_formFieldDataTypeHolder": _formFieldDataTypeHolder = (ObjectHolder)value; break;
                    case "_mergeDocumentHolder": _mergeDocumentHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="mergeDocumentID">MergeDocumentID of the entity to fetch.</param>
        /// <param name="fieldName">FieldName of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static MergeDocumentField Get(Guid mergeDocumentID, string fieldName)
        {
            OPathQuery query = new OPathQuery(typeof(MergeDocumentField), "MergeDocumentID = ? && FieldName = ?");
            object entity = DB.Engine.GetObject(query, mergeDocumentID, fieldName);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch MergeDocumentField entity with MergeDocumentID = '{0}' and FieldName = '{1}'.", mergeDocumentID, fieldName));
            }
            return (MergeDocumentField)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static MergeDocumentField GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(MergeDocumentField), opathExpression);
            return (MergeDocumentField)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static MergeDocumentField GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(MergeDocumentField))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (MergeDocumentField)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static MergeDocumentField[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(MergeDocumentField), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static MergeDocumentField[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(MergeDocumentField))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (MergeDocumentField[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static MergeDocumentField[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(MergeDocumentField), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public MergeDocumentField GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            MergeDocumentField clone = (MergeDocumentField)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.MergeDocumentID, _mergeDocumentID);
            Validator.Validate(Field.FieldName, _fieldName);
            Validator.Validate(Field.FieldType, _fieldType);
            Validator.Validate(Field.DisplayName, _displayName);
            Validator.Validate(Field.FormFieldDataTypeCode, _formFieldDataTypeCode);
            Validator.Validate(Field.IsReported, _isReported);
            Validator.Validate(Field.IsRequired, _isRequired);
            Validator.Validate(Field.LastReportedOn, _lastReportedOn);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _mergeDocumentID = Guid.Empty;
            _fieldName = null;
            _fieldType = null;
            _displayName = null;
            _formFieldDataTypeCode = null;
            _isReported = false;
            _isRequired = false;
            _lastReportedOn = DateTime.MinValue;
            _formFieldDataTypeHolder = null;
            _mergeDocumentHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the MergeDocumentField entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the MergeDocumentID field.
            /// </summary>
            MergeDocumentID,
            /// <summary>
            /// Represents the FieldName field.
            /// </summary>
            FieldName,
            /// <summary>
            /// Represents the FieldType field.
            /// </summary>
            FieldType,
            /// <summary>
            /// Represents the DisplayName field.
            /// </summary>
            DisplayName,
            /// <summary>
            /// Represents the FormFieldDataTypeCode field.
            /// </summary>
            FormFieldDataTypeCode,
            /// <summary>
            /// Represents the IsReported field.
            /// </summary>
            IsReported,
            /// <summary>
            /// Represents the IsRequired field.
            /// </summary>
            IsRequired,
            /// <summary>
            /// Represents the LastReportedOn field.
            /// </summary>
            LastReportedOn,
        }

        #endregion
    }
}