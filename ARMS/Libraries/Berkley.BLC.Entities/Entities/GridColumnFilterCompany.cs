using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a GridColumnFilterCompany entity, which maps to table 'GridColumnFilter_Company' in the database.
    /// </summary>
    public class GridColumnFilterCompany : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private GridColumnFilterCompany()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="gridColumnFilterID">GridColumnFilterID of this GridColumnFilterCompany.</param>
        /// <param name="companyID">CompanyID of this GridColumnFilterCompany.</param>
        public GridColumnFilterCompany(Guid gridColumnFilterID, Guid companyID)
        {
            _gridColumnFilterID = gridColumnFilterID;
            _companyID = companyID;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _gridColumnFilterID;
        private Guid _companyID;
        private int _displayOrder;
        private bool _display;
        private bool _required;
		private bool _isExternal;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _gridColumnFilterHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Grid Column Filter.
        /// </summary>
        public Guid GridColumnFilterID
        {
            get { return _gridColumnFilterID; }
        }

        /// <summary>
        /// Gets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets or sets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
            set
            {
                VerifyWritable();
                _displayOrder = value;
            }
        }

        /// <summary>
        /// Gets or sets the Display.
        /// </summary>
        public bool Display
        {
            get { return _display; }
            set
            {
                VerifyWritable();
                _display = value;
            }
        }

        /// <summary>
        /// Gets or sets the Required.
        /// </summary>
        public bool Required
        {
            get { return _required; }
            set
            {
                VerifyWritable();
                _required = value;
            }
        }

		/// <summary>
		/// Gets the Is External. Null value is 'false'.
		/// </summary>
		public bool IsExternal
		{
            get { return _isExternal; }
            set
            {
                VerifyWritable();
                _isExternal = value;
            }
		}
        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a GridColumnFilter object related to this entity.
        /// </summary>
        public GridColumnFilter GridColumnFilter
        {
            get
            {
                _gridColumnFilterHolder.Key = _gridColumnFilterID;
                return (GridColumnFilter)_gridColumnFilterHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_gridColumnFilterID": return _gridColumnFilterID;
                    case "_companyID": return _companyID;
                    case "_displayOrder": return _displayOrder;
                    case "_display": return _display;
                    case "_required": return _required;
					case "_isExternal": return _isExternal;
                    case "_companyHolder": return _companyHolder;
                    case "_gridColumnFilterHolder": return _gridColumnFilterHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_gridColumnFilterID": _gridColumnFilterID = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    case "_display": _display = (bool)value; break;
                    case "_required": _required = (bool)value; break;
					case "_isExternal": _isExternal = (bool)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_gridColumnFilterHolder": _gridColumnFilterHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="gridColumnFilterID">GridColumnFilterID of the entity to fetch.</param>
        /// <param name="companyID">CompanyID of the entity to fetch.</param>
		/// <param name="isExternal">IsExternal of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
		public static GridColumnFilterCompany Get(Guid gridColumnFilterID, Guid companyID, bool isExternal)
        {
			OPathQuery query = new OPathQuery(typeof(GridColumnFilterCompany), "GridColumnFilterID = ? && CompanyID = ? && IsExternal = ?");
			object entity = DB.Engine.GetObject(query, gridColumnFilterID, companyID, isExternal);	
            if (entity == null)
            {
				throw new Exception(string.Format("Failed to fetch GridColumnFilterCompany entity with GridColumnFilterID = '{0}' and CompanyID = '{1}' and IsExternal = '{2}'.", gridColumnFilterID, companyID, isExternal));
            }
            return (GridColumnFilterCompany)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static GridColumnFilterCompany GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(GridColumnFilterCompany), opathExpression);
            return (GridColumnFilterCompany)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static GridColumnFilterCompany GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(GridColumnFilterCompany))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (GridColumnFilterCompany)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static GridColumnFilterCompany[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(GridColumnFilterCompany), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static GridColumnFilterCompany[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(GridColumnFilterCompany))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (GridColumnFilterCompany[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static GridColumnFilterCompany[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(GridColumnFilterCompany), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public GridColumnFilterCompany GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            GridColumnFilterCompany clone = (GridColumnFilterCompany)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.GridColumnFilterID, _gridColumnFilterID);
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.DisplayOrder, _displayOrder);
            Validator.Validate(Field.Display, _display);
            Validator.Validate(Field.Required, _required);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _gridColumnFilterID = Guid.Empty;
            _companyID = Guid.Empty;
            _displayOrder = Int32.MinValue;
            _display = false;
            _required = false;
			_isExternal = false;
            _companyHolder = null;
            _gridColumnFilterHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the GridColumnFilterCompany entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the GridColumnFilterID field.
            /// </summary>
            GridColumnFilterID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
            /// <summary>
            /// Represents the Display field.
            /// </summary>
            Display,
            /// <summary>
            /// Represents the Required field.
            /// </summary>
            Required,
			/// <summary>
			/// Represents the IsExternal field.
			/// </summary>
			IsExternal,
        }

        #endregion
    }
}