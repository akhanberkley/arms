using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a ClientAttributeType entity, which maps to table 'ClientAttributeType' in the database.
	/// </summary>
    [Serializable]
	public class ClientAttributeType : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private ClientAttributeType()
		{
		}


        #region --- Generated Members ---

        private string _code;
        private string _name;
        private string _clientCoreGroupName = String.Empty;
        private string _dotNetDataType;
        private string _sourceTable = String.Empty;
        private string _sourceColumn = String.Empty;
        private string _filterExpression = String.Empty;
        private string _sortExpression = String.Empty;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Client Attribute Type.
        /// </summary>
        public string Code
        {
            get { return _code; }
        }

        /// <summary>
        /// Gets the Client Attribute Type.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Gets the Client Core Group Name. Null value is 'String.Empty'.
        /// </summary>
        public string ClientCoreGroupName
        {
            get { return _clientCoreGroupName; }
        }

        /// <summary>
        /// Gets the Dot Net Data Type.
        /// </summary>
        public string DotNetDataType
        {
            get { return _dotNetDataType; }
        }

        /// <summary>
        /// Gets the Source Table. Null value is 'String.Empty'.
        /// </summary>
        public string SourceTable
        {
            get { return _sourceTable; }
        }

        /// <summary>
        /// Gets the Source Column. Null value is 'String.Empty'.
        /// </summary>
        public string SourceColumn
        {
            get { return _sourceColumn; }
        }

        /// <summary>
        /// Gets the Filter Expression. Null value is 'String.Empty'.
        /// </summary>
        public string FilterExpression
        {
            get { return _filterExpression; }
        }

        /// <summary>
        /// Gets the Sort Expression. Null value is 'String.Empty'.
        /// </summary>
        public string SortExpression
        {
            get { return _sortExpression; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_code": return _code;
                    case "_name": return _name;
                    case "_clientCoreGroupName": return _clientCoreGroupName;
                    case "_dotNetDataType": return _dotNetDataType;
                    case "_sourceTable": return _sourceTable;
                    case "_sourceColumn": return _sourceColumn;
                    case "_filterExpression": return _filterExpression;
                    case "_sortExpression": return _sortExpression;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_code": _code = (string)value; break;
                    case "_name": _name = (string)value; break;
                    case "_clientCoreGroupName": _clientCoreGroupName = (string)value; break;
                    case "_dotNetDataType": _dotNetDataType = (string)value; break;
                    case "_sourceTable": _sourceTable = (string)value; break;
                    case "_sourceColumn": _sourceColumn = (string)value; break;
                    case "_filterExpression": _filterExpression = (string)value; break;
                    case "_sortExpression": _sortExpression = (string)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="code">Code of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static ClientAttributeType Get(string code)
        {
            OPathQuery query = new OPathQuery(typeof(ClientAttributeType), "Code = ?");
            object entity = DB.Engine.GetObject(query, code);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch ClientAttributeType entity with Code = '{0}'.", code));
            }
            return (ClientAttributeType)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ClientAttributeType GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ClientAttributeType), opathExpression);
            return (ClientAttributeType)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ClientAttributeType GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ClientAttributeType))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ClientAttributeType)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ClientAttributeType[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ClientAttributeType), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ClientAttributeType[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ClientAttributeType))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ClientAttributeType[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ClientAttributeType[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ClientAttributeType), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Determines if two ClientAttributeType objects have the same semantic value.
        /// </summary>
        /// <param name="one">The first object to compare.</param>
        /// <param name="two">The second object to compare.</param>
        /// <returns>True if they are semantically identical, false otherwise.</returns>
        public static bool operator ==(ClientAttributeType one, ClientAttributeType two)
        {
            if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
            {
                return true;
            }
            else if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
            {
                return false;
            }
            else
            {
                return (one.CompareTo(two) == 0);
            }
        }

        /// <summary>
        /// Determines if two ClientAttributeType objects have different semantic values.
        /// </summary>
        /// <param name="one">The first object to compare.</param>
        /// <param name="two">The second object to compare.</param>
        /// <returns>True if they are semantically dissimilar, false otherwise.</returns>
        public static bool operator !=(ClientAttributeType one, ClientAttributeType two)
        {
            if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
            {
                return false;
            }
            if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
            {
                return true;
            }
            else
            {
                return (one.CompareTo(two) != 0);
            }
        }

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="value">An object to compare with this instance.</param>
        /// <returns>Less than zero if this instance is less than value. 
        /// Zero if this instance is equal to value.
        /// Greater than zero if this instance is greater than value.</returns>
        public int CompareTo(object value)
        {
            if (value == null)
            {
                return 1;
            }
            if (value is ClientAttributeType)
            {
                ClientAttributeType obj = (ClientAttributeType)value;
                return (this.Code.CompareTo(obj.Code));
            }
            throw new ArgumentException("Value is not the correct type.");
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode()
        {
            return this.Code.GetHashCode();
        }

        /// <summary>
        /// Determines whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="value">The object to compare with the current instance. </param>
        /// <returns>true if the specified object is equal to the current instance; otherwise, false.</returns>
        public override bool Equals(object value)
        {
            return (this == (value as ClientAttributeType));
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _code = null;
            _name = null;
            _clientCoreGroupName = null;
            _dotNetDataType = null;
            _sourceTable = null;
            _sourceColumn = null;
            _filterExpression = null;
            _sortExpression = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the ClientAttributeType entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the Code field.
            /// </summary>
            Code,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the ClientCoreGroupName field.
            /// </summary>
            ClientCoreGroupName,
            /// <summary>
            /// Represents the DotNetDataType field.
            /// </summary>
            DotNetDataType,
            /// <summary>
            /// Represents the SourceTable field.
            /// </summary>
            SourceTable,
            /// <summary>
            /// Represents the SourceColumn field.
            /// </summary>
            SourceColumn,
            /// <summary>
            /// Represents the FilterExpression field.
            /// </summary>
            FilterExpression,
            /// <summary>
            /// Represents the SortExpression field.
            /// </summary>
            SortExpression,
        }

        #endregion
    }
}