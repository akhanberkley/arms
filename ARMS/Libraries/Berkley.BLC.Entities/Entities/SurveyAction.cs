using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyAction entity, which maps to table 'SurveyAction' in the database.
	/// </summary>
	public class SurveyAction : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyAction()
		{
		}


        #region --- Generated Members ---

        private Guid _id;
        private Guid _companyID;
        private string _surveyStatusCode;
        private string _typeCode;
        private string _displayName;
        private int _displayOrder;
        private string _confirmMessage = String.Empty;
        private ObjectHolder _surveyActionTypeHolder = null;
        private ObjectHolder _surveyStatusHolder = null;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Survey Action.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets the Company ID.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets the Survey Status.
        /// </summary>
        public string SurveyStatusCode
        {
            get { return _surveyStatusCode; }
        }

        /// <summary>
        /// Gets the Survey Action Type.
        /// </summary>
        public string TypeCode
        {
            get { return _typeCode; }
        }

        /// <summary>
        /// Gets the Display Name.
        /// </summary>
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        /// <summary>
        /// Gets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
        }

        /// <summary>
        /// Gets the Confirm Message. Null value is 'String.Empty'.
        /// </summary>
        public string ConfirmMessage
        {
            get { return _confirmMessage; }
        }

        /// <summary>
        /// Gets the instance of a SurveyActionType object related to this entity.
        /// </summary>
        public SurveyActionType Type
        {
            get
            {
                _surveyActionTypeHolder.Key = _typeCode;
                return (SurveyActionType)_surveyActionTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a SurveyStatus object related to this entity.
        /// </summary>
        public SurveyStatus SurveyStatus
        {
            get
            {
                _surveyStatusHolder.Key = _surveyStatusCode;
                return (SurveyStatus)_surveyStatusHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_companyID": return _companyID;
                    case "_surveyStatusCode": return _surveyStatusCode;
                    case "_typeCode": return _typeCode;
                    case "_displayName": return _displayName;
                    case "_displayOrder": return _displayOrder;
                    case "_confirmMessage": return _confirmMessage;
                    case "_surveyActionTypeHolder": return _surveyActionTypeHolder;
                    case "_surveyStatusHolder": return _surveyStatusHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_surveyStatusCode": _surveyStatusCode = (string)value; break;
                    case "_typeCode": _typeCode = (string)value; break;
                    case "_displayName": _displayName = (string)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    case "_confirmMessage": _confirmMessage = (string)value; break;
                    case "_surveyActionTypeHolder": _surveyActionTypeHolder = (ObjectHolder)value; break;
                    case "_surveyStatusHolder": _surveyStatusHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static SurveyAction Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyAction), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch SurveyAction entity with ID = '{0}'.", id));
            }
            return (SurveyAction)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyAction GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyAction), opathExpression);
            return (SurveyAction)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyAction GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyAction))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyAction)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyAction[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyAction), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyAction[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyAction))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyAction[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyAction[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyAction), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _companyID = Guid.Empty;
            _surveyStatusCode = null;
            _typeCode = null;
            _displayName = null;
            _displayOrder = Int32.MinValue;
            _confirmMessage = null;
            _surveyActionTypeHolder = null;
            _surveyStatusHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the SurveyAction entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the SurveyStatusCode field.
            /// </summary>
            SurveyStatusCode,
            /// <summary>
            /// Represents the TypeCode field.
            /// </summary>
            TypeCode,
            /// <summary>
            /// Represents the DisplayName field.
            /// </summary>
            DisplayName,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
            /// <summary>
            /// Represents the ConfirmMessage field.
            /// </summary>
            ConfirmMessage,
        }

        #endregion
    }
}