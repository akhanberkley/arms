using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents an Underwriter entity, which maps to table 'Underwriter' in the database.
    /// </summary>
    public class Underwriter : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private Underwriter()
        {
        }

        /// <summary>
        /// Fetches an array of entity objects from the database.
        /// </summary>
        /// <param name="company">The company for which to filter the underwriter list.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Underwriter[] GetArray(Company company, bool enabledOnly)
        {
            //Execute the stored procedure
            SelectProcedure sp;
            if (enabledOnly)
            {
                sp = new SelectProcedure(typeof(Underwriter), "Underwriter_GetEnabled");
                sp.AddParameter("@CompanyID", company.ID);
            }
            else
            {
                sp = new SelectProcedure(typeof(Underwriter), "Underwriter_Get");
                sp.AddParameter("@CompanyID", company.ID);
            }
            
            IList list = DB.Engine.GetObjectSet(sp);
            Array result = Array.CreateInstance(sp.ObjectType, list.Count);
            list.CopyTo(result, 0);

            return result as Underwriter[];
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="code">Code of this Underwriter.</param>
        /// <param name="companyID">CompanyID of this Underwriter.</param>
        public Underwriter(string code, Guid companyID)
        {
            _code = code;
            _companyID = companyID;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Gets the the phone number as a fomatted string value.
        /// </summary>
        public string HtmlPhoneNumber
        {
            get
            {
                return Formatter.Phone(this.PhoneNumber, "(none)");
            }
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private string _code;
        private Guid _companyID;
        private string _name;
        private string _domainName;
        private string _username;
        private string _phoneNumber;
        private string _phoneExt = String.Empty;
        private string _emailAddress = String.Empty;
        private string _title;
        private string _profitCenterCode = String.Empty;
        private bool _disabled;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _profitCenterHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Underwriter.
        /// </summary>
        public string Code
        {
            get { return _code; }
        }

        /// <summary>
        /// Gets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets or sets the Underwriter.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                VerifyWritable();
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Domain Name.
        /// </summary>
        public string DomainName
        {
            get { return _domainName; }
            set
            {
                VerifyWritable();
                _domainName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Username.
        /// </summary>
        public string Username
        {
            get { return _username; }
            set
            {
                VerifyWritable();
                _username = value;
            }
        }

        /// <summary>
        /// Gets or sets the Phone Number.
        /// </summary>
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                VerifyWritable();
                _phoneNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Phone Ext. Null value is 'String.Empty'.
        /// </summary>
        public string PhoneExt
        {
            get { return _phoneExt; }
            set
            {
                VerifyWritable();
                _phoneExt = value;
            }
        }

        /// <summary>
        /// Gets or sets the Email Address. Null value is 'String.Empty'.
        /// </summary>
        public string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                VerifyWritable();
                _emailAddress = value;
            }
        }

        /// <summary>
        /// Gets or sets the Title.
        /// </summary>
        public string Title
        {
            get { return _title; }
            set
            {
                VerifyWritable();
                _title = value;
            }
        }

        /// <summary>
        /// Gets or sets the Profit Center. Null value is 'String.Empty'.
        /// </summary>
        public string ProfitCenterCode
        {
            get { return _profitCenterCode; }
            set
            {
                VerifyWritable();
                _profitCenterCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Disabled.
        /// </summary>
        public bool Disabled
        {
            get { return _disabled; }
            set
            {
                VerifyWritable();
                _disabled = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a ProfitCenter object related to this entity.
        /// </summary>
        public ProfitCenter ProfitCenter
        {
            get
            {
                _profitCenterHolder.Key = _profitCenterCode;
                return (ProfitCenter)_profitCenterHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_code": return _code;
                    case "_companyID": return _companyID;
                    case "_name": return _name;
                    case "_domainName": return _domainName;
                    case "_username": return _username;
                    case "_phoneNumber": return _phoneNumber;
                    case "_phoneExt": return _phoneExt;
                    case "_emailAddress": return _emailAddress;
                    case "_title": return _title;
                    case "_profitCenterCode": return _profitCenterCode;
                    case "_disabled": return _disabled;
                    case "_companyHolder": return _companyHolder;
                    case "_profitCenterHolder": return _profitCenterHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_code": _code = (string)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_name": _name = (string)value; break;
                    case "_domainName": _domainName = (string)value; break;
                    case "_username": _username = (string)value; break;
                    case "_phoneNumber": _phoneNumber = (string)value; break;
                    case "_phoneExt": _phoneExt = (string)value; break;
                    case "_emailAddress": _emailAddress = (string)value; break;
                    case "_title": _title = (string)value; break;
                    case "_profitCenterCode": _profitCenterCode = (string)value; break;
                    case "_disabled": _disabled = (bool)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_profitCenterHolder": _profitCenterHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="code">Code of the entity to fetch.</param>
        /// <param name="companyID">CompanyID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static Underwriter Get(string code, Guid companyID)
        {
            OPathQuery query = new OPathQuery(typeof(Underwriter), "Code = ? && CompanyID = ?");
            object entity = DB.Engine.GetObject(query, code, companyID);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch Underwriter entity with Code = '{0}' and CompanyID = '{1}'.", code, companyID));
            }
            return (Underwriter)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Underwriter GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Underwriter), opathExpression);
            return (Underwriter)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Underwriter GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Underwriter))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Underwriter)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Underwriter[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Underwriter), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Underwriter[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Underwriter))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Underwriter[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Underwriter[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Underwriter), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public Underwriter GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            Underwriter clone = (Underwriter)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.Code, _code);
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.Name, _name);
            Validator.Validate(Field.DomainName, _domainName);
            Validator.Validate(Field.Username, _username);
            Validator.Validate(Field.PhoneNumber, _phoneNumber);
            Validator.Validate(Field.PhoneExt, _phoneExt);
            Validator.Validate(Field.EmailAddress, _emailAddress);
            Validator.Validate(Field.Title, _title);
            Validator.Validate(Field.ProfitCenterCode, _profitCenterCode);
            Validator.Validate(Field.Disabled, _disabled);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _code = null;
            _companyID = Guid.Empty;
            _name = null;
            _domainName = null;
            _username = null;
            _phoneNumber = null;
            _phoneExt = null;
            _emailAddress = null;
            _title = null;
            _profitCenterCode = null;
            _disabled = false;
            _companyHolder = null;
            _profitCenterHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the Underwriter entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the Code field.
            /// </summary>
            Code,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the DomainName field.
            /// </summary>
            DomainName,
            /// <summary>
            /// Represents the Username field.
            /// </summary>
            Username,
            /// <summary>
            /// Represents the PhoneNumber field.
            /// </summary>
            PhoneNumber,
            /// <summary>
            /// Represents the PhoneExt field.
            /// </summary>
            PhoneExt,
            /// <summary>
            /// Represents the EmailAddress field.
            /// </summary>
            EmailAddress,
            /// <summary>
            /// Represents the Title field.
            /// </summary>
            Title,
            /// <summary>
            /// Represents the ProfitCenterCode field.
            /// </summary>
            ProfitCenterCode,
            /// <summary>
            /// Represents the Disabled field.
            /// </summary>
            Disabled,
        }

        #endregion
    }
}