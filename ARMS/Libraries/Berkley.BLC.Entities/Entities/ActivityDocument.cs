using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents an ActivityDocument entity, which maps to table 'ActivityDocument' in the database.
    /// </summary>
    public class ActivityDocument : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private ActivityDocument()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this ActivityDocument.</param>
        public ActivityDocument(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _activityID;
        private string _documentName;
        private string _mimeType;
        private long _sizeInBytes;
        private string _fileNetReference;
        private DateTime _uploadedOn;
        private Guid _uploadUserID;
        private ObjectHolder _activityHolder = null;
        private ObjectHolder _uploadUserHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Activity Document ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Activity.
        /// </summary>
        public Guid ActivityID
        {
            get { return _activityID; }
            set
            {
                VerifyWritable();
                _activityID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Document Name.
        /// </summary>
        public string DocumentName
        {
            get { return _documentName; }
            set
            {
                VerifyWritable();
                _documentName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Mime Type.
        /// </summary>
        public string MimeType
        {
            get { return _mimeType; }
            set
            {
                VerifyWritable();
                _mimeType = value;
            }
        }

        /// <summary>
        /// Gets or sets the Size In Bytes.
        /// </summary>
        public long SizeInBytes
        {
            get { return _sizeInBytes; }
            set
            {
                VerifyWritable();
                _sizeInBytes = value;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Reference.
        /// </summary>
        public string FileNetReference
        {
            get { return _fileNetReference; }
            set
            {
                VerifyWritable();
                _fileNetReference = value;
            }
        }

        /// <summary>
        /// Gets or sets the Uploaded On.
        /// </summary>
        public DateTime UploadedOn
        {
            get { return _uploadedOn; }
            set
            {
                VerifyWritable();
                _uploadedOn = value;
            }
        }

        /// <summary>
        /// Gets or sets the Upload User.
        /// </summary>
        public Guid UploadUserID
        {
            get { return _uploadUserID; }
            set
            {
                VerifyWritable();
                _uploadUserID = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Activity object related to this entity.
        /// </summary>
        public Activity Activity
        {
            get
            {
                _activityHolder.Key = _activityID;
                return (Activity)_activityHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User UploadUser
        {
            get
            {
                _uploadUserHolder.Key = _uploadUserID;
                return (User)_uploadUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_activityID": return _activityID;
                    case "_documentName": return _documentName;
                    case "_mimeType": return _mimeType;
                    case "_sizeInBytes": return _sizeInBytes;
                    case "_fileNetReference": return _fileNetReference;
                    case "_uploadedOn": return _uploadedOn;
                    case "_uploadUserID": return _uploadUserID;
                    case "_activityHolder": return _activityHolder;
                    case "_uploadUserHolder": return _uploadUserHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_activityID": _activityID = (Guid)value; break;
                    case "_documentName": _documentName = (string)value; break;
                    case "_mimeType": _mimeType = (string)value; break;
                    case "_sizeInBytes": _sizeInBytes = (long)value; break;
                    case "_fileNetReference": _fileNetReference = (string)value; break;
                    case "_uploadedOn": _uploadedOn = (DateTime)value; break;
                    case "_uploadUserID": _uploadUserID = (Guid)value; break;
                    case "_activityHolder": _activityHolder = (ObjectHolder)value; break;
                    case "_uploadUserHolder": _uploadUserHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static ActivityDocument Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(ActivityDocument), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch ActivityDocument entity with ID = '{0}'.", id));
            }
            return (ActivityDocument)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ActivityDocument GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ActivityDocument), opathExpression);
            return (ActivityDocument)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ActivityDocument GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ActivityDocument))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ActivityDocument)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ActivityDocument[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ActivityDocument), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ActivityDocument[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ActivityDocument))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ActivityDocument[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ActivityDocument[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ActivityDocument), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public ActivityDocument GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            ActivityDocument clone = (ActivityDocument)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.ActivityID, _activityID);
            Validator.Validate(Field.DocumentName, _documentName);
            Validator.Validate(Field.MimeType, _mimeType);
            Validator.Validate(Field.SizeInBytes, _sizeInBytes);
            Validator.Validate(Field.FileNetReference, _fileNetReference);
            Validator.Validate(Field.UploadedOn, _uploadedOn);
            Validator.Validate(Field.UploadUserID, _uploadUserID);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _activityID = Guid.Empty;
            _documentName = null;
            _mimeType = null;
            _sizeInBytes = Int64.MinValue;
            _fileNetReference = null;
            _uploadedOn = DateTime.MinValue;
            _uploadUserID = Guid.Empty;
            _activityHolder = null;
            _uploadUserHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the ActivityDocument entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the ActivityID field.
            /// </summary>
            ActivityID,
            /// <summary>
            /// Represents the DocumentName field.
            /// </summary>
            DocumentName,
            /// <summary>
            /// Represents the MimeType field.
            /// </summary>
            MimeType,
            /// <summary>
            /// Represents the SizeInBytes field.
            /// </summary>
            SizeInBytes,
            /// <summary>
            /// Represents the FileNetReference field.
            /// </summary>
            FileNetReference,
            /// <summary>
            /// Represents the UploadedOn field.
            /// </summary>
            UploadedOn,
            /// <summary>
            /// Represents the UploadUserID field.
            /// </summary>
            UploadUserID,
        }

        #endregion
    }
}