using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a Client entity, which maps to table 'Client' in the database.
	/// </summary>
    [Serializable]
	public class Client : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private Client()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this Client.</param>
		/// <param name="attributeID">AttributeID of this Client.</param>
		public Client(string id, Guid attributeID)
		{
			_id = id;
			_attributeID = attributeID;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private string _id;
		private Guid _attributeID;
		private string _attributeValue = String.Empty;
		private DateTime _dateModified = DateTime.MinValue;
		private ObjectHolder _clientAttributeHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Client ID.
		/// </summary>
		public string ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets the Client Attribute.
		/// </summary>
		public Guid AttributeID
		{
			get { return _attributeID; }
		}

		/// <summary>
		/// Gets or sets the Client Attribute Value. Null value is 'String.Empty'.
		/// </summary>
		public string AttributeValue
		{
			get { return _attributeValue; }
			set
			{
				VerifyWritable();
				_attributeValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the Date Modified. Null value is 'DateTime.MinValue'.
		/// </summary>
		public DateTime DateModified
		{
			get { return _dateModified; }
			set
			{
				VerifyWritable();
				_dateModified = value;
			}
		}

		/// <summary>
		/// Gets the instance of a ClientAttribute object related to this entity.
		/// </summary>
		public ClientAttribute Attribute
		{
			get
			{
				_clientAttributeHolder.Key = _attributeID;
				return (ClientAttribute)_clientAttributeHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_attributeID": return _attributeID;
					case "_attributeValue": return _attributeValue;
					case "_dateModified": return _dateModified;
					case "_clientAttributeHolder": return _clientAttributeHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (string)value; break;
					case "_attributeID": _attributeID = (Guid)value; break;
					case "_attributeValue": _attributeValue = (string)value; break;
					case "_dateModified": _dateModified = (DateTime)value; break;
					case "_clientAttributeHolder": _clientAttributeHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <param name="attributeID">AttributeID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static Client Get(string id, Guid attributeID)
		{
			OPathQuery query = new OPathQuery(typeof(Client), "ID = ? && AttributeID = ?");
			object entity = DB.Engine.GetObject(query, id, attributeID);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch Client entity with ID = '{0}' and AttributeID = '{1}'.", id, attributeID));
			}
			return (Client)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static Client GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(Client), opathExpression);
			return (Client)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static Client GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(Client) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (Client)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static Client[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(Client), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static Client[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(Client) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (Client[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static Client[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(Client), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public Client GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			Client clone = (Client)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ID, _id);
			Validator.Validate(Field.AttributeID, _attributeID);
			Validator.Validate(Field.AttributeValue, _attributeValue);
			Validator.Validate(Field.DateModified, _dateModified);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = null;
			_attributeID = Guid.Empty;
			_attributeValue = null;
			_dateModified = DateTime.MinValue;
			_clientAttributeHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the Client entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the AttributeID field.
			/// </summary>
			AttributeID,
			/// <summary>
			/// Represents the AttributeValue field.
			/// </summary>
			AttributeValue,
			/// <summary>
			/// Represents the DateModified field.
			/// </summary>
			DateModified,
		}
		
		#endregion
	}
}