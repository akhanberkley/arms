using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a LineOfBusiness entity, which maps to table 'LineOfBusiness' in the database.
	/// </summary>
	public class LineOfBusiness : IObjectHelper
	{
        #region Public Fields

        /// <summary>
        /// Represents a Worker's Compensation policy type.
        /// </summary>
        public static readonly LineOfBusiness WorkersCompensation = LineOfBusiness.Get("WC");
        /// <summary>
        /// Represents a General Liability policy type.
        /// </summary>
        public static readonly LineOfBusiness GeneralLiability = LineOfBusiness.Get("GL");
        /// <summary>
        /// Represents a Commercial Auto type.
        /// </summary>
        public static readonly LineOfBusiness CommercialAuto = LineOfBusiness.Get("CA");
        /// <summary>
        /// Represents a Commercial Output policy type.
        /// </summary>
        public static readonly LineOfBusiness CommercialOutput = LineOfBusiness.Get("CO");
        /// <summary>
        /// Represents a Commerical Property policy type.
        /// </summary>
        public static readonly LineOfBusiness CommercialProperty = LineOfBusiness.Get("CP");
        /// <summary>
        /// Represents a Business Owner policy type.
        /// </summary>
        public static readonly LineOfBusiness BusinessOwner = LineOfBusiness.Get("BO");
        /// <summary>
        /// Represents an Inland Marine policy type.
        /// </summary>
        public static readonly LineOfBusiness InlandMarine = LineOfBusiness.Get("IM");
        /// <summary>
        /// Represents an Ocean Marine policy type.
        /// </summary>
        public static readonly LineOfBusiness OceanMarine = LineOfBusiness.Get("OM");
        /// <summary>
        /// Represents a Package policy type.
        /// </summary>
        public static readonly LineOfBusiness Package = LineOfBusiness.Get("PK");
        /// <summary>
        /// Represents a Commercial Umbrella policy type.
        /// </summary>
        public static readonly LineOfBusiness CommercialUmbrella = LineOfBusiness.Get("CU");
        /// <summary>
        /// Represents a Commercial Crime policy type.
        /// </summary>
        public static readonly LineOfBusiness CommercialCrime = LineOfBusiness.Get("CR");
        /// <summary>
        /// Represents a Garage policy type.
        /// </summary>
        public static readonly LineOfBusiness Garage = LineOfBusiness.Get("GA");
        /// <summary>
        /// Represents a Dealer policy type.
        /// </summary>
        public static readonly LineOfBusiness Dealer = LineOfBusiness.Get("DR");
        /// <summary>
        /// Represents a Transporation policy type.
        /// </summary>
        public static readonly LineOfBusiness Transportation = LineOfBusiness.Get("TR");

        #endregion
        
        /// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private LineOfBusiness()
		{
		}


        #region --- Generated Members ---

        private string _code;
        private string _name;
        private int _displayOrder;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Line of Business.
        /// </summary>
        public string Code
        {
            get { return _code; }
        }

        /// <summary>
        /// Gets the Line of Business.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Gets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_code": return _code;
                    case "_name": return _name;
                    case "_displayOrder": return _displayOrder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_code": _code = (string)value; break;
                    case "_name": _name = (string)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="code">Code of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static LineOfBusiness Get(string code)
        {
            OPathQuery query = new OPathQuery(typeof(LineOfBusiness), "Code = ?");
            object entity = DB.Engine.GetObject(query, code);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch LineOfBusiness entity with Code = '{0}'.", code));
            }
            return (LineOfBusiness)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static LineOfBusiness GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(LineOfBusiness), opathExpression);
            return (LineOfBusiness)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static LineOfBusiness GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(LineOfBusiness))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (LineOfBusiness)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static LineOfBusiness[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(LineOfBusiness), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static LineOfBusiness[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(LineOfBusiness))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (LineOfBusiness[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static LineOfBusiness[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(LineOfBusiness), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Determines if two LineOfBusiness objects have the same semantic value.
        /// </summary>
        /// <param name="one">The first object to compare.</param>
        /// <param name="two">The second object to compare.</param>
        /// <returns>True if they are semantically identical, false otherwise.</returns>
        public static bool operator ==(LineOfBusiness one, LineOfBusiness two)
        {
            if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
            {
                return true;
            }
            else if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
            {
                return false;
            }
            else
            {
                return (one.CompareTo(two) == 0);
            }
        }

        /// <summary>
        /// Determines if two LineOfBusiness objects have different semantic values.
        /// </summary>
        /// <param name="one">The first object to compare.</param>
        /// <param name="two">The second object to compare.</param>
        /// <returns>True if they are semantically dissimilar, false otherwise.</returns>
        public static bool operator !=(LineOfBusiness one, LineOfBusiness two)
        {
            if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
            {
                return false;
            }
            if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
            {
                return true;
            }
            else
            {
                return (one.CompareTo(two) != 0);
            }
        }

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="value">An object to compare with this instance.</param>
        /// <returns>Less than zero if this instance is less than value. 
        /// Zero if this instance is equal to value.
        /// Greater than zero if this instance is greater than value.</returns>
        public int CompareTo(object value)
        {
            if (value == null)
            {
                return 1;
            }
            if (value is LineOfBusiness)
            {
                LineOfBusiness obj = (LineOfBusiness)value;
                return (this.Code.CompareTo(obj.Code));
            }
            throw new ArgumentException("Value is not the correct type.");
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode()
        {
            return this.Code.GetHashCode();
        }

        /// <summary>
        /// Determines whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="value">The object to compare with the current instance. </param>
        /// <returns>true if the specified object is equal to the current instance; otherwise, false.</returns>
        public override bool Equals(object value)
        {
            return (this == (value as LineOfBusiness));
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _code = null;
            _name = null;
            _displayOrder = Int32.MinValue;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the LineOfBusiness entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the Code field.
            /// </summary>
            Code,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
        }

        #endregion
    }
}