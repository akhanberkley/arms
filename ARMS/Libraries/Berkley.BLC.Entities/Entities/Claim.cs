using System;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a Claim entity, which maps to table 'Claim' in the database.
    /// </summary>
    public class Claim : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private Claim()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this Filter.</param>
        public Claim(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Selective "assignment operator".  Copied data fields from the Right Hand Side instance to "this" instance
        /// </summary>
        /// <param name="oRhs">Instance with new data</param>
        /// <returns>The modified class</returns>
        public Claim Assign(Claim oRhs)
        {
            Claim oClaim = this;

            if (oClaim.IsReadOnly)
            {
                oClaim = oClaim.GetWritableInstance();
            }

            oClaim.PolicyNumber = oRhs._policyNumber;
            oClaim.PolicyMod = oRhs._policyMod;
            oClaim.PolicySymbol = oRhs._policySymbol;
            oClaim.PolicyExpireDate = oRhs._policyExpireDate;
            oClaim.LossDate = oRhs._lossDate;
            oClaim.Claimant = oRhs._claimant;
            oClaim.Amount = oRhs._amount;
            oClaim.Comment = oRhs._comment;
            oClaim.Status = oRhs._status;
            oClaim.LossType = oRhs._lossType;
            return oClaim;
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }

        #region --- Generated Members ---

        private Guid _id;
        private Guid _policyID;
        private string _policyNumber = String.Empty;
        private int _policyMod = Int32.MinValue;
        private string _policySymbol = String.Empty;
        private DateTime _policyExpireDate = DateTime.MinValue;
        private string _number;
        private DateTime _lossDate;
        private string _claimant;
        private decimal _amount;
        private string _comment;
        private string _status = String.Empty;
        private string _lossType = String.Empty;
        private ObjectHolder _policyHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Claim ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Policy.
        /// </summary>
        public Guid PolicyID
        {
            get { return _policyID; }
            set
            {
                VerifyWritable();
                _policyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Number. Null value is 'String.Empty'.
        /// </summary>
        public string PolicyNumber
        {
            get { return _policyNumber; }
            set
            {
                VerifyWritable();
                _policyNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Mod. Null value is 'Int32.MinValue'.
        /// </summary>
        public int PolicyMod
        {
            get { return _policyMod; }
            set
            {
                VerifyWritable();
                _policyMod = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Symbol. Null value is 'String.Empty'.
        /// </summary>
        public string PolicySymbol
        {
            get { return _policySymbol; }
            set
            {
                VerifyWritable();
                _policySymbol = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Expire Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime PolicyExpireDate
        {
            get { return _policyExpireDate; }
            set
            {
                VerifyWritable();
                _policyExpireDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Claim Number.
        /// </summary>
        public string Number
        {
            get { return _number; }
            set
            {
                VerifyWritable();
                _number = value;
            }
        }

        /// <summary>
        /// Gets or sets the Loss Date.
        /// </summary>
        public DateTime LossDate
        {
            get { return _lossDate; }
            set
            {
                VerifyWritable();
                _lossDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Claimant.
        /// </summary>
        public string Claimant
        {
            get { return _claimant; }
            set
            {
                VerifyWritable();
                _claimant = value;
            }
        }

        /// <summary>
        /// Gets or sets the Claim Amount.
        /// </summary>
        public decimal Amount
        {
            get { return _amount; }
            set
            {
                VerifyWritable();
                _amount = value;
            }
        }

        /// <summary>
        /// Gets or sets the Claim Comment.
        /// </summary>
        public string Comment
        {
            get { return _comment; }
            set
            {
                VerifyWritable();
                _comment = value;
            }
        }

        /// <summary>
        /// Gets or sets the Claim Status. Null value is 'String.Empty'.
        /// </summary>
        public string Status
        {
            get { return _status; }
            set
            {
                VerifyWritable();
                _status = value;
            }
        }

        /// <summary>
        /// Gets or sets the Loss Type. Null value is 'String.Empty'.
        /// </summary>
        public string LossType
        {
            get { return _lossType; }
            set
            {
                VerifyWritable();
                _lossType = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Policy object related to this entity.
        /// </summary>
        public Policy Policy
        {
            get
            {
                _policyHolder.Key = _policyID;
                return (Policy)_policyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_policyID": return _policyID;
                    case "_policyNumber": return _policyNumber;
                    case "_policyMod": return _policyMod;
                    case "_policySymbol": return _policySymbol;
                    case "_policyExpireDate": return _policyExpireDate;
                    case "_number": return _number;
                    case "_lossDate": return _lossDate;
                    case "_claimant": return _claimant;
                    case "_amount": return _amount;
                    case "_comment": return _comment;
                    case "_status": return _status;
                    case "_lossType": return _lossType;
                    case "_policyHolder": return _policyHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_policyID": _policyID = (Guid)value; break;
                    case "_policyNumber": _policyNumber = (string)value; break;
                    case "_policyMod": _policyMod = (int)value; break;
                    case "_policySymbol": _policySymbol = (string)value; break;
                    case "_policyExpireDate": _policyExpireDate = (DateTime)value; break;
                    case "_number": _number = (string)value; break;
                    case "_lossDate": _lossDate = (DateTime)value; break;
                    case "_claimant": _claimant = (string)value; break;
                    case "_amount": _amount = (decimal)value; break;
                    case "_comment": _comment = (string)value; break;
                    case "_status": _status = (string)value; break;
                    case "_lossType": _lossType = (string)value; break;
                    case "_policyHolder": _policyHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static Claim Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(Claim), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch Claim entity with ID = '{0}'.", id));
            }
            return (Claim)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Claim GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Claim), opathExpression);
            return (Claim)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Claim GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Claim))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Claim)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Claim[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Claim), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Claim[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Claim))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Claim[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Claim[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Claim), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public Claim GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            Claim clone = (Claim)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.PolicyID, _policyID);
            Validator.Validate(Field.PolicyNumber, _policyNumber);
            Validator.Validate(Field.PolicyMod, _policyMod);
            Validator.Validate(Field.PolicySymbol, _policySymbol);
            Validator.Validate(Field.PolicyExpireDate, _policyExpireDate);
            Validator.Validate(Field.Number, _number);
            Validator.Validate(Field.LossDate, _lossDate);
            Validator.Validate(Field.Claimant, _claimant);
            Validator.Validate(Field.Amount, _amount);
            Validator.Validate(Field.Comment, _comment);
            Validator.Validate(Field.Status, _status);
            Validator.Validate(Field.LossType, _lossType);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _policyID = Guid.Empty;
            _policyNumber = null;
            _policyMod = Int32.MinValue;
            _policySymbol = null;
            _policyExpireDate = DateTime.MinValue;
            _number = null;
            _lossDate = DateTime.MinValue;
            _claimant = null;
            _amount = Decimal.MinValue;
            _comment = null;
            _status = null;
            _lossType = null;
            _policyHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the Claim entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the PolicyID field.
            /// </summary>
            PolicyID,
            /// <summary>
            /// Represents the PolicyNumber field.
            /// </summary>
            PolicyNumber,
            /// <summary>
            /// Represents the PolicyMod field.
            /// </summary>
            PolicyMod,
            /// <summary>
            /// Represents the PolicySymbol field.
            /// </summary>
            PolicySymbol,
            /// <summary>
            /// Represents the PolicyExpireDate field.
            /// </summary>
            PolicyExpireDate,
            /// <summary>
            /// Represents the Number field.
            /// </summary>
            Number,
            /// <summary>
            /// Represents the LossDate field.
            /// </summary>
            LossDate,
            /// <summary>
            /// Represents the Claimant field.
            /// </summary>
            Claimant,
            /// <summary>
            /// Represents the Amount field.
            /// </summary>
            Amount,
            /// <summary>
            /// Represents the Comment field.
            /// </summary>
            Comment,
            /// <summary>
            /// Represents the Status field.
            /// </summary>
            Status,
            /// <summary>
            /// Represents the LossType field.
            /// </summary>
            LossType,
        }

        #endregion
    }
}