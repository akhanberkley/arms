using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;
using System.Text;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyReason entity, which maps to table 'Survey_Reason' in the database.
	/// </summary>
	public class SurveyReason : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyReason()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="typeID">TypeID of this SurveyReason.</param>
		/// <param name="surveyID">SurveyID of this SurveyReason.</param>
		public SurveyReason(Guid typeID, Guid surveyID)
		{
			_typeID = typeID;
			_surveyID = surveyID;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}

        public static string GetSingleLineOfReasons(Guid surveyID)
        {
            SurveyReason[] reasons = SurveyReason.GetArray("SurveyID = ?", surveyID);

            StringBuilder sb = new StringBuilder();
            if (reasons.Length > 0)
            {
                foreach (SurveyReason reason in reasons)
                {
                    sb.AppendFormat("{0}, ", reason.Type.Name);
                }
            }
            else
            {
                sb.Append("(not specified)");
            }

            return sb.ToString().Trim().TrimEnd(',');
        }


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _typeID;
		private Guid _surveyID;
		private ObjectHolder _surveyHolder = null;
		private ObjectHolder _surveyReasonTypeHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Survey Reason Type.
		/// </summary>
		public Guid TypeID
		{
			get { return _typeID; }
		}

		/// <summary>
		/// Gets the Survey.
		/// </summary>
		public Guid SurveyID
		{
			get { return _surveyID; }
		}

		/// <summary>
		/// Gets the instance of a Survey object related to this entity.
		/// </summary>
		public Survey Survey
		{
			get
			{
				_surveyHolder.Key = _surveyID;
				return (Survey)_surveyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a SurveyReasonType object related to this entity.
		/// </summary>
		public SurveyReasonType Type
		{
			get
			{
				_surveyReasonTypeHolder.Key = _typeID;
				return (SurveyReasonType)_surveyReasonTypeHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_typeID": return _typeID;
					case "_surveyID": return _surveyID;
					case "_surveyHolder": return _surveyHolder;
					case "_surveyReasonTypeHolder": return _surveyReasonTypeHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_typeID": _typeID = (Guid)value; break;
					case "_surveyID": _surveyID = (Guid)value; break;
					case "_surveyHolder": _surveyHolder = (ObjectHolder)value; break;
					case "_surveyReasonTypeHolder": _surveyReasonTypeHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="typeID">TypeID of the entity to fetch.</param>
		/// <param name="surveyID">SurveyID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static SurveyReason Get(Guid typeID, Guid surveyID)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyReason), "TypeID = ? && SurveyID = ?");
			object entity = DB.Engine.GetObject(query, typeID, surveyID);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch SurveyReason entity with TypeID = '{0}' and SurveyID = '{1}'.", typeID, surveyID));
			}
			return (SurveyReason)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyReason GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyReason), opathExpression);
			return (SurveyReason)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyReason GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyReason) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyReason)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyReason[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyReason), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyReason[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyReason) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyReason[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyReason[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyReason), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public SurveyReason GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			SurveyReason clone = (SurveyReason)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.TypeID, _typeID);
			Validator.Validate(Field.SurveyID, _surveyID);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_typeID = Guid.Empty;
			_surveyID = Guid.Empty;
			_surveyHolder = null;
			_surveyReasonTypeHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the SurveyReason entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the TypeID field.
			/// </summary>
			TypeID,
			/// <summary>
			/// Represents the SurveyID field.
			/// </summary>
			SurveyID,
		}
		
		#endregion
	}
}