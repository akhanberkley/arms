using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a SurveyActivity entity, which maps to table 'SurveyActivity' in the database.
    /// </summary>
    public class SurveyActivity : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private SurveyActivity()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this SurveyActivity.</param>
        public SurveyActivity(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _surveyID;
        private Guid _activityTypeID;
        private Guid _allocatedTo;
        private DateTime _activityDate;
        private decimal _activityHours;
        private string _comments = String.Empty;
        private ObjectHolder _activityTypeHolder = null;
        private ObjectHolder _allocatedToUserHolder = null;
        private ObjectHolder _surveyHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Survey Activity ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Survey.
        /// </summary>
        public Guid SurveyID
        {
            get { return _surveyID; }
            set
            {
                VerifyWritable();
                _surveyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Activity Type.
        /// </summary>
        public Guid ActivityTypeID
        {
            get { return _activityTypeID; }
            set
            {
                VerifyWritable();
                _activityTypeID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Allocated To.
        /// </summary>
        public Guid AllocatedTo
        {
            get { return _allocatedTo; }
            set
            {
                VerifyWritable();
                _allocatedTo = value;
            }
        }

        /// <summary>
        /// Gets or sets the Activity Date.
        /// </summary>
        public DateTime ActivityDate
        {
            get { return _activityDate; }
            set
            {
                VerifyWritable();
                _activityDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Activity Hours.
        /// </summary>
        public decimal ActivityHours
        {
            get { return _activityHours; }
            set
            {
                VerifyWritable();
                _activityHours = value;
            }
        }

        /// <summary>
        /// Gets or sets the Comments. Null value is 'String.Empty'.
        /// </summary>
        public string Comments
        {
            get { return _comments; }
            set
            {
                VerifyWritable();
                _comments = value;
            }
        }

        /// <summary>
        /// Gets the instance of a ActivityType object related to this entity.
        /// </summary>
        public ActivityType ActivityType
        {
            get
            {
                _activityTypeHolder.Key = _activityTypeID;
                return (ActivityType)_activityTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User User
        {
            get
            {
                _allocatedToUserHolder.Key = _allocatedTo;
                return (User)_allocatedToUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Survey object related to this entity.
        /// </summary>
        public Survey Survey
        {
            get
            {
                _surveyHolder.Key = _surveyID;
                return (Survey)_surveyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_surveyID": return _surveyID;
                    case "_activityTypeID": return _activityTypeID;
                    case "_allocatedTo": return _allocatedTo;
                    case "_activityDate": return _activityDate;
                    case "_activityHours": return _activityHours;
                    case "_comments": return _comments;
                    case "_activityTypeHolder": return _activityTypeHolder;
                    case "_allocatedToUserHolder": return _allocatedToUserHolder;
                    case "_surveyHolder": return _surveyHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_surveyID": _surveyID = (Guid)value; break;
                    case "_activityTypeID": _activityTypeID = (Guid)value; break;
                    case "_allocatedTo": _allocatedTo = (Guid)value; break;
                    case "_activityDate": _activityDate = (DateTime)value; break;
                    case "_activityHours": _activityHours = (decimal)value; break;
                    case "_comments": _comments = (string)value; break;
                    case "_activityTypeHolder": _activityTypeHolder = (ObjectHolder)value; break;
                    case "_allocatedToUserHolder": _allocatedToUserHolder = (ObjectHolder)value; break;
                    case "_surveyHolder": _surveyHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static SurveyActivity Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyActivity), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch SurveyActivity entity with ID = '{0}'.", id));
            }
            return (SurveyActivity)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyActivity GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyActivity), opathExpression);
            return (SurveyActivity)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyActivity GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyActivity))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyActivity)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyActivity[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyActivity), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyActivity[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyActivity))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyActivity[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyActivity[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyActivity), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public SurveyActivity GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            SurveyActivity clone = (SurveyActivity)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.SurveyID, _surveyID);
            Validator.Validate(Field.ActivityTypeID, _activityTypeID);
            Validator.Validate(Field.AllocatedTo, _allocatedTo);
            Validator.Validate(Field.ActivityDate, _activityDate);
            Validator.Validate(Field.ActivityHours, _activityHours);
            Validator.Validate(Field.Comments, _comments);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _surveyID = Guid.Empty;
            _activityTypeID = Guid.Empty;
            _allocatedTo = Guid.Empty;
            _activityDate = DateTime.MinValue;
            _activityHours = Decimal.MinValue;
            _comments = null;
            _activityTypeHolder = null;
            _allocatedToUserHolder = null;
            _surveyHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the SurveyActivity entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the SurveyID field.
            /// </summary>
            SurveyID,
            /// <summary>
            /// Represents the ActivityTypeID field.
            /// </summary>
            ActivityTypeID,
            /// <summary>
            /// Represents the AllocatedTo field.
            /// </summary>
            AllocatedTo,
            /// <summary>
            /// Represents the ActivityDate field.
            /// </summary>
            ActivityDate,
            /// <summary>
            /// Represents the ActivityHours field.
            /// </summary>
            ActivityHours,
            /// <summary>
            /// Represents the Comments field.
            /// </summary>
            Comments,
        }

        #endregion
    }
}