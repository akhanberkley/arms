using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyGradingCompany entity, which maps to table 'SurveyGrading_Company' in the database.
	/// </summary>
	public class SurveyGradingCompany : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyGradingCompany()
		{
		}


		#region --- Generated Members ---

		private string _surveyGradingCode;
		private Guid _companyID;
		private int _displayOrder;
		private ObjectHolder _companyHolder = null;
		private ObjectHolder _surveyGradingHolder = null;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Survey Grading.
		/// </summary>
		public string SurveyGradingCode
		{
			get { return _surveyGradingCode; }
		}

		/// <summary>
		/// Gets the Company.
		/// </summary>
		public Guid CompanyID
		{
			get { return _companyID; }
		}

		/// <summary>
		/// Gets the Display Order.
		/// </summary>
		public int DisplayOrder
		{
			get { return _displayOrder; }
		}

		/// <summary>
		/// Gets the instance of a Company object related to this entity.
		/// </summary>
		public Company Company
		{
			get
			{
				_companyHolder.Key = _companyID;
				return (Company)_companyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a SurveyGrading object related to this entity.
		/// </summary>
		public SurveyGrading SurveyGrading
		{
			get
			{
				_surveyGradingHolder.Key = _surveyGradingCode;
				return (SurveyGrading)_surveyGradingHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_surveyGradingCode": return _surveyGradingCode;
					case "_companyID": return _companyID;
					case "_displayOrder": return _displayOrder;
					case "_companyHolder": return _companyHolder;
					case "_surveyGradingHolder": return _surveyGradingHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_surveyGradingCode": _surveyGradingCode = (string)value; break;
					case "_companyID": _companyID = (Guid)value; break;
					case "_displayOrder": _displayOrder = (int)value; break;
					case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
					case "_surveyGradingHolder": _surveyGradingHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="surveyGradingCode">SurveyGradingCode of the entity to fetch.</param>
		/// <param name="companyID">CompanyID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static SurveyGradingCompany Get(string surveyGradingCode, Guid companyID)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyGradingCompany), "SurveyGradingCode = ? && CompanyID = ?");
			object entity = DB.Engine.GetObject(query, surveyGradingCode, companyID);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch SurveyGradingCompany entity with SurveyGradingCode = '{0}' and CompanyID = '{1}'.", surveyGradingCode, companyID));
			}
			return (SurveyGradingCompany)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyGradingCompany GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyGradingCompany), opathExpression);
			return (SurveyGradingCompany)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyGradingCompany GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyGradingCompany) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyGradingCompany)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyGradingCompany[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyGradingCompany), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyGradingCompany[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyGradingCompany) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyGradingCompany[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyGradingCompany[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyGradingCompany), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_surveyGradingCode = null;
			_companyID = Guid.Empty;
			_displayOrder = Int32.MinValue;
			_companyHolder = null;
			_surveyGradingHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the SurveyGradingCompany entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the SurveyGradingCode field.
			/// </summary>
			SurveyGradingCode,
			/// <summary>
			/// Represents the CompanyID field.
			/// </summary>
			CompanyID,
			/// <summary>
			/// Represents the DisplayOrder field.
			/// </summary>
			DisplayOrder,
		}
		
		#endregion
	}
}