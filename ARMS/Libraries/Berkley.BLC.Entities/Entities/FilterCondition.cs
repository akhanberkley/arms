using System;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a FilterCondition entity, which maps to table 'FilterCondition' in the database.
	/// </summary>
	public class FilterCondition : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private FilterCondition()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this FilterCondition.</param>
		public FilterCondition(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}

        /// <summary>
        /// Returns a string representation of this FilterCondition, suitable for display to end-users.
        /// </summary>
        /// <returns>String representing the value of this object.</returns>
        public override string ToString()
        {
            return string.Format("{0} {1} {2}",
                this.FilterField.Name,
                this.FilterOperator.DisplayShortName.ToLower(),
                this.DisplayValue);
        }


        /// <summary>
        /// Evaludates a Survey against this condition and returns true if there is a match.
        /// </summary>
        /// <param name="survey">Survey to evaluate.</param>
        /// <returns>True if audit matches; otherwise false.</returns>
        internal bool MatchesSurvey(Survey survey)
        {
            // get the value or values to be compared
            object[] values = GetValuesFromSurvey(survey);

            // this audit is a match if any one of the values matches
            foreach (object value in values)
            {
                if (ValueIsMatch(value))
                {
                    return true;
                }
            }

            // no values matched
            return false;
        }

        private object[] GetValuesFromSurvey(Survey survey)
        {
            ArrayList values = new ArrayList();

            switch (this.FilterFieldCode.Trim()) 
            {
                case "SSDT": // Survey Type Type
                    {
                        values.Add(survey.Type.TypeCode);
                        break;
                    }
                case "ANUM": // Agency Number
                    {
                        if (survey.Insured.AgencyID != Guid.Empty)
                        {
                            values.Add(survey.Insured.Agency.Number);
                        }
                        else // no agency
                        {
                            values.Add(string.Empty);
                        }
                        break;
                    }
                case "ANAM": // Agency Name
                    {
                        if (survey.Insured.AgencyID != Guid.Empty)
                        {
                            values.Add(survey.Insured.Agency.Name);
                        }
                        else // no agency
                        {
                            values.Add(string.Empty);
                        }
                        break;
                    }
                case "ASL1": // Agency Street Line 1
                    {
                        if (survey.Insured.AgencyID != Guid.Empty)
                        {
                            values.Add(survey.Insured.Agency.StreetLine1);
                        }
                        else // no agency
                        {
                            values.Add(string.Empty);
                        }
                        break;
                    }
                case "ASL2": // Agency Street Line 2
                    {
                        if (survey.Insured.AgencyID != Guid.Empty)
                        {
                            values.Add(survey.Insured.Agency.StreetLine2);
                        }
                        else // no agency
                        {
                            values.Add(string.Empty);
                        }
                        break;
                    }
                case "ACTY": // Agency City
                    {
                        if (survey.Insured.AgencyID != Guid.Empty)
                        {
                            values.Add(survey.Insured.Agency.City);
                        }
                        else // no agency
                        {
                            values.Add(string.Empty);
                        }
                        break;
                    }
                case "ASTE": // Agency State
                    {
                        if (survey.Insured.AgencyID != Guid.Empty)
                        {
                            values.Add(survey.Insured.Agency.StateCode);
                        }
                        else // no agency
                        {
                            values.Add(string.Empty);
                        }
                        break;
                    }
                case "AZIP": // Agency Zip Code
                    {
                        if (survey.Insured.AgencyID != Guid.Empty)
                        {
                            values.Add(survey.Insured.Agency.ZipCode);
                        }
                        else // no agency
                        {
                            values.Add(string.Empty);
                        }
                        break;
                    }
                case "APHN": // Agency Phone Number
                    {
                        if (survey.Insured.AgencyID != Guid.Empty)
                        {
                            values.Add(survey.Insured.Agency.PhoneNumber);
                        }
                        else // no agency
                        {
                            values.Add(string.Empty);
                        }
                        break;
                    }
                case "AFAX": // Agency Fax Number
                    {
                        if (survey.Insured.AgencyID != Guid.Empty)
                        {
                            values.Add(survey.Insured.Agency.FaxNumber);
                        }
                        else // no agency
                        {
                            values.Add(string.Empty);
                        }
                        break;
                    }
                case "ICLI": // Insured Client ID
                    {
                        values.Add(survey.Insured.ClientID);
                        break;
                    }
                case "INAM": // Insured Name
                    {
                        values.Add(survey.Insured.Name);
                        break;
                    }
                //case "IPHN": // Insured Phone
                //    {
                //        values.Add(survey.Insured.Phone);
                //        break;
                //    }
                case "ISL1": // Insured Street Line 1
                    {
                        values.Add(survey.Insured.StreetLine1);
                        break;
                    }
                case "ISL2": // Insured Street Line 2
                    {
                        values.Add(survey.Insured.StreetLine2);
                        break;
                    }
                case "ISL3": // Insured Street Line 3
                    {
                        values.Add(survey.Insured.StreetLine3);
                        break;
                    }
                case "ICTY": // Insured City
                    {
                        values.Add(survey.Insured.City);
                        break;
                    }
                case "ISTE": // Insured State
                    {
                        values.Add(survey.Insured.StateCode);
                        break;
                    }
                case "IZIP": // Insured Zip Code
                    {
                        values.Add(survey.Insured.ZipCode);
                        break;
                    }
                case "IBOP": // Insured Bus. Ops
                    {
                        values.Add(survey.Insured.BusinessOperations);
                        break;
                    }
                case "ISIC": // Insured SIC Code
                    {
                        values.Add(survey.Insured.SICCode);
                        break;
                    }
                case "IUWR": // Insured Assigned Underwriter
                    {
                        values.Add(survey.Insured.Underwriter);
                        break;
                    }
                case "LNUM": // Location Number
                    {
                        values.Add(survey.Location.Number);
                        break;
                    }
                case "LSL1": // Location Street Line 1
                    {
                        values.Add(survey.Location.StreetLine1);
                        break;
                    }
                case "LSL2": // Location Street Line 2
                    {
                        values.Add(survey.Location.StreetLine2);
                        break;
                    }
                case "LCTY": // Location City
                    {
                        values.Add(survey.Location.City);
                        break;
                    }
                case "LSTE": // Location State
                    {
                        values.Add(survey.Location.StateCode);
                        break;
                    }
                case "LZIP": // Location Zip Code
                    {
                        values.Add(survey.Location.ZipCode);
                        break;
                    }
                case "PNUM": // Policy Number
                    {
                        Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                        foreach (Policy policy in policies)
                        {
                            values.Add(policy.Number);
                        }
                        break;
                    }
                case "PMOD": // Policy Mod
                    {
                        Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                        foreach (Policy policy in policies)
                        {
                            values.Add(policy.Mod);
                        }
                        break;
                    }
                case "PSYM": // Policy Symbol
                    {
                        Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                        foreach (Policy policy in policies)
                        {
                            values.Add(policy.Symbol);
                        }
                        break;
                    }
                case "PEFD": // Policy Effective Date
                    {
                        Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                        foreach (Policy policy in policies)
                        {
                            values.Add(policy.EffectiveDate);
                        }
                        break;
                    }
                case "PEXD": // Policy Expire Date
                    {
                        Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                        foreach (Policy policy in policies)
                        {
                            values.Add(policy.ExpireDate);
                        }
                        break;
                    }
                case "PLOB": // Policy Line Of Business
                    {
                        Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                        foreach (Policy policy in policies)
                        {
                            values.Add(policy.LineOfBusinessCode);
                        }
                        break;
                    }
                case "PTEP": // Policy Total Premium
                    {
                        Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                        foreach (Policy policy in policies)
                        {
                            values.Add(policy.Premium);
                        }
                        break;
                    }
                case "PCAR": // Policy Carrier
                    {
                        Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                        foreach (Policy policy in policies)
                        {
                            values.Add(policy.Carrier);
                        }
                        break;
                    }
                case "PHAZ": // Policy Hazard Grade
                    {
                        Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                        foreach (Policy policy in policies)
                        {
                            values.Add(policy.HazardGrade);
                        }
                        break;
                    }
                case "PBRN": // Policy Branch Code
                    {
                        Policy[] policies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", survey.ID);
                        foreach (Policy policy in policies)
                        {
                            values.Add(policy.BranchCode);
                        }
                        break;
                    }
                case "BNUM": // Building Number
                    {
                        Building[] buildings = Building.GetArray("LocationID = ?", survey.LocationID);
                        foreach (Building building in buildings)
                        {
                            values.Add(building.Number);
                        }
                        break;
                    }
                case "BYER": // Building Year Built
                    {
                        Building[] buildings = Building.GetArray("LocationID = ?", survey.LocationID);
                        foreach (Building building in buildings)
                        {
                            values.Add(building.YearBuilt);
                        }
                        break;
                    }
                case "BSPR": // Building Has Sprinkler System
                    {
                        Building[] buildings = Building.GetArray("LocationID = ?", survey.LocationID);
                        foreach (Building building in buildings)
                        {
                            values.Add(building.HasSprinklerSystem);
                        }
                        break;
                    }
                case "BPUB": // Building Public Protection
                    {
                        Building[] buildings = Building.GetArray("LocationID = ?", survey.LocationID);
                        foreach (Building building in buildings)
                        {
                            values.Add(building.PublicProtection);
                        }
                        break;
                    }
                case "BSTK": // Building Stock Values
                    {
                        Building[] buildings = Building.GetArray("LocationID = ?", survey.LocationID);
                        foreach (Building building in buildings)
                        {
                            values.Add(building.StockValues);
                        }
                        break;
                    }
                case "BCON": // Building Contents
                    {
                        Building[] buildings = Building.GetArray("LocationID = ?", survey.LocationID);
                        foreach (Building building in buildings)
                        {
                            values.Add(building.Contents);
                        }
                        break;
                    }
                case "BVAL": // Building Value
                    {
                        Building[] buildings = Building.GetArray("LocationID = ?", survey.LocationID);
                        foreach (Building building in buildings)
                        {
                            values.Add(building.Value);
                        }
                        break;
                    }
                case "BINT": // Building Business Interruptions
                    {
                        Building[] buildings = Building.GetArray("LocationID = ?", survey.LocationID);
                        foreach (Building building in buildings)
                        {
                            values.Add(building.BusinessInterruption);
                        }
                        break;
                    }
                case "BLOC": // Building Location Occupancy
                    {
                        Building[] buildings = Building.GetArray("LocationID = ?", survey.LocationID);
                        foreach (Building building in buildings)
                        {
                            values.Add(building.LocationOccupancy);
                        }
                        break;
                    }
                case "SREV": // Survey Flagged for Review
                    {
                        values.Add(survey.Review);
                        break;
                    }
                case "SSDS": // Date Surveyed
                    {
                        TimeSpan span = DateTime.Today - survey.SurveyedDate;
                        values.Add(span.Days);
                        break;
                    }
                case "SSGR": // Survey Grading
                    {
                        values.Add(survey.GradingCode);
                        break;
                    }
                case "PNOV": // Policy # of Vehicles
                    {
                        Coverage[] coverages = Coverage.GetArray("TypeCode = ? && Policy.SurveyPolicyCoverages.SurveyID = ?", "NU", survey.ID);
                        foreach (Coverage coverage in coverages)
                        {
                            int strValue = 0;
                            try
                            {
                                strValue = Convert.ToInt32(coverage.Value);
                            }
                            catch (Exception)
                            {
                                //swallow the exception
                            }

                            values.Add(strValue.ToString());
                        }
                        break;
                    }
                case "SREC": // Survey Rec Classifications
                    {
                        SurveyRecommendation[] recs = SurveyRecommendation.GetArray("SurveyID = ?", survey.ID);
                        foreach (SurveyRecommendation rec in recs)
                        {
                            values.Add(rec.RecClassification.TypeCode);
                        }
                        break;
                    }
                case "CONN": // Assigned Consultant Usernames (they are unique)
                    {
                        values.Add((survey.AssignedUser != null) ? survey.AssignedUser.Username : string.Empty);
                        break;
                    }
                //Squish# 21367: Grouping of SIC Codes in 2 digits
                case "ISI2": // Insured SIC Code 2 Digit (Group by SIC Code)
                    {
                        values.Add(survey.Insured.SICCode.Substring(0,2));
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Filter field '" + this.FilterFieldCode + "' was not expected.");
                    }
            }

            // return the values as an array
            return values.ToArray();
        }

        private bool ValueIsMatch(object value)
        {
            // convert the passed value and field compare value to the data type of this field
            value = Convert.ChangeType(value, this.FilterField.SystemTypeCode);
            object compareValue = Convert.ChangeType(this.CompareValue, this.FilterField.SystemTypeCode);

            // the value must be comparable
            if (!(value is IComparable))
            {
                throw new ArgumentException("Value '" + value + "' of type '" + value.GetType() + "' does not implement IComparable.");
            }

            // get the result value of the comparison
            // we'll need this for almost all operators below
            int compare = CaseInsensitiveComparer.Default.Compare(value, compareValue);

            // return true if value is a match based on the operator
            switch (this.FilterOperatorCode)
            {
                case "EQ": // Equal To
                    {
                        return (compare == 0);
                    }
                case "NE": // Not Equal To
                    {
                        return (compare != 0);
                    }
                case "GT": // Greater Than
                    {
                        return (compare > 0);
                    }
                case "GE": // Greater Than or Equal To
                    {
                        return (compare >= 0);
                    }
                case "LT": // Less Than
                    {
                        return (compare < 0);
                    }
                case "LE": // Less Than or Equal To
                    {
                        return (compare <= 0);
                    }
                case "SW": // Starts With
                    {
                        string str1 = (value as string);
                        string str2 = (compareValue as string);
                        string pattern = "^" + Regex.Escape(str2);

                        return Regex.IsMatch(str1, pattern, RegexOptions.IgnoreCase);
                    }
                case "EW": // Ends With
                    {
                        string str1 = (value as string);
                        string str2 = (compareValue as string);
                        string pattern = Regex.Escape(str2) + "$";

                        return Regex.IsMatch(str1, pattern, RegexOptions.IgnoreCase);
                    }
                case "CT": // Contains
                    {
                        string str1 = (value as string);
                        string str2 = (compareValue as string);
                        string pattern = Regex.Escape(str2);

                        return Regex.IsMatch(str1, pattern, RegexOptions.IgnoreCase);
                    }
                default:
                    {
                        throw new NotSupportedException("Operator '" + this.FilterOperator.Name + "' was not expected.");
                    }
            }
        }

		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _id;
		private Guid _filterID;
		private string _filterFieldCode;
		private string _filterOperatorCode;
		private string _compareValue = String.Empty;
		private string _displayValue;
		private int _orderIndex;
		private ObjectHolder _filterFieldHolder = null;
		private ObjectHolder _filterHolder = null;
		private ObjectHolder _filterOperatorHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Filter Condition ID.
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets or sets the Filter.
		/// </summary>
		public Guid FilterID
		{
			get { return _filterID; }
			set
			{
				VerifyWritable();
				_filterID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Filter Field.
		/// </summary>
		public string FilterFieldCode
		{
			get { return _filterFieldCode; }
			set
			{
				VerifyWritable();
				_filterFieldCode = value;
			}
		}

		/// <summary>
		/// Gets or sets the Filter Operator.
		/// </summary>
		public string FilterOperatorCode
		{
			get { return _filterOperatorCode; }
			set
			{
				VerifyWritable();
				_filterOperatorCode = value;
			}
		}

		/// <summary>
		/// Gets or sets the Compare Value. Null value is 'String.Empty'.
		/// </summary>
		public string CompareValue
		{
			get { return _compareValue; }
			set
			{
				VerifyWritable();
				_compareValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the Display Value.
		/// </summary>
		public string DisplayValue
		{
			get { return _displayValue; }
			set
			{
				VerifyWritable();
				_displayValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the Order Index.
		/// </summary>
		public int OrderIndex
		{
			get { return _orderIndex; }
			set
			{
				VerifyWritable();
				_orderIndex = value;
			}
		}

		/// <summary>
		/// Gets the instance of a FilterField object related to this entity.
		/// </summary>
		public FilterField FilterField
		{
			get
			{
				_filterFieldHolder.Key = _filterFieldCode;
				return (FilterField)_filterFieldHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Filter object related to this entity.
		/// </summary>
		public Filter Filter
		{
			get
			{
				_filterHolder.Key = _filterID;
				return (Filter)_filterHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a FilterOperator object related to this entity.
		/// </summary>
		public FilterOperator FilterOperator
		{
			get
			{
				_filterOperatorHolder.Key = _filterOperatorCode;
				return (FilterOperator)_filterOperatorHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_filterID": return _filterID;
					case "_filterFieldCode": return _filterFieldCode;
					case "_filterOperatorCode": return _filterOperatorCode;
					case "_compareValue": return _compareValue;
					case "_displayValue": return _displayValue;
					case "_orderIndex": return _orderIndex;
					case "_filterFieldHolder": return _filterFieldHolder;
					case "_filterHolder": return _filterHolder;
					case "_filterOperatorHolder": return _filterOperatorHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (Guid)value; break;
					case "_filterID": _filterID = (Guid)value; break;
					case "_filterFieldCode": _filterFieldCode = (string)value; break;
					case "_filterOperatorCode": _filterOperatorCode = (string)value; break;
					case "_compareValue": _compareValue = (string)value; break;
					case "_displayValue": _displayValue = (string)value; break;
					case "_orderIndex": _orderIndex = (int)value; break;
					case "_filterFieldHolder": _filterFieldHolder = (ObjectHolder)value; break;
					case "_filterHolder": _filterHolder = (ObjectHolder)value; break;
					case "_filterOperatorHolder": _filterOperatorHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static FilterCondition Get(Guid id)
		{
			OPathQuery query = new OPathQuery(typeof(FilterCondition), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch FilterCondition entity with ID = '{0}'.", id));
			}
			return (FilterCondition)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static FilterCondition GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(FilterCondition), opathExpression);
			return (FilterCondition)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static FilterCondition GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(FilterCondition) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (FilterCondition)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static FilterCondition[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(FilterCondition), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static FilterCondition[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(FilterCondition) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (FilterCondition[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static FilterCondition[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(FilterCondition), opathExpression, opathSort);
            query.CommandTimeout.Equals(380);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public FilterCondition GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			FilterCondition clone = (FilterCondition)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ID, _id);
			Validator.Validate(Field.FilterID, _filterID);
			Validator.Validate(Field.FilterFieldCode, _filterFieldCode);
			Validator.Validate(Field.FilterOperatorCode, _filterOperatorCode);
			Validator.Validate(Field.CompareValue, _compareValue);
			Validator.Validate(Field.DisplayValue, _displayValue);
			Validator.Validate(Field.OrderIndex, _orderIndex);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Guid.Empty;
			_filterID = Guid.Empty;
			_filterFieldCode = null;
			_filterOperatorCode = null;
			_compareValue = null;
			_displayValue = null;
			_orderIndex = Int32.MinValue;
			_filterFieldHolder = null;
			_filterHolder = null;
			_filterOperatorHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the FilterCondition entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the FilterID field.
			/// </summary>
			FilterID,
			/// <summary>
			/// Represents the FilterFieldCode field.
			/// </summary>
			FilterFieldCode,
			/// <summary>
			/// Represents the FilterOperatorCode field.
			/// </summary>
			FilterOperatorCode,
			/// <summary>
			/// Represents the CompareValue field.
			/// </summary>
			CompareValue,
			/// <summary>
			/// Represents the DisplayValue field.
			/// </summary>
			DisplayValue,
			/// <summary>
			/// Represents the OrderIndex field.
			/// </summary>
			OrderIndex,
		}
		
		#endregion
	}
}