using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents an UserSetting entity, which maps to table 'UserSetting' in the database.
	/// </summary>
	public class UserSetting : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private UserSetting()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="userID">UserID of this UserSetting.</param>
		/// <param name="settingName">SettingName of this UserSetting.</param>
		public UserSetting(Guid userID, string settingName)
		{
			_userID = userID;
			_settingName = settingName;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _userID;
		private string _settingName;
		private string _settingValue;
		private int _valueType;
		private ObjectHolder _userHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the User.
		/// </summary>
		public Guid UserID
		{
			get { return _userID; }
		}

		/// <summary>
		/// Gets the Setting Name.
		/// </summary>
		public string SettingName
		{
			get { return _settingName; }
		}

		/// <summary>
		/// Gets or sets the Setting Value.
		/// </summary>
		public string SettingValue
		{
			get { return _settingValue; }
			set
			{
				VerifyWritable();
				_settingValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the Value Type.
		/// </summary>
		public int ValueType
		{
			get { return _valueType; }
			set
			{
				VerifyWritable();
				_valueType = value;
			}
		}

		/// <summary>
		/// Gets the instance of a User object related to this entity.
		/// </summary>
		public User User
		{
			get
			{
				_userHolder.Key = _userID;
				return (User)_userHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_userID": return _userID;
					case "_settingName": return _settingName;
					case "_settingValue": return _settingValue;
					case "_valueType": return _valueType;
					case "_userHolder": return _userHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_userID": _userID = (Guid)value; break;
					case "_settingName": _settingName = (string)value; break;
					case "_settingValue": _settingValue = (string)value; break;
					case "_valueType": _valueType = (int)value; break;
					case "_userHolder": _userHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="userID">UserID of the entity to fetch.</param>
		/// <param name="settingName">SettingName of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static UserSetting Get(Guid userID, string settingName)
		{
			OPathQuery query = new OPathQuery(typeof(UserSetting), "UserID = ? && SettingName = ?");
			object entity = DB.Engine.GetObject(query, userID, settingName);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch UserSetting entity with UserID = '{0}' and SettingName = '{1}'.", userID, settingName));
			}
			return (UserSetting)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static UserSetting GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(UserSetting), opathExpression);
			return (UserSetting)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static UserSetting GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(UserSetting) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (UserSetting)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static UserSetting[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(UserSetting), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static UserSetting[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(UserSetting) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (UserSetting[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static UserSetting[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(UserSetting), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public UserSetting GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			UserSetting clone = (UserSetting)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.UserID, _userID);
			Validator.Validate(Field.SettingName, _settingName);
			Validator.Validate(Field.SettingValue, _settingValue);
			Validator.Validate(Field.ValueType, _valueType);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_userID = Guid.Empty;
			_settingName = null;
			_settingValue = null;
			_valueType = Int32.MinValue;
			_userHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the UserSetting entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the UserID field.
			/// </summary>
			UserID,
			/// <summary>
			/// Represents the SettingName field.
			/// </summary>
			SettingName,
			/// <summary>
			/// Represents the SettingValue field.
			/// </summary>
			SettingValue,
			/// <summary>
			/// Represents the ValueType field.
			/// </summary>
			ValueType,
		}
		
		#endregion
	}
}