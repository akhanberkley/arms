using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a SurveyPolicyCoverage entity, which maps to table 'Survey_PolicyCoverage' in the database.
    /// </summary>
    public class SurveyPolicyCoverage : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private SurveyPolicyCoverage()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="surveyID">SurveyID of this SurveyPolicyCoverage.</param>
        /// <param name="locationID">LocationID of this SurveyPolicyCoverage.</param>
        /// <param name="policyID">PolicyID of this SurveyPolicyCoverage.</param>
        /// <param name="coverageNameID">CoverageNameID of this SurveyPolicyCoverage.</param>
        public SurveyPolicyCoverage(Guid surveyID, Guid locationID, Guid policyID, Guid coverageNameID)
        {
            _surveyID = surveyID;
            _locationID = locationID;
            _policyID = policyID;
            _coverageNameID = coverageNameID;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _surveyID;
        private Guid _locationID;
        private Guid _policyID;
        private Guid _coverageNameID;
        private string _reportTypeCode = String.Empty;
        private bool _active = false;
        private ObjectHolder _coverageNameHolder = null;
        private ObjectHolder _locationHolder = null;
        private ObjectHolder _policyHolder = null;
        private ObjectHolder _reportTypeHolder = null;
        private ObjectHolder _surveyHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Survey.
        /// </summary>
        public Guid SurveyID
        {
            get { return _surveyID; }
        }

        /// <summary>
        /// Gets the Location.
        /// </summary>
        public Guid LocationID
        {
            get { return _locationID; }
        }

        /// <summary>
        /// Gets the Policy.
        /// </summary>
        public Guid PolicyID
        {
            get { return _policyID; }
        }

        /// <summary>
        /// Gets the Coverage Name.
        /// </summary>
        public Guid CoverageNameID
        {
            get { return _coverageNameID; }
        }

        /// <summary>
        /// Gets or sets the Report Type. Null value is 'String.Empty'.
        /// </summary>
        public string ReportTypeCode
        {
            get { return _reportTypeCode; }
            set
            {
                VerifyWritable();
                _reportTypeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Active. Null value is 'false'.
        /// </summary>
        public bool Active
        {
            get { return _active; }
            set
            {
                VerifyWritable();
                _active = value;
            }
        }

        /// <summary>
        /// Gets the instance of a CoverageName object related to this entity.
        /// </summary>
        public CoverageName CoverageName
        {
            get
            {
                _coverageNameHolder.Key = _coverageNameID;
                return (CoverageName)_coverageNameHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Location object related to this entity.
        /// </summary>
        public Location Location
        {
            get
            {
                _locationHolder.Key = _locationID;
                return (Location)_locationHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Policy object related to this entity.
        /// </summary>
        public Policy Policy
        {
            get
            {
                _policyHolder.Key = _policyID;
                return (Policy)_policyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a ReportType object related to this entity.
        /// </summary>
        public ReportType ReportType
        {
            get
            {
                _reportTypeHolder.Key = _reportTypeCode;
                return (ReportType)_reportTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Survey object related to this entity.
        /// </summary>
        public Survey Survey
        {
            get
            {
                _surveyHolder.Key = _surveyID;
                return (Survey)_surveyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_surveyID": return _surveyID;
                    case "_locationID": return _locationID;
                    case "_policyID": return _policyID;
                    case "_coverageNameID": return _coverageNameID;
                    case "_reportTypeCode": return _reportTypeCode;
                    case "_active": return _active;
                    case "_coverageNameHolder": return _coverageNameHolder;
                    case "_locationHolder": return _locationHolder;
                    case "_policyHolder": return _policyHolder;
                    case "_reportTypeHolder": return _reportTypeHolder;
                    case "_surveyHolder": return _surveyHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_surveyID": _surveyID = (Guid)value; break;
                    case "_locationID": _locationID = (Guid)value; break;
                    case "_policyID": _policyID = (Guid)value; break;
                    case "_coverageNameID": _coverageNameID = (Guid)value; break;
                    case "_reportTypeCode": _reportTypeCode = (string)value; break;
                    case "_active": _active = (bool)value; break;
                    case "_coverageNameHolder": _coverageNameHolder = (ObjectHolder)value; break;
                    case "_locationHolder": _locationHolder = (ObjectHolder)value; break;
                    case "_policyHolder": _policyHolder = (ObjectHolder)value; break;
                    case "_reportTypeHolder": _reportTypeHolder = (ObjectHolder)value; break;
                    case "_surveyHolder": _surveyHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="surveyID">SurveyID of the entity to fetch.</param>
        /// <param name="locationID">LocationID of the entity to fetch.</param>
        /// <param name="policyID">PolicyID of the entity to fetch.</param>
        /// <param name="coverageNameID">CoverageNameID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static SurveyPolicyCoverage Get(Guid surveyID, Guid locationID, Guid policyID, Guid coverageNameID)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyPolicyCoverage), "SurveyID = ? && LocationID = ? && PolicyID = ? && CoverageNameID = ?");
            object entity = DB.Engine.GetObject(query, surveyID, locationID, policyID, coverageNameID);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch SurveyPolicyCoverage entity with SurveyID = '{0}' and LocationID = '{1}' and PolicyID = '{2}' and CoverageNameID = '{3}'.", surveyID, locationID, policyID, coverageNameID));
            }
            return (SurveyPolicyCoverage)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyPolicyCoverage GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyPolicyCoverage), opathExpression);
            return (SurveyPolicyCoverage)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyPolicyCoverage GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyPolicyCoverage))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyPolicyCoverage)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyPolicyCoverage[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyPolicyCoverage), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyPolicyCoverage[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyPolicyCoverage))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyPolicyCoverage[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyPolicyCoverage[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyPolicyCoverage), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public SurveyPolicyCoverage GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            SurveyPolicyCoverage clone = (SurveyPolicyCoverage)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.SurveyID, _surveyID);
            Validator.Validate(Field.LocationID, _locationID);
            Validator.Validate(Field.PolicyID, _policyID);
            Validator.Validate(Field.CoverageNameID, _coverageNameID);
            Validator.Validate(Field.ReportTypeCode, _reportTypeCode);
            Validator.Validate(Field.Active, _active);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _surveyID = Guid.Empty;
            _locationID = Guid.Empty;
            _policyID = Guid.Empty;
            _coverageNameID = Guid.Empty;
            _reportTypeCode = null;
            _active = false;
            _coverageNameHolder = null;
            _locationHolder = null;
            _policyHolder = null;
            _reportTypeHolder = null;
            _surveyHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the SurveyPolicyCoverage entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the SurveyID field.
            /// </summary>
            SurveyID,
            /// <summary>
            /// Represents the LocationID field.
            /// </summary>
            LocationID,
            /// <summary>
            /// Represents the PolicyID field.
            /// </summary>
            PolicyID,
            /// <summary>
            /// Represents the CoverageNameID field.
            /// </summary>
            CoverageNameID,
            /// <summary>
            /// Represents the ReportTypeCode field.
            /// </summary>
            ReportTypeCode,
            /// <summary>
            /// Represents the Active field.
            /// </summary>
            Active,
        }

        #endregion
    }
}