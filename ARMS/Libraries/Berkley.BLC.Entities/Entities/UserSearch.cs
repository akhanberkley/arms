using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents an UserSearch entity, which maps to table 'UserSearch' in the database.
    /// </summary>
    public class UserSearch : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private UserSearch()
        {
        }

        /// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this UserSearch.</param>
		public UserSearch(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _userID;
        private string _name;
        private string _filterExpression;
        private string _filterParameters;
        private int _displayOrder;
        private bool _isRequired;
        private ObjectHolder _userHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the User Search.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the User.
        /// </summary>
        public Guid UserID
        {
            get { return _userID; }
            set
            {
                VerifyWritable();
                _userID = value;
            }
        }

        /// <summary>
        /// Gets or sets the User Search.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                VerifyWritable();
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Filter Expression.
        /// </summary>
        public string FilterExpression
        {
            get { return _filterExpression; }
            set
            {
                VerifyWritable();
                _filterExpression = value;
            }
        }

        /// <summary>
        /// Gets or sets the Filter Parameters.
        /// </summary>
        public string FilterParameters
        {
            get { return _filterParameters; }
            set
            {
                VerifyWritable();
                _filterParameters = value;
            }
        }

        /// <summary>
        /// Gets or sets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
            set
            {
                VerifyWritable();
                _displayOrder = value;
            }
        }

        /// <summary>
        /// Gets or sets the Is Required.
        /// </summary>
        public bool IsRequired
        {
            get { return _isRequired; }
            set
            {
                VerifyWritable();
                _isRequired = value;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User User
        {
            get
            {
                _userHolder.Key = _userID;
                return (User)_userHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_userID": return _userID;
                    case "_name": return _name;
                    case "_filterExpression": return _filterExpression;
                    case "_filterParameters": return _filterParameters;
                    case "_displayOrder": return _displayOrder;
                    case "_isRequired": return _isRequired;
                    case "_userHolder": return _userHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_userID": _userID = (Guid)value; break;
                    case "_name": _name = (string)value; break;
                    case "_filterExpression": _filterExpression = (string)value; break;
                    case "_filterParameters": _filterParameters = (string)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    case "_isRequired": _isRequired = (bool)value; break;
                    case "_userHolder": _userHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static UserSearch Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(UserSearch), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch UserSearch entity with ID = '{0}'.", id));
            }
            return (UserSearch)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static UserSearch GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(UserSearch), opathExpression);
            return (UserSearch)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static UserSearch GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(UserSearch))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (UserSearch)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static UserSearch[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(UserSearch), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static UserSearch[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(UserSearch))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (UserSearch[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static UserSearch[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(UserSearch), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public UserSearch GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            UserSearch clone = (UserSearch)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.UserID, _userID);
            Validator.Validate(Field.Name, _name);
            Validator.Validate(Field.FilterExpression, _filterExpression);
            Validator.Validate(Field.FilterParameters, _filterParameters);
            Validator.Validate(Field.DisplayOrder, _displayOrder);
            Validator.Validate(Field.IsRequired, _isRequired);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _userID = Guid.Empty;
            _name = null;
            _filterExpression = null;
            _filterParameters = null;
            _displayOrder = Int32.MinValue;
            _isRequired = false;
            _userHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the UserSearch entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the UserID field.
            /// </summary>
            UserID,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the FilterExpression field.
            /// </summary>
            FilterExpression,
            /// <summary>
            /// Represents the FilterParameters field.
            /// </summary>
            FilterParameters,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
            /// <summary>
            /// Represents the IsRequired field.
            /// </summary>
            IsRequired,
        }

        #endregion
    }
}