using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a FilterField entity, which maps to table 'FilterField' in the database.
	/// </summary>
	public class FilterField : IObjectHelper
	{
        private TypeCode _typeCode = TypeCode.Empty;

        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private FilterField()
        {
        }

        /// <summary>
        /// Returns a TypeCode that represents the data type for values in this field.
        /// This can be used to easily convert strings to the required type for this field.
        /// </summary>
        public TypeCode SystemTypeCode
        {
            get
            {
                if (_typeCode == TypeCode.Empty)
                {
                    _typeCode = (TypeCode)Enum.Parse(typeof(TypeCode), this.DotNetDataType, true);
                }
                return _typeCode;
            }
        }


        #region --- Generated Members ---

        private string _code;
        private string _surveyStatusTypeCode;
        private string _name;
        private string _sourceTable;
        private string _sourceColumn;
        private string _dotNetDataType;
        private bool _nullAllowed;
        private string _comboEntity = String.Empty;
        private string _textField = String.Empty;
        private string _valueField = String.Empty;
        private string _filterExpression = String.Empty;
        private string _sortExpression = String.Empty;
        private int _displayOrder;
        private int _flexibleDisplayWidth = Int32.MinValue;
        private ObjectHolder _surveyStatusTypeHolder = null;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Filter Field.
        /// </summary>
        public string Code
        {
            get { return _code; }
        }

        /// <summary>
        /// Gets the Survey Status Type.
        /// </summary>
        public string SurveyStatusTypeCode
        {
            get { return _surveyStatusTypeCode; }
        }

        /// <summary>
        /// Gets the Filter Field.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Gets the Source Table.
        /// </summary>
        public string SourceTable
        {
            get { return _sourceTable; }
        }

        /// <summary>
        /// Gets the Source Column.
        /// </summary>
        public string SourceColumn
        {
            get { return _sourceColumn; }
        }

        /// <summary>
        /// Gets the Dot Net Data Type.
        /// </summary>
        public string DotNetDataType
        {
            get { return _dotNetDataType; }
        }

        /// <summary>
        /// Gets the Null Allowed.
        /// </summary>
        public bool NullAllowed
        {
            get { return _nullAllowed; }
        }

        /// <summary>
        /// Gets the Combo Entity. Null value is 'String.Empty'.
        /// </summary>
        public string ComboEntity
        {
            get { return _comboEntity; }
        }

        /// <summary>
        /// Gets the Text Field. Null value is 'String.Empty'.
        /// </summary>
        public string TextField
        {
            get { return _textField; }
        }

        /// <summary>
        /// Gets the Value Field. Null value is 'String.Empty'.
        /// </summary>
        public string ValueField
        {
            get { return _valueField; }
        }

        /// <summary>
        /// Gets the Filter Expression. Null value is 'String.Empty'.
        /// </summary>
        public string FilterExpression
        {
            get { return _filterExpression; }
        }

        /// <summary>
        /// Gets the Sort Expression. Null value is 'String.Empty'.
        /// </summary>
        public string SortExpression
        {
            get { return _sortExpression; }
        }

        /// <summary>
        /// Gets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
        }
        /// <summary>
        /// Gets the Flexible Display Width. Null value is 'Int32.MinValue'.
        /// </summary>
        public int FlexibleDisplayWidth
        {
            get { return _flexibleDisplayWidth; }
        }


        /// <summary>
        /// Gets the instance of a SurveyStatusType object related to this entity.
        /// </summary>
        public SurveyStatusType SurveyStatusType
        {
            get
            {
                _surveyStatusTypeHolder.Key = _surveyStatusTypeCode;
                return (SurveyStatusType)_surveyStatusTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_code": return _code;
                    case "_surveyStatusTypeCode": return _surveyStatusTypeCode;
                    case "_name": return _name;
                    case "_sourceTable": return _sourceTable;
                    case "_sourceColumn": return _sourceColumn;
                    case "_dotNetDataType": return _dotNetDataType;
                    case "_nullAllowed": return _nullAllowed;
                    case "_comboEntity": return _comboEntity;
                    case "_textField": return _textField;
                    case "_valueField": return _valueField;
                    case "_filterExpression": return _filterExpression;
                    case "_sortExpression": return _sortExpression;
                    case "_displayOrder": return _displayOrder;
                    case "_flexibleDisplayWidth": return _flexibleDisplayWidth;
                    case "_surveyStatusTypeHolder": return _surveyStatusTypeHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_code": _code = (string)value; break;
                    case "_surveyStatusTypeCode": _surveyStatusTypeCode = (string)value; break;
                    case "_name": _name = (string)value; break;
                    case "_sourceTable": _sourceTable = (string)value; break;
                    case "_sourceColumn": _sourceColumn = (string)value; break;
                    case "_dotNetDataType": _dotNetDataType = (string)value; break;
                    case "_nullAllowed": _nullAllowed = (bool)value; break;
                    case "_comboEntity": _comboEntity = (string)value; break;
                    case "_textField": _textField = (string)value; break;
                    case "_valueField": _valueField = (string)value; break;
                    case "_filterExpression": _filterExpression = (string)value; break;
                    case "_sortExpression": _sortExpression = (string)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    case "_flexibleDisplayWidth": _flexibleDisplayWidth = (int)value; break;
                    case "_surveyStatusTypeHolder": _surveyStatusTypeHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="code">Code of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static FilterField Get(string code)
        {
            OPathQuery query = new OPathQuery(typeof(FilterField), "Code = ?");
            object entity = DB.Engine.GetObject(query, code);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch FilterField entity with Code = '{0}'.", code));
            }
            return (FilterField)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static FilterField GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(FilterField), opathExpression);
            return (FilterField)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static FilterField GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(FilterField))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (FilterField)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static FilterField[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(FilterField), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static FilterField[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(FilterField))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (FilterField[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static FilterField[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(FilterField), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Determines if two FilterField objects have the same semantic value.
        /// </summary>
        /// <param name="one">The first object to compare.</param>
        /// <param name="two">The second object to compare.</param>
        /// <returns>True if they are semantically identical, false otherwise.</returns>
        public static bool operator ==(FilterField one, FilterField two)
        {
            if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
            {
                return true;
            }
            else if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
            {
                return false;
            }
            else
            {
                return (one.CompareTo(two) == 0);
            }
        }

        /// <summary>
        /// Determines if two FilterField objects have different semantic values.
        /// </summary>
        /// <param name="one">The first object to compare.</param>
        /// <param name="two">The second object to compare.</param>
        /// <returns>True if they are semantically dissimilar, false otherwise.</returns>
        public static bool operator !=(FilterField one, FilterField two)
        {
            if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
            {
                return false;
            }
            if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
            {
                return true;
            }
            else
            {
                return (one.CompareTo(two) != 0);
            }
        }

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="value">An object to compare with this instance.</param>
        /// <returns>Less than zero if this instance is less than value. 
        /// Zero if this instance is equal to value.
        /// Greater than zero if this instance is greater than value.</returns>
        public int CompareTo(object value)
        {
            if (value == null)
            {
                return 1;
            }
            if (value is FilterField)
            {
                FilterField obj = (FilterField)value;
                return (this.Code.CompareTo(obj.Code));
            }
            throw new ArgumentException("Value is not the correct type.");
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode()
        {
            return this.Code.GetHashCode();
        }

        /// <summary>
        /// Determines whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="value">The object to compare with the current instance. </param>
        /// <returns>true if the specified object is equal to the current instance; otherwise, false.</returns>
        public override bool Equals(object value)
        {
            return (this == (value as FilterField));
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _code = null;
            _surveyStatusTypeCode = null;
            _name = null;
            _sourceTable = null;
            _sourceColumn = null;
            _dotNetDataType = null;
            _nullAllowed = false;
            _comboEntity = null;
            _textField = null;
            _valueField = null;
            _filterExpression = null;
            _sortExpression = null;
            _displayOrder = Int32.MinValue;
            _flexibleDisplayWidth = Int32.MinValue;
            _surveyStatusTypeHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the FilterField entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the Code field.
            /// </summary>
            Code,
            /// <summary>
            /// Represents the SurveyStatusTypeCode field.
            /// </summary>
            SurveyStatusTypeCode,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the SourceTable field.
            /// </summary>
            SourceTable,
            /// <summary>
            /// Represents the SourceColumn field.
            /// </summary>
            SourceColumn,
            /// <summary>
            /// Represents the DotNetDataType field.
            /// </summary>
            DotNetDataType,
            /// <summary>
            /// Represents the NullAllowed field.
            /// </summary>
            NullAllowed,
            /// <summary>
            /// Represents the ComboEntity field.
            /// </summary>
            ComboEntity,
            /// <summary>
            /// Represents the TextField field.
            /// </summary>
            TextField,
            /// <summary>
            /// Represents the ValueField field.
            /// </summary>
            ValueField,
            /// <summary>
            /// Represents the FilterExpression field.
            /// </summary>
            FilterExpression,
            /// <summary>
            /// Represents the SortExpression field.
            /// </summary>
            SortExpression,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
            /// <summary>
            /// Represents the FlexibleDisplayWidth field.
            /// </summary>
            FlexibleDisplayWidth,
        }

        #endregion
    }
}