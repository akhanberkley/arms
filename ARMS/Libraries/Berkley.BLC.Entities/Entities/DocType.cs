using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a DocType entity, which maps to table 'DocType' in the database.
	/// </summary>
	public class DocType : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private DocType()
		{
		}


		#region --- Generated Members ---

		private string _code;
		private Guid _companyID;
		private string _description;
		private int _displayOrder;
		private ObjectHolder _companyHolder = null;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Doc Type.
		/// </summary>
		public string Code
		{
			get { return _code; }
		}

		/// <summary>
		/// Gets the Company.
		/// </summary>
		public Guid CompanyID
		{
			get { return _companyID; }
		}

		/// <summary>
		/// Gets the Doc Type.
		/// </summary>
		public string Description
		{
			get { return _description; }
		}

		/// <summary>
		/// Gets the Display Order.
		/// </summary>
		public int DisplayOrder
		{
			get { return _displayOrder; }
		}

		/// <summary>
		/// Gets the instance of a Company object related to this entity.
		/// </summary>
		public Company Company
		{
			get
			{
				_companyHolder.Key = _companyID;
				return (Company)_companyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_code": return _code;
					case "_companyID": return _companyID;
					case "_description": return _description;
					case "_displayOrder": return _displayOrder;
					case "_companyHolder": return _companyHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_code": _code = (string)value; break;
					case "_companyID": _companyID = (Guid)value; break;
					case "_description": _description = (string)value; break;
					case "_displayOrder": _displayOrder = (int)value; break;
					case "_companyHolder": _companyHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="code">Code of the entity to fetch.</param>
		/// <param name="companyID">CompanyID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static DocType Get(string code, Guid companyID)
		{
			OPathQuery query = new OPathQuery(typeof(DocType), "Code = ? && CompanyID = ?");
			object entity = DB.Engine.GetObject(query, code, companyID);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch DocType entity with Code = '{0}' and CompanyID = '{1}'.", code, companyID));
			}
			return (DocType)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static DocType GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(DocType), opathExpression);
			return (DocType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static DocType GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(DocType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (DocType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static DocType[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(DocType), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static DocType[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(DocType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (DocType[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static DocType[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(DocType), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Determines if two DocType objects have the same semantic value.
		/// </summary>
		/// <param name="one">The first object to compare.</param>
		/// <param name="two">The second object to compare.</param>
		/// <returns>True if they are semantically identical, false otherwise.</returns>
		public static bool operator == (DocType one, DocType two) 
		{
			if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
			{
				return true;
			}
			else if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
			{
				return false;
			}
			else
			{
				return (one.CompareTo(two) == 0);
			}
		}

		/// <summary>
		/// Determines if two DocType objects have different semantic values.
		/// </summary>
		/// <param name="one">The first object to compare.</param>
		/// <param name="two">The second object to compare.</param>
		/// <returns>True if they are semantically dissimilar, false otherwise.</returns>
		public static bool operator != (DocType one, DocType two) 
		{
			if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
			{
				return false;
			}
			if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
			{
				return true;
			}
			else
			{
				return (one.CompareTo(two) != 0);
			}
		}
		
		/// <summary>
		/// Compares the current instance with another object of the same type.
		/// </summary>
		/// <param name="value">An object to compare with this instance.</param>
		/// <returns>Less than zero if this instance is less than value. 
		/// Zero if this instance is equal to value.
		/// Greater than zero if this instance is greater than value.</returns>
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (value is DocType)
			{
				DocType obj = (DocType)value;
				return (this.Code.CompareTo(obj.Code));
			}
			throw new ArgumentException("Value is not the correct type.");
		}

		/// <summary>
		/// Returns the hash code for this instance.
		/// </summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		public override int GetHashCode()
		{
			return this.Code.GetHashCode();
		}

		/// <summary>
		/// Determines whether this instance is equal to the specified object.
		/// </summary>
		/// <param name="value">The object to compare with the current instance. </param>
		/// <returns>true if the specified object is equal to the current instance; otherwise, false.</returns>
		public override bool Equals(object value)
		{
			return (this == (value as DocType));
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_code = null;
			_companyID = Guid.Empty;
			_description = null;
			_displayOrder = Int32.MinValue;
			_companyHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the DocType entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the Code field.
			/// </summary>
			Code,
			/// <summary>
			/// Represents the CompanyID field.
			/// </summary>
			CompanyID,
			/// <summary>
			/// Represents the Description field.
			/// </summary>
			Description,
			/// <summary>
			/// Represents the DisplayOrder field.
			/// </summary>
			DisplayOrder,
		}
		
		#endregion
	}
}