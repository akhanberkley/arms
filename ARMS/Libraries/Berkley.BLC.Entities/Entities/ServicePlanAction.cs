using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a ServicePlanAction entity, which maps to table 'ServicePlanAction' in the database.
	/// </summary>
	public class ServicePlanAction : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private ServicePlanAction()
		{
		}


		#region --- Generated Members ---

		private Guid _id;
		private Guid _companyID;
		private string _servicePlanStatusCode;
		private string _typeCode;
		private string _displayName;
		private int _displayOrder;
		private string _confirmMessage = String.Empty;
		private ObjectHolder _companyHolder = null;
		private ObjectHolder _servicePlanActionTypeHolder = null;
		private ObjectHolder _servicePlanStatusHolder = null;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Service Plan Action.
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets the Company.
		/// </summary>
		public Guid CompanyID
		{
			get { return _companyID; }
		}

		/// <summary>
		/// Gets the Service Plan Status.
		/// </summary>
		public string ServicePlanStatusCode
		{
			get { return _servicePlanStatusCode; }
		}

		/// <summary>
		/// Gets the Service Plan Action Type.
		/// </summary>
		public string TypeCode
		{
			get { return _typeCode; }
		}

		/// <summary>
		/// Gets the Display Name.
		/// </summary>
		public string DisplayName
		{
			get { return _displayName; }
		}

		/// <summary>
		/// Gets the Display Order.
		/// </summary>
		public int DisplayOrder
		{
			get { return _displayOrder; }
		}

		/// <summary>
		/// Gets the Confirm Message. Null value is 'String.Empty'.
		/// </summary>
		public string ConfirmMessage
		{
			get { return _confirmMessage; }
		}

		/// <summary>
		/// Gets the instance of a Company object related to this entity.
		/// </summary>
		public Company Company
		{
			get
			{
				_companyHolder.Key = _companyID;
				return (Company)_companyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a ServicePlanActionType object related to this entity.
		/// </summary>
		public ServicePlanActionType Type
		{
			get
			{
				_servicePlanActionTypeHolder.Key = _typeCode;
				return (ServicePlanActionType)_servicePlanActionTypeHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a ServicePlanStatus object related to this entity.
		/// </summary>
		public ServicePlanStatus ServicePlanStatus
		{
			get
			{
				_servicePlanStatusHolder.Key = _servicePlanStatusCode;
				return (ServicePlanStatus)_servicePlanStatusHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_companyID": return _companyID;
					case "_servicePlanStatusCode": return _servicePlanStatusCode;
					case "_typeCode": return _typeCode;
					case "_displayName": return _displayName;
					case "_displayOrder": return _displayOrder;
					case "_confirmMessage": return _confirmMessage;
					case "_companyHolder": return _companyHolder;
					case "_servicePlanActionTypeHolder": return _servicePlanActionTypeHolder;
					case "_servicePlanStatusHolder": return _servicePlanStatusHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (Guid)value; break;
					case "_companyID": _companyID = (Guid)value; break;
					case "_servicePlanStatusCode": _servicePlanStatusCode = (string)value; break;
					case "_typeCode": _typeCode = (string)value; break;
					case "_displayName": _displayName = (string)value; break;
					case "_displayOrder": _displayOrder = (int)value; break;
					case "_confirmMessage": _confirmMessage = (string)value; break;
					case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
					case "_servicePlanActionTypeHolder": _servicePlanActionTypeHolder = (ObjectHolder)value; break;
					case "_servicePlanStatusHolder": _servicePlanStatusHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static ServicePlanAction Get(Guid id)
		{
			OPathQuery query = new OPathQuery(typeof(ServicePlanAction), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch ServicePlanAction entity with ID = '{0}'.", id));
			}
			return (ServicePlanAction)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static ServicePlanAction GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ServicePlanAction), opathExpression);
			return (ServicePlanAction)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static ServicePlanAction GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(ServicePlanAction) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (ServicePlanAction)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ServicePlanAction[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ServicePlanAction), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ServicePlanAction[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(ServicePlanAction) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (ServicePlanAction[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ServicePlanAction[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ServicePlanAction), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Guid.Empty;
			_companyID = Guid.Empty;
			_servicePlanStatusCode = null;
			_typeCode = null;
			_displayName = null;
			_displayOrder = Int32.MinValue;
			_confirmMessage = null;
			_companyHolder = null;
			_servicePlanActionTypeHolder = null;
			_servicePlanStatusHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the ServicePlanAction entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the CompanyID field.
			/// </summary>
			CompanyID,
			/// <summary>
			/// Represents the ServicePlanStatusCode field.
			/// </summary>
			ServicePlanStatusCode,
			/// <summary>
			/// Represents the TypeCode field.
			/// </summary>
			TypeCode,
			/// <summary>
			/// Represents the DisplayName field.
			/// </summary>
			DisplayName,
			/// <summary>
			/// Represents the DisplayOrder field.
			/// </summary>
			DisplayOrder,
			/// <summary>
			/// Represents the ConfirmMessage field.
			/// </summary>
			ConfirmMessage,
		}
		
		#endregion
	}
}