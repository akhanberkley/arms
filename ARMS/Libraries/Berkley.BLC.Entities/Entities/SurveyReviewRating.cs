using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyReviewRating entity, which maps to table 'SurveyReviewRating' in the database.
	/// </summary>
	public class SurveyReviewRating : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyReviewRating()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this SurveyReviewRating.</param>
		public SurveyReviewRating(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _id;
		private Guid _surveyReviewID;
		private Guid _reviewCategoryTypeID;
		private Guid _reviewRatingTypeID = Guid.Empty;
		private string _comments = String.Empty;
		private ObjectHolder _reviewCategoryTypeHolder = null;
		private ObjectHolder _reviewRatingTypeHolder = null;
		private ObjectHolder _surveyReviewHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Survey Review Rating ID.
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets or sets the Survey Review.
		/// </summary>
		public Guid SurveyReviewID
		{
			get { return _surveyReviewID; }
			set
			{
				VerifyWritable();
				_surveyReviewID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Review Category Type.
		/// </summary>
		public Guid ReviewCategoryTypeID
		{
			get { return _reviewCategoryTypeID; }
			set
			{
				VerifyWritable();
				_reviewCategoryTypeID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Review Rating Type. Null value is 'Guid.Empty'.
		/// </summary>
		public Guid ReviewRatingTypeID
		{
			get { return _reviewRatingTypeID; }
			set
			{
				VerifyWritable();
				_reviewRatingTypeID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Comments. Null value is 'String.Empty'.
		/// </summary>
		public string Comments
		{
			get { return _comments; }
			set
			{
				VerifyWritable();
				_comments = value;
			}
		}

		/// <summary>
		/// Gets the instance of a ReviewCategoryType object related to this entity.
		/// </summary>
		public ReviewCategoryType ReviewCategoryType
		{
			get
			{
				_reviewCategoryTypeHolder.Key = _reviewCategoryTypeID;
				return (ReviewCategoryType)_reviewCategoryTypeHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a ReviewRatingType object related to this entity.
		/// </summary>
		public ReviewRatingType ReviewRatingType
		{
			get
			{
				_reviewRatingTypeHolder.Key = _reviewRatingTypeID;
				return (ReviewRatingType)_reviewRatingTypeHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a SurveyReview object related to this entity.
		/// </summary>
		public SurveyReview SurveyReview
		{
			get
			{
				_surveyReviewHolder.Key = _surveyReviewID;
				return (SurveyReview)_surveyReviewHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_surveyReviewID": return _surveyReviewID;
					case "_reviewCategoryTypeID": return _reviewCategoryTypeID;
					case "_reviewRatingTypeID": return _reviewRatingTypeID;
					case "_comments": return _comments;
					case "_reviewCategoryTypeHolder": return _reviewCategoryTypeHolder;
					case "_reviewRatingTypeHolder": return _reviewRatingTypeHolder;
					case "_surveyReviewHolder": return _surveyReviewHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (Guid)value; break;
					case "_surveyReviewID": _surveyReviewID = (Guid)value; break;
					case "_reviewCategoryTypeID": _reviewCategoryTypeID = (Guid)value; break;
					case "_reviewRatingTypeID": _reviewRatingTypeID = (Guid)value; break;
					case "_comments": _comments = (string)value; break;
					case "_reviewCategoryTypeHolder": _reviewCategoryTypeHolder = (ObjectHolder)value; break;
					case "_reviewRatingTypeHolder": _reviewRatingTypeHolder = (ObjectHolder)value; break;
					case "_surveyReviewHolder": _surveyReviewHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static SurveyReviewRating Get(Guid id)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyReviewRating), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch SurveyReviewRating entity with ID = '{0}'.", id));
			}
			return (SurveyReviewRating)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyReviewRating GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyReviewRating), opathExpression);
			return (SurveyReviewRating)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyReviewRating GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyReviewRating) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyReviewRating)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyReviewRating[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyReviewRating), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyReviewRating[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyReviewRating) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyReviewRating[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyReviewRating[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyReviewRating), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public SurveyReviewRating GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			SurveyReviewRating clone = (SurveyReviewRating)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ID, _id);
			Validator.Validate(Field.SurveyReviewID, _surveyReviewID);
			Validator.Validate(Field.ReviewCategoryTypeID, _reviewCategoryTypeID);
			Validator.Validate(Field.ReviewRatingTypeID, _reviewRatingTypeID);
			Validator.Validate(Field.Comments, _comments);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Guid.Empty;
			_surveyReviewID = Guid.Empty;
			_reviewCategoryTypeID = Guid.Empty;
			_reviewRatingTypeID = Guid.Empty;
			_comments = null;
			_reviewCategoryTypeHolder = null;
			_reviewRatingTypeHolder = null;
			_surveyReviewHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the SurveyReviewRating entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the SurveyReviewID field.
			/// </summary>
			SurveyReviewID,
			/// <summary>
			/// Represents the ReviewCategoryTypeID field.
			/// </summary>
			ReviewCategoryTypeID,
			/// <summary>
			/// Represents the ReviewRatingTypeID field.
			/// </summary>
			ReviewRatingTypeID,
			/// <summary>
			/// Represents the Comments field.
			/// </summary>
			Comments,
		}
		
		#endregion
	}
}