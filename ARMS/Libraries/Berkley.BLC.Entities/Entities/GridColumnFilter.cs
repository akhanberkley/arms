using System;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a GridColumnFilter entity, which maps to table 'GridColumnFilter' in the database.
    /// </summary>
    public class GridColumnFilter : IObjectHelper
    {
        #region Public Fields

        /// <summary>
        /// Represents a reminder column.
        /// </summary>
        public static readonly GridColumnFilter Reminder = GridColumnFilter.Get(new Guid("E1A4B8B6-7D40-40D0-AC4F-1856126324DF"));
        /// <summary>
        /// Represents an audit link column.
        /// </summary>
        public static readonly GridColumnFilter SurveyLink = GridColumnFilter.Get(new Guid("14DFAC67-C077-4838-95B9-9B875E180928"));
        /// <summary>
        /// Represents a policy link column.
        /// </summary>
        public static readonly GridColumnFilter InsuredLink = GridColumnFilter.Get(new Guid("B4C8E9C2-62D7-4207-92E4-C7A90CB2FE48"));
        /// <summary>
        /// Represents a Map CheckBox column.
        /// </summary>
        public static readonly GridColumnFilter MapCheckBox = GridColumnFilter.Get(new Guid("9D57FF22-AD2C-4500-8C0E-FE1D3CDD51BF"));

        #endregion

        private TypeCode _typeCode = TypeCode.Empty;

        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private GridColumnFilter()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this GridColumnFilter.</param>
        public GridColumnFilter(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Returns a TypeCode that represents the data type for values in this field.
        /// This can be used to easily convert strings to the required type for this field.
        /// </summary>
        public TypeCode SystemTypeCode
        {
            get
            {
                if (_typeCode == TypeCode.Empty)
                {
                    _typeCode = (TypeCode)Enum.Parse(typeof(TypeCode), this.DotNetDataType, true);
                }
                return _typeCode;
            }
        }


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private string _name;
        private string _filterExpression;
        private string _sortExpression;
        private string _dotNetDataType;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Grid Column Filter ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                VerifyWritable();
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Filter Expression.
        /// </summary>
        public string FilterExpression
        {
            get { return _filterExpression; }
            set
            {
                VerifyWritable();
                _filterExpression = value;
            }
        }

        /// <summary>
        /// Gets or sets the Sort Expression.
        /// </summary>
        public string SortExpression
        {
            get { return _sortExpression; }
            set
            {
                VerifyWritable();
                _sortExpression = value;
            }
        }

        /// <summary>
        /// Gets or sets the Dot Net Data Type.
        /// </summary>
        public string DotNetDataType
        {
            get { return _dotNetDataType; }
            set
            {
                VerifyWritable();
                _dotNetDataType = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_name": return _name;
                    case "_filterExpression": return _filterExpression;
                    case "_sortExpression": return _sortExpression;
                    case "_dotNetDataType": return _dotNetDataType;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_name": _name = (string)value; break;
                    case "_filterExpression": _filterExpression = (string)value; break;
                    case "_sortExpression": _sortExpression = (string)value; break;
                    case "_dotNetDataType": _dotNetDataType = (string)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static GridColumnFilter Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(GridColumnFilter), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch GridColumnFilter entity with ID = '{0}'.", id));
            }
            return (GridColumnFilter)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static GridColumnFilter GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(GridColumnFilter), opathExpression);
            return (GridColumnFilter)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static GridColumnFilter GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(GridColumnFilter))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (GridColumnFilter)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static GridColumnFilter[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(GridColumnFilter), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static GridColumnFilter[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(GridColumnFilter))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (GridColumnFilter[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static GridColumnFilter[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(GridColumnFilter), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public GridColumnFilter GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            GridColumnFilter clone = (GridColumnFilter)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.Name, _name);
            Validator.Validate(Field.FilterExpression, _filterExpression);
            Validator.Validate(Field.SortExpression, _sortExpression);
            Validator.Validate(Field.DotNetDataType, _dotNetDataType);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _name = null;
            _filterExpression = null;
            _sortExpression = null;
            _dotNetDataType = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the GridColumnFilter entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the FilterExpression field.
            /// </summary>
            FilterExpression,
            /// <summary>
            /// Represents the SortExpression field.
            /// </summary>
            SortExpression,
            /// <summary>
            /// Represents the DotNetDataType field.
            /// </summary>
            DotNetDataType,
        }

        #endregion
    }
}