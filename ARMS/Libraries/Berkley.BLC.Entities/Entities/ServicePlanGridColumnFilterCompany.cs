using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a ServicePlanGridColumnFilterCompany entity, which maps to table 'ServicePlanGridColumnFilter_Company' in the database.
    /// </summary>
    public class ServicePlanGridColumnFilterCompany : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private ServicePlanGridColumnFilterCompany()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="servicePlanGridColumnFilterID">ServicePlanGridColumnFilterID of this ServicePlanGridColumnFilterCompany.</param>
        /// <param name="companyID">CompanyID of this ServicePlanGridColumnFilterCompany.</param>
        public ServicePlanGridColumnFilterCompany(Guid servicePlanGridColumnFilterID, Guid companyID)
        {
            _servicePlanGridColumnFilterID = servicePlanGridColumnFilterID;
            _companyID = companyID;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _servicePlanGridColumnFilterID;
        private Guid _companyID;
        private int _displayOrder;
        private bool _display;
        private bool _required;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _servicePlanGridColumnFilterHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Service Plan Grid Column Filter.
        /// </summary>
        public Guid ServicePlanGridColumnFilterID
        {
            get { return _servicePlanGridColumnFilterID; }
        }

        /// <summary>
        /// Gets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets or sets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
            set
            {
                VerifyWritable();
                _displayOrder = value;
            }
        }

        /// <summary>
        /// Gets or sets the Display.
        /// </summary>
        public bool Display
        {
            get { return _display; }
            set
            {
                VerifyWritable();
                _display = value;
            }
        }

        /// <summary>
        /// Gets or sets the Required.
        /// </summary>
        public bool Required
        {
            get { return _required; }
            set
            {
                VerifyWritable();
                _required = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a ServicePlanGridColumnFilter object related to this entity.
        /// </summary>
        public ServicePlanGridColumnFilter ServicePlanGridColumnFilter
        {
            get
            {
                _servicePlanGridColumnFilterHolder.Key = _servicePlanGridColumnFilterID;
                return (ServicePlanGridColumnFilter)_servicePlanGridColumnFilterHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_servicePlanGridColumnFilterID": return _servicePlanGridColumnFilterID;
                    case "_companyID": return _companyID;
                    case "_displayOrder": return _displayOrder;
                    case "_display": return _display;
                    case "_required": return _required;
                    case "_companyHolder": return _companyHolder;
                    case "_servicePlanGridColumnFilterHolder": return _servicePlanGridColumnFilterHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_servicePlanGridColumnFilterID": _servicePlanGridColumnFilterID = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    case "_display": _display = (bool)value; break;
                    case "_required": _required = (bool)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_servicePlanGridColumnFilterHolder": _servicePlanGridColumnFilterHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="servicePlanGridColumnFilterID">ServicePlanGridColumnFilterID of the entity to fetch.</param>
        /// <param name="companyID">CompanyID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static ServicePlanGridColumnFilterCompany Get(Guid servicePlanGridColumnFilterID, Guid companyID)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanGridColumnFilterCompany), "ServicePlanGridColumnFilterID = ? && CompanyID = ?");
            object entity = DB.Engine.GetObject(query, servicePlanGridColumnFilterID, companyID);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch ServicePlanGridColumnFilterCompany entity with ServicePlanGridColumnFilterID = '{0}' and CompanyID = '{1}'.", servicePlanGridColumnFilterID, companyID));
            }
            return (ServicePlanGridColumnFilterCompany)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ServicePlanGridColumnFilterCompany GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanGridColumnFilterCompany), opathExpression);
            return (ServicePlanGridColumnFilterCompany)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ServicePlanGridColumnFilterCompany GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ServicePlanGridColumnFilterCompany))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ServicePlanGridColumnFilterCompany)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlanGridColumnFilterCompany[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanGridColumnFilterCompany), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlanGridColumnFilterCompany[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ServicePlanGridColumnFilterCompany))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ServicePlanGridColumnFilterCompany[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlanGridColumnFilterCompany[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanGridColumnFilterCompany), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public ServicePlanGridColumnFilterCompany GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            ServicePlanGridColumnFilterCompany clone = (ServicePlanGridColumnFilterCompany)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ServicePlanGridColumnFilterID, _servicePlanGridColumnFilterID);
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.DisplayOrder, _displayOrder);
            Validator.Validate(Field.Display, _display);
            Validator.Validate(Field.Required, _required);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _servicePlanGridColumnFilterID = Guid.Empty;
            _companyID = Guid.Empty;
            _displayOrder = Int32.MinValue;
            _display = false;
            _required = false;
            _companyHolder = null;
            _servicePlanGridColumnFilterHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the ServicePlanGridColumnFilterCompany entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ServicePlanGridColumnFilterID field.
            /// </summary>
            ServicePlanGridColumnFilterID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
            /// <summary>
            /// Represents the Display field.
            /// </summary>
            Display,
            /// <summary>
            /// Represents the Required field.
            /// </summary>
            Required,
	
        }

        #endregion
    }
}