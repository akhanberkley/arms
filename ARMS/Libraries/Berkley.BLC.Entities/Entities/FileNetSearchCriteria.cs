﻿using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a FileNetSearchCriteria entity, which maps to table 'FileNetSearchCriteria' in the database.
    /// </summary>
    public class FileNetSearchCriteria : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private FileNetSearchCriteria()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="companyID">CompanyID of this FileNetSearchCriteria.</param>
        /// <param name="parameterID">ParameterID of this FileNetSearchCriteria.</param>
        /// <param name="value">Value of this FileNetSearchCriteria.</param>
        public FileNetSearchCriteria(Guid companyID, int duration, string policySymbol, string docType)
        {
            _companyID = companyID;
            _duration = duration;
            _docType = docType;
            _policySymbol = policySymbol;
            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _companyID;
        private int _duration;
        private string _policySymbol;      
        private string _docType;                
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets the Parameter.
        /// </summary>
        public int Duration
        {
            get { return _duration; }
        }

        /// <summary>
        /// Gets the Value.
        /// </summary>
        public string PolicySymbol
        {
            get { return _policySymbol; }
        }

              
        /// <summary>
        /// Gets the Value.
        /// </summary>
        public string DocType
        {
            get { return _docType; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_companyID": return _companyID;
                    case "_duration": return _duration;
                    case "_policySymbol": return _policySymbol;                
                    case "_docType": return _docType;                
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_duration": _duration = (int)value; break;
                    case "_policySymbol": _policySymbol = (string)value; break;                 
                    case "_docType": _docType = (string)value; break;                                          
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="companyID">CompanyID of the entity to fetch.</param>
        /// <param name="parameterID">ParameterID of the entity to fetch.</param>
        /// <param name="value">Value of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static FileNetSearchCriteria Get(Guid companyID, int parameterID, string value)
        {
            OPathQuery query = new OPathQuery(typeof(FileNetSearchCriteria), "CompanyID = ? && ParameterID = ? && Value = ?");
            object entity = DB.Engine.GetObject(query, companyID, parameterID, value);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch FileNetSearchCriteria entity with CompanyID = '{0}' and ParameterID = '{1}' and Value = '{2}'.", companyID, parameterID, value));
            }
            return (FileNetSearchCriteria)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static FileNetSearchCriteria GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(FileNetSearchCriteria), opathExpression);
            return (FileNetSearchCriteria)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static FileNetSearchCriteria GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(FileNetSearchCriteria))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (FileNetSearchCriteria)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static FileNetSearchCriteria[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(FileNetSearchCriteria), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static FileNetSearchCriteria[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(FileNetSearchCriteria))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (FileNetSearchCriteria[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static FileNetSearchCriteria[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(FileNetSearchCriteria), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public FileNetSearchCriteria GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            FileNetSearchCriteria clone = (FileNetSearchCriteria)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.Duration, _duration);
            Validator.Validate(Field.PolicySymbol, _policySymbol);
            Validator.Validate(Field.DocType, _docType);            
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _companyID = Guid.Empty;
            _duration = Int32.MinValue;            
            _policySymbol = null;
            _docType = null;            
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the FileNetSearchCriteria entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the ParameterID field.
            /// </summary>
            Duration,       
            /// <summary>
            /// Represents the PolicySymbol field.
            /// </summary>
            PolicySymbol,
            /// <summary>
            /// Represents the DocType field.
            /// </summary>
            DocType,         
        }

        #endregion
    }
}