using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a PermissionSet entity, which maps to table 'PermissionSet' in the database.
    /// </summary>
    public class PermissionSet : IObjectHelper
    {
        private static Hashtable _companyActionMap = Hashtable.Synchronized(new Hashtable());
        private static Hashtable _companyPlanActionMap = Hashtable.Synchronized(new Hashtable());
        private Hashtable _actionSettings; // lazy-loaded from SurveyActionPermission table
        private bool _isMergedSet = false;

        /// <summary>
        /// None.
        /// </summary>
        public const int SETTING_NONE = 0;
        /// <summary>
        /// Owner.
        /// </summary>
        public const int SETTING_OWNER = 1;
        /// <summary>
        /// Others.
        /// </summary>
        public const int SETTING_OTHERS = 2;
        /// <summary>
        /// Vendors.
        /// </summary>
        public const int SETTING_VENDORS = 3;
        /// <summary>
        /// Owner and others.
        /// </summary>
        public const int SETTING_OWNER_AND_OTHERS = 4;
        /// <summary>
        /// Others and vendors.
        /// </summary>
        public const int SETTING_OTHERS_AND_VENDORS = 5;
        /// <summary>
        /// Owner and vendors.
        /// </summary>
        public const int SETTING_OWNER_AND_VENDORS = 6;
        /// <summary>
        /// All.
        /// </summary>
        public const int SETTING_ALL = 7;
        
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private PermissionSet()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this PermissionSet.</param>
        public PermissionSet(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Gets a flag value indicating if the User can view the Admin page.
        /// </summary>
        public bool CanViewAdminPage
        {
            get
            {
                return (this.CanAdminUserRoles
                    || this.CanAdminUserAccounts
                    || this.CanAdminFeeCompanies
                    || this.CanAdminTerritories
                    || this.CanAdminRules
                    || this.CanAdminLetters
                    || this.CanAdminReports
                    || this.CanAdminResultsDisplay
                    || this.CanAdminRecommendations
                    || this.CanAdminServicePlanFields
                    || this.CanAdminUnderwriters
                    || this.CanAdminLinks
                    || this.CanAdminActivityTimes
                    || this.CanAdminHelpfulHints);
            }
        }

        /// <summary>
        /// Gets a flag value indicating if the User can view the Surveys page.
        /// </summary>
        public bool CanViewMySurveysPage
        {
            get
            {
                return (this.CanWorkSurveys
                    || this.CanWorkReviews
                    || this.CanSearchSurveys
                    || this.CanCreateSurveys);
            }
        }

        /// <summary>
        /// Gets a flag value indicating if the User can view the Surveys page.
        /// </summary>
        public bool CanViewServicePlansPage
        {
            get
            {
                return (this.CanWorkServicePlans || this.CanCreateServicePlans);
            }
        }

        /// <summary>
        /// Gets a flag value indicating if the User can view the letter queues.
        /// </summary>
        public static bool CanViewLetterQueue(User currentUser)
        {
            // try to get the company-specific set mail recieved date action
            SurveyAction action = SurveyAction.GetOne("CompanyID = ? && SurveyStatusCode = ? && TypeCode = ?", currentUser.CompanyID, SurveyStatus.AssignedReview.Code, SurveyActionType.SetMailReceived.Code);

            if (action == null)
            {
                return false;
            }

            SurveyActionPermission permission = SurveyActionPermission.GetOne("PermissionSetID = ? && SurveyActionID = ? && (Setting = ? || Setting = ? || Setting = ?)", currentUser.Permissions.ID, action.ID, SETTING_OWNER, SETTING_OTHERS, SETTING_OWNER_AND_OTHERS);
            return (permission != null);
        }

        /// <summary>
        /// Determines if an SurveyActionType can be preformed on a Survey based on the configuration of this PermissionSet.
        /// </summary>
        /// <param name="actionType">Type of action to be preformed.</param>
        /// <param name="survey">The survey in consideration.</param>
        /// <param name="currentUser">User taking the action.</param>
        /// <returns>True if the user can take the action based on this PermissionSet; otherwise false.</returns>
        public bool CanTakeAction(SurveyActionType actionType, Survey survey, User currentUser)
        {
            // convert action type to company-specific action
            string key = actionType.Code + ":" + survey.StatusCode + ":" + currentUser.CompanyID;

            SurveyAction action;
            if (_companyActionMap.ContainsKey(key))
            {
                action = (SurveyAction)_companyActionMap[key];
            }
            else // not in cache
            {
                // try to get the company-specific action
                action = SurveyAction.GetOne("CompanyID = ? && SurveyStatusCode = ? && TypeCode = ?",
                    currentUser.CompanyID, survey.StatusCode, actionType.Code);

                // cache the results (which could be a null)
                _companyActionMap[key] = action;
            }

            // action cannot be taken by anyone if not defined for company
            if (action == null)
            {
                return false;
            }

            // call into our worker method to determine of this action is allowed
            bool surveyOwnedByUser = (survey.AssignedUserID == currentUser.ID);
            bool surveyOwnedByVendor = (survey.AssignedUser == null) ? false : survey.AssignedUser.IsFeeCompany;
            return CanTakeAction(action, survey.ServiceType, surveyOwnedByUser, surveyOwnedByVendor);
        }

        /// <summary>
        /// Determines if an SurveyAction can be preformed on a survey based on the configuration of this PermissionSet.
        /// </summary>
        /// <param name="action">Action to be preformed.</param>
        /// <param name="type">Service type of the survey in consideration.</param>
        /// <param name="surveyOwnedByUser">Flag indicating if the survey in consideration is owned by the user.</param>
        /// <param name="surveyOwnedByVendor">Flag indicating if the survey is assigned to a fee company.</param>
        /// <returns>True if the user can take this action based on this PermissionSet; otherwise false.</returns>
        public bool CanTakeAction(SurveyAction action, ServiceType type, bool surveyOwnedByUser, bool surveyOwnedByVendor)
        {
            string typeCode = ((action.SurveyStatusCode == SurveyStatus.UnassignedSurvey.Code ||
                                action.SurveyStatusCode == SurveyStatus.OnHoldSurvey.Code ||
                                action.SurveyStatusCode == SurveyStatus.CanceledSurvey.Code) ? string.Empty : type.Code);
            int setting = GetPermissionSettingForAction(action.ID, typeCode);
            switch (setting)
            {
                case SETTING_NONE: return false;
                case SETTING_OWNER: return (surveyOwnedByUser && !surveyOwnedByVendor);
                case SETTING_OTHERS: return (!surveyOwnedByUser && !surveyOwnedByVendor);
                case SETTING_VENDORS: return (surveyOwnedByVendor && !surveyOwnedByUser);
                case SETTING_OWNER_AND_OTHERS: return !surveyOwnedByVendor;
                case SETTING_OTHERS_AND_VENDORS: return (!surveyOwnedByUser || surveyOwnedByVendor);
                case SETTING_OWNER_AND_VENDORS: return (surveyOwnedByUser || surveyOwnedByVendor);
                case SETTING_ALL: return true;
                default:
                    {
                        throw new NotSupportedException("SurveyActionPermission setting value of '" + setting + "' was not expected.");
                    }
            }
        }

        /// <summary>
        /// Determines if an ServicePlanActionType can be preformed on a service plan based on the configuration of this PermissionSet.
        /// </summary>
        /// <param name="actionType">Type of action to be preformed.</param>
        /// <param name="servicePlan">The service plan in consideration.</param>
        /// <param name="currentUser">User taking the action.</param>
        /// <returns>True if the user can take the action based on this PermissionSet; otherwise false.</returns>
        public bool CanTakeAction(ServicePlanActionType actionType, ServicePlan servicePlan, User currentUser)
        {
            // convert action type to company-specific action
            string key = actionType.Code + ":" + servicePlan.StatusCode + ":" + currentUser.CompanyID;

            ServicePlanAction action;
            if (_companyPlanActionMap.ContainsKey(key))
            {
                action = (ServicePlanAction)_companyPlanActionMap[key];
            }
            else // not in cache
            {
                // try to get the company-specific action
                action = ServicePlanAction.GetOne("CompanyID = ? && ServicePlanStatusCode = ? && TypeCode = ?",
                    currentUser.CompanyID, servicePlan.StatusCode, actionType.Code);

                // cache the results (which could be a null)
                _companyPlanActionMap[key] = action;
            }

            // action cannot be taken by anyone if not defined for company
            if (action == null)
            {
                return false;
            }

            // call into our worker method to determine of this action is allowed
            bool planOwnedByUser = (servicePlan.AssignedUserID == currentUser.ID);
            return CanTakeAction(action, planOwnedByUser);
        }

        /// <summary>
        /// Determines if an ServicePlanAction can be preformed on a service plan based on the configuration of this PermissionSet.
        /// </summary>
        /// <param name="action">Action to be preformed.</param>
        /// <returns>True if the user can take this action based on this PermissionSet; otherwise false.</returns>
        public bool CanTakeAction(ServicePlanAction action, bool planOwnedByUser)
        {
            ServicePlanActionPermission spap = ServicePlanActionPermission.GetOne("PermissionSetID = ? && ServicePlanActionID = ?", this.ID, action.ID);
            int setting = (spap != null) ? spap.Setting : SETTING_NONE;

            switch (setting)
            {
                case SETTING_NONE: return false;
                case SETTING_OWNER: return planOwnedByUser;
                case SETTING_OTHERS: return !planOwnedByUser;
                case SETTING_OWNER_AND_OTHERS: return true; ;
                case SETTING_ALL: return true;
                default:
                    {
                        throw new NotSupportedException("ServicePlanActionPermission setting value of '" + setting + "' was not expected.");
                    }
            }
        }

        private int GetPermissionSettingForAction(Guid surveyActionID, string surveyServiceTypeCode)
        {
            string key = surveyActionID.ToString() + surveyServiceTypeCode;
            object setting = ActionSettings[key];
            return (setting != null) ? (int)setting : SETTING_NONE;
        }

        private Hashtable ActionSettings
        {
            get
            {
                // lazy load our member var
                if (_actionSettings == null)
                {
                    if (!this.IsReadOnly)
                    {
                        throw new InvalidOperationException("Survey actions cannot be loaded for PermissionSet objects that are writable.");
                    }

                    // get the action list
                    SurveyActionPermission[] list = SurveyActionPermission.GetArray("PermissionSetID = ?", this.ID);

                    // build a hashtable of the permissions for quick access later
                    Hashtable settings = new Hashtable(list.Length);
                    foreach (SurveyActionPermission action in list)
                    {
                        string key = action.SurveyActionID.ToString() + action.ServiceTypeCode;
                        settings.Add(key, action.Setting);
                    }

                    // update our member var
                    _actionSettings = settings;
                }

                return _actionSettings;
            }
        }

        /// <summary>
        /// Combines the rights defined in two PermissionSet objects into a merged PermissionSet.
        /// </summary>
        /// <param name="a">First PermissionSet to be merged.</param>
        /// <param name="b">Second PermissionSet to be merged.</param>
        /// <returns>A PermissionSet instance containing the results of the merge.</returns>
        public static PermissionSet Merge(PermissionSet a, PermissionSet b)
        {
            if (a == null) throw new ArgumentNullException("a");
            if (b == null) throw new ArgumentNullException("b");

            PermissionSet result = new PermissionSet();

            // merged all the flags; keeping the higher of the pairs
            result.CanWorkSurveys = (a.CanWorkSurveys || b.CanWorkSurveys);
            result.CanWorkReviews = (a.CanWorkReviews || b.CanWorkReviews);
            result.CanWorkServicePlans = (a.CanWorkServicePlans || b.CanWorkServicePlans);
            result.CanViewCompanySummary = (a.CanViewCompanySummary || b.CanViewCompanySummary);
            result.CanRequireSavedSearch = (a.CanRequireSavedSearch || b.CanRequireSavedSearch);
            result.CanSearchSurveys = (a.CanSearchSurveys || b.CanSearchSurveys);
            result.CanCreateSurveys = (a.CanCreateSurveys || b.CanCreateSurveys);
            result.CanCreateServicePlans = (a.CanCreateServicePlans || b.CanCreateServicePlans);
            result.CanViewLinks = (a.CanViewLinks || b.CanViewLinks);
            result.CanWorkAgencyVisits = (a.CanWorkAgencyVisits || b.CanWorkAgencyVisits);
            result.CanAdminUserRoles = (a.CanAdminUserRoles || b.CanAdminUserRoles);
            result.CanAdminUserAccounts = (a.CanAdminUserAccounts || b.CanAdminUserAccounts);
            result.CanAdminFeeCompanies = (a.CanAdminFeeCompanies || b.CanAdminFeeCompanies);
            result.CanAdminTerritories = (a.CanAdminTerritories || b.CanAdminTerritories);
            result.CanAdminRules = (a.CanAdminRules || b.CanAdminRules);
            result.CanAdminLetters = (a.CanAdminLetters || b.CanAdminLetters);
            result.CanAdminReports = (a.CanAdminReports || b.CanAdminReports);
            result.CanAdminDocuments = (a.CanAdminDocuments || b.CanAdminDocuments);
            result.CanAdminResultsDisplay = (a.CanAdminResultsDisplay || b.CanAdminResultsDisplay);
            result.CanAdminRecommendations = (a.CanAdminRecommendations || b.CanAdminRecommendations);
            result.CanAdminServicePlanFields = (a.CanAdminServicePlanFields || b.CanAdminServicePlanFields);
            result.CanAdminLinks = (a.CanAdminLinks || b.CanAdminLinks);
            result.CanAdminUnderwriters = (a.CanAdminUnderwriters || b.CanAdminUnderwriters);
            result.CanAdminClients = (a.CanAdminClients || b.CanAdminClients);
            result.CanAdminActivityTimes = (a.CanAdminActivityTimes || b.CanAdminActivityTimes);
            result.CanAdminHelpfulHints = (a.CanAdminHelpfulHints || b.CanAdminHelpfulHints);
            result.CanViewSurveyHistory = (a.CanViewSurveyHistory || b.CanViewSurveyHistory);
            

            // merge action permission settings by copying A then merging B into it
            Hashtable source = b.ActionSettings;
            Hashtable dest = new Hashtable(a.ActionSettings);
            foreach (string key in source.Keys)
            {
                object sourceSetting = source[key];
                object destSetting = dest[key];

                // skip adding this setting to dest if we don't have one (for some reason) or it is NONE
                if (sourceSetting == null || (int)sourceSetting == SETTING_NONE)
                {
                    continue;
                }

                // add the setting or merge to existing
                if (destSetting == null)
                {
                    dest.Add(key, sourceSetting);
                }
                else
                {
                    // update dest only if source has higher rights
                    if ((int)sourceSetting > (int)destSetting)
                    {
                        dest[key] = sourceSetting;
                    }
                }
            }

            // add our merged action list
            result._actionSettings = dest;

            // return our filled/merged object
            return result;
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            if (_isMergedSet)
            {
                throw new InvalidOperationException("Merged PermissionSet objects cannot be persisted.");
            }
        }

        #region --- Generated Members ---

        private Guid _id;
        private bool _canWorkSurveys;
        private bool _canWorkReviews;
        private bool _canWorkServicePlans;
        private bool _canViewCompanySummary;
        private bool _canViewSurveyReviewQueues;
        private bool _canRequireSavedSearch;
        private bool _canSearchSurveys;
        private bool _canCreateSurveys;
        private bool _canCreateServicePlans;
        private bool _canViewLinks;
        private bool _canWorkAgencyVisits;
        private bool _canViewSurveyHistory;
        private bool _canAdminUserRoles;
        private bool _canAdminUserAccounts;
        private bool _canAdminFeeCompanies;
        private bool _canAdminTerritories;
        private bool _canAdminRules;
        private bool _canAdminLetters;
        private bool _canAdminReports;
        private bool _canAdminResultsDisplay;
        private bool _canAdminRecommendations;
        private bool _canAdminServicePlanFields;
        private bool _canAdminLinks;
        private bool _canAdminUnderwriters;
        private bool _canAdminClients;
        private bool _canAdminActivityTimes;
        private bool _canAdminHelpfulHints;
        private bool _canAdminDocuments;
        private bool _canViewAdministrativeNotes;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Permission Set ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Can Work Surveys.
        /// </summary>
        public bool CanWorkSurveys
        {
            get { return _canWorkSurveys; }
            set
            {
                VerifyWritable();
                _canWorkSurveys = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Work Reviews.
        /// </summary>
        public bool CanWorkReviews
        {
            get { return _canWorkReviews; }
            set
            {
                VerifyWritable();
                _canWorkReviews = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Work Service Plans.
        /// </summary>
        public bool CanWorkServicePlans
        {
            get { return _canWorkServicePlans; }
            set
            {
                VerifyWritable();
                _canWorkServicePlans = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can View Company Summary.
        /// </summary>
        public bool CanViewCompanySummary
        {
            get { return _canViewCompanySummary; }
            set
            {
                VerifyWritable();
                _canViewCompanySummary = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can View Survey Review Queues.
        /// </summary>
        public bool CanViewSurveyReviewQueues
        {
            get { return _canViewSurveyReviewQueues; }
            set
            {
                VerifyWritable();
                _canViewSurveyReviewQueues = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Require Saved Search.
        /// </summary>
        public bool CanRequireSavedSearch
        {
            get { return _canRequireSavedSearch; }
            set
            {
                VerifyWritable();
                _canRequireSavedSearch = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Search Surveys.
        /// </summary>
        public bool CanSearchSurveys
        {
            get { return _canSearchSurveys; }
            set
            {
                VerifyWritable();
                _canSearchSurveys = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Create Surveys.
        /// </summary>
        public bool CanCreateSurveys
        {
            get { return _canCreateSurveys; }
            set
            {
                VerifyWritable();
                _canCreateSurveys = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Create Service Plans.
        /// </summary>
        public bool CanCreateServicePlans
        {
            get { return _canCreateServicePlans; }
            set
            {
                VerifyWritable();
                _canCreateServicePlans = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can View Links.
        /// </summary>
        public bool CanViewLinks
        {
            get { return _canViewLinks; }
            set
            {
                VerifyWritable();
                _canViewLinks = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can View Survey History.
        /// </summary>
        public bool CanViewSurveyHistory
        {
            get { return _canViewSurveyHistory; }
            set
            {
                VerifyWritable();
                _canViewSurveyHistory = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin User Roles.
        /// </summary>
        public bool CanAdminUserRoles
        {
            get { return _canAdminUserRoles; }
            set
            {
                VerifyWritable();
                _canAdminUserRoles = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin User Accounts.
        /// </summary>
        public bool CanAdminUserAccounts
        {
            get { return _canAdminUserAccounts; }
            set
            {
                VerifyWritable();
                _canAdminUserAccounts = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Fee Companies.
        /// </summary>
        public bool CanAdminFeeCompanies
        {
            get { return _canAdminFeeCompanies; }
            set
            {
                VerifyWritable();
                _canAdminFeeCompanies = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Territories.
        /// </summary>
        public bool CanAdminTerritories
        {
            get { return _canAdminTerritories; }
            set
            {
                VerifyWritable();
                _canAdminTerritories = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Rules.
        /// </summary>
        public bool CanAdminRules
        {
            get { return _canAdminRules; }
            set
            {
                VerifyWritable();
                _canAdminRules = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Letters.
        /// </summary>
        public bool CanAdminLetters
        {
            get { return _canAdminLetters; }
            set
            {
                VerifyWritable();
                _canAdminLetters = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Reports.
        /// </summary>
        public bool CanAdminReports
        {
            get { return _canAdminReports; }
            set
            {
                VerifyWritable();
                _canAdminReports = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Results Display.
        /// </summary>
        public bool CanAdminResultsDisplay
        {
            get { return _canAdminResultsDisplay; }
            set
            {
                VerifyWritable();
                _canAdminResultsDisplay = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Recommendations.
        /// </summary>
        public bool CanAdminRecommendations
        {
            get { return _canAdminRecommendations; }
            set
            {
                VerifyWritable();
                _canAdminRecommendations = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Service Plan Fields.
        /// </summary>
        public bool CanAdminServicePlanFields
        {
            get { return _canAdminServicePlanFields; }
            set
            {
                VerifyWritable();
                _canAdminServicePlanFields = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Links.
        /// </summary>
        public bool CanAdminLinks
        {
            get { return _canAdminLinks; }
            set
            {
                VerifyWritable();
                _canAdminLinks = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Underwriters.
        /// </summary>
        public bool CanAdminUnderwriters
        {
            get { return _canAdminUnderwriters; }
            set
            {
                VerifyWritable();
                _canAdminUnderwriters = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Clients.
        /// </summary>
        public bool CanAdminClients
        {
            get { return _canAdminClients; }
            set
            {
                VerifyWritable();
                _canAdminClients = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Activity Times.
        /// </summary>
        public bool CanAdminActivityTimes
        {
            get { return _canAdminActivityTimes; }
            set
            {
                VerifyWritable();
                _canAdminActivityTimes = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Helpful Hints.
        /// </summary>
        public bool CanAdminHelpfulHints
        {
            get { return _canAdminHelpfulHints; }
            set
            {
                VerifyWritable();
                _canAdminHelpfulHints = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can Admin Documents.
        /// </summary>
        public bool CanAdminDocuments
        {
            get { return _canAdminDocuments; }
            set
            {
                VerifyWritable();
                _canAdminDocuments = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can View Links.
        /// </summary>
        public bool CanWorkAgencyVisits
        {
            get { return _canWorkAgencyVisits; }
            set
            {
                VerifyWritable();
                _canWorkAgencyVisits = value;
            }
        }

        /// <summary>
        /// Gets or sets the Can View Links.
        /// </summary>
        public bool CanViewAdministrativeNotes
        {
            get { return _canViewAdministrativeNotes; }
            set
            {
                VerifyWritable();
                _canViewAdministrativeNotes = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_canWorkSurveys": return _canWorkSurveys;
                    case "_canWorkReviews": return _canWorkReviews;
                    case "_canWorkServicePlans": return _canWorkServicePlans;
                    case "_canViewCompanySummary": return _canViewCompanySummary;
                    case "_canViewSurveyReviewQueues": return _canViewSurveyReviewQueues;
                    case "_canRequireSavedSearch": return _canRequireSavedSearch;
                    case "_canSearchSurveys": return _canSearchSurveys;
                    case "_canCreateSurveys": return _canCreateSurveys;
                    case "_canCreateServicePlans": return _canCreateServicePlans;
                    case "_canViewLinks": return _canViewLinks;
                    case "_canWorkAgencyVisits": return _canWorkAgencyVisits;                            
                    case "_canViewSurveyHistory": return _canViewSurveyHistory;
                    case "_canAdminUserRoles": return _canAdminUserRoles;
                    case "_canAdminUserAccounts": return _canAdminUserAccounts;
                    case "_canAdminFeeCompanies": return _canAdminFeeCompanies;
                    case "_canAdminTerritories": return _canAdminTerritories;
                    case "_canAdminRules": return _canAdminRules;
                    case "_canAdminLetters": return _canAdminLetters;
                    case "_canAdminReports": return _canAdminReports;
                    case "_canAdminResultsDisplay": return _canAdminResultsDisplay;
                    case "_canAdminRecommendations": return _canAdminRecommendations;
                    case "_canAdminServicePlanFields": return _canAdminServicePlanFields;
                    case "_canAdminLinks": return _canAdminLinks;
                    case "_canAdminUnderwriters": return _canAdminUnderwriters;
                    case "_canAdminClients": return _canAdminClients;
                    case "_canAdminActivityTimes": return _canAdminActivityTimes;
                    case "_canAdminHelpfulHints": return _canAdminHelpfulHints;
                    case "_canAdminDocuments": return _canAdminDocuments;
                    case "_canViewAdministrativeNotes": return _canViewAdministrativeNotes;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_canWorkSurveys": _canWorkSurveys = (bool)value; break;
                    case "_canWorkReviews": _canWorkReviews = (bool)value; break;
                    case "_canWorkServicePlans": _canWorkServicePlans = (bool)value; break;
                    case "_canViewCompanySummary": _canViewCompanySummary = (bool)value; break;
                    case "_canViewSurveyReviewQueues": _canViewSurveyReviewQueues = (bool)value; break;
                    case "_canRequireSavedSearch": _canRequireSavedSearch = (bool)value; break;
                    case "_canSearchSurveys": _canSearchSurveys = (bool)value; break;
                    case "_canCreateSurveys": _canCreateSurveys = (bool)value; break;
                    case "_canCreateServicePlans": _canCreateServicePlans = (bool)value; break;
                    case "_canViewLinks": _canViewLinks = (bool)value; break;
                    case "_canWorkAgencyVisits": _canWorkAgencyVisits = (bool)value; break;
                    case "_canViewSurveyHistory": _canViewSurveyHistory = (bool)value; break;
                    case "_canAdminUserRoles": _canAdminUserRoles = (bool)value; break;
                    case "_canAdminUserAccounts": _canAdminUserAccounts = (bool)value; break;
                    case "_canAdminFeeCompanies": _canAdminFeeCompanies = (bool)value; break;
                    case "_canAdminTerritories": _canAdminTerritories = (bool)value; break;
                    case "_canAdminRules": _canAdminRules = (bool)value; break;
                    case "_canAdminLetters": _canAdminLetters = (bool)value; break;
                    case "_canAdminReports": _canAdminReports = (bool)value; break;
                    case "_canAdminDocuments": _canAdminDocuments = (bool)value; break;
                    case "_canAdminResultsDisplay": _canAdminResultsDisplay = (bool)value; break;
                    case "_canAdminRecommendations": _canAdminRecommendations = (bool)value; break;
                    case "_canAdminServicePlanFields": _canAdminServicePlanFields = (bool)value; break;
                    case "_canAdminLinks": _canAdminLinks = (bool)value; break;
                    case "_canAdminUnderwriters": _canAdminUnderwriters = (bool)value; break;
                    case "_canAdminClients": _canAdminClients = (bool)value; break;
                    case "_canAdminActivityTimes": _canAdminActivityTimes = (bool)value; break;
                    case "_canAdminHelpfulHints": _canAdminHelpfulHints = (bool)value; break;
                    case "_canViewAdministrativeNotes": _canViewAdministrativeNotes = (bool)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static PermissionSet Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(PermissionSet), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch PermissionSet entity with ID = '{0}'.", id));
            }
            return (PermissionSet)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static PermissionSet GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(PermissionSet), opathExpression);
            return (PermissionSet)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static PermissionSet GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(PermissionSet))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (PermissionSet)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static PermissionSet[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(PermissionSet), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static PermissionSet[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(PermissionSet))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (PermissionSet[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static PermissionSet[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(PermissionSet), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public PermissionSet GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            PermissionSet clone = (PermissionSet)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.CanWorkSurveys, _canWorkSurveys);
            Validator.Validate(Field.CanWorkReviews, _canWorkReviews);
            Validator.Validate(Field.CanWorkServicePlans, _canWorkServicePlans);
            Validator.Validate(Field.CanViewCompanySummary, _canViewCompanySummary);
            Validator.Validate(Field.CanViewSurveyReviewQueues, _canViewSurveyReviewQueues);
            Validator.Validate(Field.CanRequireSavedSearch, _canRequireSavedSearch);
            Validator.Validate(Field.CanSearchSurveys, _canSearchSurveys);
            Validator.Validate(Field.CanCreateSurveys, _canCreateSurveys);
            Validator.Validate(Field.CanCreateServicePlans, _canCreateServicePlans);            
            Validator.Validate(Field.CanViewLinks, _canViewLinks);
            Validator.Validate(Field.CanWorkAgencyVisits, _canWorkAgencyVisits);
            Validator.Validate(Field.CanViewSurveyHistory, _canViewSurveyHistory);
            Validator.Validate(Field.CanAdminUserRoles, _canAdminUserRoles);
            Validator.Validate(Field.CanAdminUserAccounts, _canAdminUserAccounts);
            Validator.Validate(Field.CanAdminFeeCompanies, _canAdminFeeCompanies);
            Validator.Validate(Field.CanAdminTerritories, _canAdminTerritories);
            Validator.Validate(Field.CanAdminRules, _canAdminRules);
            Validator.Validate(Field.CanAdminLetters, _canAdminLetters);
            Validator.Validate(Field.CanAdminReports, _canAdminReports);
            Validator.Validate(Field.CanAdminResultsDisplay, _canAdminResultsDisplay);
            Validator.Validate(Field.CanAdminRecommendations, _canAdminRecommendations);
            Validator.Validate(Field.CanAdminServicePlanFields, _canAdminServicePlanFields);
            Validator.Validate(Field.CanAdminLinks, _canAdminLinks);
            Validator.Validate(Field.CanAdminUnderwriters, _canAdminUnderwriters);
            Validator.Validate(Field.CanAdminClients, _canAdminClients);
            Validator.Validate(Field.CanAdminActivityTimes, _canAdminActivityTimes);
            Validator.Validate(Field.CanAdminHelpfulHints, _canAdminHelpfulHints);
            Validator.Validate(Field.CanAdminDocuments, _canAdminDocuments);
            Validator.Validate(Field.CanViewAdministrativeNotes, _canViewAdministrativeNotes);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _canWorkSurveys = false;
            _canWorkReviews = false;
            _canWorkServicePlans = false;
            _canViewCompanySummary = false;
            _canViewSurveyReviewQueues = false;
            _canRequireSavedSearch = false;
            _canSearchSurveys = false;
            _canCreateSurveys = false;
            _canCreateServicePlans = false;
            _canViewLinks = false;
            _canWorkAgencyVisits = false;
            _canViewSurveyHistory = false;
            _canAdminUserRoles = false;
            _canAdminUserAccounts = false;
            _canAdminFeeCompanies = false;
            _canAdminTerritories = false;
            _canAdminRules = false;
            _canAdminLetters = false;
            _canAdminReports = false;
            _canAdminResultsDisplay = false;
            _canAdminRecommendations = false;
            _canAdminServicePlanFields = false;
            _canAdminLinks = false;
            _canAdminUnderwriters = false;
            _canAdminClients = false;
            _canAdminActivityTimes = false;
            _canAdminHelpfulHints = false;
            _canAdminDocuments = false;
            _canViewAdministrativeNotes = false;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the PermissionSet entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the CanWorkSurveys field.
            /// </summary>
            CanWorkSurveys,
            /// <summary>
            /// Represents the CanWorkReviews field.
            /// </summary>
            CanWorkReviews,
            /// <summary>
            /// Represents the CanWorkServicePlans field.
            /// </summary>
            CanWorkServicePlans,
            /// <summary>
            /// Represents the CanViewCompanySummary field.
            /// </summary>
            CanViewCompanySummary,
            /// <summary>
            /// Represents the CanViewSurveyReviewQueues field.
            /// </summary>
            CanViewSurveyReviewQueues,
            /// <summary>
            /// Represents the CanRequireSavedSearch field.
            /// </summary>
            CanRequireSavedSearch,
            /// <summary>
            /// Represents the CanSearchSurveys field.
            /// </summary>
            CanSearchSurveys,
            /// <summary>
            /// Represents the CanCreateSurveys field.
            /// </summary>
            CanCreateSurveys,
            /// <summary>
            /// Represents the CanCreateServicePlans field.
            /// </summary>
            CanCreateServicePlans,
            /// <summary>
            /// Represents the CanViewLinks field.
            /// </summary>
            CanViewLinks,
            /// Represents the CanWorkAgencyVisits field.
            /// </summary>
            CanWorkAgencyVisits,
            /// <summary>
            /// Represents the CanViewSurveyHistory field.
            /// </summary>
            CanViewSurveyHistory,
            /// <summary>
            /// Represents the CanAdminUserRoles field.
            /// </summary>
            CanAdminUserRoles,
            /// <summary>
            /// Represents the CanAdminUserAccounts field.
            /// </summary>
            CanAdminUserAccounts,
            /// <summary>
            /// Represents the CanAdminFeeCompanies field.
            /// </summary>
            CanAdminFeeCompanies,
            /// <summary>
            /// Represents the CanAdminTerritories field.
            /// </summary>
            CanAdminTerritories,
            /// <summary>
            /// Represents the CanAdminRules field.
            /// </summary>
            CanAdminRules,
            /// <summary>
            /// Represents the CanAdminLetters field.
            /// </summary>
            CanAdminLetters,
            /// <summary>
            /// Represents the CanAdminReports field.
            /// </summary>
            CanAdminReports,
            /// <summary>
            /// Represents the CanAdminResultsDisplay field.
            /// </summary>
            CanAdminResultsDisplay,
            /// <summary>
            /// Represents the CanAdminRecommendations field.
            /// </summary>
            CanAdminRecommendations,
            /// <summary>
            /// Represents the CanAdminServicePlanFields field.
            /// </summary>
            CanAdminServicePlanFields,
            /// <summary>
            /// Represents the CanAdminLinks field.
            /// </summary>
            CanAdminLinks,
            /// <summary>
            /// Represents the CanAdminUnderwriters field.
            /// </summary>
            CanAdminUnderwriters,
            /// <summary>
            /// Represents the CanAdminClients field.
            /// </summary>
            CanAdminClients,
            /// <summary>
            /// Represents the CanAdminActivityTimes field.
            /// </summary>
            CanAdminActivityTimes,
            /// <summary>
            /// Represents the CanAdminHelpfulHints field.
            /// </summary>
            CanAdminHelpfulHints,
            /// <summary>
            /// Represents the CanAdminDocuments field.
            /// </summary>
            CanAdminDocuments,
            /// <summary>
            /// Represents the CanAdminDocuments field.
            /// </summary>
            CanViewAdministrativeNotes,
        }

        #endregion
    }
}

