using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a ReviewCategoryType entity, which maps to table 'ReviewCategoryType' in the database.
	/// </summary>
	public class ReviewCategoryType : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private ReviewCategoryType()
		{
		}


		#region --- Generated Members ---

		private Guid _id;
		private Guid _companyID;
		private string _reviewerTypeCode;
		private string _description;
		private bool _isRequired;
		private int _displayOrder;
		private ObjectHolder _companyHolder = null;
		private ObjectHolder _reviewerTypeHolder = null;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Review Category Type.
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets the Company.
		/// </summary>
		public Guid CompanyID
		{
			get { return _companyID; }
		}

		/// <summary>
		/// Gets the Reviewer Type.
		/// </summary>
		public string ReviewerTypeCode
		{
			get { return _reviewerTypeCode; }
		}

		/// <summary>
		/// Gets the Description.
		/// </summary>
		public string Description
		{
			get { return _description; }
		}

		/// <summary>
		/// Gets the Is Required.
		/// </summary>
		public bool IsRequired
		{
			get { return _isRequired; }
		}

		/// <summary>
		/// Gets the Display Order.
		/// </summary>
		public int DisplayOrder
		{
			get { return _displayOrder; }
		}

		/// <summary>
		/// Gets the instance of a Company object related to this entity.
		/// </summary>
		public Company Company
		{
			get
			{
				_companyHolder.Key = _companyID;
				return (Company)_companyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a ReviewerType object related to this entity.
		/// </summary>
		public ReviewerType ReviewerType
		{
			get
			{
				_reviewerTypeHolder.Key = _reviewerTypeCode;
				return (ReviewerType)_reviewerTypeHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_companyID": return _companyID;
					case "_reviewerTypeCode": return _reviewerTypeCode;
					case "_description": return _description;
					case "_isRequired": return _isRequired;
					case "_displayOrder": return _displayOrder;
					case "_companyHolder": return _companyHolder;
					case "_reviewerTypeHolder": return _reviewerTypeHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (Guid)value; break;
					case "_companyID": _companyID = (Guid)value; break;
					case "_reviewerTypeCode": _reviewerTypeCode = (string)value; break;
					case "_description": _description = (string)value; break;
					case "_isRequired": _isRequired = (bool)value; break;
					case "_displayOrder": _displayOrder = (int)value; break;
					case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
					case "_reviewerTypeHolder": _reviewerTypeHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static ReviewCategoryType Get(Guid id)
		{
			OPathQuery query = new OPathQuery(typeof(ReviewCategoryType), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch ReviewCategoryType entity with ID = '{0}'.", id));
			}
			return (ReviewCategoryType)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static ReviewCategoryType GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ReviewCategoryType), opathExpression);
			return (ReviewCategoryType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static ReviewCategoryType GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(ReviewCategoryType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (ReviewCategoryType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ReviewCategoryType[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ReviewCategoryType), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ReviewCategoryType[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(ReviewCategoryType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (ReviewCategoryType[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ReviewCategoryType[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ReviewCategoryType), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Guid.Empty;
			_companyID = Guid.Empty;
			_reviewerTypeCode = null;
			_description = null;
			_isRequired = false;
			_displayOrder = Int32.MinValue;
			_companyHolder = null;
			_reviewerTypeHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the ReviewCategoryType entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the CompanyID field.
			/// </summary>
			CompanyID,
			/// <summary>
			/// Represents the ReviewerTypeCode field.
			/// </summary>
			ReviewerTypeCode,
			/// <summary>
			/// Represents the Description field.
			/// </summary>
			Description,
			/// <summary>
			/// Represents the IsRequired field.
			/// </summary>
			IsRequired,
			/// <summary>
			/// Represents the DisplayOrder field.
			/// </summary>
			DisplayOrder,
		}
		
		#endregion
	}
}