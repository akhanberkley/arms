using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a LoginHistory entity, which maps to table 'LoginHistory' in the database.
	/// </summary>
	public class LoginHistory : IObjectHelper
	{
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private LoginHistory()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="username">The username entered in the login page.</param>
        /// <param name="ipAddress">The IP Address of the user trying to login.</param>
        /// <param name="wasSuccessful">Flag value indicating whether login was successful.</param>
        public LoginHistory(string username, string ipAddress, bool wasSuccessful)
        {
            _attemptedOn = DateTime.Now;
            _username = username;
            _ipAddress = ipAddress;
            _wasSuccessful = wasSuccessful;
            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }



        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();
        }


        #region --- Generated Members ---

        private int _id;
        private DateTime _attemptedOn;
        private string _username;
        private string _ipAddress;
        private bool _wasSuccessful;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Login History ID.
        /// </summary>
        public int ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Attempted On.
        /// </summary>
        public DateTime AttemptedOn
        {
            get { return _attemptedOn; }
            set
            {
                VerifyWritable();
                _attemptedOn = value;
            }
        }

        /// <summary>
        /// Gets or sets the Username.
        /// </summary>
        public string Username
        {
            get { return _username; }
            set
            {
                VerifyWritable();
                _username = value;
            }
        }

        /// <summary>
        /// Gets or sets the IP Address.
        /// </summary>
        public string IPAddress
        {
            get { return _ipAddress; }
            set
            {
                VerifyWritable();
                _ipAddress = value;
            }
        }

        /// <summary>
        /// Gets or sets the Was Successful.
        /// </summary>
        public bool WasSuccessful
        {
            get { return _wasSuccessful; }
            set
            {
                VerifyWritable();
                _wasSuccessful = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_attemptedOn": return _attemptedOn;
                    case "_username": return _username;
                    case "_ipAddress": return _ipAddress;
                    case "_wasSuccessful": return _wasSuccessful;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (int)value; break;
                    case "_attemptedOn": _attemptedOn = (DateTime)value; break;
                    case "_username": _username = (string)value; break;
                    case "_ipAddress": _ipAddress = (string)value; break;
                    case "_wasSuccessful": _wasSuccessful = (bool)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static LoginHistory Get(int id)
        {
            OPathQuery query = new OPathQuery(typeof(LoginHistory), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch LoginHistory entity with ID = '{0}'.", id));
            }
            return (LoginHistory)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static LoginHistory GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(LoginHistory), opathExpression);
            return (LoginHistory)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static LoginHistory GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(LoginHistory))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (LoginHistory)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static LoginHistory[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(LoginHistory), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static LoginHistory[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(LoginHistory))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (LoginHistory[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static LoginHistory[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(LoginHistory), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public LoginHistory GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            LoginHistory clone = (LoginHistory)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.AttemptedOn, _attemptedOn);
            Validator.Validate(Field.Username, _username);
            Validator.Validate(Field.IPAddress, _ipAddress);
            Validator.Validate(Field.WasSuccessful, _wasSuccessful);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Int32.MinValue;
            _attemptedOn = DateTime.MinValue;
            _username = null;
            _ipAddress = null;
            _wasSuccessful = false;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the LoginHistory entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the AttemptedOn field.
            /// </summary>
            AttemptedOn,
            /// <summary>
            /// Represents the Username field.
            /// </summary>
            Username,
            /// <summary>
            /// Represents the IPAddress field.
            /// </summary>
            IPAddress,
            /// <summary>
            /// Represents the WasSuccessful field.
            /// </summary>
            WasSuccessful,
        }

        #endregion
    }
}