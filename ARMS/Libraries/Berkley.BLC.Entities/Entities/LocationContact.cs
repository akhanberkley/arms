using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a LocationContact entity, which maps to table 'LocationContact' in the database.
    /// </summary>
    public class LocationContact : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private LocationContact()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this LocationContact.</param>
        public LocationContact(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Gets the the phone number as a fomatted string value.
        /// </summary>
        public string HtmlPhoneNumber
        {
            get
            {
                return Formatter.Phone(this.PhoneNumber, "(none)");
            }
        }

        /// <summary>
        /// Gets the the phone number as a fomatted string value.
        /// </summary>
        public string HtmlPhoneNumberSingle
        {
            get
            {
                return Formatter.Phone(this.PhoneNumber, "(not specified)");
            }
        }

        /// <summary>
        /// Gets the the alternate phone number as a fomatted string value.
        /// </summary>
        public string HtmlAltPhoneNumber
        {
            get
            {
                return Formatter.Phone(this.AlternatePhoneNumber, "(none)");
            }
        }

        /// <summary>
        /// Gets the the alternate phone number as a fomatted string value.
        /// </summary>
        public string HtmlAltPhoneNumberSingle
        {
            get
            {
                return Formatter.Phone(this.AlternatePhoneNumber, "(not specified)");
            }
        }

        /// <summary>
        /// Gets the the fax number as a fomatted string value.
        /// </summary>
        public string HtmlFaxNumber
        {
            get
            {
                return Formatter.Phone(this.FaxNumber, "(none)");
            }
        }

        /// <summary>
        /// Gets the the fax number as a fomatted string value.
        /// </summary>
        public string HtmlFaxNumberSingle
        {
            get
            {
                return Formatter.Phone(this.FaxNumber, "(not specified)");
            }
        }

        /// <summary>
        /// Gets the address as an HTML-encoded multi-line string value.
        /// </summary>
        public string HtmlAddress
        {
            get
            {
                return Formatter.Address(this.StreetLine1, this.StreetLine2, this.City, this.StateCode, this.ZipCode);
            }
        }

        /// <summary>
        /// Gets or sets the policy system key to tie back to the correct location.
        /// </summary>
        private string _policySystemKey = string.Empty;
        public string PolicySystemKey
        {
            get
            {
                return _policySystemKey;
            }
            set
            {
                _policySystemKey = value;
            }
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _locationID;
        private string _name;
        private string _title = String.Empty;
        private string _phoneNumber = String.Empty;
        private string _alternatePhoneNumber = String.Empty;
        private string _faxNumber = String.Empty;
        private string _emailAddress = String.Empty;
        private string _contactReasonTypeCode = String.Empty;
        private string _companyName = String.Empty;
        private string _streetLine1 = String.Empty;
        private string _streetLine2 = String.Empty;
        private string _city = String.Empty;
        private string _stateCode = String.Empty;
        private string _zipCode = String.Empty;
        private int _priorityIndex;
        private bool _modified;
        private ObjectHolder _contactReasonTypeHolder = null;
        private ObjectHolder _locationHolder = null;
        private ObjectHolder _stateHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Location Contact ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Location.
        /// </summary>
        public Guid LocationID
        {
            get { return _locationID; }
            set
            {
                VerifyWritable();
                _locationID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                VerifyWritable();
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Title. Null value is 'String.Empty'.
        /// </summary>
        public string Title
        {
            get { return _title; }
            set
            {
                VerifyWritable();
                _title = value;
            }
        }

        /// <summary>
        /// Gets or sets the Phone Number. Null value is 'String.Empty'.
        /// </summary>
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                VerifyWritable();
                _phoneNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Alternate Phone Number. Null value is 'String.Empty'.
        /// </summary>
        public string AlternatePhoneNumber
        {
            get { return _alternatePhoneNumber; }
            set
            {
                VerifyWritable();
                _alternatePhoneNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Fax Number. Null value is 'String.Empty'.
        /// </summary>
        public string FaxNumber
        {
            get { return _faxNumber; }
            set
            {
                VerifyWritable();
                _faxNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Email Address. Null value is 'String.Empty'.
        /// </summary>
        public string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                VerifyWritable();
                _emailAddress = value;
            }
        }

        /// <summary>
        /// Gets or sets the Contact Reason Type. Null value is 'String.Empty'.
        /// </summary>
        public string ContactReasonTypeCode
        {
            get { return _contactReasonTypeCode; }
            set
            {
                VerifyWritable();
                _contactReasonTypeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Company Name. Null value is 'String.Empty'.
        /// </summary>
        public string CompanyName
        {
            get { return _companyName; }
            set
            {
                VerifyWritable();
                _companyName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Street Line 1. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine1
        {
            get { return _streetLine1; }
            set
            {
                VerifyWritable();
                _streetLine1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the Street Line 2. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine2
        {
            get { return _streetLine2; }
            set
            {
                VerifyWritable();
                _streetLine2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the City. Null value is 'String.Empty'.
        /// </summary>
        public string City
        {
            get { return _city; }
            set
            {
                VerifyWritable();
                _city = value;
            }
        }

        /// <summary>
        /// Gets or sets the State. Null value is 'String.Empty'.
        /// </summary>
        public string StateCode
        {
            get { return _stateCode; }
            set
            {
                VerifyWritable();
                _stateCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Zip Code. Null value is 'String.Empty'.
        /// </summary>
        public string ZipCode
        {
            get { return _zipCode; }
            set
            {
                VerifyWritable();
                _zipCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Priority Index.
        /// </summary>
        public int PriorityIndex
        {
            get { return _priorityIndex; }
            set
            {
                VerifyWritable();
                _priorityIndex = value;
            }
        }

        /// <summary>
        /// Gets or sets the Modified.
        /// </summary>
        public bool Modified
        {
            get { return _modified; }
            set
            {
                VerifyWritable();
                _modified = value;
            }
        }

        /// <summary>
        /// Gets the instance of a ContactReasonType object related to this entity.
        /// </summary>
        public ContactReasonType ContactReasonType
        {
            get
            {
                _contactReasonTypeHolder.Key = _contactReasonTypeCode;
                return (ContactReasonType)_contactReasonTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Location object related to this entity.
        /// </summary>
        public Location Location
        {
            get
            {
                _locationHolder.Key = _locationID;
                return (Location)_locationHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a State object related to this entity.
        /// </summary>
        public State State
        {
            get
            {
                _stateHolder.Key = _stateCode;
                return (State)_stateHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_locationID": return _locationID;
                    case "_name": return _name;
                    case "_title": return _title;
                    case "_phoneNumber": return _phoneNumber;
                    case "_alternatePhoneNumber": return _alternatePhoneNumber;
                    case "_faxNumber": return _faxNumber;
                    case "_emailAddress": return _emailAddress;
                    case "_contactReasonTypeCode": return _contactReasonTypeCode;
                    case "_companyName": return _companyName;
                    case "_streetLine1": return _streetLine1;
                    case "_streetLine2": return _streetLine2;
                    case "_city": return _city;
                    case "_stateCode": return _stateCode;
                    case "_zipCode": return _zipCode;
                    case "_priorityIndex": return _priorityIndex;
                    case "_modified": return _modified;
                    case "_contactReasonTypeHolder": return _contactReasonTypeHolder;
                    case "_locationHolder": return _locationHolder;
                    case "_stateHolder": return _stateHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_locationID": _locationID = (Guid)value; break;
                    case "_name": _name = (string)value; break;
                    case "_title": _title = (string)value; break;
                    case "_phoneNumber": _phoneNumber = (string)value; break;
                    case "_alternatePhoneNumber": _alternatePhoneNumber = (string)value; break;
                    case "_faxNumber": _faxNumber = (string)value; break;
                    case "_emailAddress": _emailAddress = (string)value; break;
                    case "_contactReasonTypeCode": _contactReasonTypeCode = (string)value; break;
                    case "_companyName": _companyName = (string)value; break;
                    case "_streetLine1": _streetLine1 = (string)value; break;
                    case "_streetLine2": _streetLine2 = (string)value; break;
                    case "_city": _city = (string)value; break;
                    case "_stateCode": _stateCode = (string)value; break;
                    case "_zipCode": _zipCode = (string)value; break;
                    case "_priorityIndex": _priorityIndex = (int)value; break;
                    case "_modified": _modified = (bool)value; break;
                    case "_contactReasonTypeHolder": _contactReasonTypeHolder = (ObjectHolder)value; break;
                    case "_locationHolder": _locationHolder = (ObjectHolder)value; break;
                    case "_stateHolder": _stateHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static LocationContact Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(LocationContact), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch LocationContact entity with ID = '{0}'.", id));
            }
            return (LocationContact)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static LocationContact GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(LocationContact), opathExpression);
            return (LocationContact)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static LocationContact GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(LocationContact))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (LocationContact)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static LocationContact[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(LocationContact), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static LocationContact[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(LocationContact))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (LocationContact[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static LocationContact[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(LocationContact), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public LocationContact GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            LocationContact clone = (LocationContact)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.LocationID, _locationID);
            Validator.Validate(Field.Name, _name);
            Validator.Validate(Field.Title, _title);
            Validator.Validate(Field.PhoneNumber, _phoneNumber);
            Validator.Validate(Field.AlternatePhoneNumber, _alternatePhoneNumber);
            Validator.Validate(Field.FaxNumber, _faxNumber);
            Validator.Validate(Field.EmailAddress, _emailAddress);
            Validator.Validate(Field.ContactReasonTypeCode, _contactReasonTypeCode);
            Validator.Validate(Field.CompanyName, _companyName);
            Validator.Validate(Field.StreetLine1, _streetLine1);
            Validator.Validate(Field.StreetLine2, _streetLine2);
            Validator.Validate(Field.City, _city);
            Validator.Validate(Field.StateCode, _stateCode);
            Validator.Validate(Field.ZipCode, _zipCode);
            Validator.Validate(Field.PriorityIndex, _priorityIndex);
            Validator.Validate(Field.Modified, _modified);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _locationID = Guid.Empty;
            _name = null;
            _title = null;
            _phoneNumber = null;
            _alternatePhoneNumber = null;
            _faxNumber = null;
            _emailAddress = null;
            _contactReasonTypeCode = null;
            _companyName = null;
            _streetLine1 = null;
            _streetLine2 = null;
            _city = null;
            _stateCode = null;
            _zipCode = null;
            _priorityIndex = Int32.MinValue;
            _modified = false;
            _contactReasonTypeHolder = null;
            _locationHolder = null;
            _stateHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the LocationContact entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the LocationID field.
            /// </summary>
            LocationID,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the Title field.
            /// </summary>
            Title,
            /// <summary>
            /// Represents the PhoneNumber field.
            /// </summary>
            PhoneNumber,
            /// <summary>
            /// Represents the AlternatePhoneNumber field.
            /// </summary>
            AlternatePhoneNumber,
            /// <summary>
            /// Represents the FaxNumber field.
            /// </summary>
            FaxNumber,
            /// <summary>
            /// Represents the EmailAddress field.
            /// </summary>
            EmailAddress,
            /// <summary>
            /// Represents the ContactReasonTypeCode field.
            /// </summary>
            ContactReasonTypeCode,
            /// <summary>
            /// Represents the CompanyName field.
            /// </summary>
            CompanyName,
            /// <summary>
            /// Represents the StreetLine1 field.
            /// </summary>
            StreetLine1,
            /// <summary>
            /// Represents the StreetLine2 field.
            /// </summary>
            StreetLine2,
            /// <summary>
            /// Represents the City field.
            /// </summary>
            City,
            /// <summary>
            /// Represents the StateCode field.
            /// </summary>
            StateCode,
            /// <summary>
            /// Represents the ZipCode field.
            /// </summary>
            ZipCode,
            /// <summary>
            /// Represents the PriorityIndex field.
            /// </summary>
            PriorityIndex,
            /// <summary>
            /// Represents the Modified field.
            /// </summary>
            Modified,
        }

        #endregion
    }
}