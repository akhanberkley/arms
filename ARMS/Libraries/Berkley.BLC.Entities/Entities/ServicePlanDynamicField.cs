using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a ServicePlanDynamicField entity, which maps to table 'ServicePlan_DynamicField' in the database.
	/// </summary>
	public class ServicePlanDynamicField : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private ServicePlanDynamicField()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="servicePlanID">ServicePlanID of this ServicePlanDynamicField.</param>
		/// <param name="dynamicFieldID">DynamicFieldID of this ServicePlanDynamicField.</param>
		public ServicePlanDynamicField(Guid servicePlanID, Guid dynamicFieldID)
		{
			_servicePlanID = servicePlanID;
			_dynamicFieldID = dynamicFieldID;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _servicePlanID;
		private Guid _dynamicFieldID;
		private string _entryText;
		private ObjectHolder _dynamicFieldHolder = null;
		private ObjectHolder _servicePlanHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Service Plan.
		/// </summary>
		public Guid ServicePlanID
		{
			get { return _servicePlanID; }
		}

		/// <summary>
		/// Gets the Dynamic Field.
		/// </summary>
		public Guid DynamicFieldID
		{
			get { return _dynamicFieldID; }
		}

		/// <summary>
		/// Gets or sets the Entry Text.
		/// </summary>
		public string EntryText
		{
			get { return _entryText; }
			set
			{
				VerifyWritable();
				_entryText = value;
			}
		}

		/// <summary>
		/// Gets the instance of a DynamicField object related to this entity.
		/// </summary>
		public DynamicField DynamicField
		{
			get
			{
				_dynamicFieldHolder.Key = _dynamicFieldID;
				return (DynamicField)_dynamicFieldHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a ServicePlan object related to this entity.
		/// </summary>
		public ServicePlan ServicePlan
		{
			get
			{
				_servicePlanHolder.Key = _servicePlanID;
				return (ServicePlan)_servicePlanHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_servicePlanID": return _servicePlanID;
					case "_dynamicFieldID": return _dynamicFieldID;
					case "_entryText": return _entryText;
					case "_dynamicFieldHolder": return _dynamicFieldHolder;
					case "_servicePlanHolder": return _servicePlanHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_servicePlanID": _servicePlanID = (Guid)value; break;
					case "_dynamicFieldID": _dynamicFieldID = (Guid)value; break;
					case "_entryText": _entryText = (string)value; break;
					case "_dynamicFieldHolder": _dynamicFieldHolder = (ObjectHolder)value; break;
					case "_servicePlanHolder": _servicePlanHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="servicePlanID">ServicePlanID of the entity to fetch.</param>
		/// <param name="dynamicFieldID">DynamicFieldID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static ServicePlanDynamicField Get(Guid servicePlanID, Guid dynamicFieldID)
		{
			OPathQuery query = new OPathQuery(typeof(ServicePlanDynamicField), "ServicePlanID = ? && DynamicFieldID = ?");
			object entity = DB.Engine.GetObject(query, servicePlanID, dynamicFieldID);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch ServicePlanDynamicField entity with ServicePlanID = '{0}' and DynamicFieldID = '{1}'.", servicePlanID, dynamicFieldID));
			}
			return (ServicePlanDynamicField)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static ServicePlanDynamicField GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ServicePlanDynamicField), opathExpression);
			return (ServicePlanDynamicField)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static ServicePlanDynamicField GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(ServicePlanDynamicField) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (ServicePlanDynamicField)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ServicePlanDynamicField[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ServicePlanDynamicField), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ServicePlanDynamicField[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(ServicePlanDynamicField) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (ServicePlanDynamicField[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ServicePlanDynamicField[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ServicePlanDynamicField), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public ServicePlanDynamicField GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			ServicePlanDynamicField clone = (ServicePlanDynamicField)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ServicePlanID, _servicePlanID);
			Validator.Validate(Field.DynamicFieldID, _dynamicFieldID);
			Validator.Validate(Field.EntryText, _entryText);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_servicePlanID = Guid.Empty;
			_dynamicFieldID = Guid.Empty;
			_entryText = null;
			_dynamicFieldHolder = null;
			_servicePlanHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the ServicePlanDynamicField entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ServicePlanID field.
			/// </summary>
			ServicePlanID,
			/// <summary>
			/// Represents the DynamicFieldID field.
			/// </summary>
			DynamicFieldID,
			/// <summary>
			/// Represents the EntryText field.
			/// </summary>
			EntryText,
		}
		
		#endregion
	}
}