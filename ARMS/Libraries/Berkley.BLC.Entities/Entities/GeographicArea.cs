using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a GeographicArea entity, which maps to table 'GeographicArea' in the database.
	/// </summary>
	public class GeographicArea : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private GeographicArea()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this GeographicArea.</param>
		public GeographicArea(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _id;
		private Guid _territoryID;
		private string _geographicUnitCode;
		private string _areaValue;
		private ObjectHolder _geographicUnitHolder = null;
		private ObjectHolder _territoryHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Geographic Area ID.
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets or sets the Territory.
		/// </summary>
		public Guid TerritoryID
		{
			get { return _territoryID; }
			set
			{
				VerifyWritable();
				_territoryID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Geographic Unit.
		/// </summary>
		public string GeographicUnitCode
		{
			get { return _geographicUnitCode; }
			set
			{
				VerifyWritable();
				_geographicUnitCode = value;
			}
		}

		/// <summary>
		/// Gets or sets the Area Value.
		/// </summary>
		public string AreaValue
		{
			get { return _areaValue; }
			set
			{
				VerifyWritable();
				_areaValue = value;
			}
		}

		/// <summary>
		/// Gets the instance of a GeographicUnit object related to this entity.
		/// </summary>
		public GeographicUnit GeographicUnit
		{
			get
			{
				_geographicUnitHolder.Key = _geographicUnitCode;
				return (GeographicUnit)_geographicUnitHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Territory object related to this entity.
		/// </summary>
		public Territory Territory
		{
			get
			{
				_territoryHolder.Key = _territoryID;
				return (Territory)_territoryHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_territoryID": return _territoryID;
					case "_geographicUnitCode": return _geographicUnitCode;
					case "_areaValue": return _areaValue;
					case "_geographicUnitHolder": return _geographicUnitHolder;
					case "_territoryHolder": return _territoryHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (Guid)value; break;
					case "_territoryID": _territoryID = (Guid)value; break;
					case "_geographicUnitCode": _geographicUnitCode = (string)value; break;
					case "_areaValue": _areaValue = (string)value; break;
					case "_geographicUnitHolder": _geographicUnitHolder = (ObjectHolder)value; break;
					case "_territoryHolder": _territoryHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static GeographicArea Get(Guid id)
		{
			OPathQuery query = new OPathQuery(typeof(GeographicArea), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch GeographicArea entity with ID = '{0}'.", id));
			}
			return (GeographicArea)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static GeographicArea GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(GeographicArea), opathExpression);
			return (GeographicArea)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static GeographicArea GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(GeographicArea) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (GeographicArea)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static GeographicArea[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(GeographicArea), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static GeographicArea[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(GeographicArea) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (GeographicArea[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static GeographicArea[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(GeographicArea), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public GeographicArea GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			GeographicArea clone = (GeographicArea)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ID, _id);
			Validator.Validate(Field.TerritoryID, _territoryID);
			Validator.Validate(Field.GeographicUnitCode, _geographicUnitCode);
			Validator.Validate(Field.AreaValue, _areaValue);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Guid.Empty;
			_territoryID = Guid.Empty;
			_geographicUnitCode = null;
			_areaValue = null;
			_geographicUnitHolder = null;
			_territoryHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the GeographicArea entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the TerritoryID field.
			/// </summary>
			TerritoryID,
			/// <summary>
			/// Represents the GeographicUnitCode field.
			/// </summary>
			GeographicUnitCode,
			/// <summary>
			/// Represents the AreaValue field.
			/// </summary>
			AreaValue,
		}
		
		#endregion
	}
}