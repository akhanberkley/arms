using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a CoverageNameType entity, which maps to table 'CoverageNameType' in the database.
	/// </summary>
	public class CoverageNameType : IObjectHelper
	{
        #region Public Fields

        /// <summary>
        /// Represents a Building coverage name type.
        /// </summary>
        public static readonly CoverageNameType Building = CoverageNameType.Get("BUILDING");
        /// <summary>
        /// Represents a Builders Risk coverage name type.
        /// </summary>
        public static readonly CoverageNameType BuildersRisk = CoverageNameType.Get("BUILDRISK");
        /// <summary>
        /// Represents a Business Income coverage name type.
        /// </summary>
        public static readonly CoverageNameType BusinessIncome = CoverageNameType.Get("BUSINCOME");
        /// <summary>
        /// Represents a Commercial Auto coverage name type.
        /// </summary>
        public static readonly CoverageNameType CommercialAuto= CoverageNameType.Get("COMMAUTO");
        /// <summary>
        /// Represents a Commercial Umbrella coverage name type.
        /// </summary>
        public static readonly CoverageNameType CommercialUmbrella = CoverageNameType.Get("UMBRELLA");
        /// <summary>
        /// Represents a Computers/Electronic Equipment coverage name type.
        /// </summary>
        public static readonly CoverageNameType ComputersElectronicEquipment = CoverageNameType.Get("ELECTEQUIP");
        /// <summary>
        /// Represents a Contractors Equipment Leased/Rented coverage name type.
        /// </summary>
        public static readonly CoverageNameType ContractorsEquipmentLeasedRented = CoverageNameType.Get("CONEQUIP");
        /// <summary>
        /// Represents a Contractors Liability coverage name type.
        /// </summary>
        public static readonly CoverageNameType ContractorsLiability = CoverageNameType.Get("CONTLIA");
        /// <summary>
        /// Represents a Crime coverage name type.
        /// </summary>
        public static readonly CoverageNameType Crime = CoverageNameType.Get("CRIME");
        /// <summary>
        /// Represents a General Liability coverage name type.
        /// </summary>
        public static readonly CoverageNameType GeneralLiability = CoverageNameType.Get("GENLIA");
        /// <summary>
        /// Represents a Jewelers Block coverage name type.
        /// </summary>
        public static readonly CoverageNameType JewelersBlock = CoverageNameType.Get("JEWELERS");
        /// <summary>
        /// Represents a Motor Truck Cargo coverage name type.
        /// </summary>
        public static readonly CoverageNameType MotorTruckCargo = CoverageNameType.Get("TRUCKCARGO");
        /// <summary>
        /// Represents a Non-Owner/Hired Auto coverage name type.
        /// </summary>
        public static readonly CoverageNameType NonOwnerHiredAuto = CoverageNameType.Get("NONOWNAUTO");
        /// <summary>
        /// Represents a Personal Property coverage name type.
        /// </summary>
        public static readonly CoverageNameType PersonalProperty = CoverageNameType.Get("PERPROP");
        /// <summary>
        /// Represents a Products/Completed Operations coverage name type.
        /// </summary>
        public static readonly CoverageNameType ProductsCompletedOperations = CoverageNameType.Get("PRODUCTOPS");
        /// <summary>
        /// Represents a Signs coverage name type.
        /// </summary>
        public static readonly CoverageNameType Signs = CoverageNameType.Get("SIGNS");
        /// <summary>
        /// Represents a Tool Floater coverage name type.
        /// </summary>
        public static readonly CoverageNameType ToolFloater = CoverageNameType.Get("TOOLFLOAT");
        /// <summary>
        /// Represents a Warehouse Legal Liability coverage name type.
        /// </summary>
        public static readonly CoverageNameType WarehouseLegalLiability = CoverageNameType.Get("WAREHOUSE");
        /// <summary>
        /// Represents an Exposure coverage name type.
        /// </summary>
        public static readonly CoverageNameType Exposure = CoverageNameType.Get("EXPOSURE");
        /// <summary>
        /// Represents an Property coverage name type.
        /// </summary>
        public static readonly CoverageNameType Property = CoverageNameType.Get("PROPERTY");
        /// <summary>
        /// Represents a Physical Damage coverage name type.
        /// </summary>
        public static readonly CoverageNameType PhysicalDamage = CoverageNameType.Get("PHYDAMAGE");
        /// <summary>
        /// Represents a Legal Liability coverage name type.
        /// </summary>
        public static readonly CoverageNameType LegalLiability = CoverageNameType.Get("LEGALLIAB");
        /// <summary>
        /// Represents a Boat Dealer coverage name type.
        /// </summary>
        public static readonly CoverageNameType BoatDealer = CoverageNameType.Get("BOATDEAL");
        /// <summary>
        /// Represents a Class of Operations coverage name type.
        /// </summary>
        public static readonly CoverageNameType ClassOfOperations = CoverageNameType.Get("CLASSOP");
        /// <summary>
        /// Represents a Driveaway Collision coverage name type.
        /// </summary>
        public static readonly CoverageNameType DriveawayCollision = CoverageNameType.Get("DRIVEAWAY");
        /// <summary>
        /// Represents a Comprehensive coverage name type.
        /// </summary>
        public static readonly CoverageNameType Comprehensive = CoverageNameType.Get("COMPRHEN");
        /// <summary>
        /// Represents a Collision coverage name type.
        /// </summary>
        public static readonly CoverageNameType Collision = CoverageNameType.Get("COLLISION");
        /// <summary>
        /// Represents a Specified Causes of Loss coverage name type.
        /// </summary>
        public static readonly CoverageNameType SpecifiedCausesOfLoss = CoverageNameType.Get("SPECIFIED");
        /// <summary>
        /// Represents a Specified Causes of Loss coverage name type.
        /// </summary>
        public static readonly CoverageNameType AccountsReceivable = CoverageNameType.Get("ACCTREC");
        /// <summary>
        /// Represents a Specified Causes of Loss coverage name type.
        /// </summary>
        public static readonly CoverageNameType ValuablePapers = CoverageNameType.Get("VALUEPAP");
        /// <summary>
        /// Represents a Specified Causes of Loss coverage name type.
        /// </summary>
        public static readonly CoverageNameType FineArtsFloater = CoverageNameType.Get("FINEARTS");
        /// <summary>
        /// Represents a Specified Causes of Loss coverage name type.
        /// </summary>
        public static readonly CoverageNameType BaileesCoverage = CoverageNameType.Get("BAILEES");
        /// <summary>
        /// Represents a Specified Causes of Loss coverage name type.
        /// </summary>
        public static readonly CoverageNameType TransitCoverage = CoverageNameType.Get("TRANSIT");
        /// <summary>
        /// Represents a Garagekeepers coverage name type.
        /// </summary>
        public static readonly CoverageNameType GarageKeepers = CoverageNameType.Get("GARAGE");

        #endregion

		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private CoverageNameType()
		{
		}


		#region --- Generated Members ---

		private string _code;
		private string _name;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Coverage Name Type.
		/// </summary>
		public string Code
		{
			get { return _code; }
		}

		/// <summary>
		/// Gets the Coverage Name Type.
		/// </summary>
		public string Name
		{
			get { return _name; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_code": return _code;
					case "_name": return _name;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_code": _code = (string)value; break;
					case "_name": _name = (string)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="code">Code of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static CoverageNameType Get(string code)
		{
			OPathQuery query = new OPathQuery(typeof(CoverageNameType), "Code = ?");
			object entity = DB.Engine.GetObject(query, code);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch CoverageNameType entity with Code = '{0}'.", code));
			}
			return (CoverageNameType)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static CoverageNameType GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CoverageNameType), opathExpression);
			return (CoverageNameType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static CoverageNameType GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(CoverageNameType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (CoverageNameType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CoverageNameType[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CoverageNameType), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CoverageNameType[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(CoverageNameType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (CoverageNameType[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CoverageNameType[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CoverageNameType), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Determines if two CoverageNameType objects have the same semantic value.
		/// </summary>
		/// <param name="one">The first object to compare.</param>
		/// <param name="two">The second object to compare.</param>
		/// <returns>True if they are semantically identical, false otherwise.</returns>
		public static bool operator == (CoverageNameType one, CoverageNameType two) 
		{
			if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
			{
				return true;
			}
			else if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
			{
				return false;
			}
			else
			{
				return (one.CompareTo(two) == 0);
			}
		}

		/// <summary>
		/// Determines if two CoverageNameType objects have different semantic values.
		/// </summary>
		/// <param name="one">The first object to compare.</param>
		/// <param name="two">The second object to compare.</param>
		/// <returns>True if they are semantically dissimilar, false otherwise.</returns>
		public static bool operator != (CoverageNameType one, CoverageNameType two) 
		{
			if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
			{
				return false;
			}
			if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
			{
				return true;
			}
			else
			{
				return (one.CompareTo(two) != 0);
			}
		}
		
		/// <summary>
		/// Compares the current instance with another object of the same type.
		/// </summary>
		/// <param name="value">An object to compare with this instance.</param>
		/// <returns>Less than zero if this instance is less than value. 
		/// Zero if this instance is equal to value.
		/// Greater than zero if this instance is greater than value.</returns>
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (value is CoverageNameType)
			{
				CoverageNameType obj = (CoverageNameType)value;
				return (this.Code.CompareTo(obj.Code));
			}
			throw new ArgumentException("Value is not the correct type.");
		}

		/// <summary>
		/// Returns the hash code for this instance.
		/// </summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		public override int GetHashCode()
		{
			return this.Code.GetHashCode();
		}

		/// <summary>
		/// Determines whether this instance is equal to the specified object.
		/// </summary>
		/// <param name="value">The object to compare with the current instance. </param>
		/// <returns>true if the specified object is equal to the current instance; otherwise, false.</returns>
		public override bool Equals(object value)
		{
			return (this == (value as CoverageNameType));
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_code = null;
			_name = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the CoverageNameType entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the Code field.
			/// </summary>
			Code,
			/// <summary>
			/// Represents the Name field.
			/// </summary>
			Name,
		}
		
		#endregion
	}
}