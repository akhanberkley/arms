using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;
using System.Text;
using System.IO;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a Location entity, which maps to table 'Location' in the database.
    /// </summary>
    public class Location : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private Location()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this Location.</param>
        public Location(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Gets the address as a formattted single line string value.
        /// </summary>
        public string SingleLineAddress
        {
            get
            {
                StringBuilder sb = new StringBuilder(200);

                bool hasLine1 = (this.StreetLine1 != null && this.StreetLine1.Length > 0);
                bool hasLine2 = (this.StreetLine2 != null && this.StreetLine2.Length > 0);
                bool hasCity = (this.City != null && this.City.Length > 0);
                bool hasState = (this.StateCode != null && this.StateCode.Length > 0);
                bool hasZip = (this.ZipCode != null && this.ZipCode.Length > 0);

                StringWriter w = new StringWriter();

                if (hasLine1)
                {
                    sb.Append(this.StreetLine1);
                    if (hasLine2 || hasCity || hasState || hasZip)
                    {
                        sb.Append(", ");
                    }
                }
                if (hasLine2)
                {
                    sb.Append(this.StreetLine2);
                    if (hasCity || hasState || hasZip)
                    {
                        sb.Append(", ");
                    }
                }
                if (hasCity)
                {
                    sb.Append(this.City);
                    if (hasState || hasZip)
                    {
                        sb.Append(", ");
                    }
                }
                if (hasState)
                {
                    sb.Append(this.StateCode);
                    if (hasZip)
                    {
                        sb.Append("  ");
                    }
                }
                if (hasZip)
                {
                    sb.Append(this.ZipCode);
                }

                string result = sb.ToString();
                return (result.Length > 0) ? result : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the public protection for the location that will apply to the buildings at that location.
        /// </summary>
        private int _publicProtection = Int32.MinValue;
        public int PublicProtection
        {
            get
            {
                return _publicProtection;
            }
            set
            {
                _publicProtection = value;
            }
        }

        /// <summary>
        /// Gets or sets the has sprinklery system flag for the location that will apply to the buildings at that location.
        /// </summary>
        private short _hasSprinklerSystem = Int16.MinValue;
        public short HasSprinklerSystem
        {
            get
            {
                return _hasSprinklerSystem;
            }
            set
            {
                _hasSprinklerSystem = value;
            }
        }

        /// <summary>
        /// Gets the address as a formattted single line string value.
        /// </summary>
        public string SingleLineAddressWithNumber
        {
            get
            {
                return string.Format("#{0} - {1}", (this.Number != Int32.MinValue) ? this.Number : 0, this.SingleLineAddress);
            }
        }

        /// <summary>
        /// Selective "assignment operator".  Copied data fields from the Right Hand Side instance to "this" instance
        /// </summary>
        /// <param name="oRhs">Instance with new data</param>
        /// <returns>The modified class</returns>
        public Location Assign(Location oRhs)
        {
            Location oLocation = this;

            if (oLocation.IsReadOnly)
            {
                oLocation = oLocation.GetWritableInstance();
            }

            oLocation.PolicySymbol = oRhs._policySymbol;
            oLocation.Number = oRhs._number;
            oLocation.StreetLine1 = oRhs._streetLine1;
            oLocation.StreetLine2 = oRhs._streetLine2;
            oLocation.City = oRhs._city;
            oLocation.StateCode = oRhs._stateCode;
            oLocation.ZipCode = oRhs._zipCode;
            oLocation.PolicySystemKey = oRhs._policySystemKey;
            oLocation.Description = oRhs._description;
            oLocation.IsActive = oRhs._isActive;
            return oLocation;
        }

        public Location InsertInsuredLocation(Location newLocation)
        {
            // insert and default a new location for the Insured
            Location nLocation = this;

            if (nLocation.IsReadOnly)
            {
                nLocation = nLocation.GetWritableInstance();
            }

            nLocation.PolicySymbol = newLocation._policySymbol;
            nLocation.Number = newLocation._number;
            nLocation.StreetLine1 = newLocation._streetLine1;
            nLocation.StreetLine2 = newLocation._streetLine2;
            nLocation.City = newLocation._city;
            nLocation.StateCode = newLocation._stateCode;
            nLocation.ZipCode = newLocation._zipCode;
            nLocation.PolicySystemKey = newLocation._policySystemKey;
            nLocation.Description = newLocation._description;
            nLocation.IsActive = newLocation._isActive;

            nLocation.Save();

            return nLocation;
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _insuredID = Guid.Empty;
        private string _policySystemKey = String.Empty;
        private string _policySymbol = String.Empty;
        private int _number = Int32.MinValue;
        private string _numberForDisplay = String.Empty;
        private string _streetLine1 = String.Empty;
        private string _streetLine2 = String.Empty;
        private string _city = String.Empty;
        private string _stateCode = String.Empty;
        private string _zipCode = String.Empty;
        private string _description = String.Empty;
        private bool _isActive;
        private ObjectHolder _insuredHolder = null;
        private ObjectHolder _stateHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Location ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Insured. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid InsuredID
        {
            get { return _insuredID; }
            set
            {
                VerifyWritable();
                _insuredID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy System Key. Null value is 'String.Empty'.
        /// </summary>
        public string PolicySystemKey
        {
            get { return _policySystemKey; }
            set
            {
                VerifyWritable();
                _policySystemKey = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Symbol. Null value is 'String.Empty'.
        /// </summary>
        public string PolicySymbol
        {
            get { return _policySymbol; }
            set
            {
                VerifyWritable();
                _policySymbol = value;
            }
        }

        /// <summary>
        /// Gets or sets the Location Number. Null value is 'Int32.MinValue'.
        /// </summary>
        public int Number
        {
            get { return _number; }
            set
            {
                VerifyWritable();
                _number = value;
            }
        }

        /// <summary>
        /// Gets or sets the Location Number For Display. Null value is 'String.Empty'.
        /// </summary>
        public string NumberForDisplay
        {
            get { return _numberForDisplay; }
            set
            {
                VerifyWritable();
                _numberForDisplay = value;
            }
        }

        /// <summary>
        /// Gets or sets the Street Line 1. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine1
        {
            get { return _streetLine1; }
            set
            {
                VerifyWritable();
                _streetLine1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the Street Line 2. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine2
        {
            get { return _streetLine2; }
            set
            {
                VerifyWritable();
                _streetLine2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the City. Null value is 'String.Empty'.
        /// </summary>
        public string City
        {
            get { return _city; }
            set
            {
                VerifyWritable();
                _city = value;
            }
        }

        /// <summary>
        /// Gets or sets the State. Null value is 'String.Empty'.
        /// </summary>
        public string StateCode
        {
            get { return _stateCode; }
            set
            {
                VerifyWritable();
                _stateCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Zip Code. Null value is 'String.Empty'.
        /// </summary>
        public string ZipCode
        {
            get { return _zipCode; }
            set
            {
                VerifyWritable();
                _zipCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Description. Null value is 'String.Empty'.
        /// </summary>
        public string Description
        {
            get { return _description; }
            set
            {
                VerifyWritable();
                _description = value;
            }
        }

        /// <summary>
        /// Gets or sets the Is Active.
        /// </summary>
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                VerifyWritable();
                _isActive = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Insured object related to this entity.
        /// </summary>
        public Insured Insured
        {
            get
            {
                _insuredHolder.Key = _insuredID;
                return (Insured)_insuredHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a State object related to this entity.
        /// </summary>
        public State State
        {
            get
            {
                _stateHolder.Key = _stateCode;
                return (State)_stateHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_insuredID": return _insuredID;
                    case "_policySystemKey": return _policySystemKey;
                    case "_policySymbol": return _policySymbol;
                    case "_number": return _number;
                    case "_numberForDisplay": return _numberForDisplay;
                    case "_streetLine1": return _streetLine1;
                    case "_streetLine2": return _streetLine2;
                    case "_city": return _city;
                    case "_stateCode": return _stateCode;
                    case "_zipCode": return _zipCode;
                    case "_description": return _description;
                    case "_isActive": return _isActive;
                    case "_insuredHolder": return _insuredHolder;
                    case "_stateHolder": return _stateHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_insuredID": _insuredID = (Guid)value; break;
                    case "_policySystemKey": _policySystemKey = (string)value; break;
                    case "_policySymbol": _policySymbol = (string)value; break;
                    case "_number": _number = (int)value; break;
                    case "_numberForDisplay": _numberForDisplay = (string)value; break;
                    case "_streetLine1": _streetLine1 = (string)value; break;
                    case "_streetLine2": _streetLine2 = (string)value; break;
                    case "_city": _city = (string)value; break;
                    case "_stateCode": _stateCode = (string)value; break;
                    case "_zipCode": _zipCode = (string)value; break;
                    case "_description": _description = (string)value; break;
                    case "_isActive": _isActive = (bool)value; break;
                    case "_insuredHolder": _insuredHolder = (ObjectHolder)value; break;
                    case "_stateHolder": _stateHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static Location Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(Location), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch Location entity with ID = '{0}'.", id));
            }
            return (Location)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Location GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Location), opathExpression);
            return (Location)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Location GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Location))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Location)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Location[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Location), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Location[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Location))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Location[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Location[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Location), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public Location GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            Location clone = (Location)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.InsuredID, _insuredID);
            Validator.Validate(Field.PolicySystemKey, _policySystemKey);
            Validator.Validate(Field.PolicySymbol, _policySymbol);
            Validator.Validate(Field.Number, _number);
            Validator.Validate(Field.NumberForDisplay, _numberForDisplay);
            Validator.Validate(Field.StreetLine1, _streetLine1);
            Validator.Validate(Field.StreetLine2, _streetLine2);
            Validator.Validate(Field.City, _city);
            Validator.Validate(Field.StateCode, _stateCode);
            Validator.Validate(Field.ZipCode, _zipCode);
            Validator.Validate(Field.Description, _description);
            Validator.Validate(Field.IsActive, _isActive);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _insuredID = Guid.Empty;
            _policySystemKey = null;
            _policySymbol = null;
            _number = Int32.MinValue;
            _numberForDisplay = null;
            _streetLine1 = null;
            _streetLine2 = null;
            _city = null;
            _stateCode = null;
            _zipCode = null;
            _description = null;
            _isActive = false;
            _insuredHolder = null;
            _stateHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the Location entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the InsuredID field.
            /// </summary>
            InsuredID,
            /// <summary>
            /// Represents the PolicySystemKey field.
            /// </summary>
            PolicySystemKey,
            /// <summary>
            /// Represents the PolicySymbol field.
            /// </summary>
            PolicySymbol,
            /// <summary>
            /// Represents the Number field.
            /// </summary>
            Number,
            /// <summary>
            /// Represents the NumberForDisplay field.
            /// </summary>
            NumberForDisplay,
            /// <summary>
            /// Represents the StreetLine1 field.
            /// </summary>
            StreetLine1,
            /// <summary>
            /// Represents the StreetLine2 field.
            /// </summary>
            StreetLine2,
            /// <summary>
            /// Represents the City field.
            /// </summary>
            City,
            /// <summary>
            /// Represents the StateCode field.
            /// </summary>
            StateCode,
            /// <summary>
            /// Represents the ZipCode field.
            /// </summary>
            ZipCode,
            /// <summary>
            /// Represents the Description field.
            /// </summary>
            Description,
            /// <summary>
            /// Represents the IsActive field.
            /// </summary>
            IsActive,
        }

        #endregion
    }
}