using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a ServicePlan entity, which maps to table 'ServicePlan' in the database.
    /// </summary>
    public class ServicePlan : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private ServicePlan()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this ServicePlan.</param>
        public ServicePlan(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Gets the html representation of the createdBy user name.
        /// </summary>
        public string HtmlCreatedBy
        {
            get
            {
                if (this.CreatedByUserID != Guid.Empty)
                {
                    return this.CreatedByUser.Name;
                }
                else
                {
                    return "(not specified)";
                }
            }
        }

        /// <summary>
        /// Gets the html representation of the AssignedTo user name.
        /// </summary>
        public string HtmlAssignedTo
        {
            get
            {
                if (this.AssignedUserID != Guid.Empty)
                {
                    return this.AssignedUser.Name;
                }
                else
                {
                    return "(not specified)";
                }
            }
        }

        /// <summary>
        /// Gets all the previous service plan(s) tied to this plan.
        /// </summary>
        public ServicePlan[] GetPreviousServicePlans
        {
            get
            {
                Hashtable list = new Hashtable();
                list.Add(ID, this);

                //First, try traversing down.
                if (this.PreviousServicePlan != null)
                {
                    ServicePlan downPrevious = this.PreviousServicePlan;
                    ServicePlan downServicePlan = null;

                    //Loop looking for previous plan
                    do
                    {
                        if (!list.Contains(downPrevious.ID))
                        {
                            list.Add(downPrevious.ID, downPrevious);
                            downServicePlan = ServicePlan.Get(downPrevious.ID);
                            downPrevious = downServicePlan.PreviousServicePlan;
                        }
                    }
                    while (downPrevious != null);
                }

                //Second, try traversing up.
                ServicePlan servicePlan = ServicePlan.GetOne("PreviousServicePlanID = ?", this.ID);
                if (servicePlan != null)
                {
                    ServicePlan upServicePlan = servicePlan;

                    //Loop looking for previous surveys
                    do
                    {
                        if (!list.Contains(upServicePlan.ID))
                        {
                            list.Add(upServicePlan.ID, upServicePlan);
                            upServicePlan = ServicePlan.GetOne("PreviousServicePlanID = ?", upServicePlan.ID);
                        }
                    }
                    while (upServicePlan != null);
                }

                ServicePlan[] servicePlans = new ServicePlan[list.Count];
                list.Values.CopyTo(servicePlans, 0);

                return servicePlans;
            }
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private int _number;
        private Guid _companyID;
        private Guid _insuredID;
        private Guid _primarySurveyID = Guid.Empty;
        private Guid _primaryPolicyID = Guid.Empty;
        private string _workflowSubmissionNumber = String.Empty;
        private Guid _previousServicePlanID = Guid.Empty;
        private Guid _typeID = Guid.Empty;
        private string _statusCode;
        private Guid _assignedUserID = Guid.Empty;
        private DateTime _createDate;
        private Guid _createdByUserID;
        private string _createStateCode;
        private DateTime _completeDate = DateTime.MinValue;
        private DateTime _lastUnderwriterReportDate = DateTime.MinValue;
        private DateTime _nextFullReportDueDate = DateTime.MinValue;
        private DateTime _lastModifiedOn = DateTime.MinValue;
        private string _gradingCode = String.Empty;
        private ObjectHolder _assignedUserHolder = null;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _createdByUserHolder = null;
        private ObjectHolder _createStateHolder = null;
        private ObjectHolder _insuredHolder = null;
        private ObjectHolder _previousServicePlanHolder = null;
        private ObjectHolder _primaryPolicyHolder = null;
        private ObjectHolder _primarySurveyHolder = null;
        private ObjectHolder _servicePlanStatusHolder = null;
        private ObjectHolder _servicePlanTypeHolder = null;
        private ObjectHolder _servicePlanGradingCodeSurveyGradingHolder = null;
        private IList _surveyList = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Service Plan ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets the Service Plan Number.
        /// </summary>
        public int Number
        {
            get { return _number; }
        }

        /// <summary>
        /// Gets or sets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
            set
            {
                VerifyWritable();
                _companyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Insured.
        /// </summary>
        public Guid InsuredID
        {
            get { return _insuredID; }
            set
            {
                VerifyWritable();
                _insuredID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Primary Survey. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid PrimarySurveyID
        {
            get { return _primarySurveyID; }
            set
            {
                VerifyWritable();
                _primarySurveyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Primary Policy. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid PrimaryPolicyID
        {
            get { return _primaryPolicyID; }
            set
            {
                VerifyWritable();
                _primaryPolicyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Workflow Submission Number. Null value is 'String.Empty'.
        /// </summary>
        public string WorkflowSubmissionNumber
        {
            get { return _workflowSubmissionNumber; }
            set
            {
                VerifyWritable();
                _workflowSubmissionNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Previous Service Plan. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid PreviousServicePlanID
        {
            get { return _previousServicePlanID; }
            set
            {
                VerifyWritable();
                _previousServicePlanID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Service Plan Type. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid TypeID
        {
            get { return _typeID; }
            set
            {
                VerifyWritable();
                _typeID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Service Plan Status.
        /// </summary>
        public string StatusCode
        {
            get { return _statusCode; }
            set
            {
                VerifyWritable();
                _statusCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Assigned User. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid AssignedUserID
        {
            get { return _assignedUserID; }
            set
            {
                VerifyWritable();
                _assignedUserID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Create Date.
        /// </summary>
        public DateTime CreateDate
        {
            get { return _createDate; }
            set
            {
                VerifyWritable();
                _createDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Created By User.
        /// </summary>
        public Guid CreatedByUserID
        {
            get { return _createdByUserID; }
            set
            {
                VerifyWritable();
                _createdByUserID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Create State.
        /// </summary>
        public string CreateStateCode
        {
            get { return _createStateCode; }
            set
            {
                VerifyWritable();
                _createStateCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Complete Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime CompleteDate
        {
            get { return _completeDate; }
            set
            {
                VerifyWritable();
                _completeDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Last Underwriter Report Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime LastUnderwriterReportDate
        {
            get { return _lastUnderwriterReportDate; }
            set
            {
                VerifyWritable();
                _lastUnderwriterReportDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Next Full Report Due Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime NextFullReportDueDate
        {
            get { return _nextFullReportDueDate; }
            set
            {
                VerifyWritable();
                _nextFullReportDueDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Last Modified On. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime LastModifiedOn
        {
            get { return _lastModifiedOn; }
            set
            {
                VerifyWritable();
                _lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets or sets the Service Plan Grading. Null value is 'String.Empty'.
        /// </summary>
        public string GradingCode
        {
            get { return _gradingCode; }
            set
            {
                VerifyWritable();
                _gradingCode = value;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User AssignedUser
        {
            get
            {
                _assignedUserHolder.Key = _assignedUserID;
                return (User)_assignedUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User CreatedByUser
        {
            get
            {
                _createdByUserHolder.Key = _createdByUserID;
                return (User)_createdByUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a CreateState object related to this entity.
        /// </summary>
        public CreateState CreateState
        {
            get
            {
                _createStateHolder.Key = _createStateCode;
                return (CreateState)_createStateHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Insured object related to this entity.
        /// </summary>
        public Insured Insured
        {
            get
            {
                _insuredHolder.Key = _insuredID;
                return (Insured)_insuredHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a ServicePlan object related to this entity.
        /// </summary>
        public ServicePlan PreviousServicePlan
        {
            get
            {
                _previousServicePlanHolder.Key = _previousServicePlanID;
                return (ServicePlan)_previousServicePlanHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Policy object related to this entity.
        /// </summary>
        public Policy PrimaryPolicy
        {
            get
            {
                _primaryPolicyHolder.Key = _primaryPolicyID;
                return (Policy)_primaryPolicyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Survey object related to this entity.
        /// </summary>
        public Survey PrimarySurvey
        {
            get
            {
                _primarySurveyHolder.Key = _primarySurveyID;
                return (Survey)_primarySurveyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a ServicePlanStatus object related to this entity.
        /// </summary>
        public ServicePlanStatus Status
        {
            get
            {
                _servicePlanStatusHolder.Key = _statusCode;
                return (ServicePlanStatus)_servicePlanStatusHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a SurveyGrading object related to this entity.
        /// </summary>
        public SurveyGrading SurveyGrading
        {
            get
            {
                _servicePlanGradingCodeSurveyGradingHolder.Key = _gradingCode;
                return (SurveyGrading)_servicePlanGradingCodeSurveyGradingHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a ServicePlanType object related to this entity.
        /// </summary>
        public ServicePlanType Type
        {
            get
            {
                _servicePlanTypeHolder.Key = _typeID;
                return (ServicePlanType)_servicePlanTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the collection of Survey objects related to this entity.
        /// </summary>
        public IList Surveys2
        {
            get
            {
                return _surveyList;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_number": return _number;
                    case "_companyID": return _companyID;
                    case "_insuredID": return _insuredID;
                    case "_primarySurveyID": return _primarySurveyID;
                    case "_primaryPolicyID": return _primaryPolicyID;
                    case "_workflowSubmissionNumber": return _workflowSubmissionNumber;
                    case "_previousServicePlanID": return _previousServicePlanID;
                    case "_typeID": return _typeID;
                    case "_statusCode": return _statusCode;
                    case "_assignedUserID": return _assignedUserID;
                    case "_createDate": return _createDate;
                    case "_createdByUserID": return _createdByUserID;
                    case "_createStateCode": return _createStateCode;
                    case "_completeDate": return _completeDate;
                    case "_lastUnderwriterReportDate": return _lastUnderwriterReportDate;
                    case "_nextFullReportDueDate": return _nextFullReportDueDate;
                    case "_lastModifiedOn": return _lastModifiedOn;
                    case "_assignedUserHolder": return _assignedUserHolder;
                    case "_gradingCode": return _gradingCode;
                    case "_companyHolder": return _companyHolder;
                    case "_createdByUserHolder": return _createdByUserHolder;
                    case "_createStateHolder": return _createStateHolder;
                    case "_insuredHolder": return _insuredHolder;
                    case "_previousServicePlanHolder": return _previousServicePlanHolder;
                    case "_primaryPolicyHolder": return _primaryPolicyHolder;
                    case "_primarySurveyHolder": return _primarySurveyHolder;
                    case "_servicePlanStatusHolder": return _servicePlanStatusHolder;
                    case "_servicePlanTypeHolder": return _servicePlanTypeHolder;
                    case "_servicePlanGradingCodeSurveyGradingHolder": return _servicePlanGradingCodeSurveyGradingHolder;
                    case "_surveyList": return _surveyList;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_number": _number = (int)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_insuredID": _insuredID = (Guid)value; break;
                    case "_primarySurveyID": _primarySurveyID = (Guid)value; break;
                    case "_primaryPolicyID": _primaryPolicyID = (Guid)value; break;
                    case "_workflowSubmissionNumber": _workflowSubmissionNumber = (string)value; break;
                    case "_previousServicePlanID": _previousServicePlanID = (Guid)value; break;
                    case "_typeID": _typeID = (Guid)value; break;
                    case "_statusCode": _statusCode = (string)value; break;
                    case "_assignedUserID": _assignedUserID = (Guid)value; break;
                    case "_createDate": _createDate = (DateTime)value; break;
                    case "_createdByUserID": _createdByUserID = (Guid)value; break;
                    case "_createStateCode": _createStateCode = (string)value; break;
                    case "_completeDate": _completeDate = (DateTime)value; break;
                    case "_lastUnderwriterReportDate": _lastUnderwriterReportDate = (DateTime)value; break;
                    case "_nextFullReportDueDate": _nextFullReportDueDate = (DateTime)value; break;
                    case "_lastModifiedOn": _lastModifiedOn = (DateTime)value; break;
                    case "_gradingCode": _gradingCode = (string)value; break;
                    case "_assignedUserHolder": _assignedUserHolder = (ObjectHolder)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_createdByUserHolder": _createdByUserHolder = (ObjectHolder)value; break;
                    case "_createStateHolder": _createStateHolder = (ObjectHolder)value; break;
                    case "_insuredHolder": _insuredHolder = (ObjectHolder)value; break;
                    case "_previousServicePlanHolder": _previousServicePlanHolder = (ObjectHolder)value; break;
                    case "_primaryPolicyHolder": _primaryPolicyHolder = (ObjectHolder)value; break;
                    case "_primarySurveyHolder": _primarySurveyHolder = (ObjectHolder)value; break;
                    case "_servicePlanStatusHolder": _servicePlanStatusHolder = (ObjectHolder)value; break;
                    case "_servicePlanTypeHolder": _servicePlanTypeHolder = (ObjectHolder)value; break;
                    case "_servicePlanGradingCodeSurveyGradingHolder": _servicePlanGradingCodeSurveyGradingHolder = (ObjectHolder)value; break;
                    case "_surveyList": _surveyList = (IList)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static ServicePlan Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlan), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch ServicePlan entity with ID = '{0}'.", id));
            }
            return (ServicePlan)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ServicePlan GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlan), opathExpression);
            return (ServicePlan)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ServicePlan GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ServicePlan))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ServicePlan)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlan[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlan), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlan[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ServicePlan))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ServicePlan[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlan[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlan), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public ServicePlan GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            ServicePlan clone = (ServicePlan)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.Number, _number);
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.InsuredID, _insuredID);
            Validator.Validate(Field.PrimarySurveyID, _primarySurveyID);
            Validator.Validate(Field.PrimaryPolicyID, _primaryPolicyID);
            Validator.Validate(Field.WorkflowSubmissionNumber, _workflowSubmissionNumber);
            Validator.Validate(Field.PreviousServicePlanID, _previousServicePlanID);
            Validator.Validate(Field.TypeID, _typeID);
            Validator.Validate(Field.StatusCode, _statusCode);
            Validator.Validate(Field.AssignedUserID, _assignedUserID);
            Validator.Validate(Field.CreateDate, _createDate);
            Validator.Validate(Field.CreatedByUserID, _createdByUserID);
            Validator.Validate(Field.CreateStateCode, _createStateCode);
            Validator.Validate(Field.CompleteDate, _completeDate);
            Validator.Validate(Field.LastUnderwriterReportDate, _lastUnderwriterReportDate);
            Validator.Validate(Field.NextFullReportDueDate, _nextFullReportDueDate);
            Validator.Validate(Field.LastModifiedOn, _lastModifiedOn);
            Validator.Validate(Field.GradingCode, _gradingCode);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _number = Int32.MinValue;
            _companyID = Guid.Empty;
            _insuredID = Guid.Empty;
            _primarySurveyID = Guid.Empty;
            _primaryPolicyID = Guid.Empty;
            _workflowSubmissionNumber = null;
            _previousServicePlanID = Guid.Empty;
            _typeID = Guid.Empty;
            _statusCode = null;
            _assignedUserID = Guid.Empty;
            _createDate = DateTime.MinValue;
            _createdByUserID = Guid.Empty;
            _createStateCode = null;
            _completeDate = DateTime.MinValue;
            _lastUnderwriterReportDate = DateTime.MinValue;
            _nextFullReportDueDate = DateTime.MinValue;
            _lastModifiedOn = DateTime.MinValue;
            _gradingCode = null;
            _assignedUserHolder = null;
            _companyHolder = null;
            _createdByUserHolder = null;
            _createStateHolder = null;
            _insuredHolder = null;
            _previousServicePlanHolder = null;
            _primaryPolicyHolder = null;
            _primarySurveyHolder = null;
            _servicePlanStatusHolder = null;
            _servicePlanTypeHolder = null;
            _servicePlanGradingCodeSurveyGradingHolder = null;
            _surveyList = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the ServicePlan entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the Number field.
            /// </summary>
            Number,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the InsuredID field.
            /// </summary>
            InsuredID,
            /// <summary>
            /// Represents the PrimarySurveyID field.
            /// </summary>
            PrimarySurveyID,
            /// <summary>
            /// Represents the PrimaryPolicyID field.
            /// </summary>
            PrimaryPolicyID,
            /// <summary>
            /// Represents the WorkflowSubmissionNumber field.
            /// </summary>
            WorkflowSubmissionNumber,
            /// <summary>
            /// Represents the PreviousServicePlanID field.
            /// </summary>
            PreviousServicePlanID,
            /// <summary>
            /// Represents the TypeID field.
            /// </summary>
            TypeID,
            /// <summary>
            /// Represents the StatusCode field.
            /// </summary>
            StatusCode,
            /// <summary>
            /// Represents the AssignedUserID field.
            /// </summary>
            AssignedUserID,
            /// <summary>
            /// Represents the CreateDate field.
            /// </summary>
            CreateDate,
            /// <summary>
            /// Represents the CreatedByUserID field.
            /// </summary>
            CreatedByUserID,
            /// <summary>
            /// Represents the CreateStateCode field.
            /// </summary>
            CreateStateCode,
            /// <summary>
            /// Represents the CompleteDate field.
            /// </summary>
            CompleteDate,
            /// <summary>
            /// Represents the LastUnderwriterReportDate field.
            /// </summary>
            LastUnderwriterReportDate,
            /// <summary>
            /// Represents the NextFullReportDueDate field.
            /// </summary>
            NextFullReportDueDate,
            /// <summary>
            /// Represents the LastModifiedOn field.
            /// </summary>
            LastModifiedOn,
            /// <summary>
            /// Represents the GradingCode field.
            /// </summary>
            GradingCode,
        }

        #endregion
    }
}