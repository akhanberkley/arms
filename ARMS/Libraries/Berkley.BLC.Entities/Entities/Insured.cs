using System;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents an Insured entity, which maps to table 'Insured' in the database.
    /// </summary>
    public class Insured : IObjectHelper
    {
        // Bits for the ExclusionBit member
        private static int miExcludeUnderWriter = 0x00001;
        private static int miExcludeName = 0x00002;
        private static int miExcludeName2 = 0x00003;
        private static int miExcludeStreetLine1 = 0x00008;
        private static int miExcludeStreetLine2 = 0x00010;
        private static int miExcludeStreetLine3 = 0x00020;
        private static int miExcludeCity = 0x00040;
        private static int miExcludeState = 0x00080;
        private static int miExcludeZip = 0x00100;
        private static int miExcludeBusiness = 0x00200;
        private static int miExcludeSIC = 0x00400;

        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private Insured()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this Insured.</param>
        public Insured(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Gets the address as an HTML-encoded multi-line string value.
        /// </summary>
        public string HtmlAddress
        {
            get
            {
                return Formatter.Address(this.StreetLine1, this.StreetLine2, this.StreetLine3, this.City, this.StateCode, this.ZipCode);
            }
        }

        /// <summary>
        /// Gets the SIC Code and Description as a single-line string value.
        /// </summary>
        public string HtmlSIC
        {
            get
            {
                return (this.SIC != null) ? string.Format("{0} - {1}", this.SIC.Code, this.SIC.Description) : "(not specified)";

            }
        }

        /// <summary>
        /// Gets the Agenct Contact Email.
        /// </summary>
        public string HtmlAgentContactEmail
        {
            get
            {
                return (this.AgencyEmail != null) ? this.AgencyEmail.EmailAddress : "(not specified)";
            }
        }

        /// <summary>
        /// Gets the Underwriter name from the lookup table, default will display (not specified).
        /// </summary>
        public string HtmlUnderwriterName
        {
            get
            {
                if (this.Underwriter.Length > 0)
                {                    
                    BLC.Entities.Underwriter uw = BLC.Entities.Underwriter.GetOne("Code = ? && CompanyID = ?", this.Underwriter, this.CompanyID);
                    return (uw != null) ? uw.Name : this.Underwriter;
                }
                else
                {
                    return "(not specified)";
                }
            }
        }

        /// <summary>
        /// Gets the Underwriter name from the lookup table, default will display (not specified).
        /// </summary>
        public string HtmlUnderwriter2Name
        {
            get
            {
                if (this.Underwriter2.Length > 0)
                {
                    BLC.Entities.Underwriter uw2 = BLC.Entities.Underwriter.GetOne("Code = ? && CompanyID = ?", this.Underwriter2, this.CompanyID);
                    return (uw2 != null) ? uw2.Name : this.Underwriter2;
                }
                else
                {
                    return "(not specified)";
                }
            }
        }


        /// <summary>
        /// Gets the Underwriter name from the lookup table, default will display (not specified).
        /// </summary>
        public string HtmlUnderwriterPhone
        {
            get
            {
                BLC.Entities.Underwriter uw = BLC.Entities.Underwriter.GetOne("Code = ? && CompanyID = ?", this.Underwriter, this.CompanyID);
                if (uw != null)
                {
                    return Formatter.Phone(uw.PhoneNumber, uw.PhoneExt, "(not specified)");
                }
                else
                {
                    return "(not specified)";
                }
            }
        }

        /// <summary>
        /// Gets the Underwriter name from the lookup table, default will display (not specified).
        /// </summary>
        public string HtmlUnderwriter2Phone
        {
            get
            {
                BLC.Entities.Underwriter uw2 = BLC.Entities.Underwriter.GetOne("Code = ? && CompanyID = ?", this.Underwriter2, this.CompanyID);
                if (uw2 != null)
                {
                    return Formatter.Phone(uw2.PhoneNumber, uw2.PhoneExt, "(not specified)");
                }
                else
                {
                    return "(not specified)";
                }
            }
        }

        #region Exclusions

        public void ResetBitfield()
        {
            ExclusionBit = 0;
        }

        private int FlipBit(bool fFlag, int iBitField, int iBit)
        {
            if (fFlag == true)
            {
                iBitField |= iBit;
            }
            else
            {
                iBitField &= ~iBit;
            }
            return iBitField;
        }

        public bool ExcludeName
        {
            get { return (ExclusionBit & miExcludeName) == miExcludeName; }
            set { ExclusionBit = FlipBit(value, ExclusionBit, miExcludeName); }
        }
        public bool ExcludeName2
        {
            get { return (ExclusionBit & miExcludeName2) == miExcludeName2; }
            set { ExclusionBit = FlipBit(value, ExclusionBit, miExcludeName2); }
        }
        public bool ExcludeStreetLine1
        {
            get { return (ExclusionBit & miExcludeStreetLine1) == miExcludeStreetLine1; }
            set { ExclusionBit = FlipBit(value, ExclusionBit, miExcludeStreetLine1); }
        }
        public bool ExcludeStreetLine2
        {
            get { return (ExclusionBit & miExcludeStreetLine2) == miExcludeStreetLine2; }
            set { ExclusionBit = FlipBit(value, ExclusionBit, miExcludeStreetLine2); }
        }
        public bool ExcludeStreetLine3
        {
            get { return (ExclusionBit & miExcludeStreetLine3) == miExcludeStreetLine3; }
            set { ExclusionBit = FlipBit(value, ExclusionBit, miExcludeStreetLine3); }
        }
        public bool ExcludeUnderWriter
        {
            get { return (ExclusionBit & miExcludeUnderWriter) == miExcludeUnderWriter; }
            set { ExclusionBit = FlipBit(value, ExclusionBit, miExcludeUnderWriter); }
        }
        public bool ExcludeCity
        {
            get { return (ExclusionBit & miExcludeCity) == miExcludeCity; }
            set { ExclusionBit = FlipBit(value, ExclusionBit, miExcludeCity); }
        }
        public bool ExcludeState
        {
            get { return (ExclusionBit & miExcludeState) == miExcludeState; }
            set { ExclusionBit = FlipBit(value, ExclusionBit, miExcludeState); }
        }
        public bool ExcludeZip
        {
            get { return (ExclusionBit & miExcludeZip) == miExcludeZip; }
            set { ExclusionBit = FlipBit(value, ExclusionBit, miExcludeZip); }
        }
        public bool ExcludeBusiness
        {
            get { return (ExclusionBit & miExcludeBusiness) == miExcludeBusiness; }
            set { ExclusionBit = FlipBit(value, ExclusionBit, miExcludeBusiness); }
        }
        public bool ExcludeSIC
        {
            get { return (ExclusionBit & miExcludeSIC) == miExcludeSIC; }
            set { ExclusionBit = FlipBit(value, ExclusionBit, miExcludeSIC); }
        }
        #endregion

        /// <summary>
        /// Selective "assignment operator".  Copied data fields from the Right Hand Side instance to "this" instance
        /// </summary>
        /// <param name="oRhs">Instance with new data</param>
        /// <returns>The modified class</returns>
        public Insured Assign(Insured oRhs)
        {
            Insured oInsured = this;

            if (oInsured.IsReadOnly)
            {
                oInsured = oInsured.GetWritableInstance();
            }

            oInsured.ClientID = oRhs._clientID;
            oInsured.PortfolioID = oRhs.PortfolioID;
            if (oRhs._companyID != Guid.Empty)
            {
                oInsured.CompanyID = oRhs._companyID;
            }
            if (oRhs.AgencyID != Guid.Empty)
            {
                oInsured.AgencyID = oRhs.AgencyID;
            }
            // The existing (LHS - Left Hand Side) object is protected with the Exclusions
            // LHS = RHS (Right Hand Side)
            if (!ExcludeName)
            {
                oInsured.Name = oRhs._name;
            }
            if (!ExcludeName2)
            {
                oInsured.Name2 = oRhs._name2;
            }
            if (!ExcludeStreetLine1)
            {
                oInsured.StreetLine1 = oRhs._streetLine1;
            }
            if (!ExcludeStreetLine2)
            {
                oInsured.StreetLine2 = oRhs._streetLine2;
            }
            if (!ExcludeStreetLine3)
            {
                oInsured.StreetLine3 = oRhs._streetLine3;
            }
            if (!ExcludeCity)
            {
                oInsured.City = oRhs._city;
            }
            if (!ExcludeState)
            {
                oInsured.StateCode = oRhs._stateCode;
            }
            if (!ExcludeZip)
            {
                oInsured.ZipCode = oRhs._zipCode;
            }
            if (!ExcludeBusiness)
            {
                oInsured.BusinessOperations = oRhs._businessOperations;
            }
            if (oRhs._sicCode != null && oRhs._sicCode.Length > 0 && !ExcludeSIC)
            {
                oInsured.SICCode = String.Format("{0:0000}", Convert.ToInt32(oRhs._sicCode));
            }
            if (!ExcludeUnderWriter)
            {
                oInsured.Underwriter = oRhs._underwriter;
            }

            //always set the current account status
            oInsured.NonrenewedDate = oRhs._nonrenewedDate;

            return oInsured;
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private string _clientID = String.Empty;
        private string _portfolioID = String.Empty;
        private Guid _companyID;
        private Guid _agencyID = Guid.Empty;
        private string _name = String.Empty;
        private string _name2 = String.Empty;
        private string _streetLine1 = String.Empty;
        private string _streetLine2 = String.Empty;
        private string _streetLine3 = String.Empty;
        private string _city = String.Empty;
        private string _stateCode = String.Empty;
        private string _zipCode = String.Empty;
        private string _businessOperations = String.Empty;
        private string _businessCategory = String.Empty;
        private string _dotNumber = String.Empty;
        private string _sicCode = String.Empty;
        private string _naicsCode = String.Empty;
        private string _underwriter = String.Empty;
        private string _underwriter2 = String.Empty;        
        private string _companyWebsite = String.Empty;
        private string _agentContactName = String.Empty;
        private string _agentContactPhoneNumber = String.Empty;
        private Guid _agentEmailID = Guid.Empty;
        private DateTime _nonrenewedDate = DateTime.MinValue;
        private int _exclusionBit;
        private string _contactName = String.Empty;
        private string _contactPhoneNumber = String.Empty;
        private string _contactEmail = String.Empty;
        private ObjectHolder _agencyHolder = null;
        private ObjectHolder _agentEmailAgencyEmailHolder = null;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _sicHolder = null;
        private ObjectHolder _stateHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Insured ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Client ID. Null value is 'String.Empty'.
        /// </summary>
        public string ClientID
        {
            get { return _clientID; }
            set
            {
                VerifyWritable();
                _clientID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Portfolio ID. Null value is 'String.Empty'.
        /// </summary>
        public string PortfolioID
        {
            get { return _portfolioID; }
            set
            {
                VerifyWritable();
                _portfolioID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
            set
            {
                VerifyWritable();
                _companyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Agency. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid AgencyID
        {
            get { return _agencyID; }
            set
            {
                VerifyWritable();
                _agencyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Name. Null value is 'String.Empty'.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                VerifyWritable();
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Name 2. Null value is 'String.Empty'.
        /// </summary>
        public string Name2
        {
            get { return _name2; }
            set
            {
                VerifyWritable();
                _name2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the Street Line 1. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine1
        {
            get { return _streetLine1; }
            set
            {
                VerifyWritable();
                _streetLine1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the Street Line 2. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine2
        {
            get { return _streetLine2; }
            set
            {
                VerifyWritable();
                _streetLine2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the Street Line 3. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine3
        {
            get { return _streetLine3; }
            set
            {
                VerifyWritable();
                _streetLine3 = value;
            }
        }

        /// <summary>
        /// Gets or sets the City. Null value is 'String.Empty'.
        /// </summary>
        public string City
        {
            get { return _city; }
            set
            {
                VerifyWritable();
                _city = value;
            }
        }

        /// <summary>
        /// Gets or sets the State. Null value is 'String.Empty'.
        /// </summary>
        public string StateCode
        {
            get { return _stateCode; }
            set
            {
                VerifyWritable();
                _stateCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Zip Code. Null value is 'String.Empty'.
        /// </summary>
        public string ZipCode
        {
            get { return _zipCode; }
            set
            {
                VerifyWritable();
                _zipCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Business Operations. Null value is 'String.Empty'.
        /// </summary>
        public string BusinessOperations
        {
            get { return _businessOperations; }
            set
            {
                VerifyWritable();
                _businessOperations = value;
            }
        }

        /// <summary>
        /// Gets or sets the Business Category. Null value is 'String.Empty'.
        /// </summary>
        public string BusinessCategory
        {
            get { return _businessCategory; }
            set
            {
                VerifyWritable();
                _businessCategory = value;
            }
        }

        /// <summary>
        /// Gets or sets the DOT Number. Null value is 'String.Empty'.
        /// </summary>
        public string DOTNumber
        {
            get { return _dotNumber; }
            set
            {
                VerifyWritable();
                _dotNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the SIC. Null value is 'String.Empty'.
        /// </summary>
        public string SICCode
        {
            get { return _sicCode; }
            set
            {
                VerifyWritable();
                _sicCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the NAICS Code. Null value is 'String.Empty'.
        /// </summary>
        public string NAICSCode
        {
            get { return _naicsCode; }
            set
            {
                VerifyWritable();
                _naicsCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Underwriter. Null value is 'String.Empty'.
        /// </summary>
        public string Underwriter
        {
            get { return _underwriter; }
            set
            {
                VerifyWritable();
                _underwriter = value;
            }
        }

        /// <summary>
        /// Gets or sets the Underwriter. Null value is 'String.Empty'.
        /// </summary>
        public string Underwriter2
        {
            get { return _underwriter2; }
            set
            {

                VerifyWritable();
                _underwriter2 = value;
            }
        }
        
        /// Gets or sets the Company Website. Null value is 'String.Empty'.
        /// </summary>
        public string CompanyWebsite
        {
            get { return _companyWebsite; }
            set
            {
                VerifyWritable();
                _companyWebsite = value;
            }
        }

        /// <summary>
        /// Gets or sets the Agent Contact Name. Null value is 'String.Empty'.
        /// </summary>
        public string AgentContactName
        {
            get { return _agentContactName; }
            set
            {
                VerifyWritable();
                _agentContactName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Agent Contact Phone Number. Null value is 'String.Empty'.
        /// </summary>
        public string AgentContactPhoneNumber
        {
            get { return _agentContactPhoneNumber; }
            set
            {
                VerifyWritable();
                _agentContactPhoneNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Agent Email. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid AgentEmailID
        {
            get { return _agentEmailID; }
            set
            {
                VerifyWritable();
                _agentEmailID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Nonrenewed Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime NonrenewedDate
        {
            get { return _nonrenewedDate; }
            set
            {
                VerifyWritable();
                _nonrenewedDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Exclusion Bit.
        /// </summary>
        public int ExclusionBit
        {
            get { return _exclusionBit; }
            set
            {
                VerifyWritable();
                _exclusionBit = value;
            }
        }

        /// <summary>
        /// Gets or sets the Insured Contact Name. Null value is 'String.Empty'.
        /// </summary>
        public string ContactName
        {
            get { return _contactName; }
            set
            {
                VerifyWritable();
                _contactName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Insured Contact Phone Number. Null value is 'String.Empty'.
        /// </summary>
        public string ContactPhoneNumber
        {
            get { return _contactPhoneNumber; }
            set
            {
                VerifyWritable();
                _contactPhoneNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Insured Contact Email. Null value is 'String.Empty'.
        /// </summary>
        public string ContactEmail
        {
            get { return _contactEmail; }
            set
            {
                VerifyWritable();
                _contactEmail = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Agency object related to this entity.
        /// </summary>
        public Agency Agency
        {
            get
            {
                _agencyHolder.Key = _agencyID;
                return (Agency)_agencyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a AgencyEmail object related to this entity.
        /// </summary>
        public AgencyEmail AgencyEmail
        {
            get
            {
                _agentEmailAgencyEmailHolder.Key = _agentEmailID;
                return (AgencyEmail)_agentEmailAgencyEmailHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a SIC object related to this entity.
        /// </summary>
        public SIC SIC
        {
            get
            {
                _sicHolder.Key = _sicCode;
                return (SIC)_sicHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a State object related to this entity.
        /// </summary>
        public State State
        {
            get
            {
                _stateHolder.Key = _stateCode;
                return (State)_stateHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_clientID": return _clientID;
                    case "_portfolioID": return _portfolioID;
                    case "_companyID": return _companyID;
                    case "_agencyID": return _agencyID;
                    case "_name": return _name;
                    case "_name2": return _name2;
                    case "_streetLine1": return _streetLine1;
                    case "_streetLine2": return _streetLine2;
                    case "_streetLine3": return _streetLine3;
                    case "_city": return _city;
                    case "_stateCode": return _stateCode;
                    case "_zipCode": return _zipCode;
                    case "_businessOperations": return _businessOperations;
                    case "_businessCategory": return _businessCategory;
                    case "_dotNumber": return _dotNumber;
                    case "_sicCode": return _sicCode;
                    case "_naicsCode": return _naicsCode;
                    case "_underwriter": return _underwriter;
                    case "_underwriter2": return _underwriter2;    
                    case "_companyWebsite": return _companyWebsite;
                    case "_agentContactName": return _agentContactName;
                    case "_agentContactPhoneNumber": return _agentContactPhoneNumber;
                    case "_agentEmailID": return _agentEmailID;
                    case "_nonrenewedDate": return _nonrenewedDate;
                    case "_exclusionBit": return _exclusionBit;
                    case "_contactName": return _contactName;
                    case "_contactPhoneNumber": return _contactPhoneNumber;
                    case "_contactEmail": return _contactEmail;
                    case "_agencyHolder": return _agencyHolder;
                    case "_agentEmailAgencyEmailHolder": return _agentEmailAgencyEmailHolder;
                    case "_companyHolder": return _companyHolder;
                    case "_sicHolder": return _sicHolder;
                    case "_stateHolder": return _stateHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_clientID": _clientID = (string)value; break;
                    case "_portfolioID": _portfolioID = (string)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_agencyID": _agencyID = (Guid)value; break;
                    case "_name": _name = (string)value; break;
                    case "_name2": _name2 = (string)value; break;
                    case "_streetLine1": _streetLine1 = (string)value; break;
                    case "_streetLine2": _streetLine2 = (string)value; break;
                    case "_streetLine3": _streetLine3 = (string)value; break;
                    case "_city": _city = (string)value; break;
                    case "_stateCode": _stateCode = (string)value; break;
                    case "_zipCode": _zipCode = (string)value; break;
                    case "_businessOperations": _businessOperations = (string)value; break;
                    case "_businessCategory": _businessCategory = (string)value; break;
                    case "_dotNumber": _dotNumber = (string)value; break;
                    case "_sicCode": _sicCode = (string)value; break;
                    case "_naicsCode": _naicsCode = (string)value; break;
                    case "_underwriter": _underwriter = (string)value; break;
                    case "_underwriter2": _underwriter2 = (string)value; break;                    
                    case "_companyWebsite": _companyWebsite = (string)value; break;
                    case "_agentContactName": _agentContactName = (string)value; break;
                    case "_agentContactPhoneNumber": _agentContactPhoneNumber = (string)value; break;
                    case "_agentEmailID": _agentEmailID = (Guid)value; break;
                    case "_nonrenewedDate": _nonrenewedDate = (DateTime)value; break;
                    case "_exclusionBit": _exclusionBit = (int)value; break;
                    case "_contactName": _contactName = (string)value; break;
                    case "_contactPhoneNumber": _contactPhoneNumber = (string)value; break;
                    case "_contactEmail": _contactEmail = (string)value; break;
                    case "_agencyHolder": _agencyHolder = (ObjectHolder)value; break;
                    case "_agentEmailAgencyEmailHolder": _agentEmailAgencyEmailHolder = (ObjectHolder)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_sicHolder": _sicHolder = (ObjectHolder)value; break;
                    case "_stateHolder": _stateHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static Insured Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(Insured), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch Insured entity with ID = '{0}'.", id));
            }
            return (Insured)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Insured GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Insured), opathExpression);
            return (Insured)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Insured GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Insured))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Insured)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Insured[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Insured), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Insured[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Insured))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Insured[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Insured[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Insured), opathExpression, opathSort);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public Insured GetWritableInstance()
        
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            Insured clone = (Insured)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }

        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.ClientID, _clientID);
            Validator.Validate(Field.PortfolioID, _portfolioID);
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.AgencyID, _agencyID);
            Validator.Validate(Field.Name, _name);
            Validator.Validate(Field.Name2, _name2);
            Validator.Validate(Field.StreetLine1, _streetLine1);
            Validator.Validate(Field.StreetLine2, _streetLine2);
            Validator.Validate(Field.StreetLine3, _streetLine3);
            Validator.Validate(Field.City, _city);
            Validator.Validate(Field.StateCode, _stateCode);
            Validator.Validate(Field.ZipCode, _zipCode);
            Validator.Validate(Field.BusinessOperations, _businessOperations);
            Validator.Validate(Field.BusinessCategory, _businessCategory);
            Validator.Validate(Field.DOTNumber, _dotNumber);
            Validator.Validate(Field.SICCode, _sicCode);
            Validator.Validate(Field.NAICSCode, _naicsCode);
            Validator.Validate(Field.Underwriter, _underwriter);
            Validator.Validate(Field.Underwriter, _underwriter2);  
            Validator.Validate(Field.CompanyWebsite, _companyWebsite);
            Validator.Validate(Field.AgentContactName, _agentContactName);
            Validator.Validate(Field.AgentContactPhoneNumber, _agentContactPhoneNumber);
            Validator.Validate(Field.AgentEmailID, _agentEmailID);
            Validator.Validate(Field.NonrenewedDate, _nonrenewedDate);
            Validator.Validate(Field.ExclusionBit, _exclusionBit);
            Validator.Validate(Field.ContactName, _contactName);
            Validator.Validate(Field.ContactPhoneNumber, _contactPhoneNumber);
            Validator.Validate(Field.ContactEmail, _contactEmail);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _clientID = null;
            _portfolioID = null;
            _companyID = Guid.Empty;
            _agencyID = Guid.Empty;
            _name = null;
            _name2 = null;
            _streetLine1 = null;
            _streetLine2 = null;
            _streetLine3 = null;
            _city = null;
            _stateCode = null;
            _zipCode = null;
            _businessOperations = null;
            _businessCategory = null;
            _dotNumber = null;
            _sicCode = null;
            _naicsCode = null;
            _underwriter = null;
            _underwriter2 = null;            
            _companyWebsite = null;
            _agentContactName = null;
            _agentContactPhoneNumber = null;
            _agentEmailID = Guid.Empty;
            _nonrenewedDate = DateTime.MinValue;
            _exclusionBit = Int32.MinValue;
            _contactName = null;
            _contactPhoneNumber = null;
            _contactEmail = null;
            _agencyHolder = null;
            _agentEmailAgencyEmailHolder = null;
            _companyHolder = null;
            _sicHolder = null;
            _stateHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the Insured entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the ClientID field.
            /// </summary>
            ClientID,
            /// <summary>
            /// Represents the PortfolioID field.
            /// </summary>
            PortfolioID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the AgencyID field.
            /// </summary>
            AgencyID,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the Name2 field.
            /// </summary>
            Name2,
            /// <summary>
            /// Represents the StreetLine1 field.
            /// </summary>
            StreetLine1,
            /// <summary>
            /// Represents the StreetLine2 field.
            /// </summary>
            StreetLine2,
            /// <summary>
            /// Represents the StreetLine3 field.
            /// </summary>
            StreetLine3,
            /// <summary>
            /// Represents the City field.
            /// </summary>
            City,
            /// <summary>
            /// Represents the StateCode field.
            /// </summary>
            StateCode,
            /// <summary>
            /// Represents the ZipCode field.
            /// </summary>
            ZipCode,
            /// <summary>
            /// Represents the BusinessOperations field.
            /// </summary>
            BusinessOperations,
            /// <summary>
            /// Represents the BusinessCategory field.
            /// </summary>
            BusinessCategory,
            /// <summary>
            /// Represents the DOTNumber field.
            /// </summary>
            DOTNumber,
            /// <summary>
            /// Represents the SICCode field.
            /// </summary>
            SICCode,
            /// <summary>
            /// Represents the NAICSCode field.
            /// </summary>
            NAICSCode,
            /// <summary>
            /// Represents the Underwriter field.
            /// </summary>
            Underwriter,
            /// Represents the Underwriter field.
            /// </summary>
            Underwriter2, 
            /// <summary>
            /// Represents the CompanyWebsite field.
            /// </summary>
            CompanyWebsite,
            /// <summary>
            /// Represents the AgentContactName field.
            /// </summary>
            AgentContactName,
            /// <summary>
            /// Represents the AgentContactPhoneNumber field.
            /// </summary>
            AgentContactPhoneNumber,
            /// <summary>
            /// Represents the AgentEmailID field.
            /// </summary>
            AgentEmailID,
            /// <summary>
            /// Represents the NonrenewedDate field.
            /// </summary>
            NonrenewedDate,
            /// <summary>
            /// Represents the ExclusionBit field.
            /// </summary>
            ExclusionBit,
            /// <summary>
            /// Represents the ContactName field.
            /// </summary>
            ContactName,
            /// <summary>
            /// Represents the ContactPhoneNumber field.
            /// </summary>
            ContactPhoneNumber,
            /// <summary>
            /// Represents the ContactEmail field.
            /// </summary>
            ContactEmail,
        }

        #endregion
    }
}