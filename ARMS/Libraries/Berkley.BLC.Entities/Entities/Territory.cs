using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a Territory entity, which maps to table 'Territory' in the database.
	/// </summary>
	public class Territory : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private Territory()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this Territory.</param>
		public Territory(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


        #region --- Generated Members ---

        private Guid _id;
        private Guid _companyID;
        private string _surveyStatusTypeCode;
        private string _name;
        private string _description = String.Empty;
        private bool _inHouseSelection;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _surveyStatusTypeHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Territory ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
            set
            {
                VerifyWritable();
                _companyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Survey Status Type.
        /// </summary>
        public string SurveyStatusTypeCode
        {
            get { return _surveyStatusTypeCode; }
            set
            {
                VerifyWritable();
                _surveyStatusTypeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Territory Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                VerifyWritable();
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Territory Description. Null value is 'String.Empty'.
        /// </summary>
        public string Description
        {
            get { return _description; }
            set
            {
                VerifyWritable();
                _description = value;
            }
        }


        /// <summary>
        /// Gets or sets the In House Selection.
        /// </summary>
        public bool InHouseSelection
        {
            get { return _inHouseSelection; }
            set
            {
                VerifyWritable();
                _inHouseSelection = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a SurveyStatusType object related to this entity.
        /// </summary>
        public SurveyStatusType SurveyStatusType
        {
            get
            {
                _surveyStatusTypeHolder.Key = _surveyStatusTypeCode;
                return (SurveyStatusType)_surveyStatusTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_companyID": return _companyID;
                    case "_surveyStatusTypeCode": return _surveyStatusTypeCode;
                    case "_name": return _name;
                    case "_description": return _description;
                    case "_inHouseSelection": return _inHouseSelection;
                    case "_companyHolder": return _companyHolder;
                    case "_surveyStatusTypeHolder": return _surveyStatusTypeHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_surveyStatusTypeCode": _surveyStatusTypeCode = (string)value; break;
                    case "_name": _name = (string)value; break;
                    case "_description": _description = (string)value; break;
                    case "_inHouseSelection": _inHouseSelection = (bool)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_surveyStatusTypeHolder": _surveyStatusTypeHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static Territory Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(Territory), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch Territory entity with ID = '{0}'.", id));
            }
            return (Territory)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Territory GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Territory), opathExpression);
            return (Territory)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Territory GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Territory))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Territory)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Territory[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Territory), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Territory[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Territory))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Territory[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Territory[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Territory), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public Territory GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            Territory clone = (Territory)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.SurveyStatusTypeCode, _surveyStatusTypeCode);
            Validator.Validate(Field.Name, _name);
            Validator.Validate(Field.Description, _description);
            Validator.Validate(Field.InHouseSelection, _inHouseSelection);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _companyID = Guid.Empty;
            _surveyStatusTypeCode = null;
            _name = null;
            _description = null;
            _inHouseSelection = false;
            _companyHolder = null;
            _surveyStatusTypeHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the Territory entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the SurveyStatusTypeCode field.
            /// </summary>
            SurveyStatusTypeCode,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the Description field.
            /// </summary>
            Description,
            /// <summary>
            /// Represents the InHouseSelection field.
            /// </summary>
            InHouseSelection,
        }

        #endregion
    }
}