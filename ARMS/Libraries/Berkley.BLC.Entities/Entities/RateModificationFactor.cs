using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a RateModificationFactor entity, which maps to table 'RateModificationFactor' in the database.
	/// </summary>
	public class RateModificationFactor : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private RateModificationFactor()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this RateModificationFactor.</param>
		public RateModificationFactor(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _id;
		private Guid _policyID;
		private string _stateCode;
		private string _typeCode;
		private decimal _rate;
		private ObjectHolder _policyHolder = null;
		private ObjectHolder _rateModificationFactorTypeHolder = null;
		private ObjectHolder _stateHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Rate Modification Factor ID.
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets or sets the Policy.
		/// </summary>
		public Guid PolicyID
		{
			get { return _policyID; }
			set
			{
				VerifyWritable();
				_policyID = value;
			}
		}

		/// <summary>
		/// Gets or sets the State.
		/// </summary>
		public string StateCode
		{
			get { return _stateCode; }
			set
			{
				VerifyWritable();
				_stateCode = value;
			}
		}

		/// <summary>
		/// Gets or sets the Rate Modification Factor Type.
		/// </summary>
		public string TypeCode
		{
			get { return _typeCode; }
			set
			{
				VerifyWritable();
				_typeCode = value;
			}
		}

		/// <summary>
		/// Gets or sets the Rate.
		/// </summary>
		public decimal Rate
		{
			get { return _rate; }
			set
			{
				VerifyWritable();
				_rate = value;
			}
		}

		/// <summary>
		/// Gets the instance of a Policy object related to this entity.
		/// </summary>
		public Policy Policy
		{
			get
			{
				_policyHolder.Key = _policyID;
				return (Policy)_policyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a RateModificationFactorType object related to this entity.
		/// </summary>
		public RateModificationFactorType Type
		{
			get
			{
				_rateModificationFactorTypeHolder.Key = _typeCode;
				return (RateModificationFactorType)_rateModificationFactorTypeHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a State object related to this entity.
		/// </summary>
		public State State
		{
			get
			{
				_stateHolder.Key = _stateCode;
				return (State)_stateHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_policyID": return _policyID;
					case "_stateCode": return _stateCode;
					case "_typeCode": return _typeCode;
					case "_rate": return _rate;
					case "_policyHolder": return _policyHolder;
					case "_rateModificationFactorTypeHolder": return _rateModificationFactorTypeHolder;
					case "_stateHolder": return _stateHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (Guid)value; break;
					case "_policyID": _policyID = (Guid)value; break;
					case "_stateCode": _stateCode = (string)value; break;
					case "_typeCode": _typeCode = (string)value; break;
					case "_rate": _rate = (decimal)value; break;
					case "_policyHolder": _policyHolder = (ObjectHolder)value; break;
					case "_rateModificationFactorTypeHolder": _rateModificationFactorTypeHolder = (ObjectHolder)value; break;
					case "_stateHolder": _stateHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static RateModificationFactor Get(Guid id)
		{
			OPathQuery query = new OPathQuery(typeof(RateModificationFactor), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch RateModificationFactor entity with ID = '{0}'.", id));
			}
			return (RateModificationFactor)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static RateModificationFactor GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(RateModificationFactor), opathExpression);
			return (RateModificationFactor)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static RateModificationFactor GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(RateModificationFactor) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (RateModificationFactor)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static RateModificationFactor[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(RateModificationFactor), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static RateModificationFactor[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(RateModificationFactor) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (RateModificationFactor[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static RateModificationFactor[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(RateModificationFactor), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public RateModificationFactor GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			RateModificationFactor clone = (RateModificationFactor)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ID, _id);
			Validator.Validate(Field.PolicyID, _policyID);
			Validator.Validate(Field.StateCode, _stateCode);
			Validator.Validate(Field.TypeCode, _typeCode);
			Validator.Validate(Field.Rate, _rate);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Guid.Empty;
			_policyID = Guid.Empty;
			_stateCode = null;
			_typeCode = null;
			_rate = Decimal.MinValue;
			_policyHolder = null;
			_rateModificationFactorTypeHolder = null;
			_stateHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the RateModificationFactor entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the PolicyID field.
			/// </summary>
			PolicyID,
			/// <summary>
			/// Represents the StateCode field.
			/// </summary>
			StateCode,
			/// <summary>
			/// Represents the TypeCode field.
			/// </summary>
			TypeCode,
			/// <summary>
			/// Represents the Rate field.
			/// </summary>
			Rate,
		}
		
		#endregion
	}
}