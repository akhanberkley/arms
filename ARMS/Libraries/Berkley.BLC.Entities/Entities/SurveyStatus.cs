using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyStatus entity, which maps to table 'SurveyStatus' in the database.
	/// </summary>
	public class SurveyStatus : IObjectHelper
    {
        #region Public Fields

        /// <summary>
        /// Represents a On Hold Survey status type.
        /// </summary>
        public static readonly SurveyStatus OnHoldSurvey = SurveyStatus.Get("OH");
        /// <summary>
        /// Represents a Awaiting Acceptance status type.
        /// </summary>
        public static readonly SurveyStatus AwaitingAcceptance = SurveyStatus.Get("SW");
        /// <summary>
        /// Represents a Assigned Survey status type.
        /// </summary>
        public static readonly SurveyStatus AssignedSurvey = SurveyStatus.Get("SA");
        /// <summary>
        /// Represents a Unassigned Survey status type.
        /// </summary>
        public static readonly SurveyStatus UnassignedSurvey = SurveyStatus.Get("SU");
        /// <summary>
        /// Represents a Assigned Review status type.
        /// </summary>
        public static readonly SurveyStatus AssignedReview = SurveyStatus.Get("RA");
        /// <summary>
        /// Represents a Unassigned Review status type.
        /// </summary>
        public static readonly SurveyStatus UnassignedReview = SurveyStatus.Get("RU");
        /// <summary>
        /// Represents a Closed Survey status type.
        /// </summary>
        public static readonly SurveyStatus ClosedSurvey = SurveyStatus.Get("SC");
        /// <summary>
        /// Represents a Canceled Survey status type.
        /// </summary>
        public static readonly SurveyStatus CanceledSurvey = SurveyStatus.Get("CC");
        /// <summary>
        /// Represents a Returning to Company status type.
        /// </summary>
        public static readonly SurveyStatus ReturningToCompany = SurveyStatus.Get("SR");

        #endregion

        /// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyStatus()
		{
		}

        /// <summary>
        /// Determines if this Status matches one in the the specified list.
        /// </summary>
        /// <param name="list">The list of SurveyStatus objects to compare.</param>
        /// <returns>True if this status matches one in the list.</returns>
        public bool IsInList(params SurveyStatus[] list)
        {
            foreach (SurveyStatus status in list)
            {
                if (status == this)
                {
                    return true;
                }
            }
            return false;
        }

        #region --- Generated Members ---

        private string _code;
        private string _name;
        private string _typeCode;
        private int _displayOrder;
        private ObjectHolder _surveyStatusTypeHolder = null;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Survey Status.
        /// </summary>
        public string Code
        {
            get { return _code; }
        }

        /// <summary>
        /// Gets the Survey Status.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Gets the Survey Status Type.
        /// </summary>
        public string TypeCode
        {
            get { return _typeCode; }
        }

        /// <summary>
        /// Gets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
        }

        /// <summary>
        /// Gets the instance of a SurveyStatusType object related to this entity.
        /// </summary>
        public SurveyStatusType Type
        {
            get
            {
                _surveyStatusTypeHolder.Key = _typeCode;
                return (SurveyStatusType)_surveyStatusTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_code": return _code;
                    case "_name": return _name;
                    case "_typeCode": return _typeCode;
                    case "_displayOrder": return _displayOrder;
                    case "_surveyStatusTypeHolder": return _surveyStatusTypeHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_code": _code = (string)value; break;
                    case "_name": _name = (string)value; break;
                    case "_typeCode": _typeCode = (string)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    case "_surveyStatusTypeHolder": _surveyStatusTypeHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="code">Code of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static SurveyStatus Get(string code)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyStatus), "Code = ?");
            object entity = DB.Engine.GetObject(query, code);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch SurveyStatus entity with Code = '{0}'.", code));
            }
            return (SurveyStatus)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyStatus GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyStatus), opathExpression);
            return (SurveyStatus)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyStatus GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyStatus))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyStatus)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyStatus[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyStatus), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyStatus[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyStatus))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyStatus[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyStatus[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyStatus), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Determines if two SurveyStatus objects have the same semantic value.
        /// </summary>
        /// <param name="one">The first object to compare.</param>
        /// <param name="two">The second object to compare.</param>
        /// <returns>True if they are semantically identical, false otherwise.</returns>
        public static bool operator ==(SurveyStatus one, SurveyStatus two)
        {
            if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
            {
                return true;
            }
            else if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
            {
                return false;
            }
            else
            {
                return (one.CompareTo(two) == 0);
            }
        }

        /// <summary>
        /// Determines if two SurveyStatus objects have different semantic values.
        /// </summary>
        /// <param name="one">The first object to compare.</param>
        /// <param name="two">The second object to compare.</param>
        /// <returns>True if they are semantically dissimilar, false otherwise.</returns>
        public static bool operator !=(SurveyStatus one, SurveyStatus two)
        {
            if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
            {
                return false;
            }
            if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
            {
                return true;
            }
            else
            {
                return (one.CompareTo(two) != 0);
            }
        }

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="value">An object to compare with this instance.</param>
        /// <returns>Less than zero if this instance is less than value. 
        /// Zero if this instance is equal to value.
        /// Greater than zero if this instance is greater than value.</returns>
        public int CompareTo(object value)
        {
            if (value == null)
            {
                return 1;
            }
            if (value is SurveyStatus)
            {
                SurveyStatus obj = (SurveyStatus)value;
                return (this.Code.CompareTo(obj.Code));
            }
            throw new ArgumentException("Value is not the correct type.");
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode()
        {
            return this.Code.GetHashCode();
        }

        /// <summary>
        /// Determines whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="value">The object to compare with the current instance. </param>
        /// <returns>true if the specified object is equal to the current instance; otherwise, false.</returns>
        public override bool Equals(object value)
        {
            return (this == (value as SurveyStatus));
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _code = null;
            _name = null;
            _typeCode = null;
            _displayOrder = Int32.MinValue;
            _surveyStatusTypeHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the SurveyStatus entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the Code field.
            /// </summary>
            Code,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the TypeCode field.
            /// </summary>
            TypeCode,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
        }

        #endregion
    }
}