using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a VWLocation entity, which maps to table 'VW_Location' in the database.
    /// </summary>
    public class VWLocation : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private VWLocation()
        {
        }

        /// <summary>
        /// Determines if the location is to be included in the survey or not.
        /// </summary>
        public bool LocationExcluded
        {
            get
            {
                return LocationExclusionCount > 0;
            }
        }

        /// <summary>
        /// Determines if all the policies have been excluded for this location.
        /// </summary>
        public bool AllPoliciesExcluded
        {
            get
            {
                return (ActivePolicyCount == PolicyExclusionCount);
            }
        }

        /// <summary>
        /// Determines if any contacts have been added for this location.
        /// </summary>
        public bool NoContactsAdded
        {
            get
            {
                return ContactCount <= 0;
            }
        }

        /// <summary>
        /// Determines if any active policies exist for this location.
        /// </summary>
        public bool NoActivePolicies
        {
            get
            {
                return ActivePolicyCount <= 0;
            }
        }

        /// <summary>
        /// Determines if all of the reports have been selected for all the policies of the location.
        /// </summary>
        public bool NotAllPolicyReportsSelected
        {
            get
            {
                return ActivePolicyCount != (PolicyExclusionCount + PolicyBeenViewedCount);
            }
        }


        #region --- Generated Members ---

        private Guid _locationID;
        private Guid _insuredID = Guid.Empty;
        private string _policySystemKey = String.Empty;
        private string _policySymbol = String.Empty;
        private int _locationNumber = Int32.MinValue;
        private string _streetLine1 = String.Empty;
        private string _streetLine2 = String.Empty;
        private string _city = String.Empty;
        private string _stateCode = String.Empty;
        private string _zipCode = String.Empty;
        private string _description = String.Empty;
        private bool _isActive;
        private int _buildingCount = Int32.MinValue;
        private int _contactCount = Int32.MinValue;
        private int _locationExclusionCount = Int32.MinValue;
        private int _activePolicyCount = Int32.MinValue;
        private int _policyExclusionCount = Int32.MinValue;
        private int _policyBeenViewedCount = Int32.MinValue;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Location ID.
        /// </summary>
        public Guid LocationID
        {
            get { return _locationID; }
        }

        /// <summary>
        /// Gets the Insured ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid InsuredID
        {
            get { return _insuredID; }
        }

        /// <summary>
        /// Gets the Policy System Key. Null value is 'String.Empty'.
        /// </summary>
        public string PolicySystemKey
        {
            get { return _policySystemKey; }
        }

        /// <summary>
        /// Gets the Policy Symbol. Null value is 'String.Empty'.
        /// </summary>
        public string PolicySymbol
        {
            get { return _policySymbol; }
        }

        /// <summary>
        /// Gets the Location Number. Null value is 'Int32.MinValue'.
        /// </summary>
        public int LocationNumber
        {
            get { return _locationNumber; }
        }

        /// <summary>
        /// Gets the Street Line 1. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine1
        {
            get { return _streetLine1; }
        }

        /// <summary>
        /// Gets the Street Line 2. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine2
        {
            get { return _streetLine2; }
        }

        /// <summary>
        /// Gets the City. Null value is 'String.Empty'.
        /// </summary>
        public string City
        {
            get { return _city; }
        }

        /// <summary>
        /// Gets the State Code. Null value is 'String.Empty'.
        /// </summary>
        public string StateCode
        {
            get { return _stateCode; }
        }

        /// <summary>
        /// Gets the Zip Code. Null value is 'String.Empty'.
        /// </summary>
        public string ZipCode
        {
            get { return _zipCode; }
        }

        /// <summary>
        /// Gets the Description. Null value is 'String.Empty'.
        /// </summary>
        public string Description
        {
            get { return _description; }
        }

        /// <summary>
        /// Gets the Is Active.
        /// </summary>
        public bool IsActive
        {
            get { return _isActive; }
        }

        /// <summary>
        /// Gets the Building Count. Null value is 'Int32.MinValue'.
        /// </summary>
        public int BuildingCount
        {
            get { return _buildingCount; }
        }

        /// <summary>
        /// Gets the Contact Count. Null value is 'Int32.MinValue'.
        /// </summary>
        public int ContactCount
        {
            get { return _contactCount; }
        }

        /// <summary>
        /// Gets the Location Exclusion Count. Null value is 'Int32.MinValue'.
        /// </summary>
        public int LocationExclusionCount
        {
            get { return _locationExclusionCount; }
        }

        /// <summary>
        /// Gets the Active Policy Count. Null value is 'Int32.MinValue'.
        /// </summary>
        public int ActivePolicyCount
        {
            get { return _activePolicyCount; }
        }

        /// <summary>
        /// Gets the Policy Exclusion Count. Null value is 'Int32.MinValue'.
        /// </summary>
        public int PolicyExclusionCount
        {
            get { return _policyExclusionCount; }
        }

        /// <summary>
        /// Gets the Policy Been Viewed Count. Null value is 'Int32.MinValue'.
        /// </summary>
        public int PolicyBeenViewedCount
        {
            get { return _policyBeenViewedCount; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_locationID": return _locationID;
                    case "_insuredID": return _insuredID;
                    case "_policySystemKey": return _policySystemKey;
                    case "_policySymbol": return _policySymbol;
                    case "_locationNumber": return _locationNumber;
                    case "_streetLine1": return _streetLine1;
                    case "_streetLine2": return _streetLine2;
                    case "_city": return _city;
                    case "_stateCode": return _stateCode;
                    case "_zipCode": return _zipCode;
                    case "_description": return _description;
                    case "_isActive": return _isActive;
                    case "_buildingCount": return _buildingCount;
                    case "_contactCount": return _contactCount;
                    case "_locationExclusionCount": return _locationExclusionCount;
                    case "_activePolicyCount": return _activePolicyCount;
                    case "_policyExclusionCount": return _policyExclusionCount;
                    case "_policyBeenViewedCount": return _policyBeenViewedCount;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_locationID": _locationID = (Guid)value; break;
                    case "_insuredID": _insuredID = (Guid)value; break;
                    case "_policySystemKey": _policySystemKey = (string)value; break;
                    case "_policySymbol": _policySymbol = (string)value; break;
                    case "_locationNumber": _locationNumber = (int)value; break;
                    case "_streetLine1": _streetLine1 = (string)value; break;
                    case "_streetLine2": _streetLine2 = (string)value; break;
                    case "_city": _city = (string)value; break;
                    case "_stateCode": _stateCode = (string)value; break;
                    case "_zipCode": _zipCode = (string)value; break;
                    case "_description": _description = (string)value; break;
                    case "_isActive": _isActive = (bool)value; break;
                    case "_buildingCount": _buildingCount = (int)value; break;
                    case "_contactCount": _contactCount = (int)value; break;
                    case "_locationExclusionCount": _locationExclusionCount = (int)value; break;
                    case "_activePolicyCount": _activePolicyCount = (int)value; break;
                    case "_policyExclusionCount": _policyExclusionCount = (int)value; break;
                    case "_policyBeenViewedCount": _policyBeenViewedCount = (int)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="locationID">LocationID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static VWLocation Get(Guid locationID)
        {
            OPathQuery query = new OPathQuery(typeof(VWLocation), "LocationID = ?");
            object entity = DB.Engine.GetObject(query, locationID);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch VWLocation entity with LocationID = '{0}'.", locationID));
            }
            return (VWLocation)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static VWLocation GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWLocation), opathExpression);
            return (VWLocation)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static VWLocation GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(VWLocation))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (VWLocation)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWLocation[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWLocation), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWLocation[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(VWLocation))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (VWLocation[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWLocation[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWLocation), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _locationID = Guid.Empty;
            _insuredID = Guid.Empty;
            _policySystemKey = null;
            _policySymbol = null;
            _locationNumber = Int32.MinValue;
            _streetLine1 = null;
            _streetLine2 = null;
            _city = null;
            _stateCode = null;
            _zipCode = null;
            _description = null;
            _isActive = false;
            _buildingCount = Int32.MinValue;
            _contactCount = Int32.MinValue;
            _locationExclusionCount = Int32.MinValue;
            _activePolicyCount = Int32.MinValue;
            _policyExclusionCount = Int32.MinValue;
            _policyBeenViewedCount = Int32.MinValue;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the VWLocation entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the LocationID field.
            /// </summary>
            LocationID,
            /// <summary>
            /// Represents the InsuredID field.
            /// </summary>
            InsuredID,
            /// <summary>
            /// Represents the PolicySystemKey field.
            /// </summary>
            PolicySystemKey,
            /// <summary>
            /// Represents the PolicySymbol field.
            /// </summary>
            PolicySymbol,
            /// <summary>
            /// Represents the LocationNumber field.
            /// </summary>
            LocationNumber,
            /// <summary>
            /// Represents the StreetLine1 field.
            /// </summary>
            StreetLine1,
            /// <summary>
            /// Represents the StreetLine2 field.
            /// </summary>
            StreetLine2,
            /// <summary>
            /// Represents the City field.
            /// </summary>
            City,
            /// <summary>
            /// Represents the StateCode field.
            /// </summary>
            StateCode,
            /// <summary>
            /// Represents the ZipCode field.
            /// </summary>
            ZipCode,
            /// <summary>
            /// Represents the Description field.
            /// </summary>
            Description,
            /// <summary>
            /// Represents the IsActive field.
            /// </summary>
            IsActive,
            /// <summary>
            /// Represents the BuildingCount field.
            /// </summary>
            BuildingCount,
            /// <summary>
            /// Represents the ContactCount field.
            /// </summary>
            ContactCount,
            /// <summary>
            /// Represents the LocationExclusionCount field.
            /// </summary>
            LocationExclusionCount,
            /// <summary>
            /// Represents the ActivePolicyCount field.
            /// </summary>
            ActivePolicyCount,
            /// <summary>
            /// Represents the PolicyExclusionCount field.
            /// </summary>
            PolicyExclusionCount,
            /// <summary>
            /// Represents the PolicyBeenViewedCount field.
            /// </summary>
            PolicyBeenViewedCount,
            /// <summary>
            /// Represents the PolicyBeenViewedCount field.
            /// </summary>
            LocationExcluded,
            /// <summary>
            /// Represents the PolicyBeenViewedCount field.
            /// </summary>
            AllPoliciesExcluded,
            /// <summary>
            /// Represents the PolicyBeenViewedCount field.
            /// </summary>
            NoContactsAdded,
            /// <summary>
            /// Represents the PolicyBeenViewedCount field.
            /// </summary>
            NoActivePolicies,
            /// <summary>
            /// Represents the PolicyBeenViewedCount field.
            /// </summary>
            NotAllPolicyReportsSelected,
        }

        #endregion
    }
}