using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a Notification entity, which maps to table 'Notification' in the database.
	/// </summary>
	public class Notification : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private Notification()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this Notification.</param>
		public Notification(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _id;
		private string _description;
		private DateTime _startDate;
		private DateTime _endDate;
		private string _link = String.Empty;
		private int _type = Int32.MinValue;
		private int _severity = Int32.MinValue;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Notification ID.
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets or sets the Description.
		/// </summary>
		public string Description
		{
			get { return _description; }
			set
			{
				VerifyWritable();
				_description = value;
			}
		}

		/// <summary>
		/// Gets or sets the Start Date.
		/// </summary>
		public DateTime StartDate
		{
			get { return _startDate; }
			set
			{
				VerifyWritable();
				_startDate = value;
			}
		}

		/// <summary>
		/// Gets or sets the End Date.
		/// </summary>
		public DateTime EndDate
		{
			get { return _endDate; }
			set
			{
				VerifyWritable();
				_endDate = value;
			}
		}

		/// <summary>
		/// Gets or sets the Link. Null value is 'String.Empty'.
		/// </summary>
		public string Link
		{
			get { return _link; }
			set
			{
				VerifyWritable();
				_link = value;
			}
		}

		/// <summary>
		/// Gets or sets the Type. Null value is 'Int32.MinValue'.
		/// </summary>
		public int Type
		{
			get { return _type; }
			set
			{
				VerifyWritable();
				_type = value;
			}
		}

		/// <summary>
		/// Gets or sets the Severity. Null value is 'Int32.MinValue'.
		/// </summary>
		public int Severity
		{
			get { return _severity; }
			set
			{
				VerifyWritable();
				_severity = value;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_description": return _description;
					case "_startDate": return _startDate;
					case "_endDate": return _endDate;
					case "_link": return _link;
					case "_type": return _type;
					case "_severity": return _severity;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (Guid)value; break;
					case "_description": _description = (string)value; break;
					case "_startDate": _startDate = (DateTime)value; break;
					case "_endDate": _endDate = (DateTime)value; break;
					case "_link": _link = (string)value; break;
					case "_type": _type = (int)value; break;
					case "_severity": _severity = (int)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static Notification Get(Guid id)
		{
			OPathQuery query = new OPathQuery(typeof(Notification), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch Notification entity with ID = '{0}'.", id));
			}
			return (Notification)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static Notification GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(Notification), opathExpression);
			return (Notification)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static Notification GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(Notification) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (Notification)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static Notification[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(Notification), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static Notification[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(Notification) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (Notification[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static Notification[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(Notification), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public Notification GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			Notification clone = (Notification)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ID, _id);
			Validator.Validate(Field.Description, _description);
			Validator.Validate(Field.StartDate, _startDate);
			Validator.Validate(Field.EndDate, _endDate);
			Validator.Validate(Field.Link, _link);
			Validator.Validate(Field.Type, _type);
			Validator.Validate(Field.Severity, _severity);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Guid.Empty;
			_description = null;
			_startDate = DateTime.MinValue;
			_endDate = DateTime.MinValue;
			_link = null;
			_type = Int32.MinValue;
			_severity = Int32.MinValue;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the Notification entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the Description field.
			/// </summary>
			Description,
			/// <summary>
			/// Represents the StartDate field.
			/// </summary>
			StartDate,
			/// <summary>
			/// Represents the EndDate field.
			/// </summary>
			EndDate,
			/// <summary>
			/// Represents the Link field.
			/// </summary>
			Link,
			/// <summary>
			/// Represents the Type field.
			/// </summary>
			Type,
			/// <summary>
			/// Represents the Severity field.
			/// </summary>
			Severity,
		}
		
		#endregion
	}
}