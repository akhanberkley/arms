using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyQualityType entity, which maps to table 'SurveyQualityType' in the database.
	/// </summary>
	public class SurveyQualityType : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyQualityType()
		{
		}


		#region --- Generated Members ---

		private string _code;
		private string _name;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Survey Quality Type.
		/// </summary>
		public string Code
		{
			get { return _code; }
		}

		/// <summary>
		/// Gets the Survey Quality Type.
		/// </summary>
		public string Name
		{
			get { return _name; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_code": return _code;
					case "_name": return _name;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_code": _code = (string)value; break;
					case "_name": _name = (string)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="code">Code of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static SurveyQualityType Get(string code)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyQualityType), "Code = ?");
			object entity = DB.Engine.GetObject(query, code);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch SurveyQualityType entity with Code = '{0}'.", code));
			}
			return (SurveyQualityType)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyQualityType GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyQualityType), opathExpression);
			return (SurveyQualityType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyQualityType GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyQualityType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyQualityType)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyQualityType[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyQualityType), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyQualityType[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyQualityType) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyQualityType[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyQualityType[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyQualityType), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Determines if two SurveyQualityType objects have the same semantic value.
		/// </summary>
		/// <param name="one">The first object to compare.</param>
		/// <param name="two">The second object to compare.</param>
		/// <returns>True if they are semantically identical, false otherwise.</returns>
		public static bool operator == (SurveyQualityType one, SurveyQualityType two) 
		{
			if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
			{
				return true;
			}
			else if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
			{
				return false;
			}
			else
			{
				return (one.CompareTo(two) == 0);
			}
		}

		/// <summary>
		/// Determines if two SurveyQualityType objects have different semantic values.
		/// </summary>
		/// <param name="one">The first object to compare.</param>
		/// <param name="two">The second object to compare.</param>
		/// <returns>True if they are semantically dissimilar, false otherwise.</returns>
		public static bool operator != (SurveyQualityType one, SurveyQualityType two) 
		{
			if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
			{
				return false;
			}
			if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
			{
				return true;
			}
			else
			{
				return (one.CompareTo(two) != 0);
			}
		}
		
		/// <summary>
		/// Compares the current instance with another object of the same type.
		/// </summary>
		/// <param name="value">An object to compare with this instance.</param>
		/// <returns>Less than zero if this instance is less than value. 
		/// Zero if this instance is equal to value.
		/// Greater than zero if this instance is greater than value.</returns>
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (value is SurveyQualityType)
			{
				SurveyQualityType obj = (SurveyQualityType)value;
				return (this.Code.CompareTo(obj.Code));
			}
			throw new ArgumentException("Value is not the correct type.");
		}

		/// <summary>
		/// Returns the hash code for this instance.
		/// </summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		public override int GetHashCode()
		{
			return this.Code.GetHashCode();
		}

		/// <summary>
		/// Determines whether this instance is equal to the specified object.
		/// </summary>
		/// <param name="value">The object to compare with the current instance. </param>
		/// <returns>true if the specified object is equal to the current instance; otherwise, false.</returns>
		public override bool Equals(object value)
		{
			return (this == (value as SurveyQualityType));
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_code = null;
			_name = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the SurveyQualityType entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the Code field.
			/// </summary>
			Code,
			/// <summary>
			/// Represents the Name field.
			/// </summary>
			Name,
		}
		
		#endregion
	}
}