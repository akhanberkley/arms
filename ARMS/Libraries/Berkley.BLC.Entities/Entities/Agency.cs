using System;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents an Agency entity, which maps to table 'Agency' in the database.
    /// </summary>
    public class Agency : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private Agency()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this Agency.</param>
        public Agency(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Gets the address as an HTML-encoded multi-line string value.
        /// </summary>
        public string HtmlAddress
        {
            get
            {
                return Formatter.Address(this.StreetLine1, this.StreetLine2, this.City, this.StateCode, this.ZipCode);
            }
        }

        /// <summary>
        /// Gets the the phone number as a fomatted string value.
        /// </summary>
        public string HtmlPhoneNumber
        {
            get
            {
                return Formatter.Phone(this.PhoneNumber, "(not specified)");
            }
        }

        /// <summary>
        /// Gets the the fax number as a fomatted string value.
        /// </summary>
        public string HtmlFaxNumber
        {
            get
            {
                return Formatter.Phone(this.FaxNumber, "(not specified)");
            }
        }

        /// <summary>
        /// Selective "assignment operator".  Copied data fields from the Right Hand Side instance to "this" instance
        /// </summary>
        /// <param name="oRhs">Instance with new data</param>
        /// <returns>The modified class</returns>
        public Agency Assign(Agency oRhs)
        {
            Agency oAgency = this;

            if (oAgency.IsReadOnly)
            {
                oAgency = oAgency.GetWritableInstance();
            }

            oAgency.Number = oRhs._number;
            oAgency.SubNumber = oRhs._subNumber;
            oAgency.Name = oRhs._name;
            oAgency.StreetLine1 = oRhs._streetLine1;
            oAgency.StreetLine2 = oRhs._streetLine2;
            oAgency.City = oRhs._city;
            oAgency.StateCode = oRhs._stateCode;
            oAgency.ZipCode = oRhs._zipCode;
            oAgency.PhoneNumber = oRhs._phoneNumber;
            oAgency.FaxNumber = oRhs._faxNumber;
            oAgency.Producer = oRhs._producer;
            return oAgency;
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _companyID;
        private string _number = String.Empty;
        private string _subNumber = String.Empty;
        private string _name = String.Empty;
        private string _streetLine1 = String.Empty;
        private string _streetLine2 = String.Empty;
        private string _city = String.Empty;
        private string _stateCode = String.Empty;
        private string _zipCode = String.Empty;
        private string _phoneNumber = String.Empty;
        private string _faxNumber = String.Empty;
        private string _producer = String.Empty;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _stateHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Agency ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
            set
            {
                VerifyWritable();
                _companyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Agency Number. Null value is 'String.Empty'.
        /// </summary>
        public string Number
        {
            get { return _number; }
            set
            {
                VerifyWritable();
                _number = value;
            }
        }

        /// <summary>
        /// Gets or sets the Agency Sub Number. Null value is 'String.Empty'.
        /// </summary>
        public string SubNumber
        {
            get { return _subNumber; }
            set
            {
                VerifyWritable();
                _subNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Agency Name. Null value is 'String.Empty'.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                VerifyWritable();
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Producer Name. Null value is 'String.Empty'.
        /// </summary>
        public string Producer
        {
            get { return _producer; }
            set
            {
                VerifyWritable();
                _producer = value;
            }
        }

        /// <summary>
        /// Gets or sets the Street Line 1. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine1
        {
            get { return _streetLine1; }
            set
            {
                VerifyWritable();
                _streetLine1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the Street Line 2. Null value is 'String.Empty'.
        /// </summary>
        public string StreetLine2
        {
            get { return _streetLine2; }
            set
            {
                VerifyWritable();
                _streetLine2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the City. Null value is 'String.Empty'.
        /// </summary>
        public string City
        {
            get { return _city; }
            set
            {
                VerifyWritable();
                _city = value;
            }
        }

        /// <summary>
        /// Gets or sets the State. Null value is 'String.Empty'.
        /// </summary>
        public string StateCode
        {
            get { return _stateCode; }
            set
            {
                VerifyWritable();
                _stateCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Zip Code. Null value is 'String.Empty'.
        /// </summary>
        public string ZipCode
        {
            get { return _zipCode; }
            set
            {
                VerifyWritable();
                _zipCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Phone Number. Null value is 'String.Empty'.
        /// </summary>
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                VerifyWritable();
                _phoneNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Fax Number. Null value is 'String.Empty'.
        /// </summary>
        public string FaxNumber
        {
            get { return _faxNumber; }
            set
            {
                VerifyWritable();
                _faxNumber = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a State object related to this entity.
        /// </summary>
        public State State
        {
            get
            {
                _stateHolder.Key = _stateCode;
                return (State)_stateHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_companyID": return _companyID;
                    case "_number": return _number;
                    case "_subNumber": return _subNumber;
                    case "_name": return _name;
                    case "_streetLine1": return _streetLine1;
                    case "_streetLine2": return _streetLine2;
                    case "_city": return _city;
                    case "_stateCode": return _stateCode;
                    case "_zipCode": return _zipCode;
                    case "_phoneNumber": return _phoneNumber;
                    case "_faxNumber": return _faxNumber;
                    case "_companyHolder": return _companyHolder;
                    case "_stateHolder": return _stateHolder;
                    case "_producer": return _producer;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_number": _number = (string)value; break;
                    case "_subNumber": _subNumber = (string)value; break;
                    case "_name": _name = (string)value; break;
                    case "_streetLine1": _streetLine1 = (string)value; break;
                    case "_streetLine2": _streetLine2 = (string)value; break;
                    case "_city": _city = (string)value; break;
                    case "_stateCode": _stateCode = (string)value; break;
                    case "_zipCode": _zipCode = (string)value; break;
                    case "_phoneNumber": _phoneNumber = (string)value; break;
                    case "_faxNumber": _faxNumber = (string)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_stateHolder": _stateHolder = (ObjectHolder)value; break;
                    case "_producer": _producer = (string)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static Agency Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(Agency), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch Agency entity with ID = '{0}'.", id));
            }
            return (Agency)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Agency GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Agency), opathExpression);
            return (Agency)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Agency GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Agency))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Agency)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Agency[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Agency), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Agency[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Agency))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Agency[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Agency[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Agency), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public Agency GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            Agency clone = (Agency)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.Number, _number);
            Validator.Validate(Field.SubNumber, _subNumber);
            Validator.Validate(Field.Name, _name);
            Validator.Validate(Field.Producer, _name);
            Validator.Validate(Field.StreetLine1, _streetLine1);
            Validator.Validate(Field.StreetLine2, _streetLine2);
            Validator.Validate(Field.City, _city);
            Validator.Validate(Field.StateCode, _stateCode);
            Validator.Validate(Field.ZipCode, _zipCode);
            Validator.Validate(Field.PhoneNumber, _phoneNumber);
            Validator.Validate(Field.FaxNumber, _faxNumber);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _companyID = Guid.Empty;
            _number = null;
            _subNumber = null;
            _name = null;
            _streetLine1 = null;
            _streetLine2 = null;
            _city = null;
            _stateCode = null;
            _zipCode = null;
            _phoneNumber = null;
            _faxNumber = null;
            _companyHolder = null;
            _stateHolder = null;
            _producer = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the Agency entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the Number field.
            /// </summary>
            Number,
            /// <summary>
            /// Represents the SubNumber field.
            /// </summary>
            SubNumber,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the StreetLine1 field.
            /// </summary>
            StreetLine1,
            /// <summary>
            /// Represents the StreetLine2 field.
            /// </summary>
            StreetLine2,
            /// <summary>
            /// Represents the City field.
            /// </summary>
            City,
            /// <summary>
            /// Represents the StateCode field.
            /// </summary>
            StateCode,
            /// <summary>
            /// Represents the ZipCode field.
            /// </summary>
            ZipCode,
            /// <summary>
            /// Represents the PhoneNumber field.
            /// </summary>
            PhoneNumber,
            /// <summary>
            /// Represents the FaxNumber field.
            /// </summary>
            FaxNumber,
            /// <summary>
            /// Represents the Producer field.
            /// </summary>
            Producer,
        }

        #endregion
    }
}