using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a Coverage entity, which maps to table 'Coverage' in the database.
	/// </summary>
	public class Coverage : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private Coverage()
		{
		}

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this Coverage.</param>
        public Coverage(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


        #region --- Generated Members ---

        private Guid _id;
        private Guid _insuredID;
        private Guid _policyID = Guid.Empty;
        private Guid _nameID;
        private string _typeCode;
        private string _value = String.Empty;
        private ObjectHolder _coverageNameHolder = null;
        private ObjectHolder _coverageTypeHolder = null;
        private ObjectHolder _insuredHolder = null;
        private ObjectHolder _policyHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Coverage ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Insured.
        /// </summary>
        public Guid InsuredID
        {
            get { return _insuredID; }
            set
            {
                VerifyWritable();
                _insuredID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid PolicyID
        {
            get { return _policyID; }
            set
            {
                VerifyWritable();
                _policyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Coverage Name.
        /// </summary>
        public Guid NameID
        {
            get { return _nameID; }
            set
            {
                VerifyWritable();
                _nameID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Coverage Type.
        /// </summary>
        public string TypeCode
        {
            get { return _typeCode; }
            set
            {
                VerifyWritable();
                _typeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Coverage Value. Null value is 'String.Empty'.
        /// </summary>
        public string Value
        {
            get { return _value; }
            set
            {
                VerifyWritable();
                _value = value;
            }
        }

        /// <summary>
        /// Gets the instance of a CoverageName object related to this entity.
        /// </summary>
        public CoverageName Name
        {
            get
            {
                _coverageNameHolder.Key = _nameID;
                return (CoverageName)_coverageNameHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a CoverageType object related to this entity.
        /// </summary>
        public CoverageType Type
        {
            get
            {
                _coverageTypeHolder.Key = _typeCode;
                return (CoverageType)_coverageTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Insured object related to this entity.
        /// </summary>
        public Insured Insured
        {
            get
            {
                _insuredHolder.Key = _insuredID;
                return (Insured)_insuredHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Policy object related to this entity.
        /// </summary>
        public Policy Policy
        {
            get
            {
                _policyHolder.Key = _policyID;
                return (Policy)_policyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_insuredID": return _insuredID;
                    case "_policyID": return _policyID;
                    case "_nameID": return _nameID;
                    case "_typeCode": return _typeCode;
                    case "_value": return _value;
                    case "_coverageNameHolder": return _coverageNameHolder;
                    case "_coverageTypeHolder": return _coverageTypeHolder;
                    case "_insuredHolder": return _insuredHolder;
                    case "_policyHolder": return _policyHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_insuredID": _insuredID = (Guid)value; break;
                    case "_policyID": _policyID = (Guid)value; break;
                    case "_nameID": _nameID = (Guid)value; break;
                    case "_typeCode": _typeCode = (string)value; break;
                    case "_value": _value = (string)value; break;
                    case "_coverageNameHolder": _coverageNameHolder = (ObjectHolder)value; break;
                    case "_coverageTypeHolder": _coverageTypeHolder = (ObjectHolder)value; break;
                    case "_insuredHolder": _insuredHolder = (ObjectHolder)value; break;
                    case "_policyHolder": _policyHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static Coverage Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(Coverage), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch Coverage entity with ID = '{0}'.", id));
            }
            return (Coverage)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Coverage GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Coverage), opathExpression);
            return (Coverage)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Coverage GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Coverage))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Coverage)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Coverage[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Coverage), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Coverage[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Coverage))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Coverage[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Coverage[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Coverage), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public Coverage GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            Coverage clone = (Coverage)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.InsuredID, _insuredID);
            Validator.Validate(Field.PolicyID, _policyID);
            Validator.Validate(Field.NameID, _nameID);
            Validator.Validate(Field.TypeCode, _typeCode);
            //INC0295011 Doug Bruce 2015-08-20 - The following try/catch was added because validation was throwing a user-visible error when the field exceeded the max length, when
            // it is must more transparent to just truncate _value and move on.  Originally the truncation was in Validator.cs, but scope prevents _value changes from being retained.
            try
            {
                Validator.Validate(Field.Value, _value);
            }
            catch (ValidationException ex)
            {
                if (ex.Message.Contains("cannot exceed"))
                {
                    UIFieldInfo field = UIMapper.Instance.GetFieldInfo(Field.Value);
                    _value = _value.Substring(0,field.MaxLength);
                }
            }
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _insuredID = Guid.Empty;
            _policyID = Guid.Empty;
            _nameID = Guid.Empty;
            _typeCode = null;
            _value = null;
            _coverageNameHolder = null;
            _coverageTypeHolder = null;
            _insuredHolder = null;
            _policyHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the Coverage entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the InsuredID field.
            /// </summary>
            InsuredID,
            /// <summary>
            /// Represents the PolicyID field.
            /// </summary>
            PolicyID,
            /// <summary>
            /// Represents the NameID field.
            /// </summary>
            NameID,
            /// <summary>
            /// Represents the TypeCode field.
            /// </summary>
            TypeCode,
            /// <summary>
            /// Represents the Value field.
            /// </summary>
            Value,
        }

        #endregion
    }
}