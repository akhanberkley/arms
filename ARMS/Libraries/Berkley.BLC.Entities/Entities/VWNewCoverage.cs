using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a VWNewCoverage entity, which maps to table 'VW_NewCoverage' in the database.
    /// </summary>
    public class VWNewCoverage : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private VWNewCoverage()
        {
        }


        #region --- Generated Members ---

        private Guid _id = Guid.Empty;
        private Guid _surveyID;
        private Guid _locationID;
        private Guid _policyID;
        private Guid _coverageNameID;
        private string _coverageNameTypeCode;
        private string _coverageNameTypeName;
        private string _reportTypeCode = String.Empty;
        private Guid _coverageID;
        private string _coverageTypeName;
        private string _coverageValue = String.Empty;
        private string _dotNetDataType;
        private int _coverageNameDisplayOrder;
        private int _coverageValueDisplayOrder;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets the Survey ID.
        /// </summary>
        public Guid SurveyID
        {
            get { return _surveyID; }
        }

        /// <summary>
        /// Gets the Location ID.
        /// </summary>
        public Guid LocationID
        {
            get { return _locationID; }
        }

        /// <summary>
        /// Gets the Policy ID.
        /// </summary>
        public Guid PolicyID
        {
            get { return _policyID; }
        }

        /// <summary>
        /// Gets the Coverage Name ID.
        /// </summary>
        public Guid CoverageNameID
        {
            get { return _coverageNameID; }
        }

        /// <summary>
        /// Gets the Coverage Name Type Code.
        /// </summary>
        public string CoverageNameTypeCode
        {
            get { return _coverageNameTypeCode; }
        }

        /// <summary>
        /// Gets the Coverage Name Type Name.
        /// </summary>
        public string CoverageNameTypeName
        {
            get { return _coverageNameTypeName; }
        }

        /// <summary>
        /// Gets the Report Type Code. Null value is 'String.Empty'.
        /// </summary>
        public string ReportTypeCode
        {
            get { return _reportTypeCode; }
        }

        /// <summary>
        /// Gets the Coverage ID.
        /// </summary>
        public Guid CoverageID
        {
            get { return _coverageID; }
        }

        /// <summary>
        /// Gets the Coverage Type Name.
        /// </summary>
        public string CoverageTypeName
        {
            get { return _coverageTypeName; }
        }

        /// <summary>
        /// Gets the Coverage Value. Null value is 'String.Empty'.
        /// </summary>
        public string CoverageValue
        {
            get { return _coverageValue; }
        }

        /// <summary>
        /// Gets the Dot Net Data Type.
        /// </summary>
        public string DotNetDataType
        {
            get { return _dotNetDataType; }
        }

        /// <summary>
        /// Gets the Coverage Name Display Order.
        /// </summary>
        public int CoverageNameDisplayOrder
        {
            get { return _coverageNameDisplayOrder; }
        }

        /// <summary>
        /// Gets the Coverage Value Display Order.
        /// </summary>
        public int CoverageValueDisplayOrder
        {
            get { return _coverageValueDisplayOrder; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_surveyID": return _surveyID;
                    case "_locationID": return _locationID;
                    case "_policyID": return _policyID;
                    case "_coverageNameID": return _coverageNameID;
                    case "_coverageNameTypeCode": return _coverageNameTypeCode;
                    case "_coverageNameTypeName": return _coverageNameTypeName;
                    case "_reportTypeCode": return _reportTypeCode;
                    case "_coverageID": return _coverageID;
                    case "_coverageTypeName": return _coverageTypeName;
                    case "_coverageValue": return _coverageValue;
                    case "_dotNetDataType": return _dotNetDataType;
                    case "_coverageNameDisplayOrder": return _coverageNameDisplayOrder;
                    case "_coverageValueDisplayOrder": return _coverageValueDisplayOrder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_surveyID": _surveyID = (Guid)value; break;
                    case "_locationID": _locationID = (Guid)value; break;
                    case "_policyID": _policyID = (Guid)value; break;
                    case "_coverageNameID": _coverageNameID = (Guid)value; break;
                    case "_coverageNameTypeCode": _coverageNameTypeCode = (string)value; break;
                    case "_coverageNameTypeName": _coverageNameTypeName = (string)value; break;
                    case "_reportTypeCode": _reportTypeCode = (string)value; break;
                    case "_coverageID": _coverageID = (Guid)value; break;
                    case "_coverageTypeName": _coverageTypeName = (string)value; break;
                    case "_coverageValue": _coverageValue = (string)value; break;
                    case "_dotNetDataType": _dotNetDataType = (string)value; break;
                    case "_coverageNameDisplayOrder": _coverageNameDisplayOrder = (int)value; break;
                    case "_coverageValueDisplayOrder": _coverageValueDisplayOrder = (int)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static VWNewCoverage Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(VWNewCoverage), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch VWNewCoverage entity with ID = '{0}'.", id));
            }
            return (VWNewCoverage)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static VWNewCoverage GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWNewCoverage), opathExpression);
            return (VWNewCoverage)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static VWNewCoverage GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(VWNewCoverage))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (VWNewCoverage)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWNewCoverage[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWNewCoverage), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWNewCoverage[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(VWNewCoverage))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (VWNewCoverage[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWNewCoverage[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWNewCoverage), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _surveyID = Guid.Empty;
            _locationID = Guid.Empty;
            _policyID = Guid.Empty;
            _coverageNameID = Guid.Empty;
            _coverageNameTypeCode = null;
            _coverageNameTypeName = null;
            _reportTypeCode = null;
            _coverageID = Guid.Empty;
            _coverageTypeName = null;
            _coverageValue = null;
            _dotNetDataType = null;
            _coverageNameDisplayOrder = Int32.MinValue;
            _coverageValueDisplayOrder = Int32.MinValue;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the VWNewCoverage entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the SurveyID field.
            /// </summary>
            SurveyID,
            /// <summary>
            /// Represents the LocationID field.
            /// </summary>
            LocationID,
            /// <summary>
            /// Represents the PolicyID field.
            /// </summary>
            PolicyID,
            /// <summary>
            /// Represents the CoverageNameID field.
            /// </summary>
            CoverageNameID,
            /// <summary>
            /// Represents the CoverageNameTypeCode field.
            /// </summary>
            CoverageNameTypeCode,
            /// <summary>
            /// Represents the CoverageNameTypeName field.
            /// </summary>
            CoverageNameTypeName,
            /// <summary>
            /// Represents the ReportTypeCode field.
            /// </summary>
            ReportTypeCode,
            /// <summary>
            /// Represents the CoverageID field.
            /// </summary>
            CoverageID,
            /// <summary>
            /// Represents the CoverageTypeName field.
            /// </summary>
            CoverageTypeName,
            /// <summary>
            /// Represents the CoverageValue field.
            /// </summary>
            CoverageValue,
            /// <summary>
            /// Represents the DotNetDataType field.
            /// </summary>
            DotNetDataType,
            /// <summary>
            /// Represents the CoverageNameDisplayOrder field.
            /// </summary>
            CoverageNameDisplayOrder,
            /// <summary>
            /// Represents the CoverageValueDisplayOrder field.
            /// </summary>
            CoverageValueDisplayOrder,
        }

        #endregion
    }
}