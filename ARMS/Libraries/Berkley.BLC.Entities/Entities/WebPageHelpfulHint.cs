using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a WebPageHelpfulHint entity, which maps to table 'WebPageHelpfulHint' in the database.
    /// </summary>
    public class WebPageHelpfulHint : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private WebPageHelpfulHint()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="webPagePath">WebPagePath of this WebPageHelpfulHint.</param>
        /// <param name="companyID">CompanyID of this WebPageHelpfulHint.</param>
        public WebPageHelpfulHint(string webPagePath, Guid companyID)
        {
            _webPagePath = webPagePath;
            _companyID = companyID;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private string _webPagePath;
        private Guid _companyID;
        private string _webPageDescription;
        private string _helpfulHintText = String.Empty;
        private bool _alwaysVisible;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _webPageHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Web Page Path.
        /// </summary>
        public string WebPagePath
        {
            get { return _webPagePath; }
        }

        /// <summary>
        /// Gets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets or sets the Web Page Description.
        /// </summary>
        public string WebPageDescription
        {
            get { return _webPageDescription; }
            set
            {
                VerifyWritable();
                _webPageDescription = value;
            }
        }

        /// <summary>
        /// Gets or sets the Helpful Hint Text. Null value is 'String.Empty'.
        /// </summary>
        public string HelpfulHintText
        {
            get { return _helpfulHintText; }
            set
            {
                VerifyWritable();
                _helpfulHintText = value;
            }
        }

        /// <summary>
        /// Gets or sets the Always Visible.
        /// </summary>
        public bool AlwaysVisible
        {
            get { return _alwaysVisible; }
            set
            {
                VerifyWritable();
                _alwaysVisible = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a WebPage object related to this entity.
        /// </summary>
        public WebPage WebPage
        {
            get
            {
                _webPageHolder.Key = _webPagePath;
                return (WebPage)_webPageHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_webPagePath": return _webPagePath;
                    case "_companyID": return _companyID;
                    case "_webPageDescription": return _webPageDescription;
                    case "_helpfulHintText": return _helpfulHintText;
                    case "_alwaysVisible": return _alwaysVisible;
                    case "_companyHolder": return _companyHolder;
                    case "_webPageHolder": return _webPageHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_webPagePath": _webPagePath = (string)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_webPageDescription": _webPageDescription = (string)value; break;
                    case "_helpfulHintText": _helpfulHintText = (string)value; break;
                    case "_alwaysVisible": _alwaysVisible = (bool)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_webPageHolder": _webPageHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="webPagePath">WebPagePath of the entity to fetch.</param>
        /// <param name="companyID">CompanyID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static WebPageHelpfulHint Get(string webPagePath, Guid companyID)
        {
            OPathQuery query = new OPathQuery(typeof(WebPageHelpfulHint), "WebPagePath = ? && CompanyID = ?");
            object entity = DB.Engine.GetObject(query, webPagePath, companyID);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch WebPageHelpfulHint entity with WebPagePath = '{0}' and CompanyID = '{1}'.", webPagePath, companyID));
            }
            return (WebPageHelpfulHint)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static WebPageHelpfulHint GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(WebPageHelpfulHint), opathExpression);
            return (WebPageHelpfulHint)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static WebPageHelpfulHint GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(WebPageHelpfulHint))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (WebPageHelpfulHint)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static WebPageHelpfulHint[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(WebPageHelpfulHint), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static WebPageHelpfulHint[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(WebPageHelpfulHint))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (WebPageHelpfulHint[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static WebPageHelpfulHint[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(WebPageHelpfulHint), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public WebPageHelpfulHint GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            WebPageHelpfulHint clone = (WebPageHelpfulHint)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.WebPagePath, _webPagePath);
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.WebPageDescription, _webPageDescription);
            Validator.Validate(Field.HelpfulHintText, _helpfulHintText);
            Validator.Validate(Field.AlwaysVisible, _alwaysVisible);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _webPagePath = null;
            _companyID = Guid.Empty;
            _webPageDescription = null;
            _helpfulHintText = null;
            _alwaysVisible = false;
            _companyHolder = null;
            _webPageHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the WebPageHelpfulHint entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the WebPagePath field.
            /// </summary>
            WebPagePath,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the WebPageDescription field.
            /// </summary>
            WebPageDescription,
            /// <summary>
            /// Represents the HelpfulHintText field.
            /// </summary>
            HelpfulHintText,
            /// <summary>
            /// Represents the AlwaysVisible field.
            /// </summary>
            AlwaysVisible,
        }

        #endregion
    }
}