using System;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a MergeDocument entity, which maps to table 'MergeDocument' in the database.
    /// </summary>
    public class MergeDocument : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private MergeDocument()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this MergeDocument.</param>
        public MergeDocument(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _companyID;
        private string _name;
        private bool _disabled;
        private bool _isLetter;
        private int _daysUntilReminder = Int32.MinValue;
        private string _reminderDateTypeCode = String.Empty;
        private int _priorityIndex;
        private int _type = Int32.MinValue;
        private bool _isCritical = false;
        private ObjectHolder _companyHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Merge Document ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
            set
            {
                VerifyWritable();
                _companyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Merge Document Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                VerifyWritable();
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Disabled.
        /// </summary>
        public bool Disabled
        {
            get { return _disabled; }
            set
            {
                VerifyWritable();
                _disabled = value;
            }
        }

        /// <summary>
        /// Gets or sets the Is Letter.
        /// </summary>
        public bool IsLetter
        {
            get { return _isLetter; }
            set
            {
                VerifyWritable();
                _isLetter = value;
            }
        }

        /// <summary>
        /// Gets or sets the Days Until Reminder. Null value is 'Int32.MinValue'.
        /// </summary>
        public int DaysUntilReminder
        {
            get { return _daysUntilReminder; }
            set
            {
                VerifyWritable();
                _daysUntilReminder = value;
            }
        }

        /// <summary>
        /// Gets or sets the Reminder Date Type Code. Null value is 'String.Empty'.
        /// </summary>
        public string ReminderDateTypeCode
        {
            get { return _reminderDateTypeCode; }
            set
            {
                VerifyWritable();
                _reminderDateTypeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Priority Index.
        /// </summary>
        public int PriorityIndex
        {
            get { return _priorityIndex; }
            set
            {
                VerifyWritable();
                _priorityIndex = value;
            }
        }

        /// <summary>
        /// Gets or sets the Merge Document Type. Null value is 'Int32.MinValue'.
        /// </summary>
        public int Type
        {
            get { return _type; }
            set
            {
                VerifyWritable();
                _type = value;
            }
        }

        /// <summary>
        /// Gets or sets the Is Critical. Null value is 'false'.
        /// </summary>
        public bool IsCritical
        {
            get { return _isCritical; }
            set
            {
                VerifyWritable();
                _isCritical = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_companyID": return _companyID;
                    case "_name": return _name;
                    case "_disabled": return _disabled;
                    case "_isLetter": return _isLetter;
                    case "_isDocument": return _isLetter;
                    case "_type": return _type;
                    case "_daysUntilReminder": return _daysUntilReminder;
                    case "_reminderDateTypeCode": return _reminderDateTypeCode;
                    case "_priorityIndex": return _priorityIndex;
                    case "_isCritical": return _isCritical;
                    case "_companyHolder": return _companyHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_name": _name = (string)value; break;
                    case "_disabled": _disabled = (bool)value; break;
                    case "_isLetter": _isLetter = (bool)value; break;
                    case "_type": _type = (int)value; break;
                    case "_daysUntilReminder": _daysUntilReminder = (int)value; break;
                    case "_reminderDateTypeCode": _reminderDateTypeCode = (string)value; break;
                    case "_priorityIndex": _priorityIndex = (int)value; break;
                    case "_isCritical": _isCritical = (bool)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static MergeDocument Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(MergeDocument), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch MergeDocument entity with ID = '{0}'.", id));
            }
            return (MergeDocument)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static MergeDocument GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(MergeDocument), opathExpression);
            return (MergeDocument)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static MergeDocument GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(MergeDocument))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (MergeDocument)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static MergeDocument[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(MergeDocument), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static MergeDocument[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(MergeDocument))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (MergeDocument[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static MergeDocument[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(MergeDocument), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public MergeDocument GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            MergeDocument clone = (MergeDocument)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.Name, _name);
            Validator.Validate(Field.Disabled, _disabled);
            Validator.Validate(Field.IsLetter, _isLetter);
            Validator.Validate(Field.DaysUntilReminder, _daysUntilReminder);
            Validator.Validate(Field.ReminderDateTypeCode, _reminderDateTypeCode);
            Validator.Validate(Field.PriorityIndex, _priorityIndex);
            Validator.Validate(Field.Type, _type);
            Validator.Validate(Field.IsCritical, _isCritical);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _companyID = Guid.Empty;
            _name = null;
            _disabled = false;
            _isLetter = false;
            _daysUntilReminder = Int32.MinValue;
            _reminderDateTypeCode = null;
            _priorityIndex = Int32.MinValue;
            _type = Int32.MinValue;
            _isCritical = false;
            _companyHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the MergeDocument entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the Disabled field.
            /// </summary>
            Disabled,
            /// <summary>
            /// Represents the IsLetter field.
            /// </summary>
            IsLetter,
            /// <summary>
            /// Represents the DaysUntilReminder field.
            /// </summary>
            DaysUntilReminder,
            /// <summary>
            /// Represents the ReminderDateTypeCode field.
            /// </summary>
            ReminderDateTypeCode,
            /// <summary>
            /// Represents the PriorityIndex field.
            /// </summary>
            PriorityIndex,
            /// <summary>
            /// Represents the Type field.
            /// </summary>
            Type,
            /// <summary>
            /// Represents the IsCritical field.
            /// </summary>
            IsCritical,
        }

        #endregion
    }
}