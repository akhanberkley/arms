using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents an UserTerritory entity, which maps to table 'User_Territory' in the database.
	/// </summary>
	public class UserTerritory : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private UserTerritory()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="userID">UserID of this UserTerritory.</param>
		/// <param name="territoryID">TerritoryID of this UserTerritory.</param>
		public UserTerritory(Guid userID, Guid territoryID)
		{
			_userID = userID;
			_territoryID = territoryID;

			_isReadOnly = false;

            //NOTE: had to turn off tracking as it causes delete/insert problems on EditUser page
            //DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


        #region --- Generated Members ---

        private Guid _userID;
        private Guid _territoryID;
        private int _percentAllocated = Int32.MinValue;
        private decimal _minPremiumAmount = Decimal.MinValue;
        private decimal _maxPremiumAmount = Decimal.MinValue;
        private ObjectHolder _territoryHolder = null;
        private ObjectHolder _userHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the User.
        /// </summary>
        public Guid UserID
        {
            get { return _userID; }
        }

        /// <summary>
        /// Gets the Territory.
        /// </summary>
        public Guid TerritoryID
        {
            get { return _territoryID; }
        }

        /// <summary>
        /// Gets or sets the Percent Allocated. Null value is 'Int32.MinValue'.
        /// </summary>
        public int PercentAllocated
        {
            get { return _percentAllocated; }
            set
            {
                VerifyWritable();
                _percentAllocated = value;
            }
        }

        /// <summary>
        /// Gets or sets the Min Premium Amount. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal MinPremiumAmount
        {
            get { return _minPremiumAmount; }
            set
            {
                VerifyWritable();
                _minPremiumAmount = value;
            }
        }

        /// <summary>
        /// Gets or sets the Max Premium Amount. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal MaxPremiumAmount
        {
            get { return _maxPremiumAmount; }
            set
            {
                VerifyWritable();
                _maxPremiumAmount = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Territory object related to this entity.
        /// </summary>
        public Territory Territory
        {
            get
            {
                _territoryHolder.Key = _territoryID;
                return (Territory)_territoryHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User User
        {
            get
            {
                _userHolder.Key = _userID;
                return (User)_userHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_userID": return _userID;
                    case "_territoryID": return _territoryID;
                    case "_percentAllocated": return _percentAllocated;
                    case "_minPremiumAmount": return _minPremiumAmount;
                    case "_maxPremiumAmount": return _maxPremiumAmount;
                    case "_territoryHolder": return _territoryHolder;
                    case "_userHolder": return _userHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_userID": _userID = (Guid)value; break;
                    case "_territoryID": _territoryID = (Guid)value; break;
                    case "_percentAllocated": _percentAllocated = (int)value; break;
                    case "_minPremiumAmount": _minPremiumAmount = (decimal)value; break;
                    case "_maxPremiumAmount": _maxPremiumAmount = (decimal)value; break;
                    case "_territoryHolder": _territoryHolder = (ObjectHolder)value; break;
                    case "_userHolder": _userHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="userID">UserID of the entity to fetch.</param>
        /// <param name="territoryID">TerritoryID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static UserTerritory Get(Guid userID, Guid territoryID)
        {
            OPathQuery query = new OPathQuery(typeof(UserTerritory), "UserID = ? && TerritoryID = ?");
            object entity = DB.Engine.GetObject(query, userID, territoryID);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch UserTerritory entity with UserID = '{0}' and TerritoryID = '{1}'.", userID, territoryID));
            }
            return (UserTerritory)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static UserTerritory GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(UserTerritory), opathExpression);
            return (UserTerritory)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static UserTerritory GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(UserTerritory))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (UserTerritory)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static UserTerritory[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(UserTerritory), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static UserTerritory[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(UserTerritory))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (UserTerritory[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static UserTerritory[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(UserTerritory), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public UserTerritory GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            UserTerritory clone = (UserTerritory)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.UserID, _userID);
            Validator.Validate(Field.TerritoryID, _territoryID);
            Validator.Validate(Field.PercentAllocated, _percentAllocated);
            Validator.Validate(Field.MinPremiumAmount, _minPremiumAmount);
            Validator.Validate(Field.MaxPremiumAmount, _maxPremiumAmount);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _userID = Guid.Empty;
            _territoryID = Guid.Empty;
            _percentAllocated = Int32.MinValue;
            _minPremiumAmount = Decimal.MinValue;
            _maxPremiumAmount = Decimal.MinValue;
            _territoryHolder = null;
            _userHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the UserTerritory entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the UserID field.
            /// </summary>
            UserID,
            /// <summary>
            /// Represents the TerritoryID field.
            /// </summary>
            TerritoryID,
            /// <summary>
            /// Represents the PercentAllocated field.
            /// </summary>
            PercentAllocated,
            /// <summary>
            /// Represents the MinPremiumAmount field.
            /// </summary>
            MinPremiumAmount,
            /// <summary>
            /// Represents the MaxPremiumAmount field.
            /// </summary>
            MaxPremiumAmount,
        }

        #endregion
    }
}