using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a Policy entity, which maps to table 'Policy' in the database.
    /// </summary>
    public class Policy : IObjectHelper, Berkley.BLC.Core.IPolicy
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private Policy()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this Policy.</param>
        public Policy(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Gets the address as a formatted currency amount for a textbox.
        /// </summary>
        public decimal TextBoxPremium
        {
            get
            {
                return (this.Premium != decimal.MinValue) ? Math.Round(this.Premium, 0) : decimal.MinValue;
            }
        }

        /// <summary>
        /// Gets or sets the policy system key to tie back to the correct location.
        /// </summary>
        private string _policySystemKey = string.Empty;
        public string PolicySystemKey
        {
            get
            {
                return _policySystemKey;
            }
            set
            {
                _policySystemKey = value;
            }
        }

        /// <summary>
        /// Selective "assignment operator".  Copied data fields from the Right Hand Side instance to "this" instance
        /// </summary>
        /// <param name="oRhs">Instance with new data</param>
        /// <returns>The modified class</returns>
        public Policy Assign(Policy oRhs)
        {
			Policy oPolicy = this;

			if (oPolicy.IsReadOnly)
			{
				oPolicy = oPolicy.GetWritableInstance();
			}

			oPolicy.Number = oRhs._number;
			oPolicy.Mod = oRhs._mod;
			oPolicy.Symbol = oRhs._symbol;
			oPolicy.EffectiveDate = oRhs._effectiveDate;
			oPolicy.ExpireDate = oRhs._expireDate;
			oPolicy.LineOfBusinessCode = oRhs._lineOfBusinessCode;
			oPolicy.Premium = oRhs._premium;
			oPolicy.Carrier = oRhs._carrier;
			if (oRhs._hazardGrade != null && oRhs._hazardGrade.Length > 0 && oRhs._hazardGrade != "0")
			{
				oPolicy.HazardGrade = oRhs._hazardGrade;
			}
			oPolicy.BranchCode = oRhs._branchCode;
			oPolicy.ProfitCenter = oRhs._profitCenter;
            oPolicy.StateCode = oRhs.StateCode;
			oPolicy.IsActive = oRhs._isActive;
			return oPolicy;
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }

        #region --- Generated Members ---

        private Guid _id;
        private Guid _insuredID;
        private string _number = String.Empty;
        private int _mod = Int32.MinValue;
        private string _symbol = String.Empty;
        private DateTime _effectiveDate = DateTime.MinValue;
        private DateTime _expireDate = DateTime.MinValue;
        private string _lineOfBusinessCode = String.Empty;
        private decimal _premium = Decimal.MinValue;
        private string _carrier = String.Empty;
        private string _hazardGrade = String.Empty;
        private string _branchCode = String.Empty;
        private string _profitCenter = String.Empty;
        private string _ratingCompany = String.Empty;
        private string _stateCode = String.Empty;
        private bool _isActive;
        private ObjectHolder _insuredHolder = null;
        private ObjectHolder _lineOfBusinessHolder = null;
        private ObjectHolder _policyStateHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Policy ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Insured.
        /// </summary>
        public Guid InsuredID
        {
            get { return _insuredID; }
            set
            {
                VerifyWritable();
                _insuredID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Number. Null value is 'String.Empty'.
        /// </summary>
        public string Number
        {
            get { return _number; }
            set
            {
                VerifyWritable();
                _number = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Mod. Null value is 'Int32.MinValue'.
        /// </summary>
        public int Mod
        {
            get { return _mod; }
            set
            {
                VerifyWritable();
                _mod = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Symbol. Null value is 'String.Empty'.
        /// </summary>
        public string Symbol
        {
            get { return _symbol; }
            set
            {
                VerifyWritable();
                _symbol = value;
            }
        }

        /// <summary>
        /// Gets or sets the Effective Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime EffectiveDate
        {
            get { return _effectiveDate; }
            set
            {
                VerifyWritable();
                _effectiveDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Expire Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime ExpireDate
        {
            get { return _expireDate; }
            set
            {
                VerifyWritable();
                _expireDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Line of Business. Null value is 'String.Empty'.
        /// </summary>
        public string LineOfBusinessCode
        {
            get { return _lineOfBusinessCode; }
            set
            {
                VerifyWritable();
                _lineOfBusinessCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Premium. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal Premium
        {
            get { return _premium; }
            set
            {
                VerifyWritable();
                _premium = value;
            }
        }

        /// <summary>
        /// Gets or sets the Carrier. Null value is 'String.Empty'.
        /// </summary>
        public string Carrier
        {
            get { return _carrier; }
            set
            {
                VerifyWritable();
                _carrier = value;
            }
        }

        /// <summary>
        /// Gets or sets the Hazard Grade. Null value is 'String.Empty'.
        /// </summary>
        public string HazardGrade
        {
            get { return _hazardGrade; }
            set
            {
                VerifyWritable();
                _hazardGrade = value;
            }
        }

        /// <summary>
        /// Gets or sets the Branch Code. Null value is 'String.Empty'.
        /// </summary>
        public string BranchCode
        {
            get { return _branchCode; }
            set
            {
                VerifyWritable();
                _branchCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Profit Center. Null value is 'String.Empty'.
        /// </summary>
        public string ProfitCenter
        {
            get { return _profitCenter; }
            set
            {
                VerifyWritable();
                _profitCenter = value;
            }
        }

        /// <summary>
        /// Gets or sets the Rating Company. Null value is 'String.Empty'.
        /// </summary>
        public string RatingCompany
        {
            get { return _ratingCompany; }
            set
            {
                VerifyWritable();
                _ratingCompany = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy State. Null value is 'String.Empty'.
        /// </summary>
        public string StateCode
        {
            get { return _stateCode; }
            set
            {
                VerifyWritable();
                _stateCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Is Active.
        /// </summary>
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                VerifyWritable();
                _isActive = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Insured object related to this entity.
        /// </summary>
        public Insured Insured
        {
            get
            {
                _insuredHolder.Key = _insuredID;
                return (Insured)_insuredHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a LineOfBusiness object related to this entity.
        /// </summary>
        public LineOfBusiness LineOfBusiness
        {
            get
            {
                _lineOfBusinessHolder.Key = _lineOfBusinessCode;
                return (LineOfBusiness)_lineOfBusinessHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a State object related to this entity.
        /// </summary>
        public State State
        {
            get
            {
                _policyStateHolder.Key = _stateCode;
                return (State)_policyStateHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_insuredID": return _insuredID;
                    case "_number": return _number;
                    case "_mod": return _mod;
                    case "_symbol": return _symbol;
                    case "_effectiveDate": return _effectiveDate;
                    case "_expireDate": return _expireDate;
                    case "_lineOfBusinessCode": return _lineOfBusinessCode;
                    case "_premium": return _premium;
                    case "_carrier": return _carrier;
                    case "_hazardGrade": return _hazardGrade;
                    case "_branchCode": return _branchCode;
                    case "_profitCenter": return _profitCenter;
                    case "_ratingCompany": return _ratingCompany;
                    case "_stateCode": return _stateCode;
                    case "_isActive": return _isActive;
                    case "_insuredHolder": return _insuredHolder;
                    case "_lineOfBusinessHolder": return _lineOfBusinessHolder;
                    case "_policyStateHolder": return _policyStateHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_insuredID": _insuredID = (Guid)value; break;
                    case "_number": _number = (string)value; break;
                    case "_mod": _mod = (int)value; break;
                    case "_symbol": _symbol = (string)value; break;
                    case "_effectiveDate": _effectiveDate = (DateTime)value; break;
                    case "_expireDate": _expireDate = (DateTime)value; break;
                    case "_lineOfBusinessCode": _lineOfBusinessCode = (string)value; break;
                    case "_premium": _premium = (decimal)value; break;
                    case "_carrier": _carrier = (string)value; break;
                    case "_hazardGrade": _hazardGrade = (string)value; break;
                    case "_branchCode": _branchCode = (string)value; break;
                    case "_profitCenter": _profitCenter = (string)value; break;
                    case "_ratingCompany": _ratingCompany = (string)value; break;
                    case "_stateCode": _stateCode = (string)value; break;
                    case "_isActive": _isActive = (bool)value; break;
                    case "_insuredHolder": _insuredHolder = (ObjectHolder)value; break;
                    case "_lineOfBusinessHolder": _lineOfBusinessHolder = (ObjectHolder)value; break;
                    case "_policyStateHolder": _policyStateHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static Policy Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(Policy), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch Policy entity with ID = '{0}'.", id));
            }
            return (Policy)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Policy GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Policy), opathExpression);
            return (Policy)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Policy GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Policy))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Policy)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Policy[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Policy), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Policy[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Policy))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Policy[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Policy[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Policy), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public Policy GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            Policy clone = (Policy)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.InsuredID, _insuredID);
            Validator.Validate(Field.Number, _number);
            Validator.Validate(Field.Mod, _mod);
            Validator.Validate(Field.Symbol, _symbol);
            Validator.Validate(Field.EffectiveDate, _effectiveDate);
            Validator.Validate(Field.ExpireDate, _expireDate);
            Validator.Validate(Field.LineOfBusinessCode, _lineOfBusinessCode);
            Validator.Validate(Field.Premium, _premium);
            Validator.Validate(Field.Carrier, _carrier);
            Validator.Validate(Field.HazardGrade, _hazardGrade);
            Validator.Validate(Field.BranchCode, _branchCode);
            Validator.Validate(Field.ProfitCenter, _profitCenter);
            Validator.Validate(Field.RatingCompany, _ratingCompany);
            Validator.Validate(Field.StateCode, _stateCode);
            Validator.Validate(Field.IsActive, _isActive);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _insuredID = Guid.Empty;
            _number = null;
            _mod = Int32.MinValue;
            _symbol = null;
            _effectiveDate = DateTime.MinValue;
            _expireDate = DateTime.MinValue;
            _lineOfBusinessCode = null;
            _premium = Decimal.MinValue;
            _carrier = null;
            _hazardGrade = null;
            _branchCode = null;
            _profitCenter = null;
            _ratingCompany = null;
            _stateCode = null;
            _isActive = false;
            _insuredHolder = null;
            _lineOfBusinessHolder = null;
            _policyStateHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the Policy entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the InsuredID field.
            /// </summary>
            InsuredID,
            /// <summary>
            /// Represents the Number field.
            /// </summary>
            Number,
            /// <summary>
            /// Represents the Mod field.
            /// </summary>
            Mod,
            /// <summary>
            /// Represents the Symbol field.
            /// </summary>
            Symbol,
            /// <summary>
            /// Represents the EffectiveDate field.
            /// </summary>
            EffectiveDate,
            /// <summary>
            /// Represents the ExpireDate field.
            /// </summary>
            ExpireDate,
            /// <summary>
            /// Represents the LineOfBusinessCode field.
            /// </summary>
            LineOfBusinessCode,
            /// <summary>
            /// Represents the Premium field.
            /// </summary>
            Premium,
            /// <summary>
            /// Represents the Carrier field.
            /// </summary>
            Carrier,
            /// <summary>
            /// Represents the HazardGrade field.
            /// </summary>
            HazardGrade,
            /// <summary>
            /// Represents the BranchCode field.
            /// </summary>
            BranchCode,
            /// <summary>
            /// Represents the ProfitCenter field.
            /// </summary>
            ProfitCenter,
            /// <summary>
            /// Represents the RatingCompany field.
            /// </summary>
            RatingCompany,
            /// <summary>
            /// Represents the StateCode field.
            /// </summary>
            StateCode,
            /// <summary>
            /// Represents the IsActive field.
            /// </summary>
            IsActive,
        }

        #endregion
    }
}