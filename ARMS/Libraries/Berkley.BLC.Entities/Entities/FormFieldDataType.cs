using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a FormFieldDataType entity, which maps to table 'FormFieldDataType' in the database.
	/// </summary>
	public class FormFieldDataType : IObjectHelper
	{
        #region Public Fields

        /// <summary>
        /// Represents a date type form field.
        /// </summary>
        public static readonly FormFieldDataType Date = FormFieldDataType.Get("D");
        /// <summary>
        /// Represents a money type form field.
        /// </summary>
        public static readonly FormFieldDataType Money = FormFieldDataType.Get("M");
        /// <summary>
        /// Represents a number type form field.
        /// </summary>
        public static readonly FormFieldDataType Number = FormFieldDataType.Get("N");
        /// <summary>
        /// Represents a text type form field.
        /// </summary>
        public static readonly FormFieldDataType Text = FormFieldDataType.Get("T");
        /// <summary>
        /// Represents a yes/no type form field.
        /// </summary>
        public static readonly FormFieldDataType YesNo = FormFieldDataType.Get("Y");

        #endregion
        
        /// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private FormFieldDataType()
		{
		}


        #region --- Generated Members ---

        private string _code;
        private string _displayName;
        private string _dotNetDataType;
        private bool _isBooleanField;
        private int _displayOrder;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Form Field Data Type.
        /// </summary>
        public string Code
        {
            get { return _code; }
        }

        /// <summary>
        /// Gets the Form Field Data Type Display Name.
        /// </summary>
        public string DisplayName
        {
            get { return _displayName; }
        }

        /// <summary>
        /// Gets the Dot Net Data Type.
        /// </summary>
        public string DotNetDataType
        {
            get { return _dotNetDataType; }
        }

        /// <summary>
        /// Gets the Is Boolean Field.
        /// </summary>
        public bool IsBooleanField
        {
            get { return _isBooleanField; }
        }

        /// <summary>
        /// Gets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_code": return _code;
                    case "_displayName": return _displayName;
                    case "_dotNetDataType": return _dotNetDataType;
                    case "_isBooleanField": return _isBooleanField;
                    case "_displayOrder": return _displayOrder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_code": _code = (string)value; break;
                    case "_displayName": _displayName = (string)value; break;
                    case "_dotNetDataType": _dotNetDataType = (string)value; break;
                    case "_isBooleanField": _isBooleanField = (bool)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="code">Code of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static FormFieldDataType Get(string code)
        {
            OPathQuery query = new OPathQuery(typeof(FormFieldDataType), "Code = ?");
            object entity = DB.Engine.GetObject(query, code);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch FormFieldDataType entity with Code = '{0}'.", code));
            }
            return (FormFieldDataType)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static FormFieldDataType GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(FormFieldDataType), opathExpression);
            return (FormFieldDataType)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static FormFieldDataType GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(FormFieldDataType))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (FormFieldDataType)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static FormFieldDataType[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(FormFieldDataType), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static FormFieldDataType[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(FormFieldDataType))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (FormFieldDataType[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static FormFieldDataType[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(FormFieldDataType), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Determines if two FormFieldDataType objects have the same semantic value.
        /// </summary>
        /// <param name="one">The first object to compare.</param>
        /// <param name="two">The second object to compare.</param>
        /// <returns>True if they are semantically identical, false otherwise.</returns>
        public static bool operator ==(FormFieldDataType one, FormFieldDataType two)
        {
            if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
            {
                return true;
            }
            else if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
            {
                return false;
            }
            else
            {
                return (one.CompareTo(two) == 0);
            }
        }

        /// <summary>
        /// Determines if two FormFieldDataType objects have different semantic values.
        /// </summary>
        /// <param name="one">The first object to compare.</param>
        /// <param name="two">The second object to compare.</param>
        /// <returns>True if they are semantically dissimilar, false otherwise.</returns>
        public static bool operator !=(FormFieldDataType one, FormFieldDataType two)
        {
            if (object.ReferenceEquals(one, null) && object.ReferenceEquals(two, null))
            {
                return false;
            }
            if (object.ReferenceEquals(one, null) || object.ReferenceEquals(two, null))
            {
                return true;
            }
            else
            {
                return (one.CompareTo(two) != 0);
            }
        }

        /// <summary>
        /// Compares the current instance with another object of the same type.
        /// </summary>
        /// <param name="value">An object to compare with this instance.</param>
        /// <returns>Less than zero if this instance is less than value. 
        /// Zero if this instance is equal to value.
        /// Greater than zero if this instance is greater than value.</returns>
        public int CompareTo(object value)
        {
            if (value == null)
            {
                return 1;
            }
            if (value is FormFieldDataType)
            {
                FormFieldDataType obj = (FormFieldDataType)value;
                return (this.Code.CompareTo(obj.Code));
            }
            throw new ArgumentException("Value is not the correct type.");
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode()
        {
            return this.Code.GetHashCode();
        }

        /// <summary>
        /// Determines whether this instance is equal to the specified object.
        /// </summary>
        /// <param name="value">The object to compare with the current instance. </param>
        /// <returns>true if the specified object is equal to the current instance; otherwise, false.</returns>
        public override bool Equals(object value)
        {
            return (this == (value as FormFieldDataType));
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _code = null;
            _displayName = null;
            _dotNetDataType = null;
            _isBooleanField = false;
            _displayOrder = Int32.MinValue;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the FormFieldDataType entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the Code field.
            /// </summary>
            Code,
            /// <summary>
            /// Represents the DisplayName field.
            /// </summary>
            DisplayName,
            /// <summary>
            /// Represents the DotNetDataType field.
            /// </summary>
            DotNetDataType,
            /// <summary>
            /// Represents the IsBooleanField field.
            /// </summary>
            IsBooleanField,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
        }

        #endregion
    }
}