using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyRecommendation entity, which maps to table 'Survey_Recommendation' in the database.
	/// </summary>
	public class SurveyRecommendation : IObjectHelper
	{
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private SurveyRecommendation()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this SurveyRecommendation.</param>
        public SurveyRecommendation(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _surveyID;
        private Guid _recommendationID;
        private string _recommendationNumber = String.Empty;
        private string _entryText = String.Empty;
        private string _comments = String.Empty;
        private DateTime _dateCreated = DateTime.MinValue;
        private DateTime _dateCompleted = DateTime.MinValue;
        private Guid _recClassificationID = Guid.Empty;
        private Guid _recStatusID = Guid.Empty;
        private int _priorityIndex;
        private ObjectHolder _recClassificationHolder = null;
        private ObjectHolder _recommendationHolder = null;
        private ObjectHolder _recStatusHolder = null;
        private ObjectHolder _surveyHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Survey Recommendation ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Survey.
        /// </summary>
        public Guid SurveyID
        {
            get { return _surveyID; }
            set
            {
                VerifyWritable();
                _surveyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Recommendation.
        /// </summary>
        public Guid RecommendationID
        {
            get { return _recommendationID; }
            set
            {
                VerifyWritable();
                _recommendationID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Recommendation Number. Null value is 'String.Empty'.
        /// </summary>
        public string RecommendationNumber
        {
            get { return _recommendationNumber; }
            set
            {
                VerifyWritable();
                _recommendationNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Entry Text. Null value is 'String.Empty'.
        /// </summary>
        public string EntryText
        {
            get { return _entryText; }
            set
            {
                VerifyWritable();
                _entryText = value;
            }
        }

        /// <summary>
        /// Gets or sets the Comments. Null value is 'String.Empty'.
        /// </summary>
        public string Comments
        {
            get { return _comments; }
            set
            {
                VerifyWritable();
                _comments = value;
            }
        }

        /// <summary>
        /// Gets or sets the Date Created. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set
            {
                VerifyWritable();
                _dateCreated = value;
            }
        }

        /// <summary>
        /// Gets or sets the Date Completed. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime DateCompleted
        {
            get { return _dateCompleted; }
            set
            {
                VerifyWritable();
                _dateCompleted = value;
            }
        }

        /// <summary>
        /// Gets or sets the Rec Classification. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid RecClassificationID
        {
            get { return _recClassificationID; }
            set
            {
                VerifyWritable();
                _recClassificationID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Rec Status. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid RecStatusID
        {
            get { return _recStatusID; }
            set
            {
                VerifyWritable();
                _recStatusID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Priority Index.
        /// </summary>
        public int PriorityIndex
        {
            get { return _priorityIndex; }
            set
            {
                VerifyWritable();
                _priorityIndex = value;
            }
        }

        /// <summary>
        /// Gets the instance of a RecClassification object related to this entity.
        /// </summary>
        public RecClassification RecClassification
        {
            get
            {
                _recClassificationHolder.Key = _recClassificationID;
                return (RecClassification)_recClassificationHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Recommendation object related to this entity.
        /// </summary>
        public Recommendation Recommendation
        {
            get
            {
                _recommendationHolder.Key = _recommendationID;
                return (Recommendation)_recommendationHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a RecStatus object related to this entity.
        /// </summary>
        public RecStatus RecStatus
        {
            get
            {
                _recStatusHolder.Key = _recStatusID;
                return (RecStatus)_recStatusHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Survey object related to this entity.
        /// </summary>
        public Survey Survey
        {
            get
            {
                _surveyHolder.Key = _surveyID;
                return (Survey)_surveyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_surveyID": return _surveyID;
                    case "_recommendationID": return _recommendationID;
                    case "_recommendationNumber": return _recommendationNumber;
                    case "_entryText": return _entryText;
                    case "_comments": return _comments;
                    case "_dateCreated": return _dateCreated;
                    case "_dateCompleted": return _dateCompleted;
                    case "_recClassificationID": return _recClassificationID;
                    case "_recStatusID": return _recStatusID;
                    case "_priorityIndex": return _priorityIndex;
                    case "_recClassificationHolder": return _recClassificationHolder;
                    case "_recommendationHolder": return _recommendationHolder;
                    case "_recStatusHolder": return _recStatusHolder;
                    case "_surveyHolder": return _surveyHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_surveyID": _surveyID = (Guid)value; break;
                    case "_recommendationID": _recommendationID = (Guid)value; break;
                    case "_recommendationNumber": _recommendationNumber = (string)value; break;
                    case "_entryText": _entryText = (string)value; break;
                    case "_comments": _comments = (string)value; break;
                    case "_dateCreated": _dateCreated = (DateTime)value; break;
                    case "_dateCompleted": _dateCompleted = (DateTime)value; break;
                    case "_recClassificationID": _recClassificationID = (Guid)value; break;
                    case "_recStatusID": _recStatusID = (Guid)value; break;
                    case "_priorityIndex": _priorityIndex = (int)value; break;
                    case "_recClassificationHolder": _recClassificationHolder = (ObjectHolder)value; break;
                    case "_recommendationHolder": _recommendationHolder = (ObjectHolder)value; break;
                    case "_recStatusHolder": _recStatusHolder = (ObjectHolder)value; break;
                    case "_surveyHolder": _surveyHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static SurveyRecommendation Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyRecommendation), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch SurveyRecommendation entity with ID = '{0}'.", id));
            }
            return (SurveyRecommendation)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyRecommendation GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyRecommendation), opathExpression);
            return (SurveyRecommendation)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyRecommendation GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyRecommendation))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyRecommendation)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyRecommendation[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyRecommendation), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyRecommendation[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyRecommendation))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyRecommendation[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyRecommendation[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyRecommendation), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public SurveyRecommendation GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            SurveyRecommendation clone = (SurveyRecommendation)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.SurveyID, _surveyID);
            Validator.Validate(Field.RecommendationID, _recommendationID);
            Validator.Validate(Field.RecommendationNumber, _recommendationNumber);
            Validator.Validate(Field.EntryText, _entryText);
            Validator.Validate(Field.Comments, _comments);
            Validator.Validate(Field.DateCreated, _dateCreated);
            Validator.Validate(Field.DateCompleted, _dateCompleted);
            Validator.Validate(Field.RecClassificationID, _recClassificationID);
            Validator.Validate(Field.RecStatusID, _recStatusID);
            Validator.Validate(Field.PriorityIndex, _priorityIndex);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _surveyID = Guid.Empty;
            _recommendationID = Guid.Empty;
            _recommendationNumber = null;
            _entryText = null;
            _comments = null;
            _dateCreated = DateTime.MinValue;
            _dateCompleted = DateTime.MinValue;
            _recClassificationID = Guid.Empty;
            _recStatusID = Guid.Empty;
            _priorityIndex = Int32.MinValue;
            _recClassificationHolder = null;
            _recommendationHolder = null;
            _recStatusHolder = null;
            _surveyHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the SurveyRecommendation entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the SurveyID field.
            /// </summary>
            SurveyID,
            /// <summary>
            /// Represents the RecommendationID field.
            /// </summary>
            RecommendationID,
            /// <summary>
            /// Represents the RecommendationNumber field.
            /// </summary>
            RecommendationNumber,
            /// <summary>
            /// Represents the EntryText field.
            /// </summary>
            EntryText,
            /// <summary>
            /// Represents the Comments field.
            /// </summary>
            Comments,
            /// <summary>
            /// Represents the DateCreated field.
            /// </summary>
            DateCreated,
            /// <summary>
            /// Represents the DateCompleted field.
            /// </summary>
            DateCompleted,
            /// <summary>
            /// Represents the RecClassificationID field.
            /// </summary>
            RecClassificationID,
            /// <summary>
            /// Represents the RecStatusID field.
            /// </summary>
            RecStatusID,
            /// <summary>
            /// Represents the PriorityIndex field.
            /// </summary>
            PriorityIndex,
        }

        #endregion
    }
}