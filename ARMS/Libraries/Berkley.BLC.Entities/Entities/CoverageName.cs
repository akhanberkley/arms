using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a CoverageName entity, which maps to table 'CoverageName' in the database.
    /// </summary>
    public class CoverageName : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private CoverageName()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this CoverageName.</param>
        public CoverageName(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _companyID;
        private string _lineOfBusinessCode;
        private string _subLineOfBusinessCode = String.Empty;
        private string _typeCode;
        private bool _isRequired;
        private bool _migrationOnly;
        private int _displayOrder;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _coverageNameTypeHolder = null;
        private ObjectHolder _lineOfBusinessHolder = null;
        private ObjectHolder _subLineOfBusinessHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Coverage Name.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
            set
            {
                VerifyWritable();
                _companyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Line of Business.
        /// </summary>
        public string LineOfBusinessCode
        {
            get { return _lineOfBusinessCode; }
            set
            {
                VerifyWritable();
                _lineOfBusinessCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Sub Line of Business. Null value is 'String.Empty'.
        /// </summary>
        public string SubLineOfBusinessCode
        {
            get { return _subLineOfBusinessCode; }
            set
            {
                VerifyWritable();
                _subLineOfBusinessCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Coverage Name Type.
        /// </summary>
        public string TypeCode
        {
            get { return _typeCode; }
            set
            {
                VerifyWritable();
                _typeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Is Required.
        /// </summary>
        public bool IsRequired
        {
            get { return _isRequired; }
            set
            {
                VerifyWritable();
                _isRequired = value;
            }
        }

        /// <summary>
        /// Gets or sets the Migration Only.
        /// </summary>
        public bool MigrationOnly
        {
            get { return _migrationOnly; }
            set
            {
                VerifyWritable();
                _migrationOnly = value;
            }
        }

        /// <summary>
        /// Gets or sets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
            set
            {
                VerifyWritable();
                _displayOrder = value;
            }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a CoverageNameType object related to this entity.
        /// </summary>
        public CoverageNameType Type
        {
            get
            {
                _coverageNameTypeHolder.Key = _typeCode;
                return (CoverageNameType)_coverageNameTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a LineOfBusiness object related to this entity.
        /// </summary>
        public LineOfBusiness LineOfBusiness
        {
            get
            {
                _lineOfBusinessHolder.Key = _lineOfBusinessCode;
                return (LineOfBusiness)_lineOfBusinessHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a LineOfBusiness object related to this entity.
        /// </summary>
        public LineOfBusiness SubLineOfBusiness
        {
            get
            {
                _subLineOfBusinessHolder.Key = _subLineOfBusinessCode;
                return (LineOfBusiness)_subLineOfBusinessHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_companyID": return _companyID;
                    case "_lineOfBusinessCode": return _lineOfBusinessCode;
                    case "_subLineOfBusinessCode": return _subLineOfBusinessCode;
                    case "_typeCode": return _typeCode;
                    case "_isRequired": return _isRequired;
                    case "_migrationOnly": return _migrationOnly;
                    case "_displayOrder": return _displayOrder;
                    case "_companyHolder": return _companyHolder;
                    case "_coverageNameTypeHolder": return _coverageNameTypeHolder;
                    case "_lineOfBusinessHolder": return _lineOfBusinessHolder;
                    case "_subLineOfBusinessHolder": return _subLineOfBusinessHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_lineOfBusinessCode": _lineOfBusinessCode = (string)value; break;
                    case "_subLineOfBusinessCode": _subLineOfBusinessCode = (string)value; break;
                    case "_typeCode": _typeCode = (string)value; break;
                    case "_isRequired": _isRequired = (bool)value; break;
                    case "_migrationOnly": _migrationOnly = (bool)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_coverageNameTypeHolder": _coverageNameTypeHolder = (ObjectHolder)value; break;
                    case "_lineOfBusinessHolder": _lineOfBusinessHolder = (ObjectHolder)value; break;
                    case "_subLineOfBusinessHolder": _subLineOfBusinessHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static CoverageName Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(CoverageName), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch CoverageName entity with ID = '{0}'.", id));
            }
            return (CoverageName)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static CoverageName GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(CoverageName), opathExpression);
            return (CoverageName)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static CoverageName GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(CoverageName))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (CoverageName)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static CoverageName[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(CoverageName), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static CoverageName[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(CoverageName))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (CoverageName[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static CoverageName[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(CoverageName), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public CoverageName GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            CoverageName clone = (CoverageName)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.LineOfBusinessCode, _lineOfBusinessCode);
            Validator.Validate(Field.SubLineOfBusinessCode, _subLineOfBusinessCode);
            Validator.Validate(Field.TypeCode, _typeCode);
            Validator.Validate(Field.IsRequired, _isRequired);
            Validator.Validate(Field.MigrationOnly, _migrationOnly);
            Validator.Validate(Field.DisplayOrder, _displayOrder);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _companyID = Guid.Empty;
            _lineOfBusinessCode = null;
            _subLineOfBusinessCode = null;
            _typeCode = null;
            _isRequired = false;
            _migrationOnly = false;
            _displayOrder = Int32.MinValue;
            _companyHolder = null;
            _coverageNameTypeHolder = null;
            _lineOfBusinessHolder = null;
            _subLineOfBusinessHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the CoverageName entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the LineOfBusinessCode field.
            /// </summary>
            LineOfBusinessCode,
            /// <summary>
            /// Represents the SubLineOfBusinessCode field.
            /// </summary>
            SubLineOfBusinessCode,
            /// <summary>
            /// Represents the TypeCode field.
            /// </summary>
            TypeCode,
            /// <summary>
            /// Represents the IsRequired field.
            /// </summary>
            IsRequired,
            /// <summary>
            /// Represents the MigrationOnly field.
            /// </summary>
            MigrationOnly,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
        }

        #endregion
    }
}