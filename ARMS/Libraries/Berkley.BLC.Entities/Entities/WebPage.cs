using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a WebPage entity, which maps to table 'WebPage' in the database.
	/// </summary>
	public class WebPage : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private WebPage()
		{
		}


		#region --- Generated Members ---

		private string _path;
		private string _name;
		private bool _isLoginRequired;
		private bool _isAddEditPage;
		private bool _noCachingOnBrowser;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Web Page.
		/// </summary>
		public string Path
		{
			get { return _path; }
		}

		/// <summary>
		/// Gets the Web Page.
		/// </summary>
		public string Name
		{
			get { return _name; }
		}

		/// <summary>
		/// Gets the Is Login Required.
		/// </summary>
		public bool IsLoginRequired
		{
			get { return _isLoginRequired; }
		}

		/// <summary>
		/// Gets the Is Add Edit Page.
		/// </summary>
		public bool IsAddEditPage
		{
			get { return _isAddEditPage; }
		}

		/// <summary>
		/// Gets the No Caching On Browser.
		/// </summary>
		public bool NoCachingOnBrowser
		{
			get { return _noCachingOnBrowser; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_path": return _path;
					case "_name": return _name;
					case "_isLoginRequired": return _isLoginRequired;
					case "_isAddEditPage": return _isAddEditPage;
					case "_noCachingOnBrowser": return _noCachingOnBrowser;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_path": _path = (string)value; break;
					case "_name": _name = (string)value; break;
					case "_isLoginRequired": _isLoginRequired = (bool)value; break;
					case "_isAddEditPage": _isAddEditPage = (bool)value; break;
					case "_noCachingOnBrowser": _noCachingOnBrowser = (bool)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="path">Path of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static WebPage Get(string path)
		{
			OPathQuery query = new OPathQuery(typeof(WebPage), "Path = ?");
			object entity = DB.Engine.GetObject(query, path);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch WebPage entity with Path = '{0}'.", path));
			}
			return (WebPage)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static WebPage GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(WebPage), opathExpression);
			return (WebPage)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static WebPage GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(WebPage) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (WebPage)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static WebPage[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(WebPage), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static WebPage[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(WebPage) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (WebPage[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static WebPage[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(WebPage), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_path = null;
			_name = null;
			_isLoginRequired = false;
			_isAddEditPage = false;
			_noCachingOnBrowser = false;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the WebPage entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the Path field.
			/// </summary>
			Path,
			/// <summary>
			/// Represents the Name field.
			/// </summary>
			Name,
			/// <summary>
			/// Represents the IsLoginRequired field.
			/// </summary>
			IsLoginRequired,
			/// <summary>
			/// Represents the IsAddEditPage field.
			/// </summary>
			IsAddEditPage,
			/// <summary>
			/// Represents the NoCachingOnBrowser field.
			/// </summary>
			NoCachingOnBrowser,
		}
		
		#endregion
	}
}