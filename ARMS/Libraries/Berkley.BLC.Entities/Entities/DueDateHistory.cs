using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a DueDateHistory entity, which maps to table 'DueDateHistory' in the database.
	/// </summary>
	public class DueDateHistory : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private DueDateHistory()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this DueDateHistory.</param>
		public DueDateHistory(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _id;
		private Guid _companyID;
		private Guid _surveyID;
		private Guid _requestedByUserID;
		private DateTime _currentDueDate;
		private DateTime _updatedDueDate;
		private bool _approved;
		private Guid _approvalByUserID = Guid.Empty;
		private DateTime _createDateTime;
		private ObjectHolder _approvalByUserHolder = null;
		private ObjectHolder _companyHolder = null;
		private ObjectHolder _requestedByUserHolder = null;
		private ObjectHolder _surveyHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the ID.
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets or sets the Company.
		/// </summary>
		public Guid CompanyID
		{
			get { return _companyID; }
			set
			{
				VerifyWritable();
				_companyID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Survey.
		/// </summary>
		public Guid SurveyID
		{
			get { return _surveyID; }
			set
			{
				VerifyWritable();
				_surveyID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Requested By User.
		/// </summary>
		public Guid RequestedByUserID
		{
			get { return _requestedByUserID; }
			set
			{
				VerifyWritable();
				_requestedByUserID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Current Due Date.
		/// </summary>
		public DateTime CurrentDueDate
		{
			get { return _currentDueDate; }
			set
			{
				VerifyWritable();
				_currentDueDate = value;
			}
		}

		/// <summary>
		/// Gets or sets the Updated Due Date.
		/// </summary>
		public DateTime UpdatedDueDate
		{
			get { return _updatedDueDate; }
			set
			{
				VerifyWritable();
				_updatedDueDate = value;
			}
		}

		/// <summary>
		/// Gets or sets the Approved.
		/// </summary>
		public bool Approved
		{
			get { return _approved; }
			set
			{
				VerifyWritable();
				_approved = value;
			}
		}

		/// <summary>
		/// Gets or sets the Approval By User. Null value is 'Guid.Empty'.
		/// </summary>
		public Guid ApprovalByUserID
		{
			get { return _approvalByUserID; }
			set
			{
				VerifyWritable();
				_approvalByUserID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Create Date Time.
		/// </summary>
		public DateTime CreateDateTime
		{
			get { return _createDateTime; }
			set
			{
				VerifyWritable();
				_createDateTime = value;
			}
		}

		/// <summary>
		/// Gets the instance of a User object related to this entity.
		/// </summary>
		public User ApprovalByUser
		{
			get
			{
				_approvalByUserHolder.Key = _approvalByUserID;
				return (User)_approvalByUserHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Company object related to this entity.
		/// </summary>
		public Company Company
		{
			get
			{
				_companyHolder.Key = _companyID;
				return (Company)_companyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a User object related to this entity.
		/// </summary>
		public User RequestedByUser
		{
			get
			{
				_requestedByUserHolder.Key = _requestedByUserID;
				return (User)_requestedByUserHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Survey object related to this entity.
		/// </summary>
		public Survey Survey
		{
			get
			{
				_surveyHolder.Key = _surveyID;
				return (Survey)_surveyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_companyID": return _companyID;
					case "_surveyID": return _surveyID;
					case "_requestedByUserID": return _requestedByUserID;
					case "_currentDueDate": return _currentDueDate;
					case "_updatedDueDate": return _updatedDueDate;
					case "_approved": return _approved;
					case "_approvalByUserID": return _approvalByUserID;
					case "_createDateTime": return _createDateTime;
					case "_approvalByUserHolder": return _approvalByUserHolder;
					case "_companyHolder": return _companyHolder;
					case "_requestedByUserHolder": return _requestedByUserHolder;
					case "_surveyHolder": return _surveyHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (Guid)value; break;
					case "_companyID": _companyID = (Guid)value; break;
					case "_surveyID": _surveyID = (Guid)value; break;
					case "_requestedByUserID": _requestedByUserID = (Guid)value; break;
					case "_currentDueDate": _currentDueDate = (DateTime)value; break;
					case "_updatedDueDate": _updatedDueDate = (DateTime)value; break;
					case "_approved": _approved = (bool)value; break;
					case "_approvalByUserID": _approvalByUserID = (Guid)value; break;
					case "_createDateTime": _createDateTime = (DateTime)value; break;
					case "_approvalByUserHolder": _approvalByUserHolder = (ObjectHolder)value; break;
					case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
					case "_requestedByUserHolder": _requestedByUserHolder = (ObjectHolder)value; break;
					case "_surveyHolder": _surveyHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static DueDateHistory Get(Guid id)
		{
			OPathQuery query = new OPathQuery(typeof(DueDateHistory), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch DueDateHistory entity with ID = '{0}'.", id));
			}
			return (DueDateHistory)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static DueDateHistory GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(DueDateHistory), opathExpression);
			return (DueDateHistory)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static DueDateHistory GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(DueDateHistory) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (DueDateHistory)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static DueDateHistory[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(DueDateHistory), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static DueDateHistory[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(DueDateHistory) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (DueDateHistory[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static DueDateHistory[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(DueDateHistory), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public DueDateHistory GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			DueDateHistory clone = (DueDateHistory)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ID, _id);
			Validator.Validate(Field.CompanyID, _companyID);
			Validator.Validate(Field.SurveyID, _surveyID);
			Validator.Validate(Field.RequestedByUserID, _requestedByUserID);
			Validator.Validate(Field.CurrentDueDate, _currentDueDate);
			Validator.Validate(Field.UpdatedDueDate, _updatedDueDate);
			Validator.Validate(Field.Approved, _approved);
			Validator.Validate(Field.ApprovalByUserID, _approvalByUserID);
			Validator.Validate(Field.CreateDateTime, _createDateTime);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Guid.Empty;
			_companyID = Guid.Empty;
			_surveyID = Guid.Empty;
			_requestedByUserID = Guid.Empty;
			_currentDueDate = DateTime.MinValue;
			_updatedDueDate = DateTime.MinValue;
			_approved = false;
			_approvalByUserID = Guid.Empty;
			_createDateTime = DateTime.MinValue;
			_approvalByUserHolder = null;
			_companyHolder = null;
			_requestedByUserHolder = null;
			_surveyHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the DueDateHistory entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the ID field.
			/// </summary>
			ID,
			/// <summary>
			/// Represents the CompanyID field.
			/// </summary>
			CompanyID,
			/// <summary>
			/// Represents the SurveyID field.
			/// </summary>
			SurveyID,
			/// <summary>
			/// Represents the RequestedByUserID field.
			/// </summary>
			RequestedByUserID,
			/// <summary>
			/// Represents the CurrentDueDate field.
			/// </summary>
			CurrentDueDate,
			/// <summary>
			/// Represents the UpdatedDueDate field.
			/// </summary>
			UpdatedDueDate,
			/// <summary>
			/// Represents the Approved field.
			/// </summary>
			Approved,
			/// <summary>
			/// Represents the ApprovalByUserID field.
			/// </summary>
			ApprovalByUserID,
			/// <summary>
			/// Represents the CreateDateTime field.
			/// </summary>
			CreateDateTime,
		}
		
		#endregion
	}
}