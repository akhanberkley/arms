using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a FeeUser entity, which maps to table 'FeeUser' in the database.
	/// </summary>
	public class FeeUser : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private FeeUser()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this FeeUser.</param>
		public FeeUser(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}

        /// <summary>
        /// Computes a password hash that is specific to this User for storage in the database.
        /// </summary>
        /// <param name="value">Clear-text passowrd to be encoded.</param>
        /// <returns>SHA1 encrypted value with salt added.</returns>
        public string CreatePasswordHash(string value)
        {
            return Security.CreateSha1Hash(value, this.ID.ToString());
        }

		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


        #region --- Generated Members ---

        private Guid _id;
        private Guid _feeCompanyUserID;
        private string _username;
        private string _name;
        private string _emailAddress = String.Empty;
        private string _phoneNumber = String.Empty;
        private string _alternatePhoneNumber = String.Empty;
        private bool _accountDisabled;
        private string _passwordHash;
        private int _failedLoginAttempts;
        private DateTime _lockedOutUntil = DateTime.MinValue;
        private ObjectHolder _feeCompanyUserHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Fee User ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Fee Company User.
        /// </summary>
        public Guid FeeCompanyUserID
        {
            get { return _feeCompanyUserID; }
            set
            {
                VerifyWritable();
                _feeCompanyUserID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Username.
        /// </summary>
        public string Username
        {
            get { return _username; }
            set
            {
                VerifyWritable();
                _username = value;
            }
        }

        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                VerifyWritable();
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets the Email Address. Null value is 'String.Empty'.
        /// </summary>
        public string EmailAddress
        {
            get { return _emailAddress; }
            set
            {
                VerifyWritable();
                _emailAddress = value;
            }
        }

        /// <summary>
        /// Gets or sets the Phone Number. Null value is 'String.Empty'.
        /// </summary>
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                VerifyWritable();
                _phoneNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Alternate Phone Number. Null value is 'String.Empty'.
        /// </summary>
        public string AlternatePhoneNumber
        {
            get { return _alternatePhoneNumber; }
            set
            {
                VerifyWritable();
                _alternatePhoneNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Account Disabled.
        /// </summary>
        public bool AccountDisabled
        {
            get { return _accountDisabled; }
            set
            {
                VerifyWritable();
                _accountDisabled = value;
            }
        }

        /// <summary>
        /// Gets or sets the Password Hash.
        /// </summary>
        public string PasswordHash
        {
            get { return _passwordHash; }
            set
            {
                VerifyWritable();
                _passwordHash = value;
            }
        }

        /// <summary>
        /// Gets or sets the Failed Login Attempts.
        /// </summary>
        public int FailedLoginAttempts
        {
            get { return _failedLoginAttempts; }
            set
            {
                VerifyWritable();
                _failedLoginAttempts = value;
            }
        }

        /// <summary>
        /// Gets or sets the Locked Out Until. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime LockedOutUntil
        {
            get { return _lockedOutUntil; }
            set
            {
                VerifyWritable();
                _lockedOutUntil = value;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User FeeCompanyUser
        {
            get
            {
                _feeCompanyUserHolder.Key = _feeCompanyUserID;
                return (User)_feeCompanyUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_feeCompanyUserID": return _feeCompanyUserID;
                    case "_username": return _username;
                    case "_name": return _name;
                    case "_emailAddress": return _emailAddress;
                    case "_phoneNumber": return _phoneNumber;
                    case "_alternatePhoneNumber": return _alternatePhoneNumber;
                    case "_accountDisabled": return _accountDisabled;
                    case "_passwordHash": return _passwordHash;
                    case "_failedLoginAttempts": return _failedLoginAttempts;
                    case "_lockedOutUntil": return _lockedOutUntil;
                    case "_feeCompanyUserHolder": return _feeCompanyUserHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_feeCompanyUserID": _feeCompanyUserID = (Guid)value; break;
                    case "_username": _username = (string)value; break;
                    case "_name": _name = (string)value; break;
                    case "_emailAddress": _emailAddress = (string)value; break;
                    case "_phoneNumber": _phoneNumber = (string)value; break;
                    case "_alternatePhoneNumber": _alternatePhoneNumber = (string)value; break;
                    case "_accountDisabled": _accountDisabled = (bool)value; break;
                    case "_passwordHash": _passwordHash = (string)value; break;
                    case "_failedLoginAttempts": _failedLoginAttempts = (int)value; break;
                    case "_lockedOutUntil": _lockedOutUntil = (DateTime)value; break;
                    case "_feeCompanyUserHolder": _feeCompanyUserHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static FeeUser Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(FeeUser), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch FeeUser entity with ID = '{0}'.", id));
            }
            return (FeeUser)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static FeeUser GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(FeeUser), opathExpression);
            return (FeeUser)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static FeeUser GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(FeeUser))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (FeeUser)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static FeeUser[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(FeeUser), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static FeeUser[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(FeeUser))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (FeeUser[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static FeeUser[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(FeeUser), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public FeeUser GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            FeeUser clone = (FeeUser)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.FeeCompanyUserID, _feeCompanyUserID);
            Validator.Validate(Field.Username, _username);
            Validator.Validate(Field.Name, _name);
            Validator.Validate(Field.EmailAddress, _emailAddress);
            Validator.Validate(Field.PhoneNumber, _phoneNumber);
            Validator.Validate(Field.AlternatePhoneNumber, _alternatePhoneNumber);
            Validator.Validate(Field.AccountDisabled, _accountDisabled);
            Validator.Validate(Field.PasswordHash, _passwordHash);
            Validator.Validate(Field.FailedLoginAttempts, _failedLoginAttempts);
            Validator.Validate(Field.LockedOutUntil, _lockedOutUntil);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _feeCompanyUserID = Guid.Empty;
            _username = null;
            _name = null;
            _emailAddress = null;
            _phoneNumber = null;
            _alternatePhoneNumber = null;
            _accountDisabled = false;
            _passwordHash = null;
            _failedLoginAttempts = Int32.MinValue;
            _lockedOutUntil = DateTime.MinValue;
            _feeCompanyUserHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the FeeUser entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the FeeCompanyUserID field.
            /// </summary>
            FeeCompanyUserID,
            /// <summary>
            /// Represents the Username field.
            /// </summary>
            Username,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the EmailAddress field.
            /// </summary>
            EmailAddress,
            /// <summary>
            /// Represents the PhoneNumber field.
            /// </summary>
            PhoneNumber,
            /// <summary>
            /// Represents the AlternatePhoneNumber field.
            /// </summary>
            AlternatePhoneNumber,
            /// <summary>
            /// Represents the AccountDisabled field.
            /// </summary>
            AccountDisabled,
            /// <summary>
            /// Represents the PasswordHash field.
            /// </summary>
            PasswordHash,
            /// <summary>
            /// Represents the FailedLoginAttempts field.
            /// </summary>
            FailedLoginAttempts,
            /// <summary>
            /// Represents the LockedOutUntil field.
            /// </summary>
            LockedOutUntil,
        }

        #endregion
    }
}