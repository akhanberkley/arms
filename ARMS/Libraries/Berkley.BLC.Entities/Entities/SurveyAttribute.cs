using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyAttribute entity, which maps to table 'SurveyAttribute' in the database.
	/// </summary>
	public class SurveyAttribute : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyAttribute()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="surveyID">SurveyID of this SurveyAttribute.</param>
		/// <param name="companyAttributeID">CompanyAttributeID of this SurveyAttribute.</param>
		public SurveyAttribute(Guid surveyID, Guid companyAttributeID)
		{
			_surveyID = surveyID;
			_companyAttributeID = companyAttributeID;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _surveyID;
		private Guid _companyAttributeID;
		private string _attributeValue = String.Empty;
		private string _lastModifiedBy = String.Empty;
		private DateTime _lastModifiedDate = DateTime.MinValue;
		private ObjectHolder _companyAttributeHolder = null;
		private ObjectHolder _surveyHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Survey.
		/// </summary>
		public Guid SurveyID
		{
			get { return _surveyID; }
		}

		/// <summary>
		/// Gets the Company Attribute.
		/// </summary>
		public Guid CompanyAttributeID
		{
			get { return _companyAttributeID; }
		}

		/// <summary>
		/// Gets or sets the Attribute Value. Null value is 'String.Empty'.
		/// </summary>
		public string AttributeValue
		{
			get { return _attributeValue; }
			set
			{
				VerifyWritable();
				_attributeValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the Last Modified By. Null value is 'String.Empty'.
		/// </summary>
		public string LastModifiedBy
		{
			get { return _lastModifiedBy; }
			set
			{
				VerifyWritable();
				_lastModifiedBy = value;
			}
		}

		/// <summary>
		/// Gets or sets the Last Modified Date. Null value is 'DateTime.MinValue'.
		/// </summary>
		public DateTime LastModifiedDate
		{
			get { return _lastModifiedDate; }
			set
			{
				VerifyWritable();
				_lastModifiedDate = value;
			}
		}

		/// <summary>
		/// Gets the instance of a CompanyAttribute object related to this entity.
		/// </summary>
		public CompanyAttribute CompanyAttribute
		{
			get
			{
				_companyAttributeHolder.Key = _companyAttributeID;
				return (CompanyAttribute)_companyAttributeHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Survey object related to this entity.
		/// </summary>
		public Survey Survey
		{
			get
			{
				_surveyHolder.Key = _surveyID;
				return (Survey)_surveyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_surveyID": return _surveyID;
					case "_companyAttributeID": return _companyAttributeID;
					case "_attributeValue": return _attributeValue;
					case "_lastModifiedBy": return _lastModifiedBy;
					case "_lastModifiedDate": return _lastModifiedDate;
					case "_companyAttributeHolder": return _companyAttributeHolder;
					case "_surveyHolder": return _surveyHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_surveyID": _surveyID = (Guid)value; break;
					case "_companyAttributeID": _companyAttributeID = (Guid)value; break;
					case "_attributeValue": _attributeValue = (string)value; break;
					case "_lastModifiedBy": _lastModifiedBy = (string)value; break;
					case "_lastModifiedDate": _lastModifiedDate = (DateTime)value; break;
					case "_companyAttributeHolder": _companyAttributeHolder = (ObjectHolder)value; break;
					case "_surveyHolder": _surveyHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="surveyID">SurveyID of the entity to fetch.</param>
		/// <param name="companyAttributeID">CompanyAttributeID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static SurveyAttribute Get(Guid surveyID, Guid companyAttributeID)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyAttribute), "SurveyID = ? && CompanyAttributeID = ?");
			object entity = DB.Engine.GetObject(query, surveyID, companyAttributeID);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch SurveyAttribute entity with SurveyID = '{0}' and CompanyAttributeID = '{1}'.", surveyID, companyAttributeID));
			}
			return (SurveyAttribute)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyAttribute GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyAttribute), opathExpression);
			return (SurveyAttribute)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyAttribute GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyAttribute) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyAttribute)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyAttribute[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyAttribute), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyAttribute[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyAttribute) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyAttribute[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyAttribute[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyAttribute), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public SurveyAttribute GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			SurveyAttribute clone = (SurveyAttribute)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.SurveyID, _surveyID);
			Validator.Validate(Field.CompanyAttributeID, _companyAttributeID);
			Validator.Validate(Field.AttributeValue, _attributeValue);
			Validator.Validate(Field.LastModifiedBy, _lastModifiedBy);
			Validator.Validate(Field.LastModifiedDate, _lastModifiedDate);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_surveyID = Guid.Empty;
			_companyAttributeID = Guid.Empty;
			_attributeValue = null;
			_lastModifiedBy = null;
			_lastModifiedDate = DateTime.MinValue;
			_companyAttributeHolder = null;
			_surveyHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the SurveyAttribute entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the SurveyID field.
			/// </summary>
			SurveyID,
			/// <summary>
			/// Represents the CompanyAttributeID field.
			/// </summary>
			CompanyAttributeID,
			/// <summary>
			/// Represents the AttributeValue field.
			/// </summary>
			AttributeValue,
			/// <summary>
			/// Represents the LastModifiedBy field.
			/// </summary>
			LastModifiedBy,
			/// <summary>
			/// Represents the LastModifiedDate field.
			/// </summary>
			LastModifiedDate,
		}
		
		#endregion
	}
}