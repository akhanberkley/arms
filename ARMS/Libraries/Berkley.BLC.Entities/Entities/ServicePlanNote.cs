using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a ServicePlanNote entity, which maps to table 'ServicePlanNote' in the database.
    /// </summary>
    public class ServicePlanNote : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private ServicePlanNote()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this ServicePlanNote.</param>
        public ServicePlanNote(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _servicePlanID;
        private Guid _userID;
        private DateTime _entryDate;
        private string _comment;
        private ObjectHolder _servicePlanHolder = null;
        private ObjectHolder _userHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Service Plan Note ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Service Plan.
        /// </summary>
        public Guid ServicePlanID
        {
            get { return _servicePlanID; }
            set
            {
                VerifyWritable();
                _servicePlanID = value;
            }
        }

        /// <summary>
        /// Gets or sets the User.
        /// </summary>
        public Guid UserID
        {
            get { return _userID; }
            set
            {
                VerifyWritable();
                _userID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Entry Date.
        /// </summary>
        public DateTime EntryDate
        {
            get { return _entryDate; }
            set
            {
                VerifyWritable();
                _entryDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Comment.
        /// </summary>
        public string Comment
        {
            get { return _comment; }
            set
            {
                VerifyWritable();
                _comment = value;
            }
        }

        /// <summary>
        /// Gets the instance of a ServicePlan object related to this entity.
        /// </summary>
        public ServicePlan ServicePlan
        {
            get
            {
                _servicePlanHolder.Key = _servicePlanID;
                return (ServicePlan)_servicePlanHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User User
        {
            get
            {
                _userHolder.Key = _userID;
                return (User)_userHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_servicePlanID": return _servicePlanID;
                    case "_userID": return _userID;
                    case "_entryDate": return _entryDate;
                    case "_comment": return _comment;
                    case "_servicePlanHolder": return _servicePlanHolder;
                    case "_userHolder": return _userHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_servicePlanID": _servicePlanID = (Guid)value; break;
                    case "_userID": _userID = (Guid)value; break;
                    case "_entryDate": _entryDate = (DateTime)value; break;
                    case "_comment": _comment = (string)value; break;
                    case "_servicePlanHolder": _servicePlanHolder = (ObjectHolder)value; break;
                    case "_userHolder": _userHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static ServicePlanNote Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanNote), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch ServicePlanNote entity with ID = '{0}'.", id));
            }
            return (ServicePlanNote)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ServicePlanNote GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanNote), opathExpression);
            return (ServicePlanNote)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ServicePlanNote GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ServicePlanNote))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ServicePlanNote)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlanNote[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanNote), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlanNote[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ServicePlanNote))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ServicePlanNote[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlanNote[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanNote), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public ServicePlanNote GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            ServicePlanNote clone = (ServicePlanNote)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.ServicePlanID, _servicePlanID);
            Validator.Validate(Field.UserID, _userID);
            Validator.Validate(Field.EntryDate, _entryDate);
            Validator.Validate(Field.Comment, _comment);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _servicePlanID = Guid.Empty;
            _userID = Guid.Empty;
            _entryDate = DateTime.MinValue;
            _comment = null;
            _servicePlanHolder = null;
            _userHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the ServicePlanNote entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the ServicePlanID field.
            /// </summary>
            ServicePlanID,
            /// <summary>
            /// Represents the UserID field.
            /// </summary>
            UserID,
            /// <summary>
            /// Represents the EntryDate field.
            /// </summary>
            EntryDate,
            /// <summary>
            /// Represents the Comment field.
            /// </summary>
            Comment,
        }

        #endregion
    }
}