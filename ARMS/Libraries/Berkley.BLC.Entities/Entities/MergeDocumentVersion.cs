using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a MergeDocumentVersion entity, which maps to table 'MergeDocumentVersion' in the database.
	/// </summary>
	public class MergeDocumentVersion : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private MergeDocumentVersion()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this MergeDocumentVersion.</param>
		public MergeDocumentVersion(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


        #region --- Generated Members ---

        private Guid _id;
        private Guid _mergeDocumentID;
        private int _versionNumber;
        private bool _isActive;
        private string _documentName;
        private string _mimeType;
        private long _sizeInBytes;
        private string _fileNetReference;
        private DateTime _uploadedOn;
        private ObjectHolder _mergeDocumentHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Merge Document Version ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Merge Document.
        /// </summary>
        public Guid MergeDocumentID
        {
            get { return _mergeDocumentID; }
            set
            {
                VerifyWritable();
                _mergeDocumentID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Version Number.
        /// </summary>
        public int VersionNumber
        {
            get { return _versionNumber; }
            set
            {
                VerifyWritable();
                _versionNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Is Active.
        /// </summary>
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                VerifyWritable();
                _isActive = value;
            }
        }

        /// <summary>
        /// Gets or sets the Document Name.
        /// </summary>
        public string DocumentName
        {
            get { return _documentName; }
            set
            {
                VerifyWritable();
                _documentName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Mime Type.
        /// </summary>
        public string MimeType
        {
            get { return _mimeType; }
            set
            {
                VerifyWritable();
                _mimeType = value;
            }
        }

        /// <summary>
        /// Gets or sets the Size In Bytes.
        /// </summary>
        public long SizeInBytes
        {
            get { return _sizeInBytes; }
            set
            {
                VerifyWritable();
                _sizeInBytes = value;
            }
        }

        /// <summary>
        /// Gets or sets the File Net Reference.
        /// </summary>
        public string FileNetReference
        {
            get { return _fileNetReference; }
            set
            {
                VerifyWritable();
                _fileNetReference = value;
            }
        }

        /// <summary>
        /// Gets or sets the Uploaded On.
        /// </summary>
        public DateTime UploadedOn
        {
            get { return _uploadedOn; }
            set
            {
                VerifyWritable();
                _uploadedOn = value;
            }
        }

        /// <summary>
        /// Gets the instance of a MergeDocument object related to this entity.
        /// </summary>
        public MergeDocument MergeDocument
        {
            get
            {
                _mergeDocumentHolder.Key = _mergeDocumentID;
                return (MergeDocument)_mergeDocumentHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_mergeDocumentID": return _mergeDocumentID;
                    case "_versionNumber": return _versionNumber;
                    case "_isActive": return _isActive;
                    case "_documentName": return _documentName;
                    case "_mimeType": return _mimeType;
                    case "_sizeInBytes": return _sizeInBytes;
                    case "_fileNetReference": return _fileNetReference;
                    case "_uploadedOn": return _uploadedOn;
                    case "_mergeDocumentHolder": return _mergeDocumentHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_mergeDocumentID": _mergeDocumentID = (Guid)value; break;
                    case "_versionNumber": _versionNumber = (int)value; break;
                    case "_isActive": _isActive = (bool)value; break;
                    case "_documentName": _documentName = (string)value; break;
                    case "_mimeType": _mimeType = (string)value; break;
                    case "_sizeInBytes": _sizeInBytes = (long)value; break;
                    case "_fileNetReference": _fileNetReference = (string)value; break;
                    case "_uploadedOn": _uploadedOn = (DateTime)value; break;
                    case "_mergeDocumentHolder": _mergeDocumentHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static MergeDocumentVersion Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(MergeDocumentVersion), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch MergeDocumentVersion entity with ID = '{0}'.", id));
            }
            return (MergeDocumentVersion)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static MergeDocumentVersion GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(MergeDocumentVersion), opathExpression);
            return (MergeDocumentVersion)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static MergeDocumentVersion GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(MergeDocumentVersion))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (MergeDocumentVersion)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static MergeDocumentVersion[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(MergeDocumentVersion), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static MergeDocumentVersion[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(MergeDocumentVersion))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (MergeDocumentVersion[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static MergeDocumentVersion[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(MergeDocumentVersion), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public MergeDocumentVersion GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            MergeDocumentVersion clone = (MergeDocumentVersion)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.MergeDocumentID, _mergeDocumentID);
            Validator.Validate(Field.VersionNumber, _versionNumber);
            Validator.Validate(Field.IsActive, _isActive);
            Validator.Validate(Field.DocumentName, _documentName);
            Validator.Validate(Field.MimeType, _mimeType);
            Validator.Validate(Field.SizeInBytes, _sizeInBytes);
            Validator.Validate(Field.FileNetReference, _fileNetReference);
            Validator.Validate(Field.UploadedOn, _uploadedOn);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _mergeDocumentID = Guid.Empty;
            _versionNumber = Int32.MinValue;
            _isActive = false;
            _documentName = null;
            _mimeType = null;
            _sizeInBytes = Int64.MinValue;
            _fileNetReference = null;
            _uploadedOn = DateTime.MinValue;
            _mergeDocumentHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the MergeDocumentVersion entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the MergeDocumentID field.
            /// </summary>
            MergeDocumentID,
            /// <summary>
            /// Represents the VersionNumber field.
            /// </summary>
            VersionNumber,
            /// <summary>
            /// Represents the IsActive field.
            /// </summary>
            IsActive,
            /// <summary>
            /// Represents the DocumentName field.
            /// </summary>
            DocumentName,
            /// <summary>
            /// Represents the MimeType field.
            /// </summary>
            MimeType,
            /// <summary>
            /// Represents the SizeInBytes field.
            /// </summary>
            SizeInBytes,
            /// <summary>
            /// Represents the FileNetReference field.
            /// </summary>
            FileNetReference,
            /// <summary>
            /// Represents the UploadedOn field.
            /// </summary>
            UploadedOn,
        }

        #endregion
    }
}