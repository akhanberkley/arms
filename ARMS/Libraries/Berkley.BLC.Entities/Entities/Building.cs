using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;
using Berkley.BLC.Core.Extensions;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a Building entity, which maps to table 'Building' in the database.
    /// </summary>
    public class Building : IObjectHelper
    {
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private Building()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this Building.</param>
        public Building(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Gets the address as an HTML-encoded multi-line string value.
        /// </summary>
        public string HtmlHasSprinklerSystem
        {
            get
            {
                return Formatter.BoolToYesNo(this.HasSprinklerSystem, "(not specified)");
            }
        }

        /// <summary>
        /// Gets the address as an HTML-encoded multi-line string value.
        /// </summary>
        public string HtmlHasSprinklerSystemCredit
        {
            get
            {
                return Formatter.BoolToYesNo(this.HasSprinklerSystemCredit, "(not specified)");
            }
        }

        /// <summary>
        /// Gets the address as an HTML-encoded multi-line string value.
        /// </summary>
        public string HtmlHasSprinklerSystemNone
        {
            get
            {
                return Formatter.BoolToYesNo(this.HasSprinklerSystem, "(none)");
            }
        }

        /// <summary>
        /// Gets the address as an HTML-encoded multi-line string value.
        /// </summary>
        public string HtmlHasSprinklerSystemCreditNone
        {
            get
            {
                return Formatter.BoolToYesNo(this.HasSprinklerSystemCredit, "(none)");
            }
        }

		/// <summary>
		/// Selective "assignment operator".  Copied data fields from the Right Hand Side instance to "this" instance
		/// </summary>
		/// <param name="oRhs">Instance with new data</param>
		/// <returns>The modified class</returns>
		public Building Assign(Building oRhs)
		{
			Building oBuilding = this;

			if (oBuilding.IsReadOnly)
			{
				oBuilding = oBuilding.GetWritableInstance();
			}
			oBuilding.Number = oRhs._number;
			oBuilding.YearBuilt = oRhs._yearBuilt;
			oBuilding.HasSprinklerSystem = oRhs._hasSprinklerSystem;
            oBuilding.HasSprinklerSystemCredit = oRhs._hasSprinklerSystemCredit;
			oBuilding.PublicProtection = oRhs._publicProtection;
			oBuilding.StockValues = oRhs._stockValues;
			oBuilding.Contents = oRhs._contents;
			oBuilding.Value = oRhs._value;
			oBuilding.BusinessInterruption = oRhs._businessInterruption;
			oBuilding.LocationOccupancy = oRhs._locationOccupancy;
            oBuilding.ValuationTypeCode = oRhs._valuationTypeCode;
            oBuilding.CoinsurancePercentage = oRhs._coinsurancePercentage;
            oBuilding.ITV = oRhs._itv;
			return oBuilding;
		}


        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _locationID;
        private short _number = Int16.MinValue;
        private int _yearBuilt = Int32.MinValue;
        private short _hasSprinklerSystem = Int16.MinValue;
        private short _hasSprinklerSystemCredit = Int16.MinValue;
        private int _publicProtection = Int32.MinValue;
        private decimal _stockValues = Decimal.MinValue;
        private decimal _contents = Decimal.MinValue;
        private decimal _value = Decimal.MinValue;
        private decimal _businessInterruption = Decimal.MinValue;
        private string _locationOccupancy = String.Empty;
        private string _valuationTypeCode = String.Empty;
        private decimal _coinsurancePercentage = Decimal.MinValue;
        private decimal _changeInEnvironmentalControl = Decimal.MinValue;
        private decimal _scientificAnimals = Decimal.MinValue;
        private decimal _contamination = Decimal.MinValue;
        private decimal _radioActiveContamination = Decimal.MinValue;
        private decimal _flood = Decimal.MinValue;
        private decimal _earthquake = Decimal.MinValue;
        private short _itv = Int16.MinValue;
        private ObjectHolder _buildingValuationTypeHolder = null;
        private ObjectHolder _locationHolder = null;
        private bool _isReadOnly = true;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Building ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets or sets the Location.
        /// </summary>
        public Guid LocationID
        {
            get { return _locationID; }
            set
            {
                VerifyWritable();
                _locationID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Building Number. Null value is 'Int16.MinValue'.
        /// </summary>
        public short Number
        {
            get { return _number; }
            set
            {
                VerifyWritable();
                _number = value;
            }
        }

        /// <summary>
        /// Gets or sets the Year Built. Null value is 'Int32.MinValue'.
        /// </summary>
        public int YearBuilt
        {
            get { return _yearBuilt; }
            set
            {
                VerifyWritable();
                _yearBuilt = value;
            }
        }

        /// <summary>
        /// Gets or sets the Has Sprinkler System. Null value is 'Int16.MinValue'.
        /// </summary>
        public short HasSprinklerSystem
        {
            get { return _hasSprinklerSystem; }
            set
            {
                VerifyWritable();
                _hasSprinklerSystem = value;
            }
        }

        /// <summary>
        /// Gets or sets the Has Sprinkler System Credit. Null value is 'Int16.MinValue'.
        /// </summary>
        public short HasSprinklerSystemCredit
        {
            get { return _hasSprinklerSystemCredit; }
            set
            {
                VerifyWritable();
                _hasSprinklerSystemCredit = value;
            }
        }

        /// <summary>
        /// Gets or sets the Public Protection. Null value is 'Int32.MinValue'.
        /// </summary>
        public int PublicProtection
        {
            get { return _publicProtection; }
            set
            {
                VerifyWritable();
                _publicProtection = value;
            }
        }

        /// <summary>
        /// Gets or sets the Stock Values. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal StockValues
        {
            get { return _stockValues; }
            set
            {
                VerifyWritable();
                _stockValues = value;
            }
        }

        /// <summary>
        /// Gets or sets the Contents. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal Contents
        {
            get { return _contents; }
            set
            {
                VerifyWritable();
                _contents = value;
            }
        }

        /// <summary>
        /// Gets or sets the Building Value. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal Value
        {
            get { return _value; }
            set
            {
                VerifyWritable();
                _value = value;
            }
        }

        /// <summary>
        /// Gets or sets the Business Interruption. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal BusinessInterruption
        {
            get { return _businessInterruption; }
            set
            {
                VerifyWritable();
                _businessInterruption = value;
            }
        }

        /// <summary>
        /// Gets or sets the Location Occupancy. Null value is 'String.Empty'.
        /// </summary>
        public string LocationOccupancy
        {
            get { return _locationOccupancy; }
            set
            {
                VerifyWritable();
                _locationOccupancy = value.MaxLength(500);
            }
        }

        /// <summary>
        /// Gets or sets the Building Valuation Type. Null value is 'String.Empty'.
        /// </summary>
        public string ValuationTypeCode
        {
            get { return _valuationTypeCode; }
            set
            {
                VerifyWritable();
                _valuationTypeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Coinsurance Percentage. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal CoinsurancePercentage
        {
            get { return _coinsurancePercentage; }
            set
            {
                VerifyWritable();
                _coinsurancePercentage = value;
            }
        }

        /// <summary>
        /// Gets or sets the Change In Environmental Control. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal ChangeInEnvironmentalControl
        {
            get { return _changeInEnvironmentalControl; }
            set
            {
                VerifyWritable();
                _changeInEnvironmentalControl = value;
            }
        }

        /// <summary>
        /// Gets or sets the Scientific Animals. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal ScientificAnimals
        {
            get { return _scientificAnimals; }
            set
            {
                VerifyWritable();
                _scientificAnimals = value;
            }
        }

        /// <summary>
        /// Gets or sets the Contamination. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal Contamination
        {
            get { return _contamination; }
            set
            {
                VerifyWritable();
                _contamination = value;
            }
        }

        /// <summary>
        /// Gets or sets the Radio Active Contamination. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal RadioActiveContamination
        {
            get { return _radioActiveContamination; }
            set
            {
                VerifyWritable();
                _radioActiveContamination = value;
            }
        }

        /// <summary>
        /// Gets or sets the Flood. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal Flood
        {
            get { return _flood; }
            set
            {
                VerifyWritable();
                _flood = value;
            }
        }

        /// <summary>
        /// Gets or sets the Earthquake. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal Earthquake
        {
            get { return _earthquake; }
            set
            {
                VerifyWritable();
                _earthquake = value;
            }
        }

        /// <summary>
        /// Gets or sets the ITV. Null value is 'Int16.MinValue'.
        /// </summary>
        public short ITV
        {
            get { return _itv; }
            set
            {
                VerifyWritable();
                _itv = value;
            }
        }

        /// <summary>
        /// Gets the instance of a BuildingValuationType object related to this entity.
        /// </summary>
        public BuildingValuationType ValuationType
        {
            get
            {
                _buildingValuationTypeHolder.Key = _valuationTypeCode;
                return (BuildingValuationType)_buildingValuationTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Location object related to this entity.
        /// </summary>
        public Location Location
        {
            get
            {
                _locationHolder.Key = _locationID;
                return (Location)_locationHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_locationID": return _locationID;
                    case "_number": return _number;
                    case "_yearBuilt": return _yearBuilt;
                    case "_hasSprinklerSystem": return _hasSprinklerSystem;
                    case "_hasSprinklerSystemCredit": return _hasSprinklerSystemCredit;
                    case "_publicProtection": return _publicProtection;
                    case "_stockValues": return _stockValues;
                    case "_contents": return _contents;
                    case "_value": return _value;
                    case "_businessInterruption": return _businessInterruption;
                    case "_locationOccupancy": return _locationOccupancy;
                    case "_valuationTypeCode": return _valuationTypeCode;
                    case "_coinsurancePercentage": return _coinsurancePercentage;
                    case "_changeInEnvironmentalControl": return _changeInEnvironmentalControl;
                    case "_scientificAnimals": return _scientificAnimals;
                    case "_contamination": return _contamination;
                    case "_radioActiveContamination": return _radioActiveContamination;
                    case "_flood": return _flood;
                    case "_earthquake": return _earthquake;
                    case "_itv": return _itv;
                    case "_buildingValuationTypeHolder": return _buildingValuationTypeHolder;
                    case "_locationHolder": return _locationHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_locationID": _locationID = (Guid)value; break;
                    case "_number": _number = (short)value; break;
                    case "_yearBuilt": _yearBuilt = (int)value; break;
                    case "_hasSprinklerSystem": _hasSprinklerSystem = (short)value; break;
                    case "_hasSprinklerSystemCredit": _hasSprinklerSystemCredit = (short)value; break;
                    case "_publicProtection": _publicProtection = (int)value; break;
                    case "_stockValues": _stockValues = (decimal)value; break;
                    case "_contents": _contents = (decimal)value; break;
                    case "_value": _value = (decimal)value; break;
                    case "_businessInterruption": _businessInterruption = (decimal)value; break;
                    case "_locationOccupancy": _locationOccupancy = (string)value; break;
                    case "_valuationTypeCode": _valuationTypeCode = (string)value; break;
                    case "_coinsurancePercentage": _coinsurancePercentage = (decimal)value; break;
                    case "_changeInEnvironmentalControl": _changeInEnvironmentalControl = (decimal)value; break;
                    case "_scientificAnimals": _scientificAnimals = (decimal)value; break;
                    case "_contamination": _contamination = (decimal)value; break;
                    case "_radioActiveContamination": _radioActiveContamination = (decimal)value; break;
                    case "_flood": _flood = (decimal)value; break;
                    case "_earthquake": _earthquake = (decimal)value; break;
                    case "_itv": _itv = (short)value; break;
                    case "_buildingValuationTypeHolder": _buildingValuationTypeHolder = (ObjectHolder)value; break;
                    case "_locationHolder": _locationHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static Building Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(Building), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch Building entity with ID = '{0}'.", id));
            }
            return (Building)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Building GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Building), opathExpression);
            return (Building)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Building GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Building))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Building)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Building[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Building), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Building[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Building))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Building[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Building[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Building), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public Building GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            Building clone = (Building)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.LocationID, _locationID);
            Validator.Validate(Field.Number, _number);
            Validator.Validate(Field.YearBuilt, _yearBuilt);
            Validator.Validate(Field.HasSprinklerSystem, _hasSprinklerSystem);
            Validator.Validate(Field.HasSprinklerSystemCredit, _hasSprinklerSystemCredit);
            Validator.Validate(Field.PublicProtection, _publicProtection);
            Validator.Validate(Field.StockValues, _stockValues);
            Validator.Validate(Field.Contents, _contents);
            Validator.Validate(Field.Value, _value);
            Validator.Validate(Field.BusinessInterruption, _businessInterruption);
            Validator.Validate(Field.LocationOccupancy, _locationOccupancy);
            Validator.Validate(Field.ValuationTypeCode, _valuationTypeCode);
            Validator.Validate(Field.CoinsurancePercentage, _coinsurancePercentage);
            Validator.Validate(Field.ChangeInEnvironmentalControl, _changeInEnvironmentalControl);
            Validator.Validate(Field.ScientificAnimals, _scientificAnimals);
            Validator.Validate(Field.Contamination, _contamination);
            Validator.Validate(Field.RadioActiveContamination, _radioActiveContamination);
            Validator.Validate(Field.Flood, _flood);
            Validator.Validate(Field.Earthquake, _earthquake);
            Validator.Validate(Field.ITV, _itv);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _locationID = Guid.Empty;
            _number = Int16.MinValue;
            _yearBuilt = Int32.MinValue;
            _hasSprinklerSystem = Int16.MinValue;
            _hasSprinklerSystemCredit = Int16.MinValue;
            _publicProtection = Int32.MinValue;
            _stockValues = Decimal.MinValue;
            _contents = Decimal.MinValue;
            _value = Decimal.MinValue;
            _businessInterruption = Decimal.MinValue;
            _locationOccupancy = null;
            _valuationTypeCode = null;
            _coinsurancePercentage = Decimal.MinValue;
            _changeInEnvironmentalControl = Decimal.MinValue;
            _scientificAnimals = Decimal.MinValue;
            _contamination = Decimal.MinValue;
            _radioActiveContamination = Decimal.MinValue;
            _flood = Decimal.MinValue;
            _earthquake = Decimal.MinValue;
            _itv = Int16.MinValue;
            _buildingValuationTypeHolder = null;
            _locationHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the Building entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the LocationID field.
            /// </summary>
            LocationID,
            /// <summary>
            /// Represents the Number field.
            /// </summary>
            Number,
            /// <summary>
            /// Represents the YearBuilt field.
            /// </summary>
            YearBuilt,
            /// <summary>
            /// Represents the HasSprinklerSystem field.
            /// </summary>
            HasSprinklerSystem,
            /// <summary>
            /// Represents the HasSprinklerSystemCredit field.
            /// </summary>
            HasSprinklerSystemCredit,
            /// <summary>
            /// Represents the PublicProtection field.
            /// </summary>
            PublicProtection,
            /// <summary>
            /// Represents the StockValues field.
            /// </summary>
            StockValues,
            /// <summary>
            /// Represents the Contents field.
            /// </summary>
            Contents,
            /// <summary>
            /// Represents the Value field.
            /// </summary>
            Value,
            /// <summary>
            /// Represents the BusinessInterruption field.
            /// </summary>
            BusinessInterruption,
            /// <summary>
            /// Represents the LocationOccupancy field.
            /// </summary>
            LocationOccupancy,
            /// <summary>
            /// Represents the ValuationTypeCode field.
            /// </summary>
            ValuationTypeCode,
            /// <summary>
            /// Represents the CoinsurancePercentage field.
            /// </summary>
            CoinsurancePercentage,
            /// <summary>
            /// Represents the ChangeInEnvironmentalControl field.
            /// </summary>
            ChangeInEnvironmentalControl,
            /// <summary>
            /// Represents the ScientificAnimals field.
            /// </summary>
            ScientificAnimals,
            /// <summary>
            /// Represents the Contamination field.
            /// </summary>
            Contamination,
            /// <summary>
            /// Represents the RadioActiveContamination field.
            /// </summary>
            RadioActiveContamination,
            /// <summary>
            /// Represents the Flood field.
            /// </summary>
            Flood,
            /// <summary>
            /// Represents the Earthquake field.
            /// </summary>
            Earthquake,
            /// <summary>
            /// Represents the ITV field.
            /// </summary>
            ITV,
        }

        #endregion
    }
}