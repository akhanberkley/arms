using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a ReportTypeCompany entity, which maps to table 'ReportType_Company' in the database.
	/// </summary>
	public class ReportTypeCompany : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private ReportTypeCompany()
		{
		}


		#region --- Generated Members ---

		private Guid _companyID;
		private string _code;
		private string _name;
		private int _displayOrder;
		private ObjectHolder _companyHolder = null;
		private ObjectHolder _reportTypeHolder = null;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Company.
		/// </summary>
		public Guid CompanyID
		{
			get { return _companyID; }
		}

		/// <summary>
		/// Gets the Report Type.
		/// </summary>
		public string Code
		{
			get { return _code; }
		}

		/// <summary>
		/// Gets the Report Type Name.
		/// </summary>
		public string Name
		{
			get { return _name; }
		}

		/// <summary>
		/// Gets the Display Order.
		/// </summary>
		public int DisplayOrder
		{
			get { return _displayOrder; }
		}

		/// <summary>
		/// Gets the instance of a Company object related to this entity.
		/// </summary>
		public Company Company
		{
			get
			{
				_companyHolder.Key = _companyID;
				return (Company)_companyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a ReportType object related to this entity.
		/// </summary>
		public ReportType ReportType
		{
			get
			{
				_reportTypeHolder.Key = _code;
				return (ReportType)_reportTypeHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_companyID": return _companyID;
					case "_code": return _code;
					case "_name": return _name;
					case "_displayOrder": return _displayOrder;
					case "_companyHolder": return _companyHolder;
					case "_reportTypeHolder": return _reportTypeHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_companyID": _companyID = (Guid)value; break;
					case "_code": _code = (string)value; break;
					case "_name": _name = (string)value; break;
					case "_displayOrder": _displayOrder = (int)value; break;
					case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
					case "_reportTypeHolder": _reportTypeHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="companyID">CompanyID of the entity to fetch.</param>
		/// <param name="reportTypeCode">ReportTypeCode of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static ReportTypeCompany Get(Guid companyID, string reportTypeCode)
		{
			OPathQuery query = new OPathQuery(typeof(ReportTypeCompany), "CompanyID = ? && ReportTypeCode = ?");
			object entity = DB.Engine.GetObject(query, companyID, reportTypeCode);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch ReportTypeCompany entity with CompanyID = '{0}' and ReportTypeCode = '{1}'.", companyID, reportTypeCode));
			}
			return (ReportTypeCompany)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static ReportTypeCompany GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ReportTypeCompany), opathExpression);
			return (ReportTypeCompany)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static ReportTypeCompany GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(ReportTypeCompany) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (ReportTypeCompany)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ReportTypeCompany[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ReportTypeCompany), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ReportTypeCompany[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(ReportTypeCompany) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (ReportTypeCompany[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static ReportTypeCompany[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(ReportTypeCompany), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_companyID = Guid.Empty;
			_code = null;
			_name = null;
			_displayOrder = Int32.MinValue;
			_companyHolder = null;
			_reportTypeHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the ReportTypeCompany entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the CompanyID field.
			/// </summary>
			CompanyID,
			/// <summary>
			/// Represents the ReportTypeCode field.
			/// </summary>
			Code,
			/// <summary>
			/// Represents the ReportTypeName field.
			/// </summary>
			Name,
			/// <summary>
			/// Represents the DisplayOrder field.
			/// </summary>
			DisplayOrder,
		}
		
		#endregion
	}
}