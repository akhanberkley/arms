using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a SurveyType entity, which maps to table 'SurveyType' in the database.
    /// </summary>
    public class SurveyType : IObjectHelper
    {
        /// <summary>
        /// None.
        /// </summary>
        public const int SETTING_ALL = 0;
        /// <summary>
        /// Survey Request.
        /// </summary>
        public const int SETTING_SURVEY_REQUEST = 1;
        /// <summary>
        /// Service Visit.
        /// </summary>
        public const int SETTING_SERVICE_VISIT = 2;
        /// <summary>
        /// Future Visit.
        /// </summary>
        public const int SETTING_FUTURE_VISIT = 3;
        /// <summary>
        /// Survey Request and Service Visit.
        /// </summary>
        public const int SETTING_SURVEY_REQUEST_AND_SERVICE_VISIT = 4;
        /// <summary>
        /// Survey Request and Future Visit.
        /// </summary>
        public const int SETTING_SURVEY_REQUEST_AND_FUTURE_VISIT = 5;
        /// <summary>
        /// Service Visit and Future Visit.
        /// </summary>
        public const int SETTING_SERVICE_VISIT_AND_FUTURE_VISIT = 6;
            
        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private SurveyType()
        {
        }

        /// <summary>
        /// Returns an array of survey types for survey requests.
        public static SurveyType[] GetRequestSurveyTypes(Company company)
        {
            ArrayList args = new ArrayList();
            args.Add(company.ID);
            args.Add(SETTING_ALL);
            args.Add(SETTING_SURVEY_REQUEST);
            args.Add(SETTING_SURVEY_REQUEST_AND_SERVICE_VISIT);
            args.Add(SETTING_SURVEY_REQUEST_AND_FUTURE_VISIT);

            return SurveyType.GetSortedArray("CompanyID = ? && (Setting = ? || Setting = ? || Setting = ? || Setting = ?) && Disabled = False", "DisplayOrder ASC", args.ToArray());
        }

        /// <summary>
        /// Returns an array of survey types for service visits.
        public static SurveyType[] GetServiceSurveyTypes(Company company)
        {
            ArrayList args = new ArrayList();
            args.Add(company.ID);
            args.Add(SETTING_ALL);
            args.Add(SETTING_SERVICE_VISIT);
            args.Add(SETTING_SURVEY_REQUEST_AND_SERVICE_VISIT);
            args.Add(SETTING_SERVICE_VISIT_AND_FUTURE_VISIT);

            return SurveyType.GetSortedArray("CompanyID = ? && (Setting = ? || Setting = ? || Setting = ? || Setting = ?) && Disabled = False", "DisplayOrder ASC", args.ToArray());
        }

        /// <summary>
        /// Returns an array of survey types for future visits.
        public static SurveyType[] GetFutureSurveyTypes(Company company)
        {
            ArrayList args = new ArrayList();
            args.Add(company.ID);
            args.Add(SETTING_ALL);
            args.Add(SETTING_FUTURE_VISIT);
            args.Add(SETTING_SURVEY_REQUEST_AND_FUTURE_VISIT);
            args.Add(SETTING_SERVICE_VISIT_AND_FUTURE_VISIT);

            return SurveyType.GetSortedArray("CompanyID = ? && (Setting = ? || Setting = ? || Setting = ? || Setting = ?) && Disabled = False", "DisplayOrder ASC", args.ToArray());
        }

        /// <summary>
        /// Gets the SurveyTypeID based on the type code and company.
        /// </summary>
        public static Guid GetOne(string surveyTypeCode, Company company)
        {
            SurveyType result = SurveyType.GetOne("TypeCode = ? && CompanyID = ?", surveyTypeCode, company.ID);
            return result.ID;
        }


        #region --- Generated Members ---

        private Guid _id;
        private Guid _companyID;
        private string _typeCode;
        private int _displayOrder;
        private int _setting;
        private bool _disabled;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _surveyTypeTypeHolder = null;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Survey Type.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets the Survey Type Type.
        /// </summary>
        public string TypeCode
        {
            get { return _typeCode; }
        }

        /// <summary>
        /// Gets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
        }

        /// <summary>
        /// Gets the Setting.
        /// </summary>
        public int Setting
        {
            get { return _setting; }
        }

        /// <summary>
        /// Gets the Disabled.
        /// </summary>
        public bool Disabled
        {
            get { return _disabled; }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a SurveyTypeType object related to this entity.
        /// </summary>
        public SurveyTypeType Type
        {
            get
            {
                _surveyTypeTypeHolder.Key = _typeCode;
                return (SurveyTypeType)_surveyTypeTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_companyID": return _companyID;
                    case "_typeCode": return _typeCode;
                    case "_displayOrder": return _displayOrder;
                    case "_setting": return _setting;
                    case "_disabled": return _disabled;
                    case "_companyHolder": return _companyHolder;
                    case "_surveyTypeTypeHolder": return _surveyTypeTypeHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_typeCode": _typeCode = (string)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    case "_setting": _setting = (int)value; break;
                    case "_disabled": _disabled = (bool)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_surveyTypeTypeHolder": _surveyTypeTypeHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static SurveyType Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyType), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch SurveyType entity with ID = '{0}'.", id));
            }
            return (SurveyType)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyType GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyType), opathExpression);
            return (SurveyType)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static SurveyType GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyType))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyType)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyType[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyType), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyType[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(SurveyType))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (SurveyType[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static SurveyType[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(SurveyType), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _companyID = Guid.Empty;
            _typeCode = null;
            _displayOrder = Int32.MinValue;
            _setting = Int32.MinValue;
            _disabled = false;
            _companyHolder = null;
            _surveyTypeTypeHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the SurveyType entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the TypeCode field.
            /// </summary>
            TypeCode,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
            /// <summary>
            /// Represents the Setting field.
            /// </summary>
            Setting,
            /// <summary>
            /// Represents the Disabled field.
            /// </summary>
            Disabled,
        }

        #endregion
    }
}