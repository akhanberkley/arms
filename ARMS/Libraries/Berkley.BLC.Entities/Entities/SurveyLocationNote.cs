using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a SurveyLocationNote entity, which maps to table 'Survey_LocationNote' in the database.
	/// </summary>
	public class SurveyLocationNote : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private SurveyLocationNote()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="surveyID">SurveyID of this SurveyLocationNote.</param>
		/// <param name="locationID">LocationID of this SurveyLocationNote.</param>
		public SurveyLocationNote(Guid surveyID, Guid locationID)
		{
			_surveyID = surveyID;
			_locationID = locationID;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _surveyID;
		private Guid _locationID;
		private Guid _userID = Guid.Empty;
        private string _userName = String.Empty;
		private DateTime _entryDate;
		private string _comment;
		private ObjectHolder _locationHolder = null;
		private ObjectHolder _surveyHolder = null;
		private ObjectHolder _userHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Survey.
		/// </summary>
		public Guid SurveyID
		{
			get { return _surveyID; }
		}

		/// <summary>
		/// Gets the Location.
		/// </summary>
		public Guid LocationID
		{
			get { return _locationID; }
		}

		/// <summary>
		/// Gets or sets the User. Null value is 'Guid.Empty'.
		/// </summary>
		public Guid UserID
		{
			get { return _userID; }
			set
			{
				VerifyWritable();
				_userID = value;
			}
		}

        /// <summary>
        /// Gets or sets the User Name. Null value is 'String.Empty'.
        /// </summary>
        public string UserName
        {
            get { return _userName; }
            set
            {
                VerifyWritable();
                _userName = value;
            }
        }

		/// <summary>
		/// Gets or sets the Entry Date.
		/// </summary>
		public DateTime EntryDate
		{
			get { return _entryDate; }
			set
			{
				VerifyWritable();
				_entryDate = value;
			}
		}

		/// <summary>
		/// Gets or sets the Comment.
		/// </summary>
		public string Comment
		{
			get { return _comment; }
			set
			{
				VerifyWritable();
				_comment = value;
			}
		}

		/// <summary>
		/// Gets the instance of a Location object related to this entity.
		/// </summary>
		public Location Location
		{
			get
			{
				_locationHolder.Key = _locationID;
				return (Location)_locationHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Survey object related to this entity.
		/// </summary>
		public Survey Survey
		{
			get
			{
				_surveyHolder.Key = _surveyID;
				return (Survey)_surveyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a User object related to this entity.
		/// </summary>
		public User User
		{
			get
			{
				_userHolder.Key = _userID;
				return (User)_userHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_surveyID": return _surveyID;
					case "_locationID": return _locationID;
					case "_userID": return _userID;
                    case "_userName": return _userName;
					case "_entryDate": return _entryDate;
					case "_comment": return _comment;
					case "_locationHolder": return _locationHolder;
					case "_surveyHolder": return _surveyHolder;
					case "_userHolder": return _userHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_surveyID": _surveyID = (Guid)value; break;
					case "_locationID": _locationID = (Guid)value; break;
					case "_userID": _userID = (Guid)value; break;
                    case "_userName": _userName = (string)value; break;
					case "_entryDate": _entryDate = (DateTime)value; break;
					case "_comment": _comment = (string)value; break;
					case "_locationHolder": _locationHolder = (ObjectHolder)value; break;
					case "_surveyHolder": _surveyHolder = (ObjectHolder)value; break;
					case "_userHolder": _userHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="surveyID">SurveyID of the entity to fetch.</param>
		/// <param name="locationID">LocationID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static SurveyLocationNote Get(Guid surveyID, Guid locationID)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyLocationNote), "SurveyID = ? && LocationID = ?");
			object entity = DB.Engine.GetObject(query, surveyID, locationID);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch SurveyLocationNote entity with SurveyID = '{0}' and LocationID = '{1}'.", surveyID, locationID));
			}
			return (SurveyLocationNote)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyLocationNote GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyLocationNote), opathExpression);
			return (SurveyLocationNote)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static SurveyLocationNote GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyLocationNote) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyLocationNote)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyLocationNote[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyLocationNote), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyLocationNote[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(SurveyLocationNote) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (SurveyLocationNote[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static SurveyLocationNote[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(SurveyLocationNote), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public SurveyLocationNote GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			SurveyLocationNote clone = (SurveyLocationNote)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.SurveyID, _surveyID);
			Validator.Validate(Field.LocationID, _locationID);
			Validator.Validate(Field.UserID, _userID);
            Validator.Validate(Field.UserName, _userName);
			Validator.Validate(Field.EntryDate, _entryDate);
			Validator.Validate(Field.Comment, _comment);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_surveyID = Guid.Empty;
			_locationID = Guid.Empty;
			_userID = Guid.Empty;
            _userName = null;
			_entryDate = DateTime.MinValue;
			_comment = null;
			_locationHolder = null;
			_surveyHolder = null;
			_userHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the SurveyLocationNote entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the SurveyID field.
			/// </summary>
			SurveyID,
			/// <summary>
			/// Represents the LocationID field.
			/// </summary>
			LocationID,
			/// <summary>
			/// Represents the UserID field.
			/// </summary>
			UserID,
            /// <summary>
            /// Represents the UserName field.
            /// </summary>
            UserName,
			/// <summary>
			/// Represents the EntryDate field.
			/// </summary>
			EntryDate,
			/// <summary>
			/// Represents the Comment field.
			/// </summary>
			Comment,
		}
		
		#endregion
	}
}