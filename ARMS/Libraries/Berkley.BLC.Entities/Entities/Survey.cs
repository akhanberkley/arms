using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents a Survey entity, which maps to table 'Survey' in the database.
    /// </summary>
    public class Survey : IObjectHelper
    {
        private Guid _originalAssignedUserID = Guid.Empty;
        private string _originalStatusCode = null;

        /// <summary>
        /// Private constructor for use by the ObjectSpace engine.
        /// </summary>
        private Survey()
        {
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="id">ID of this Survey.</param>
        public Survey(Guid id)
        {
            _id = id;

            _isReadOnly = false;

            DB.Engine.StartTracking(this, InitialState.Inserted);
        }

        /// <summary>
        /// Gets the html representation of the consultant name.
        /// </summary>
        public string HtmlRecommendedConsultant
        {
            get
            {
                if (this.StatusCode == SurveyStatus.OnHoldSurvey.Code && this.RecommendedUser != null)
                {
                    return this.RecommendedUser.Name;
                }
                else
                {
                    return "(not specified)";
                }
            }
        }

        /// <summary>
        /// Gets the html representation of the consultant name.
        /// </summary>
        public string HtmlConsultant
        {
            get
            {
                if (this.StatusCode == SurveyStatus.AssignedSurvey.Code ||
                    this.StatusCode == SurveyStatus.AwaitingAcceptance.Code ||
                    this.StatusCode == SurveyStatus.ReturningToCompany.Code)
                {
                    return AssignedUser.Name;
                }
                else
                {
                    if (ConsultantUser != null && ConsultantUser.Name.Length > 0)
                    {
                        return ConsultantUser.Name;
                    }
                    else
                    {
                        return "(not specified)";
                    }
                }
            }
        }

        /// <summary>
        /// Gets the Consultant name that can be presented to an end-user in a datagrid.
        /// </summary>
        public string HtmlConsultantGrid
        {
            get
            {
                if (this.StatusCode == SurveyStatus.AssignedSurvey.Code ||
                    this.StatusCode == SurveyStatus.AwaitingAcceptance.Code ||
                    this.StatusCode == SurveyStatus.ReturningToCompany.Code)
                {
                    return AssignedUser.Name;
                }
                else
                {
                    if (ConsultantUser != null && ConsultantUser.Name.Length > 0)
                    {
                        return ConsultantUser.Name;
                    }
                    else
                    {
                        return "(none)";
                    }
                }
            }
        }

        /// <summary>
        /// Gets the html representation of the createdBy user name.
        /// </summary>
        public string HtmlCreatedBy
        {
            get
            {
                if (this.CreateByInternalUserID != Guid.Empty)
                {
                    return this.CreateByInternalUser.Name;
                }
                else if (this.CreateByExternalUserName != string.Empty)
                {
                    return this.CreateByExternalUserName;
                }
                else
                {
                    return "(not specified)";
                }
            }
        }

        /// <summary>
        /// Gets the Underwriter name from the lookup table, default will display (not specified).
        /// </summary>
        public string HtmlUnderwriterName
        {
            get
            {
                if (this.UnderwriterCode.Length > 0)
                {

                    BLC.Entities.Underwriter uw = BLC.Entities.Underwriter.GetOne("Code = ? && CompanyID = ?", this.UnderwriterCode, this.CompanyID);
                    return (uw != null) ? uw.Name : this.UnderwriterCode;
                }
                else
                {
                    return "(not specified)";
                }
            }
        }

             
        /// <summary>
        /// Gets the Underwriter name from the lookup table, default will display (not specified).
        /// </summary>
        public string HtmlUnderwriterPhone
        {
            get
            {
                BLC.Entities.Underwriter uw = BLC.Entities.Underwriter.GetOne("Code = ? && CompanyID = ?", this.UnderwriterCode, this.CompanyID);
                if (uw != null)
                {
                    return Formatter.Phone(uw.PhoneNumber, uw.PhoneExt, "(not specified)");
                }
                else
                {
                    return "(not specified)";
                }
            }
        }

        /// <summary>
        /// Gets the Underwriter name from the lookup table, default will display (not specified).
        /// </summary>
        public string HtmlUnderwriter2Name
        {
            get
            {
                if (this.Underwriter2Code.Length > 0)
                {

                    BLC.Entities.Underwriter uw2 = BLC.Entities.Underwriter.GetOne("Code = ? && CompanyID = ?", this.Underwriter2Code, this.CompanyID);
                    return (uw2 != null) ? uw2.Name : this.Underwriter2Code;
                }
                else
                {
                    return "(not specified)";
                }
            }
        }


        /// <summary>
        /// Gets the Underwriter name from the lookup table, default will display (not specified).
        /// </summary>
        public string HtmlUnderwriter2Phone
        {
            get
            {
                BLC.Entities.Underwriter uw2 = BLC.Entities.Underwriter.GetOne("Code = ? && CompanyID = ?", this.Underwriter2Code, this.CompanyID);
                if (uw2 != null)
                {
                    return Formatter.Phone(uw2.PhoneNumber, uw2.PhoneExt, "(not specified)");
                }
                else
                {
                    return "(not specified)";
                }
            }
        }


        /// <summary>
        /// Gets the TotalActivityHours from lookup table, default will display (not specified).
        /// </summary>
        public decimal TotalActivityHours
        {
            get
            {
                BLC.Entities.SurveyActivity[] activities = BLC.Entities.SurveyActivity.GetArray("SurveyID = ?", this.ID);

                if (activities != null && activities.Length > 0)
                {
                    decimal total = 0;
                    foreach (SurveyActivity s in activities)
                    {
                        total += s.ActivityHours;
                    }

                    return total;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets all the previous survey(s) tied to this survey.
        /// </summary>
        public Survey[] GetPreviousSurveys
        {
            get
            {
                Hashtable list = new Hashtable();
                list.Add(ID, this);

                //First, try traversing down.
                if (this.PreviousSurvey != null)
                {
                    Survey downPrevious = this.PreviousSurvey;
                    Survey downSurvey = null;

                    //Loop looking for previous surveys
                    do
                    {
                        if (!list.Contains(downPrevious.ID))
                        {
                            list.Add(downPrevious.ID, downPrevious);
                            downSurvey = Survey.Get(downPrevious.ID);
                            downPrevious = downSurvey.PreviousSurvey;
                        }
                    }
                    while (downPrevious != null);
                }

                //Second, try traversing up.
                Survey survey = Survey.GetOne("PreviousSurveyID = ?", this.ID);
                if (survey != null)
                {
                    Survey upSurvey = survey;

                    //Loop looking for previous surveys
                    do
                    {
                        if (!list.Contains(upSurvey.ID))
                        {
                            list.Add(upSurvey.ID, upSurvey);
                            upSurvey = Survey.GetOne("PreviousSurveyID = ?", upSurvey.ID);
                        }
                    }
                    while (upSurvey != null);
                }

                Survey[] surveys = new Survey[list.Count];
                list.Values.CopyTo(surveys, 0);

                return surveys;
            }
        }


        /// <summary>
        /// Gets all the surveys for all the inter-related surveys
        /// </summary>
        /// <returns>Comma delimited string of survey ids.</returns>
        public string GetAllSurveysForAccount
        {
            get
            {
                // get all surveys recs for all the interconnected survey requests
                // or all the service visits recs for the entire service plan
                List<Survey> surveys = new List<Survey>();
                if (this.ServiceType == ServiceType.ServiceVisit)
                {
                    ServicePlan plan = ServicePlan.GetOne("Surveys[SurveyID = ?]", this.ID);
                    if (plan != null)
                    {
                        ServicePlan[] servicePlans = plan.GetPreviousServicePlans;
                        foreach (ServicePlan servicePlan in servicePlans)
                        {
                            surveys.AddRange(Survey.GetArray("CompanyID = ? && CreateStateCode != ? && CreateStateCode != ? && LocationID = ? && ServicePlanSurveys[ServicePlanID = ?]",
                                    this.CompanyID,
                                    CreateState.InProgress.Code,
                                    CreateState.ErrorCreating.Code,
                                    this.LocationID,
                                    servicePlan.ID));
                        }
                    }
                    else
                    {
                        surveys.AddRange(this.GetPreviousSurveys);
                    }
                }
                else
                {
                    surveys.AddRange(this.GetPreviousSurveys);
                }

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < surveys.Count; i++)
                {
                    sb.AppendFormat("SurveyID = '{0}'", surveys[i].ID);

                    if (i != (surveys.Count - 1))
                    {
                        sb.Append(" || ");
                    }
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// Gets the ID of the user assinged this Survey when loaded from the database, or last persisted.
        /// </summary>
        public Guid OriginalAssignedUserID
        {
            get { return _originalAssignedUserID; }
        }

        /// <summary>
        /// Gets the status code this Survey had when loaded from the database, or last persisted.
        /// </summary>
        public string OriginalStatusCode
        {
            get { return _originalStatusCode; }
        }

        /// <summary>
        /// Validates this instance.
        /// An exception of type ValidationException is thrown if validation fails.
        /// </summary>
        public void Validate()
        {
            ValidateFields();

            //TODO: add additional validation logic here.
        }


        #region --- Generated Members ---

        private Guid _id;
        private int _number;
        private Guid _companyID;
        private Guid _policysystemID;
        private Guid _insuredID;
        private Guid _locationID = Guid.Empty;
        private Guid _primaryPolicyID = Guid.Empty;
        private Guid _previousSurveyID = Guid.Empty;
        private Guid _typeID;
        private Guid _transactionTypeID = Guid.Empty;
        private string _serviceTypeCode;
        private string _statusCode;
        private Guid _qualityID = Guid.Empty;
        private Guid _assignedByUserID = Guid.Empty;
        private Guid _assignedUserID = Guid.Empty;
        private Guid _recommendedUserID = Guid.Empty;
        private string _createStateCode;
        private DateTime _createDate;
        private Guid _createByInternalUserID = Guid.Empty;
        private string _createByExternalUserName = String.Empty;
        private DateTime _assignDate = DateTime.MinValue;
        private DateTime _reassignDate = DateTime.MinValue;
        private DateTime _acceptDate = DateTime.MinValue;
        private DateTime _surveyedDate = DateTime.MinValue;
        private DateTime _reportCompleteDate = DateTime.MinValue;
        private DateTime _dueDate = DateTime.MinValue;
        private DateTime _completeDate = DateTime.MinValue;
        private DateTime _canceledDate = DateTime.MinValue;
        private DateTime _underwritingAcceptDate = DateTime.MinValue;
        private DateTime _reminderDate = DateTime.MinValue;
        private string _reminderComment = String.Empty;
        private DateTime _mailReceivedDate = DateTime.MinValue;
        private bool _correction = false;
        private bool _nonProductive = false;
        private bool _review = false;
        private bool _nonDisclosureRequired = false;
        private decimal _hours = Decimal.MinValue;
        private decimal _callCount = Decimal.MinValue;
        private string _gradingCode = String.Empty;
        private string _severityRatingCode = String.Empty;
        private bool _recsRequired = false;
        private bool _uwNotified = false;
        private bool _displayClaims = false;
        private decimal _feeConsultantCost = Decimal.MinValue;
        private string _invoiceNumber = String.Empty;
        private DateTime _invoiceDate = DateTime.MinValue;
        private bool _futureVisitNeeded = false;
        private Guid _futureVisitSurveyTypeID = Guid.Empty;
        private DateTime _futureVisitDueDate = DateTime.MinValue;
        private Guid _consultantUserID = Guid.Empty;
        private Guid _reviewerUserID = Guid.Empty;
        private string _workflowSubmissionNumber = String.Empty;
        private string _workflowReference = String.Empty;
        private bool _underwriterNotified = false;
        private string _underwriterCode = String.Empty;
        private string _underwriter2Code = String.Empty;
        private string _legacySystemID = String.Empty;
        private DateTime _lastRefreshDate = DateTime.MinValue;
        private DateTime _cancelRefreshDate = DateTime.MinValue;
        private DateTime _lastModifiedOn = DateTime.MinValue;
        private string _priority = String.Empty;
        private bool _inHouseSelection;
        private string _approvalCode = String.Empty;
        private DateTime _reviewerCompleteDate = DateTime.MinValue;
        private bool _containsCriticalRec = false;
        private bool _containsOpenCriticalRec = false;
        private ObjectHolder _assignedByUserHolder = null;
        private ObjectHolder _assignedUserHolder = null;
        private ObjectHolder _companyHolder = null;
        private ObjectHolder _consultantUserHolder = null;
        private ObjectHolder _createByInternalUserHolder = null;
        private ObjectHolder _createStateHolder = null;
        private ObjectHolder _futureVisitSurveyTypeHolder = null;
        private ObjectHolder _insuredHolder = null;
        private ObjectHolder _locationHolder = null;
        private ObjectHolder _previousSurveyHolder = null;
        private ObjectHolder _primaryPolicyHolder = null;
        private ObjectHolder _recommendedUserHolder = null;
        private ObjectHolder _reviewerUserHolder = null;
        private ObjectHolder _serviceTypeHolder = null;
        private ObjectHolder _severityRatingHolder = null;
        private ObjectHolder _surveyGradingHolder = null;
        private ObjectHolder _surveyQualityHolder = null;
        private ObjectHolder _surveyStatusHolder = null;
        private ObjectHolder _surveyTypeHolder = null;
        private ObjectHolder _transactionTypeHolder = null;
        private IList _servicePlanList = null;
        private IList _surveyReasonTypeList = null;
        private bool _isReadOnly = true;
        private DateTime _requestedDate = DateTime.MinValue;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Survey ID.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets the Survey Number.
        /// </summary>
        public int Number
        {
            get { return _number; }
        }

        /// <summary>
        /// Gets or sets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
            set
            {
                VerifyWritable();
                _companyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the PolicySystemID
        /// </summary>
        public Guid PolicySystemID
        {
            get { return _policysystemID; }
            set
            {
                VerifyWritable();
                _policysystemID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Insured.
        /// </summary>
        public Guid InsuredID
        {
            get { return _insuredID; }
            set
            {
                VerifyWritable();
                _insuredID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Location. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid LocationID
        {
            get { return _locationID; }
            set
            {
                VerifyWritable();
                _locationID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Primary Policy. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid PrimaryPolicyID
        {
            get { return _primaryPolicyID; }
            set
            {
                VerifyWritable();
                _primaryPolicyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Previous Survey. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid PreviousSurveyID
        {
            get { return _previousSurveyID; }
            set
            {
                VerifyWritable();
                _previousSurveyID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Survey Type.
        /// </summary>
        public Guid TypeID
        {
            get { return _typeID; }
            set
            {
                VerifyWritable();
                _typeID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Transaction Type. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid TransactionTypeID
        {
            get { return _transactionTypeID; }
            set
            {
                VerifyWritable();
                _transactionTypeID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Service Type.
        /// </summary>
        public string ServiceTypeCode
        {
            get { return _serviceTypeCode; }
            set
            {
                VerifyWritable();
                _serviceTypeCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Survey Status.
        /// </summary>
        public string StatusCode
        {
            get { return _statusCode; }
            set
            {
                VerifyWritable();
                _statusCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Survey Quality. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid QualityID
        {
            get { return _qualityID; }
            set
            {
                VerifyWritable();
                _qualityID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Assigned By User ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid AssignedByUserID
        {
            get { return _assignedByUserID; }
            set
            {
                VerifyWritable();
                _assignedByUserID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Assigned User. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid AssignedUserID
        {
            get { return _assignedUserID; }
            set
            {
                VerifyWritable();
                _assignedUserID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Recommended User. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid RecommendedUserID
        {
            get { return _recommendedUserID; }
            set
            {
                VerifyWritable();
                _recommendedUserID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Create State.
        /// </summary>
        public string CreateStateCode
        {
            get { return _createStateCode; }
            set
            {
                VerifyWritable();
                _createStateCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Create Date.
        /// </summary>
        public DateTime CreateDate
        {
            get { return _createDate; }
            set
            {
                VerifyWritable();
                _createDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Create By Internal User. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid CreateByInternalUserID
        {
            get { return _createByInternalUserID; }
            set
            {
                VerifyWritable();
                _createByInternalUserID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Create By External User Name. Null value is 'String.Empty'.
        /// </summary>
        public string CreateByExternalUserName
        {
            get { return _createByExternalUserName; }
            set
            {
                VerifyWritable();
                _createByExternalUserName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Assign Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime AssignDate
        {
            get { return _assignDate; }
            set
            {
                VerifyWritable();
                _assignDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Reassign Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime ReassignDate
        {
            get { return _reassignDate; }
            set
            {
                VerifyWritable();
                _reassignDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Accept Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime AcceptDate
        {
            get { return _acceptDate; }
            set
            {
                VerifyWritable();
                _acceptDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Surveyed Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime SurveyedDate
        {
            get { return _surveyedDate; }
            set
            {
                VerifyWritable();
                _surveyedDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Report Complete Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime ReportCompleteDate
        {
            get { return _reportCompleteDate; }
            set
            {
                VerifyWritable();
                _reportCompleteDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Due Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime DueDate
        {
            get { return _dueDate; }
            set
            {
                VerifyWritable();
                _dueDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Complete Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime CompleteDate
        {
            get { return _completeDate; }
            set
            {
                VerifyWritable();
                _completeDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Canceled Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime CanceledDate
        {
            get { return _canceledDate; }
            set
            {
                VerifyWritable();
                _canceledDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Underwriting Accept Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime UnderwritingAcceptDate
        {
            get { return _underwritingAcceptDate; }
            set
            {
                VerifyWritable();
                _underwritingAcceptDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Reminder Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime ReminderDate
        {
            get { return _reminderDate; }
            set
            {
                VerifyWritable();
                _reminderDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Reminder Comment. Null value is 'String.Empty'.
        /// </summary>
        public string ReminderComment
        {
            get { return _reminderComment; }
            set
            {
                VerifyWritable();
                _reminderComment = value;
            }
        }

        /// <summary>
        /// Gets or sets the Mail Received Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime MailReceivedDate
        {
            get { return _mailReceivedDate; }
            set
            {
                VerifyWritable();
                _mailReceivedDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Correction. Null value is 'false'.
        /// </summary>
        public bool Correction
        {
            get { return _correction; }
            set
            {
                VerifyWritable();
                _correction = value;
            }
        }

        /// <summary>
        /// Gets or sets the Non Productive. Null value is 'false'.
        /// </summary>
        public bool NonProductive
        {
            get { return _nonProductive; }
            set
            {
                VerifyWritable();
                _nonProductive = value;
            }
        }

        /// <summary>
        /// Gets or sets the Review. Null value is 'false'.
        /// </summary>
        public bool Review
        {
            get { return _review; }
            set
            {
                VerifyWritable();
                _review = value;
            }
        }

        /// <summary>
        /// Gets or sets the Non Disclosure Required. Null value is 'false'.
        /// </summary>
        public bool NonDisclosureRequired
        {
            get { return _nonDisclosureRequired; }
            set
            {
                VerifyWritable();
                _nonDisclosureRequired = value;
            }
        }

        /// <summary>
        /// Gets or sets the Survey Hours. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal Hours
        {
            get { return _hours; }
            set
            {
                VerifyWritable();
                _hours = value;
            }
        }

        /// <summary>
        /// Gets or sets the Call Count. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal CallCount
        {
            get { return _callCount; }
            set
            {
                VerifyWritable();
                _callCount = value;
            }
        }

        /// <summary>
        /// Gets or sets the Survey Grading. Null value is 'String.Empty'.
        /// </summary>
        public string GradingCode
        {
            get { return _gradingCode; }
            set
            {
                VerifyWritable();
                _gradingCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Severity Rating. Null value is 'String.Empty'.
        /// </summary>
        public string SeverityRatingCode
        {
            get { return _severityRatingCode; }
            set
            {
                VerifyWritable();
                _severityRatingCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Recs Required. Null value is 'false'.
        /// </summary>
        public bool RecsRequired
        {
            get { return _recsRequired; }
            set
            {
                VerifyWritable();
                _recsRequired = value;
            }
        }

        /// <summary>
        /// Gets or sets the Underwriter Notified. Null value is 'false'.
        /// </summary>
        public bool UWNotified
        {
            get { return _uwNotified; }
            set
            {
                VerifyWritable();
                _uwNotified = value;
            }
        }


        /// <summary>
        /// Gets or sets the Do Not Display Claims. Null value is 'false'.
        /// </summary>
        public bool DisplayClaims
        {
            get { return _displayClaims; }
            set
            {
                VerifyWritable();
                _displayClaims = value;
            }
        }

        /// <summary>
        /// Gets or sets the Fee Consultant Cost. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal FeeConsultantCost
        {
            get { return _feeConsultantCost; }
            set
            {
                VerifyWritable();
                _feeConsultantCost = value;
            }
        }

        /// <summary>
        /// Gets or sets the Invoice Number. Null value is 'String.Empty'.
        /// </summary>
        public string InvoiceNumber
        {
            get { return _invoiceNumber; }
            set
            {
                VerifyWritable();
                _invoiceNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Invoice Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime InvoiceDate
        {
            get { return _invoiceDate; }
            set
            {
                VerifyWritable();
                _invoiceDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Future Visit Needed. Null value is 'false'.
        /// </summary>
        public bool FutureVisitNeeded
        {
            get { return _futureVisitNeeded; }
            set
            {
                VerifyWritable();
                _futureVisitNeeded = value;
            }
        }

        /// <summary>
        /// Gets or sets the Future Visit Survey Type. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid FutureVisitSurveyTypeID
        {
            get { return _futureVisitSurveyTypeID; }
            set
            {
                VerifyWritable();
                _futureVisitSurveyTypeID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Future Visit Due Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime FutureVisitDueDate
        {
            get { return _futureVisitDueDate; }
            set
            {
                VerifyWritable();
                _futureVisitDueDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Consultant User. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid ConsultantUserID
        {
            get { return _consultantUserID; }
            set
            {
                VerifyWritable();
                _consultantUserID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Reviewer User. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid ReviewerUserID
        {
            get { return _reviewerUserID; }
            set
            {
                VerifyWritable();
                _reviewerUserID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Workflow Submission Number. Null value is 'String.Empty'.
        /// </summary>
        public string WorkflowSubmissionNumber
        {
            get { return _workflowSubmissionNumber; }
            set
            {
                VerifyWritable();
                _workflowSubmissionNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Workflow Reference. Null value is 'String.Empty'.
        /// </summary>
        public string WorkflowReference
        {
            get { return _workflowReference; }
            set
            {
                VerifyWritable();
                _workflowReference = value;
            }
        }

        /// <summary>
        /// Gets or sets the Underwriter Notified. Null value is 'false'.
        /// </summary>
        public bool UnderwriterNotified
        {
            get { return _underwriterNotified; }
            set
            {
                VerifyWritable();
                _underwriterNotified = value;
            }
        }

        /// <summary>
        /// Gets or sets the Underwriter Code. Null value is 'String.Empty'.
        /// </summary>
        public string UnderwriterCode
        {
            get { return _underwriterCode; }
            set
            {
                VerifyWritable();
                _underwriterCode = value;
            }
        }
        /// <summary>
        /// Gets or sets the Underwriter Secondary Code. Null value is 'String.Empty'.
        /// </summary>
        public string Underwriter2Code
        {
            get { return _underwriter2Code; }
            set
            {
                VerifyWritable();
                _underwriter2Code = value;
            }
        }
        /// <summary>
        /// Gets or sets the Legacy System ID. Null value is 'String.Empty'.
        /// </summary>
        public string LegacySystemID
        {
            get { return _legacySystemID; }
            set
            {
                VerifyWritable();
                _legacySystemID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Last Refresh Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime LastRefreshDate
        {
            get { return _lastRefreshDate; }
            set
            {
                VerifyWritable();
                _lastRefreshDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Cancel Refresh Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime CancelRefreshDate
        {
            get { return _cancelRefreshDate; }
            set
            {
                VerifyWritable();
                _cancelRefreshDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Last Modified On. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime LastModifiedOn
        {
            get { return _lastModifiedOn; }
            set
            {
                VerifyWritable();
                _lastModifiedOn = value;
            }
        }

        /// <summary>
        /// Gets or sets the Priority. Null value is 'String.Empty'.
        /// </summary>
        public string Priority
        {
            get { return _priority; }
            set
            {
                VerifyWritable();
                _priority = value;
            }
        }

        /// <summary>
        /// Gets or sets the In House Selection.
        /// </summary>
        public bool InHouseSelection
        {
            get { return _inHouseSelection; }
            set
            {
                VerifyWritable();
                _inHouseSelection = value;
            }
        }

        /// <summary>
        /// Gets or sets the Approval Code. Null value is 'String.Empty'.
        /// </summary>
        public string ApprovalCode
        {
            get { return _approvalCode; }
            set
            {
                VerifyWritable();
                _approvalCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the Contains Critical Rec.
        /// </summary>
        public bool ContainsCriticalRec
        {
            get { return _containsCriticalRec; }
            set
            {
                VerifyWritable();
                _containsCriticalRec = value;
            }
        }

        /// <summary>
        /// Gets or sets the Contains Critical Rec.
        /// </summary>
        public bool ContainsOpenCriticalRec
        {
            get { return _containsOpenCriticalRec; }
            set
            {
                VerifyWritable();
                _containsOpenCriticalRec = value;
            }
        }
        /// <summary>
        /// Gets or sets the Tech Complete Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime ReviewerCompleteDate
        {
            get { return _reviewerCompleteDate; }
            set
            {
                VerifyWritable();
                _reviewerCompleteDate = value;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User AssignedByUser
        {
            get
            {
                _assignedByUserHolder.Key = _assignedByUserID;
                return (User)_assignedByUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User AssignedUser
        {
            get
            {
                _assignedUserHolder.Key = _assignedUserID;
                return (User)_assignedUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User ConsultantUser
        {
            get
            {
                _consultantUserHolder.Key = _consultantUserID;
                return (User)_consultantUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User CreateByInternalUser
        {
            get
            {
                _createByInternalUserHolder.Key = _createByInternalUserID;
                return (User)_createByInternalUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a CreateState object related to this entity.
        /// </summary>
        public CreateState CreateState
        {
            get
            {
                _createStateHolder.Key = _createStateCode;
                return (CreateState)_createStateHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a SurveyType object related to this entity.
        /// </summary>
        public SurveyType FutureVisitSurveyType
        {
            get
            {
                _futureVisitSurveyTypeHolder.Key = _futureVisitSurveyTypeID;
                return (SurveyType)_futureVisitSurveyTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Insured object related to this entity.
        /// </summary>
        public Insured Insured
        {
            get
            {
                _insuredHolder.Key = _insuredID;
                return (Insured)_insuredHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Location object related to this entity.
        /// </summary>
        public Location Location
        {
            get
            {
                _locationHolder.Key = _locationID;
                return (Location)_locationHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Survey object related to this entity.
        /// </summary>
        public Survey PreviousSurvey
        {
            get
            {
                _previousSurveyHolder.Key = _previousSurveyID;
                return (Survey)_previousSurveyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a Policy object related to this entity.
        /// </summary>
        public Policy PrimaryPolicy
        {
            get
            {
                _primaryPolicyHolder.Key = _primaryPolicyID;
                return (Policy)_primaryPolicyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User RecommendedUser
        {
            get
            {
                _recommendedUserHolder.Key = _recommendedUserID;
                return (User)_recommendedUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a User object related to this entity.
        /// </summary>
        public User ReviewerUser
        {
            get
            {
                _reviewerUserHolder.Key = _reviewerUserID;
                return (User)_reviewerUserHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a ServiceType object related to this entity.
        /// </summary>
        public ServiceType ServiceType
        {
            get
            {
                _serviceTypeHolder.Key = _serviceTypeCode;
                return (ServiceType)_serviceTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a SeverityRating object related to this entity.
        /// </summary>
        public SeverityRating SeverityRating
        {
            get
            {
                _severityRatingHolder.Key = _severityRatingCode;
                return (SeverityRating)_severityRatingHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a SurveyGrading object related to this entity.
        /// </summary>
        public SurveyGrading Grading
        {
            get
            {
                _surveyGradingHolder.Key = _gradingCode;
                return (SurveyGrading)_surveyGradingHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a SurveyQuality object related to this entity.
        /// </summary>
        public SurveyQuality Quality
        {
            get
            {
                _surveyQualityHolder.Key = _qualityID;
                return (SurveyQuality)_surveyQualityHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a SurveyStatus object related to this entity.
        /// </summary>
        public SurveyStatus Status
        {
            get
            {
                _surveyStatusHolder.Key = _statusCode;
                return (SurveyStatus)_surveyStatusHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a SurveyType object related to this entity.
        /// </summary>
        public SurveyType Type
        {
            get
            {
                _surveyTypeHolder.Key = _typeID;
                return (SurveyType)_surveyTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the instance of a TransactionType object related to this entity.
        /// </summary>
        public TransactionType TransactionType
        {
            get
            {
                _transactionTypeHolder.Key = _transactionTypeID;
                return (TransactionType)_transactionTypeHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets the collection of ServicePlan objects related to this entity.
        /// </summary>
        public IList ServicePlans2
        {
            get
            {
                return _servicePlanList;
            }
        }

        /// <summary>
        /// Gets the collection of SurveyReasonType objects related to this entity.
        /// </summary>
        public IList ReasonTypes
        {
            get
            {
                return _surveyReasonTypeList;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
        }

        /// <summary>
        /// Gets or sets the Requested Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime RequestedDate
        {
            get { return _requestedDate; }
            set
            {
                VerifyWritable();
                _requestedDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_number": return _number;
                    case "_companyID": return _companyID;
                    case "_policysystemID": return _policysystemID;
                    case "_insuredID": return _insuredID;
                    case "_locationID": return _locationID;
                    case "_primaryPolicyID": return _primaryPolicyID;
                    case "_previousSurveyID": return _previousSurveyID;
                    case "_typeID": return _typeID;
                    case "_transactionTypeID": return _transactionTypeID;
                    case "_qualityID": return _qualityID;
                    case "_serviceTypeCode": return _serviceTypeCode;
                    case "_statusCode": return _statusCode;
                    case "_assignedByUserID": return _assignedByUserID;
                    case "_assignedUserID": return _assignedUserID;
                    case "_recommendedUserID": return _recommendedUserID;
                    case "_createStateCode": return _createStateCode;
                    case "_createDate": return _createDate;
                    case "_createByInternalUserID": return _createByInternalUserID;
                    case "_createByExternalUserName": return _createByExternalUserName;
                    case "_assignDate": return _assignDate;
                    case "_reassignDate": return _reassignDate;
                    case "_acceptDate": return _acceptDate;
                    case "_surveyedDate": return _surveyedDate;
                    case "_reportCompleteDate": return _reportCompleteDate;
                    case "_dueDate": return _dueDate;
                    case "_completeDate": return _completeDate;
                    case "_canceledDate": return _canceledDate;
                    case "_underwritingAcceptDate": return _underwritingAcceptDate;
                    case "_reminderDate": return _reminderDate;
                    case "_reminderComment": return _reminderComment;
                    case "_mailReceivedDate": return _mailReceivedDate;
                    case "_correction": return _correction;
                    case "_nonProductive": return _nonProductive;
                    case "_review": return _review;
                    case "_nonDisclosureRequired": return _nonDisclosureRequired;
                    case "_hours": return _hours;
                    case "_callCount": return _callCount;
                    case "_gradingCode": return _gradingCode;
                    case "_severityRatingCode": return _severityRatingCode;
                    case "_recsRequired": return _recsRequired;
                    case "_uwNotified": return _uwNotified;
                    case "_displayClaims": return _displayClaims;
                    case "_feeConsultantCost": return _feeConsultantCost;
                    case "_invoiceNumber": return _invoiceNumber;
                    case "_invoiceDate": return _invoiceDate;
                    case "_futureVisitNeeded": return _futureVisitNeeded;
                    case "_futureVisitSurveyTypeID": return _futureVisitSurveyTypeID;
                    case "_futureVisitDueDate": return _futureVisitDueDate;
                    case "_consultantUserID": return _consultantUserID;
                    case "_reviewerUserID": return _reviewerUserID;
                    case "_workflowSubmissionNumber": return _workflowSubmissionNumber;
                    case "_workflowReference": return _workflowReference;
                    case "_underwriterNotified": return _underwriterNotified;
                    case "_underwriterCode": return _underwriterCode;
                    case "_underwriter2Code": return _underwriter2Code;
                    case "_legacySystemID": return _legacySystemID;
                    case "_lastRefreshDate": return _lastRefreshDate;
                    case "_cancelRefreshDate": return _cancelRefreshDate;
                    case "_lastModifiedOn": return _lastModifiedOn;
                    case "_priority": return _priority;
                    case "_inHouseSelection": return _inHouseSelection;
                    case "_approvalCode": return _approvalCode;
                    case "_containsCriticalRec": return _containsCriticalRec;
                    case "_containsOpenCriticalRec": return _containsOpenCriticalRec;
                    case "_assignedByUserHolder": return _assignedByUserHolder;
                    case "_assignedUserHolder": return _assignedUserHolder;
                    case "_companyHolder": return _companyHolder;
                    case "_consultantUserHolder": return _consultantUserHolder;
                    case "_createByInternalUserHolder": return _createByInternalUserHolder;
                    case "_createStateHolder": return _createStateHolder;
                    case "_futureVisitSurveyTypeHolder": return _futureVisitSurveyTypeHolder;
                    case "_insuredHolder": return _insuredHolder;
                    case "_locationHolder": return _locationHolder;
                    case "_previousSurveyHolder": return _previousSurveyHolder;
                    case "_primaryPolicyHolder": return _primaryPolicyHolder;
                    case "_recommendedUserHolder": return _recommendedUserHolder;
                    case "_reviewerUserHolder": return _reviewerUserHolder;
                    case "_serviceTypeHolder": return _serviceTypeHolder;
                    case "_severityRatingHolder": return _severityRatingHolder;
                    case "_surveyGradingHolder": return _surveyGradingHolder;
                    case "_surveyQualityHolder": return _surveyQualityHolder;
                    case "_surveyStatusHolder": return _surveyStatusHolder;
                    case "_surveyTypeHolder": return _surveyTypeHolder;
                    case "_transactionTypeHolder": return _transactionTypeHolder;
                    case "_servicePlanList": return _servicePlanList;
                    case "_surveyReasonTypeList": return _surveyReasonTypeList;
                    case "_reviewerCompleteDate": return _reviewerCompleteDate;
                    case "_requestedDate": return _requestedDate;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_number": _number = (int)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_policysystemID": _policysystemID = (Guid)value; break;
                    case "_insuredID": _insuredID = (Guid)value; break;
                    case "_locationID": _locationID = (Guid)value; break;
                    case "_primaryPolicyID": _primaryPolicyID = (Guid)value; break;
                    case "_previousSurveyID": _previousSurveyID = (Guid)value; break;
                    case "_typeID": _typeID = (Guid)value; break;
                    case "_transactionTypeID": _transactionTypeID = (Guid)value; break;
                    case "_qualityID": _qualityID = (Guid)value; break;
                    case "_serviceTypeCode": _serviceTypeCode = (string)value; break;
                    case "_statusCode": _statusCode = (string)value; break;
                    case "_assignedByUserID": _assignedByUserID = (Guid)value; break;
                    case "_assignedUserID": _assignedUserID = (Guid)value; break;
                    case "_recommendedUserID": _recommendedUserID = (Guid)value; break;
                    case "_createStateCode": _createStateCode = (string)value; break;
                    case "_createDate": _createDate = (DateTime)value; break;
                    case "_createByInternalUserID": _createByInternalUserID = (Guid)value; break;
                    case "_createByExternalUserName": _createByExternalUserName = (string)value; break;
                    case "_assignDate": _assignDate = (DateTime)value; break;
                    case "_reassignDate": _reassignDate = (DateTime)value; break;
                    case "_acceptDate": _acceptDate = (DateTime)value; break;
                    case "_surveyedDate": _surveyedDate = (DateTime)value; break;
                    case "_reportCompleteDate": _reportCompleteDate = (DateTime)value; break;
                    case "_dueDate": _dueDate = (DateTime)value; break;
                    case "_completeDate": _completeDate = (DateTime)value; break;
                    case "_canceledDate": _canceledDate = (DateTime)value; break;
                    case "_underwritingAcceptDate": _underwritingAcceptDate = (DateTime)value; break;
                    case "_reminderDate": _reminderDate = (DateTime)value; break;
                    case "_reminderComment": _reminderComment = (string)value; break;
                    case "_mailReceivedDate": _mailReceivedDate = (DateTime)value; break;
                    case "_correction": _correction = (bool)value; break;
                    case "_nonProductive": _nonProductive = (bool)value; break;
                    case "_review": _review = (bool)value; break;
                    case "_nonDisclosureRequired": _nonDisclosureRequired = (bool)value; break;
                    case "_hours": _hours = (decimal)value; break;
                    case "_callCount": _callCount = (decimal)value; break;
                    case "_gradingCode": _gradingCode = (string)value; break;
                    case "_severityRatingCode": _severityRatingCode = (string)value; break;
                    case "_recsRequired": _recsRequired = (bool)value; break;
                    case "_uwNotified": _uwNotified = (bool)value; break;
                    case "_displayClaims": _displayClaims = (bool)value; break;
                    case "_feeConsultantCost": _feeConsultantCost = (decimal)value; break;
                    case "_invoiceNumber": _invoiceNumber = (string)value; break;
                    case "_invoiceDate": _invoiceDate = (DateTime)value; break;
                    case "_futureVisitNeeded": _futureVisitNeeded = (bool)value; break;
                    case "_futureVisitSurveyTypeID": _futureVisitSurveyTypeID = (Guid)value; break;
                    case "_futureVisitDueDate": _futureVisitDueDate = (DateTime)value; break;
                    case "_consultantUserID": _consultantUserID = (Guid)value; break;
                    case "_reviewerUserID": _reviewerUserID = (Guid)value; break;
                    case "_workflowSubmissionNumber": _workflowSubmissionNumber = (string)value; break;
                    case "_workflowReference": _workflowReference = (string)value; break;
                    case "_underwriterNotified": _underwriterNotified = (bool)value; break;
                    case "_underwriterCode": _underwriterCode = (string)value; break;
                    case "_underwriter2Code": _underwriter2Code = (string)value; break;
                    case "_legacySystemID": _legacySystemID = (string)value; break;
                    case "_lastRefreshDate": _lastRefreshDate = (DateTime)value; break;
                    case "_cancelRefreshDate": _cancelRefreshDate = (DateTime)value; break;
                    case "_lastModifiedOn": _lastModifiedOn = (DateTime)value; break;
                    case "_priority": _priority = (string)value; break;
                    case "_inHouseSelection": _inHouseSelection = (bool)value; break;
                    case "_approvalCode": _approvalCode = (string)value; break;
                    case "_containsCriticalRec": _containsCriticalRec = (bool)value; break;
                    case "_containsOpenCriticalRec": _containsOpenCriticalRec = (bool)value; break;
                    case "_reviewerCompleteDate": _reviewerCompleteDate = (DateTime)value; break;
                    case "_assignedByUserHolder": _assignedByUserHolder = (ObjectHolder)value; break;
                    case "_assignedUserHolder": _assignedUserHolder = (ObjectHolder)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    case "_consultantUserHolder": _consultantUserHolder = (ObjectHolder)value; break;
                    case "_createByInternalUserHolder": _createByInternalUserHolder = (ObjectHolder)value; break;
                    case "_createStateHolder": _createStateHolder = (ObjectHolder)value; break;
                    case "_futureVisitSurveyTypeHolder": _futureVisitSurveyTypeHolder = (ObjectHolder)value; break;
                    case "_insuredHolder": _insuredHolder = (ObjectHolder)value; break;
                    case "_locationHolder": _locationHolder = (ObjectHolder)value; break;
                    case "_previousSurveyHolder": _previousSurveyHolder = (ObjectHolder)value; break;
                    case "_primaryPolicyHolder": _primaryPolicyHolder = (ObjectHolder)value; break;
                    case "_recommendedUserHolder": _recommendedUserHolder = (ObjectHolder)value; break;
                    case "_reviewerUserHolder": _reviewerUserHolder = (ObjectHolder)value; break;
                    case "_serviceTypeHolder": _serviceTypeHolder = (ObjectHolder)value; break;
                    case "_severityRatingHolder": _severityRatingHolder = (ObjectHolder)value; break;
                    case "_surveyGradingHolder": _surveyGradingHolder = (ObjectHolder)value; break;
                    case "_surveyQualityHolder": _surveyQualityHolder = (ObjectHolder)value; break;
                    case "_surveyStatusHolder": _surveyStatusHolder = (ObjectHolder)value; break;
                    case "_surveyTypeHolder": _surveyTypeHolder = (ObjectHolder)value; break;
                    case "_transactionTypeHolder": _transactionTypeHolder = (ObjectHolder)value; break;
                    case "_servicePlanList": _servicePlanList = (IList)value; break;
                    case "_surveyReasonTypeList": _surveyReasonTypeList = (IList)value; break;
                    case "_requestedDate": _requestedDate = (DateTime)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static Survey Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(Survey), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch Survey entity with ID = '{0}'.", id));
            }
            return (Survey)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Survey GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Survey), opathExpression);
            return (Survey)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static Survey GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Survey))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Survey)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Survey[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Survey), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Survey[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(Survey))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (Survey[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Survey[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(Survey), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// Returns a writable instance of the current object.
        /// </summary>
        /// <returns>A new instance of the current object.</returns>
        public Survey GetWritableInstance()
        {
            if (!_isReadOnly)
            {
                throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
            }

            Survey clone = (Survey)this.MemberwiseClone();
            clone._isReadOnly = false;

            DB.Engine.StartTracking(clone, InitialState.Unchanged);

            return clone;
        }


        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        public void Save()
        {
            Save(null);
        }

        /// <summary>
        /// Saves this instance to the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Save(Transaction trans)
        {
            VerifyWritable();
            Validate();

            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Inserted);
            }

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        public void Delete()
        {
            Delete(null);
        }

        /// <summary>
        /// Deletes this instance from the database.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will be preformed.</param>
        public void Delete(Transaction trans)
        {
            if (DB.Engine.GetObjectState(this) == ObjectState.Unknown)
            {
                DB.Engine.StartTracking(this, InitialState.Unchanged);
            }

            DB.Engine.MarkForDeletion(this);

            if (trans != null)
            {
                trans.PersistChanges(this);
            }
            else
            {
                DB.Engine.PersistChanges(this);
            }

            DB.Engine.EndTracking(this);

            _isReadOnly = true;
        }


        private void VerifyWritable()
        {
            if (_isReadOnly)
            {
                throw new InvalidOperationException("This instance is read-only.");
            }
        }

        private void ValidateFields()
        {
            Validator.Validate(Field.ID, _id);
            Validator.Validate(Field.Number, _number);
            Validator.Validate(Field.CompanyID, _companyID);
            Validator.Validate(Field.PolicySystemID, _policysystemID);
            Validator.Validate(Field.InsuredID, _insuredID);
            Validator.Validate(Field.LocationID, _locationID);
            Validator.Validate(Field.PrimaryPolicyID, _primaryPolicyID);
            Validator.Validate(Field.PreviousSurveyID, _previousSurveyID);
            Validator.Validate(Field.TypeID, _typeID);
            Validator.Validate(Field.TransactionTypeID, _transactionTypeID);
            Validator.Validate(Field.QualityID, _qualityID);
            Validator.Validate(Field.ServiceTypeCode, _serviceTypeCode);
            Validator.Validate(Field.StatusCode, _statusCode);
            Validator.Validate(Field.AssignedByUserID, _assignedByUserID);
            Validator.Validate(Field.AssignedUserID, _assignedUserID);
            Validator.Validate(Field.RecommendedUserID, _recommendedUserID);
            Validator.Validate(Field.CreateStateCode, _createStateCode);
            Validator.Validate(Field.CreateDate, _createDate);
            Validator.Validate(Field.CreateByInternalUserID, _createByInternalUserID);
            Validator.Validate(Field.CreateByExternalUserName, _createByExternalUserName);
            Validator.Validate(Field.AssignDate, _assignDate);
            Validator.Validate(Field.ReassignDate, _reassignDate);
            Validator.Validate(Field.AcceptDate, _acceptDate);
            Validator.Validate(Field.SurveyedDate, _surveyedDate);
            Validator.Validate(Field.ReportCompleteDate, _reportCompleteDate);
            Validator.Validate(Field.DueDate, _dueDate);
            Validator.Validate(Field.CompleteDate, _completeDate);
            Validator.Validate(Field.CanceledDate, _canceledDate);
            Validator.Validate(Field.UnderwritingAcceptDate, _underwritingAcceptDate);
            Validator.Validate(Field.ReminderDate, _reminderDate);
            Validator.Validate(Field.ReminderComment, _reminderComment);
            Validator.Validate(Field.MailReceivedDate, _mailReceivedDate);
            Validator.Validate(Field.Correction, _correction);
            Validator.Validate(Field.NonProductive, _nonProductive);
            Validator.Validate(Field.Review, _review);
            Validator.Validate(Field.NonDisclosureRequired, _nonDisclosureRequired);
            Validator.Validate(Field.Hours, _hours);
            Validator.Validate(Field.CallCount, _callCount);
            Validator.Validate(Field.GradingCode, _gradingCode);
            Validator.Validate(Field.SeverityRatingCode, _severityRatingCode);
            Validator.Validate(Field.RecsRequired, _recsRequired);
            Validator.Validate(Field.UWNotified, _uwNotified);
            Validator.Validate(Field.DisplayClaims, _displayClaims);
            Validator.Validate(Field.FeeConsultantCost, _feeConsultantCost);
            Validator.Validate(Field.InvoiceNumber, _invoiceNumber);
            Validator.Validate(Field.InvoiceDate, _invoiceDate);
            Validator.Validate(Field.FutureVisitNeeded, _futureVisitNeeded);
            Validator.Validate(Field.FutureVisitSurveyTypeID, _futureVisitSurveyTypeID);
            Validator.Validate(Field.FutureVisitDueDate, _futureVisitDueDate);
            Validator.Validate(Field.ConsultantUserID, _consultantUserID);
            Validator.Validate(Field.ReviewerUserID, _reviewerUserID);
            Validator.Validate(Field.WorkflowSubmissionNumber, _workflowSubmissionNumber);
            Validator.Validate(Field.WorkflowReference, _workflowReference);
            Validator.Validate(Field.UnderwriterNotified, _underwriterNotified);
            Validator.Validate(Field.Underwriter2Code, _underwriter2Code);
            Validator.Validate(Field.LegacySystemID, _legacySystemID);
            Validator.Validate(Field.LastRefreshDate, _lastRefreshDate);
            Validator.Validate(Field.CancelRefreshDate, _cancelRefreshDate);
            Validator.Validate(Field.LastModifiedOn, _lastModifiedOn);
            Validator.Validate(Field.Priority, _priority);
            Validator.Validate(Field.InHouseSelection, _inHouseSelection);
            Validator.Validate(Field.ReviewerCompleteDate, _reviewerCompleteDate);
            Validator.Validate(Field.ApprovalCode, _approvalCode);
            Validator.Validate(Field.ContainsCriticalRec, _containsCriticalRec);
            Validator.Validate(Field.ContainsOpenCriticalRec, _containsOpenCriticalRec);
            Validator.Validate(Field.RequestedDate, _requestedDate);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _number = Int32.MinValue;
            _companyID = Guid.Empty;
            _policysystemID = Guid.Empty;
            _insuredID = Guid.Empty;
            _locationID = Guid.Empty;
            _primaryPolicyID = Guid.Empty;
            _previousSurveyID = Guid.Empty;
            _typeID = Guid.Empty;
            _transactionTypeID = Guid.Empty;
            _qualityID = Guid.Empty;
            _serviceTypeCode = null;
            _statusCode = null;
            _assignedByUserID = Guid.Empty;
            _assignedUserID = Guid.Empty;
            _recommendedUserID = Guid.Empty;
            _createStateCode = null;
            _createDate = DateTime.MinValue;
            _createByInternalUserID = Guid.Empty;
            _createByExternalUserName = null;
            _assignDate = DateTime.MinValue;
            _reassignDate = DateTime.MinValue;
            _acceptDate = DateTime.MinValue;
            _surveyedDate = DateTime.MinValue;
            _reportCompleteDate = DateTime.MinValue;
            _dueDate = DateTime.MinValue;
            _completeDate = DateTime.MinValue;
            _canceledDate = DateTime.MinValue;
            _underwritingAcceptDate = DateTime.MinValue;
            _reminderDate = DateTime.MinValue;
            _reminderComment = null;
            _mailReceivedDate = DateTime.MinValue;
            _correction = false;
            _nonProductive = false;
            _review = false;
            _nonDisclosureRequired = false;
            _hours = Decimal.MinValue;
            _callCount = Decimal.MinValue;
            _gradingCode = null;
            _severityRatingCode = null;
            _recsRequired = false;
            _uwNotified = false;
            _displayClaims = false;
            _feeConsultantCost = Decimal.MinValue;
            _invoiceNumber = null;
            _invoiceDate = DateTime.MinValue;
            _futureVisitNeeded = false;
            _futureVisitSurveyTypeID = Guid.Empty;
            _futureVisitDueDate = DateTime.MinValue;
            _consultantUserID = Guid.Empty;
            _reviewerUserID = Guid.Empty;
            _workflowSubmissionNumber = null;
            _workflowReference = null;
            _underwriterNotified = false;
            _underwriterCode = null;
            _underwriter2Code = null;
            _legacySystemID = null;
            _lastRefreshDate = DateTime.MinValue;
            _cancelRefreshDate = DateTime.MinValue;
            _lastModifiedOn = DateTime.MinValue;
            _priority = null;
            _inHouseSelection = false;
            _reviewerCompleteDate = DateTime.MinValue;
            _assignedByUserHolder = null;
            _assignedUserHolder = null;
            _companyHolder = null;
            _consultantUserHolder = null;
            _createByInternalUserHolder = null;
            _createStateHolder = null;
            _futureVisitSurveyTypeHolder = null;
            _insuredHolder = null;
            _locationHolder = null;
            _previousSurveyHolder = null;
            _primaryPolicyHolder = null;
            _recommendedUserHolder = null;
            _reviewerUserHolder = null;
            _serviceTypeHolder = null;
            _severityRatingHolder = null;
            _surveyGradingHolder = null;
            _surveyQualityHolder = null;
            _surveyStatusHolder = null;
            _surveyTypeHolder = null;
            _transactionTypeHolder = null;
            _servicePlanList = null;
            _surveyReasonTypeList = null;
            _approvalCode = null;
            _containsCriticalRec = false;
            _containsOpenCriticalRec = false;
            _requestedDate = DateTime.MinValue;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the Survey entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the Number field.
            /// </summary>
            Number,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the InsuredID field.
            /// </summary>
            InsuredID,
            /// <summary>
            /// Represents the LocationID field.
            /// </summary>
            LocationID,
            /// <summary>
            /// Represents the PrimaryPolicyID field.
            /// </summary>
            PrimaryPolicyID,
            /// <summary>
            /// Represents the PreviousSurveyID field.
            /// </summary>
            PreviousSurveyID,
            /// <summary>
            /// Represents the TypeID field.
            /// </summary>
            TypeID,
            /// <summary>
            /// Represents the TransactionTypeID field.
            /// </summary>
            TransactionTypeID,
            /// <summary>
            /// Represents the QualityID field.
            /// </summary>
            QualityID,
            /// <summary>
            /// Represents the ServiceTypeCode field.
            /// </summary>
            ServiceTypeCode,
            /// <summary>
            /// Represents the StatusCode field.
            /// </summary>
            StatusCode,
            /// <summary>
            /// Represents the AssignedByUserID field.
            /// </summary>
            AssignedByUserID,
            /// <summary>
            /// Represents the AssignedUserID field.
            /// </summary>
            AssignedUserID,
            /// <summary>
            /// Represents the RecommendedUserID field.
            /// </summary>
            RecommendedUserID,
            /// <summary>
            /// Represents the CreateStateCode field.
            /// </summary>
            CreateStateCode,
            /// <summary>
            /// Represents the CreateDate field.
            /// </summary>
            CreateDate,
            /// <summary>
            /// Represents the CreateByInternalUserID field.
            /// </summary>
            CreateByInternalUserID,
            /// <summary>
            /// Represents the CreateByExternalUserName field.
            /// </summary>
            CreateByExternalUserName,
            /// <summary>
            /// Represents the AssignDate field.
            /// </summary>
            AssignDate,
            /// <summary>
            /// Represents the ReassignDate field.
            /// </summary>
            ReassignDate,
            /// <summary>
            /// Represents the AcceptDate field.
            /// </summary>
            AcceptDate,
            /// <summary>
            /// Represents the SurveyedDate field.
            /// </summary>
            SurveyedDate,
            /// <summary>
            /// Represents the ReportCompleteDate field.
            /// </summary>
            ReportCompleteDate,
            /// <summary>
            /// Represents the DueDate field.
            /// </summary>
            DueDate,
            /// <summary>
            /// Represents the CompleteDate field.
            /// </summary>
            CompleteDate,
            /// <summary>
            /// Represents the CanceledDate field.
            /// </summary>
            CanceledDate,
            /// <summary>
            /// Represents the UnderwritingAcceptDate field.
            /// </summary>
            UnderwritingAcceptDate,
            /// <summary>
            /// Represents the ReminderDate field.
            /// </summary>
            ReminderDate,
            /// <summary>
            /// Represents the ReminderComment field.
            /// </summary>
            ReminderComment,
            /// <summary>
            /// Represents the MailReceivedDate field.
            /// </summary>
            MailReceivedDate,
            /// <summary>
            /// Represents the Correction field.
            /// </summary>
            Correction,
            /// <summary>
            /// Represents the NonProductive field.
            /// </summary>
            NonProductive,
            /// <summary>
            /// Represents the Review field.
            /// </summary>
            Review,
            /// <summary>
            /// Represents the NonDisclosureRequired field.
            /// </summary>
            NonDisclosureRequired,
            /// <summary>
            /// Represents the Hours field.
            /// </summary>
            Hours,
            /// <summary>
            /// Represents the CallCount field.
            /// </summary>
            CallCount,
            /// <summary>
            /// Represents the GradingCode field.
            /// </summary>
            GradingCode,
            /// <summary>
            /// Represents the SeverityRatingCode field.
            /// </summary>
            SeverityRatingCode,
            /// <summary>
            /// Represents the RecsRequired field.
            /// </summary>
            RecsRequired,
            /// Represents the UWNotified field.
            /// </summary>
            UWNotified,
            /// <summary>
            /// Represents the DisplayClaims field.
            /// </summary>
            DisplayClaims,
            /// <summary>
            /// Represents the FeeConsultantCost field.
            /// </summary>
            FeeConsultantCost,
            /// <summary>
            /// Represents the InvoiceNumber field.
            /// </summary>
            InvoiceNumber,
            /// <summary>
            /// Represents the InvoiceDate field.
            /// </summary>
            InvoiceDate,
            /// <summary>
            /// Represents the FutureVisitNeeded field.
            /// </summary>
            FutureVisitNeeded,
            /// <summary>
            /// Represents the FutureVisitSurveyTypeID field.
            /// </summary>
            FutureVisitSurveyTypeID,
            /// <summary>
            /// Represents the FutureVisitDueDate field.
            /// </summary>
            FutureVisitDueDate,
            /// <summary>
            /// Represents the ConsultantUserID field.
            /// </summary>
            ConsultantUserID,
            /// <summary>
            /// Represents the ReviewerUserID field.
            /// </summary>
            ReviewerUserID,
            /// <summary>
            /// Represents the WorkflowSubmissionNumber field.
            /// </summary>
            WorkflowSubmissionNumber,
            /// <summary>
            /// Represents the WorkflowReference field.
            /// </summary>
            WorkflowReference,
            /// <summary>
            /// Represents the UnderwriterNotified field.
            /// </summary>
            UnderwriterNotified,
            /// <summary>
            /// Represents the UnderwriterCode field.
            /// </summary>
            UnderwriterCode,
            /// <summary>
            /// Represents the LegacySystemID field.
            /// </summary>
            LegacySystemID,
            /// <summary>
            /// Represents the LastRefreshDate field.
            /// </summary>
            LastRefreshDate,
            /// <summary>
            /// Represents the CancelRefreshDate field.
            /// </summary>
            CancelRefreshDate,
            /// <summary>
            /// Represents the LastModifiedOn field.
            /// </summary>
            LastModifiedOn,
            /// <summary>
            /// Represents the Priority field.
            /// </summary>
            Priority,
            /// <summary>
            /// Represents the InHouseSelection field.
            /// </summary>
            InHouseSelection,
            /// <summary>
            /// Represents the TechCompleteDate field.
            /// </summary>
            ReviewerCompleteDate,
            /// <summary>
            /// Represents the ApprovalCode field.
            /// </summary>
            ApprovalCode,
            /// <summary>
            /// Represents the ContainsCriticalRec field.
            /// </summary>
            ContainsCriticalRec,
            /// Represents the ContainsOpenCriticalRec field.
            /// </summary>
            ContainsOpenCriticalRec,
            /// <summary>
            /// Represents the RequestedDate field.
            /// </summary>
            RequestedDate,
            PolicySystemID,
            Underwriter2Code,
        }

        #endregion
    }
}