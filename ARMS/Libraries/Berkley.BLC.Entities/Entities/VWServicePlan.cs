using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a VWServicePlan entity, which maps to table 'VW_ServicePlan' in the database.
	/// </summary>
    [Serializable]
	public class VWServicePlan : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private VWServicePlan()
		{
		}


        #region --- Generated Members ---

        private Guid _id = Guid.Empty;
        private Guid _servicePlanID;
        private int _servicePlanNumber;
        private Guid _companyID;
        private string _companyAbbreviation;
        private string _servicePlanStatusCode;
        private string _servicePlanStatusName;
        private string _servicePlanTypeName = String.Empty;
        private Guid _assignedUserID = Guid.Empty;
        private string _assignedUserName = String.Empty;
        private DateTime _createDate;
        private DateTime _completeDate = DateTime.MinValue;
        private Guid _createdByUserID;
        private string _createdByUserName = String.Empty;
        private DateTime _lastModifiedOn = DateTime.MinValue;
        private string _createStateCode;
        private Guid _insuredID = Guid.Empty;
        private string _clientID = String.Empty;
        private string _insuredName = String.Empty;
        private string _insuredName2 = String.Empty;
        private string _insuredStreetLine1 = String.Empty;
        private string _insuredStreetLine2 = String.Empty;
        private string _insuredStreetLine3 = String.Empty;
        private string _insuredCity = String.Empty;
        private string _insuredStateCode = String.Empty;
        private string _insuredZipCode = String.Empty;
        private string _businessOperations = String.Empty;
        private string _sicCode = String.Empty;
        private string _underwriter = String.Empty;
        private string _underwriterCode = String.Empty;
        private string _underwriter2Code = String.Empty;
        private DateTime _insuredNonRenewedDate = DateTime.MinValue;
        private string _agencyNumber = String.Empty;
        private string _agencySubNumber = String.Empty;
        private string _agencyName = String.Empty;
        private string _agencyStreetLine1 = String.Empty;
        private string _agencyStreetLine2 = String.Empty;
        private string _agencyCity = String.Empty;
        private string _agencyStateCode = String.Empty;
        private string _agencyZipCode = String.Empty;
        private string _agencyPhoneNumber = String.Empty;
        private string _agencyFaxNumber = String.Empty;
        private string _serviceVisitCount = String.Empty;
        private DateTime _policyExpireDate = DateTime.MinValue;
        private decimal _policyPremium = Decimal.MinValue;
        private DateTime _todaysDate = DateTime.MinValue;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets the Service Plan ID.
        /// </summary>
        public Guid ServicePlanID
        {
            get { return _servicePlanID; }
        }

        /// <summary>
        /// Gets the Service Plan Number.
        /// </summary>
        public int ServicePlanNumber
        {
            get { return _servicePlanNumber; }
        }

        /// <summary>
        /// Gets the Company ID.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets the Company Abbreviation.
        /// </summary>
        public string CompanyAbbreviation
        {
            get { return _companyAbbreviation; }
        }

        /// <summary>
        /// Gets the Service Plan Status Code.
        /// </summary>
        public string ServicePlanStatusCode
        {
            get { return _servicePlanStatusCode; }
        }

        /// <summary>
        /// Gets the Service Plan Status Name.
        /// </summary>
        public string ServicePlanStatusName
        {
            get { return _servicePlanStatusName; }
        }

        /// <summary>
        /// Gets the Service Plan Type Name. Null value is 'String.Empty'.
        /// </summary>
        public string ServicePlanTypeName
        {
            get { return _servicePlanTypeName; }
        }

        /// <summary>
        /// Gets the Assigned User ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid AssignedUserID
        {
            get { return _assignedUserID; }
        }

        /// <summary>
        /// Gets the Assigned User Name. Null value is 'String.Empty'.
        /// </summary>
        public string AssignedUserName
        {
            get { return _assignedUserName; }
        }

        /// <summary>
        /// Gets the Create Date.
        /// </summary>
        public DateTime CreateDate
        {
            get { return _createDate; }
        }

        /// <summary>
        /// Gets the Complete Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime CompleteDate
        {
            get { return _completeDate; }
        }

        /// <summary>
        /// Gets the Created By User ID.
        /// </summary>
        public Guid CreatedByUserID
        {
            get { return _createdByUserID; }
        }

        /// <summary>
        /// Gets the Created By User Name. Null value is 'String.Empty'.
        /// </summary>
        public string CreatedByUserName
        {
            get { return _createdByUserName; }
        }

        /// <summary>
        /// Gets the Last Modified On. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime LastModifiedOn
        {
            get { return _lastModifiedOn; }
        }

        /// <summary>
        /// Gets the Create State Code.
        /// </summary>
        public string CreateStateCode
        {
            get { return _createStateCode; }
        }

        /// <summary>
        /// Gets the Insured ID. Null value is 'Guid.Empty'.
        /// </summary>
        public Guid InsuredID
        {
            get { return _insuredID; }
        }

        /// <summary>
        /// Gets the Client ID. Null value is 'String.Empty'.
        /// </summary>
        public string ClientID
        {
            get { return _clientID; }
        }

        /// <summary>
        /// Gets the Insured Name. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredName
        {
            get { return _insuredName; }
        }

        /// <summary>
        /// Gets the Insured Name 2. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredName2
        {
            get { return _insuredName2; }
        }

        /// <summary>
        /// Gets the Insured Street Line 1. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredStreetLine1
        {
            get { return _insuredStreetLine1; }
        }

        /// <summary>
        /// Gets the Insured Street Line 2. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredStreetLine2
        {
            get { return _insuredStreetLine2; }
        }

        /// <summary>
        /// Gets the Insured Street Line 3. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredStreetLine3
        {
            get { return _insuredStreetLine3; }
        }

        /// <summary>
        /// Gets the Insured City. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredCity
        {
            get { return _insuredCity; }
        }

        /// <summary>
        /// Gets the Insured State Code. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredStateCode
        {
            get { return _insuredStateCode; }
        }

        /// <summary>
        /// Gets the Insured Zip Code. Null value is 'String.Empty'.
        /// </summary>
        public string InsuredZipCode
        {
            get { return _insuredZipCode; }
        }

        /// <summary>
        /// Gets the Business Operations. Null value is 'String.Empty'.
        /// </summary>
        public string BusinessOperations
        {
            get { return _businessOperations; }
        }

        /// <summary>
        /// Gets the SIC Code. Null value is 'String.Empty'.
        /// </summary>
        public string SICCode
        {
            get { return _sicCode; }
        }

        /// <summary>
        /// Gets the Underwriter. Null value is 'String.Empty'.
        /// </summary>
        public string Underwriter
        {
            get { return _underwriter; }
        }

        /// <summary>
        /// Gets the Underwriter Code. Null value is 'String.Empty'.
        /// </summary>
        public string UnderwriterCode
        {
            get { return _underwriterCode; }
        }

        /// <summary>
        /// Gets the Underwriter Code. Null value is 'String.Empty'.
        /// </summary>
        public string Underwriter2Code
        {
            get { return _underwriter2Code; }
        }

        /// <summary>
        /// Gets the Insured Non Renewed Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime InsuredNonRenewedDate
        {
            get { return _insuredNonRenewedDate; }
        }

        /// <summary>
        /// Gets the Agency Number. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyNumber
        {
            get { return _agencyNumber; }
        }

        /// <summary>
        /// Gets the Agency Sub Number. Null value is 'String.Empty'.
        /// </summary>
        public string AgencySubNumber
        {
            get { return _agencySubNumber; }
        }

        /// <summary>
        /// Gets the Agency Name. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyName
        {
            get { return _agencyName; }
        }

        /// <summary>
        /// Gets the Agency Street Line 1. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyStreetLine1
        {
            get { return _agencyStreetLine1; }
        }

        /// <summary>
        /// Gets the Agency Street Line 2. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyStreetLine2
        {
            get { return _agencyStreetLine2; }
        }

        /// <summary>
        /// Gets the Agency City. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyCity
        {
            get { return _agencyCity; }
        }

        /// <summary>
        /// Gets the Agency State Code. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyStateCode
        {
            get { return _agencyStateCode; }
        }

        /// <summary>
        /// Gets the Agency Zip Code. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyZipCode
        {
            get { return _agencyZipCode; }
        }

        /// <summary>
        /// Gets the Agency Phone Number. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyPhoneNumber
        {
            get { return _agencyPhoneNumber; }
        }

        /// <summary>
        /// Gets the Agency Fax Number. Null value is 'String.Empty'.
        /// </summary>
        public string AgencyFaxNumber
        {
            get { return _agencyFaxNumber; }
        }

        /// <summary>
        /// Gets the Service Visit Count. Null value is 'String.Empty'.
        /// </summary>
        public string ServiceVisitCount
        {
            get { return _serviceVisitCount; }
        }

        /// <summary>
        /// Gets the Policy Expire Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime PolicyExpireDate
        {
            get { return _policyExpireDate; }
        }

        /// <summary>
        /// Gets the Policy Premium. Null value is 'Decimal.MinValue'.
        /// </summary>
        public decimal PolicyPremium
        {
            get { return _policyPremium; }
        }


        /// <summary>
        /// Gets the Todays Date. Null value is 'DateTime.MinValue'.
        /// </summary>
        public DateTime TodaysDate
        {
            get { return _todaysDate; }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_servicePlanID": return _servicePlanID;
                    case "_servicePlanNumber": return _servicePlanNumber;
                    case "_companyID": return _companyID;
                    case "_companyAbbreviation": return _companyAbbreviation;
                    case "_servicePlanStatusCode": return _servicePlanStatusCode;
                    case "_servicePlanStatusName": return _servicePlanStatusName;
                    case "_servicePlanTypeName": return _servicePlanTypeName;
                    case "_assignedUserID": return _assignedUserID;
                    case "_assignedUserName": return _assignedUserName;
                    case "_createDate": return _createDate;
                    case "_completeDate": return _completeDate;
                    case "_createdByUserID": return _createdByUserID;
                    case "_createdByUserName": return _createdByUserName;
                    case "_lastModifiedOn": return _lastModifiedOn;
                    case "_createStateCode": return _createStateCode;
                    case "_insuredID": return _insuredID;
                    case "_clientID": return _clientID;
                    case "_insuredName": return _insuredName;
                    case "_insuredName2": return _insuredName2;
                    case "_insuredStreetLine1": return _insuredStreetLine1;
                    case "_insuredStreetLine2": return _insuredStreetLine2;
                    case "_insuredStreetLine3": return _insuredStreetLine3;
                    case "_insuredCity": return _insuredCity;
                    case "_insuredStateCode": return _insuredStateCode;
                    case "_insuredZipCode": return _insuredZipCode;
                    case "_businessOperations": return _businessOperations;
                    case "_sicCode": return _sicCode;
                    case "_underwriter": return _underwriter;
                    case "_underwriterCode": return _underwriterCode;
                    case "_underwriter2Code": return _underwriter2Code;
                    case "_insuredNonRenewedDate": return _insuredNonRenewedDate;
                    case "_agencyNumber": return _agencyNumber;
                    case "_agencySubNumber": return _agencySubNumber;
                    case "_agencyName": return _agencyName;
                    case "_agencyStreetLine1": return _agencyStreetLine1;
                    case "_agencyStreetLine2": return _agencyStreetLine2;
                    case "_agencyCity": return _agencyCity;
                    case "_agencyStateCode": return _agencyStateCode;
                    case "_agencyZipCode": return _agencyZipCode;
                    case "_agencyPhoneNumber": return _agencyPhoneNumber;
                    case "_agencyFaxNumber": return _agencyFaxNumber;
                    case "_serviceVisitCount": return _serviceVisitCount;
                    case "_policyExpireDate": return _policyExpireDate;
                    case "_policyPremium": return _policyPremium;
                    case "_todaysDate": return _todaysDate;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_servicePlanID": _servicePlanID = (Guid)value; break;
                    case "_servicePlanNumber": _servicePlanNumber = (int)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_companyAbbreviation": _companyAbbreviation = (string)value; break;
                    case "_servicePlanStatusCode": _servicePlanStatusCode = (string)value; break;
                    case "_servicePlanStatusName": _servicePlanStatusName = (string)value; break;
                    case "_servicePlanTypeName": _servicePlanTypeName = (string)value; break;
                    case "_assignedUserID": _assignedUserID = (Guid)value; break;
                    case "_assignedUserName": _assignedUserName = (string)value; break;
                    case "_createDate": _createDate = (DateTime)value; break;
                    case "_completeDate": _completeDate = (DateTime)value; break;
                    case "_createdByUserID": _createdByUserID = (Guid)value; break;
                    case "_createdByUserName": _createdByUserName = (string)value; break;
                    case "_lastModifiedOn": _lastModifiedOn = (DateTime)value; break;
                    case "_createStateCode": _createStateCode = (string)value; break;
                    case "_insuredID": _insuredID = (Guid)value; break;
                    case "_clientID": _clientID = (string)value; break;
                    case "_insuredName": _insuredName = (string)value; break;
                    case "_insuredName2": _insuredName2 = (string)value; break;
                    case "_insuredStreetLine1": _insuredStreetLine1 = (string)value; break;
                    case "_insuredStreetLine2": _insuredStreetLine2 = (string)value; break;
                    case "_insuredStreetLine3": _insuredStreetLine3 = (string)value; break;
                    case "_insuredCity": _insuredCity = (string)value; break;
                    case "_insuredStateCode": _insuredStateCode = (string)value; break;
                    case "_insuredZipCode": _insuredZipCode = (string)value; break;
                    case "_businessOperations": _businessOperations = (string)value; break;
                    case "_sicCode": _sicCode = (string)value; break;
                    case "_underwriter": _underwriter = (string)value; break;
                    case "_underwriterCode": _underwriterCode = (string)value; break;
                    case "_underwriter2Code": _underwriter2Code = (string)value; break;
                    case "_insuredNonRenewedDate": _insuredNonRenewedDate = (DateTime)value; break;
                    case "_agencyNumber": _agencyNumber = (string)value; break;
                    case "_agencySubNumber": _agencySubNumber = (string)value; break;
                    case "_agencyName": _agencyName = (string)value; break;
                    case "_agencyStreetLine1": _agencyStreetLine1 = (string)value; break;
                    case "_agencyStreetLine2": _agencyStreetLine2 = (string)value; break;
                    case "_agencyCity": _agencyCity = (string)value; break;
                    case "_agencyStateCode": _agencyStateCode = (string)value; break;
                    case "_agencyZipCode": _agencyZipCode = (string)value; break;
                    case "_agencyPhoneNumber": _agencyPhoneNumber = (string)value; break;
                    case "_agencyFaxNumber": _agencyFaxNumber = (string)value; break;
                    case "_serviceVisitCount": _serviceVisitCount = (string)value; break;
                    case "_policyExpireDate": _policyExpireDate = (DateTime)value; break;
                    case "_policyPremium": _policyPremium = (decimal)value; break;
                    case "_todaysDate": _todaysDate = (DateTime)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static VWServicePlan Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(VWServicePlan), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch VWServicePlan entity with ID = '{0}'.", id));
            }
            return (VWServicePlan)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static VWServicePlan GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWServicePlan), opathExpression);
            return (VWServicePlan)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static VWServicePlan GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(VWServicePlan))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (VWServicePlan)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWServicePlan[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWServicePlan), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWServicePlan[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(VWServicePlan))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (VWServicePlan[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static VWServicePlan[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(VWServicePlan), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _servicePlanID = Guid.Empty;
            _servicePlanNumber = Int32.MinValue;
            _companyID = Guid.Empty;
            _companyAbbreviation = null;
            _servicePlanStatusCode = null;
            _servicePlanStatusName = null;
            _servicePlanTypeName = null;
            _assignedUserID = Guid.Empty;
            _assignedUserName = null;
            _createDate = DateTime.MinValue;
            _completeDate = DateTime.MinValue;
            _createdByUserID = Guid.Empty;
            _createdByUserName = null;
            _lastModifiedOn = DateTime.MinValue;
            _createStateCode = null;
            _insuredID = Guid.Empty;
            _clientID = null;
            _insuredName = null;
            _insuredName2 = null;
            _insuredStreetLine1 = null;
            _insuredStreetLine2 = null;
            _insuredStreetLine3 = null;
            _insuredCity = null;
            _insuredStateCode = null;
            _insuredZipCode = null;
            _businessOperations = null;
            _sicCode = null;
            _underwriter = null;
            _underwriterCode = null;
            _underwriter2Code = null;
            _insuredNonRenewedDate = DateTime.MinValue;
            _agencyNumber = null;
            _agencySubNumber = null;
            _agencyName = null;
            _agencyStreetLine1 = null;
            _agencyStreetLine2 = null;
            _agencyCity = null;
            _agencyStateCode = null;
            _agencyZipCode = null;
            _agencyPhoneNumber = null;
            _agencyFaxNumber = null;
            _serviceVisitCount = null;
            _policyExpireDate = DateTime.MinValue;
            _policyPremium = Decimal.MinValue;
            _todaysDate = DateTime.MinValue;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the VWServicePlan entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the ServicePlanID field.
            /// </summary>
            ServicePlanID,
            /// <summary>
            /// Represents the ServicePlanNumber field.
            /// </summary>
            ServicePlanNumber,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the CompanyAbbreviation field.
            /// </summary>
            CompanyAbbreviation,
            /// <summary>
            /// Represents the ServicePlanStatusCode field.
            /// </summary>
            ServicePlanStatusCode,
            /// <summary>
            /// Represents the ServicePlanStatusName field.
            /// </summary>
            ServicePlanStatusName,
            /// <summary>
            /// Represents the ServicePlanTypeName field.
            /// </summary>
            ServicePlanTypeName,
            /// <summary>
            /// Represents the AssignedUserID field.
            /// </summary>
            AssignedUserID,
            /// <summary>
            /// Represents the AssignedUserName field.
            /// </summary>
            AssignedUserName,
            /// <summary>
            /// Represents the CreateDate field.
            /// </summary>
            CreateDate,
            /// <summary>
            /// Represents the CompleteDate field.
            /// </summary>
            CompleteDate,
            /// <summary>
            /// Represents the CreatedByUserID field.
            /// </summary>
            CreatedByUserID,
            /// <summary>
            /// Represents the CreatedByUserName field.
            /// </summary>
            CreatedByUserName,
            /// <summary>
            /// Represents the LastModifiedOn field.
            /// </summary>
            LastModifiedOn,
            /// <summary>
            /// Represents the CreateStateCode field.
            /// </summary>
            CreateStateCode,
            /// <summary>
            /// Represents the InsuredID field.
            /// </summary>
            InsuredID,
            /// <summary>
            /// Represents the ClientID field.
            /// </summary>
            ClientID,
            /// <summary>
            /// Represents the InsuredName field.
            /// </summary>
            InsuredName,
            /// <summary>
            /// Represents the InsuredName2 field.
            /// </summary>
            InsuredName2,
            /// <summary>
            /// Represents the InsuredStreetLine1 field.
            /// </summary>
            InsuredStreetLine1,
            /// <summary>
            /// Represents the InsuredStreetLine2 field.
            /// </summary>
            InsuredStreetLine2,
            /// <summary>
            /// Represents the InsuredStreetLine3 field.
            /// </summary>
            InsuredStreetLine3,
            /// <summary>
            /// Represents the InsuredCity field.
            /// </summary>
            InsuredCity,
            /// <summary>
            /// Represents the InsuredStateCode field.
            /// </summary>
            InsuredStateCode,
            /// <summary>
            /// Represents the InsuredZipCode field.
            /// </summary>
            InsuredZipCode,
            /// <summary>
            /// Represents the BusinessOperations field.
            /// </summary>
            BusinessOperations,
            /// <summary>
            /// Represents the SICCode field.
            /// </summary>
            SICCode,
            /// <summary>
            /// Represents the Underwriter field.
            /// </summary>
            Underwriter,
            /// <summary>
            /// Represents the UnderwriterCode field.
            /// </summary>
            UnderwriterCode,
            /// Represents the Underwriter2Code field.
            /// </summary>
            Underwriter2Code,
            /// <summary>
            /// Represents the InsuredNonRenewedDate field.
            /// </summary>
            InsuredNonRenewedDate,
            /// <summary>
            /// Represents the AgencyNumber field.
            /// </summary>
            AgencyNumber,
            /// <summary>
            /// Represents the AgencySubNumber field.
            /// </summary>
            AgencySubNumber,
            /// <summary>
            /// Represents the AgencyName field.
            /// </summary>
            AgencyName,
            /// <summary>
            /// Represents the AgencyStreetLine1 field.
            /// </summary>
            AgencyStreetLine1,
            /// <summary>
            /// Represents the AgencyStreetLine2 field.
            /// </summary>
            AgencyStreetLine2,
            /// <summary>
            /// Represents the AgencyCity field.
            /// </summary>
            AgencyCity,
            /// <summary>
            /// Represents the AgencyStateCode field.
            /// </summary>
            AgencyStateCode,
            /// <summary>
            /// Represents the AgencyZipCode field.
            /// </summary>
            AgencyZipCode,
            /// <summary>
            /// Represents the AgencyPhoneNumber field.
            /// </summary>
            AgencyPhoneNumber,
            /// <summary>
            /// Represents the AgencyFaxNumber field.
            /// </summary>
            AgencyFaxNumber,
            /// <summary>
            /// Represents the ServiceVisitCount field.
            /// </summary>
            ServiceVisitCount,
            /// <summary>
            /// Represents the PolicyExpireDate field.
            /// </summary>
            PolicyExpireDate,
            /// Represents the PolicyPremium field.
            /// </summary>
            PolicyPremium,
            /// <summary>
            /// Represents the TodaysDate field.
            /// </summary>
            TodaysDate,
        }

        #endregion
    }
}