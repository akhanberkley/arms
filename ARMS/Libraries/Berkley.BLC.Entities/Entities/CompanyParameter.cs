using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a CompanyParameter entity, which maps to table 'CompanyParameter' in the database.
	/// </summary>
	public class CompanyParameter : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private CompanyParameter()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="companyID">CompanyID of this CompanyParameter.</param>
		/// <param name="parameterID">ParameterID of this CompanyParameter.</param>
		/// <param name="value">Value of this CompanyParameter.</param>
		public CompanyParameter(Guid companyID, int parameterID, string value)
		{
			_companyID = companyID;
			_parameterID = parameterID;
			_value = value;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _companyID;
		private int _parameterID;
		private Guid _divisionID = Guid.Empty;
		private string _value;
		private DateTime _createDateTime;
		private int _sortOrder;
		private DateTime _effectiveDate = DateTime.MinValue;
		private ObjectHolder _companyHolder = null;
		private ObjectHolder _parameterParameterHolder = null;
		private bool _isReadOnly = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Company.
		/// </summary>
		public Guid CompanyID
		{
			get { return _companyID; }
		}

		/// <summary>
		/// Gets the Parameter.
		/// </summary>
		public int ParameterID
		{
			get { return _parameterID; }
		}

		/// <summary>
		/// Gets or sets the Division ID. Null value is 'Guid.Empty'.
		/// </summary>
		public Guid DivisionID
		{
			get { return _divisionID; }
			set
			{
				VerifyWritable();
				_divisionID = value;
			}
		}

		/// <summary>
		/// Gets the Value.
		/// </summary>
		public string Value
		{
			get { return _value; }
		}

		/// <summary>
		/// Gets or sets the Create Date Time.
		/// </summary>
		public DateTime CreateDateTime
		{
			get { return _createDateTime; }
			set
			{
				VerifyWritable();
				_createDateTime = value;
			}
		}

		/// <summary>
		/// Gets or sets the Sort Order.
		/// </summary>
		public int SortOrder
		{
			get { return _sortOrder; }
			set
			{
				VerifyWritable();
				_sortOrder = value;
			}
		}

		/// <summary>
		/// Gets or sets the Effective Date. Null value is 'DateTime.MinValue'.
		/// </summary>
		public DateTime EffectiveDate
		{
			get { return _effectiveDate; }
			set
			{
				VerifyWritable();
				_effectiveDate = value;
			}
		}

		/// <summary>
		/// Gets the instance of a Company object related to this entity.
		/// </summary>
		public Company Company
		{
			get
			{
				_companyHolder.Key = _companyID;
				return (Company)_companyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Parameter object related to this entity.
		/// </summary>
		public Parameter Parameter
		{
			get
			{
				_parameterParameterHolder.Key = _parameterID;
				return (Parameter)_parameterParameterHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_companyID": return _companyID;
					case "_parameterID": return _parameterID;
					case "_divisionID": return _divisionID;
					case "_value": return _value;
					case "_createDateTime": return _createDateTime;
					case "_sortOrder": return _sortOrder;
					case "_effectiveDate": return _effectiveDate;
					case "_companyHolder": return _companyHolder;
					case "_parameterParameterHolder": return _parameterParameterHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_companyID": _companyID = (Guid)value; break;
					case "_parameterID": _parameterID = (int)value; break;
					case "_divisionID": _divisionID = (Guid)value; break;
					case "_value": _value = (string)value; break;
					case "_createDateTime": _createDateTime = (DateTime)value; break;
					case "_sortOrder": _sortOrder = (int)value; break;
					case "_effectiveDate": _effectiveDate = (DateTime)value; break;
					case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
					case "_parameterParameterHolder": _parameterParameterHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="companyID">CompanyID of the entity to fetch.</param>
		/// <param name="parameterID">ParameterID of the entity to fetch.</param>
		/// <param name="value">Value of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static CompanyParameter Get(Guid companyID, int parameterID, string value)
		{
			OPathQuery query = new OPathQuery(typeof(CompanyParameter), "CompanyID = ? && ParameterID = ? && Value = ?");
			object entity = DB.Engine.GetObject(query, companyID, parameterID, value);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch CompanyParameter entity with CompanyID = '{0}' and ParameterID = '{1}' and Value = '{2}'.", companyID, parameterID, value));
			}
			return (CompanyParameter)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static CompanyParameter GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CompanyParameter), opathExpression);
			return (CompanyParameter)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static CompanyParameter GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(CompanyParameter) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (CompanyParameter)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CompanyParameter[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CompanyParameter), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CompanyParameter[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(CompanyParameter) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (CompanyParameter[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static CompanyParameter[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(CompanyParameter), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public CompanyParameter GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			CompanyParameter clone = (CompanyParameter)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.CompanyID, _companyID);
			Validator.Validate(Field.ParameterID, _parameterID);
			Validator.Validate(Field.DivisionID, _divisionID);
			Validator.Validate(Field.Value, _value);
			Validator.Validate(Field.CreateDateTime, _createDateTime);
			Validator.Validate(Field.SortOrder, _sortOrder);
			Validator.Validate(Field.EffectiveDate, _effectiveDate);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_companyID = Guid.Empty;
			_parameterID = Int32.MinValue;
			_divisionID = Guid.Empty;
			_value = null;
			_createDateTime = DateTime.MinValue;
			_sortOrder = Int32.MinValue;
			_effectiveDate = DateTime.MinValue;
			_companyHolder = null;
			_parameterParameterHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the CompanyParameter entity.
		/// </summary>
		public enum Field
		{
			/// <summary>
			/// Represents the CompanyID field.
			/// </summary>
			CompanyID,
			/// <summary>
			/// Represents the ParameterID field.
			/// </summary>
			ParameterID,
			/// <summary>
			/// Represents the DivisionID field.
			/// </summary>
			DivisionID,
			/// <summary>
			/// Represents the Value field.
			/// </summary>
			Value,
			/// <summary>
			/// Represents the CreateDateTime field.
			/// </summary>
			CreateDateTime,
			/// <summary>
			/// Represents the SortOrder field.
			/// </summary>
			SortOrder,
			/// <summary>
			/// Represents the EffectiveDate field.
			/// </summary>
			EffectiveDate,
		}
		
		#endregion
	}
}