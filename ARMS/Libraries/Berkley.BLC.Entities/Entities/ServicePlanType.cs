using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents a ServicePlanType entity, which maps to table 'ServicePlanType' in the database.
	/// </summary>
	public class ServicePlanType : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private ServicePlanType()
		{
		}


        #region --- Generated Members ---

        private Guid _id;
        private Guid _companyID;
        private string _name;
        private int _displayOrder;
        private bool _disabled;
        private ObjectHolder _companyHolder = null;

        #endregion

        #region --- Generated Properties ---

        /// <summary>
        /// Gets the Service Plan Type.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        /// <summary>
        /// Gets the Company.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets the Service Plan Type.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Gets the Display Order.
        /// </summary>
        public int DisplayOrder
        {
            get { return _displayOrder; }
        }

        /// <summary>
        /// Gets the Disabled.
        /// </summary>
        public bool Disabled
        {
            get { return _disabled; }
        }

        /// <summary>
        /// Gets the instance of a Company object related to this entity.
        /// </summary>
        public Company Company
        {
            get
            {
                _companyHolder.Key = _companyID;
                return (Company)_companyHolder.InnerObject;
            }
        }

        /// <summary>
        /// Gets or sets the value of an internal member.
        /// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
        /// </summary>
        public object this[string memberName]
        {
            get
            {
                switch (memberName)
                {
                    case "_id": return _id;
                    case "_companyID": return _companyID;
                    case "_name": return _name;
                    case "_displayOrder": return _displayOrder;
                    case "_disabled": return _disabled;
                    case "_companyHolder": return _companyHolder;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
            set
            {
                // handle null values
                if (value == null) return;

                switch (memberName)
                {
                    case "_id": _id = (Guid)value; break;
                    case "_companyID": _companyID = (Guid)value; break;
                    case "_name": _name = (string)value; break;
                    case "_displayOrder": _displayOrder = (int)value; break;
                    case "_disabled": _disabled = (bool)value; break;
                    case "_companyHolder": _companyHolder = (ObjectHolder)value; break;
                    default:
                        {
                            throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
                        }
                }
            }
        }


        #endregion

        #region --- Generated Methods ---

        /// <summary>
        /// Gets the record with the specified key from the database.
        /// An exception is thrown if no entity exists with the specified key.
        /// </summary>
        /// <param name="id">ID of the entity to fetch.</param>
        /// <returns>A reference to the entity retrieved.</returns>
        public static ServicePlanType Get(Guid id)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanType), "ID = ?");
            object entity = DB.Engine.GetObject(query, id);
            if (entity == null)
            {
                throw new Exception(string.Format("Failed to fetch ServicePlanType entity with ID = '{0}'.", id));
            }
            return (ServicePlanType)entity;
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ServicePlanType GetOne(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanType), opathExpression);
            return (ServicePlanType)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches the first entity from the database that matches the specified OPath query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
        public static ServicePlanType GetOne(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ServicePlanType))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ServicePlanType)DB.Engine.GetObject(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlanType[] GetArray(string opathExpression, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanType), opathExpression);
            return GetArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPathQuery object to execute.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlanType[] GetArray(OPathQuery query, params object[] parameters)
        {
            if (query.ObjectType != typeof(ServicePlanType))
            {
                throw new ArgumentException("Query does not return the correct type.");
            }
            return (ServicePlanType[])DB.FetchArray(query, parameters);
        }

        /// <summary>
        /// Fetches an array of entity objects from the database matching the specified OPath expression.
        /// </summary>
        /// <param name="opathExpression">OPath query expression to use to filter the records.</param>
        /// <param name="opathSort">OPath sort order expression to use to order the results.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static ServicePlanType[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
        {
            OPathQuery query = new OPathQuery(typeof(ServicePlanType), opathExpression, opathSort);
            return GetArray(query, parameters);
        }


        /// <summary>
        /// This method suppresses compiler warnings about members never being initalized.
        /// Do not call this method from code.
        /// </summary>
        private void SuppressCompilerWarnings()
        {
            _id = Guid.Empty;
            _companyID = Guid.Empty;
            _name = null;
            _displayOrder = Int32.MinValue;
            _disabled = false;
            _companyHolder = null;
        }


        #endregion

        #region --- Generated Field Enum ---

        /// <summary>
        /// Specifies a field of the ServicePlanType entity.
        /// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the CompanyID field.
            /// </summary>
            CompanyID,
            /// <summary>
            /// Represents the Name field.
            /// </summary>
            Name,
            /// <summary>
            /// Represents the DisplayOrder field.
            /// </summary>
            DisplayOrder,
            /// <summary>
            /// Represents the Disabled field.
            /// </summary>
            Disabled,
        }

        #endregion
    }
}