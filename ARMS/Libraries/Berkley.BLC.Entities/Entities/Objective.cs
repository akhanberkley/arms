using System;
using System.Data;
using System.Collections;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
	/// <summary>
	/// Represents an Objective entity, which maps to table 'Objective' in the database.
	/// </summary>
	public class Objective : IObjectHelper
	{
		/// <summary>
		/// Private constructor for use by the ObjectSpace engine.
		/// </summary>
		private Objective()
		{
		}

		/// <summary>
		/// Creates a new instance of this class.
		/// </summary>
		/// <param name="id">ID of this Objective.</param>
		public Objective(Guid id)
		{
			_id = id;

			_isReadOnly = false;

			DB.Engine.StartTracking(this, InitialState.Inserted);
		}


		/// <summary>
		/// Validates this instance.
		/// An exception of type ValidationException is thrown if validation fails.
		/// </summary>
		public void Validate()
		{
			ValidateFields();

			//TODO: add additional validation logic here.
		}


		#region --- Generated Members ---

		private Guid _id;
		private int _number;
		private Guid _surveyID = Guid.Empty;
		private string _text;
		private string _commentsText = String.Empty;
		private DateTime _targetDate;
		private string _statusCode;
		private Guid _servicePlanID = Guid.Empty;
		private ObjectHolder _objectiveStatusHolder = null;
		private ObjectHolder _servicePlanHolder = null;
		private ObjectHolder _surveyHolder = null;
		private bool _isReadOnly = true;
        private bool _isPrint = true;

		#endregion

		#region --- Generated Properties ---

		/// <summary>
		/// Gets the Objective ID.
		/// </summary>
		public Guid ID
		{
			get { return _id; }
		}

		/// <summary>
		/// Gets or sets the Objective Number.
		/// </summary>
		public int Number
		{
			get { return _number; }
			set
			{
				VerifyWritable();
				_number = value;
			}
		}

		/// <summary>
		/// Gets or sets the Survey. Null value is 'Guid.Empty'.
		/// </summary>
		public Guid SurveyID
		{
			get { return _surveyID; }
			set
			{
				VerifyWritable();
				_surveyID = value;
			}
		}

		/// <summary>
		/// Gets or sets the Objective Text.
		/// </summary>
		public string Text
		{
			get { return _text; }
			set
			{
				VerifyWritable();
				_text = value;
			}
		}

        /// <summary>
        /// Gets a value indicating whether this needs to be printed in Service Plan letter
        /// </summary>
        public bool IsPrint
        {
            get { return _isPrint; }
            set
            {
                VerifyWritable();
                _isPrint = value;
            }
        }

		/// <summary>
		/// Gets or sets the Comments Text. Null value is 'String.Empty'.
		/// </summary>
		public string CommentsText
		{
			get { return _commentsText; }
			set
			{
				VerifyWritable();
				_commentsText = value;
			}
		}

		/// <summary>
		/// Gets or sets the Target Date.
		/// </summary>
		public DateTime TargetDate
		{
			get { return _targetDate; }
			set
			{
				VerifyWritable();
				_targetDate = value;
			}
		}

		/// <summary>
		/// Gets or sets the Objective Status.
		/// </summary>
		public string StatusCode
		{
			get { return _statusCode; }
			set
			{
				VerifyWritable();
				_statusCode = value;
			}
		}

		/// <summary>
		/// Gets or sets the Service Plan. Null value is 'Guid.Empty'.
		/// </summary>
		public Guid ServicePlanID
		{
			get { return _servicePlanID; }
			set
			{
				VerifyWritable();
				_servicePlanID = value;
			}
		}

		/// <summary>
		/// Gets the instance of a ObjectiveStatus object related to this entity.
		/// </summary>
		public ObjectiveStatus Status
		{
			get
			{
				_objectiveStatusHolder.Key = _statusCode;
				return (ObjectiveStatus)_objectiveStatusHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a ServicePlan object related to this entity.
		/// </summary>
		public ServicePlan ServicePlan
		{
			get
			{
				_servicePlanHolder.Key = _servicePlanID;
				return (ServicePlan)_servicePlanHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets the instance of a Survey object related to this entity.
		/// </summary>
		public Survey Survey
		{
			get
			{
				_surveyHolder.Key = _surveyID;
				return (Survey)_surveyHolder.InnerObject;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this instance is read-only.
		/// </summary>
		public bool IsReadOnly
		{
			get { return _isReadOnly; }
		}

     

		/// <summary>
		/// Gets or sets the value of an internal member.
		/// NOTE: This method is for use by the WORM engine for improved performance and should not be used elsewhere.
		/// </summary>
		public object this[string memberName]
		{
			get
			{
				switch( memberName )
				{
					case "_id": return _id;
					case "_number": return _number;
					case "_surveyID": return _surveyID;
					case "_text": return _text;
					case "_commentsText": return _commentsText;
                    case "_isPrint": return _isPrint;
					case "_targetDate": return _targetDate;
					case "_statusCode": return _statusCode;
					case "_servicePlanID": return _servicePlanID;
					case "_objectiveStatusHolder": return _objectiveStatusHolder;
					case "_servicePlanHolder": return _servicePlanHolder;
					case "_surveyHolder": return _surveyHolder;
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
			set
			{
				// handle null values
				if( value == null ) return;
			
				switch( memberName )
				{
					case "_id": _id = (Guid)value; break;
					case "_number": _number = (int)value; break;
					case "_surveyID": _surveyID = (Guid)value; break;
					case "_text": _text = (string)value; break;
					case "_commentsText": _commentsText = (string)value; break;
                    case "_isPrint": _isPrint = (bool)value; break;
					case "_targetDate": _targetDate = (DateTime)value; break;
					case "_statusCode": _statusCode = (string)value; break;
					case "_servicePlanID": _servicePlanID = (Guid)value; break;
					case "_objectiveStatusHolder": _objectiveStatusHolder = (ObjectHolder)value; break;
					case "_servicePlanHolder": _servicePlanHolder = (ObjectHolder)value; break;
					case "_surveyHolder": _surveyHolder = (ObjectHolder)value; break;		
					default:
					{
						throw new Exception(string.Format("Missing case for member '{0}'.", memberName));
					}
				}
			}
		}


		#endregion

		#region --- Generated Methods ---

		/// <summary>
		/// Gets the record with the specified key from the database.
		/// An exception is thrown if no entity exists with the specified key.
		/// </summary>
		/// <param name="id">ID of the entity to fetch.</param>
		/// <returns>A reference to the entity retrieved.</returns>
		public static Objective Get(Guid id)
		{
			OPathQuery query = new OPathQuery(typeof(Objective), "ID = ?");
			object entity = DB.Engine.GetObject(query, id);	
			if( entity == null )
			{
				throw new Exception(string.Format("Failed to fetch Objective entity with ID = '{0}'.", id));
			}
			return (Objective)entity;
		}

		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static Objective GetOne(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(Objective), opathExpression);
			return (Objective)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches the first entity from the database that matches the specified OPath query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>A reference to the first entity retrieved; or null if no records were found.</returns>
		public static Objective GetOne(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(Objective) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (Objective)DB.Engine.GetObject(query, parameters);
		}
		
		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static Objective[] GetArray(string opathExpression, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(Objective), opathExpression);
			return GetArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database using the specified query.
		/// </summary>
		/// <param name="query">OPathQuery object to execute.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static Objective[] GetArray(OPathQuery query, params object[] parameters)
		{
			if( query.ObjectType != typeof(Objective) )
			{
				throw new ArgumentException("Query does not return the correct type.");
			}
			return (Objective[])DB.FetchArray(query, parameters);
		}

		/// <summary>
		/// Fetches an array of entity objects from the database matching the specified OPath expression.
		/// </summary>
		/// <param name="opathExpression">OPath query expression to use to filter the records.</param>
		/// <param name="opathSort">OPath sort order expression to use to order the results.</param>
		/// <param name="parameters">Parameter values to use with executing the query.</param>
		/// <returns>Array filled with entity objects returned from the database.</returns>
		public static Objective[] GetSortedArray(string opathExpression, string opathSort, params object[] parameters)
		{
			OPathQuery query = new OPathQuery(typeof(Objective), opathExpression, opathSort);
			return GetArray(query, parameters);
		}


		/// <summary>
		/// Returns a writable instance of the current object.
		/// </summary>
		/// <returns>A new instance of the current object.</returns>
		public Objective GetWritableInstance()
		{
			if( !_isReadOnly )
			{
				throw new InvalidOperationException("A writable instance can only be created from a read-only instance.");
			}

			Objective clone = (Objective)this.MemberwiseClone();
			clone._isReadOnly = false;

			DB.Engine.StartTracking(clone, InitialState.Unchanged);

			return clone;
		}


		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		public void Save()
		{	
			Save(null);	
		}

		/// <summary>
		/// Saves this instance to the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Save(Transaction trans)
		{
			VerifyWritable();
			Validate();

			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Inserted);
			}

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);

			_isReadOnly = true;
		}


		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		public void Delete()
		{
			Delete(null);
		}

		/// <summary>
		/// Deletes this instance from the database.
		/// </summary>
		/// <param name="trans">Transaction under which this operation will be preformed.</param>
		public void Delete(Transaction trans)
		{
			if( DB.Engine.GetObjectState(this) == ObjectState.Unknown )
			{
				DB.Engine.StartTracking(this, InitialState.Unchanged);
			}

			DB.Engine.MarkForDeletion(this);

			if( trans != null )
			{
				trans.PersistChanges(this);
			}
			else
			{
				DB.Engine.PersistChanges(this);
			}

			DB.Engine.EndTracking(this);
			
			_isReadOnly = true;
		}


		private void VerifyWritable()
		{
			if( _isReadOnly )
			{
				throw new InvalidOperationException("This instance is read-only.");
			}
		}

		private void ValidateFields()
		{
			Validator.Validate(Field.ID, _id);
			Validator.Validate(Field.Number, _number);
			Validator.Validate(Field.SurveyID, _surveyID);
			Validator.Validate(Field.Text, _text);
			Validator.Validate(Field.CommentsText, _commentsText);
            Validator.Validate(Field.IsPrint, _isPrint);
			Validator.Validate(Field.TargetDate, _targetDate);
			Validator.Validate(Field.StatusCode, _statusCode);
			Validator.Validate(Field.ServicePlanID, _servicePlanID);
		}


		/// <summary>
		/// This method suppresses compiler warnings about members never being initalized.
		/// Do not call this method from code.
		/// </summary>
		private void SuppressCompilerWarnings()
		{
			_id = Guid.Empty;
			_number = Int32.MinValue;
			_surveyID = Guid.Empty;
			_text = null;
			_commentsText = null;
            _isPrint = true;
			_targetDate = DateTime.MinValue;
			_statusCode = null;
			_servicePlanID = Guid.Empty;
			_objectiveStatusHolder = null;
			_servicePlanHolder = null;
			_surveyHolder = null;
		}


		#endregion

		#region --- Generated Field Enum ---
		
		/// <summary>
		/// Specifies a field of the Objective entity.
		/// </summary>
        public enum Field
        {
            /// <summary>
            /// Represents the ID field.
            /// </summary>
            ID,
            /// <summary>
            /// Represents the Number field.
            /// </summary>
            Number,
            /// <summary>
            /// Represents the SurveyID field.
            /// </summary>
            SurveyID,
            /// <summary>
            /// Represents the Text field.
            /// </summary>
            Text,
            /// <summary>
            /// Represents the CommentsText field.
            /// </summary>
            CommentsText,
            /// <summary>
            /// Represents the TargetDate field.
            /// </summary>
            TargetDate,
            /// <summary>
            /// Represents the StatusCode field.
            /// </summary>
            StatusCode,
            /// <summary>
            /// Represents the ServicePlanID field.
            /// </summary>
            ServicePlanID,
            IsPrint,
        }
		
		#endregion
	}
}