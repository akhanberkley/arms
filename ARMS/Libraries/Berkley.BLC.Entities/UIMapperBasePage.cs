using QCI.Web;
using QCI.Web.Validation;
using System;
using System.Collections;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Provides a base class for web pages utilizing the UI Mapping infrastructure.
    /// </summary>
    public class UIMapperBasePage : System.Web.UI.Page
    {
        private class ControlGroup
        {
            private string _name;
            private Hashtable _typeMaps;

            public ControlGroup(string name)
            {
                _name = name;
                _typeMaps = new Hashtable();
            }

            public string Name
            {
                get { return _name; }
            }

            public TypeMap this[Type type]
            {
                get { return this[type.FullName]; }
            }

            public TypeMap this[string typeFullName]
            {
                get { return (TypeMap)_typeMaps[typeFullName]; }
            }

            public PropertyMap this[PropertyInfo propertyInfo]
            {
                get
                {
                    TypeMap typeMap = this[propertyInfo.ReflectedType];
                    if (typeMap != null)
                    {
                        PropertyMap propMap = typeMap[propertyInfo.Name];
                        if (propMap != null)
                        {
                            return propMap;
                        }
                    }
                    return null;
                }
            }

            public void Add(TypeMap map)
            {
                string key = map.Type.FullName;
                _typeMaps.Add(key, map);
            }

            public void Add(PropertyMap map)
            {
                TypeMap typeMap = this[map.PropertyInfo.ReflectedType];
                if (typeMap == null)
                {
                    typeMap = new TypeMap(map.PropertyInfo.ReflectedType);
                    this.Add(typeMap);
                }
                typeMap.Add(map);
            }
        }

        private class TypeMap
        {
            private Type _type;
            private Hashtable _propertyMaps;

            public TypeMap(Type type)
            {
                _type = type;
                _propertyMaps = new Hashtable();
            }

            public Type Type
            {
                get { return _type; }
            }

            public PropertyMap this[string propertyName]
            {
                get { return (PropertyMap)_propertyMaps[propertyName]; }
            }

            public PropertyMap this[PropertyInfo propertyInfo]
            {
                get
                {
                    if (propertyInfo.ReflectedType != _type)
                    {
                        throw new ArgumentException("Type of PropertyInfo does not match type of PropertyMap.");
                    }
                    return this[propertyInfo.Name];
                }
            }

            public void Add(PropertyMap map)
            {
                string key = map.PropertyInfo.Name;
                _propertyMaps.Add(key, map);
            }

            public PropertyMap[] GetAllPropertyMaps()
            {
                PropertyMap[] items = new PropertyMap[_propertyMaps.Count];
                _propertyMaps.Values.CopyTo(items, 0);
                return items;
            }
        }

        private class PropertyMap
        {
            //private string _groupName;
            private PropertyInfo _propertyInfo;
            //private string _controlName;
            private Control _control;

            public PropertyMap(PropertyInfo propertyInfo, Control control)
            {
                //_groupName = groupName;
                _propertyInfo = propertyInfo;
                _control = control;
            }

            public PropertyInfo PropertyInfo
            {
                get { return _propertyInfo; }
            }

            public Control Control
            {
                get { return _control; }
            }
        }


        private const string DEFAULT_GROUP = "";
        private Hashtable _groups = new Hashtable();

        /// <summary>
        /// Registers a control with the UI Mapping infrastructure.
        /// </summary>
        /// <param name="control">Control to map to the specified property.</param>
        /// <param name="propertyInfo">Property to be mapped to the specified control.</param>
        public void RegisterControl(Control control, PropertyInfo propertyInfo)
        {
            RegisterControl(control, propertyInfo, DEFAULT_GROUP);
        }

        /// <summary>
        /// Registers a control with the UI Mapping infrastructure.
        /// </summary>
        /// <param name="control">Control to map to the specified property.</param>
        /// <param name="propertyInfo">Property to be mapped to the specified control.</param>
        /// <param name="groupName">Name of the group to which this control is to be associated.</param>
        public void RegisterControl(Control control, PropertyInfo propertyInfo, string groupName)
        {
            if (control == null) throw new ArgumentNullException("control");
            if (propertyInfo == null) throw new ArgumentNullException("propertyInfo");
            if (groupName == null) throw new ArgumentNullException("groupName");
            //if (fieldName == null) throw new ArgumentNullException("fieldName");
            //if (fieldName.Length == 0) throw new ArgumentException("Value cannot be an empty string.", "fieldName");

            ControlGroup group = (ControlGroup)_groups[groupName];
            if (group == null)
            {
                group = new ControlGroup(groupName);
                _groups.Add(groupName, group);
            }

            // a property cannot be mapped to more than once (in the same group)
            if (group[propertyInfo] != null)
            {
                string msg = "Control '{0}' could not be registered. ";
                if (groupName == DEFAULT_GROUP)
                {
                    msg += "Property '{1}' is already mapped to a control on this page. Specify a group name to resove the conflict.";
                }
                else
                {
                    msg += "Property '{1}' is already mapped to a control on this page in group '{2}'.";
                }
                string propName = propertyInfo.ReflectedType.Name + "." + propertyInfo.Name;
                throw new Exception(string.Format(msg, control.ID, propName, groupName));
            }

            // to help avoid bugs during populate entity, make sure no property is registered with the default group if 
            // the same property is also registered with a custom group
            if (groupName != DEFAULT_GROUP)
            {
                ControlGroup defaultGroup = (ControlGroup)_groups[DEFAULT_GROUP];
                if (defaultGroup != null && defaultGroup[propertyInfo] != null)
                {
                    string msg = "A group name is required for control '{0}' since property '{1}' is mapped to more than one control on the page.";
                    string controlID = defaultGroup[propertyInfo].Control.ID;
                    string propName = propertyInfo.ReflectedType.Name + "." + propertyInfo.Name;
                    throw new Exception(string.Format(msg, controlID, propName));
                }
            }

            // add a new control/property map to the group
            group.Add(new PropertyMap(propertyInfo, control));
        }


        /// <summary>
        /// Populates the mapped field controls on the page with values taken from the specified entity.
        /// </summary>
        /// <param name="entity">Entity from which to extract field values.</param>
        public void PopulateFields(object entity)
        {
            PopulateFields(DEFAULT_GROUP, entity);
        }

        /// <summary>
        /// Populates the mapped field controls on the page with values taken from the specified entity.
        /// </summary>
        /// <param name="groupName">Name of the group of controls to modify.</param>
        /// <param name="entity">Entity from which to extract field values.</param>
        public void PopulateFields(string groupName, object entity)
        {
            if (groupName == null) throw new ArgumentNullException("groupName");
            if (entity == null) throw new ArgumentNullException("entity");

            // get the group specified
            ControlGroup group = (ControlGroup)_groups[groupName];
            if (group == null)
            {
                throw new ArgumentException("No controls are in a group named '" + groupName + "'.", "groupName");
            }

            // get the type map matching the type of entity passed
            Type entityType = entity.GetType();
            TypeMap typeMap = group[entityType];
            if (typeMap == null)
            {
                throw new ArgumentException("No controls are assigned to a property in type '" + entityType.FullName + "'.", "entity");
            }

            // move values from the entity to controls mapped to this type of entity
            foreach (PropertyMap propMap in typeMap.GetAllPropertyMaps())
            {
                // we can't set the control's new value if we can't get the value from the mapped property
                if (!propMap.PropertyInfo.CanRead)
                {
                    continue;
                }

                // get the value of the property from the entity
                object value = propMap.PropertyInfo.GetValue(entity, null);

                // set the value of the control
                SetControlValue(propMap.Control, value);
            }
        }


        /// <summary>
        /// Populates the fields of the specified entity with values taken from the mapped field controls on the page.
        /// </summary>
        /// <param name="entity">Entity to be populate with values.</param>
        public void PopulateEntity(object entity)
        {
            PopulateEntity(DEFAULT_GROUP, entity);
        }

        /// <summary>
        /// Populates the fields of the specified entity with values taken from the mapped field controls on the page.
        /// </summary>
        /// <param name="groupName">Name of the group of controls to evaluate.</param>
        /// <param name="entity">Entity to populate with values.</param>
        public void PopulateEntity(string groupName, object entity)
        {
            if (groupName == null) throw new ArgumentNullException("groupName");
            if (entity == null) throw new ArgumentNullException("entity");

            // get the group specified
            ControlGroup group = (ControlGroup)_groups[groupName];
            if (group == null)
            {
                throw new ArgumentException("No controls are assigned to a group named '" + groupName + "'.", "groupName");
            }

            // get the type map matching the type of entity passed
            Type entityType = entity.GetType();
            TypeMap typeMap = group[entityType];
            if (typeMap == null)
            {
                throw new ArgumentException("No controls are assigned to properties related to type '" + entityType.FullName + "'.", "entity");
            }

            // move values from the entity to controls mapped to this type of entity
            int updateCount = 0;
            foreach (PropertyMap propMap in typeMap.GetAllPropertyMaps())
            {
                // we have to skip setting this property if it does not allow writing
                if (!propMap.PropertyInfo.CanWrite)
                {
                    continue;
                }

                // don't extract values from hidden controls (they are probably hidden for a reason)
                Control control = propMap.Control;
                bool isVisible = true;
                while (control != null)
                {
                    if (!control.Visible)
                    {
                        isVisible = false;
                        break;
                    }
                    control = control.Parent;
                }
                if (!isVisible)
                {
                    continue;
                }

                // get the value from the control
                object value = GetControlValue(propMap.Control);

                // convert the value to the property's type (we have to special case Guid target types)
                Type targetType = propMap.PropertyInfo.PropertyType;
                if (targetType == typeof(Guid) && value is string)
                {
                    string str = (string)value;
                    value = (str.Length > 0) ? new Guid(str) : Guid.Empty;
                }
                else if (targetType == typeof(Int16) && value is string)
                {
                    string str = (string)value;
                    value = (str.Length > 0) ? Int16.Parse(str) : Int16.MinValue;
                }
                else
                {
                    value = Convert.ChangeType(value, targetType);
                }

                // set the entity's property value
                propMap.PropertyInfo.SetValue(entity, value, null);
                updateCount += 1;
            }

            // throw an error if no properties of the entity were set - almost guaranteed to be a bug.
            if (updateCount == 0)
            {
                throw new Exception("Entity was not populated with any value from page.  No writable properties are associated to type '" + entityType.FullName + "'.");
            }
        }


        internal Control FindControlByField(Enum field)
        {
            return FindControlByField(field, DEFAULT_GROUP);
        }

        internal Control FindControlByField(Enum field, string groupName)
        {
            if (groupName == null) throw new ArgumentNullException("groupName");

            ControlGroup group = (ControlGroup)_groups[groupName];
            if (group == null)
            {
                throw new ArgumentException("No controls are assigned to a group named '" + groupName + "'.", "groupName");
            }

            UIFieldInfo fieldInfo = UIMapper.Instance.GetFieldInfo(field);
            PropertyMap propMap = group[fieldInfo.PropertyInfo];

            return (propMap != null) ? propMap.Control : null;
        }


        internal string FindNameByField(Enum field)
        {
            return FindNameByField(field, DEFAULT_GROUP);
        }

        internal string FindNameByField(Enum field, string groupName)
        {
            if (groupName == null) throw new ArgumentNullException("groupName");

            ControlGroup group = (ControlGroup)_groups[groupName];
            if (group == null)
            {
                throw new ArgumentException("No controls are assigned to a group named '" + groupName + "'.", "groupName");
            }

            UIFieldInfo fieldInfo = UIMapper.Instance.GetFieldInfo(field);
            PropertyMap propMap = group[fieldInfo.PropertyInfo];
            if (propMap != null)
            {
                MappedFieldControl control = (propMap.Control as MappedFieldControl);
                if (control != null)
                {
                    return control.Name;
                }
            }

            return fieldInfo.DisplayName;
        }


        /// <summary>
        /// Adds a script to the page that will cause an a human-readable version of a FieldValidationException to be displayed when the page loads.
        /// The page focus will also be set to the field indicated as the source of the exception.
        /// </summary>
        /// <param name="ex">Exception to show the user.</param>
        public void ShowErrorMessage(FieldValidationException ex)
        {
            JavaScript.ShowMessage(this, ex.Message);
            Control control = ex.Control;
            if (control is MappedFieldControl)
            {
                control = (control as MappedFieldControl).InputControl;
            }
            if (control != null)
            {
                JavaScript.SetInputFocus(control);
            }
        }

        /// <summary>
        /// Adds a script to the page that will cause a human-readable version of a ValidationException to be displayed when the page loads.
        /// The page focus will also be set to the field indicated as the source of the exception.
        /// </summary>
        /// <param name="ex">Exception to show the user.</param>
        public void ShowErrorMessage(ValidationException ex)
        {
            ShowErrorMessage(ex, DEFAULT_GROUP);
        }

        /// <summary>
        /// Adds a script to the page that will cause a human-readable version of a ValidationException to be displayed when the page loads.
        /// The page focus will also be set to the field indicated as the source of the exception.
        /// </summary>
        /// <param name="ex">Exception to show the user.</param>
        /// <param name="group">Name of the group containing the control that is the source of the exception.</param>
        public void ShowErrorMessage(ValidationException ex, string group)
        {
            // build the array of field names
            string[] values = new string[ex.MessageFields.Length];
            for (int i = 0; i < values.Length; i++)
            {
                string value = this.FindNameByField(ex.MessageFields[i], group);
                if (value == null)
                {
                    UIFieldInfo fieldInfo = UIMapper.Instance.GetFieldInfo(ex.MessageFields[i]);
                    if (fieldInfo.DisplayName != null || fieldInfo.DisplayName.Length > 0)
                    {
                        value = fieldInfo.DisplayName;
                    }
                    else // no display name
                    {
                        value = fieldInfo.EnumValue.ToString();
                    }
                }
                values[i] = value;
            }

            // build the final string and add a javascript alert to the page
            JavaScript.ShowMessage(this, string.Format(ex.MessageFormat, values));

            // set the focus to the problem field, if was can find it			
            Control ctrl = this.FindControlByField(ex.Field);
            if (ctrl != null)
            {
                if (ctrl is MappedFieldControl)
                {
                    ctrl = (ctrl as MappedFieldControl).InputControl;
                }
                if (ctrl != null)
                {
                    JavaScript.SetInputFocus(ctrl);
                }
            }
        }



        private object GetControlValue(Control control)
        {
            if (control is MappedFieldControl)
            {
                return (control as MappedFieldControl).GetValue();
            }
            if (control is TextBox)
            {
                return (control as TextBox).Text;
            }
            if (control is CheckBox)
            {
                return (control as CheckBox).Checked;
            }
            if (control is Label)
            {
                return (control as Label).Text;
            }
            if (control is DropDownList)
            {
                return (control as DropDownList).SelectedValue;
            }

            throw new NotSupportedException("Control type '" + control.GetType() + "' is not currently supported.");
        }

        private void SetControlValue(Control control, object value)
        {
            if (control is MappedFieldControl)
            {
                (control as MappedFieldControl).SetValue(value);
            }
            else if (control is TextBox)
            {
                (control as TextBox).Text = value.ToString();
            }
            else if (control is CheckBox)
            {
                (control as CheckBox).Checked = Convert.ToBoolean(value);
            }
            else if (control is Label)
            {
                (control as Label).Text = value.ToString();
            }
            else if (control is DropDownList)
            {
                (control as DropDownList).SelectedValue = value.ToString();
            }
            else
            {
                throw new NotSupportedException("Control type '" + control.GetType() + "' is not currently supported.");
            }
        }

    }

    internal delegate void ControlRegisteredEventHandler(object sender, ControlRegisteredEventArgs e);

    internal class ControlRegisteredEventArgs : EventArgs
    {
        private UIFieldInfo _fieldInfo;
        private PropertyInfo _propertyInfo;

        public ControlRegisteredEventArgs(UIFieldInfo fieldInfo, PropertyInfo propertyInfo)
        {
            _fieldInfo = fieldInfo;
            _propertyInfo = propertyInfo;
        }

        public UIFieldInfo FieldInfo
        {
            get { return _fieldInfo; }
        }

        public PropertyInfo PropertyInfo
        {
            get { return _propertyInfo; }
        }
    }
}
