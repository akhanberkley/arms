using System;
using System.Text;

namespace Berkley.BLC.Entities
{
    public class SpecialQueries
    {
        /// <summary>
        /// Creates a SELECT COUNT(*) statement from a OPath FilterExpress and FilterExpression set.
        /// </summary>
        /// <param name="strFilterExpression">Filter expression such as "SurveyStatusCode = ?&& ProfitCenterByUser LIKE ?&& CompanyID = ? && CreateStateCode != ?"</param>
        /// <param name="strParameters">Paramters such as "RA,%CAROLINA%,77c82bd6-0bd9-4d79-92fd-c133788a3b0e,I"</param>
        /// <param name="oEntity">An instance of the Entity Class that will be queried for COUNT(*)</param>
        /// <param name="strTableName">The name of the table such as "dbo.VW_Survey"</param>
        /// <returns>The COUNT from the query or throws an Exception upon error</returns>
        public static int GetCount(string strFilterExpression, string strParameters, System.Type oType, string strTableName)
        {
            StringBuilder oBuilder = new StringBuilder();
            oBuilder.Append("SELECT COUNT(*) FROM  ");
            oBuilder.Append(strTableName);
            oBuilder.Append(" WHERE ");

            //Final clean-up
            string result = BuildQuery(oBuilder, strFilterExpression, strParameters, oType, strTableName);

            try
            {
                return (int)DB.Engine.ExecuteScalar(result);
            }
            catch (Exception oException)
            {
                throw new Exception("Error getting Counts from " + strTableName + ". Query: " + oBuilder.ToString() + " Message: " + oException.Message);
            }
        }

        public static Guid GetTopSurveyID(string strFilterExpression, string strParameters, System.Type oType, string strTableName)
        {
            StringBuilder oBuilder = new StringBuilder();
            oBuilder.Append("SELECT TOP 1 SurveyID FROM  ");
            oBuilder.Append(strTableName);
            oBuilder.Append(" WHERE ");

            //Final clean-up
            string result = BuildQuery(oBuilder, strFilterExpression, strParameters, oType, strTableName);

            try
            {
                object surveyID = DB.Engine.ExecuteScalar(result);
                return (surveyID != null) ? (Guid)surveyID : Guid.Empty;
            }
            catch (Exception oException)
            {
                throw new Exception("Error getting Counts from " + strTableName + ". Query: " + oBuilder.ToString() + " Message: " + oException.Message);
            }
        }

        private static string BuildQuery(StringBuilder oBuilder, string strFilterExpression, string strParameters, System.Type oType, string strTableName)
        {
            //Convert the OPath query into a SQL statement
            string[] strParamArray = strParameters.Replace("'", "''").Split('~');
            string[] strFilterArray = strFilterExpression.ToUpper().Split(new string[] { "&&" }, StringSplitOptions.RemoveEmptyEntries);

            string result = string.Empty;
            foreach (string s in strFilterArray)
            {
                if (s.Contains("NOT ISNULL"))
                {
                    oBuilder.Append(s.Replace("NOT ISNULL", string.Empty));
                    oBuilder.Append(" IS NOT NULL && ");
                }
                else if (s.Contains("ISNULL"))
                {
                    oBuilder.Append(s.Replace("ISNULL", string.Empty));
                    oBuilder.Append(" IS NULL && ");
                }
                else
                {
                    oBuilder.Append(s + " && ");
                }
            }

            //Replace the "?"
            int counter = 0;
            foreach (char q in oBuilder.ToString())
            {
                if (q == '?')
                {
                    result += "'{" + counter.ToString() + "}'";
                    counter++;
                }
                else
                {
                    result += q;
                }
            }

            //Final clean-up
            return string.Format(result.Trim().TrimEnd('&').Replace("&&", "AND"), strParamArray).Replace("||", "OR");
        }
    }
}
