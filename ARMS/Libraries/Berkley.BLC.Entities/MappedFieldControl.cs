using System;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Serves as the abstract base class for all field user controls.
    /// </summary>
    public class MappedFieldControl : System.Web.UI.UserControl
    {
        private UIFieldInfo _fieldInfo;
        private EventHandler _registeredEvent;

        /// <summary>
        /// Raises the Init event.
        /// </summary>
        /// <param name="e">An EventArgs object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            if (!(this.Page is UIMapperBasePage))
            {
                throw new Exception(string.Format("Controls of type '{0}' can only be placed on pages that inherit from '{1}'.", this.GetType(), typeof(UIMapperBasePage)));
            }

            // the field property must be set
            if (this.Field == null)
            {
                throw new InvalidOperationException("Control '" + this.ID + "' does not have a Field property specified.  Use empty string to indicate no mapping to an entity field.");
            }

            // note: blank field properties are allowed, they are just not bound to the UI mapper or registered.
            if (this.Field.Length != 0)
            {
                // initialize the field info member var based on the specified field name
                _fieldInfo = UIMapper.Instance.GetFieldInfo(this.Field, false);

                // get the property info for the mapped property
                PropertyInfo propInfo = (_fieldInfo != null) ? _fieldInfo.PropertyInfo : UIMapper.Instance.GetPropertyInfo(this.Field);

                // use the default name if none was specified on the control
                if (this.ViewState["Name"] == null)
                {
                    if (_fieldInfo == null)
                    {
                        throw new InvalidOperationException("Control '" + this.ID + "' must specify value for the Name property since field '" + this.Field + "' does not exist in the mapping field.");
                    }
                    if (_fieldInfo.DisplayName == null)
                    {
                        throw new InvalidOperationException("Control '" + this.ID + "' must specify value for the Name property since no default name is provided for field '" + UIMapper.GetFieldName(_fieldInfo.EnumValue) + "' in the mapping field.");
                    }
                    this.Name = _fieldInfo.DisplayName;
                }

                // set the requried flag if it was not specified on the control
                if (_fieldInfo != null && this.ViewState["IsRequired"] == null)
                {
                    this.IsRequired = _fieldInfo.IsRequired;
                }

                // register this control with the page
                string group = this.Group;
                if (group == null)
                {
                    (this.Page as UIMapperBasePage).RegisterControl(this, propInfo);
                }
                else
                {
                    (this.Page as UIMapperBasePage).RegisterControl(this, propInfo, group);
                }
                OnRegistered(new EventArgs());
            }

            // now call the base's version of this method
            base.OnInit(e);
        }


        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An EventArgs object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                if (this.Name == null || this.Name.Length == 0)
                {
                    throw new InvalidOperationException("Name property of control '" + this.ID + "' must be specified.");
                }

                // copy any attributes assigned on the user control to the input control
                WebControl control = (this.InputControl as WebControl);
                if (control != null)
                {
                    foreach (string key in this.Attributes.Keys)
                    {
                        control.Attributes.Add(key, this.Attributes[key]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error during pre-render for control '" + this.ID + "'.", ex);
            }

            base.OnPreRender(e);
        }


        /// <summary>
        /// Occurs when this server control is registed with its host page.
        /// </summary>
        public event EventHandler Registered
        {
            add { _registeredEvent += value; }
            remove { _registeredEvent -= value; }
        }


        /// <summary>
        /// Gets the UIFieldInfo associated to this control.
        /// </summary>
        public UIFieldInfo FieldInfo
        {
            get { return _fieldInfo; }
        }


        /// <summary>
        /// Gets or sets the name of this control, which is suitable to present to the user.
        /// </summary>
        public string Name
        {
            get
            {
                object value = this.ViewState["Name"];
                return (value != null) ? (string)value : null;
            }
            set
            {
                this.ViewState["Name"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the entity field to which this control is linked.
        /// </summary>
        public virtual string Field
        {
            get
            {
                object value = this.ViewState["Field"];
                return (value != null) ? (string)value : null;
            }
            set
            {
                this.ViewState["Field"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the control group to which this control belongs.
        /// </summary>
        public string Group
        {
            get
            {
                object value = this.ViewState["Group"];
                return (value != null) ? (string)value : null;
            }
            set
            {
                this.ViewState["Group"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating if a HTML table row tag should be injected into the page by this control.
        /// </summary>
        public bool TableRow
        {
            get
            {
                object value = this.ViewState["TableRow"];
                return (value != null) ? (bool)value : true;
            }
            set
            {
                this.ViewState["TableRow"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating if this field is required.
        /// </summary>
        public bool IsRequired
        {
            get
            {
                object value = this.ViewState["IsRequired"];
                return (value != null) ? (bool)value : false;
            }
            set
            {
                this.ViewState["IsRequired"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the tab index of the server control.
        /// </summary>
        public short TabIndex
        {
            get
            {
                WebControl control = (this.InputControl as WebControl);
                return (control != null) ? control.TabIndex : (short)0;
            }
            set
            {
                WebControl control = (this.InputControl as WebControl);
                if (control != null)
                {
                    control.TabIndex = value;
                }
            }
        }


        /// <summary>
        /// Gets the internal control responsible for capturing user input.
        /// </summary>
        public virtual Control InputControl
        {
            get { throw new NotImplementedException("Subclass must implement this property."); }
        }


        /// <summary>
        /// Gets the user input value from the control.
        /// </summary>
        /// <returns>Value from the control.</returns>
        public virtual object GetValue()
        {
            throw new NotImplementedException("Method not implemented for this control.");
        }


        /// <summary>
        /// Sets the user input value of the control.
        /// </summary>
        /// <param name="value">Value to be set.</param>
        public virtual void SetValue(object value)
        {
            throw new NotImplementedException("Method not implemented for this control.");
        }



        /// <summary>
        /// Gets the default text for the label when none is specified explicitly.
        /// </summary>
        /// <returns>The value to use for the label.</returns>
        protected virtual string GetDefaultLabelText()
        {
            return this.Name;
        }


        /// <summary>
        /// Raises the Registered event.
        /// </summary>
        /// <param name="e">An EventArgs object that contains the event data.</param>
        protected virtual void OnRegistered(EventArgs e)
        {
            if (_registeredEvent != null)
            {
                _registeredEvent(this, e);
            }
        }
    }
}

