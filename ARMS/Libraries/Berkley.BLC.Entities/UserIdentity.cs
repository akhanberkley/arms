using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Represents the identity of a user logged into the application.
    /// </summary>
    public class UserIdentity : MarshalByRefObject, IIdentity
    {
        private FormsIdentity _formsIdentity;
        private string _cacheKey;
        private Guid _userID;
        private string _username;
        private string _name;
        private string _initials;
        private string _emailAddress;
        private Guid _companyID;
        private string _companyName;
        private bool _isSystemAdmin;
        private bool _isUnderwriter;

        private static string _appPath = null;

        /// <summary>
        /// Initializes a new instance of the UserIdentity class.
        /// </summary>
        /// <param name="authTicket">The authentication ticket upon which this identity is based.</param>
        /// <param name="cacheKey">Key value that can be used to remove the identity in the HTTP cache when needed.</param>
        public UserIdentity(FormsAuthenticationTicket authTicket, string cacheKey, HttpRequest request)
        {
            // get the Forms identity from the passed ticket
            _formsIdentity = new FormsIdentity(authTicket);
            _cacheKey = cacheKey;

            string sUsername = request.ServerVariables["LOGON_USER"];
            if (sUsername == null) sUsername = string.Empty;
            sUsername = (sUsername.IndexOf('\\') > 0) ? sUsername.Substring(sUsername.IndexOf('\\') + 1) : sUsername;

            try
            {
                // note: the Name property contains the user ID, NOT the username!!
                _userID = new Guid(_formsIdentity.Name);

                // fetch the user identity info for this user from the database
                User user = User.Get(_userID);
                _username = user.Username;
                _name = (user.IsUnderwriter && sUsername != string.Empty) ? sUsername : user.Name;
                _emailAddress = user.EmailAddress;
                _companyID = user.CompanyID;
                _companyName = user.Company.Name;
                _isUnderwriter = user.IsUnderwriter;

                if (user.IsUnderwriter)
                {
                    Underwriter underwriter = Underwriter.GetOne("Username = ?", sUsername);
                    if (underwriter != null)
                    {
                        _initials = underwriter.Code;
                    }
                }

                // see if this user is a system admin
                _isSystemAdmin = false;

                string sysAdminList = System.Configuration.ConfigurationManager.AppSettings["SystemAdminAccounts"];
                if (sysAdminList != null && sysAdminList.Length > 0)
                {
                    string domainUsername = HttpContext.Current.Request.ServerVariables["LOGON_USER"];
                    string[] accounts = sysAdminList.Split(';');
                    foreach (string account in accounts)
                    {
                        if (string.Compare(account, domainUsername, true) == 0) // match
                        {
                            _isSystemAdmin = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // rethrow a more detailed message
                throw new Exception("Failed to get identity information for user '" + _userID + "'.", ex);
            }
        }


        /// <summary>
        /// Gets the user of the current HTTP request, if any.
        /// </summary>
        /// <returns>Identity of the user making the request; or null if the user has no identity.</returns>
        public static UserIdentity Current
        {
            get
            {
                HttpContext context = HttpContext.Current;
                if (context != null)
                {
                    return (context.User.Identity as UserIdentity);
                }
                else
                {
                    return null;
                }
            }
        }


        #region --- IIdentity Members ---

        /// <summary>
        /// Gets a value that indicates whether the user has been authenticated.
        /// </summary>
        public bool IsAuthenticated
        {
            get { return _formsIdentity.IsAuthenticated; }
        }


        /// <summary>
        /// Gets the name of the current user.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Gets the intitials of the current user.
        /// </summary>
        public string Initials
        {
            get { return _initials; }
        }


        /// <summary>
        /// Gets the type of authentication used.
        /// </summary>
        public string AuthenticationType
        {
            get { return _formsIdentity.AuthenticationType; }
        }


        #endregion

        /// <summary>
        /// Gets the base FormsAuthenticationTicket used to build this identity.
        /// </summary>
        internal FormsAuthenticationTicket Ticket
        {
            get { return _formsIdentity.Ticket; }
        }


        /// <summary>
        /// Gets the ID of this user.
        /// </summary>
        public Guid UserID
        {
            get { return _userID; }
        }

        /// <summary>
        /// Gets the email address of this user.
        /// </summary>
        public string EmailAddress
        {
            get { return _emailAddress; }
        }

        /// <summary>
        /// Gets the ID of the company to which this user is associated.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets the name of the company to which this user is associated.
        /// </summary>
        public string CompanyName
        {
            get { return _companyName; }
        }

        /// <summary>
        /// Gets a flag value indicating if this user has an account that can be logged out.
        /// </summary>
        public bool CanLogout
        {
            get { return _isSystemAdmin; }
        }

        /// <summary>
        /// Gets a flag value indicating if this user is an underwriter.
        /// </summary>
        public bool IsUnderwriter
        {
            get { return _isUnderwriter; }
        }


        /// <summary>
        /// Gets the PermissionSet that defines the permissions for this user.
        /// </summary>
        public PermissionSet Permissions
        {
            get
            {
                return PermissionSetCache.Fetch(this);
            }
        }


        /// <summary>
        /// Retrieves the specified setting for this user for the current page, or the default value provided if the setting is not found.
        /// </summary>
        /// <param name="name">Name of setting to retrieve for this page.</param>
        /// <param name="defaultValue">Value to return if setting does not exist.</param>
        /// <returns>The value of the setting, or defaultValue if not found.</returns>
        public object GetPageSetting(string name, object defaultValue)
        {
            if (name == null || name.Length == 0) throw new ArgumentNullException("name");

            using (SqlDataReader dr = StoredProcedures.UserSetting_Get(_userID, GetDBSettingName(name)))
            {
                if (!dr.Read())
                {
                    return defaultValue;
                }

                TypeCode code = (TypeCode)dr.GetInt32(1); // ValueType
                if (code == TypeCode.Empty)
                {
                    return null;
                }

                object value = dr.GetValue(0); // SettingValue
                if (value == DBNull.Value)
                {
                    return defaultValue;
                }

                return Convert.ChangeType(value, code);
            }
        }

        /// <summary>
        /// Assigns a new value to the specified setting for this user for the current page.
        /// </summary>
        /// <param name="name">Name of setting to update for this page.</param>
        /// <param name="value">Value to be stored.</param>
        public void SavePageSetting(string name, object value)
        {
            if (name == null || name.Length == 0) throw new ArgumentNullException("name");

            TypeCode code = Convert.GetTypeCode(value);
            if (code == TypeCode.Object)
            {
                throw new ArgumentException("Values of type '" + value.GetType() + "' cannot be stored.", "value");
            }

            StoredProcedures.UserSetting_InsertOrUpdate(_userID, GetDBSettingName(name), Convert.ToString(value), (int)code);
        }

        /// <summary>
        /// Deletes the specified setting for this user for the current page.
        /// </summary>
        /// <param name="name">Name of the setting to delete for this page.</param>
        public void DeletePageSetting(string name)
        {
            if (name == null || name.Length == 0) throw new ArgumentNullException("name");

            StoredProcedures.UserSetting_Delete(_userID, GetDBSettingName(name));
        }

        /// <summary>
        /// Deletes the specified sort setting for this user for the current page.
        /// </summary>
        /// <param name="name">Name of the setting to delete for this page.</param>
        public void DeletePageSortSetting(string name)
        {
            if (name == null || name.Length == 0) throw new ArgumentNullException("name");

            StoredProcedures.UserSetting_Delete(_userID, name);
        }


        private string GetDBSettingName(string settingName)
        {
            HttpRequest request = HttpContext.Current.Request;
            if (request == null)
            {
                throw new Exception("There is not current HTTP Request in the application context.");
            }

            if (_appPath == null)
            {
                // get the virtual path to this web application
                _appPath = HttpContext.Current.Request.ApplicationPath.TrimEnd('/');
            }

            string path = request.Path.Substring(_appPath.Length).ToLower();

            return path + "::" + settingName;
        }


        /// <summary>
        /// Removes this user's principal and identity from the system cache to 
        /// fource the user's identity to be reloaded on the next request.
        /// </summary>
        public void RemoveFromCache()
        {
            // remove this user's principal object from the cache
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Cache.Remove(_cacheKey);
            }
        }

        /// <summary>
        /// Removes this user's principal and identity from the system cache to 
        /// fource the user's identity to be reloaded on the next request.
        /// </summary>
        public static void RemoveFromCache(User user)
        {
            // remove this user's principal object from the cache
            if (HttpContext.Current != null)
            {
                string key = "GenericPrincipal." + user.ID.ToString();
                HttpContext.Current.Cache.Remove("");
            }
        }
    }
}
