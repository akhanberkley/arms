using System;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Utility created for general UI data formatting.
    /// </summary>
    public class Formatter
    {
        private Formatter()
        {
        }

        /// <summary>
        /// Formats the passed value into a phone format.
        /// </summary>
        /// <param name="value">Value to format.</param>
        /// /// <param name="value">The default null value.</param>
        public static string Phone(string value, string nullValue)
        {
            string result = nullValue;
            if (value != null && value.Length > 0)
            {
                long number = long.MinValue;
                try
                {
                    number = long.Parse(value);
                }
                catch { }

                if (number != long.MinValue && value.Length == 7)
                {
                    result = number.ToString("###-####");
                }
                else if (number != long.MinValue && value.Length == 10)
                {
                    result = number.ToString("(###) ###-####");
                }
                else
                {
                    result = value;
                }
            }

            return result;
        }

        /// <summary>
        /// Formats the passed value into a phone format.
        /// </summary>
        /// <param name="value">Value to format.</param>
        /// <param name="value">Phone extension to be appended.</param>
        /// /// <param name="value">The default null value.</param>
        public static string Phone(string value, string ext, string nullValue)
        {
            string result = nullValue;
            if (value != null && value.Length > 0)
            {
                long number = long.MinValue;
                try
                {
                    number = long.Parse(value);
                }
                catch { }

                if (number != long.MinValue && value.Length == 7)
                {
                    result = number.ToString("###-####");

                    if (ext != null && ext.Trim().Length > 0)
                    {
                        result = string.Format("{0} ext. {1}", result, ext);
                    }
                }
                else if (number != long.MinValue && value.Length == 10)
                {
                    result = number.ToString("(###) ###-####");

                    if (ext != null && ext.Trim().Length > 0)
                    {
                        result = string.Format("{0} ext. {1}", result, ext);
                    }
                }
                else
                {
                    result = value;
                }  
            }

            return result;
        }

        /// <summary>
        /// Formats the passed value into a sender address format.
        /// </summary>
        /// <param name="streetLine1">StreetLine1 field to add to the address.</param>
        /// <param name="streetLine2">StreetLine2 field to add to the address.</param>
        /// <param name="city">City field to add to the address.</param>
        /// <param name="state">State field to add to the address.</param>
        /// <param name="zipCode">ZipCode field to add to the address.</param>
        public static string Address(string streetLine1, string streetLine2, string city, string state, string zipCode)
        {
            if (streetLine1 == string.Empty && streetLine2 == string.Empty && city == string.Empty && state == string.Empty && zipCode == string.Empty)
            {
                return "(not specified)";
            }
            else
            {
                return Address(streetLine1, streetLine2, null, city, state, zipCode);
            }
        }

        /// <summary>
        /// Formats the passed value into a sender address format.
        /// </summary>
        /// <param name="streetLine1">StreetLine1 field to add to the address.</param>
        /// <param name="streetLine2">StreetLine2 field to add to the address.</param>
        /// <param name="streetLine3">StreetLine3 field to add to the address.</param>
        /// <param name="city">City field to add to the address.</param>
        /// <param name="state">State field to add to the address.</param>
        /// <param name="zipCode">ZipCode field to add to the address.</param>
        public static string Address(string streetLine1, string streetLine2, string streetLine3, string city, string state, string zipCode)
        {
            if (streetLine1 == string.Empty && streetLine2 == string.Empty && streetLine3 == string.Empty && city == string.Empty && state == string.Empty && zipCode == string.Empty)
            {
                return "(not specified)";
            }
            
            StringWriter w = new StringWriter();

            if (streetLine1 != null && streetLine1.Length > 0)
            {
                HttpUtility.HtmlEncode(streetLine1, w);
                w.WriteLine("<br>");
            }
            if (streetLine2 != null && streetLine2.Length > 0)
            {
                HttpUtility.HtmlEncode(streetLine2, w);
                w.WriteLine("<br>");
            }
            if (streetLine3 != null && streetLine3.Length > 0)
            {
                HttpUtility.HtmlEncode(streetLine3, w);
                w.WriteLine("<br>");
            }
            HttpUtility.HtmlEncode(city + ", " + state + "  " + zipCode, w);

            return w.ToString();
        }

        /// <summary>
        /// Gets the address as a formattted single line string value.
        /// </summary>
        public static string SingleLineAddressWithLocationNumber(string streetLine1, string streetLine2, string city, string state, string zipCode, int locationNumber)
        {
            StringBuilder sb = new StringBuilder(200);

            bool hasLine1 = (streetLine1 != null && streetLine1.Length > 0);
            bool hasLine2 = (streetLine2 != null && streetLine2.Length > 0);
            bool hasCity = (city != null && city.Length > 0);
            bool hasState = (state != null && state.Length > 0);
            bool hasZip = (zipCode != null && zipCode.Length > 0);

            StringWriter w = new StringWriter();

            if (hasLine1)
            {
                sb.Append(streetLine1);
                if (hasLine2 || hasCity || hasState || hasZip)
                {
                    sb.Append(", ");
                }
            }
            if (hasLine2)
            {
                sb.Append(streetLine2);
                if (hasCity || hasState || hasZip)
                {
                    sb.Append(", ");
                }
            }
            if (hasCity)
            {
                sb.Append(city);
                if (hasState || hasZip)
                {
                    sb.Append(", ");
                }
            }
            if (hasState)
            {
                sb.Append(state);
                if (hasZip)
                {
                    sb.Append("  ");
                }
            }
            if (hasZip)
            {
                sb.Append(zipCode);
            }

            string result = sb.ToString();
            return (result.Length > 0) ? string.Format("#{0} - {1}", locationNumber, result): string.Empty;
        }

        /// <summary>
        /// Formats the passed value into a sender address format.
        /// </summary>
        /// <param name="value">Converts the small int to a UI displayed Yes/No or Not Specified.</param>
        public static string BoolToYesNo(short value, string nullValue)
        {
            string result = nullValue;

            if (value == 0)
            {
                result = "No";
            }
            else if (value == 1)
            {
                result = "Yes";
            }

            return result;
        }
    }
}