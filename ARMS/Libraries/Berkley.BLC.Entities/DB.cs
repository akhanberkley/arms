using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Reflection;

//using Berkley.BLC.Business.UIMapping;
using Wilson.ORMapper;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Container for all high-level database related methods.
    /// </summary>
    public sealed class DB
    {
        private const string MAP_FILE = "DBMap.xml";
        private static string _connectionString = null;
        private static ObjectSpace _engine = null;

        /// <summary>
        /// Private constructor to prevent object instantiation of this class.
        /// </summary>
        private DB()
        {
        }


        /// <summary>
        /// Determines whether or not a connection to the data source has been established.
        /// </summary>
        public static bool Initialized
        {
            get
            {
                return (_connectionString != null);
            }
        }

        /// <summary>
        /// Gets the connection string used for all data access preformed by this class.
        /// </summary>
        public static string ConnectionString
        {
            get { return _connectionString; }
        }

        /// <summary>
        /// Gets the instance of the Wilson ObjectSpace engine being used by the application.
        /// Singleton pattern is used to ensure only one instance exists.
        /// </summary>
        public static ObjectSpace Engine
        {
            get
            {
                if (_engine == null)
                {
                    if (_connectionString == null)
                    {
                        throw new InvalidOperationException("Initalize() method must be called before this property can be used.");
                    }
                    _engine = CreateEngine(_connectionString, MAP_FILE);

#if DEBUG_WORM
                    WormLogger logger = new WormLogger(Console.Out);
                    _engine.SetInterceptor(logger);
#endif
                }
                return _engine;
            }
        }


        /// <summary>
        /// Initializes the WORM engine and the UIMapper for use.
        /// </summary>
        /// <param name="connStr">The connection string to be used for all data access.</param>
        public static void Initialize(string connStr)
        {
            // initialize the field helper engine (all business entities depend on it)
            UIMapper.Initialize(MAP_FILE);

            // keep the connection string around to initalize the WORM engine with when first used
            _connectionString = connStr;
        }


        /// <summary>
        /// Creates a new SqlConnection based on the default connection string.
        /// The connection object returned must be opened by the caller before use.
        /// </summary>
        /// <returns>A new transation object for user by the caller.</returns>
        public static SqlConnection GetNewConnection()
        {
            // call our worker overload default the connection open state to closed
            return GetNewConnection(false);
        }

        /// <summary>
        /// Creates a new SqlConnection based on the default connection string and opens it if requested to do so.
        /// </summary>
        /// <param name="openConnection">Flag indicating whether the connection should be returned in an open state.</param>
        /// <returns>A new transation object for user by the caller.</returns>
        public static SqlConnection GetNewConnection(bool openConnection)
        {
            SqlConnection conn = new SqlConnection(_connectionString);
            conn.ConnectionTimeout.Equals(380);
            if (openConnection) conn.Open();
            return conn;
        }


        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        internal static Array FetchArray(OPathQuery query, params object[] parameters)
        {
            query.CommandTimeout = 380;
            IList list = DB.Engine.GetObjectSet(query, parameters);

            Array result = Array.CreateInstance(query.ObjectType, list.Count);
            list.CopyTo(result, 0);

            return result;
        }

        /// <summary>
        /// Fetches an array of entity objects from the database using the specified query.
        /// </summary>
        /// <param name="query">OPath query expression to use to filter the records.</param>
        /// <param name="parameters">Parameter values to use with executing the query.</param>
        /// <returns>Array filled with entity objects returned from the database.</returns>
        public static Array FetchArray(CompiledQuery query, params object[] parameters)
        {
            IList list = DB.Engine.GetObjectSet(query, parameters);

            Array result = Array.CreateInstance(query.ObjectType, list.Count);
            list.CopyTo(result, 0);

            return result;
        }


        private static ObjectSpace CreateEngine(string connString, string mapPath)
        {
            // initialize the WORM engine
            using (Stream mappings = Assembly.GetExecutingAssembly().GetManifestResourceStream(typeof(DB), MAP_FILE))
            {
                // NOTE:
                // sessionMinutes = Number of minutes an instance is cached after the last access.
                // cleanupMinutes = Number of minutes between cleanup processes, which scans the cache for old instances.
                return new ObjectSpace(mappings, connString, Provider.MsSql, 5, 5);
            }
        }
    }

    internal class WormLogger : Wilson.ORMapper.IInterceptCommand
    {
        //private TextWriter _writer;

        public WormLogger(TextWriter writer)
        {
            //_writer = writer;
        }

        public void InterceptCommand(Guid transactionID, Type entityType, Wilson.ORMapper.CommandInfo commandInfo, IDbCommand dbCommand)
        {
            //_writer.WriteLine();
            //_writer.WriteLine(dbCommand.CommandText);

            Debug.WriteLine("--------------------");
            if (entityType != null)
            {
                Debug.WriteLine("[" + entityType.Name + "]");
            }
            Debug.WriteLine(dbCommand.CommandText);
            foreach (SqlParameter p in dbCommand.Parameters)
            {
                string line = string.Format("{0} = {1} ({2})",
                    p.ParameterName,
                    (p.Value != null && p.Value != DBNull.Value ? p.Value.ToString() : "null"),
                    p.DbType.ToString());

                Debug.WriteLine(line);
            }
        }
    }
}