using System;
using System.Reflection;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Contains all details for a mapped field.
    /// </summary>
    public class UIFieldInfo
    {
        private Type _entityType;
        private Enum _enumValue;
        private Type _dataType;
        private PropertyInfo _propertyInfo;

        private string _displayName;
        private bool _isNullable;
        private object _nullValue;
        private bool _isRequired;
        private int _maxLength;
        private int _minLength;
        private int _columns;
        private int _rows;
        private string _format;
        private string _inputMask;

        /// <summary>
        /// Created a new instance of this class.
        /// </summary>
        /// <param name="entityType">Type containing the field.</param>
        /// <param name="field">Field to be mapped.</param>
        /// <param name="fieldType">Data type of the field.</param>
        public UIFieldInfo(Type entityType, Enum field, Type fieldType)
        {
            if (entityType == null) throw new ArgumentNullException("entityType");

            _entityType = entityType;
            _enumValue = field;
            _dataType = fieldType;

            _propertyInfo = this.EntityType.GetProperty(this.EnumValue.ToString());
            if (_propertyInfo == null)
            {
                throw new Exception("Entity '" + this.EntityType.Name + "' does not have a property named '" + this.EnumValue.ToString() + "' defined.");
            }
            if (_propertyInfo.PropertyType != _dataType)
            {
                throw new Exception("Actual data type of property '" + this.EnumValue.ToString() + "' in entity '" + this.EntityType.Name + "' does not match the one specified.");
            }
        }


        /// <summary>
        /// Gets the entity type containing the mapped field.
        /// </summary>
        public Type EntityType
        {
            get { return _entityType; }
        }

        /// <summary>
        /// Gets the enumerated value that represents the mapped field.
        /// </summary>
        public Enum EnumValue
        {
            get { return _enumValue; }
        }

        /// <summary>
        /// Gets the data type of the mapped field.
        /// </summary>
        public Type DataType
        {
            get { return _dataType; }
        }

        /// <summary>
        /// Gets the reflected PropertyInfo for the mapped field.
        /// </summary>
        public System.Reflection.PropertyInfo PropertyInfo
        {
            get { return _propertyInfo; }
        }

        /// <summary>
        /// Gets the human-readable display name of this field.
        /// </summary>
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating if this field allows nulls.
        /// </summary>
        public bool IsNullable
        {
            get { return _isNullable; }
            set { _isNullable = value; }
        }

        /// <summary>
        /// Gets or sets the value that is used to indicate 'null'.
        /// </summary>
        public object NullValue
        {
            get { return _nullValue; }
            set { _nullValue = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating if this field requires a value.
        /// </summary>
        public bool IsRequired
        {
            get { return _isRequired; }
            set { _isRequired = value; }
        }

        /// <summary>
        /// Gets or sets the maximum number of characters that can be entered.
        /// This property is used mainly for string fields.
        /// </summary>
        public int MaxLength
        {
            get { return _maxLength; }
            set { _maxLength = value; }
        }

        /// <summary>
        /// Gets or sets the minimum number of characters that can be entered.
        /// This property is used mainly for string fields.
        /// </summary>
        public int MinLength
        {
            get { return _minLength; }
            set { _minLength = value; }
        }

        /// <summary>
        /// Gets or sets the input width when the field is mapped to a TextBox.
        /// </summary>
        public int Columns
        {
            get { return _columns; }
            set { _columns = value; }
        }

        /// <summary>
        /// Gets or sets the input height when the field is mapped to a TextBox.
        /// </summary>
        public int Rows
        {
            get { return _rows; }
            set { _rows = value; }
        }

        /// <summary>
        /// Gets or sets the regular expression pattern all values must match.
        /// </summary>
        public string Format
        {
            get { return _format; }
            set { _format = value; }
        }

        /// <summary>
        /// Gets or sets the name of the client-side input mask/auto format to use on the input field.
        /// </summary>
        public string InputMask
        {
            get { return _inputMask; }
            set { _inputMask = value; }
        }
    }
}

