using System;
using System.Collections;

namespace Berkley.BLC.Entities
{
    /// <summary>
    /// Provided a static cache of PermissionSet objects related to Users and Roles.
    /// </summary>
    public class PermissionSetCache
    {
        private static Hashtable userCache = Hashtable.Synchronized(new Hashtable());
        private static Hashtable roleCache = Hashtable.Synchronized(new Hashtable());
        private static Hashtable roleToUserMap = Hashtable.Synchronized(new Hashtable()); // maps role to a hash of users
        private static object SyncRoot = new object();

        private PermissionSetCache()
        {
            // prevent object instantiation
        }


        /// <summary>
        /// Gets the effecive PermissionSet for a UserIdentity, which is a merge between the 
        /// permissions for the user's assigned role and permissions given to them specifically.
        /// </summary>
        /// <param name="identity">ID of the user for which to get permissions.</param>
        /// <returns>A PermissionSet that represents the permissions currently assigned to this user.</returns>
        /// <remarks>This method is provided for use by the UserIdentity class to avoid it having to load the user page hit.</remarks>
        internal static PermissionSet Fetch(UserIdentity identity)
        {
            PermissionSet result = (PermissionSet)userCache[identity.UserID];
            if (result == null)
            {
                User user = User.Get(identity.UserID);
                result = Fetch(user);
            }
            return result;
        }


        /// <summary>
        /// Gets the effecive PermissionSet for a User, which is a merge between the 
        /// permissions for the user's assigned role and permissions given to them specifically.
        /// </summary>
        /// <param name="user">The user for which to get permissions.</param>
        /// <returns>A PermissionSet that represents the permissions currently assigned to this user.</returns>
        public static PermissionSet Fetch(User user)
        {
            PermissionSet result = (PermissionSet)userCache[user.ID];
            if (result == null)
            {
                lock (SyncRoot)
                {
                    // get the permission set related to this user's role
                    result = Fetch(user.Role);

                    // merge the user's set if one is specified
                    if (user.PermissionSetID != Guid.Empty)
                    {
                        //result = PermissionSet.Merge(result, user.PermissionSet);
                    }

                    // relate this user to their role (for effecient role clean up)
                    MapUserToRole(user, user.Role);

                    userCache[user.ID] = result;
                }
            }
            return result;
        }

        /// <summary>
        /// Gets the PermissionSet to an associated to a Role.
        /// </summary>
        /// <param name="role">The role for which to get the permissions.</param>
        /// <returns>A PermissionSet that represents the permissions currently assigned to this user.</returns>
        public static PermissionSet Fetch(UserRole role)
        {
            PermissionSet result = (PermissionSet)roleCache[role.ID];
            if (result == null)
            {
                lock (SyncRoot)
                {
                    result = role.PermissionSet;
                    roleCache[role.ID] = result;
                }
            }
            return result;
        }


        /// <summary>
        /// Clears the permissions associated to a User from the cache.
        /// </summary>
        /// <param name="user">The user to clear from the cache.</param>
        public static void Clear(User user)
        {
            lock (SyncRoot)
            {
                userCache.Remove(user.ID);
            }
        }

        /// <summary>
        /// Clears the permissions associated to a Role from the cache.
        /// </summary>
        /// <param name="role">The role to clear from the cache.</param>
        public static void Clear(UserRole role)
        {
            lock (SyncRoot)
            {
                roleCache.Remove(role.ID);

                // remove permissions for all users mapped to this role
                Hashtable users = (Hashtable)roleToUserMap[role.ID];
                if (users != null)
                {
                    foreach (Guid key in users.Keys)
                    {
                        userCache.Remove(key);
                    }

                    // reset the user map
                    users.Clear();
                }
            }
        }


        private static void MapUserToRole(User user, UserRole role)
        {
            lock (SyncRoot)
            {
                Hashtable users = (Hashtable)roleToUserMap[role.ID];
                if (users == null)
                {
                    users = new Hashtable();
                    roleToUserMap[role.ID] = users;
                }
                if (!users.ContainsKey(user.ID))
                {
                    users.Add(user.ID, null);
                }
            }
        }
    }
}
