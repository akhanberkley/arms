using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Configuration;

using Berkley.BLC.Entities;
using QCI.ExceptionManagement;
using Wilson.ORMapper;

namespace Berkley.BLC.InsuredStatus.Utility
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Initialize DB
                DB.Initialize(Berkley.BLC.Core.CoreSettings.ARMSConnectionString);

                if (args == null || args.Length == 0) // no args, process all companies
                {
                    Company[] companies = Company.GetArray("", null);
                    foreach (Company company in companies)
                    {
                        List<String> writtenStatuses = new List<String>();
                        List<String> nonRenewedStatuses = new List<String>();
                        
                        ProcessNonRenewed(company);

                        try {writtenStatuses.AddRange(ConfigurationManager.AppSettings[company.Abbreviation + "_ClearanceWrittenStatuses"].Split(','));}
                        catch (NullReferenceException) { /*swallow exception*/}

                        try {nonRenewedStatuses.AddRange(ConfigurationManager.AppSettings[company.Abbreviation + "_ClearanceNonRenewedStatuses"].Split(',')); }
                        catch (NullReferenceException) { /*swallow exception*/}

                        if (writtenStatuses.Count > 0 || nonRenewedStatuses.Count > 0)
                        {
                            ProcessProspects(company, writtenStatuses, nonRenewedStatuses);
                        }
                    }
                }
                else
                {
                    foreach (string arg in args)
                    {
                        Company company = Company.GetOne("Abbreviation = ?", arg);
                        if (company != null)
                        {
                            List<String> writtenStatuses = new List<String>();
                            List<String> nonRenewedStatuses = new List<String>();
                            
                            ProcessNonRenewed(company);

                            try { writtenStatuses.AddRange(ConfigurationManager.AppSettings[company.Abbreviation + "_ClearanceWrittenStatuses"].Split(',')); }
                            catch (NullReferenceException) { /*swallow exception*/}

                            try { nonRenewedStatuses.AddRange(ConfigurationManager.AppSettings[company.Abbreviation + "_ClearanceNonRenewedStatuses"].Split(',')); }
                            catch (NullReferenceException) { /*swallow exception*/}

                            if (writtenStatuses.Count > 0 || nonRenewedStatuses.Count > 0)
                            {
                                ProcessProspects(company, writtenStatuses, nonRenewedStatuses);
                            }
                        }
                        else
                        {
                            throw new NotSupportedException(string.Format("Not expecting company '{0}'", arg));
                        } 
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
                ExceptionManager.Publish(ex);
            }
        }

        private static void ProcessNonRenewed(Company company)
        {
            Console.WriteLine(string.Format("Checking Insureds for {0}.", company.Name));
            Guid surveyID = Guid.Empty;
            
            //Retrieve all the non-closed survey's
            Survey[] surveys = Survey.GetArray("Status.TypeCode != ? && !ISNULL(PrimaryPolicyID) && CreateStateCode != ? && CreateStateCode != ? && CompanyID = ? && PrimaryPolicy.Symbol != ? && ISNULL(WorkflowSubmissionNumber)", SurveyStatusType.Completed.Code, CreateState.InProgress.Code, CreateState.Migrated.Code, company.ID, "UNK");
            //Survey[] surveys = Survey.GetArray("ID = ?", "ea78d91e-5c68-41bf-84da-1b099b038efb");
            //Initialize the variables
            Hashtable list = new Hashtable();
            DateTime timeStamp = DateTime.Now;
            int counter = 0;

            foreach (Survey survey in surveys)
            {
                try
                {
                    Insured importedInsured;
                    surveyID = survey.ID;
                    if (list.ContainsKey(survey.PrimaryPolicy.Number))
                    {
                        importedInsured = (Insured)list[survey.PrimaryPolicy.Number];
                    }
                    else
                    {
                        Import.ImportFactory factory = new Import.ImportFactory(company);

                        string policyNumber = StripNonNumericChars(survey.PrimaryPolicy.Number);

                        //check if a portfolio id exist for this client
                        string clientID = survey.Insured.ClientID;
                        if (company.SupportsPortfolioID && survey.Insured.ClientID != survey.Insured.PortfolioID)
                        {
                            Insured insured = Insured.GetOne("CompanyID = ? && ClientID = ? && !ISNULL(PortfolioID)", company.ID, clientID);
                            if (insured != null)
                            {
                                clientID = insured.PortfolioID;
                            }
                        }
                        importedInsured = factory.ImportOnlyInsuredForAccountStatus(policyNumber, clientID);

                        //cache the recently imported insured for possible later use
                        list.Add(survey.PrimaryPolicy.Number, importedInsured);
                    }

                    if (importedInsured != null && ((survey.Insured.NonrenewedDate == DateTime.MinValue && importedInsured.NonrenewedDate != DateTime.MinValue) ||
                       (survey.Insured.NonrenewedDate != DateTime.MinValue && importedInsured.NonrenewedDate == DateTime.MinValue)))
                    {
                        //update the non-renewed field based on whatever was returned from the policy system
                        Insured insuredToUpdate = survey.Insured.GetWritableInstance();

                        insuredToUpdate.NonrenewedDate = importedInsured.NonrenewedDate;
                        insuredToUpdate.Save();

                        counter++;
                    }
                    else if (importedInsured == null && survey.Insured.NonrenewedDate == DateTime.MinValue)
                    {
                        //the imported insured no longer returned from policy system, set the Non-Renewed date
                        Insured insuredToUpdate = survey.Insured.GetWritableInstance();

                        insuredToUpdate.NonrenewedDate = DateTime.Now;
                        insuredToUpdate.Save();

                        counter++;
                    }
                }
                catch (Exception ex)
                {
                    Exception synchException = new Exception(string.Format("Synch failed with survey id '{0}'. See inner exception for details.", surveyID), ex);

                    Console.Write(synchException.ToString());
                    ExceptionManager.Publish(synchException);
                }
            }

            Console.WriteLine(string.Format("Found {0}/{1} surveys with non-renewed accounts in {2}.", counter, surveys.Length, (DateTime.Now - timeStamp)));
        }

        private static void ProcessProspects(Company company, List<String> arrWrittenStatuses, List<String> arrNonRenewedStatuses)
        {
            Console.WriteLine(string.Format("Checking Prospects for {0}.", company.Name));
            Guid surveyID = Guid.Empty;

            //Retrieve all the non-closed prospect surveys
            Survey[] surveys = Survey.GetArray("Type.TypeCode = ? && Status.TypeCode != ? && !ISNULL(WorkflowSubmissionNumber) && CreateStateCode != ? && CreateStateCode != ? && CompanyID = ?", SurveyTypeType.Prospect.Code, SurveyStatusType.Completed.Code, CreateState.InProgress.Code, CreateState.Migrated.Code, company.ID);

            //Initialize the variables
            DateTime timeStamp = DateTime.Now;
            int counter = 0;

            foreach (Survey survey in surveys)
            {
                try
                {
                    Import.ImportFactory factory = new Import.ImportFactory(company);
                    surveyID = survey.ID;

                    //Get the current status in Clearance
                    string submissionNumber = StripNonNumericChars(survey.WorkflowSubmissionNumber);
                    string submissionStatus = factory.ImportSubmissionStatus(submissionNumber);

                    if (submissionStatus != null)
                    {
                        if (arrWrittenStatuses.Count > 0)
                        {
                            if (arrWrittenStatuses.Contains(submissionStatus))
                            {
                                Survey writableSurvey = survey.GetWritableInstance();

                                using (Transaction trans = DB.Engine.BeginTransaction())
                                {
                                    //update the survey type to "Prospect Written" in order to indicate the type has changed
                                    writableSurvey.TypeID = SurveyType.GetOne(SurveyTypeType.ProspectWritten.Code, company);
                                    writableSurvey.Save(trans);

                                    //Add a history comment to let the users know of the change
                                    SurveyHistory history = new SurveyHistory(Guid.NewGuid());
                                    history.SurveyID = writableSurvey.ID;
                                    history.EntryDate = DateTime.Now;
                                    history.EntryText = string.Format("ARMS changed the survey type from '{0}' to '{1}'.", survey.Type.Type.Name, SurveyTypeType.ProspectWritten.Name);
                                    history.Save(trans);

                                    trans.Commit();
                                }

                                counter++;
                            }
                        }

                        if (arrNonRenewedStatuses.Count > 0)
                        {
                            //Check if the prospect non-renewed/canceled
                            if (survey.Insured.NonrenewedDate == DateTime.MinValue && arrNonRenewedStatuses.Contains(submissionStatus))
                            {
                                //update the non-renewed field based on whatever was returned from the Clearance system
                                Insured insuredToUpdate = survey.Insured.GetWritableInstance();

                                insuredToUpdate.NonrenewedDate = DateTime.Today;
                                insuredToUpdate.Save();
                            }
                            else if (survey.Insured.NonrenewedDate != DateTime.MinValue && !arrNonRenewedStatuses.Contains(submissionStatus))
                            {
                                //update the non-renewed field based on whatever was returned from the Clearance system
                                Insured insuredToUpdate = survey.Insured.GetWritableInstance();

                                insuredToUpdate.NonrenewedDate = DateTime.MinValue;
                                insuredToUpdate.Save();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exception synchException = new Exception(string.Format("Synch failed with survey id '{0}'. See inner exception for details.", surveyID), ex);

                    Console.Write(synchException.ToString());
                    ExceptionManager.Publish(synchException);
                }
            }

            Console.WriteLine(string.Format("Found {0}/{1} prospect to written surveys in {2}.", counter, surveys.Length, (DateTime.Now - timeStamp)));
        }

        private static string StripNonNumericChars(string number)
        {
            string result = string.Empty;
            
            char[] characters = number.ToCharArray();
            foreach (char character in characters)
            {
                if (Char.IsDigit(character))
                {
                    result += character;
                }
            }

            return result;
        }
    }
}
