using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public abstract class BaseCompanyEntity : ICompanyEntity
	{
		private Dictionary<int, int> moColumnMapDictionary = new Dictionary<int, int>();

		public BaseCompanyEntity()
		{
			BuildMap(ref moColumnMapDictionary);
		}

		public Dictionary<int, int> MapDictionary
		{
			get { return moColumnMapDictionary; }
		}

		protected abstract void BuildMap(ref Dictionary<int, int> oColumnMapDictionary);

		#region ICompanyEntity Members

		public int this[int iKey]
		{
			get { return moColumnMapDictionary[iKey]; }
			set { moColumnMapDictionary[iKey] = value; }
		}

		public Dictionary<int, int> ColumnMapDictionary
		{
			get { return moColumnMapDictionary; }
		}

		public int GetColumn(int iKey)
		{
			return moColumnMapDictionary[iKey];
		}

		public void SetColumn(int iKey, int iNewValue)
		{
			moColumnMapDictionary[iKey] = iNewValue;
		}

		#endregion

	}
}
