using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

using Berkley.BLC.Business;
using Berkley.BLC.Core;
using Berkley.BLC.Import.Properties;
using Berkley.BLC.Entities;

namespace Berkley.BLC.Import
{
	public class BMAGImportBuilder : BaseImportBuilder, IImportBuilder
	{
		private BlcStoredProcedure moStoredProcedure = null;
		private CompanyPolicyEntity moPolicyFieldEntity = new CompanyPolicyEntity();
        private CompanyInsuredEntity moInsuredFieldEntity = new CompanyInsuredEntity();
		private CompanyAgencyEntity moAgencyFieldEntity = new CompanyAgencyEntity();
		private CompanyLocationEntity moLocationFieldEntity = new CompanyLocationEntity();
		private CompanyBuildingEntity moBuildingFieldEntity = new CompanyBuildingEntity();
		private CompanyClaimEntity moCompanyClaimEntity = new CompanyClaimEntity();
        private CompanyRateModFactorEntity moCompanyRateModFactorEntity = new CompanyRateModFactorEntity();
		private BMAGLineOfBusiness moLineOfBusiness = new BMAGLineOfBusiness();

		#region Line Of Business / Coverage
		private CompanyCoverageEntity moCoverageFieldEntity = new CompanyCoverageEntity();
		private BMAGCoverageDictionary moCoverageDictionary = new BMAGCoverageDictionary();
		#endregion

		public BMAGImportBuilder(string strClientId, string strPolicyNumber, string strAgentNumber)
		{
			base.Initialize(this, strClientId, strPolicyNumber, strAgentNumber);
			// Rearrange fields in moPolicyFieldEntity as necessary
		}

		#region IImportBuilder Members

		public BlcStoredProcedure GetStoredProcedure()
		{
			if (moStoredProcedure == null)
			{
				string strConnection = CoreSettings.BMAGConnectionString;
				CompanySpEntityList oList = new CompanySpEntityList(strConnection, base.ClientId, base.PolicyNumber, base.AgentNumber);
				oList.Load(Settings.Default.BMAGImportFileList);
				moStoredProcedure = new BlcStoredProcedure(oList);
			}
			return moStoredProcedure;
		}

		public BlcStoredProcedure GetStoredProcedure(CompanySpEntity.SpType eType)
		{
			if (moStoredProcedure == null)
			{
				string strConnection = CoreSettings.BMAGConnectionString;
				CompanySpEntityList oList = new CompanySpEntityList(strConnection, base.ClientId, base.PolicyNumber, base.AgentNumber);
				oList.Load(Settings.Default.BMAGImportFileList);
				moStoredProcedure = new BlcStoredProcedure(oList, eType);
			}
			return moStoredProcedure;
		}


		private string TrimPolicyNumber(string strPolicyNumber)
		{
			if (strPolicyNumber.Length > 2)
			{
				string strReturn = strPolicyNumber.Substring(2, strPolicyNumber.Length - 2);
				return strReturn;
			}
			else
			{
				return strPolicyNumber;
			}
		} 

		public BlcInsured BuildComplete(BlcInsured oBlcInsured)
		{
			// The BlcDataSet has been filled out.  Do the Insert into the BLC DB
			// If all of the company (derived) classes do the same thing, put the basic insert/update
			// in BaseImportBuilder
			Policy oPolicy;
			BMAGLineOfBusiness oLineOfBusiness = new BMAGLineOfBusiness();
			LineOfBusiness.LineOfBusinessType eType;
			List<Policy> oPolicyList = oBlcInsured.PolicyList;
			for(int i=0; i < oPolicyList.Count; i++)
			{
				oPolicy = oPolicyList[i];
				eType = oLineOfBusiness.GetType(oPolicy.Symbol);
				oPolicy.LineOfBusinessCode = oLineOfBusiness.GetBlcName(eType);
				oPolicy.Number = TrimPolicyNumber(oPolicy.Number);
				oPolicyList[i] = oPolicy;
			}
			return oBlcInsured;
		}

		public CompanyCoverageEntity CoverageFieldEntity
		{
			get { return moCoverageFieldEntity; }
		}

		public List<CompanyCoverageColumn> GetCoverage(LineOfBusiness.LineOfBusinessType eType)
		{
			return moCoverageDictionary.GetColumnList(eType);
		}

		public int GetMaxCoverageColumns()
		{
			return moCoverageDictionary.GetColumnCount();
		}

        public int GetMaxColumnNumber()
        {
            return moCoverageDictionary.GetMaxColumnNumber();
        }
		
		// The derived classes can change the field mapping
		public CompanyPolicyEntity PolicyFieldEntity 
		{
			get { return moPolicyFieldEntity; } 
		}

        // The derived classes can change the field mapping
        public CompanyInsuredEntity InsuredFieldEntity
        {
            get { return moInsuredFieldEntity; }
        }

		public CompanyAgencyEntity AgencyFieldEntity 
		{
			get { return moAgencyFieldEntity; }
		}

		public CompanyLocationEntity LocationFieldEntity
		{
			get { return moLocationFieldEntity; }
		}

		public CompanyBuildingEntity BuildingFieldEntity
		{
			get { return moBuildingFieldEntity; }
		}

		public LineOfBusiness LineOfBusinessFieldEntity
		{
			get { return moLineOfBusiness; }
		}

		public CompanyClaimEntity ClaimFieldEntity
		{
			get { return moCompanyClaimEntity; }
		}

        public CompanyRateModFactorEntity RateModFactorFieldEntity
        {
            get { return moCompanyRateModFactorEntity; }
        }

		public string CompanyAbbreviation 
		{
			get { return "BMAG"; }
		}


		#endregion
	}
}
