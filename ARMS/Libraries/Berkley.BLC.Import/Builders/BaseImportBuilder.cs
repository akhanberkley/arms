using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Berkley.BLC.Business;
using Berkley.BLC.Entities;

namespace Berkley.BLC.Import
{
    /// <summary>
    /// This class 1. Contains common functions for use in DB Access
    ///            2. Defines the Entities/classes that make up a Survey Insert into the BLC Database
    /// </summary>
    public class BaseImportBuilder
    {
        private IImportBuilder moImportBuilder;
        private BlcInsured moBlcInsured = new BlcInsured();
        private string mstrClientId;
        private string mstrPolicyNumber;
        private string mstrAgentNumber;
        private List<string> moProperCaseExclusions = new List<string>();
        private List<string> dbaKeyWords = new List<string>();
        private List<string> policyList = new List<string>();
        private List<string> locationKeys = new List<string>();

        public BaseImportBuilder()
        {
        }

        public void Initialize(IImportBuilder oImportBuilder, string strClientId, string strPolicyNumber, string strAgentNumber)
        {
            moImportBuilder = oImportBuilder;
            mstrClientId = strClientId;
            mstrPolicyNumber = strPolicyNumber;
            mstrAgentNumber = strAgentNumber;

            moProperCaseExclusions.Add("LLC");
            moProperCaseExclusions.Add("PO");
            moProperCaseExclusions.Add("P.O.");
            moProperCaseExclusions.Add("DBA");
            moProperCaseExclusions.Add("NW");
            moProperCaseExclusions.Add("NE");
            moProperCaseExclusions.Add("SW");
            moProperCaseExclusions.Add("SE");

            //TODO: Impliment this list check
            dbaKeyWords.Add("INSURED");
            dbaKeyWords.Add("ENDORSEMENT");
        }

        protected string ClientId
        {
            get { return mstrClientId; }
        }

        protected string PolicyNumber
        {
            get { return mstrPolicyNumber; }
        }

        protected string AgentNumber
        {
            get { return mstrAgentNumber; }
        }

        protected string SearchParameters
        {
            get
            {
                string strReturn = "";
                // Trim off the 2 digit pre-pended company ID
                if (mstrPolicyNumber != null && mstrPolicyNumber.Length > 0)
                {
                    string strPolicy = mstrPolicyNumber.Length >= 2 ? mstrPolicyNumber.Substring(2, mstrPolicyNumber.Length - 2) : mstrPolicyNumber;
                    strReturn = "Policy Number: " + strPolicy + " ";
                }
                if (mstrClientId != null && mstrClientId.Length > 0)
                {
                    strReturn += "Client ID: " + mstrClientId + " ";
                }
                if (mstrAgentNumber != null && mstrAgentNumber.Length > 0)
                {
                    strReturn += "Agency Number: " + mstrAgentNumber + " ";
                }
                return strReturn;
            }
        }

        /// <summary>
        /// Trims a Zip code to only 5 digits
        /// </summary>
        /// <param name="strZipCode">The zip code</param>
        public static string TrimZipCode(string strZipCode)
        {
            if (strZipCode.Length > 5)
            {
                for (int i = strZipCode.Length - 1; i >= strZipCode.Length - 4; i--)
                {
                    if (strZipCode[i] != '0')
                    {
                        return strZipCode;
                    }
                }
                return strZipCode.Substring(0, 5);
            }
            else
            {
                return strZipCode;
            }
        }

        public BlcInsured[] GetAllInsured()
        {
            BlcStoredProcedure oStoredProcedure = moImportBuilder.GetStoredProcedure(CompanySpEntity.SpType.Insured);
            DataTable oDataTable;
            Dictionary<CompanySpEntity.SpType, DataTable> oCompanyEntities = oStoredProcedure.DataTables;
            List<BlcInsured> oInsuredList = new List<BlcInsured>();

            // Fill in all Company Entites here.  The list of stored procedures in the .config file
            // will dictate the parts of the BlcDataSet that get filled in.
            foreach (CompanySpEntity.SpType eType in oCompanyEntities.Keys)
            {
                oDataTable = oCompanyEntities[eType];
                switch (eType)
                {
                    case CompanySpEntity.SpType.Insured:
                        // Add the one agency.
                        if (oDataTable.Rows.Count > 0)
                        {
                            foreach (DataRow oRow in oDataTable.Rows)
                            {
                                BlcInsured oInsured = new BlcInsured();
                                BuildInsured(oRow, ref oInsured);
                                oInsuredList.Add(oInsured);
                            }
                        }
                        else
                        {
                            throw new BlcNoInsuredException("Insured not found for " + SearchParameters);
                        }
                        break;

                    default:
                        break;
                }
            }
            return oInsuredList.ToArray();
        }

        public Agency BuildForAgency()
        {
            BlcStoredProcedure oStoredProcedure = moImportBuilder.GetStoredProcedure(CompanySpEntity.SpType.Agency);
            DataTable oDataTable;
            Dictionary<CompanySpEntity.SpType, DataTable> oCompanyEntities = oStoredProcedure.DataTables;

            // Fill in all Company Entites here.  The list of stored procedures in the .config file
            // will dictate the parts of the BlcDataSet that get filled in.
            foreach (CompanySpEntity.SpType eType in oCompanyEntities.Keys)
            {
                oDataTable = oCompanyEntities[eType];
                switch (eType)
                {
                    case CompanySpEntity.SpType.Agency:
                        //Add the one agency.
                        if (oDataTable.Rows.Count == 1)
                        {
                            moBlcInsured.AgencyEntity = BuildAgency(oDataTable.Rows[0]);
                            return moBlcInsured.AgencyEntity;
                        }
                        else
                        {
                            throw new BlcImportException("Agency not found for " + SearchParameters);
                        }

                    default:
                        break;
                }
            }
            return null;
        }

        public Insured BuildForAccountStatus()
        {
            BlcStoredProcedure oStoredProcedure = moImportBuilder.GetStoredProcedure(CompanySpEntity.SpType.Policy);
            Dictionary<CompanySpEntity.SpType, DataTable> oCompanyEntities = oStoredProcedure.DataTables;

            Insured insured = null;
            foreach (CompanySpEntity.SpType eType in oCompanyEntities.Keys)
            {
                if (eType == CompanySpEntity.SpType.Policy)
                {
                    DataTable dt = oCompanyEntities[eType];
                    ArrayList policyStatusList = new ArrayList();

                    if (dt.Rows.Count > 0)
                    {
                        insured = new Insured(Guid.NewGuid());
                        foreach (DataRow row in dt.Rows)
                        {
                            BuildPolicy(row, ref policyStatusList);
                        }

                        //Set the account status based on the statuses of all the insured's policies
                        //Only if all policies are non-renewed, is the client set as non-renewed
                        insured.NonrenewedDate = (!policyStatusList.Contains("R")) ? DateTime.Now : DateTime.MinValue;
                    }
                }
            }
            return insured;
        }

        /// <summary>
        /// Call all of the company stored procedures and returns the aggregate data.
        /// </summary>
        /// <param name="iInsuredIndex">Zero based. Get the iInsuredIndex by calling the GetAllInsured() and picking the correct index.</param>
        /// <returns>A BlcInsured filled out with data from all of the stored procedures</returns>
        public BlcInsured BuildForPolicies()
        {
            BlcStoredProcedure oStoredProcedure = moImportBuilder.GetStoredProcedure();
            DataTable oDataTable;
            DataTable dtBuildings = new DataTable();
            DataTable dtPolicies = new DataTable();
            Dictionary<CompanySpEntity.SpType, DataTable> oCompanyEntities = oStoredProcedure.DataTables;

            // Fill in all Company Entites here.  The list of stored procedures in the .config file
            // will dictate the parts of the BlcDataSet that get filled in.
            foreach (CompanySpEntity.SpType eType in oCompanyEntities.Keys)
            {
                oDataTable = oCompanyEntities[eType];
                switch (eType)
                {
                    case CompanySpEntity.SpType.Policy:
                        dtPolicies = RenameColumns(oDataTable);
                        ArrayList policyStatusList = new ArrayList();

                        //Add the many policies of the insured.
                        foreach (DataRow oRow in oDataTable.Rows)
                        {
                            Policy policy = BuildPolicy(oRow, ref policyStatusList);
                            if (policy != null)
                            {
                                moBlcInsured.PolicyList.Add(policy);
                            }
                        }
                        break;

                    case CompanySpEntity.SpType.Coverage:
                        //Add the many coverages of the policy.
                        foreach (DataRow oRow in oDataTable.Rows)
                        {
                            foreach (BlcCoverage oBlcCoverage in BuildCoverages(oRow))
                            {
                                moBlcInsured.CoverageList.Add(new CoverageKey(oBlcCoverage), oBlcCoverage);
                            }
                        }

                        //Look for policies that do not have any coverages
                        foreach (Policy policy in moBlcInsured.PolicyList)
                        {
                            if (!policyList.Contains(string.Format("{0}-{1}", policy.Number, policy.Mod)))
                            {
                                BuildEmptyCoverages(policy.Number, policy.Mod, policy.Symbol);
                            }
                        }
                        break;

                    case CompanySpEntity.SpType.Building:
                        //Add the many buildings of the location.
                        dtBuildings = RenameColumns(oDataTable);
                        foreach (DataRow oRow in oDataTable.Rows)
                        {
                            moBlcInsured.BuildingList.Add(BuildBuildings(oRow));
                        }
                        break;

                    case CompanySpEntity.SpType.Location:
                        //Add the many locations of the insured.
                        List<LocationImportBuilder> builtlocations = new List<LocationImportBuilder>();
                        ArrayList uniqueLocations = new ArrayList();

                        foreach (DataRow oRow in oDataTable.Rows)
                        {
                            builtlocations.Add(BuildLocation(oRow, dtBuildings, dtPolicies));
                        }

                        //Now that the locations are built, loop thru removing the duplicates
                        for (int i = 0; i < builtlocations.Count; i++)
                        {
                            LocationImportBuilder a = builtlocations[i];

                            for (int j = 0; j < builtlocations.Count; j++)
                            {
                                LocationImportBuilder b = builtlocations[j];

                                if (!uniqueLocations.Contains(a.Location.Number + a.Location.City) && !uniqueLocations.Contains(b.Location.Number + b.Location.City))
                                {
                                    if (a.Location.ID != b.Location.ID)
                                    {
                                        if (a.Location.Number == b.Location.Number && a.Location.City == b.Location.City)
                                        {
                                            if (b.BuildingCount > a.BuildingCount)
                                            {
                                                a = b;
                                            }

                                            if (builtlocations.Count == (i + 1))
                                            {
                                                //check if this is the last location in the list
                                                moBlcInsured.PolicyLocationList.Add(a.Location);
                                                uniqueLocations.Add(a.Location.Number + a.Location.City);
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            moBlcInsured.PolicyLocationList.Add(a.Location);
                                            uniqueLocations.Add(a.Location.Number + a.Location.City);
                                            break;
                                        }
                                    }
                                    else if (a.Location.ID == b.Location.ID && builtlocations.Count == (i + 1))
                                    {
                                        //check if this is the last location in the list
                                        moBlcInsured.PolicyLocationList.Add(a.Location);
                                        uniqueLocations.Add(a.Location.Number + a.Location.City);
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            return moImportBuilder.BuildComplete(moBlcInsured);
        }

        /// <summary>
        /// Call all of the company stored procedures and returns the aggregate data.
        /// </summary>
        /// <param name="iInsuredIndex">Zero based. Get the iInsuredIndex by calling the GetAllInsured() and picking the correct index.</param>
        /// <returns>A BlcInsured filled out with data from all of the stored procedures</returns>
        public BlcInsured Build(int iInsuredIndex)
        {
            BlcStoredProcedure oStoredProcedure = moImportBuilder.GetStoredProcedure();
            DataTable oDataTable;
            DataTable dtBuildings = new DataTable();
            DataTable dtPolicies = new DataTable();
            Dictionary<CompanySpEntity.SpType, DataTable> oCompanyEntities = oStoredProcedure.DataTables;

            // Fill in all Company Entites here.  The list of stored procedures in the .config file
            // will dictate the parts of the BlcDataSet that get filled in.
            foreach (CompanySpEntity.SpType eType in oCompanyEntities.Keys)
            {
                oDataTable = oCompanyEntities[eType];
                switch (eType)
                {
                    case CompanySpEntity.SpType.Insured:
                        // The Stored Procedure can return more than one row
                        // Build up an array of BlcInsured to return
                        int iCount = 0;
                        if (oDataTable.Rows.Count > 0)
                        {
                            if (iInsuredIndex + 1 > oDataTable.Rows.Count)
                            {
                                throw new BlcNoInsuredException(string.Format("Insured Index {0} out of bounds for {1}", iInsuredIndex, SearchParameters));
                            }

                            foreach (DataRow oRow in oDataTable.Rows)
                            {
                                if (iCount++ == iInsuredIndex)
                                {
                                    BuildInsured(oRow, ref moBlcInsured);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            throw new BlcNoInsuredException("Insured not found for " + SearchParameters);
                        }
                        break;

                    case CompanySpEntity.SpType.Agency:
                        //Add the one agency.
                        if (oDataTable.Rows.Count == 1)
                        {
                            moBlcInsured.AgencyEntity = BuildAgency(oDataTable.Rows[0]);
                        }
                        else
                        {
                            throw new BlcImportException("Agency not found for " + SearchParameters);
                        }
                        break;

                    case CompanySpEntity.SpType.Policy:
                        dtPolicies = RenameColumns(oDataTable);
                        ArrayList policyStatusList = new ArrayList();

                        //Add the many policies of the insured.
                        foreach (DataRow oRow in oDataTable.Rows)
                        {
                            Policy policy = BuildPolicy(oRow, ref policyStatusList);
                            if (policy != null)
                            {
                                moBlcInsured.PolicyList.Add(policy);
                            }
                        }

                        //Set the account status based on the statuses of all the insured's policies
                        //Only if all policies are non-renewed, is the client set as non-renewed
                        moBlcInsured.InsuredEntity.NonrenewedDate = (!policyStatusList.Contains("R")) ? DateTime.Now : DateTime.MinValue;
                        break;

                    case CompanySpEntity.SpType.Building:
                        //Add the many buildings of the location.
                        dtBuildings = RenameColumns(oDataTable);
                        foreach (DataRow oRow in oDataTable.Rows)
                        {
                            moBlcInsured.BuildingList.Add(BuildBuildings(oRow));
                        }
                        break;

                    case CompanySpEntity.SpType.Location:
                        //Add the many locations of the insured.
                        List<LocationImportBuilder> builtlocations = new List<LocationImportBuilder>();
                        ArrayList uniqueLocations = new ArrayList();

                        foreach (DataRow oRow in oDataTable.Rows)
                        {
                            builtlocations.Add(BuildLocation(oRow, dtBuildings, dtPolicies));
                        }

                        int highestLocationNumber = (builtlocations.Count > 0) ? builtlocations[builtlocations.Count - 1].Location.Number : 0;

                        //Now that the locations are built, loop thru removing the duplicates
                        for (int i = 0; i < builtlocations.Count; i++)
                        {
                            LocationImportBuilder a = builtlocations[i];

                            for (int j = 0; j < builtlocations.Count; j++)
                            {
                                LocationImportBuilder b = builtlocations[j];

                                if (!uniqueLocations.Contains(a.Location.Number + a.Location.City) && !uniqueLocations.Contains(b.Location.Number + b.Location.City))
                                {
                                    if (a.Location.ID != b.Location.ID)
                                    {
                                        if (a.Location.Number == b.Location.Number && a.Location.City == b.Location.City)
                                        {
                                            if (b.BuildingCount > a.BuildingCount)
                                            {
                                                a = b;
                                            }

                                            if (a.Location.Number == highestLocationNumber)
                                            {
                                                //check if this is the last location in the list
                                                moBlcInsured.PolicyLocationList.Add(a.Location);
                                                uniqueLocations.Add(a.Location.Number + a.Location.City);
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            moBlcInsured.PolicyLocationList.Add(a.Location);
                                            uniqueLocations.Add(a.Location.Number + a.Location.City);
                                            break;
                                        }
                                    }
                                    else if (a.Location.ID == b.Location.ID && builtlocations.Count == (i + 1))
                                    {
                                        //check if this is the last location in the list
                                        moBlcInsured.PolicyLocationList.Add(a.Location);
                                        uniqueLocations.Add(a.Location.Number + a.Location.City);
                                        break;
                                    }
                                }
                            }
                        }

                        break;

                    case CompanySpEntity.SpType.Coverage:
                        //Add the many coverages of the policy.
                        foreach (DataRow oRow in oDataTable.Rows)
                        {
                            foreach (BlcCoverage oBlcCoverage in BuildCoverages(oRow))
                            {
                                moBlcInsured.CoverageList.Add(new CoverageKey(oBlcCoverage), oBlcCoverage);
                            }
                        }

                        //Look for policies that do not have any coverages
                        foreach (Policy policy in moBlcInsured.PolicyList)
                        {
                            if (!policyList.Contains(string.Format("{0}-{1}", policy.Number, policy.Mod)))
                            {
                                BuildEmptyCoverages(policy.Number, policy.Mod, policy.Symbol);
                            }
                        }
                        break;

                    case CompanySpEntity.SpType.Claim:
                        //Add the many claims of a policy.
                        foreach (DataRow oRow in oDataTable.Rows)
                        {
                            moBlcInsured.ClaimList.Add(BuildClaim(oRow));
                        }
                        break;

                    case CompanySpEntity.SpType.RateModFactor:
                        //Add the many rate mod factors of a policy.
                        foreach (DataRow oRow in oDataTable.Rows)
                        {
                            moBlcInsured.RateModFactorList.Add(BuildRateModFactor(oRow));
                        }
                        break;
                }
            }
            return moImportBuilder.BuildComplete(moBlcInsured);
        }

        public BlcInsured BuildEmptyCoverages(string strBlcLineOfBusiness)
        {
            DataTable oTable = new DataTable();
            LineOfBusiness oLineOfBusiness = moImportBuilder.LineOfBusinessFieldEntity;
            DataRow oRow;

            for (int i = 0; i < moImportBuilder.GetMaxColumnNumber(); i++)
            {
                oTable.Columns.Add();
            }

            oRow = oTable.Rows.Add(new string[] { "", "", "", "", "", "" });
            oRow[(int)CompanyCoverageEntity.FieldColumns.PolicySymbol] = oLineOfBusiness.GetCompanyLob(strBlcLineOfBusiness);
            oRow[(int)CompanyCoverageEntity.FieldColumns.PolicyNumber] = mstrPolicyNumber;
            oRow[(int)CompanyCoverageEntity.FieldColumns.PolicyMod] = string.Empty;

            foreach (DataRow oDataRow in oTable.Rows)
            {
                foreach (BlcCoverage oBlcCoverage in BuildCoverages(oDataRow))
                {
                    moBlcInsured.CoverageList.Add(new CoverageKey(oBlcCoverage), oBlcCoverage);
                }
            }
            return moBlcInsured;
        }

        public BlcInsured BuildEmptyCoverages(string policyNumber, int policyMod, string policySymbol)
        {
            DataTable oTable = new DataTable();
            LineOfBusiness oLineOfBusiness = moImportBuilder.LineOfBusinessFieldEntity;
            DataRow oRow;

            for (int i = 0; i < moImportBuilder.GetMaxColumnNumber(); i++)
            {
                oTable.Columns.Add();
            }

            oRow = oTable.Rows.Add(new string[] { "", "", "", "", "", "" });
            oRow[(int)CompanyCoverageEntity.FieldColumns.PolicySymbol] = policySymbol;
            oRow[(int)CompanyCoverageEntity.FieldColumns.PolicyNumber] = policyNumber;
            oRow[(int)CompanyCoverageEntity.FieldColumns.PolicyMod] = policyMod;

            foreach (DataRow oDataRow in oTable.Rows)
            {
                foreach (BlcCoverage oBlcCoverage in BuildCoverages(oDataRow))
                {
                    moBlcInsured.CoverageList.Add(new CoverageKey(oBlcCoverage), oBlcCoverage);
                }
            }
            return moBlcInsured;
        }

        #region Read Routines

        private string ReadString(DataRow oRow, ICompanyEntity oICompanyEntity, int iColumn)
        {
            if (iColumn != (int)CompanyInsuredEntity.InsuredColumn.Undefined)
            {
                // Call the ICompanyEntity indexer[] which returns the mapped column number
                string strReturn = SafeRead(oRow[oICompanyEntity[iColumn]]);
                if (strReturn == "-1")
                {
                    return string.Empty;
                }
                else
                {
                    return strReturn;
                }
            }
            else
            {
                return "";
            }
        }

        private bool ReadBoolean(DataRow oRow, ICompanyEntity oICompanyEntity, int iColumn)
        {
            if (iColumn != (int)CompanyInsuredEntity.InsuredColumn.Undefined)
            {
                // Call the ICompanyEntity indexer[] which returns the mapped column number
                return Convert.ToBoolean(SafeRead(oRow[oICompanyEntity[iColumn]]));
            }
            else
            {
                return false;
            }
        }

        private DateTime ReadDateTime(DataRow oRow, ICompanyEntity oICompanyEntity, int iColumn)
        {
            if (iColumn != (int)CompanyPolicyEntity.PolicyColumn.Undefined)
            {
                // Call the ICompanyEntity indexer[] which returns the mapped column number
                string strDateTime = SafeRead(oRow[oICompanyEntity[iColumn]]);
                DateTime oReturn = ParseDateTime(strDateTime);
                return oReturn;
            }
            else
            {
                return new DateTime();
            }
        }

        private int ReadInteger(DataRow oRow, ICompanyEntity oICompanyEntity, int iColumn)
        {
            if (iColumn != (int)CompanyPolicyEntity.PolicyColumn.Undefined)
            {
                // Call the ICompanyEntity indexer[] which returns the mapped column number
                int iReturn = SafeReadInteger(oRow[oICompanyEntity[iColumn]].ToString());
                if (iReturn == -1)
                {
                    return int.MinValue;
                }
                else
                {
                    return iReturn;
                }
            }
            else
            {
                return -1;
            }
        }

        private int ReadInteger(DataRow oRow, ICompanyEntity oICompanyEntity, int iColumn, int iLength)
        {
            if (iColumn != (int)CompanyPolicyEntity.PolicyColumn.Undefined)
            {
                // Call the ICompanyEntity indexer[] which returns the mapped column number
                int result = SafeReadInteger(oRow[oICompanyEntity[iColumn]].ToString());

                int iReturn = (result.ToString().Length != iLength) ? int.MinValue : result;
                if (iReturn == -1)
                {
                    return int.MinValue;
                }
                else
                {
                    return iReturn;
                }
            }
            else
            {
                return -1;
            }
        }

        private decimal ReadDecimal(DataRow oRow, ICompanyEntity oICompanyEntity, int iColumn)
        {
            if (iColumn != (int)CompanyPolicyEntity.PolicyColumn.Undefined)
            {
                // Call the ICompanyEntity indexer[] which returns the mapped column number
                decimal dReturn = SafeReadDecimal(oRow[oICompanyEntity[iColumn]].ToString());
                if (dReturn == -1)
                {
                    return decimal.MinValue;
                }
                else
                {
                    return dReturn;
                }
            }
            else
            {
                return -1;
            }
        }

        private decimal ReadPercentage(DataRow oRow, ICompanyEntity oICompanyEntity, int iColumn)
        {
            decimal result = ReadDecimal(oRow, oICompanyEntity, iColumn);
            return (result != Decimal.MinValue) ? result / 100 : Decimal.MinValue;
        }

        private string ReadState(DataRow oRow, ICompanyEntity oICompanyEntity, int iColumn)
        {
            // Only allow valid state codes
            string strState = ReadString(oRow, oICompanyEntity, iColumn);

            return (State.GetOne("Code = ?", strState) != null) ? strState : string.Empty;
        }

        private string ReadValuationType(DataRow oRow, ICompanyEntity oICompanyEntity, int iColumn)
        {
            // Only allow valid state codes
            string result = ReadString(oRow, oICompanyEntity, iColumn);

            return (BuildingValuationType.GetOne("Code = ?", result) != null) ? result : string.Empty;
        }

        private string ReadRateModType(DataRow oRow, ICompanyEntity oICompanyEntity, int iColumn)
        {
            string strRateModType = ReadString(oRow, oICompanyEntity, iColumn);

            // Only allow valid rate mod types
            if (strRateModType != null && strRateModType.ToUpper().Contains("CNTRCT"))
            {
                return RateModificationFactorType.Construction.Code;
            }
            else if (strRateModType != null && strRateModType.ToUpper().Contains("EXPER"))
            {
                return RateModificationFactorType.Experience.Code;
            }
            else
            {
                return RateModificationFactorType.Unknown.Code;
            }
        }
        #endregion

        /*
         *	Need the agencyid populated in the insured table...  
            Also, have your code default newly created locations with the IncludedInSurvey field to true... 
            There was no premium brought thru 
            the first two digits of the policy need to be truncated 
         * */

        private BlcCoverage[] BuildCoverages(DataRow oRow)
        {
            string strValue = "";
            string strPolicyNumber = "";
            string strPolicyMod = "";
            int iColumn;
            //CoverageValue oCoverageValue;
            BlcCoverage oBlcCoverage;
            List<BlcCoverage> moReturnList = new List<BlcCoverage>();

            try
            {
                // Field Mapping for the Coverage/Line of Business
                CompanyCoverageEntity oCoverageFields = moImportBuilder.CoverageFieldEntity;

                strPolicyNumber = ReadString(oRow, oCoverageFields, (int)CompanyCoverageEntity.FieldColumns.PolicyNumber);
                strPolicyMod = ReadString(oRow, oCoverageFields, (int)CompanyCoverageEntity.FieldColumns.PolicyMod);
                policyList.Add(string.Format("{0}-{1}", strPolicyNumber, strPolicyMod));

                // Defines each LOB
                LineOfBusiness oLineOfBusiness = moImportBuilder.LineOfBusinessFieldEntity;

                // The PolicySymbol field holds which line of business the row represents
                strValue = ReadString(oRow, oCoverageFields, (int)CompanyCoverageEntity.FieldColumns.PolicySymbol);
                // Convert that to a Typed name
                LineOfBusiness.LineOfBusinessType eType = oLineOfBusiness.GetType(strValue);

                // Switch Policy Symbol and get the field mapping for this LOB
                List<CompanyCoverageColumn> oCompanyColumnList = moImportBuilder.GetCoverage(eType);

                if (oCompanyColumnList != null)
                {
                    // Iterage Rows in Coverage - Ignore the column numbers defined in CompanyCoverageEntity
                    foreach (CompanyCoverageColumn oCompanyCoverageColumn in oCompanyColumnList)
                    {
                        oBlcCoverage = new BlcCoverage();

                        if (strPolicyNumber != null && strPolicyNumber.Length > 3)
                        {
                            if (strPolicyNumber.Length == 8 && strPolicyNumber.StartsWith("9"))
                            {
                                //hack for BNP, better solution would be to override this method
                                oBlcCoverage.PolicyNumber = strPolicyNumber.Substring(1, strPolicyNumber.Length - 1);
                            }
                            else
                            {
                                oBlcCoverage.PolicyNumber = strPolicyNumber.Substring(2, strPolicyNumber.Length - 2);
                            }
                        }

                        if (strPolicyMod != null && strPolicyMod.Length > 0)
                        {
                            oBlcCoverage.PolicyMod = strPolicyMod;
                        }

                        // Get the mapped column number
                        iColumn = oCompanyCoverageColumn.ColumnNumber;
                        strValue = SafeRead(oRow[iColumn]);
                        if (strValue == "-1")
                        {
                            strValue = string.Empty;
                        }

                        // Set the Coverage members
                        oBlcCoverage.Value = strValue.Trim();
                        oBlcCoverage.LineOfBusinessAsString = oLineOfBusiness.GetBlcName(oCompanyCoverageColumn.LineOfBusiness);
                        oBlcCoverage.CompanyAbbreviation = moImportBuilder.CompanyAbbreviation;

                        // The name of the coverage i.e. General Liability
                        oBlcCoverage.CoverageNameType = oCompanyCoverageColumn.ColumnGroupTypeAsString;

                        // A name given to the value itself i.e. Limits
                        oBlcCoverage.CoverageValueType = oCompanyCoverageColumn.ColumnValueTypeAsString;

                        if (oCompanyCoverageColumn.SubLineOfBusiness != LineOfBusiness.LineOfBusinessType.Unknown)
                        {
                            oBlcCoverage.SubLineOfBusinessAsString = oLineOfBusiness.GetBlcName(oCompanyCoverageColumn.SubLineOfBusiness);
                        }

                        moReturnList.Add(oBlcCoverage);
                    }
                }
            }
            catch (Exception oException)
            {
                Exception oReturn = new Exception(string.Format(oException.Message + ". Policy {0}", strPolicyNumber), oException.InnerException);
                throw oReturn;
            }
            return moReturnList.ToArray();
        }

        private BlcBuilding BuildBuildings(DataRow oRow)
        {
            BlcBuilding oBuilding = new BlcBuilding(Guid.NewGuid());
            CompanyBuildingEntity oBuildingFields = moImportBuilder.BuildingFieldEntity;

            oBuilding.Number = (short)ReadInteger(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.BuildingNumber);
            oBuilding.PolicySystemKey = ReadString(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.PolicySystemKey);
            // Location.Number
            oBuilding.LocationAsInt = ReadInteger(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.LocationId);
            oBuilding.Contents = ReadDecimal(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.BuildingContents);
            oBuilding.Number = (short)ReadInteger(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.BuildingNumber);
            oBuilding.Value = ReadDecimal(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.BuildingValue);
            oBuilding.BusinessInterruption = ReadDecimal(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.BusinessInterruption);
            oBuilding.LocationOccupancy = ReadString(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.LocationOccupancy);
            oBuilding.PublicProtection = ReadInteger(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.PublicProtection);
            oBuilding.HasSprinklerSystem = (short)ReadInteger(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.SprinklerSystem);
            //oBuilding.SquareFootage = ReadDecimal(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.SquareFootage);
            oBuilding.StockValues = ReadDecimal(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.StockValues);
            oBuilding.YearBuilt = ReadInteger(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.YearBuilt, 4);

            try { oBuilding.ValuationTypeCode = ReadValuationType(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.BuildingValuationType); }
            catch (IndexOutOfRangeException) { }
            try { oBuilding.CoinsurancePercentage = ReadPercentage(oRow, oBuildingFields, (int)CompanyBuildingEntity.BuildingColumn.CoinsurancePercentage); }
            catch (IndexOutOfRangeException) { }

            return oBuilding;
        }

        private BlcClaim BuildClaim(DataRow oRow)
        {
            BlcClaim oBlcClaim = new BlcClaim();
            Claim oClaim = new Claim(Guid.NewGuid());
            CompanyClaimEntity oClaimFields = moImportBuilder.ClaimFieldEntity;

            oClaim.Amount = ReadDecimal(oRow, oClaimFields, (int)CompanyClaimEntity.ClaimColumn.ClaimAmount);
            oClaim.Claimant = ReadString(oRow, oClaimFields, (int)CompanyClaimEntity.ClaimColumn.Claimant);
            oClaim.Comment = ReadString(oRow, oClaimFields, (int)CompanyClaimEntity.ClaimColumn.ClaimComment);
            oClaim.LossDate = ReadDateTime(oRow, oClaimFields, (int)CompanyClaimEntity.ClaimColumn.DateOfLoss);

            oBlcClaim.Entity = oClaim;
            oBlcClaim.PolicyMod = ReadInteger(oRow, oClaimFields, (int)CompanyClaimEntity.ClaimColumn.PolicyVersion);
            oBlcClaim.PolicyNumber = ReadString(oRow, oClaimFields, (int)CompanyClaimEntity.ClaimColumn.PolicyNumber);
            return oBlcClaim;
        }

        private BlcRateModFactor BuildRateModFactor(DataRow oRow)
        {
            BlcRateModFactor oBlcRateModFactor = new BlcRateModFactor();
            RateModificationFactor oFactor = new RateModificationFactor(Guid.NewGuid());
            CompanyRateModFactorEntity oFactorFields = moImportBuilder.RateModFactorFieldEntity;

            oFactor.StateCode = ReadState(oRow, oFactorFields, (int)CompanyRateModFactorEntity.RateModFactorColumn.State);
            oFactor.Rate = ReadDecimal(oRow, oFactorFields, (int)CompanyRateModFactorEntity.RateModFactorColumn.Rate);
            oFactor.TypeCode = ReadRateModType(oRow, oFactorFields, (int)CompanyRateModFactorEntity.RateModFactorColumn.Description);

            oBlcRateModFactor.Entity = oFactor;
            oBlcRateModFactor.PolicyMod = ReadInteger(oRow, oFactorFields, (int)CompanyRateModFactorEntity.RateModFactorColumn.PolicyVersion);
            oBlcRateModFactor.PolicyNumber = ReadString(oRow, oFactorFields, (int)CompanyRateModFactorEntity.RateModFactorColumn.PolicyNumber);
            return oBlcRateModFactor;
        }

        private LocationImportBuilder BuildLocation(DataRow oRow, DataTable dtBuildings, DataTable dtPolicies)
        {
            Location oLocation = new Location(Guid.NewGuid());
            CompanyLocationEntity oLocationFields = moImportBuilder.LocationFieldEntity;

            oLocation.PolicySystemKey = ReadString(oRow, oLocationFields, (int)CompanyLocationEntity.LocationColumn.PolicySystemKey);
            oLocation.Number = ReadInteger(oRow, oLocationFields, (int)CompanyLocationEntity.LocationColumn.LocationId);
            oLocation.City = ReadString(oRow, oLocationFields, (int)CompanyLocationEntity.LocationColumn.LocationCity);
            oLocation.ZipCode = TrimZipCode(ReadString(oRow, oLocationFields, (int)CompanyLocationEntity.LocationColumn.LocationZip));
            oLocation.StateCode = ReadState(oRow, oLocationFields, (int)CompanyLocationEntity.LocationColumn.LocationState);
            oLocation.StreetLine1 = ReadString(oRow, oLocationFields, (int)CompanyLocationEntity.LocationColumn.LocationAddress);
            oLocation.IsActive = true;

            string policyNumber = oLocation.PolicySystemKey.Remove(0, oLocation.PolicySystemKey.IndexOf('-') + 1);
            DataRow[] drPolicies = dtPolicies.Select(string.Format("column1 = '{0}'", policyNumber));
            if (drPolicies.Length > 0)
            {
                oLocation.PolicySymbol = drPolicies[0][3].ToString();
            }

            //New rule 1/14/2010:  If the location info already exist and this one has no buildings, do not import
            DataRow[] buildingCount = dtBuildings.Select(string.Format("column1 = '{0}'", oLocation.PolicySystemKey));

            return new LocationImportBuilder(oLocation, buildingCount.Length);
        }

        private Agency BuildAgency(DataRow oRow)
        {
            Agency oAgency = new Agency(Guid.NewGuid());
            CompanyAgencyEntity oAgencyFields = moImportBuilder.AgencyFieldEntity;

            oAgency.City = ReadString(oRow, oAgencyFields, (int)CompanyAgencyEntity.FieldColumns.City);
            oAgency.FaxNumber = ReadString(oRow, oAgencyFields, (int)CompanyAgencyEntity.FieldColumns.FacsimileNumber);
            oAgency.Name = ReadString(oRow, oAgencyFields, (int)CompanyAgencyEntity.FieldColumns.Name);
            oAgency.Number = ReadString(oRow, oAgencyFields, (int)CompanyAgencyEntity.FieldColumns.Number);
            oAgency.ZipCode = TrimZipCode(ReadString(oRow, oAgencyFields, (int)CompanyAgencyEntity.FieldColumns.PostalCode));
            oAgency.StateCode = ReadState(oRow, oAgencyFields, (int)CompanyAgencyEntity.FieldColumns.State);
            oAgency.StreetLine1 = ReadString(oRow, oAgencyFields, (int)CompanyAgencyEntity.FieldColumns.StreetAddress);
            oAgency.StreetLine2 = ReadString(oRow, oAgencyFields, (int)CompanyAgencyEntity.FieldColumns.StreetAddress2);
            oAgency.PhoneNumber = ReadString(oRow, oAgencyFields, (int)CompanyAgencyEntity.FieldColumns.TelephoneNumber);

            return oAgency;
        }

        /// <summary>
        /// Builds an <see cref="agency"/> for a given agency code in the APS xml file.
        /// </summary>
        /// <param name="apsAgency">The agency data object.</param>
        /// <returns>The Agency Entity <see cref="Agency"/>.</returns>
        internal Agency BuildAgency(APSProxy.agency apsAgency)
        {
            Agency agency = new Agency(Guid.NewGuid());

            //name & number
            agency.Name = apsAgency.dbaName;
            agency.Number = apsAgency.agencyCode;

            //address
            foreach (APSProxy.location location in apsAgency.locations)
            {
                if (location.isPrimaryLocation)
                {
                    agency.StreetLine1 = location.line1;
                    agency.StreetLine2 = location.line2;
                    agency.City = location.city;
                    agency.StateCode = (location.usState != null) ? location.usState.description : string.Empty;
                    agency.ZipCode = location.zip;

                    //contact info
                    foreach (APSProxy.locationCommunication communication in location.locationCommunications)
                    {
                        if (communication.communicationType.label == "Phone Number")
                        {
                            agency.PhoneNumber = communication.communicationValue;
                        }

                        if (communication.communicationType.label == "Fax Number")
                        {
                            agency.FaxNumber = communication.communicationValue;
                        }
                    }
                }
            }
            return agency;
        }

        /// <summary>
        /// Builds an <see cref="Insured"/> for a given row in the 
        /// insured file.
        /// </summary>
        /// <param name="row">The row from which to build the <see cref="Insured"/>.</param>
        /// <returns>The Insured Entity <see cref="Insured"/>.</returns>
        private Insured BuildInsured(DataRow oRow, ref BlcInsured oBlcInsured)
        {
            bool fDbaInAddr1 = false;
            CompanyInsuredEntity oInsuredFields = moImportBuilder.InsuredFieldEntity;
            Insured oInsured = new Insured(Guid.NewGuid());

            oInsured.ClientID = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.ClientID);
            oInsured.Name = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.InsuredName1);
            oInsured.Name2 = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.InsuredName2);
            oInsured.StreetLine1 = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.StreetAddress1);
            oInsured.StreetLine2 = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.StreetAddress2);
            oInsured.StreetLine3 = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.StreetAddress3);
            oInsured.City = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.City);
            oInsured.StateCode = ReadState(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.State);
            oInsured.ZipCode = TrimZipCode(ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.ZipCode));
            oInsured.BusinessOperations = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.BusinessOperations);

            try { oInsured.BusinessCategory = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.BusinessCategory); }
            catch (IndexOutOfRangeException) { }

            //pad the SICCode so that there is always 4 digits
            string strSIC = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.SICCode);
            oInsured.SICCode = (strSIC != string.Empty) ? strSIC.PadLeft(4, '0') : string.Empty;

            try { oInsured.NAICSCode = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.NAICSCode); }
            catch (IndexOutOfRangeException) { }

            oBlcInsured.Owner = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.OwnerName);
            oBlcInsured.OwnerPhone = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.OwnerPhone);
            if (oBlcInsured.Owner.Length == 1 && oBlcInsured.OwnerPhone.Length <= 0)
            {
                oBlcInsured.Owner = String.Empty;
            }
            oInsured.Underwriter = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.Underwriter);

            if (!ContainsDigit(oInsured.StreetLine1) || oInsured.StreetLine1.ToUpper().Contains("INSURED") || oInsured.StreetLine1.ToUpper().Contains("SEE GU"))
            {
                if (oInsured.StreetLine2 != null && oInsured.StreetLine2.Length > 0)
                {
                    if (ContainsDigit(oInsured.StreetLine2))
                    {
                        fDbaInAddr1 = true;
                    }
                }
            }

            if (fDbaInAddr1)
            {
                oInsured.Name2 = oInsured.StreetLine1;
                oInsured.StreetLine1 = oInsured.StreetLine2;
                oInsured.StreetLine2 = oInsured.StreetLine3;
                oInsured.StreetLine3 = "";
            }

            //try and find any past agents entered for this insured
            Insured pastInsured = Insured.GetOne("!ISNULL(AgentContactName) && ClientID = ? && StreetLine1 = ?", oInsured.ClientID, oInsured.StreetLine1);
            if (pastInsured != null)
            {
                oInsured.AgentContactName = pastInsured.AgentContactName;
                oInsured.AgentContactPhoneNumber = pastInsured.AgentContactPhoneNumber;
                oInsured.AgentEmailID = pastInsured.AgentEmailID;
            }

            //stub out the initial agency entity using the agency number
            Agency agency = new Agency(Guid.NewGuid());
            agency.Number = ReadString(oRow, oInsuredFields, (int)CompanyInsuredEntity.InsuredColumn.AgencyNumber);
            oBlcInsured.AgencyEntity = agency;

            oBlcInsured.InsuredEntity = oInsured;
            return oInsured;
        }

        /// <summary>
        /// Builds an <see cref="Policy"/> for a given row in the 
        /// policy file.
        /// </summary>
        /// <param name="row">The row from which to build the <see cref="Policy"/>.</param>
        /// <param name="policyStatusList">List of all the policy statuses for the insrued.</param>
        /// <returns>A <see cref="Policy"/>.</returns>
        private Policy BuildPolicy(DataRow oRow, ref ArrayList policyStatusList)
        {
            Policy oPolicy = new Policy(Guid.NewGuid());
            CompanyPolicyEntity oPolicyFields = moImportBuilder.PolicyFieldEntity;

            //check if this policy is one not supported by Loss Control
            oPolicy.Symbol = ReadString(oRow, oPolicyFields, (int)CompanyPolicyEntity.PolicyColumn.PolicySymbol);
            if (oPolicy.Symbol == moImportBuilder.LineOfBusinessFieldEntity.GetCompanyLob("DO"))
            {
                return null;
            }

            oPolicy.ProfitCenter = ReadString(oRow, oPolicyFields, (int)CompanyPolicyEntity.PolicyColumn.ProfitCenter);
            oPolicy.BranchCode = ReadString(oRow, oPolicyFields, (int)CompanyPolicyEntity.PolicyColumn.BranchCode);
            oPolicy.Carrier = ReadString(oRow, oPolicyFields, (int)CompanyPolicyEntity.PolicyColumn.Carrier);
            oPolicy.EffectiveDate = ReadDateTime(oRow, oPolicyFields, (int)CompanyPolicyEntity.PolicyColumn.EffectiveDate);
            oPolicy.ExpireDate = ReadDateTime(oRow, oPolicyFields, (int)CompanyPolicyEntity.PolicyColumn.ExpirationDate);
            oPolicy.HazardGrade = ReadString(oRow, oPolicyFields, (int)CompanyPolicyEntity.PolicyColumn.HazardGrade);
            oPolicy.Mod = ReadInteger(oRow, oPolicyFields, (int)CompanyPolicyEntity.PolicyColumn.PolicyMod);
            oPolicy.Number = ReadString(oRow, oPolicyFields, (int)CompanyPolicyEntity.PolicyColumn.PolicyNumber);
            oPolicy.Premium = ReadDecimal(oRow, oPolicyFields, (int)CompanyPolicyEntity.PolicyColumn.PremiumAmount);

            //try and find past hazard grade for the policy
            if (oPolicy.HazardGrade == string.Empty)
            {
                Policy pastPolicy = Policy.GetOne("Number = ? && !ISNULL(HazardGrade) && HazardGrade != '' && Symbol = ?", oPolicy.Number.Remove(0, 2), oPolicy.Symbol);
                oPolicy.HazardGrade = (pastPolicy != null) ? pastPolicy.HazardGrade : "";
            }

            //Add the policy status to the list for setting the client status later
            policyStatusList.Add(ReadString(oRow, oPolicyFields, (int)CompanyPolicyEntity.PolicyColumn.PolicyStatus));

            return oPolicy;
        }

        private bool ContainsDigit(string strRhs)
        {
            if (strRhs != null && strRhs.Length > 0)
            {
                foreach (char c in strRhs)
                {
                    if (Char.IsDigit(c))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        #region Common Utility Functions from Pats Audit Request Classes

        private bool StringCompare(object A, object B)
        {
            if (string.Compare(SafeRead(A), SafeRead(B), true) == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Determines if a string contains any other string.
        /// </summary>
        /// <param name="value">The string to interrogate.</param>
        /// <param name="values">A <see cref="string"/> array that contains 
        /// the strings that are to be determined if they are contained
        /// within 'value'.</param>
        /// <returns>true if 'value' contains any of the listed strings, 
        /// false otherwise.</returns>
        protected static bool ContainsAny(string value, params string[] values)
        {
            bool result = false;

            for (int i = 0; result == false && i < values.Length; i++)
                result = Contains(value, values[i]);

            return result;
        }
        /// <summary>
        /// Determines if a string contains all other strings.
        /// </summary>
        /// <param name="value">The string to interrogate.</param>
        /// <param name="values">A <see cref="string"/> array that contains 
        /// the strings that are to be determined if they are contained
        /// within 'value'.</param>
        /// <returns>true if 'value' contains all of the listed strings, 
        /// false otherwise.</returns>
        protected static bool ContainsAll(string value, params string[] values)
        {
            bool result = true;

            for (int i = 0; result == true && i < values.Length; i++)
                result = Contains(value, values[i]);

            return result;
        }
        /// <summary>
        /// Determines if a string contains another string.
        /// </summary>
        /// <param name="value">The string to interrogate.</param>
        /// <param name="text">A <see cref="string"/> to be determined if it is
        /// contained within 'value'.</param>
        /// <returns>true if 'value' contains the string, false otherwise.</returns>
        protected static bool Contains(string value, string text)
        {
            return value.IndexOf(text) >= 0;
        }

        /// <summary>
        /// Strips all non-alphanumeric characters from a string.
        /// </summary>
        /// <param name="value">The string to parse.</param>
        /// <returns>A string with only alphanumeric characters.</returns>
        private string StripNonAlphaNumericCharacters(string value)
        {
            string result = string.Empty;

            foreach (char character in value)
                if (char.IsLetterOrDigit(character))
                    result += character;

            return result;
        }
        /// <summary>
        /// Parses a date/time string from the input files and converts it 
        /// into a representative <see cref="DateTime"/> object.
        /// </summary>
        /// <param name="dateTime">The string to parse.</param>
        /// <returns>A DateTime representative of the dateTime.</returns>
        private DateTime ParseDateTime(string dateTime)
        {
            DateTime result = DateTime.MinValue;

            if (IsMeaningful(dateTime))
            {
                try
                {
                    result = Convert.ToDateTime(dateTime, FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // swallow the error and return DateTime.MinValue
                }
                catch (FormatException)
                {
                    // swallow the error and return DateTime.MinValue
                }
            }

            return result;
        }
        /// <summary>
        /// Parses a date string from the input files and converts it 
        /// into a representative <see cref="DateTime"/> object.
        /// </summary>
        /// <param name="date">The string to parse.</param>
        /// <returns>A DateTime representative of the date.</returns>
        private DateTime ParseDate(string date)
        {
            DateTime result = DateTime.MinValue;

            if (IsMeaningful(date))
            {
                try
                {
                    result = DateTime.ParseExact(date, "MM/dd/yyyy", FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // swallow the error and return DateTime.MinValue
                }
                catch (FormatException)
                {
                    // swallow the error and return DateTime.MinValue
                }
            }

            return result;
        }
        #endregion

        #region From Pats Import___.cs

        /// <summary>
        /// Returns an value from the object specified.
        /// </summary>
        /// <param name="amount">The object from which to find the value.</param>
        /// <returns>The value.</returns>
        protected decimal SafeReadDecimal(object amount)
        {
            decimal result = decimal.MinValue;

            try
            {
                if (amount != null)
                    result = decimal.Parse(amount.ToString(), FormatProvider);
            }
            catch (ArgumentNullException)
            {
                // return decimal.MinValue
            }
            catch (FormatException)
            {
                // return decimal.MinValue
            }
            catch (OverflowException)
            {
                // return decimal.MinValue
            }

            return result;
        }

        /// <summary>
        /// Returns an value from the object specified.
        /// </summary>
        /// <param name="amount">The object from which to find the value.</param>
        /// <param name="nullValue">The default value if null.</param>
        /// <returns>The value.</returns>
        protected decimal SafeReadDecimal(object amount, decimal nullValue)
        {
            decimal result = nullValue;

            try
            {
                if (amount != null)
                    result = decimal.Parse(amount.ToString(), FormatProvider);
            }
            catch (ArgumentNullException)
            {
                // return decimal.MinValue
            }
            catch (FormatException)
            {
                // return decimal.MinValue
            }
            catch (OverflowException)
            {
                // return decimal.MinValue
            }

            return result;
        }

        /// <summary>
        /// Returns an value from the array, row, and column specified.
        /// </summary>
        /// <param name="value">The value to parse.</param>
        /// <returns>The value.</returns>
        protected int SafeReadInteger(string value)
        {
            int result = int.MinValue;

            if (IsMeaningful(value))
            {
                try
                {
                    result = int.Parse(value, FormatProvider);
                }
                catch (ArgumentNullException)
                {
                    // return decimal.MinValue
                }
                catch (FormatException)
                {
                    // return decimal.MinValue
                }
                catch (OverflowException)
                {
                    // return decimal.MinValue
                }
            }

            return result;
        }

        /// <summary>
        /// Returns an value from the object in the datatable.
        /// </summary>
        /// <param name="data">The object from which to find the value.</param>
        /// <returns>The value.</returns>
        protected static string SafeRead(object data)
        {
            string result = string.Empty;

            if (data != null && data.ToString().Trim().Length > 0)
                result = data.ToString().Trim();

            return result;
        }

        /// <summary>
        /// Determines whether or not a string has anything in it.
        /// </summary>
        /// <param name="text">The string to test.</param>
        /// <returns>True if the string has content, false otherwise.</returns>
        protected bool IsMeaningful(string text)
        {
            return text != null && text.Trim().Length > 0;
        }
        /// <summary>
        /// Determines whether or not a string has anything in it.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>True if the value has content, false otherwise.</returns>
        protected bool IsMeaningful(int value)
        {
            return value > int.MinValue && value < int.MaxValue;
        }
        /// <summary>
        /// Determines whether or not a decimal has anything in it.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>True if the value has content, false otherwise.</returns>
        protected bool IsMeaningful(decimal value)
        {
            return value > decimal.MinValue && value < decimal.MaxValue;
        }
        /// <summary>
        /// Determines whether or not a value has any meaningful content.
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <returns>True if the value has content, false otherwise.</returns>
        protected bool IsMeaningful(Array value)
        {
            return value != null && value.Length > 0;
        }
        /// <summary>
        /// Builds the assembly path, allowing for both full and relative 
        /// paths.
        /// </summary>
        /// <param name="path">The path to format.</param>
        /// <returns>The full path to the the assembly.</returns>
        private string FormatAssemblyPath(string path)
        {
            string combinedPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
            return System.IO.Path.GetFullPath(combinedPath);
        }
        /// <summary>
        /// Renames all the columns names to generic names (Column1, Column2, etc.).
        /// </summary>
        /// <param name="table">The DataTable whose columns will be renamed.</param>
        /// <returns>A DataTable.</returns>
        private DataTable RenameColumns(DataTable table)
        {
            DataTable dt = table;

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                dt.Columns[i].ColumnName = string.Format("column{0}", i); ;
            }

            return dt;
        }
        /// <summary>
        /// Returns a format provider useful for string parsing.
        /// </summary>
        protected static IFormatProvider FormatProvider
        {
            get
            {
                return System.Globalization.CultureInfo.InvariantCulture;
            }
        }

        /// <summary>
        /// Called by <see cref="ProperCase"/>. Determines whether or not a 
        /// string has any lower case characters.
        /// </summary>
        /// <param name="text">The string to test.</param>
        /// <returns>True if the string has lower-case characters, false otherwise.</returns>
        private bool ContainsLowerCase(string text)
        {
            bool result = false;

            if (IsMeaningful(text))
                for (int i = 0; result == false && i < text.Length; i++)
                    result = char.IsLower(text[i]);

            return result;
        }


        /// <summary>
        /// Some of the data from the policy system is upper-cased. In effort
        /// to clean up the look of the information, this very simplistic proper
        /// casing method was written. It trades simplicity for less than perfect
        /// results. If, at some point in the future, more accurate results are
        /// desired, it would be better to refactor the code into its own class.
        /// </summary>
        /// <param name="text">The string to proper case.</param>
        /// <returns>The proper-cased string.</returns>
        protected string ProperCase(string text)
        {
            string result = text;

            /*
             * if the string is not meaningful or already has some lower
             * case values, do not attempt to proper case it.
             */
            if (IsMeaningful(text) && !ContainsLowerCase(text))
            {
                System.Text.StringBuilder builder = new System.Text.StringBuilder(text.Length);

                /*
                 * split the string into words
                 */
                foreach (string word in text.Split(new char[] { ' ' }))
                {
                    if (word.Length > 0)
                    {
                        if (moProperCaseExclusions != null && moProperCaseExclusions.Contains(word))
                        {
                            builder.Append(word + ' ');
                        }
                        else
                        {
                            /*
                             * upper case the first letter and lower case the rest.
                             */
                            builder.Append(char.ToUpper(word[0], System.Globalization.CultureInfo.InvariantCulture));
                            builder.Append(word.Substring(1, word.Length - 1).ToLower(System.Globalization.CultureInfo.InvariantCulture) + ' ');
                        }
                    }
                    else
                    {
                        /*
                         * if the 'word' was a space, make sure that it is included in the results.
                         */
                        builder.Append(' ');
                    }
                }

                result = builder.ToString();
            }

            return result.Trim();
        }
        #endregion

    }
}
