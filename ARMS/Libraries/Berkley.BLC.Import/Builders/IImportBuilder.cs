using System;
using System.Collections.Generic;

using Berkley;
using Berkley.BLC.Business;

namespace Berkley.BLC.Import
{
    /// <summary>
    /// Summary description for IImportBuilder.
    /// </summary>
    public interface IImportBuilder
    {
        BlcStoredProcedure GetStoredProcedure();
        BlcStoredProcedure GetStoredProcedure(CompanySpEntity.SpType eType);

        CompanyPolicyEntity PolicyFieldEntity { get; }
        CompanyInsuredEntity InsuredFieldEntity { get; }
        CompanyAgencyEntity AgencyFieldEntity { get; }
        CompanyLocationEntity LocationFieldEntity { get; }
        CompanyBuildingEntity BuildingFieldEntity { get; }
        CompanyCoverageEntity CoverageFieldEntity { get; }
        CompanyClaimEntity ClaimFieldEntity { get; }
        CompanyRateModFactorEntity RateModFactorFieldEntity { get; }

        LineOfBusiness LineOfBusinessFieldEntity { get; }

        string CompanyAbbreviation { get; }

        BlcInsured BuildComplete(BlcInsured oBlcInsured);
        List<CompanyCoverageColumn> GetCoverage(LineOfBusiness.LineOfBusinessType eType);
        int GetMaxCoverageColumns();
        int GetMaxColumnNumber();
    }
}
