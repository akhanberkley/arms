using Berkley.BLC.Business;
using Berkley.BLC.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Berkley.BLC.Import
{
    public class BSIGPolicyStarImportBuilder : TransactionalPolicyStarBaseImportBuilder, IImportBuilder
    {
        private CompanyPolicyEntity moPolicyFieldEntity = new CompanyPolicyEntity();
        private CompanyInsuredEntity moInsuredFieldEntity = new CompanyInsuredEntity();
        private CompanyAgencyEntity moAgencyFieldEntity = new CompanyAgencyEntity();
        private CompanyLocationEntity moLocationFieldEntity = new CompanyLocationEntity();
        private CompanyBuildingEntity moBuildingFieldEntity = new CompanyBuildingEntity();
        private CompanyClaimEntity moCompanyClaimEntity = new CompanyClaimEntity();
        private CompanyRateModFactorEntity moCompanyRateModFactorEntity = new CompanyRateModFactorEntity();
        private BMAGLineOfBusiness moLineOfBusiness = new BMAGLineOfBusiness();

        #region Line Of Business / Coverage
        private CompanyCoverageEntity moCoverageFieldEntity = new CompanyCoverageEntity();
        private BMAGCoverageDictionary moCoverageDictionary = new BMAGCoverageDictionary();
        #endregion

        public BSIGPolicyStarImportBuilder(string strPolicyNumber)
        {
            base.Initialize(this, strPolicyNumber, Company.BerkleySouthEast);
            // Rearrange fields in moPolicyFieldEntity as necessary
        }

        #region IImportBuilder Members

        public BlcStoredProcedure GetStoredProcedure()
        {
            return null;
        }

        public BlcStoredProcedure GetStoredProcedure(CompanySpEntity.SpType eType)
        {
            return null;
        }

        public BlcInsured BuildComplete(BlcInsured oBlcInsured)
        {
            // The BlcDataSet has been filled out.  Do the Insert into the BLC DB
            // If all of the company (derived) classes do the same thing, put the basic insert/update
            // in BaseImportBuilder
            Policy oPolicy;
            BMAGLineOfBusiness oLineOfBusiness = new BMAGLineOfBusiness();
            LineOfBusiness.LineOfBusinessType eType;
            List<Policy> oPolicyList = oBlcInsured.PolicyList;

            oPolicyList = oPolicyList.OrderByDescending(pol => pol.Mod).ToList();

            //**************************************************************
            //Only take the most recent policy mod - Hack for P* issue
            List<Policy> filteredPolicies = new List<Policy>();

            foreach (Policy p in oPolicyList)
            {
                if (filteredPolicies == null || filteredPolicies.Where(c => c.Number == p.Number).Count() == 0)
                {
                    filteredPolicies.Add(p);
                }
                else
                {
                    oBlcInsured.PolicyList.Remove(p);
                }
            }

            if (filteredPolicies.Count > 0)
                oPolicyList = filteredPolicies;
            //***************************************************************

            List<Location> locations = oBlcInsured.PolicyLocationList;
            List<Location> filteredLocation = new List<Location>();

            locations = locations.OrderBy(loc => loc.PolicySymbol).ToList();
            foreach (Location l in locations)
            {
                if ((filteredLocation == null || filteredLocation.Where(c => c.StreetLine1 == l.StreetLine1).Count() == 0) && oPolicyList.Where(p => p.Symbol == l.PolicySymbol).Count() > 0)
                {
                    filteredLocation.Add(l);
                }
                else
                {
                    oBlcInsured.PolicyLocationList.Remove(l);
                }
            }

            //Buildings
            List<Berkley.BLC.Business.BlcBuilding> buildings = oBlcInsured.BuildingList;
            List<Berkley.BLC.Business.BlcBuilding> filteredBuilding = new List<Berkley.BLC.Business.BlcBuilding>();

            buildings = buildings.OrderBy(b => b.LocationID).ToList();
            foreach (Berkley.BLC.Business.BlcBuilding b in buildings)
            {
                if ((filteredBuilding == null || filteredBuilding.Where(c => c.Contents == b.Contents && c.Number == b.Number && c.LocationAsInt == b.LocationAsInt).Count() == 0))
                {
                    filteredBuilding.Add(b);
                }
                else
                {
                    oBlcInsured.BuildingList.Remove(b);
                }
            }

            //Coverages
            List<Berkley.BLC.Business.BlcCoverage> coverages = oBlcInsured.CoverageList;
            List<Berkley.BLC.Business.BlcCoverage> filteredCoverages = new List<Berkley.BLC.Business.BlcCoverage>();

            coverages = coverages.OrderBy(b => b.PolicyMod).ToList();
            foreach (Berkley.BLC.Business.BlcCoverage cov in coverages)
            {
                if (filteredPolicies.Where(c => c.Number == cov.PolicyNumber && c.Mod.ToString() == cov.PolicyMod).Count() == 0)
                {
                    oBlcInsured.CoverageList.Remove(cov);
                }
            }

            for (int i = 0; i < oPolicyList.Count; i++)
            {
                oPolicy = oPolicyList[i];
                eType = oLineOfBusiness.GetType(oPolicy.Symbol);
                oPolicy.LineOfBusinessCode = oLineOfBusiness.GetBlcName(eType);
                oPolicyList[i] = oPolicy;
            }

            //convert the underwriter
            if (!UnderwriterExist(oBlcInsured.InsuredEntity.Underwriter))
            {
                //attempt to get the assigned underwriter from BCS
                Prospect.ProspectWebService service = new Prospect.ProspectWebService();
                Underwriter underwriter = service.GetAssignedUnderwriter(base.PolicyNumber, base.BerkleyCompany);
                oBlcInsured.InsuredEntity.Underwriter = (underwriter != null) ? underwriter.Code : string.Empty;
            }

            return oBlcInsured;
        }

        private bool UnderwriterExist(string initials)
        {
            bool result = false;

            if (initials.Length > 0)
            {
                Underwriter underwriter = Underwriter.GetOne("Code = ? && CompanyID = ?", initials, Company.BerkleyMidAtlanticGroup.ID);
                result = (underwriter != null) ? true : false;
            }

            return result;
        }

        public Agency BuildForAgency(string agencyNumber)
        {
            Berkley.Import.PolicyStar.APSWebService service = new Berkley.Import.PolicyStar.APSWebService(Company.BerkleyMidAtlanticGroup.APSEndPointUrl);
            APSProxy.agency apsAgency = service.GetAgency(agencyNumber);

            return base.BuildAgency(apsAgency);
        }

        public CompanyCoverageEntity CoverageFieldEntity
        {
            get { return moCoverageFieldEntity; }
        }

        public List<CompanyCoverageColumn> GetCoverage(LineOfBusiness.LineOfBusinessType eType)
        {
            return moCoverageDictionary.GetColumnList(eType);
        }

        public int GetMaxCoverageColumns()
        {
            return moCoverageDictionary.GetColumnCount();
        }

        public int GetMaxColumnNumber()
        {
            return moCoverageDictionary.GetMaxColumnNumber();
        }

        // The derived classes can change the field mapping
        public CompanyPolicyEntity PolicyFieldEntity
        {
            get { return moPolicyFieldEntity; }
        }

        // The derived classes can change the field mapping
        public CompanyInsuredEntity InsuredFieldEntity
        {
            get { return moInsuredFieldEntity; }
        }

        public CompanyAgencyEntity AgencyFieldEntity
        {
            get { return moAgencyFieldEntity; }
        }

        public CompanyLocationEntity LocationFieldEntity
        {
            get { return moLocationFieldEntity; }
        }

        public CompanyBuildingEntity BuildingFieldEntity
        {
            get { return moBuildingFieldEntity; }
        }

        public LineOfBusiness LineOfBusinessFieldEntity
        {
            get { return moLineOfBusiness; }
        }

        public CompanyClaimEntity ClaimFieldEntity
        {
            get { return moCompanyClaimEntity; }
        }

        public CompanyRateModFactorEntity RateModFactorFieldEntity
        {
            get { return moCompanyRateModFactorEntity; }
        }

        public string CompanyAbbreviation
        {
            get { return "BMAG"; }
        }


        #endregion
    }
}
