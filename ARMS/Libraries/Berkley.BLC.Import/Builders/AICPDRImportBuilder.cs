using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

using Berkley.BLC.Business;
using Berkley.BLC.Core;
using Berkley.BLC.Import.Properties;
using Berkley.BLC.Entities;

namespace Berkley.BLC.Import
{
    public class AICPDRImportBuilder : PolicyStarBaseImportBuilder, IImportBuilder
    {
        private CompanyPolicyEntity moPolicyFieldEntity = new CompanyPolicyEntity();
        private CompanyInsuredEntity moInsuredFieldEntity = new CompanyInsuredEntity();
        private CompanyAgencyEntity moAgencyFieldEntity = new CompanyAgencyEntity();
        private CompanyLocationEntity moLocationFieldEntity = new CompanyLocationEntity();
        private CompanyBuildingEntity moBuildingFieldEntity = new CompanyBuildingEntity();
        private CompanyClaimEntity moCompanyClaimEntity = new CompanyClaimEntity();
        private CompanyRateModFactorEntity moCompanyRateModFactorEntity = new CompanyRateModFactorEntity();
        private AICLineOfBusiness moLineOfBusiness = new AICLineOfBusiness();

        #region Line Of Business / Coverage
        private CompanyCoverageEntity moCoverageFieldEntity = new CompanyCoverageEntity();
        private AICCoverageDictionary moCoverageDictionary = new AICCoverageDictionary();
        #endregion

        public AICPDRImportBuilder(string strPolicyNumber)
        {
            base.Initialize(this, strPolicyNumber, Company.AcadiaInsuranceCompany);
            // Rearrange fields in moPolicyFieldEntity as necessary
        }

        #region IImportBuilder Members

        public BlcStoredProcedure GetStoredProcedure()
        {
            return null;
        }

        public BlcStoredProcedure GetStoredProcedure(CompanySpEntity.SpType eType)
        {
            return null;
        }

        public BlcInsured BuildComplete(BlcInsured oBlcInsured)
        {
            // The BlcDataSet has been filled out.  Do the Insert into the BLC DB
            // If all of the company (derived) classes do the same thing, put the basic insert/update
            // in BaseImportBuilder
            Policy oPolicy;
            AICLineOfBusiness oLineOfBusiness = new AICLineOfBusiness();
            LineOfBusiness.LineOfBusinessType eType;
            List<Policy> oPolicyList = oBlcInsured.PolicyList;
            for (int i = 0; i < oPolicyList.Count; i++)
            {
                oPolicy = oPolicyList[i];
                eType = oLineOfBusiness.GetType(oPolicy.Symbol);
                oPolicy.LineOfBusinessCode = oLineOfBusiness.GetBlcName(eType);
                oPolicyList[i] = oPolicy;
            }

            //convert the underwriter
            oBlcInsured.InsuredEntity.Underwriter = ConvertUnderwriter(oBlcInsured.InsuredEntity.Underwriter);

            return oBlcInsured;
        }

        private string ConvertUnderwriter(string name)
        {
            string result = string.Empty;

            if (name.Length > 0)
            {
                Underwriter underwriter = Underwriter.GetOne("Name = ? && CompanyID = ?", name, Company.AcadiaInsuranceCompany.ID);
                result = (underwriter != null) ? underwriter.Code : name;
            }

            return result;
        }

        public CompanyCoverageEntity CoverageFieldEntity
        {
            get { return moCoverageFieldEntity; }
        }

        public List<CompanyCoverageColumn> GetCoverage(LineOfBusiness.LineOfBusinessType eType)
        {
            return moCoverageDictionary.GetColumnList(eType);
        }

        public int GetMaxCoverageColumns()
        {
            return moCoverageDictionary.GetColumnCount();
        }

        public int GetMaxColumnNumber()
        {
            return moCoverageDictionary.GetMaxColumnNumber();
        }

        // The derived classes can change the field mapping
        public CompanyPolicyEntity PolicyFieldEntity
        {
            get { return moPolicyFieldEntity; }
        }

        // The derived classes can change the field mapping
        public CompanyInsuredEntity InsuredFieldEntity
        {
            get { return moInsuredFieldEntity; }
        }

        public CompanyAgencyEntity AgencyFieldEntity
        {
            get { return moAgencyFieldEntity; }
        }

        public CompanyLocationEntity LocationFieldEntity
        {
            get { return moLocationFieldEntity; }
        }

        public CompanyBuildingEntity BuildingFieldEntity
        {
            get { return moBuildingFieldEntity; }
        }

        public LineOfBusiness LineOfBusinessFieldEntity
        {
            get { return moLineOfBusiness; }
        }

        public CompanyClaimEntity ClaimFieldEntity
        {
            get { return moCompanyClaimEntity; }
        }

        public CompanyRateModFactorEntity RateModFactorFieldEntity
        {
            get { return moCompanyRateModFactorEntity; }
        }

        public string CompanyAbbreviation
        {
            get { return "AIC"; }
        }


        #endregion
    }
}
