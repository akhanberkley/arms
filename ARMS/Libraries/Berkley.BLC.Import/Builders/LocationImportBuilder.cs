using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Berkley.BLC.Entities;

namespace Berkley.BLC.Import
{
    class LocationImportBuilder
    {
        private int _buildingCount = 0;
        private Location _location = null;

        public LocationImportBuilder(Location location, int buildingCount)
        {
            _location = location;
            _buildingCount = buildingCount;
        }

        public int BuildingCount
        {
            get
            {
                return _buildingCount;
            }
        }

        public Location Location
        {
            get
            {
                return _location;
            }
        }
    }
}
