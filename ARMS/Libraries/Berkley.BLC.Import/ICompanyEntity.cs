using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public interface ICompanyEntity
	{
		int this[int iKey] { get; set; }

		Dictionary<int, int> ColumnMapDictionary { get; }
		int GetColumn(int iKey);
		void SetColumn(int iKey, int iNewValue);
	}
}
