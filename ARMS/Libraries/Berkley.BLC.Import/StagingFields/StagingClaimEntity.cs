using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class StagingClaimEntity : BaseCompanyEntity
	{
		#region Enum
		/// <summary>
		/// Translates the meaning of each column in the policy system's policy
		/// file.
		/// </summary>
		public enum ClaimColumn
		{
			/// <summary>
			/// The policy number to which the diary is applicable.
			/// </summary>
			PolicyNumber = 0,
			/// <summary>
			/// The policy version to which the diary is applicable.
			/// </summary>
			PolicyVersion = 1,
			/// <summary>
			/// The date of the claim.
			/// </summary>
            ClaimNumber = 2,
			LossDate = 3,
			Claimant = 4,
			ClaimAmount = 5,
            ClassCode = 6,
			ClaimComment = 7,
            ClaimStatus = 8,
            LossType = 9
		}
		#endregion

        public StagingClaimEntity()
		{
		}

		protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
		{
			oColumnMapDictionary.Add((int)ClaimColumn.PolicyNumber, 0);
			oColumnMapDictionary.Add((int)ClaimColumn.PolicyVersion, 1);
            oColumnMapDictionary.Add((int)ClaimColumn.ClaimNumber, 2);
			oColumnMapDictionary.Add((int)ClaimColumn.LossDate, 3);
			oColumnMapDictionary.Add((int)ClaimColumn.Claimant, 4);
			oColumnMapDictionary.Add((int)ClaimColumn.ClaimAmount, 5);
            oColumnMapDictionary.Add((int)ClaimColumn.ClassCode, 6);
			oColumnMapDictionary.Add((int)ClaimColumn.ClaimComment, 7);
            oColumnMapDictionary.Add((int)ClaimColumn.ClaimStatus, 8);
            oColumnMapDictionary.Add((int)ClaimColumn.LossType, 9);
		}
	}
}
