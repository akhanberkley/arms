using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class StagingLocationContactEntity : BaseCompanyEntity
	{
		#region Enum
		/// <summary>
		/// Translates the meaning of each column in the policy system's policy
		/// file.
		/// </summary>
		public enum LocationColumn
		{
            LocationId = 0,
            Name,
            Title,
            PhoneNumber,
            AlternatePhoneNumber,
            FaxNumber,
            EmailAddress,
            CompanyName,
            StreetLine1,
            StreetLine2,
            City,
            StateCode,
            ZipCode,
            PriorityIndex
		}
		#endregion

        public StagingLocationContactEntity()
		{
		}

		protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
		{
            oColumnMapDictionary.Add((int)LocationColumn.LocationId, 0);
            oColumnMapDictionary.Add((int)LocationColumn.Name, 1);
            oColumnMapDictionary.Add((int)LocationColumn.Title, 2);
            oColumnMapDictionary.Add((int)LocationColumn.PhoneNumber, 3);
            oColumnMapDictionary.Add((int)LocationColumn.AlternatePhoneNumber, 4);
            oColumnMapDictionary.Add((int)LocationColumn.FaxNumber, 5);
			oColumnMapDictionary.Add((int)LocationColumn.EmailAddress, 6);
			oColumnMapDictionary.Add((int)LocationColumn.CompanyName, 7);
			oColumnMapDictionary.Add((int)LocationColumn.StreetLine1, 8);
            oColumnMapDictionary.Add((int)LocationColumn.StreetLine2, 9);
            oColumnMapDictionary.Add((int)LocationColumn.City, 10);
            oColumnMapDictionary.Add((int)LocationColumn.StateCode, 11);
            oColumnMapDictionary.Add((int)LocationColumn.ZipCode, 12);
            oColumnMapDictionary.Add((int)LocationColumn.PriorityIndex, 13);
		}
	}
}
