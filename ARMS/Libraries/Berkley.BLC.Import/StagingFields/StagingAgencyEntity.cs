using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class StagingAgencyEntity : BaseCompanyEntity
	{
		#region Enum
		/// <summary>
		/// Translates the meaning of each column in the policy system's policy
		/// file.
		/// </summary>
		public enum FieldColumns
		{
			Number = 0,
			/// <summary>
			/// The agency's name.
			/// </summary>
			Name,
			/// <summary>
			/// The agency's street address.
			/// </summary>
            StreetLine1,
			/// <summary>
			/// The agency's street address.
			/// </summary>
            StreetLine2,
			/// <summary>
			/// The agency's city.
			/// </summary>
			City,
			/// <summary>
			/// The agency's state.
			/// </summary>
			State,
			/// <summary>
			/// The agency's postal code.
			/// </summary>
            ZipCode,
			/// <summary>
			/// The agency's telephone number.
			/// </summary>
            PhoneNumber,
			/// <summary>
			/// The agency's facsimile number.
			/// </summary>
            FaxNumber
		}
		#endregion

		public StagingAgencyEntity()
		{
		}

		protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
		{
			oColumnMapDictionary.Add((int)FieldColumns.Number, 0);
			oColumnMapDictionary.Add((int)FieldColumns.Name, 1);
            oColumnMapDictionary.Add((int)FieldColumns.StreetLine1, 2);
            oColumnMapDictionary.Add((int)FieldColumns.StreetLine2, 3);
			oColumnMapDictionary.Add((int)FieldColumns.City, 4);
			oColumnMapDictionary.Add((int)FieldColumns.State, 5);
            oColumnMapDictionary.Add((int)FieldColumns.ZipCode, 6);
            oColumnMapDictionary.Add((int)FieldColumns.PhoneNumber, 7);
            oColumnMapDictionary.Add((int)FieldColumns.FaxNumber, 8);
		}
	}
}
