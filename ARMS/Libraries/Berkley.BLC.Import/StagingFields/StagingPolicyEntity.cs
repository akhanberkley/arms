using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class StagingPolicyEntity : BaseCompanyEntity
	{
		#region Enum
		/// <summary>
		/// Translates the meaning of each column in the policy system's policy
		/// file.
		/// </summary>
		public enum PolicyColumn
		{
            /// <summary>
            /// The client identifier from the policy system.
            /// </summary>
            PolicyId = 0,
            /// <summary>
            /// The policy number.
            /// </summary>
            PolicyNumber,
            /// <summary>
            /// The policy version.
            /// </summary>
            PolicyVersion,
            /// <summary>
            /// The policy symbol.
            /// </summary>
            PolicySymbol,
            /// <summary>
			/// The client identifier from the policy system.
			/// </summary>
			ClientID,
            /// <summary>
            /// The create date of the policy.
            /// </summary>
            CreateDate,
            /// <summary>
            /// Flag indicating written business verse submission.
            /// </summary>
            IsSubmission,
			/// <summary>
			/// The effective date of the policy.
			/// </summary>
			EffectiveDate,
			/// <summary>
			/// The expiration date of the policy.
			/// </summary>
			ExpirationDate,
            /// <summary>
            /// The cancel date of the policy.
            /// </summary>
            CancelDate,
			/// <summary>
			/// The premium amount.
			/// </summary>
			TotalEstimatedPremium,
            /// <summary>
            /// The insurance carrier.
            /// </summary>
            Carrier,
            /// <summary>
            /// The policy status.
            /// </summary>
            PolicyStatus,
            /// <summary>
            /// The state abbreviation.
            /// </summary>
            StateAbbreviation,
            /// <summary>
            /// The anniversary rate date.
            /// </summary>
            AnniversaryRateDate,
			/// <summary>
			/// The branch code.
			/// </summary>
			BranchCode,
			/// <summary>
			/// The hazard grade.
			/// </summary>
			HazardGrade,
            /// <summary>
			/// The profit center.
			/// </summary>
			ProfitCenter,
            /// <summary>
            /// The rating company.
            /// </summary>
            RatingCompany,
       
		}
		#endregion

        public StagingPolicyEntity()
		{
		}

		protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
		{
            oColumnMapDictionary.Add((int)PolicyColumn.PolicyId, 0);
            oColumnMapDictionary.Add((int)PolicyColumn.PolicyNumber, 1);
            oColumnMapDictionary.Add((int)PolicyColumn.PolicyVersion, 2);
			oColumnMapDictionary.Add((int)PolicyColumn.PolicySymbol, 3);
            oColumnMapDictionary.Add((int)PolicyColumn.ClientID, 4);
            oColumnMapDictionary.Add((int)PolicyColumn.CreateDate, 5);
            oColumnMapDictionary.Add((int)PolicyColumn.IsSubmission, 6);
            oColumnMapDictionary.Add((int)PolicyColumn.EffectiveDate, 7);
			oColumnMapDictionary.Add((int)PolicyColumn.ExpirationDate, 8);
            oColumnMapDictionary.Add((int)PolicyColumn.CancelDate, 9);
            oColumnMapDictionary.Add((int)PolicyColumn.TotalEstimatedPremium, 10);
            oColumnMapDictionary.Add((int)PolicyColumn.Carrier, 11);
            oColumnMapDictionary.Add((int)PolicyColumn.PolicyStatus, 11);
            oColumnMapDictionary.Add((int)PolicyColumn.StateAbbreviation, 11);
            oColumnMapDictionary.Add((int)PolicyColumn.AnniversaryRateDate, 11);
            oColumnMapDictionary.Add((int)PolicyColumn.BranchCode, 11);
            oColumnMapDictionary.Add((int)PolicyColumn.HazardGrade, 11);
            oColumnMapDictionary.Add((int)PolicyColumn.ProfitCenter, 11);
            oColumnMapDictionary.Add((int)PolicyColumn.RatingCompany, 11);
		}

	}
}
