using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class StagingPolicyCoverageEntity : BaseCompanyEntity
	{
		#region Enum
		/// <summary>
		/// Translates the meaning of each column in the policy system's policy
		/// file.
		/// </summary>
		public enum FieldColumns
		{
            PolicyId = 0,
            PolicyNumber,
            PolicyVersion,
            PolicySymbol,
            CoverageCode,
            CoverageValue,
            ClassCodeDescription,
            ClassCodePayroll,
            ClassCodeEmployeeCount
		}
		#endregion

        public StagingPolicyCoverageEntity()
		{
		}

		protected override void BuildMap(ref Dictionary<int, int> oColumnMapDictionary)
		{
            oColumnMapDictionary.Add((int)FieldColumns.PolicyId, 0);
            oColumnMapDictionary.Add((int)FieldColumns.PolicyNumber, 1);
			oColumnMapDictionary.Add((int)FieldColumns.PolicyVersion, 2);
            oColumnMapDictionary.Add((int)FieldColumns.PolicySymbol, 3);
            oColumnMapDictionary.Add((int)FieldColumns.CoverageCode, 4);
            oColumnMapDictionary.Add((int)FieldColumns.CoverageValue, 5);
			oColumnMapDictionary.Add((int)FieldColumns.ClassCodeDescription, 6);
			oColumnMapDictionary.Add((int)FieldColumns.ClassCodePayroll, 7);
            oColumnMapDictionary.Add((int)FieldColumns.ClassCodeEmployeeCount, 8);
		}
	}
}
