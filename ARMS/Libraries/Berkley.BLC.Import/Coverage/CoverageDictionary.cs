using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    /// <summary>
    /// Derive from this class and override BuildCoverageMap()
    /// In order to change the column mapping for a company
    /// </summary>
    public class CoverageDictionary
    {
        #region Coverage Map Enum
        public enum CoverageMap
        {
            // Business Owner --
            BoGeneralLiabilityOccurrenceLimits,
            BoGeneralLiabilityAggregateLimits,
            BoBusinessIncomeLimits,
            BoBusinessIncomeFromDependentProperties,

            // Workers Comp --
            WcExposure,

            // Commercial Umbrella
            CuOccurrenceLimits,
            CuAggregateLimits,

            // Commercial Output --			
            CoBuildingLimit,
            CoBuildingCatastropheLimit,
            CoPersonalPropertyLimit,

            // Commercial Auto --
            CaBodilyInjuryLimit,
            CaPhysicalDamageLimit,
            CaVehicleCount,
            CaNonOwnerBodilyInjuryLimit,
            CaNonOwnerPhysicalDamageLimit,
            CaNonOwnerAutoLiability,
            CaGaragekeepersLimits,
            CaNonownedHiredAutoLimits,
            CaTractors,
            CaTrailers,
            CaServiceUnits,
            CaCommercialUnits,
            CaPPTsExHeavy,
            CaPPTsHeavy,
            CaPPTsMedium,
            CaPPTsLight,
            CaActivePlates,
            CaInactivePlates,

            // Commercial Crime --
            CcInsidePremisesTheftLimits,
            CcInsidePremisesRobberyLimits,
            CcOutsidePremisesRobberyLimits,
            CcTheftPerEmployeeLimits,
            CcForgeryAlterationLimits,
            CcClientsPropertyLimits,
            CcFundsTransferFraudLimits,
            CcComputerFraudLimits,

            // Garage --
            GaComprehensiveDeductable,
            GaComprehensiveLimits,
            GaSpecialCausesOfLossDeductable,
            GaSpecialCausesOfLossLimits,
            GaCollisionDeductable,
            GaCollisionLimits,
            GaDealerClassOfOpRegularOPFurnishedAutos,
            GaDealerClassOfOpRegularOPNonFurnishedAutos,
            GaDealerClassOfOpEmployeesNonRegularOp,
            GaDealerClassOfOpNonEmployeesUnder25,
            GaDealerClassOfOpNonEmployeesOver25,
            GaDealerPhysicalDamageComprehensiveLimits,
            GaDealerPhysicalDamageCollisionLimits,
            GaDealerDriveawayCollisionVehicleCount,
            GaDealerDriveawayCollisionTripCount,
            GaDealerDriveawayCollisionPriceNew,
            GaDealerDriveawayCollisionMileage,
            GaDealerDriveawayCollisionDeductable,
            GaGaragekeepersLimits,

            // General Liability --
            GlGeneralLiabilityOccurrenceLimits,
            GlGeneralLiabilityAggregateLimits,
            GlGeneralLiabilityExposures,
            GlProductsOperationsLimits,
            GlFRPLiabilityLimits,
            GlNonOwnerBodilyInjuryLimit,
            GlNonOwnerPhysicalDamageLimit,
            GlProfessionalLiabilityOccurrenceLimits,
            GlProfessionalLiabilityAggregateLimits,
            GlSexualAbuseOccuranceLimits,
            GlSexualAbuseAggregateLimits,

            // Commerical Property --
            CpBusinessIncomeLimits,

            // Inland Marine --
            ImMotorTruckCargoLegalLiabilityLimits,
            ImMotorTruckCargoVehicleLimits,
            ImMotorTruckCargoTerminalCoverage,
            ImMotorTruckCargoTerminalLimits,
            ImBuildersRiskConstructionLimits,
            ImBuildersRiskExistingBuildingLimits,
            ImWarehouseLegalLiabilitySpoilageLimits,
            ImWarehouseLegalLiabilityColdStorageLimits,
            ImJewelersBlockPremisesLimits,
            ImContractorsEquipmentOwnedEquipment,
            ImContractorsEquipmentToOthersLimits,
            ImContractorsEquipmentFromOthersLimits,
            ImToolFloaterLimits,
            ImToolFloaterEmployeeToolsLimits,
            ImComputerHardwareLimits,
            ImComputerSoftwareMediaLimits,
            ImSignsScheduledLimits,
            ImSignsUnscheduledLimits,
            ImAccountsReceivableInsuredPremises1,
            ImAccountsReceivableInsuredPremises2,
            ImValuablePapersProperty1,
            ImValuablePapersProperty2,
            ImValuablePapersProperty3,
            ImValuablePapersAllOtherCoveredProperty,
            ImFineArtsFloaterOnPremises,
            ImFineArtsFloaterTransportation,
            ImBaileesCoveragePremises,
            ImBaileesCoverageTransit,
            ImTransitCoverageHiredCarrier,
            ImTransitCoverageOwnedVehicle,
            ImTransitTransportationLimit,
            ImTransitTransportationScheduleLimit,
            ImInstallFloaterInstallationCatostrophic,
            ImInstallFloaterJobsiteSchedule,
            ImInstallFloaterInTransit,
            ImInstallFloaterTemporaryLocations,
            ImMobileEquipmentLimits,
            ImEDPEquipmentHardwareLimits,
            ImEDPEquipmentSoftwareLimits,
            ImEquipment,
            
            // Ocean Marine
            OmPhysicalDamageVesselLimits,
            OmPhysicalDamageExcessCollisionLimits,
            OmPhysicalDamageBreachOfWarrantyLimits,
            OmLegalLiabilityLimits,
            OmLegalLiabilityMedicalPaymentsLimits,
            OmBoatDealersCoverageDLimits,
            OmBoatDealersCoverageALimits,
            OmBuildersRiskAmountInsured,
            OmBuildersRiskAgreedValue,
        }
        #endregion

        protected Dictionary<CoverageMap, int> moCoverageDictionary = new Dictionary<CoverageMap, int>();
        private Dictionary<LineOfBusiness.LineOfBusinessType, List<CompanyCoverageColumn>> moLineOfBusinessDictionary = new Dictionary<LineOfBusiness.LineOfBusinessType, List<CompanyCoverageColumn>>();

        public CoverageDictionary()
        {
            BuildCoverageMap();
            BuildColumns();
        }

        public int GetColumnCount()
        {
            return moCoverageDictionary.Count;
        }

        public virtual int GetMaxColumnNumber()
        {
            return 0;
        }

        public List<CompanyCoverageColumn> GetColumnList(LineOfBusiness.LineOfBusinessType eLineOfBusiness)
        {
            return moLineOfBusinessDictionary[eLineOfBusiness];
        }

        protected virtual void BuildColumns()
        {
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.BusinessOwner, CreateBusinessOwners());
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.CommercialAuto, CreateCommercialAuto());
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.CommercialCrime, CreateCommercialCrime());
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.CommercialOutputPolicy, CreateCommercialOutput());
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.CommercialProperty, CreateCommercialProperty());
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.CommercialUmbrella, CreateCommercialUmbrella());
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.Garage, CreateGarage());
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.GeneralLiability, CreateGeneralLiability());
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.InlandMarine, CreateInlandMarine());
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.Package, CreatePackages());
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.WorkersComp, CreateWorkersComp());
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.OceanMarine, CreateOceanMarine());
            moLineOfBusinessDictionary.Add(LineOfBusiness.LineOfBusinessType.Transportation, CreateTransporation());
            
        }

        #region Business Owners
        protected virtual List<CompanyCoverageColumn> CreateBusinessOwners()
        {
            return new List<CompanyCoverageColumn>();
        }
        #endregion

        #region Workers Comp
        protected virtual List<CompanyCoverageColumn> CreateWorkersComp()
        {
            return new List<CompanyCoverageColumn>();
        }
        #endregion

        #region Commercial Output
        protected virtual List<CompanyCoverageColumn> CreateCommercialOutput()
        {
            return new List<CompanyCoverageColumn>();
        }

        #endregion

        #region Commercial Auto
        protected virtual List<CompanyCoverageColumn> CreateCommercialAuto()
        {
            return new List<CompanyCoverageColumn>();
        }
        #endregion

        #region Commercial Crime
        protected virtual List<CompanyCoverageColumn> CreateCommercialCrime()
        {
            return new List<CompanyCoverageColumn>();
        }
        #endregion

        #region Commercial Umbrella
        protected virtual List<CompanyCoverageColumn> CreateCommercialUmbrella()
        {
            return new List<CompanyCoverageColumn>();
        }
        #endregion

        #region Garage
        protected virtual List<CompanyCoverageColumn> CreateGarage()
        {
            return new List<CompanyCoverageColumn>();
        }
        #endregion

        #region General Liablility
        protected virtual List<CompanyCoverageColumn> CreateGeneralLiability()
        {
            return new List<CompanyCoverageColumn>();
        }
        #endregion

        #region Commercial Property
        protected virtual List<CompanyCoverageColumn> CreateCommercialProperty()
        {
            return new List<CompanyCoverageColumn>();
        }
        #endregion

        #region Transporation
        protected virtual List<CompanyCoverageColumn> CreateTransporation()
        {
            return new List<CompanyCoverageColumn>();
        }
        #endregion

        #region Inland Marine
        protected virtual List<CompanyCoverageColumn> CreateInlandMarine()
        {
            return new List<CompanyCoverageColumn>();
        }
        #endregion

        #region Ocean Marine
        protected virtual List<CompanyCoverageColumn> CreateOceanMarine()
        {
            return new List<CompanyCoverageColumn>();
        }
        #endregion

        #region Packages

        protected virtual List<CompanyCoverageColumn> CreatePackages()
        {
            return new List<CompanyCoverageColumn>();
        }

        #endregion

        #region Build Coverage MAP

        protected virtual void BuildCoverageMap()
        {
            //this section is company specific
        }
        #endregion

    }
}
