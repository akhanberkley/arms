using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class BARSCoverageDictionary : CoverageDictionary
    {
        public BARSCoverageDictionary()
        {
        }

        #region Business Owner
        protected override List<CompanyCoverageColumn> CreateBusinessOwners()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            #region Business Owner

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.BoGeneralLiabilityOccurrenceLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.GeneralLiability,
                                                        ColumnValueType.Enum.OccuranceLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.BoGeneralLiabilityAggregateLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.GeneralLiability,
                                                        ColumnValueType.Enum.AggregateLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB_AGG }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.BoBusinessIncomeLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.BusinessIncome,
                                                        ColumnValueType.Enum.Limits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.BoBusinessIncomeFromDependentProperties],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.BusinessIncome,
                                                        ColumnValueType.Enum.FromDependentProperties,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.BINC_DPND }, 3));
            #endregion

            #region Commercial Auto

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                        ColumnGroupType.Enum.CommercialAuto,
                                                        ColumnValueType.Enum.BodilyInjuryLimit,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaVehicleCount],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                        ColumnGroupType.Enum.CommercialAuto,
                                                        ColumnValueType.Enum.NumberOfVehicles,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));
            
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerAutoLiability],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                        ColumnGroupType.Enum.NonOwnerAuto,
                                                        ColumnValueType.Enum.Liability,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 6));
            #endregion

            #region Inland Marine
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                        ColumnGroupType.Enum.ContractorsEquipmentLeased,
                                                        ColumnValueType.Enum.OwnedEquipment,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 7));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                        ColumnGroupType.Enum.ContractorsEquipmentLeased,
                                                        ColumnValueType.Enum.ToOthers,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 8));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                        ColumnGroupType.Enum.ContractorsEquipmentLeased,
                                                        ColumnValueType.Enum.FromOthers,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 9));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImToolFloaterLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                        ColumnGroupType.Enum.ToolFloater,
                                                        ColumnValueType.Enum.YourTools,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 10));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                        ColumnGroupType.Enum.ToolFloater,
                                                        ColumnValueType.Enum.EmployeeTools,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 11));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImComputerHardwareLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                        ColumnGroupType.Enum.ComputerEquipment,
                                                        ColumnValueType.Enum.HardwareLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 12));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImSignsScheduledLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                        ColumnGroupType.Enum.Signs,
                                                        ColumnValueType.Enum.ScheduledLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 13));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                        ColumnGroupType.Enum.Signs,
                                                        ColumnValueType.Enum.UnscheduledLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 14));

            #endregion

            #region Commercial Umbrella

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                        ColumnValueType.Enum.OccuranceLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 15));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuAggregateLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                        ColumnValueType.Enum.AggregateLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 16));
            #endregion

            #region Crime

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits],
                                                   LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                   ColumnGroupType.Enum.Crime,
                                                   ColumnValueType.Enum.InsideThePremesisTheftOfMoneyAndSecurites,
                                                   new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 17));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits],
                                                    LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.InsideThePremesisRobberyOfMoneyAndSecurites,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 18));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits],
                                                    LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.OutsideThePremesisRobberyOfMoneyAndSecurites,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 19));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits],
                                                    LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.TheftPerEmployee,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 20));

            #endregion

            return oColumns;
        }
        #endregion

        #region Workers Comp
        protected override List<CompanyCoverageColumn> CreateWorkersComp()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.WcExposure],
                                                    LineOfBusiness.LineOfBusinessType.WorkersComp,
                                                    ColumnGroupType.Enum.Exposure,
                                                    ColumnValueType.Enum.ClassCodePayrollEmployees,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            return oColumns;
        }
        #endregion

        #region Commercial Output
        protected override List<CompanyCoverageColumn> CreateCommercialOutput()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CoBuildingLimit],
                                        LineOfBusiness.LineOfBusinessType.CommercialOutputPolicy,
                                        ColumnGroupType.Enum.Building,
                                        ColumnValueType.Enum.Limits,
                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CoBuildingCatastropheLimit],
                                        LineOfBusiness.LineOfBusinessType.CommercialOutputPolicy,
                                        ColumnGroupType.Enum.Building,
                                        ColumnValueType.Enum.CatastropheLimits,
                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.DN_CATLMT }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CoPersonalPropertyLimit],
                                        LineOfBusiness.LineOfBusinessType.CommercialOutputPolicy,
                                        ColumnGroupType.Enum.PersonalProperty,
                                        ColumnValueType.Enum.Limits,
                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            return oColumns;
        }

        #endregion

        #region Commercial Auto
        protected override List<CompanyCoverageColumn> CreateCommercialAuto()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaVehicleCount],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.NumberOfVehicles,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.NonOwnerAuto,
                                                    ColumnValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.NonOwnerAuto,
                                                    ColumnValueType.Enum.PhysicalDamageLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaGaragekeepersLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.Garagekeeper,
                                                    ColumnValueType.Enum.Garagekeeper,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            return oColumns;
        }
        #endregion

        #region Commercial Crime
        protected override List<CompanyCoverageColumn> CreateCommercialCrime()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits],
                                                   LineOfBusiness.LineOfBusinessType.CommercialCrime,
                                                   ColumnGroupType.Enum.Crime,
                                                   ColumnValueType.Enum.InsideThePremesisTheftOfMoneyAndSecurites,
                                                   new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialCrime,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.InsideThePremesisRobberyOfMoneyAndSecurites,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialCrime,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.OutsideThePremesisRobberyOfMoneyAndSecurites,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialCrime,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.TheftPerEmployee,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));

            return oColumns;
        }
        #endregion

        #region Commercial Umbrella
        protected override List<CompanyCoverageColumn> CreateCommercialUmbrella()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialUmbrella,
                                                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialUmbrella,
                                                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 1));

            return oColumns;
        }
        #endregion

        #region Garage
        protected override List<CompanyCoverageColumn> CreateGarage()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            
            #region General Liability
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            #endregion

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaComprehensiveDeductable],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    ColumnGroupType.Enum.Comprehensive,
                                                    ColumnValueType.Enum.Deductable,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaComprehensiveLimits],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    ColumnGroupType.Enum.Comprehensive,
                                                    ColumnValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaSpecialCausesOfLossDeductable],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    ColumnGroupType.Enum.SpecifiedCausesOfLoss,
                                                    ColumnValueType.Enum.Deductable,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaSpecialCausesOfLossLimits],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    ColumnGroupType.Enum.SpecifiedCausesOfLoss,
                                                    ColumnValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaCollisionDeductable],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    ColumnGroupType.Enum.Collision,
                                                    ColumnValueType.Enum.Deductable,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaCollisionLimits],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    ColumnGroupType.Enum.Collision,
                                                    ColumnValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 7));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaDealerClassOfOpRegularOPFurnishedAutos],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.Dealer,
                                                    ColumnGroupType.Enum.ClassOfOperations,
                                                    ColumnValueType.Enum.RegularOpFurnishedAutos,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 8));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaDealerClassOfOpRegularOPNonFurnishedAutos],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.Dealer,
                                                    ColumnGroupType.Enum.ClassOfOperations,
                                                    ColumnValueType.Enum.RegularOpNonFurnishedAutos,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 9));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaDealerClassOfOpEmployeesNonRegularOp],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.Dealer,
                                                    ColumnGroupType.Enum.ClassOfOperations,
                                                    ColumnValueType.Enum.EmployeesNonRegularOp,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 10));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaDealerClassOfOpNonEmployeesUnder25],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.Dealer,
                                                    ColumnGroupType.Enum.ClassOfOperations,
                                                    ColumnValueType.Enum.NonEmployeesUnder25,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 11));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaDealerClassOfOpNonEmployeesOver25],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.Dealer,
                                                    ColumnGroupType.Enum.ClassOfOperations,
                                                    ColumnValueType.Enum.NonEmployeesOver25,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 12));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaDealerPhysicalDamageComprehensiveLimits],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.Dealer,
                                                    ColumnGroupType.Enum.PhysicalDamage,
                                                    ColumnValueType.Enum.ComprehensiveLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 13));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaDealerPhysicalDamageCollisionLimits],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.Dealer,
                                                    ColumnGroupType.Enum.PhysicalDamage,
                                                    ColumnValueType.Enum.CollisionLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 14));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionVehicleCount],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.Dealer,
                                                    ColumnGroupType.Enum.DriveawayCollision,
                                                    ColumnValueType.Enum.NumberOfVehicles,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 15));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionTripCount],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.Dealer,
                                                    ColumnGroupType.Enum.DriveawayCollision,
                                                    ColumnValueType.Enum.NumberOfTrips,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 16));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionPriceNew],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.Dealer,
                                                    ColumnGroupType.Enum.DriveawayCollision,
                                                    ColumnValueType.Enum.PriceNew,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 17));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionMileage],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.Dealer,
                                                    ColumnGroupType.Enum.DriveawayCollision,
                                                    ColumnValueType.Enum.Mileage,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 18));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionDeductable],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    LineOfBusiness.LineOfBusinessType.Dealer,
                                                    ColumnGroupType.Enum.DriveawayCollision,
                                                    ColumnValueType.Enum.Deductable,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 19));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GaGaragekeepersLimits],
                                                    LineOfBusiness.LineOfBusinessType.Garage,
                                                    ColumnGroupType.Enum.Garagekeeper,
                                                    ColumnValueType.Enum.Garagekeeper,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 20));

            return oColumns;
        }
        #endregion

        #region General Liablility
        protected override List<CompanyCoverageColumn> CreateGeneralLiability()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_AGG }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.Exposures,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.ProductsOperations,
                                                    ColumnValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROD }, 3));

            return oColumns;
        }
        #endregion

        #region Commercial Property
        protected override List<CompanyCoverageColumn> CreateCommercialProperty()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                                                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                                                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                                                    ColumnGroupType.Enum.BusinessIncome,
                                                    ColumnValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 2));
            return oColumns;
        }
        #endregion

        #region Inland Marine
        protected override List<CompanyCoverageColumn> CreateInlandMarine()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.MotorTruckCargo,
                                                    ColumnValueType.Enum.LegalLiabilityLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CRGO_SCHED }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.MotorTruckCargo,
                                                    ColumnValueType.Enum.VehicleLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROP_VEH }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.ConstructionLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CSE_CONS }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.ExistingBuildingLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.JSITE_SCHD }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilitySpoilageLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.WarehouseLiability,
                                                    ColumnValueType.Enum.SpoilageLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilityColdStorageLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.WarehouseLiability,
                                                    ColumnValueType.Enum.ColdStorageLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImJewelersBlockPremisesLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.JewelersBlock,
                                                    ColumnValueType.Enum.YourPremisesLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                                                    ColumnValueType.Enum.OwnedEquipment,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.FROM_OTH }, 7));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                                                    ColumnValueType.Enum.ToOthers,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TO_OTH }, 8));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                                                    ColumnValueType.Enum.FromOthers,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PRMS_RENT }, 9));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImToolFloaterLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ToolFloater,
                                                    ColumnValueType.Enum.YourTools,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.YOUR_TOOL }, 10));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ToolFloater,
                                                    ColumnValueType.Enum.EmployeeTools,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EMP_TOOL }, 11));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImComputerHardwareLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ComputerEquipment,
                                                    ColumnValueType.Enum.HardwareLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 12));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImComputerSoftwareMediaLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ComputerEquipment,
                                                    ColumnValueType.Enum.SoftwareMedia,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 13));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImSignsScheduledLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.Signs,
                                                    ColumnValueType.Enum.ScheduledLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN }, 14));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.Signs,
                                                    ColumnValueType.Enum.UnscheduledLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN_USCHD }, 15));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImAccountsReceivableInsuredPremises1],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.AccountsReceivable,
                                                    ColumnValueType.Enum.InsuredPremises1,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 16));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImAccountsReceivableInsuredPremises2],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.AccountsReceivable,
                                                    ColumnValueType.Enum.InsuredPremises2,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 17));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImValuablePapersProperty1],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ValuablePapers,
                                                    ColumnValueType.Enum.Property1,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 18));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImValuablePapersProperty2],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ValuablePapers,
                                                    ColumnValueType.Enum.Property2,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 19));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImValuablePapersProperty3],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ValuablePapers,
                                                    ColumnValueType.Enum.Property3,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 20));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImValuablePapersAllOtherCoveredProperty],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ValuablePapers,
                                                    ColumnValueType.Enum.AllOtherCoveredProperty,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 21));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImFineArtsFloaterOnPremises],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.FineArtsFloater,
                                                    ColumnValueType.Enum.OnPremises,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 22));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImFineArtsFloaterTransportation],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.FineArtsFloater,
                                                    ColumnValueType.Enum.Transportation,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 23));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImBaileesCoveragePremises],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.BaileesCoverage,
                                                    ColumnValueType.Enum.Premises,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 24));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImBaileesCoverageTransit],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.BaileesCoverage,
                                                    ColumnValueType.Enum.Transit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 25));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImTransitCoverageHiredCarrier],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.TransitCoverage,
                                                    ColumnValueType.Enum.HiredCarrier,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 26));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImTransitCoverageOwnedVehicle],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.TransitCoverage,
                                                    ColumnValueType.Enum.OwnedVehicle,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 27));

            return oColumns;
        }
        #endregion

        #region Ocean Marine
        protected override List<CompanyCoverageColumn> CreateOceanMarine()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmPhysicalDamageVesselLimits],
                                                   LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                   ColumnGroupType.Enum.PhysicalDamage,
                                                   ColumnValueType.Enum.VesselLimits,
                                                   new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmPhysicalDamageExcessCollisionLimits],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.PhysicalDamage,
                                                    ColumnValueType.Enum.ExcessCollisionLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmPhysicalDamageBreachOfWarrantyLimits],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.PhysicalDamage,
                                                    ColumnValueType.Enum.BreachofWarrantyLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmLegalLiabilityLimits],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.LegalLiability,
                                                    ColumnValueType.Enum.LegalLiabilityLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmLegalLiabilityMedicalPaymentsLimits],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.LegalLiability,
                                                    ColumnValueType.Enum.MedicalPaymentsLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmBoatDealersCoverageDLimits],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.BoatDealer,
                                                    ColumnValueType.Enum.CoverageDLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmBoatDealersCoverageALimits],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.BoatDealer,
                                                    ColumnValueType.Enum.CoverageALimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmBuildersRiskAmountInsured],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.AmountInsured,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 7));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmBuildersRiskAgreedValue],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.AgreedValue,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 8));

            return oColumns;
        }
        #endregion

        #region Package

        protected override List<CompanyCoverageColumn> CreatePackages()
        {
            List<CompanyCoverageColumn> oPackageList = new List<CompanyCoverageColumn>();

            #region Commercial Auto

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaVehicleCount],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.NumberOfVehicles,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.NonOwnerAuto,
                                ColumnValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.NonOwnerAuto,
                                ColumnValueType.Enum.PhysicalDamageLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaGaragekeepersLimits],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.Garagekeeper,
                                ColumnValueType.Enum.Garagekeeper,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            #endregion

            #region Inland Marine
            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImMobileEquipmentLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.MobileEquipment,
                    ColumnValueType.Enum.Limits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            #endregion

            #region General Liability
            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 7));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.Exposures,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 8));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.ProductsOperations,
                    ColumnValueType.Enum.Limits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 9));

            #endregion

            #region Crime

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.InsideThePremesisTheftOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 10));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.InsideThePremesisRobberyOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 11));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.OutsideThePremesisRobberyOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 12));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.TheftPerEmployee,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 13));

            #endregion

            #region Property

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    ColumnGroupType.Enum.BusinessIncome,
                    ColumnValueType.Enum.Limits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 14));

            #endregion

            return oPackageList;
        }

        #endregion

        public override int GetMaxColumnNumber()
        {
            //largest column number plus one
            return 88;
        }

        protected override void BuildCoverageMap()
        {
            // Business Owner
            moCoverageDictionary[CoverageMap.BoGeneralLiabilityOccurrenceLimits] = 4;
            moCoverageDictionary[CoverageMap.BoGeneralLiabilityAggregateLimits] = 5;
            moCoverageDictionary[CoverageMap.BoBusinessIncomeLimits] = 6;
            moCoverageDictionary[CoverageMap.BoBusinessIncomeFromDependentProperties] = 7;

            // Commercial Output
            moCoverageDictionary[CoverageMap.CoBuildingLimit] = 9;
            moCoverageDictionary[CoverageMap.CoBuildingCatastropheLimit] = 10;
            moCoverageDictionary[CoverageMap.CoPersonalPropertyLimit] = 11;

            // Commercial Umbrella
            moCoverageDictionary[CoverageMap.CuOccurrenceLimits] = 39;
            moCoverageDictionary[CoverageMap.CuAggregateLimits] = 40;

            // Workers Comp
            moCoverageDictionary[CoverageMap.WcExposure] = 12;

            // Commercial Auto
            moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit] = 13;
            moCoverageDictionary[CoverageMap.CaVehicleCount] = 15;
            moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit] = 16;
            moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit] = 17;
            moCoverageDictionary[CoverageMap.CaNonOwnerAutoLiability] = 8;
            moCoverageDictionary[CoverageMap.CaGaragekeepersLimits] = 14;

            // Commercial Crime
            moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits] = 41;
            moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits] = 42;
            moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits] = 44;
            moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits] = 45;

            // Garage
            moCoverageDictionary[CoverageMap.GaComprehensiveDeductable] = 68;
            moCoverageDictionary[CoverageMap.GaComprehensiveLimits] = 73;
            moCoverageDictionary[CoverageMap.GaSpecialCausesOfLossDeductable] = 69;
            moCoverageDictionary[CoverageMap.GaSpecialCausesOfLossLimits] = 72;
            moCoverageDictionary[CoverageMap.GaCollisionDeductable] = 70;
            moCoverageDictionary[CoverageMap.GaCollisionLimits] = 71;
            moCoverageDictionary[CoverageMap.GaGaragekeepersLimits] = 14;

            moCoverageDictionary[CoverageMap.GaDealerClassOfOpRegularOPFurnishedAutos] = 56;
            moCoverageDictionary[CoverageMap.GaDealerClassOfOpRegularOPNonFurnishedAutos] = 57;
            moCoverageDictionary[CoverageMap.GaDealerClassOfOpEmployeesNonRegularOp] = 58;
            moCoverageDictionary[CoverageMap.GaDealerClassOfOpNonEmployeesUnder25] = 59;
            moCoverageDictionary[CoverageMap.GaDealerClassOfOpNonEmployeesOver25] = 60;
            moCoverageDictionary[CoverageMap.GaDealerPhysicalDamageComprehensiveLimits] = 61;
            moCoverageDictionary[CoverageMap.GaDealerPhysicalDamageCollisionLimits] = 62;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionVehicleCount] = 63;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionTripCount] = 64;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionPriceNew] = 65;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionMileage] = 66;
            moCoverageDictionary[CoverageMap.GaDealerDriveawayCollisionDeductable] = 67;

            // General Liability
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits] = 21;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits] = 22;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures] = 23;
            moCoverageDictionary[CoverageMap.GlProductsOperationsLimits] = 24;
            moCoverageDictionary[CoverageMap.GlFRPLiabilityLimits] = 87;

            // Commercial Property
            moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits] = 43;

            // Inland Marine
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits] = 25;
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits] = 26;
            moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits] = 27;
            moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits] = 28;
            moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilitySpoilageLimits] = 29;
            moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilityColdStorageLimits] = 30;
            moCoverageDictionary[CoverageMap.ImJewelersBlockPremisesLimits] = 31;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment] = 46;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits] = 32;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits] = 33;
            moCoverageDictionary[CoverageMap.ImToolFloaterLimits] = 34;
            moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits] = 35;
            moCoverageDictionary[CoverageMap.ImComputerHardwareLimits] = 36;
            moCoverageDictionary[CoverageMap.ImComputerSoftwareMediaLimits] = 86;
            moCoverageDictionary[CoverageMap.ImSignsScheduledLimits] = 37;
            moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits] = 38;
            moCoverageDictionary[CoverageMap.ImAccountsReceivableInsuredPremises1] = 74;
            moCoverageDictionary[CoverageMap.ImAccountsReceivableInsuredPremises2] = 75;
            moCoverageDictionary[CoverageMap.ImValuablePapersProperty1] = 76;
            moCoverageDictionary[CoverageMap.ImValuablePapersProperty2] = 77;
            moCoverageDictionary[CoverageMap.ImValuablePapersProperty3] = 78;
            moCoverageDictionary[CoverageMap.ImValuablePapersAllOtherCoveredProperty] = 79;
            moCoverageDictionary[CoverageMap.ImFineArtsFloaterOnPremises] = 80;
            moCoverageDictionary[CoverageMap.ImFineArtsFloaterTransportation] = 81;
            moCoverageDictionary[CoverageMap.ImBaileesCoveragePremises] = 82;
            moCoverageDictionary[CoverageMap.ImBaileesCoverageTransit] = 83;
            moCoverageDictionary[CoverageMap.ImTransitCoverageHiredCarrier] = 84;
            moCoverageDictionary[CoverageMap.ImTransitCoverageOwnedVehicle] = 85;
            moCoverageDictionary[CoverageMap.ImMobileEquipmentLimits] = 43;

            // Ocean Marine
            moCoverageDictionary[CoverageMap.OmPhysicalDamageVesselLimits] = 47;
            moCoverageDictionary[CoverageMap.OmPhysicalDamageExcessCollisionLimits] = 48;
            moCoverageDictionary[CoverageMap.OmPhysicalDamageBreachOfWarrantyLimits] = 49;
            moCoverageDictionary[CoverageMap.OmLegalLiabilityLimits] = 50;
            moCoverageDictionary[CoverageMap.OmLegalLiabilityMedicalPaymentsLimits] = 51;
            moCoverageDictionary[CoverageMap.OmBoatDealersCoverageDLimits] = 52;
            moCoverageDictionary[CoverageMap.OmBoatDealersCoverageALimits] = 55;
            moCoverageDictionary[CoverageMap.OmBuildersRiskAmountInsured] = 53;
            moCoverageDictionary[CoverageMap.OmBuildersRiskAgreedValue] = 54;
        }
    }
}
