using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class CompanyCoverageColumn
    {
        private LineOfBusiness.LineOfBusinessType meLineOfBusiness;
        private LineOfBusiness.LineOfBusinessType meSubLineOfBusiness;
        private ColumnGroupType.Enum meColumnGroupType;
        private ColumnValueType.Enum meColumnValueType;
        private PolicyStarCoverageValueType.Enum[] mePolicyStarCoverageValueTypes;
        private string meStagingCoverageValueType;
        private int miColumnNumber = 0;
        private int miDisplayOrder = -1;
        private ColumnGroupType moColumnGroupType = new ColumnGroupType();
        private ColumnValueType moColumnValueType = new ColumnValueType();
        private PolicyStarCoverageValueType moPolicyStarCoverageValueType = new PolicyStarCoverageValueType();

        public CompanyCoverageColumn(	int iColumnNumber, 
                                        LineOfBusiness.LineOfBusinessType eLineOfBusiness,
                                        ColumnGroupType.Enum eColumnGroupType,
                                        ColumnValueType.Enum eColumnValueType,
                                        PolicyStarCoverageValueType.Enum[] ePolicyStarCoverageValueTypes,
                                        int iDisplayOrder )
        {
            miColumnNumber = iColumnNumber;
            meLineOfBusiness = eLineOfBusiness;
            meColumnGroupType = eColumnGroupType;
            meColumnValueType = eColumnValueType;
            mePolicyStarCoverageValueTypes = ePolicyStarCoverageValueTypes;
            miDisplayOrder = iDisplayOrder;
        }

        public CompanyCoverageColumn(int iColumnNumber,
                                LineOfBusiness.LineOfBusinessType eLineOfBusiness,
                                LineOfBusiness.LineOfBusinessType eSubLineOfBusiness,
                                ColumnGroupType.Enum eColumnGroupType,
                                ColumnValueType.Enum eColumnValueType,
                                PolicyStarCoverageValueType.Enum[] ePolicyStarCoverageValueTypes,
                                int iDisplayOrder )
        {
            miColumnNumber = iColumnNumber;
            meLineOfBusiness = eLineOfBusiness;
            meSubLineOfBusiness = eSubLineOfBusiness;
            meColumnGroupType = eColumnGroupType;
            meColumnValueType = eColumnValueType;
            mePolicyStarCoverageValueTypes = ePolicyStarCoverageValueTypes;
            miDisplayOrder = iDisplayOrder;
        }


        public CompanyCoverageColumn(int iColumnNumber,
                                        LineOfBusiness.LineOfBusinessType eLineOfBusiness,
                                        ColumnGroupType.Enum eColumnGroupType,
                                        ColumnValueType.Enum eColumnValueType,
                                        string stagingCoverageValueType,
                                        int iDisplayOrder)
        {
            miColumnNumber = iColumnNumber;
            meLineOfBusiness = eLineOfBusiness;
            meColumnGroupType = eColumnGroupType;
            meColumnValueType = eColumnValueType;
            meStagingCoverageValueType = stagingCoverageValueType;
            miDisplayOrder = iDisplayOrder;
        }


        public CompanyCoverageColumn(int iColumnNumber,
                                LineOfBusiness.LineOfBusinessType eLineOfBusiness,
                                LineOfBusiness.LineOfBusinessType eSubLineOfBusiness,
                                ColumnGroupType.Enum eColumnGroupType,
                                ColumnValueType.Enum eColumnValueType,
                                string stagingCoverageValueType,
                                int iDisplayOrder)
        {
            miColumnNumber = iColumnNumber;
            meLineOfBusiness = eLineOfBusiness;
            meSubLineOfBusiness = eSubLineOfBusiness;
            meColumnGroupType = eColumnGroupType;
            meColumnValueType = eColumnValueType;
            meStagingCoverageValueType = stagingCoverageValueType;
            miDisplayOrder = iDisplayOrder;
        }

        public CompanyCoverageColumn()
        {
        }

        public LineOfBusiness.LineOfBusinessType LineOfBusiness
        {
            get { return meLineOfBusiness; }
        }

        public LineOfBusiness.LineOfBusinessType SubLineOfBusiness
        {
            get { return meSubLineOfBusiness; }
        }

        public ColumnGroupType.Enum ColumnGroupType
        {
            get { return meColumnGroupType; }
        }

        public string ColumnGroupTypeAsString
        {
            get 
            {
                return moColumnGroupType[(int)meColumnGroupType];
            }
        }

        public ColumnValueType.Enum ColumnValueType
        {
            get { return meColumnValueType; }
        }

        public string ColumnValueTypeAsString
        {
            get 
            {
                return moColumnValueType[(int)meColumnValueType];
            }
        }

        public PolicyStarCoverageValueType.Enum[] PolicyStarCoverageValueTypes
        {
            get { return mePolicyStarCoverageValueTypes; }
        }

        public string StagingCoverageValueType
        {
            get { return meStagingCoverageValueType; }
        }

        public List<string> CoverageCodesAsStringArray
        {
            get
            {
                List<string> list = new List<string>();
                if (mePolicyStarCoverageValueTypes != null)
                {
                    foreach (PolicyStarCoverageValueType.Enum code in mePolicyStarCoverageValueTypes)
                    {
                        list.Add(moPolicyStarCoverageValueType[(int)code]);
                    }
                }

                return list;
            }
        }

        public int ColumnNumber
        {
            get { return miColumnNumber; }
        }

        public int DisplayOrder
        {
            get { return miDisplayOrder; }
        }
    }
}
