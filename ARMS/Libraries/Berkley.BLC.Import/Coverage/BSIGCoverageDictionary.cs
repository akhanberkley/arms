using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class BSIGCoverageDictionary : CoverageDictionary
	{
        public BSIGCoverageDictionary()
		{
		}

        #region Business Owner
        protected override List<CompanyCoverageColumn> CreateBusinessOwners()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            #region Business Owner

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.BoGeneralLiabilityOccurrenceLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.GeneralLiability,
                                                        ColumnValueType.Enum.OccuranceLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR, PolicyStarCoverageValueType.Enum.LIAB_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.BoGeneralLiabilityAggregateLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.GeneralLiability,
                                                        ColumnValueType.Enum.AggregateLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_AGG, PolicyStarCoverageValueType.Enum.LIAB_AGG  }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.BoBusinessIncomeLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.BusinessIncome,
                                                        ColumnValueType.Enum.Limits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.BoBusinessIncomeFromDependentProperties],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.BusinessIncome,
                                                        ColumnValueType.Enum.FromDependentProperties,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.BINC_DPND }, 3));
            #endregion

            #region Commercial Auto

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerAutoLiability],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.NonOwnerAuto,
                                                        ColumnValueType.Enum.Liability,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 4));
            #endregion

            #region Commercial Umbrella

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                        ColumnValueType.Enum.OccuranceLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 5));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuAggregateLimits],
                                                        LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                        ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                        ColumnValueType.Enum.AggregateLimits,
                                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 6));
            #endregion

            #region Crime

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits],
                                                   LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                   ColumnGroupType.Enum.Crime,
                                                   ColumnValueType.Enum.InsideThePremesisTheftOfMoneyAndSecurites,
                                                   new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 7));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits],
                                                    LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.InsideThePremesisRobberyOfMoneyAndSecurites,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 8));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits],
                                                    LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.OutsideThePremesisRobberyOfMoneyAndSecurites,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 9));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits],
                                                    LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.TheftPerEmployee,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 10));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcForgeryAlterationLimits],
                                                    LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.ForgeryAlteration,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 11));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcClientsPropertyLimits],
                                                    LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.ClientsProperty,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 12));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcFundsTransferFraudLimits],
                                                    LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.FundsTransferFraud,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 13));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcComputerFraudLimits],
                                                    LineOfBusiness.LineOfBusinessType.BusinessOwner,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.ComputerFraud,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 14));

            #endregion

            return oColumns;
        }
        #endregion

        #region Workers Comp
        protected override List<CompanyCoverageColumn> CreateWorkersComp()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.WcExposure],
                                                    LineOfBusiness.LineOfBusinessType.WorkersComp,
                                                    ColumnGroupType.Enum.Exposure,
                                                    ColumnValueType.Enum.ClassCodePayrollEmployees,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            return oColumns;
        }
        #endregion

        #region Commercial Output
        protected override List<CompanyCoverageColumn> CreateCommercialOutput()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CoBuildingLimit],
                                        LineOfBusiness.LineOfBusinessType.CommercialOutputPolicy,
                                        ColumnGroupType.Enum.Building,
                                        ColumnValueType.Enum.Limits,
                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CoBuildingCatastropheLimit],
                                        LineOfBusiness.LineOfBusinessType.CommercialOutputPolicy,
                                        ColumnGroupType.Enum.Building,
                                        ColumnValueType.Enum.CatastropheLimits,
                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.DN_CATLMT }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CoPersonalPropertyLimit],
                                        LineOfBusiness.LineOfBusinessType.CommercialOutputPolicy,
                                        ColumnGroupType.Enum.PersonalProperty,
                                        ColumnValueType.Enum.Limits,
                                        new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            return oColumns;
        }

        #endregion

        #region Commercial Auto
        protected override List<CompanyCoverageColumn> CreateCommercialAuto()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.PhysicalDamageLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.COMP }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaVehicleCount],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.NumberOfVehicles,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.NonOwnerAuto,
                                                    ColumnValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.NonOwnerAuto,
                                                    ColumnValueType.Enum.PhysicalDamageLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 4));

            return oColumns;
        }
        #endregion

        #region Commercial Umbrella
        protected override List<CompanyCoverageColumn> CreateCommercialUmbrella()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialUmbrella,
                                                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialUmbrella,
                                                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 1));

            return oColumns;
        }
        #endregion

        #region General Liablility
        protected override List<CompanyCoverageColumn> CreateGeneralLiability()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR, PolicyStarCoverageValueType.Enum.LIAB_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_AGG, PolicyStarCoverageValueType.Enum.LIAB_AGG }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.Exposures,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL1 }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.ProductsOperations,
                                                    ColumnValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROD, PolicyStarCoverageValueType.Enum.PROD_CPT }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlNonOwnerBodilyInjuryLimit],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.NonOwnerAuto,
                                                    ColumnValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 4));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlNonOwnerPhysicalDamageLimit],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.NonOwnerAuto,
                                                    ColumnValueType.Enum.PhysicalDamageLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 5));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlProfessionalLiabilityOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.ProfessionalLiability,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROF_OCCR }, 6));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlProfessionalLiabilityAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.ProfessionalLiability,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROF_AGG }, 7));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlSexualAbuseOccuranceLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.SexualAbuse,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.ABSE_OCCR }, 8));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlSexualAbuseAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.SexualAbuse,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.ABSE_AGG }, 9));

            return oColumns;
        }
        #endregion

        #region Commercial Property
        protected override List<CompanyCoverageColumn> CreateCommercialProperty()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            #region Commercial Auto

            oColumns.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                LineOfBusiness.LineOfBusinessType.CommercialProperty,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB }, 0));

            oColumns.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit],
                                LineOfBusiness.LineOfBusinessType.CommercialProperty,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.PhysicalDamageLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.COMP }, 1));

            oColumns.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaVehicleCount],
                                LineOfBusiness.LineOfBusinessType.CommercialProperty,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.NumberOfVehicles,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                LineOfBusiness.LineOfBusinessType.CommercialProperty,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.NonOwnerAuto,
                                ColumnValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 3));

            oColumns.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                LineOfBusiness.LineOfBusinessType.CommercialProperty,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.NonOwnerAuto,
                                ColumnValueType.Enum.PhysicalDamageLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 4));

            #endregion

            #region Inland Marine
            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                    ColumnValueType.Enum.OwnedEquipment,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.FROM_OTH }, 5));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                    ColumnValueType.Enum.ToOthers,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TO_OTH }, 6));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                    ColumnValueType.Enum.FromOthers,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PRMS_RENT }, 7));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImToolFloaterLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ToolFloater,
                    ColumnValueType.Enum.YourTools,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.YOUR_TOOL }, 8));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ToolFloater,
                    ColumnValueType.Enum.EmployeeTools,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EMP_TOOL }, 9));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImComputerHardwareLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ComputerEquipment,
                    ColumnValueType.Enum.HardwareLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 10));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImSignsScheduledLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.Signs,
                    ColumnValueType.Enum.ScheduledLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN }, 11));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.Signs,
                    ColumnValueType.Enum.UnscheduledLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN_USCHD }, 12));

            #endregion

            #region Commercial Umbrella
            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                    ColumnValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 13));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CuAggregateLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                    ColumnValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 14));
            #endregion

            #region General Liability
            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR }, 15));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_AGG }, 16));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.Exposures,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 17));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.ProductsOperationsLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROD }, 18));

            #endregion

            #region Crime

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.InsideThePremesisTheftOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 19));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.InsideThePremesisRobberyOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 20));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.OutsideThePremesisRobberyOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 21));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.TheftPerEmployee,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 22));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcForgeryAlterationLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.ForgeryAlteration,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 23));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcClientsPropertyLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.ClientsProperty,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 24));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcFundsTransferFraudLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.FundsTransferFraud,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 25));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcComputerFraudLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.ComputerFraud,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 26));

            #endregion

            #region Commercial Property

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits],
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.BusinessIncome,
                    ColumnValueType.Enum.Limits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 23));

            #endregion

            return oColumns;
        }
        #endregion

        #region Transportation
        protected override List<CompanyCoverageColumn> CreateTransporation()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            #region Commercial Auto

            oColumns.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                LineOfBusiness.LineOfBusinessType.Transportation,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB }, 0));

            oColumns.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit],
                                LineOfBusiness.LineOfBusinessType.Transportation,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.PhysicalDamageLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.COMP }, 1));

            oColumns.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaVehicleCount],
                                LineOfBusiness.LineOfBusinessType.Transportation,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.NumberOfVehicles,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                LineOfBusiness.LineOfBusinessType.Transportation,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.NonOwnerAuto,
                                ColumnValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 3));

            oColumns.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                LineOfBusiness.LineOfBusinessType.Transportation,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.NonOwnerAuto,
                                ColumnValueType.Enum.PhysicalDamageLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 4));

            #endregion

            #region Inland Marine
            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                    ColumnValueType.Enum.OwnedEquipment,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.FROM_OTH }, 5));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                    ColumnValueType.Enum.ToOthers,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TO_OTH }, 6));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                    ColumnValueType.Enum.FromOthers,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PRMS_RENT }, 7));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImToolFloaterLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ToolFloater,
                    ColumnValueType.Enum.YourTools,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.YOUR_TOOL }, 8));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ToolFloater,
                    ColumnValueType.Enum.EmployeeTools,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EMP_TOOL }, 9));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImComputerHardwareLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ComputerEquipment,
                    ColumnValueType.Enum.HardwareLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 10));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImSignsScheduledLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.Signs,
                    ColumnValueType.Enum.ScheduledLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN }, 11));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.Signs,
                    ColumnValueType.Enum.UnscheduledLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN_USCHD }, 12));

            #endregion

            #region Commercial Umbrella
            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                    ColumnValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 13));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CuAggregateLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                    ColumnValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 14));
            #endregion

            #region General Liability
            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR }, 15));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_AGG }, 16));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.Exposures,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 17));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.ProductsOperationsLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROD }, 18));

            #endregion

            #region Crime

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.InsideThePremesisTheftOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 19));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.InsideThePremesisRobberyOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 20));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.OutsideThePremesisRobberyOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 21));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.TheftPerEmployee,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 22));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcForgeryAlterationLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.ForgeryAlteration,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 23));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcClientsPropertyLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.ClientsProperty,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 24));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcFundsTransferFraudLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.FundsTransferFraud,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 25));

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcComputerFraudLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.ComputerFraud,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 26));

            #endregion

            #region Commercial Property

            oColumns.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits],
                    LineOfBusiness.LineOfBusinessType.Transportation,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.BusinessIncome,
                    ColumnValueType.Enum.Limits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 23));

            #endregion

            return oColumns;
        }
        #endregion

        #region Inland Marine
        protected override List<CompanyCoverageColumn> CreateInlandMarine()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.MotorTruckCargo,
                                                    ColumnValueType.Enum.LegalLiabilityLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CRGO_CAT }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.MotorTruckCargo,
                                                    ColumnValueType.Enum.VehicleLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROP_VEH }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.ConstructionLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.BLDRS_CAT }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.ExistingBuildingLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CSE_CONS }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilitySpoilageLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.WarehouseLiability,
                                                    ColumnValueType.Enum.SpoilageLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilityColdStorageLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.WarehouseLiability,
                                                    ColumnValueType.Enum.ColdStorageLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImJewelersBlockPremisesLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.JewelersBlock,
                                                    ColumnValueType.Enum.YourPremisesLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                                                    ColumnValueType.Enum.OwnedEquipment,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CONTR_CAT }, 7));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                                                    ColumnValueType.Enum.ToOthers,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TO_OTH }, 8));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                                                    ColumnValueType.Enum.FromOthers,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.FROM_OTH }, 9));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImToolFloaterLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ToolFloater,
                                                    ColumnValueType.Enum.YourTools,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NEW_EQUIP }, 10));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ToolFloater,
                                                    ColumnValueType.Enum.EmployeeTools,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EMP_TOOL }, 11));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImComputerHardwareLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.ComputerEquipment,
                                                    ColumnValueType.Enum.HardwareLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EDP_HDWR }, 12));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImSignsScheduledLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.Signs,
                                                    ColumnValueType.Enum.ScheduledLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN }, 13));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.Signs,
                                                    ColumnValueType.Enum.UnscheduledLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN_USCHD }, 14));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImTransitTransportationLimit],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.TransitCoverage,
                                                    ColumnValueType.Enum.TransportationLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TRNP }, 15));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImTransitTransportationScheduleLimit],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.TransitCoverage,
                                                    ColumnValueType.Enum.TransportationScheduleLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TRNP_SCHED }, 16));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImInstallFloaterInstallationCatostrophic],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.InstallationFloater,
                                                    ColumnValueType.Enum.InstallationCatostrophic,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.INST_CAT }, 17));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImInstallFloaterJobsiteSchedule],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.InstallationFloater,
                                                    ColumnValueType.Enum.JobsiteSchedule,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.INST_JSITE }, 18));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImInstallFloaterInTransit],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.InstallationFloater,
                                                    ColumnValueType.Enum.InTransit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TRNS_IN }, 19));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImInstallFloaterTemporaryLocations],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.InstallationFloater,
                                                    ColumnValueType.Enum.TemporaryLocations,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TEMP_STO }, 20));

            return oColumns;
        }
        #endregion

        #region Ocean Marine
        protected override List<CompanyCoverageColumn> CreateOceanMarine()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmPhysicalDamageVesselLimits],
                                                   LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                   ColumnGroupType.Enum.PhysicalDamage,
                                                   ColumnValueType.Enum.VesselLimits,
                                                   new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmPhysicalDamageExcessCollisionLimits],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.PhysicalDamage,
                                                    ColumnValueType.Enum.ExcessCollisionLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmPhysicalDamageBreachOfWarrantyLimits],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.PhysicalDamage,
                                                    ColumnValueType.Enum.BreachofWarrantyLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmLegalLiabilityLimits],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.LegalLiability,
                                                    ColumnValueType.Enum.LegalLiabilityLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmLegalLiabilityMedicalPaymentsLimits],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.LegalLiability,
                                                    ColumnValueType.Enum.MedicalPaymentsLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmBoatDealersCoverageDLimits],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.BoatDealer,
                                                    ColumnValueType.Enum.CoverageDLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmBoatDealersCoverageALimits],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.BoatDealer,
                                                    ColumnValueType.Enum.CoverageALimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 6));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmBuildersRiskAmountInsured],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.AmountInsured,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 7));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.OmBuildersRiskAgreedValue],
                                                    LineOfBusiness.LineOfBusinessType.OceanMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.AgreedValue,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 8));

            return oColumns;
        }
        #endregion

        #region Package

        protected override List<CompanyCoverageColumn> CreatePackages()
        {
            List<CompanyCoverageColumn> oPackageList = new List<CompanyCoverageColumn>();

            #region Commercial Auto

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB }, 0));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.PhysicalDamageLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.COMP }, 1));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaVehicleCount],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.CommercialAuto,
                                ColumnValueType.Enum.NumberOfVehicles,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.NonOwnerAuto,
                                ColumnValueType.Enum.BodilyInjuryLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 3));

            oPackageList.Add(new CompanyCoverageColumn(
                                moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                LineOfBusiness.LineOfBusinessType.Package,
                                LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                ColumnGroupType.Enum.NonOwnerAuto,
                                ColumnValueType.Enum.PhysicalDamageLimit,
                                new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 4));

            #endregion

            #region Inland Marine
            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                    ColumnValueType.Enum.OwnedEquipment,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CONTR_CAT }, 5));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                    ColumnValueType.Enum.ToOthers,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TO_OTH }, 6));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ContractorsEquipmentLeased,
                    ColumnValueType.Enum.FromOthers,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.FROM_OTH }, 7));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImToolFloaterLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ToolFloater,
                    ColumnValueType.Enum.YourTools,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NEW_EQUIP }, 8));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ToolFloater,
                    ColumnValueType.Enum.EmployeeTools,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EMP_TOOL }, 9));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImComputerHardwareLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.ComputerEquipment,
                    ColumnValueType.Enum.HardwareLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EDP_HDWR }, 10));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImSignsScheduledLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.Signs,
                    ColumnValueType.Enum.ScheduledLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN }, 11));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                    ColumnGroupType.Enum.Signs,
                    ColumnValueType.Enum.UnscheduledLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.SIGN_USCHD }, 12));

            #endregion

            #region Commercial Umbrella
            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                    ColumnValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 13));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CuAggregateLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                    ColumnValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 14));
            #endregion

            #region General Liability
            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR, PolicyStarCoverageValueType.Enum.LIAB_OCCR }, 15));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_AGG, PolicyStarCoverageValueType.Enum.LIAB_AGG }, 16));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.Exposures,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL1 }, 17));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.GeneralLiability,
                    ColumnValueType.Enum.ProductsOperationsLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROD, PolicyStarCoverageValueType.Enum.PROD_CPT }, 18));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlProfessionalLiabilityOccurrenceLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.ProfessionalLiability,
                    ColumnValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROF_OCCR }, 19));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlProfessionalLiabilityAggregateLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.ProfessionalLiability,
                    ColumnValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROF_AGG }, 20));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlSexualAbuseOccuranceLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.SexualAbuse,
                    ColumnValueType.Enum.OccuranceLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.ABSE_OCCR }, 21));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.GlSexualAbuseAggregateLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                    ColumnGroupType.Enum.SexualAbuse,
                    ColumnValueType.Enum.AggregateLimits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.ABSE_AGG }, 22));

            #endregion

            #region Crime

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.InsideThePremesisTheftOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 23));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.InsideThePremesisRobberyOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 24));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.OutsideThePremesisRobberyOfMoneyAndSecurites,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 25));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.TheftPerEmployee,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 26));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcForgeryAlterationLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.ForgeryAlteration,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 27));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcClientsPropertyLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.ClientsProperty,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 28));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcFundsTransferFraudLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.FundsTransferFraud,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 29));

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CcComputerFraudLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.Unknown,
                    ColumnGroupType.Enum.Crime,
                    ColumnValueType.Enum.ComputerFraud,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 30));

            #endregion

            #region Commercial Property

            oPackageList.Add(new CompanyCoverageColumn(
                    moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits],
                    LineOfBusiness.LineOfBusinessType.Package,
                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                    ColumnGroupType.Enum.BusinessIncome,
                    ColumnValueType.Enum.Limits,
                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 31));

            #endregion

            return oPackageList;
        }

        #endregion

        public override int GetMaxColumnNumber()
        {
            //largest column number plus one
            return 66;
        }

        protected override void BuildCoverageMap()
        {
            // Business Owner
            moCoverageDictionary[CoverageMap.BoGeneralLiabilityOccurrenceLimits] = 4;
            moCoverageDictionary[CoverageMap.BoGeneralLiabilityAggregateLimits] = 5;
            moCoverageDictionary[CoverageMap.BoBusinessIncomeLimits] = 6;
            moCoverageDictionary[CoverageMap.BoBusinessIncomeFromDependentProperties] = 7;

            // Commercial Output
            moCoverageDictionary[CoverageMap.CoBuildingLimit] = 9;
            moCoverageDictionary[CoverageMap.CoBuildingCatastropheLimit] = 10;
            moCoverageDictionary[CoverageMap.CoPersonalPropertyLimit] = 11;

            // Commercial Umbrella
            moCoverageDictionary[CoverageMap.CuOccurrenceLimits] = 39;
            moCoverageDictionary[CoverageMap.CuAggregateLimits] = 40;

            // Workers Comp
            moCoverageDictionary[CoverageMap.WcExposure] = 12;

            // Commercial Auto
            moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit] = 13;
            moCoverageDictionary[CoverageMap.CaPhysicalDamageLimit] = 14;
            moCoverageDictionary[CoverageMap.CaVehicleCount] = 15;
            moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit] = 16;
            moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit] = 17;
            moCoverageDictionary[CoverageMap.CaNonOwnerAutoLiability] = 8;

            // Commercial Crime
            moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits] = 41;
            moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits] = 42;
            moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits] = 44;
            moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits] = 45;
            moCoverageDictionary[CoverageMap.CcForgeryAlterationLimits] = 47;
            moCoverageDictionary[CoverageMap.CcClientsPropertyLimits] = 48;
            moCoverageDictionary[CoverageMap.CcFundsTransferFraudLimits] = 49;
            moCoverageDictionary[CoverageMap.CcComputerFraudLimits] = 50;

            // General Liability
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits] = 21;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits] = 22;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures] = 23;
            moCoverageDictionary[CoverageMap.GlProductsOperationsLimits] = 24;
            moCoverageDictionary[CoverageMap.GlNonOwnerBodilyInjuryLimit] = 59;
            moCoverageDictionary[CoverageMap.GlNonOwnerPhysicalDamageLimit] = 59;
            moCoverageDictionary[CoverageMap.GlProfessionalLiabilityOccurrenceLimits] = 59;
            moCoverageDictionary[CoverageMap.GlProfessionalLiabilityAggregateLimits] = 59;
            moCoverageDictionary[CoverageMap.GlSexualAbuseOccuranceLimits] = 59;
            moCoverageDictionary[CoverageMap.GlSexualAbuseAggregateLimits] = 59;

            // Commercial Property
            moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits] = 43;

            // Inland Marine
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits] = 25;
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits] = 26;
            moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits] = 27;
            moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits] = 28;
            moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilitySpoilageLimits] = 29;
            moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilityColdStorageLimits] = 30;
            moCoverageDictionary[CoverageMap.ImJewelersBlockPremisesLimits] = 31;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentOwnedEquipment] = 46;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentToOthersLimits] = 32;
            moCoverageDictionary[CoverageMap.ImContractorsEquipmentFromOthersLimits] = 33;
            moCoverageDictionary[CoverageMap.ImToolFloaterLimits] = 34;
            moCoverageDictionary[CoverageMap.ImToolFloaterEmployeeToolsLimits] = 35;
            moCoverageDictionary[CoverageMap.ImComputerHardwareLimits] = 36;
            moCoverageDictionary[CoverageMap.ImSignsScheduledLimits] = 37;
            moCoverageDictionary[CoverageMap.ImSignsUnscheduledLimits] = 38;
            //moCoverageDictionary[CoverageMap.ImTransitTransportationLimit] = 60;
            //moCoverageDictionary[CoverageMap.ImTransitTransportationScheduleLimit] = 61;
            //moCoverageDictionary[CoverageMap.ImInstallFloaterInstallationCatostrophic] = 62;
            //moCoverageDictionary[CoverageMap.ImInstallFloaterJobsiteSchedule] = 63;
            //moCoverageDictionary[CoverageMap.ImInstallFloaterInTransit] = 64;
            //moCoverageDictionary[CoverageMap.ImInstallFloaterTemporaryLocations] = 65;

            moCoverageDictionary[CoverageMap.ImTransitTransportationLimit] = 59;
            moCoverageDictionary[CoverageMap.ImTransitTransportationScheduleLimit] = 59;
            moCoverageDictionary[CoverageMap.ImInstallFloaterInstallationCatostrophic] = 59;
            moCoverageDictionary[CoverageMap.ImInstallFloaterJobsiteSchedule] = 59;
            moCoverageDictionary[CoverageMap.ImInstallFloaterInTransit] = 59;
            moCoverageDictionary[CoverageMap.ImInstallFloaterTemporaryLocations] = 59;

            // Ocean Marine
            moCoverageDictionary[CoverageMap.OmPhysicalDamageVesselLimits] = 51;
            moCoverageDictionary[CoverageMap.OmPhysicalDamageExcessCollisionLimits] = 52;
            moCoverageDictionary[CoverageMap.OmPhysicalDamageBreachOfWarrantyLimits] = 53;
            moCoverageDictionary[CoverageMap.OmLegalLiabilityLimits] = 54;
            moCoverageDictionary[CoverageMap.OmLegalLiabilityMedicalPaymentsLimits] = 55;
            moCoverageDictionary[CoverageMap.OmBoatDealersCoverageDLimits] = 56;
            moCoverageDictionary[CoverageMap.OmBoatDealersCoverageALimits] = 57;
            moCoverageDictionary[CoverageMap.OmBuildersRiskAmountInsured] = 58;
            moCoverageDictionary[CoverageMap.OmBuildersRiskAgreedValue] = 59;
        }
	}
}
