using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class BOGCoverageDictionary : CoverageDictionary
    {
        public BOGCoverageDictionary()
        {
        }

        #region Workers Comp
        protected override List<CompanyCoverageColumn> CreateWorkersComp()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.WcExposure],
                                                    LineOfBusiness.LineOfBusinessType.WorkersComp,
                                                    ColumnGroupType.Enum.Exposure,
                                                    ColumnValueType.Enum.ClassCodePayrollEmployees,
                                                    "CLASS", 0));

            return oColumns;
        }
        #endregion

        #region Commercial Auto
        protected override List<CompanyCoverageColumn> CreateCommercialAuto()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.LIAB }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaVehicleCount],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.CommercialAuto,
                                                    ColumnValueType.Enum.NumberOfVehicles,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.NonOwnerAuto,
                                                    ColumnValueType.Enum.BodilyInjuryLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.HRD_LIAB }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.NonOwnerAuto,
                                                    ColumnValueType.Enum.PhysicalDamageLimit,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NOWN_LIAB }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CaGaragekeepersLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialAuto,
                                                    ColumnGroupType.Enum.Garagekeeper,
                                                    ColumnValueType.Enum.Garagekeeper,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            return oColumns;
        }
        #endregion

        #region Commercial Crime
        protected override List<CompanyCoverageColumn> CreateCommercialCrime()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits],
                                                   LineOfBusiness.LineOfBusinessType.CommercialCrime,
                                                   ColumnGroupType.Enum.Crime,
                                                   ColumnValueType.Enum.InsideThePremesisTheftOfMoneyAndSecurites,
                                                   new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialCrime,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.InsideThePremesisRobberyOfMoneyAndSecurites,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialCrime,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.OutsideThePremesisRobberyOfMoneyAndSecurites,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialCrime,
                                                    ColumnGroupType.Enum.Crime,
                                                    ColumnValueType.Enum.TheftPerEmployee,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 3));

            return oColumns;
        }
        #endregion

        #region Commercial Umbrella
        protected override List<CompanyCoverageColumn> CreateCommercialUmbrella()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialUmbrella,
                                                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.EA_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CuAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialUmbrella,
                                                    ColumnGroupType.Enum.CommercialUmbrellaNCR,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.GENL_AGG }, 1));

            return oColumns;
        }
        #endregion

        #region General Liablility
        protected override List<CompanyCoverageColumn> CreateGeneralLiability()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.OccuranceLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL_OCCR, PolicyStarCoverageValueType.Enum.LIAB_OCCR }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.AggregateLimits,
                                                    "General Aggregate Limit", 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.GeneralLiability,
                                                    ColumnValueType.Enum.Exposures,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CGL1 }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.GlProductsOperationsLimits],
                                                    LineOfBusiness.LineOfBusinessType.GeneralLiability,
                                                    ColumnGroupType.Enum.ProductsOperations,
                                                    ColumnValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROD, PolicyStarCoverageValueType.Enum.PROD_CPT }, 3));

            return oColumns;
        }
        #endregion

        #region Commercial Property
        protected override List<CompanyCoverageColumn> CreateCommercialProperty()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits],
                                                    LineOfBusiness.LineOfBusinessType.CommercialProperty,
                                                    ColumnGroupType.Enum.BusinessIncome,
                                                    ColumnValueType.Enum.Limits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.TE }, 0));
            return oColumns;
        }
        #endregion

        #region Inland Marine
        protected override List<CompanyCoverageColumn> CreateInlandMarine()
        {
            List<CompanyCoverageColumn> oColumns = new List<CompanyCoverageColumn>();
            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.MotorTruckCargo,
                                                    ColumnValueType.Enum.LegalLiabilityLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CRGO_CAT }, 0));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.MotorTruckCargo,
                                                    ColumnValueType.Enum.VehicleLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.PROP_VEH }, 1));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.ConstructionLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.BLDRS_CAT }, 2));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.BuildersRisk,
                                                    ColumnValueType.Enum.ExistingBuildingLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.CSE_CONS }, 3));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilitySpoilageLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.WarehouseLiability,
                                                    ColumnValueType.Enum.SpoilageLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 4));

            oColumns.Add(new CompanyCoverageColumn(moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilityColdStorageLimits],
                                                    LineOfBusiness.LineOfBusinessType.InlandMarine,
                                                    ColumnGroupType.Enum.WarehouseLiability,
                                                    ColumnValueType.Enum.ColdStorageLimits,
                                                    new PolicyStarCoverageValueType.Enum[] { PolicyStarCoverageValueType.Enum.NONE }, 5));

            return oColumns;
        }
        #endregion

        protected override void BuildCoverageMap()
        {

            // Commercial Umbrella
            moCoverageDictionary[CoverageMap.CuOccurrenceLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.CuAggregateLimits] = int.MinValue;

            // Workers Comp
            moCoverageDictionary[CoverageMap.WcExposure] = int.MinValue;

            // Commercial Auto
            moCoverageDictionary[CoverageMap.CaBodilyInjuryLimit] = int.MinValue;
            moCoverageDictionary[CoverageMap.CaVehicleCount] = int.MinValue;
            moCoverageDictionary[CoverageMap.CaNonOwnerBodilyInjuryLimit] = int.MinValue;
            moCoverageDictionary[CoverageMap.CaNonOwnerPhysicalDamageLimit] = int.MinValue;
            moCoverageDictionary[CoverageMap.CaNonOwnerAutoLiability] = int.MinValue;
            moCoverageDictionary[CoverageMap.CaGaragekeepersLimits] = int.MinValue;

            // Commercial Crime
            moCoverageDictionary[CoverageMap.CcInsidePremisesRobberyLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.CcTheftPerEmployeeLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.CcInsidePremisesTheftLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.CcOutsidePremisesRobberyLimits] = int.MinValue;

            // General Liability
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityOccurrenceLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityAggregateLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.GlGeneralLiabilityExposures] = int.MinValue;
            moCoverageDictionary[CoverageMap.GlProductsOperationsLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.GlFRPLiabilityLimits] = int.MinValue;

            // Commercial Property
            moCoverageDictionary[CoverageMap.CpBusinessIncomeLimits] = int.MinValue;

            // Inland Marine
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoLegalLiabilityLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImMotorTruckCargoVehicleLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImBuildersRiskConstructionLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImBuildersRiskExistingBuildingLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilitySpoilageLimits] = int.MinValue;
            moCoverageDictionary[CoverageMap.ImWarehouseLegalLiabilityColdStorageLimits] = int.MinValue;

        }
    }
}
