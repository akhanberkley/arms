using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class BLSLineOfBusiness : LineOfBusiness
	{
		public BLSLineOfBusiness()
		{
		}

		protected override void DerivedBuildMap(ref Dictionary<int, string> oDictionary)
		{
			// Pipe delimit multiple symbols for a LOB
            oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(Entities.Company.BerkleyLifeSciences, Entities.LineOfBusiness.CommercialAuto));
            oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(Entities.Company.BerkleyLifeSciences, Entities.LineOfBusiness.GeneralLiability));
            oDictionary.Add((int)LineOfBusinessType.CommercialUmbrella, GetCompanyPolicySymbols(Entities.Company.BerkleyLifeSciences, Entities.LineOfBusiness.CommercialUmbrella));
            oDictionary.Add((int)LineOfBusinessType.InlandMarine, GetCompanyPolicySymbols(Entities.Company.BerkleyLifeSciences, Entities.LineOfBusiness.InlandMarine));
            oDictionary.Add((int)LineOfBusinessType.Package, GetCompanyPolicySymbols(Entities.Company.BerkleyLifeSciences, Entities.LineOfBusiness.Package));
            oDictionary.Add((int)LineOfBusinessType.WorkersComp, GetCompanyPolicySymbols(Entities.Company.BerkleyLifeSciences, Entities.LineOfBusiness.WorkersCompensation));

            moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, Entities.LineOfBusiness.CommercialAuto.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, Entities.LineOfBusiness.GeneralLiability.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialUmbrella, Entities.LineOfBusiness.CommercialUmbrella.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, Entities.LineOfBusiness.InlandMarine.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.Package, Entities.LineOfBusiness.Package.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.WorkersComp, Entities.LineOfBusiness.WorkersCompensation.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialProperty, Entities.LineOfBusiness.CommercialProperty.Code);
		}

	}
}
