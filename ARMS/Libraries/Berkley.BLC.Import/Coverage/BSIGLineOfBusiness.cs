using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class BSIGLineOfBusiness : LineOfBusiness
	{
		public BSIGLineOfBusiness()
		{ 
		}

		protected override void DerivedBuildMap(ref Dictionary<int, string> oDictionary)
		{
            oDictionary.Add((int)LineOfBusinessType.BusinessOwner, GetCompanyPolicySymbols(Entities.Company.BerkleyMidAtlanticGroup, Entities.LineOfBusiness.BusinessOwner));
            oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(Entities.Company.BerkleyMidAtlanticGroup, Entities.LineOfBusiness.CommercialAuto));
            oDictionary.Add((int)LineOfBusinessType.CommercialOutputPolicy, GetCompanyPolicySymbols(Entities.Company.BerkleyMidAtlanticGroup, Entities.LineOfBusiness.CommercialOutput));
            oDictionary.Add((int)LineOfBusinessType.CommercialProperty, GetCompanyPolicySymbols(Entities.Company.BerkleyMidAtlanticGroup, Entities.LineOfBusiness.CommercialProperty));
            oDictionary.Add((int)LineOfBusinessType.CommercialUmbrella, GetCompanyPolicySymbols(Entities.Company.BerkleyMidAtlanticGroup, Entities.LineOfBusiness.CommercialUmbrella));
            oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(Entities.Company.BerkleyMidAtlanticGroup, Entities.LineOfBusiness.GeneralLiability));
            oDictionary.Add((int)LineOfBusinessType.InlandMarine, GetCompanyPolicySymbols(Entities.Company.BerkleyMidAtlanticGroup, Entities.LineOfBusiness.InlandMarine));
            oDictionary.Add((int)LineOfBusinessType.OceanMarine, GetCompanyPolicySymbols(Entities.Company.BerkleyMidAtlanticGroup, Entities.LineOfBusiness.OceanMarine));
            oDictionary.Add((int)LineOfBusinessType.Package, GetCompanyPolicySymbols(Entities.Company.BerkleyMidAtlanticGroup, Entities.LineOfBusiness.Package));
            oDictionary.Add((int)LineOfBusinessType.WorkersComp, GetCompanyPolicySymbols(Entities.Company.BerkleyMidAtlanticGroup, Entities.LineOfBusiness.WorkersCompensation));
            oDictionary.Add((int)LineOfBusinessType.Transportation, GetCompanyPolicySymbols(Entities.Company.BerkleyMidAtlanticGroup, Entities.LineOfBusiness.Transportation));

            moBlcLobDictionary.Add(LineOfBusinessType.BusinessOwner, Entities.LineOfBusiness.BusinessOwner.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, Entities.LineOfBusiness.CommercialAuto.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialOutputPolicy, Entities.LineOfBusiness.CommercialOutput.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialProperty, Entities.LineOfBusiness.CommercialProperty.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialUmbrella, Entities.LineOfBusiness.CommercialUmbrella.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, Entities.LineOfBusiness.GeneralLiability.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, Entities.LineOfBusiness.InlandMarine.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.OceanMarine, Entities.LineOfBusiness.OceanMarine.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.Package, Entities.LineOfBusiness.Package.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.WorkersComp, Entities.LineOfBusiness.WorkersCompensation.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.Transportation, Entities.LineOfBusiness.Transportation.Code);
		}
	}
}
