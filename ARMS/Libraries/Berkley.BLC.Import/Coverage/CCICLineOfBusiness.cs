using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class CCICLineOfBusiness : LineOfBusiness
	{
		public CCICLineOfBusiness()
		{ 
		}

		protected override void DerivedBuildMap(ref Dictionary<int, string> oDictionary)
		{
            oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(Entities.Company.CarolinaCasualtyInsuranceGroup, Entities.LineOfBusiness.CommercialAuto));
            oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(Entities.Company.CarolinaCasualtyInsuranceGroup, Entities.LineOfBusiness.GeneralLiability));
            //oDictionary.Add((int)LineOfBusinessType.InlandMarine, "ARP");

			moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, Entities.LineOfBusiness.CommercialAuto.Code);
			moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, Entities.LineOfBusiness.GeneralLiability.Code);
			//moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, "IM");
		}
	}
}
