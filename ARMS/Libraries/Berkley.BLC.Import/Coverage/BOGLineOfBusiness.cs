using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class BOGLineOfBusiness : LineOfBusiness
	{
        public BOGLineOfBusiness()
		{
		}

		protected override void DerivedBuildMap(ref Dictionary<int, string> oDictionary)
		{
			// Pipe delimit multiple symbols for a LOB
            oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(Entities.Company.BerkleyOilGas, Entities.LineOfBusiness.CommercialAuto));
            oDictionary.Add((int)LineOfBusinessType.CommercialProperty, GetCompanyPolicySymbols(Entities.Company.BerkleyOilGas, Entities.LineOfBusiness.CommercialProperty));
            oDictionary.Add((int)LineOfBusinessType.CommercialUmbrella, GetCompanyPolicySymbols(Entities.Company.BerkleyOilGas, Entities.LineOfBusiness.CommercialUmbrella));
            oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(Entities.Company.BerkleyOilGas, Entities.LineOfBusiness.GeneralLiability));
            oDictionary.Add((int)LineOfBusinessType.InlandMarine, GetCompanyPolicySymbols(Entities.Company.BerkleyOilGas, Entities.LineOfBusiness.InlandMarine));
            oDictionary.Add((int)LineOfBusinessType.WorkersComp, GetCompanyPolicySymbols(Entities.Company.BerkleyOilGas, Entities.LineOfBusiness.WorkersCompensation));
            oDictionary.Add((int)LineOfBusinessType.CommercialCrime, GetCompanyPolicySymbols(Entities.Company.BerkleyOilGas, Entities.LineOfBusiness.CommercialCrime));

            moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, Entities.LineOfBusiness.CommercialAuto.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialCrime, Entities.LineOfBusiness.CommercialCrime.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialProperty, Entities.LineOfBusiness.CommercialProperty.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialUmbrella, Entities.LineOfBusiness.CommercialUmbrella.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, Entities.LineOfBusiness.GeneralLiability.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, Entities.LineOfBusiness.InlandMarine.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.WorkersComp, Entities.LineOfBusiness.WorkersCompensation.Code);
		}

	}
}
