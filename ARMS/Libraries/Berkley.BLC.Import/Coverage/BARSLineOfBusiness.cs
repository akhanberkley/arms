using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class BARSLineOfBusiness : LineOfBusiness
	{
		public BARSLineOfBusiness()
		{
		}

		protected override void DerivedBuildMap(ref Dictionary<int, string> oDictionary)
		{
			// Pipe delimit multiple symbols for a LOB
            oDictionary.Add((int)LineOfBusinessType.BusinessOwner, GetCompanyPolicySymbols(Entities.Company.BerkleyAgribusinessRiskSpecialists, Entities.LineOfBusiness.BusinessOwner));
            oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(Entities.Company.BerkleyAgribusinessRiskSpecialists, Entities.LineOfBusiness.CommercialAuto));
            oDictionary.Add((int)LineOfBusinessType.CommercialProperty, GetCompanyPolicySymbols(Entities.Company.BerkleyAgribusinessRiskSpecialists, Entities.LineOfBusiness.CommercialProperty));
            oDictionary.Add((int)LineOfBusinessType.CommercialCrime, GetCompanyPolicySymbols(Entities.Company.BerkleyAgribusinessRiskSpecialists, Entities.LineOfBusiness.CommercialCrime));
            oDictionary.Add((int)LineOfBusinessType.CommercialUmbrella, GetCompanyPolicySymbols(Entities.Company.BerkleyAgribusinessRiskSpecialists, Entities.LineOfBusiness.CommercialUmbrella));
            oDictionary.Add((int)LineOfBusinessType.Garage, GetCompanyPolicySymbols(Entities.Company.BerkleyAgribusinessRiskSpecialists, Entities.LineOfBusiness.Garage));
            oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(Entities.Company.BerkleyAgribusinessRiskSpecialists, Entities.LineOfBusiness.GeneralLiability));
            oDictionary.Add((int)LineOfBusinessType.InlandMarine, GetCompanyPolicySymbols(Entities.Company.BerkleyAgribusinessRiskSpecialists, Entities.LineOfBusiness.InlandMarine));
            oDictionary.Add((int)LineOfBusinessType.Package, GetCompanyPolicySymbols(Entities.Company.BerkleyAgribusinessRiskSpecialists, Entities.LineOfBusiness.Package));
            oDictionary.Add((int)LineOfBusinessType.WorkersComp, GetCompanyPolicySymbols(Entities.Company.BerkleyAgribusinessRiskSpecialists, Entities.LineOfBusiness.WorkersCompensation));

            moBlcLobDictionary.Add(LineOfBusinessType.BusinessOwner, Entities.LineOfBusiness.BusinessOwner.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, Entities.LineOfBusiness.CommercialAuto.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialCrime, Entities.LineOfBusiness.CommercialCrime.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialProperty, Entities.LineOfBusiness.CommercialProperty.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialUmbrella, Entities.LineOfBusiness.CommercialUmbrella.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.Dealer, Entities.LineOfBusiness.Dealer.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.Garage, Entities.LineOfBusiness.Garage.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, Entities.LineOfBusiness.GeneralLiability.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, Entities.LineOfBusiness.InlandMarine.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.OceanMarine, Entities.LineOfBusiness.OceanMarine.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.Package, Entities.LineOfBusiness.Package.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.WorkersComp, Entities.LineOfBusiness.WorkersCompensation.Code);
		}

	}
}
