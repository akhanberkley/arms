using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class ColumnGroupType : BaseNamedTypeEntity
    {
        #region Enum
        /// <summary>
        /// Groupings of Coverages for all companies
        /// </summary>
        public enum Enum
        {
            Building,
            BuildersRisk,
            BusinessIncome,
            ClassOfOperations,
            Collision,
            ContractorsEquipmentLeased,
            ContractorsLiability,
            CommercialAuto,
            CommercialUmbrellaNCR,
            Comprehensive,
            ComputerEquipment,
            Crime,
            GeneralLiability,
            JewelersBlock,
            MotorTruckCargo,
            NonOwnerAuto,
            OptionalReportsCoverages,
            PersonalProperty,
            ProductsOperations,
            Signs,
            SpecifiedCausesOfLoss,
            ToolFloater,
            WarehouseLiability,
            Exposure,
            PhysicalDamage,
            LegalLiability,
            BoatDealer,
            DriveawayCollision,
            AccountsReceivable,
            ValuablePapers,
            FineArtsFloater,
            BaileesCoverage,
            TransitCoverage,
            Garagekeeper,
            FranchiseRestaurantProgram,
            InstallationFloater,
            MobileEquipment,
            EDPEquipment,
            ProfessionalLiability,
            SexualAbuse,
        }
        #endregion

        public ColumnGroupType()
        {
        }

        /// <summary>
        /// Each group has a name in the BLC DB 
        /// </summary>
        protected override void BuildMap(ref Dictionary<int, string> oDictionary)
        {
            oDictionary.Add((int)Enum.Building, "BUILDING");
            oDictionary.Add((int)Enum.BuildersRisk, "BUILDRISK");
            oDictionary.Add((int)Enum.BusinessIncome, "BUSINCOME");
            oDictionary.Add((int)Enum.ClassOfOperations, "CLASSOP");
            oDictionary.Add((int)Enum.Collision, "COLLISION");
            oDictionary.Add((int)Enum.ContractorsEquipmentLeased, "CONEQUIP");
            oDictionary.Add((int)Enum.ContractorsLiability, "CONTLIA");
            oDictionary.Add((int)Enum.Comprehensive, "COMPRHEN");
            oDictionary.Add((int)Enum.Crime, "CRIME");
            oDictionary.Add((int)Enum.ComputerEquipment, "ELECTEQUIP");
            oDictionary.Add((int)Enum.GeneralLiability, "GENLIA");
            oDictionary.Add((int)Enum.JewelersBlock, "JEWELERS");
            oDictionary.Add((int)Enum.NonOwnerAuto, "NONOWNAUTO");
            oDictionary.Add((int)Enum.PersonalProperty, "PERPROP");
            oDictionary.Add((int)Enum.ProductsOperations, "PRODUCTOPS");
            oDictionary.Add((int)Enum.Signs, "SIGNS");
            oDictionary.Add((int)Enum.SpecifiedCausesOfLoss, "SPECIFIED");
            oDictionary.Add((int)Enum.ToolFloater, "TOOLFLOAT");
            oDictionary.Add((int)Enum.MotorTruckCargo, "TRUCKCARGO");
            oDictionary.Add((int)Enum.CommercialUmbrellaNCR, "UMBRELLA");
            oDictionary.Add((int)Enum.CommercialAuto, "COMMAUTO");
            oDictionary.Add((int)Enum.WarehouseLiability, "WAREHOUSE");
            oDictionary.Add((int)Enum.Exposure, "EXPOSURE");
            oDictionary.Add((int)Enum.PhysicalDamage, "PHYDAMAGE");
            oDictionary.Add((int)Enum.LegalLiability, "LEGALLIAB");
            oDictionary.Add((int)Enum.BoatDealer, "BOATDEAL");
            oDictionary.Add((int)Enum.DriveawayCollision, "DRIVEAWAY");
            oDictionary.Add((int)Enum.AccountsReceivable, "ACCTREC");
            oDictionary.Add((int)Enum.ValuablePapers, "VALUEPAP");
            oDictionary.Add((int)Enum.FineArtsFloater, "FINEARTS");
            oDictionary.Add((int)Enum.BaileesCoverage, "BAILEES");
            oDictionary.Add((int)Enum.TransitCoverage, "TRANSIT");
            oDictionary.Add((int)Enum.Garagekeeper, "GARAGE");
            oDictionary.Add((int)Enum.FranchiseRestaurantProgram, "FRP");
            oDictionary.Add((int)Enum.InstallationFloater, "INSTALL");
            oDictionary.Add((int)Enum.MobileEquipment, "MOBILE");
            oDictionary.Add((int)Enum.EDPEquipment, "EDPEQUIP");
            oDictionary.Add((int)Enum.ProfessionalLiability, "PROFESSION");
            oDictionary.Add((int)Enum.SexualAbuse, "SEXABUSE");
        }
    }

}
