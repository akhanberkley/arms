using System.Collections.Generic;

namespace Berkley.BLC.Import
{
    public class BSUMLineOfBusiness : LineOfBusiness
    {
        public BSUMLineOfBusiness()
        {
        }

        protected override void DerivedBuildMap(ref Dictionary<int, string> oDictionary)
        {
            // Pipe delimit multiple symbols for a LOB
            oDictionary.Add((int)LineOfBusinessType.CommercialAuto, GetCompanyPolicySymbols(Entities.Company.BerkleySpecialtyUnderwritingManagers, Entities.LineOfBusiness.CommercialAuto));
            oDictionary.Add((int)LineOfBusinessType.CommercialProperty, GetCompanyPolicySymbols(Entities.Company.BerkleySpecialtyUnderwritingManagers, Entities.LineOfBusiness.CommercialProperty));
            //oDictionary.Add((int)LineOfBusinessType.CommercialUmbrella, GetCompanyPolicySymbols(Entities.Company.BerkleySpecialtyUnderwritingManagers, Entities.LineOfBusiness.CommercialUmbrella));
            oDictionary.Add((int)LineOfBusinessType.GeneralLiability, GetCompanyPolicySymbols(Entities.Company.BerkleySpecialtyUnderwritingManagers, Entities.LineOfBusiness.GeneralLiability));
            oDictionary.Add((int)LineOfBusinessType.InlandMarine, GetCompanyPolicySymbols(Entities.Company.BerkleySpecialtyUnderwritingManagers, Entities.LineOfBusiness.InlandMarine));
            oDictionary.Add((int)LineOfBusinessType.WorkersComp, GetCompanyPolicySymbols(Entities.Company.BerkleySpecialtyUnderwritingManagers, Entities.LineOfBusiness.WorkersCompensation));
            //oDictionary.Add((int)LineOfBusinessType.CommercialCrime, GetCompanyPolicySymbols(Entities.Company.BerkleySpecialtyUnderwritingManagers, Entities.LineOfBusiness.CommercialCrime));
            oDictionary.Add((int)LineOfBusinessType.Package, GetCompanyPolicySymbols(Entities.Company.BerkleySpecialtyUnderwritingManagers, Entities.LineOfBusiness.Package));
            oDictionary.Add((int)LineOfBusinessType.Garage, GetCompanyPolicySymbols(Entities.Company.BerkleySpecialtyUnderwritingManagers, Entities.LineOfBusiness.Garage));

            moBlcLobDictionary.Add(LineOfBusinessType.CommercialAuto, Entities.LineOfBusiness.CommercialAuto.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.CommercialCrime, Entities.LineOfBusiness.CommercialCrime.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.CommercialProperty, Entities.LineOfBusiness.CommercialProperty.Code);
            //moBlcLobDictionary.Add(LineOfBusinessType.CommercialUmbrella, Entities.LineOfBusiness.CommercialUmbrella.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.GeneralLiability, Entities.LineOfBusiness.GeneralLiability.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.InlandMarine, Entities.LineOfBusiness.InlandMarine.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.WorkersComp, Entities.LineOfBusiness.WorkersCompensation.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.Package, Entities.LineOfBusiness.Package.Code);
            moBlcLobDictionary.Add(LineOfBusinessType.Garage, Entities.LineOfBusiness.Garage.Code);
        }
    }
}
