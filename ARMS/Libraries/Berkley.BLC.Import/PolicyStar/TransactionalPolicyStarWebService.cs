using Berkley.BLC.Import.TransactionalPolicyStar;
using Berkley.BLC.Entities;
using Berkley.BLC.Import;
using Berkley.BLC.Core;
using System;
using System.Text;
using QCI.ExceptionManagement;

namespace Berkley.Import.PolicyStar
{
    public class TransactionalPolicyStarWebService
    {
        PremiumAuditServiceService service;

        public TransactionalPolicyStarWebService(string endPointUrl)
        {
            service = new PremiumAuditServiceService();
            service.Timeout = 10000000;

            // set the URL based on the environment and company
            service.Url = endPointUrl;
        }

        /// <summary>
        /// Returns a PremiumAuditLossControlDO object of policies for the insured specified by a Policy Number.
        /// </summary>
        /// <param name="policyNumber">The policy number.</param>
        /// <param name="accountNumber">The account number for the company.</param>
        /// <returns>A PremiumAuditLossControlDO object.</returns>
        public PremiumAuditLossControlDO GetPolicies(string policyNumber)
        {
            return service.getPolicyByPolicyNbr(policyNumber, string.Empty, string.Empty);
        }

        /// <summary>
        /// Returns a PremiumAuditLossControlDO object of quotes for the insured specified by a Quote Number.
        /// </summary>
        /// <param name="quoteNumber">The quote number.</param>
        /// <param name="accountNumber">The account number for the company.</param>
        /// <returns>A PremiumAuditLossControlDO object.</returns>
        public PremiumAuditLossControlDO GetQuotes(string quoteNumber, string accountNumber)
        {
            try
            {
                return service.getQuoteByNbr(quoteNumber, string.Empty, string.Empty);
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(new PolicySTARWebServiceException(string.Format("An exception occurred connecting with the PolicySTAR service: QuoteNumber={0}, AccountNumber={1}.", quoteNumber, accountNumber), ex));
                throw new BlcNoInsuredException("");
            }
        }

        /// <summary>
        /// Returns a collection of policies with their respective status for a given client.
        /// </summary>
        /// <param name="portfolioID">The portfolio or client id.</param>
        /// <param name="accountNumber">The account number for the company.</param>
        /// <returns>A boolean.</returns>
        public PolicyDataDO[] GetAccountStatusByPolicy(string portfolioID, string accountNumber)
        {
            try
            {
                return service.getPolicyStatus(portfolioID, accountNumber);
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(new PolicySTARWebServiceException(string.Format("An exception occurred connecting with the PolicySTAR service: PortfolioID={0}, AccountNumber={1}.", portfolioID, accountNumber), ex));
                throw new BlcNoInsuredException("");
            }
        }
    }
}
