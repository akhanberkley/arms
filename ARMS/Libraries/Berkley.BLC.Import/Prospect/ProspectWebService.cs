//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Xml;
//using System.Data;
//using System.IO;

//using Berkley.BLC.Core;
//using Berkley.BLC.Entities;

//namespace Berkley.BLC.Import
//{
//    public class ProspectWebService
//    {
//        private SubmissionProxy _submissionService;
//        private ClientProxy _clientService;
        
//        public ProspectWebService()
//        {
//            _submissionService = new SubmissionProxy();
//            _clientService = new ClientProxy();

//            _submissionService.Timeout = 100000;
//            _clientService.Timeout = 100000;

//            switch (CoreSettings.ConfigurationType)
//            {
//                case ConfigurationType.Production:
//                    _submissionService.Url = "http://bcs-services:3406/Submission.asmx";
//                    _clientService.Url = "http://bcs-services:3406/Client.asmx";
//                    break;
//                case ConfigurationType.Test:
//                    _submissionService.Url = "http://btsdetstwfa01:3406/Submission.asmx";
//                    _clientService.Url = "http://btsdetstwfa01:3406/Client.asmx";
//                    break;
//                case ConfigurationType.Integration:
//                    _submissionService.Url = "http://btsdeintwfa01:3406/Submission.asmx";
//                    _clientService.Url = "http://btsdeintwfa01:3406/Client.asmx";
//                    break;
//                case ConfigurationType.Development:
//                    _submissionService.Url = "http://btsdedevwfa01:3406/Submission.asmx";
//                    _clientService.Url = "http://btsdedevwfa01:3406/Client.asmx";
//                    break;
//                default:

//                    throw new NotSupportedException("Expecting an environment to be set.");
//            }
//        }

//        /// <summary>
//        /// Returns a flag indicating if a submission exist or not in Clearance given a policy number.
//        /// </summary>
//        /// <param name="policyNumber">The policy number to query.</param>
//        /// <param name="company">The Berkley company.</param>
//        public bool DoesSubmissionExist(string policyNumber, Company company)
//        {
//            // By default, always return true if this is a non-production environment due to 
//            // Clearance non-production environments not containing all the necessary policies/submissions
//            //if (CoreSettings.ConfigurationType != ConfigurationType.Production)
//            //{
//            //    return true;
//            //}
            
//            bool result = false;
//            string xml;

//            try
//            {
//                xml = _submissionService.GetSubmissionsByPolicyNumberAndCompanyNumber(policyNumber, company.Number);

//                // Validation
//                if (xml == null || xml.Length == 0)
//                {
//                    result = false;
//                }
//            }
//            catch (Exception ex)
//            {
//                throw new Exception(string.Format("Prospect web service not available: {0}", ex.Message));
//            }

//            // Check if any submissions exist in the xml
//            DataSet ds = Deserialize(xml);
//            if (ds.Tables["submissions"] != null && ds.Tables["submissions"].Rows.Count > 0)
//            {
//                result = true;
//            }

//            return result;
//        }

//        /// <summary>
//        /// Returns a dataset of submission(s) from Clearance given the submission number.
//        /// </summary>
//        /// <param name="submissionNumber">The submission number to query.</param>
//        /// <param name="company">The Berkley company.</param>
//        public DataSet GetSubmissionFromSubmissionNumber(string submissionNumber, Company company)
//        {
//            string xml;
//            try
//            {
//                xml = _submissionService.GetSubmissionBySubmissionNumberAndCompanyNumber(submissionNumber, company.Number, String.Empty);
//            }
//            catch (Exception ex)
//            {
//                throw new BlcProspectWebServiceException(string.Format("Prospect web service not available: {0}", ex.Message));
//            }

//            //validation
//            if (xml == null || xml.Length == 0)
//            {
//                throw new BlcProspectWebServiceException(string.Format("Submission number {0} not found in Clearance.", submissionNumber));
//            }

//            return Deserialize(xml);
//        }

//        /// <summary>
//        /// Returns a dataset of submission(s) from Clearance given the client id.
//        /// </summary>
//        /// <param name="clientID">The client id to query.</param>
//        /// <param name="company">The Berkley company.</param>
//        public DataSet CallSubmissionFromClientId(string clientID, Company company)
//        {
//            string xml;
//            try
//            {
//                xml = _submissionService.GetSubmissionsByClientId(company.Number, clientID);
//            }
//            catch (Exception ex)
//            {
//                throw new BlcProspectWebServiceException(string.Format("Prospect web service not available: {0}", ex.Message));
//            }

//            //validation
//            if (xml == null || xml.Length == 0 || !xml.Contains("clientcore_client_id"))
//            {
//                throw new BlcProspectWebServiceException(string.Format("Client ID {0} not found in Clearance or no submissions exist for this client.", clientID));
//            }

//            return Deserialize(xml);
//        }

//        /// <summary>
//        /// Returns a dataset of client information from Client Core given the client id.
//        /// </summary>
//        /// <param name="clientID">The client id to query.</param>
//        /// <param name="company">The Berkley company.</param>
//        public DataSet CallClientWebService(string clientID, Company company)
//        {
//            string xml;
//            try
//            {
//                xml = _clientService.GetClientById(company.Number, clientID);
//            }
//            catch (Exception ex)
//            {
//                throw new BlcProspectWebServiceException(string.Format("Prospect client web service not available: {0}", ex.Message));
//            }

//            //validation
//            if (xml == null || xml.Length == 0)
//            {
//                throw new BlcProspectWebServiceException(string.Format("Client ID {0} not found in Clearance.", clientID));
//            }

//            return Deserialize(xml);
//        }

//        /// <summary>
//        /// Returns the status of the submission.
//        /// </summary>
//        /// <param name="submissionNumber">The submission number.</param>
//        /// <param name="company">The Berkley company.</param>
//        public string GetSubmissionStatus(string submissionNumber, Company company)
//        {
//            string result = string.Empty;

//            try
//            {
//                DataSet ds = GetSubmissionFromSubmissionNumber(submissionNumber, company);
//                foreach (DataTable table in ds.Tables)
//                {
//                    if (table.Rows.Count > 0)
//                    {
//                        if (table.TableName == "submission_status")
//                        {
//                            result = (table.Rows[0]["status_code"] != null) ? table.Rows[0]["status_code"].ToString() : string.Empty;
//                            break;
//                        }
//                    }
//                }
//            }
//            catch (BlcProspectWebServiceException)
//            {
//                //swallow this exception and return the empty string as a default
//            }

//            return result;
//        }


//        private DataSet Deserialize(string xml)
//        {
//            StringReader stringReader = new StringReader(xml);
//            XmlTextReader xmlReader = new XmlTextReader(stringReader);

//            DataSet ds = new DataSet();
//            ds.ReadXml(xmlReader);

//            return ds;
//        }
//    }
//}
