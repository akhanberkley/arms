using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;

using Berkley.BLC.Business;
using Berkley.BLC.Entities;
using Berkley.BLC.Import;
using Berkley.BLC.Import.Prospect;

namespace Berkley.BLC.Import
{
    public class ProspectBuilder
    {
        private ProspectWebService _service;

        public ProspectBuilder()
        {
            _service = new ProspectWebService();
        }

        /// <summary>
        /// Returns a blcInsured based on the submission number passed.
        /// </summary>
        /// <param name="submissionNumber">The submission number.</param>
        /// <param name="company">The Berkley company.</param>
        public BlcInsured BuildFromSubmissionNumber(string submissionNumber, Berkley.BLC.Entities.Company company)
        {
            DataSet dataSet = _service.GetSubmissionFromSubmissionNumber(submissionNumber, company);

            foreach (DataTable table in dataSet.Tables)
            {
                if (table.Rows.Count > 0)
                {
                    switch (table.TableName)
                    {
                        case "submission":
                            string clientID = (string)table.Rows[0]["clientcore_client_id"];
                            if (clientID == string.Empty || clientID.Length == 0)
                            {
                                throw new Exception(string.Format("Client ID not found from Submission ID: {0}", submissionNumber));
                            }
                            return Build(true, clientID, submissionNumber, company);

                        default:
                            break;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Returns a blcInsured based on the client ID passed.
        /// </summary>
        /// <param name="submissionNumber">The submission number.</param>
        /// <param name="clientID">The client ID.</param>
        /// <param name="company">The Berkley company.</param>
        public BlcInsured BuildFromClientID(string submissionNumber, string clientID, Berkley.BLC.Entities.Company company)
        {
            DataSet dataSet = _service.CallSubmissionFromClientId(clientID, company);

            foreach (DataTable table in dataSet.Tables)
            {
                if (table.Rows.Count > 0 && table.TableName.ToLower() == "submission")
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        string number = (string)dr["submission_number"];
                        if (number != null && number.Trim().Length > 0 && number == submissionNumber)
                        {
                            return Build(true, clientID, submissionNumber, company);
                        }
                    }

                    throw new BlcProspectWebServiceException(string.Format("Submission Number {0} is not associated with Client ID {1} in Clearance.", submissionNumber, clientID));
                }
            }
            return null;
        }

        /// <summary>
        /// Returns a blcInsured object populated with the insured, agency, and policy information.
        /// </summary>
        /// <param name="buildFromClientID">Flag determining if propsect is built from client id.</param>
        /// <param name="clientID">The client ID.</param>
        /// <param name="submissionNumber">The submission number.</param>
        /// <param name="company">The Berkley company.</param>
        public BlcInsured Build(bool buildFromClientID, string clientID, string submissionNumber, Berkley.BLC.Entities.Company company)
        {
            BlcInsured blcInsured = new BlcInsured();
            Insured insured = new Insured(Guid.NewGuid());
            Agency agency = new Agency(Guid.NewGuid());

            blcInsured.InsuredEntity = insured;
            blcInsured.AgencyEntity = agency;
            BuildProspectClient(clientID, company, ref blcInsured);
            BuildProspectAgency(clientID, submissionNumber, company, ref blcInsured);

            //Building from submission number or client id?
            if (buildFromClientID)
            {
                BuildProspectPolicies(clientID, company, ref blcInsured);
            }
            else
            {
                BuildProspectPolicy(submissionNumber, company, ref blcInsured);
            }

            return blcInsured;
        }

        /// <summary>
        /// Returns a blcInsured object populated with a policy derived from the submission number.
        /// </summary>
        /// <param name="submissionNumber">The submission number.</param>
        /// <param name="company">The Berkley company.</param>
        /// <param name="blcInsured">The BLC insured object.</param>
        public BlcInsured BuildProspectPolicy(string submissionNumber, Berkley.BLC.Entities.Company company, ref BlcInsured blcInsured)
        {
            DataSet dataSet = _service.GetSubmissionFromSubmissionNumber(submissionNumber, company);

            foreach (DataTable table in dataSet.Tables)
            {
                if (table.Rows.Count > 0 && table.TableName.ToLower() == "submission")
                {
                    BuildPolicyFromSubmission(table.Rows[0], company, ref blcInsured);
                }
            }

            return blcInsured;
        }

        /// <summary>
        /// Returns a blcInsured object populated with a list of policies based on the client id.
        /// </summary>
        /// <param name="clientID">The client id.</param>
        /// <param name="company">The Berkley company.</param>
        /// <param name="blcInsured">The BLC insured object.</param>
        public BlcInsured BuildProspectPolicies(string clientID, Berkley.BLC.Entities.Company company, ref BlcInsured blcInsured)
        {
            DataSet dataSet = _service.CallSubmissionFromClientId(clientID, company);

            foreach (DataTable table in dataSet.Tables)
            {
                if (table.Rows.Count > 0 && table.TableName.ToLower() == "submission")
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        BuildPolicyFromSubmission(dr, company, ref blcInsured);
                    }
                }
            }

            return blcInsured;
        }

        /// <summary>
        /// Returns a blcInsured object populated with the agency information.
        /// </summary>
        /// <param name="clientID">The client id.</param>
        /// <param name="submissionNumber">The submission number.</param>
        /// <param name="company">The Berkley company.</param>
        /// <param name="blcInsured">The BLC insured object.</param>
        public BlcInsured BuildProspectAgency(string clientID, string submissionNumber, Berkley.BLC.Entities.Company company, ref BlcInsured blcInsured)
        {
            DataSet dataSet = _service.GetSubmissionFromSubmissionNumber(submissionNumber, company);

            foreach (DataTable table in dataSet.Tables)
            {
                if (table.Rows.Count > 0)
                {
                    switch (table.TableName)
                    {
                        case "submission":
                            BuildSubmission(table.Rows[0], company, ref blcInsured);
                            blcInsured.InsuredEntity.ClientID = clientID;
                            break;
                        case "agency":
                            BuildAgency(table.Rows[0], ref blcInsured, company);
                            break;
                        case "":
                            break;
                    }
                }
            }

            return blcInsured;
        }

        /// <summary>
        /// Returns a blcInsured object populated with the client information.
        /// </summary>
        /// <param name="clientID">The client id.</param>
        /// <param name="company">The Berkley company.</param>
        /// <param name="blcInsured">The BLC insured object.</param>
        public BlcInsured BuildProspectClient(string clientID, Berkley.BLC.Entities.Company company, ref BlcInsured blcInsured)
        {
            DataSet dataSet = _service.CallClientWebService(clientID, company);

            blcInsured.InsuredEntity.ClientID = clientID;
            foreach (DataTable table in dataSet.Tables)
            {
                if (table.Rows.Count > 0)
                {
                    switch (table.TableName)
                    {
                        case "address-do":
                            BuildInsuredAddress(table.Rows[0], ref blcInsured);
                            BuildLocations(table.Rows, ref blcInsured);
                            break;
                        case "names":
                            BuildClient(table.Rows, ref blcInsured);
                            break;
                        case "ClearanceClient":
                            BuildClearanceClient(table.Rows[0], ref blcInsured);
                            break;
                        case "":
                            break;
                    }
                }
            }
            return blcInsured;
        }

        /// <summary>
        /// Returns a blcInsured object populated with some additional submission details.
        /// </summary>
        /// <param name="dr">The datarow to extract the submission info.</param>
        /// <param name="company">The Berkley company.</param>
        /// <param name="blcInsured">The BLC insured object.</param>
        public BlcInsured BuildSubmission(DataRow dr, Company company, ref BlcInsured blcInsured)
        {
            blcInsured.AgencyEntity.Name = (string)dr["agency_name"];

            //try to attach the underwriter
            string strName = (string)dr["underwriter_name"];
            if (strName != null && strName.Trim().Length > 0)
            {
                Underwriter underwriter = Underwriter.GetOne("CompanyID = ? && Username = ?", company.ID, strName);
                if (underwriter != null)
                {
                    blcInsured.InsuredEntity.Underwriter = underwriter.Code;
                }
            }

            return blcInsured;
        }

        /// <summary>
        /// Returns a blcInsured object and builds a policy from the submission information.
        /// </summary>
        /// <param name="dr">The datarow to extract the submission info.</param>
        /// <param name="company">The Berkley company.</param>
        /// <param name="blcInsured">The BLC insured object.</param>
        public BlcInsured BuildPolicyFromSubmission(DataRow dr, Company company, ref BlcInsured blcInsured)
        {
            Policy policy = new Policy(Guid.NewGuid());

            try { policy.Number = (dr["policy_number"] != DBNull.Value) ? (String)dr["policy_number"] : String.Empty; }
            catch (Exception)
            {
                policy.Number = "";
            }

            try { policy.Premium = (Convert.ToDecimal(dr["estimated_written_premium"]) > 0 ? Convert.ToDecimal(dr["estimated_written_premium"]) : Decimal.MinValue); }
            catch
            {
                policy.Premium = Decimal.MinValue;
            }

            try { policy.EffectiveDate = Convert.ToDateTime(dr["effective_date"]); }
            catch
            {
                policy.EffectiveDate = DateTime.MinValue;
            }

            //BIZ RULE
            //Only pull policies that have an effective date within the past year
            if (policy.EffectiveDate < DateTime.Today.AddDays(-366))
            {
                return blcInsured;
            }

            try { policy.ExpireDate = Convert.ToDateTime(dr["expiration_date"]); }
            catch
            {
                policy.ExpireDate = DateTime.MinValue;
            }

            try { policy.Symbol = (dr["code_types"] != DBNull.Value) ? (String)dr["code_types"] : String.Empty; }
            catch 
            {
                policy.Symbol = string.Empty;
            }

            //if it is not found under code_types, then try under the submission_code_types
            if (string.IsNullOrEmpty(policy.Symbol))
            {
                try
                {
                    DataSet dataSet = _service.GetSubmissionFromSubmissionNumber(dr["submission_number"].ToString(), company);
                    foreach (DataTable table in dataSet.Tables)
                    {
                        if (table.Rows.Count > 0 && table.TableName.ToLower() == "submission_code_type")
                        {
                            foreach (DataRow drCodeType in table.Rows)
                            {
                                if (drCodeType["code_name"].ToString().ToLower() == "policy symbol" ||
                                    drCodeType["code_name"].ToString().ToLower() == "policy type")
                                {
                                    policy.Symbol = drCodeType["code_value"].ToString();
                                    break;
                                }
                            }
                        }
                    }
                }
                catch
                {
                    policy.Symbol = string.Empty;
                }
            }

            if (policy.Symbol != null && policy.Symbol.Trim().Length > 0)
            {
                policy.Symbol = policy.Symbol.Replace("CONVERSION", "ZZZ");

                ImportFactory factory = new ImportFactory(company);
                policy.LineOfBusinessCode = factory.DetermineLOB(policy.Symbol);

                if (policy.LineOfBusiness == null)
                {
                    //no LOB, don't add the policy
                    return blcInsured;
                }
                else
                {
                    blcInsured.PolicyList.Add(policy);
                }
            }

            return blcInsured;
        }


        public BlcInsured BuildAgency(DataRow dr, ref BlcInsured blcInsured, Company company)
        {
            try
            {
                string agencyNumber = (string)dr["agency_number"];
                ImportFactory oFactory = new ImportFactory(company);
                Agency agency = oFactory.ImportAgency(agencyNumber, string.Empty);

                blcInsured.AgencyEntity = agency;
            }
            catch (Exception)
            {
                Agency agency = Agency.GetOne("Number = ? && CompanyID = ?", dr["agency_number"], company.ID);
                if (agency == null)
                {
                    throw new BlcImportException("No agency was found for this submission.");
                }
                else
                {
                    blcInsured.AgencyEntity = agency;
                }
            }

            return blcInsured;
        }

        public Agency BuildAgency(DataSet ds)
        {
            DataRow dr = ds.Tables[0].Rows[0];
            Agency agency = new Agency(Guid.NewGuid());

            try { agency.Number = (dr["agency_number"] != DBNull.Value) ? (string)dr["agency_number"] : string.Empty; }
            catch
            {
                agency.Number = string.Empty;
            }

            try { agency.Name = (dr["agency_name"] != DBNull.Value) ? (string)dr["agency_name"] : string.Empty; }
            catch
            {
                agency.Name = string.Empty;
            }

            try { agency.City = (dr["city"] != DBNull.Value) ? (string)dr["city"] : string.Empty; }
            catch
            {
                agency.City = string.Empty;
            }

            try { agency.StateCode = (dr["state"] != DBNull.Value) ? (string)dr["state"] : string.Empty; }
            catch
            {
                agency.StateCode = string.Empty;
            }

            try { agency.PhoneNumber = (dr["phone"] != DBNull.Value) ? (string)dr["phone"] : string.Empty; }
            catch
            {
                agency.PhoneNumber = string.Empty;
            }

            try { agency.FaxNumber = (dr["fax"] != DBNull.Value) ? (string)dr["fax"] : string.Empty; }
            catch
            {
                agency.FaxNumber = string.Empty;
            }

            //remove the "0" from BCS
            agency.FaxNumber = (agency.FaxNumber == "0") ? string.Empty : agency.FaxNumber;

            return agency;
        }

        public BlcInsured BuildClient(DataRowCollection drs, ref BlcInsured blcInsured)
        {
            foreach (DataRow dr in drs)
            {
                try
                {
                    if ((string)dr["primary_name_flag"] == "Y" && dr["name_business"] != DBNull.Value)
                    {
                        blcInsured.InsuredEntity.Name = (string)dr["name_business"];
                        break;
                    }
                }
                catch (ArgumentException)
                {
                    blcInsured.InsuredEntity.Name = (string)dr["name_business"];
                    break;
                }
            }
            return blcInsured;
        }

        public BlcInsured BuildClearanceClient(DataRow dr, ref BlcInsured blcInsured)
        {
            try
            {
                string strCode = (string)dr["sic_code"];
                if (strCode != null && strCode.Length > 0 && strCode != "0")
                {
                    //pad the SICCode so that there is always 4 digits
                    blcInsured.InsuredEntity.SICCode = strCode.Trim().PadLeft(4, '0');
                }
            }
            catch (Exception)
            {
            }

            try
            {
                string strBusinessOps = (string)dr["business_description"];
                if (strBusinessOps != null && strBusinessOps.Length > 0)
                {
                    blcInsured.InsuredEntity.BusinessOperations = strBusinessOps;
                }
            }
            catch (Exception)
            {
            }
            return blcInsured;
        }


        public BlcInsured BuildInsuredAddress(DataRow dr, ref BlcInsured blcInsured)
        {
            try { blcInsured.InsuredEntity.StreetLine1 = (dr["house_nbr"] != DBNull.Value) ? (string)dr["house_nbr"] : string.Empty; }
            catch (Exception)
            {
                blcInsured.InsuredEntity.StreetLine1 = string.Empty;
            }

            try { blcInsured.InsuredEntity.StreetLine1 += (dr["address1"] != DBNull.Value) ? " " + (string)dr["address1"] : string.Empty; }
            catch
            {
                blcInsured.InsuredEntity.StreetLine1 += string.Empty;
            }

            try { blcInsured.InsuredEntity.City = (dr["city"] != DBNull.Value) ? (string)dr["city"] : string.Empty; }
            catch
            {
                blcInsured.InsuredEntity.City = string.Empty;
            }

            try { blcInsured.InsuredEntity.StateCode = ReadState((dr["state_prov_cd"] != DBNull.Value) ? (string)dr["state_prov_cd"] : string.Empty); }
            catch
            {
                blcInsured.InsuredEntity.StateCode = string.Empty;
            }

            try { blcInsured.InsuredEntity.ZipCode = BaseImportBuilder.TrimZipCode((string)dr["postal_code"]); }
            catch
            {
                blcInsured.InsuredEntity.ZipCode = string.Empty;
            }

            return blcInsured;
        }

        public BlcInsured BuildLocations(DataRowCollection rows, ref BlcInsured blcInsured)
        {
            System.Collections.ArrayList list = new System.Collections.ArrayList();

            for (int i = 0; i < rows.Count; i++)
            {
                DataRow dr = rows[i];
                Location location = new Location(Guid.NewGuid());
                location.Number = i + 1;
                location.IsActive = true;

                try { location.StreetLine1 = (dr["house_nbr"] != DBNull.Value) ? (string)dr["house_nbr"] : string.Empty; }
                catch (Exception)
                {
                    location.StreetLine1 = string.Empty;
                }

                try { location.StreetLine1 += (dr["address1"] != DBNull.Value) ? " " + (string)dr["address1"] : string.Empty; }
                catch
                {
                    location.StreetLine1 += string.Empty;
                }

                try { location.City = (dr["city"] != DBNull.Value) ? (string)dr["city"] : string.Empty; }
                catch
                {
                    location.City = string.Empty;
                }

                try { location.StateCode = ReadState((dr["state_prov_cd"] != DBNull.Value) ? (string)dr["state_prov_cd"] : string.Empty); }
                catch
                {
                    location.StateCode = string.Empty;
                }

                try { location.ZipCode = BaseImportBuilder.TrimZipCode((string)dr["postal_code"]); }
                catch
                {
                    location.ZipCode = string.Empty;
                }

                // create key based on address info
                string locationKey = string.Concat(location.StreetLine1, location.City, location.StateCode, location.ZipCode);

                if (!list.Contains(locationKey) && locationKey != string.Empty)
                {
                    blcInsured.PolicyLocationList.Add(location);
                    list.Add(locationKey);
                }
            }

            return blcInsured;
        }

        private string ReadString(object obj)
        {
            return (obj != null) ? obj.ToString().Trim() : string.Empty;
        }

        private string ReadState(string strState)
        {
            strState = ReadString(strState);

            // Only allow valid state codes from the PDR
            if (strState != string.Empty && State.GetOne("Code = ?", strState) != null)
            {
                return strState;
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
