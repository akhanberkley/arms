using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
    public class APSWebServiceException : Exception
    {
        public APSWebServiceException(string strText, Exception innerException)
            : base(strText, innerException)
        {

        }
    }
}

