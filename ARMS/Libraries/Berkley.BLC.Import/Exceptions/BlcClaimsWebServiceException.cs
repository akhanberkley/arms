using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class BlcClaimsWebServiceException : Exception
	{
		public BlcClaimsWebServiceException(string strMessage)
			: base(strMessage)
		{
		}
	}
}
