using System;
using System.Collections.Generic;
using System.Text;

namespace Berkley.BLC.Import
{
	public class BlcNoInsuredException : Exception
	{
		public BlcNoInsuredException(string strMessage)
			: base(strMessage)
		{
		}
	}
}
