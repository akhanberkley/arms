﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.ARMS.Data.Tests
{
    [TestClass]
    public class Company
    {
        [TestMethod]
        public void TestSqlDatabase()
        {
            using (var db = new SqlDb())
            {
                var company = db.Companies.FirstOrDefault();
                Assert.IsTrue(company != null);

                Assert.IsFalse(db.EntityHasChanges(company));
                company.CompanyName = "12345";
                Assert.IsTrue(db.EntityHasChanges(company));
            }
        }
    }
}
