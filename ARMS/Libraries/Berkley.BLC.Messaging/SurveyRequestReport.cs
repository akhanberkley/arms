using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// Provides the list of survey requests for the response.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "SurveyRequestReport")]
    public class SurveyRequestReport
    {
        #region fields
        /// <summary>
        /// The list of survey request numbers.
        /// </summary>
        private List<Int32> surveyRequestNumbers = new List<Int32>();
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SurveyRequestReport()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The list of survey request numbers.
        /// </summary>
        [XmlArrayItem(ElementName = "SurveyRequestNumber", Type = typeof(Int32))]
        [XmlArray("SurveyRequestList")]
        public List<Int32> SurveyRequestNumbers
        {
            get
            {
                return (this.surveyRequestNumbers == null) ? new List<Int32>() : this.surveyRequestNumbers;
            }
            set
            {
                this.surveyRequestNumbers = value;
            }
        }
        #endregion
    }
}