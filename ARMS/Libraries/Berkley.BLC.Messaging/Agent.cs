using System;
using System.Collections;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// Provides the agent contact information for the agency.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "Agent")]
    public class Agent
    {
        #region fields
        /// <summary>
        /// The agent name.
        /// </summary>
        private string name;
        /// <summary>
        /// The agent's contact information.
        /// </summary>
        private Communication communication;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Agent()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The agent name.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
        /// <summary>
        /// The agent's contact information.
        /// </summary>
        [XmlElement]
        public Communication Communication
        {
            get
            {
                return this.communication;
            }
            set
            {
                this.communication = value;
            }
        }
        #endregion
    }
}