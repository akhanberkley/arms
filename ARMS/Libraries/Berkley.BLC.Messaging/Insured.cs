using System;
using System.Collections;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// Provides the insured information of the survey request.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "Insured")]
    public class Insured
    {
        #region fields
        /// <summary>
        /// The client id.
        /// </summary>
        private string clientID;
        /// <summary>
        /// The insured name.
        /// </summary>
        private string name;
        /// <summary>
        /// The alternate insured name.
        /// </summary>
        private string alternateName;
        /// <summary>
        /// The insured address.
        /// </summary>
        private Address addresss;
        /// <summary>
        /// The business operations.
        /// </summary>
        private string businessOperations;
        /// <summary>
        /// The SIC Code.
        /// </summary>
        private string sicCode;
        /// <summary>
        /// The underwriter.
        /// </summary>
        private Underwriter underwriter;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Insured()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The client id.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string ClientID
        {
            get
            {
                return this.clientID;
            }
            set
            {
                this.clientID = value;
            }
        }
        /// <summary>
        /// The insured name.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
        /// <summary>
        /// The alternate insured name.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string AlternateName
        {
            get
            {
                return this.alternateName;
            }
            set
            {
                this.alternateName = value;
            }
        }
        /// <summary>
        /// The insured address.
        /// </summary>
        [XmlElement]
        public Address Address
        {
            get
            {
                return this.addresss;
            }
            set
            {
                this.addresss = value;
            }
        }
        /// <summary>
        /// The business operations.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string BusinessOperations
        {
            get
            {
                return this.businessOperations;
            }
            set
            {
                this.businessOperations = value;
            }
        }
        /// <summary>
        /// The SIC code.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string SIC
        {
            get
            {
                return this.sicCode;
            }
            set
            {
                this.sicCode = value;
            }
        }
        /// <summary>
        /// The underwriter.
        /// </summary>
        [XmlElement]
        public Underwriter Underwriter
        {
            get
            {
                return this.underwriter;
            }
            set
            {
                this.underwriter = value;
            }
        }
        #endregion
    }
}