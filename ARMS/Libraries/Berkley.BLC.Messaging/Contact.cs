using System;
using System.Collections;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// Provides the contact information for the insured.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "Contact")]
    public class Contact
    {
        #region fields
        /// <summary>
        /// The contact's name.
        /// </summary>
        private string name;
        /// <summary>
        /// The contact's title.
        /// </summary>
        private string title;
        /// <summary>
        /// The contact's contact information.
        /// </summary>
        private Communication communication;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Contact()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The contact's name.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
        /// <summary>
        /// The contact's title.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Title
        {
            get
            {
                return this.title;
            }
            set
            {
                this.title = value;
            }
        }
        /// <summary>
        /// The contact's contact information.
        /// </summary>
        [XmlElement]
        public Communication Communication
        {
            get
            {
                return this.communication;
            }
            set
            {
                this.communication = value;
            }
        }
        #endregion
    }
}