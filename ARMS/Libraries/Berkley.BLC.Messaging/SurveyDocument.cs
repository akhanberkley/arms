using System;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
	/// <summary>
	/// Describes a document, such as a PDF file or a Microsoft Excel
	/// spreadsheet, that is part of the survey request.
	/// </summary>
	[XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "SurveyDocument")]
	public class SurveyDocument
	{
		#region fields
        /// <summary>
        /// The filenet id of the document.
        /// </summary>
        private string fileNetId;
        /// <summary>
		/// The name of the document.
		/// </summary>
		private string name;
		/// <summary>
		/// The MIME type of the document.
		/// </summary>
		private string mimeType;
		/// <summary>
		/// The file extension of the document.
		/// </summary>
		private string extension;
		/// <summary>
		/// The actual content of the document.
		/// </summary>
		private byte[] content;
		#endregion

		#region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
        public SurveyDocument() { }
		#endregion

		#region properties
        /// <summary>
        /// The filenet id of the document.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string FileNetId
        {
            get
            {
                return this.fileNetId;
            }
            set
            {
                this.fileNetId = value;
            }
        }
        /// <summary>
		/// The name of the document.
		/// </summary>
		[XmlElement(DataType = "string")]
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
                this.name = value;
			}
		}
		/// <summary>
		/// The MIME type of the document.
		/// </summary>
		[XmlElement(DataType = "string")]
		public string MimeType
		{
			get
			{
				return this.mimeType;
			}
			set
			{
				this.mimeType = value;
			}
		}
		/// <summary>
		/// The file extension of the document.
		/// </summary>
		[XmlElement(DataType = "string")]
		public string Extension
		{
			get
			{
                return this.extension;
			}
			set
			{
                this.extension = value;
			}
		}
		/// <summary>
		/// The actual content of the document.
		/// </summary>
		[XmlElement(DataType = "base64Binary")]
		public byte[] Content
		{
			get
			{
				return this.content;
			}
			set
			{
				this.content = value;
			}
		}
		#endregion

		#region methods
		#endregion
	}
}
