using System;
using System.Collections;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// Provides the underwriter contact information.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "Underwriter")]
    public class Underwriter
    {
        #region fields
        /// <summary>
        /// The underwriter name.
        /// </summary>
        private string name;
        /// <summary>
        /// The underwriter's contact information.
        /// </summary>
        private Communication communication;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Underwriter()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The underwriter name.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
        /// <summary>
        /// The underwriter's contact information.
        /// </summary>
        [XmlElement]
        public Communication Communication
        {
            get
            {
                return this.communication;
            }
            set
            {
                this.communication = value;
            }
        }
        #endregion
    }
}