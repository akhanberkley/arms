using System;
using System.Collections;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
	/// <summary>
	/// Provides the address information for a place.
	/// </summary>
	[XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "Address")]
	public class Address
	{
		#region fields
		/// <summary>
		/// A street address.
		/// </summary>
		private string streetLine1;
		/// <summary>
		/// An additional street address.
		/// </summary>
		private string streetLine2;
		/// <summary>
		/// A city.
		/// </summary>
		private string city;
		/// <summary>
		/// A state abbreviation.
		/// </summary>
		private string stateAbbreviation;
		/// <summary>
		/// A zip code.
		/// </summary>
		private string zipCode;
		#endregion

		#region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		public Address()
		{
		}
		#endregion

		#region properties
		/// <summary>
		/// A street address.
		/// </summary>
		[XmlElement(DataType = "string")]
        public string StreetLine1
		{
			get
			{
                return this.streetLine1;
			}
			set
			{
                this.streetLine1 = value;
			}
		}
		/// <summary>
		/// An additional street address.
		/// </summary>
		[XmlElement(DataType = "string")]
        public string StreetLine2
		{
			get
			{
                return this.streetLine2;
			}
			set
			{
                this.streetLine2 = value;
			}
		}
		/// <summary>
		/// A city.
		/// </summary>
		[XmlElement(DataType = "string")]
		public string City
		{
			get
			{
				return this.city;
			}
			set
			{
				this.city = value;
			}
		}
		/// <summary>
		/// A region.
		/// </summary>
        [XmlElement(DataType = "string")]
		public string StateAbbreviation
		{
			get
			{
				return this.stateAbbreviation;
			}
			set
			{
                this.stateAbbreviation = value;
			}
		}
		/// <summary>
		/// A postal code.
		/// </summary>
		[XmlElement(DataType = "string")]
		public string ZipCode
		{
			get
			{
				return this.zipCode;
			}
			set
			{
                this.zipCode = value;
			}
		}
		#endregion
	}
}