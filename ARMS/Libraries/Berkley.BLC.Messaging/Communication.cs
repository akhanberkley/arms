using System;
using System.Collections;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// Provides the communication information.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "Communication")]
    public class Communication
    {
        #region fields
        /// <summary>
        /// A phone number.
        /// </summary>
        private string phoneNumber;
        /// <summary>
        /// An alternate phone number
        /// </summary>
        private string alternatePhoneNumber;
        /// <summary>
        /// A fax number.
        /// </summary>
        private string faxNumber;
        /// <summary>
        /// An email addresss
        /// </summary>
        private string emailAddress;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Communication()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// A street address.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string PhoneNumber
        {
            get
            {
                return this.phoneNumber;
            }
            set
            {
                this.phoneNumber = value;
            }
        }
        /// <summary>
        /// An alternate phone number
        /// </summary>
        [XmlElement(DataType = "string")]
        public string AlternatePhoneNumber
        {
            get
            {
                return this.alternatePhoneNumber;
            }
            set
            {
                this.alternatePhoneNumber = value;
            }
        }
        /// <summary>
        /// A fax number.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string FaxNumber
        {
            get
            {
                return this.faxNumber;
            }
            set
            {
                this.faxNumber = value;
            }
        }
        /// <summary>
        /// An email address.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string EmailAddress
        {
            get
            {
                return this.emailAddress;
            }
            set
            {
                this.emailAddress = value;
            }
        }
        #endregion
    }
}