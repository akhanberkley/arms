/*===========================================================================
  File:			Form.cs
  
  Summary:		Forms from a company's policy system.  At the time of
				authoring, this class only pertained to CWG.
			
  Classes:		Form
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (November, 2006)
  
=============================================================================
  Copyright (c) 2006 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.Xml.Serialization;

namespace Berkley.PATS.Messaging
{
	/// <summary>
	///  Forms from a company's policy system.  At the time of
	///  authoring, this class only pertained to CWG.
	/// </summary>
	[XmlType(Namespace = "http://wrberkley.com/schema/pats/v1", TypeName = "Form")]
	public class Form
	{
		#region fields
		/// <summary>
		/// The title of the form for an <see cref="InsurancePolicy"/>.
		/// </summary>
		private string title;
		/// <summary>
		/// The description of the form for an <see cref="InsurancePolicy"/>.
		/// </summary>
		private string description;
		#endregion

		#region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		public Form()
		{
		}
		#endregion

		#region properties
		/// <summary>
		/// The title of the form for an <see cref="InsurancePolicy"/>.
		/// </summary>
		[XmlElement(DataType = "string")]
		public string Title
		{
			get
			{
				return this.title;
			}
			set
			{
				this.title = value;
			}
		}
		/// <summary>
		/// The description of the form for an <see cref="InsurancePolicy"/>.
		/// </summary>
		[XmlElement(DataType = "string")]
		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}
		#endregion
	}
}