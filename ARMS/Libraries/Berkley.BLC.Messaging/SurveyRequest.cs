using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// Provides the details of the loss control survey request.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "SurveyRequest")]
    public class SurveyRequest
    {
        #region fields
        /// <summary>
        /// The survey id.
        /// </summary>
        private string surveyId;
        /// <summary>
        /// The survey number.
        /// </summary>
        private int surveyRequestNumber;
        /// <summary>
        /// The survey status.
        /// </summary>
        private string surveyStatus;
        /// <summary>
        /// The survey create date.
        /// </summary>
        private DateTime createDate;
        /// <summary>
        /// The survey due date.
        /// </summary>
        private DateTime dueDate;
        /// <summary>
        /// The date the survey was assigned.
        /// </summary>
        private DateTime dateAssigned;
        /// <summary>
        /// The date the survey was accepted.
        /// </summary>
        private DateTime dateAccepted;
        /// <summary>
        /// The name of the assigned user.
        /// </summary>
        private string assignedUser;
        /// <summary>
        /// The date the survey was last modified.
        /// </summary>
        private DateTime lastModifiedOn;
        /// <summary>
        /// The notes attached to the survey.
        /// </summary>
        private List<SurveyNote> surveyNotes = new List<SurveyNote>();
        /// <summary>
        /// Supporting documents for the survey.
        /// </summary>
        private List<SurveyDocument> surveyDocuments = new List<SurveyDocument>();
        /// <summary>
        /// The insured for the survey.
        /// </summary>
        private Insured insured;
        /// <summary>
        /// The agency.
        /// </summary>
        private Agency agency;
        /// <summary>
        /// The locations to survey.
        /// </summary>
        private List<Location> locations = new List<Location>();
        /// <summary>
        /// The claims against the insured.
        /// </summary>
        private List<Claim> claims = new List<Claim>();
        /// <summary>
        /// The type of survey.
        /// </summary>
        private string surveyType;
        /// <summary>
        /// The date of survey.
        /// </summary>
        private DateTime dateSurveyed;

        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SurveyRequest()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The survey id.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string SurveyId
        {
            get
            {
                return this.surveyId;
            }
            set
            {
                this.surveyId = value;
            }
        }
        /// <summary>
        /// The survey number.
        /// </summary>
        [XmlElement(DataType = "int")]
        public int SurveyRequestNumber
        {
            get
            {
                return this.surveyRequestNumber;
            }
            set
            {
                this.surveyRequestNumber = value;
            }
        }
        /// <summary>
        /// The survey number.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string SurveyStatus
        {
            get
            {
                return this.surveyStatus;
            }
            set
            {
                this.surveyStatus = value;
            }
        }
        /// <summary>
        /// The survey create date.
        /// </summary>
        [XmlElement(DataType = "date")]
        public DateTime DateCreated
        {
            get
            {
                return this.createDate;
            }
            set
            {
                this.createDate = value;
            }
        }
        /// <summary>
        /// The survey due date.
        /// </summary>
        [XmlElement(DataType = "date")]
        public DateTime DueDate
        {
            get
            {
                return this.dueDate;
            }
            set
            {
                this.dueDate = value;
            }
        }
        /// <summary>
        /// The date the survey was assigned.
        /// </summary>
        [XmlElement(DataType = "date")]
        public DateTime DateAssigned
        {
            get
            {
                return this.dateAssigned;
            }
            set
            {
                this.dateAssigned = value;
            }
        }
        /// <summary>
        /// The date the survey was accepted.
        /// </summary>
        [XmlElement(DataType = "date")]
        public DateTime DateAccepted
        {
            get
            {
                return this.dateAccepted;
            }
            set
            {
                this.dateAccepted = value;
            }
        }
        /// <summary>
        /// The name of the assigned user.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string AssignedUser
        {
            get
            {
                return this.assignedUser;
            }
            set
            {
                this.assignedUser = value;
            }
        }
        /// <summary>
        /// The date the survey was last modified.
        /// </summary>
        [XmlElement(DataType = "date")]
        public DateTime LastModifiedOn
        {
            get
            {
                return this.lastModifiedOn;
            }
            set
            {
                this.lastModifiedOn = value;
            }
        }
        /// <summary>
        /// The notes attached to the survey.
        /// </summary>
        [XmlArrayItem(ElementName = "SurveyNote", Type = typeof(SurveyNote))]
        [XmlArray("SurveyNoteList")]
        public List<SurveyNote> SurveyNotes
        {
            get
            {
                return (this.surveyNotes == null) ? new List<SurveyNote>() : this.surveyNotes;
            }
            set
            {
                this.surveyNotes = value;
            }
        }
        /// <summary>
        /// The supporting documents attached to the survey.
        /// </summary>
        [XmlArrayItem(ElementName = "SurveyDocument", Type = typeof(SurveyDocument))]
        [XmlArray("SurveyDocumentList")]
        public List<SurveyDocument> SurveyDocuments
        {
            get
            {
                return (this.surveyDocuments == null) ? new List<SurveyDocument>() : this.surveyDocuments;
            }
            set
            {
                this.surveyDocuments = value;
            }
        }
        ///// <summary>
        ///// The supporting internal documents attached to the survey.
        ///// </summary>
        //[XmlArrayItem(ElementName = "SurveyDocumentId", Type = typeof(SurveyDocumentId))]
        //[XmlArray("SurveyDocumentIdList")]
        //public List<SurveyDocumentId> SurveyDocumentIds
        //{
        //    get
        //    {
        //        return (this.surveyDocumentIds == null) ? new List<SurveyDocumentId>() : this.surveyDocumentIds;
        //    }
        //    set
        //    {
        //        this.surveyDocumentIds = value;
        //    }
        //}
        /// <summary>
        /// The insured for the survey.
        /// </summary>
        [XmlElement]
        public Insured Insured
        {
            get
            {
                return this.insured;
            }
            set
            {
                this.insured = value;
            }
        }
        /// <summary>
        /// The agency.
        /// </summary>
        [XmlElement]
        public Agency Agency
        {
            get
            {
                return this.agency;
            }
            set
            {
                this.agency = value;
            }
        }
        /// <summary>
        /// The locations to survey.
        /// </summary>
        [XmlArrayItem(ElementName = "Location", Type = typeof(Location))]
        [XmlArray("LocationList")]
        public List<Location> Locations
        {
            get
            {
                return (this.locations == null) ? new List<Location>() : this.locations;
            }
            set
            {
                this.locations = value;
            }
        }
        /// <summary>
        /// The claims against the insured.
        /// </summary>
        [XmlArrayItem(ElementName = "Claim", Type = typeof(Claim))]
        [XmlArray("ClaimList")]
        public List<Claim> Claims
        {
            get
            {
                return (this.claims == null) ? new List<Claim>() : this.claims;
            }
            set
            {
                this.claims = value;
            }
        }

        /// <summary>
        /// The type of survey.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string SurveyType
        {
            get
            {
                return this.surveyType;
            }
            set
            {
                this.surveyType = value;
            }
        }

        /// <summary>
        /// The survey date.
        /// </summary>
        [XmlElement(DataType = "date")]
        public DateTime DateSurveyed
        {
            get
            {
                return this.dateSurveyed;
            }
            set
            {
                this.dateSurveyed = value;
            }
        }

        #endregion
    }
}