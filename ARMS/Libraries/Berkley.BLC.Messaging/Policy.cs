using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// Provides the policy details.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "Policy")]
    public class Policy
    {
        #region fields
        /// <summary>
        /// The policy number.
        /// </summary>
        private string number;
        /// <summary>
        /// The policy mod/version.
        /// </summary>
        private int mod;
        /// <summary>
        /// The policy symbol.
        /// </summary>
        private string symbol;
        /// <summary>
        /// The line of business.
        /// </summary>
        private string lineOfBusiness;
        /// <summary>
        /// The hazard grade.
        /// </summary>
        private string hazardGrade;
        /// <summary>
        /// The effective date.
        /// </summary>
        private DateTime effectiveDate;
        /// <summary>
        /// The expiration date.
        /// </summary>
        private DateTime expirationDate;
        /// <summary>
        /// The branch code.
        /// </summary>
        private string branchCode;
        /// <summary>
        /// The carrier.
        /// </summary>
        private string carrier;
        /// <summary>
        /// The profit center.
        /// </summary>
        private string profitCenter;
        /// <summary>
        /// The reportable coverages of the policy.
        /// </summary>
        private List<ReportableCoverage> reportableCoverages = new List<ReportableCoverage>();
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Policy()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The policy number.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Number
        {
            get
            {
                return this.number;
            }
            set
            {
                this.number = value;
            }
        }
        /// <summary>
        /// The policy mod/version.
        /// </summary>
        [XmlElement(DataType = "int")]
        public int Mod
        {
            get
            {
                return this.mod;
            }
            set
            {
                this.mod = value;
            }
        }
        /// <summary>
        /// The policy symbol.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Symbol
        {
            get
            {
                return this.symbol;
            }
            set
            {
                this.symbol = value;
            }
        }
        /// <summary>
        /// The line of business.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string LineOfBusiness
        {
            get
            {
                return this.lineOfBusiness;
            }
            set
            {
                this.lineOfBusiness = value;
            }
        }
        /// <summary>
        /// The hazard grade.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string HazardGrade
        {
            get
            {
                return this.hazardGrade;
            }
            set
            {
                this.hazardGrade = value;
            }
        }
        /// <summary>
        /// The effective date.
        /// </summary>
        [XmlElement(DataType = "date")]
        public DateTime EffectiveDate
        {
            get
            {
                return this.effectiveDate;
            }
            set
            {
                this.effectiveDate = value;
            }
        }
        /// <summary>
        /// The expiration date.
        /// </summary>
        [XmlElement(DataType = "date")]
        public DateTime ExpirationDate
        {
            get
            {
                return this.expirationDate;
            }
            set
            {
                this.expirationDate = value;
            }
        }
        /// <summary>
        /// The branch code.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string BranchCode
        {
            get
            {
                return this.branchCode;
            }
            set
            {
                this.branchCode = value;
            }
        }
        /// <summary>
        /// The carrier.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Carrier
        {
            get
            {
                return this.carrier;
            }
            set
            {
                this.carrier = value;
            }
        }
        /// <summary>
        /// The profit center.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string ProfitCenter
        {
            get
            {
                return this.profitCenter;
            }
            set
            {
                this.profitCenter = value;
            }
        }
        /// <summary>
        /// The reportable coverages.
        /// </summary>
        [XmlArrayItem(ElementName = "ReportableCoverage", Type = typeof(ReportableCoverage))]
        [XmlArray("ReportableCoverageList")]
        public List<ReportableCoverage> ReportableCoverages
        {
            get
            {
                return (this.reportableCoverages == null) ? new List<ReportableCoverage>() : this.reportableCoverages;
            }
            set
            {
                this.reportableCoverages = value;
            }
        }
        #endregion
    }
}