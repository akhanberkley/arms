using System;
using System.Collections;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// Provides the claims of the insured.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "Claim")]
    public class Claim
    {
        #region fields
        /// <summary>
        /// The policy number tied to the claim.
        /// </summary>
        private string policyNumber;
        /// <summary>
        /// The policy symbol tied to the claim.
        /// </summary>
        private string policySymbol;
        /// <summary>
        /// The policy expiration date tied to the claim.
        /// </summary>
        private DateTime policyExpirationDate;
        /// <summary>
        /// The date of the loss.
        /// </summary>
        private DateTime lossDate;
        /// <summary>
        /// The claimant.
        /// </summary>
        private string claimant;
        /// <summary>
        /// The claim status.
        /// </summary>
        private string claimStatus;
        /// <summary>
        /// The loss type.
        /// </summary>
        private string lossType;
        /// <summary>
        /// The claim comment.
        /// </summary>
        private string claimComment;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Claim()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The policy number tied to the claim.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string PolicyNumber
        {
            get
            {
                return this.policyNumber;
            }
            set
            {
                this.policyNumber = value;
            }
        }
        /// <summary>
        /// The policy symbol tied to the claim.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string PolicySymbol
        {
            get
            {
                return this.policySymbol;
            }
            set
            {
                this.policySymbol = value;
            }
        }
        /// <summary>
        /// The policy expiration date tied to the claim.
        /// </summary>
        [XmlElement(DataType = "date")]
        public DateTime PolicyExpirationDate
        {
            get
            {
                return this.policyExpirationDate;
            }
            set
            {
                this.policyExpirationDate = value;
            }
        }
        /// <summary>
        /// The date of the loss.
        /// </summary>
        [XmlElement(DataType = "date")]
        public DateTime LossDate
        {
            get
            {
                return this.lossDate;
            }
            set
            {
                this.lossDate = value;
            }
        }
        /// <summary>
        /// The claimant.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Claimant
        {
            get
            {
                return this.claimant;
            }
            set
            {
                this.claimant = value;
            }
        }
        /// <summary>
        /// The claim status.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string ClaimStatus
        {
            get
            {
                return this.claimStatus;
            }
            set
            {
                this.claimStatus = value;
            }
        }
        /// <summary>
        /// The loss type.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string LossType
        {
            get
            {
                return this.lossType;
            }
            set
            {
                this.lossType = value;
            }
        }
        /// <summary>
        /// The claim comment.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string ClaimComment
        {
            get
            {
                return this.claimComment;
            }
            set
            {
                this.claimComment = value;
            }
        }
        #endregion
    }
}