using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// An insured's location.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "Location")]
    public class Location
    {
        #region fields
        /// <summary>
        /// The location number.
        /// </summary>
        private int number;
        /// <summary>
        /// The location description.
        /// </summary>
        private string description;
        /// <summary>
        /// The location address.
        /// </summary>
        private Address addresss;
        /// <summary>
        /// The insured contacts.
        /// </summary>
        private List<Contact> contacts = new List<Contact>();
        /// <summary>
        /// The policies of the survey.
        /// </summary>
        private List<Policy> policies = new List<Policy>();
        /// <summary>
        /// The buildings at the location.
        /// </summary>
        private List<Building> buildings = new List<Building>();
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Location()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The location number.
        /// </summary>
        [XmlElement(DataType = "int")]
        public int Number
        {
            get
            {
                return this.number;
            }
            set
            {
                this.number = value;
            }
        }
        /// <summary>
        /// The location description.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Description
        {
            get
            {
                return this.description;
            }
            set
            {
                this.description = value;
            }
        }
        /// <summary>
        /// The location address.
        /// </summary>
        [XmlElement]
        public Address Address
        {
            get
            {
                return this.addresss;
            }
            set
            {
                this.addresss = value;
            }
        }
        /// <summary>
        /// The location contacts.
        /// </summary>
        [XmlArrayItem(ElementName = "Contact", Type = typeof(Contact))]
        [XmlArray("ContactList")]
        public List<Contact> Contacts
        {
            get
            {
                return (this.contacts == null) ? new List<Contact>() : this.contacts;
            }
            set
            {
                this.contacts = value;
            }
        }
        /// <summary>
        /// The policies of the survey.
        /// </summary>
        [XmlArrayItem(ElementName = "Policy", Type = typeof(Policy))]
        [XmlArray("PolicyList")]
        public List<Policy> Policies
        {
            get
            {
                return (this.policies == null) ? new List<Policy>() : this.policies;
            }
            set
            {
                this.policies = value;
            }
        }
        /// <summary>
        /// The buildings at the location.
        /// </summary>
        [XmlArrayItem(ElementName = "Building", Type = typeof(Building))]
        [XmlArray("BuildingList")]
        public List<Building> Buildings
        {
            get
            {
                return (this.buildings == null) ? new List<Building>() : this.buildings;
            }
            set
            {
                this.buildings = value;
            }
        }
        #endregion
    }
}