using System;
using System.Collections;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// Provides a note or a comment attached to the survey request.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "SurveyNote")]
    public class SurveyNote
    {
        #region fields
        /// <summary>
        /// The person that made the note.
        /// </summary>
        private string enteredBy;
        /// <summary>
        /// The date of the note.
        /// </summary>
        private DateTime dateEntered;
        /// <summary>
        /// The text of the note.
        /// </summary>
        private string noteText;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SurveyNote()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The person that made the note.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string EnteredBy
        {
            get
            {
                return this.enteredBy;
            }
            set
            {
                this.enteredBy = value;
            }
        }
        /// <summary>
        /// The date of the note.
        /// </summary>
        [XmlElement(DataType = "date")]
        public DateTime DateEntered
        {
            get
            {
                return this.dateEntered;
            }
            set
            {
                this.dateEntered = value;
            }
        }
        /// <summary>
        /// The text of the comment.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string NoteText
        {
            get
            {
                return this.noteText;
            }
            set
            {
                this.noteText = value;
            }
        }
        #endregion
    }
}