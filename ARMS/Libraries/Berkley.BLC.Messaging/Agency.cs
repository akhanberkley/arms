using System;
using System.Collections;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// Provides the agency information of the insured.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "Agency")]
    public class Agency
    {
        #region fields
        /// <summary>
        /// The agency number.
        /// </summary>
        private string number;
        /// <summary>
        /// The agency name.
        /// </summary>
        private string name;
        /// <summary>
        /// The agency address.
        /// </summary>
        private Address addresss;
        /// <summary>
        /// The agency contact information.
        /// </summary>
        private Communication communication;
        /// <summary>
        /// The agent.
        /// </summary>
        private Agent agent;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Agency()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The agency number.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Number
        {
            get
            {
                return this.number;
            }
            set
            {
                this.number = value;
            }
        }
        /// <summary>
        /// The agency name.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
        /// <summary>
        /// The agency address.
        /// </summary>
        [XmlElement]
        public Address Address
        {
            get
            {
                return this.addresss;
            }
            set
            {
                this.addresss = value;
            }
        }
        /// <summary>
        /// The agency contact information.
        /// </summary>
        [XmlElement]
        public Communication Communication
        {
            get
            {
                return this.communication;
            }
            set
            {
                this.communication = value;
            }
        }
        /// <summary>
        /// The agent.
        /// </summary>
        [XmlElement]
        public Agent Agent
        {
            get
            {
                return this.agent;
            }
            set
            {
                this.agent = value;
            }
        }
        #endregion
    }
}