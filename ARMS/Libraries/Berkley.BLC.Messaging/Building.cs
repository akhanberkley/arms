using System;
using System.Collections;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// A building at a location.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "Building")]
    public class Building
    {
        #region fields
        /// <summary>
        /// The building number.
        /// </summary>
        private string number;
        /// <summary>
        /// The year the building was built.
        /// </summary>
        private int yearBuilt;
        /// <summary>
        /// A flag indicating if the buiding has a sprinkler system.
        /// </summary>
        private bool hasSprinklerSystem;
        /// <summary>
        /// The public protection value.
        /// </summary>
        private int publicProtection;
        /// <summary>
        /// The stock values of the building.
        /// </summary>
        private decimal stockValues;
        /// <summary>
        /// The value of the contents in the building.
        /// </summary>
        private decimal contents;
        /// <summary>
        /// The building value.
        /// </summary>
        private decimal buildingValue;
        /// <summary>
        /// The business interruption value.
        /// </summary>
        private decimal businessInterruption;
        /// <summary>
        /// The location occupancy description.
        /// </summary>
        private string locationOccupancy;
        /// <summary>
        /// The building valuation type.
        /// </summary>
        private string buildingValuationType;
        /// <summary>
        /// The coinsurance percentage.
        /// </summary>
        private double coinsurancePercentage;
        /// <summary>
        /// The change in environmental control coverage.
        /// </summary>
        private decimal changeInEnvironmentalControl;
        /// <summary>
        /// The scientific animals coverage.
        /// </summary>
        private decimal scientificAnimals;
        /// <summary>
        /// The contamination coverage.
        /// </summary>
        private decimal contamination;
        /// <summary>
        /// The radio active contamination coverage.
        /// </summary>
        private decimal radioActiveContamination;
        /// <summary>
        /// The flood coverage.
        /// </summary>
        private decimal flood;
        /// <summary>
        /// The earthquake coverage.
        /// </summary>
        private decimal earthquake;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Building()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The building number.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string Number
        {
            get
            {
                return this.number;
            }
            set
            {
                this.number = value;
            }
        }
        /// <summary>
        /// The year the building was built.
        /// </summary>
        [XmlElement(DataType = "int")]
        public int YearBuilt
        {
            get
            {
                return this.yearBuilt;
            }
            set
            {
                this.yearBuilt = value;
            }
        }
        /// <summary>
        /// A flag indicating if the buiding has a sprinkler system.
        /// </summary>
        [XmlElement(DataType = "boolean")]
        public bool HasSprinklerSystem
        {
            get
            {
                return this.hasSprinklerSystem;
            }
            set
            {
                this.hasSprinklerSystem = value;
            }
        }
        /// <summary>
        /// The public protection value.
        /// </summary>
        [XmlElement(DataType = "int")]
        public int PublicProtection
        {
            get
            {
                return this.publicProtection;
            }
            set
            {
                this.publicProtection = value;
            }
        }
        /// <summary>
        /// The stock values of the building.
        /// </summary>
        [XmlElement(DataType = "decimal")]
        public decimal StockValues
        {
            get
            {
                return this.stockValues;
            }
            set
            {
                this.stockValues = value;
            }
        }
        /// <summary>
        /// The value of the contents in the building.
        /// </summary>
        [XmlElement(DataType = "decimal")]
        public decimal Contents
        {
            get
            {
                return this.contents;
            }
            set
            {
                this.contents = value;
            }
        }
        /// <summary>
        /// The building value.
        /// </summary>
        [XmlElement(DataType = "decimal")]
        public decimal BuildingValue
        {
            get
            {
                return this.buildingValue;
            }
            set
            {
                this.buildingValue = value;
            }
        }
        /// <summary>
        /// The business interruption value.
        /// </summary>
        [XmlElement(DataType = "decimal")]
        public decimal BusinessInterruption
        {
            get
            {
                return this.businessInterruption;
            }
            set
            {
                this.businessInterruption = value;
            }
        }
        /// <summary>
        /// The location occupancy description.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string LocationOccupancy
        {
            get
            {
                return this.locationOccupancy;
            }
            set
            {
                this.locationOccupancy = value;
            }
        }
        /// <summary>
        /// The valuation type of the building.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string BuildingValuationType
        {
            get
            {
                return this.buildingValuationType;
            }
            set
            {
                this.buildingValuationType = value;
            }
        }
        /// <summary>
        /// The coinsurance percentage.
        /// </summary>
        [XmlElement(DataType = "double")]
        public double CoinsurancePercentage
        {
            get
            {
                return this.coinsurancePercentage;
            }
            set
            {
                this.coinsurancePercentage = value;
            }
        }
        /// <summary>
        /// The change in environmental control coverage.
        /// </summary>
        [XmlElement(DataType = "decimal")]
        public decimal ChangeInEnvironmentalControlCoverage
        {
            get
            {
                return this.changeInEnvironmentalControl;
            }
            set
            {
                this.changeInEnvironmentalControl = value;
            }
        }
        /// <summary>
        /// The scientif animals coverage.
        /// </summary>
        [XmlElement(DataType = "decimal")]
        public decimal ScientificAnimalsCoverage
        {
            get
            {
                return this.scientificAnimals;
            }
            set
            {
                this.scientificAnimals = value;
            }
        }
        /// <summary>
        /// The contamination coverage.
        /// </summary>
        [XmlElement(DataType = "decimal")]
        public decimal ContaminationCoverage
        {
            get
            {
                return this.contamination;
            }
            set
            {
                this.contamination = value;
            }
        }
        /// <summary>
        /// The radio active contamination coverage.
        /// </summary>
        [XmlElement(DataType = "decimal")]
        public decimal RadioActiveContaminationCoverage
        {
            get
            {
                return this.radioActiveContamination;
            }
            set
            {
                this.radioActiveContamination = value;
            }
        }
        /// <summary>
        /// The flood coverage.
        /// </summary>
        [XmlElement(DataType = "decimal")]
        public decimal FloodCoverage
        {
            get
            {
                return this.flood;
            }
            set
            {
                this.flood = value;
            }
        }
        /// <summary>
        /// The earthquake coverage.
        /// </summary>
        [XmlElement(DataType = "decimal")]
        public decimal EarthquakeCoverage
        {
            get
            {
                return this.earthquake;
            }
            set
            {
                this.earthquake = value;
            }
        }
        #endregion
    }
}