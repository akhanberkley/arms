using System;
using System.Collections;
using System.Xml.Serialization;

namespace Berkley.BLC.Messaging
{
    /// <summary>
    /// Provides the reportable coverage.
    /// </summary>
    [XmlType(Namespace = "http://wrberkley.com/schema/arms/v1", TypeName = "ReportableCoverage")]
    public class ReportableCoverage
    {
        #region fields
        /// <summary>
        /// The coverage name.
        /// </summary>
        private string coverageName;
        /// <summary>
        /// The coverage type.
        /// </summary>
        private string coverageType;
        /// <summary>
        /// The coverage value.
        /// </summary>
        private string coverageValue;
        #endregion

        #region constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ReportableCoverage()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// The coverage name.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string CoverageName
        {
            get
            {
                return this.coverageName;
            }
            set
            {
                this.coverageName = value;
            }
        }
        /// <summary>
        /// The coverage Type.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string CoverageType
        {
            get
            {
                return this.coverageType;
            }
            set
            {
                this.coverageType = value;
            }
        }
        /// <summary>
        /// A coverage value.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string CoverageValue
        {
            get
            {
                return this.coverageValue;
            }
            set
            {
                this.coverageValue = value;
            }
        }
        #endregion
    }
}