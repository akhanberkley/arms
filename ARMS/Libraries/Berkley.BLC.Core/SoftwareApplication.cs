/*===========================================================================
  File:			SoftwareApplication.cs
  
  Summary:		Provides a base implementation for the ISoftwareApplication
				interface.
			
  Classes:		SoftwareApplication
  
  Enumerations:	None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using Berkley.BLC.Core;

namespace Berkley.BLC.Core
{
	/// <summary>
	/// Provides a base implementation for the ISoftwareApplication 
	/// interface.
	/// </summary>
	public class SoftwareApplication : ISoftwareApplication
	{
		#region fields
		/// <summary>
		/// The name of the software application.
		/// </summary>
		private string name;
		/// <summary>
		/// The unique code for the software application.
		/// </summary>
		private string code;
		/// <summary>
		/// The primary business contact for the software application.
		/// </summary>
		private IContact businessContact;
		/// <summary>
		/// The primary techical contact for the software application.
		/// </summary>
		private IContact technicalContact;
		#endregion

		#region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		public SoftwareApplication(){}
		/// <summary>
		/// Instantiates the object statefully.
		/// </summary>
		/// <param name="name">The name of the software application.</param>
		/// <param name="code">The unique code for the software application.</param>
		/// <param name="businessContact">The primary business contact for the 
		/// software application.</param>
		/// <param name="technicalContact">The primary techical contact for the 
		/// software application.</param>
		public SoftwareApplication(string name, string code, IContact businessContact, IContact technicalContact)
		{
			this.name = name;
			this.code = code;
			this.businessContact = businessContact;
			this.technicalContact = technicalContact;
		}
		#endregion

		#region properties
		#endregion

		#region methods
		#endregion

		#region ISoftwareApplication Members
		/// <summary>
		/// The name of the software application.
		/// </summary>
		public virtual string Name
		{
			get
			{
				return name;
			}
		}
		/// <summary>
		/// The unique code for the software application.
		/// </summary>
		public virtual string Code
		{
			get
			{
				return code;
			}
		}
		/// <summary>
		/// The primary business contact for the software application.
		/// </summary>
		public virtual IContact BusinessContact
		{
			get
			{
				return businessContact;
			}
		}
		/// <summary>
		/// The primary techical contact for the software application.
		/// </summary>
		public virtual IContact TechnicalContact
		{
			get
			{
				return technicalContact;
			}
		}
		#endregion
	}
}
