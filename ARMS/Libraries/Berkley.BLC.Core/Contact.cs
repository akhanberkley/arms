/*===========================================================================
  File:			Contact.cs
  
  Summary:		Provides the base implementation for the IContact interface.
			
  Classes:		Contact
  
  Enumerations:	None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using Berkley.BLC.Core;

namespace Berkley.BLC.Core
{
	/// <summary>
	/// Provides the base implementation for the IContact interface.
	/// </summary>
	public class Contact : IContact
	{
		#region fields
		/// <summary>
		/// The name of the	contact.
		/// </summary>
		private string name;
		/// <summary>
		/// The primary email address of the contact.
		/// </summary>
		private string email;
		/// <summary>
		/// The primary telephone number of the contact.
		/// </summary>
		private string phone;
		#endregion

		#region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		public Contact(){}
		/// <summary>
		/// Instantiates the contact statefully.
		/// </summary>
		/// <param name="name">The name of the	contact.</param>
		/// <param name="emailAddress">The primary email address of the contact.</param>
		/// <param name="telephoneNumber">The primary telephone number of the contact.</param>
		public Contact(string name, string emailAddress, string telephoneNumber)
		{
			this.name = name;
			this.phone = telephoneNumber;
			this.email = emailAddress;
		}
		#endregion

		#region properties
		#endregion

		#region methods
		#endregion

		#region IContact Members
		/// <summary>
		/// The name of the	contact.
		/// </summary>
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}
		/// <summary>
		/// The primary email address of the contact.
		/// </summary>
		public string EmailAddress
		{
			get
			{
				return email;
			}
			set
			{
				email = value;
			}
		}
		/// <summary>
		/// The primary telephone number of the contact.
		/// </summary>
		public string TelephoneNumber
		{
			get
			{
				return phone;
			}
			set
			{
				phone = value;
			}
		}
		#endregion
	}
}
