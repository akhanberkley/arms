/*===========================================================================
  File:			IContact.cs
  
  Summary:		Serves as a contract of behavior for classes that represent a 
				person, organization, or system that can be contacted.
			
  Classes:		None
  
  Enumerations:	None
  
  Interfaces:	IContact
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.BLC.Core
{
	/// <summary>
	/// Serves as a contract of behavior for classes that represent a 
	/// person, organization, or system that can be contacted.
	/// </summary>
	public interface IContact
	{
		/// <summary>
		/// The name of the	contact.
		/// </summary>
		string Name{get;}
		/// <summary>
		/// The primary email address of the contact.
		/// </summary>
		string EmailAddress{get;}
		/// <summary>
		/// The primary telephone number of the contact.
		/// </summary>
		string TelephoneNumber{get;}
	}
}