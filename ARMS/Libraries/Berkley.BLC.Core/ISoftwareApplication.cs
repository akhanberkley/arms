/*===========================================================================
  File:			ISoftwareApplication.cs
  
  Summary:		Serves as a contract of behavior for classes that represent a 
				software application.
			
  Classes:		None
  
  Enumerations:	None
  
  Interfaces:	ISoftwareApplication
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.BLC.Core
{
	/// <summary>
	/// Serves as a contract of behavior for classes that represent a 
	/// software application.
	/// </summary>
	public interface ISoftwareApplication
	{
		/// <summary>
		/// The name of the software application.
		/// </summary>
		string Name{get;}
		/// <summary>
		/// The unique code for the software application.
		/// </summary>
		string Code{get;}
		/// <summary>
		/// The primary business contact for the software application.
		/// </summary>
		IContact BusinessContact{get;}
		/// <summary>
		/// The primary techical contact for the software application.
		/// </summary>
		IContact TechnicalContact{get;}
	}
}