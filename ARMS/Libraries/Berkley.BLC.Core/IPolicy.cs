using System;

namespace Berkley.BLC.Core
{
    /// <summary>
    /// Serves as a contract of behavior for classes that represent an
    /// insurance policy
    /// </summary>
    public interface IPolicy
    {
        /// <summary>
        /// The policy number.
        /// </summary>
        string Number { get; }

        /// <summary>
        /// The policy symbol.
        /// </summary>
        string Symbol { get; }

        /// <summary>
        /// Effective date of this policy.
        /// </summary>
        DateTime EffectiveDate { get; }
    }
}
