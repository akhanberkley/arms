﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;


namespace Berkley.BLC.Core
{

    public sealed class Session
    {
        public struct Keys
        {
            public const string CompanyId = "CompanyId";
           
            //public static readonly string UserCompany = "UserIdCompany";
            public static readonly string UserSession = string.Format("UserSession4{0}", HttpContext.Current.User.Identity.Name);
        }

        public static T Get<T>(string key)
        {
            if (HttpContext.Current.Session[key] != null)
                return (T)HttpContext.Current.Session[key];
            else return default(T);
        }
        
        public static T Get<T>(string key, int id)
        {
            return Get<T>(key, id.ToString());
        }

        public static T Get<T>(string key, string id)
        {
            return (T)HttpContext.Current.Session[string.Format(key, id)];
        }

        public static void Set<T>(string key, T value)
        {
            HttpContext.Current.Session[key] = value;
        }

        public static void Set<T>(string key,int id, T value)
        {
            HttpContext.Current.Session[string.Format(key, id)] = value;
        }

        public static void Set<T>(string key, string id, T value)
        {
            HttpContext.Current.Session[string.Format(key, id)] = value;
        }

        public static void Remove(string key)
        {
            HttpContext.Current.Session.Remove(key);
        }

        public static void Remove(string key, int id)
        {
            HttpContext.Current.Session.Remove(string.Format(key,id));
        }

    }

    public sealed class Cache
    {
        public static T Get<T>(string key)
        {
            if (HttpContext.Current.Cache[key] != null)
                return (T)HttpContext.Current.Cache[key];
            else return default(T);
        }

        public static T Get<T>(string key, int companyId)
        {
            return (T)HttpContext.Current.Cache[string.Format(key,companyId)];
        }

        public static void Set(string key, object value)
        {
            //due to generic objects - can't serialize for WCF call for Clearing both servers
            BTS.CacheEngine.V2.CacheEngine engine = new BTS.CacheEngine.V2.CacheEngine(180);
            engine.AddItemToCache(key, value);
        }

        public static void Set(string key, int id, object value)
        {
            Set(string.Format(key,id), value);
        }        

        //public static void Remove(string key)
        //{
        //    RemoveAllServerCache(key);
        //}

        //public static void Remove(string key, int id)
        //{
        //    Remove(string.Format(key,id));
        //}

        public static void RemoveLocal(string key)
        {
            BTS.CacheEngine.V2.CacheEngine engine = new BTS.CacheEngine.V2.CacheEngine(180);
            engine.RemoveItemFromCache(key);
        }

        //public static void RemoveAllServerCache(string key)
        //{
        //    string serv = Config.GetAppSetting("BOAServers");
        //    string[] servers = serv.Split('|');

        //    if (servers.Length > 0)
        //    {
        //        foreach (string s in servers)
        //        {
        //            CacheService.CacheServiceClient client = new CacheService.CacheServiceClient();
        //            System.ServiceModel.EndpointAddress address = new System.ServiceModel.EndpointAddress(string.Format("http://{0}/Library/Services/CacheService.svc", s));
        //            client.Endpoint.Address = address;

        //            try
        //            {
        //                client.RemoveCacheKey(key);
        //            }
        //            catch (Exception ex)
        //            {
        //                QCI.ExceptionManagement.ExceptionManager.PublishEmail(ex);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        //Use local - config entry missing
        //        RemoveLocal(key);
        //        QCI.ExceptionManagement.ExceptionManager.PublishEmail(new Exception("Please set local machine config 'BOAServers' - using local as default"));
        //    }
        //}

        
        public struct Keys
        {
            public const string Parameters = "Parameters";
            public const string CompanyParameters = "CompanyParameters{0}";
            
        }
    }

    public sealed class Cookie
    {
        public static string Get(string key)
        {
            if (HttpContext.Current.Response.Cookies[key] != null && HttpContext.Current.Response.Cookies[key].Value != null)
                return Security.DecryptString(HttpContext.Current.Response.Cookies[key].Value);
            else return string.Empty;
        }

        public static string Get(string key, int companyId)
        {
            return Get(string.Format(key, companyId));
        }

        public static void Set(string key, object value)
        {
            HttpCookie cookie = HttpContext.Current.Response.Cookies[key];

            if (cookie == null)
            {
                cookie = new HttpCookie(key, value.ToString());
                HttpContext.Current.Response.Cookies.Add(cookie);
                return;
            }

            HttpContext.Current.Response.Cookies[key].Value = Security.EncryptString(value.ToString());
        }

        public static void Set(string key, int id, object value)
        {
            Set(string.Format(key, id), value);
        }

        public static void Remove(string key)
        {
            HttpContext.Current.Response.Cookies.Remove(key);
        }

        public static void Remove(string key, int id)
        {
            Remove(string.Format(key, id));
        }

        public struct Keys
        {
            public const string Company = "Company";
            public const string UserId = "UserId";
        }
    }
   
}
