/*===========================================================================
  File:     ConfigurationType.cs
  
  Summary:  An environment in which a software system can operate. This falls 
			under the category of Software Configuration Management in Software
			Engineering. 
			
  Classes:  None
  
  Enums:	ConfigurationType
  
  Origin:   Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.BLC.Core
{
	/// <summary>
	/// An environment in which a software system can operate. This falls 
	/// under the category of Software Configuration Management in Software
	/// Engineering. 
	/// </summary>
	public enum ConfigurationType
	{
		/// <summary>
		/// The development configuration.
		/// </summary>
		Development,
		/// <summary>
		/// The integration configuration.
		/// </summary>
		Integration,
		/// <summary>
		/// The testing configuration.
		/// </summary>
		Test,
		/// <summary>
		/// The production configuration.
		/// </summary>
		Production
	}
}