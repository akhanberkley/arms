﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Web;
using System.Web.Security;


namespace Berkley.BLC.Core
{
    public sealed partial class Security
    {
         

        //Key for symmetric encryption/decription
        private static readonly string _key = "s????F?p|???T??O?a<?N ??6? ?;";

        public static string GenerateSecurityCode(int length)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            string s = string.Empty;
            byte[] rnd = null;
            bool isOnlyAlpha = true;
            while (s.Length < length)
            {
                rnd = new byte[1];
                rng.GetBytes(rnd);

                if (((rnd[0] > 63 && rnd[0] < 91)
                    || (rnd[0] > 49 && rnd[0] < 58)
                    || (rnd[0] > 34 && rnd[0] < 39)
                    || (rnd[0] > 96 && rnd[0] < 123))
                    && rnd[0] != 124
                    && rnd[0] != 60
                    && rnd[0] != 62
                    && rnd[0] != 79
                    && rnd[0] != 73
                    && rnd[0] != 108
                    )
                {
                    s = s + System.Text.ASCIIEncoding.ASCII.GetString(rnd);
                    if (isOnlyAlpha && !((rnd[0] > 64 && rnd[0] < 91) || (rnd[0] > 96 && rnd[0] < 123)))
                        isOnlyAlpha = false;
                }

                if (s.Length == length && isOnlyAlpha)
                    s = s.Substring(length / 3, length / 3);
            }

            return s;
        }

        public static string GenerateAlphaNumericSecurityCode(int length)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            string s = string.Empty;
            byte[] rnd = null;
            bool isOnlyAlpha = true;
            while (s.Length < length)
            {
                rnd = new byte[1];
                rng.GetBytes(rnd);

                if ((rnd[0] > 96 && rnd[0] < 123)
                    || (rnd[0] > 47 && rnd[0] < 58))
                {
                    s = s + System.Text.ASCIIEncoding.ASCII.GetString(rnd);
                    if (isOnlyAlpha && !((rnd[0] > 64 && rnd[0] < 91) || (rnd[0] > 96 && rnd[0] < 123)))
                        isOnlyAlpha = false;
                }
            }

            return s;
        }

        /// <summary>
        /// Returns encrypted string + IV
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string EncryptString(string str)
        {
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            //aes.Padding = PaddingMode.None;
            aes.Key = System.Text.Encoding.Default.GetBytes(_key);

            byte[] input = System.Text.Encoding.Default.GetBytes(str);

            var encryptor = aes.CreateEncryptor(aes.Key,aes.IV);

            byte[] result1 = encryptor.TransformFinalBlock(input, 0, input.Length);

            StringBuilder result = new StringBuilder();

            result.Append(ByteArrayToHex(result1));
            
            result.Append('|');

            result.Append(ByteArrayToHex(aes.IV));

            result.Append('|');

            return result.ToString();
        }

        /// <summary>
        /// Decrypts a string
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string DecryptString(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return string.Empty;

            string result = string.Empty;

            string[] parts = str.Split('|');

            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            //aes.Padding = PaddingMode.None;
            aes.Key = System.Text.Encoding.Default.GetBytes(_key);

            //byte[] input = System.Text.Encoding.Default.GetBytes(parts[0]);
            //byte[] IV = System.Text.Encoding.Default.GetBytes(parts[1]);

            byte[] input = HexToByteArray(parts[0]);
            byte[] IV = HexToByteArray(parts[1]);

            var decryptor = aes.CreateDecryptor(aes.Key, IV);

            result = Encoding.Default.GetString(decryptor.TransformFinalBlock(input, 0, input.Length));

            return result;
        }

            //http://stackoverflow.com/questions/321370/convert-hex-string-to-byte-array
        public static byte[] HexToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length).Where(x => 0 == x % 2).Select(x => Convert.ToByte(hex.Substring(x, 2), 16)).ToArray();
        }

        //http://stackoverflow.com/questions/311165/how-do-you-convert-byte-array-to-hexadecimal-string-and-vice-versa-in-c
        public static string ByteArrayToHex(byte[] bytes)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);
            foreach (byte b in bytes)
                result.AppendFormat("{0:x2}", b);

            return result.ToString();
        }

    }
}
