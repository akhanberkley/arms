/*===========================================================================
  File:			Insured.cs
  
  Summary:		Serves as a contract of behavior for classes that represent an
				insured.
			
  Classes:		None
  
  Enumerations:	None
  
  Interfaces:	IInsured
  
  Origin:		Premium Audit Tracking System (September, 2007)
  
=============================================================================
  Copyright (c) 2007 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.BLC.Core
{
    /// <summary>
    /// Serves as a contract of behavior for classes that represent an
    /// insured.
    /// </summary>
    public interface IInsured
    {
        /// <summary>
        /// The client id.
        /// </summary>
        string ClientID { get; }
    }
}