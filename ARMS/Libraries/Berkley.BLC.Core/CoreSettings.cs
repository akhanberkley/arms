using Berkley.BLC.Core.Properties;
using System;
using System.Configuration;

namespace Berkley.BLC.Core
{
    public class CoreSettings
    {
        public static string ARMSConnectionString { get { return ConfigurationManager.AppSettings["ARMSConnectionString"]; } }
        public static string ARMSRepositoryConnectionString { get { return ConfigurationManager.AppSettings["ARMSFormFieldRepositoryConnectionString"]; } }
        public static string AICConnectionString { get { return ConfigurationManager.AppSettings["AICConnectionString"]; } }
        public static string CWGConnectionString { get { return ConfigurationManager.AppSettings["CWGConnectionString"]; } }
        public static string BNPConnectionString { get { return ConfigurationManager.AppSettings["BNPConnectionString"]; } }
        public static string BARSConnectionString { get { return ConfigurationManager.AppSettings["BARSConnectionString"]; } }
        public static string NationalAccountsConnectionString { get { return ConfigurationManager.AppSettings["NationalAccountsConnectionString"]; } }
        public static string BMAGConnectionString { get { return ConfigurationManager.AppSettings["BMAGConnectionString"]; } }
        public static string CCICConnectionString { get { return ConfigurationManager.AppSettings["CCICConnectionString"]; } }
        public static string USIGConnectionString { get { return ConfigurationManager.AppSettings["USIGConnectionString"]; } }
        public static string BOGConnectionString { get { return ConfigurationManager.AppSettings["ARMSBOGConnectionString"]; } }
        public static string SurveyPage { get { return ConfigurationManager.AppSettings["SurveyPage"]; } }
        public static string SmtpServer { get { return ConfigurationManager.AppSettings["SmtpServer"]; } }
        public static string ClaimsWebServiceId { get { return Resources.ClaimsWebServiceId; } }
        public static string AddDocAllowedAttempts { get { return ConfigurationManager.AppSettings["AddDocAllowedAttempts"]; } }
        public static string MapService { get { return ConfigurationManager.AppSettings["MapService"]; } }
        public static string BOGEmailNotification { get { return ConfigurationManager.AppSettings["ARMSBOGEmailNotificationList"]; } }
        public static string BSDConnectionString { get { return ConfigurationManager.AppSettings["BSUMConnectionString"]; } }

        /// <summary>
        /// Returns the configuration to which the workflow operations will be performed.
        /// </summary>
        public static Berkley.BLC.Core.ConfigurationType ConfigurationType
        {
            get
            {
                ConfigurationType result;

                try
                {
                    string configuration = ConfigurationManager.AppSettings["ARMSConfigurationType"];
                    result = (ConfigurationType)Enum.Parse(typeof(ConfigurationType), configuration, true);
                }
                catch (ArgumentNullException)
                {
                    result = ConfigurationType.Development;
                    System.Diagnostics.Trace.WriteLine("A software configuration setting (ConfigurationType) was not found while executing Berkley.BLC.Business.WorkflowHelper. The configuration will default to 'Development'.");
                }
                catch (ArgumentException)
                {
                    result = ConfigurationType.Development;
                    System.Diagnostics.Trace.WriteLine("A software configuration setting (ConfigurationType) was found, but did not have a value that can be converted to a ConfigurationType enumeration. This occurred while executing Berkley.BLC.Business.WorkflowHelper. The configuration will default to 'Development'.");
                }
                return result;
            }
        }
    }
}
