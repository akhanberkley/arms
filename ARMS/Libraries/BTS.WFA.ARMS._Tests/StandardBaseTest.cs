﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.ARMS._Tests
{
    public class StandardBaseTest
    {
        [TestInitialize]
        public void Initialization()
        {
            Application.Initialize<Data.SqlDb>(Application.GetStandardDebugHandlers());   
            Debug.WriteLine("--Standard Setup Completed--");
            Debug.WriteLine("----------------------------");
        }
    }
}
