﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.ARMS._Tests.Services
{
    [TestClass]
    public class TerritoryServiceTests : StandardBaseTest
    {
        [TestMethod]
        public void GetAdminTerritories()
        {
            ARMS.Services.TerritoryService.GetTerritories(Application.CompanyKeyMap["aic"]);

        }

        [TestMethod]
        public void SaveTerritory()
        {
            var Territory = new ViewModels.TerritoryViewModel();
            Territory.ID = Guid.NewGuid();
            Territory.Name = "Test testing 1";
            Territory.Description = "Territory Description";
            Territory.InHouseSelection = true;            
            Territory.UserTerritories.Add(new ViewModels.UserTerritoriesViewModel() { User = new ViewModels.UserViewModel() { Name = "Cleo Powell" }, PercentAllocated = 100 });
            
           
            ARMS.Services.TerritoryService.SaveTerritory(Territory, Application.CompanyKeyMap["aic"]);
        }
    }
}
