﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using BTS.WFA.ARMS.Services;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS._Tests.Services
{
    [TestClass]
    public class RequestQueueServiceTests : StandardBaseTest
    {
        [TestMethod]
        public void GetRequestQueue()   //WA-141 Doug Bruce 2015-03-27 - RequestQueue DB Structure
        {
            //RequestQueueManagerService qManager = new ARMS.Services.RequestQueueManagerService();
            var rqs = RequestQueueManagerService.Search(Application.CompanyKeyMap["bmag"], (q) => q);
            Assert.AreEqual(3, rqs.Count);
        }
    }
}
