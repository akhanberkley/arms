﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS._Tests.Services
{
    [TestClass]
    public class LinkService : StandardBaseTest
    {
        [TestMethod]
        public void GetLinksByCompanyID()
        {
            ARMS.Services.LinkService.GetAll(Application.CompanyNumberMap["52"]);
        }

        [TestMethod]
        public void AddLink()
        {
            var newLink = new LinkViewModel();
            newLink.URL = "http://www.wrberkley.com";
            newLink.Description = "W. R. Berkley Corporation is a financial services company focused on the property and casualty insurance business.";
            
            ARMS.Services.LinkService.Save(newLink, Application.CompanyNumberMap["52"]);
        }

        [TestMethod]
        public void UpdateLink()
        {
            //updateLink = Get a link to make changes to
            //ARMS.Services.LinkService.UpdateLink(updateLink, Application.CompanyNumberMap["52"]);
        }

        [TestMethod]
        public void DeleteLink()
        {
            // nop
        }
    }
}
