﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS._Tests.Services
{
    [TestClass]
    public class RecsServiceTests : StandardBaseTest
    {
        [TestMethod]
        public void GetAdminRecss()
        {
            ARMS.Services.RecsService.GetRecs(Application.CompanyKeyMap["aic"]);

        }

        [TestMethod]
        public void SaveRec()
        {
            var Rec = new ViewModels.RecViewModel();
            Rec.Code = "Test Code";
            Rec.Title = "Test Title ";
            Rec.Description = "Test Description";
            ARMS.Services.RecsService.SaveRec(Rec, Application.CompanyKeyMap["aic"]);
        }
    }
}
