﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace BTS.WFA.ARMS._Tests.Services
{

    [TestClass]
    public class UsersServiceTests : StandardBaseTest
    {
        //[TestMethod]
        //public void GetUsers()
        //{
        //    ARMS.Services.UsersService.GetUser(Application.CompanyNumberMap["aic"],true);

        //}

        [TestMethod]
        public void SaveUser()
        {
            var user = new ViewModels.UserViewModel();
            
            //user.Title = "Test Title ";
            user.Name = "Test Description";
            ARMS.Services.UsersService.SaveUser(user, Application.CompanyKeyMap["aic"]);
        }
    }
}
