﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BTS.WFA.ARMS._Tests.Services
{
    [TestClass]
    public class SurveyServiceTests : StandardBaseTest
    {

        //[TestMethod]
        //public void GetSurveySearchPage()
        //{
        //    ARMS.Services.SurveyService.GetSearchViewModel(Application.CompanyKeyMap["aic"]);

        //}

        [TestMethod]
        public void GetSurveyBySurveyNumber()
        {
            ARMS.Services.SurveyQueryService.Get(Application.CompanyKeyMap["aic"], 161032);
        }

        #region AIC
        [TestMethod]
        public void AICCreateANewSurvey()
        {
            //ARMS.Services.SurveyService.Save(Application.CompanyKeyMap["aic"], new ViewModels.SurveyViewModel());
        }
        #endregion

        [TestMethod]
        public void GetSurveyCWG()
        {
            //var survey = ARMS.Services.SurveyQueryService.Get(Application.CompanyKeyMap["CWG"], 146221);
            var survey = ARMS.Services.SurveyQueryService.Get(Application.CompanyKeyMap["CWG"], 146381);
        }
    }
}
