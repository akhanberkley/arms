﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace BTS.WFA.ARMS._Tests.Services
{
    [TestClass]
    public class DocumentsServiceTests : StandardBaseTest
    {
        [TestMethod]
        public void SaveDocument()
        {
            var Document = new ViewModels.DocumentViewModel();
            Document.Name = "New Test Document";
            Document.Disabled = false;
            ARMS.Services.DocumentsService.SaveDocument(Document, Application.CompanyKeyMap["aic"]);
        }
    }
}
