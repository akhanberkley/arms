﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS._Tests.Services
{
    [TestClass]
   public class WebPageHelpfulHintServiceTests : StandardBaseTest
    {
       [TestMethod]
       public void GetWebPageHelpfulHints()
       {
           ARMS.Services.WebPageHelpfulHintService.GetHelpfulHint(Application.CompanyKeyMap["aic"]);            
           
       }
       
        [TestMethod]
        public void UpdateHelpfulHint()
        {
            var WebPageHelpfulHint = new ViewModels.WebPageHelpfulHintViewModel();
            WebPageHelpfulHint.HelpfulHintText  = " Test Helpful hint Text";
            WebPageHelpfulHint.WebPagePath = "/AdminActivityTimeEdit.aspx";
            WebPageHelpfulHint.AlwaysVisible = false;
            ARMS.Services.WebPageHelpfulHintService.UpdateWebPageHelpfulHint(WebPageHelpfulHint,Application.CompanyKeyMap["aic"]);
        } 
    }
}
