﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace BTS.WFA.ARMS._Tests.Services
{

    [TestClass]
    public class FeeCompaniesService : StandardBaseTest
    {
        [TestMethod]
        public void GetFeeCompanies()
        {
            ARMS.Services.FeeCompaniesService.GetFeeCompanies(Application.CompanyNumberMap["52"]);

        }

        [TestMethod]
        public void SaveUser()
        {
            var user = new ViewModels.UserViewModel();

            user.Title = "Test Title ";
            user.Name = "Test Description";
            ARMS.Services.FeeCompaniesService.SaveFeeCompany(user, Application.CompanyNumberMap["52"]);
        }
    }
}
