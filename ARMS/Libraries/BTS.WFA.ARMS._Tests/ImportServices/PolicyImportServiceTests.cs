﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS._Tests.ImportServices
{
    [TestClass]
    public class PolicyImportServiceTests : StandardBaseTest
    {
        [TestMethod]
        public void GetPolicyByPolicyNumber()
        {
            var result = ARMS.ImportServices.PolicyImportService.FindsPolicies(Application.CompanyKeyMap["aic"], "0305942");
            Assert.IsTrue(result);
        }
    }
}
