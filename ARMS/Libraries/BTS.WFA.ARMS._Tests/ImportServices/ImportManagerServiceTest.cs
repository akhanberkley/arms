﻿using System.ServiceModel.Web;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BTS.WFA.ARMS.ImportServices;

namespace BTS.WFA.ARMS._Tests.ImportServices
{
    [TestClass]
    public class ImportManagerServiceTest : StandardBaseTest, IService
    {
        [TestMethod]
        public void AsyncProcessTest()
        {
            ImportManagerService ims = new ImportManagerService();
            var result = ims.TestAsyncProcess();    //start the processes in parallel and wait til they are all done.
        }
    }
}
