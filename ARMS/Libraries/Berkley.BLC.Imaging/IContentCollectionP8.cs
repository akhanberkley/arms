/*===========================================================================
  File:			IContentCollectionP8.cs
  
  Summary:		Serves as a typed collection of IFile objects. This class can 
				be killed as soon as Microsoft releases Generics (and the 
				calling code is suitably refactored).
			
  Classes:		IContentCollectionP8
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:   	WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.Collections;

namespace Berkley.Imaging
{
	/// <summary>
	/// Serves as a typed collection of IFile objects. This class can be killed
	/// as soon as Microsoft releases Generics (and the calling code is 
	/// refactored).
	/// </summary>
	public class IContentCollectionP8 : CollectionBase
	{
		#region constants
		#endregion

		#region fields
		#endregion

		#region events
		#endregion

		#region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		public IContentCollectionP8(){}
		/// <summary>
		/// Instantiates the collection pre-loaded with the provided
		/// array.
		/// </summary>
		/// <param name="files">The array of IFile objects to be loaded into
		/// the collection.</param>
		public IContentCollectionP8(ContentP8[] files)
		{
			base.InnerList.AddRange(files);
		}
		#endregion

		#region properties
		#endregion

		#region methods
		/// <summary>
		/// Indexer. Returns the IFile object at a provided location
		/// in the list.
		/// </summary>
		public ContentP8 this[int index] 
		{
			get
			{
				return base.List[index] as ContentP8; 
			} 
			set 
			{ 
				base.List[index] = value;
			}
		}
		/// <summary>
		/// Adds an IFile object to the collection.
		/// </summary>
		/// <param name="value">The IFile object to add to the collection.</param>
		public void Add(ContentP8 value)
		{ 
			if (value != null)
				base.List.Add(value);
		}
		/// <summary>
		/// Concatenates a list of IFile objects to the collection.
		/// </summary>
		/// <param name="list">The list of IFile objects to append to this
		/// collection.</param>
		public void AddRange(IContentCollectionP8 list)
		{
			if (list != null && list.Count > 0)
				base.InnerList.AddRange(list);
		}
		/// <summary>
		/// Copies the entire collection to a compatible one-dimensional IFile[], 
		/// starting at the specified index of the target array. 
		/// </summary>
		/// <param name="array">The one-dimensional IFile[] that is the destination 
		/// of the elements copied from collection. The IFile[] must have zero-based 
		/// indexing.</param>
		/// <param name="index">The zero-based index at which the copying begins.</param>
		public void CopyTo(ContentP8[] array, int index)
		{
			((IContentCollectionP8)this).CopyTo(array, index);
		}
		/// <summary>
		/// Determines whether an element is in the collection.
		/// </summary>
		/// <param name="value">The IFile object to locate in the ArrayList.</param>
		/// <returns>true if item is found in the collection; otherwise, false.</returns>
		public bool Contains(ContentP8 value) 
		{
			return ((IList)this).Contains((object)value);
		}
		/// <summary>
		/// Inserts an element into the collection at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which value should be inserted.</param>
		/// <param name="value">The IFile object to insert.</param>
		public void Insert(int index, ContentP8 value) 
		{
			((IList)this).Insert(index, (object)value);
		}
		/// <summary>
		/// Removes the first occurrence of a specific object from the collection.
		/// </summary>
		/// <param name="value">The IFile object to remove from the ArrayList.</param>
		public void Remove(IFile value) 
		{
			((IList)this).Remove((object)value);
		}
		/// <summary>
		/// Returns the zero-based index of the first occurrence of a value in the 
		/// collection or in a portion of it.
		/// </summary>
		/// <param name="value">The Object to locate in the collection.</param>
		/// <returns>The zero-based index of the first occurrence of value 
		/// within the entire collection, if found; otherwise, -1.</returns>
		public int IndexOf(IFile value) 
		{
			return ((IList)this).IndexOf((object)value);
		}
		#endregion

		#region event handlers
		#endregion
	}
}