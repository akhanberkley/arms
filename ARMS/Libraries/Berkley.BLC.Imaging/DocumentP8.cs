/*===========================================================================
  File:     DocumentP8.cs
  
  Summary:  Abstract base class for classes that implement the IDocument
			interface. 
			
  Classes:  DocumentP8
  
  Enums:	None
  
  Origin:   WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using Berkley.BLC.Entities;

namespace Berkley.Imaging
{
	/// <summary>
	/// Abstract base class for classes that implement the IDocument
	/// interface. 
	/// </summary>
	public class DocumentP8 //: IDocumentP8
	{
		#region fields
		/// <summary>
		/// A list of the contentList associated with this document.
		/// </summary>
		private IContentCollectionP8 contentList;
		/// <summary>
		/// A list of the indices associated with this document.
		/// </summary>
		private IPropertyCollectionP8 properties;
		/// <summary>
		/// The partially unique id of this document. The id is unique to 
		/// a given imaging system, a single id may conflict between
		/// systems.
		/// </summary>
		private string id;
		/// <summary>
		/// The company to whom the document belongs.
		/// </summary>
		private Company company;
		/// <summary>
		/// The type of document.
		/// </summary>
		private DocumentType type;
        /// <summary>
        /// The file name of the document.
        /// </summary>
        private string fileName;
		/// <summary>
		/// The MIME type of the document.
		/// </summary>
		private string mimeType;
		/// <summary>
		/// The file extension of the document.
		/// </summary>
		private string extension;
        private string _docNumber = String.Empty;
        private string _policyNumber = String.Empty;
        private string _clientID = String.Empty;
        private string _accountID = String.Empty;
        private string _policySymbol = String.Empty;
        private string _docType = String.Empty;
        private string _entryDate = String.Empty;
        private string _docRemarks = String.Empty;
        private int _pageCount = 0;
        /// <summary>
        /// The required DocumentClass for this P8 document.
        /// </summary>
        private string docClass = String.Empty;
        /// <summary>
        /// Folder path (optional).
        /// </summary>
        private string folderPath = String.Empty;
        #endregion

		#region constructors
		#endregion

		#region IDocument Members
		/// <summary>
		/// A list of the contentList associated with this document.
		/// </summary>
		public virtual IContentCollectionP8 ContentList
		{
			get
			{
				if (contentList == null)
					contentList = new IContentCollectionP8();

				return contentList;
			}
            set
            {
                contentList = value;
            }
        }
		/// <summary>
		/// A list of the indices associated with this document.
		/// </summary>
		public virtual IPropertyCollectionP8 Properties
		{
			get
			{
				if (properties == null)
					properties = new IPropertyCollectionP8();

				return properties;
			}
            set
            {
                properties = value;
            }
        }
		/// <summary>
		/// The partially unique id of this document. The id is unique to 
		/// a given imaging system, a single id may conflict between
		/// systems.
		/// </summary>
		public virtual string Id
		{
			get
			{
				return this.id;
			}
			set
			{
				this.id = value;
			}
		}
		/// <summary>
		/// The company to whom the document belongs.
		/// </summary>
		public virtual Company Company
		{
			get
			{
				return this.company;
			}
			set
			{
				this.company = value;
			}
		}
		/// <summary>
		/// The type of document.
		/// </summary>
		public virtual DocumentType Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}
        /// <summary>
        /// The file name of the document.
        /// </summary>
        public virtual string FileName
        {
            get
            {
                return this.fileName;
            }
            set
            {
                this.fileName = value;
            }
        }
		/// <summary>
		/// The MIME type of the document.
		/// </summary>
		public virtual string MimeType
		{
			get
			{
				return this.mimeType;
			}
			set
			{
				this.mimeType = value;
			}
		}
		/// <summary>
		/// The file extension of the document.
		/// </summary>
		public virtual string Extension
		{
			get
			{
				return this.extension;
			}
			set
			{
				this.extension = value;
			}
		}
        /// <summary>
        /// Gets or sets the Doc Number.
        /// </summary>
        public string DocNumber
        {
            get { return _docNumber; }
            set
            {
                _docNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Number.
        /// </summary>
        public string PolicyNumber
        {
            get { return _policyNumber; }
            set
            {
                _policyNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the Client ID.
        /// </summary>
        public string ClientID
        {
            get { return _clientID; }
            set
            {
                _clientID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Account ID.
        /// </summary>
        public string AccountID
        {
            get { return _accountID; }
            set
            {
                _accountID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Policy Symbol.
        /// </summary>
        public string PolicySymbol
        {
            get { return _policySymbol; }
            set
            {
                _policySymbol = value;
            }
        }

        /// <summary>
        /// Gets or sets the Doc Type.
        /// </summary>
        public string DocType
        {
            get { return _docType; }
            set
            {
                _docType = value;
            }
        }

        /// <summary>
        /// Gets or sets the Entry Date.
        /// </summary>
        public string EntryDate
        {
            get { return _entryDate; }
            set
            {
                _entryDate = value;
            }
        }

        /// <summary>
        /// Gets or sets the Doc Remarks.
        /// </summary>
        public string DocRemarks
        {
            get { return _docRemarks; }
            set
            {
                _docRemarks = value;
            }
        }

        /// <summary>
        /// Gets or sets the Page Count.
        /// </summary>
        public int PageCount
        {
            get { return _pageCount; }
            set
            {
                _pageCount = value;
            }
        }

        /// <summary>
        /// Required document class
        /// </summary>
        public string DocClass
        {
            get
            {
                return this.docClass;
            }
            set
            {
                this.docClass = value;
            }
        }

        /// <summary>
        /// folderPath (of what?) 
        /// </summary>
        public string FolderPath
        {
            get
            {
                return this.folderPath;
            }
            set
            {
                this.folderPath = value;
            }
        }
        #endregion
	}
}