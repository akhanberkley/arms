/*===========================================================================
  File:			IIndex.cs
  
  Summary:		Serves as a typed collection of IIndex objects. This class 
				can be killed as soon as Microsoft releases Generics (and 
				the calling code is suitably refactored).
			
  Classes:		IIndexCollection
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.Collections;

namespace Berkley.Imaging
{
	/// <summary>
	/// Serves as a typed collection of IIndex objects. This class can be killed
	/// as soon as Microsoft releases Generics (and the calling code is 
	/// refactored).
	/// </summary>
	public class IIndexCollection : CollectionBase
	{
		#region constants
		#endregion

		#region fields
		#endregion

		#region events
		#endregion

		#region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		public IIndexCollection(){}
		/// <summary>
		/// Instantiates the collection pre-loaded with the provided
		/// array.
		/// </summary>
		/// <param name="indexes">The array of IIndex objects to be loaded into
		/// the collection.</param>
		public IIndexCollection(IIndex[] indexes)
		{
			base.InnerList.AddRange(indexes);
		}
		#endregion

		#region properties
		#endregion

		#region methods
		/// <summary>
		/// Indexer. Returns the IIndex object at a provided location
		/// in the list.
		/// </summary>
		public IIndex this[int index] 
		{
			get
			{
				return base.List[index] as IIndex; 
			} 
			set 
			{ 
				base.List[index] = value;
			}
		}
		/// <summary>
		/// Indexer. Returns the IIndex object matching the provided
		/// index name.
		/// </summary>
		public IIndex this[string indexName] 
		{
			get
			{
				int i = IndexOf(indexName);
				if (i >= 0)
					return this[i];
				else
					return null;
			} 
			set 
			{ 
				int i = IndexOf(indexName);
				if (i >= 0)
					base.List[i] = value;
			}
		}
		/// <summary>
		/// Adds an IIndex object to the collection.
		/// </summary>
		/// <param name="value">The IIndex object to add to the collection.</param>
		public void Add(IIndex value)
		{ 
			if (value != null)
				base.List.Add(value);
		}
		/// <summary>
		/// Concatenates a list of IIndex objects to the collection.
		/// </summary>
		/// <param name="list">The list of IIndex objects to append to this
		/// collection.</param>
		public void AddRange(IIndexCollection list)
		{
			if (list != null && list.Count > 0)
				base.InnerList.AddRange(list);
		}
		/// <summary>
		/// Copies the entire collection to a compatible one-dimensional IIndex[], 
		/// starting at the specified index of the target array. 
		/// </summary>
		/// <param name="array">The one-dimensional IIndex[] that is the destination 
		/// of the elements copied from collection. The IIndex[] must have zero-based 
		/// indexing.</param>
		/// <param name="index">The zero-based index at which the copying begins.</param>
		public void CopyTo(IIndex[] array, int index)
		{
			((ICollection)this).CopyTo(array, index);
		}
		/// <summary>
		/// Determines whether an element is in the collection.
		/// </summary>
		/// <param name="value">The IIndex object to locate in the collection.</param>
		/// <returns>true if item is found in the collection; otherwise, false.</returns>
		public bool Contains(IIndex value) 
		{
			return this.IndexOf(value) >= 0;
		}
		/// <summary>
		/// Determines whether an element is in the collection.
		/// </summary>
		/// <param name="indexName">The index name to locate in the collection.</param>
		/// <returns>true if item is found in the collection; otherwise, false.</returns>
		public bool Contains(string indexName) 
		{
			return this.IndexOf(indexName) >= 0;
		}
		/// <summary>
		/// Inserts an element into the collection at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which value should be inserted.</param>
		/// <param name="value">The IIndex object to insert.</param>
		public void Insert(int index, IIndex value) 
		{
			((IList)this).Insert(index, (object)value);
		}
		/// <summary>
		/// Removes the first occurrence of a specific object from the collection.
		/// </summary>
		/// <param name="value">The IIndex object to remove from the ArrayList.</param>
		public void Remove(IIndex value) 
		{
			((IList)this).Remove((object)value);
		}
		/// <summary>
		/// Returns the zero-based index of the first occurrence of a value in the 
		/// collection or in a portion of it.
		/// </summary>
		/// <param name="value">The Object to locate in the collection.</param>
		/// <returns>The zero-based index of the first occurrence of value 
		/// within the entire collection, if found; otherwise, -1.</returns>
		public int IndexOf(IIndex value) 
		{
			int result = -1;

			if (value != null)
				result = IndexOf(value.Name);

			return result;
		}
		/// <summary>
		/// Returns the zero-based index of the first occurrence of a value in the 
		/// collection or in a portion of it.
		/// </summary>
		/// <param name="indexName">The name of the index to locate in the collection.</param>
		/// <returns>The zero-based index of the first occurrence of value 
		/// within the entire collection, if found; otherwise, -1.</returns>
		public int IndexOf(string indexName) 
		{
			int result = -1;

			for(int i=0; i<this.Count && result < 0; i++)
			{
				if (this[i].Name.Equals(indexName))
					result = i;
			}

			return result;
		}
		#endregion

		#region event handlers
		#endregion
	}
}