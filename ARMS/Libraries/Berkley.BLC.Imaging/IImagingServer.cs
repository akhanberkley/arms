/*===========================================================================
  File:			IImagingServer.cs
  
  Summary:		Serves as a contract of behavior for all imaging servers.
			
  Classes:		None
  
  Enums:		None
  
  Interfaces:	IImagingServer
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using Berkley.BLC.Entities;

namespace Berkley.Imaging
{
	/// <summary>
	/// Serves as a contract of behavior for all imaging servers.
	/// </summary>
	public interface IImagingServer
	{
		/// <summary>
		/// Retrieves an existing document (by Id) from the imaging 
		/// server.
		/// </summary>
		/// <param name="id">The Id of the document.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
		/// <returns>A document. If none is found, null is returned.</returns>
        IDocument RetrieveDocument(string id, string fileNetUsername, string fileNetPassword);
        /// <summary>
        /// Retrieves an existing document (by Id) from the imaging 
        /// server.
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <param name="page">The page to return.</param>
        /// <param name="attempts">The number of attempts to retrieve the document.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        /// <returns>A document. If none is found, null is returned.</returns>
        IDocument RetrieveDocument(string id, int page, int attempts, string fileNetUsername, string fileNetPassword);
        /// <summary>
        /// Retrieves the page count index;
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        /// <returns>A document. If none is found, null is returned.</returns>
        Int32 RetrievePageCount(string id, string fileNetUsername, string fileNetPassword);
        /// <summary>
        /// Retrieves a list of indexes (by the filter passed) from the imaging server.
        /// </summary>
        /// <param name="document">The document to get the company entity from.</param>
        /// <param name="policyNumbers">The policy number(s) filter for the list of indexes.</param>
        /// <param name="clientID">The client id filter for the list of indexes.</param>
        /// <param name="docID">The doc id filter for the list of indexes.</param>
        /// <param name="docTypes">The doc type filter for the list of indexes.</param>
        /// <param name="entryStartDate">The entry start date filter for the list of indexes.</param>
        /// <param name="entryEndDate">The entry end date filter for the list of indexes.</param>
        /// <param name="attempts">The number of attempts at retrieving the list of indexes.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        /// <returns>A document. If none is found, null is returned.</returns>
        IDocument[] RetrieveIndexes(IDocument document, string[] policyNumbers, string clientID, string docID, string[] docTypes, DateTime entryStartDate, DateTime entryEndDate, int attempts, string fileNetUsername, string fileNetPassword);
        /// <summary>
        /// Removes an existing document (by Id) from the imaging server.
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        /// <returns>A completed note. If none is found, null is returned.</returns>
        void RemoveDocument(string id, string fileNetUsername, string fileNetPassword);
        /// <summary>
        /// Adds an new document to the imaging system.
        /// </summary>
        /// <param name="document">The document to add.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        /// <returns>The document Id.</returns>
		string Add(IDocument document, string fileNetUsername, string fileNetPassword);
		/// <summary>
		/// Updates an existing document in the imaging system.
		/// </summary>
		/// <param name="document">The document to update.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPassword">The filenet password.</param>
        void Update(IDocument document, string fileNetUsername, string fileNetPassword);
		/// <summary>
		/// Creates a new, empty document.
		/// </summary>
		/// <returns>A new, empty document.</returns>
		IDocument CreateDocument();
		/// <summary>
		/// Creates a new, empty file.
		/// </summary>
		/// <returns>A new, empty file.</returns>
		IFile CreateFile();
		/// <summary>
		/// Creates a new, empty index.
		/// </summary>
		/// <returns>A new, empty index.</returns>
		IIndex CreateIndex();
		/// <summary>
		/// Specifies the configuration to which the instance is targeted.
		/// </summary>
		Berkley.BLC.Core.ConfigurationType Configuration{get;set;}
        /// <summary>
        /// Specifies the company.
        /// </summary>
        Berkley.BLC.Entities.Company ImagingCompany { get; set; }
        /// <summary>
        /// Sets items required for P8 FileNet call for this company.
        /// </summary>
        /// <param name="Username">This company's FileNet P8 username</param>
        /// <param name="Password">This company's FileNet P8 password</param>
        /// <param name="DocClass">This company's FileNet P8 Doc Class</param>
        void SetUserP8(string fnUsernameP8, string fnPasswordP8, string fnDocClassP8);   //WA-703 Doug Bruce 2015-05-21 - for backwards compatibility - to get the P8 doc number
    }
}