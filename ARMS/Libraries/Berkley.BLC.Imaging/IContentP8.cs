/*===========================================================================
  File:			IContentP8.cs
  
  Summary:		Serves as a contract of behavior for all imaging files.
			
  Classes:		None
  
  Enums:		None
  
  Interfaces:	IContentP8
  
  Origin:   	WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging
{
	/// <summary>
	/// Serves as a contract of behavior for all imaging files.
	/// </summary>
	public interface IContentP8
	{
		/// <summary>
		/// The actual, imagable document.
		/// </summary>
		System.IO.Stream Content{get;set;}
	}
}