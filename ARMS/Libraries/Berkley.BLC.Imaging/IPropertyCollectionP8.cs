/*===========================================================================
  File:			IPropertyCollectionP8.cs
  
  Summary:		Serves as a typed collection of IPropertyP8 objects. This class 
				can be killed as soon as Microsoft releases Generics (and 
				the calling code is suitably refactored).
			
  Classes:		IPropertyCollectionP8
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:   	WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.Collections;

namespace Berkley.Imaging
{
	/// <summary>
	/// Serves as a typed collection of IPropertyP8 objects. This class can be killed
	/// as soon as Microsoft releases Generics (and the calling code is 
	/// refactored).
	/// </summary>
	public class IPropertyCollectionP8 : CollectionBase
	{
		#region constants
		#endregion

		#region fields
		#endregion

		#region events
		#endregion

		#region constructors
		/// <summary>
		/// Default constructor.
		/// </summary>
		public IPropertyCollectionP8(){}
		/// <summary>
		/// Instantiates the collection pre-loaded with the provided
		/// array.
		/// </summary>
		/// <param name="properties">The array of IPropertyP8 objects to be loaded into
		/// the collection.</param>
		public IPropertyCollectionP8(PropertyP8[] properties)
		{
			base.InnerList.AddRange(properties);
		}
		#endregion

		#region properties
		#endregion

		#region methods
		/// <summary>
		/// propertyer. Returns the IPropertyP8 object at a provided location
		/// in the list.
		/// </summary>
        public PropertyP8 this[int index] 
		{
			get
			{
				return base.List[index] as PropertyP8; 
			} 
			set 
			{ 
				base.List[index] = value;
			}
		}
		/// <summary>
		/// propertyer. Returns the IPropertyP8 object matching the provided
		/// property name.
		/// </summary>
		public PropertyP8 this[string propertyName] 
		{
			get
			{
				int i = propertyOf(propertyName);
				if (i >= 0)
					return this[i];
				else
					return null;
			} 
			set 
			{ 
				int i = propertyOf(propertyName);
				if (i >= 0)
					base.List[i] = value;
			}
		}
		/// <summary>
		/// Adds an IPropertyP8 object to the collection.
		/// </summary>
		/// <param name="value">The IPropertyP8 object to add to the collection.</param>
		public void Add(PropertyP8 value)
		{ 
			if (value != null)
				base.List.Add(value);
		}
		/// <summary>
		/// Concatenates a list of IPropertyP8 objects to the collection.
		/// </summary>
		/// <param name="list">The list of IPropertyP8 objects to append to this
		/// collection.</param>
		public void AddRange(IPropertyCollectionP8 list)
		{
			if (list != null && list.Count > 0)
				base.InnerList.AddRange(list);
		}
		/// <summary>
		/// Copies the entire collection to a compatible one-dimensional IPropertyP8[], 
		/// starting at the specified property of the target array. 
		/// </summary>
		/// <param name="array">The one-dimensional IPropertyP8[] that is the destination 
		/// of the elements copied from collection. The IPropertyP8[] must have zero-based 
		/// propertying.</param>
		/// <param name="property">The zero-based property at which the copying begins.</param>
		public void CopyTo(PropertyP8[] array, int index)
		{
			((IPropertyCollectionP8)this).CopyTo(array, index);
		}
		/// <summary>
		/// Determines whether an element is in the collection.
		/// </summary>
		/// <param name="value">The IPropertyP8 object to locate in the collection.</param>
		/// <returns>true if item is found in the collection; otherwise, false.</returns>
		public bool Contains(PropertyP8 value) 
		{
			return this.propertyOf(value) >= 0;
		}
		/// <summary>
		/// Determines whether an element is in the collection.
		/// </summary>
		/// <param name="propertyName">The property name to locate in the collection.</param>
		/// <returns>true if item is found in the collection; otherwise, false.</returns>
		public bool Contains(string propertyName) 
		{
			return this.propertyOf(propertyName) >= 0;
		}
		/// <summary>
		/// Inserts an element into the collection at the specified property.
		/// </summary>
		/// <param name="index">The zero-based property at which value should be inserted.</param>
		/// <param name="value">The IPropertyP8 object to insert.</param>
		public void Insert(int index, PropertyP8 value) 
		{
			((IList)this).Insert(index, (object)value);
		}
		/// <summary>
		/// Removes the first occurrence of a specific object from the collection.
		/// </summary>
		/// <param name="value">The IPropertyP8 object to remove from the ArrayList.</param>
		public void Remove(PropertyP8 value) 
		{
			((IList)this).Remove((object)value);
		}
		/// <summary>
		/// Returns the zero-based property of the first occurrence of a value in the 
		/// collection or in a portion of it.
		/// </summary>
		/// <param name="value">The Object to locate in the collection.</param>
		/// <returns>The zero-based property of the first occurrence of value 
		/// within the entire collection, if found; otherwise, -1.</returns>
		public int propertyOf(PropertyP8 value) 
		{
			int result = -1;

			if (value != null)
				result = propertyOf(value.Name);

			return result;
		}
		/// <summary>
		/// Returns the zero-based property of the first occurrence of a value in the 
		/// collection or in a portion of it.
		/// </summary>
		/// <param name="propertyName">The name of the property to locate in the collection.</param>
		/// <returns>The zero-based property of the first occurrence of value 
		/// within the entire collection, if found; otherwise, -1.</returns>
		public int propertyOf(string propertyName) 
		{
			int result = -1;

			for(int i=0; i<this.Count && result < 0; i++)
			{
				if (this[i].Name.Equals(propertyName))
					result = i;
			}

			return result;
		}
		#endregion

		#region event handlers
		#endregion
	}
}