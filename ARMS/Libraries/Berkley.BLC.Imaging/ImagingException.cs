/*===========================================================================
  File:			IIndex.cs
  
  Summary:		Serves as a typed collection of IIndex objects. This class 
				can be killed as soon as Microsoft releases Generics (and 
				the calling code is suitably refactored).
			
  Classes:		IIndexCollection
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace Berkley.Imaging
{
	/// <summary>
	/// Abstract. Defines a category of imaging exceptions.
	/// </summary>
	[Serializable()]
	public abstract class ImagingException : Exception
	{
		#region constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="ImagingException"/> 
		/// class.
		/// </summary>
		protected ImagingException() : base(Berkley.Imaging.Properties.Resources.MissingFileExceptionMessage)
		{
			// stub. use base implmentation.
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="ImagingException"/>
		/// class with a specified error message.
		/// </summary>
		/// <param name="message">The message that describes the error.</param>
		protected ImagingException(string message): base(message) 
		{
			// stub. use base implmentation.
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="ImagingException"/> 
		/// class with a specified error message and a reference to the inner 
		/// exception that is the cause of this exception.
		/// </summary>
		/// <param name="message">The error message that explains the reason for 
		/// the exception.</param>
		/// <param name="innerException">The exception that is the cause of the 
		/// current exception. If the innerException parameter is not a null 
		/// reference, the current exception is raised in a catch block that 
		/// handles the inner exception.</param>
		protected ImagingException(string message, Exception innerException): base (message, innerException)
		{
			// stub. use base implmentation.
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="ImagingException"/>
		/// class with serialized data.
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> that holds 
		/// the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="StreamingContext"/> that 
		/// contains contextual information about the source or destination.</param>
		protected ImagingException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// stub. use base implmentation.
		}
		#endregion

		#region methods
		#endregion
	}
}
