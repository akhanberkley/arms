/*===========================================================================
  File:     DocumentType.cs
  
  Summary:  The various types of documents that are imaged.
			
  Classes:  None
  
  Enums:	DocumentType
  
  Origin:   Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging
{
	/// <summary>
	/// The various types of documents that are imaged.
	/// </summary>
	public enum DocumentType
	{
		/// <summary>
		/// An underwriting document.
		/// </summary>
		Underwriting,
		/// <summary>
		/// A claims document.
		/// </summary>
		Claims,
		/// <summary>
		/// An unspecified document.
		/// </summary>
		Other
	}
}
