/*===========================================================================
  File:			IDocumentP8.cs
  
  Summary:		Serves as a contract of behavior for all imaging documents.
			
  Classes:		None
  
  Enums:		None
  
  Interfaces:	IDocumentP8
  
  Origin:   	WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using Berkley.BLC.Entities;

namespace Berkley.Imaging
{
	/// <summary>
	/// Serves as a contract of behavior for all imaging documents.
	/// </summary>
	public interface IDocumentP8
	{
		/// <summary>
		/// A list of the files associated with this document.
		/// </summary>
		IContentCollectionP8 ContentList{get;}
		/// <summary>
		/// A list of the indices associated with this document.
		/// </summary>
		IPropertyCollectionP8 Properties{get;}
		/// <summary>
		/// The partially unique id of this document. The id is unique to 
		/// a given imaging system, a single id may conflict between
		/// systems.
		/// </summary>
		string Id{get;set;}
		/// <summary>
		/// The company to whom the document belongs.
		/// </summary>
		Company Company{get;set;}
		/// <summary>
		/// The type of document.
		/// </summary>
		DocumentType Type{get;set;}
        /// <summary>
        /// The file name of the document.
        /// </summary>
        string FileName { get;set;}
		/// <summary>
		/// The MIME type of the document.
		/// </summary>
		string MimeType{get;set;}
        /// <summary>
        /// The Extension of the document.
        /// </summary>
        string Extension { get;set;}
        /// <summary>
        /// The doc number of the document.
        /// </summary>
        string DocNumber { get;set;}
		/// <summary>
		/// The policy number of the document.
		/// </summary>
		string PolicyNumber{get;set;}
        /// <summary>
        /// The policy symbol of the document.
        /// </summary>
        string PolicySymbol { get;set;}
        /// <summary>
        /// The doc type of the document.
        /// </summary>
        string DocType { get;set;}
        /// <summary>
        /// The entry date of the document.
        /// </summary>
        string EntryDate { get;set;}
        /// <summary>
        /// The doc remarks of the document.
        /// </summary>
        string DocRemarks { get;set;}
        /// <summary>
        /// The page count of the document.
        /// </summary>
        int PageCount { get;set;}
	}
}