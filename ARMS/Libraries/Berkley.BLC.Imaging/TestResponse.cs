﻿using System;

namespace Berkley.Imaging
{
    public class TestResponse
    {
        public TestResponse()
        {
        }

        public TestResponse(string description,
            string command,
            string contentType)
        {
            IsSuccessful = false;
            Command = command;
            Description = description;
            Message = "Failed";
            InternalResponse = null;
            ContentType = contentType;
            ResponseType = null;
            ResponseByteArray = null;
            ResponseStream = null;
            Error = null;
        }

        public bool IsSuccessful;
        public string Command;
        public string Description;
        public string Message;
        public string InternalResponse;
        public string ContentType;
        public string ResponseType;
        public byte[] ResponseByteArray;
        public System.IO.Stream ResponseStream;
        public Exception Error;
    }
}
