/*===========================================================================
  File:			PropertyP8.cs
  
  Summary:		Abstract base class for classes that implement the IIndex
				interface. 
			
  Classes:		PropertyP8
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:   	WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging
{
	/// <summary>
	/// Abstract base class for classes that implement the IIndex
	/// interface. 
	/// </summary>
	public class PropertyP8 //: IPropertyP8
	{
		#region fields
		/// <summary>
		/// The unique name of the index.
		/// </summary>
		private string name;
		/// <summary>
		/// The value of the index.
		/// </summary>
		private string value;
		#endregion

		#region properties
		/// <summary>
		/// The unique name of the index.
		/// </summary>
		public virtual string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}
		/// <summary>
		/// The value of the index.
		/// </summary>
		public virtual string Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}
		#endregion

        #region methods
        /// <summary>
        /// Import FileNet object contents into this one.
        /// </summary>
        public virtual void Init()
        {
            this.Name = "";
            this.Value = "";
        }
        #endregion
    }
}