/*===========================================================================
  File:			ServerFactoryP8.cs
  
  Summary:		Controls the creation of a connection with the specified 
				field imaging system. Each imaging system has its own 
				requirements for connectivity. The purpose of this class is 
				to encapsulate the variances in connectivity.
			
  Classes:		ServerFactoryP8
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:   	WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Reflection;

using Berkley.BLC.Entities;
using Berkley.Imaging.Properties;

namespace Berkley.Imaging
{
	/// <summary>
	/// Controls the creation of a connection with the specified field
	/// imaging system. Each imaging system has its own requirements for 
	/// connectivity. The purpose of this class is to encapsulate the
	/// variances in connectivity.
	/// </summary>
	public sealed class ServerFactoryP8
	{
		#region fields
		#endregion

		#region inner class
		/// <summary>
		/// Provides a reference to a single Hashtable object that is
		/// directed at this assembly.
		/// </summary>
		private sealed class ServerFactorySingleton
		{
			/// <summary>
			/// The ResourceManager singleton.
			/// </summary>
			private static volatile Hashtable instance;
			/// <summary>
			/// Serves as a mutex to manage initialization of the singleton.
			/// </summary>
			private static object syncRoot = new Object();

			/// <summary>
			/// Private constructor to force singleton usage.
			/// </summary>
			private ServerFactorySingleton(){}

			#region properties
			/// <summary>
			/// A reference to the ResourceManager singleton
			/// </summary>
			public static Hashtable Instance
			{
				get
				{
					/*
					 * if the singleton has not been created..
					 */
					if (instance == null) 
					{
						/*
						 * block any other threads from creating the singleton.
						 */
						lock (syncRoot) 
						{
							/*
							 * create singleton 
							 */
							if (instance == null) 
							{
								/*
								 * iterate through all of the imaging server assemblies in the 
								 * configuration file.
								 */
								NameValueCollection servers = new Berkley.BLC.Core.BerkleyXmlKeyValueCollection(Settings.Default.ImageServerAssemblies).KeyValues;

								instance = new Hashtable(servers.Count);
								if (servers != null && servers.Count > 0)
								{
									foreach(string key in servers.AllKeys)
									{
										string assemblyName = servers[key];
										if (!instance.ContainsKey(assemblyName))
										{
											/*
											 * load the assembly
											 */
											Assembly assembly = Assembly.Load(assemblyName);
					
											/*
											 * look for a class that implements the IImagingServerP8
											 * interface. if one is found, add it to the singleton
											 * hashtable as an instantiated object.
											 */
											foreach(Type type in assembly.GetTypes())
											{
												if (typeof(IImagingServerP8).IsAssignableFrom(type))
												{
													instance.Add(assemblyName, Activator.CreateInstance(type));
													break;
												}
											}
										}

									}
								}
							}
						}
					}

					/*
					 * return the result
					 */
					return instance;
				}
			}
			#endregion

			#region methods
			/// <summary>
			/// Looks up an image server given the company provided.
			/// </summary>
			/// <param name="company">The company whose imaging server to 
			/// locate.</param>
			/// <returns>The company's imaging server if found. Otherwise, null.</returns>
			public static IImagingServerP8 FindServer(Company company)
			{
				IImagingServerP8 result = null;

				if (company != null)
				{
					/*
					 * find the key for the image server by cross-referencing the 
					 * company name. Using the name makes the configuration easy, but
					 * may not work since the name is not 100% static.
					 */
					NameValueCollection crossReference = new Berkley.BLC.Core.BerkleyXmlKeyValueCollection(Settings.Default.ImageServerAssemblies).KeyValues;

					if (crossReference != null)
					{
						string key = null;

						if (company.Abbreviation != null)
							key = crossReference[company.Abbreviation];

						/*
						 * if there was no match by name, try id.
						 */
						if (key == null || key.Length == 0)
							key = crossReference[company.ID.ToString()];

						/*
						 * if there was no match by id, try name.
						 */
						if (key == null || key.Length == 0 && company.Name != null)
							key = crossReference[company.Name];

						/*
						 * now, use the key to try and find the matching server
						 */
						if (key != null && Instance.ContainsKey(key))
							result = Instance[key] as IImagingServerP8;
					}
				}

				/*
				 * return the result
				 */
				return result;
			}
			public static IImagingServerP8 DefaultServer
			{
				get
				{
					IImagingServerP8 result = null;

					NameValueCollection crossReference = new Berkley.BLC.Core.BerkleyXmlKeyValueCollection(Settings.Default.ImageServerAssemblies).KeyValues;
					string key = crossReference["Default"];
					if (key != null && Instance.ContainsKey(key))
						result = Instance[key] as IImagingServerP8;

					return result;
				}
			}
			/// <summary>
			/// Builds the assembly path, allowing for both full and relative 
			/// paths.
			/// </summary>
			/// <param name="path">The path to format.</param>
			/// <returns>The full path to the the assembly.</returns>
			private static string FormatAssemblyPath(string path)
			{
				string combinedPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);
				return System.IO.Path.GetFullPath(combinedPath);
			}
			#endregion
		}
		#endregion

		#region constructors
		/// <summary>
		/// Private constructor to disallow instantiation.
		/// </summary>
		private ServerFactoryP8(){}
		#endregion

		#region methods
		/// <summary>
		/// Retrieves the <see cref="IImagingServerP8"/> for the specified
		/// company.
		/// </summary>
		/// <param name="company">The company that is used to determine the 
		/// appropriate imaging server.</param>
		/// <param name="configuration">The configuration that is to be accessed.</param>
		/// <returns>The <see cref="IImagingServerP8"/> for the specified company.</returns>
		public static IImagingServerP8 GetServer(Company company, Berkley.BLC.Core.ConfigurationType configuration)
		{
			IImagingServerP8 result = ServerFactorySingleton.FindServer(company);

			if (result == null)
				result = ServerFactorySingleton.DefaultServer;

			if (result != null)
				result.Configuration = configuration;

            if (result != null)
            {
                result.ImagingCompany = company;
            }

			return result;
		}
		#endregion
	}
}