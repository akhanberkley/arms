/*===========================================================================
  File:			IPropertyP8.cs
  
  Summary:		Serves as a contract of behavior for all imaging indexes.
			
  Classes:		None
  
  Enums:		None
  
  Interfaces:	IPropertyP8
  
  Origin:   	WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging
{
	/// <summary>
	/// Serves as a contract of behavior for all imaging indexes.
	/// </summary>
	public interface IPropertyP8
	{
		/// <summary>
		/// The name of the index.
		/// </summary>
		string Name{get;set;}
		/// <summary>
		/// The value of the index.
		/// </summary>
		string Value{get;set;}
	}
}