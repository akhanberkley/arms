/*===========================================================================
  File:     ContentP8.cs
  
  Summary:  Abstract base class for classes that implement the IFile
			interface. 
			
  Classes:  ContentP8
  
  Enums:	None
  
  Origin:   WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging
{
	/// <summary>
	/// Abstract base class for classes that implement the IFile
	/// interface. 
	/// </summary>
    public class ContentP8 //: IContentP8
    {
        #region fields
        private byte[] content;
        private string filename;
        private string mimetype;
        #endregion

        #region members
        /// <summary>
        /// The content that contains the file image.
        /// </summary>
        public virtual byte[] ContentBytes
        {
            get
            {
                return this.content;
            }
            set
            {
                this.content = value;
            }
        }

        /// <summary>
        /// The content that contains the file image.
        /// </summary>
        public virtual System.IO.Stream Content
        {
            get
            {
                if (this.content != null)
                {
                    System.IO.Stream stream = new System.IO.MemoryStream(this.content);
                    return stream;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                this.content = null;
                this.content = new byte[value.Length];
                value.Position = 0;
                value.Read(this.content, 0, (int)value.Length);

                //byte[] valueContent = new byte[value.Length];
                //value.Position = 0;
                //value.Read(valueContent, 0, (int)value.Length);
                //this.content = valueContent;
            }
        }

        /// <summary>
        /// The file name associated with this content.
        /// </summary>
        public virtual string FileName
        {
            get
            {
                return this.filename;
            }
            set
            {
                this.filename = value;
            }
        }

        /// <summary>
        /// The MIME type associated with this content.
        /// </summary>
        public virtual string MimeType
        {
            get
            {
                return this.mimetype;
            }
            set
            {
                this.mimetype = value;
            }
        }
        #endregion

        #region methods

        /// <summary>
        /// Export contents of this object to the base class FileNet understands
        /// </summary>
        public virtual void Init()
        {
            this.content = new byte[] { };
            this.filename = "";
            this.mimetype = "";
        }

        #endregion
    }
}
