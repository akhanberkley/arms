/*===========================================================================
  File:			IIndex.cs
  
  Summary:		Serves as a contract of behavior for all imaging indexes.
			
  Classes:		None
  
  Enums:		None
  
  Interfaces:	IIndex
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging
{
	/// <summary>
	/// Serves as a contract of behavior for all imaging indexes.
	/// </summary>
	public interface IIndex
	{
		/// <summary>
		/// The name of the index.
		/// </summary>
		string Name{get;set;}
		/// <summary>
		/// The value of the index.
		/// </summary>
		string Value{get;set;}
	}
}