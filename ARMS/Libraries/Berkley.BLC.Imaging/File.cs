/*===========================================================================
  File:     File.cs
  
  Summary:  Abstract base class for classes that implement the IFile
			interface. 
			
  Classes:  File
  
  Enums:	None
  
  Origin:   Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging
{
	/// <summary>
	/// Abstract base class for classes that implement the IFile
	/// interface. 
	/// </summary>
	public abstract class File : IFile
	{
		#region fields
		/// <summary>
		/// The stream that contains the file image.
		/// </summary>
		private System.IO.Stream stream;
		#endregion

		#region IFile Members
		/// <summary>
		/// The stream that contains the file image.
		/// </summary>
		public virtual System.IO.Stream Content
		{
			get
			{
				return this.stream;
			}
			set
			{
				this.stream = value;
			}
		}
		#endregion
	}
}
