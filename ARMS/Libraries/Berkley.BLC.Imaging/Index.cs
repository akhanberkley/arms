/*===========================================================================
  File:			Index.cs
  
  Summary:		Abstract base class for classes that implement the IIndex
				interface. 
			
  Classes:		Index
  
  Enums:		None
  
  Interfaces:	None
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging
{
	/// <summary>
	/// Abstract base class for classes that implement the IIndex
	/// interface. 
	/// </summary>
	public abstract class Index : IIndex
	{
		#region fields
		/// <summary>
		/// The unique name of the index.
		/// </summary>
		private string name;
		/// <summary>
		/// The value of the index.
		/// </summary>
		private string value;
		#endregion

		#region properties
		/// <summary>
		/// The unique name of the index.
		/// </summary>
		public virtual string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}
		/// <summary>
		/// The value of the index.
		/// </summary>
		public virtual string Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}
		#endregion
	}
}