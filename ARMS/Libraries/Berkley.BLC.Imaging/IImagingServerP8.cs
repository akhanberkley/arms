/*===========================================================================
  File:			IImagingServerP8.cs
  
  Summary:		Serves as a contract of behavior for all imaging servers.
			
  Classes:		None
  
  Enums:		None
  
  Interfaces:	IImagingServerP8
  
  Origin:   	WA-621 - ARMS FileNet P8 Upgrade (April, 2015)
  
=============================================================================
  Copyright (c) 2015 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;
using Berkley.BLC.Entities;
using System.Collections.Generic;

namespace Berkley.Imaging
{
	/// <summary>
	/// Serves as a contract of behavior for all imaging servers.
	/// </summary>
	public interface IImagingServerP8
	{
		/// <summary>
		/// Retrieves an existing document (by Id) from the imaging 
		/// server.
		/// </summary>
		/// <param name="id">The Id of the document.</param>
		/// <returns>A document. If none is found, null is returned.</returns>
        DocumentP8 RetrieveDocument(string id);
        /// <summary>
        /// Retrieves an existing document (by Id) from the imaging 
        /// server.
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <param name="page">The page to return.</param>
        /// <param name="attempts">The number of attempts to retrieve the document.</param>
        /// <param name="overrideDocClass">DocClass to override the one from the company object.</param>
        /// <returns>A document. If none is found, null is returned.</returns>
        DocumentP8 RetrieveDocument(string id, int page, int attempts);
        /// <summary>
        /// Retrieves the page count index;
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <returns>A document. If none is found, null is returned.</returns>
        Int32 RetrievePageCount(string id);
        /// <summary>
        /// Retrieves a list of indexes (by the filter passed) from the imaging server.
        /// </summary>
        /// <param name="document">The document to get the company entity from.</param>
        /// <param name="policyNumbers">The policy number(s) filter for the list of indexes.</param>
        /// <param name="clientID">The client id filter for the list of indexes.</param>
        /// <param name="docID">The doc id filter for the list of indexes.</param>
        /// <param name="docTypes">The doc type filter for the list of indexes.</param>
        /// <param name="entryStartDate">The entry start date filter for the list of indexes.</param>
        /// <param name="entryEndDate">The entry end date filter for the list of indexes.</param>
        /// <param name="attempts">The number of attempts at retrieving the list of indexes.</param>
        /// <param name="fileNetUsername">The filenet username.</param>
        /// <param name="fileNetPolicyLike">True if looking for partial policy numbers.</param>
        /// <returns>A document. If none is found, null is returned.</returns>
        List<DocumentP8> RetrieveIndexes(DocumentP8 document, string[] policyNumbers, string clientID, string docID, string[] docTypes, DateTime entryStartDate, DateTime entryEndDate, int attempts, bool fileNetPolicyLike);
        /// <summary>
        /// Removes an existing document (by Id) from the imaging server.
        /// </summary>
        /// <param name="id">The Id of the document.</param>
        /// <returns>A completed note. If none is found, null is returned.</returns>
        void RemoveDocument(string id);
        /// <summary>
        /// Adds an new document to the imaging system.
        /// </summary>
        /// <param name="document">The document to add.</param>
        /// <returns>The document Id.</returns>
		string Add(DocumentP8 document);
		/// <summary>
		/// Updates an existing document in the imaging system.
		/// </summary>
		/// <param name="document">The document to update.</param>
        void UpdateProperties(string docId, List<PropertyP8> updatedProperties);
		/// <summary>
		/// Creates a new, empty document.
		/// </summary>
		/// <returns>A new, empty document.</returns>
		DocumentP8 CreateDocument();
		/// <summary>
		/// Creates a new, empty file.
		/// </summary>
		/// <returns>A new, empty file.</returns>
		ContentP8 CreateFile();
		/// <summary>
		/// Creates a new, empty index.
		/// </summary>
		/// <returns>A new, empty index.</returns>
		PropertyP8 CreateIndex();
		/// <summary>
		/// Specifies the configuration to which the instance is targeted.
		/// </summary>
		Berkley.BLC.Core.ConfigurationType Configuration{get;set;}
        /// <summary>
        /// Specifies the company.
        /// </summary>
        Berkley.BLC.Entities.Company ImagingCompany { get; set; }
        /// <summary>
        /// Sets items required for FileNet call for this company.
        /// </summary>
        /// <param name="Username">This company's FileNet IS username</param>
        /// <param name="Password">This company's FileNet IS password</param>
        /// <param name="DocClass">This company's FileNet IS Doc Class</param>
        void SetUser(string fnUsername, string fnPassword, string fnDocClass, string fnCompanyNo);
        /// <summary>
        /// Sets the Uri automatically based on environment and web.config settings.
        /// </summary>
        //WA-905 Doug Bruce 2015-09-22 - Dynamic endpoint support
        void SetEndpointName();   //WA-905 Doug Bruce 2015-09-22
        /// <summary>
        /// Specifies the endpoint name of the configuration block in web.config.
        /// </summary>
        //WA-905 Doug Bruce 2015-09-22 - Dynamic endpoint support
        string EndpointName { get; set; } //WA-905 Doug Bruce 2015-09-17
        string FileNetDocClass { get; set; }
        string FileNetCompanyNo { get; set; }   //WA-3060 Doug Bruce 2015-10-06
    }
}