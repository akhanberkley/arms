/*===========================================================================
  File:			IFile.cs
  
  Summary:		Serves as a contract of behavior for all imaging files.
			
  Classes:		None
  
  Enums:		None
  
  Interfaces:	IFile
  
  Origin:		Premium Audit Tracking System (October, 2005)
  
=============================================================================
  Copyright (c) 2005 Berkley Technology Services, LLC. All rights reserved.
=============================================================================*/

using System;

namespace Berkley.Imaging
{
	/// <summary>
	/// Serves as a contract of behavior for all imaging files.
	/// </summary>
	public interface IFile
	{
		/// <summary>
		/// The actual, imagable document.
		/// </summary>
		System.IO.Stream Content{get;set;}
	}
}