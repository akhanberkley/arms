﻿/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/jqueryui/jqueryui.d.ts" />
/// <reference path="../scripts/typings/moment/moment.d.ts" />
/// <reference path="../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../scripts/typings/toastr/toastr.d.ts" />
/// <reference path="../scripts/typings/angular-ui-bootstrap/angular-ui-bootstrap.d.ts" />
/// <reference path="../scripts/typings/google.analytics/ga.d.ts" />
/// <reference path="../scripts/typings/angular-ui/angular-ui-router.d.ts" />
module arms {
    'use strict';

    //Setup toastr settings
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-offset",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": 250,
        "hideDuration": 500,
        "timeOut": 3000,
        "extendedTimeOut": 1000,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    //Setup angular
    export var app = angular.module('arms', ['ui.router', 'ngSanitize', 'ui.bootstrap.modal', 'ui.bootstrap.tooltip']);

    app.run(function ($rootScope: ng.IRootScopeService, $location: ng.ILocationService, $state: ng.ui.IStateService, AlertService: AlertService, UserService: UserService) {
        //Google Analytics Setup
        $rootScope.$on('$stateChangeSuccess',(event, toState, toParams, fromState, fromParams) => {
            if (toState.analytics) {
                ga('send', 'pageview', { page: $location.path(), title: toState.analytics });
            }
        });

        //state failure (didn't have access, etc)
        $rootScope.$on('$stateChangeError',(event, toState, toParams, fromState, fromParams, error) => {
            UserService.UserPromise.then((user) => {
                var companyKey = (<string>toParams['companyKey']).toUpperCase();
                if (companyKey != user.Company.LongAbbreviation && user.IsSystemAdministrator) {
                    AlertService.OpenImpersonationModal(companyKey);
                } else {
                    AlertService.OpenWarningModal({ title: 'Request Failed', message: error });
                    $state.go('index');
                }
            });
        });
    });

    app.config(function ($httpProvider: ng.IHttpProvider) {
        $httpProvider.interceptors.push(function ($location: ng.ILocationService, $injector: ng.auto.IInjectorService, $q: ng.IQService, $rootScope: AppRootScope) {
            return {
                request: function (config: ng.IRequestConfig) {
                    //Passing in actual url for the server to log (otherwise the server won't see anything past the '#' in the Url)
                    config.headers.RefererFullUrl = $location.absUrl();
                    
                    //Passing in impersonated user if there is one
                    if ($rootScope['ImpersonatedUserName']) {
                        config.headers.Impersonate = $rootScope['ImpersonatedUserName'];
                    }

                    return config;
                },
                response: (response: ng.IRequestConfig) => {//Version change handling
                    var responseVersion = response.headers("version");
                    if (responseVersion != null && $rootScope.appVersion != null && $rootScope.appVersion != responseVersion) {
                        var $modal = <ng.ui.bootstrap.IModalService>$injector.get('$modal');
                        var $modalStack = $injector.get('$modalStack');

                        //Only open this if we haven't opened one yet
                        var currentModal = $modalStack.getTop();
                        if (currentModal == null || currentModal.value.modalScope.RefreshPage == null) {
                            $modal.open({
                                size: 'sm',
                                template: '<version-error-modal></version-error-modal>'
                            });
                        }
                    }

                    return response;
                },
                responseError: (response: any) => {//Global error handling
                    switch (response.status) {
                        case 401:
                        case 403:
                            if (response.config.IsAuthRetry === undefined) {
                                response.config.IsAuthRetry = true;
                                return new $q((resolve, reject) => {
                                    var listener = $rootScope.$on('Login-Completed', function () {
                                        var httpRetry = $injector.get('$http')(response.config);
                                        resolve(httpRetry);
                                        listener();//remove the listener
                                    });

                                    $rootScope.$broadcast('Login-Required');
                                });
                            } else {
                                alert('You do not have access to perform this action');
                            }
                            break;
                        case 404:
                        case 405:
                            alert('Not Found (' + response.config.url + ')');
                            break;
                        case 0:
                            //request was cancelled
                            break;
                        case 500:
                            var modal = $injector.get('$modal').open({
                                size: 'sm',
                                template: '<server-error-modal request-url="requestUrl" message="message" modal="modal"></server-error-modal>',
                                controller: ($scope: ng.IScope) => {
                                    $scope['requestUrl'] = response.config.url;
                                    $scope['message'] = (response.data || {}).ExceptionMessage;
                                    $scope['modal'] = modal;
                                }
                            });
                            break;
                    }

                    return $q.reject(response);
                }
            };
        });
    });
}