﻿/// <reference path="../../app.ts" />
module arms {
    'use strict';

    app.directive('surveyPolicyForm',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/survey/survey-policy-form.html',
            replace: true,
            scope: {
                Policy: '=policy'
            },
            bindToController: true,
            controller: SurveyPolicyFormController,
            controllerAs: 'vm'
        }
    });

    class SurveyPolicyFormController extends BaseController {
        Policy: any;
        LineOfBusinesses: CodeItem[];
        ProfitCenters: CodeItem[];
        States: State[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, DomainService: DomainService) {
            super($scope, $state, UtilityService);

            DomainService.States().then((states) => this.States = states);
            DomainService.LinesOfBusiness(this.companyKey).then((lobs) => this.LineOfBusinesses = lobs);
            DomainService.ProfitCenters(this.companyKey).then((pc) => this.ProfitCenters = pc);

        }
    }
} 