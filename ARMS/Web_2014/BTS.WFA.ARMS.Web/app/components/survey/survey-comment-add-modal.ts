﻿/// <reference path="../../app.ts" />
module arms {
    'use strict';
    app.directive('surveyCommentAddModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/survey/survey-comment-add-modal.html',
            replace: true,
            scope: {
                Survey: '=survey',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: SurveyCommentAddModalController,
            controllerAs: 'vm'
        };
    });

    interface CommentType {
        Description: string;
        Location?: SurveyLocation;
        Group?: string;
    }

    class SurveyCommentAddModalController extends BaseController {
        Survey: Survey;
        CommentTypeOptions: CommentType[];
        NewComment: SurveyComment;
        NewCommentLocation: CommentType;
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, private $http: ng.IHttpService) {
            super($scope, $state, UtilityService);

            this.NewComment = { Text: '', Internal: true };

            this.CommentTypeOptions = [];
            this.CommentTypeOptions.push({ Description: 'Add to Entire Request' });
            this.NewCommentLocation = this.CommentTypeOptions[0];

            angular.forEach(this.Survey.Locations,(l) => {
                this.CommentTypeOptions.push({ Group: 'Add to Individual Location', Description: `${l.Number} - ${l.Address.Address1}`, Location: l });
            });
        }

        ToggleInternal() {
            this.NewComment.Internal = !this.NewComment.Internal;
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}