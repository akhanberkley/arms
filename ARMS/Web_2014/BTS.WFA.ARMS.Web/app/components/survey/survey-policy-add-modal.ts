﻿/// <reference path="../../app.ts" />
module arms {
    'use strict';
    app.directive('surveyPolicyAddModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/survey/survey-policy-add-modal.html',
            replace: true,
            scope: {
                Survey: '=survey',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: SurveyPolicyAddModalController,
            controllerAs: 'vm'
        };
    });

    class SurveyPolicyAddModalController extends BaseController {
        Survey: any;
        NewPolicy: SurveyPolicy;
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, private $http: ng.IHttpService) {
            super($scope, $state, UtilityService);

            this.NewPolicy = <SurveyPolicy>{};
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}