﻿/// <reference path="../../app.ts" />
module arms {
    'use strict';

    //Temp Code
    var underwriter = <Contact>{ Name: 'Abbey Sanborn', Phone: '207.874.5761', Email: 'Abbey.Sanborn@acadia-ins.com' };
    var agency = <Agency>{ "Code": "07931", "Name": "A & B Insurance Group, LLC", "Phone": "9783990025", "Fax": "9783990079", "Address": { "Address1": "239 Littleton Road, Suite 4B", "City": "Westford", "State": "MA", "PostalCode": "01886" } };
    var agent = <Contact>{ "Name": "Wendy M. Ashe", "Phone": "978-399-0025", "Email": "wendy@abinsgroup.com" };

    var sampleSurvey = {
        DueDate: new Date('4/8/2015'),
        SurveyReasons: <string[]>[],
        Insured: {
            Id: 2234689,
            Name: 'Heisenberg\'s Vamanos Pest Control',
            AdditionalName: 'HVPC Co.',
            Address: {
                Address1: '4725 Candelaria Rd NE',
                Address2: 'Unit 101',
                City: 'Las Vegas',
                State: 'NV',
                PostalCode: '89101'
            },
            PrimaryContact: {
                Name: '',
                Phone: '',
                Email: ''
            },
            Agency: agency,
            Agent: agent
        },
        Underwriter: underwriter,
        Policies: [
            {
                LineOfBusiness: { Code: 'PK', Description: 'Package' },
                PolicySymbol: 'CPA',
                Number: '0075437.21',
                EffectiveDate: new Date('11/12/2014'),
                ExpirationDate: new Date('11/12/2015'),
                BranchCode: 'NM',
                ProfitCenter: '',
                Carrier: '',
                PrimaryState: '',
                Premium: 106631,
                HazardGrade: 5,
                Coverages: [
                    {
                        Included: true,
                        Name: { Code: 'asdf', Description: 'Property (Commercial Auto)' },
                        Items: <SurveyCoverageItem[]>[]
                    },
                    {
                        Included: true,
                        Name: { Code: 'asdf', Description: 'General Liability (General Liabilty)' },
                        Items: [{ Name: 'Occurance Limits', Value: '1,000,000' }, { Name: 'Aggregate Limits', Value: '2,000,000' }, { Name: 'Total Exposure (Rating)', Value: '56,602.40' }]
                    },
                    {
                        Included: true,
                        Name: { Code: 'asdf', Description: 'Products Operations (General Liability)' },
                        Items: [{ Name: 'Limits', Value: '2,000,000' }]
                    },
                    {
                        Included: false,
                        Name: { Code: 'asdf', Description: 'Contractors Equipment (Inland Marine)' },
                        Items: [{ Name: 'Owned Equipment', Value: '' }, { Name: 'Leased from Others', Value: '' }, { Name: 'Leased to Others', Value: '' }]
                    },
                    {
                        Included: false,
                        Name: { Code: 'asdf', Description: 'Tool Floater (Inland Marine)' },
                        Items: [{ Name: 'Other Tools', Value: '' }, { Name: 'Employee Tools', Value: '' }]
                    },
                    {
                        Included: false,
                        Name: { Code: 'asdf', Description: 'Computer Equipment (Inland Marine)' },
                        Items: [{ Name: 'Hardware Limits', Value: '' }]
                    },
                    {
                        Included: false,
                        Name: { Code: 'asdf', Description: 'Signs (Inland Marine)' },
                        Items: [{ Name: 'Scheduled Limits', Value: '' }, { Name: 'Unscheduled Limits', Value: '' }]
                    },
                    {
                        Included: true,
                        Name: { Code: 'asdf', Description: 'Exposure (Workers Compensation)' },
                        Items: [{ Name: 'Class Code', Value: '6 - Chemical Mfg.' }, { Name: 'Payroll', Value: '0' }, { Name: '# of Employees', Value: '' }]
                    },
                    {
                        Included: false,
                        Name: { Code: 'asdf', Description: 'Business Income (Commercial Property)' },
                        Items: [{ Name: 'Limits', Value: '' }]
                    }
                ]
            },
            {
                LineOfBusiness: { Code: 'CA', Description: 'Commercial Auto' },
                PolicySymbol: 'CAA',
                Number: '0075439.21',
                EffectiveDate: new Date('11/12/2014'),
                ExpirationDate: new Date('11/12/2015'),
                BranchCode: 'NM',
                ProfitCenter: '',
                Carrier: '',
                PrimaryState: '',
                Premium: 22172,
                HazardGrade: 5,
                Coverages: [
                    {
                        Included: true,
                        Name: { Code: 'asdf', Description: 'Commercial Auto Details' },
                        Items: [{ Name: 'Bodily Injury Limit', Value: '1,000,000' }, { Name: 'Physical Damage Limit', Value: '' }, { Name: 'Number of Vehicle/Units', Value: '22' }]
                    },
                    {
                        Included: true,
                        Name: { Code: 'asdf', Description: 'Non-Owned/Hired Auto Details' },
                        Items: [{ Name: 'Bodily Injury Limit', Value: '1,000,000' }, { Name: 'Physical Damage Limit', Value: '1,000,000' }]
                    }
                ]
            },
            {
                LineOfBusiness: { Code: 'CU', Description: 'Commercial Umbrella' },
                PolicySymbol: 'CUA',
                Number: '0075453.21',
                EffectiveDate: new Date('11/12/2014'),
                ExpirationDate: new Date('11/12/2015'),
                BranchCode: 'NM',
                ProfitCenter: '',
                Carrier: '',
                PrimaryState: '',
                Premium: 10447,
                HazardGrade: 5,
                Coverages: [
                    {
                        Included: true,
                        Name: { Code: 'asdf', Description: 'Umbrella' },
                        Items: [{ Name: 'Occurance Limits', Value: '7,000,000' }, { Name: 'Aggregate Limits', Value: '7,000,000' }]
                    }
                ]
            },
            {
                LineOfBusiness: { Code: 'IM', Description: 'Inland Marine' },
                PolicySymbol: 'CIM',
                Number: '0228882.17',
                EffectiveDate: new Date('11/12/2014'),
                ExpirationDate: new Date('11/12/2015'),
                BranchCode: 'NM',
                ProfitCenter: '',
                Carrier: '',
                PrimaryState: '',
                Premium: 4525,
                HazardGrade: 0,
                Coverages: [
                    {
                        Included: false,
                        Name: { Code: 'asdf', Description: 'Motor Truck Cargo' },
                        Items: [{ Name: 'Vehicle Limits', Value: '' }, { Name: 'Legal Liability Limits', Value: '' }]
                    },
                    {
                        Included: false,
                        Name: { Code: 'asdf', Description: 'Builders Risk' },
                        Items: [{ Name: 'Construction Limits', Value: '' }, { Name: 'Existing Building Limits', Value: '' }]
                    },
                    {
                        Included: false,
                        Name: { Code: 'asdf', Description: 'Warehouse Liability' },
                        Items: [{ Name: 'Spoilage Limits', Value: '' }, { Name: 'Cold Storage Limits', Value: '' }]
                    },
                    {
                        Included: false,
                        Name: { Code: 'asdf', Description: 'Jewelers Block' },
                        Items: [{ Name: 'Your Premises Limits', Value: '' }]
                    },
                    {
                        Included: false,
                        Name: { Code: 'asdf', Description: 'Tool Floater' },
                        Items: [{ Name: 'Other Tools', Value: '' }, { Name: 'Employee Tools', Value: '' }]
                    },
                    {
                        Included: false,
                        Name: { Code: 'asdf', Description: 'Computer Equipment' },
                        Items: [{ Name: 'Hardware Limits', Value: '' }, { Name: 'Software Media Limits', Value: '' }]
                    }
                ]
            }
        ],
        Locations: [
            {
                Included: true,
                Number: 1,
                Description: '',
                Address: { Address1: '54 Bradley Street', Address2: '', City: 'Albuquerque', State: 'NM', PostalCode: '87048' },
                Buildings: [
                    { Number: 1, Included: true, YearBuilt: 1989, Sprinkler: true, SprinklerCredit: true, PublicProtection: true, Value: 1250000 }
                ],
                Contacts: [
                    { Number: 1, Name: 'Barbara Kollander', Title: 'Business Manager', Phone: '515-925-3115', Email: 'bkollander@email.com' }
                ],
                Comments: [
                    { Id: 1, Author: 'BHall', Date: new Date('1/1/2014'), Text: 'This location has a location specific comment! This is the comment!' }
                ]
            },
            {
                Included: true,
                Number: 2,
                Description: '',
                Address: { Address1: '60 Bradley Street', Address2: '', City: 'Albuquerque', State: 'NM', PostalCode: '87048' },
                Buildings: [
                    { Number: 1, Included: true, YearBuilt: 1991, Sprinkler: true, SprinklerCredit: true, PublicProtection: true, Value: 750000 }
                ],
                Contacts: [
                    { Number: 1, Name: 'Barbara Kollander', Title: 'Business Manager', Phone: '515-925-3115', Email: 'bkollander@email.com' }
                ],
                Comments: [
                    { Id: 1, Author: 'BGill', Date: new Date('1/2/2014'), Text: 'First unique comment for Location 2' }
                ]
            },
            {
                Included: true,
                Number: 3,
                Description: '',
                Address: { Address1: '70 Pine Street', Address2: '', City: 'Albuquerque', State: 'NM', PostalCode: '87048' },
                Buildings: [
                    { Number: 1, Included: true, YearBuilt: 1989, Sprinkler: true, SprinklerCredit: true, PublicProtection: true, Value: 1250000 },
                    { Number: 2, Included: true, YearBuilt: 1990, Sprinkler: false, SprinklerCredit: false, PublicProtection: true, Value: 1000000 }
                ],
                Contacts: [
                    { Number: 1, Name: 'Barbara Kollander', Title: 'Business Manager', Phone: '515-925-3115', Email: 'bkollander@email.com' }
                ],
                Comments: [
                    { Id: 1, Author: 'KFlaherty', Date: new Date('1/3/2014'), Text: 'Unique Comment to Locatoin 3 Only' }
                ]
            },
            {
                Included: true,
                Number: 4,
                Description: '',
                Address: { Address1: '72 Pine Street', Address2: '', City: 'Albuquerque', State: 'NM', PostalCode: '87048' },
                Buildings: [
                    { Number: 1, Included: true, YearBuilt: 1990, Sprinkler: false, SprinklerCredit: false, PublicProtection: false, Value: 500000 }
                ],
                Contacts: [
                    { Number: 1, Name: 'Barbara Kollander', Title: 'Business Manager', Phone: '515-925-3115', Email: 'bkollander@email.com' }
                ],
                Comments: [
                    { Id: 1, Author: 'PHerrmann', Date: new Date('1/4/2014'), Text: 'This is another location specific commment!' }
                ]
            }
        ],
        Documents: [
            { Included: false, DocumentType: 'APP', FileName: 'Commercial Auto', EntryDate: new Date('10/12/2010'), Number: 124563566, PolicyNumber: '0075439.21', PolicyType: 'CAA', Remarks: 'Commercial Auto 2010' },
            { Included: false, DocumentType: 'APP', FileName: 'Commercial Auto', EntryDate: new Date('10/12/2011'), Number: 126548978, PolicyNumber: '0075439.21', PolicyType: 'CAA', Remarks: 'Commercial Auto 2011' },
            { Included: false, DocumentType: 'APP', FileName: 'Commercial Auto', EntryDate: new Date('10/12/2012'), Number: 125789625, PolicyNumber: '0075439.21', PolicyType: 'CAA', Remarks: 'Commercial Auto 2012' },
            { Included: true, DocumentType: 'APP', FileName: 'Commercial Auto', EntryDate: new Date('10/12/2013'), Number: 377236675, PolicyNumber: '0075439.21', PolicyType: 'CAA', Remarks: 'Newest Commercial Auto' },
            { Included: false, DocumentType: 'APP', FileName: 'Commercial Umbrella', EntryDate: new Date('10/12/2010'), Number: 123456632, PolicyNumber: '0075453.21', PolicyType: 'CUA', Remarks: 'Commercial Umbrella 2010' },
            { Included: false, DocumentType: 'APP', FileName: 'Commercial Umbrella', EntryDate: new Date('10/12/2011'), Number: 124579569, PolicyNumber: '0075453.21', PolicyType: 'CUA', Remarks: 'Commercial Umbrella 2011' },
            { Included: false, DocumentType: 'APP', FileName: 'Commercial Umbrella', EntryDate: new Date('10/12/2012'), Number: 125489789, PolicyNumber: '0075453.21', PolicyType: 'CUA', Remarks: 'Commercial Umbrella 2012' },
            { Included: true, DocumentType: 'APP', FileName: 'Commercial Umbrella', EntryDate: new Date('10/12/2013'), Number: 477945658, PolicyNumber: '0075453.21', PolicyType: 'CUA', Remarks: 'Newest Commercial Umbrella' },
            { Included: false, DocumentType: 'APP', FileName: 'Package', EntryDate: new Date('10/12/2010'), Number: 124576511, PolicyNumber: '0075437.21', PolicyType: 'CPA', Remarks: 'Package 2010' },
            { Included: false, DocumentType: 'APP', FileName: 'Package', EntryDate: new Date('10/12/2011'), Number: 124569789, PolicyNumber: '0075437.21', PolicyType: 'CPA', Remarks: 'Package 2011' },
            { Included: false, DocumentType: 'APP', FileName: 'Package', EntryDate: new Date('10/12/2012'), Number: 125794256, PolicyNumber: '0075437.21', PolicyType: 'CPA', Remarks: 'Package 2012' },
            { Included: true, DocumentType: 'APP', FileName: 'Package', EntryDate: new Date('10/12/2013'), Number: 377234759, PolicyNumber: '0075437.21', PolicyType: 'CPA', Remarks: 'Newest Package' },
            { Included: false, DocumentType: 'ISO', FileName: 'Est Loss Cost', EntryDate: new Date('10/12/2010'), Number: 125468256, PolicyNumber: '0075437.21', PolicyType: 'CPA', Remarks: 'Cost Loss as of 2010' },
            { Included: false, DocumentType: 'ISO', FileName: 'Est Loss Cost', EntryDate: new Date('10/12/2011'), Number: 125974584, PolicyNumber: '0075437.21', PolicyType: 'CPA', Remarks: 'Est Cost Loss as of 2011' },
            { Included: false, DocumentType: 'ISO', FileName: 'Est Loss Cost', EntryDate: new Date('10/12/2012'), Number: 125463656, PolicyNumber: '0075437.21', PolicyType: 'CPA', Remarks: 'Est Cost Loss as of 2012' },
            { Included: true, DocumentType: 'ISO', FileName: 'Est Loss Cost', EntryDate: new Date('10/12/2013'), Number: 377236669, PolicyNumber: '0075437.21', PolicyType: 'CPA', Remarks: 'Newest Est Cost Loss' },
            { Included: false, DocumentType: 'SOV', FileName: 'Unsigned', EntryDate: new Date('10/12/2013'), Number: 377236670, PolicyNumber: '0075437.21', PolicyType: 'CPA', Remarks: 'This Document was Unsigned' }
        ],
        Comments: [{ Id: 1, Author: 'PHerrmann', Date: new Date('08/20/2014'), Text: 'This is a comment that does not belong to an individual location, but rather the entire request.' }]
    };
    //End temp code


    app.directive('survey',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/survey/survey.html',
            replace: true,
            scope: {
                surveyId: '='
            },
            bindToController: true,
            controller: SurveyController,
            controllerAs: 'vm'
        }
    });

    class SurveyController extends BaseController {
        surveyId: number;
        CurrentStateRoute: string;
        Survey: Survey;

        SurveyReasons: string[];
        States: State[];
        Underwriters: Contact[];
        NewUnderwriter: Contact;
        NewAgent: Contact;
        Agents: Contact[];

        SelectedPolicy: SurveyPolicy;
        ShowSelectedPolicyDetail: boolean;
        ShowSelectedPolicyCoverageDetail: boolean;

        SelectedLocation: SurveyLocation;
        ShowSelectedLocationDetail: boolean;
        ShowLocationMap: boolean;
        ShowLocationAddress: boolean;
        ShowLocationBuildings: boolean;
        ShowLocationContacts: boolean;
        ShowLocationComments: boolean;

        DocumentFilterIncluded: boolean;
        DocumentFilterExcluded: boolean;
        DocumentFilterText: string;
        FilteredDocuments: SurveyDocument[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, DomainService: DomainService, $q: ng.IQService, private $http: ng.IHttpService, private $modal: ng.ui.bootstrap.IModalService, private $filter: ng.IFilterService) {
            super($scope, $state, UtilityService);

            this.UtilityService.SetPageTitle('Request');

            this.OnStateChange();
            $scope.$on('$stateChangeSuccess',(event, toState, toParams, fromState, fromParams) => {
                this.OnStateChange();
            });

            this.AppState.PageGenerating = true;
            this.NewUnderwriter = <Contact>{};
            this.NewAgent = <Contact>{};
            this.DocumentFilterIncluded = true;
            this.DocumentFilterExcluded = true;

            var pagePromises = <ng.IPromise<any>[]>[];
            pagePromises.push(DomainService.States().then((states) => this.States = states));
            pagePromises.push(DomainService.Underwriters(this.companyKey).then((underwriters) => {
                this.Underwriters = angular.copy(underwriters);
                angular.forEach(this.Underwriters,(u) => (<any>u).SelectGroup = 'Existing Underwriters');
            }));
            pagePromises.push(DomainService.SurveyReasons(this.companyKey).then((surveyReasons) => this.SurveyReasons = surveyReasons));

            if (this.surveyId == 1234) {
                this.Survey = <any>sampleSurvey;
            } else {
                pagePromises.push($http.get(`/api/${this.companyKey}/Survey/${this.surveyId}`).then((result: ng.IHttpPromiseCallbackArg<Survey>) => {
                    this.Survey = result.data;
                }));
            }


            $q.all(pagePromises).then(() => {
                if (this.Survey != null) {
                    if (this.Survey.Insured != null) {
                        this.AgencyChanged(false);
                    }

                    angular.forEach(this.Survey.Policies,(p) => {
                        this.CalculateIncludedCoverageTotal(p);
                    });

                    this.DocumentFilterChanged();
                    this.AppState.PageGenerating = false;
                }
            });
        }

        //Page Level
        OnStateChange() {
            this.CurrentStateRoute = this.$state.current.name;
            if (this.CurrentStateRoute == 'ck.survey') {
                this.$state.go('.insured', null, { location: 'replace' });
            }
        }
        Continue() {
            switch (this.CurrentStateRoute) {
                case 'ck.survey.insured': this.$state.go('^.agency', null, { location: 'replace' }); break;
                case 'ck.survey.agency': this.$state.go('^.coverages', null, { location: 'replace' }); break;
                case 'ck.survey.coverages': this.$state.go('^.locations', null, { location: 'replace' }); break;
                case 'ck.survey.locations': this.$state.go('^.documents', null, { location: 'replace' }); break;
                case 'ck.survey.documents': this.$state.go('^.assignment', null, { location: 'replace' }); break;
            }
        }
        ShowAddCommentModal() {
            var modal = this.$modal.open({
                windowClass: 'modal-md',
                template: '<survey-comment-add-modal survey="survey" modal="modal"></survey-comment-add-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['survey'] = this.Survey;
                    $scope['modal'] = modal;
                }
            });
        }

        //Insured
        AddUnderwriter() {
            this.Underwriters.push(this.NewUnderwriter);
            this.Survey.Underwriter = this.NewUnderwriter;

            this.NewUnderwriter = <Contact>{};
        }

        //Agency
        AddAgent() {
            this.Agents.push(this.NewAgent);
            (<any>this.Survey).Agent = this.NewAgent;

            this.NewAgent = <Contact>{};
        }

        AgencySearch = (term: string) => {
            var encodedTerm = this.UtilityService.ODataString(term);
            var criteria = `(contains(AgencyName, ${encodedTerm}) or contains(AgencyNumber, ${encodedTerm}))`;
            var url = `/api/${this.companyKey}/Agency?$filter=${criteria}&$orderby=AgencyName,AgencyNumber&$top=10`;

            return this.$http.get(url).then((response: ng.IHttpPromiseCallbackArg<Agency[]>) => {
                return response.data;
            });
        }

        AgencyChanged(resetAgent: boolean) {
            if (resetAgent) {
                this.Survey.Insured.Agent = null;
            }

            this.Agents = null;
            if (this.Survey.Insured.Agency != null) {
                this.$http.get(`/api/${this.companyKey}/Agency/${this.Survey.Insured.Agency.Code}/Agent`).then((response: ng.IHttpPromiseCallbackArg<Contact[]>) => {
                    this.Agents = response.data;
                    if (resetAgent && this.Agents.length == 1) {
                        this.Survey.Insured.Agent = this.Agents[0];
                    }

                    angular.forEach(this.Agents,(u) => (<any>u).SelectGroup = 'Existing Agents');
                });
            }
        }

        //Coverages
        CalculateIncludedCoverageTotal(policy: SurveyPolicy) {
            policy.CoverageIncludedCount = policy.Coverages.filter((c) => c.Included).length;
        }

        SelectPolicy(policy: SurveyPolicy, showPolicy: boolean) {
            if (policy.CoverageIncludedCount > 0) {
                this.SelectedPolicy = policy;

                if (showPolicy) {
                    this.ShowSelectedPolicyDetail = true;
                }
            }
        }
        ToggleShowSelectedPolicyDetail() {
            this.ShowSelectedPolicyDetail = !this.ShowSelectedPolicyDetail;
            if (!this.ShowSelectedPolicyDetail) {
                this.ShowSelectedPolicyCoverageDetail = false;
            }
        }
        ToggleShowSelectedPolicyCoverageDetail() {
            this.ShowSelectedPolicyCoverageDetail = !this.ShowSelectedPolicyCoverageDetail;
        }

        TogglePolicyExpansion(policy: SurveyPolicy) {
            if (policy.CoverageIncludedCount > 0) {
                policy.Expanded = (!policy.Expanded);
            }
        }
        TogglePolicyInclusion(policy: SurveyPolicy) {
            var included = (policy.CoverageIncludedCount == 0);
            angular.forEach(policy.Coverages,(c) => c.Included = included);

            this.CalculateIncludedCoverageTotal(policy);
            if (!included) {
                policy.Expanded = false;
                if (policy == this.SelectedPolicy) {
                    this.SelectedPolicy = null;
                    this.ShowSelectedPolicyDetail = false;
                }
            }
        }
        ToggleCoverageInclusion(coverage: SurveyCoverage, policy: SurveyPolicy) {
            coverage.Included = (!coverage.Included);
            this.CalculateIncludedCoverageTotal(policy);
        }

        ShowAddPolicyModal() {
            var modal = this.$modal.open({
                windowClass: 'modal-md',
                template: '<survey-policy-add-modal survey="survey" modal="modal"></survey-policy-add-modal>',
                controller: ($scope: ng.IScope) => {
                    $scope['survey'] = this.Survey;
                    $scope['modal'] = modal;
                }
            });
        }

        //Locations
        SelectLocation(location: SurveyLocation, showLocation: boolean) {
            if (location.Included) {
                this.SelectedLocation = location;

                if (showLocation) {
                    this.ShowSelectedLocationDetail = true;
                }
            }
        }
        LocationDetailVisible() {
            return this.ShowSelectedLocationDetail || this.ShowLocationMap;
        }
        ToggleLocationInclusion(location: SurveyLocation) {
            location.Included = !location.Included;
            if (!location.Included && location == this.SelectedLocation) {
                this.SelectedLocation = null;
                this.ShowSelectedLocationDetail = false;
            }
        }
        ToggleLocationBuildingInclusion(building: SurveyBuilding) {
            building.Included = !building.Included;
        }
        ToggleShowSelectedLocationDetail() {
            this.ShowSelectedLocationDetail = !this.ShowSelectedLocationDetail;
            if (this.ShowSelectedLocationDetail) {
                this.ShowLocationMap = false;
            }
        }
        ToggleShowLocationMap() {
            this.ShowLocationMap = !this.ShowLocationMap;
            if (this.ShowLocationMap) {
                this.ShowSelectedLocationDetail = false;
            }
        }
        ToggleShowLocationAddress() {
            this.ShowLocationAddress = !this.ShowLocationAddress;
        }
        ToggleShowLocationBuildings() {
            this.ShowLocationBuildings = !this.ShowLocationBuildings;
        }
        ToggleShowLocationContacts() {
            this.ShowLocationContacts = !this.ShowLocationContacts;
        }
        ToggleShowLocationComments() {
            this.ShowLocationComments = !this.ShowLocationComments;
        }

        //Documents
        ToggleDocumentFilterIncluded() {
            this.DocumentFilterIncluded = !this.DocumentFilterIncluded;
            this.DocumentFilterChanged();
        }
        ToggleDocumentFilterExcluded() {
            this.DocumentFilterExcluded = !this.DocumentFilterExcluded;
            this.DocumentFilterChanged();
        }
        DocumentFilterChanged() {
            var docs = this.Survey.Documents.filter((d: any) => (this.DocumentFilterIncluded && d.Included) || (this.DocumentFilterExcluded && !d.Included));
            this.FilteredDocuments = this.$filter('filter')(docs, this.DocumentFilterText);
        }

        ToggleDocumentInclusion(document: SurveyDocument) {
            document.Included = !document.Included;
            this.DocumentFilterChanged();
        }
    }
} 