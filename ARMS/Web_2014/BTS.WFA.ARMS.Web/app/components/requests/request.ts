﻿/// <reference path="../../app.ts" />
module arms {
    'use strict';

    app.directive('request',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/requests/request.html',
            replace: true,
            scope: {
                Request: '=request'
            },
            bindToController: true,
            controller: RequestController,
            controllerAs: 'vm'
        }
    });

    class RequestController extends BaseController {
        Request: Request;
        SurveyReasons: string[];

        ShowPastSurveys: boolean;
        ShowRequestInformation: boolean;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, DomainService: DomainService, $http: ng.IHttpService) {
            super($scope, $state, UtilityService);

            this.ShowRequestInformation = true;
            DomainService.SurveyReasons(this.companyKey).then((surveyReasons) => this.SurveyReasons = surveyReasons);
        }

        ToggleShowPastSurveys() {
            this.ShowPastSurveys = !this.ShowPastSurveys;
        }
        ToggleShowRequestInformation() {
            this.ShowRequestInformation = !this.ShowRequestInformation;
        }
    }
}  