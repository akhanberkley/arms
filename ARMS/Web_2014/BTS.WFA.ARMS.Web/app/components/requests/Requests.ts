﻿/// <reference path="../../app.ts" />
module arms {
    'use strict';

    app.directive('requests',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/requests/requests.html',
            replace: true,
            bindToController: true,
            controller: RequestQueueController,
            controllerAs: 'vm'
        }
    });

    class RequestQueueController extends BaseController {
        RequestQueue: Request[];
        ShowSelectedRequestDetail: boolean;
        SelectedRequest: Request;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, UserService: UserService, $http: ng.IHttpService) {
            super($scope, $state, UtilityService);

            this.UtilityService.SetPageTitle('Request Queue');

            this.AppState.PageGenerating = true;
            $http.get('/api/' + this.companyKey + '/Requests?$orderby=CreateDate desc').then((response: ng.IHttpPromiseCallbackArg<Request[]>) => {
                this.RequestQueue = response.data;
                this.AppState.PageGenerating = false;
            });
        }

        SelectRequest(request: Request, showRequest: boolean) {
            this.SelectedRequest = request;
            if (showRequest) {
                this.ShowSelectedRequestDetail = true;
            }
        }

        ToggleShowRequestDetail() {
            this.ShowSelectedRequestDetail = !this.ShowSelectedRequestDetail;
        }
    }
}