﻿/// <reference path="../../../app.ts" />
module arms {
    'use strict';
    app.directive('roles',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/roles/roles.html',
            replace: true,
            bindToController: true,
            controller: RolesController,
            controllerAs: 'vm'
        }
    });

    class RolesController extends BaseController {
        UserRoles: UserRole[];
        SelectedUserRole: UserRole;

        ShowGeneralPermissions: boolean;
        ShowSurveyPermissions: boolean;
        ShowServicePlanPermissions: boolean;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, UserService: UserService, $http: ng.IHttpService) {
            super($scope, $state, UtilityService);

            UtilityService.SetPageTitle('Administration - User Roles');
            $http.get(`/api/${this.companyKey}/UserRole`).then((results: ng.IHttpPromiseCallbackArg<UserRole[]>) => {
                this.UserRoles = results.data;
            });
        }

        ToggleShowGeneralPermission() {
            this.ShowGeneralPermissions = !this.ShowGeneralPermissions;
        }
        ToggleShowSurveyPermissions() {
            this.ShowSurveyPermissions = !this.ShowSurveyPermissions;
        }
        ToggleShowServicePlanPermissions() {
            this.ShowServicePlanPermissions = !this.ShowServicePlanPermissions;
        }
    }
}