﻿angular.module('arms').controller('ActivityTimeCtrl', function ($scope, $http, $filter, $location, $modal, $stateParams, TableService) {
    //var companyNumber = $stateParams.companyNumber;
    var companyNumber = "52"

    $http({ method: 'GET', url: '/api/' + companyNumber + '/ActivityTime/GetAll' }).success(function (page, status, headers, config) {
     
        $scope.Activities = page.Activities;
        $scope.ActivityTypes = page.ActivityTypes;
        $scope.Users = page.Users;

        $scope.years = ["(All)", "2009", "2010", "2011", "2012", "2013", "2014"];
        $scope.months = [
      "(All)",
      { "Code": "01", "Text": "January" },
      { "Code": "02", "Text": "February" },
      { "Code": "03", "Text": "March" },
      { "Code": "04", "Text": "April" },
      { "Code": "05", "Text": "May" },
      { "Code": "06", "Text": "June" },
      { "Code": "07", "Text": "July" },
      { "Code": "08", "Text": "August" },
      { "Code": "09", "Text": "September" },
      { "Code": "10", "Text": "October" },
      { "Code": "11", "Text": "November" },
      { "Code": "12", "Text": "December" },
        ];
      
      
        $scope.Activities = TableService.CreatePager(page.Activities, 15, function (fullList) {            
            return $filter('filter')(fullList, function (item) {                
                return item.AllocatedTo.ID.toLowerCase() === $scope.user.toLowerCase() && item.Date.toLowerCase().indexOf($scope.year) !== -1 && item.Date.toLowerCase().indexOf($scope.month) !== -1;
            });
        });



        var showModal = function (item, isNew) {
            var clonedItem = angular.copy(item);
            var pager = $scope.Activities;

            modalInstance = $modal.open({
                templateUrl: 'activitytime-item-modal',
                controller: function ($scope, $modalInstance) {
                    var originalCode = clonedItem.ID;                  
                    $scope.Item = clonedItem;
                    clonedItem.Users = angular.copy(page.Users);
                    clonedItem.ActivityTypes = angular.copy(page.ActivityTypes);


                    $scope.ShowDelete = (isNew) ? false : true;
                    $scope.SaveButtonText = (isNew) ? 'Add Item' : 'Update Item';
                    $scope.Form = {};
                    $scope.ValidationErrors = [];
                    $scope.SavingItem = false;
                    $scope.Update = function () {
                        $scope.SavingItem = true;
                        //ValidationService.Reset($scope.Form.ActivityTime);
                        $scope.ValidationErrors = [];

                        $http({ method: 'POST', url: '/api/' + companyNumber + '/ActivityTime/' + ((isNew) ? '' : originalCode), data: $scope.Item }).success(function (activitytime, status, headers, config) {
                            if (isNew) {
                                pager.Items.push(activitytime);
                                pager.Items.sort(function (a, b) { return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0); });
                                pager.FilterChanged();
                            }
                            else {
                                for (var i = 0; i < pager.Items.length; i++) {
                                    if (pager.Items[i].ID == activitytime.ID) {
                                        pager.Items[i] = activitytime;
                                        pager.FilterChanged();
                                        break;
                                    };
                                }
                            }

                            $scope.SavingItem = false;
                            $modalInstance.close();
                        }).error(function (data, status, headers, config) {
                            if (status == 400) {
                                ValidationService.SetValidationResults($scope.Form.activitytime, data);
                                $scope.ValidationErrors = data;
                            }
                            $scope.SavingItem = false;
                        });
                    };
                    $scope.Delete = function () {
                        if (confirm('Are you sure you want to delete this item?')) {
                            $scope.SavingItem = true;
                            //ValidationService.Reset($scope.Form.user);
                            $scope.ValidationErrors = [];
                            $http({ method: 'DELETE', url: '/api/' + companyNumber + '/ActivityTime/' + originalCode }).success(function (activitytime, status, headers, config) {                                                            
                                $modalInstance.close();
                            }).error(function (data, status, headers, config) {
                                if (status == 400) {
                                    //ValidationService.SetValidationResults($scope.Form.user, data);
                                    $scope.ValidationErrors = data;
                                }
                                $scope.SavingItem = false;
                            });

                        }
                    };
                    $scope.Cancel = function () { $modalInstance.dismiss('cancel'); };
                }
            });
        };

     
        $scope.NewActivityTime = function () { showModal(page.NewActivityTime, true); };
        $scope.SelectActivityTime = function (activitytime) { showModal(activitytime, false); };

        $scope.PageLoading = false;
    });

});
