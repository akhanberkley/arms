﻿/// <reference path="../../../app.ts" />
module arms {
    'use strict';
    app.directive('assignmentRules',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/rules/assignment-rules.html',
            replace: true,
            scope: {
                id: '@'
            },
            bindToController: true,
            controller: RulesController,
            controllerAs: 'vm'
        }
    });

    class RulesController extends BaseController {
        SelectedUser: User;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, $q: ng.IQService,
            private UserService: UserService) {
            super($scope, $state, UtilityService);

                UserService.UserPromise.then((user) => {
                    this.SelectedUser = user;
            });

    }
}
}
//angular.module('arms').controller('AssignmentRuleCtrl', function ($scope, $http, $filter, $location, $modal, $stateParams, TableService, ValidationService) {
//    //var companyNumber = $stateParams.companyNumber;
//    var companyNumber = "52"

//    $http({ method: 'GET', url: '/api/' + companyNumber + '/AssignmentRule/GetAll' }).success(function (page, status, headers, config) {
//        $scope.AssignmentRules = page.AssignmentRule;
//        $scope.AssignmentRuleActions = page.AssignmentRuleAction;
//        $scope.FilterField = page.FilterField;
//        $scope.FilterOperator = page.FilterOperator;
//        $scope.AssignmentRules = TableService.CreatePager($scope.AssignmentRules, 15);


//        var showModal = function (item, isNew) {
//            var clonedItem = angular.copy(item);            

//            var pager = $scope.AssignmentRules;

//            modalInstance = $modal.open({
//                templateUrl: 'rule-item-modal',
//                controller: function ($scope, $modalInstance) {
//                    var originalCode = clonedItem.ID;

//                    clonedItem.AssignmentRuleActions = angular.copy(page.AssignmentRuleAction);
//                    clonedItem.FilterField = angular.copy(page.FilterField);
//                    clonedItem.FilterOperator = angular.copy(page.FilterOperator);
                    
//                    if (item.SurveyStatusType.Code = "S")
//                    {                       
//                        clonedItem.AssignmentRuleActions = $filter('filter')(clonedItem.AssignmentRuleActions, function (i) {
//                            return i.Code.charAt(0) === 'A';
//                        });
//                    }
//                    else
//                    {
//                        clonedItem.AssignmentRuleActions = $filter('filter')(clonedItem.AssignmentRuleActions, function (i) {
//                            return i.Code.charAt(0) === 'H';
//                        });
//                    }

//                    $scope.Item = clonedItem;
//                    $scope.ShowDelete = !isNew;
//                    $scope.SaveButtonText = (isNew) ? 'Add Item' : 'Update Item';
//                    $scope.Form = {};
//                    $scope.ValidationErrors = [];
//                    $scope.SavingItem = false;

               
//                    $scope.PopulateFilterValue = function (fieldcode)
//                    {

                      
//                        $http({ method: 'GET', url: '/api/' + companyNumber + '/AssignmentRule/FieldValues/' + fieldcode}).success(function (data, status, headers, config) {                        
                                                   
//                            clonedItem.FieldValue = data;

//                            }
//                        ).error(function (data, status, headers, config) {
//                            if (status == 400) {
//                                $scope.ValidationErrors = data;
//                            }                         
//                        });

//                    }

//                    $scope.Update = function () {
//                        $scope.SavingItem = true;
//                        //ValidationService.Reset($scope.Form.Rule);
//                        $scope.ValidationErrors = [];

//                        $http({ method: 'POST', url: '/api/' + companyNumber + '/AssignmentRule/' + ((isNew) ? '' : originalCode), data: $scope.Item }).success(function (rule, status, headers, config) {
//                            if (isNew) {
//                                pager.Items.push(rule);
//                                pager.Items.sort(function (a, b) { return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0); });
//                                pager.FilterChanged();
//                            }
//                            else {
//                                for (var i = 0; i < pager.Items.length; i++) {
//                                    if (pager.Items[i].ID == rule.ID) {
//                                        pager.Items[i] = rule;
//                                        pager.FilterChanged();
//                                        break;
//                                    };
//                                }
//                            }

//                            $scope.SavingItem = false;
//                            $modalInstance.close();
//                        }).error(function (data, status, headers, config) {
//                            if (status == 400) {
//                                $scope.ValidationErrors = data;
//                            }
//                            $scope.SavingItem = false;
//                        });
//                    };

//                    $scope.Cancel = function () { $modalInstance.dismiss('cancel'); };
//                }
//            });
//        };

//        $scope.NewRule = function () { showModal(page.NewRule, true); };
//        $scope.SelectRule = function (rule) { showModal(rule, false); };

//        $scope.PageLoading = false;
//    });
//});