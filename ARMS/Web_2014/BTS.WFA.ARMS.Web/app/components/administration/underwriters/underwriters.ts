﻿/// <reference path="../../../app.ts" />
module arms {
    'use strict';
    app.directive('underwriters',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/underwriters/underwriters.html',
            replace: true,
            scope: {
                id: '@'
            },
            bindToController: true,
            controller: UnderwriterController,
            controllerAs: 'vm'
        }
    });

    class UnderwriterController extends BaseController {
        SelectedUser: User;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, $q: ng.IQService,
            private UserService: UserService) {
            super($scope, $state, UtilityService);

            UserService.UserPromise.then((user) => {
                this.SelectedUser = user;
            });

        }
    }
}
//angular.module('arms').controller('UnderwriterCtrl', function ($scope, $http, $filter, $location, $modal, $stateParams, TableService, ValidationService) {
//    //var companyNumber = $stateParams.companyNumber;
//    var companyNumber = "52"

//    $http({ method: 'GET', url: '/api/' + companyNumber + '/Underwriter/GetAll' }).success(function (page, status, headers, config) {
       
//        $scope.Underwriters = page.Underwriters;
//        $scope.Underwriters = TableService.CreatePager(page.Underwriters, 15, function (fullList) {
//            var accountdisabledfilter = ($scope.AccountDisabledFilter === undefined || $scope.AccountDisabledFilter === false) ? false : true;
//            var namefilter = ($scope.NameFilter || '').toLowerCase();
//            return $filter('filter')(fullList, function (item) {
//                return item.Name.toLowerCase().indexOf(namefilter) !== -1 && item.Disabled === accountdisabledfilter;
//            });
//        });


//        var showModal = function (item, isNew) {
//            var clonedItem = angular.copy(item);
//            var pager = $scope.Underwriters;

//            modalInstance = $modal.open({
//                templateUrl: 'underwriter-item-modal',
//                controller: function ($scope, $modalInstance) {
//                    var originalCode = clonedItem.Code;
//                    clonedItem.ProfitCenters = angular.copy(page.ProfitCenters);                                                            
                    
//                    $scope.Item = clonedItem;
//                    if (isNew) $scope.Item.DomainName = "WRBTS";

//                    $scope.SaveButtonText = (isNew) ? 'Add Item' : 'Update Item';
//                    $scope.Form = {};
//                    $scope.ValidationErrors = [];
//                    $scope.SavingItem = false;
//                    $scope.Update = function () {
//                        $scope.SavingItem = true;
//                        //ValidationService.Reset($scope.Form.Underwriter);
//                        $scope.ValidationErrors = [];

//                        $http({ method: 'POST', url: '/api/' + companyNumber + '/Underwriter/' + ((isNew) ? '' : originalCode), data: $scope.Item }).success(function (underwriter, status, headers, config) {
//                            if (isNew) {
//                                pager.Items.push(underwriter);
//                                pager.Items.sort(function (a, b) { return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0); });
//                                pager.FilterChanged();
//                            }
//                            else {
//                                for (var i = 0; i < pager.Items.length; i++) {
//                                    if (pager.Items[i].ID == underwriter.ID) {
//                                        pager.Items[i] = underwriter;
//                                        pager.FilterChanged();
//                                        break;
//                                    };
//                                }
//                            }

//                            $scope.SavingItem = false;
//                            $modalInstance.close();
//                        }).error(function (data, status, headers, config) {
//                            if (status == 400) {
//                                ValidationService.SetValidationResults($scope.Form.underwriter, data);
//                                $scope.ValidationErrors = data;
//                            }
//                            $scope.SavingItem = false;
//                        });
//                    };
                    
//                    $scope.Cancel = function () { $modalInstance.dismiss('cancel'); };
//                }
//            });
//        };

//        $scope.NewUnderwriter = function () { showModal(page.NewUnderwriter, true); };
//        $scope.SelectUnderwriter = function (underwriter) { showModal(underwriter, false); };

//        $scope.PageLoading = false;
//    });

//});