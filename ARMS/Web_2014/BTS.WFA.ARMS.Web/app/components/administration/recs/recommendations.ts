﻿/// <reference path="../../../app.ts" />
module arms {
    'use strict';
    app.directive('recommendations',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/recs/recommendations.html',
            replace: true,
            scope: {
                id: '@'
            },
            bindToController: true,
            controller: RecommendationsController,
            controllerAs: 'vm'
        }
    });

    class RecommendationsController extends BaseController {
        SelectedUser: User;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, $q: ng.IQService,
            private UserService: UserService) {
            super($scope, $state, UtilityService);

            UserService.UserPromise.then((user) => {
                this.SelectedUser = user;
            });

        }
    }
}
//angular.module('arms').controller('RecsCtrl', function ($scope, $http, $filter, $location, $modal, $stateParams, TableService) {
//    //var companyNumber = $stateParams.companyNumber;
//    var companyNumber = "52"

//    $http({ method: 'GET', url: '/api/' + companyNumber + '/Recs/GetAll' }).success(function (page, status, headers, config) {
//        $scope.Recs = page.Recs;
        
//        $scope.Recs = TableService.CreatePager(page.Recs, 15, function (fullList) {
//            var disabledfilter = ($scope.DisabledFilter === undefined || $scope.DisabledFilter === false) ? false : true;
//            var namefilter = ($scope.NameFilter || '').toLowerCase();
//            return $filter('filter')(fullList, function (item) {
//                return item.Title.toLowerCase().indexOf(namefilter) !== -1 && item.Disabled === disabledfilter;
//            });
//        });

//        var showModal = function (item, isNew) {
//            var clonedItem = angular.copy(item);
//            var pager = $scope.Recs;

//            modalInstance = $modal.open({
//                templateUrl: 'rec-item-modal',
//                controller: function ($scope, $modalInstance) {
//                    var originalCode = clonedItem.ID;                 

//                    $scope.Item = clonedItem;
                    

//                    $scope.SaveButtonText = (isNew) ? 'Add Item' : 'Update Item';
//                    $scope.Form = {};
//                    $scope.ValidationErrors = [];
//                    $scope.SavingItem = false;
//                    $scope.Update = function () {
//                        $scope.SavingItem = true;
//                        //ValidationService.Reset($scope.Form.Rec);
//                        $scope.ValidationErrors = [];

//                        $http({ method: 'POST', url: '/api/' + companyNumber + '/Rec/' + ((isNew) ? '' : originalCode), data: $scope.Item }).success(function (rec, status, headers, config) {
//                            if (isNew) {
//                                pager.Items.push(rec);
//                                pager.Items.sort(function (a, b) { return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0); });
//                                pager.FilterChanged();
//                            }
//                            else {
//                                for (var i = 0; i < pager.Items.length; i++) {
//                                    if (pager.Items[i].ID == rec.ID) {
//                                        pager.Items[i] = rec;
//                                        pager.FilterChanged();
//                                        break;
//                                    };
//                                }
//                            }

//                            $scope.SavingItem = false;
//                            $modalInstance.close();
//                        }).error(function (data, status, headers, config) {
//                            if (status == 400) {
//                                ValidationService.SetValidationResults($scope.Form.rec, data);
//                                $scope.ValidationErrors = data;
//                            }
//                            $scope.SavingItem = false;
//                        });
//                    };

//                    $scope.Cancel = function () { $modalInstance.dismiss('cancel'); };
//                }
//            });
//        };

//        $scope.NewRec = function () { showModal(page.NewRec, true); };
//        $scope.SelectRec = function (rec) { showModal(rec, false); };

//        $scope.PageLoading = false;

//    });

//});