﻿/// <reference path="../../../app.ts" />
module arms {
    'use strict';
    app.directive('users',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/user/users.html',
            replace: true,
            controller: UsersController,
            controllerAs: 'vm'
        }
    });

    class UsersController extends BaseController {
        Roles: CodeItem[];
        User: User;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, private UserService: UserService) {
            super($scope, $state, UtilityService);

                //UtilService.SetPageTitle('User Administration');
                //DomainService.UserRoles(this.companyKey).then((data) => { this.Roles = data; });
                UserService.UserPromise.then((user) => { this.User = user; });

            //if ($state.is('ck.administration.users')) {
            //    UtilityService.RaiseFocusTrigger('UserSearchInput');
            //}
        }
    }
}
//angular.module('arms').controller('UserCtrl', function ($scope, $http, $filter, $location, $modal, $stateParams, TableService, ValidationService) {
//    //var companyNumber = $stateParams.companyNumber;
    
//    var companyNumber = "52"
//    var isFeeCompany=false;    
//    if ($location.path().contains('feecompanies')) {
//        $scope.isFeeCompany = true;
//    }
//    else {
//        $scope.isFeeCompany = false;
//    }

    

//    $http({ method: 'GET', url: '/api/' + companyNumber + '/User/GetAll/' + $scope.isFeeCompany }).success(function (page, status, headers, config) {
        
//        $scope.UserList = page.Users;
//        $scope.ProfitCenters = page.ProfitCenters;
//        $scope.States = page.States;
//        $scope.UserRoles = page.UserRoles;
//        $scope.Managers = page.Managers;      
//        $scope.UserList = $filter('orderBy')($scope.UserList, 'Name', false);       
        
//        //$scope.order = function (orderByField, reverseSort) {
//        //     $scope.UserList = $filter('orderBy')($scope.UserList, orderByField, reverseSort);           
//        //};          
      
//        $scope.UserList = TableService.CreatePager($scope.UserList, 15, function (fullList) {
//            var accountdisabledfilter = ($scope.AccountDisabledFilter === undefined || $scope.AccountDisabledFilter === false) ? false : true;            
//            var namefilter = ($scope.NameFilter || '').toLowerCase();
//            return $filter('filter')(fullList, function (item) {
//                return item.Name.toLowerCase().indexOf(namefilter) !== -1 && item.AccountDisabled === accountdisabledfilter;
//            });
//        });
      
         

       
        

//        var showModal = function (item, isNew, isFeeCompany) {
//            var clonedItem = angular.copy(item);            
//            var pager = $scope.UserList;
            
//            modalInstance = $modal.open({
//                templateUrl: 'user-item-modal',
//                controller: function ($scope, $modalInstance) {
//                    var originalCode = clonedItem.ID;
//                    clonedItem.ProfitCenters = angular.copy(page.ProfitCenters);
//                    clonedItem.Managers = angular.copy(page.Users);
//                    clonedItem.States = angular.copy(page.States);
//                    clonedItem.UserRoles = angular.copy(page.UserRoles);
//                    $scope.Item = clonedItem;
//                    $scope.isFeeCompany = isFeeCompany;
//                    $scope.ShowDelete = !isNew;
//                    $scope.SaveButtonText = (isNew) ? 'Add Item' : 'Update Item';                    
//                    $scope.Form = {};
//                    $scope.ValidationErrors = [];
//                    $scope.SavingItem = false;
//                    if (isNew) $scope.Item.DomainName = "WRBTS";
//                    $scope.Update = function () {
//                        $scope.SavingItem = true;
//                        ValidationService.Reset($scope.Form.User);
//                        $scope.ValidationErrors = [];
//                        $scope.Item.IsFeeCompany = isFeeCompany;
//                        $http({ method: 'POST', url: '/api/' + companyNumber + '/User/' + ((isNew) ? '' : originalCode), data: $scope.Item }).success(function (user, status, headers, config) {                            
//                            if (isNew) {                                
//                                pager.Items.push(user);
//                                pager.Items.sort(function (a, b) { return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0); });
//                                pager.FilterChanged();
//                            }
//                            else {                               
//                                for (var i = 0; i < pager.Items.length; i++) {
//                                    if (pager.Items[i].ID == user.ID) {
//                                        pager.Items[i] = user;
//                                        pager.FilterChanged();
//                                        break;
//                                    };
//                                }
//                            }

//                            $scope.SavingItem = false;
//                            $modalInstance.close();
//                        }).error(function (data, status, headers, config) {
//                            if (status == 400) {
//                                ValidationService.SetValidationResults($scope.Form.user, data);
//                                $scope.ValidationErrors = data;
//                            }
//                            $scope.SavingItem = false;
//                        });
//                    };
             
//                    $scope.Cancel = function () { $modalInstance.dismiss('cancel'); };
//                }
//            });
//        };
        
//        $scope.NewUser = function () { showModal(page.NewUser, true, $scope.isFeeCompany); };
//        $scope.SelectUser = function (user) { showModal(user, false, $scope.isFeeCompany); };

//        $scope.PageLoading = false;
//    });

//});
