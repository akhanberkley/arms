﻿/// <reference path="../../../app.ts" />
module arms {
    'use strict';
    app.directive('user',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/user/user.html',
            replace: true,
            scope: {
                id: '@'
            },
            bindToController: true,
            controller: UserController,
            controllerAs: 'vm'
        }
    });

    class UserController extends BaseController {
        SelectedUser: User;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, $q: ng.IQService,
            private UserService: UserService) {
            super($scope, $state, UtilityService);

            UserService.UserPromise.then((user) => {
                this.SelectedUser = user;
            });

        }
    }
}