﻿angular.module('arms').controller('DocumentCtrl', function ($scope, $http, $filter, $location, $modal, $stateParams, TableService, ValidationService) {
    //var companyNumber = $stateParams.companyNumber;
    var companyNumber = "52";
    var documentType = 0;
    if ($location.path().contains('document')) {
        $scope.documentType = 3;
    }
    else if ($location.path().contains('report')) {
        $scope.documentType = 1;
    }
    else {
        $scope.documentType = 2;
    }



    $http({ method: 'GET', url: '/api/' + companyNumber + '/Documents/GetAll/' + $scope.documentType }).success(function (page, status, headers, config) {
        $scope.Documents = page.Documents;
        $scope.DocumentTypes = page.DocumentTypes;        
        $scope.Documents = TableService.CreatePager(page.Documents, 15, function (fullList) {
            var disabledFilter = ($scope.DisabledFilter === undefined || $scope.DisabledFilter === false) ? false : true;            
            return $filter('filter')(fullList, function (item) {
                return item.Disabled === disabledFilter;
            });
        });

        var showModal = function (item, isNew, documentType) {
            var clonedItem = angular.copy(item);
            var pager = $scope.Documents;

            modalInstance = $modal.open({
                templateUrl: 'document-item-modal',
                controller: function ($scope, $modalInstance) {
                    var originalCode = clonedItem.ID;
                    //clonedItem.Documents = angular.copy(page.DocumentVersions);

                    $scope.Item = clonedItem;
                    $scope.documentType = documentType;
                    $scope.SaveButtonText = (isNew) ? 'Add Item' : 'Update Item';
                    $scope.Form = {};
                    $scope.ValidationErrors = [];
                    $scope.SavingItem = false;
                    $scope.Update = function () {
                        $scope.SavingItem = true;
                        //ValidationService.Reset($scope.Form.Document);
                        $scope.ValidationErrors = [];

                        $http({ method: 'POST', url: '/api/' + companyNumber + '/Document/' + ((isNew) ? '' : originalCode), data: $scope.Item }).success(function (document, status, headers, config) {
                            if (isNew) {
                                pager.Items.push(document);
                                pager.Items.sort(function (a, b) { return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0); });
                                pager.FilterChanged();
                            }
                            else {
                                for (var i = 0; i < pager.Items.length; i++) {
                                    if (pager.Items[i].ID == document.ID) {
                                        pager.Items[i] = document;
                                        pager.FilterChanged();
                                        break;
                                    };
                                }
                            }

                            $scope.SavingItem = false;
                            $modalInstance.close();
                        }).error(function (data, status, headers, config) {
                            if (status == 400) {
                                ValidationService.SetValidationResults($scope.Form.document, data);
                                $scope.ValidationErrors = data;
                            }
                            $scope.SavingItem = false;
                        });
                    };

                    $scope.Cancel = function () { $modalInstance.dismiss('cancel'); };
                }
            });
        };

        $scope.NewDocument = function () { showModal(page.NewDocument, true, $scope.documentType); };
        $scope.SelectDocument = function (document) { showModal(document, false, $scope.documentType); };

        $scope.PageLoading = false;

    });

});


