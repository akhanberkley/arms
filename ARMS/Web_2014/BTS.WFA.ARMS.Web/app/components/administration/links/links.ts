﻿/// <reference path="../../../app.ts" />
module arms {
    'use strict';
    app.directive('links',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/links/links.html',
            replace: true,
            scope: {
                id: '@'
            },
            bindToController: true,
            controller: LinksController,
            controllerAs: 'vm'
        }
    });

    interface Link {
        Id: string;
        Description: string;
        URL: string;
        DisplayOrder: number;
    }

    class LinksController extends BaseController {
        SelectedUser: User;
        Links: Link[];

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, $q: ng.IQService, $http: ng.IHttpService,
            private UserService: UserService) {
            super($scope, $state, UtilityService);

            UserService.UserPromise.then((user) => {
                this.SelectedUser = user;
            });

            this.UtilityService.SetPageTitle('Manage Links');

            $http.get('/api/' + this.companyKey + '/Links').then((response: ng.IHttpPromiseCallbackArg<Link[]>) => {
                this.Links = response.data;

                console.log(this.Links);
            });

        }

        NewLink() {
            alert("Open New Link Modal");
        } 

        SelectLink(link: Link) {
            console.log(link);
        }

        EditLink(link: Link) {
            alert("Edit selected link");
            //console.log(link.Description);
        }

        DeleteLink(link: Link) {
            if (confirm('Are you sure you want to delete this item?')) {
                alert("Delete selected link");
                //console.log(link.Description);
            }
        }

        OpenModal() {
            alert("Open a modal");
        }
    }
}
//angular.module('arms').controller('LinkCtrl', function ($scope, $http, $filter, $location, $modal, $stateParams, TableService) {
//    //var companyNumber = $stateParams.companyNumber;
//    var companyNumber = "52"

//    $http({ method: 'GET', url: '/api/' + companyNumber + '/Link/GetAll' }).success(function (page, status, headers, config) {
//        $scope.Links = page.Links;
//        $scope.Links = TableService.CreatePager(page.Links, 15, function (fullList) {
//            var namefilter = ($scope.NameFilter || '').toLowerCase();
//            return $filter('filter')(fullList, function (item) {
//                return item.Description.toLowerCase().indexOf(namefilter) !== -1;
//            });
//        });
//        var showModal = function (item, isNew) {
//            var clonedItem = angular.copy(item);
//            var pager = $scope.Links;

//            modalInstance = $modal.open({
//                templateUrl: 'link-item-modal',
//                controller: function ($scope, $modalInstance) {
//                    var originalCode = clonedItem.ID;

//                    $scope.Item = clonedItem;

//                    $scope.ShowDelete = (isNew) ? false : true;
//                    $scope.SaveButtonText = (isNew) ? 'Add Item' : 'Update Item';
//                    $scope.Form = {};
//                    $scope.ValidationErrors = [];
//                    $scope.SavingItem = false;
//                    $scope.Update = function () {
//                        $scope.SavingItem = true;
//                        //ValidationService.Reset($scope.Form.Link);
//                        $scope.ValidationErrors = [];

//                        $http({ method: 'POST', url: '/api/' + companyNumber + '/Link/' + ((isNew) ? '' : originalCode), data: $scope.Item }).success(function (link, status, headers, config) {
//                            if (isNew) {
//                                pager.Items.push(link);
//                                pager.Items.sort(function (a, b) { return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0); });
//                                pager.FilterChanged();
//                            }
//                            else {
//                                for (var i = 0; i < pager.Items.length; i++) {
//                                    if (pager.Items[i].ID == link.ID) {
//                                        pager.Items[i] = link;
//                                        pager.FilterChanged();
//                                        break;
//                                    };
//                                }
//                            }

//                            $scope.SavingItem = false;
//                            $modalInstance.close();
//                        }).error(function (data, status, headers, config) {
//                            if (status == 400) {
//                                ValidationService.SetValidationResults($scope.Form.link, data);
//                                $scope.ValidationErrors = data;
//                            }
//                            $scope.SavingItem = false;
//                        });
//                    };
//                    $scope.Delete = function () {
//                        if (confirm('Are you sure you want to delete this item?')) {
//                            $scope.SavingItem = true;
//                            //ValidationService.Reset($scope.Form.user);
//                            $scope.ValidationErrors = [];                          
//                            $http({ method: 'DELETE', url: '/api/' + companyNumber + '/Link/' + originalCode }).success(function (link, status, headers, config) {                                                              
//                                $scop.addAlert('Deleted Successfully !', 'alert-success');
//                                $scope.SavingItem = false;
//                                $modalInstance.close();
//                            }).error(function (data, status, headers, config) {
//                                if (status == 400) {
//                                    //ValidationService.SetValidationResults($scope.Form.user, data);
//                                    $scope.ValidationErrors = data;
//                                }
//                                $scope.SavingItem = false;
//                            });

//                        }
//                    };
//                    $scope.Cancel = function () { $modalInstance.dismiss('cancel'); };
//                }
//            });
//        };

//        $scope.NewLink = function () { showModal(page.NewLink, true); };
//        $scope.SelectLink = function (link) { showModal(link, false); };

//        $scope.PageLoading = false;

//    });

//});


