﻿/// <reference path="../../../app.ts" />
module arms {
    'use strict';
    app.directive('territory',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/territories/territory.html',
            replace: true,
            scope: {
                id: '@'
            },
            bindToController: true,
            controller: TerritoryController,
            controllerAs: 'vm'
        }
    });

    class TerritoryController extends BaseController {
        SelectedUser: User;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, $q: ng.IQService,
            private UserService: UserService) {
            super($scope, $state, UtilityService);

            UserService.UserPromise.then((user) => {
                this.SelectedUser = user;
            });

        }
    }
}
//angular.module('arms').controller('TerritoryCtrl', function ($scope, $http, $filter, $location, $modal, $stateParams, TableService) {
//    //var companyNumber = $stateParams.companyNumber;
//    var companyNumber = "52"

//    $http({ method: 'GET', url: '/api/' + companyNumber + '/Territory/GetAll' }).success(function (page, status, headers, config) {
//        $scope.Territories = page.Territory;
//        $scope.Users = page.User;
//        $scope.Territories = TableService.CreatePager(page.Territory, 15, function (fullList) {
//            var surveyfilter = ($scope.SurveyFilter || '');            
//            return $filter('filter')(fullList, function (item) {
//                return item.SurveyStatusType.Code == surveyfilter;
//            });
//        });

//        var showModal = function (item, isNew) {
//            var clonedItem = angular.copy(item);
//            var pager = $scope.Territories;

//            modalInstance = $modal.open({
//                templateUrl: 'territory-item-modal',
//                controller: function ($scope, $modalInstance) {
//                    var originalCode = clonedItem.ID;

//                    $scope.Item = clonedItem;
//                    clonedItem.Users = angular.copy(page.User);
                   
//                    $scope.Percent = ($scope.Item.UserTerritories[0].PercentAllocated != null) ? true : false;

//                    $scope.ShowDelete = (isNew) ? false : true;
//                    $scope.SaveButtonText = (isNew) ? 'Add Item' : 'Update Item';
//                    $scope.Form = {};
//                    $scope.ValidationErrors = [];
//                    $scope.SavingItem = false;

//                    $scope.AddUser = function (ID) {

                        
                        
//                        var u = $filter('filter')(clonedItem.Users, function (item) {                         
//                            return item.ID === ID;
//                        });

//                        //$scope.Item.UserTerritories.User.push(u[0]);
                        
                        
//                    };

//                    $scope.Update = function () {
//                        $scope.SavingItem = true;
//                        //ValidationService.Reset($scope.Form.Territory);
//                        $scope.ValidationErrors = [];

//                        $http({ method: 'POST', url: '/api/' + companyNumber + '/Territory/' + ((isNew) ? '' : originalCode), data: $scope.Item }).success(function (territory, status, headers, config) {
//                            if (isNew) {
//                                pager.Items.push(territory);
//                                pager.Items.sort(function (a, b) { return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0); });
//                                pager.FilterChanged();
//                            }
//                            else {
//                                for (var i = 0; i < pager.Items.length; i++) {
//                                    if (pager.Items[i].ID == territory.ID) {
//                                        pager.Items[i] = territory;
//                                        pager.FilterChanged();
//                                        break;
//                                    };
//                                }
//                            }

//                            $scope.SavingItem = false;
//                            $modalInstance.close();
//                        }).error(function (data, status, headers, config) {
//                            if (status == 400) {
//                                ValidationService.SetValidationResults($scope.Form.territory, data);
//                                $scope.ValidationErrors = data;
//                            }
//                            $scope.SavingItem = false;
//                        });
//                    };
//                    $scope.Delete = function () {
//                        if (confirm('Are you sure you want to delete this item?')) {
//                            $scope.SavingItem = true;
//                            //ValidationService.Reset($scope.Form.user);
//                            $scope.ValidationErrors = [];
//                            $http({ method: 'DELETE', url: '/api/' + companyNumber + '/Territory/' + originalCode }).success(function (territory, status, headers, config) {
//                                $scop.addAlert('Deleted Successfully !', 'alert-success');
//                                $scope.SavingItem = false;
//                                $modalInstance.close();
//                            }).error(function (data, status, headers, config) {
//                                if (status == 400) {
//                                    //ValidationService.SetValidationResults($scope.Form.user, data);
//                                    $scope.ValidationErrors = data;
//                                }
//                                $scope.SavingItem = false;
//                            });

//                        }
//                    };
//                    $scope.Cancel = function () { $modalInstance.dismiss('cancel'); };
//                }
//            });
//        };

//        $scope.NewTerritory = function () { showModal(page.NewTerritory, true); };
//        $scope.SelectTerritory = function (territory) { showModal(territory, false); };

//        $scope.PageLoading = false;


//    });

//});