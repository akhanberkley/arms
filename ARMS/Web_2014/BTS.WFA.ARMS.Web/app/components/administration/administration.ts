﻿/// <reference path="../../app.ts" />
module arms {
    'use strict';

    app.directive('administration',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/administration/administration.html',
            replace: true,
            bindToController: true,
            controller: AdministrationController,
            controllerAs: 'vm'
        }
    });

    class AdministrationController extends BaseController {
        User: User;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, UserService: UserService) {
            super($scope, $state, UtilityService);

            this.UtilityService.SetPageTitle('Administration');
            UserService.UserPromise.then((user) => {
                this.User = user;
            });
        }
    }
}