﻿/// <reference path="../../app.ts" />
module arms {
    'use strict';

    app.directive('dashboard',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/dashboard/dashboard.html',
            replace: true,
            bindToController: true,
            controller: DashboardController,
            controllerAs: 'vm'
        }
    });

    class DashboardController extends BaseController {

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService) {
            super($scope, $state, UtilityService);

            this.UtilityService.SetPageTitle('Dashboard');

        }
    }
}