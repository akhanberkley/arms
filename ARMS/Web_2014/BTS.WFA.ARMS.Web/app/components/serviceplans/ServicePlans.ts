﻿/// <reference path="../../app.ts" />
module arms {
    'use strict';

    app.directive('servicePlans',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/serviceplans/ServicePlans.html',
            replace: true,
            bindToController: true,
            controller: ServicePlansController,
            controllerAs: 'vm'
        }
    });

    class ServicePlansController extends BaseController {
        pageHeading: string;
        pageSubheading: string;
        
        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService) {
            super($scope, $state, UtilityService);

            this.pageHeading = 'Service Plans';
            this.pageSubheading = 'This is the Service Plans summary page';
            this.companyKey = $state.params['companyKey'];

            // service call
            
        }
    }
}
//angular.module('arms').controller('ServicePlanCtrl', function ($scope, $http, $filter, $location, $modal, $stateParams, TableService) {
//    //var companyNumber = $stateParams.companyNumber;
//    var companyNumber = "52"
//    $scope.PageHeading = 'Service Plans'
//    $scope.Message = 'Service Plans Page'
//    $http({ method: 'GET', url: '/api/' + companyNumber + '/ServicePlan/GetAll' }).success(function (page, status, headers, config) {
//        $scope.ServicePlans = page.ServicePlans;
//        $scope.Users = page.Users;
//        $scope.ServicePlanStatus = page.ServicePlanStatus;       
//        $scope.ServicePlans = TableService.CreatePager(page.ServicePlans, 15, function (fullList) {          
//             return $filter('filter')(fullList, function (item) {                             
//                return (item.AssignedUser != null && item.AssignedUser.ID === $scope.user) && (item.ServicePlanStatusCode === $scope.StatusFilter) //&& ($scope.InsuredNameFilter != '' && item.Insured.Name.toLowerCase().indexOf($scope.InsuredNameFilter.toLowerCase()) !== -1) //&& (item.Number != '' && item.Number.toString().indexOf($scope.NumberFilter) !== -1);
//            });
//        });
//    });
//});