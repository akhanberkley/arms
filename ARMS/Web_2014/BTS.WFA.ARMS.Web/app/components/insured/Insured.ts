﻿/// <reference path="../../app.ts" />
module arms {
    'use strict';

    app.directive('insured',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/insured/insured.html',
            replace: true,
            bindToController: true,
            controller: InsuredController,
            controllerAs: 'vm'
        }
    });

    class InsuredController extends BaseController {

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService) {
            super($scope, $state, UtilityService);

            // service call
            
        }
    }
}
//angular.module('arms').controller('InsuredCtrl', function ($scope, $http, $filter, $location, $modal, $stateParams, TableService) {
//    //var companyNumber = $stateParams.companyNumber;
//    var companyNumber = "52"
//    //alert($stateParams.servicePlanID);
//    var servicePlanID = $stateParams.servicePlanID;


//    $http({ method: 'GET', url: '/api/' + companyNumber + '/Insured/' + servicePlanID }).success(function (data, status, headers, config) {

//        $scope.ServicePlan = data;
//        $scope.Policies = data.Insured.Policy;
//        $scope.Locations = data.Insured.Location;
        
            
       
        
//        $scope.Policies = TableService.CreatePager($scope.Policies, 10);
//        $scope.Locations = TableService.CreatePager($scope.Locations, 10);

//        $scope.Claims = [];
//        for (var p = 0 ; p < $scope.Policies.Items.length ; p++) {

//            for (var c = 0; c < $scope.Policies.Items[p].Claim.length ; c++) {
//                $scope.Claims.push($scope.Policies.Items[p].Claim[c]);                
//            }
//        }        
//        $scope.Claims = TableService.CreatePager($scope.Claims, 10);


//    }).error(function (data, status, headers, config) {

//    });

//});