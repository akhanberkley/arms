﻿/// <reference path="../app.ts" />
module arms {
    'use strict';

    export interface AppState {
        Loading: boolean;
        PageGenerating: boolean;
        StillWaiting: boolean;
    }

    export class UtilityService {
        AppState: AppState;

        constructor(private $filter: ng.IFilterService, private $timeout: ng.ITimeoutService, private $rootScope: ng.IRootScopeService,
            private $window: ng.IWindowService, private $state: ng.ui.IStateService, private $http: ng.IHttpService,
            private $q: ng.IQService, private $animate: ng.IAnimateService/*, private AlertService: AlertService*/) {

            this.AppState = { Loading: false, PageGenerating: false, StillWaiting: false };
        }

        SetPageTitle(title: string) {
            this.$window.document.title = ((title) ? title + ' - ' : '') + 'Berkley Loss Control';
        }

        BrowserIsInternetExplorer() {
            return (this.$window.navigator.userAgent.indexOf('MSIE ') >= 0 || this.$window.navigator.userAgent.indexOf('Trident/') >= 0);
        }

        ODataString(value: string) { return "'" + value.replace(/'/g, "''").replace(/&/g, "%26").replace(/#/g, "%23") + "'"; }
        CreateSaveRequest(item: any, url: string, itemId: any = null, params: any = null) {
            var config = <ng.IRequestConfig>{};
            config.params = params;
            return (itemId == null) ? this.$http.post(url, item, config) : this.$http.put(url + '/' + itemId, item, config);
        }

        CreateValidationResultsFromServerResponse(statusCode: number, data: any) {
            var results = <ValidationResult[]>[];
            if (statusCode == 500) {
                var exception = <ServerException>data;
                var valResult = { Property: 'Server Error', Message: 'Server Error' };
                if (exception.ExceptionMessage) {
                    valResult.Message += ': ' + exception.ExceptionMessage;
                }
                results.push(valResult);
            } else if (statusCode == 400) {
                results = <ValidationResult[]>data;
            }

            return results;
        }
    }

    app.service('UtilityService', UtilityService);
}