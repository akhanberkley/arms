﻿/// <reference path="../app.ts" />
module arms {
    'use strict';

    app.factory('$exceptionHandler', function ($log: ng.ILogService, $injector: ng.auto.IInjectorService) {
        var $http: ng.IHttpService;
        var AlertService: AlertService;
        return function (exception: any, cause: any) {
            $log.error(exception);

            $http = $http || <ng.IHttpService>$injector.get('$http');
            $http.post('/api/WebClient/LogJavascriptException', { Exception: exception.stack, Cause: '' + cause });

            AlertService = AlertService || <AlertService>$injector.get('AlertService');
            AlertService.Notify(NotificationType.Error, 'Whoops!  Something unexpected happened.  The issue has been logged.');
        };
    });
}