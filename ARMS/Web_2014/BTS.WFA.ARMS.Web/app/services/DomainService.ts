﻿/// <reference path="../app.ts" />
module arms {
    'use strict';
    export class DomainService {
        private cachedPromises: any;
        constructor(private $http: ng.IHttpService, private $q: ng.IQService, private UtilityService: UtilityService) {
            this.cachedPromises = {};
        }

        private getCachedPromise<T>(url: string, customLogic: (items: T) => void = null) {
            var promise = <ng.IPromise<T>>this.cachedPromises[url];
            if (promise == null) {
                promise = this.cachedPromises[url] = this.$http.get(url).then((response) => {
                    var items = <T>response.data;
                    if (customLogic) {
                        customLogic(items);
                    }
                    return items;
                });
            }

            return promise;
        }

        ResetCache() {
            this.cachedPromises = {};
        }
        //CompanyInformation(companyKey: string) {
        //    return this.getCachedPromise<CompanySettings>('/api/Company/' + companyKey + '/Settings');
        //}
        ImpersonatableCompanies() {
            return this.getCachedPromise<ImpersonatableCompany[]>('/api/Domains/ImpersonatableCompany');
        }

        States() {
            return this.getCachedPromise<State[]>('/api/Domains/State');
        }

        Underwriters(companyKey: string) {
            return this.getCachedPromise<Contact[]>(`/api/${companyKey}/Domains/Underwriter`);
        }

        LinesOfBusiness(companyKey: string): ng.IPromise<CodeItem[]> {
            return this.getCachedPromise<CodeItem[]>(`/api/${companyKey}/Domains/LineOfBusiness`);
        }

        SurveyReasons(companyKey: string) {
            return this.getCachedPromise<string[]>(`/api/${companyKey}/Domains/SurveyReason`);
        }

        ProfitCenters(companyKey: string): ng.IPromise<CodeItem[]> {
            return this.getCachedPromise<CodeItem[]>(`/api/${companyKey}/Domains/ProfitCenter`);
        }
    }

    app.service('DomainService', DomainService);
}