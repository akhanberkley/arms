﻿/// <reference path="../app.ts" />
module arms {
    'use strict';

    export class UserService {
        public UserPromise: ng.IPromise<User>;

        constructor(private $rootScope: AppRootScope, private $http: ng.IHttpService, private $q: ng.IQService) {

            this.UserPromise = new $q((resolve, reject) => {
                //Wait until we've been logged in before getting the user; we are doing this separately opposed to allowing the Index view to authenticate
                //because the ADFS redirect would cause us to lose the full link if a user just clicked on a link from another system and wasn't logged in
                var loginListener = $rootScope.$on('Login-Completed',() => {
                    this.getCurrentUserPromise().then((user) => {
                        resolve(user);
                    });

                    loginListener();//remove the listener
                });
            });
        }

        private getCurrentUserPromise() {
            return new this.$q((resolve, reject) => {
                this.$http.get('/api/User/Current').then((response: ng.IHttpPromiseCallbackArg<User>) => {
                    var data = response.data;

                    data.AdministratorLinks = [];
                    if (data.IsSystemAdministrator || data.Role.GeneralPermissions.AdminUserAccounts)
                        data.AdministratorLinks.push(new AdministrationLink('Users', 'ck.administration.users'));
                    if (data.IsSystemAdministrator || data.Role.GeneralPermissions.AdminUserRoles)
                        data.AdministratorLinks.push(new AdministrationLink('User Roles', 'ck.administration.roles'));
                    if (data.IsSystemAdministrator || data.Role.GeneralPermissions.AdminLinks)
                        data.AdministratorLinks.push(new AdministrationLink('Links', 'ck.administration.links'));

                    resolve(data);
                });
            });
        }

        SetImpersonatedUserName(userName: string) {
            this.$rootScope['ImpersonatedUserName'] = userName;
            this.UserPromise = this.getCurrentUserPromise();
        }
        IsImpersontating() {
            return (this.$rootScope['ImpersonatedUserName'] != null);
        }
    }

    app.service('UserService', UserService);
}