﻿/// <reference path="../app.ts" />
module arms {
    'use strict';

    export class RequestQueueService {

        constructor(public $http: ng.IHttpService, private $q: ng.IQService, private UtilityService: UtilityService) { }

        RequestTypeCodes(): CodeItem[] {
            return [{ Code: "PN", Description: "Policy #" },
                { Code: "SN", Description: "Submission #" },
                { Code: "CI", Description: "Client Id" }];
        }

        CreateRequest(companyKey: string, typeCode: string, value: string): ng.IPromise<RequestSaveResult> {
            return new this.$q((resolve, reject) => {
                var request = this.UtilityService.CreateSaveRequest({}, '/api/' + companyKey + '/Requests', null, { typeCode: typeCode, value: value });
                request.success((data, status, headers, config) => {
                    resolve({ Request: data });
                }).error((data, status, headers, config) => {
                    resolve({ ValidationErrors: this.UtilityService.CreateValidationResultsFromServerResponse(status, data) });
                });
            });
        }
    }

    app.service('RequestQueueService', RequestQueueService);
}