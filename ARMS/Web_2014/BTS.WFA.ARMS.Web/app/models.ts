﻿/// <reference path="app.ts" />
module arms {
    'use strict';

    export class BaseController {
        protected companyKey: string;
        protected AppState: AppState;

        constructor(protected $scope: ng.IScope, protected $state: ng.ui.IStateService, protected UtilityService: UtilityService) {
            this.companyKey = $state.params['companyKey'];
            this.AppState = UtilityService.AppState;
        }
    }

    export interface AppRootScope extends ng.IRootScopeService {
        appVersion: string;
    }

    export class AdministrationLink {
        constructor(public name: string, public state: string) { }
    }

    export interface CodeItem {
        Code: string;
        Description: string;
    }

    export interface Company {
        CompanyName: string;
        CompanyNumber: string;
        Abbreviation: string;
        LongAbbreviation: string;
    }

    export interface ImpersonatableCompany {
        Company: Company;
        Users: CodeItem[];
    }

    export interface State {
        StateName: string;
        Abbreviation: string;
    }

    export interface ValidationResult {
        Message: string;
        Property: string;
    }
    export interface ServerException {
        ExceptionMessage: string;
        StackTrace: string;
    }

    export interface User {
        UserName: string;
        Company: Company;
        Role: UserRole;
        Address: Address;
        IsSystemAdministrator: boolean;
        IsFeeCompany: boolean;
        IsUnderwriter: boolean;
        AdministratorLinks: AdministrationLink[];
    }
    export interface UserSaveResult {
        User?: User;
        ValidationErrors?: ValidationResult[];
    }
    export interface UserRole {
        Name: string;
        Description: string;
        GeneralPermissions: UserRoleGeneralPermissions;
        SurveyStatusPermissions: any[];
    }

    export interface UserRoleGeneralPermissions {
        ViewCompanySummary: boolean;
        SearchSurveys: boolean;
        CreateSurveys: boolean;
        CreateServicePlans: boolean;
        ViewSurveyReviewQueues: boolean;
        ViewLinks: boolean;
        ViewSurveyHistory: boolean;
        RequireSavedSearch: boolean;
        AdminUserRoles: boolean;
        AdminUserAccounts: boolean;
        AdminFeeCompanies: boolean;
        AdminTerritories: boolean;
        AdminRules: boolean;
        AdminLetters: boolean;
        AdminReports: boolean;
        AdminServicePlanTemplates: boolean;
        AdminResultsDisplay: boolean;
        AdminRecommendations: boolean;
        AdminLinks: boolean;
        AdminUnderwriters: boolean;
        AdminClients: boolean;
        AdminActivityTimes: boolean;
        AdminHelpfulHints: boolean;
    }

    export interface Address {
        Address1: string;
        Address2: string;
        City: string;
        State: string;
        PostalCode: string;
    }

    export interface Request {
        CompanyId: string;
        StatusCode: string;
        InputTypeCode: string;
        InputValue: string;
        CreateDate: any;
        CreateUser?: string;
        LastModifiedDate?: any;
        LastModifiedUser?: string;
        LastRetryDate?: any;
        LastRetryCount: number;
        TrashDate?: any;
        DataBCS: string; // should be booleans
        DataClientCore: string;
        DataGenesys: string;
        DataAPS: string;
        DataAPlus: string;
        DataBUS: string;
        Survey: Survey;
    }
    export interface RequestSaveResult {
        Request?: Request;
        ValidationErrors?: ValidationResult[];
    }

    export interface Contact {
        Name: string;
        Phone: string;
        Email: string;
    }
    export interface Agency {
        Code: string;
        Name: string;
        Address: Address;
        Phone: string;
        Fax: string;
    }

    export interface Survey {
        Number: number;
        DueDate: Date;
        SurveyReasons: string[];
        Insured: SurveyInsured;
        Underwriter: Contact;
        Policies: SurveyPolicy[];
        Locations: SurveyLocation[];
        Documents: SurveyDocument[];
        Comments: SurveyComment[];
    }
    export interface SurveyInsured {
        Id: string;
        Name: string;
        AdditionalName: string;
        Address: Address;
        PrimaryContact: Contact;
        Agency: Agency;
        Agent: Contact;
    }
    export interface SurveyComment {
        Id?: number;
        Author?: string;
        Date?: Date;
        Text: string;
        Internal: boolean;
    }
    export interface SurveyPolicy {
        LineOfBusiness: CodeItem;
        PolicySymbol: string;
        Number: string;
        EffectiveDate: Date;
        ExpirationDate: Date;
        BranchCode: string;
        ProfitCenter: CodeItem;
        Carrier: string;
        PrimaryState: string;
        Premium: number;
        HazardGrade: number;
        Coverages: SurveyCoverage[];
        CoverageIncludedCount?: number;
        Expanded?: boolean;
    }
    export interface SurveyCoverage {
        Included: boolean;
        Name: CodeItem;
        Items: SurveyCoverageItem[];
    }
    export interface SurveyCoverageItem {
        Type: CodeItem;
        Value: string;
    }
    export interface SurveyLocation {
        Included: boolean;
        Number: number;
        Description: string;
        Address: Address;
        Buildings: SurveyBuilding[];
        Contacts: Contact[];
        Comments: SurveyComment[];
    }
    export interface SurveyBuilding {
        Included: boolean;
        Number: number;
        YearBuilt: number;
        Sprinkler: boolean;
        SprinklerCredit: boolean;
        PublicProtection: boolean;
        Value: number;
    }
    export interface SurveyDocument {
        Included: boolean;
        DocumentType: string;
        FileName: string;
        EntryDate: Date;
        Number: string;
        PolicyNumber: string;
        PolicyType: string;
        Remarks: string;
    }
}