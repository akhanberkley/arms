﻿/// <reference path="../app.ts" />
module arms {
    'use strict';
    app.filter('YesNo', function () {
        return function (value: boolean) {
            return (value) ? 'Yes' : 'No';
        };
    });
} 