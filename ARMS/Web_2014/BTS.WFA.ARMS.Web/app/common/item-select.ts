﻿/// <reference path="../app.ts" />
module arms {
    'use strict';
    export interface ItemSelectOptions {
        source(term: string): ng.IPromise<any[]>;
        multiSelect?: boolean;
        minLength?: number;
        display?: string;
        tooltip?: string;
        matchTemplate?: string;
        delay?: number;
        appendToBody?: boolean;
        expand? (model: any): void;
        onBlur? (): void;
    }

    app.directive('itemSelect', function (): ng.IDirective {
        return {
            restrict: 'A',
            require: ['ngModel', 'itemSelect'],
            scope: {
                Options: '=itemSelect'
            },
            link: function (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, ctrls: any[]) {
                (<ItemSelectController>ctrls[1]).ngModel = <ng.INgModelController>ctrls[0];
            },
            bindToController: true,
            controller: ItemSelectController,
            controllerAs: 'vm'
        };
    });

    class ItemSelectController {
        Options: ItemSelectOptions;
        DisplayCompiled: ng.ICompiledExpression;
        TooltipCompiled: ng.ICompiledExpression;
        MatchTemplateElement: JQuery;

        Expand: () => void;
        ngModel: ng.INgModelController;

        Input: JQuery;
        Anchor: JQuery;
        AnchorSpan: JQuery;
        AnchorSpanLabel: JQuery;
        AnchorButton: JQuery;
        Menu: JQuery;
        MenuItems: JQuery[];
        MouseInMenu: boolean;

        SearchedTerm: string;
        SelectedMatch: JQuery;
        SearchTimeout: ng.IPromise<any>;

        constructor(private $scope: ng.IScope, private $element: ng.IRootElementService, $attrs: ng.IAttributes, $parse: ng.IParseService, private $timeout: ng.ITimeoutService,
            private UtilityService: UtilityService, private $position: ng.ui.bootstrap.IPositionService, private $document: ng.IDocumentService,
            private $compile: ng.ICompileService, $templateCache: ng.ITemplateCacheService) {

            //Create the functions to determine read-only display values
            this.DisplayCompiled = (this.Options.display == null) ? null : $parse(this.Options.display);
            this.TooltipCompiled = (this.Options.tooltip == null) ? null : $parse(this.Options.tooltip);

            //Create template menu item
            this.MatchTemplateElement = angular.element('<li><a class="item-select-option">' + $templateCache.get(this.Options.matchTemplate) + '</a></li>');

            //Setup DOM
            $element.addClass('item-select');
            this.setupDOM();

            //The following attributes on the root element should be propagated to the input
            $attrs.$observe('tabindex',(tabindex) => { this.Input.attr('tabindex', tabindex); });
            $attrs.$observe('placeholder',(tabindex) => { this.Input.attr('placeholder', tabindex); });

            //Watches
            $scope.$watch(() => this.Options.expand,(expand: (model: any) => void) => {
                if (expand != null) {
                    this.Expand = () => expand(this.ngModel.$modelValue);
                    this.AnchorSpanLabel.on('click',() => this.Expand());
                    this.AnchorSpanLabel.addClass('expand-link');
                } else {
                    this.AnchorSpanLabel.off();
                    this.AnchorSpanLabel.removeClass('expand-link');
                }
            });

            if (this.Options.multiSelect) {
                this.Anchor.hide();
            } else {
                $scope.$watch(() => this.ngModel.$viewValue,(val: any) => {
                    if (val != null) {
                        this.Input.hide();
                        this.Anchor.show();

                        var description = (this.Options.display) ? this.DisplayCompiled({ i: val }) : val;
                        this.AnchorSpanLabel.text(description);
                        this.AnchorSpanLabel.attr('title',(this.Options.tooltip) ? this.TooltipCompiled({ i: val }) : description);
                    } else {
                        this.Input.show();
                        this.Anchor.hide();
                    }
                });
            }
        }

        private setupDOM() {
            //Read Only DOM
            this.Anchor = angular.element('<a href="#">')
                .on('click',() => { return false; })
                .on('keydown',(evt) => {
                switch (evt.which) {
                    case 32://space
                    case 13://enter
                        evt.preventDefault();
                        (this.Expand || angular.noop)();
                        break;
                    case 8://backspace
                    case 46://delete
                        evt.preventDefault();
                        this.Clear();
                        break;
                }
            });
            this.AnchorSpan = angular.element('<span>')
                .on('click',() => this.Anchor.focus());;
            this.AnchorSpanLabel = angular.element('<label>');
            this.AnchorButton = angular.element('<button type="button" class="btn btn-default" tabindex="-1" title="Clear"><i class="fa fa-times"></i></button>')
                .on('click',() => this.Clear());

            this.$element.append(this.Anchor);
            this.Anchor.append(this.AnchorSpan);
            this.AnchorSpan.append(this.AnchorSpanLabel);
            this.Anchor.append(this.AnchorButton);

            //Input DOM
            var initialFocus = true;
            this.Input = angular.element('<input type="text" class="form-control" />')
                .on('focus',(evt) => {
                if (initialFocus) {
                    this.RefreshMatches(true, false);
                    initialFocus = false;
                } else {
                    this.SetMenuVisibility(true);
                }
            }).on('blur',(evt) => {
                if (!this.MouseInMenu) {
                    this.SetMenuVisibility(false);

                    if (this.Options.onBlur) { //putting this in a $timeout since onBlur likely modifies another scope; screen won't be updated
                        this.$timeout(() => { this.Options.onBlur(); });
                    }
                }
            }).on('keydown',(evt) => {
                switch (evt.which) {
                    case 27://esc
                        this.SetMenuVisibility(false);
                        break;
                    case 13://enter
                        if (this.SelectedMatch != null) {
                            this.Select(true);
                        }
                        evt.preventDefault();
                        break;
                    case 9://tab
                        if (!evt.shiftKey && this.SelectedMatch != null) {
                            this.Select(false);
                        }
                        break;
                    case 38://up
                        var nextMatch = (this.SelectedMatch) ? this.SelectedMatch.prev() : this.MenuItems[0];
                        if (nextMatch.length > 0) {
                            this.SetMenuItemActive(nextMatch);
                            this.ScrollToMenuItem();
                        }
                        evt.preventDefault();
                        break;
                    case 40://down
                        var nextMatch = (this.SelectedMatch) ? this.SelectedMatch.next() : this.MenuItems[0];
                        if (nextMatch.length > 0) {
                            this.SetMenuItemActive(nextMatch);
                            this.ScrollToMenuItem();
                        }
                        evt.preventDefault();
                        break;
                }
            }).on('keyup change',(evt) => {
                this.RefreshMatches(true, false);
            });

            this.$element.append(this.Input);

            //Menu DOM
            this.MouseInMenu = false;
            this.Menu = angular.element('<ul class="dropdown-menu item-select-menu">')
                .on('mouseleave',() => this.MouseInMenu = false)
                .hide();

            if (this.Options.appendToBody) {
                this.$scope.$on('$destroy',() => { this.Menu.remove(); });
            }

            ((this.Options.appendToBody) ? this.$document.find('body') : this.$element).append(this.Menu);
        }

        HasMenuItems() {
            return (this.MenuItems != null && this.MenuItems.length > 0);
        }
        SetMenuItemActive(menuItem: any) {
            this.SelectedMatch = menuItem;
            angular.forEach(this.MenuItems,(item) => {
                if (item.is(menuItem)) {
                    item.addClass('active');
                } else {
                    item.removeClass('active');
                }
            });
        }
        ScrollToMenuItem() {
            this.Menu.scrollTop((this.SelectedMatch) ? this.SelectedMatch[0].offsetTop : 0);
        }
        SetMenuVisibility(visible: boolean) {
            if (visible && this.HasMenuItems()) {
                if (this.Options.appendToBody) {
                    var position = this.$position.offset(this.$element);
                    position.top = position.top + this.$element.prop('offsetHeight');
                    this.Menu.css({ left: position.left + 'px', top: position.top + 'px' });
                }

                this.Menu.show();
            } else {
                this.Menu.hide();
                this.MouseInMenu = false;
            }
        }
        ClearMenu() {
            this.SelectedMatch = null;
            angular.forEach(this.MenuItems,(item: ng.IAugmentedJQuery) => {
                var scope = item.scope();
                if (scope != null) {
                    scope.$destroy();
                }
                
                item.remove();
            });
            this.MenuItems = [];
        }

        RefreshMatches(selectInitialItem: boolean, forceRefresh: boolean) {
            var term = this.Input.val() || '';

            if (forceRefresh || this.SearchedTerm != term) {
                this.SearchedTerm = term;

                if (this.SearchTimeout) {
                    this.$timeout.cancel(this.SearchTimeout);
                }

                if (term.length < ((this.Options.minLength != null) ? this.Options.minLength : 1)) {
                    this.ClearMenu();
                    this.SetMenuVisibility(false);
                } else {
                    var delay = this.Options.delay || ((this.Options.minLength == 0) ? 0 : 250);

                    this.SearchTimeout = this.$timeout(() => {
                        this.Options.source(this.SearchedTerm).then((items) => {
                            if (this.SearchedTerm == (this.Input.val() || '')) {//only do the work if this is the correct request
                                this.ClearMenu();
                                angular.forEach(items,(item) => {
                                    var childScope = this.$scope.$new(true);
                                    childScope['i'] = item;

                                    var li = this.$compile(this.MatchTemplateElement.clone())(childScope)
                                        .on('mouseenter',() => {
                                        this.MouseInMenu = true;
                                        this.SetMenuItemActive(li);
                                    }).on('click',() => {
                                        this.Select(true);
                                    }).data('Item', item);

                                    this.Menu.append(li);
                                    this.MenuItems.push(li);
                                });

                                if (items != null && items.length > 0) {
                                    if (selectInitialItem) {
                                        this.SetMenuItemActive(this.MenuItems[0]);
                                    }

                                    this.SetMenuVisibility(true);
                                } else {
                                    this.SetMenuVisibility(false);
                                }
                            }
                        });
                    }, delay);
                }
            }
        }

        Select(keepFocus: boolean) {
            this.ngModel.$setViewValue(this.SelectedMatch.data('Item'));
            if (keepFocus) {
                if (this.Options.multiSelect) {
                    this.ClearMenu();
                    this.Input.val('');
                    this.Input.focus();
                    if (this.Options.minLength == 0) {
                        this.RefreshMatches(false, true);
                    }
                } else {
                    this.SetMenuVisibility(false);
                    this.Anchor.focus();
                }
            }
        }

        Clear() {
            this.ngModel.$setViewValue(null);
            //Wait for model changed function to fire
            this.$timeout(() => {
                this.Input.focus();
                  
                //IE won't focus at the end of the input w/o this
                if (this.UtilityService.BrowserIsInternetExplorer()) {
                    var elm = <any>this.Input[0];//converting to any because these IE fields aren't in the definition
                    elm.selectionStart = elm.selectionEnd = (elm.value || '').length;
                }
            });
        }
    }
} 