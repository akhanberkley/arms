﻿/// <reference path="../app.ts" />
module arms {
    'use strict';
    app.directive('checkboxList', function ($timeout: ng.ITimeoutService): ng.IDirective {
        return {
            restrict: 'E',
            templateUrl: '/app/common/checkbox-list.html',
            require: ['ngModel', 'checkboxList'],
            scope: {
                Options: '=options',
                Display: '@display',
                TrackBy: '@trackby',
            },
            link: function (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, ctrls: any[]) {
                (<CheckboxListController>ctrls[1]).ngModel = <ng.INgModelController>ctrls[0];
            },
            bindToController: true,
            controller: CheckboxListController,
            controllerAs: 'vm'
        };
    });

    interface DisplayItem {
        Item: any;
        Display: string;
        Included: boolean;
        TrackByValue: any;
    }

    class CheckboxListController {
        Options: string[];
        ngModel: ng.INgModelController;
        UpdatingValues: boolean;

        Display: string;
        DisplayCompiled: ng.ICompiledExpression;
        TrackBy: string;
        TrackByCompiled: ng.ICompiledExpression;
        DisplayItems: DisplayItem[];
        ValueArray: any[];

        constructor($scope: ng.IScope, $parse: ng.IParseService) {
            this.DisplayCompiled = $parse((this.Display == null) ? 'i' : this.Display);
            this.TrackByCompiled = $parse((this.TrackBy == null) ? 'i' : this.TrackBy);

            $scope.$watchCollection(() => this.ngModel.$viewValue,(val: any) => {
                this.ValueArray = val;
                if (this.ValueArray != null) {
                    if (this.UpdatingValues) {
                        this.UpdatingValues = false;
                    } else {
                        this.setupDisplayedItems();
                    }
                }
            });

            $scope.$watchCollection(() => this.Options,(items) => {
                if (this.ValueArray != null) {
                    this.setupDisplayedItems();
                }
            });
        }

        setupDisplayedItems() {
            var displayItems = <DisplayItem[]>[];
            angular.forEach(this.Options,(i) => {
                var trackBy = this.TrackByCompiled({ i: i });

                displayItems.push({
                    Included: (this.FindMatchingValue(trackBy) != null),
                    Item: i,
                    Display: this.DisplayCompiled({ i: i }),
                    TrackByValue: trackBy
                });
            });

            this.DisplayItems = displayItems;
        }

        ToggleItemInclusion(item: DisplayItem) {
            this.UpdatingValues = true;
            item.Included = !item.Included;
            if (item.Included) {
                this.ValueArray.push(item.Item);
            } else {
                var match = this.FindMatchingValue(item.TrackByValue);
                if (match != null) {
                    this.ValueArray.splice(this.ValueArray.indexOf(match), 1);
                }
            }
        }

        FindMatchingValue(trackByValue: any) {
            var matches = this.ValueArray.filter((i) => this.TrackByCompiled({ i: i }) == trackByValue);
            return (matches.length > 0) ? matches[0] : null;
        }
    }
} 