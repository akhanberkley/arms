﻿/// <reference path="../app.ts" />
module arms {
    'use strict';
    app.directive('versionErrorModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/common/version-error-modal.html',
            replace: true,
            bindToController: true,
            controller: VersionErrorModalController,
            controllerAs: 'vm'
        };
    });

    class VersionErrorModalController extends BaseController {
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, private $window: ng.IWindowService) {
            super($scope, $state, UtilityService);
        }

        RefreshPage() {
            this.$window.location.reload();
        }
    }
}