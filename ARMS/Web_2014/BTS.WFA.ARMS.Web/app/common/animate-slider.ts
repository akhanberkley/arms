﻿/// <reference path="../app.ts" />
module arms {
    'use strict';
    app.directive('animateSlider', function ($timeout: ng.ITimeoutService): ng.IDirective {
        return {
            restrict: 'A',
            scope: {
                animateSlider: '='
            },
            link: function (scope, element) {
                var initialCall = true;
                scope.$watch('animateSlider',(val) => {
                    if (initialCall) {
                        initialCall = false;
                        if (val != true) {
                            element.hide();
                        }
                    } else {
                        if (val) {
                            element.slideDown(250);
                        } else {
                            element.slideUp(250);
                        }
                    }
                });
            }
        };
    });
} 