﻿/// <reference path="../app.ts" />
module arms {
    'use strict';
    app.directive('stopClickPropagation', function (): ng.IDirective {
        return {
            restrict: 'A',
            replace: true,
            link: function (scope, element) {
                element.click((e) => e.stopPropagation());
            }
        };
    });
} 