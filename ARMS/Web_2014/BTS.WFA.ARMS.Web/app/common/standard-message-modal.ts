﻿/// <reference path="../app.ts" />
module arms {
    'use strict';
    app.directive('standardMessageModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/common/standard-message-modal.html',
            replace: true,
            scope: {
                Title: '=title',
                Message: '=message',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: StandardMessageModalController,
            controllerAs: 'vm'
        };
    });

    class StandardMessageModalController extends BaseController {
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService) {
            super($scope, $state, UtilityService);
        }

        Close() {
            this.ModalInstance.dismiss();
        }
    }
}