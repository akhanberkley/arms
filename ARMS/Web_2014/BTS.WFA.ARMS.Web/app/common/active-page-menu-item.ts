﻿/// <reference path="../app.ts" />
module arms {
    'use strict';
    app.directive('activePageMenuItem', function ($state: ng.ui.IStateService, $position: ng.ui.bootstrap.IPositionService): ng.IDirective {
        return {
            restrict: 'A',
            replace: true,
            link: function (scope, element, attrs) {
                var arrowElement = $('#list-navigation-arrow');

                var setupElement = function () {
                    if ($state.is(attrs['activePageMenuItem'])) {
                        element.addClass('active');

                        //using setTimeout as if it's run initially, everything might not be on the page/positioned correctly
                        setTimeout(() => {
                            arrowElement.show();
                            var left = $position.position(element).left + (element.width() / 2) + 15;
                            arrowElement.css('left', left + 'px')
                        });
                    } else {
                        element.removeClass('active');
                    }
                };

                setupElement();
                scope.$on('$stateChangeSuccess',(event, toState, toParams, fromState, fromParams) => {
                    setupElement();
                });
            }
        };
    });
} 