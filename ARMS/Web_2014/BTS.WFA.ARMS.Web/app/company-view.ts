﻿/// <reference path="app.ts" />
module arms {
    'use strict';

    app.directive('companyView',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/company-view.html',
            replace: true,
            scope: {
                companyKey: '@'
            },
            bindToController: true,
            controller: CompanyViewController,
            controllerAs: 'vm'
        };
    });

    class CompanyViewController extends BaseController {
        SideBarCollapsed: boolean;
        User: User;
        Links: NavigationLink[];

        NewRequestType: CodeItem;
        NewRequestValue: string;
        RequestTypeCodes: any[];
        CreatingNewRequest: boolean;

        ImpersonatableCompanies: ImpersonatableCompany[];
        SelectedImpersonatableCompany: ImpersonatableCompany;
        SelectedImpersonatableUser: CodeItem;
        IsImpersonating: boolean;

        constructor($scope: ng.IScope, $state: ng.ui.IStateService, UtilityService: UtilityService, private UserService: UserService, private RequestQueueService: RequestQueueService, private AlertService: AlertService, private DomainService: DomainService) {
            super($scope, $state, UtilityService);

            this.SideBarCollapsed = false;

            this.RequestTypeCodes = RequestQueueService.RequestTypeCodes();
            this.NewRequestType = this.RequestTypeCodes.filter((t) => t.Code == 'PN')[0];

            this.IsImpersonating = UserService.IsImpersontating();
            UserService.UserPromise.then((user) => {
                this.User = user;

                this.Links = [];
                this.Links.push(new NavigationLink('Dashboard', 'ck.dashboard', 'fa fa-tachometer',() => $state.includes('ck.dashboard')));
                this.Links.push(new NavigationLink('Requests', 'ck.requests', 'fa fa-check',() => $state.includes('ck.requests')));
                this.Links.push(new NavigationLink('Recent Request', 'ck.survey({surveyId: 1234})', 'fa fa-history',() => $state.includes('ck.survey')));
                if (user.AdministratorLinks.length > 0) {
                    this.Links.push(new NavigationLink('Administration', 'ck.administration', 'fa fa-cogs',() => $state.includes('ck.administration')));
                }
            });
        }

        ToggleSideBar() {
            this.SideBarCollapsed = !this.SideBarCollapsed;
        }

        OpenSearch() {
            alert("Open Advanced Search");
        }
        OpenHelp() {
            alert("Open Help");
        }

        OpenProfile() {
        }

        SubmitRequest() {
            var value = (this.NewRequestValue == null) ? '' : this.NewRequestValue.trim();
            if (value.length > 0) {
                this.CreatingNewRequest = true;
                this.RequestQueueService.CreateRequest(this.companyKey, this.NewRequestType.Code, value).then((results) => {
                    if (results.ValidationErrors) {
                        console.log("Request had validation errors");
                    } else {
                        this.NewRequestValue = '';
                        this.AlertService.Notify(NotificationType.Success, `Request for ${this.NewRequestType.Description} ${value} has been created`);

                        if (this.$state.is('ck.requests')) {
                            this.$state.reload();
                        } else {
                            this.$state.go('ck.requests', null, { location: 'replace' });
                        }
                    }

                    this.CreatingNewRequest = false;
                });
            }
        }
    }

    class NavigationLink {
        constructor(private Title: string, private RouteUrl: string, private Icon: string, private IsActive?: () => boolean) { }
    }
}