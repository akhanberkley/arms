﻿/// <reference path="app.ts" />
module arms {
    'use strict';

    app.directive('companyImpersonationModal',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/company-impersonation-modal.html',
            replace: true,
            scope: {
                companyKey: '=',
                modal: '='
            }
        };
    });

    app.directive('companyImpersonation',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/company-impersonation.html',
            replace: true,
            scope: {
                companyKey: '=',
                ModalInstance: '=modal'
            },
            bindToController: true,
            controller: CompanyViewController,
            controllerAs: 'vm'
        };
    });

    class CompanyViewController {
        companyKey: string;
        ModalInstance: ng.ui.bootstrap.IModalServiceInstance;
        ImpersonatableCompanies: ImpersonatableCompany[];
        SelectedImpersonatableCompany: ImpersonatableCompany;
        SelectedImpersonatableUser: CodeItem;

        constructor(private $state: ng.ui.IStateService, private $urlRouter: ng.ui.IUrlRouterService, private UserService: UserService, private RequestQueueService: RequestQueueService, private AlertService: AlertService, DomainService: DomainService) {

            UserService.UserPromise.then((User) => {
                DomainService.ImpersonatableCompanies().then((companies) => {
                    this.ImpersonatableCompanies = companies;

                    var currentCompanyAbbreviation = this.companyKey.toUpperCase();
                    this.SelectedImpersonatableCompany = companies.filter((c) => c.Company.LongAbbreviation == currentCompanyAbbreviation)[0];
                    var currentUserName = User.UserName.toUpperCase();
                    this.SelectedImpersonatableUser = this.SelectedImpersonatableCompany.Users.filter((u) => u.Code.toUpperCase() == currentUserName)[0];
                });
            });
        }

        SelectedImpersonatableCompanyChanged() {
            this.SelectedImpersonatableUser = null;
        }
        Impersonate() {
            this.UserService.SetImpersonatedUserName(this.SelectedImpersonatableUser.Code);

            if (this.companyKey.toUpperCase() == this.SelectedImpersonatableCompany.Company.LongAbbreviation) {
                this.$urlRouter.sync();
                if (this.ModalInstance) {
                    this.ModalInstance.close();
                }
            } else {
                this.$state.go('index', null, { reload: true });
            }
        }
    }
}