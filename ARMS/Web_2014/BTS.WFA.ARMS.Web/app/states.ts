﻿/// <reference path="app.ts" />
module arms {
    'use strict';

    app.config(function ($locationProvider: ng.ILocationProvider) {
        $locationProvider.html5Mode(true).hashPrefix('!');
    });

    app.config(function ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) {
        $urlRouterProvider.otherwise("/");

        //Always remove trailing slash
        $urlRouterProvider.rule(function ($injector: ng.auto.IInjectorService, $location: ng.ILocationService) {
            var path = $location.path();
            if (path != '/' && path.slice(-1) === '/') {
                $location.replace().path(path.slice(0, -1));
            }
        });

        //If route names have a period in them, they are sub-routes in ui.router; they inherit the parent's info and are nested views
        $stateProvider.state('index', {
            url: "/",
            resolve: { User: function (UserService: UserService) { return UserService.UserPromise; } },
            controller: function ($state: ng.ui.IStateService, User: User) {
                $state.go('ck.dashboard', { companyKey: User.Company.LongAbbreviation.toLowerCase() }, { location: 'replace' });
            }
        })
            .state('ck', {
            url: "/{companyKey}",
            resolve: {
                User: function (UserService: UserService) { return UserService.UserPromise; },
                HasAccess: function ($q: ng.IQService, $state: ng.ui.IStateService, User: User, $stateParams: ng.ui.IStateParamsService) {
                    return new $q((resolve, reject) => {
                        var companyKey = (<string>$stateParams['companyKey']).toUpperCase();
                        if (companyKey == User.Company.LongAbbreviation) {
                            resolve();
                        } else {
                            reject('Company ' + companyKey + ' does not exist or you do not have access');
                        }
                    });
                }
            },
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                return `<company-view company-key="${$stateParams['companyKey']}"></company-view>`;
            },
            controller: function ($state: ng.ui.IStateService) {
                var key = <string>$state.params["companyKey"];
                var lowerKey = key.toLowerCase();
                if (key != lowerKey) {
                    $state.go('.', { companyKey: lowerKey }, { location: 'replace' });
                }
            }
        })
            .state('ck.dashboard', {
            url: "/dashboard",
            analytics: 'Dashboard',
            //template: '<dashboard></dashboard>'
            templateUrl: 'app/components/dashboard/dashboard.html'
        });

        Administration($stateProvider);
        Requests($stateProvider);
    });

    function Administration(provider: ng.ui.IStateProvider) {
        provider.state('ck.administration', {
            url: "/administration",
            template: '<administration></administration>',
            analytics: 'Administration'
        })
            .state('ck.administration.users', {
            url: "/users",
            template: '<users></users>',
            analytics: 'User Administration'
        })
            .state('ck.administration.roles', {
            url: "/roles",
            template: '<roles></roles>',
            analytics: 'Roles Administration'
        })
            .state('ck.administration.links', {
            url: "/links",
            template: '<links></links>',
            analytics: 'Links Administration'
        })

    }

    function Requests(provider: ng.ui.IStateProvider) {
        provider.state('ck.requests', {
            url: "/requests",
            template: '<requests></requests>',
            analytics: 'Request Queue'
        }).state('ck.survey', {
            url: "/survey/{surveyId}",
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                return `<survey survey-id="${$stateParams['surveyId']}"></survey>`;
            },
            analytics: 'Request'
        }).state('ck.survey.insured', {
            url: "/insured",
            templateUrl: '/app/components/survey/survey-insured.html'
        }).state('ck.survey.agency', {
            url: "/agency",
            templateUrl: '/app/components/survey/survey-agency.html'
        }).state('ck.survey.coverages', {
            url: "/coverages",
            templateUrl: '/app/components/survey/survey-coverages.html'
        }).state('ck.survey.locations', {
            url: "/locations",
            templateUrl: '/app/components/survey/survey-locations.html'
        }).state('ck.survey.documents', {
            url: "/documents",
            templateUrl: '/app/components/survey/survey-documents.html'
        }).state('ck.survey.assignment', {
            url: "/assignment",
            templateUrl: '/app/components/survey/survey-assignment.html'
        });
    }
}
