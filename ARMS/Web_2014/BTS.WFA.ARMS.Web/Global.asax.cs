﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Newtonsoft.Json.Converters;

namespace BTS.WFA.ARMS.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            //MVC
            RouteTable.Routes.MapRoute("Index", "", new { controller = "Home", action = "Index" });
            RouteTable.Routes.MapRoute("UnsupportedBrowser", "UnsupportedBrowser", new { controller = "Home", action = "UnsupportedBrowser" });
            RouteTable.Routes.MapRoute("Login", "login", new { controller = "Home", action = "Login" });

            //Web Api
            GlobalConfiguration.Configure(config =>
            {
                config.MapHttpAttributeRoutes();

                config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
                config.Filters.Add(new Api.Filters.WebExceptionFilterAttribute());
                config.MessageHandlers.Add(new Api.Handlers.AuthenticationHandler());
            });

            var appHandlers = ARMS.Application.CreateHandlers(ConfigurationManager.AppSettings["ApplicationHandlers"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            ARMS.Application.Initialize<ARMS.Data.SqlDb>(appHandlers.ToArray());

            BundleTable.Bundles.Add(new ScriptBundle("~/bundles/framework-scripts").Include("~/scripts/analytics.js")
                                                                                   .Include("~/scripts/moment-2.8.3.js")
                                                                                   .Include("~/scripts/FileSaver-2014-08-29.js")
                                                                                   .Include("~/scripts/toastr-2.1.1/toastr.js")
                                                                                   .IncludeDirectory("~/scripts/angular-ui", "*.js", true));
            BundleTable.Bundles.Add(new StyleBundle("~/bundles/framework-styles").Include("~/styles/bts-bootstrap/bts-bootstrap.v1.5.0.css")
                                                                                 .Include("~/scripts/toastr-2.1.1/toastr.css"));

            //We aren'a minifying this one to help with debugging
            BundleTable.Bundles.Add(new Bundle("~/bundles/app-scripts").Include("~/app/models.js")
                                                                       .Include("~/app/app.js")
                                                                       .Include("~/app/states.js")
                                                                       .IncludeDirectory("~/app", "*.js", true));

            BundleTable.Bundles.Add(new StyleBundle("~/bundles/app-styles").Include("~/styles/site-layout.css")
                                                                           .Include("~/styles/global.css")
                                                                           .Include("~/styles/customizations-bootstrap.css")
                                                                           .IncludeDirectory("~/app", "*.css", true));
        }
    }
}