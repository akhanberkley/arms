﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace BTS.WFA.ARMS.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Views/Index.cshtml");
        }

        public ActionResult UnsupportedBrowser()
        {
            return View("~/Views/UnsupportedBrowser.cshtml");
        }

        [Authorize]
        public ActionResult Login()
        {
            return new HttpStatusCodeResult(200);
        }
    }
}