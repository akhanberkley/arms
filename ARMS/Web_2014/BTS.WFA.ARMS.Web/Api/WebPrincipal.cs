﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Web.Api
{
    public class WebPrincipal : IPrincipal
    {
        private WebIdentity webIdentity;
        public IIdentity Identity { get { return webIdentity; } }

        public WebPrincipal(string userName, UserViewModel overrideUser)
        {
            var user = overrideUser ?? Services.UsersService.GetActiveUser(userName);

            if (user != null)
                webIdentity = new WebIdentity(user);
        }

        public bool IsInRole(string role)
        {
            //
            throw new NotImplementedException();

            /*
            return (role == Domains.Roles.SystemAdministrator && webIdentity.UserViewModel.UserType == Domains.UserTypes.SystemAdministrator)
                    || webIdentity.UserViewModel.Roles.Contains(role);
            */
        }
        public bool HasAccessToCompany(string companyKey)
        {
            //
            throw new NotImplementedException();

            /*
            return webIdentity.UserViewModel.Companies.Any(c => c.Code == companyNumber);
            */
        }
    }

    public class WebIdentity : IIdentity
    {
        public WebIdentity(UserViewModel user)
        {
            IsAuthenticated = true;
            Name = user.UserName;
            Roles = new List<string>();
            UserViewModel = user;
        }

        public string AuthenticationType { get { return "ARMS"; } }
        public bool IsAuthenticated { get; set; }
        public string Name { get; set; }
        public UserViewModel UserViewModel { get; set; }
        public List<string> Roles { get; set; }
    }
}