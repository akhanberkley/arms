﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/Documents")]
    public class DocumentController : ApiController
    {      
        [HttpPost, Route(""), Route("{ID}")]
        public HttpResponseMessage Save(CompanyDetail company, DocumentViewModel document)
        {
            var result = Services.DocumentsService.SaveDocument(document, company);

            //if (result.ValidationResults.Count > 0)
            //    return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, result.ValidationResults);
            //else
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

    }
}
