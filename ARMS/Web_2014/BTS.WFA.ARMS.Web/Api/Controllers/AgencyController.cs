﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.OData.Query;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/Agency")]
    public class AgencyController : BaseController
    {
        [HttpGet, Route("")]
        [System.Web.Http.Description.ResponseType(typeof(List<AgencyViewModel>))]
        public virtual HttpResponseMessage Get(CompanyDetail company, ODataQueryOptions<Data.Models.Agency> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(Services.AgencyService.Search(company, query.ApplyTo));
        }

        [HttpGet, Route("{agencyCode}/Agent")]
        public virtual List<ContactViewModel> Get(CompanyDetail company, string agencyCode)
        {
            return Services.AgencyService.GetAgents(company, agencyCode);
        }
    }
}
