﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BTS.WFA.ARMS.Services;
using BTS.WFA.ARMS.ImportServices;
using BTS.WFA.ARMS.ViewModels;
using System.Web.OData.Query;
//using Newtonsoft.Json;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/Requests")]
    public class RequestQueueController : BaseController
    {
        [HttpGet, Route("")]
        public HttpResponseMessage Get(CompanyDetail company, ODataQueryOptions<Data.Models.RequestQueue> query)
        {
            ValidateOdataQuery(query);
            return CreateODataResponse(RequestQueueManagerService.Search(company, query.ApplyTo));
        }

        [HttpPost, Route("")]
        public HttpResponseMessage Insert(CompanyDetail company, string typeCode, string value)
        {
            RequestViewModel request = RequestQueueManagerService.CreateRequest(company, typeCode, value);
            ProcessRequest(company, request);
            return CreateValidatedResponse(request);
        }

        [HttpPost, Route("{requestId}/Process")]
        public void ProcessRequest(CompanyDetail company, RequestViewModel request)
        {
            ImportManagerServiceResult result = ImportManagerService.ProcessRequest(company, request);
            //return CreateValidatedResponse(result);
        }
        //public void ProcessRequest(CompanyDetail company, RequestViewModel request)
        //{
        //    ImportManagerService importManager = new ImportManagerService(company, request, request.Survey);
        //    importManager.ProcessRequest();
        //    //return CreateValidatedResponse(result);
        //}
    }
}
