﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
      [RoutePrefix("api/{company}")]
    public class TerritoryController : ApiController
    {
        [HttpPost, Route("Territory/"), Route("Territory/{ID}")]
        public HttpResponseMessage Save(CompanyDetail company, TerritoryViewModel territory)
        {
            var result = Services.TerritoryService.SaveTerritory(territory, company);

            //if (result.ValidationResults.Count > 0)
            //    return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, result.ValidationResults);
            //else
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }


    }
}
