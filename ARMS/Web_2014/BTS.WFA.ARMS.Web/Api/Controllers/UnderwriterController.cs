﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}")]
    public class UnderwriterController : ApiController
    {

        //[HttpGet, Route("Underwriters")]
        //public UnderwriterAdministrationViewModel Get(CompanyDetail company)
        //{
        //    return Services.UnderwriterService.GetUnderwriter(company);
        //}

        [HttpPost, Route("Underwriter"), Route("Underwriter/{Code}")]
        public HttpResponseMessage Save(CompanyDetail company, UnderwriterViewModel underwriter)
        {
            var result = Services.UnderwriterService.SaveUnderwriter(underwriter, company);

            //if (result.ValidationResults.Count > 0)
            //    return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, result.ValidationResults);
            //else
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

    }
}
