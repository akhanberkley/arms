﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/WebClient")]
    [System.Web.Http.Description.ApiExplorerSettings(IgnoreApi = true)]
    public class WebClientController : BaseController
    {
        [HttpPost, Route("LogJavascriptException")]
        public virtual void Log(JavascriptExceptionViewModel exception)
        {
            Application.HandleException(new JavascriptException(), exception);
        }

        [HttpGet, Route("ServerTime")]
        public virtual DateTime ServerTime()
        {
            return DateTime.Now;
        }
    }
}