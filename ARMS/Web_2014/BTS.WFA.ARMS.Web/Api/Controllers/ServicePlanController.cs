﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}")]
    public class ServicePlanController : ApiController
    {

        //[HttpGet, Route("ServicePlans")]
        //public ServicePlanPageViewModel Get(CompanyDetail company)
        //{
        //    return Services.ServicePlanService.GetServicePlanPage(company);
        //}

        [HttpGet, Route("ServicePlan/{servicePlanNumber}")]
        public ServicePlanViewModel GetServicePlanByNumber(CompanyDetail company, int servicePlanNumber)
        {
            return Services.ServicePlanService.GetServicePlanByNumber(servicePlanNumber, company);
        }


    }
}
