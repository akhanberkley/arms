﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BTS.WFA.ARMS;
using BTS.WFA.ARMS.ViewModels;
//using BTS.WFA.BCS.Web.Api.Filters;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/Survey")]
    public class SurveyController : BaseController
    {
        [HttpGet, Route("{surveyNumber}")]
        public SurveyViewModel Get(CompanyDetail company, int surveyNumber)
        {
            return Services.SurveyQueryService.Get(company, surveyNumber);
        }
    }
}