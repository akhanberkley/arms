﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/UserRole")]
    public class UserRoleController : ApiController
    {
        [HttpGet, Route("")]
        public List<UserRoleViewModel> Get(CompanyDetail company)
        {
            return Services.UserRolesService.GetAll(company);
        }
    }
}


 
