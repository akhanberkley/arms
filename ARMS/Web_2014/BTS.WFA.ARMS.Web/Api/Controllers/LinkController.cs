﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}")]
    public class LinkController : ApiController
    {

        [HttpGet, Route("Links")]
        public List<LinkViewModel> Get(CompanyDetail company)
        {
            return Services.LinkService.GetAll(company);
        }

        [HttpPost, Route("Link"), Route("Link/{ID}")]
        public HttpResponseMessage Save(CompanyDetail company, LinkViewModel link)
        {
            var result = Services.LinkService.SaveLink(link, company);

            //if (result.ValidationResults.Count > 0)
            //    return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, result.ValidationResults);
            //else
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete, Route("Link"), Route("Link/{ID}")]
        public HttpResponseMessage Delete(CompanyDetail company, Guid ID)
        {
            var results = Services.LinkService.DeleteLink(ID, company);

            if (results.Count > 0)
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, results);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.OK , results);
        }

    }
}
