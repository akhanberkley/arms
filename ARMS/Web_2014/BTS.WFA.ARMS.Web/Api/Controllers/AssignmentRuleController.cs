﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}/AssignmentRule")]
    public class AssignmentRuleController : ApiController
    {
        [HttpGet, Route("FieldValues/{fieldcode}")]
        public List<CodeListItemViewModel> Get(CompanyDetail company, string fieldcode)
        {
            return Services.AssignmentRuleService.GetFieldValues(fieldcode, company);
        }



        [HttpPost, Route(""), Route("{ID}")]
        public HttpResponseMessage Save(CompanyDetail company, AssignmentRuleViewModel rule)
        {
            var result = Services.AssignmentRuleService.SaveAssignmentRule(rule, company);

            //if (result.ValidationResults.Count > 0)
            //    return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, result.ValidationResults);
            //else
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete, Route(""), Route("{ID}")]
        public HttpResponseMessage Delete(CompanyDetail company, Guid ID)
        {
            var results = Services.AssignmentRuleService.DeleteAssignmentRule(ID, company);

            if (results.Count > 0)
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, results);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, results);
        }

    }
}
