﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BTS.WFA.ARMS.ViewModels;
using BTS.WFA.ARMS.Web.Api.Filters;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : BaseController
    {
        [HttpGet, Route("Current")]
        public UserViewModel GetCurrentUser()
        {
            return (User.Identity as WebIdentity).UserViewModel;
        }

        //[HttpPost, Route("User/Current/DefaultCompany/{companyKey}")]
        //public void UpdateCurrentUsersDefaultCompany(string companyKey)
        //{
        //    var user = (User.Identity as WebIdentity);
        //    BCS.Services.UserService.UpdateDefaultCompany(user.UserViewModel.Id, Application.CompanyNumberMap[companyNumber]);
        //    Api.Handlers.AuthenticationHandler.ClearUserFromCache(user.Name);
        //}

        //[HttpGet, Route("{companyKey}/User/GetAll/{feeCompany}")]

        //public UserPageViewModel Get(string companyKey,bool feeCompany)
        //{
        //    return Services.UsersService.GetUser(Application.CompanyNumberMap[companyNumber],feeCompany);
        //}

        //[HttpPost, Route("{companyKey}/User/"), Route("{companyKey}/User/{ID}")]            
        //public HttpResponseMessage Save(string companyKey, UserViewModel user)
        //{
        //    var result = Services.UsersService.SaveUser(user,Application.CompanyNumberMap[companyNumber]);

        //    //if (result.ValidationResults.Count > 0)
        //    //    return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, result.ValidationResults);
        //    //else
        //    return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        //}

        //[HttpDelete, Route("{companyKey}/User/"), Route("{companyKey}/User/{ID}")]
        //public HttpResponseMessage Delete(string companyKey, Guid ID)
        //{
        //    var results = Services.UsersService.DeleteUser(ID, Application.CompanyNumberMap[companyNumber]);

        //    if (results.Count > 0)
        //        return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, results);
        //    else
        //        return Request.CreateResponse(System.Net.HttpStatusCode.OK);
        //}
    }
}