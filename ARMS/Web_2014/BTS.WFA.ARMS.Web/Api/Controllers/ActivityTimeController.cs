﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{

    [RoutePrefix("api/{company}/ActivityTime")]
    public class ActivityTimeController : ApiController
    {
        [HttpGet, Route("GetAll")]

        public ActivityTimesPageViewModel Get(CompanyDetail company)
        {
            return Services.ActivityTimesService.GetViewInformation(company);
        }

        [HttpPost, Route(""), Route("{ID}")]
        public HttpResponseMessage Save(CompanyDetail company, ActivityViewModel activitytime)
        {
            var result = Services.ActivityTimesService.SaveActivityTime(activitytime, company);

            //if (result.ValidationResults.Count > 0)
            //    return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, result.ValidationResults);
            //else
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

        [HttpDelete, Route(""), Route("{ID}")]
        public HttpResponseMessage Delete(CompanyDetail company, Guid ID)
        {
            var results = Services.ActivityTimesService.DeleteActivityTime(ID, company);

            if (results.Count > 0)
                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, results);
            else
                return Request.CreateResponse(System.Net.HttpStatusCode.OK, results);
        }

    }
}
