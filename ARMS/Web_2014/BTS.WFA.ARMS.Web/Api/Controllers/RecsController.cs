﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/{company}")]
    public class RecsController : ApiController
    {

        [HttpGet, Route("Recs")]
        public RecsPageViewModel Get(CompanyDetail company)
        {
            return Services.RecsService.GetRecs(company);
        }

        [HttpPost, Route("Rec/"), Route("Rec/{ID}")]
        public HttpResponseMessage Save(CompanyDetail company, RecViewModel rec)
        {
            var result = Services.RecsService.SaveRec(rec, company);

            //if (result.ValidationResults.Count > 0)
            //    return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, result.ValidationResults);
            //else
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
        }

    }
}
