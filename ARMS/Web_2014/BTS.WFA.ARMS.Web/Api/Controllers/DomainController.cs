﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BTS.WFA.ARMS;
using BTS.WFA.ARMS.ViewModels;
//using BTS.WFA.BCS.Web.Api.Filters;
using BTS.WFA.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/Domains")]
    public class DomainController : BaseController
    {
        [HttpGet, Route("ImpersonatableCompany")]
        public List<ImpersonatableCompanyViewModel> ImpersonatableCompanies()
        {
            return Services.DomainService.ImpersonatableCompanies();
        }

        [HttpGet, Route("State")]
        public List<StateViewModel> StateList()
        {
            return Services.DomainService.FullStateList();
        }

        [HttpGet, Route("~/api/{company}/Domains/SurveyReason")]
        public List<string> SurveyReason(CompanyDetail company)
        {
            return Services.DomainService.SurveyReasons(company);
        }

        [HttpGet, Route("~/api/{company}/Domains/Underwriter")]
        public List<ContactViewModel> Underwriters(CompanyDetail company)
        {
            return Services.DomainService.Underwriters(company);
        }

        [HttpGet, Route("~/api/{company}/Domains/LineOfBusiness")]
        public List<LineOfBusinessViewModel> LinesOfBusiness(CompanyDetail company)
        {
            return Services.DomainService.LinesOfBusiness(company);
        }

        [HttpGet, Route("~/api/{company}/Domains/ProfitCenter")]
        public List<CodeListItemViewModel> ProfitCenter(CompanyDetail company)
        {
            return Services.DomainService.ProfitCenters(company);
        }
    }
}