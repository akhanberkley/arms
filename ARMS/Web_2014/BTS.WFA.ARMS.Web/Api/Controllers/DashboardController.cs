﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Controllers
{
    [RoutePrefix("api/Dashboard")]
    public class DashboardController : BaseController
    {
        [HttpGet, Route("")]
        public string Get()
        {
            return "Welcome to ARMS ! Click on Administrator tab for Admin Links";
        }
        //public UserPageViewModel Get()
        //{
        //    var page = Services.UsersService.GetUser(Application.CompanyNumberMap["52"],false);
        //    return page;
        //}
    }
}
