﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Net.Http;
using System.Threading;

namespace BTS.WFA.ARMS.Web.Api.Filters
{
    /// <summary>
    /// Verifies the user has access to the 'CompanyNumber' passed into the controller action
    /// </summary>
    public class CompanyAccessAuthorizationAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            //
            throw new NotImplementedException();

            /*
            var user = Thread.CurrentPrincipal as WebPrincipal;
            var identity = user.Identity as WebIdentity;

            if (identity.UserViewModel.UserType == Domains.UserTypes.SystemAdministrator)
                return true;
            else
            {
                string companyNumber = null;
                var routeData = actionContext.Request.GetRouteData();
                var routeKvp = routeData.Values.FirstOrDefault(kvp => kvp.Key.Equals("companynumber", StringComparison.CurrentCultureIgnoreCase));
                if (!routeKvp.Equals(default(KeyValuePair<string, object>)))
                    companyNumber = routeKvp.Value.ToString();
                else
                {
                    var queryString = HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query);
                    var queryMatch = queryString["companynumber"];
                    if (!String.IsNullOrEmpty(queryMatch))
                        companyNumber = queryMatch;
                }

                return (companyNumber == null || user.HasAccessToCompany(companyNumber));
            }
            */
        }
    }
}