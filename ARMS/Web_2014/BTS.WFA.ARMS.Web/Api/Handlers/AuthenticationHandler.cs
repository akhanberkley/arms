﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.IdentityModel.Tokens;
using BTS.WFA.ARMS.ViewModels;

namespace BTS.WFA.ARMS.Web.Api.Handlers
{
    public class AuthenticationHandler : DelegatingHandler
    {
        private const string cachePrefix = "USERNAME|";
        private TokenValidationParameters jwtValidationParameters = null;

        public AuthenticationHandler()
        {
            var config = new System.IdentityModel.Configuration.IdentityConfiguration();
            var issuerNameRegistry = config.IssuerNameRegistry as ValidatingIssuerNameRegistry;
            if (issuerNameRegistry != null)//this will be null if not using ADFS (local debugging)
            {
                var key = issuerNameRegistry.IssuingAuthorities.SelectMany(a => a.SymmetricKeys).First();
                var keyBytes = new byte[key.Length * sizeof(char)];
                System.Buffer.BlockCopy(key.ToCharArray(), 0, keyBytes, 0, keyBytes.Length);

                jwtValidationParameters = new TokenValidationParameters
                {
                    ValidAudiences = config.AudienceRestriction.AllowedAudienceUris.Select(a => a.ToString()),
                    ValidIssuers = issuerNameRegistry.IssuingAuthorities.Select(i => i.Name),
                    IssuerSigningToken = new System.ServiceModel.Security.Tokens.BinarySecretSecurityToken(keyBytes)
                };
            }
        }

        /// <summary>
        /// Set this property to test as a specific user
        /// </summary>
        public static UserViewModel OverrideUser { get; set; }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //Local = windows auth; deployed = ADFS (cookies from Login page); external system = JWT
            var currentContext = HttpContext.Current;

            //Try and parse the JWT if exists
            if (!currentContext.User.Identity.IsAuthenticated && jwtValidationParameters != null)
            {
                var authHeader = currentContext.Request.Headers.Get("Authorization");
                if (authHeader != null && authHeader.StartsWith("Bearer "))
                {
                    try
                    {
                        SecurityToken token = null;
                        currentContext.User = (new JwtSecurityTokenHandler()).ValidateToken(authHeader.Substring(7), jwtValidationParameters, out token);
                    }
                    catch (Exception ex)
                    {
                        Application.HandleException(ex, "JWT Parsing");
                    }
                }
            }

            //They should be logged in by this point
            if (!currentContext.User.Identity.IsAuthenticated)
            {
                var response = request.CreateResponse(System.Net.HttpStatusCode.Forbidden);
                response.ReasonPhrase = "Unauthorized";
                return Task.FromResult<HttpResponseMessage>(response);
            }
            else
            {
                var userName = currentContext.User.Identity.Name;
                if (!(currentContext.User is WindowsPrincipal))
                {
                    var claim = ((ClaimsIdentity)currentContext.User.Identity).FindFirst(ClaimTypes.WindowsAccountName);
                    if (claim != null)
                        userName = claim.Value;
                }

                var user = GetUser(currentContext, userName);
                if (user == null || user.Identity == null)
                {
                    var response = request.CreateResponse(System.Net.HttpStatusCode.Forbidden);
                    response.ReasonPhrase = "User not found";
                    return Task.FromResult<HttpResponseMessage>(response);
                }
                else
                {
                    Thread.CurrentPrincipal = currentContext.User = user;
                    return base.SendAsync(request, cancellationToken);
                }
            }
        }

        private static IPrincipal GetUser(HttpContext currentContext, string userName)
        {
            var cacheKey = cachePrefix + userName.ToUpper();
            IPrincipal cachedUser = (IPrincipal)currentContext.Cache[cacheKey];
            if (cachedUser == null)
            {
                cachedUser = new WebPrincipal(userName, OverrideUser);
                if (cachedUser.Identity == null)
                    return null;
                else
                    currentContext.Cache.Add(cacheKey, cachedUser, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 10, 0), CacheItemPriority.Default, null);
            }

            var impersonateUserName = currentContext.Request.Headers["Impersonate"];
            if (!String.IsNullOrEmpty(impersonateUserName) && !String.Equals(impersonateUserName, userName, StringComparison.CurrentCultureIgnoreCase) && ((WebIdentity)cachedUser.Identity).UserViewModel.IsSystemAdministrator)
                return GetUser(currentContext, impersonateUserName);
            else
                return cachedUser;
        }

        public static void ClearUserFromCache(string userName)
        {
            HttpContext.Current.Cache.Remove(cachePrefix + userName.ToUpper());
        }
    }
}