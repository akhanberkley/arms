<%@ Application Language="C#" %>
<%@ Import namespace="Berkley.BLC.Entities" %>
<%@ Import namespace="QCI.DataAccess" %>
<%@ Import namespace="QCI.ExceptionManagement" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // initalize the DAL with the default connection string - which is used for all database access
        string connStr = Berkley.BLC.Core.CoreSettings.ARMSConnectionString;
		if( connStr == null ) throw new Exception("Configuration setting 'ConnectionString' must be specified in the application config file.");
		DB.Initialize(connStr);
		SqlHelper.ConnectionString = connStr;

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }

    void Application_AuthenticateRequest(object sender, EventArgs e)
    {
        //  Code
    }
        
    void Application_Error(object sender, EventArgs e) 
    {

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Application_BeginRequest(Object sender, EventArgs e)
    {
        //  Code
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
