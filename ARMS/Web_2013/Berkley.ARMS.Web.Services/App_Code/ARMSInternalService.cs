﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Specialized;

using Messaging = Berkley.BLC.Messaging;
using Entity = Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.ExceptionManagement;

/// <summary>
/// Summary description for SurveyRequest
/// </summary>
[WebService(
    Namespace = "http://wrberkley.com/services/arms/v1",
    Description = "The ARMS Internal Service allows other BTS applications to consume various methods.")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class ARMSInternalService : System.Web.Services.WebService
{

    public ARMSInternalService()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region Enums

    public enum StatusType
    {
        [Common.StringValue("OH")]
        OnHoldSurvey,

        [Common.StringValue("SW")]
        AwaitingAcceptance,

        [Common.StringValue("SA")]
        AssignedSurvey,

        [Common.StringValue("SU")]
        UnassignedSurvey,

        [Common.StringValue("SR")]
        ReturningToCompany,

        [Common.StringValue("RU")]
        UnassignedReview,

        [Common.StringValue("RA")]
        AssignedReview,

        [Common.StringValue("SC")]
        ClosedSurvey,

        [Common.StringValue("CC")]
        CanceledSurvey,
    }
    #endregion

    /// <summary>
    /// Retrieve a list of survey requests by client id.
    /// </summary>
    /// <param name="companyNumber">The company number.</param>
    /// <param name="sourceSystem">The name of the system calling this method.</param>
    /// <param name="clientId">The insured's client id.</param>
    /// <param name="startDate">Return surveys with a due date greater than or equal to this date.</param>
    /// <param name="endDate">Return surveys with a due date less than or equal to this date.</param>
    /// <param name="statuses">Return surveys with these statuses.</param>
    [WebMethod(
         BufferResponse = false,
         CacheDuration = 0,
         Description = "Retrieves a list of surveys by client id.",
         EnableSession = false,
         MessageName = "GetSurveyRequestsByClientId",
         TransactionOption = System.EnterpriseServices.TransactionOption.Disabled)]
    public Messaging.SurveyRequest[] GetSurveyRequestsByClientId(string companyNumber, string sourceSystem, string clientId, string startDate, string endDate, StatusType[] statuses)
    {
        try
        {
            Entity.Company company = Authenticate(companyNumber, sourceSystem);
            List<Messaging.SurveyRequest> list = new List<Messaging.SurveyRequest>();

            if (string.IsNullOrEmpty(clientId))
            {
                throw new SoapException("clientId parameter is required", SoapException.ClientFaultCode);
            }

            if (string.IsNullOrEmpty(startDate))
            {
                throw new SoapException("startDate parameter is required", SoapException.ClientFaultCode);
            }

            try { DateTime.Parse(startDate); }
            catch
            {
                throw new SoapException("startDate is not in valid date format", SoapException.ClientFaultCode);
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                try { DateTime.Parse(endDate); }
                catch
                {
                    throw new SoapException("endDate is not in valid date format", SoapException.ClientFaultCode);
                }
            }

            //begin the query
            string strQuery = " CompanyID = ? && DueDate >= ? && Insured.ClientID = ? ";
            ArrayList args = new ArrayList();
            args.Add(company.ID);
            args.Add(startDate);
            args.Add(clientId);

            if (!string.IsNullOrEmpty(endDate))
            {
                strQuery += " && DueDate <= ? ";
                args.Add(endDate);
            }

            if (statuses != null)
            {
                for (int i = 0; i < statuses.Length; i++)
                {
                    StatusType status = statuses[i];

                    if (i == 0)
                    {
                        strQuery += " && (";
                    }
                    
                    strQuery += " StatusCode = ? ";

                    if (i != (statuses.Length - 1))
                    {
                        strQuery += " || ";
                    }
                    else
                    {
                        strQuery += ")";
                    }

                    args.Add(Common.StringEnum.GetStringValue(status));
                }
            }

            //get the survey meeting the criteria and build the xml to be returned
            RequestBuilder builder = new RequestBuilder();
            Entity.Survey[] surveys = Entity.Survey.GetArray(strQuery, args.ToArray());

            //make sure the number of surveys is not greater than what it allowed
            int maxCount = int.Parse(System.Configuration.ConfigurationManager.AppSettings["MaxAllowedSurveyCount"]);
            if (surveys.Length > maxCount)
            {
                throw new SoapException(string.Format("Maximum number of surveys that can be returned is {0}. Please refine your criteria.", maxCount), SoapException.ClientFaultCode);
            }

            foreach (Entity.Survey survey in surveys)
            {
                Messaging.SurveyRequest builtRequest = builder.BuildSurveyRequestMessageLite(survey);
                list.Add(builtRequest);
            }

            return list.ToArray();
        }
        catch (SoapException se)
        {
            throw new SoapException(se.Message, SoapException.ClientFaultCode);
        }
        catch (Exception ex)
        {
            NameValueCollection additionalInfo = new NameValueCollection();
            additionalInfo.Add("Method", "GetSurveyRequestsByClientId");
            additionalInfo.Add("CompanyNumber", companyNumber);
            additionalInfo.Add("SourceSystem", sourceSystem);
            additionalInfo.Add("ClientID", clientId.ToString());
            additionalInfo.Add("StartDate", startDate);
            additionalInfo.Add("EndDate", endDate);

            ExceptionManager.Publish(ex, additionalInfo);
            throw new ConsumerSoapException();
        }
    }

    private Entity.Company Authenticate(string companyNumber, string sourceSystem)
    {
        if (string.IsNullOrEmpty(companyNumber))
        {
            throw new SoapException("companyNumber parameter is required", SoapException.ClientFaultCode);
        }

        if (string.IsNullOrEmpty(sourceSystem))
        {
            throw new SoapException("sourceSystem parameter is required", SoapException.ClientFaultCode);
        }

        //confirm the company exist in ARMS
        Entity.Company company = Entity.Company.GetOne("Number = ?", companyNumber);
        if (company == null)
        {
            throw new SoapException(string.Format("Company Number '{0}' is not supported in ARMS", companyNumber), SoapException.ClientFaultCode);
        }

        string authorizedSystems = System.Configuration.ConfigurationManager.AppSettings["AuthorizedSourceSystems"];
        if (!authorizedSystems.Contains(sourceSystem))
        {
            throw new SoapException(string.Format("Source System '{0}' does not have permission to consume this web method", sourceSystem), SoapException.ClientFaultCode);
        }

        return company;
    }

}
