using System;
using System.Collections.Generic;
using System.Text;

public class ConsumerSoapException : System.Web.Services.Protocols.SoapException
{
    public ConsumerSoapException()
        : base("An exception has occurred in the ARMS system.  An administrator has been notified.", System.Web.Services.Protocols.SoapException.ServerFaultCode)
	{
	}
}
