﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Specialized;

using Messaging = Berkley.BLC.Messaging;
using Entity = Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.ExceptionManagement;

/// <summary>
/// Summary description for SurveyRequest
/// </summary>
[WebService(
    Namespace = "http://wrberkley.com/services/arms/v1",
    Description = "The ARMS Vendor Service allows a company to retrieve new survey requests and allows companies to submit those requests when they are complete.")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class VendorService : System.Web.Services.WebService
{

    public VendorService()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region Enums
    public enum RetrievalType
    {
        NewWork,
        NewCorrections,
        AllWorkInProgress,
        AllCompletedInPastSixMonths
    }

    public enum MIMEType
    {
        [Common.StringValue("application/pdf")]
        PDF,

        [Common.StringValue("text/plain")]
        PlainText,

        [Common.StringValue("application/vnd.ms-excel")]
        Excel2003AndEarlier,

        [Common.StringValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")]
        Excel2007AndLater,

        [Common.StringValue("application/msword")]
        Word2003AndEarlier,

        [Common.StringValue("application/vnd.openxmlformats-officedocument.wordprocessingml.document")]
        Word2007AndLater,

        [Common.StringValue("application/vnd.ms-powerpoint")]
        PowerPoint2003AndEarlier,

        [Common.StringValue("application/vnd.openxmlformats-officedocument.presentationml.presentation")]
        PowerPoint2007AndLater,

        [Common.StringValue("image/jpeg")]
        JPG,

        [Common.StringValue("image/gif")]
        GIF,

        [Common.StringValue("image/png")]
        PNG,

        [Common.StringValue("image/bmp")]
        BMP,

        [Common.StringValue("image/tiff")]
        TIF,

        [Common.StringValue("application/zip")]
        ZIP
    }
    #endregion

    /// <summary>
    /// Retrieve a survey request by the survey number.
    /// </summary>
    /// <param name="userName">The username to authenticate.</param>
    /// <param name="password">The password to authenticate.</param>
    /// <param name="surveyRequestNumber">The survey number of the survey request to retrieve.</param>
    [WebMethod(
         BufferResponse = false,
         CacheDuration = 0,
         Description = "Retrieve a survey request by the survey number.",
         EnableSession = false,
         MessageName = "GetSurveyRequestByNumber",
         TransactionOption = System.EnterpriseServices.TransactionOption.Disabled)]
    public Messaging.SurveyRequest GetSurveyRequestByNumber(string userName, string password, int surveyRequestNumber)
    {
        try
        {
            Entity.FeeUser authedUser = Authenticate(userName, password);
            Entity.Survey survey = ValidateSurveyForRetrieval(authedUser, surveyRequestNumber);

            RequestBuilder builder = new RequestBuilder();
            Messaging.SurveyRequest builtRequest = builder.BuildSurveyRequestMessage(survey);

            //set the accepted date if it hasn't been set yet and the surveyRequest is succusfully built
            if (survey.Status == Entity.SurveyStatus.AwaitingAcceptance)
            {
                SurveyManager manager = SurveyManager.GetInstance(survey, authedUser.FeeCompanyUser, true);
                manager.Accept();
            }

            return builtRequest;
        }
        catch (SoapException se)
        {
            throw new SoapException(se.Message, SoapException.ClientFaultCode);
        }
        catch (Exception ex)
        {
            NameValueCollection additionalInfo = new NameValueCollection();
            additionalInfo.Add("Method", "GetSurveyRequestByNumber");
            additionalInfo.Add("UserName", userName);
            additionalInfo.Add("SurveyRequestNumber", surveyRequestNumber.ToString());

            ExceptionManager.Publish(ex, additionalInfo);
            throw new ConsumerSoapException();
        }
    }

    /// <summary>
    /// Retrieve a list of survey request numbers by specifying the retrieval type.
    /// </summary>
    /// <param name="userName">The username to authenticate.</param>
    /// <param name="password">The password to authenticate.</param>
    /// <param name="requestType">The type of surveys to retrieve.</param>
    [WebMethod(
         BufferResponse = false,
         CacheDuration = 0,
         Description = "Retrieve a list of survey request numbers by specifying the retrieval type.",
         EnableSession = false,
         MessageName = "GetSurveyRequestList",
         TransactionOption = System.EnterpriseServices.TransactionOption.Disabled)]
    public Messaging.SurveyRequestReport GetSurveyRequestList(string userName, string password, RetrievalType requestType)
    {
        try
        {
            Entity.FeeUser authedUser = Authenticate(userName, password);

            Messaging.SurveyRequestReport report = new Messaging.SurveyRequestReport();

            ArrayList args = new ArrayList();
            string strQuery = "";

            switch (requestType)
            {
                case RetrievalType.AllWorkInProgress:
                    args.Add(authedUser.FeeCompanyUser.CompanyID);
                    args.Add(authedUser.FeeCompanyUserID);
                    args.Add(Entity.SurveyStatus.AssignedSurvey.Code);
                    strQuery += "CompanyID = ? && AssignedUserID = ? && StatusCode = ?";
                    break;

                case RetrievalType.NewWork:
                    args.Add(authedUser.FeeCompanyUser.CompanyID);
                    args.Add(authedUser.FeeCompanyUserID);
                    args.Add(Entity.SurveyStatus.AwaitingAcceptance.Code);
                    strQuery += "CompanyID = ? && AssignedUserID = ? && StatusCode = ? && (Correction = False || ISNULL(Correction))";
                    break;

                case RetrievalType.NewCorrections:
                    args.Add(authedUser.FeeCompanyUser.CompanyID);
                    args.Add(authedUser.FeeCompanyUserID);
                    args.Add(Entity.SurveyStatus.AwaitingAcceptance.Code);
                    args.Add(Entity.SurveyStatus.AssignedSurvey.Code);
                    strQuery += "CompanyID = ? && AssignedUserID = ? && (StatusCode = ? || StatusCode = ?) && Correction = True";
                    break;

                case RetrievalType.AllCompletedInPastSixMonths:
                    args.Add(authedUser.FeeCompanyUser.CompanyID);
                    args.Add(authedUser.FeeCompanyUserID);
                    args.Add(Entity.SurveyStatusType.Survey.Code);
                    args.Add(DateTime.Today.AddMonths(-6));
                    strQuery += "CompanyID = ? && ConsultantUserID = ? && Status.TypeCode != ? && ReportCompleteDate >= ?";
                    break;

                default: //return an empty list
                    return report;
            }

            RequestBuilder builder = new RequestBuilder();

            Entity.Survey[] surveys = Entity.Survey.GetArray(strQuery, args.ToArray());
            foreach (Entity.Survey survey in surveys)
            {
                report.SurveyRequestNumbers.Add(survey.Number);
            }

            return report;
        }
        catch (SoapException se)
        {
            throw new SoapException(se.Message, SoapException.ClientFaultCode);
        }
        catch (Exception ex)
        {
            NameValueCollection additionalInfo = new NameValueCollection();
            additionalInfo.Add("Method", "GetSurveyRequestList");
            additionalInfo.Add("UserName", userName);
            additionalInfo.Add("RequestType", requestType.ToString());

            ExceptionManager.Publish(ex, additionalInfo);
            throw new ConsumerSoapException();
        }
    }

    /// <summary>
    /// Send an incompleted survey request back to ARMS.
    /// </summary>
    /// <param name="userName">The username to authenticate.</param>
    /// <param name="password">The password to authenticate.</param>
    /// <param name="surveyRequestNumber">The survey number of the survey request to return.</param>
    [WebMethod(
         BufferResponse = false,
         CacheDuration = 0,
         Description = "Send an incomplete survey request back to ARMS.",
         EnableSession = false,
         MessageName = "ReturnIncompletedSurveyRequest",
         TransactionOption = System.EnterpriseServices.TransactionOption.Disabled)]
    public void ReturnIncompletedSurveyRequest(string userName, string password, int surveyRequestNumber)
    {
        try
        {
            Entity.FeeUser authedUser = Authenticate(userName, password);
            Entity.Survey survey = ValidateSurveyForSubmittal(authedUser, surveyRequestNumber);

            SurveyManager manager = SurveyManager.GetInstance(survey, authedUser.FeeCompanyUser, true);
            manager.ReturnToCompany();
        }
        catch (SoapException se)
        {
            throw new SoapException(se.Message, SoapException.ClientFaultCode);
        }
        catch (Exception ex)
        {
            NameValueCollection additionalInfo = new NameValueCollection();
            additionalInfo.Add("Method", "ReturnIncompletedSurveyRequest");
            additionalInfo.Add("UserName", userName);
            additionalInfo.Add("SurveyRequestNumber", surveyRequestNumber.ToString());

            ExceptionManager.Publish(ex, additionalInfo);
            throw new ConsumerSoapException();
        }
    }

    /// <summary>
    /// Submit a comment to an ARMS survey request.
    /// </summary>
    /// <param name="userName">The username to authenticate.</param>
    /// <param name="password">The password to authenticate.</param>
    /// <param name="surveyRequestNumber">The survey number of the survey request to that has been completed.</param>
    /// <param name="commentText">The text of the comment.</param>
    [WebMethod(
         BufferResponse = false,
         CacheDuration = 0,
         Description = "Submit a comment to an ARMS survey request.",
         EnableSession = false,
         MessageName = "SubmitComment",
         TransactionOption = System.EnterpriseServices.TransactionOption.Disabled)]
    public void SubmitComment(string userName, string password, int surveyRequestNumber, string commentText)
    {
        try
        {
            Entity.FeeUser authedUser = Authenticate(userName, password);
            Entity.Survey survey = ValidateSurveyForSubmittal(authedUser, surveyRequestNumber);

            if (String.IsNullOrEmpty(commentText))
            {
                throw new SoapException("commentText parameter is required", SoapException.ClientFaultCode);
            }

            //attach the comment
            SurveyManager manager = SurveyManager.GetInstance(survey, authedUser.FeeCompanyUser, true);
            manager.AddNote(commentText, false, false, DateTime.Now, string.Empty);
        }
        catch (SoapException se)
        {
            throw new SoapException(se.Message, SoapException.ClientFaultCode);
        }
        catch (Exception ex)
        {
            NameValueCollection additionalInfo = new NameValueCollection();
            additionalInfo.Add("Method", "SubmitCompletedSurveyRequest");
            additionalInfo.Add("UserName", userName);
            additionalInfo.Add("SurveyRequestNumber", surveyRequestNumber.ToString());
            additionalInfo.Add("DocumentName", commentText);

            ExceptionManager.Publish(ex, additionalInfo);
            throw new ConsumerSoapException();
        }
    }

    /// <summary>
    /// Submit a document to an ARMS survey request.
    /// </summary>
    /// <param name="userName">The username to authenticate.</param>
    /// <param name="password">The password to authenticate.</param>
    /// <param name="surveyRequestNumber">The survey number of the survey request to that has been completed.</param>
    /// <param name="documentContent">The content of the document.</param>
    /// <param name="mimeType">The MIME type of the document.</param>
    /// <param name="documentName">The name of the document including the extension.</param>
    [WebMethod(
         BufferResponse = false,
         CacheDuration = 0,
         Description = "Submit a document to an ARMS survey request.",
         EnableSession = false,
         MessageName = "SubmitDocument",
         TransactionOption = System.EnterpriseServices.TransactionOption.Disabled)]
    public void SubmitDocument(string userName, string password, int surveyRequestNumber, byte[] documentContent, MIMEType mimeType, string documentName)
    {
        try
        {
            Entity.FeeUser authedUser = Authenticate(userName, password);
            Entity.Survey survey = ValidateSurveyForSubmittal(authedUser, surveyRequestNumber);

            if (documentContent == null || documentContent.Length == 0)
            {
                throw new SoapException("documentContent parameter is required", SoapException.ClientFaultCode);
            }

            if (String.IsNullOrEmpty(documentName))
            {
                throw new SoapException("documentName parameter is required", SoapException.ClientFaultCode);
            }

            if (documentName.Length > 200)
            {
                throw new SoapException("The document name cannot exceed 200 characters.", SoapException.ClientFaultCode);
            }

            if (System.IO.Path.GetExtension(documentName).Length <= 0)
            {
                throw new SoapException("The document name must contain a file extension.", SoapException.ClientFaultCode);
            }

            if (mimeType == null)
            {
                throw new SoapException("The document MIME type is required.", SoapException.ClientFaultCode);
            }

            SurveyManager manager = SurveyManager.GetInstance(survey, authedUser.FeeCompanyUser, true);

            //attach document
            System.IO.Stream stream = new System.IO.MemoryStream(documentContent);
            stream.Seek(0, System.IO.SeekOrigin.Begin);
            manager.AttachDocument(documentName, Common.StringEnum.GetStringValue(mimeType), stream, null, string.Empty, true);
        }
        catch (SoapException se)
        {
            throw new SoapException(se.Message, SoapException.ClientFaultCode);
        }
        catch (Exception ex)
        {
            NameValueCollection additionalInfo = new NameValueCollection();
            additionalInfo.Add("Method", "SubmitCompletedSurveyRequest");
            additionalInfo.Add("UserName", userName);
            additionalInfo.Add("SurveyRequestNumber", surveyRequestNumber.ToString());
            additionalInfo.Add("DocumentName", documentName);

            ExceptionManager.Publish(ex, additionalInfo);
            throw new ConsumerSoapException();
        }
    }

    /// <summary>
    /// Submit a completed request and survey report back to ARMS.
    /// </summary>
    /// <param name="userName">The username to authenticate.</param>
    /// <param name="password">The password to authenticate.</param>
    /// <param name="surveyRequestNumber">The survey number of the survey request to that has been completed.</param>
    /// <param name="dateSurveyed">The date of the survey.</param>
    /// <param name="totalHours">Total hours spent on the survey.</param>
    /// <param name="surveyCost">The cost of the survey.</param>
    /// <param name="documentContent">The content of the document.</param>
    /// <param name="mimeType">The MIME type of the document.</param>
    /// <param name="documentName">The name of the document including the extension.</param>
    [WebMethod(
         BufferResponse = false,
         CacheDuration = 0,
         Description = "Submit a completed survey request and report document back to ARMS.",
         EnableSession = false,
         MessageName = "SubmitCompletedSurveyRequest",
         TransactionOption = System.EnterpriseServices.TransactionOption.Disabled)]
    public void SubmitCompletedSurveyRequest(string userName, string password, int surveyRequestNumber, DateTime dateSurveyed, int totalHours, decimal surveyCost, byte[] documentContent, MIMEType mimeType, string documentName)
    {
        try
        {
            Entity.FeeUser authedUser = Authenticate(userName, password);
            Entity.Survey survey = ValidateSurveyForSubmittal(authedUser, surveyRequestNumber);

            if (dateSurveyed == DateTime.MinValue)
            {
                throw new SoapException("dateSurveyed parameter is required", SoapException.ClientFaultCode);
            }

            if (totalHours <= 0)
            {
                throw new SoapException("totalHours parameter is required", SoapException.ClientFaultCode);
            }

            if (survey.Company.RequireVendorCost && surveyCost <= 0)
            {
                throw new SoapException("surveyCost parameter is required", SoapException.ClientFaultCode);
            }

            if (documentContent == null || documentContent.Length == 0)
            {
                throw new SoapException("documentContent parameter is required", SoapException.ClientFaultCode);
            }

            if (String.IsNullOrEmpty(documentName))
            {
                throw new SoapException("documentName parameter is required", SoapException.ClientFaultCode);
            }

            if (documentName.Length > 200)
            {
                throw new SoapException("The document name cannot exceed 200 characters.", SoapException.ClientFaultCode);
            }

            if (System.IO.Path.GetExtension(documentName).Length <= 0)
            {
                throw new SoapException("The document name must contain a file extension.", SoapException.ClientFaultCode);
            }

            if (mimeType == null)
            {
                throw new SoapException("The document MIME type is required.", SoapException.ClientFaultCode);
            }

            //set survey details
            SurveyManager manager = SurveyManager.GetInstance(survey, authedUser.FeeCompanyUser, true);
            manager.SetSurveyDetails(dateSurveyed, totalHours, string.Empty, string.Empty, Decimal.MinValue, surveyCost, false,false, string.Empty, DateTime.MinValue);

            //attach document
            System.IO.Stream stream = new System.IO.MemoryStream(documentContent);
            stream.Seek(0, System.IO.SeekOrigin.Begin);
            manager.AttachDocument(documentName, Common.StringEnum.GetStringValue(mimeType), stream, null, string.Empty, true);

            //send for reveiew
            manager.SendForReview(true);
        }
        catch (SoapException se)
        {
            throw new SoapException(se.Message, SoapException.ClientFaultCode);
        }
        catch (Exception ex)
        {
            NameValueCollection additionalInfo = new NameValueCollection();
            additionalInfo.Add("Method", "SubmitCompletedSurveyRequest");
            additionalInfo.Add("UserName", userName);
            additionalInfo.Add("SurveyRequestNumber", surveyRequestNumber.ToString());
            additionalInfo.Add("DateSurveyed", dateSurveyed.ToString());
            additionalInfo.Add("TotalHours", totalHours.ToString());
            additionalInfo.Add("SurveyCost", surveyCost.ToString());
            additionalInfo.Add("DocumentName", documentName);

            ExceptionManager.Publish(ex, additionalInfo);
            throw new ConsumerSoapException();
        }
    }

    /// <summary>
    /// Submit a completed request back to ARMS without a document.
    /// </summary>
    /// <param name="userName">The username to authenticate.</param>
    /// <param name="password">The password to authenticate.</param>
    /// <param name="surveyRequestNumber">The survey number of the survey request to that has been completed.</param>
    /// <param name="dateSurveyed">The date of the survey.</param>
    /// <param name="totalHours">Total hours spent on the survey.</param>
    /// <param name="surveyCost">The cost of the survey.</param>
    [WebMethod(
         BufferResponse = false,
         CacheDuration = 0,
         Description = "Submit a completed survey request back to ARMS without a document.",
         EnableSession = false,
         MessageName = "SubmitCompletedSurveyRequestWithoutDocument",
         TransactionOption = System.EnterpriseServices.TransactionOption.Disabled)]
    public void SubmitCompletedSurveyRequestWithoutDocument(string userName, string password, int surveyRequestNumber, DateTime dateSurveyed, int totalHours, decimal surveyCost)
    {
        try
        {
            Entity.FeeUser authedUser = Authenticate(userName, password);
            Entity.Survey survey = ValidateSurveyForSubmittal(authedUser, surveyRequestNumber);

            if (dateSurveyed == DateTime.MinValue)
            {
                throw new SoapException("dateSurveyed parameter is required", SoapException.ClientFaultCode);
            }

            if (totalHours <= 0)
            {
                throw new SoapException("totalHours parameter is required", SoapException.ClientFaultCode);
            }

            if (survey.Company.RequireVendorCost && surveyCost <= 0)
            {
                throw new SoapException("surveyCost parameter is required", SoapException.ClientFaultCode);
            }

            Entity.SurveyDocument surveyDocument = Entity.SurveyDocument.GetOne("SurveyID = ? && UploadUserID = ?", survey.ID, authedUser.FeeCompanyUserID);
            if (surveyDocument == null)
            {
                throw new SoapException("At least one document must already be attached to this survey request in order to submit for completion", SoapException.ClientFaultCode);
            }

            //set survey details
            SurveyManager manager = SurveyManager.GetInstance(survey, authedUser.FeeCompanyUser, true);
            manager.SetSurveyDetails(dateSurveyed, totalHours, string.Empty, string.Empty, Decimal.MinValue, surveyCost, false,false,string.Empty, DateTime.MinValue);

            //send for reveiew
            manager.SendForReview(true);
        }
        catch (SoapException se)
        {
            throw new SoapException(se.Message, SoapException.ClientFaultCode);
        }
        catch (Exception ex)
        {
            NameValueCollection additionalInfo = new NameValueCollection();
            additionalInfo.Add("Method", "SubmitCompletedSurveyRequest");
            additionalInfo.Add("UserName", userName);
            additionalInfo.Add("SurveyRequestNumber", surveyRequestNumber.ToString());
            additionalInfo.Add("DateSurveyed", dateSurveyed.ToString());
            additionalInfo.Add("TotalHours", totalHours.ToString());
            additionalInfo.Add("SurveyCost", surveyCost.ToString());

            ExceptionManager.Publish(ex, additionalInfo);
            throw new ConsumerSoapException();
        }
    }

    private Entity.FeeUser Authenticate(string userName, string password)
    {
        if (String.IsNullOrEmpty(userName))
        {
            throw new SoapException("userName parameter is required", SoapException.ClientFaultCode);
        }

        if (String.IsNullOrEmpty(password))
        {
            throw new SoapException("password parameter is required", SoapException.ClientFaultCode);
        }

        //authenticate the username/password entered
        string failureMessage;
        Entity.FeeUser authedUser = Security.Authenticate(userName, password, out failureMessage);
        if (authedUser == null)
        {
            throw new SoapException(failureMessage, SoapException.ClientFaultCode);
        }

        return authedUser;
    }

    private Entity.Survey RetrieveSurvey(Entity.FeeUser authedUser, int surveyNumber)
    {
        if (surveyNumber <= 0)
        {
            throw new SoapException("surveyRequestNumber parameter is required", SoapException.ClientFaultCode);
        }

        //check if the survey exist
        Entity.Survey survey = Entity.Survey.GetOne("Number = ?", surveyNumber);
        if (survey == null)
        {
            throw new SoapException(string.Format("Survey #{0} could not be found in ARMS.", surveyNumber), SoapException.ClientFaultCode);
        }

        return survey;
    }

    private Entity.Survey ValidateSurveyForRetrieval(Entity.FeeUser authedUser, int surveyNumber)
    {
        Entity.Survey survey = RetrieveSurvey(authedUser, surveyNumber);
        
        //check if the active survey is assigned to the user
        if ((survey.Status.Type == Entity.SurveyStatusType.Survey) && (survey.AssignedUser == null || survey.AssignedUserID != authedUser.FeeCompanyUserID))
        {
            throw new SoapException(string.Format("Survey #{0} is not assigned to your company.", surveyNumber), SoapException.ClientFaultCode);
        }

        //check if the completed survey was assigned to the user
        if ((survey.Status.Type == Entity.SurveyStatusType.Review || survey.Status.Type == Entity.SurveyStatusType.Completed) && (survey.ConsultantUser == null || survey.ConsultantUserID != authedUser.FeeCompanyUserID))
        {
            throw new SoapException(string.Format("Survey #{0} is not assigned to your company.", surveyNumber), SoapException.ClientFaultCode);
        }

        return survey;
    }

    private Entity.Survey ValidateSurveyForSubmittal(Entity.FeeUser authedUser, int surveyNumber)
    {
        Entity.Survey survey = RetrieveSurvey(authedUser, surveyNumber);

        //check if the active survey is assigned to the user
        if (survey.Status.Type != Entity.SurveyStatusType.Survey || survey.AssignedUser == null || survey.AssignedUserID != authedUser.FeeCompanyUserID)
        {
            throw new SoapException(string.Format("Survey #{0} is not assigned to your company.", surveyNumber), SoapException.ClientFaultCode);
        }

        return survey;
    }
}
