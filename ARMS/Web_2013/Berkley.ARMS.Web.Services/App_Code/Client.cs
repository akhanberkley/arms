﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using Berkley.BLC.Entities;
using Wilson.ORMapper;

/// <summary>
/// Summary description for Client
/// </summary>
[WebService(Namespace = "http://wrberkley.com/services/arms/v1")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Client : System.Web.Services.WebService {

    public Client () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    ///// <summary>
    ///// Updates the existing client id in ARMS with the new client id that is passed.
    ///// </summary>
    ///// <param name="companyNumber">The company number.</param>
    ///// <param name="oldClientID">The old client id.</param>
    ///// <param name="newClientID">The new client id.</param>
    //[WebMethod(BufferResponse = false, CacheDuration = 0, Description = "Updates the existing client id in ARMS with the new client id that is passed.")]
    //public int UpdateClientID(string companyNumber, string oldClientID, string newClientID)
    //{
    //    try
    //    {
    //        int counter = 0;
    //        Insured[] insureds = Insured.GetArray("Company.Number = ? && ClientID = ?", companyNumber, oldClientID);

    //        if (insureds.Length > 0)
    //        {
    //            using (Transaction trans = DB.Engine.BeginTransaction())
    //            {
    //                for (int i = 0; i < insureds.Length; i++)
    //                {
    //                    Insured insured = insureds[i].GetWritableInstance();
    //                    insured.ClientID = newClientID;
    //                    insured.Save(trans);

    //                    //incriment
    //                    counter = counter + 1;
    //                }
    //                trans.Commit();
    //            }
    //        }

    //        return counter;
    //    }
    //    catch (Exception ex)
    //    {
    //        QCI.ExceptionManagement.ExceptionManager.Publish(ex);
    //        throw new Exception("", ex);
    //    }
    //}
    
}
