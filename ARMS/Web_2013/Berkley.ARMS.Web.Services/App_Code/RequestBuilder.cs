﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;

using Messaging = Berkley.BLC.Messaging;
using Entity = Berkley.BLC.Entities;

/// <summary>
/// Summary description for RequestBuilder
/// </summary>
public class RequestBuilder
{
	public RequestBuilder() {}

    public Messaging.SurveyRequest BuildSurveyRequestMessageLite(Entity.Survey survey)
    {
        //populate the basic survey details
        Messaging.SurveyRequest surveyRequest = new Messaging.SurveyRequest();
        surveyRequest.SurveyId = survey.ID.ToString();
        surveyRequest.SurveyRequestNumber = survey.Number;
        surveyRequest.SurveyStatus = survey.Status.Name;
        surveyRequest.DateCreated = survey.CreateDate;
        surveyRequest.DueDate = survey.DueDate;
        surveyRequest.DateAssigned = survey.AssignDate;
        surveyRequest.DateAccepted = survey.AcceptDate;
        surveyRequest.AssignedUser = survey.HtmlConsultantGrid.Replace("(none)", string.Empty);
        surveyRequest.LastModifiedOn = survey.LastModifiedOn;
        surveyRequest.DateSurveyed = survey.SurveyedDate;
        surveyRequest.SurveyType = survey.Type.Type.Name;

        //build the insured
        surveyRequest.Insured = BuildInsured(survey.Insured);

        //add any survey Documents
        Entity.SurveyDocument[] surveyDocuments = Entity.SurveyDocument.GetSortedArray("SurveyID = ? && IsRemoved = false", "UploadedOn DESC", survey.ID);
        foreach (Entity.SurveyDocument surveyDocument in surveyDocuments)
        {
            surveyRequest.SurveyDocuments.Add(BuildSurveyDocumentLite(surveyDocument));
        }

        //add any survey Locations
        Entity.Location[] locations = Entity.Location.GetSortedArray("SurveyPolicyCoverages[SurveyID = ?]", "Number ASC", survey.ID);
        foreach (Entity.Location surveyLocation in locations)
        {
            surveyRequest.Locations.Add(BuildLocationLite(surveyLocation, survey));
        }

        //Add notes to UW
        //add any survey Documents
        Entity.SurveyNote[] surveyNotes = Entity.SurveyNote.GetSortedArray("SurveyID = ? && InternalOnly = false && AdminOnly = false && Comment LIKE 'Instructions to UW %'", "EntryDate DESC", survey.ID);
        foreach (Entity.SurveyNote surveyNote in surveyNotes)
        {
            surveyRequest.SurveyNotes.Add(BuildSurveyNote(surveyNote));
        }

        return surveyRequest;
    }
    
    public Messaging.SurveyRequest BuildSurveyRequestMessage(Entity.Survey survey)
    {
        //populate the survey details
        Messaging.SurveyRequest surveyRequest = new Messaging.SurveyRequest();
        surveyRequest.SurveyRequestNumber = survey.Number;
        surveyRequest.SurveyStatus = DetermineStatus(survey);
        surveyRequest.DueDate = survey.DueDate;
        surveyRequest.DateAssigned = survey.AssignDate;
        surveyRequest.DateAccepted = survey.AcceptDate;
        surveyRequest.LastModifiedOn = survey.LastModifiedOn;

        //build the insured
        surveyRequest.Insured = BuildInsured(survey.Insured);

        //build the agency
        surveyRequest.Agency = BuildAgency(survey.Insured);

        //BIZ rule:  do not return the notes or documents for completed survey requests
        if (survey.Status.Type == Entity.SurveyStatusType.Survey)
        {
            //attach any survey notes
            Entity.SurveyNote[] surveyNotes = Entity.SurveyNote.GetSortedArray("SurveyID = ? && InternalOnly = False && AdminOnly = false", "EntryDate DESC", survey.ID);
            foreach (Entity.SurveyNote surveyNote in surveyNotes)
            {
                surveyRequest.SurveyNotes.Add(BuildSurveyNote(surveyNote));
            }

            //add any survey documents
            Entity.SurveyDocument[] surveyDocuments = Entity.SurveyDocument.GetSortedArray("SurveyID = ? && IsRemoved = false", "UploadedOn DESC", survey.ID);
            foreach (Entity.SurveyDocument surveyDocument in surveyDocuments)
            {
                surveyRequest.SurveyDocuments.Add(BuildSurveyDocument(surveyDocument));
            }
        }

        //build the locations
        Entity.Location[] locations = Entity.Location.GetSortedArray("SurveyPolicyCoverages[SurveyID = ?]", "Number ASC", survey.ID);
        foreach (Entity.Location location in locations)
        {
            surveyRequest.Locations.Add(BuildLocation(survey, location));
        }

        //build the claims
        if (survey.Company.DisplayClaimsByDefault || survey.DisplayClaims)
        {
            Entity.Claim[] claims = Entity.Claim.GetSortedArray("Policy[InsuredID = ?]", "LossDate DESC", survey.InsuredID);
            foreach (Entity.Claim claim in claims)
            {
                surveyRequest.Claims.Add(BuildClaim(claim));
            }
        }

        return surveyRequest;
    }

    private Messaging.SurveyNote BuildSurveyNote(Entity.SurveyNote surveyNote)
    {
        Messaging.SurveyNote newNote = new Messaging.SurveyNote();

        newNote.EnteredBy = (surveyNote.User != null) ? surveyNote.User.Name : "Underwriter";
        newNote.DateEntered = surveyNote.EntryDate;
        newNote.NoteText = surveyNote.Comment;

        return newNote;
    }

    private Messaging.SurveyDocument BuildSurveyDocument(Entity.SurveyDocument surveyDocument)
    {
        Messaging.SurveyDocument newDocument = new Messaging.SurveyDocument();

        //retrieve the document from filenet
        StreamDocument docRetriever = new StreamDocument(surveyDocument, surveyDocument.Survey.Company);

        newDocument.Name = surveyDocument.DocumentName;
        newDocument.MimeType = docRetriever.DocMimeType;
        newDocument.Extension = System.IO.Path.GetExtension(surveyDocument.DocumentName);

		byte[] result = new byte[docRetriever.DocumentStream.Length];
		docRetriever.DocumentStream.Write(result, 0, result.Length);
		docRetriever.DocumentStream.Close();
        newDocument.Content = result;

        return newDocument;
    }

    private Messaging.SurveyDocument BuildSurveyDocumentLite(Entity.SurveyDocument surveyDocument)
    {
        Messaging.SurveyDocument newDocument = new Messaging.SurveyDocument();
        newDocument.FileNetId = surveyDocument.FileNetReference;

        return newDocument;
    }

    private Messaging.Insured BuildInsured(Entity.Insured insured)
    {
        Messaging.Insured newInsured = new Messaging.Insured();

        newInsured.ClientID = insured.ClientID;
        newInsured.Name = insured.Name;
        newInsured.AlternateName = insured.Name2;
        newInsured.BusinessOperations = insured.BusinessOperations;
        newInsured.SIC = insured.SICCode;

        newInsured.Address = new Messaging.Address();
        newInsured.Address.StreetLine1 = insured.StreetLine1;
        newInsured.Address.StreetLine2 = insured.StreetLine2;
        newInsured.Address.City = insured.City;
        newInsured.Address.StateAbbreviation = insured.StateCode;
        newInsured.Address.ZipCode = insured.ZipCode;

        if (!String.IsNullOrEmpty(insured.Underwriter))
        {
            Entity.Underwriter underwriter = Entity.Underwriter.GetOne("Code = ? && CompanyID = ?", insured.Underwriter, insured.CompanyID);
            if (underwriter != null)
            {
                newInsured.Underwriter = new Messaging.Underwriter();
                newInsured.Underwriter.Name = underwriter.Name;
                newInsured.Underwriter.Communication = new Messaging.Communication();
                newInsured.Underwriter.Communication.PhoneNumber = underwriter.PhoneNumber;
            }
        }

        return newInsured;
    }

    private Messaging.Agency BuildAgency(Entity.Insured insured)
    {
        Messaging.Agency newAgency = new Messaging.Agency();

        newAgency.Number = insured.Agency.Number;
        newAgency.Name = insured.Agency.Name;
        newAgency.Address = new Messaging.Address();
        newAgency.Address.StreetLine1 = insured.Agency.StreetLine1;
        newAgency.Address.StreetLine2 = insured.Agency.StreetLine2;
        newAgency.Address.City = insured.Agency.City;
        newAgency.Address.StateAbbreviation = insured.Agency.StateCode;
        newAgency.Address.ZipCode = insured.Agency.ZipCode;

        newAgency.Communication = new Messaging.Communication();
        newAgency.Communication.PhoneNumber = insured.Agency.PhoneNumber;
        newAgency.Communication.FaxNumber = insured.Agency.FaxNumber;

        newAgency.Agent = new Messaging.Agent();
        newAgency.Agent.Name = insured.AgentContactName;
        newAgency.Agent.Communication = new Messaging.Communication();
        newAgency.Agent.Communication.PhoneNumber = insured.AgentContactPhoneNumber;

        if (insured.AgentEmailID != Guid.Empty)
        {
            Entity.AgencyEmail agentEmail = Entity.AgencyEmail.GetOne("ID = ?", insured.AgentEmailID);
            if (agentEmail != null)
            {
                newAgency.Agent.Communication.EmailAddress = agentEmail.EmailAddress;
            }
        }

        return newAgency;
    }

    private Messaging.Location BuildLocation(Entity.Survey survey, Entity.Location location)
    {
        Messaging.Location newLocation = new Messaging.Location();
        
        newLocation.Number = location.Number;
        newLocation.Description = location.Description;
        newLocation.Address = new Messaging.Address();
        newLocation.Address.StreetLine1 = location.StreetLine1;
        newLocation.Address.StreetLine2 = location.StreetLine2;
        newLocation.Address.City = location.City;
        newLocation.Address.StateAbbreviation = location.StateCode;
        newLocation.Address.ZipCode = location.ZipCode;

        //Add any contacts
        Entity.LocationContact[] contacts = Entity.LocationContact.GetSortedArray("LocationID = ?", "PriorityIndex ASC", location.ID);
        foreach (Entity.LocationContact contact in contacts)
        {
            newLocation.Contacts.Add(BuildContact(contact));
        }

        //Build the policies for this location
        Entity.Policy[] policies = Entity.Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && LocationID = ? && Active = true]", "Number ASC", survey.ID, location.ID);
        foreach (Entity.Policy policy in policies)
        {
            newLocation.Policies.Add(BuildPolicy(survey, location, policy));
        }

        //Build the buildings for this location
        Entity.Building[] buildings = Entity.Building.GetSortedArray("LocationID = ?", "Number ASC", location.ID);
        foreach (Entity.Building building in buildings)
        {
            newLocation.Buildings.Add(BuildBuilding(building));
        }

        return newLocation;
    }

    private Messaging.Location BuildLocationLite(Entity.Location location, Entity.Survey survey)
    {
        Messaging.Location newLocation = new Messaging.Location();

        newLocation.Number = location.Number;
        newLocation.Description = location.Description;
        newLocation.Address = new Messaging.Address();
        newLocation.Address.StreetLine1 = location.StreetLine1;
        newLocation.Address.StreetLine2 = location.StreetLine2;
        newLocation.Address.City = location.City;
        newLocation.Address.StateAbbreviation = location.StateCode;
        newLocation.Address.ZipCode = location.ZipCode;

        ////Add any contacts
        //Entity.LocationContact[] contacts = Entity.LocationContact.GetSortedArray("LocationID = ?", "PriorityIndex ASC", location.ID);
        //foreach (Entity.LocationContact contact in contacts)
        //{
        //    newLocation.Contacts.Add(BuildContact(contact));
        //}

        ////Build the policies for this location
        Entity.Policy[] policies = Entity.Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && LocationID = ? && Active = true]", "Number ASC", survey.ID, location.ID);
        foreach (Entity.Policy policy in policies)
        {
            newLocation.Policies.Add(BuildPolicyLite(survey, location, policy));
        }

        ////Build the buildings for this location
        //Entity.Building[] buildings = Entity.Building.GetSortedArray("LocationID = ?", "Number ASC", location.ID);
        //foreach (Entity.Building building in buildings)
        //{
        //    newLocation.Buildings.Add(BuildBuilding(building));
        //}

        return newLocation;
    }

    private Messaging.Contact BuildContact(Entity.LocationContact contact)
    {
        Messaging.Contact newContact = new Messaging.Contact();

        newContact.Name = contact.Name;
        newContact.Title = contact.Title;
        newContact.Communication = new Messaging.Communication();
        newContact.Communication.PhoneNumber = contact.PhoneNumber;
        newContact.Communication.AlternatePhoneNumber = contact.AlternatePhoneNumber;
        newContact.Communication.FaxNumber = contact.FaxNumber;
        newContact.Communication.EmailAddress = contact.EmailAddress;

        return newContact;
    }

    private Messaging.Policy BuildPolicy(Entity.Survey survey, Entity.Location location, Entity.Policy policy)
    {
        Messaging.Policy newPolicy = new Messaging.Policy();

        newPolicy.Number = policy.Number;
        newPolicy.Mod = policy.Mod;
        newPolicy.Symbol = policy.Symbol;
        newPolicy.LineOfBusiness = (policy.LineOfBusiness != null) ? policy.LineOfBusiness.Name : string.Empty;
        newPolicy.HazardGrade = policy.HazardGrade;
        newPolicy.EffectiveDate = policy.EffectiveDate;
        newPolicy.ExpirationDate = policy.ExpireDate;
        newPolicy.BranchCode = policy.BranchCode;
        newPolicy.Carrier = policy.Carrier;
        newPolicy.ProfitCenter = policy.ProfitCenter;

        Entity.VWExistingCoverage[] coverages = Entity.VWExistingCoverage.GetSortedArray("SurveyID = ? && LocationID = ? && PolicyID = ? && ReportTypeCode = ?", "CoverageNameDisplayOrder DESC, CoverageValueDisplayOrder ASC", survey.ID, location.ID, policy.ID, Entity.ReportType.Report.Code);
        foreach (Entity.VWExistingCoverage coverage in coverages)
        {
            newPolicy.ReportableCoverages.Add(BuildReportableCoverage(coverage));
        }

        return newPolicy;
    }

    private Messaging.Policy BuildPolicyLite(Entity.Survey survey, Entity.Location location, Entity.Policy policy)
    {
        Messaging.Policy newPolicy = new Messaging.Policy();

        newPolicy.Number = policy.Number;
        newPolicy.Mod = policy.Mod;
        newPolicy.Symbol = policy.Symbol;
        newPolicy.LineOfBusiness = (policy.LineOfBusiness != null) ? policy.LineOfBusiness.Name : string.Empty;
        newPolicy.HazardGrade = policy.HazardGrade;
        newPolicy.EffectiveDate = policy.EffectiveDate;
        newPolicy.ExpirationDate = policy.ExpireDate;
        newPolicy.BranchCode = policy.BranchCode;
        newPolicy.Carrier = policy.Carrier;
        newPolicy.ProfitCenter = policy.ProfitCenter;

        return newPolicy;
    }

    private Messaging.ReportableCoverage BuildReportableCoverage(Entity.VWExistingCoverage coverage)
    {
        Messaging.ReportableCoverage newCoverage = new Messaging.ReportableCoverage();

        newCoverage.CoverageName = coverage.CoverageNameTypeName;
        newCoverage.CoverageType = coverage.CoverageTypeName;
        newCoverage.CoverageValue = coverage.CoverageValue;

        return newCoverage;
    }

    private Messaging.Building BuildBuilding(Entity.Building building)
    {
        Messaging.Building newBuilding = new Messaging.Building();

        newBuilding.Number = building.Number.ToString();
        newBuilding.YearBuilt = ReadInteger(building.YearBuilt);
        newBuilding.HasSprinklerSystem = (building.HasSprinklerSystem == 1);
        newBuilding.PublicProtection = ReadInteger(building.PublicProtection);
        newBuilding.BuildingValue = ReadDecimal(building.Value);
        newBuilding.Contents = ReadDecimal(building.Contents);
        newBuilding.StockValues = ReadDecimal(building.StockValues);
        newBuilding.BusinessInterruption = ReadDecimal(building.BusinessInterruption);
        newBuilding.LocationOccupancy = building.LocationOccupancy;
        newBuilding.BuildingValuationType = (building.ValuationTypeCode != string.Empty) ? building.ValuationType.Description : string.Empty;
        newBuilding.CoinsurancePercentage = (building.CoinsurancePercentage != Decimal.MinValue) ? (double)building.CoinsurancePercentage : 0.0;
        newBuilding.ChangeInEnvironmentalControlCoverage = ReadDecimal(building.ChangeInEnvironmentalControl);
        newBuilding.ScientificAnimalsCoverage = ReadDecimal(building.ScientificAnimals);
        newBuilding.ContaminationCoverage = ReadDecimal(building.Contamination);
        newBuilding.RadioActiveContaminationCoverage = ReadDecimal(building.RadioActiveContamination);
        newBuilding.FloodCoverage = ReadDecimal(building.Flood);
        newBuilding.EarthquakeCoverage = ReadDecimal(building.Earthquake);

        return newBuilding;
    }

    private Messaging.Claim BuildClaim(Entity.Claim claim)
    {
        Messaging.Claim newClaim = new Messaging.Claim();

        newClaim.PolicyNumber = claim.PolicyNumber;
        newClaim.PolicySymbol = claim.PolicySymbol;
        newClaim.PolicyExpirationDate = claim.PolicyExpireDate;
        newClaim.LossDate = claim.LossDate;
        newClaim.Claimant = claim.Claimant;
        newClaim.ClaimStatus = claim.Status;
        newClaim.LossType = claim.LossType;
        newClaim.ClaimComment = claim.Comment;

        return newClaim;
    }

    private int ReadInteger(int value)
    {
        return (value != Int32.MinValue) ? value : 0;
    }

    private decimal ReadDecimal(decimal amount)
    {
        return (amount != Decimal.MinValue) ? amount : 0.0M;
    }

    private string DetermineStatus(Entity.Survey survey)
    {
        string result;
        Entity.SurveyStatus status = survey.Status;
        
        if (status == Entity.SurveyStatus.AssignedReview ||
            status == Entity.SurveyStatus.UnassignedReview ||
            status == Entity.SurveyStatus.ClosedSurvey)
        {
            result = "Completed";
        }
        else if (status == Entity.SurveyStatus.AssignedSurvey ||
            status == Entity.SurveyStatus.AwaitingAcceptance)
        {
            result = (survey.Correction) ? "Open Correction" : "Open Assigned";
        }
        else if (status == Entity.SurveyStatus.CanceledSurvey)
        {
            result = "Cancelled";
        }
        else if (status == Entity.SurveyStatus.ReturningToCompany)
        {
            result = "Requesting Return Without Completion";
        }
        else
        {
            throw new Exception(string.Format("Survey #{0} does not have a valid status for sending to a vendor.", survey.Number));
        }

        return result;
    }
}