﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

using Berkley.BLC.Entities;
using QCI.ExceptionManagement;
using QCI.Encryption;

/// <summary>
/// Contains security related utility methods.
/// </summary>
public sealed class Security
{
    private const int MAX_FAILED_LOGINS_ALLOWED = 10;
    private const int MINUTES_LOCKED_OUT = 3;

    /// <summary>
    /// Private constructor to prevent object instantiation of this class.
    /// </summary>
    private Security() { }


    /// <summary>
    /// Authenticates a user's credentials aginst information found in the database.
    /// Failed login attempts are records and the account will be locked out after a
    /// pre-defined number of failures.
    /// </summary>
    /// <param name="username">The username of the user to authenticate.</param>
    /// <param name="password">The user-entered password to be verified.</param>
    /// <param name="failureMessage">Message that can be displayed to the end-user to tell them why authentication failed.</param>
    /// <returns>Reference to a FeeUser object if the authenticated was successful; otherwise null.</returns>
    public static FeeUser Authenticate(string username, string password, out string failureMessage)
    {
        string ipAddress = "000000000000000";

        // try to authenticate the user
        try
        {
            // try to get the user record by username
            FeeUser user = FeeUser.GetOne("Username = ?", username);

            // see if the username was valid (e.g., a user record was found)
            if (user == null)
            {
                LoginHistory login = new LoginHistory(username, ipAddress, false);
                login.Save();
                failureMessage = "The username and/or password entered was not valid.";
                return null;
            }

            // see if the account is disabled
            if (user.AccountDisabled)
            {
                RecordLoginFailure(user, ipAddress);
                failureMessage = "Your account has been disabled. Contact your administrator for more information.";
                return null;
            }

            // see if the account is currently locked out (too many failed attempts)
            if (user.LockedOutUntil != DateTime.MinValue && user.LockedOutUntil > DateTime.Now)
            {
                RecordLoginFailure(user, ipAddress);
                failureMessage = "Your account has been temporarily locked due to too many failed login attempts.\\nPlease wait a few minutes and try again.";
                return null;
            }

            // check the password by comparing hashed values
            string passwordHash = user.CreatePasswordHash(password);
            if (passwordHash != user.PasswordHash)
            {
                RecordLoginFailure(user, ipAddress);
                failureMessage = "The username and/or password entered was not valid.";
                return null;
            }

            // if we get here, user has been authenticated
            failureMessage = null;
            RecordLoginSuccess(user, ipAddress);
            return user;
        }
        catch (Exception ex)
        {
            throw new Exception("Failed to authenticate user '" + username + "'.", ex);
        }
    }

    private static void RecordLoginSuccess(FeeUser user, string ipAddress)
    {
        LoginHistory login = new LoginHistory(user.Username, ipAddress, true);
        login.Save();

        user = user.GetWritableInstance();
        user.FailedLoginAttempts = 0;
        user.LockedOutUntil = DateTime.MinValue;
        user.Save();
    }

    private static void RecordLoginFailure(FeeUser user, string ipAddress)
    {
        LoginHistory login = new LoginHistory(user.Username, ipAddress, false);
        login.Save();

        // increment the failed attempts count
        user = user.GetWritableInstance();

        // lock them out for a while if they have too many failed attempts
        user.FailedLoginAttempts += 1;
        if (user.FailedLoginAttempts >= MAX_FAILED_LOGINS_ALLOWED)
        {
            user.LockedOutUntil = DateTime.Now.AddMinutes(MINUTES_LOCKED_OUT);
            user.FailedLoginAttempts = 0;
        }

        user.Save();
    }
}
