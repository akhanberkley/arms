using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Specialized;
using System.Collections.Generic;

using Berkley.BLC.Entities;
using QCI.Web.Validation;
using Berkley.BLC.Business;
using Berkley.Imaging;


/// <summary>
/// Summary description for Document
/// </summary>
public class StreamDocument
{

    #region Properties

    public System.IO.Stream DocumentStream
    {
        get
        {
            return _contentStream;
        }
        set
        {
            _contentStream = value;
        }
    }

    public string DocMimeType
    {
        get
        {
            return _docMimeType;
        }
        set
        {
            _docMimeType = value;
        }
    }

    #endregion

    private string _fileNetReference = string.Empty;
    private string _docMimeType = string.Empty;
    private string _docName = string.Empty;
    private System.IO.Stream _contentStream = null;

    /// <summary>
    /// Performs the necessary operations to stream back a document and attach to the http response.
    /// </summary>
    /// <param name="doc">The document.</param>
    /// <param name="user">The current user.</param>
    public StreamDocument(SurveyDocument doc, Company company)
    {
        if (doc == null)
        {
            throw new NotSupportedException("The document cannot be null.");
        }

        ImagingHelper imaging = new ImagingHelper(company);
        string docExtension = GetExtension(doc.MimeType, true, doc.DocumentName);

        //--------------------------------------------------------------------------------
        //Check CompanyParameters to see if PDF Conversion should be bypassed (default no)
        //--------------------------------------------------------------------------------
        bool doPDFConversion = !(Berkley.BLC.Business.Utility.GetCompanyParameter("P8BypassPDFConversion", company).ToLower() == "true");

        //--------------------------------------------------------------------------------
        //IF NOT BYPASSED, images needed to be converted into pdfs (especially multi-paged tiffs)
        //--------------------------------------------------------------------------------
        int pageCount = imaging.RetrievePageCount(doc.FileNetReference);	//WA-756, Doug Bruce, 2015-06-23 - P8 - Used for finding/counting pages in a multipage document (split over multiple ContentList entries).
        if (doPDFConversion && (pageCount > 1 || docExtension.Contains("tif")))  //WA-905 Doug Bruce 2015-09-22 - merge tifs to pdf
        {
            IFile[] files = imaging.RetrieveFile(doc.FileNetReference, pageCount);

            List<String> fileList = new List<string>(files.Length + 1);
            foreach (Berkley.Imaging.IFile file in files)
            {
                string filePath = ConfigurationManager.AppSettings["TempDocPath"] + Guid.NewGuid().ToString() + docExtension;
                fileList.Add(filePath);

                MemoryStream ms = (MemoryStream)file.Content;
                FileStream fs = new FileStream(filePath, FileMode.Create);

                fs.Write(ms.ToArray(), 0, (int)ms.Length);
                fs.Flush();
                fs.Close();
            }

            //build our new path
            string pdfFilePath = ConfigurationManager.AppSettings["TempDocPath"] + Guid.NewGuid().ToString() + ".pdf";

            //convert the image documents into a pdf
            WRBCConvert.Converter pdfConverter = new WRBCConvert.Converter();
            try
            {
                _contentStream = pdfConverter.MakePDF(fileList.ToArray(), pdfFilePath, true);
            }
            catch (Exception)
            {
                _contentStream = null;
            }

            if (_contentStream != null)
            {
                //Conversion successful; override the type information to pdfs
                _docMimeType = "application/pdf";
                docExtension = ".pdf";  //WA-855 - Changed tif to pdf here only for special case of multiple page tifs.
            }
            else
            {
                //Conversion failed; revert to original file.
                _contentStream = imaging.RetrieveFileImage(doc.FileNetReference);
                _docMimeType = doc.MimeType;
            }
        }
        else
        {
            _contentStream = imaging.RetrieveFileImage(doc.FileNetReference);
            _docMimeType = doc.MimeType;
        }

        //WA-881 Doug Bruce 2015-08-31 - Handle case with no file extension (should not happen except with converted P8 files - filenet extensions required by ARMS UI).
        int extPos = doc.DocumentName.LastIndexOf('.');
        if (extPos >= 0)
        {
            //strip off the original extension
            _docName = doc.DocumentName.Substring(0, extPos);
            //then add our new one
            _docName += docExtension;
        }
        else
        {
            //Look up extension based on mime type
            docExtension = GetExtension(_docMimeType, false, "");
            _docName = doc.DocumentName + docExtension;
        }

        //always replace tiffs with pdfs
        //_docName = doc.DocumentName.Replace(".tiff", ".pdf").Replace(".tif", ".pdf");     //WA-855 - Removed because adobe won't read tifs renamed to pdfs.
    }

    /// <summary>
    /// Returns a document's extension based on 1) file extension, and 2) the MIME type passed.
    /// WA-855 Doug Bruce 2015-08-25 - If they conflict, filename extension wins, except for 
    /// .tif case below.
    /// </summary>
    /// <param name="mimeType">The MIME Type.</param>
    public static string GetExtension(string mimeType, bool returnImageExt, string fileName)
    {
        mimeType = mimeType.Trim().ToLower();
        string fileExtension = System.IO.Path.GetExtension(fileName).ToLower();
        string result = fileExtension;

        MimeType mimeTypeObject = MimeType.GetOne("Name = ?", mimeType);

        if (mimeTypeObject != null)
        {
            if (mimeTypeObject.DocumentExtension == ".tif")
            {
                result = (returnImageExt) ? mimeTypeObject.DocumentExtension : ".pdf";
            }
            else if (System.IO.Path.GetExtension(fileName) == ".dcd")
            {
                result = ".dcd";
            }
            else
            {
                //WA-881 Doug Bruce 2015-08-31 - Added to support generic extension retrieval from mime type
                result = mimeTypeObject.DocumentExtension;
            }
        }
        else
        {
            throw new NotSupportedException(string.Format("Was not expecting a MIME type of: '{0}'.", mimeType));
        }

        return result;
    }
}
