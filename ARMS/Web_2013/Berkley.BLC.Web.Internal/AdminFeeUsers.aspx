<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminFeeUsers.aspx.cs" Inherits="AdminFeeUsersAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span>
    <span style="MARGIN-LEFT: 20px">
	    <a class="nav" href="AdminUser.aspx?id=<%= _companyUser.ID %>"><< Back to Company Details</a>
    </span><br>
    for <%= HtmlEncode(_companyUser.Name) %><br>
    <br>

    <asp:CheckBox ID="chkShowDisabled" Runat="server" CssClass="chk" AutoPostBack="True" Text="Show users with disabled accounts." />
    <asp:datagrid ID="grdUsers" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="30" AllowSorting="True">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
		    <asp:hyperlinkcolumn HeaderText="Name" DataTextField="Name" 
			    DataNavigateUrlField="ID" DataNavigateUrlFormatString="AdminFeeUserEdit.aspx?id={0}" 
			    SortExpression="Name ASC" />
		    <asp:BoundColumn HeaderText="Username" DataField="Username" SortExpression="Username ASC" />
		    <asp:BoundColumn HeaderText="Phone" DataField="PhoneNumber" SortExpression="PhoneNumber ASC, Name ASC" />
		    <asp:BoundColumn HeaderText="Alt Phone" DataField="AlternatePhoneNumber" SortExpression="AlternatePhoneNumber ASC, Name ASC" />
		    <asp:hyperlinkcolumn HeaderText="Email Address" DataTextField="EmailAddress" 
			    DataNavigateUrlField="EmailAddress" DataNavigateUrlFormatString="mailto:{0}" 
			    SortExpression="EmailAddress ASC, Name ASC" />
	    </columns>
    </asp:datagrid>

    <a href="AdminFeeUserEdit.aspx?cid=<%= _companyUser.ID %>">Create New Fee Consultant</a>
    
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
