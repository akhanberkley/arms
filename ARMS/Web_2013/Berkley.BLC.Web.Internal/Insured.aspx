<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" ValidateRequest="false" CodeFile="Insured.aspx.cs" Inherits="InsuredAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="ComboSearch" Src="~/Fields/ComboFieldSearch.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
    <script language="javascript">var counter = 0;</script>
</head>
<body style="margin:0px">
    <link rel="stylesheet" media="screen" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <link rel="stylesheet" media="print" type="text/css" href="<%= base.TemplatePath %>/Print-Insured.css" />

    <div class="insuredscreen">
    <form id="frm" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <uc:Header ID="ucHeader" runat="server" />
    
    <% if (CheckRefresh()) { %>
    <script type="text/javascript">
        $(document).ready(function() { 
             $(function () { 
                $(window).load(function () { 
                    $("#btnRefreshData").click();
                }); 
             })
        });
    </script>
    <% } %>
    
    <span class="headingForPrint" id="lblHeadingForPrint" runat="server">Survey Location:&nbsp;<%= cboLocations.SelectedItem %></span>
    <span class="heading">Survey Location:&nbsp;<asp:dropdownlist id="cboLocations" runat="server" AutoPostBack="True" CssClass="cbo"></asp:dropdownlist></span> 
    <%if (ARMSUtility.GetBoolFromQueryString("locations", false)) {%>
        <a class="nav" id="BackToLocations" href="Locations.aspx<%= ARMSUtility.GetQueryStringForNav("locations") %>"><< Back to Locations</a>
    <%} else { %>
        <a class="nav" id="BackToSurvey" href="Survey.aspx<%= ARMSUtility.GetQueryStringForNav("locationid") %>"><% if (_eSurvey.ServiceType == Berkley.BLC.Entities.ServiceType.ServiceVisit) {%><< Back to Service Visit<%} else {%><< Back to Survey<%}%></a>
    <%} %>
    <br><br>
    
    <%-- // Place a table containing a drop down selection for the view type: Print, Read-Only, Edit --%>
    <div class="view" style="margin-bottom:10px">
    <table width="725" cellpadding="0" cellspacing="0">
        <tr>
            <td width="10"><asp:dropdownlist id="cboMode" runat="server" AutoPostBack="True" CssClass="cbo"></asp:dropdownlist></td>
            <td>&nbsp;&nbsp;<input type="button" id="btnPrintPage" value="Print" class="btn" onClick="window.print()" /></td>
            <td align="right">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <%if (_eSurvey.PrimaryPolicy != null && !SurveyTypeType.IsNotWrittenBusiness(_eSurvey.Type.Type) && CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID && String.IsNullOrEmpty(_eSurvey.WorkflowSubmissionNumber))
                          {%>
                        <td align="right">Last Refreshed: <span style="font-size:13px; font-weight:bold"><%= GetRefreshDate().ToShortDateString() %></span>&nbsp;&nbsp;&nbsp;<asp:Button ID="btnRefreshData" runat="server" CssClass="btn" Text="Refresh Data" /></td>
                        <% } %>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </div>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    
    <div class="surveyDetails" id="divSurvey" runat="server">
    <cc:Box id="boxSurvey" runat="server" title="Survey Information" width="725" EncodeTitle="false">
        <table>
            <tr>
                <td valign="top" width="350px">
                    <table class="fields">
                        <uc:Label id="lblNumber" runat="server" field="Survey.Number" />
                        <uc:Label id="lblStatus" runat="server" field="SurveyStatus.Name" />
                        <uc:Label id="lblCreateDate" runat="server" field="Survey.CreateDate" Name="Created On" />
                        <uc:Label id="lblCreatedBy" runat="server" field="Survey.HtmlCreatedBy" Name="Created By" />
                        <uc:label id="lblAssignDate" runat="server" field="Survey.AssignDate" Name="Assigned Date" />
                        <uc:label id="lblAcceptDate" runat="server" field="Survey.AcceptDate" Name="Accepted Date" />
                    </table>
                </td>
                <td width="10px" align="center"><img class="sep" src="<%= AppPath %>/images/black_pixel.gif" height="90px" width="1px"></td>
                <td valign="top">
                    <table class="fields">
                        <uc:Label id="lblDueDate" runat="server" field="Survey.DueDate" />
                        <uc:Label id="lblType" runat="server" field="SurveyTypeType.Name" Name="Survey Type" />
                        <uc:Label id="lblAssignedUser" runat="server" field="User.Name" Name="Assigned Rep" />
                        <% if (CurrentUser.Company.DisplayPolicyPremium) { %>
                        <uc:Label id="lblTotalPremium" runat="server" field="VWSurvey.PolicyPremium" Name="Total Premium" ValueFormat="{0:c}" />
                        <% } %>
                    </table>
                </td>
            </tr>
        </table>
    </cc:Box>
    </div>
    
    <hr class="boxBreak" />
    
    <%if (cboMode.SelectedValue == ScreenMode.EditInsured || cboMode.SelectedValue == ScreenMode.EditInsuredAndPolicy){%>
    <cc:Box id="boxLocation" runat="server" title="Location" width="725">
        <table class="fields">
            <uc:Number id="numLocationNumber" runat="server" field="Location.Number" Columns="10" IsRequired="True" Group="txt" />
            <uc:Text id="txtLocationStreetLine1" runat="server" field="Location.StreetLine1" Name="Address 1" Columns="70" IsRequired="True" Group="txt" />
            <uc:Text id="txtLocationStreetLine2" runat="server" field="Location.StreetLine2" Name="Address 2" Columns="70" Group="txt" />
            <uc:Text id="txtLocationCity" runat="server" field="Location.City" IsRequired="True" Group="txt" />
            <uc:Combo id="cboLocationState" runat="server" field="Location.StateCode" Name="State" topItemText="" isRequired="True" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" Group="txt" />
            <uc:Text id="txtLocationZipCode" runat="server" field="Location.ZipCode" IsRequired="True" Group="txt" />
            <uc:Text id="txtLocationDescription" runat="server" field="Location.Description" Name="Location Description" Rows="1" Directions="(ex. 'Apartment Complex')" Columns="63" Group="txt" />
            <tr>
                <td>&nbsp;</td>
                <td class="buttons">
                    <asp:Button id="btnUpdateLocation" Runat="server" CssClass="btn" Text="Save Location Updates" />
                </td>
            </tr>
        </table>
    </cc:Box>
    <hr class="boxBreak" />
    <% } %>    

    <cc:Box id="boxInsured" runat="server" title="Insured" width="725" EncodeTitle="false">
    <div id="boxInsuredContent">
        <%if (cboMode.SelectedValue == ScreenMode.EditInsured || cboMode.SelectedValue == ScreenMode.EditInsuredAndPolicy){%>
        <table class="fields">
            <uc:Text id="txtClientID" runat="server" field="Insured.ClientID" Columns="25" IsRequired="True" Group="txt" />
            <% if (CurrentUser.Company.SupportsPortfolioID) {%>
            <uc:Text id="txtPortfolioID" runat="server" field="Insured.PortfolioID" Columns="25" IsRequired="False" Group="txt" />
            <% } %>
            <uc:Text id="txtName" runat="server" field="Insured.Name" Columns="60" IsRequired="True" Group="txt" />
            <uc:Text id="txtName2" runat="server" field="Insured.Name2" Columns="60" Group="txt" />
            <uc:Text id="txtStreetLine1" runat="server" field="Insured.StreetLine1" Name="Mailing Address 1" Columns="70" IsRequired="True" Group="txt" />
            <uc:Text id="txtStreetLine2" runat="server" field="Insured.StreetLine2" Name="Mailing Address 2" Columns="70" Group="txt" />
            <uc:Text id="txtStreetLine3" runat="server" field="Insured.StreetLine3" Name="Mailing Address 3" Columns="70" Group="txt" />
            <uc:Text id="txtCity" runat="server" field="Insured.City" IsRequired="True" Group="txt" />
            <uc:Combo id="cboState" runat="server" field="Insured.StateCode" Name="State" topItemText="" isRequired="True" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" Group="txt" />
            <uc:Text id="txtZipCode" runat="server" field="Insured.ZipCode" IsRequired="True" Group="txt" />
            <uc:Text id="txtBusinessOperations" runat="server" field="Insured.BusinessOperations" Columns="75" Rows="1" Name="Business Ops." IsRequired="True" Group="txt" />
            
            <% if (CurrentUser.Company.SupportsBusinessCategory) {%>
            <uc:Text id="txtBusinessCategory" runat="server" field="Insured.BusinessCategory" Columns="15" Name="Business Category" IsRequired="False" Group="txt" />
            <% } %>
            <% if (CurrentUser.Company.SupportsDOTNumber) {%>
            <uc:Text id="txtDOTNumber" runat="server" field="Insured.DOTNumber" Name="DOT Number" IsRequired="False" Group="txt" />
            <% } %>
            <uc:Text id="txtCompanyWebsite" runat="server" field="Insured.CompanyWebsite" Columns="75" Rows="1" IsRequired="False" Group="txt" />
            <% if (!_hideSICCode) {%>
            <uc:ComboSearch id="cboSIC" runat="server" field="Insured.SICCode" Name="SIC" topItemText="" isRequired="True" Group="txt" />
            <% } %>
            <% if (CurrentUser.Company.SupportsNAICS) {%>
            <uc:ComboSearch id="cboNAIC" runat="server" field="Insured.NAICSCode" Name="NAICS" topItemText="" isRequired="True" Group="txt" />
            <% } %>
            <uc:Combo id="cboUnderwriter" runat="server" field="Survey.UnderwriterCode" Name="Underwriter" topItemText="" isRequired="True" IgnoreMapping="true" Group="txt" />
            <%if (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplaySecUnderwriter", CurrentUser.Company).ToUpper() == "TRUE"){%>
            <uc:Combo id="cboUnderwriter2" runat="server" field="Survey.Underwriter2Code" Name="Underwriter Sec" topItemText=""  IgnoreMapping="true" Group="txt" />
            <% } %>
            <%if (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplayContactOnInsured", CurrentUser.Company).ToUpper() == "TRUE") {%>
            <uc:Text ID="txtContactName" runat="server" Field="Insured.ContactName" Name="Primary Contact" IsRequired="false" Group="txt"  />
            <uc:Text ID="txtContactPhone" runat="server" Field="Insured.ContactPhoneNumber" Name="Contact Phone" IsRequired="false" Group="txt"  />
            <uc:Text ID="txtcontactEmail" runat="server" Field="Insured.ContactEmail" Name="Contact Email" Columns="70" IsRequired="false" Group="txt"  />      
            <% }  %>
            <%if (Berkley.BLC.Business.Utility.GetCompanyParameter("HideAccountStatus", CurrentUser.Company).ToUpper() == "TRUE") {%>
            <tr>
                <td class="label" valign="top" nowrap>Account Status</td>
                <td class="lbl"><%= GetAccountStatus(_eSurvey)%></td>
            </tr>
            <% } %>
            <tr>
                <td>&nbsp;</td>
                <td class="buttons">
                    <asp:Button id="btnUpdateInsured" Runat="server" CssClass="btn" Text="Save Insured Updates" />
                </td>
            </tr>
        </table>
        <%}else{%>
        <table>
            <tr>
                <td valign="top" width="350px">
                    <table class="fields">
                        <uc:Label id="lblClientID" runat="server" field="Insured.ClientID" Group="lbl" />
                        <% if (CurrentUser.Company.SupportsPortfolioID) {%>
                        <uc:Label id="lblPortfolioID" runat="server" field="Insured.PortfolioID" Group="lbl" />
                        <% } %>
                        <uc:Label id="lblName" runat="server" field="Insured.Name" Group="lbl" />
                        <uc:Label id="lblName2" runat="server" field="Insured.Name2" Group="lbl" />
                        <uc:Label id="lblAddress" runat="server" field="Insured.HtmlAddress" Name="Address" Group="lbl" />
                        <uc:Label id="lblBusinessOperations" runat="server" field="Insured.BusinessOperations" Name="Business Ops." Group="lbl" />
                        <% if (CurrentUser.Company.SupportsBusinessCategory) {%>
                        <uc:Label id="lblBusinessCategory" runat="server" field="Insured.BusinessCategory" Name="Business Category" Group="lbl" />
                        <% } %>
                        <% if (CurrentUser.Company.SupportsDOTNumber) {%>
                        <uc:Label id="lblDOTNumber" runat="server" field="Insured.DOTNumber" Name="DOT Number" Group="lbl" />
                        <% } %>
                        <uc:Label id="lblCompanyWebsite" runat="server" field="Insured.CompanyWebsite" Group="lbl" />
                        <% if (!_hideSICCode) {%>
                        <uc:Label id="lblSIC" runat="server" field="Insured.HtmlSIC" Name="SIC" Group="lbl" />
                        <% } %>
                        <% if (CurrentUser.Company.SupportsNAICS) {%>
                        <uc:Label id="lblNAICS" runat="server" field="Insured.NAICSCode" Name="NAICS" Group="lbl" />
                        <% } %>
                    </table>
                </td>
                <td width="10px" align="center"><img class="sep" src="<%= AppPath %>/images/black_pixel.gif" height="110px" width="1px"></td>
                <td valign="top">
                    <table class="fields">
                        <uc:Label id="lblUnderwriter" runat="server" field="Survey.HtmlUnderwriterName" Name="Underwriter" Group="lbl" />
                        <uc:Label id="lblUnderwriterPhone" runat="server" field="Survey.HtmlUnderwriterPhone" Name="Underwriter Phone" Group="lbl" />
                        <%if (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplaySecUnderwriter", CurrentUser.Company).ToUpper() == "TRUE"){%>
                        <uc:Label id="lblUnderwriter2" runat="server" field="Survey.HtmlUnderwriter2Name" Name="Underwriter2" Group="lbl" />                        
                        <uc:Label id="lblUnderwriter2Phone" runat="server" field="Survey.HtmlUnderwriter2Phone" Name="Underwriter2 Phone" Group="lbl" />
                        <% }  %>
                        <%if (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplayContactOnInsured", CurrentUser.Company).ToUpper() == "TRUE") {%>
                        <uc:Label ID="lblContactName" runat="server" Field="Insured.ContactName" Name="Primary Contact" Group="lbl"  />
                        <uc:Label ID="lblContactPhoneNumber" runat="server" Field="Insured.ContactPhoneNumber" Name="Contact Phone" Group="lbl"  />
                        <uc:Label ID="lblContactEmail" runat="server" Field="Insured.ContactEmail" Name="Contact Email" Group="lbl"  />      
                        <% }  %>
                        <tr>
                            <td class="label" valign="top" nowrap>Account Status</td>
                            <td class="lbl"><%= GetAccountStatus(_eSurvey)%></td>
                        </tr>
                    </table>
                </td>
            </tr>
            
        </table>
        <% } %>
    </div>
    <div id="boxInsuredPrintMessage" style="DISPLAY: none">
        This details of this section have been set to not print.
    </div>
    </cc:Box>
    
    <hr class="boxBreak" />
  
    <cc:Box id="boxContacts" runat="server" Title="Contacts" width="725" EncodeTitle="false">
    <div id="boxContactsContent">
        <% if (cboMode.SelectedValue == ScreenMode.EditInsured || cboMode.SelectedValue == ScreenMode.EditInsuredAndPolicy) {%>
            <table width="100%">
                <tr>
                    <td style="white-space:nowrap;">
                        <strong>Note: Update Contact Information at Each and Every Visit as Needed</strong>
                    </td>
                    <td align="right" style="width:100%;">
                        <img width="20px" height="20px" src="<%= ARMSUtility.AppPath %>/images/arrow_forward.png" alt="" />
                    </td>
                    <td style="white-space:nowrap;">
                        <a id="lnkContactManager" href="ContactManager.aspx<%= ARMSUtility.GetQueryStringForNav() %>">Manage All Contacts</a>
                    </td>
                </tr>
            </table>
            <asp:datagrid ID="grdContacts" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
                <headerstyle CssClass="header" />
                <itemstyle CssClass="item" />
                <alternatingitemstyle CssClass="altItem" />
                <footerstyle CssClass="footer" />
                <columns>
                    <asp:BoundColumn HeaderText="Rank" DataField="PriorityIndex" />
                    <asp:TemplateColumn HeaderText="Name">
                        <ItemTemplate>
                            <a href="Contact.aspx<%= ARMSUtility.GetQueryStringForNav() %>&contactid=<%# HtmlEncode(Container.DataItem, "ID") %>">
                                <%# HtmlEncodeNullable(Container.DataItem, "Name", string.Empty, "(none)") %>
                            </a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Title">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Title", string.Empty, "(none)") %>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Phone">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "HtmlPhoneNumber", string.Empty, "(none)") %>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Email">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "EmailAddress", string.Empty, "(none)") %>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Mail Options">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "ContactReasonType.ShortDescription", null, "(none)") %>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Move">
                        <ItemTemplate>
                            &nbsp;<asp:LinkButton ID="btnUp" Runat="server" CommandName="MoveUp">Up</asp:LinkButton>&nbsp;/
                            &nbsp;<asp:LinkButton ID="btnDown" Runat="server" CommandName="MoveDown">Down</asp:LinkButton>&nbsp;
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            &nbsp;<asp:LinkButton ID="btnRemoveContact" Runat="server" CommandName="Remove" OnPreRender="btnRemoveContact_PreRender" CausesValidation="false">Remove</asp:LinkButton>&nbsp;
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </columns>
            </asp:datagrid>
        
            <a href="Contact.aspx<%= ARMSUtility.GetQueryStringForNav("contactid") %>">Add Contact</a>
        <%} else {%>
            <% if (repContacts.Items.Count > 0) { %>
            <asp:Repeater ID="repContacts" Runat="server">
                <HeaderTemplate>
                    <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0" cellpadding="0">
                        <tr>
                            <td nowrap><u>Name</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Title</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Phone</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Alt Phone</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Fax</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Email</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Mail Options</u></td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Name", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Title", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px" nowrap><%# HtmlEncode(Container.DataItem, "HtmlPhoneNumber")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncode(Container.DataItem, "HtmlAltPhoneNumber")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncode(Container.DataItem, "HtmlFaxNumber")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "EmailAddress", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "ContactReasonType.ShortDescription", null, "(none)")%></td>
                    </tr>			
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <%} else {%>
            <div style="PADDING-LEFT: 10px">
                (none)
            </div>
            <% } %>
        <% } %>
    </div>
    <div id="boxContactsPrintMessage" style="DISPLAY: none">
        This details of this section have been set to not print.
    </div>
    </cc:Box>
    
    <hr class="boxBreak" />
    
    <cc:Box id="boxAgency" runat="server" Title="Agency" width="725" EncodeTitle="false">
    <div id="boxAgencyContent">
        <%if (cboMode.SelectedValue == ScreenMode.Print) {%><table><tr><td valign="top" width="350px"><% } %>
        
        <table class="fields">
            
            <%if (cboMode.SelectedValue == ScreenMode.EditInsured || cboMode.SelectedValue == ScreenMode.EditInsuredAndPolicy){%>
                <uc:Text id="txtAgencyNumber" runat="server" field="Agency.Number" IsRequired="True" Group="txt" />                
                <uc:Text id="txtAgencyName" runat="server" field="Agency.Name" IsRequired="True" Group="txt" />
                <uc:Text id="txtAgencyStreetLine1" runat="server" field="Agency.StreetLine1" Name="Street Address 1" Columns="70" IsRequired="True" Group="txt" />
                <uc:Text id="txtAgencyStreetLine2" runat="server" field="Agency.StreetLine2" Name="Street Address 2" Columns="70" Group="txt" />
                <uc:Text id="txtAgencyCity" runat="server" field="Agency.City" IsRequired="True" Group="txt" />
                <uc:Combo id="cboAgencyState" runat="server" field="Agency.StateCode" Name="State" topItemText="" isRequired="True" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" Group="txt" />
                <uc:Text id="txtAgencyZip" runat="server" field="Agency.ZipCode" IsRequired="True" Group="txt" />
                <uc:Text id="txtAgencyPhone" runat="server" field="Agency.PhoneNumber" IsRequired="true" Directions="(ex. 5551234567)" Name="Phone Number" Group="txt" /> 
                <uc:Text id="txtAgencyFax" runat="server" field="Agency.FaxNumber" IsRequired="false" Directions="(ex. 5551234567)" Name="Fax Number" Group="txt" /> 
                
            <%}else{%>

                <uc:Label id="lblAgencyNumber" runat="server" field="Agency.Number" Group="lbl" />                
                <uc:Label id="lblAgencyName" runat="server" field="Agency.Name" Group="lbl" />
                <uc:Label id="lblAgencyAddress" runat="server" field="Agency.HtmlAddress" Name="Agency Address" Group="lbl" />
         
                <%if (cboMode.SelectedValue == ScreenMode.Print) {%>
                </table></td>
                <td width="10px" align="center"><img class="sep" src="<%= AppPath %>/images/black_pixel.gif" height="90px" width="1px"></td>
                <td valign="top"><table class="fields">
                <% } %>
      
                <uc:Label id="lblAgencyPhone" runat="server" field="Agency.HtmlPhoneNumber" Name="Phone Number" Group="lbl" />
                <uc:Label id="lblAgencyFax" runat="server" field="Agency.HtmlFaxNumber" Name="Fax Number" Group="lbl" />
            <% } %>

            <%if (cboMode.SelectedValue == ScreenMode.ReadOnly || cboMode.SelectedValue == ScreenMode.Print || cboMode.SelectedValue == ScreenMode.EditPolicy) {%>                
                <uc:Label id="lblProducer" runat="server" field="Agency.Producer" Group="lbl" />                
                <uc:Label id="lblAgentContactName" runat="server" field="Insured.AgentContactName" Name="Agent Contact Name" Group="lbl" />
                <uc:Label id="lblAgentContactPhone" runat="server" field="Insured.AgentContactPhoneNumber" Name="Agent Phone" Group="lbl" />
                <uc:Label id="lblAgenctEmail" runat="server" field="Insured.HtmlAgentContactEmail" Name="Agent Email" Group="lbl" />
            
                <%if (cboMode.SelectedValue == ScreenMode.Print) {%>
                </table></td></tr>
                <% } %>
            
            <%}else{%>                                
                <uc:Text id="txtProducer" runat="server" field="Agency.Producer" IsRequired="False" Group="txt" />                   
                <uc:Text id="txtAgentContactName" runat="server" field="Insured.AgentContactName" Name="Agent Contact Name" IsRequired="False" Group="txtAgent" />
                <uc:Text id="txtAgentContactPhone" runat="server" field="Insured.AgentContactPhoneNumber" Name="Agent Phone" IsRequired="False" Group="txtAgent" />
                <uc:Combo id="cboAgentEmail" runat="server" field="Insured.AgentEmailID" Name="Agent Email" topItemText="-- select or add email --" isRequired="False" Group="txtAgent" />
                <tr id="rowLnkAddAgencyEmail">
                    <td>&nbsp;</td>
                    <td>
                        <a href="javascript:Toggle('rowAddAgencyEmail'); Toggle('rowLnkAddAgencyEmail');">Add Email to List</a>
                    </td>
                </tr>
                <tr id="rowAddAgencyEmail" style="DISPLAY: none" runat="Server">
                    <td>&nbsp;</td>
                    <td>
                        New Email<br />
                        <asp:TextBox ID="txtNewAgencyEmail" runat="server" Columns="60" MaxLength="100" CssClass="txt"></asp:TextBox><br /><br />
                        <asp:Button ID="btnAddEmail" Runat="server" CssClass="btn" Text="Add" CausesValidation="False" />&nbsp;
                        <input type="button" value="Cancel" class="btn" onclick="javascript:Toggle('rowAddAgencyEmail'); Toggle('rowLnkAddAgencyEmail'); document.getElementById('txtNewAgencyEmail').innerText = ''">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="buttons">
                        <asp:Button id="btnUpdateAgency" Runat="server" CssClass="btn" Text="Save Agency Updates" />
                    </td>
                </tr>
            <% } %>
        </table>
    </div>
    <div id="boxAgencyPrintMessage" style="DISPLAY: none">
        This details of this section have been set to not print.
    </div>
    </cc:Box>
    
    <hr class="boxBreak" />
    
    <% if (cboMode.SelectedValue == ScreenMode.Print) {%>
    <cc:Box id="boxPolicySummary" runat="server" Title="Policies" width="725" EncodeTitle="false">
    <div id="boxPolicySummaryContent">
        <% if (repPolicySummary.Items.Count > 0) { %>
            <asp:Repeater ID="repPolicySummary" Runat="server">
                <HeaderTemplate>
                    <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0">
                        <tr>
                            <td nowrap><u>Policy #</u></td>
                            <td style="PADDING-LEFT: 15px"><u>Mod</u></td>
                            <td style="PADDING-LEFT: 15px"><u>Symbol</u></td>
                            <td style="PADDING-LEFT: 15px"><u>LOB</u></td>
                            <% if (_hideHazardGrade!="true"){ %>
                            <td style="PADDING-LEFT: 15px"><u>Hazard Grade</u></td>
                            <% } %>
                            <td style="PADDING-LEFT: 15px"><u>Effective</u></td>
                            <td style="PADDING-LEFT: 15px"><u>Expiration</u></td>
                            <td style="PADDING-LEFT: 15px"><u>Premium</u></td>
                            <td style="PADDING-LEFT: 15px"><u>Primary State</u></td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Mod", Int32.MinValue, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Symbol", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "LineOfBusiness.Name", string.Empty, "(none)")%></td>
                        <% if (_hideHazardGrade!="true"){ %>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "HazardGrade", string.Empty, "(none)")%></td>
                        <% } %>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "EffectiveDate", "{0:d}", DateTime.MinValue, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "ExpireDate", "{0:d}", DateTime.MinValue, "(none)")%></td>
                        <% if (CurrentUser.Company.DisplayPolicyPremium) { %>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Premium", "{0:C0}", decimal.MinValue, "(none)")%></td>
                        <% } else { %>
                            <td valign="top" style="PADDING-LEFT: 15px">NA</td>
                        <% } %>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "StateCode", string.Empty, "(none)")%></td>
                    </tr>			
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        <%} else {%>
        <div style="PADDING-LEFT: 10px">
            (none)
        </div>
        <% } %>
    </div>
    <div id="boxPolicySummaryPrintMessage" style="DISPLAY: none">
        This details of this section have been set to not print.
    </div>   
    </cc:Box>
    
    <hr class="boxBreak" />
    <% } %>

    
    <% if (CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID) { %> 
    <cc:Box id="boxPolicies" runat="server" Title="Policies/Coverages" width="725" EncodeTitle="false">
    <div id="boxPoliciesContent">
        <% if (cboMode.SelectedValue == ScreenMode.EditPolicy || cboMode.SelectedValue == ScreenMode.EditInsuredAndPolicy) {%>
            <asp:datagrid ID="grdPolicies" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
                <headerstyle CssClass="header" />
                <itemstyle CssClass="item" />
                <alternatingitemstyle CssClass="altItem" />
                <footerstyle CssClass="footer" />
                <columns>
                    <asp:TemplateColumn HeaderText="Policy #">
                        <ItemTemplate>
                                    <a href="CreateSurveyPolicy.aspx<%= ARMSUtility.GetQueryStringForNav() %>&policyid=<%# HtmlEncode(Container.DataItem, "ID")%>">
                                        <%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(none)").PadLeft(3,'0')%>
                                    </a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Mod">
                        <ItemTemplate>
                                <%# HtmlEncodeNullable(Container.DataItem, "Mod", Int32.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Symbol">
                        <ItemTemplate>
                                <%# HtmlEncodeNullable(Container.DataItem, "Symbol", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="LOB">
                        <ItemTemplate>
                            <%# HtmlEncode(Container.DataItem, "LineOfBusiness.Name") %>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Hazard Grade">
                        <ItemTemplate>
                                <%# HtmlEncodeNullable(Container.DataItem, "HazardGrade", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Effective" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "EffectiveDate", "{0:d}", DateTime.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Expiration" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "ExpireDate", "{0:d}", DateTime.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Premium">
                        <ItemTemplate>
                        <% if (CurrentUser.Company.DisplayPolicyPremium) { %>
                            <%# HtmlEncodeNullable(Container.DataItem, "Premium", "{0:C}", Decimal.MinValue, "(none)")%>
                        <% } else { %>
                            NA
                        <% } %>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Primary State">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "StateCode", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Reporting">
                        <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                                <asp:LinkButton ID="lnkEditReports" runat="server" OnClick="lnkEditReports_OnClick" CausesValidation="false">
                                    Edit
                                </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Remove">
                        <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                                <asp:LinkButton ID="lnkRemovePolicy" runat="server" CommandName="Remove" OnPreRender="lnkRemovePolicy_PreRender" CausesValidation="false">
                                    Remove
                                </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Include" >
                        <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkPolicy" runat="server" AutoPostBack="True" OnCheckedChanged="chkPolicy_CheckedChanged"></asp:CheckBox> 
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </columns>
            </asp:datagrid>

            <a href="CreateSurveyPolicy.aspx<%= ARMSUtility.GetQueryStringForNav() %>">Add Line of Business</a>

        <%} else {%>
            <% if (repPolicies.Items.Count > 0) { %>
            <asp:Repeater ID="repPolicies" Runat="server">
            <ItemTemplate>

                <a class="lnkPolicy" href="javascript:Toggle('<%# DataBinder.Eval(Container.DataItem, "ID") %>')"><%# GetSinglePolicyLine(Container.DataItem) %> </a>
                <% if (cboMode.SelectedValue != ScreenMode.Print) {%>
                <div id="<%# HtmlEncode(Container.DataItem, "ID") %>" style="MARGIN-TOP: 10px; DISPLAY: none">
                <% } %>
                    <table style="margin-left: 20px">
                        <%if (_eSurvey.Type != null) {%>
                        <tr><td><u><b>Policy Details</b></u></td></tr>
                        <tr><td> 
                            <table class="fields"   >
                                <tr>
                                    <td class="label" valign="top" nowrap>Policy Number</td>
                                    <td><%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(not specified)")%></td>
                                </tr>
                                <tr>
                                    <td class="label" valign="top" nowrap>Policy Mod</td>
                                    <td><%# HtmlEncodeNullable(Container.DataItem, "Mod", Int32.MinValue, "(not specified)")%></td>
                                </tr>
                                <tr>
                                    <td class="label" valign="top" nowrap>Policy Symbol</td>
                                    <td><%# HtmlEncodeNullable(Container.DataItem, "Symbol", string.Empty, "(not specified)")%></td>
                                </tr>
                                <tr>
                                    <td class="label" valign="top" nowrap>Line of Business</td>
                                    <td><%# HtmlEncodeNullable(Container.DataItem, "LineOfBusiness.Name", string.Empty, "(not specified)")%></td>
                                </tr>
                                <tr>
                                    <td class="label" valign="top" nowrap>Effective Date</td>
                                    <td><%# HtmlEncodeNullable(Container.DataItem, "EffectiveDate", "{0:d}", DateTime.MinValue, "(not specified)")%></td>
                                </tr>
                                <tr>
                                    <td class="label" valign="top" nowrap>Expiration Date</td>
                                    <td><%# HtmlEncodeNullable(Container.DataItem, "ExpireDate", "{0:d}", DateTime.MinValue, "(not specified)")%></td>
                                </tr>
                                <tr>
                                    <td class="label" valign="top" nowrap>Premium</td>
                                    <% if (CurrentUser.Company.DisplayPolicyPremium) { %>
                                        <td><%# HtmlEncodeNullable(Container.DataItem, "Premium", "{0:C}", Decimal.MinValue, "(not specified)")%></td>
                                    <% } else { %>
                                        <td>NA</td>
                                    <% } %>
                                </tr>

                                <% if (_hideHazardGrade!="true"){ %>
                                <tr>
                                    <td class="label" valign="top" nowrap>Hazard Grade</td>
                                    <td><%# HtmlEncodeNullable(Container.DataItem, "HazardGrade", string.Empty, "(not specified)")%></td>
                                </tr>
                                <% } %>

                                <tr>
                                    <td class="label" valign="top" nowrap><% =CurrentUser.Company.PolicyBranchCodeLabel %></td>
                                    <td><%# HtmlEncodeNullable(Container.DataItem, "BranchCode", string.Empty, "(not specified)") %></td>
                                </tr>
                                <tr>
                                    <td class="label" valign="top" nowrap>Carrier</td>
                                    <td><%# HtmlEncodeNullable(Container.DataItem, "Carrier", string.Empty, "(not specified)") %></td>
                                </tr>
                                <tr>
                                    <td class="label" valign="top" nowrap>Profit Center</td>
                                    <td><%# GetProfitCenterName(Container.DataItem) %></td>
                                </tr>
                                <tr>
                                    <td class="label" valign="top" nowrap>Primary State</td>
                                    <td><%# HtmlEncodeNullable(Container.DataItem, "StateCode", string.Empty, "(not specified)") %></td>
                                </tr>
                            </table>
                            </td></tr>
                            <%} %>
                            <tr><td><u><b>Coverages</b></u></td></tr>
                            <tr><td>
                            
                            <asp:Repeater ID="repCoverages" DataSource="<%# GetCoveragesForPolicy(Container.DataItem) %>" Runat="server" OnItemCreated="repCoverages_ItemCreated">
                            <ItemTemplate>
                                    <tr id="rowCoverageName" runat="server">
                                        <td class="label" valign="top" nowrap><b><%# HtmlEncode(Container.DataItem, "DisplayCoverageName")%></b></td>
                                        <td><%# HtmlEncode(Container.DataItem, "DisplayReport")%></td>
                                    </tr>
                                    <tr>
                                        <asp:Repeater ID="repCoverageValues" Runat="server" DataSource="<%# GetValuesForCoverage(Container.DataItem) %>">
                                            <ItemTemplate>
                                                <tr id="rowCoverageValue" runat="server">
                                                    <td class="label" valign="top" nowrap><%# GetEncodedName(Container.DataItem) %></td>
                                                    <td><%# GetEncodedValue(Container.DataItem) %></td>
                                                    <td><%# GetCoverageDetails(Container.DataItem)%></td>  
                                                </tr>

                                            </ItemTemplate>

                                        </asp:Repeater>
                                    </tr>
                            </ItemTemplate>
                            <SeparatorTemplate>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </SeparatorTemplate>
                            <HeaderTemplate>
                                <table class="fields">
                            </HeaderTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                            </asp:Repeater>
                                </td></tr>
                            </table>
                            <% if (cboMode.SelectedValue != ScreenMode.Print) {%> </div> <% } %>
                    </ItemTemplate>
                    <SeparatorTemplate>
                        <br>
                        <hr>
                    </SeparatorTemplate>
            </asp:Repeater>
            <%} else {%>
            <div style="PADDING-LEFT: 10px">
                (none)
            </div>
            <% } %>
            
        <% } %>
    </div>
    <div id="boxPoliciesPrintMessage" style="DISPLAY: none">
        This details of this section have been set to not print.
    </div>
    </cc:Box>
    
    <hr class="boxBreak" />
    
    <% if (CurrentUser.Company.SupportsBuildings || cboMode.SelectedValue == ScreenMode.ReadOnly || cboMode.SelectedValue == ScreenMode.Print) { %>
    <cc:Box id="boxBuildings" runat="server" Title="Buildings" TitleRightCorner="Scott" width="725" EncodeTitle="false">
    <div id="boxBuildingsContent"> 
        <% if (cboMode.SelectedValue == ScreenMode.EditInsured || cboMode.SelectedValue == ScreenMode.EditInsuredAndPolicy) {%>
            <asp:datagrid ID="grdBuildings" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="True">
                <headerstyle CssClass="header" />
                <itemstyle CssClass="item" />
                <alternatingitemstyle CssClass="altItem" />
                <footerstyle CssClass="footer" />
                <columns> 
                    <asp:TemplateColumn HeaderText="Building Number" SortExpression="Number ASC">
                        <ItemTemplate>
                            <a href="CreateSurveyBuilding.aspx<%= ARMSUtility.GetQueryStringForNav() %>&buildingid=<%# HtmlEncode(Container.DataItem, "ID")%>">
                                <%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(none)").PadLeft(3,'0')%>
                            </a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Year Built" SortExpression="YearBuilt ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "YearBuilt", Int32.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Sprinkler System" SortExpression="HasSprinklerSystem ASC">
                        <ItemTemplate>
                            <%# GetBoolLabel(HtmlEncode(Container.DataItem, "HasSprinklerSystem"))%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Sprinkler
                         System Credit" SortExpression="HasSprinklerSystemCredit ASC">
                    <ItemTemplate>
                        <%# GetBoolLabel(HtmlEncode(Container.DataItem, "HasSprinklerSystemCredit"))%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Public Protection" SortExpression="PublicProtection ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "PublicProtection", Int32.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Building Value" SortExpression="Value ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Value", "{0:C0}", Decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Building Contents" SortExpression="Contents ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Contents", "{0:C0}", Decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Stock Values" SortExpression="StockValues ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "StockValues", "{0:C0}", Decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Business Interruption" SortExpression="BusinessInterruption ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "BusinessInterruption", "{0:C0}", Decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Location Occupancy" SortExpression="LocationOccupancy ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "LocationOccupancy", string.Empty, "(none)") %>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Valuation Type" SortExpression="ValuationTypeCode ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "ValuationTypeCode", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Coinsurance" SortExpression="CoinsurancePercentage ASC" HeaderStyle-VerticalAlign="top">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "CoinsurancePercentage", "{0:P1}", Decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Change in Env. Control" SortExpression="ChangeInEnvironmentalControl ASC" HeaderStyle-VerticalAlign="top">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "ChangeInEnvironmentalControl", "{0:P1}", Decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Scientific Animals" SortExpression="ScientificAnimals ASC" HeaderStyle-VerticalAlign="top">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "ScientificAnimals", "{0:P1}", Decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Contamination" SortExpression="Contamination ASC" HeaderStyle-VerticalAlign="top">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Contamination", "{0:P1}", Decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Radio Active Contamination" SortExpression="RadioActiveContamination ASC" HeaderStyle-VerticalAlign="top">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "RadioActiveContamination", "{0:P1}", Decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Flood" SortExpression="Flood ASC" HeaderStyle-VerticalAlign="top">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Flood", "{0:P1}", Decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Earthquake" SortExpression="Earthquake ASC" HeaderStyle-VerticalAlign="top">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Earthquake", "{0:P1}", Decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            &nbsp;<asp:LinkButton ID="btnRemoveBuilding" Runat="server" CommandName="Remove" OnPreRender="btnRemoveBuilding_PreRender" CausesValidation="false">Remove</asp:LinkButton>&nbsp;
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </columns>
            </asp:datagrid>
            <a id="lnkAddBuilding" href="CreateSurveyBuilding.aspx<%= ARMSUtility.GetQueryStringForNav() %>">Add Building</a>
            
        <% } else { %>
            <asp:Repeater ID="repLocations" Runat="server">
                <HeaderTemplate>
                    <table>
                </HeaderTemplate>
                <FooterTemplate>
                   </table> 
                </FooterTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" nowrap>
                            <b><%# GetLocationDetails(Container.DataItem) %></b>&nbsp;&nbsp;&nbsp;&nbsp;<a href="<%# GetMappingURL(Container.DataItem) %>" target="_blank">(Map It)</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <% if (CurrentUser.Company.SupportsBuildings) { %>
                                <% if (1 > 0) { %>
                                <asp:Repeater ID="repBuildings" DataSource="<%# GetBuildingsForLocation(Container.DataItem) %>" Runat="server">
                                    <HeaderTemplate>
                                        <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0">
                                            <tr>
                                                <td>Building<br /><u>Number</u></td>
                                                <% if (CurrentUser.Company.ID != Company.BerkleyLifeSciences.ID) { %>
                                                <td style="PADDING-LEFT: 15px">Year<br /><u>Built</u></td>
                                                <td style="PADDING-LEFT: 15px" valign="bottom"><U><%=CurrentUser.Company.BuildingSprinklerSystemLabel %></U></td>
                                                <td style="PADDING-LEFT: 15px" valign="bottom"><U><%=CurrentUser.Company.BuildingSprinklerSystemLabel%> Credit</U></td>
                                                <td style="PADDING-LEFT: 15px">Public<br /><u>Protection</u></td>
                                                <% } %>

                                                <td style="PADDING-LEFT: 15px">Building<br /><u>Value</u></td>
                                                <td style="PADDING-LEFT: 15px">Building<br /><u>Contents</u></td>
                                                <td style="PADDING-LEFT: 15px">Stock<br /><u>Values</u></td>
                                                <td style="PADDING-LEFT: 15px">Business<br /><u>Interruption</u></td>
                                                <td style="PADDING-LEFT: 15px">Location<br /><u>Occupancy</u></td>
                                                <% if (CurrentUser.Company.SupportsBuildingValuation) {%>
                                                <td style="PADDING-LEFT: 15px">Valuation<br /><u>Type</u></td>
                                                <td style="PADDING-LEFT: 15px"><u>Coinsurance</u></td>
                                                <% } %>
                                                <% if (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID) { %>
                                                <td style="PADDING-LEFT: 15px"><u>Change in Env. Control</u></td>
                                                <td style="PADDING-LEFT: 15px"><u>Scientific Animals</u></td>
                                                <td style="PADDING-LEFT: 15px"><u>Contamination</u></td>
                                                <td style="PADDING-LEFT: 15px"><u>Radio Active Contamination</u></td>
                                                <td style="PADDING-LEFT: 15px"><u>Flood</u></td>
                                                <td style="PADDING-LEFT: 15px"><u>Earthquake</u></td>
                                                <% } %>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(none)")%></td>
                                            
                                            <% if (CurrentUser.Company.ID != Company.BerkleyLifeSciences.ID) { %>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "YearBuilt", Int32.MinValue, "(none)")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncode(Container.DataItem, "HtmlHasSprinklerSystemNone")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncode(Container.DataItem, "HtmlHasSprinklerSystemCreditNone")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "PublicProtection", Int32.MinValue, "(none)")%></td>
                                            <% } %>
                                            
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Value", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Contents", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "StockValues", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "BusinessInterruption", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "LocationOccupancy", string.Empty, "(none)")%></td>
                                            <% if (CurrentUser.Company.SupportsBuildingValuation) {%>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "ValuationTypeCode", string.Empty, "(none)")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "CoinsurancePercentage", "{0:P1}", decimal.MinValue, "(none)")%></td>
                                            <% } %>

                                            <% if (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID) { %>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "ChangeInEnvironmentalControl", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "ScientificAnimals", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Contamination", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "RadioActiveContamination", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Flood", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                            <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Earthquake", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                            <% } %>
                                        </tr>			
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <%} else {%>
                                <div style="PADDING-LEFT: 10px">
                                    (none)
                                </div>
                                <% } %>
                            <% } %>
                        </td>
                    </tr>			
                </ItemTemplate>
            </asp:Repeater>
        <% } %>
    </div>
    <div id="boxBuildingsPrintMessage" style="DISPLAY: none">
        This details of this section have been set to not print.
    </div>
    </cc:Box>
    <% } %>


    <% if (CurrentUser.Company.ImportRateModFactors) { %>
    <hr class="boxBreak" />
    <cc:Box id="boxRateModFactors" runat="server" title="Rate Modification Factors" width="725" EncodeTitle="false">
    <div id="boxRateModFactorsContent"> 
        <% if (repRateModFactors.Items.Count > 0) { %>
        <asp:Repeater ID="repRateModFactors" Runat="server">
            <HeaderTemplate>
                <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0">
                    <tr>
                        <td nowrap><u>Policy #</u></td>
                        <td style="PADDING-LEFT: 20px" nowrap><u>Mod</u></td>
                        <td style="PADDING-LEFT: 20px" nowrap><u>State</u></td>
                        <td style="PADDING-LEFT: 20px" nowrap><u>Description</u></td>
                        <td style="PADDING-LEFT: 20px" nowrap><u>Rate</u></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Policy.Number", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 20px" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Policy.Mod", Int32.MinValue, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 20px" nowrap><%# HtmlEncodeNullable(Container.DataItem, "State.Name", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "Type.Description", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "Rate", "{0:N3}", decimal.MinValue, "(none)")%></td>
                </tr>			
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <%} else {%>
        <div style="PADDING-LEFT: 10px">
            (none)
        </div>
        <% } %>
    </div>
    <div id="boxRateModFactorsPrintMessage" style="DISPLAY: none">
        This details of this section have been set to not print.
    </div>
    </cc:Box>
    <% } %>
    
    <hr class="boxBreak" />
    
    <div class="surveyDetails">
    <cc:Box id="boxObjectives" runat="server" title="Objectives" width="725">
        <% if( repObjectives.Items.Count > 0 ) { %>
        <asp:Repeater ID="repObjectives" Runat="server">
            <HeaderTemplate>
                <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0" cellpadding="0">
                    <tr>
                        <td nowrap><u>Number</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Objective</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Comments</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Target Date</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Status</u></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Text", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "CommentsText", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px" nowrap><%# HtmlEncodeNullable(Container.DataItem, "TargetDate", "{0:d}", DateTime.MinValue, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Status.Name", string.Empty, "(none)")%></td>
                </tr>			
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <% } else { %>
        <div style="PADDING-BOTTOM: 10px">
            (none)
        </div>
        <% } %>
    </cc:Box>

    <hr class="boxBreak" />
    
    <cc:Box id="boxNotes" runat="server" title="Comments" width="725">
        <% if( repNotes.Items.Count > 0 ) { %>
        <asp:Repeater ID="repNotes" Runat="server">
            <HeaderTemplate>
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="BORDER-BOTTOM: solid 1px black">
                        <%# GetUserName(DataBinder.Eval(Container.DataItem, "User")) %> - 
                        <%# HtmlEncode(Container.DataItem, "EntryDate", "{0:g}") %>
                        <span style="float:right;"><em><%# GetCommentPrivacy(Container.DataItem) %></em></span>
                    </td>
                </tr>
                <tr class="comment <%# GetCommentClass(Container.DataItem) %>">
                    <td>
                        <%# GetEncodedComment(Container.DataItem) %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <% } else { %>
        <div class="comment">
            (none)
        </div>
        <% } %>
    </cc:Box>
    <% } %>
    </div>


    <% if (CurrentUser.Company.YearsOfLossHistory > 0) { %>
    <hr class="boxBreak" />
    <cc:Box id="boxClaim" runat="server" title="Claims" width="725" EncodeTitle="false">
    <div id="boxClaimsContent"> 
        <% if (grdClaims.Items.Count > 0 || repClaims.Items.Count > 0) { %>
            <% if (!CurrentUser.Company.DisplayClaimsByDefault) { %>
            <asp:CheckBox ID="chkDisplayClaims" runat="server" CssClass="chk" Text="Display Claims" AutoPostBack="true" /><br /><br />
            <% } %>

            <% if (_hideExportClaimsToExcel != "true") { %>
            <asp:LinkButton ID="btnExport" runat="server">Export Claims to Excel</asp:LinkButton>
            <% } %>
        
            <% if (_disclaimerMessage.Length > 0){ %>
               <b> Disclaimer: <%= _disclaimerMessage %></b>
            <% } %>

        <% } %>
        
        <% if (cboMode.SelectedValue == ScreenMode.EditInsured || cboMode.SelectedValue == ScreenMode.EditInsuredAndPolicy) {%>
            <asp:datagrid ID="grdClaims" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="10" AllowSorting="True">
                <headerstyle CssClass="header" />
                <itemstyle CssClass="item" />
                <alternatingitemstyle CssClass="altItem" />
                <footerstyle CssClass="footer" />
                <pagerstyle CssClass="pager" Mode="NumericPages" />
                <columns>
                    <asp:TemplateColumn HeaderText="Policy #" SortExpression="PolicyNumber ASC">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "PolicyNumber", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Sym" SortExpression="PolicySymbol ASC">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "PolicySymbol", string.Empty, "")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Policy Exp" SortExpression="PolicyExpireDate ASC">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "PolicyExpireDate", "{0:d}", DateTime.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Loss Date" SortExpression="LossDate ASC">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "LossDate", "{0:d}", DateTime.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Amount" SortExpression="Amount ASC">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Amount", "{0:C}", decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Claimant" SortExpression="Claimant ASC">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Claimant", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" SortExpression="Status ASC">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Status", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Loss Type" SortExpression="LossType ASC">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "LossType", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Claim Comment" SortExpression="Comment ASC">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Comment", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </columns>
            </asp:datagrid>
        <%} else {%>
            <% if (repClaims.Items.Count > 0) { %>
            <asp:Repeater ID="repClaims" Runat="server">
                <HeaderTemplate>
                    <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0">
                        <tr>
                            <td nowrap><u>Policy #</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Sym</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Policy Exp</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Loss Date</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Amount</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Claimant</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Status</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Loss Type</u></td>
                            <td style="PADDING-LEFT: 15px" nowrap><u>Claim Comment</u></td>

                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" nowrap ><%# HtmlEncodeNullable(Container.DataItem, "PolicyNumber", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"  ><%# HtmlEncodeNullable(Container.DataItem, "PolicySymbol", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px" nowrap><%# HtmlEncodeNullable(Container.DataItem, "PolicyExpireDate", "{0:d}", DateTime.MinValue, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px" nowrap><%# HtmlEncodeNullable(Container.DataItem, "LossDate", "{0:d}", DateTime.MinValue, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Amount", "{0:C}", decimal.MinValue, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Claimant", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Status", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "LossType", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Comment", string.Empty, "(none)")%></td>
                    </tr>			
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <%} else {%>
            <div style="PADDING-LEFT: 10px">
                <%if (_eSurvey.PrimaryPolicy != null && !SurveyTypeType.IsNotWrittenBusiness(_eSurvey.Type.Type) && CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID && String.IsNullOrEmpty(_eSurvey.WorkflowSubmissionNumber)) {%>
                    (none)
                <%} else {%>
                    <b><%= _claimsProspectMsg %></b>
                <% } %>
            </div>
            <% } %>
            
        <% } %>
    </div>
    <div id="boxClaimsPrintMessage" style="DISPLAY: none">
        This details of this section have been set to not print.
    </div>
     
    </cc:Box>
    <% } %>

    <uc:HintFooter ID="ucHintFooter" runat="server" VerticalOffset="156" />
    <uc:Footer ID="ucFooter" runat="server" />

    <div style="display: none;"><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" alt="" /></div>

    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.blockUI.defaults.css = {};
            $.blockUI.defaults.overlayCSS.opacity = .2;

            $('#btnRefreshData', this).click(function () {
                if (Page_IsValid) {

                    var response = confirm("You are about to refresh this survey's data from the policy system.  Please be patient, this process can take a short while.");
                    if (response == true) {
                        $.blockUI({ message: '<h3><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" />&nbsp;&nbsp;&nbsp;Refreshing, one moment please...</h3>' });
                    }
                    else {
                        $.unblockUI();
                        AjaxMethods.SetRefreshCancelDate();
                        return false;
                    }

                }
            });
        });
    </script>
    <script type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script type="text/javascript">
    function Toggle(id)
    {
        ToggleControlDisplay(id);
    }

    function GetAgency(agencyNum, companyNum)
    {
        var arrFields = AjaxMethods.GetAgency(agencyNum, companyNum).value;

        if(arrFields.length > 1) {
            document.getElementById("txtAgencyName_txt").value = arrFields[0];
            document.getElementById("txtAgencyStreetLine1_txt").value = arrFields[1];
            document.getElementById("txtAgencyStreetLine2_txt").value = arrFields[2];
            document.getElementById("txtAgencyCity_txt").value = arrFields[3];
            document.getElementById("cboAgencyState_cbo").value = arrFields[4];
            document.getElementById("txtAgencyZip_txt").value = arrFields[5];
            document.getElementById("txtAgencyPhone_txt").value = arrFields[6];
            document.getElementById("txtAgencyFax_txt").value = arrFields[7];
        }
        else {
            alert(arrFields[0]);
            document.getElementById("txtAgencyNumber_txt").value = "";
        }
    }

    var imgA = "<%= ARMSUtility.AppPath %>/Images/plus.png";
    var imgB = "<%= ARMSUtility.AppPath %>/Images/minus.png";

        function ToggleExpandCollaspe(obj, id) {

            ToggleControlDisplay(id);

            obj.src = obj.src.match(imgA) ?
                      obj.src.replace(imgA, imgB) :
                      obj.src.replace(imgB, imgA);
        }

    </script>
    
    </form>
    </div>
</body>
</html>
