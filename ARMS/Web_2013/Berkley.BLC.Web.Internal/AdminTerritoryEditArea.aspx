<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminTerritoryEditArea.aspx.cs" Inherits="AdminTerritoryEditAreaAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
    
    <style>
    TABLE.list TD { PADDING-RIGHT: 20px; }
    </style>

    <script language="javascript">
    function defaultButtonTrap(e, btnID)
    {
	    if( !e ) var e = window.event;
	    if( e.keyCode == 13 )
	    {
		    var btn = document.getElementById(btnID);
		    if( btn != null && btn.click ) btn.click();
		    return false;
	    }
    }
    </script>    
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span>
    <span style="MARGIN-LEFT: 40px">
	    <a class="nav" href="AdminTerritoryEdit.aspx?territoryid=<%= _terra.ID %>"><< Back to Edit Territory</a>
    </span><br>
    for <asp:Label ID="lblTerritory" Runat="server" /><br>
    <br>

    <table cellspacing="0" cellpadding="0" border="0" width="500">
    <tr>
	    <td>

        <cc:Box id="boxProfitCenter" runat="server" title="Profit Center" width="100%">
	        <div id="divListPC"></div>

	        <input type="text" id="txtValuePC" name="txtName" class="txt" size="30" maxlength="30" autocomplete="on" onkeypress="return defaultButtonTrap(event, 'btnAddPC')" />
	        <input type="button" id="btnAddPC" value="Add" class="btn" onclick="javascript:btnAdd_OnClick('PC')" />
	        <a id="lnkTogglePC" style="margin-left:30px" href="javascript:ToggleRemoveMode('PC', true)">Enable Item Removal</a>
	        <input type="hidden" id="hdnListPC" runat="server" name="hdnListPC" value="" />
        </cc:Box>

	    </td>
    </tr>
    <tr>
	    <td>

    <cc:Box id="boxStates" runat="server" title="States" width="100%">
	    <div id="divListST"></div>

	    <input type="text" id="txtValueST" name="txtName" class="txt" size="10" maxlength="2" autocomplete="on" onkeypress="return defaultButtonTrap(event, 'btnAddST')" />
	    <input type="button" id="btnAddST" value="Add" class="btn" onclick="javascript:btnAdd_OnClick('ST')" />
	    <a id="lnkToggleST" style="margin-left:30px" href="javascript:ToggleRemoveMode('ST', true)">Enable Item Removal</a>
	    <input type="hidden" id="hdnListST" runat="server" name="hdnListST" value="" />
    </cc:Box>

	    </td>
    </tr>
    <tr>
	    <td>
    	
    <cc:Box id="boxZ3" runat="server" title="3-Digit Zip Codes" width="100%">
	    <div id="divListZ3"></div>

	    <input type="text" id="txtValueZ3" name="txtName" class="txt" size="10" maxlength="3" autocomplete="on" onkeypress="return defaultButtonTrap(event, 'btnAddZ3')" />
	    <input type="button" id="btnAddZ3" value="Add" class="btn" onclick="javascript:btnAdd_OnClick('Z3')" />
	    <a id="lnkToggleZ3" style="margin-left:30px" href="javascript:ToggleRemoveMode('Z3', true)">Enable Item Removal</a>
	    <input type="hidden" id="hdnListZ3" runat="server" name="hdnListZ3" value="" />
    </cc:Box>

	    </td>
    </tr>
    <tr>
	    <td>
    	
    <cc:Box id="boxZ5" runat="server" title="5-Digit Zip Codes" width="100%">
	    <div id="divListZ5"></div>

	    <input type="text" id="txtValueZ5" name="txtName" class="txt" size="10" maxlength="5" autocomplete="on" onkeypress="return defaultButtonTrap(event, 'btnAddZ5')" />
	    <input type="button" id="btnAddZ5" value="Add" class="btn" onclick="javascript:btnAdd_OnClick('Z5')" />
	    <a id="lnkToggleZ5" style="margin-left:30px" href="javascript:ToggleRemoveMode('Z5', true)">Enable Item Removal</a>
	    <input type="hidden" id="hdnListZ5" runat="server" name="hdnListZ5" value="" />
    </cc:Box>

	    </td>
    </tr>
    <tr>
	    <td>

    <div id="divCopyLink">
	    <a href="javascript:ShowCopy(true)">Copy areas from another territory</a><br>
	    <br>
    </div>
    <div id="divCopyBox" style="display:none">
	    <cc:Box id="boxCopy" runat="server" title="Copy Territory" width="100%">
		    Territory: <asp:DropDownList ID="cboTerritory" Runat="server" CssClass="cbo" /><br>
		    <div style="margin:5px 0px 10px 20px">
			    <asp:CheckBox ID="chkClearAllFirst" Runat="server" CssClass="chk" Text="Clear all areas in this territory before copying." /><br>
			    <asp:CheckBox ID="chkSkipConflicts" Runat="server" CssClass="chk" Text="Skip any conflicting areas rather than abort the copy." /><br>
		    </div>
		    <asp:Button ID="btnCopy" Runat="server" CssClass="btn" Text="Copy Areas" />&nbsp;
		    <input type="button" id="btnCopyCancel" value="Cancel" class="btn" onclick="javascript:ShowCopy(false)" />
	    </cc:Box>
	    <br>
	    <br>
    </div>

	    </td>
    </tr>
    </table>

    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Save Changes" />&nbsp;
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" />

    <uc:Footer ID="ucFooter" runat="server" />

    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
    function ShowCopy(show)
    {
	    var divCopyLink = document.getElementById('divCopyLink');
	    var divCopyBox = document.getElementById('divCopyBox');
    	
	    ChangeControlDisplay('divCopyLink', !show);
	    ChangeControlDisplay('divCopyBox', show);
    }
    </script>
    <script language="javascript">
    var _territoryID = '<%= _terra.ID %>';

    var _removalEnabled = new Array();
    _removalEnabled['PC'] = false;
    _removalEnabled['ST'] = false;
    _removalEnabled['Z3'] = false;
    _removalEnabled['Z5'] = false;

    var _currentRowCount = new Array();
    _currentRowCount['PC'] = 0;
    _currentRowCount['ST'] = 0;
    _currentRowCount['Z3'] = 0;
    _currentRowCount['Z5'] = 0;

    function ToggleTest(lnk)
    {
	    alert(lnk.id);
	    _removalEnabled = !_removalEnabled;
	    lnk.innerHTML = (_removalEnabled) ? 'Disable Item Removal' : 'Enable Item Removal';
    }

    function Page_Init()
    {
	    var list;
	    
	    list = GetPostBackArray("hdnListPC");
	    Refresh_List('PC', list);
    	
	    list = GetPostBackArray("hdnListST");
	    Refresh_List('ST', list);
    	
	    list = GetPostBackArray("hdnListZ3");
	    Refresh_List('Z3', list);
    	
	    list = GetPostBackArray("hdnListZ5");
	    Refresh_List('Z5', list);
    }

    function ToggleRemoveMode(box, refresh)
    {
	    var lnk = document.getElementById("lnkToggle" + box);
	    var flag = !_removalEnabled[box]
	    _removalEnabled[box] = flag;
	    lnk.innerHTML = (flag) ? 'Disable Item Removal' : 'Enable Item Removal';

	    if( refresh )
	    {
		    var list = GetPostBackArray("hdnList" + box);
		    Refresh_List(box, list);
	    }
    }

    function btnAdd_OnClick(box)
    {
	    var txt = document.getElementById("txtValue" + box);
	    var value = txt.value.toUpperCase();
	    if( value.length == 0 )
	    {
		    return;
	    }
	    
	    // get the list
	    var list = GetPostBackArray("hdnList" + box);

	    // see if this value is already in the list
	    if( BinarySearch(list, value) >= 0 )
	    {
		    alert("Value '" + value + "' is already in this list.");
		    return;
	    }
	    
	    // see if the server has a problem with adding this value
	    var response = AjaxMethods.CheckValue(_territoryID, box, value);
	    if( response == null || response.error != null ) // error calling server
	    {
		    alert("A problem occurred while tying to communicate with the server.\nPlease try again in a few moments.");
		    return;
	    }
	    if (response.value) // error message returned
	    {
		    alert(response.value);
		    return;
	    }
    	
	    // disable remove mode if currently enabled
	    if( _removalEnabled[box] )
	    {
		    ToggleRemoveMode(box, false);
	    }

	    // add the value to our internal array
	    list[list.length] = value;
	    list.sort();
	    SetPostBackArray("hdnList" + box, list);
    	
	    // refresh the list and clear the textbox
	    Refresh_List(box, list);
	    txt.value = "";
	    txt.focus();
    }

    function Remove(box, index)
    {
	    if( !_removalEnabled[box] )
	    {
		    return;
	    }
    	
	    // remove the item at the specified index
	    var list = GetPostBackArray("hdnList" + box);
	    RemoveAtIndex(list, index);
	    SetPostBackArray("hdnList" + box, list);

	    // now refresh the list
	    Refresh_List(box, list);
    }

    function Refresh_List(box, values)
    {
	    var maxCols = 8;
	    var minRows = 5;
    	
	    // determine number of rows and columns for table
	    var rows;
	    if( _removalEnabled[box] && _currentRowCount[box] > 0 )
	    {
		    rows = _currentRowCount[box];
	    }
	    else
	    {
		    rows = Math.ceil(values.length / maxCols);
	    }	 
	    if (rows < minRows)
	    {
		    rows = minRows;
	    }
	    var cols = Math.ceil(values.length / rows);
    	
	    // store our new row count for use next time
	    _currentRowCount[box] = rows;

	    // build the html table to render (using an array for better string building perf)
	    var showLinks = _removalEnabled[box];

	    var sb = new StringBuilder();
	    //sb.append("<br>" + values.length + " items => " + cols + " cols x " + rows + " rows<br><br>");
	    sb.append("<table class='list'><tr>");
	    if( values.length > 0 )
	    {
		    for (var c = 0; c < cols; c++)
		    {
			    sb.append("<td valign='top'><ul>");
			    for (var r = 0; r < rows; r++)
			    {
				    var index = c * rows + r;
				    if (index < values.length)
				    {
					    sb.append("<li>");
					    if (!showLinks)
					    {
						    sb.append(values[index]);
					    }
					    else // remove links
					    {
						    sb.append("<a href=\"javascript:Remove('" + box + "'," + index + ")\">");
						    sb.append(values[index]);
						    sb.append("</a>");
					    }
					    sb.append("</li>");
				    }
			    }
			    sb.append("</ul></td>");
		    }
	    }
	    else // no items to show
	    {
		    sb.append("<td><ul>");
		    sb.append("<li>(none)</li>");
		    sb.append("</ul></td>");
	    }
	    sb.append("</tr></table>");

	    var divList = document.getElementById("divList" + box);
	    divList.innerHTML = sb.toString();
    }

    function GetPostBackArray(id) // returns an array
    {
	    var hdn = document.getElementById(id);
	    //alert("Get " + id + " = " + hdn.value);
	    if (hdn.value.length > 0)
	    {
		    return hdn.value.split(',');
	    }
	    else
	    {
		    return new Array();
	    }
    }

    function SetPostBackArray(id, a)
    {
	    var hdn = document.getElementById(id);
	    hdn.value = a.join(',');
	    //alert("Set " + id + " = " + hdn.value);
    }

    function RemoveAtIndex(a, index)
    {
	    a.splice(index, 1);
    }

    function BinarySearch(a, value) // returns the index or -1
    {
	    var left = 0;
	    var right = a.length - 1;

	    while (left <= right)
	    {
		    var mid = Math.floor((left + right) / 2);
		    if (a[mid] == value)
		    {
			    return mid;
		    }
		    if (value < a[mid])
		    {
			    right = mid - 1;
		    }
		    else
		    {
			    left = mid + 1;
		    }
	    }

	    // not found
	    return -1;
    }
    </script>

    <script language="javascript">
    Page_Init();
    </script>

    </form>
</body>
</html>
