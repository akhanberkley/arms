<%@ Page Language="C#" AutoEventWireup="false" CodeFile="Locations.aspx.cs" Inherits="LocationsAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><a class="nav" href="Survey.aspx<%= ARMSUtility.GetQueryStringForNav("locationid") %>"><%if (_eSurvey.ServiceType == Berkley.BLC.Entities.ServiceType.ServiceVisit) {%><< Back to Service Visit<%} else {%><< Back to Survey<%}%></a><br><br>
    
    <% if (grdAttachedLocations.Items.Count > 1) {%>
    <asp:CheckBox ID="chkEnableSeperation" Runat="server" CssClass="chk" AutoPostBack="True" Text="Separate locations into new survey(s)." />
    <% } %>
    
    <% if (!chkEnableSeperation.Checked) {%>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    
    <cc:Box id="boxAttachedLocations" runat="server" Title="Surveyed Locations" width="600">
    <asp:datagrid ID="grdAttachedLocations" Runat="server" Width="100%" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
	        <asp:TemplateColumn HeaderText="Location&nbsp;#" SortExpression="Number ASC">
	            <ItemTemplate>
	                <a href="Insured.aspx<%= ARMSUtility.GetQueryStringForNav("locationid") %>&locationid=<%# HtmlEncode(Container.DataItem, "ID")%>&locations=1">
		                <%# HtmlEncodeNullable(Container.DataItem, "Number", Int32.MinValue, "(none)").PadLeft(3,'0')%>
		            </a>
	            </ItemTemplate>
	        </asp:TemplateColumn>
	        <asp:TemplateColumn HeaderText="Address Line 1" SortExpression="StreetLine1 ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "StreetLine1", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Address Line 2" SortExpression="StreetLine2 ASC">
                <ItemTemplate>
                    <%# HtmlEncodeNullable(Container.DataItem, "StreetLine2", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
	        <asp:TemplateColumn HeaderText="City" SortExpression="City ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "City", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
	        <asp:TemplateColumn HeaderText="State" SortExpression="StateCode ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "StateCode", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Zip" SortExpression="ZipCode ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "ZipCode", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Policy" SortExpression="PolicySymbol ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "PolicySymbol", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Buildings">
	                <ItemTemplate>
		                <%# GetBuildingCount(Container.DataItem) %>
	                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Primary Location">
			        <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
			        <ItemTemplate>
                            <%--<asp:LinkButton ID="lnkMakePrimary" runat="server" CommandName="MAKEPRIMARY" CausesValidation="false">Make Primary</asp:LinkButton>--%>
                            <cc:RadioButtonForRepeater ID="rdoPrimary" CommandName="MAKEPRIMARY" ClientIDMode="Static" CssClass="rdo" runat="server" CausesValidation="false" AutoPostBack="true" />
				    </ItemTemplate>
		        </asp:TemplateColumn>
            <asp:TemplateColumn>
	            <ItemTemplate>
		            &nbsp;<asp:LinkButton ID="btnRemoveLocation" Runat="server" CommandName="Remove">Remove</asp:LinkButton>&nbsp;
	            </ItemTemplate>
            </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>
    
        <asp:LinkButton ID="lnkAddLocation" runat="server">Create New Location</asp:LinkButton>
    </cc:Box>

    <cc:Box id="boxNonAttachedLocations" runat="server" Title="Other Existing Insured Locations" width="600">
    <asp:datagrid ID="grdNonAttachedLocations" Runat="server" Width="100%" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
	        <asp:TemplateColumn HeaderText="Location&nbsp;#" SortExpression="Number ASC">
	            <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "Number", Int32.MinValue, "(none)").PadLeft(3,'0')%>
	            </ItemTemplate>
	        </asp:TemplateColumn>
	        <asp:TemplateColumn HeaderText="Address Line 1" SortExpression="StreetLine1 ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "StreetLine1", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Address Line 2" SortExpression="StreetLine2 ASC">
                <ItemTemplate>
                    <%# HtmlEncodeNullable(Container.DataItem, "StreetLine2", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
	        <asp:TemplateColumn HeaderText="City" SortExpression="City ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "City", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
	        <asp:TemplateColumn HeaderText="State" SortExpression="StateCode ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "StateCode", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Zip" SortExpression="ZipCode ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "ZipCode", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Policy" SortExpression="PolicySymbol ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "PolicySymbol", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Buildings">
	                <ItemTemplate>
		                <%# GetBuildingCount(Container.DataItem) %>
	                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn>
	            <ItemTemplate>
		            &nbsp;<asp:LinkButton ID="btnRemoveLocation" Runat="server" CommandName="Add">Add</asp:LinkButton>&nbsp;
	            </ItemTemplate>
            </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>
    </cc:Box>

    <%} else {%>
    <table>
        <tr>
            <td>
                <asp:datagrid ID="grdLocationsForSeperation" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
	                <headerstyle CssClass="header" />
	                <itemstyle CssClass="item" />
	                <alternatingitemstyle CssClass="altItem" />
	                <footerstyle CssClass="footer" />
	                <pagerstyle CssClass="pager" Mode="NumericPages" />
	                <columns>
	                    <asp:TemplateColumn HeaderText="Location&nbsp;#" SortExpression="Number ASC">
	                        <ItemTemplate>
		                        <%# HtmlEncodeNullable(Container.DataItem, "Number", Int32.MinValue, "(none)").PadLeft(3,'0')%>
	                        </ItemTemplate>
	                    </asp:TemplateColumn>
	                    <asp:TemplateColumn HeaderText="Address Line 1" SortExpression="StreetLine1 ASC">
                            <ItemTemplate>
	                            <%# HtmlEncodeNullable(Container.DataItem, "StreetLine1", string.Empty, "(none)")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Address Line 2" SortExpression="StreetLine2 ASC">
                            <ItemTemplate>
	                            <%# HtmlEncodeNullable(Container.DataItem, "StreetLine2", string.Empty, "(none)")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
	                    <asp:TemplateColumn HeaderText="City" SortExpression="City ASC">
                            <ItemTemplate>
	                            <%# HtmlEncodeNullable(Container.DataItem, "City", string.Empty, "(none)")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
	                    <asp:TemplateColumn HeaderText="State" SortExpression="StateCode ASC">
                            <ItemTemplate>
	                            <%# HtmlEncodeNullable(Container.DataItem, "StateCode", string.Empty, "(none)")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Zip" SortExpression="ZipCode ASC">
                            <ItemTemplate>
	                            <%# HtmlEncodeNullable(Container.DataItem, "ZipCode", string.Empty, "(none)")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Buildings">
	                            <ItemTemplate>
		                            <%# GetBuildingCount(Container.DataItem) %>
	                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Survey">
	                            <ItemTemplate>
		                            <asp:DropDownList ID="cboSurveyNumber" runat="server" CssClass="cbo"></asp:DropDownList>
	                            </ItemTemplate>
                        </asp:TemplateColumn>
	                </columns>
                </asp:datagrid>
            </td>
        </tr>
        <tr>
            <td align="right"><asp:Button ID="btnSeperate" Runat="server" CssClass="btn" Text="Separate Locations" /></td>
        </tr>
    </table>
    
    <%} %>
 
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />

    <div style="display: none;"><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" alt="" /></div>
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.blockUI.defaults.css = {};
            $.blockUI.defaults.overlayCSS.opacity = .2;

            $('#rdoPrimary', this).click(function () {
                    $.blockUI({ message: '<h3><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" />&nbsp;&nbsp;&nbsp;Processing, one moment please...</h3>' });
            });
        });
    </script>
    </form>
</body>
</html>
