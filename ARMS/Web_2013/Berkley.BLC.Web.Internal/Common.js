function AddToEvent(elem, eventName, code)
{
	var ev = elem[eventName];
	if (typeof(ev) == "function")
	{
		ev = ev.toString();
		var firstBrace = ev.indexOf("{");
		var lastBrace = ev.lastIndexOf("}");
		ev = ev.substring(firstBrace + 1, lastBrace);
	}
	else
	{
		ev = "";
	}
	
	if (code.substring(code.length - 1) != ";")
	{
		code += ";"
	}
	
	elem[eventName] = new Function(code + " " + ev);
}

function ToggleControlVisibility(elemID)
{
    var elem = document.getElementById(elemID);
    if( !elem ) return;
    var visible = (elem.style.visibility != 'hidden');
	ChangeControlVisibility(elemID, !visible);
}

function ChangeControlVisibility(elemID, visible)
{
    var elem = document.getElementById(elemID);
    if( !elem ) return;
	elem.style.visibility = (visible ? 'visible' : 'hidden');
	ToggleValidators(elem, visible);
}

function ToggleControlDisplay(elemID)
{
    var elem = document.getElementById(elemID);
    if( !elem ) return;
    var show = (elem.style.display != 'none');
	ChangeControlDisplay(elemID, !show);
}

function ChangeControlDisplay(elemID, show)
{
    var elem = document.getElementById(elemID);
    if( !elem ) return;
	elem.style.display = (show ? '' : 'none');
	ToggleValidators(elem, show);
}

function ToggleValidators(elem, enabled)
{
	elem.disabled = !enabled;
	var vals = elem.Validators;
	if( !vals ) return;
    for( var i = vals.length - 1; i >= 0; i-- )
    {
		ValidatorEnable(vals[i], enabled);
    }
}

function FindListItemByValue(lst, value)
{
	var count = lst.options.length;
	for( var i = 0; i < count; i++ )
	{
		if( lst.options[i].value == value )
		{
			return i;
		}
	}
	return -1;
}

function FindListItemByText(lst, value)
{
	var count = lst.options.length;
	for( var i = 0; i < count; i++ )
	{
		if( lst.options[i].text == value )
		{
			return i;
		}
	}
	return -1;
}


// Initializes a new instance of the StringBuilder class
// and appends the given value if supplied.
function StringBuilder(value)
{
    this.strings = new Array("");
    this.append(value);
}

// Appends the given value to the end of this instance.
StringBuilder.prototype.append = function(value)
{
    if (value) this.strings.push(value);
}

// Clears the string buffer
StringBuilder.prototype.clear = function()
{
    this.strings.length = 1;
}

// Converts this instance to a String.
StringBuilder.prototype.toString = function()
{
    return this.strings.join("");
}