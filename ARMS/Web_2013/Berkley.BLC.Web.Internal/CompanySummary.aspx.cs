using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;

public partial class CompanySummaryAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(CompanySummaryAspx_Load);
    }
    #endregion

    private Hashtable _userCountsByTeam; // each item is a hashtable of users with counts
    private Hashtable _unassignedCounts;
    private Hashtable _vendors; // needed to seperate out totals for company and fee auditor teams

    protected int _returnedCount; // used during HTML rendering
    protected int _holdCount; // used during HTML rendering
    protected int _unassignedReviewCount; // used during HTML rendering
    protected bool _showDataEntryGrid; // used during HTML rendering
    protected string _itemClass; // used during HTML rendering
    protected string _surveyDaysCountColumnHeader;// used to disaply grid column header at runtime. 
    protected string _showSurveyDaysCountColumnHeader; //used to show/hide grid SurveyDaysCount at runtime.  

    void CompanySummaryAspx_Load(object sender, EventArgs e)
    {
        // bind the import summary repeater
        //ImportCounts[] importList = GetImportHistory();
        //repImport.DataSource = importList;
        //repImport.DataBind();

        // load the team summary hashtables
        LoadTeamSummaryCounts();

        // get user sets assinged to each team
        // note: we have to get users with disabled accounts so we can filter them as needed
        User[] companyConsultants = this.CurrentUser.Company.GetConsultants("IsFeeCompany = false && IsUnderwriter = false");
        User[] vendors = this.CurrentUser.Company.GetConsultants("IsFeeCompany = true && IsUnderwriter = false");
        User[] reviews = this.CurrentUser.Company.GetReviewStaff(null, null);

        // build a hashtable of all vendors, we'll need this to get accurate team totals
        _vendors = new Hashtable(vendors.Length);
        foreach (User user in vendors)
        {
            _vendors.Add(user.ID, user);
        }

        // bind all the repeaters with users
        repCCs.DataSource = GetUsersToShow(companyConsultants, SurveyStatusType.Survey);
        repCCs.DataBind();

        repVs.DataSource = GetUsersToShow(vendors, SurveyStatusType.Survey);
        repVs.DataBind();

        repAFs.DataSource = GetUsersToShow(reviews, SurveyStatusType.Review);
        repAFs.DataBind();

        // check for notification
        Notification notification = Notification.GetOne("EndDate >= ? && StartDate <= ?", DateTime.Now, DateTime.Now);
        if (notification != null)
        {
            boxNotification.Visible = true;
            divNotificationMessage.InnerHtml = notification.Description;
            lnkEnhancementList.HRef = notification.Link;
        }

        Company company = Company.GetOne("ID = ?", this.CurrentUser.CompanyID);
        int surveyDaysCount = company.SurveyDaysCount;
        _surveyDaysCountColumnHeader = surveyDaysCount +" Day Completed";

        _showSurveyDaysCountColumnHeader = Berkley.BLC.Business.Utility.GetCompanyParameter("ShowSurveyDaysCountHeader", CurrentUser.Company).ToLower(); 
    }

    protected int GetCountForUser(string team, object dataItem, string field)
    {
        User user = (dataItem as User);

        // get the user's counts for the specified team
        Hashtable users = (Hashtable)_userCountsByTeam[team];
        UserCounts counts = (UserCounts)users[user.ID];

        // return the field requested
        return (counts != null) ? counts[field] : 0;
    }

    protected int GetTotalForTeam(string team, string field)
    {
        return GetTotalForTeam(team, field, false);
    }

    protected int GetTotalForTeam(string team, string field, bool countVendors)
    {
        Hashtable userCounts = (Hashtable)_userCountsByTeam[team];

        // scan the users and tally the counts for the specified field
        int result = 0;
        foreach (UserCounts counts in userCounts.Values)
        {
            bool isVendor = _vendors.ContainsKey(counts.UserID);
            if (isVendor == countVendors)
            {
                result += counts[field];
            }
        }

        return result;
    }

    protected int GetUnassignedCount(SurveyStatusType team)
    {
        object count = _unassignedCounts[team.Code];
        return (count != null) ? (int)count : 0;
    }


    private void LoadTeamSummaryCounts()
    {
        // execute our helper sproc to get a multi-table dataset with the info we need
        string command = string.Format("EXEC Company_GetUserSummaryValues @CompanyID = '{0}'", this.CurrentUser.CompanyID);
        DataSet ds = DB.Engine.GetDataSet(command);

        _userCountsByTeam = new Hashtable(5);
        _userCountsByTeam.Add(SurveyStatusType.Survey.Code, new Hashtable(20));
        _userCountsByTeam.Add(SurveyStatusType.Review.Code, new Hashtable(20));
        _userCountsByTeam.Add(SurveyStatus.AwaitingAcceptance.Code, new Hashtable(20));
        _userCountsByTeam.Add("C", new Hashtable(20));

        // process the first table, which has all the counts by team, user, and field
        foreach (DataRow row in ds.Tables[0].Rows)
        {
            string teamCode = (string)row["Team"];
            Guid userID = (Guid)row["UserID"];
            string field = (string)row["Field"];
            int count = (int)row["Count"];


            // get the users for the specified team
            Hashtable users = (Hashtable)_userCountsByTeam[teamCode];

            // get or create the summary object for this user
            UserCounts counts = (UserCounts)users[userID];
            if (counts == null)
            {
                counts = new UserCounts(userID);
                users.Add(userID, counts);
            }

            // add this count to the right accumulator
            counts[field] += count;
        }

        // process the second table, which holds the unassigned count for each team
        _unassignedCounts = new Hashtable(3);
        foreach (DataRow row in ds.Tables[1].Rows)
        {
            string teamCode = (string)row["Team"];
            int count = (int)row["Count"];

            _unassignedCounts.Add(teamCode, count);
        }

        // the third table has the returned count
        DataRow returnedRow = (ds.Tables[2].Rows.Count > 0) ? ds.Tables[2].Rows[0] : null;
        _returnedCount = (returnedRow != null) ? (int)returnedRow["Count"] : 0;

        // the forth table has the hold count
        DataRow holdRow = (ds.Tables[3].Rows.Count > 0) ? ds.Tables[3].Rows[0] : null;
        _holdCount = (holdRow != null) ? (int)holdRow["Count"] : 0;

        // the sixth table has the unassigned review count
        DataRow unassignedReviewRow = (ds.Tables[4].Rows.Count > 0) ? ds.Tables[4].Rows[0] : null;
        _unassignedReviewCount = (unassignedReviewRow != null) ? (int)unassignedReviewRow["Count"] : 0;
    }

    private User[] GetUsersToShow(User[] users, SurveyStatusType team)
    {
        Hashtable teamUserCounts = (Hashtable)_userCountsByTeam[team.Code];

        // remove all disabled users not assigned any audits
        ArrayList results = new ArrayList(users.Length);
        foreach (User user in users)
        {
            if (user.AccountDisabled)
            {
                if (teamUserCounts.ContainsKey(user.ID)) // user has a survey
                {
                    results.Add(user);
                }
            }
            else
            {
                results.Add(user);
            }
        }

        return (User[])results.ToArray(typeof(User));
    }

    private ImportCounts[] GetImportHistory()
    {
        DateTime startDate = DateTime.Today.AddDays(-14);
        DateTime endDate = DateTime.Today.AddDays(1); // we want everything up to midnight today

        // execute our helper sproc to get a data table we can process
        string command = string.Format("EXEC Company_GetImportSummary @CompanyID = '{0}', @StartDate = '{1:d}', @EndDate = '{2:d}'",
            this.CurrentUser.CompanyID, startDate, endDate);
        DataSet ds = DB.Engine.GetDataSet(command);
        DataTable dt = ds.Tables[0];

        // fill a hashtable with all the dates to be shown (skip weekend days)
        Hashtable items = new Hashtable(dt.Rows.Count);
        for (DateTime date = startDate; date < endDate; date = date.AddDays(1))
        {
            DayOfWeek dow = date.DayOfWeek;
            if (dow != DayOfWeek.Saturday && dow != DayOfWeek.Sunday)
            {
                items[date] = new ImportCounts(date);
            }
        }

        // process each record returned from the sproc above, loading a hashtable with ImportCounts objects
        foreach (DataRow row in dt.Rows)
        {
            DateTime date = (DateTime)row["Date"];
            string field = (string)row["Field"];
            int count = (int)row["Count"];

            // create a new entry if we don't already have it (e.g., weekend days with activity)
            ImportCounts counts = (ImportCounts)items[date];
            if (counts == null)
            {
                counts = new ImportCounts(date);
                items[date] = counts;
            }

            // add this count to the right accumulator
            counts[field] += count;
        }

        // copy the items into an array and sort them
        ImportCounts[] list = new ImportCounts[items.Count];
        items.Values.CopyTo(list, 0);
        Array.Sort(list);

        return list;
    }


    private class UserCounts
    {
        private Guid _userID;
        private int _total = 0;
        private int _awaitingAcceptance = 0;
        private int _correction = 0;
        private int _surveyNearDue = 0;
        private int _reviewNearDue = 0;
        private int _pastDue = 0;
        private int _surveyRequest = 0;
        private int _serviceVisit = 0;
        private int _surveyDaysCount = 0;

        public UserCounts(Guid userID)
        {
            _userID = userID;
        }


        public Guid UserID
        {
            get { return _userID; }
        }

        public int this[string field]
        {
            get
            {
                switch (field)
                {
                    case "Total": return this.Total;
                    case "AwaitingAcceptance": return this.AwaitingAcceptance;
                    case "Correction": return this.Correction;
                    case "SurveyNearDue": return this.SurveyNearDue;
                    case "ReviewNearDue": return this.ReviewNearDue;
                    case "PastDue": return this.PastDue;
                    case "SurveyRequest": return this.SurveyRequest;
                    case "ServiceVisit": return this.ServiceVisit;
                    case "SurveyDaysCount": return this.SurveyDaysCount;
                    default:
                        {
                            throw new NotSupportedException("Field '" + field + "' was not expected.");
                        }
                }
            }
            set
            {
                switch (field)
                {
                    case "Total": this.Total = value; break;
                    case "AwaitingAcceptance": this.AwaitingAcceptance = value; break;
                    case "Correction": this.Correction = value; break;
                    case "SurveyNearDue": this.SurveyNearDue = value; break;
                    case "ReviewNearDue": this.ReviewNearDue = value; break;
                    case "PastDue": this.PastDue = value; break;
                    case "SurveyRequest": this.SurveyRequest = value; break;
                    case "ServiceVisit": this.ServiceVisit = value; break;
                    case "SurveyDaysCount": this.SurveyDaysCount = value; break;
                    default:
                        {
                            throw new NotSupportedException("Field '" + field + "' was not expected.");
                        }
                }
            }
        }


        public int Total
        {
            get { return _total; }
            set { _total = value; }
        }

        public int AwaitingAcceptance
        {
            get { return _awaitingAcceptance; }
            set { _awaitingAcceptance = value; }
        }

        public int Correction
        {
            get { return _correction; }
            set { _correction = value; }
        }

        public int SurveyNearDue
        {
            get { return _surveyNearDue; }
            set { _surveyNearDue = value; }
        }

        public int ReviewNearDue
        {
            get { return _reviewNearDue; }
            set { _reviewNearDue = value; }
        }

        public int PastDue
        {
            get { return _pastDue; }
            set { _pastDue = value; }
        }

        public int SurveyRequest
        {
            get { return _surveyRequest; }
            set { _surveyRequest = value; }
        }

        public int ServiceVisit
        {
            get { return _serviceVisit; }
            set { _serviceVisit = value; }
        }

        public int SurveyDaysCount
        {
            get { return _surveyDaysCount; }
            set { _surveyDaysCount = value; }
        }

    }

    private class ImportCounts : IComparable
    {
        private DateTime _date;
        private int _notAssigned = 0;
        private int _assignedToCompanyConsultant = 0;
        private int _assignedToVendor = 0;
        private int _onHold = 0;
        private int _errorCreating = 0;
        private int _inProgress = 0;

        public ImportCounts(DateTime date)
        {
            _date = date;
        }


        public DateTime Date
        {
            get { return _date; }
        }


        public int this[string field]
        {
            get
            {
                switch (field)
                {
                    case "U": return this.NotAssigned;
                    case "A": return this.AssignedToCompanyConsultant;
                    case "V": return this.AssignedToVendor;
                    case "H": return this.OnHold;
                    case "E": return this.ErrorCreating;
                    case "I": return this.InProgress;
                    default:
                        {
                            throw new NotSupportedException("Field '" + field + "' was not expected.");
                        }
                }
            }
            set
            {
                switch (field)
                {
                    case "U": this.NotAssigned = value; break;
                    case "A": this.AssignedToCompanyConsultant = value; break;
                    case "V": this.AssignedToVendor = value; break;
                    case "H": this.OnHold = value; break;
                    case "E": this.ErrorCreating = value; break;
                    case "I": this.InProgress = value; break;
                    default:
                        {
                            throw new NotSupportedException("Field '" + field + "' was not expected.");
                        }
                }
            }
        }


        public int NotAssigned
        {
            get { return _notAssigned; }
            set { _notAssigned = value; }
        }

        public int AssignedToCompanyConsultant
        {
            get { return _assignedToCompanyConsultant; }
            set { _assignedToCompanyConsultant = value; }
        }

        public int AssignedToVendor
        {
            get { return _assignedToVendor; }
            set { _assignedToVendor = value; }
        }

        public int OnHold
        {
            get { return _onHold; }
            set { _onHold = value; }
        }

        public int ErrorCreating
        {
            get { return _errorCreating; }
            set { _errorCreating = value; }
        }

        public int InProgress
        {
            get { return _inProgress; }
            set { _inProgress = value; }
        }

        public int Total
        {
            get
            {
                return this.NotAssigned
                    + this.AssignedToCompanyConsultant
                    + this.AssignedToVendor
                    + this.OnHold;
                //NOTE: don't count -> this.ErrorImporting;
                //NOTE: don't count -> this.InProgress;
            }
        }


        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            if (obj is ImportCounts)
            {
                ImportCounts item = (ImportCounts)obj;
                return -1 * this.Date.CompareTo(item.Date);
            }
            throw new ArgumentException("Value is not the correct type.");
        }

        #endregion
    }
}

