using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using System.Data.SqlClient;
using Berkley.BLC.Entities;
using Berkley.BLC.Import;
using Berkley.BLC.Business;
using Wilson.ORMapper;
using QCI.Web.Validation;
using QCI.Web;
using System.Linq;
using System.Text;

public partial class CreateSurveySearchAspx : AppBasePage
{
    internal static string surveyType = "";
    #region Event Handlers
    
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(CreateSurveySearchAspx_Load);
        this.PreRender += new EventHandler(CreateSurveySearchAspx_PreRender);
        this.btnSearch.Click += new EventHandler(btnSearch_Click);
        this.btnCreateSurvey.Click += new EventHandler(btnCreateSurvey_Click);
        this.btnExpressRequest.Click += new EventHandler(btnExpressRequest_Click);
        this.btnExpressRequest.PreRender += new EventHandler(btnExpressRequest_PreRender);
        this.btnCreateManualSurvey.Click += new EventHandler(btnCreateManualSurvey_Click);
        this.lnkCreateManualSurvey.Click += new EventHandler(lnkCreateManualSurvey_Click);
        this.btnView.Click += new EventHandler(btnView_Click);
        this.cboSurveyType.SelectedIndexChanged += new EventHandler(cboSurveyType_SelectedIndexChanged);
        this.grdSurveys.ItemCommand += new DataGridCommandEventHandler(grdSurveys_ItemCommand);
    }
    #endregion

    private bool _enableManualCreation = false;
    protected string _SurveyTypeLabel = string.Empty;
    protected string _surveyRequestByInsuredName;
    protected string _noSaveIncompleteSurvey;
    protected bool _showSurveyRequestedDateField;
    protected bool _showOpenSurveysCount = false;
    protected int _openSurveysCount;
    protected string _urlParam = string.Empty;
    protected bool _defaultpolicysystem;
    protected bool _selectedSystemFactory;
    protected bool _hideSICCode;
    protected bool _isCWGSurvey;
    
    void CreateSurveySearchAspx_Load(object sender, EventArgs e)
    {
        bool manualFlagInQueryString = ARMSUtility.GetBoolFromQueryString("manual", false);
        lnkCreateManualSurvey.Visible = (CurrentUser.Company.CreateSurveyType == CreateSurveyType.Both);
        lnkCreateManualSurvey.Text = (CurrentUser.Company.CreateSurveyType == CreateSurveyType.Both && manualFlagInQueryString) ? "(Create Survey From Policy)" : "(Create Survey From Scratch)";
        
        /* Company Parameters */
        _surveyRequestByInsuredName = Berkley.BLC.Business.Utility.GetCompanyParameter("SurveyRequestByInsuredName", CurrentUser.Company).ToLower();
        _showOpenSurveysCount = (Berkley.BLC.Business.Utility.GetCompanyParameter("ShowOpenSurveyCount", CurrentUser.Company).ToLower() == "true") ? true : false;
        _defaultpolicysystem = (Berkley.BLC.Business.Utility.GetCompanyParameter("DefaultPolicySystem", CurrentUser.Company).ToLower() == "true") ? true : false;
        _selectedSystemFactory = (Berkley.BLC.Business.Utility.GetCompanyParameter("SelectedSystemFactory", CurrentUser.Company).ToLower() == "true") ? true : false;
        _showSurveyRequestedDateField = (Berkley.BLC.Business.Utility.GetCompanyParameter("ShowSurveyRequestedDateField", CurrentUser.Company).ToLower() == "true") ? true : false;
        _hideSICCode = (Berkley.BLC.Business.Utility.GetCompanyParameter("HideSICCode", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _isCWGSurvey = (Berkley.BLC.Business.Utility.GetCompanyParameter("IsCWGSurvey", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;

        boxInsured.Visible = false;
        if (_isCWGSurvey)
        {
            trSurveyReasons.Visible = false;
            cboSurveyReasonType.Visible = true;
        }
        else
        {            
            cboSurveyReasonType.Visible = false;
        }

        if (!this.IsPostBack)
        {
            _enableManualCreation = (CurrentUser.Company.CreateSurveyType == CreateSurveyType.Manual) ||
                                    (CurrentUser.Company.CreateSurveyType == CreateSurveyType.Both && manualFlagInQueryString);
            _noSaveIncompleteSurvey = Utility.GetCompanyParameter("NoSaveIncompleteSurvey", CurrentUser.Company);
            cboSurveyType.AutoPostback = !_enableManualCreation;
            btnSearch.Visible = !_enableManualCreation;
            btnCreateManualSurvey.Visible = _enableManualCreation;

            numPolicyNumber.MaxLength = (CurrentUser.Company.ID == Company.CarolinaCasualtyInsuranceGroup.ID) ? 20 : (CurrentUser.Company.ID == Company.BerkleySpecialtyUnderwritingManagers.ID) ? 11 : 7;
            valPolicyNumber.Type = (CurrentUser.Company.ID == Company.CarolinaCasualtyInsuranceGroup.ID || CurrentUser.Company.ID == Company.BerkleySpecialtyUnderwritingManagers.ID) ? ValidationDataType.String : ValidationDataType.Integer;

            trPolicyNumber.Visible = false;
            numSubmissionNumber.Visible = false;
            txtQuoteNumber.Visible = false;
            cboPolicySystem.Visible = false;
            cboTransactionType.Visible = false;
            trInsured.Visible = false; 

            //clear out the cache
            this.UserIdentity.DeletePageSortSetting("createsurvey");

            // populate the survey type list with company specific types
            SurveyType[] surveyTypes = SurveyType.GetRequestSurveyTypes(CurrentUser.Company);
            cboSurveyType.DataBind(surveyTypes, "TypeCode", "Type.Name");

            SurveyReasonType[] surveyReasonTypes = SurveyReasonType.GetRequestSurveyReasonTypes(CurrentUser.Company);
            cboSurveyReasonType.DataBind(surveyReasonTypes, "ID", "Name");


            // populate the transaction type list with company specific types
            TransactionType[] transactionTypes = TransactionType.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            cboTransactionType.DataBind(transactionTypes, "ID", "Description");
            cboTransactionType.Visible = (cboTransactionType.Items.Count > 1);

            // populate the policy system dropdown
            CompanyPolicySystem[] policySystems = CompanyPolicySystem.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            cboPolicySystem.DataBind(policySystems, "PolicySystemCompanyID", "PolicySystemCompany.LongAbbreviation");

            if (_defaultpolicysystem && cboPolicySystem.Items.Count > 1)
                cboPolicySystem.SelectedIndex = 1;

            // If Requested Date is to be shown, also it's requirement to true and default the date to today
            if (_showSurveyRequestedDateField)
            {
                dtRequestedDate.IsRequired = true;
                dtRequestedDate.Date = DateTime.Now;
            }

            SurveyReasonType[] reasonTypes = SurveyReasonType.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            chkListSurveyReasons.DataSource = reasonTypes;
            chkListSurveyReasons.DataBind();            
            if (_isCWGSurvey)
            {
                trSurveyReasons.Visible = false;
                cboSurveyReasonType.Visible = true;
            }
            else
            {
                trSurveyReasons.Visible = reasonTypes.Length > 0;
                cboSurveyReasonType.Visible = false;
            }
            if (CurrentUser.Company.CreateSurveyType == CreateSurveyType.Both)
            {
                JavaScript.AddConfirmationNoticeToWebControl(btnCreateManualSurvey, "WARNING:  This survey request " +
                        "is being created without an existing policy or submission number.  " +
                        "No communication will exist between this survey and a workflow system (BPM)." +
                        "\r\rAre you sure you want to continue?");
            }

            //Added By Vilas Meshram: 02/18/2013: squish #21352 : Change Survey Type to Request Type labeling
            _SurveyTypeLabel = Berkley.BLC.Business.Utility.GetCompanyParameter("SurveyTypeLabel", CurrentUser.Company);
            if (!string.IsNullOrWhiteSpace(_SurveyTypeLabel))
                cboSurveyType.Name = _SurveyTypeLabel;
            //Added By Vilas Meshram: 02/18/2013: squish #21352 : Change Survey Type to Request Type labeling
        }
    }

    void CreateSurveySearchAspx_PreRender(object sender, EventArgs e)
    {
        base.PageHeading = (_enableManualCreation) ? "Manually Create Survey Request" : "Create Survey Request";
        
        Survey[] surveys;
        if (UserIdentity.Current.IsUnderwriter)
        {
            surveys = Survey.GetSortedArray("CompanyID = ? && CreateByExternalUserName = ? && CreateStateCode = ? && StatusCode = ? && ServiceTypeCode = ?", "CreateDate DESC",
            CurrentUser.CompanyID,
            UserIdentity.Current.Name,
            CreateState.InProgress.Code,
            SurveyStatus.UnassignedSurvey.Code,
            ServiceType.SurveyRequest.Code);
        }
        else
        {
            
            surveys = Survey.GetSortedArray("CompanyID = ? && CreateByInternalUserID = ? && CreateStateCode = ? && StatusCode = ? && ServiceTypeCode = ?", "CreateDate DESC",
            CurrentUser.CompanyID,
            CurrentUser.ID,
            CreateState.InProgress.Code,
            SurveyStatus.UnassignedSurvey.Code,
            ServiceType.SurveyRequest.Code);

            if (_noSaveIncompleteSurvey != null && _noSaveIncompleteSurvey.ToUpper() == "FALSE")
            {
                boxInProgressSurveys.Visible = false;
                for(int i=0;i<surveys.Length;i++)
                {
                    Guid surveyID = surveys[i].ID;
                    DB.Engine.ExecuteCommand(string.Format("EXEC Survey_delete @SurveyID='{0}'", surveyID));
                }
            }

            surveys = Survey.GetSortedArray("CompanyID = ? && CreateByInternalUserID = ? && CreateStateCode = ? && StatusCode = ? && ServiceTypeCode = ?", "CreateDate DESC",
            CurrentUser.CompanyID,
            CurrentUser.ID,
            CreateState.InProgress.Code,
            SurveyStatus.UnassignedSurvey.Code,
            ServiceType.SurveyRequest.Code);
        }

        boxInProgressSurveys.Title = string.Format("Incomplete Survey Requests ({0})", surveys.Length);
        DataGridHelper.BindGrid(grdSurveys, surveys, "ID", "You currently have no incomplete survey requests.");

        if (!this.IsPostBack)
        {
            if (surveys.Length > 0 && CurrentUser.IsUnderwriter)
            {
                JavaScript.ShowMessage(this, string.Format("You currently have {0} incomplete survey request(s).  Please either complete or delete them.", surveys.Length));
            }
        }

    }

    void cboSurveyType_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (cboSurveyType.SelectedValue == string.Empty)
        {
            trPolicyNumber.Visible = false;
            cboPolicySystem.Visible = false;
            numSubmissionNumber.Visible = false;
            txtQuoteNumber.Visible = false;
            trInsured.Visible = false; 
        }
        //Added By Vilas Meshram: 03/12/2013: Squish# 21338: Survey requests by insured name 
        else if (_surveyRequestByInsuredName == "true")
        {
            trPolicyNumber.Visible = false;
            cboPolicySystem.Visible = false;
            numSubmissionNumber.Visible = false;
            txtQuoteNumber.Visible = false;
            trInsured.Visible = true; 
        }
        else if (SurveyTypeType.IsNotWrittenBusiness(SurveyTypeType.Get(cboSurveyType.SelectedValue)))
        {
            trPolicyNumber.Visible = false;
            cboPolicySystem.Visible = (cboPolicySystem.Items.Count > 1);
            numSubmissionNumber.Visible = true;
            txtQuoteNumber.Visible = CurrentUser.Company.ImportQuotes;
            trInsured.Visible = false; 
        }
        else
        {
            trPolicyNumber.Visible = true;
            cboPolicySystem.Visible = (cboPolicySystem.Items.Count > 1);
            numSubmissionNumber.Visible = CurrentUser.Company.CreateServicePlansFromProspects;
            txtQuoteNumber.Visible = false;
            trInsured.Visible = false; 
        }
        surveyType = cboSurveyType.SelectedValue.ToString();
    }

    void grdSurveys_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "DELETE")
        {
            Guid surveyID = (Guid)grdSurveys.DataKeys[e.Item.ItemIndex];
            DB.Engine.ExecuteCommand(string.Format("EXEC Survey_delete @SurveyID='{0}'", surveyID));
        }
    }

    void lnkCreateManualSurvey_Click(object sender, EventArgs e)
    {
        bool manualFlagInQueryString = ARMSUtility.GetBoolFromQueryString("manual", false);
        if (manualFlagInQueryString)
        {
            Response.Redirect("createsurveysearch.aspx", true);
        }
        else
        {
            Response.Redirect("createsurveysearch.aspx?manual=1", true);
        }
    }

    void btnSearch_Click(object sender, EventArgs e)
    {
        // double check state of validators due to the post back
        if (!this.IsValid)
        {
            return;
        }
        try
        {
            ReconcileInsured oReconcileInsured;
            BlcInsured oBlcInsured;
            SurveyTypeType eSurveyType = SurveyTypeType.Get(cboSurveyType.SelectedValue);

            int checkedCounted = 0;
            foreach (ListItem item in chkListSurveyReasons.Items)
            {
                if (item.Selected)
                {
                    checkedCounted = checkedCounted + 1;
                }
            }

            if (CurrentUser.Company.CreateServicePlansFromProspects && numPolicyNumber.Text.Trim().Length <= 0 && numSubmissionNumber.Text.Trim().Length <= 0)
            {
                throw new FieldValidationException(numPolicyNumber, "The policy or submission number must be entered.");
            }

            if (CurrentUser.Company.CreateServicePlansFromProspects && numPolicyNumber.Text.Trim().Length > 0 && numSubmissionNumber.Text.Trim().Length > 0)
            {
                throw new FieldValidationException(numPolicyNumber, "Please enter only the policy or submission number, not both.");
            }

            //Added By Vilas Meshram: 03/14/2013: Squish# 21338: Survey requests by insured name 
            if (_surveyRequestByInsuredName == "true")
            {
                if (txtInsured.Text.Trim().Length <= 0)
                {
                    throw new FieldValidationException(txtInsured, "The Insured must be entered.");
                }

                if (txtInsured.Text.Trim() != hdName.Value)
                {
                    throw new FieldValidationException(txtInsured, "Invalid Insured name. Please select Insured name from presented list.");
                }
                
                if (chkListSurveyReasons.Visible && checkedCounted == 0 && eSurveyType != SurveyTypeType.NewBusiness)
                {
                    throw new FieldValidationException(numPolicyNumber, "At least one survey reason must be selected.");
                }

                //Run the importfactory to get the insured data from the policy system
                ImportFactory factory = new ImportFactory(CurrentUser.Company);

                //Check if this company imports from other policy systems (i.e. national accounts company)
                if (cboPolicySystem.Items.Count > 1)
                {
                    Company selectedCompany = Company.Get(new Guid(cboPolicySystem.SelectedValue));
                    if (_selectedSystemFactory)
                        factory = new ImportFactory(selectedCompany);
                    oBlcInsured = factory.ImportInsuredByPolicy(Convert.ToInt32(selectedCompany.Number).ToString() + numPolicyNumber.Text.Trim(), CurrentUser.Company.YearsOfLossHistory > 0);

                    if (oBlcInsured.AgencyEntity == null || oBlcInsured.AgencyEntity.Name == string.Empty)
                    {
                        string agencyNumber = (oBlcInsured.AgencyEntity != null) ? oBlcInsured.AgencyEntity.Number : string.Empty;
                        ImportFactory agencyFactory = new ImportFactory(selectedCompany);
                        oBlcInsured.AgencyEntity = agencyFactory.ImportAgency(agencyNumber, numPolicyNumber.Text.Trim());
                    }
                }
                else
                {
                    numPolicyNumber.Text = hdPolicyNumber.Value.Trim();
                    oBlcInsured = factory.ImportInsuredByPolicy(numPolicyNumber.Text.Trim(), CurrentUser.Company.YearsOfLossHistory > 0);
                }

            }
            else if (SurveyTypeType.IsNotWrittenBusiness(eSurveyType) || (CurrentUser.Company.CreateServicePlansFromProspects && numSubmissionNumber.Text.Trim().Length > 0))
            {
                if (numSubmissionNumber.Text.Trim().Length <= 0)
                {
                    throw new FieldValidationException(numPolicyNumber, "The submission number must be entered.");
                }

                if (chkListSurveyReasons.Visible && checkedCounted == 0)
                {
                    throw new FieldValidationException(numPolicyNumber, "At least one survey reason must be selected.");
                }

                //Check if this company imports from other companies clearance systems
                Company submissionCompany = (cboPolicySystem.Items.Count > 1) ? Company.Get(new Guid(cboPolicySystem.SelectedValue)) : CurrentUser.Company;

                //First check for a valid submission
                //Check if we can pull any data from Clearance
                ProspectBuilder oBuilder = new ProspectBuilder();
                oBlcInsured = oBuilder.BuildFromSubmissionNumber(numSubmissionNumber.Text.Trim(), submissionCompany);

                //If a quote number was entered....
                if (CurrentUser.Company.ImportQuotes && txtQuoteNumber.Text.Trim().Length > 0)
                {
                    //Run the importfactory to get the insured data from the quote
                    ImportFactory factory = new ImportFactory(CurrentUser.Company);
                    oBlcInsured = factory.ImportInsuredByQuote(txtQuoteNumber.Text.Trim());
                }
            }

            else
            {
                if (numPolicyNumber.Text.Trim().Length <= 0)
                {
                    throw new FieldValidationException(numPolicyNumber, "The policy number must be entered.");
                }

                if (chkListSurveyReasons.Visible && checkedCounted == 0 && eSurveyType != SurveyTypeType.NewBusiness)
                {
                    throw new FieldValidationException(numPolicyNumber, "At least one survey reason must be selected.");
                }

                //Run the importfactory to get the insured data from the policy system
                ImportFactory factory = new ImportFactory(CurrentUser.Company);

                //Check if this company imports from other policy systems (i.e. national accounts company)
                if (cboPolicySystem.Items.Count > 1)
                {
                    Company selectedCompany = Company.Get(new Guid(cboPolicySystem.SelectedValue));
                    
                    if (_selectedSystemFactory)
                    {
                        factory = new ImportFactory(selectedCompany);
                        oBlcInsured = factory.ImportInsuredByPolicy(Convert.ToInt32(selectedCompany.Number).ToString() + numPolicyNumber.Text.Trim(), CurrentUser.Company.YearsOfLossHistory > 0);
                    }
                    else
                    {
                        oBlcInsured = factory.ImportInsuredByPolicy(Convert.ToInt32(selectedCompany.Number).ToString() + numPolicyNumber.Text.Trim(), CurrentUser.Company.YearsOfLossHistory > 0);
                    }

                    if (oBlcInsured.AgencyEntity == null || oBlcInsured.AgencyEntity.Name == string.Empty)
                    {
                        string agencyNumber = (oBlcInsured.AgencyEntity != null) ? oBlcInsured.AgencyEntity.Number : string.Empty;
                        ImportFactory agencyFactory = new ImportFactory(selectedCompany);
                        oBlcInsured.AgencyEntity = agencyFactory.ImportAgency(agencyNumber, numPolicyNumber.Text.Trim());
                    }
                }
                else
                {
                    oBlcInsured = factory.ImportInsuredByPolicy(numPolicyNumber.Text.Trim(), CurrentUser.Company.YearsOfLossHistory > 0);
                }
            }

            oReconcileInsured = new ReconcileInsured(oBlcInsured, this.CurrentUser.Company);
            oReconcileInsured.NewInsert();

            btnCreateSurvey.Attributes.Add("InsuredID", oBlcInsured.InsuredEntity.ID.ToString());
            btnCreateSurvey.Attributes.Add("SurveyTypeCode", eSurveyType.Code);            

            btnExpressRequest.Attributes.Add("InsuredID", oBlcInsured.InsuredEntity.ID.ToString());
            btnExpressRequest.Attributes.Add("SurveyTypeCode", eSurveyType.Code);
            


            //Check if the insured is non-renewed and display warning
            if (oBlcInsured.InsuredEntity.NonrenewedDate != DateTime.MinValue)
            {
                btnCreateSurvey.Attributes.Add("onclick", "javascript: Page_IsValid=false; return confirm('Account was non-renewed. Do you want to continue creating a survey request?');");
            }

            //Check if this is a remote agency and if they have access to view the policy/insurd details
            if (!String.IsNullOrEmpty(CurrentUser.RemoteAgencyNumbers))
            {
                if (!CurrentUser.RemoteAgencyNumbers.Contains(oBlcInsured.AgencyEntity.Number))
                {
                    throw new FieldValidationException(numPolicyNumber, "You do not have access to create a survey from this policy or submission.  Please use a different policy or submission number and try again.");
                }
            }
            //Open Surveys
            //Guid surveyTypeID = SurveyType.GetOne(eSurveyType.Code, this.CurrentUser.Company);
            Survey[] eSurvey = Survey.GetArray("CompanyID = ? && (StatusCode = 'SU' OR StatusCode = 'SA' OR StatusCode = 'RU' OR StatusCode = 'RA' OR StatusCode = 'SW') && Insured.ClientID = ?", this.CurrentUser.CompanyID, oBlcInsured.InsuredEntity.ClientID);
            _openSurveysCount = eSurvey.Length;
            btnView.Text = string.Format("View Active Requests ({0})", _openSurveysCount);

            boxInsured.Visible = true;
            this.PopulateFields(oBlcInsured.InsuredEntity);
            //PrepConsultant();

        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (BlcNoInsuredException ex)
        {
            JavaScript.ShowMessage(this, ex.Message + "\r\n\r\n* Note that if this policy was issued/renewed today, it may not be available to ARMS until the following day.");
            return;
        }
        catch (Berkley.BLC.Import.BlcProspectWebServiceException ex)
        {
            JavaScript.ShowMessage(this, ex.Message);
            return;
        }
        catch (Berkley.BLC.Import.Prospect.BlcProspectWebServiceException ex)
        {
            JavaScript.ShowMessage(this, ex.Message);
            return;
        }
    }

    /*
      private void PrepConsultant()
      {
          // populate combo with all consultants
          User[] eUsers;

          //filter
          StringBuilder sb = new StringBuilder();
          sb.Append("AccountDisabled = false && ");
          sb.Append("(Role[PermissionSet.SurveyActionPermissions[");
          sb.Append("(Setting = ? || Setting = ? || Setting = ? || Setting = ?) ]]) ");
        


          //params
          ArrayList args = new ArrayList();
          args.Add(PermissionSet.SETTING_OWNER);
          args.Add(PermissionSet.SETTING_OWNER_AND_OTHERS);
          args.Add(PermissionSet.SETTING_OWNER_AND_VENDORS);
          args.Add(PermissionSet.SETTING_ALL);
          eUsers = this.CurrentUser.Company.GetConsultants(sb.ToString(), args.ToArray());
     
          ComboHelper.BindCombo(cboUsers, eUsers, "ID", "Name");
          cboUsers.Items.Insert(0, new ListItem("-- System Selection --", ""));

      }
      */

    //Modified By Vilas Meshram: 02/15/2013: #21356: To handle Express request
    void btnCreateSurvey_Click(object sender, EventArgs e)
    {
         Survey eSurvey = null;
         
         if (CreateSurvey(sender, ref eSurvey))
         {
             //stub out the locations and policies for status of completion purposes
             DB.Engine.ExecuteCommand(string.Format("EXEC CreateSurvey_InsertPolicyLocations @InsuredID='{0}'", eSurvey.InsuredID));             
             Response.Redirect(string.Format("CreateSurvey.aspx?surveyid={0}", eSurvey.ID), true);
         }
    }

    //Added By Vilas Meshram: 02/15/2013: #21356: To handle Express request
    void btnExpressRequest_Click(object sender, EventArgs e)
    {
        Survey eSurvey = null;

        if (CreateSurvey(sender, ref eSurvey))
        {
            //default all the reportable coverages as being selected as "report" for quick survey creation
            DB.Engine.ExecuteCommand(string.Format("EXEC CreateSurvey_InsertPolicyLocationsCoverageName @InsuredID='{0}'", eSurvey.InsuredID));
            Response.Redirect(string.Format("CreateSurvey.aspx?surveyid={0}", eSurvey.ID), true);
        }
    }

    protected void btnExpressRequest_PreRender(object sender, EventArgs e)
    {
        string strMessage = Berkley.BLC.Business.Utility.GetCompanyParameter("ExpressRequestMessage", CurrentUser.Company);
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, strMessage);
    }
    /*
    void btnCreateSurvey_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        Guid gInsuredID = new Guid(btn.Attributes["InsuredID"]);
        SurveyTypeType eSurveyType = SurveyTypeType.Get(cboSurveyType.SelectedValue);

        Insured eInsured = Insured.Get(gInsuredID);

        //Check if the user is resubmitting the same survey twice
        Survey dupSurvey = Survey.GetOne("InsuredID = ?", gInsuredID);
        if (dupSurvey != null)
        {
            JavaScript.ShowMessage(this, "You will be creating a duplicate survey request.  Please either use or delete the incomplete survey request below for this account before continuing.");
            return;
        }

        //Check if any policies exist
        Policy[] policies = Policy.GetSortedArray("InsuredID = ?", "Mod DESC", gInsuredID);

        //Stub out new survey to include all insured locations until they are later broken out
        Survey eSurvey = new Survey(new Guid());

        //commit changes
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            eSurvey.CompanyID = this.CurrentUser.Company.ID;
            eSurvey.InsuredID = eInsured.ID;

            Policy primaryPolicy = null;
            if (!SurveyTypeType.IsNotWrittenBusiness(eSurveyType))
            {
                if (policies.Length == 0)
                {
                    JavaScript.ShowMessage(this, "Unable to create a survey request due to no policies being returned for this account.  Please contact your Loss Control Department if you have any questions.");
                    return;
                }

                primaryPolicy = Policy.GetOne("Number = ? && InsuredID = ?", numPolicyNumber.Text, gInsuredID);
                eSurvey.PrimaryPolicyID = (primaryPolicy != null) ? primaryPolicy.ID : Policy.GetOne("InsuredID = ?", gInsuredID).ID;
            }

            eSurvey.TypeID = SurveyType.GetOne(eSurveyType.Code, this.CurrentUser.Company);
            eSurvey.TransactionTypeID = (cboTransactionType.Items.Count > 1) ? Guid.Parse(cboTransactionType.SelectedValue) : Guid.Empty;
            eSurvey.ServiceTypeCode = ServiceType.SurveyRequest.Code;
            eSurvey.StatusCode = SurveyStatus.UnassignedSurvey.Code;
            eSurvey.CreateStateCode = CreateState.InProgress.Code;
            eSurvey.CreateDate = DateTime.Now;
            eSurvey.CreateByInternalUserID = (!UserIdentity.Current.IsUnderwriter) ? CurrentUser.ID : Guid.Empty;
            eSurvey.CreateByExternalUserName = (UserIdentity.Current.IsUnderwriter) ? UserIdentity.Current.Name : string.Empty;
            eSurvey.DueDate = (CurrentUser.Company.SurveyDaysUntilDue > 0) ? DateTime.Today.AddDays(this.CurrentUser.Company.SurveyDaysUntilDue) : DateTime.MinValue;
            eSurvey.WorkflowSubmissionNumber = (SurveyTypeType.IsNotWrittenBusiness(eSurveyType) || (CurrentUser.Company.CreateServicePlansFromProspects && numSubmissionNumber.Text.Trim().Length > 0)) ? numSubmissionNumber.Text : string.Empty;
            eSurvey.UnderwriterCode = eInsured.Underwriter;
            eSurvey.Save(trans);

            //save any reasons checked
            foreach (ListItem item in chkListSurveyReasons.Items)
            {
                if (item.Selected)
                {
                    SurveyReason surveyReason = new SurveyReason(new Guid(item.Value), eSurvey.ID);
                    surveyReason.Save(trans);
                }
            }

            if (SurveyTypeType.IsNotWrittenBusiness(eSurveyType) || (CurrentUser.Company.CreateServicePlansFromProspects && numSubmissionNumber.Text.Trim().Length > 0))
            {
                foreach (Policy policy in policies)
                {
                    if (policy != null)
                    {
                        if (policy.LineOfBusiness != null)
                        {
                            //stub out the coverages for the newly created lob
                            ImportFactory factory = new ImportFactory(CurrentUser.Company);
                            BlcInsured oBlcInsured = factory.GetEmptyCoverages(policy.LineOfBusiness);

                            foreach (BlcCoverage blcCoverage in oBlcInsured.CoverageList)
                            {
                                Coverage eCoverage = new Coverage(new Guid());
                                eCoverage.InsuredID = eSurvey.InsuredID;
                                eCoverage.PolicyID = policy.ID;
                                eCoverage.TypeCode = blcCoverage.CoverageValueType;

                                //get the coverage name
                                CoverageNameTable tbl = new CoverageNameTable();
                                tbl.Load(this.CurrentUser.Company);
                                eCoverage.NameID = tbl.GetId(blcCoverage);

                                eCoverage.Save(trans);
                            }
                        }
                        else
                        {
                            JavaScript.ShowMessage(this, "Unable to create a survey request from this submission number.  Please select a policy symbol for all of the submissions for this client in the Clearance system and retry generating this request.");
                            return;
                        }
                    }
                }
            }

            trans.Commit();
        }

        //default all the reportable coverages as being selected as "report" for quick survey creation
        if (CurrentUser.Company.SupportsQuickSurveyCreation)
        {
            DB.Engine.ExecuteCommand(string.Format("EXEC CreateSurvey_InsertPolicyLocationsCoverageName @InsuredID='{0}'", eSurvey.InsuredID));
        }
        else
        {
            //stub out the locations and policies for status of completion purposes
            DB.Engine.ExecuteCommand(string.Format("EXEC CreateSurvey_InsertPolicyLocations @InsuredID='{0}'", eSurvey.InsuredID));
        }

        Response.Redirect(string.Format("CreateSurvey.aspx?surveyid={0}", eSurvey.ID), true);
    }
    */
        
    void btnCreateManualSurvey_Click(object sender, EventArgs e)
    {
        //Stub out new survey
        Survey survey = new Survey(new Guid());
        
        //commit changes
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            Insured insured = new Insured(Guid.NewGuid());
            insured.CompanyID = CurrentUser.CompanyID;
            insured.ExclusionBit = 0;
            insured.Save(trans);

            survey.CompanyID = this.CurrentUser.Company.ID;
            survey.InsuredID = insured.ID;
            survey.TypeID = SurveyType.GetOne(cboSurveyType.SelectedValue, this.CurrentUser.Company);
            survey.ServiceTypeCode = ServiceType.SurveyRequest.Code;
            survey.StatusCode = SurveyStatus.UnassignedSurvey.Code;
            survey.CreateStateCode = CreateState.InProgress.Code;
            survey.CreateDate = DateTime.Now;
            survey.CreateByInternalUserID = (!UserIdentity.Current.IsUnderwriter) ? CurrentUser.ID : Guid.Empty;
            survey.CreateByExternalUserName = (UserIdentity.Current.IsUnderwriter) ? UserIdentity.Current.Name : string.Empty;
            survey.DueDate = (CurrentUser.Company.SurveyDaysUntilDue > 0) ? DateTime.Today.AddDays(this.CurrentUser.Company.SurveyDaysUntilDue) : DateTime.MinValue;
            survey.WorkflowSubmissionNumber = string.Empty;
            survey.Save(trans);

            if (_isCWGSurvey)
            {
                SurveyReason surveyReason = new SurveyReason(new Guid(cboSurveyReasonType.SelectedValue), survey.ID);
                surveyReason.Save(trans);
            }
            else { 
            //save any reasons checked
            foreach (ListItem item in chkListSurveyReasons.Items)
            {
                if (item.Selected)
                {
                    SurveyReason surveyReason = new SurveyReason(new Guid(item.Value), survey.ID);
                    surveyReason.Save(trans);
                }
            }
            }
            trans.Commit();
        }

        Response.Redirect(string.Format("CreateSurvey.aspx?surveyid={0}", survey.ID), true);
    }
    private bool CreateSurvey(object sender, ref Survey eSurvey) 
    {
        Button btn = (Button)sender;
        Guid gInsuredID = new Guid(btn.Attributes["InsuredID"]);

        SurveyTypeType eSurveyType = SurveyTypeType.Get(cboSurveyType.SelectedValue);

        Insured eInsured = Insured.Get(gInsuredID);

        //Check if the user is resubmitting the same survey twice
        Survey dupSurvey = Survey.GetOne("InsuredID = ?", gInsuredID);
        if (dupSurvey != null)
        {
            JavaScript.ShowMessage(this, "You will be creating a duplicate survey request.  Please either use or delete the incomplete survey request below for this account before continuing.");
            return false;
        }

        //Check if any policies exist
        Policy[] policies = Policy.GetArray("InsuredID = ?", gInsuredID);

        //Stub out new survey to include all insured locations until they are later broken out
        eSurvey = new Survey(new Guid());
        //Survey eSurvey = new Survey(new Guid());

        //commit changes
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            eSurvey.CompanyID = this.CurrentUser.Company.ID;
            eSurvey.InsuredID = eInsured.ID;
            if (cboPolicySystem.SelectedValue != "")
            {
                eSurvey.PolicySystemID = new Guid(cboPolicySystem.SelectedValue);
            }
            else
            {
                eSurvey.PolicySystemID = this.CurrentUser.Company.ID;
            }

            Policy primaryPolicy = null;
            if (!SurveyTypeType.IsNotWrittenBusiness(eSurveyType))
            {
                if (policies.Length == 0)
                {
                    JavaScript.ShowMessage(this, "Unable to create a survey request due to no policies being returned for this account.  Please contact your Loss Control Department if you have any questions.");
                    return false;
                }

                primaryPolicy = Policy.GetOne("Number = ? && InsuredID = ?", numPolicyNumber.Text, gInsuredID);
                eSurvey.PrimaryPolicyID = (primaryPolicy != null) ? primaryPolicy.ID : Policy.GetOne("InsuredID = ?", gInsuredID).ID;
            }

            eSurvey.TypeID = SurveyType.GetOne(eSurveyType.Code, this.CurrentUser.Company);
            eSurvey.TransactionTypeID = (cboTransactionType.Items.Count > 1) ? Guid.Parse(cboTransactionType.SelectedValue) : Guid.Empty;
            eSurvey.ServiceTypeCode = ServiceType.SurveyRequest.Code;
            eSurvey.StatusCode = SurveyStatus.UnassignedSurvey.Code;
            eSurvey.CreateStateCode = CreateState.InProgress.Code;
            eSurvey.CreateDate = DateTime.Now;
            eSurvey.CreateByInternalUserID = (!UserIdentity.Current.IsUnderwriter) ? CurrentUser.ID : Guid.Empty;
            eSurvey.CreateByExternalUserName = (UserIdentity.Current.IsUnderwriter) ? UserIdentity.Current.Name : string.Empty;
            eSurvey.DueDate = (CurrentUser.Company.SurveyDaysUntilDue > 0) ? DateTime.Today.AddDays(this.CurrentUser.Company.SurveyDaysUntilDue) : DateTime.MinValue;
            eSurvey.WorkflowSubmissionNumber = (SurveyTypeType.IsNotWrittenBusiness(eSurveyType) || (CurrentUser.Company.CreateServicePlansFromProspects && numSubmissionNumber.Text.Trim().Length > 0)) ? numSubmissionNumber.Text : string.Empty;
            eSurvey.UnderwriterCode = eInsured.Underwriter;
            eSurvey.RequestedDate = (dtRequestedDate.Date != null) ? dtRequestedDate.Date : DateTime.MinValue;
            // Save these values
            eSurvey.Save(trans);

            if (_isCWGSurvey)
            {
                SurveyReason surveyReason = new SurveyReason(new Guid(cboSurveyReasonType.SelectedValue), eSurvey.ID);
                surveyReason.Save(trans);
            }
            else
            {
                //save any reasons checked
                //save any reasons checked
                foreach (ListItem item in chkListSurveyReasons.Items)
                {
                    if (item.Selected)
                    {
                        SurveyReason surveyReason = new SurveyReason(new Guid(item.Value), eSurvey.ID);
                        surveyReason.Save(trans);
                    }
                }
            }
                     

            if (SurveyTypeType.IsNotWrittenBusiness(eSurveyType) || (CurrentUser.Company.CreateServicePlansFromProspects && numSubmissionNumber.Text.Trim().Length > 0))
            {
                foreach (Policy policy in policies)
                {
                    if (policy != null)
                    {
                        if (policy.LineOfBusiness != null)
                        {
                            //stub out the coverages for the newly created lob
                            ImportFactory factory = new ImportFactory(CurrentUser.Company);
                            BlcInsured oBlcInsured = factory.GetEmptyCoverages(policy.LineOfBusiness);

                            foreach (BlcCoverage blcCoverage in oBlcInsured.CoverageList)
                            {
                                Coverage eCoverage = new Coverage(new Guid());
                                eCoverage.InsuredID = eSurvey.InsuredID;
                                eCoverage.PolicyID = policy.ID;
                                eCoverage.TypeCode = blcCoverage.CoverageValueType;

                                //get the coverage name
                                CoverageNameTable tbl = new CoverageNameTable();
                                tbl.Load(this.CurrentUser.Company);
                                eCoverage.NameID = tbl.GetId(blcCoverage);

                                eCoverage.Save(trans);
                            }
                        }
                        else
                        {
                            JavaScript.ShowMessage(this, "Unable to create a survey request from this submission number.  Please select a policy symbol for all of the submissions for this client in the Clearance system and retry generating this request.");
                            return false;
                        }
                    }
                }
            }

            trans.Commit();
            return true;
        }

    }

    void btnView_Click(object sender, EventArgs e)
    {
        SearchCriteria c = new SearchCriteria();
        c.InsuredClientID = lblClientID.Text.Trim(); 
        _urlParam = c.ToQueryString();

        Response.Redirect(string.Format("SearchResults.aspx?name={0}{1}", HttpUtility.UrlEncode(base.PageHeading), _urlParam));
   }

    //Added By Vilas Meshram: 03/12/2013: Squish# 21338: When the user begins typing the name in the new insured name field, the system will present a list of options as 
    //                                    the user continues to type the name (similar to auto completing in Google search).
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetInsured(string prefixText)
    {
        if (prefixText == string.Empty)
            return null;
        prefixText = prefixText.Replace("'", "''");
        string mySurveyType = surveyType;
        //DataTable dt = StoredProcedures.CreateSurvey_GetSearchedInsuredName(prefixText);
        DataTable dt = StoredProcedures.CreateSurvey_GetSearchedInsuredNameBySurveyType(prefixText,surveyType);

        List<string> InsuredNames = new List<string>();
        string InsuredItem = string.Empty;

        foreach (DataRow row in dt.Rows)
        {
            InsuredItem = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row["Name"].ToString(), row["Number"].ToString());
            InsuredNames.Add(InsuredItem);
        }

        return InsuredNames;
    }
 
}





