using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web;
using Wilson.ORMapper;

public partial class AdminLinksAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminLinksAspx_Load);
        this.PreRender += new EventHandler(AdminLinksAspx_PreRender);
        this.grdLinks.ItemCommand += new DataGridCommandEventHandler(grdLinks_ItemCommand);
    }
    #endregion

    void AdminLinksAspx_Load(object sender, EventArgs e)
    {
    }

    void AdminLinksAspx_PreRender(object sender, EventArgs e)
    {
        // get the company specific links
        Link[] eLinks = Link.GetSortedArray("CompanyID = ?", "PriorityIndex ASC", this.CurrentUser.CompanyID);

        string noRecordsMessage = "There are no links for your company.";
        DataGridHelper.BindGrid(grdLinks, eLinks, "ID", noRecordsMessage);
    }

    protected void btnDelete_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to delete this link?");
    }

    void grdLinks_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid linkID = (Guid)grdLinks.DataKeys[e.Item.ItemIndex];

        // get the current links from the DB and find the index of the target columns
        Link[] links = Link.GetSortedArray("CompanyID = ?", "PriorityIndex ASC", this.UserIdentity.CompanyID);
        int targetIndex = int.MinValue;
        for (int i = 0; i < links.Length; i++)
        {
            if (links[i].ID == linkID)
            {
                targetIndex = i;
                break;
            }
        }
        if (targetIndex == int.MinValue) // not found
        {
            return;
        }

        Link targetLink = links[targetIndex];

        // execute the requested transformation on the rules
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            ArrayList linkList = new ArrayList(links);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEUP":
                    {
                        linkList.RemoveAt(targetIndex);
                        if (targetIndex > 0)
                        {
                            linkList.Insert(targetIndex - 1, targetLink);
                        }
                        else // top item (move to bottom)
                        {
                            linkList.Add(targetLink);
                        }
                        break;
                    }
                case "MOVEDOWN":
                    {
                        linkList.RemoveAt(targetIndex);
                        if (targetIndex < links.Length - 1)
                        {
                            linkList.Insert(targetIndex + 1, targetLink);
                        }
                        else // bottom item (move to top)
                        {
                            linkList.Insert(0, targetLink);
                        }
                        break;
                    }
                case "REMOVE":
                    {
                        linkList.RemoveAt(targetIndex);
                        links[targetIndex].Delete(trans);
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the links still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < linkList.Count; i++)
            {
                Link link = (linkList[i] as Link).GetWritableInstance();
                link.PriorityIndex = (i + 1);
                link.Save(trans);
            }

            trans.Commit();
        }
    }
}
