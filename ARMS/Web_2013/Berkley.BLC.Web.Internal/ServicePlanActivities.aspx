<%@ Page Language="C#" AutoEventWireup="false" CodeFile="ServicePlanActivities.aspx.cs" Inherits="ServicePlanActivitiesAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><a class="nav" href="ServicePlan.aspx<%= ARMSUtility.GetQueryStringForNav() %>"><< Back to Service Plan</a><br><br>

    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <asp:datagrid ID="grdActivities" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="20" AllowSorting="True">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
		    <asp:TemplateColumn HeaderText="Date" SortExpression="ActivityDate DESC">
	            <ItemTemplate>
		            <a href="ServicePlanActivityEdit.aspx<%= ARMSUtility.GetQueryStringForNav() %>&activityid=<%# HtmlEncode(Container.DataItem, "ID")%>">
		                <%# HtmlEncodeNullable(Container.DataItem, "ActivityDate", "{0:d}", DateTime.MinValue, "(none)")%>
		            </a>
	            </ItemTemplate>
            </asp:TemplateColumn>
		    <asp:TemplateColumn HeaderText="Hours" SortExpression="ActivityHours ASC">
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "ActivityHours", decimal.MinValue, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Activity Type" SortExpression="ActivityType.Name ASC">
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "ActivityType.Name", string.Empty, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Allocated To" SortExpression="User.Name ASC">
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "User.Name", string.Empty, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn>
	            <ItemTemplate>
		            &nbsp;<asp:LinkButton ID="btnRemoveRec" Runat="server" CommandName="Remove" OnPreRender="btnRemoveActivity_PreRender">Remove</asp:LinkButton>&nbsp;
	            </ItemTemplate>
            </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>

    <a href="ServicePlanActivityEdit.aspx<%= ARMSUtility.GetQueryStringForNav("surveyrecid") %>">Add New Activity Time</a>
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
