<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminServicePlanFieldEdit.aspx.cs" Inherits="AdminServicePlanFieldEditAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <cc:Box id="boxField" runat="server" title="Service Plan Field" width="600">
        <table class="fields">
        <uc:Text id="txtDescription" runat="server" field="DynamicField.Description" Columns="40" />
        <uc:Check id="chkIsRequired" runat="server" field="DynamicField.IsRequired" />
        <uc:Check id="chkFieldDisabled" runat="server" field="DynamicField.Disabled" />
        </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Add/Update" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
