<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminRoleEdit.aspx.cs" Inherits="AdminRoleEditAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register TagPrefix="uc" TagName="PermissionSetEditor" Src="~/Controls/PermissionSetEditor.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><a class="nav" href="AdminRoles.aspx"><< Back to User Roles</a></span><br><br>
    
    <cc:Box id="boxRole" runat="server" title="Role Information" width="500">
	<table class="fields">
	<uc:Text id="txtName" runat="server" field="UserRole.Name" Label="Name" Columns="70" />
	<uc:Text id="txtDescription" runat="server" field="UserRole.Description" Columns="70" Rows="6" />
	</table>
    </cc:Box>

    <uc:PermissionSetEditor id="ucPermissions" runat="server" />

    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Add/Update" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />

    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
