<%@ Page Language="C#" AutoEventWireup="false" ValidateRequest="false" CodeFile="Contact.aspx.cs" Inherits="ContactAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register TagPrefix="cf" Namespace="Berkley.BLC.Web.Internal.Fields" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxContacts" runat="server" Title="Update Contact" width="600">
        <table class="fields">
            <uc:Text id="txtName" runat="server" field="LocationContact.Name" Columns="50" />
            <uc:Text id="txtTitle" runat="server" field="LocationContact.Title" Columns="60" />
            <uc:Text id="txtPhoneNumber" runat="server" field="LocationContact.PhoneNumber" IsRequired="true" Directions="(ex. 5551234567)" Name="Phone" /> 
            <uc:Text id="txtAltPhoneNumber" runat="server" field="LocationContact.AlternatePhoneNumber" Directions="(ex. 5551234567)" Name="Alt Phone" />
            <uc:Text id="txtFaxNumber" runat="server" field="LocationContact.FaxNumber" Directions="(ex. 5551234567)" />
            <uc:Text id="txtEmailAddress" runat="server" field="LocationContact.EmailAddress" Name="Email" Columns="70" />
            <uc:Combo id="cboContactOption" runat="server" Name="Mail Option" Field="LocationContact.ContactReasonTypeCode" IsRequired="true" topItemText="" />
            <% if (_eContact == null) { %>
            <tr>
                <td class="label" nowrap>Add Contact To *</td>
                <td>
                    <asp:RadioButton ID="rdoAllClientLocations" GroupName="ContactType" Text="All Client Locations" runat="server" Checked="true" />&nbsp;&nbsp;&nbsp;
                    <asp:RadioButton ID="rdoThisLocationOnly" GroupName="ContactType" Text="This Location ONLY" runat="server" />
                    <cf:RadioButtonRequiredFieldValidator id="valContactType" runat="server" CssClass="val" ControlToValidate="rdoAllClientLocations" ErrorMessage="Add Contact To is required." Text="*" Display="Dynamic" />
                </td>
            </tr>
            <% } %>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr><td align="right"><asp:CheckBox ID="chkSameAsCorporateAddress" runat="server" /></td><td> Same as Corporate Mailing Address</td></tr>
            <uc:Text id="txtCompanyName" runat="server" field="LocationContact.CompanyName" Name="Company" Columns="70" />
            <uc:Text id="txtStreetLine1" runat="server" field="LocationContact.StreetLine1" Columns="80" />
            <uc:Text id="txtStreetLine2" runat="server" field="LocationContact.StreetLine2" Columns="80" />
            <uc:Text id="txtCity" runat="server" field="LocationContact.City" />
            <uc:Combo id="cboState" runat="server" field="LocationContact.StateCode" topItemText="" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" />
            <uc:Text id="txtZipCode" runat="server" field="LocationContact.ZipCode" />
        </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" />
    <asp:Button ID="btnAddAnother" Runat="server" CssClass="btn" Text="Add Another" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />

    <uc:HintFooter ID="ucHintFooter" runat="server" VerticalOffset="130" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript">
	function SameAsCorporateAddress(isChecked, insuredID) {
	    var arrFields = AjaxMethods.SameAsCorporateAddress(isChecked, insuredID).value;
	   
	    document.getElementById("txtCompanyName_txt").value = arrFields[0];
	    document.getElementById("txtStreetLine1_txt").value = arrFields[1];
        document.getElementById("txtStreetLine2_txt").value = arrFields[2];
        document.getElementById("txtCity_txt").value = arrFields[3];
        document.getElementById("cboState_cbo").value = arrFields[4];
        document.getElementById("txtZipCode_txt").value = arrFields[5];
	}
    </script>
    </form>
</body>
</html>
