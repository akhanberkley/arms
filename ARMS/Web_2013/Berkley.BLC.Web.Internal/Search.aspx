<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" ValidateRequest="false" CodeFile="Search.aspx.cs" Inherits="SearchAspx" %>

<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="ComboSearch" Src="~/Fields/ComboFieldSearch.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="DateRange" Src="~/Controls/DateRange.ascx" %>
<%@ Register TagPrefix="uc" TagName="NumberRange" Src="~/Controls/NumberRange.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
    <script language="javascript">var counter = 0;</script>
</head>
<body style="margin: 0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <uc:Header ID="ucHeader" runat="server" />
        <uc:HintHeader ID="ucHintHeader" runat="server" />

        <cc:Box ID="boxSearch" runat="server" Title="Search Criteria" Width="500">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td valign="top">
                        <table class="fields">
                            <cc:Group ID="grpSurvey" runat="server" Label="Survey Criteria" />
                            <uc:Number ID="numSurveyNumber" runat="server" Field="Survey.Number" IsRequired="false" />
                            <% if (_showPolicySystem){ %>
                            <uc:Combo ID="cboPolicySystem" runat="server" Field="" Name="Policy System" TopItemText="" IsRequired="false" />
                                <% } %>
                            <% if (CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID)
                               { %>
                            <uc:Combo ID="cboSurveyType" runat="server" Field="" Name="Survey Type" TopItemText="" IsRequired="false" />
                            <% } %>

                            <uc:Combo ID="cboServiceType" runat="server" Field="Survey.ServiceTypeCode" Name="Service Type" TopItemText="" IsRequired="false"
                                DataType="ServiceType" DataValueField="Code" DataTextField="Name" DataOrder="DisplayOrder ASC" />

                            <% if (CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID)
                               { %>
                            <uc:Combo ID="cboUser" runat="server" Field="" Name="Assigned User" TopItemText="" IsRequired="false" />
                            <uc:Combo ID="cboRecommendedUser" runat="server" Field="" Name="Recommended User" TopItemText="" IsRequired="false" />
                            <uc:Combo ID="cboConsultant" runat="server" Field="" Name="Surveys Completed By" TopItemText="" IsRequired="false" />
                            <uc:Combo ID="cboReviewer" runat="server" Field="" Name="Reviews Completed By" TopItemText="" IsRequired="false" />
                            <uc:Combo ID="cboCreatedByStaff" runat="server" Field="" Name="Created By LC Staff" TopItemText="" IsRequired="false" />
                            <uc:Combo ID="cboCreatedByNonStaff" runat="server" Field="" Name="Created By Non-LC Staff" TopItemText="" IsRequired="false" />
                            <% } %>

                            <uc:Combo ID="cboStatus" runat="server" Field="Survey.StatusCode" TopItemText="" IsRequired="false"
                                DataType="SurveyStatus" DataValueField="Code" DataTextField="Name" DataOrder="DisplayOrder ASC" />

                            <% if (CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID)
                               { %>
                            <uc:Combo ID="cboProfitCenterByUser" runat="server" Field="ProfitCenter.Code" Name="Profit Center By User" TopItemText="" IsRequired="false" />
                            <% } %>

                            <uc:Combo ID="cboUnderwritingAccepted" runat="server" Field="" Name="Underwriting Accepted" TopItemText="" IsRequired="false" />
                            <uc:Combo ID="cboCorrection" runat="server" Field="Survey.Correction" Name="Returned For Correction" TopItemText="" IsRequired="false" />
                            <uc:Combo ID="cboOpenRecs" runat="server" Field="Location.IsActive" Name="Open Recs" TopItemText="" IsRequired="false" />

                            <uc:Combo ID="cboCriticalRecs" onChange="ToggleOpenCriticalRecsDisplay(this.value);" runat="server" Field="Survey.ContainsCriticalRec" Name="Critical Recs" TopItemText="" IsRequired="false" NoEndTR="true" />
                            <% if (_displayRecStatusFilter)
                               { %>
                            <span id="divOpenCriticalRecs" style="visibility: hidden;" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;Open Only
				<asp:CheckBox ID="chkOpenCriticalRecs" CssClass="chk" runat="server"></asp:CheckBox>
                            </span>
                            <% } %>
                            <uc:Combo ID="cboQA" runat="server" Field="Survey.QualityID" Name="Quality Assessment" TopItemText="" IsRequired="false" />
                            <uc:DateRange ID="drLastModified" runat="server" Label="Last Modified" IsRequired="false" />
                            <uc:DateRange ID="drCreatedOn" runat="server" Label="Created On" IsRequired="false" />
                            <uc:DateRange ID="drDueDate" runat="server" Label="Due Date" IsRequired="false" />
                            <uc:DateRange ID="drAssignDate" runat="server" Label="Assigned Date" IsRequired="false" />
                            <uc:DateRange ID="drAcceptDate" runat="server" Label="Accepted Date" IsRequired="false" />
                            <uc:DateRange ID="drUnderwritingAcceptDate" runat="server" Label="Underwriting Accepted Date" IsRequired="false" />
                            <uc:DateRange ID="drDateSurveyed" runat="server" Label="Date Surveyed" IsRequired="false" />
                            <uc:DateRange ID="drCompletedReportDate" runat="server" Label="Report Submitted Date" IsRequired="false" />
                            <uc:DateRange ID="drCanceledDate" runat="server" Label="Canceled Date" IsRequired="false" />
                            <uc:DateRange ID="drCompletedDate" runat="server" Label="Closed Date" IsRequired="false" />
                            <uc:DateRange ID="drReviewerCompletedDate" runat="server" Label="Reviewer Completed Date" IsRequired="false" />
                            <uc:Combo ID="cboLettersSent" runat="server" Field="LocationContact.Name" Name="Survey Letter Sent" TopItemText="" IsRequired="false" Visible="true" />
                            <uc:Combo ID="cboReleaseOwnershipReason" runat="server" Field="" Name="Release Ownership Reason" TopItemText="" IsRequired="false" />
                        </table>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%= AppPath %>/images/black_pixel.gif" height="375px" width="1px">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td valign="top">
                        <table class="fields">
                            <cc:Group ID="grpInsured" runat="server" Label="Insured Criteria" />
                            <uc:Text ID="txtClientID" runat="server" Field="Insured.ClientID" IsRequired="false" Columns="20" />
                            <% if (CurrentUser.Company.SupportsPortfolioID)
                               {%>
                            <uc:Text ID="txtPortfolioID" runat="server" Field="Insured.PortfolioID" IsRequired="false" Columns="20" />
                            <% } %>
                            <uc:Text ID="txtPolicyNumber" runat="server" Field="Policy.Number" Name="Policy Number" IsRequired="false" Columns="20" />
                            <uc:Text ID="txtInsuredName" runat="server" Field="Insured.Name" Name="Insured Name" IsRequired="false" Columns="30" />
                            <uc:Text ID="txtInsuredDBA" runat="server" Field="Insured.Name2" Name="Insured DBA" IsRequired="false" Columns="30" />
                            <uc:Text ID="txtInsuredCity" runat="server" Field="Insured.City" Name="Insured City" IsRequired="false" />
                            <uc:Combo ID="cboInsuredState" runat="server" Field="Insured.StateCode" Name="Insured State" TopItemText="" IsRequired="false"
                                DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" />
                            <uc:Text ID="txtInsuredZipCode" runat="server" Field="Insured.ZipCode" Name="Insured Zip Code" IsRequired="false" />

                            <% if (CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID)
                               { %>
                            <uc:DateRange ID="drNonRenewedDate" runat="server" Label="Insured Non-Renewed" IsRequired="false" />
                            <% } %>

                            <uc:Text ID="txtBusinessOperations" runat="server" Field="Insured.BusinessOperations" IsRequired="false" Columns="50" Rows="1" />

                            <% if (CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID)
                               { %>
                            <uc:Combo ID="cboProfitCenterByRegion" runat="server" Field="Policy.ProfitCenter" Name="Profit Center By Region" TopItemText="" IsRequired="false" />
                            <% } %>

                            <uc:ComboSearch ID="cboSIC" runat="server" Field="Insured.SICCode" Name="SIC" TopItemText="" IsRequired="false" />

                            <% if (CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID)
                               { %>
                            <uc:Combo ID="cboUnderwriter" runat="server" Field="Insured.Underwriter" Name="Underwriter" TopItemText="" IsRequired="false" />
                            <% } %>

                            <uc:Text ID="txtLocationCity" runat="server" Field="Location.City" Name="Location City" IsRequired="false" />
                            <uc:Combo ID="cboLocationState" runat="server" Field="Location.StateCode" Name="Location State" TopItemText="" IsRequired="false"
                                DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" />
                            <uc:Text ID="txtLocationZipCode" runat="server" Field="Location.ZipCode" Name="Location Zip Code" IsRequired="false" />
                            <uc:NumberRange ID="numTotalPremium" runat="server" Label="Survey Policy Premium" IsRequired="false" />

                            <% if (CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID)
                               { %>
                            <uc:DateRange ID="drPolicyEffectiveDate" runat="server" Label="Policy Effective Date" IsRequired="false" />
                            <uc:DateRange ID="drPolicyExpireDate" runat="server" Label="Policy Expiration Date" IsRequired="false" />
                            <% } %>

                            <% if (CurrentUser.Company.ID == Company.BerkleyMidAtlanticGroup.ID)
                               { %>
                            <uc:Combo ID="cboPACEAccount" runat="server" Field="Survey.NonProductive" Name="PACE Account" TopItemText="" IsRequired="false" />
                            <uc:Combo ID="cboKeyAccount" runat="server" Field="Survey.Review" Name="Key Account" TopItemText="" IsRequired="false" />
                            <uc:Combo ID="cboLossSensitive" runat="server" Field="Survey.FutureVisitNeeded" Name="Loss Sensitive" TopItemText="" IsRequired="false" />
                            <% } %>

                            <cc:Group ID="grpAgency" runat="server" Label="Agency Criteria" />
                            <uc:Text ID="txtAgencyNumber" runat="server" Field="Agency.Number" IsRequired="false" />
                            <uc:Text ID="txtAgencyName" runat="server" Field="Agency.Name" IsRequired="false" Columns="50" />
                            <uc:Combo ID="cboAgencyState" runat="server" Field="Agency.StateCode" Name="Agency State" TopItemText="" IsRequired="false"
                                DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" />
                            <tr>
                                <td>&nbsp;</td>
                                <td class="buttons">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="btn" Text="Search" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnLucky" runat="server" CssClass="btn" Text="First Survey Found" ToolTip="Redirects you to the first survey found in the search results." />
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
        </cc:Box>

        <uc:HintFooter ID="ucHintFooter" runat="server" />
        <uc:Footer ID="ucFooter" runat="server" />

        <div style="display: none;">
            <img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" alt="" />
        </div>
        <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery.blockUI.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $.blockUI.defaults.css = {};
                $.blockUI.defaults.overlayCSS.opacity = .2;

                $('#frm').submit(function () {
                    if (Page_IsValid) {
                        $.blockUI({ message: '<h3><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" />&nbsp;&nbsp;&nbsp;Processing, one moment please...</h3>' });
                    }
                });
            });


            function ToggleOpenCriticalRecsDisplay(criticalrecs) {
                if (document.getElementById('divOpenCriticalRecs') != null) {
                    if (criticalrecs == '1') {
                        document.getElementById('divOpenCriticalRecs').style.visibility = 'visible';
                        document.getElementById('chkOpenCriticalRecs').checked = false;
                    }
                    else {
                        document.getElementById('divOpenCriticalRecs').style.visibility = 'hidden';
                    }
                }
            }
        </script>

    </form>
</body>
</html>
