using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using QCI.Web;
using Berkley.BLC.Entities;

public partial class AdminHelpfulHintAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminHelpfulHintAspx_Load);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.cboWebPage.SelectedIndexChanged += new EventHandler(cboWebPage_SelectedIndexChanged);
    }
    #endregion

    void AdminHelpfulHintAspx_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            WebPageHelpfulHint[] pages = WebPageHelpfulHint.GetSortedArray("CompanyID = ?", "WebPageDescription ASC", CurrentUser.CompanyID);
            ComboHelper.BindCombo(cboWebPage, pages, "WebPagePath", "WebPageDescription");

            cboWebPage_SelectedIndexChanged(null, null);
        }
    }

    void cboWebPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        // populate the fields
        WebPageHelpfulHint page = WebPageHelpfulHint.Get(cboWebPage.SelectedValue, CurrentUser.CompanyID);
        txtHelpfulHintText.Text = page.HelpfulHintText;
        chkAlwaysVisible.Checked = page.AlwaysVisible;
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            WebPageHelpfulHint page = WebPageHelpfulHint.Get(cboWebPage.SelectedValue, CurrentUser.CompanyID).GetWritableInstance();

            string result = (txtHelpfulHintText.Text == "\r\n") ? string.Empty : txtHelpfulHintText.Text;
            page.HelpfulHintText = result;
            page.AlwaysVisible = chkAlwaysVisible.Checked;

            // commit changes
            page.Save();
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect("Administration.aspx", true);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Administration.aspx", true);
    }
}
