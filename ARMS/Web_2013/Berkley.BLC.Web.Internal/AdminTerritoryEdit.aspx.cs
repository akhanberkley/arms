using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web;
using QCI.Web.Validation;

public partial class AdminTerritoryEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminTerritoryEditAspx_Load);
        this.PreRender += new EventHandler(AdminTerritoryEditAspx_PreRender);
        this.btnAddUser.Click += new EventHandler(btnAddUser_Click);
        this.repUsers.ItemCommand += new RepeaterCommandEventHandler(repUsers_ItemCommand);
        this.repUsers.ItemDataBound += new RepeaterItemEventHandler(repUsers_ItemDataBound);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected Territory _terra;
    protected string _typeCode;

    void AdminTerritoryEditAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("territoryid", false);
        _terra = (id != Guid.Empty) ? Territory.Get(id) : null;
        if (_terra == null)
        {
            _typeCode = ARMSUtility.GetStringFromQueryString("type", true);
        }
        else
        {
            _typeCode = _terra.SurveyStatusTypeCode;
        }

        // update the header
        string typeName;
        switch (_typeCode)
        {
            case "S": typeName = "Consultant"; break;
            case "R": typeName = "Review"; break;
            default:
                {
                    throw new NotSupportedException("Code '" + _typeCode + "' was not expected.");
                }
        }
        this.PageHeading = string.Format("{0} {1} Territory", (_terra == null) ? "Add" : "Edit", typeName);

        if (!this.IsPostBack)
        {
            if (_terra != null) // edit
            {
                this.PopulateFields(_terra);

                // build the user list and push it into our viewstate property
                UserTerritory[] users = UserTerritory.GetArray("TerritoryID = ?", _terra.ID);
                UserAllocation[] list = new UserAllocation[users.Length];
                for (int i = 0; i < users.Length; i++)
                {
                    UserTerritory user = users[i];
                    list[i] = new UserAllocation(user.UserID, user.User.Name, user.PercentAllocated.ToString(), user.MinPremiumAmount.ToString(), user.MaxPremiumAmount.ToString());
                }
                this.UserList = list;

                //check the first user's value to determine which allocation to check
                if (UserList.Length > 0)
                {
                    if (UserList[0].MaxPremiumAmountValue > decimal.MinValue)
                    {
                        rdoPremium.Checked = true;
                        rdoPercent.Checked = false;
                    }
                    else
                    {
                        rdoPremium.Checked = false;
                        rdoPercent.Checked = true;
                    }
                }
                else
                {
                    rdoPremium.Checked = false;
                    rdoPercent.Checked = true;
                }

                chkInHouse.Visible = (Berkley.BLC.Business.Utility.GetCompanyParameter("UsesInHouseSelection", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
            }

            // load the user combo
            RefreshUserCombo();

            btnSubmit.Text = (_terra == null) ? "Add" : "Update";
        }
        else // postback
        {
            // extract out the allocation values from the repeater
            foreach (RepeaterItem item in repUsers.Items)
            {
                if (rdoPercent.Checked)
                {
                    TextBox txt = (item.FindControl("txtPercentage") as TextBox);
                    if (txt != null)
                    {
                        try
                        {
                            // get the user id from the cmd argument of the button
                            LinkButton btn = (item.FindControl("btnRemoveUser") as LinkButton);
                            if (btn == null) throw new Exception("Could not find LinkButton with ID 'btnRemoveUser' in repeater item.");
                            Guid userID = new Guid(btn.CommandArgument);

                            // find the user in our list and update the allocation
                            foreach (UserAllocation user in this.UserList)
                            {
                                if (user.UserID == userID)
                                {
                                    user.PercentAllocation = txt.Text.Trim();
                                }
                            }
                        }
                        catch (FieldValidationException ex)
                        {
                            this.ShowErrorMessage(ex);
                        }
                    }
                }
                else
                {
                    TextBox txtMin = (item.FindControl("txtMALMin") as TextBox);
                    TextBox txtMax = (item.FindControl("txtMALMax") as TextBox);
                    if (txtMin != null && txtMax != null)
                    {
                        try
                        {
                            // get the user id from the cmd argument of the button
                            LinkButton btn = (item.FindControl("btnRemoveUser") as LinkButton);
                            if (btn == null) throw new Exception("Could not find LinkButton with ID 'btnRemoveUser' in repeater item.");
                            Guid userID = new Guid(btn.CommandArgument);

                            // find the user in our list and update the allocation
                            foreach (UserAllocation user in this.UserList)
                            {
                                if (user.UserID == userID)
                                {
                                    user.MinPremiumAmount = txtMin.Text.Trim();
                                    user.MaxPremiumAmount = txtMax.Text.Trim();
                                }
                            }
                        }
                        catch (FieldValidationException ex)
                        {
                            this.ShowErrorMessage(ex);
                        }
                    }
                }
            }
        }
    }

    void AdminTerritoryEditAspx_PreRender(object sender, EventArgs e)
    {
        // rebind the user list every page hit
        RebindUserRepeater();
    }

    protected void btnRemoveUser_PreRender(object sender, System.EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        JavaScript.AddConfirmationNoticeToWebControl(btn, "Are you sure you want to remove this user from the territory?");
    }

    void btnAddUser_Click(object sender, EventArgs e)
    {
        try
        {
            string idString = cboUsers.SelectedValue;
            if (idString == null || idString.Length == 0)
            {
                throw new FieldValidationException(cboUsers, "A user must be selected.");
            }

            Berkley.BLC.Entities.User user = Berkley.BLC.Entities.User.Get(new Guid(idString));

            // get the old/current user list and determine allocation left
            UserAllocation[] oldList = this.UserList;
            int percentLeft = 100;
            foreach (UserAllocation item in oldList)
            {
                int value = item.PercentAllocationValue;
                if (value != int.MinValue)
                {
                    percentLeft -= value;
                }
            }
            if (percentLeft < 0)
            {
                percentLeft = 0;
            }

            // build a new user list array with the new user added
            UserAllocation[] newList = new UserAllocation[oldList.Length + 1];
            oldList.CopyTo(newList, 1);
            newList[0] = new UserAllocation(user.ID, user.Name, percentLeft.ToString(), decimal.Zero.ToString(), decimal.Zero.ToString());

            // update our viewstate property
            this.UserList = newList;

            // refresh the user combo
            RefreshUserCombo();
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
    }

    void repUsers_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            Guid targetID = new Guid(e.CommandArgument.ToString()); //todo: native guid?

            // remove the selected user from our list
            ArrayList newList = new ArrayList(this.UserList);
            for (int i = 0; i < newList.Count; i++)
            {
                UserAllocation user = (UserAllocation)newList[i];
                if (user.UserID == targetID)
                {
                    newList.RemoveAt(i);
                }
            }

            // update our viewstate property
            this.UserList = (UserAllocation[])newList.ToArray(typeof(UserAllocation));

            // refresh the user combo
            RefreshUserCombo();
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        bool isAdd = (_terra == null);
        try
        {
            Territory terra;
            if (isAdd) // add
            {
                terra = new Territory(Guid.NewGuid());
                terra.SurveyStatusTypeCode = _typeCode;
                terra.CompanyID = this.CurrentUser.CompanyID;
            }
            else // edit
            {
                terra = _terra.GetWritableInstance();
            }

            this.PopulateEntity(terra);

            // build old/new user sets and verify
            UserTerritory[] oldUsers = UserTerritory.GetArray("TerritoryID = ?", terra.ID);
            ArrayList newUsers = new ArrayList();
            int totalAllocation = 0;
            foreach (UserAllocation userAlloc in this.UserList)
            {
                int alloc = userAlloc.PercentAllocationValue;
                UserTerritory user = new UserTerritory(userAlloc.UserID, terra.ID);

                // verify: percentage is a valid integer value
                if (rdoPercent.Checked)
                {
                    if (alloc == int.MinValue)
                        throw new FieldValidationException("All user allocation amounts must be integer values and cannot be blank.");

                    user.PercentAllocated = alloc;
                    user.MinPremiumAmount = decimal.MinValue;
                    user.MaxPremiumAmount = decimal.MinValue;
                }

                // verify: min/max value is a valid decimal value
                if (rdoPremium.Checked)
                {
                    //validations
                    if (userAlloc.MinPremiumAmountValue == decimal.MinValue || userAlloc.MaxPremiumAmountValue == decimal.MaxValue)
                        throw new FieldValidationException("All user allocation amounts must be integer values and cannot be blank.");

                    if (userAlloc.MinPremiumAmountValue < 0)
                        throw new FieldValidationException(string.Format("Minimum amount for {0} must be greater than or equal to 0.", userAlloc.Name));

                    if (userAlloc.MaxPremiumAmountValue < 0)
                        throw new FieldValidationException(string.Format("Maximum amount for {0} must be less than 1000000000.", userAlloc.Name));

                    if (userAlloc.MinPremiumAmountValue > userAlloc.MaxPremiumAmountValue)
                        throw new FieldValidationException(string.Format("Maximum amount for {0} must be greater than minimum.", userAlloc.Name));

                    user.PercentAllocated = int.MinValue;
                    user.MinPremiumAmount = Math.Round(userAlloc.MinPremiumAmountValue, 2);
                    user.MaxPremiumAmount = Math.Round(userAlloc.MaxPremiumAmountValue, 2);

                    //make comparsons with other premium values
                    foreach (UserAllocation compareAlloc in this.UserList)
                    {
                        //only compare different users
                        if (userAlloc.UserID != compareAlloc.UserID)
                        {
                            if (userAlloc.MinPremiumAmountValue >= compareAlloc.MinPremiumAmountValue && userAlloc.MinPremiumAmountValue <= compareAlloc.MaxPremiumAmountValue)
                                throw new FieldValidationException(string.Format("Minimum amount for {0} cannot be within or equal the amounts of other users for this territory.", userAlloc.Name));

                            if (userAlloc.MaxPremiumAmountValue <= compareAlloc.MaxPremiumAmountValue && userAlloc.MaxPremiumAmountValue >= compareAlloc.MinPremiumAmountValue)
                                throw new FieldValidationException(string.Format("Maximum amount for {0} cannot be within or equal the amounts of other users for this territory.", userAlloc.Name));
                        }
                    }
                }

                newUsers.Add(user);
                totalAllocation += alloc;
            }

            // verify: total allocation is 100% (unless no one is assigned)
            if (newUsers.Count > 0 && totalAllocation != 100)
            {
                throw new FieldValidationException("Total allocation for users must add to 100%");
            }

            // if we get here, validation was successful.

            // commit all changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                terra.Save(trans);

                // refresh user assignments
                foreach (UserTerritory user in oldUsers)
                {
                    user.Delete(trans);
                }
                foreach (UserTerritory user in newUsers)
                {
                    user.Save(trans);
                }

                trans.Commit();
            }

            // update our member var so redirect will work correctly
            _terra = terra;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        if (isAdd)
        {
            // send the user to the area edit page
            Response.Redirect("AdminTerritoryEditArea.aspx?territoryid=" + _terra.ID);
        }
        else
        {
            // return the user
            Response.Redirect(GetReturnUrl(), true);
        }
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        return "AdminTerritories.aspx?type=" + _typeCode;
    }


    private void RebindUserRepeater()
    {
        // get and sort the users in the list
        UserAllocation[] userList = this.UserList;
        Array.Sort(userList);

        // bind the repeater
        repUsers.DataSource = userList;
        repUsers.DataBind();
    }

    private void RefreshUserCombo()
    {
        // get all available users based on type
        Berkley.BLC.Entities.User[] users;

        //filter
        StringBuilder sb = new StringBuilder();
        sb.Append("AccountDisabled = false && ");
        sb.Append("(Role[PermissionSet.SurveyActionPermissions[");
        sb.Append("(Setting = ? || Setting = ? || Setting = ? || Setting = ?) && ");
        sb.Append("SurveyAction.SurveyStatusCode = ?]]) ");

        //params
        ArrayList args = new ArrayList();
        args.Add(PermissionSet.SETTING_OWNER);
        args.Add(PermissionSet.SETTING_OWNER_AND_OTHERS);
        args.Add(PermissionSet.SETTING_OWNER_AND_VENDORS);
        args.Add(PermissionSet.SETTING_ALL);

        switch (_typeCode)
        {
            case "S":
                {
                    args.Add(SurveyStatus.AssignedSurvey.Code);
                    sb.Append("|| (IsFeeCompany = true && CompanyID = ?)");
                    args.Add(CurrentUser.CompanyID);

                    users = this.CurrentUser.Company.GetConsultants(sb.ToString(), args.ToArray());
                    break;
                }
            case "R":
                {
                    args.Add(SurveyStatus.AssignedReview.Code);

                    users = this.CurrentUser.Company.GetReviewStaff(sb.ToString(), args.ToArray());
                    break;
                }
            default:
                {
                    throw new NotSupportedException("Code '" + _typeCode + "' was not expected.");
                }
        }

        // build a "clean" list of users by excluding all users already assigned to this territory
        ArrayList cleanList = new ArrayList(users.Length);
        UserAllocation[] allocList = this.UserList;
        for (int i = 0; i < users.Length; i++)
        {
            Berkley.BLC.Entities.User user = users[i];
            bool found = false;
            for (int j = 0; j < allocList.Length; j++)
            {
                if (user.ID == allocList[j].UserID)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                cleanList.Add(user);
            }
        }

        // bind the clean list to our combo
        ComboHelper.BindCombo(cboUsers, cleanList, "ID", "Name");
        cboUsers.Items.Insert(0, new ListItem("-- select user --", ""));
    }

    void repUsers_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // we only care about repeater items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //determine which allocations types we want to display
            HtmlContainerControl div;

            if (rdoPercent.Checked)
                div = (HtmlContainerControl)e.Item.FindControl("divPremium");
            else if (rdoPremium.Checked)
                div = (HtmlContainerControl)e.Item.FindControl("divPercentage");
            else
                throw new NotSupportedException("Either the percentage or premium limits allocation must be selected.");

            if (div != null)
                div.Attributes.Add("style", "display:none");
        }
    }


    private UserAllocation[] UserList
    {
        get
        {
            object value = this.ViewState["UserList"];
            return (value != null) ? (UserAllocation[])value : new UserAllocation[0];
        }
        set
        {
            this.ViewState["UserList"] = value;
        }
    }

    [Serializable()]
    protected class UserAllocation : IComparable
    {
        private Guid _userID;
        private string _name;
        private string _percentAllocation;
        private string _minPremiumAmount;
        private string _maxPremiumAmount;

        public UserAllocation(Guid userID, string name, string percentAllocation, string minPremiumAmount, string maxPremiumAmount)
        {
            _userID = userID;
            _name = name;
            _percentAllocation = percentAllocation;
            _minPremiumAmount = minPremiumAmount;
            _maxPremiumAmount = maxPremiumAmount;
        }

        public Guid UserID
        {
            get { return _userID; }
        }

        public string Name
        {
            get { return _name; }
        }

        public string PercentAllocation
        {
            get { return _percentAllocation; }
            set { _percentAllocation = value; }
        }

        public int PercentAllocationValue
        {
            get
            {
                if (_percentAllocation == null || _percentAllocation.Length == 0)
                {
                    return int.MinValue;
                }
                try
                {
                    return int.Parse(_percentAllocation);
                }
                catch
                {
                    return int.MinValue;
                }
            }
        }

        public string MinPremiumAmount
        {
            get { return _minPremiumAmount; }
            set { _minPremiumAmount = value; }
        }

        public decimal MinPremiumAmountValue
        {
            get
            {
                if (_minPremiumAmount == null || _minPremiumAmount.Length == 0)
                {
                    return decimal.MinValue;
                }
                try
                {
                    return decimal.Parse(_minPremiumAmount);
                }
                catch
                {
                    return decimal.MinValue;
                }
            }
        }

        public string MaxPremiumAmount
        {
            get { return _maxPremiumAmount; }
            set { _maxPremiumAmount = value; }
        }

        public decimal MaxPremiumAmountValue
        {
            get
            {
                if (_maxPremiumAmount == null || _maxPremiumAmount.Length == 0)
                {
                    return decimal.MinValue;
                }
                try
                {
                    return decimal.Parse(_maxPremiumAmount);
                }
                catch
                {
                    return decimal.MinValue;
                }
            }
        }

        #region IComparable Members

        public int CompareTo(object value)
        {
            if (value == null)
            {
                return 1;
            }
            if (!(value is UserAllocation))
            {
                throw new ArgumentException("Value must be of type UserAllocation.", "value");
            }
            return string.Compare(this.Name, (value as UserAllocation).Name);
        }

        #endregion
    }

}
