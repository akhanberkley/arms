using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using Wilson.ORMapper;

public partial class ServicePlanActivitiesAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(ServicePlanActivitiesAspx_Load);
        this.PreRender += new EventHandler(ServicePlanActivitiesAspx_PreRender);
        this.grdActivities.ItemCommand += new DataGridCommandEventHandler(grdActivities_ItemCommand);
        this.grdActivities.SortCommand += new DataGridSortCommandEventHandler(grdActivities_SortCommand);
        this.grdActivities.PageIndexChanged += new DataGridPageChangedEventHandler(grdActivities_PageIndexChanged);
    }
    #endregion

    protected ServicePlan _eServicePlan;

    void ServicePlanActivitiesAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("planid", true);
        _eServicePlan = ServicePlan.Get(id);

        // restore sort expression, reverse state, and page index settings
        this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", grdActivities.Columns[0].SortExpression);
        this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", false);
        grdActivities.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);
    }

    void ServicePlanActivitiesAspx_PreRender(object sender, EventArgs e)
    {
        string sortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sortExp = sortExp.Replace(" DESC", " TEMP");
            sortExp = sortExp.Replace(" ASC", " DESC");
            sortExp = sortExp.Replace(" TEMP", " ASC");
        }
        
        // get the activities for this service plan
        ServicePlanActivity[] eActivities = ServicePlanActivity.GetSortedArray("ServicePlanID = ?", sortExp, _eServicePlan.ID);
        DataGridHelper.BindGrid(grdActivities, eActivities, "ID", "There are no activity times for this service plan.");
    }

    protected void btnRemoveActivity_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this activity?");
    }

    void grdActivities_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            // get the ID of the territory selected
            Guid id = (Guid)grdActivities.DataKeys[e.Item.ItemIndex];

            // remove the objective
            ServicePlanActivity eActivity = ServicePlanActivity.Get(id);
            if (eActivity != null)
            {
                eActivity.Delete();

                //Update the service plan history
                ServicePlanManager manager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
                manager.ActivityManagement(eActivity, ServicePlanManager.UIAction.Remove);
            }
        }
    }

    void grdActivities_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdActivities.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);
    }

    void grdActivities_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sortRev);
    }
}
