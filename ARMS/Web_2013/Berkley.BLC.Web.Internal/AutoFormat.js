var _autoFormatInitalized = false;
var _pendingAutoFormatField = null;

function StartAutoFormat(txt, formatType)
{
	if (!_autoFormatInitalized)
	{
		// add our format update to the front of the ClientValidate so it runs before the validators check the fields
		if (typeof(Page_ClientValidate) == "function")
		{
			Page_ClientValidate = AutoFormat_UpdateFunction(Page_ClientValidate, "AutoFormat_UpdateField(null)");
		}
		_autoFormatInitalized = true;
	}
	
	if (!txt.autoFormatOn)
	{
		txt.autoFormatType = formatType.toLowerCase();
		AutoFormat_AddToEvent(txt, "onchange", "AutoFormat_UpdateField(this)");
		AutoFormat_AddToEvent(txt, "onblur", "AutoFormat_UpdateField(this)");
		txt.autoFormatOn = true;
	}
	
	txt.autoFormatNeeded = true;
	_pendingAutoFormatField = txt;
}

function AutoFormat_UpdateField(txt)
{
	if (txt == null)
	{
		txt = _pendingAutoFormatField;
	}
	if (txt == null || !txt.autoFormatNeeded)
	{
		return;	
	}
	
	var value = txt.value;
	if (value.length > 0)
	{
		var result;
		switch (txt.autoFormatType)
		{
			case "date" : result = AutoFormat_FormatDate(value); break;
			case "phonenumber": result = AutoFormat_FormatPhone(value); break;
			case "ssn": result = AutoFormat_FormatSSN(value); break;
			case "zipcode": result = AutoFormat_FormatZipCode(value); break;
			default: result = value; break;
		}

		txt.value = result;
	}

	AutoFormat_UpdateValidators(txt);

	txt.autoFormatNeeded = false;
	_pendingAutoFormatField = null;
}

function AutoFormat_UpdateValidators(control)
{
    if (typeof(Page_ValidationActive) != "undefined" && Page_ValidationActive)
    {
		var vals = control.Validators;
		if (!vals) return;
	    
		for (var i = 0; i < vals.length; i++)
		{
			ValidatorValidate(vals[i]);
		}
	    
		ValidatorUpdateIsValid();
    }
}

function AutoFormat_FormatDate(value)
{
	// trim and replace standard delimiters with slashes
	var result = Trim(value);
	result = result.replace(/\./g, "/");
	result = result.replace(/\-/g, "/");
	result = result.replace(/\s/g, "/");
	
	// add seperators if the value is exactly 6 or 8 digits
	result = result.replace(/^(\d{2})(\d{2})(\d{2}|\d{4})$/g, "$1/$2/$3");

	// add the current year if none provided, or make year 4 digits	
	var values = result.split("/");
	if (values.length == 2)
	{
		var year = new Date().getYear() % 100;
		year += (year < 38) ? 2000 : 1900;
		result = values[0] + "/" + values[1] + "/" + year;
	}
	if (values.length == 3)
	{
		var year = parseInt(values[2], 10);
		if (!isNaN(year) && year < 100)
		{
			year += (year < 30) ? 2000 : 1900;
			result = values[0] + "/" + values[1] + "/" + year;
		}
	}

	return result;
}

function AutoFormat_FormatPhone(value)
{
	// strip non-digits from string
	value = Trim(value);
	value = value.replace(/\D/g, "");

	// build the new string char-by-char, adding the symbols
	var result = "";
	for (var i = 0; i < value.length; i++)
	{
		if (i == 3) result += "-";
		if (i == 6) result += "-";
		if (i == 10) result += " x";
		result += value.charAt(i);
	}

	return result;
}

function AutoFormat_FormatSSN(value)
{
	// strip non-digits from string
	value = Trim(value);
	value = value.replace(/\D/g, "");

	// build the new string char-by-char, adding the symbols
	var result = "";
	for (var i = 0; i < value.length; i++)
	{
		if (i == 3) result += "-";
		if (i == 5) result += "-";
		result += value.charAt(i);
	}
	
	return result;	
}

function AutoFormat_FormatZipCode(value)
{
	// strip non-digits from string
	value = Trim(value);
	value = value.replace(/\D/g, "");

	// build the new string char-by-char, adding the symbols
	var result = "";
	for (var i = 0; i < value.length; i++)
	{
		if (i == 5) result += "-";
		result += value.charAt(i);
	}

	return result;
}


function AutoFormat_AddToEvent(elem, eventName, code)
{
	elem[eventName] = AutoFormat_UpdateFunction(elem[eventName], code, null);
}

function AutoFormat_UpdateFunction(func, codeBefore, codeAfter)
{
	var code = "";	
	if (typeof(func) == "function")
	{
		code = func.toString();
		var firstBrace = code.indexOf("{");
		var lastBrace = code.lastIndexOf("}");
		code = code.substring(firstBrace + 1, lastBrace);
	}
	
	if (codeBefore != null && codeBefore.length > 0)
	{
		if (codeBefore.substring(codeBefore.length - 1) != ";")
		{
			codeBefore += ";"
		}
		code = codeBefore + " " + code;
	}

	if (codeAfter != null && codeAfter.length > 0)
	{
		if (codeAfter.substring(codeAfter.length - 1) != ";")
		{
			codeAfter += ";"
		}
		code += " " + codeAfter;
	}

	return new Function(code);
}


function Trim(str)
{
	return (str != null) ? str.replace(/^\s*|\s*$/,"") : null;
}

function NumbersOnly(evt)
{
	if (!evt) evt = window.event;
	if (!evt) return;
	
	var code = (evt.charCode ? evt.charCode : (evt.which ? evt.which : evt.keyCode));
	if (code > 0x20 && (code < 0x30 || code > 0x39))
	{
		evt.returnValue = false;
		if (evt.preventDefault) evt.preventDefault();
	}
}
