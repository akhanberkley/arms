﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" ValidateRequest="false" CodeFile="MapOvi.aspx.cs" Inherits="MapOviAspx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">   
    <head>     
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />     
        <title><%= base.PageTitle %></title>  

        <style type="text/css"> 
            #map{
                height:650px;
                width: 800px;
                border: 1px solid black; 
            }
            .notice {
                color: red;
            }
       </style>

        <!--for Ovi map -->
        <script type='text/javascript'>

            var geocoder;
            var iRecCount = 0;
            var store = '';

            function getCoordinatesMapOvi(pAddress) {
                //debugger;

                var Cordinates;
                var result;

                geocoder = new ovi.mapsapi.search.Manager();

                geocoder.addObserver("state", function (manager, key, value)
                {
                    if (value == "finished" || value == "failed")
                    {
                        Cordinates = geocode_callback(manager.locations, value);

                        //alert('Cordinates : ' + Cordinates);
                        var latitude = (Cordinates != null && Cordinates != 'undefined')? Cordinates.latitude : null;
                        var longitude = (Cordinates != null && Cordinates != 'undefined')? Cordinates.longitude : null;

                        //return address with latitude and longitude
                        var addr = pAddress;
                        addr = addr.replace(/\,/g, ";");

                        result = latitude + ';' + longitude + ';' + addr;

                        if (store == '')
                            store = result;
                        else
                            store = store + '~' + result;

                        var tot = document.getElementById("hnTotalAddress").value;

                        if (iRecCount == tot)
                        {
                            DoPostBack('__EVENTARGUMENT', store)
                        }
                    }
                }
                );
               
                geocode(pAddress);
                
            }

            function geocode(address) {
                geocoder.geocode(address);
            }

            function geocode_callback(response, status) {
                debugger;
                if (status == "finished") {
                    iRecCount = iRecCount + 1;
                    if (response.length > 0) {
                        var address = response[0].address.value;
                        var coords = response[0].displayPosition;
                        return coords;
                    }
                }
                else {
                    iRecCount = iRecCount + 1;
                }
            }


            function __doPostBack(eventTarget, eventArgument) {
                //alert('eventArgument :' + eventArgument);
                if (!frm.onsubmit || (frm.onsubmit() != false)) {
                    frm.__EVENTTARGET.value = eventTarget;
                    frm.__EVENTARGUMENT.value = eventArgument;
                    frm.submit();
                }

            }

            function DoPostBack(obj, arguments) {
                
                __doPostBack(obj.id, arguments);
            }

        </script>

    </head> 

    <body style="margin:0px;">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server"> 

    <asp:HiddenField ID="hnCordinates"  runat="server"  Value ="" />
    <asp:HiddenField ID="hnTotalAddress"  runat="server"  Value =""  />
    <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
    <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />

    <table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="pageBody" width="100%" height="100%" valign="top">
    <!-- END PAGE HEADER -->
    
    <span class="heading"><%= base.PageHeading %></span>
              

    <table>
        <tr>
            <td rowspan="3" valign="top">
                <div id="map"></div>  
                <script src="http://api.maps.ovi.com/jsl.js" type="text/javascript" charset="utf-8"></script>      
            </td>

            <td>
                <asp:Repeater ID="repAssignedUsers" Runat="server">
                    <HeaderTemplate>
                        <table style="MARGIN: 5px 0px 0px 20px;">
                            <tr>
                                <td style="font-size:14px;">
                                    <b>Key:</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border: 1px solid black; background-color: white; " cellspacing="0" cellpadding="0" width="100%">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td valign="top" style="PADDING: 5px 0px 5px 10px; font-size:13px;"><%# HtmlEncode(Container.DataItem, "OrderIndex")%>)</td>
                            <td valign="top" style="PADDING: 5px 0px 5px 10px; font-size:13px;"><%# HtmlEncode(Container.DataItem, "UserName")%></td>
                            <td valign="top" style="PADDING: 5px 10px 5px 13px;">
                                <table style="border: 1px solid black; background-color:<%# HtmlEncode(Container.DataItem, "CSSColor")%>;">
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>			
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                        </td>
                            </tr>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>

        </tr>
        <tr>
            <td>
                <asp:Repeater ID="repCityCenterLocations" Runat="server">
                    <HeaderTemplate>
                        <table style="MARGIN: 5px 0px 0px 20px;">
                            <tr>
                                <td style="font-size:13px;">
                                    <b>Locations Mapped to City Center:</b><br />
                                    <span style="font-size:smaller" class="">Specified address was not mappable.</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid black; background-color: white; ">
                                    <table style="" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <th>&nbsp;&nbsp;&nbsp;</th>
                                            <th>Survey #</th>
                                            <th>Location #</th>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                        <div style="height: 170px; width: 100%; overflow: auto">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="PADDING: 4px 0px 3px 10px;"><div style="border: 1px solid black; background-color:<%# HtmlEncode(Container.DataItem, "Color")%>; height: 9px; width: 9px;">&nbsp;</div></td>
                            <td valign="top" style="PADDING: 3px 0px 3px 10px; font-size:11px;"><%# HtmlEncode(Container.DataItem, "SurveyNumber")%></td>
                            <td valign="top" style="PADDING: 3px 0px 3px 10px; font-size:11px;"><%# HtmlEncode(Container.DataItem, "LocationNumber")%></td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr bgcolor="#e8e8e8">
                            <td style="PADDING: 4px 0px 3px 10px;"><div style="border: 1px solid black; background-color:<%# HtmlEncode(Container.DataItem, "Color")%>; height: 9px; width: 9px;">&nbsp;</div></td>
                            <td valign="top" style="PADDING: 3px 0px 3px 10px; font-size:11px;"><%# HtmlEncode(Container.DataItem, "SurveyNumber")%></td>
                            <td valign="top" style="PADDING: 3px 0px 3px 10px; font-size:11px;"><%# HtmlEncode(Container.DataItem, "LocationNumber")%></td>
                        </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                                            </table>
                                        </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Repeater ID="repInvalidLocations" Runat="server">
                    <HeaderTemplate>
                        <table style="MARGIN: 5px 0px 0px 20px;">
                            <tr>
                                <td style="font-size:13px;">
                                    <b>Locations Not Mapped:</b><br />
                                    <span style="font-size:smaller" class="">Specified address was invalid.</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid black; background-color: white; ">
                                    <table style="" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <th>&nbsp;&nbsp;&nbsp;</th>
                                            <th>Survey #</th>
                                            <th>Location #</th>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                        <div style="height: 120px; width: 100%; overflow: auto">
                                            <table cellspacing="0" cellpadding="0" width="100%">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="PADDING: 4px 0px 3px 10px;"><div style="border: 1px solid black; background-color:<%# HtmlEncode(Container.DataItem, "Color")%>; height: 9px; width: 9px;">&nbsp;</div></td>
                            <td valign="top" style="PADDING: 3px 0px 3px 10px; font-size:11px;"><%# HtmlEncode(Container.DataItem, "SurveyNumber")%></td>
                            <td valign="top" style="PADDING: 3px 0px 3px 10px; font-size:11px;"><%# HtmlEncode(Container.DataItem, "LocationNumber")%></td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr bgcolor="#e8e8e8">
                            <td style="PADDING: 4px 0px 3px 10px;"><div style="border: 1px solid black; background-color:<%# HtmlEncode(Container.DataItem, "Color")%>; height: 9px; width: 9px;">&nbsp;</div></td>
                            <td valign="top" style="PADDING: 3px 0px 3px 10px; font-size:11px;"><%# HtmlEncode(Container.DataItem, "SurveyNumber")%></td>
                            <td valign="top" style="PADDING: 3px 0px 3px 10px; font-size:11px;"><%# HtmlEncode(Container.DataItem, "LocationNumber")%></td>
                        </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                                            </table>
                                        </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>  

    <uc:Footer ID="ucFooter" runat="server" />              

    </form>

    </body> 

</html> 
