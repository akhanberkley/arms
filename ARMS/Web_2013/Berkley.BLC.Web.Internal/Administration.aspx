<%@ Page Language="C#" AutoEventWireup="false" CodeFile="Administration.aspx.cs" Inherits="AdministrationAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxTask" runat="server" title="Select a Task">
        <ul>
        <% if( _permissions.CanAdminUserAccounts ) { %>
        <li><a href="AdminUsers.aspx">Manage User Accounts</a></li>
        <% } %>
        <% if( _permissions.CanAdminFeeCompanies ) { %>
        <li><a href="AdminFeeCompanies.aspx">Manage Fee Companies</a></li>
        <% } %>
        <% if( _permissions.CanAdminUserRoles ) { %>
        <li><a href="AdminRoles.aspx">Manage User Roles</a></li>
        <% } %>
        <% if( _permissions.CanAdminRules ) { %>
        <li><a href="AdminAssignmentRules.aspx">Modify Assignment Rules</a></li>
        <% } %>
        <% if( _permissions.CanAdminResultsDisplay ) { %>
        <li><a href="AdminResultsDisplay.aspx">Modify Results Display</a></li>
        <% } %>
        <% if( _permissions.CanAdminRecommendations ) { %>
        <li><a href="AdminRecs.aspx">Manage Recommendations</a></li>
        <% } %>
        <% if( _permissions.CanAdminLetters ) { %>
        <li><a href="AdminLetters.aspx">Manage Letter Templates</a></li>
        <% } %>
        <% if( _permissions.CanAdminReports ) { %>
        <li><a href="AdminReports.aspx">Manage Report Templates</a></li>
        <% } %>
        <% if (_permissions.CanAdminTerritories) { %>
        <li><a href="AdminTerritories.aspx">Manage Territories</a></li>
        <% } %>
        <% if( _permissions.CanAdminDocuments ) { %>
        <li><a href="AdminDocuments.aspx">Manage Service Plan Document Templates</a></li>
        <% } %>
        <% if (_permissions.CanAdminServicePlanFields) { %>
        <li><a href="AdminServicePlanFields.aspx">Manage Service Plan Fields</a></li>
        <% } %>
        <% if( _permissions.CanAdminLinks ) { %>
        <li><a href="AdminLinks.aspx">Manage Links</a></li>
        <% } %>
        <% if( _permissions.CanAdminUnderwriters ) { %>
        <li><a href="AdminUnderwriters.aspx">Manage Underwriters</a></li>
        <% } %>
        <% if( _permissions.CanAdminHelpfulHints ) { %>
        <li><a href="AdminHelpfulHint.aspx">Manage Helpful Hints</a></li>
        <% } %>
        <% if( _permissions.CanAdminActivityTimes ) { %>
        <li><a href="AdminActivityTimes.aspx">Manage Administrative Times</a></li>
        <% } %>
        </ul>
    </cc:Box>
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
