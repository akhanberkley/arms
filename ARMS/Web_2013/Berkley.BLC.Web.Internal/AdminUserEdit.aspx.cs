using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web;
using QCI.Web.Validation;

public partial class AdminUserEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminUserEditAspx_Load);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected Berkley.BLC.Entities.User _user; // used during HTML rendering
    protected bool _isFeeCompany; // used during HTML rendering

    void AdminUserEditAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("id", false);
        _user = (id != Guid.Empty) ? Berkley.BLC.Entities.User.Get(id) : null;

        // this is a fee company if they are marked as one, or a new user with FA flag set
        _isFeeCompany = (_user != null) ? _user.IsFeeCompany : (Request.QueryString["fa"] == "true");

        if (!this.IsPostBack)
        {
            // populate the role combo
            UserRole[] roles = UserRole.GetSortedArray("CompanyID = ? && ID != Company.FeeCompanyUserRoleID && ID != Company.UnderwriterUserRoleID", "Name ASC", this.CurrentUser.CompanyID);
            ComboHelper.BindCombo(cboRoles, roles, "ID", "Name");
            cboRoles.Items.Insert(0, new ListItem("-- select role --", ""));

            //populate the company specific profit centers
            ProfitCenter[] eProfitCenters = ProfitCenter.GetSortedArray("CompanyID = ?", "Name ASC", CurrentUser.CompanyID);
            cboProfitCenter.DataBind(eProfitCenters, "Code", "Name");

            //populate the managers
            User[] managers = Berkley.BLC.Entities.User.GetSortedArray("CompanyID = ? && AccountDisabled = 0 && IsFeeCompany = 0 && IsUnderwriter = 0", "Name ASC", CurrentUser.CompanyID);
            cboManager.DataBind(managers, "ID", "Name");

            if (_user != null) // edit
            {
                this.PopulateFields(_user);

                // select the user's role
                ComboHelper.SelectComboItem(cboRoles, _user.RoleID.ToString());
                //cboRoles_SelectedIndexChanged(null, null);
            }
            else // add
            {
                txtDomainName.Text = this.CurrentUser.Company.PrimaryDomainName;

                if (_isFeeCompany)
                {
                    ComboHelper.SelectComboItem(cboRoles, this.CurrentUser.Company.FeeCompanyUserRoleID.ToString());
                    //cboRoles_SelectedIndexChanged(null, null);
                }
            }

            // configure the page
            this.PageHeading = (_user == null ? "Add " : "Edit ") + (!_isFeeCompany ? "User" : "Fee Company");

            boxUser.Title = (!_isFeeCompany) ? "User Information" : "Company Information";
            txtName.Name = (!_isFeeCompany) ? "Name" : "Company Name";
            txtDomainName.Visible = !_isFeeCompany;
            txtUsername.Visible = !_isFeeCompany;
            txtWorkPhone.Name = (!_isFeeCompany) ? "Work Phone" : "Phone Number";
            txtHomePhone.Visible = !_isFeeCompany;
            txtMobilePhone.Name = (!_isFeeCompany) ? "Mobile Number" : "Alternate Number";

            boxPermissions.Visible = !_isFeeCompany;

            btnSubmit.Text = (_user == null) ? "Add" : "Update";
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            Berkley.BLC.Entities.User user;
            if (_user == null) // add
            {
                user = new User(Guid.NewGuid());
                user.CompanyID = this.UserIdentity.CompanyID;
            }
            else // edit
            {
                user = _user.GetWritableInstance();
            }

            this.PopulateEntity(user);

            // default the timezone to EST
            user.TimeZoneCode = "US/Eastern";

            // auto-gen the domain and username if this is a new fee company
            if (_user == null && _isFeeCompany)
            {
                user.DomainName = this.CurrentUser.Company.PrimaryDomainName;
                string randomDigits = Guid.NewGuid().ToString().Substring(0, 8).ToUpper();
                user.Username = string.Format("FC_{0}", randomDigits);
            }

            // rule: domain + username must be unique across the entire database
            Berkley.BLC.Entities.User dupTest = Berkley.BLC.Entities.User.GetOne("DomainName = ? && Username = ? && ID != ?", user.DomainName, user.Username, user.ID);
            if (dupTest != null)
            {
                throw new FieldValidationException(txtUsername.InputControl, "The username entered is already being used by another user.  Please enter a different one.");
            }

            // rule: a role must be selected	
            string roleID = cboRoles.SelectedValue;
            if (roleID == null || roleID.Length == 0 && !_isFeeCompany)
            {
                throw new FieldValidationException(cboRoles, "A security role must be selected.");
            }

            // set the role; making sure the fee companies always get the defined Fee Company role
            if (!_isFeeCompany)
            {
                user.RoleID = new Guid(roleID);
            }
            else
            {
                user.RoleID = this.CurrentUser.Company.FeeCompanyUserRoleID;
                user.IsFeeCompany = true;
            }

            // commit all changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                user.Save(trans);
                trans.Commit();
            }

            // remove user from cache to force refresh
            PermissionSetCache.Clear(user);

            // update our member var so the redirect will work correctly
            _user = user;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl());
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        if (!_isFeeCompany) // company user
        {
            return (_user == null) ? "AdminUsers.aspx" : "AdminUser.aspx?id=" + _user.ID;
        }
        else // fee company
        {
            return (_user == null) ? "AdminFeeCompanies.aspx" : "AdminUser.aspx?id=" + _user.ID;
        }
    }

}
