<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminUnderwriters.aspx.cs" Inherits="AdminUnderwritersAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <asp:CheckBox ID="chkShowDisabled" Runat="server" CssClass="chk" AutoPostBack="True" Text="Show disabled underwriters." />
    <asp:datagrid ID="grdUnderwriters" Runat="server" CssClass="grid" AutoGenerateColumns="False" PageSize="25" AllowPaging="True" AllowSorting="True">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
             <asp:hyperlinkcolumn HeaderText="Name" DataTextField="Name" DataNavigateUrlField="Code"
		        DataNavigateUrlFormatString="AdminUnderwriterEdit.aspx?underwriterid={0}" SortExpression="Name ASC" />
		    <asp:BoundColumn HeaderText="Username" DataField="Username" SortExpression="Username ASC" />
		    <asp:BoundColumn HeaderText="Initials" DataField="Code" SortExpression="Code ASC" />
		    <asp:TemplateColumn HeaderText="Email" SortExpression="EmailAddress ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "EmailAddress", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
		    <asp:BoundColumn HeaderText="Phone" DataField="HtmlPhoneNumber" SortExpression="HtmlPhoneNumber ASC" />
		    <asp:TemplateColumn HeaderText="Ext" SortExpression="PhoneExt ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "PhoneExt", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn HeaderText="Title" DataField="Title" SortExpression="Title ASC" />
            <asp:TemplateColumn HeaderText="Profit Center" SortExpression="ProfitCenterCode ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "ProfitCenter.Name", null, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>

    <a href="AdminUnderwriterEdit.aspx">Add a New Underwriter</a>

    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
