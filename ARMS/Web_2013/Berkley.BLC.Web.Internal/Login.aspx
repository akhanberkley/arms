﻿<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" CodeFile="Login.aspx.cs" Inherits="Berkley.BLC.Web.Internal.LoginAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server"> 
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br>
    <br>
    <div style="width:500px" class="instructions"><p>Welcome to the Account Reporting Management System.  Please login below.</p></div>
    <br>
    
    <cc:Box id="boxLogin" runat="server" Title="Login Information">
        <table class="fields">
	    <uc:Combo id="cboCompany" runat="server" field="Company.Name" DataOrder="Name ASC" IsRequired="true" AutoPostback="true" 
		    DataType="Company" DataValueField="ID" DataTextField="Name" />
	    <uc:Combo id="cboUser" runat="server" field="" Name="User" IsRequired="true" />
	    <tr>
		    <td></td>
		    <td class="buttons">
			    <asp:Button ID="btnLogin" Runat="server" CssClass="btn" Text="Login" />
		    </td>
	    </tr>
	    </table>
	</cc:Box>

    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
