<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminResultsDisplay.aspx.cs" Inherits="AdminResultsDisplayAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><br /><br />
    
    Show: <asp:DropDownList ID="cboFilter" Runat="server" CssClass="cbo" AutoPostBack="True" />
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <%if (cboFilter.SelectedValue.Contains("S"))
      { %>
    <table>
        <tr>
            <td valign="top">
                <cc:Box id="boxDisplayedCols" runat="server" title="Currently Displayed Columns" width="285">
                <asp:datagrid ID="grdCols" Runat="server" CssClass="grid" AutoGenerateColumns="False">
	                <headerstyle CssClass="header" />
	                <itemstyle CssClass="item" />
	                <alternatingitemstyle CssClass="altItem" />
	                <footerstyle CssClass="footer" />
	                <pagerstyle CssClass="pager" Mode="NumericPages" />
	                <columns>
		                <asp:BoundColumn HeaderText="Order" DataField="DisplayOrder" />
		                <asp:TemplateColumn HeaderText="Column Name" HeaderStyle-Wrap="false">
			                <ItemStyle Wrap="False"></ItemStyle>
			                <ItemTemplate>
				                <%# HtmlEncode(Container.DataItem, "GridColumnFilter.Name").Replace("&lt;br&gt;", " ").Replace("&amp;nbsp;", " ")%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="Move">
			                <ItemStyle Wrap="False"></ItemStyle>
			                <ItemTemplate>
				                <asp:LinkButton ID="btnUp" Runat="server" CommandName="MoveUp">Up</asp:LinkButton> /
				                <asp:LinkButton ID="btnDown" Runat="server" CommandName="MoveDown">Down</asp:LinkButton>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="Remove">
			                <ItemTemplate>
				                <asp:LinkButton ID="btnDelete" Runat="server" CommandName="Remove">Remove</asp:LinkButton>
			                </ItemTemplate>
		                </asp:TemplateColumn>
	                </columns>
                </asp:datagrid>
                </cc:Box> 
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">
                <cc:Box id="boxAvailableCols" runat="server" title="Available Columns" width="160">
                <asp:datagrid ID="grdColsNotIncluded" Runat="server" CssClass="grid" AutoGenerateColumns="False">
	                <headerstyle CssClass="header" />
	                <itemstyle CssClass="item" />
	                <alternatingitemstyle CssClass="altItem" />
	                <footerstyle CssClass="footer" />
	                <pagerstyle CssClass="pager" Mode="NumericPages" />
	                <columns>
	                    <asp:BoundColumn HeaderText="Order" DataField="DisplayOrder" />
		                <asp:TemplateColumn HeaderText="Column Name" HeaderStyle-Wrap="false">
			                <ItemStyle Wrap="False"></ItemStyle>
			                <ItemTemplate>
				                <%# HtmlEncode(Container.DataItem, "GridColumnFilter.Name").Replace("&lt;br&gt;", " ").Replace("&amp;nbsp;", " ")%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="Move">
			                <ItemStyle Wrap="False"></ItemStyle>
			                <ItemTemplate>
				                <asp:LinkButton ID="btnAvailableUp" Runat="server" CommandName="MoveAvailableUp">Up</asp:LinkButton> /
				                <asp:LinkButton ID="btnAvailableDown" Runat="server" CommandName="MoveAvailableDown">Down</asp:LinkButton>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="Add">
			                <ItemTemplate>
				                <asp:LinkButton ID="btnAdd" Runat="server" CommandName="Add">Add</asp:LinkButton>
			                </ItemTemplate>
		                </asp:TemplateColumn>
	                </columns>
                </asp:datagrid>
                </cc:Box>   
            </td>
        </tr>
    </table>
    <% } else { %>
    <table>
        <tr>
            <td valign="top">
                <cc:Box id="boxDisplayedPlanCols" runat="server" title="Currently Displayed Columns" width="285">
                <asp:datagrid ID="grdPlanCols" Runat="server" CssClass="grid" AutoGenerateColumns="False">
	                <headerstyle CssClass="header" />
	                <itemstyle CssClass="item" />
	                <alternatingitemstyle CssClass="altItem" />
	                <footerstyle CssClass="footer" />
	                <pagerstyle CssClass="pager" Mode="NumericPages" />
	                <columns>
		                <asp:BoundColumn HeaderText="Order" DataField="DisplayOrder" />
		                <asp:TemplateColumn HeaderText="Column Name" HeaderStyle-Wrap="false">
			                <ItemStyle Wrap="False"></ItemStyle>
			                <ItemTemplate>
				                <%# HtmlEncode(Container.DataItem, "ServicePlanGridColumnFilter.Name").Replace("&lt;br&gt;", " ").Replace("&amp;nbsp;", " ")%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="Move">
			                <ItemStyle Wrap="False"></ItemStyle>
			                <ItemTemplate>
				                <asp:LinkButton ID="btnUp" Runat="server" CommandName="MoveUp">Up</asp:LinkButton> /
				                <asp:LinkButton ID="btnDown" Runat="server" CommandName="MoveDown">Down</asp:LinkButton>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="Remove">
			                <ItemTemplate>
				                <asp:LinkButton ID="btnDelete" Runat="server" CommandName="Remove">Remove</asp:LinkButton>
			                </ItemTemplate>
		                </asp:TemplateColumn>
	                </columns>
                </asp:datagrid>
                </cc:Box> 
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">
                <cc:Box id="boxAvailablePlanCols" runat="server" title="Available Columns" width="160">
                <asp:datagrid ID="grdPlanColsNotIncluded" Runat="server" CssClass="grid" AutoGenerateColumns="False">
	                <headerstyle CssClass="header" />
	                <itemstyle CssClass="item" />
	                <alternatingitemstyle CssClass="altItem" />
	                <footerstyle CssClass="footer" />
	                <pagerstyle CssClass="pager" Mode="NumericPages" />
	                <columns>
	                    <asp:BoundColumn HeaderText="Order" DataField="DisplayOrder" />
		                <asp:TemplateColumn HeaderText="Column Name" HeaderStyle-Wrap="false">
			                <ItemStyle Wrap="False"></ItemStyle>
			                <ItemTemplate>
				                <%# HtmlEncode(Container.DataItem, "ServicePlanGridColumnFilter.Name").Replace("&lt;br&gt;", " ").Replace("&amp;nbsp;", " ")%>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="Move">
			                <ItemStyle Wrap="False"></ItemStyle>
			                <ItemTemplate>
				                <asp:LinkButton ID="btnAvailableUp" Runat="server" CommandName="MoveAvailableUp">Up</asp:LinkButton> /
				                <asp:LinkButton ID="btnAvailableDown" Runat="server" CommandName="MoveAvailableDown">Down</asp:LinkButton>
			                </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn HeaderText="Add">
			                <ItemTemplate>
				                <asp:LinkButton ID="btnAdd" Runat="server" CommandName="Add">Add</asp:LinkButton>
			                </ItemTemplate>
		                </asp:TemplateColumn>
	                </columns>
                </asp:datagrid>
                </cc:Box>   
            </td>
        </tr>
    </table>
    <% } %>
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
