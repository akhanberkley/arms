<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" ValidateRequest="false" CodeFile="CreateSurvey.aspx.cs" Inherits="CreateSurveyAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintAlwaysVisibleFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Date" Src="~/Fields/DateField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="ComboSearch" Src="~/Fields/ComboFieldSearch.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >

<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title> 
</head>

<body style="margin:0px" onload="RemoveHrefs();">

    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css" />  

    <form id="frm" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/AjaxMethodsService.asmx" />
        </Services>
    </asp:ScriptManager>
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><br /><br />
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxInsured" runat="server" Title="Insured" width="600">
        <a id="lnkInsured" href="javascript:Toggle('divInsured')">Show/Hide</a><br />
        <div id="divInsured" style="MARGIN-TOP: 5px">
            <table class="fields">
                <uc:Text id="txtClientID" runat="server" field="Insured.ClientID" Columns="25" IsRequired="True" />
                <% if (CurrentUser.Company.SupportsPortfolioID) {%>
                <uc:Text id="txtPortfolioID" runat="server" field="Insured.PortfolioID" Columns="25" IsRequired="False" />
                <% } %>
                <uc:Text id="txtName" runat="server" field="Insured.Name" Columns="60" IsRequired="True" />
                <uc:Text id="txtName2" runat="server" field="Insured.Name2" Columns="60" />
                <uc:Text id="txtStreetLine1" runat="server" field="Insured.StreetLine1" Name="Mailing Address 1" Columns="70" IsRequired="True" />
                <uc:Text id="txtStreetLine2" runat="server" field="Insured.StreetLine2" Name="Mailing Address 2" Columns="70" />
                <uc:Text id="txtStreetLine3" runat="server" field="Insured.StreetLine3" Name="Mailing Address 3" Columns="70" />
                <uc:Text id="txtCity" runat="server" field="Insured.City" IsRequired="True" />
                <uc:Combo id="cboState" runat="server" field="Insured.StateCode" Name="State" topItemText="" isRequired="True" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" />
                <uc:Text id="txtZipCode" runat="server" field="Insured.ZipCode" IsRequired="True" />
                <uc:Text id="txtBusinessOperations" runat="server" field="Insured.BusinessOperations" Columns="75" Rows="1" Name="Business Ops." IsRequired="True" />
                <% if (CurrentUser.Company.SupportsBusinessCategory) {%>
                <uc:Text id="txtBusinessCategory" runat="server" field="Insured.BusinessCategory" Columns="15" Name="Business Category" IsRequired="False" />
                <% } %>
                <% if (CurrentUser.Company.SupportsDOTNumber) {%>
                <uc:Text id="txtDOTNumber" runat="server" field="Insured.DOTNumber" Name="DOT Number" IsRequired="False" />
                <% } %>
                <uc:Text id="txtCompanyWebsite" runat="server" field="Insured.CompanyWebsite" Columns="75" Rows="1" IsRequired="False" />
                <% if (!_hideSICCode) {%>
                <uc:ComboSearch id="cboSIC" runat="server" field="Insured.SICCode" Name="SIC" topItemText="" isRequired="True" />
                <% } %>
                <% if (CurrentUser.Company.SupportsNAICS) {%>
                <uc:ComboSearch id="cboNAIC" runat="server" field="Insured.NAICSCode" Name="NAICS" topItemText="" isRequired="True" />
                <% } %>
                <uc:Combo id="cboUnderwriter" runat="server" field="Survey.UnderwriterCode" Name="Underwriter" topItemText="" isRequired="True" IgnoreMapping="true" />
                <%if (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplaySecUnderwriter", CurrentUser.Company).ToUpper() == "TRUE"){%>
                <uc:Combo id="cboUnderwriter2" runat="server" field="Survey.Underwriter2Code" Name="Underwriter2" topItemText=""  IgnoreMapping="true" />
                <% } %>
                <%if (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplayContactOnInsured", CurrentUser.Company).ToUpper() == "TRUE") {%>
                <uc:Text ID="txtContactName" runat="server" Field="Insured.ContactName" Name="Primary Contact" IsRequired="false"  />
                <uc:Text ID="txtContactPhone" runat="server" Field="Insured.ContactPhoneNumber" Name="Contact Phone" IsRequired="false"  />
                <uc:Text ID="txtcontactEmail" runat="server" Field="Insured.ContactEmail" Name="Contact Email" Columns="70" IsRequired="false"  />      
                <% }  %>
            </table>
        </div>
    </cc:Box>
    
    <cc:Box id="boxAgency" runat="server" Title="Agency" width="600">
        <a id="lnkAgency" href="javascript:Toggle('divAgency')">Show/Hide</a><br />
        <div id="divAgency" style="MARGIN-TOP: 5px">
            <table class="fields">
                <% if (_dispSurveyRequestAgencyLookup){ %>
                <uc:Combo id="cboAgencyLookup" runat="server" field="" name="Agency Lookup" topItemText="" IsRequired="false" />
                <tr><td><hr size="1" /></td></tr>
                <% } %>
                <uc:Text id="txtAgencyNumber" runat="server" field="Agency.Number" IsRequired="True" Group="txt" />
                <% if (_eSurvey.PrimaryPolicy != null || !String.IsNullOrEmpty(_eSurvey.WorkflowSubmissionNumber)) { %>
                <uc:Label id="lblAgencyName" runat="server" field="Agency.Name" Group="lbl" />
                <uc:Label id="lblAgencyAddress" runat="server" field="Agency.HtmlAddress" Name="Address" Group="lbl" />
                <uc:Label id="lblAgencyPhone" runat="server" field="Agency.HtmlPhoneNumber" Name="Phone Number" Group="lbl" />
                <uc:Label id="lblAgencyFax" runat="server" field="Agency.HtmlFaxNumber" Name="Fax Number" Group="lbl" />
                <% } else { %>
                <uc:Text id="txtAgencyName" runat="server" field="Agency.Name" Columns="60" IsRequired="true" Group="txt" />
                <uc:Text id="txtAgencyStreetLine1" runat="server" field="Agency.StreetLine1" Columns="70" IsRequired="true" Group="txt" />
                <uc:Text id="txtAgencyStreetLine2" runat="server" field="Agency.StreetLine2" Columns="70" Group="txt" />
                <uc:Text id="txtAgencyCity" runat="server" field="Agency.City" IsRequired="true" Group="txt" />
                <uc:Combo id="cboAgencyState" runat="server" field="Agency.StateCode" Name="State" topItemText="" isRequired="True" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" Group="txt" />
                <uc:Text id="txtAgencyZipCode" runat="server" field="Agency.ZipCode" IsRequired="true" Group="txt" />
                <uc:Text id="txtAgencyPhone" runat="server" field="Agency.PhoneNumber" Name="Phone Number" Group="txt" IsRequired="true" />
                <uc:Text id="txtAgencyFax" runat="server" field="Agency.FaxNumber" Name="Fax Number" Group="txt" />
                <% } %>
                <uc:Text id="txtProducer" runat="server" field="Agency.Producer" IsRequired="False" Group="txt" />   
                <uc:Text id="txtAgentContactName" runat="server" field="Insured.AgentContactName" Name="Agent Contact Name" IsRequired="False" />
                <uc:Text id="txtAgentContactPhone" runat="server" field="Insured.AgentContactPhoneNumber" Name="Agent Phone" IsRequired="False" />
                <uc:Combo id="cboAgentEmail" runat="server" field="Insured.AgentEmailID" Name="Agent Email" topItemText="-- select or add email --" isRequired="False" />
                <tr id="rowLnkAddAgencyEmail">
                    <td>&nbsp;</td>
                    <td>
                        <a id="lnkAddEmail" href="javascript:Toggle('rowAddAgencyEmail'); Toggle('rowLnkAddAgencyEmail');">Add Email to List</a>
                    </td>
                </tr>
                <tr id="rowAddAgencyEmail" style="DISPLAY: none" runat="Server">
                    <td>&nbsp;</td>
                    <td>
                        New Email<br />
                        <asp:TextBox ID="txtNewAgencyEmail" runat="server" Columns="60" MaxLength="100" CssClass="txt"></asp:TextBox><br /><br />
                        <asp:Button ID="btnAddEmail" Runat="server" CssClass="btn" Text="Add" CausesValidation="False" />&nbsp;
                        <input type="button" value="Cancel" class="btn" onclick="javascript:Toggle('rowAddAgencyEmail'); Toggle('rowLnkAddAgencyEmail'); document.getElementById('txtNewAgencyEmail').innerText = ''">
                    </td>
                </tr>
            </table>
        </div>
    </cc:Box>
    
    <cc:Box id="boxLocations" runat="server" Title="Locations" width="600">
        <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:LinkButton id="lnkSelectAll" runat="server" CausesValidation="false">Select All</asp:LinkButton>&nbsp;&nbsp;|&nbsp;&nbsp;
                            <asp:LinkButton id="lnkUnselectAll" runat="server" CausesValidation="false">Unselect All</asp:LinkButton>
                        </td>
                        <td align="right">
                            <asp:CheckBox ID="chkOneSurveyForAllLocations" runat="server" Text="One Survey for all Locations" CssClass="chk" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:datagrid ID="grdLocations" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="1000" AllowSorting="True">
                                <headerstyle CssClass="header" />
                                <itemstyle CssClass="item" />
                                <alternatingitemstyle CssClass="altItem" />
                                <footerstyle CssClass="footer" />
                                <pagerstyle CssClass="pager" Mode="NumericPages" />
                                <columns>
                                    <asp:BoundColumn HeaderText="Location&nbsp;#" DataField="LocationNumber" SortExpression="LocationNumber ASC, BuildingCount DESC" />
                                    <asp:TemplateColumn HeaderText="Address Line 1" SortExpression="StreetLine1 ASC">
                                        <ItemTemplate>
                                            <%# HtmlEncodeNullable(Container.DataItem, "StreetLine1", string.Empty, "(none)")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="City" SortExpression="City ASC">
                                        <ItemTemplate>
                                            <%# HtmlEncodeNullable(Container.DataItem, "City", string.Empty, "(none)")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="State" SortExpression="StateCode ASC">
                                        <ItemTemplate>
                                            <%# HtmlEncodeNullable(Container.DataItem, "StateCode", string.Empty, "(none)")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Zip" SortExpression="ZipCode ASC">
                                        <ItemTemplate>
                                            <%# HtmlEncodeNullable(Container.DataItem, "ZipCode", string.Empty, "(none)")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Policy" SortExpression="PolicySymbol ASC">
                                        <ItemTemplate>
                                            <%# HtmlEncodeNullable(Container.DataItem, "PolicySymbol", string.Empty, "(none)")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Buildings">
                                        <ItemTemplate>
                                            <%# HtmlEncode(Container.DataItem, "BuildingCount")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Complete">
                                        <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                                <asp:LinkButton  ID="lnkCompletionStatus" runat="server" CausesValidation="true">
                                                    <img ID="imgCompletionStatus" border="0" runat="server" />
                                                </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Include">
                                        <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkLocation" runat="server"></asp:CheckBox> 
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            &nbsp;<asp:LinkButton ID="btnRemoveLOB" Runat="server" CommandName="Remove" OnPreRender="btnRemoveLocation_PreRender" CausesValidation="true">Remove</asp:LinkButton>&nbsp;
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </columns>
                            </asp:datagrid>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="center" valign="top">
                <div id="divCopyCoverages" style="padding-top:45px; padding-left:5px" runat="server">
                    <asp:LinkButton  ID="lnkCopyCoverages" runat="server" CausesValidation="true" ToolTip="This link applies the selected coverages from the completed location and copies them to all the other locations.">Copy<br />Coverages</asp:LinkButton><br />
                    <img src="<%= ARMSUtility.AppPath %>/images/an_arrow_down.gif" />
                </div>
            </td>
        </tr>
        </table>
        <asp:LinkButton ID="lnkAddLocation" runat="server">Add Location</asp:LinkButton>
    </cc:Box>
    
    <cc:Box id="boxDocuments" runat="server"  width="600">
        <a id="lnkAddDocument" href="javascript:Toggle('divAddDocument')">Show/Hide</a>
        <div id="divAddDocument" runat="server" style="MARGIN-TOP: 10px">
            <% if( repDocs.Items.Count > 0 ) { %>
            <asp:Repeater ID="repDocs" Runat="server">
                <HeaderTemplate>
                    <table cellspacing="0" cellpadding="0" border="0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td style="PADDING-BOTTOM: 3px">
                            <a id="lnkDownloadDocInNewWindow" href='<%# DataBinder.Eval(Container.DataItem, "ID", "Document.aspx?docid={0}") %>' target="_blank" runat="server"><%# GetFileNameWithSize(Container.DataItem) %></a>
                            <asp:LinkButton ID="lnkDownloadDoc" runat="server" OnClick="lnkDownloadDoc_OnClick"><%# GetFileNameWithSize(Container.DataItem) %></asp:LinkButton>
                        </td>
                        <td style="PADDING-LEFT: 10px">
                            From <%# GetUserName(DataBinder.Eval(Container.DataItem, "UploadUser")) %> on <%# HtmlEncode(Container.DataItem, "UploadedOn", "{0:g}") %>
                        </td>
                        <td style="PADDING-LEFT: 15px" valign="top" runat="server">
                        <asp:LinkButton ID="lnkRemoveDoc" runat="server" OnClick="lnkRemoveDoc_OnClick">Remove</asp:LinkButton>
                    </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <% } else { %>
            <div style="PADDING-BOTTOM: 10px">
                (none)
            </div>
            <% } %>
        </div>
        
        <div id="divUploadDoc" runat="server">
            Attach Document<br />
            <input type="file" id="fileUploader" runat="server" name="fileDocument" class="txt" size="80"><br />
            <%if (cboDocType.Items.Count > 0 && CurrentUser.Company.RequireDocTypeOnCreateSurvey) { %>
                Doc Type<br />
                <asp:DropDownList ID="cboDocType" runat="server" CssClass="cbo"></asp:DropDownList><br />
                <%if (CurrentUser.Company.SupportsDocRemarks) { %>
                    Doc Remarks&nbsp;<span style="font-size:10px">(225 character limit) (optional)</span><br />
                    <asp:TextBox ID="txtDocRemarks" runat="server" Columns="35" CssClass="txt" MaxLength="225"></asp:TextBox>
                <% } %>
            <% } %>
            <div style="padding-top:5px">
                <asp:Button ID="btnAttachDocument" Runat="server" Text="Attach Document" CssClass="btn" CausesValidation="False" />
                <% if (String.IsNullOrEmpty(CurrentUser.RemoteAgencyNumbers)) { %>
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lnkDocumentRetriever" runat="server"><span style="font-size:12px">...Search & Retrieve Additional Documents from FileNet >></span></asp:LinkButton>
                <% } %>
            </div>
        </div>
    </cc:Box>
    
    <cc:Box id="boxAssignment" runat="server" Title="Assignment" width="600">
        <table class="fields">
        <% if (_usesInHouseSelection){ %>
            <tr>
                <td align ="right">&nbsp</td>
                <td>
                    <asp:RadioButton ID="rdoInHouseSelectionSystem" GroupName="rdoInHouseSelection" CssClass="rdo" Text="System Selection" runat="server" Checked="true" />
                    <asp:RadioButton ID="rdoInHouseSelectionInHouse" GroupName="rdoInHouseSelection" CssClass="rdo" Text="In-house Consultant" runat="server" />
                </td>
            </tr>
        <% } %>
        <%if (_canAssignConsultant){%>
            <tr>
                <td><asp:Label ID="lblAssign" Runat="server">Assign Consultant</asp:Label>&nbsp&nbsp&nbsp</td>
                <td><asp:DropDownList ID="cboConsultants" runat="server" CssClass="cbo" /></td>
            </tr>
        <% } %>
        <% if (SurveyAssignPriority == "true"){ %>
            <tr>
                <td align ="right"><asp:Label ID="lblSurveyPriority" runat="server" Text ="Survey Priority" ></asp:Label>&nbsp&nbsp&nbsp</td>
                <td><asp:DropDownList ID="ddlSurveyPriority" runat ="server" Width="200" > </asp:DropDownList> </td>
            </tr>
        <% } %>     
            <uc:Date ID="dtDateDue" runat="server" Field="Survey.DueDate" Name="Due Date *" IsRequired="False" />
        <% if (CurrentUser.Company.SupportsNonDisclosure){ %>
            <uc:Check ID="chkNonDisclosure" runat="server" Field="Survey.NonDisclosureRequired" Name="Non-Disclosure Agreement Required" Directions="* If yes, ensure NDA is executed prior to submitting the request." IsRequired="false" />
        <% } %>
            <uc:Text id="txtComments" runat="server" field="Agency.StreetLine1" Name="Comments <br />or Instructions" style="width: 450px" Rows="5" EnableCheckSpelling="true" />
            <tr>
                <td align ="right"><asp:Label ID="lblCommentVisibleTo" Runat="server">Visible to</asp:Label>&nbsp&nbsp&nbsp</td>
                <td>
                    <asp:Panel runat="server" ID="NotePrivacyDiv">
                        <asp:RadioButton ID="rdoPrivacyAll" runat="server" Text="All" GroupName="rdoNotePrivacy" />
                        <asp:RadioButton ID="rdoPrivacyInternal" runat="server" Text="Internal Only" GroupName="rdoNotePrivacy" />
                        <asp:RadioButton ID="rdoPrivacyAdministrator" runat="server" Text="Administrators Only" GroupName="rdoNotePrivacy" />
                    </asp:Panel>
                </td>
            </tr>
       </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" CausesValidation="True" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />

    <uc:HintFooter ID="ucHintFooter" runat="server" VerticalOffset="130" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript" type="text/javascript">
    function Toggle(id)
    {
        ToggleControlDisplay(id);
    }
    function SetExclusion(isChecked, surveyID, locationID, companyID, lnkID, imgID)
    {
        var lnk = document.getElementById(lnkID);
        var img = document.getElementById(imgID);

        AjaxMethodsService.ExclusionCheck(isChecked, surveyID, locationID, companyID, function (result, eventArgs) {
            img.setAttribute('src', result);

            if (!isChecked) {
                var href = lnk.getAttribute("href");
                if (href && href != "" && href != null) {
                    lnk.setAttribute('href_bak', href);
                }
                lnk.removeAttribute('href');
            }
            else {
                var href_bak = lnk.getAttribute("href_bak");
                if (href_bak && href_bak != "" && href_bak != null) {
                    lnk.setAttribute('href', lnk.attributes['href_bak'].nodeValue);
                }
            }
        });
    }
    function RemoveHrefs() { 
        arrLinks = new Array();
        arrLinks = frm.getElementsByTagName('a');
        
        for (i=0; i<arrLinks.length; i++) { 
            var att = arrLinks[i].getAttribute("flagHref");
            if (att != null && att == "1"){
                var href = arrLinks[i].getAttribute("href");
                if(href && href != "" && href != null){
                    arrLinks[i].setAttribute('href_bak', href);
                }
                arrLinks[i].removeAttribute('href');
            }
        }
    } 
    function GetAgency(agencyNum, insuredID, companyNum)
    {
        AjaxMethodsService.GetAgency(agencyNum, insuredID, companyNum, function (arrFields, eventArgs) {
            //clear the agent dropdown and re-populate
            document.getElementById("cboAgentEmail_cbo").options.length = 0

            AjaxMethodsService.GetAgencyEmails(agencyNum, insuredID, companyNum, function (arrEmails, eventArgs) {
                for (var i = 0; i < arrEmails.length; ++i) {
                    document.getElementById("cboAgentEmail_cbo").options[i] = new Option(arrEmails[i].split('|')[0], arrEmails[i].split('|')[1]);
                }

                if (arrFields.length == 4) {
                    document.getElementById("lblAgencyName_lblValue").innerHTML = arrFields[0];
                    document.getElementById("lblAgencyAddress_lblValue").innerHTML = arrFields[1];
                    document.getElementById("lblAgencyPhone_lblValue").innerHTML = arrFields[2];
                    document.getElementById("lblAgencyFax_lblValue").innerHTML = arrFields[3];
                }
                else if (arrFields.length == 8) {
                    document.getElementById("txtAgencyName_txt").value = arrFields[0];
                    document.getElementById("txtAgencyStreetLine1_txt").value = arrFields[1];
                    document.getElementById("txtAgencyStreetLine2_txt").value = arrFields[2];
                    document.getElementById("txtAgencyCity_txt").value = arrFields[3];
                    document.getElementById("cboAgencyState_cbo").value = arrFields[4];
                    document.getElementById("txtAgencyZipCode_txt").value = arrFields[5];
                    document.getElementById("txtAgencyPhone_txt").value = arrFields[6];
                    document.getElementById("txtAgencyFax_txt").value = arrFields[7];
                }
                else {

                    alert(arrFields[0]);
                    if (arrFields[0].indexOf('*') < 0) {
                        document.getElementById("txtAgencyNumber_txt").value = "";
                    }
                    else {
                        document.getElementById("txtAgencyName_txt").value = "";
                        document.getElementById("txtAgencyStreetLine1_txt").value = "";
                        document.getElementById("txtAgencyStreetLine2_txt").value = "";
                        document.getElementById("txtAgencyCity_txt").value = "";
                        document.getElementById("cboAgencyState_cbo").value = "";
                        document.getElementById("txtAgencyZipCode_txt").value = "";
                        document.getElementById("txtAgencyPhone_txt").value = "";
                        document.getElementById("txtAgencyFax_txt").value = "";
                        document.getElementById("txtAgentContactName_txt").value = "";
                        document.getElementById("txtAgentContactPhone_txt").value = "";
                        document.getElementById("cboAgentEmail_cbo").value = "";
                    }
                }
            });
        });
    }
    </script>

    <!-- Added By Vilas Meshram: 03/01/2013: Squish# 21365: Drow down for priority -->
    <link rel="stylesheet" type="text/css" href="<%= ARMSUtility.TemplatePath %>/MsDropdown/dd.css" />
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery-1.6.1.min.js"></script>
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery.dd.js"></script>

    <!-- Script is used to call the JQuery for dropdown -->
    <script type="text/javascript" language="javascript">
        $(document).ready(function (e) {
            try {
                $("#ddlSurveyPriority").msDropDown();
            } catch (e) {
                alert(e.message);
            }
        });
    </script>
    <!-- Added By Vilas Meshram: 03/01/2013: Squish# 21365: Drow down for priority -->

    <div style="display: none;"><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" alt="" /></div>
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function() { 
            $.blockUI.defaults.css = {}; 
            $.blockUI.defaults.overlayCSS.opacity = .2; 
            
            Page_IsValid = false;
            
            $('#btnSubmit', this).click(function() { 
                if (Page_IsValid) {
                    $.blockUI({ message: '<h3><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" />&nbsp;&nbsp;&nbsp;Processing, one moment please...</h3>' });
                }
            }); 
        });
    </script>
    </form>
</body>
</html>
