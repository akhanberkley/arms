<%@ Page Language="C#" AutoEventWireup="false" CodeFile="Objectives.aspx.cs" Inherits="ObjectivesAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><a class="nav" href="Survey.aspx<%= ARMSUtility.GetQueryStringForNav("objectiveid") %>"><< Back to Service Visit</a><br><br>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <asp:datagrid ID="grdObjectives" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
            <asp:TemplateColumn HeaderText="Number" SortExpression="Number ASC">
                <ItemTemplate>
                    <a href="ObjectiveEdit.aspx<%= ARMSUtility.GetQueryStringForNav() %>&objectiveid=<%# HtmlEncode(Container.DataItem, "ID")%>"><%# HtmlEncode(Container.DataItem, "Number").PadLeft(3,'0')%></a>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Objective" SortExpression="Text ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "Text", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
             <asp:TemplateColumn HeaderText="Comments" SortExpression="CommentsText ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "CommentsText", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Target Date" SortExpression="TargetDate ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "TargetDate", "{0:d}", DateTime.MinValue, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Status" SortExpression="Status.Name ASC">
                <ItemTemplate>
	                <%# HtmlEncodeNullable(Container.DataItem, "Status.Name", string.Empty, "(none)")%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <ItemTemplate>
	                &nbsp;<asp:LinkButton ID="btnRemoveObjective" Runat="server" CommandName="Remove" OnPreRender="btnRemoveObjective_PreRender" CausesValidation="false">Remove</asp:LinkButton>&nbsp;
                </ItemTemplate>
            </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>

    <a href="ObjectiveEdit.aspx<%= ARMSUtility.GetQueryStringForNav("objectiveid") %>">Create New Objective</a>
 
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
