<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminRecs.aspx.cs" Inherits="AdminRecsAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <asp:CheckBox ID="chkShowDisabled" Runat="server" CssClass="chk" AutoPostBack="True" Text="Show disabled recs." />
    <asp:datagrid ID="grdRecs" Runat="server" CssClass="grid" AutoGenerateColumns="False" PageSize="25" AllowPaging="True" AllowSorting="True">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
		    <asp:hyperlinkcolumn HeaderText="Code" DataTextField="Code" DataNavigateUrlField="ID"
			    DataNavigateUrlFormatString="AdminRecEdit.aspx?recid={0}" SortExpression="Code ASC" />
		    <asp:BoundColumn HeaderText="Title" DataField="Title" SortExpression="Title ASC" />
	    </columns>
    </asp:datagrid>

    <a href="AdminRecEdit.aspx">Add a New Rec</a>
    
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
