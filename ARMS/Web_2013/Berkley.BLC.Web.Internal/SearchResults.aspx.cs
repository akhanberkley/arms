using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web.Validation;
using QCI.ExceptionManagement;
using QCI.Web;

public partial class SearchResultsAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
        BuildDataGrid();
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(SearchResultsAspx_Load);
        this.PreRender += new EventHandler(SearchResultsAspx_PreRender);
        this.grdItems.PageIndexChanged += new DataGridPageChangedEventHandler(grdItems_PageIndexChanged);
        this.grdItems.SortCommand += new DataGridSortCommandEventHandler(grdItems_SortCommand);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.ddlItemsPerPage.SelectedIndexChanged += new EventHandler(ddlItemsPerPage_SelectedIndexChanged);
        this.btnExport.Click += new EventHandler(btnExport_Click);
        this.cboFilterBy.SelectedIndexChanged += new EventHandler(cboFilterBy_SelectedIndexChanged);
        this.lnkMapSelectedLocations.Click += new EventHandler(lnkMapSelectedLocations_Click);
    }
    #endregion

    protected SearchCriteria _criteria; // used during HTML rendering
    protected int _recordCount = 0;
    protected bool _mapSelectedLocations;

    void SearchResultsAspx_Load(object sender, EventArgs e)
    {
        // load search criteria from query string
        _criteria = SearchCriteria.Create(HttpContext.Current.Request.QueryString);

        if (!this.IsPostBack)
        {
            cboFilterBy.SelectedValue = (string)this.UserIdentity.GetPageSetting("FilterBy", string.Empty);

            //Give the ability to save searches on behalf of other users
            if (CurrentUser.Role.PermissionSet.CanRequireSavedSearch)
            {
                cboUser.Visible = true;
                User[] users = Berkley.BLC.Entities.User.GetSortedArray("CompanyID = ? && IsFeeCompany = false && IsUnderwriter = false && AccountDisabled = false", "Name ASC", this.CurrentUser.CompanyID);
                cboUser.DataBind(users, "ID", "Name");
                cboUser.SelectedValue = CurrentUser.ID.ToString();
            }

            // restore sort expression, reverse state, and page index settings
            this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", "DueDate");
            this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", true);
        }
    }

    void SearchResultsAspx_PreRender(object sender, EventArgs e)
    {
        this.grdItems.PageSize = (int)this.UserIdentity.GetPageSetting("PageCount", 20);
        this.ddlItemsPerPage.SelectedValue = this.grdItems.PageSize.ToString();

        //little pre-work if searching by policy number
        string policyNumber = ARMSUtility.GetStringFromQueryString("pnum", false);        
        string queryByPolicy = SearchCriteria.BuildPolicyQuery(CurrentUser.CompanyID, policyNumber);        
        // build the filter and get the current sort expression
        object[] args;
        string query = _criteria.GetOPathQuery(this.UserIdentity.CompanyID, out args) + queryByPolicy;

        
        List<object> argsList = new List<object>();
        argsList.AddRange(args);
        if (cboFilterBy.SelectedValue == "TwoYears")
        {
            query += " && SurveyedDate >= ? ";
            argsList.Add(DateTime.Today.AddYears(-2));
        }
        else if (cboFilterBy.SelectedValue == "Assigned")
        {
            query += " && (SurveyStatusCode = ? || SurveyStatusCode = ?) ";
            argsList.Add(SurveyStatus.AssignedSurvey.Code);
            argsList.Add(SurveyStatus.AwaitingAcceptance.Code);
        }

        string sortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sortExp = sortExp.Replace(" DESC", " TEMP");
            sortExp = sortExp.Replace(" ASC", " DESC");
            sortExp = sortExp.Replace(" TEMP", " ASC");
        }

        bool consultantSearchFilter = (Berkley.BLC.Business.Utility.GetCompanyParameter("ConsultantSearchFilter", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        if (consultantSearchFilter && CurrentUser.Role.IsConsultant)
        {
            query += " && ((ConsultantUserID = ? || AssignedUserID = ?)) ";
            argsList.Add(CurrentUser.ID);
            argsList.Add(CurrentUser.ID);
        }

        // execute the search query
        VWSurvey[] eSurveys = VWSurvey.GetSortedArray(query, sortExp, argsList.ToArray());
        _recordCount = eSurveys.Length;

        //log large searches
        int maxCount = int.Parse(ConfigurationManager.AppSettings["SearchResultThreshold"]);
        if (_recordCount > maxCount)
        {
            ExceptionManager.Publish(new Exception(string.Format("WARNING: A user performed a search that returned over {0} results. \nUser: {1} \nCompany: {2} \nResult Count: {3} \nQuery Details: {4}", maxCount, CurrentUser.Name, CurrentUser.Company.Abbreviation, _recordCount, HttpContext.Current.Request.QueryString.ToString())));
        }

        this.ViewState["SearchResults"] = eSurveys;

        // bind the results
        grdItems.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindPagingGrid(grdItems, eSurveys, null, "No surveys were found matching your search criteria.");

        SetMappingURL(eSurveys);
    }

    void lnkMapSelectedLocations_Click(object sender, EventArgs e)
    {
        List<string> surveyNumbers = new List<string>();

        foreach (DataGridItem dataGridItem in grdItems.Items)
        {
            if (((CheckBox)dataGridItem.FindControl("ChkMap")).Checked)
            {
                string sSurveyNumber = ((HyperLink)dataGridItem.FindControl("lnkSurvey")).Text.ToString();
                surveyNumbers.Add(sSurveyNumber);
            }
        }

        Response.Redirect(string.Format("mapOvi.aspx?name={0}&surveys={1}", HttpUtility.UrlEncode(base.PageHeading), String.Join(",", surveyNumbers.ToArray())));
        //lnkMapLocations.HRef = string.Format("mapOvi.aspx?name={0}&surveys={1}", HttpUtility.UrlEncode(base.PageHeading), String.Join(",", surveyNumbers.ToArray()));
    }

    private void SetMappingURL(VWSurvey[] surveys)
    {
        //get the list of unique location zip codes by consultant
        List<string> surveyNumbers = new List<string>();
        foreach (VWSurvey survey in surveys)
        {
            surveyNumbers.Add(survey.SurveyNumber.ToString());
        }

        //lnkMapLocations.HRef = string.Format("map.aspx?name={0}&surveys={1}", HttpUtility.UrlEncode(base.PageHeading), String.Join(",", surveyNumbers.ToArray()));
        lnkMapLocations.HRef = string.Format("mapOvi.aspx?name={0}&surveys={1}", HttpUtility.UrlEncode(base.PageHeading), String.Join(",", surveyNumbers.ToArray()));
        lnkMapLocations.Visible = (surveys.Length > 0);

        int maxSurveyCount = int.Parse(ConfigurationManager.AppSettings["MaxSurveyCountToMap"]);
        if (surveys.Length > maxSurveyCount)
        {
            lnkMapLocations.Disabled = true;
            lnkMapLocations.HRef = string.Empty;
        }
        else
        {
            lnkMapLocations.Disabled = false;
        }
    }

    void grdItems_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdItems.CurrentPageIndex = e.NewPageIndex;
    }

    void grdItems_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sortRev);
    }

    void ddlItemsPerPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList list = (DropDownList)sender;
        this.UserIdentity.SavePageSetting("PageCount", Convert.ToInt32(list.SelectedValue));
    }

    void cboFilterBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.UserIdentity.SavePageSetting("FilterBy", cboFilterBy.SelectedValue);
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        SearchCriteria c = new SearchCriteria();

        object[] args;
        string query = _criteria.GetOPathQuery(this.UserIdentity.CompanyID, out args);

        UserSearch eSearch = new UserSearch(Guid.NewGuid());
        eSearch.FilterExpression = query.Trim();
        eSearch.Name = lblSearchName.Text.Trim();
        eSearch.UserID = (cboUser.Visible) ? new Guid(cboUser.SelectedValue) : this.CurrentUser.ID;
        eSearch.FilterParameters = ArrayToString(args);
        eSearch.IsRequired = false;

        //get the highest number and incriment by one
        UserSearch[] eSearches = UserSearch.GetSortedArray("UserID = ?", "DisplayOrder DESC", CurrentUser.ID);
        eSearch.DisplayOrder = (eSearches.Length > 0) ? eSearches[0].DisplayOrder + 1 : 1;

        // commit all changes
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            eSearch.Save(trans);
            trans.Commit();
        }

        Response.Redirect("MySurveys.aspx", true);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("SearchResults.aspx{0}", ARMSUtility.GetQueryStringForNav()), true);
    }


    void btnExport_Click(object sender, EventArgs e)
    {
        VWSurvey[] surveys = (VWSurvey[])this.ViewState["SearchResults"];
        ExportItemList oExportDataList = (ExportItemList)this.ViewState["Exports"];

        if (surveys == null || oExportDataList == null)
        {
            JavaScript.ShowMessage(this, "The session has timed out.  Please try again.");
            return;
        }

        string strErrorMessage = "No Surveys Found";
        object oSavedText = this.ViewState["EmptyExportText"];
        if (oSavedText != null)
        {
            strErrorMessage = oSavedText.ToString();
        }

        // Get the CSV data
        string strCsv = oExportDataList.GetCsv(surveys, oExportDataList, strErrorMessage);

        Response.ContentType = "application/x-msexcel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=survey.csv");

        // Write out the CSV data
        System.Text.ASCIIEncoding oEncoding = new System.Text.ASCIIEncoding(); ;
        byte[] rbBytes = oEncoding.GetBytes(strCsv);
        Response.OutputStream.Write(rbBytes, 0, rbBytes.Length);

        Response.End();
    }

    private string GetReturnUrl()
    {
        return "Search.aspx";
    }

    private string ArrayToString(object[] args)
    {
        string result = string.Empty;
        foreach (object param in args)
        {
            if (param.ToString().ToUpper() == "TRUE")
                result += "1~";
            else if (param.ToString().ToUpper() == "FALSE")
                result += "0~";
            else
                result += string.Format("{0}~", param);
        }

        return (result.Length > 0) ? result.Remove(result.Length - 1, 1) : result;
    }

    private void BuildDataGrid()
    {
        ExportItemList oExportList = new ExportItemList(this);

        //Get the columns by order from the DB
        GridColumnFilterCompany[] filterCols = GridColumnFilterCompany.GetSortedArray("CompanyID = ? && IsExternal = 0", "Display DESC, DisplayOrder ASC", CurrentUser.CompanyID);

        foreach (GridColumnFilterCompany column in filterCols)
        {
            string columnName = (column.GridColumnFilter.Name == "Call<br>Count") ? CurrentUser.Company.CallCountLabel : column.GridColumnFilter.Name;
            if (column.Display && column.GridColumnFilter.ID != GridColumnFilter.Reminder.ID)
            {
                TemplateColumn template = new TemplateColumn();
                template.HeaderText = columnName;
                template.SortExpression = column.GridColumnFilter.SortExpression;
                template.ItemTemplate = new MyTemplate(column.GridColumnFilter);

                grdItems.Columns.Add(template);

                if (columnName.ToLower().Trim() == "map")
                    _mapSelectedLocations = true;

            }

            if (this.IsPostBack)
            {
                if (columnName.ToLower().Trim() != "map")
                {
                    // Grab the field names for Excel export
                    string strFilterName = column.GridColumnFilter.FilterExpression == null || column.GridColumnFilter.FilterExpression.Length == 0 ? "" : column.GridColumnFilter.FilterExpression.Replace("\r\n", " ").Trim();
                    string strSortName = column.GridColumnFilter.SortExpression == null || column.GridColumnFilter.SortExpression.Length == 0 ? "" : column.GridColumnFilter.SortExpression.Substring(0, column.GridColumnFilter.SortExpression.IndexOf(" "));
                    string strDataType = column.GridColumnFilter.DotNetDataType == null || column.GridColumnFilter.DotNetDataType.Length == 0 ? "" : column.GridColumnFilter.DotNetDataType.Replace("\r\n", " ").Trim();
                    oExportList.Add(new ExportItem(column.GridColumnFilter.ID, strSortName, strFilterName, strDataType, columnName, column.DisplayOrder));
                }
            }
        }

        //oExportList.Sort();
        this.ViewState["Exports"] = oExportList;
    }

    private class MyTemplate : ITemplate
    {
        private string expression;
        private GridColumnFilter filter;
        public MyTemplate(GridColumnFilter filter)
        {
            this.filter = filter;
            this.expression = filter.FilterExpression;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            //figure out the type of control to bind
            if (filter.ID == GridColumnFilter.SurveyLink.ID)
            {
                HyperLink link = new HyperLink();
                link.ID = "lnkSurvey";
                link.DataBinding += new EventHandler(this.BindHyperLink);
                container.Controls.Add(link);
            }
            else if (filter.ID == GridColumnFilter.InsuredLink.ID)
            {
                HyperLink link = new HyperLink();
                link.ID = "lnkInsured";
                link.DataBinding += new EventHandler(this.BindHyperLink);
                container.Controls.Add(link);
            }
            else if (filter.ID == GridColumnFilter.MapCheckBox.ID)
            {
                CheckBox chkMap = new CheckBox();
                chkMap.ID = "chkMap";
                container.Controls.Add(chkMap);
            }
            else
            {
                Literal l = new Literal();
                l.DataBinding += new EventHandler(this.BindData);
                container.Controls.Add(l);
            }
        }

        public void BindData(object sender, EventArgs e)
        {
            Literal l = (Literal)sender;
            DataGridItem container = (DataGridItem)l.NamingContainer;
            //Survey eSurvey = (Survey)container.DataItem;

            switch (filter.SystemTypeCode)
            {
                case TypeCode.DateTime:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:d}", DateTime.MinValue, "(none)");
                    break;
                case TypeCode.Decimal:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:C}", Decimal.MinValue, "(none)");
                    break;
                case TypeCode.Int64:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:N2}", Decimal.MinValue, "(none)");
                    break;
                case TypeCode.Int32:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:N0}", Int32.MinValue, "(none)");
                    break;
                default:
                    if (expression.ToLower().Equals("priority"))
                    {
                        var d = (VWSurvey)container.DataItem;
                        string imgName = !string.IsNullOrEmpty(d.Priority) ? d.Priority : "./Images/colors/none.png";
                        var imgHtml =
                            string.Format(
                                " <img id='imgPriority' src='{0}'  height='20' width='20' />",
                                imgName);
                        l.Text = imgHtml;
                    }
                    else
                        l.Text = HtmlEncodeNullable(container.DataItem, expression, string.Empty, "(none)");
                    break;
            }
        }

        public void BindHyperLink(object sender, EventArgs e)
        {
            HyperLink link = (HyperLink)sender;
            DataGridItem container = (DataGridItem)link.NamingContainer;
            link.Text = HtmlEncodeNullable(container.DataItem, expression, string.Empty, "(none)");

            if (link.ID == "lnkSurvey")
            {
                link.NavigateUrl = string.Format("Survey.aspx?surveyid={0}&NavFromSearch=0&{1}", HtmlEncode(container.DataItem, "SurveyID"), HttpContext.Current.Request.QueryString.ToString());
            }
            else
            {
                link.NavigateUrl = string.Format("Insured.aspx?surveyid={0}&locationid={1}&NavFromSearch=0&{2}", HtmlEncode(container.DataItem, "SurveyID"), HtmlEncode(container.DataItem, "LocationID"), HttpContext.Current.Request.QueryString.ToString());
            }
        }
    }
}
