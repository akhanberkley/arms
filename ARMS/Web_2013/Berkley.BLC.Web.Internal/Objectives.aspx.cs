using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using Wilson.ORMapper;

public partial class ObjectivesAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(ObjectivesAspx_Load);
        this.PreRender += new EventHandler(ObjectivesAspx_PreRender);
        this.grdObjectives.ItemCommand += new DataGridCommandEventHandler(grdObjectives_ItemCommand);
        this.grdObjectives.SortCommand += new DataGridSortCommandEventHandler(grdObjectives_SortCommand);
        this.grdObjectives.PageIndexChanged += new DataGridPageChangedEventHandler(grdObjectives_PageIndexChanged);
    }
    #endregion

    protected Survey _eSurvey;

    void ObjectivesAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        _eSurvey = Survey.Get(id);

        // restore sort expression, reverse state, and page index settings
        this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", grdObjectives.Columns[2].SortExpression);
        this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", false);
        grdObjectives.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);
    }

    void ObjectivesAspx_PreRender(object sender, EventArgs e)
    {
        string sortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sortExp = sortExp.Replace(" DESC", " TEMP");
            sortExp = sortExp.Replace(" ASC", " DESC");
            sortExp = sortExp.Replace(" TEMP", " ASC");
        }
        
        // get the objectives for this survey
        Objective[] eObjectives = Objective.GetSortedArray("SurveyID = ?", sortExp, _eSurvey.ID);
        DataGridHelper.BindPagingGrid(grdObjectives, eObjectives, "ID", "There are no objectives for this service visit.");
    }

    void grdObjectives_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            // get the ID of the territory selected
            Guid id = (Guid)grdObjectives.DataKeys[e.Item.ItemIndex];

            // remove the objective
            Objective eObjective = Objective.Get(id);
            if (eObjective != null)
            {
                eObjective.Delete();

                //Update the survey history
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
                manager.ObjectiveManagement(eObjective, SurveyManager.UIAction.Remove);
            }
        }
    }

    protected void btnRemoveObjective_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this objective?");
    }

    void grdObjectives_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdObjectives.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);
    }

    void grdObjectives_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sortRev);
    }
}
