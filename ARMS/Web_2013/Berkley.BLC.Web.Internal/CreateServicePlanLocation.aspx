<%@ Page Language="C#" AutoEventWireup="false" CodeFile="CreateServicePlanLocation.aspx.cs" Inherits="CreateServicePlanLocationAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <cc:Box id="boxLocation" runat="server" Title="Location" width="650">
        <table class="fields">
            <uc:Number id="numLocationNumber" runat="server" field="Location.Number" Columns="10" IsRequired="True" />
            <uc:Text id="txtStreetLine1" runat="server" field="Location.StreetLine1" Name="Address 1" Columns="70" IsRequired="True" />
            <uc:Text id="txtStreetLine2" runat="server" field="Location.StreetLine2" Name="Address 2" Columns="70" />
            <uc:Text id="txtCity" runat="server" field="Location.City" IsRequired="True" />
            <uc:Combo id="cboInsuredState" runat="server" field="Location.StateCode" Name="State" topItemText="" isRequired="True" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" />
            <uc:Text id="txtZipCode" runat="server" field="Location.ZipCode" IsRequired="True" />
            <uc:Text id="txtLocationDescription" runat="server" field="Location.Description" Name="Location Description" Rows="1" Directions="(ex. 'Apartment Complex')" Columns="63" />
        </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Add" />&nbsp;
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />

    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
