using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using Wilson.ORMapper;
using QCI.Web.Validation;
using QCI.Web;
using QCI.ExceptionManagement;
using Aspose.Words;

public partial class AdminDocumentEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminDocumentEditAspx_Load);
        this.PreRender += new EventHandler(AdminDocumentEditAspx_PreRender);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.btnAttachDocument.Click += new EventHandler(btnAttachDocument_Click);
        this.grdVersions.ItemCommand += new DataGridCommandEventHandler(grdVersions_ItemCommand);
        this.lnkManageField.Click += new EventHandler(lnkManageField_Click);
    }
    #endregion

    protected MergeDocument _eMergeDoc;
    protected Guid _surveyID = Guid.Empty;
    protected MergeDocumentType mergeType;

    void AdminDocumentEditAspx_Load(object sender, EventArgs e)
    {
        Guid gMergeDocID = ARMSUtility.GetGuidFromQueryString("mergedocid", false);
        _eMergeDoc = (gMergeDocID != Guid.Empty) ? MergeDocument.Get(gMergeDocID) : null;

        //isLettersAdmin = ARMSUtility.GetBoolFromQueryString("letterflag", true);
        //numReminderDays.Visible = isLettersAdmin;

        mergeType = MergeDocumentType.Get(ARMSUtility.GetIntFromQueryString("typeflag",true));
        numReminderDays.Visible = (mergeType.ID == MergeDocumentType.Letter.ID);

        if (!this.IsPostBack)
        {
            if (_eMergeDoc != null) // edit
            {
                this.PopulateFields(_eMergeDoc);
            }
        }

        //avoiding the mapper in this call for performance (getting a single random survey object was painfully slow)
        _surveyID = Guid.Parse(DB.Engine.ExecuteScalar(string.Format("SELECT TOP 1 SurveyID FROM Survey WHERE CompanyID = '{0}' AND CreateStateCode != '{1}' AND CreateStateCode != '{2}'", CurrentUser.CompanyID, CreateState.InProgress.Code, CreateState.Migrated.Code)).ToString());

        btnSubmit.Text = (_eMergeDoc == null) ? "Add" : "Update";
        //base.PageHeading = (_eMergeDoc == null) ? string.Format("Add {0}", (isLettersAdmin) ? "Letter" : "Report") : string.Format("Update '{0}'", _eMergeDoc.Name);        
        base.PageHeading = (_eMergeDoc == null) ? string.Format("Add {0}", mergeType.Name) : string.Format("Update '{0}'", _eMergeDoc.Name);
    }

    void AdminDocumentEditAspx_PreRender(object sender, EventArgs e)
    {
        // get
        MergeDocumentVersion[] eVersions = MergeDocumentVersion.GetSortedArray("MergeDocumentID = ?", "VersionNumber ASC", (_eMergeDoc != null) ? _eMergeDoc.ID : Guid.Empty);
        boxReporting.Visible = eVersions.Length > 0 && !_eMergeDoc.Disabled && CurrentUser.Company.SupportsExtractableDocuments && _eMergeDoc.Type != MergeDocumentType.Letter.ID;
        //trManageFields.Visible = false;

        grdVersions.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindPagingGrid(grdVersions, eVersions, "ID", "There are no template versions currently uploaded.");
    }

    protected string GetBoolLabel(object dataItem)
    {
        MergeDocumentVersion eDocVersion = (dataItem as MergeDocumentVersion);
        return (eDocVersion.IsActive) ? "Yes" : "No";
    }

    protected void btnDelete_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to delete this template?");
    }

    void grdVersions_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid gDocVersionID = (Guid)grdVersions.DataKeys[e.Item.ItemIndex];
        MergeDocumentVersion eDocVersion = MergeDocumentVersion.Get(gDocVersionID).GetWritableInstance();

        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            if (e.CommandName.ToUpper() == "MAKEACTIVE")
            {
                // execute the requested transformation on the documents

                //First make all other documents inactive
                MergeDocumentVersion[] otherVersions = MergeDocumentVersion.GetArray("MergeDocumentID = ? && ID != ?", _eMergeDoc.ID, eDocVersion.ID);
                for (int i = 0; i < otherVersions.Length; i++)
                {
                    MergeDocumentVersion otherVersion = otherVersions[i].GetWritableInstance();
                    otherVersion.IsActive = false;
                    otherVersion.Save(trans);
                }

                //Make the selected document active
                eDocVersion.IsActive = true;
                eDocVersion.Save(trans);
                trans.Commit();
            }
            else if (e.CommandName.ToUpper() == "REMOVE")
            {
                if (eDocVersion.IsActive)
                {
                    JavaScript.ShowMessage(this, "Cannot remove the 'Active' template.");
                    return;
                }
                else
                {
                    eDocVersion.Delete(trans);
                    trans.Commit();
                }
            }
        }
    }

    void btnAttachDocument_Click(object sender, EventArgs e)
    {
        HttpPostedFile postedFile = fileUploader.PostedFile;
        string docExt = string.Empty;

        try
        {
            // make sure a filename was uploaded
            if (postedFile == null || postedFile.FileName.Length == 0) // no filename was specified
            {
                throw new FieldValidationException(fileUploader, "Please select a file to submit.");
            }

            // make sure the file name is not larger the max allowed
            string fileName = System.IO.Path.GetFileName(postedFile.FileName);
            if (fileName.Length > 100)
            {
                throw new FieldValidationException(fileUploader, "The file name cannot exceed 100 characters.");
            }

            // check for an extesion
            if (System.IO.Path.GetExtension(fileName).Length <= 0)
            {
                throw new FieldValidationException(fileUploader, "The file name must contain a file extension.");
            }

            docExt = System.IO.Path.GetExtension(fileName).ToUpper();

            // must be a .doc or .docx extension
            if (System.IO.Path.GetExtension(fileName).ToUpper() != ".DOC" && System.IO.Path.GetExtension(fileName).ToUpper() != ".DOCX")
            {
                throw new FieldValidationException(fileUploader, "The system currently only supports Microsoft Word documents.");
            }

            // make sure the file has content
            int max = int.Parse(ConfigurationManager.AppSettings["MaxRequestLength"]);
            if (postedFile.ContentLength <= 0) // the file is empty
            {
                throw new FieldValidationException(fileUploader, "The file submitted is empty.");
            }
            else if (postedFile.ContentLength > max)
            {
                // the maximum file size is 10 Megabytes
                throw new FieldValidationException(fileUploader, "The file size cannot be larger than 10 Megabytes.");
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // attach this template version to the MergeDocument
        try
        {
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //make all other version inactive
                MergeDocumentVersion[] otherVersions = MergeDocumentVersion.GetSortedArray("MergeDocumentID = ?", "VersionNumber DESC", _eMergeDoc.ID);
                for (int i = 0; i < otherVersions.Length; i++)
                {
                    MergeDocumentVersion otherVersion = otherVersions[i].GetWritableInstance();
                    otherVersion.IsActive = false;
                    otherVersion.Save(trans);
                }

                // save the guid of the merge doc in the "category" or the word doc for later retrieval
                Aspose.Words.License license = new Aspose.Words.License();
                license.SetLicense("Aspose.Words.lic");
                Document doc = new Document(postedFile.InputStream);
                System.IO.Stream docStream = (System.IO.Stream)postedFile.InputStream;

                //if (doc.BuiltInDocumentProperties.Category.Trim().Length <= 0)
                //{
                    doc.BuiltInDocumentProperties.Category = _eMergeDoc.ID.ToString();

                    System.IO.MemoryStream asposeStream = new System.IO.MemoryStream();
                    doc.Save(asposeStream, (docExt == ".DOCX") ? SaveFormat.Docx : SaveFormat.Doc);
                    docStream = asposeStream;
                //}

                //reset position
                docStream.Position = 0;

                // remove any pathing info from the specified filename and get the extension
                string fileName = System.IO.Path.GetFileName(postedFile.FileName);
                string extension = System.IO.Path.GetExtension(fileName);

                // save the doc to the imaging server and get the FileNet reference
                ImagingHelper imaging = new ImagingHelper(this.CurrentUser.Company);
                string fileNetRef = imaging.StoreFileImage(extension, postedFile.ContentType, docStream);

                // create a record of this file with the survey 
                MergeDocumentVersion docVersion = new MergeDocumentVersion(Guid.NewGuid());
                docVersion.MergeDocumentID = _eMergeDoc.ID;
                docVersion.VersionNumber = (otherVersions.Length > 0) ? otherVersions[0].VersionNumber + 1 : 1;
                docVersion.IsActive = true;
                docVersion.DocumentName = fileName;
                docVersion.MimeType = postedFile.ContentType;
                docVersion.SizeInBytes = postedFile.InputStream.Length;
                docVersion.FileNetReference = fileNetRef;
                docVersion.UploadedOn = DateTime.Now;
                docVersion.Save(trans);

                trans.Commit();
            }

            Response.Redirect(string.Format("AdminDocumentEdit.aspx{0}", ARMSUtility.GetQueryStringForNav()), false);
        }
        catch (Exception ex)
        {
            //ExceptionManager.Publish(ex);
            throw (ex);
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            MergeDocument eMergeDoc;
            if (_eMergeDoc == null) // add
            {
                eMergeDoc = new MergeDocument(Guid.NewGuid());
                eMergeDoc.CompanyID = this.UserIdentity.CompanyID;

                //get the highest number and incriment by one
                MergeDocument[] eMergeDocs = MergeDocument.GetSortedArray("CompanyID = ? && Type = ? && Disabled = false", "PriorityIndex DESC", this.UserIdentity.CompanyID, mergeType.ID);
                eMergeDoc.PriorityIndex = (eMergeDocs.Length > 0) ? eMergeDocs[0].PriorityIndex + 1 : 1;
            }
            else // edit
            {
                eMergeDoc = _eMergeDoc.GetWritableInstance();

                // disable form fields if this doc is disabled
                if (chkDisabled.Checked)
                {
                    MergeDocumentField[] docFields = MergeDocumentField.GetArray("MergeDocumentID = ? && IsReported = true", eMergeDoc.ID);
                    foreach (MergeDocumentField docField in docFields)
                    {
                        MergeDocumentField docFieldToDelete = docField.GetWritableInstance();
                        docFieldToDelete.IsReported = false;
                        docFieldToDelete.Save();
                    }
                }
            }

            this.PopulateEntity(eMergeDoc);
            eMergeDoc.Type = mergeType.ID;

            // commit all changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                eMergeDoc.Save(trans);
                trans.Commit();
            }

            // update our member var so the redirect will work correctly
            _eMergeDoc = eMergeDoc;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl(), true);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    void lnkManageField_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("AdminDocumentExtraction.aspx?mergedocid={0}&typeflag={1}", _eMergeDoc.ID, mergeType.ID), true);
    }

    private string GetReturnUrl()
    {
        //return (isLettersAdmin) ? "AdminLetters.aspx" : "AdminReports.aspx";

        switch (mergeType.ID)
        {
            case 1:
                return "AdminReports.aspx";
            case 2:
                return "AdminLetters.aspx";
            case 3:
                return "AdminDocuments.aspx";
            default:
                return "Administration.aspx";
 
        }
    }
}
