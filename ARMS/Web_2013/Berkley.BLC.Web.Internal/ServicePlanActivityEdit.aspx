<%@ Page Language="C#" AutoEventWireup="false" CodeFile="ServicePlanActivityEdit.aspx.cs" Inherits="ServicePlanActivityEditAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Date" Src="~/Fields/DateField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><br><br>
  
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxActivity" runat="server" Title="Activity" width="625">
        <table class="fields">
            <uc:Combo id="cboActivityType" runat="server" field="ActivityType.ID" Name="Activity Type" topItemText="" isRequired="True" />
            <uc:Combo id="cboAllocatedTo" runat="server" field="User.ID" Name="Allocated To" topItemText="" isRequired="True" />
            <uc:Date id="dtActivityDate" runat="server" field="ServicePlanActivity.ActivityDate" Name="Date" IsRequired="True" />
            <uc:Number id="numHours" runat="server" field="ServicePlanActivity.ActivityHours" Name="Hours" IsRequired="True" />
            <uc:Text id="txtComments" runat="server" field="ServicePlanActivity.Comments" Name="Comments" Rows="5" Columns="88" />
        </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
