using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using QCI.Web.Validation;
using QCI.ExceptionManagement;
using System.Collections.Generic;
using Wilson.ORMapper;
using System.IO;
using Berkley.Imaging;

public partial class SurveyAspx : AppBasePage
{
    #region Event Handlers

    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(SurveyAspx_Load);
        this.PreRender += new EventHandler(SurveyAspx_PreRender);
        this.repActions.ItemCommand += new RepeaterCommandEventHandler(repActions_ItemCommand);
        this.repActions.ItemDataBound += new RepeaterItemEventHandler(repActions_ItemDataBound);
        this.repCompleteActions.ItemCommand += new RepeaterCommandEventHandler(repCompleteActions_ItemCommand);
        this.repCompleteActions.ItemDataBound += new RepeaterItemEventHandler(repCompleteActions_ItemDataBound);
        this.repDocs.ItemDataBound += new RepeaterItemEventHandler(repDocs_ItemDataBound);
        this.repHistory.ItemDataBound += repHistory_ItemDataBound;
        this.repNotes.ItemDataBound += new RepeaterItemEventHandler(repNotes_ItemDataBound);
        this.btnAddNote.Click += new EventHandler(btnAddNote_Click);
        this.btnAttachDocument.Click += new EventHandler(btnAttachDocument_Click);
        this.btnAssign.Click += new EventHandler(btnAssign_Click);
        this.btnAssignCancel.Click += new EventHandler(btnAssignCancel_Click);
        this.btnDueDateUpdate.Click += new EventHandler(btnDueDateUpdate_Click);
        this.btnDueDateCancel.Click += new EventHandler(btnDueDateCancel_Click);
        this.btnApproveDueDateSubmit.Click += new EventHandler(btnApproveDueDateSubmit_Click);
        this.btnApproveDueDateCancel.Click += new EventHandler(btnApproveDueDateCancel_Click);
        this.btnReportSubmittedDateUpdate.Click += new EventHandler(btnReportSubmittedDateUpdate_Click);
        this.btnReportSubmittedDateCancel.Click += new EventHandler(btnReportSubmittedDateCancel_Click);
        this.btnCompleteDateUpdate.Click += new EventHandler(btnCompleteDateUpdate_Click);
        this.btnCompleteDateCancel.Click += new EventHandler(btnCompleteDateCancel_Click);
        this.lnkNonProductive.Click += new EventHandler(lnkNonProductive_Click);
        this.lnkFlagForReview.Click += new EventHandler(lnkFlagForReview_Click);
        this.btnLetterCancel.Click += new EventHandler(btnLetterCancel_Click);
        this.btnReportCancel.Click += new EventHandler(btnReportCancel_Click);
        this.btnSetDetailsSubmit.Click += new EventHandler(btnSetDetailsSubmit_Click);
        this.btnSetDetailsCancel.Click += new EventHandler(btnSetDetailsCancel_Click);
        this.btnMailReceivedDateUpdate.Click += new EventHandler(btnMailReceivedDateUpdate_Click);
        this.btnMailReceivedDateCancel.Click += new EventHandler(btnMailReceivedDateCancel_Click);
        this.btnCancellation.Click += new EventHandler(btnCancellation_Click);
        this.btnCancellationCancel.Click += new EventHandler(btnCancellationCancel_Click);
        this.btnCloseAndNotify.Click += new EventHandler(btnCloseAndNotify_Click);
        this.btnCloseAndNotifyCancel.Click += new EventHandler(btnCloseAndNotifyCancel_Click);
        this.btnReleaseOwnership.Click += new EventHandler(btnReleaseOwnership_Click);
        this.btnReleaseOwnershipCancel.Click += new EventHandler(btnReleaseOwnershipCancel_Click);
        this.btnTypeUpdate.Click += new EventHandler(btnTypeUpdate_Click);
        this.btnTypeCancel.Click += new EventHandler(btnTypeCancel_Click);
        this.btnCompleteSubmit.Click += new EventHandler(btnCompleteSubmit_Click);
        this.btnCompleteCancel.Click += new EventHandler(btnCompleteCancel_Click);
        this.grdObjectives.ItemCommand += new DataGridCommandEventHandler(grdObjectives_ItemCommand);
        this.grdPlanObjectives.ItemCommand += new DataGridCommandEventHandler(grdPlanObjectives_ItemCommand);
        this.btnMailSentDateUpdate.Click += new EventHandler(btnMailSentDateUpdate_Click);
        this.btnMailSentDateCancel.Click += new EventHandler(btnMailSentDateCancel_Click);
        this.btnQAUpdate.Click += new EventHandler(btnQAUpdate_Click);
        this.btnQACancel.Click += new EventHandler(btnQACancel_Click);
        this.btnAcceptedDateUpdate.Click += new EventHandler(btnAcceptedDateUpdate_Click);
        this.btnAcceptedDateCancel.Click += new EventHandler(btnAcceptedDateCancel_Click);
        this.btnAssignedDateUpdate.Click += new EventHandler(btnAssignedDateUpdate_Click);
        this.btnAssignedDateCancel.Click += new EventHandler(btnAssignedDateCancel_Click);
        this.btnCreateNewSurvey.Click += new EventHandler(btnCreateNewSurvey_Click);
        this.btnCreateNewSurveyCancel.Click += new EventHandler(btnCreateNewSurveyCancel_Click);
        this.btnNotifyUW.Click += new EventHandler(btnNotifyUW_Click);
        this.btnNotifyUWCancel.Click += new EventHandler(btnNotifyUWCancel_Click);
        this.btnChangePrimaryPolicyCancel.Click += new EventHandler(btnChangePrimaryPolicyCancel_Click);
        this.btnChangePrimaryPolicyUpdate.Click += new EventHandler(btnChangePrimaryPolicyUpdate_Click);
        this.repSurveyActivities.ItemDataBound += new RepeaterItemEventHandler(repSurveyActivities_ItemDataBound);
        this.btnEmailUW.Click += new EventHandler(btnEmailUW_Click);
        this.btnEmailUWCancel.Click += new EventHandler(btnEmailUWCancel_Click);
        this.btnCancelPriority.Click += new EventHandler(btnPriorityCancel_Click);
        this.btnSavePriority.Click += new EventHandler(SavePriority_Click);
        this.btnAddPlanObjective.Click += new EventHandler(btnAddPlanObjective_Click);
        this.rdoUserType.SelectedIndexChanged += new EventHandler(rdoUserType_SelectedIndexChanged);
        this.rdlUserType.SelectedIndexChanged += new EventHandler(rdlUserType_SelectedIndexChanged);
        //this.btnTestP8.Click += new EventHandler(btnTestP8_Click);      //WA-621 Doug Bruce 2015-05-04 - Temporary test button
     
    }
    #endregion

    protected Survey _eSurvey;
    protected bool _usesPlanObjectives;
    protected string _handleCompletedCallLabel;
    protected string _uwHideNonCompletedDetails;
    protected string _uwHideCompletedDetails;
    protected int _serveysRecordCount = 0;
    protected string _urlParam = string.Empty;
    protected bool _displayInsuredCount = false;
    protected bool _showSurveyRequestedDateField;
    protected SurveyManager.ReleaseOwnershipType _releaseType = SurveyManager.ReleaseOwnershipType.none;
    protected string _hideUWAssignedUser;
    protected string _customRecommendationSnapShot;
    protected Guid _servicePlanid;
    protected string _requestedByPage;
    protected bool _displayActivityTimes = false;
    protected bool _displayUserType = false;
    protected bool _displayUndAgencyInfo = false;
    protected bool _showUserOption = false;    
    protected bool _isCWGSurvey;

    void SurveyAspx_Load(object sender, EventArgs e)
    {
        // register our AJAX class for use in client script
        Ajax.Utility.RegisterTypeForAjax(typeof(AjaxMethods));

        Guid id = ARMSUtility.GetGuidFromQueryString("surveyid", true);

        _eSurvey = Survey.Get(id);

        //Added By Vilas Meshram: 05/16/2013 : Squish# 21329 : Add Additional Merge Fields for Service Plan and Survey Templates
        _servicePlanid = ARMSUtility.GetGuidFromQueryString("planid", false);

        if (_servicePlanid == System.Guid.Empty)
        {
            ServicePlanSurvey _servicePlanSurvey = ServicePlanSurvey.GetOne("SurveyID = ?", _eSurvey.ID);
            if (_servicePlanSurvey != null)
                _servicePlanid = _servicePlanSurvey.ServicePlanID;
            else
                _servicePlanid = System.Guid.Empty;
        }
        //Added By Vilas Meshram: 05/16/2013 : Squish# 21329 : Add Additional Merge Fields for Service Plan and Survey Templates

        //Added By Vilas Meshram: 05/23/2013: Squish# 21331 : Modification of Recommendation Snap Shot.
        _requestedByPage = (ARMSUtility.GetStringFromQueryString("requestedByPage", false) ==null) ? "&requestedByPage=Survey.aspx" : string.Empty ;

        //Display activity times without simplified account management
        _displayActivityTimes = (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplayActivityTimes", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _displayUserType = (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplayUserType", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _displayUndAgencyInfo = (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplayUndAgencyInfo", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _showUserOption = (Berkley.BLC.Business.Utility.GetCompanyParameter("ShowUserOption", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;        
        _isCWGSurvey = (Berkley.BLC.Business.Utility.GetCompanyParameter("IsCWGSurvey", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        if (!this.IsPostBack)
        {
            ShowActionControl(this.repActions);
            ShowCompleteActionControl(this.repCompleteActions);

            this.lblCallCount.Name = CurrentUser.Company.CallCountLabel;

            //set some labels based on survey verse visit
            lblNumber.Name = (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Service Visit Number" : "Survey Number";
            lblType.Name = (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Visit Type" : "Survey Type";
            lblStatus.Name = (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Visit Status" : "Survey Status";
            lblSurveyHours.Name = (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Visit Hours" : "Survey Hours";

            //Added By Vilas Meshram: 02/14/2013:Link to display the total number of surveys on the survey screen.  Clicking the link will show the search grid with the results
            _displayInsuredCount = (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplayInsuredCount", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
            if (_displayInsuredCount)
            {
                SearchCriteria c = new SearchCriteria();
                c.InsuredClientID = _eSurvey.Insured.ClientID;
                _urlParam = c.ToQueryString();
              
                object[] args;
                string query = c.GetOPathQuery(this.UserIdentity.CompanyID, out args);

                VWSurvey[] eSurveys = VWSurvey.GetSortedArray(query,"", args.ToArray());
                _serveysRecordCount = eSurveys.Length;
            }

            _showSurveyRequestedDateField = (Berkley.BLC.Business.Utility.GetCompanyParameter("ShowSurveyRequestedDateField", CurrentUser.Company).ToLower() == "true") ? true : false;

            // If user is IsUnderwriter and HideUWAssignedUser = true then hide
            _hideUWAssignedUser = Berkley.BLC.Business.Utility.GetCompanyParameter("HideUWAssignedUser", CurrentUser.Company).ToLower();

            //Added By Vilas Meshram: 05/14/2013: Squish# 21331 : Modification of Recommendation Snap Shot.
            _customRecommendationSnapShot = Berkley.BLC.Business.Utility.GetCompanyParameter("CustomRecommendationSnapShot", CurrentUser.Company).ToLower();

    }


    }

    void SurveyAspx_PreRender(object sender, EventArgs e)
    {
        if (this.IsPostBack)
        {
            // re-load the survey to avoid dirty reads after postbacks
            _eSurvey = Survey.Get(_eSurvey.ID);
        }
        imgPriority.ImageUrl = !string.IsNullOrEmpty(_eSurvey.Priority) ? _eSurvey.Priority : "./Images/priorities/none.png";
        // our date picker needs this script
        ARMSUtility.AddJavaScriptToPage(this, ARMSUtility.AppPath + "/AutoFormat.js");

        // put the insured and survey number in the header
        base.PageHeading = string.Format((_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Service Visit #{0} ({1})" : "Survey #{0} ({1})", _eSurvey.Number, ARMSUtility.GetSubstring(_eSurvey.Insured.Name, 80));
        boxSurvey.Title = (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Service Visit Information" : "Survey Information";

        // populate all our static fields
        this.PopulateFields(_eSurvey);
        this.PopulateFields(_eSurvey.Insured);
        this.PopulateFields(_eSurvey.Insured.Agency);
        this.PopulateFields(_eSurvey.Status);
        this.PopulateFields(_eSurvey.Company);

        string command = string.Format("EXEC Recs_GetCompletedCount @SurveyID = '{0}'", _eSurvey.ID);
        string recRount = DB.Engine.GetDataSet(command).Tables[0].Rows[0][0].ToString();

        this.lblOpenRecs.Text = recRount;
        //if (!lblOpenRecs.Text.StartsWith("0"))
        //{
        //    imgRecsArrow.Visible = true;
        //    imgRecsArrow.ImageUrl = ARMSUtility.AppPath + "/images/long_red_arrow_left_2.gif";
        //}

        // populate statics that could be null
        if (_eSurvey.AssignedUser != null)
        {
            this.PopulateFields(_eSurvey.AssignedUser);
        }
        else
        {
            lblAssignedUser.Text = "(not specified)";
        }
        if (_eSurvey.Type != null)
        {
            this.PopulateFields(_eSurvey.Type.Type);
        }
        else
        {
            this.lblType.Text = "(not specified)";
        }
        if (_eSurvey.Grading != null)
        {
            this.PopulateFields(_eSurvey.Grading);
        }
        else
        {
            lblGrading.Text = "(not specified)";
        }

        if (_eSurvey.TransactionType != null)
        {
            this.PopulateFields(_eSurvey.TransactionType);
        }
        else
        {
            lblTransactionType.Text = "(not specified)";
        }

        if (_eSurvey.SeverityRating != null)
        {
            this.PopulateFields(_eSurvey.SeverityRating);
        }
        else
        {
            lblSeverityRating.Text = "(not specified)";
        }

        if (_eSurvey.Quality != null)
        {
            this.PopulateFields(_eSurvey.Quality.Type);
        }
        else
        {
            lblQA.Text = "(not specified)";
        }

        if (_eSurvey.PrimaryPolicyID != Guid.Empty)
        {
            lblCreatedWith.Text = string.Format("Policy #{0}{1}", _eSurvey.PrimaryPolicy.Symbol, (_eSurvey.PrimaryPolicy.Symbol != string.Empty) ? _eSurvey.PrimaryPolicy.Number.Replace(_eSurvey.PrimaryPolicy.Symbol, "") : _eSurvey.PrimaryPolicy.Number);
        }
        else if (_eSurvey.WorkflowSubmissionNumber != string.Empty)
        {
            lblCreatedWith.Text = string.Format("Submission #{0}", _eSurvey.WorkflowSubmissionNumber);
        }
        else
        {
            lblCreatedWith.Text = "(not specified)";
        }

        //Get the list of policies from a scaler function
        string policyNumberList = DB.Engine.ExecuteScalar(string.Format("SELECT dbo.GetSurveyPolicyList('{0}')", _eSurvey.ID)).ToString();
        lblPolicyNumbers.Text = (!string.IsNullOrEmpty(policyNumberList)) ? policyNumberList : "(not specified)";

        //Get the survey hours from a view
        lblSurveyHours.Text = DB.Engine.ExecuteScalar(string.Format("SELECT SurveyHours FROM VW_Survey_SurveyHours WHERE SurveyID = '{0}'", _eSurvey.ID)).ToString();

        if (SurveyReasonType.GetOne("CompanyID = ?", CurrentUser.CompanyID) != null)
        {
            lblReasons.Visible = true;
            lblReasons.Text = SurveyReason.GetSingleLineOfReasons(_eSurvey.ID);
        }

        //populate the dynamic bool fields
        lnkNonProductive.Text = (_eSurvey.NonProductive) ? "Yes" : "No";
        lblNonProductive.Text = lnkNonProductive.Text;
        lnkFlagForReview.Text = (_eSurvey.Review) ? "Yes" : "No";
        lblFlagForReview.Text = lnkFlagForReview.Text;

        // get all documents associated to this survey
        SurveyDocument[] docs = SurveyDocument.GetSortedArray("SurveyID = ? && IsRemoved = false", "UploadedOn DESC", _eSurvey.ID);
        repDocs.DataSource = docs;
        repDocs.DataBind();
        boxDocs.Title = string.Format("Documents ({0})", docs.Length);


        // Notes associated to this survey
        bool _usesNotesAsInstructions = (Utility.GetCompanyParameter("UsesNotesAsInstructions", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        bool _setCommentsInternalOnly = (Utility.GetCompanyParameter("defaultCommentsInternalOnly", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        bool _enableAdministrativeNotes = (Utility.GetCompanyParameter("EnableAdministrativeNotes", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        bool _userCanViewAdministrativeNotes = CurrentUser.Permissions.CanViewAdministrativeNotes;

        rdoPrivacyAll.Checked = true;
        rdoPrivacyAdministrator.Visible = false;

        rdoAll.Checked = true;
        rdoAdministrator.Visible = false;
        if (_setCommentsInternalOnly)
        {
            rdoInternal.Checked = true;
        }
        // Enable the Admin Only privacy option based on company parameter
        if (_enableAdministrativeNotes && _userCanViewAdministrativeNotes)
        {
            rdoAdministrator.Visible = true;
        }

        SurveyNote[] notes;
        rdoPrivacyAll.Checked = true;
        rdoPrivacyAdministrator.Visible = false;
        
        if (_usesNotesAsInstructions)
        {
            notes = SurveyNote.GetSortedArray("SurveyID = ? && (NoteBySurveyCreator = true || NoteByConsultant = true)", "EntryDate DESC", _eSurvey.ID);

            // hide the notes privacy options
            NotePrivacyDiv.Visible = false;

            if (CurrentUser.Role.IsConsultant)
            {
                boxNotes.Title = "Instructions";
                btnAddNote.Text = "Add Instruction";
            }
            else
            {
                btnAddNote.Text = "Add Comment";
            }

        } else {
            notes = SurveyNote.GetSortedArray("SurveyID = ?", "EntryDate DESC", _eSurvey.ID);

            // Set Internal Only to true based on company parameter
            if (_setCommentsInternalOnly)
            {
                rdoPrivacyInternal.Checked = true;
            }

            // Enable the Admin Only privacy option based on company parameter
            if (_enableAdministrativeNotes && _userCanViewAdministrativeNotes)
            {
                rdoPrivacyAdministrator.Visible = true;
            }

            //boxNotes.Title = string.Format("Comments ({0})", notes.Length);
        }

        // Filter out the Administrative Notes if the user is not pemritted to see them
        if (!_userCanViewAdministrativeNotes)
        {
            notes = notes.Where(x => x.AdminOnly != true).ToArray();
        }

        repNotes.DataSource = notes;
        repNotes.DataBind();

        // get the objectives for this survey
        Objective[] eObjectives = Objective.GetSortedArray("SurveyID = ?", "Number ASC", _eSurvey.ID);
        DataGridHelper.BindGrid(grdObjectives, eObjectives, "ID", "There are no objectives for this service visit.");
        repObjectives.DataSource = eObjectives;
        repObjectives.DataBind();
        boxObjectives.Title = string.Format("Objectives ({0})", eObjectives.Length);

        DataGridHelper.BindGrid(grdPlanObjectives, eObjectives, "ID", "There are no objectives for this service visit.");
        boxServicePlanObjectives.Title = string.Format("Service Visit Objectives ({0})", eObjectives.Length);

        // get all recs associated to this survey
        SurveyRecommendation[] eRecs = SurveyRecommendation.GetSortedArray(_eSurvey.GetAllSurveysForAccount, "PriorityIndex ASC");
        repRecs.DataSource = eRecs;
        repRecs.DataBind();
        boxRecs.Title = string.Format("Recommendations ({0})", eRecs.Length);

        // get all history associated to this survey
        SurveyHistory[] history = SurveyHistory.GetSortedArray("SurveyID = ?  && IsRemoved = false", "EntryDate DESC", _eSurvey.ID);
        repHistory.DataSource = history;
        repHistory.DataBind();
        boxHistory.Title = string.Format("History ({0})", history.Length);
        boxHistory.Visible = CurrentUser.Permissions.CanViewSurveyHistory;

        // get all the doc types for the company
        DocType[] docTypes = DocType.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", this.CurrentUser.CompanyID);
        ComboHelper.BindCombo(cboDocType, docTypes, "Code", "{0} - {1}", "Code|Description".Split('|'));
        if (docTypes.Length > 0)
        {
            cboDocType.Items.Insert(0, new ListItem("-- select doc type --", ""));
        }

        // determine available actions the user can take on this survey
        SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
        SurveyAction[] actions = manager.GetAvailableActions();

        _usesPlanObjectives = (Utility.GetCompanyParameter("UsesPlanObjectives", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;

        if (_usesPlanObjectives)
        {
            // get Plan objectives
            Guid planId = ARMSUtility.GetGuidFromQueryString("planid", false);
            Guid surveyId = ARMSUtility.GetGuidFromQueryString("surveyid", false);
            List<Objective> planobjectives = Objective.GetSortedArray("ServicePlanID = ? && isNull(SurveyID)", "Number ASC", planId).ToList();
            List<Objective> surveyObjectives = Objective.GetSortedArray("SurveyID = ?", "Number ASC", surveyId).ToList();

            //Objective[] cd = from c in planobjectives
            //            where !(from o in surveyObjectives
            //           select o.ServicePlanID).Contains(c.ServicePlanID) select c;

            //Objective[] bb = planobjectives.Where(i => !surveyObjectives.Contains(i)).ToArray();

            //Objective[] res = planobjectives.Where(item => (surveyObjectives.Where(sp => sp.ServicePlanID == item.ServicePlanID)).ToArray();
            //List<Objective> ls = planobjectives.RemoveAll(item => surveyObjectives.Select(c => c.ServicePlanID.ToString()) = item.ServicePlanID.ToString());

            ComboHelper.BindCombo(ddlAvailablePlanObjectives, planobjectives, "ID", "Text");
            if (planobjectives != null && planobjectives.Count > 0)
            {
                ddlAvailablePlanObjectives.Items.Insert(0, new ListItem("-- select plan objective --", ""));
            }

        }

        if (!this.IsPostBack)
        {
            //HandleCompletedCall labeling
            _handleCompletedCallLabel = Berkley.BLC.Business.Utility.GetCompanyParameter("HandleCompletedCallLabel", CurrentUser.Company);
            if (!string.IsNullOrWhiteSpace(_handleCompletedCallLabel))
                boxCompletedCall.Title = _handleCompletedCallLabel;

            //Hide UW non completed details
            _uwHideNonCompletedDetails = Berkley.BLC.Business.Utility.GetCompanyParameter("HideUWNonCompletedDetails", CurrentUser.Company);
            if (_uwHideNonCompletedDetails.ToString().ToLower() == "true" && _eSurvey.CompleteDate == DateTime.MinValue && CurrentUser.IsUnderwriter)
            {
                boxNotes.Visible = false;
                boxDocs.Visible = false;
                boxObjectives.Visible = false;
                boxServicePlanObjectives.Visible = false;
                boxRecs.Visible = false;
                boxHistory.Visible = false;
            }

            //Hide UW completed details
            _uwHideCompletedDetails = Berkley.BLC.Business.Utility.GetCompanyParameter("HideUWNonCompletedDetails", CurrentUser.Company);
            if (_uwHideCompletedDetails.ToString().ToLower() == "true" && _eSurvey.CompleteDate != DateTime.MinValue && CurrentUser.IsUnderwriter)
            {
                boxNotes.Visible = false;
                boxHistory.Visible = false;
            }
        }
        // turn on (or show) the actions available
        EnableActions(actions);
    }

    private void EnableActions(SurveyAction[] eActions)
    {
        //// reset integrated actions
        divAddNote.Visible = false;
        divUploadDoc.Visible = false;
        lnkNonProductive.Visible = false;
        lnkFlagForReview.Visible = false;
        grdObjectives.Visible = false;
        listItemInsuredLocations.Visible = false;

        //// configure page for each integrated action in list
        ArrayList nonIntegrated = new ArrayList(eActions.Length);
        ArrayList otherActions = new ArrayList();
        foreach (SurveyAction eAction in eActions)
        {
            if (eAction.TypeCode == SurveyActionType.AddNote.Code)
            {
                //if just added note, keep the focus there
                if (ARMSUtility.GetIntFromQueryString("note", false).Equals(1))
                {
                    JavaScript.SetInputFocus(lnkAddNote);
                }

                divAddNote.Visible = true;
            }
            else if (eAction.TypeCode == SurveyActionType.AddDocument.Code)
            {
                divUploadDoc.Visible = true;
            }
            else if (eAction.TypeCode == SurveyActionType.EditInsured.Code ||
                    eAction.TypeCode == SurveyActionType.RemoveDocument.Code ||
                    eAction.TypeCode == SurveyActionType.RemoveRec.Code ||
                    eAction.TypeCode == SurveyActionType.RemoveNote.Code ||
                    eAction.TypeCode == SurveyActionType.RemoveHistory.Code ||
                    eAction.TypeCode == SurveyActionType.EditPolicy.Code ||
                    (eAction.TypeCode == SurveyActionType.NotifyUnderwriting.Code && _eSurvey.UnderwriterNotified) ||
                    (eAction.TypeCode == SurveyActionType.Close.Code && !_eSurvey.UnderwriterNotified && CurrentUser.CompanyID != Company.UnionStandardInsuranceGroup.ID) ||
                    (eAction.TypeCode == SurveyActionType.CloseAndNotify.Code && _eSurvey.UnderwriterNotified && CurrentUser.CompanyID != Company.ContinentalWesternGroup.ID) ||
                    (eAction.TypeCode == SurveyActionType.AccountManagement.Code && CurrentUser.Company.SupportsSimplifiedAcctMgmt) ||
                    //(eAction.TypeCode == SurveyActionType.ChangePrimaryPolicy.Code && SurveyTypeType.IsNotWrittenBusiness(_eSurvey.Type.Type)) ||
                    (eAction.TypeCode == SurveyActionType.ReviewQCR.Code && _eSurvey.ConsultantUser != null && CurrentUser.ID != _eSurvey.ConsultantUserID) ||
                    (eAction.TypeCode == SurveyActionType.ReviewQCR.Code && SurveyReview.GetOne("SurveyID = ? && ReviewerTypeCode = ?", _eSurvey.ID, ReviewerType.Manager.Code) == null) ||
                    (eAction.TypeCode == SurveyActionType.UWQuestionnaire.Code && SurveyReview.GetOne("SurveyID = ? && ReviewerTypeCode = ?", _eSurvey.ID, ReviewerType.Underwriter.Code) == null) || 
                    (eAction.TypeCode == SurveyActionType.AcknowledgeByUW.Code && _eSurvey.UnderwritingAcceptDate != DateTime.MinValue))
            {
                //do nothing, action is not used on this screen
            }
            else if (eAction.TypeCode == SurveyActionType.SendForReview.Code ||
                    eAction.TypeCode == SurveyActionType.SetSurveyDetails.Code)
            {
                //populate complete actions repeater
                if (eAction.TypeCode == SurveyActionType.SendForReview.Code)
                {
                    eAction.DisplayName = (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Complete Service Visit" : "Complete Request";
                }
                else
                {
                    eAction.DisplayName = (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Set Visit Details" : "Set Survey Details";
                }

                otherActions.Add(eAction);
            }
            else if (eAction.TypeCode == SurveyActionType.ChangePrimaryPolicy.Code)
            {
                if (!String.IsNullOrEmpty(_eSurvey.WorkflowSubmissionNumber))
                {
                    eAction.DisplayName = "Change Submission Number";
                }
                
                nonIntegrated.Add(eAction);
            }
            else if (eAction.TypeCode == SurveyActionType.ReopenSurvey.Code)
            {
                eAction.DisplayName = (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Reopen Service Visit" : "Reopen Survey";
                nonIntegrated.Add(eAction);
            }
            else if (eAction.TypeCode == SurveyActionType.AcceptSurvey.Code)
            {
                eAction.DisplayName = (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Accept Visit" : "Accept Survey";
                nonIntegrated.Add(eAction);
            }
            else if (eAction.TypeCode == SurveyActionType.ChangeSurveyType.Code)
            {
                eAction.DisplayName = (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Change Visit Type" : "Change Survey Type";
                nonIntegrated.Add(eAction);
            }
            else if (eAction.TypeCode == SurveyActionType.CancelSurvey.Code)
            {
                eAction.DisplayName = (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Cancel Service Visit" : "Cancel Survey";
                nonIntegrated.Add(eAction);
            }
            else if (eAction.TypeCode == SurveyActionType.NonProductive.Code)
            {
                lnkNonProductive.Visible = true;
                lblNonProductive.Visible = false;
            }
            else if (eAction.TypeCode == SurveyActionType.Review.Code)
            {
                lnkFlagForReview.Visible = true;
                lblFlagForReview.Visible = false;
            }
            else if (eAction.TypeCode == SurveyActionType.ManageObjectives.Code)
            {
                lnkAddObjective.Visible = true;
                lnkAddObjective.HRef = string.Format("ObjectiveEdit.aspx{0}", ARMSUtility.GetQueryStringForNav("objectiveid"));
                grdObjectives.Visible = true;
                repObjectives.Visible = false;
            }
            else if (eAction.TypeCode == SurveyActionType.ManageLocations.Code)
            {
                listItemInsuredLocations.Visible = true;
            }
            else // not integrated
            {
                nonIntegrated.Add(eAction);
            }
        }

        // bind the actions repeater with what is left
        repActions.DataSource = nonIntegrated;
        repActions.DataBind();

        // bind the "complete" actions
        repCompleteActions.DataSource = otherActions;
        repCompleteActions.DataBind();
    }

    private void ShowActionControl(Control ctrl)
    {
        this.repActions.Visible = (ctrl == this.repActions);
        this.divAssign.Visible = (ctrl == this.divAssign);
        this.divSetDetails.Visible = (ctrl == this.divSetDetails);
        this.divType.Visible = (ctrl == this.divType);
        this.divDueDate.Visible = (ctrl == this.divDueDate);
        this.divApproveDueDate.Visible = (ctrl == this.divApproveDueDate);
        this.divAssignedDate.Visible = (ctrl == this.divAssignedDate);
        this.divAcceptedDate.Visible = (ctrl == this.divAcceptedDate);
        this.divReportSubmittedDate.Visible = (ctrl == this.divReportSubmittedDate);
        this.divCompleteDate.Visible = (ctrl == this.divCompleteDate);
        this.divLetters.Visible = (ctrl == this.divLetters);
        this.divReports.Visible = (ctrl == this.divReports);
        this.divMailReceivedDate.Visible = (ctrl == this.divMailReceivedDate);
        this.divMailSentDate.Visible = (ctrl == this.divMailSentDate);
        this.divCancel.Visible = (ctrl == this.divCancel);
        this.divCloseAndNotify.Visible = (ctrl == this.divCloseAndNotify);
        this.divReleaseOwnership.Visible = (ctrl == this.divReleaseOwnership);
        this.divQA.Visible = (ctrl == this.divQA);
        this.divCreateNewSurvey.Visible = (ctrl == this.divCreateNewSurvey);
        this.divChangeSurveyPriority.Visible = (ctrl == this.divChangeSurveyPriority);
        this.divNotifyUW.Visible = (ctrl == this.divNotifyUW);
        this.divChangePrimaryPolicy.Visible = (ctrl == this.divChangePrimaryPolicy);
        this.divEmailUW.Visible = (ctrl == this.divEmailUW);
        
    }

    private void ShowCompleteActionControl(Control ctrl)
    {
        this.repCompleteActions.Visible = (ctrl == this.repCompleteActions);
        this.divSetDetails.Visible = (ctrl == this.divSetDetails);
        this.divCompleteRequest.Visible = (ctrl == this.divCompleteRequest);
    }

    void repActions_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // we only care about repeater items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // see if this action has a confirm prompt specified
            SurveyAction action = (SurveyAction)e.Item.DataItem;

            // attach the message to the button control
            LinkButton btn = (LinkButton)e.Item.FindControl("btnAction");

            if (action.Type == SurveyActionType.CloseAndNotify && _eSurvey.MailReceivedDate == DateTime.MinValue && !SurveyTypeType.IsNotWrittenBusiness(_eSurvey.Type.Type) && _eSurvey.Status != SurveyStatus.AssignedSurvey && _eSurvey.ServiceType == ServiceType.SurveyRequest)
            {
                JavaScript.AddConfirmationNoticeToWebControl(btn, "Are you sure you want to close this survey without setting the mail received date?");
            }
            else if (action.Type == SurveyActionType.ReleaseOwnership && _eSurvey.Status == SurveyStatus.AssignedSurvey && _eSurvey.AssignedUser.IsFeeCompany)
            {
                JavaScript.AddConfirmationNoticeToWebControl(btn, "Please note that the survey will be in 'Returning to Company' status awaiting for the vendor to acknowledge the release of ownership.");
            }
            else if (action.ConfirmMessage != null && action.ConfirmMessage.Trim().Length > 0)
            {
                JavaScript.AddConfirmationNoticeToWebControl(btn, action.ConfirmMessage);
            }
        }
    }

    void rdoUserType_SelectedIndexChanged(object sender, EventArgs e)
    {
        PrepForEmailUW();
    }
    void rdlUserType_SelectedIndexChanged(object sender, EventArgs e)
    {
        PrepForReopen();
    }


    void repCompleteActions_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // we only care about repeater items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // see if this action has a confirm prompt specified
            SurveyAction action = (SurveyAction)e.Item.DataItem;

            // attach the message to the button control
            LinkButton btn = (LinkButton)e.Item.FindControl("btnCompleteAction");

            if (action.ConfirmMessage != null && action.ConfirmMessage.Trim().Length > 0)
            {
                JavaScript.AddConfirmationNoticeToWebControl(btn, action.ConfirmMessage);
            }
            else if (_eSurvey.ServiceType == ServiceType.ServiceVisit && action.Type == SurveyActionType.SendForReview && !CurrentUser.Company.OptionToReviewIsVisible)
            {
                JavaScript.AddConfirmationNoticeToWebControl(btn, "Are you sure you want to complete this service visit?");
            }
        }
    }

    void repActions_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string actionID = e.CommandName;
        SurveyAction action = SurveyAction.Get(new Guid(actionID));
        TakeAction(action);
    }

    void repCompleteActions_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string actionID = e.CommandName;
        SurveyAction action = SurveyAction.Get(new Guid(actionID));
        TakeAction(action);
    }

    private void TakeAction(SurveyAction action)
    {
        try
        {
            // determine what action to take based on the type
            SurveyActionType actionType = action.Type;
            if (actionType == SurveyActionType.EditDueDate)
            {
                PrepForDueDateChange();
                ShowActionControl(this.divDueDate);
            }
            else if (actionType == SurveyActionType.ApproveDueDate)
            {
                PrepForApproveDueDate();
                ShowActionControl(this.divApproveDueDate);
            }
            else if (actionType == SurveyActionType.EditReportSubmittedDate)
            {
                PrepForReportSubmittedDateChange();
                ShowActionControl(this.divReportSubmittedDate);
            }
            else if (actionType == SurveyActionType.EditCompleteDate)
            {
                PrepForCompleteDateChange();
                ShowActionControl(this.divCompleteDate);
            }
            else if (actionType == SurveyActionType.EditAssignedDate)
            {
                PrepForAssignedDateChange();
                ShowActionControl(this.divAssignedDate);
            }
            else if (actionType == SurveyActionType.EditAcceptedDate)
            {
                PrepForAcceptedDateChange();
                ShowActionControl(this.divAcceptedDate);
            }
            else if (actionType == SurveyActionType.AssignSurvey)
            {
                PrepForAssignment();
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divAssign);
            }
            else if (actionType == SurveyActionType.TakeOwnership)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.TakeOwnership(this.CurrentUser);
                Response.Redirect(GetReturnUrl(), true);
            }
            else if (actionType == SurveyActionType.AcceptSurvey)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.Accept();
                Response.Redirect(GetReturnUrl(), true);
            }
            else if (actionType == SurveyActionType.SetSurveyDetails)
            {
                PrepForSettingDetails();
                ShowCompleteActionControl(this.divSetDetails);
            }
            else if (actionType == SurveyActionType.AssignSurveyTerritory)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                if (!manager.AssignBasedOnTerritory())
                {
                    string msg;
                    if (_eSurvey.StatusCode == SurveyStatus.UnassignedReview.Code)
                        msg = "This survey could not be assigned based on the current review territory assignments.";
                    else
                        msg = "This survey could not be assigned based on the current consultant territory assignments.";

                    JavaScript.ShowMessage(this, msg);
                    return;
                }
                else
                {
                    Response.Redirect(GetReturnUrl(), true);
                }
            }
            else if (actionType == SurveyActionType.ReleaseOwnership)
            {
                PrepForReleaseOwnership();
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divReleaseOwnership);
            }
            else if (actionType == SurveyActionType.SendForReview)
            {
                string strBuilder = ValidateCompletion();
                if (strBuilder != string.Empty)
                {
                    throw new FieldValidationException("The following outstanding items still need to be complete:\n\n" + strBuilder);
                }

                if ((_eSurvey.ServiceType == ServiceType.ServiceVisit && !CurrentUser.Company.OptionToReviewIsVisible) || _eSurvey.Correction)
                {
                    SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                    manager.SendForReview(true);
                    Response.Redirect(GetReturnUrl(), true);
                }
                else
                {
                    //Modified by Vilas Meshram: 02/12/2013 : To Hide the Future Visits Needed : Yes/No option and submit it directly.
                    if (Utility.GetCompanyParameter("HideFutureVisits", CurrentUser.Company).ToUpper() == "TRUE")
                    {
                        SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                        manager.SendForReview(true);
                        Response.Redirect(GetReturnUrl(), true);
                    }
                    else
                    {
                        PrepForCompleteSurvey();
                        this.ViewState["ActionTypeCode"] = actionType.Code;
                        ShowCompleteActionControl(this.divCompleteRequest);
                }
            }
            }
            else if (actionType == SurveyActionType.Reject)
            {
                PrepForReturn();
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divAssign);
            }
            else if (actionType == SurveyActionType.ChangePrimaryPolicy)
            {
                PrepForPolicyChange();
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divChangePrimaryPolicy);
            }
            else if (actionType == SurveyActionType.ChangeSurveyType)
            {
                PrepForTypeChange();
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divType);
            }
            else if (actionType == SurveyActionType.CancelSurvey)
            {
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divCancel);
            }
            else if (actionType == SurveyActionType.CloseAndNotify)
            {
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divCloseAndNotify);
            }
            else if (actionType == SurveyActionType.Close)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.Close();
                Response.Redirect(GetReturnUrl(), true);
            }
            else if (actionType == SurveyActionType.AcknowledgeByUW)
            {
                string userName = (!string.IsNullOrEmpty(Request.ServerVariables["LOGON_USER"])) ? Request.ServerVariables["LOGON_USER"].Replace(CurrentUser.DomainName + "\\", "") : string.Empty;
                
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.AcceptByUW(userName);
                Response.Redirect(GetReturnUrl(), true);
            }
            else if (actionType == SurveyActionType.DeleteSurvey)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.Delete();

                //Determine where to redirect
                string returnURL;
                if (HttpContext.Current.Request.QueryString.Get("NavFromSearch") != null)
                {
                    returnURL = string.Format("SearchResults.aspx{0}", ARMSUtility.GetQueryStringForNav("surveyid"));
                }
                else if (HttpContext.Current.Request.QueryString.Get("NavFromPlan") != null)
                {
                    returnURL = string.Format("ServicePlan.aspx{0}", ARMSUtility.GetQueryStringForNav("surveyid"));
                }
                else
                {
                    returnURL = string.Format("Surveys.aspx{0}", ARMSUtility.GetQueryStringForNav("surveyid"));
                }

                Response.Redirect(returnURL, true);
            }
            else if (actionType == SurveyActionType.ReopenSurvey)
            {
                PrepForReopen();
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divAssign);
            }
            else if (actionType == SurveyActionType.CreateServicePlan)
            {
                Response.Redirect(string.Format("CreateServicePlanSearch.aspx{0}", ARMSUtility.GetQueryStringForNav()), true);
            }
            else if (actionType == SurveyActionType.ManageRecs)
            {
                Response.Redirect(string.Format("recommendations.aspx{0}", ARMSUtility.GetQueryStringForNav()), true);
            }
            else if (actionType == SurveyActionType.ManageObjectives)
            {
                Response.Redirect(string.Format("objectives.aspx{0}", ARMSUtility.GetQueryStringForNav()), true);
            }
            else if (actionType == SurveyActionType.AccountManagement)
            {
                Response.Redirect(string.Format("SurveyActivities.aspx{0}", ARMSUtility.GetQueryStringForNav()), true);
            }
            else if (actionType == SurveyActionType.PerformQCR || actionType == SurveyActionType.ReviewQCR || actionType == SurveyActionType.ManageRatings)
            {
                Response.Redirect(string.Format("SurveyReview.aspx{0}&reviewertype={1}", ARMSUtility.GetQueryStringForNav(), ReviewerType.Manager.Code), true);
            }
            else if (actionType == SurveyActionType.UWQuestionnaire)
            {
                Response.Redirect(string.Format("SurveyReview.aspx{0}&reviewertype={1}", ARMSUtility.GetQueryStringForNav(), ReviewerType.Underwriter.Code), true);
            }
            else if (actionType == SurveyActionType.GenerateLetter)
            {
                PrepForGenerateLetters();
                ShowActionControl(this.divLetters);
            }
            else if (actionType == SurveyActionType.DownloadReport)
            {
                PrepForGenerateReports();
                ShowActionControl(this.divReports);
            }
            else if (actionType == SurveyActionType.SetMailReceived)
            {
                PrepForMailReceivedDateChange();
                ShowActionControl(this.divMailReceivedDate);
            }
            else if (actionType == SurveyActionType.SetMailSent)
            {
                PrepForMailSentDateChange();
                ShowActionControl(this.divMailSentDate);
            }
            else if (actionType == SurveyActionType.QualityAssessment)
            {
                PrepForQA();
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divQA);
            }
            else if (actionType == SurveyActionType.CreateNewSurvey)
            {
                PrepForCreateNewSurvey();
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divCreateNewSurvey);
            }
            else if (actionType == SurveyActionType.NotifyUnderwriting)
            {
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divNotifyUW);
            }
            else if (actionType == SurveyActionType.EmailUW)
            {
                PrepForEmailUW();
                ShowActionControl(this.divEmailUW);
            }            
            else if (actionType == SurveyActionType.ChangeSurveyPriority)
            {
                PrepForPriorityChange();
                BindImagePriority();
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divChangeSurveyPriority);
            }

            else
            {
                throw new NotSupportedException("Action Type '" + actionType.Code + "' was not expected.");
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
    }

    private string ValidateCompletion()
    {
        bool usesAcctMgmt = CurrentUser.Role.PermissionSet.CanTakeAction(SurveyActionType.AccountManagement, _eSurvey, CurrentUser) || CurrentUser.Company.SupportsSimplifiedAcctMgmt || _displayActivityTimes;
        string strBuilder = string.Empty;

        if (_eSurvey.SurveyedDate == DateTime.MinValue ||
            (!usesAcctMgmt && _eSurvey.Hours == Decimal.MinValue) ||
            (_eSurvey.Company.SupportsOverallGrading && _eSurvey.GradingCode == string.Empty) ||
            (_eSurvey.Company.SupportsSeverityRating && _eSurvey.SeverityRatingCode == string.Empty) ||
            (_eSurvey.Company.SupportsCallCount && _eSurvey.CallCount == Decimal.MinValue))
        {
            strBuilder += string.Format("All of the {0} details must be entered.\n", (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "visit" : "survey");
        }

        //Check if there are any required recs still open
        SurveyRecommendation[] requiredRecs = SurveyRecommendation.GetArray("SurveyID = ? && RecStatus.Type.Code = ? && RecClassification.RequiredOnSurveyCompletion = true", _eSurvey.ID, RecStatusType.Open.Code);
        if (requiredRecs.Length > 0)
        {
            strBuilder += "All advisory recs must be closed.\n";
        }
        else
        {
            //Check if Recs are required and if any were entered
            if (_eSurvey.RecsRequired)
            {
                SurveyRecommendation[] recs = SurveyRecommendation.GetArray("SurveyID = ?", _eSurvey.ID);
                if (recs.Length <= 0)
                {
                    strBuilder += "Recs need to be added since they were marked as being required.\n";
                }
            }
        }


        foreach (SurveyReasonType reasontype in _eSurvey.ReasonTypes)
            if (reasontype.Name == "Rec Check" && !_eSurvey.UWNotified && (Berkley.BLC.Business.Utility.GetCompanyParameter("ShowUWNotified", CurrentUser.Company) == "true"))
            {
                strBuilder += "Underwriter must be notified of Rec Check results.\n";                
            }
        

        //Check if user uses Account Mangement and if any activity times were entered
        if (usesAcctMgmt)
        {
            ActivityType[] activityTypes = ActivityType.GetArray("CompanyID = ?", CurrentUser.CompanyID);
            SurveyActivity[] activities = SurveyActivity.GetArray("SurveyID = ?", _eSurvey.ID);
            if (activities.Length <= 0 && activityTypes.Length > 0)
            {
                if (CurrentUser.Company.SupportsSimplifiedAcctMgmt || _displayActivityTimes)
                {
                    strBuilder += string.Format("Activity times must be entered under the {0} details section.\n", (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "visit" : "survey");
                }
                else
                {
                    strBuilder += "Activity times must be entered using account management.\n";
                }
            }
        }

        //Check if this user attached any docs before completing (looking for reports)
        SurveyDocument[] docs = SurveyDocument.GetArray("SurveyID = ?", _eSurvey.ID);
        if (CurrentUser.Company.RequireDocumentUpload && docs.Length <= 0)
        {
            strBuilder += "All of the necessary documents must be attached.\n";
        }

        //Check for open objectives
        Objective[] objectives = Objective.GetArray("SurveyID = ? && StatusCode = ?", _eSurvey.ID, ObjectiveStatus.Open.Code);
        if (objectives.Length > 0)
        {
            strBuilder += "All of the open objectives must be complete.";
        }

        return strBuilder;
    }

    protected void btnRemoveObjective_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this objective?");
    }

    void grdObjectives_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            // get the ID of the territory selected
            Guid id = (Guid)grdObjectives.DataKeys[e.Item.ItemIndex];

            // remove the objective
            Objective eObjective = Objective.Get(id);
            if (eObjective != null)
            {
                AttributeHelper.DeleteObjectiveAttributes(eObjective);
                eObjective.Delete();

                //Update the survey history
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
                manager.ObjectiveManagement(eObjective, SurveyManager.UIAction.Remove);
            }
        }

        Response.Redirect(string.Format("Survey.aspx{0}", ARMSUtility.GetQueryStringForNav()));
    }

    void grdPlanObjectives_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            // get the ID of the territory selected
            Guid id = (Guid)grdPlanObjectives.DataKeys[e.Item.ItemIndex];

            // remove the objective
            Objective eObjective = Objective.Get(id);
            if (eObjective != null)
            {
                AttributeHelper.DeleteObjectiveAttributes(eObjective);
                eObjective.Delete();

                //Update the survey history
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
                manager.ObjectiveManagement(eObjective, SurveyManager.UIAction.Remove);
            }
        }

        Response.Redirect(string.Format("Survey.aspx{0}", ARMSUtility.GetQueryStringForNav()));
    }

    protected void lnkDownloadDoc_OnClick(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            SurveyDocument doc = SurveyDocument.Get(new Guid(lnk.Attributes["DocumentID"]));

            StreamDocument docRetriever = new StreamDocument(doc, CurrentUser.Company);
            docRetriever.AttachDocToResponse(this, docRetriever.DocMimeType);   //WA-621 Doug Bruce 2015-05-05 - Added MimeType for correct display.
        }
        catch (System.Web.HttpException ex)
        {
            //ignore http exceptions for when the user quits a doc download and the stream doesn't get flushed
            if (ex.Message.Contains("The remote host closed the connection"))
            {
                //swallow exception
            }
            else
            {
                throw new Exception("See inner exception for details.", ex);
            }
        }
    }

    #region ACTION: Assignment

    private void PrepForAssignment()
    {
        User[] eUsers;

        //filter
        StringBuilder sb = new StringBuilder();
        sb.Append("AccountDisabled = false && ");
        sb.Append("(Role[PermissionSet.SurveyActionPermissions[");
        sb.Append("(Setting = ? || Setting = ? || Setting = ? || Setting = ?) && ");
        sb.Append("ServiceTypeCode = ? && SurveyAction.SurveyStatusCode = ?]]) ");


        //params
        ArrayList args = new ArrayList();
        args.Add(PermissionSet.SETTING_OWNER);
        args.Add(PermissionSet.SETTING_OWNER_AND_OTHERS);
        args.Add(PermissionSet.SETTING_OWNER_AND_VENDORS);
        args.Add(PermissionSet.SETTING_ALL);
        args.Add(_eSurvey.ServiceTypeCode);

        // get all available users based on permissions
        if (_eSurvey.Status.Type == SurveyStatusType.Survey)
        {
            args.Add(SurveyStatus.AssignedSurvey.Code);
            sb.Append("|| (IsFeeCompany = true && AccountDisabled = false && CompanyID = ?)");
            args.Add(CurrentUser.CompanyID);

            eUsers = this.CurrentUser.Company.GetConsultants(sb.ToString(), args.ToArray());
        }
        else if (_eSurvey.Status.Type == SurveyStatusType.Review)
        {
            args.Add(SurveyStatus.AssignedReview.Code);
            eUsers = this.CurrentUser.Company.GetReviewStaff(sb.ToString(), args.ToArray());
        }
        else
        {
            throw new NotSupportedException("Survey status '" + _eSurvey.StatusCode + "' was not expected.");
        }

        // populate the combo
        ComboHelper.BindCombo(cboUsers, eUsers, "ID", "Name");
        cboUsers.Items.Insert(0, new ListItem("-- select user --", ""));

        // set the assign label
        lblAssign.Text = "Assign to:";
    }

    private void PrepForReturn()
    {
        // populate combo with all consultants
        User[] eUsers;

        //filter
        StringBuilder sb = new StringBuilder();
        sb.Append("AccountDisabled = false && ");
        sb.Append("(Role[PermissionSet.SurveyActionPermissions[");
        sb.Append("(Setting = ? || Setting = ? || Setting = ? || Setting = ?) && ");
        sb.Append("ServiceTypeCode = ? && SurveyAction.SurveyStatusCode = ?]]) ");


        //params
        ArrayList args = new ArrayList();
        args.Add(PermissionSet.SETTING_OWNER);
        args.Add(PermissionSet.SETTING_OWNER_AND_OTHERS);
        args.Add(PermissionSet.SETTING_OWNER_AND_VENDORS);
        args.Add(PermissionSet.SETTING_ALL);
        args.Add(_eSurvey.ServiceTypeCode);

        // get all available users based on permissions
        if (_eSurvey.Status.Type == SurveyStatusType.Review)
        {
            args.Add(SurveyStatus.AssignedSurvey.Code);
            sb.Append("|| (IsFeeCompany = true && CompanyID = ?)");
            args.Add(CurrentUser.CompanyID);

            eUsers = this.CurrentUser.Company.GetConsultants(sb.ToString(), args.ToArray());
        }
        else
        {
            throw new NotSupportedException("Survey status '" + _eSurvey.StatusCode + "' was not expected.");
        }

        ComboHelper.BindCombo(cboUsers, eUsers, "ID", "Name");
        cboUsers.Items.Insert(0, new ListItem("-- select consultant --", ""));

        // try to select the original auditor
        if (_eSurvey.ConsultantUserID != Guid.Empty)
        {
            ComboHelper.SelectComboItem(cboUsers, _eSurvey.ConsultantUserID.ToString());
        }

        // set the assign label
        lblAssign.Text = string.Format("Return {0} to:", (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "visit" : "survey");
    }

    void btnAssign_Click(object sender, EventArgs e)
    {
        try
        {
            if (cboUsers.SelectedValue.Length == 0)
            {
                throw new FieldValidationException(cboUsers, "Please select a user to assign.");
            }

            Guid userID = new Guid(cboUsers.SelectedValue);
            User targetUser = Berkley.BLC.Entities.User.Get(userID);

            string code = (string)this.ViewState["ActionTypeCode"];
            SurveyActionType actionType = SurveyActionType.Get(code);

            if (actionType == SurveyActionType.AssignSurvey)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.Assign(targetUser);
            }
            else if (actionType == SurveyActionType.Reject)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.ReturnForCorrection(targetUser);
            }
            else if (actionType == SurveyActionType.ReopenSurvey)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.Reopen(targetUser);
            }
            else
            {
                throw new NotSupportedException("Action Type '" + actionType.Code + "' was not expected.");
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnAssignCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Reopen

    private void PrepForReopen()
    {

        if(_showUserOption)
        {
            if (rdlUserType.SelectedIndex.Equals(0))
            {
                User[] users = this.CurrentUser.Company.GetConsultants(null, null);
            ComboHelper.BindCombo(cboUsers, users, "ID", "Name");
            cboUsers.Items.Insert(0, new ListItem("-- select user --", ""));
            }
            else
            {
                User[] users = this.CurrentUser.Company.GetReviewStaff(null, null);
                ComboHelper.BindCombo(cboUsers, users, "ID", "Name");
                cboUsers.Items.Insert(0, new ListItem("-- select reviewer --", ""));
            }
        }
        else
        {
            User[] users = this.CurrentUser.Company.GetActiveUsers();
            ComboHelper.BindCombo(cboUsers, users, "ID", "Name");
            cboUsers.Items.Insert(0, new ListItem("-- select user --", ""));
        }

    
        // set the assign label
        lblAssign.Text = string.Format("Assign reopened {0} to:", (_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "visit" : "survey");
    }

    #endregion

    #region ACTION: Cancel

    void btnCancellation_Click(object sender, EventArgs e)
    {
        try
        {
            string text = FieldValidation.ValidateStringField(txtCancellationExplanation, "Reason for Cancellation", 1600, true);

            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
            manager.Cancel(text);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnCancellationCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: CloseAndNotify

    void btnCloseAndNotify_Click(object sender, EventArgs e)
    {
        try
        {
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
            manager.CloseAndNotify(txtNotifyInstructions.Text);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnCloseAndNotifyCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Notify UW

    void btnNotifyUW_Click(object sender, EventArgs e)
    {
        try
        {

            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
            manager.NotifyUW(txtNotifyUWInstructions.Text);

            // show the actions list again
            Response.Redirect(GetReturnUrl(), true);

        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
    }

    void btnNotifyUWCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Email UW

    private void PrepForEmailUW()
    {
                
        if (rdoUserType.SelectedIndex.Equals(1))
        {
            User[] reps = Berkley.BLC.Entities.User.GetSortedArray("CompanyID = ? && AccountDisabled = False && EmailAddress != ? && IsFeeCompany = False", "Name ASC", CurrentUser.CompanyID, string.Empty);
            ComboHelper.BindCombo(cboUnderwriters, reps, "ID", "Name");
            cboUnderwriters.Items.Insert(0, new ListItem("-- select user --", ""));
        }
        else
        {
            Underwriter[] underwriters = Underwriter.GetSortedArray("CompanyID = ? && Disabled = False && EmailAddress != ?", "Name ASC", CurrentUser.CompanyID, string.Empty);
            ComboHelper.BindCombo(cboUnderwriters, underwriters, "Code", "Name");
            cboUnderwriters.Items.Insert(0, new ListItem("-- select underwriter --", ""));
        }

        //Underwriter[] underwriters = Underwriter.GetSortedArray("CompanyID = ? && Disabled = False && EmailAddress != ?", "Name ASC", CurrentUser.CompanyID, string.Empty);
        //ComboHelper.BindCombo(cboUnderwriters, underwriters, "Code", "Name");

        

        // try to select the underwriter for the account
        if (_eSurvey.Insured.Underwriter != string.Empty)
        {
            ComboHelper.SelectComboItem(cboUnderwriters, _eSurvey.Insured.Underwriter);
        }
    }
        

    void btnEmailUW_Click(object sender, EventArgs e)
    {
        try
        {
            if (rdoUserType.SelectedIndex.Equals(1))
            {
                if (cboUnderwriters.SelectedValue.Length == 0)
                {
                    throw new FieldValidationException(cboUnderwriters, "Please select a user to email.");
                }

                if (txtEmailForUW.Text.Length == 0)
                {
                    throw new FieldValidationException(cboUnderwriters, "Please enter the text to be sent to the user.");
                }
                
                //check if the UW record has a valid email
                 User reps = Berkley.BLC.Entities.User.GetOne("ID = ?", cboUnderwriters.SelectedValue);
                 if (!ARMSUtility.EmailRegex(reps.EmailAddress))
                 {
                     throw new FieldValidationException(cboUnderwriters, "The email address on record for this user is not valid.  Please contact an administrator.");
                 }

                 SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                 manager.EmailUser(reps, txtEmailForUW.Text);
            }
            else
            {
                if (cboUnderwriters.SelectedValue.Length == 0)
                {
                    throw new FieldValidationException(cboUnderwriters, "Please select an underwriter to email.");
                }

                if (txtEmailForUW.Text.Length == 0)
                {
                    throw new FieldValidationException(cboUnderwriters, "Please enter the text to be sent to the underwriter.");
                }

                //check if the UW record has a valid email
                Underwriter underwriter = Underwriter.Get(cboUnderwriters.SelectedValue, CurrentUser.CompanyID);
                if (!ARMSUtility.EmailRegex(underwriter.EmailAddress))
                {
                    throw new FieldValidationException(cboUnderwriters, "The email address on record for this underwriter is not valid.  Please contact an administrator.");
                }

                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.EmailUW(underwriter, txtEmailForUW.Text);
            }

            // show the actions list again
            Response.Redirect(GetReturnUrl(), true);

        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
    }

    void btnEmailUWCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion      

    #region ACTION: Change Survey Type

    private void PrepForTypeChange()
    {
        SurveyType[] types;
        if (_eSurvey.ServiceType == ServiceType.SurveyRequest)
        {
            types = SurveyType.GetRequestSurveyTypes(CurrentUser.Company);
        }
        else
        {
            types = SurveyType.GetServiceSurveyTypes(CurrentUser.Company);
        }

        ComboHelper.BindCombo(cboTypes, types, "TypeCode", "Type.Name");
        cboTypes.Items.Insert(0, new ListItem("-- select type --", ""));

        JavaScript.SetInputFocus(cboTypes);
    }

    void btnTypeUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            string code = cboTypes.SelectedValue;
            if (code.Length == 0)
            {
                throw new FieldValidationException(cboTypes, "Please select a new type first.");
            }

            // get the selected type
            SurveyTypeType type = SurveyTypeType.Get(code);

            // change the audit type
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
            manager.ChangeSurveyType(type);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnTypeCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion
    //20121111 Dustin
    #region ACTION: Change Survey Priority

    //Added By Vilas Meshram: 03/04/2013: Squish# 21365: Dropdown for priority
    private void PrepForPriorityChange()
    {
        var selectedPriority = _eSurvey.Priority;
        PriorityRating[] priorities = PriorityRating.GetSortedArray("CompanyID = ?", "DisplayOrder ASC, Name ASC", CurrentUser.CompanyID);

        ddlSurveyPriority.DataTextField = "Description";
        ddlSurveyPriority.DataValueField = "Name";
        ddlSurveyPriority.DataSource = priorities;
        ddlSurveyPriority.DataBind();

        foreach (ListItem item in ddlSurveyPriority.Items)
            if (item.Value == selectedPriority)
            {
                ddlSurveyPriority.SelectedValue = selectedPriority;
                break;
            }
    }

    //Commented By Vilas Meshram: 03/04/2013: Squish# 21365: using Dropdown for priority

    //private void PrepForPriorityChange()
    //{
    //    PriorityRating[] priorities = PriorityRating.GetSortedArray("CompanyID = ?", "DisplayOrder ASC, Name ASC", CurrentUser.CompanyID);
    //    var colors = (from p in priorities select p.Name).ToArray();
    //    rblPriority.DataSource = colors;
    //    rblPriority.DataBind();
    //    /*20121113 Dustin: displaying a list of images and allowing a user to select 1 was challenging.
    //     * while in a repeater control, radio buttons would not work properly, even if you applied the GroupName attribute you could select more than 1.
    //     * A radio button list using a background image was the best thing I could find with a little looking.  It's a hack, but it's effective.
    //     * I'm trying to be mindful of the time I spend on this problem.
    //   */
    //    foreach (ListItem item in rblPriority.Items)
    //    {
    //        var url = item.Text;
    //        item.Text = "";
    //        item.Text = "..";//this was needed to make sure the image wasn't behind the selector button
    //        item.Selected = _eSurvey.Priority.Equals(url);

    //        item.Attributes.CssStyle.Add("background-image", "url(" + url + ")");
    //        item.Attributes.CssStyle.Add("background-repeat", "no-repeat");
    //        item.Attributes.CssStyle.Add("background-position", "right top");
    //        item.Attributes.CssStyle.Add("font-size", "300%");//another hack, to make sure the images have enough height and are not covered with the text.
    //        item.Attributes.CssStyle.Add("color", "white");
    //    }
    //}


    /// <summary>
    /// This Method is used to bind titles to each element of dropdown
    /// Added By Vilas Meshram: 03/04/2013: Squish# 21365: Dropdown for priority:
    /// </summary>
    protected void BindImagePriority()
    {
        if (ddlSurveyPriority != null)
        {
            foreach (ListItem li in ddlSurveyPriority.Items)
            {
                 li.Attributes["title"] = li.Value.Substring(2); // setting text value of item as tooltip
            }
        }
    }

    protected void SavePriority_Click(object sender, EventArgs e)
    {
        try
        {
            var priority = string.Empty;

            //Modified By Vilas Meshram: 03/04/2013: Squish# 21365: Dropdown for priority
            if (ddlSurveyPriority.Items.Count > 0)
                priority = ddlSurveyPriority.SelectedValue;

            if (priority.Length == 0)
                throw new FieldValidationException(ddlSurveyPriority, "Please select a priority first.");

            //foreach (ListItem item in rblPriority.Items.Cast<ListItem>().Where(item => item.Selected))
            //    priority = item.Value;
            
            //if (priority.Length == 0)
            //    throw new FieldValidationException(rblPriority, "Please select a priority first.");


            //Modified By Vilas Meshram: 03/04/2013: Squish# 21365: Dropdown for priority

            // change the audit type
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
            manager.ChangeSurveyPriority(priority);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }
    void btnPriorityCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }
    #endregion

    #region ACTION: Release Ownership

    private void PrepForReleaseOwnership()
    {
        SurveyReleaseOwnershipType[] types = SurveyReleaseOwnershipType.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);

        ComboHelper.BindCombo(cboReleaseReasons, types, "ID", "Name");
        cboReleaseReasons.Items.Insert(0, new ListItem("-- select reason --", ""));
    }

    protected void cboReleaseReasons_SelectedIndexChanged(object sender, EventArgs e)
    {
        SurveyReleaseOwnershipType releaseType = null;
        if (cboReleaseReasons.SelectedValue.Length > 0)
        {
            releaseType = SurveyReleaseOwnershipType.Get(new Guid(cboReleaseReasons.SelectedValue));

            if (releaseType.Name =="Request Due Date Change" || releaseType.Name =="Request Extension") 
                newDueDate.Visible = true;
            else 
                newDueDate.Visible = false; 
        }
        else
            newDueDate.Visible = false; 
    }

    void btnReleaseOwnership_Click(object sender, EventArgs e)
    {
        try
        {
  
            //some validation
            if ((cboReleaseReasons.SelectedValue.Length <= 0 && _eSurvey.Status.Type == SurveyStatusType.Survey) ||
                txtReasonForReleaseOwnership.Text.Trim().Length > 500 || txtReasonForReleaseOwnership.Text.Trim().Length <= 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Please correct the following:");
                
                if (cboReleaseReasons.SelectedValue.Length <= 0 && _eSurvey.Status.Type == SurveyStatusType.Survey)
                {
                    sb.AppendLine("- Reason for releasing ownership is required.");
                }

                if (txtReasonForReleaseOwnership.Text.Trim().Length > 500)
                {
                    sb.AppendLine("- Comments cannot exceed 500 characters.");
                }
                else if (txtReasonForReleaseOwnership.Text.Trim().Length <= 0)
                {
                    sb.AppendLine("- Comments for releasing ownership are required.");
                }

                JavaScript.ShowMessage(this, sb.ToString());
                return;
            }

            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);

            SurveyReleaseOwnershipType releaseType = null;
            
            if (cboReleaseReasons.SelectedValue.Length > 0)
            {
                releaseType = SurveyReleaseOwnershipType.Get(new Guid(cboReleaseReasons.SelectedValue));

                if (releaseType.Name == "Request Due Date Change")
                    _releaseType = SurveyManager.ReleaseOwnershipType.RequestDueDateChange;
                else if (releaseType.Name == "Request Extension")
                    _releaseType = SurveyManager.ReleaseOwnershipType.RequestExtension;
            }

            //Added By Vilas Meshram: 02/19/2013: Squish #21347 :Due Date Extension
            if( _releaseType == SurveyManager.ReleaseOwnershipType.RequestDueDateChange || _releaseType==SurveyManager.ReleaseOwnershipType.RequestExtension)
            {
                DateTime date = dtpNewDueDate.Date;
                if (date == DateTime.MinValue)
                {
                    throw new FieldValidationException(dtpNewDueDate, "Please enter a new date.");
                }

                manager.ChangeDueDate(date, txtReasonForReleaseOwnership.Text.Trim(),_releaseType, rdoInternal.Checked, rdoAdministrator.Checked);
             }
            //Added By Vilas Meshram: 02/19/2013: Squish #21347 :Due Date Extensio
            else
                manager.ReleaseOwnership(releaseType, txtReasonForReleaseOwnership.Text,rdoInternal.Checked, rdoAdministrator.Checked);
            

        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnReleaseOwnershipCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    //Added By Vilas Meshram:02/21/2013: Squish #21347 :Due Date Extension
    protected void ChangeDateApprove_CheckedChanged(Object sender, EventArgs e)
    {
        if (rdoNewDate.Checked)
            dvApproveNewDate.Visible = true;
        else
            dvApproveNewDate.Visible = false;
    }


    #endregion

    #region ACTION: Complete Request

    private void PrepForCompleteSurvey()
    {
        //filter
        StringBuilder sb = new StringBuilder();
        sb.Append("AccountDisabled = false && ");
        sb.Append("(Role[PermissionSet.SurveyActionPermissions[");
        sb.Append("(Setting = ? || Setting = ? || Setting = ? || Setting = ?) && ");
        sb.Append("ServiceTypeCode = ? && SurveyAction.SurveyStatusCode = ?]]) ");
        sb.Append("|| (IsFeeCompany = true && CompanyID = ?)");

        //params
        ArrayList args = new ArrayList();
        args.Add(PermissionSet.SETTING_OWNER);
        args.Add(PermissionSet.SETTING_OWNER_AND_OTHERS);
        args.Add(PermissionSet.SETTING_OWNER_AND_VENDORS);
        args.Add(PermissionSet.SETTING_ALL);
        args.Add(_eSurvey.ServiceTypeCode);
        args.Add(SurveyStatus.AssignedSurvey.Code);
        args.Add(CurrentUser.CompanyID);

        User[] eUsers = this.CurrentUser.Company.GetConsultants(sb.ToString(), args.ToArray());
        ComboHelper.BindCombo(cboFutureUsers, eUsers, "ID", "Name");
        cboFutureUsers.SelectedValue = (_eSurvey.AssignedUser != null) ? _eSurvey.AssignedUserID.ToString() : CurrentUser.ID.ToString();
        
        rdoVisitYes.Attributes.Add("OnClick", "Visit()");
        rdoVisitNo.Attributes.Add("OnClick", "Visit()");

        // populate the combos
        SurveyType[] types = SurveyType.GetFutureSurveyTypes(CurrentUser.Company);
        ComboHelper.BindCombo(cboSurveyType, types, "ID", "Type.Name");

        // populate the combos
        SurveyReasonType[] reasontypes = SurveyReasonType.GetFutureSurveyReasonTypes(CurrentUser.Company);
        ComboHelper.BindCombo(cboSurveyReasonType, reasontypes, "ID", "Name");

        cboSurveyType.Items.Insert(0, new ListItem("-- select survey type --", ""));

        if (!_eSurvey.FutureVisitNeeded)
        {
            divFutureVisit.Attributes.Add("style", "DISPLAY: none");
        }
        else
        {
            rdoVisitYes.Enabled = false;
            rdoVisitNo.Enabled = false;
            dpFutureDateSurveyed.Enabled = false;
            cboSurveyType.Enabled = false;

            dpFutureDateSurveyed.Date = _eSurvey.FutureVisitDueDate;
            cboSurveyType.SelectedValue = _eSurvey.FutureVisitSurveyTypeID.ToString();
        }

        rdoVisitYes.Checked = _eSurvey.FutureVisitNeeded;
        rdoVisitNo.Checked = !_eSurvey.FutureVisitNeeded;

        if (CurrentUser.Company.OptionToReviewIsVisible)
        {
            SurveyRecommendation[] recs = SurveyRecommendation.GetArray("SurveyID = ? && RecStatus.TypeCode = ?", _eSurvey.ID, RecStatusType.Open.Code);
            if (recs.Length > 0)
            {
                rdoReviewYes.Checked = true;
                rdoReviewYes.Enabled = false;
                rdoReviewNo.Enabled = false;
            }
            else
            {
                rdoReviewNo.Checked = true;
                rdoReviewYes.Checked = false;
            }
        }
    }

    void btnCompleteSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (CurrentUser.Company.OptionToReviewIsVisible && string.IsNullOrEmpty(txtNotifyInstructionsOnCompletion.Text))
            {
                divFutureVisit.Attributes.Clear();

                if (rdoVisitYes.Checked)
                {
                    divFutureVisit.Attributes.Add("style", "DISPLAY: inline");
                }
                else
                {
                    divFutureVisit.Attributes.Add("style", "DISPLAY: none");
                }
                
                throw new FieldValidationException("UW instructions are required.");
            }
            
            if (rdoVisitYes.Checked && cboSurveyType.SelectedValue == string.Empty)
            {
                divFutureVisit.Attributes.Clear();
                divFutureVisit.Attributes.Add("style", "DISPLAY: inline");
                throw new FieldValidationException("Future Survey Type must be specified.");

            }

            if (rdoVisitYes.Checked && dpFutureDateSurveyed.Date == DateTime.MinValue)
            {
                divFutureVisit.Attributes.Clear();
                divFutureVisit.Attributes.Add("style", "DISPLAY: inline");
                throw new FieldValidationException("Future Survey Date must be specified.");
            }

            if (rdoVisitYes.Checked && txtFutureSurveyPurpose.Text.Trim().Length <= 0)
            {
                divFutureVisit.Attributes.Clear();
                divFutureVisit.Attributes.Add("style", "DISPLAY: inline");
                throw new FieldValidationException("Purpose of Future Survey must be specified.");
            }

            SurveyType type = (rdoVisitYes.Checked) ? SurveyType.Get(new Guid(cboSurveyType.SelectedValue)) : null;           
            User user = Berkley.BLC.Entities.User.Get(new Guid(cboFutureUsers.SelectedValue));

            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
           using (Transaction trans = DB.Engine.BeginTransaction())
           {
            if (_isCWGSurvey)
            {                
                SurveyReason surveyReason = new SurveyReason(new Guid(cboSurveyReasonType.SelectedValue), _eSurvey.ID);
                surveyReason.Save(trans);            
            }
            trans.Commit();
           }
            manager.SendForReview(rdoVisitYes.Checked, (!CurrentUser.Company.OptionToReviewIsVisible || rdoReviewYes.Checked), type, user, dpFutureDateSurveyed.Date, txtFutureSurveyPurpose.Text.Trim(), txtNotifyInstructionsOnCompletion.Text);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnCompleteCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Set Survey Details

    private void PrepForSettingDetails()
    {
        // populate the combos
        SurveyGradingCompany[] eGradings = SurveyGradingCompany.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
        ComboHelper.BindCombo(cboOverallGrading, eGradings, "SurveyGrading.Code", "SurveyGrading.Name");
        cboOverallGrading.Items.Insert(0, new ListItem("-- select grading --", ""));

        SeverityRating[] severityRatings = SeverityRating.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
        ComboHelper.BindCombo(cboSeverityRating, severityRatings, "Code", "Name");
        cboSeverityRating.Items.Insert(0, new ListItem("-- select rating --", ""));

        //populate the repeater
        if (CurrentUser.Role.PermissionSet.CanTakeAction(SurveyActionType.AccountManagement, _eSurvey, CurrentUser) && (CurrentUser.Company.SupportsSimplifiedAcctMgmt || _displayActivityTimes))
        {
            ActivityType[] activityTypes = ActivityType.GetSortedArray("CompanyID = ? && (LevelCode = ? || LevelCode = ?) && Disabled != True", "PriorityIndex ASC", this.CurrentUser.CompanyID, ActivityTypeLevel.Survey.Code, ActivityTypeLevel.All.Code);
            repSurveyActivities.DataSource = activityTypes;
            repSurveyActivities.DataBind();
        }

        //load the values, if any
        dpDateSurveyed.Date = _eSurvey.SurveyedDate;
        txtTotalHours.Text = (_eSurvey.Hours != decimal.MinValue) ? _eSurvey.Hours.ToString("N2") : string.Empty;
        cboOverallGrading.SelectedValue = _eSurvey.GradingCode;
        cboSeverityRating.SelectedValue = _eSurvey.SeverityRatingCode;
        cCallCount.Text = (_eSurvey.CallCount != decimal.MinValue) ? _eSurvey.CallCount.ToString("N2") : string.Empty;
        txtCost.Text = (_eSurvey.FeeConsultantCost != decimal.MinValue) ? _eSurvey.FeeConsultantCost.ToString("N2") : string.Empty;
        rdoRecYes.Checked = _eSurvey.RecsRequired;
        rdoRecNo.Checked = !_eSurvey.RecsRequired;
        rdoUWNotifiedYes.Checked = _eSurvey.UWNotified;
        rdoUWNotifiedNo.Checked = !_eSurvey.UWNotified;
        
        txtInvoiceNumber.Text = _eSurvey.InvoiceNumber;
        dpInvoiceDate.Date = _eSurvey.InvoiceDate;
    }

    void repSurveyActivities_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //get controls
            TextBox txtActivity = (TextBox)e.Item.FindControl("txtActivity");
            ActivityType activityType = (ActivityType)e.Item.DataItem;
            txtActivity.Attributes.Add("ActivityTypeID", activityType.ID.ToString());


            SurveyActivity surveyActivity = SurveyActivity.GetOne("SurveyID = ? && ActivityTypeID = ?", _eSurvey.ID, activityType.ID);
            if (surveyActivity != null && surveyActivity.ActivityHours != Decimal.MinValue)
            {
                txtActivity.Text = surveyActivity.ActivityHours.ToString();
            }
        }
    }

    void btnSetDetailsSubmit_Click(object sender, EventArgs e)
    {
        // perform some validation
        try
        {
            decimal hours = decimal.MinValue;
            if (txtTotalHours.Text.Trim().Length > 0)
            {
                hours = (decimal)FieldValidation.ValidateFloatField(txtTotalHours, "Total Hours");
            }

            decimal calls = decimal.MinValue;
            if (cCallCount.Text.Trim().Length > 0)
            {
                TextBox txtCallCount = new TextBox();
                txtCallCount.Text = cCallCount.Text;

                calls = (decimal)FieldValidation.ValidateFloatField(txtCallCount, "Call Count");
            }

            decimal cost = decimal.MinValue;
            if (txtCost.Text.Trim().Length > 0)
            {
                cost = (decimal)FieldValidation.ValidateFloatField(txtCost, "Cost");
            }

            //Set the details
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
            manager.SetSurveyDetails(dpDateSurveyed.Date, hours, cboOverallGrading.SelectedValue, cboSeverityRating.SelectedValue, calls, cost, rdoRecYes.Checked,rdoUWNotifiedYes.Checked, txtInvoiceNumber.Text, dpInvoiceDate.Date);

            foreach (RepeaterItem item in repSurveyActivities.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    TextBox txtActivity = (TextBox)item.FindControl("txtActivity");
                    ActivityType activityType = ActivityType.Get(new Guid(txtActivity.Attributes["ActivityTypeID"]));

                    SurveyActivity surveyActivity;
                    SurveyActivity existingSurveyActivity = SurveyActivity.GetOne("SurveyID = ? && ActivityTypeID = ?", _eSurvey.ID, activityType.ID);
                    if (existingSurveyActivity != null)
                    {
                        if (string.IsNullOrEmpty(txtActivity.Text))
                        {
                            existingSurveyActivity.Delete();

                            SurveyHistory history = new SurveyHistory(Guid.NewGuid());
                            history.SurveyID = _eSurvey.ID;
                            history.EntryDate = DateTime.Now;
                            history.EntryText = string.Format("{0} removed the {1}.", CurrentUser.Name, existingSurveyActivity.ActivityType.Name);
                            history.Save();
                        }
                        else
                        {
                            decimal value = (decimal)FieldValidation.ValidateFloatField(txtActivity, activityType.Name);

                            if (value != existingSurveyActivity.ActivityHours)
                            {
                                surveyActivity = existingSurveyActivity.GetWritableInstance();
                                surveyActivity.ActivityHours = (decimal)FieldValidation.ValidateFloatField(txtActivity, activityType.Name);
                                surveyActivity.Save();

                                SurveyHistory history = new SurveyHistory(Guid.NewGuid());
                                history.SurveyID = _eSurvey.ID;
                                history.EntryDate = DateTime.Now;
                                history.EntryText = string.Format("{0} updated the {1} from {2:N2} to {3:N2} hours.", CurrentUser.Name, surveyActivity.ActivityType.Name, existingSurveyActivity.ActivityHours, value);
                                history.Save();
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(txtActivity.Text))
                        {
                            surveyActivity = new SurveyActivity(Guid.NewGuid());
                            surveyActivity.ActivityDate = DateTime.Now;
                            surveyActivity.ActivityHours = (decimal)FieldValidation.ValidateFloatField(txtActivity, activityType.Name);
                            surveyActivity.ActivityTypeID = activityType.ID;
                            surveyActivity.AllocatedTo = _eSurvey.AssignedUserID;
                            surveyActivity.SurveyID = _eSurvey.ID;
                            surveyActivity.Save();

                            SurveyHistory history = new SurveyHistory(Guid.NewGuid());
                            history.SurveyID = _eSurvey.ID;
                            history.EntryDate = DateTime.Now;
                            history.EntryText = string.Format("{0} entered a new {1} of {2:N2} hours.", CurrentUser.Name, surveyActivity.ActivityType.Name, surveyActivity.ActivityHours);
                            history.Save();
                        }
                    }
                }
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnSetDetailsCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Change Due Date

    private void PrepForDueDateChange()
    {
        dpDueDate.Date = _eSurvey.DueDate;
        //dpDueDate.MinDate = DateTime.Today;
        //dpDueDate.MaxDate = DateTime.Today.AddYears(1);
        JavaScript.SetInputFocus(this.dpDueDate);
    }

    void btnDueDateUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime date = dpDueDate.Date;
            if (date == DateTime.MinValue)
            {
                throw new FieldValidationException(dpDueDate, "Please enter a new due date.");
            }

            // change the survey due date if different than current
            if (date != _eSurvey.DueDate)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.ChangeDueDate(date,null,SurveyManager.ReleaseOwnershipType.none,false,false);
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnDueDateCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Approve Due Date

    private void PrepForApproveDueDate()
    {

        //Added By Vilas Meshram: 02/20/2013: Squish #21347 :Due Date Extension
        string strComments = string.Empty;
        SurveyHistory[] SurveyHist = SurveyHistory.GetSortedArray("SurveyID = ?  && IsRemoved = false", "EntryDate DESC", _eSurvey.ID);

        if (SurveyHist != null && SurveyHist.Length > 0)
        {
            for (int i = 0; i < SurveyHist.Length; i++)
            {
                //While submitting for request for Due Date chage we added "Request Due Date Change Reason:". so here we are searching lasted one entry for this entry in the current surveyID
                string strChar = "Request Due Date Change Reason";

                if (SurveyHist[i].EntryText.ToString().Contains(strChar))
                {
                    _releaseType = SurveyManager.ReleaseOwnershipType.RequestDueDateChange;
                    this.ViewState["ActionTypeCode"] = SurveyManager.ReleaseOwnershipType.RequestDueDateChange;
                }

                if (SurveyHist[i].EntryText.ToString().Contains(strChar)==false)
                {
                    strChar = "Request Extension Change Reason";
                    _releaseType = SurveyManager.ReleaseOwnershipType.RequestExtension;
                    this.ViewState["ActionTypeCode"] = SurveyManager.ReleaseOwnershipType.RequestExtension;
                }

                if (SurveyHist[i].EntryText.ToString().Contains(strChar))
                {
                    string MessageString = SurveyHist[i].EntryText.ToString();
                    int pos = MessageString.IndexOf(strChar);
                    strComments = MessageString.Substring(pos); 
                    break;
                }
            }
        }

        //Added By Vilas Meshram: 02/20/2013: Squish #21347 :Due Date Extension

        DueDateHistory[] hist = DueDateHistory.GetSortedArray("SurveyID = ? && IsNull(ApprovalByUserID)", "CreateDateTime DESC", _eSurvey.ID);

        if (hist != null && hist.Length > 0)
        {
            string strlblChangeDateApprove = string.Format("{0} - From {1} to {2}.", hist[0].RequestedByUser.Name, hist[0].CurrentDueDate.ToShortDateString(), hist[0].UpdatedDueDate.ToShortDateString());
            
            if (strComments != string.Empty)
                strlblChangeDateApprove +=Environment.NewLine + strComments;

            lblChangeDateApprove.Text = strlblChangeDateApprove;
            //lblChangeDateApprove.Text = string.Format("{0} - From {1} to {2}", hist[0].RequestedByUser.Name, hist[0].CurrentDueDate.ToShortDateString(), hist[0].UpdatedDueDate.ToShortDateString());
        }
        else
        {
            //Nothing to approve  
            JavaScript.ShowMessage(this, "There are no approvals for this survey");
            // show the actions list again
            Response.Redirect(GetReturnUrl(), true);
        }


    }

    void btnApproveDueDateSubmit_Click(object sender, EventArgs e)
    {  
     
        try
        {
            _releaseType = (SurveyManager.ReleaseOwnershipType)ViewState["ActionTypeCode"]; 

            DueDateHistory[] hist = DueDateHistory.GetSortedArray("SurveyID = ? && IsNull(ApprovalByUserID)", "CreateDateTime DESC", _eSurvey.ID);

            if (hist != null && hist.Length > 0)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                //if (rdoApprove.Checked)
                //     manager.ApproveDenyDueDate(hist, true);
                //else if (rdoDeny.Checked)
                //    manager.ApproveDenyDueDate(hist, false);
                
                if (rdoApprove.Checked)
                    manager.ApproveDenyDueDate(hist, SurveyManager.ApproveDeny.Approve, DateTime.MinValue, txtApprovDenyReason.Text.Trim(), _releaseType);

                else if (rdoDeny.Checked)
                    manager.ApproveDenyDueDate(hist, SurveyManager.ApproveDeny.Deny, DateTime.MinValue, txtApprovDenyReason.Text.Trim(), _releaseType);

                else if (rdoNewDate.Checked)
                {
                    DateTime date = dpApproveNewDate.Date;
                    if (date == DateTime.MinValue)
                    {
                        throw new FieldValidationException(dpApproveNewDate, "Please enter a new date.");
                    }
                    manager.ApproveDenyDueDate(hist, SurveyManager.ApproveDeny.NewDate, date, txtApprovDenyReason.Text.Trim(), _releaseType);
                }

            }
            else
            {
                JavaScript.ShowMessage(this, "There are no approvals for this survey");
                // show the actions list again 
            }
        }

        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl(), true);

    }

    void btnApproveDueDateCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Change Assigned Date

    private void PrepForAssignedDateChange()
    {
        dpAssignedDate.Date = _eSurvey.AssignDate;
        JavaScript.SetInputFocus(this.dpAssignedDate);
    }

    void btnAssignedDateUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime date = dpAssignedDate.Date;
            if (date == DateTime.MinValue)
            {
                throw new FieldValidationException(dpAssignedDate, "Please enter a new assigned date.");
            }

            // change the survey assign date if different than current
            if (date != _eSurvey.AssignDate)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.ChangeAssignedDate(date);
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnAssignedDateCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Change Accepted Date

    private void PrepForAcceptedDateChange()
    {
        dpAcceptedDate.Date = _eSurvey.AcceptDate;
        JavaScript.SetInputFocus(this.dpAcceptedDate);
    }

    void btnAcceptedDateUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime date = dpAcceptedDate.Date;
            if (date == DateTime.MinValue)
            {
                throw new FieldValidationException(dpAcceptedDate, "Please enter a new accepted date.");
            }

            // change the survey accept date if different than current
            if (date != _eSurvey.AcceptDate)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.ChangeAcceptedDate(date);
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnAcceptedDateCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Change Report Submitted Date

    private void PrepForReportSubmittedDateChange()
    {
        dpReportSubmittedDate.Date = _eSurvey.ReportCompleteDate;
        JavaScript.SetInputFocus(this.dpReportSubmittedDate);
    }

    void btnReportSubmittedDateUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime date = dpReportSubmittedDate.Date;
            if (date == DateTime.MinValue)
            {
                throw new FieldValidationException(dpReportSubmittedDate, "Please enter a new report submitted date.");
            }

            // change the survey report submitted date if different than current
            if (date != _eSurvey.ReportCompleteDate)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.ChangeReportSubmittedDate(date);
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnReportSubmittedDateCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Change Complete Date

    private void PrepForCompleteDateChange()
    {
        dpCompleteDate.Date = _eSurvey.CompleteDate;
        JavaScript.SetInputFocus(this.dpCompleteDate);
    }

    void btnCompleteDateUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime date = dpCompleteDate.Date;
            if (date == DateTime.MinValue)
            {
                throw new FieldValidationException(dpDueDate, "Please enter a new complete date.");
            }

            // change the survey complete date if different than current
            if (date != _eSurvey.CompleteDate)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.ChangeCompleteDate(date);
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnCompleteDateCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Set Non-Productive

    void lnkNonProductive_Click(object sender, EventArgs e)
    {
        SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
        manager.ToggleValue(SurveyActionType.NonProductive);
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Set Review Flag

    void lnkFlagForReview_Click(object sender, EventArgs e)
    {
        SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
        manager.ToggleValue(SurveyActionType.Review);
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Add Note

    void btnAddNote_Click(object sender, EventArgs e)
    {
        try
        {
            string comment = FieldValidation.ValidateStringField(txtNote, "Comment", 8000, true);

            // add the note to this survey
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
            manager.AddNote(comment, rdoPrivacyInternal.Checked, rdoPrivacyAdministrator.Checked, DateTime.Now, UserIdentity.Name);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Attach Document

    void btnAttachDocument_Click(object sender, EventArgs e)
    {
        HttpPostedFile postedFile = fileUploader.PostedFile;
        string docMimeType = postedFile.ContentType;
        string docExt = System.IO.Path.GetExtension(postedFile.FileName).ToLower();

        try
        {
            // make sure a filename was uploaded
            if (postedFile == null || postedFile.FileName.Length == 0) // no filename was specified
            {
                throw new FieldValidationException("Please select a file to submit.");
            }

            // make sure the file name is not larger the max allowed
            string fileName = System.IO.Path.GetFileName(postedFile.FileName);
            if (fileName.Length > 200)
            {
                throw new FieldValidationException("The file name cannot exceed 200 characters.");
            }

            // check for an extesion
            if (System.IO.Path.GetExtension(fileName).Length <= 0)
            {
                throw new FieldValidationException("The file name must contain a file extension.");
            }

            // make sure the file has content
            int max = int.Parse(ConfigurationManager.AppSettings["MaxRequestLength"]);
            if (postedFile.ContentLength <= 0) // the file is empty
            {
                throw new FieldValidationException("The file submitted is empty.");
            }
            else if (postedFile.ContentLength > max)
            {
                // the maximum file size is 10 Megabytes
                throw new FieldValidationException("The file size cannot be larger than 15 Megabytes.");
            }

            // check if the company has doc types and if one was selected
            if ((cboDocType.Items.Count > 0 && cboDocType.SelectedValue == "") ||
                (CurrentUser.Company.RequireDocRemarks && string.IsNullOrEmpty(txtDocRemarks.Text)))
            {
                string strBuilder = "The following document fields are required:\n";

                if (cboDocType.Items.Count > 0 && cboDocType.SelectedValue == "")
                {
                    strBuilder += "- Doc Type.\n";
                }

                if (CurrentUser.Company.RequireDocRemarks && string.IsNullOrEmpty(txtDocRemarks.Text))
                {
                    strBuilder += "- Doc Remarks.\n";
                }

                throw new FieldValidationException(strBuilder);
            }

            //WA-855 Doug Bruce 2015-08-25 - Use our table mime type if it conflicts with HttpPostedFile's.
            //docMimeType = OverrideMimeType(postedFile.ContentType, docExt);
            //if (docMimeType == "")
            //{
            //    throw new Exception(String.Format("Mime Type not found in table for type [{0}] and file ext [{1}]",postedFile.ContentType,docExt));
            //}
        }
        catch (FieldValidationException ex)
        {
            JavaScript.SetInputFocus(boxDocs);
            this.ShowErrorMessage(ex);
            return;
        }

        // attach this file to the survey
        try
        {
            DocType docType = DocType.GetOne("Code = ?", cboDocType.SelectedValue);

            //check if this is a reportable document
            if (CurrentUser.Company.SupportsExtractableDocuments)
            {
                if (docExt == ".doc" || docExt == ".docx")
                {
                    MailMerge mergedDoc = new MailMerge(postedFile.InputStream, postedFile.FileName);
                    if (mergedDoc.IsExtractable)
                    {
                        string validationMsg = mergedDoc.Extract(_eSurvey);
                        if (!String.IsNullOrEmpty(validationMsg))
                        {
                            JavaScript.ShowMessage(this, validationMsg);
                            JavaScript.SetInputFocus(boxDocs);
                            return;
                        }
                    }
                }
            }
            
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
            manager.AttachDocument(postedFile.FileName, docMimeType, postedFile.InputStream, docType, txtDocRemarks.Text, true);

            Response.Redirect(GetReturnUrl(), false);
        }
        catch (Exception ex)
        {
            //ExceptionManager.Publish(ex);
            throw ex;
        }
    }

    #endregion

    #region ACTION: Remove Document

    void repDocs_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            SurveyDocument eDoc = (SurveyDocument)e.Item.DataItem;
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkRemoveDoc");
            LinkButton lnkDownload = (LinkButton)e.Item.FindControl("lnkDownloadDoc");
            HtmlAnchor lnkDownloadDocInNewWindow = (HtmlAnchor)e.Item.FindControl("lnkDownloadDocInNewWindow");
            lnk.Attributes.Add("DocumentID", eDoc.ID.ToString());
            lnkDownload.Attributes.Add("DocumentID", eDoc.ID.ToString());

            //determine which link to display
            lnkDownload.Visible = !eDoc.MimeType.Contains("htm");
            lnkDownloadDocInNewWindow.Visible = eDoc.MimeType.Contains("htm");

            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdRemoveDoc");
            td.Visible = (CurrentUser.Permissions.CanTakeAction(SurveyActionType.RemoveDocument, _eSurvey, CurrentUser));

            JavaScript.AddConfirmationNoticeToWebControl(lnk, string.Format("Are your sure you want to remove '{0}'?", eDoc.DocumentName));
        }
    }

    protected void lnkRemoveDoc_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        SurveyDocument doc = SurveyDocument.Get(new Guid(lnk.Attributes["DocumentID"]));

        SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
        manager.RemoveDocument(doc);
    }

    #endregion

    #region ACTION: Remove History Entry

    void repHistory_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            SurveyHistory surveyHistory = (SurveyHistory)e.Item.DataItem;
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkRemoveHistory");
            lnk.Attributes.Add("HistoryID", surveyHistory.ID.ToString());

            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdRemoveHistory");
            td.Visible = (CurrentUser.Permissions.CanTakeAction(SurveyActionType.RemoveHistory, _eSurvey, CurrentUser));

            JavaScript.AddConfirmationNoticeToWebControl(lnk, string.Format("Are your sure you want to remove history entry '{0}'?", surveyHistory.EntryText));
        }
    }

    protected void lnkRemoveHistory_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        SurveyHistory surveyHistory = SurveyHistory.Get(new Guid(lnk.Attributes["HistoryID"]));

        SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
        manager.RemoveHistoryEntry(surveyHistory);
    }

    #endregion

    #region ACTION: Remove Note

    void repNotes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            SurveyNote note = (SurveyNote)e.Item.DataItem;
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkRemoveNote");
            lnk.Attributes.Add("NoteID", note.ID.ToString());

            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdRemoveNote");
            td.Visible = (CurrentUser.Permissions.CanTakeAction(SurveyActionType.RemoveNote, _eSurvey, CurrentUser));

            JavaScript.AddConfirmationNoticeToWebControl(lnk, "Are your sure you want to remove this comment?");
        }
    }

    protected void lnkRemoveNote_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        SurveyNote note = SurveyNote.Get(new Guid(lnk.Attributes["NoteID"]));

        SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
        manager.RemoveNote(note);
    }

    #endregion

    #region ACTION: Set Mail Received Date

    private void PrepForMailReceivedDateChange()
    {
        dpMailReceivedDate.Date = _eSurvey.MailReceivedDate;
        //dpMailReceivedDate.MinDate = DateTime.MinValue;
        //dpMailReceivedDate.MaxDate = DateTime.MaxValue;
        JavaScript.SetInputFocus(this.dpMailReceivedDate);
    }

    void btnMailReceivedDateUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            //validate
            if (dpMailReceivedDate.Text.Trim().Length > 0)
            {
                Convert.ToDateTime(dpMailReceivedDate.Text);
            }
        }
        catch (FormatException)
        {
            FieldValidationException ex = new FieldValidationException(dpMailReceivedDate, "Please enter a valid mail received date.");
            this.ShowErrorMessage(ex);
            return;
        }


        // change the survey mail received date if different than current
        DateTime date = dpMailReceivedDate.Date;
        if (date != _eSurvey.MailReceivedDate)
        {
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
            manager.SetMailReceivedDate(date);
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnMailReceivedDateCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Set Mail Sent Date

    private void PrepForMailSentDateChange()
    {
        cboSentDateLetters.Attributes.Add("OnChange", string.Format("GetLetterSentDate('{0}', this.value)", _eSurvey.ID));

        dpMailSentDate.Date = DateTime.MinValue;
        //dpMailSentDate.MinDate = DateTime.MinValue;
        //dpMailSentDate.MaxDate = DateTime.MaxValue;
        JavaScript.SetInputFocus(this.dpMailSentDate);

        MergeDocumentVersion[] eLetters = MergeDocumentVersion.GetSortedArray("MergeDocument[CompanyID = ? && Disabled = false && Type = ?] && IsActive = true",
            "MergeDocument.PriorityIndex ASC", this.CurrentUser.CompanyID, MergeDocumentType.Letter.ID);

        // populate the combo
        ComboHelper.BindCombo(cboSentDateLetters, eLetters, "ID", "MergeDocument.Name");
        cboSentDateLetters.Items.Insert(0, new ListItem("-- select letter --", ""));
    }

    void btnMailSentDateUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            //validate
            if (dpMailSentDate.Text.Trim().Length > 0)
            {
                Convert.ToDateTime(dpMailSentDate.Text);
            }
        }
        catch (FormatException)
        {
            FieldValidationException ex = new FieldValidationException(dpMailSentDate, "Please enter a valid mail sent date.");
            this.ShowErrorMessage(ex);
            return;
        }

        if (cboSentDateLetters.SelectedValue == string.Empty)
        {
            JavaScript.ShowMessage(this, "Please select a letter.");
            return;
        }

        try
        {
            MergeDocument doc = MergeDocument.GetOne("Versions[ID = ?]", cboSentDateLetters.SelectedValue);
            SurveyLetter letter = SurveyLetter.GetOne("SurveyID = ? && MergeDocumentID = ?", _eSurvey.ID, doc.ID);
            DateTime existingDate = (letter != null) ? letter.SentDate : DateTime.MinValue;

            // change the survey mail received date if different than current
            DateTime date = dpMailSentDate.Date;
            if (date != existingDate)
            {
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.SetMailSentDate(doc, date);
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnMailSentDateCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Generate Letter

    private void PrepForGenerateLetters()
    {
        MergeDocumentVersion[] eLetters = MergeDocumentVersion.GetSortedArray("MergeDocument[CompanyID = ? && Disabled = false && Type = ?] && IsActive = true",
            "MergeDocument.PriorityIndex ASC", this.CurrentUser.CompanyID, MergeDocumentType.Letter.ID);

        // populate the combo
        ComboHelper.BindCombo(cboLetters, eLetters, "ID", "MergeDocument.Name");
        cboLetters.Items.Insert(0, new ListItem("-- select letter --", ""));
    }

    void btnLetterCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Generate Report

    private void PrepForGenerateReports()
    {
        MergeDocumentVersion[] eReports = MergeDocumentVersion.GetSortedArray("MergeDocument[CompanyID = ? && Disabled = false && Type = ?] && IsActive = true",
            "MergeDocument.PriorityIndex ASC", this.CurrentUser.CompanyID, MergeDocumentType.Report.ID);

        // populate the combo
        ComboHelper.BindCombo(cboReports, eReports, "ID", "MergeDocument.Name");
        cboReports.Items.Insert(0, new ListItem("-- select report --", ""));
    }

    void btnReportCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Create New Survey

    private void PrepForCreateNewSurvey()
    {
        //filter
        StringBuilder sb = new StringBuilder();
        sb.Append("AccountDisabled = false && ");
        sb.Append("(Role[PermissionSet.SurveyActionPermissions[");
        sb.Append("(Setting = ? || Setting = ? || Setting = ? || Setting = ?) && ");
        sb.Append("ServiceTypeCode = ? && SurveyAction.SurveyStatusCode = ?]]) ");
        sb.Append("|| (IsFeeCompany = true && CompanyID = ?)");

        //params
        ArrayList args = new ArrayList();
        args.Add(PermissionSet.SETTING_OWNER);
        args.Add(PermissionSet.SETTING_OWNER_AND_OTHERS);
        args.Add(PermissionSet.SETTING_OWNER_AND_VENDORS);
        args.Add(PermissionSet.SETTING_ALL);
        args.Add(_eSurvey.ServiceTypeCode);
        args.Add(SurveyStatus.AssignedSurvey.Code);
        args.Add(CurrentUser.CompanyID);

        User[] eUsers = this.CurrentUser.Company.GetConsultants(sb.ToString(), args.ToArray());
        ComboHelper.BindCombo(cboNewSurveyUsers, eUsers, "ID", "Name");
        cboNewSurveyUsers.Items.Insert(0, new ListItem("-- select assigned user --", ""));

        // populate the combos
        SurveyType[] types = SurveyType.GetFutureSurveyTypes(CurrentUser.Company);
        ComboHelper.BindCombo(cboNewSurveySurveyTypes, types, "ID", "Type.Name");
        cboNewSurveySurveyTypes.Items.Insert(0, new ListItem("-- select survey type --", ""));
    }

    void btnCreateNewSurvey_Click(object sender, EventArgs e)
    {
        Survey newSurvey = null;
        
        try
        {
            if (cboNewSurveyUsers.SelectedValue == string.Empty)
            {
                throw new FieldValidationException("Assigned User must be specified.");
            }
            
            if (cboNewSurveySurveyTypes.SelectedValue == string.Empty)
            {
                throw new FieldValidationException("Survey Type must be specified.");
            }

            if (dpNewSurveyDueDate.Date == DateTime.MinValue)
            {
                throw new FieldValidationException("Due Date must be specified.");
            }


            SurveyType type = SurveyType.Get(new Guid(cboNewSurveySurveyTypes.SelectedValue));
            User user = Berkley.BLC.Entities.User.Get(new Guid(cboNewSurveyUsers.SelectedValue));

            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
            newSurvey = manager.CreateSurveyFromExistingSurvey(type, user, dpNewSurveyDueDate.Date);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        string url = string.Format("survey.aspx{0}&surveyid={1}", ARMSUtility.GetQueryStringForNav("surveyid"), newSurvey.ID);
        JavaScript.ShowMessageAndRedirect(this, "A new survey has be succesfully created.  You will now be redirected to that new survey request.", url);
    }

    void btnCreateNewSurveyCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Set QA

    private void PrepForQA()
    {
        SurveyQuality[] surveyQualities = SurveyQuality.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", this.CurrentUser.CompanyID);

        // populate the combo
        ComboHelper.BindCombo(cboQualities, surveyQualities, "ID", "Type.Name");
        cboQualities.Items.Insert(0, new ListItem("-- select quality --", ""));

        //set the existing value
        cboQualities.SelectedValue = (_eSurvey.Quality != null) ? _eSurvey.QualityID.ToString() : "";
    }

    void btnQAUpdate_Click(object sender, EventArgs e)
    {
        //some validation
        if (cboQualities.SelectedValue == string.Empty)
        {
            JavaScript.ShowMessage(this, "Select a quality assessment.");
            return;
        }
        
        // set the QA
        SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
        manager.SetQualityAssessment(SurveyQuality.Get(new Guid(cboQualities.SelectedValue)), txtQAComments.Text);

        Response.Redirect(GetReturnUrl(), true);
    }

    void btnQACancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Change Primary Policy

    private void PrepForPolicyChange()
    {
        this.txtPrimaryPolicy.Text = (_eSurvey.PrimaryPolicy != null) ? _eSurvey.PrimaryPolicy.Number : _eSurvey.WorkflowSubmissionNumber;
        this.lblPrimaryPolicy.InnerText = (_eSurvey.PrimaryPolicy != null) ? "Primary Policy:" : "Submission Number:";
    }

    void btnChangePrimaryPolicyUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtPrimaryPolicy.Text.Length <= 0)
            {
                if (_eSurvey.PrimaryPolicy != null)
                {
                    throw new FieldValidationException(txtPrimaryPolicy, "Please enter a new policy number.");
                }
                else
                {
                    throw new FieldValidationException(txtPrimaryPolicy, "Please enter a new submission number.");
                }
            }

            if (_eSurvey.PrimaryPolicy != null)
            {
                //Validate that the policy number exists
                Berkley.BLC.Import.ImportFactory factory = new Berkley.BLC.Import.ImportFactory(CurrentUser.Company);
                bool valid = factory.ValidPolicy(txtPrimaryPolicy.Text);
                if (!valid)
                {
                    JavaScript.ShowMessage(this, string.Format("#{0} is not a valid policy number in the policy system(s).", txtPrimaryPolicy.Text));
                    return;
                }
            }
            else
            {
                //Validate that the submission number exists
                Berkley.BLC.Import.ImportFactory factory = new Berkley.BLC.Import.ImportFactory(CurrentUser.Company);
                bool valid = factory.ValidSubmission(txtPrimaryPolicy.Text);
                if (!valid)
                {
                    JavaScript.ShowMessage(this, string.Format("#{0} is not a valid submission number in clearance.", txtPrimaryPolicy.Text));
                    return;
                }
            }

            //Set the new policy/submission number
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);

            if (_eSurvey.PrimaryPolicy != null)
            {
                manager.ChangePrimaryPolicy(txtPrimaryPolicy.Text);
            }
            else if (!String.IsNullOrEmpty(_eSurvey.WorkflowSubmissionNumber))
            {
                manager.ChangeSubmissionNumber(txtPrimaryPolicy.Text);
            }
            else
            {
                throw new InvalidOperationException("Cannot update the primary policy or submission number since neither currently exist for this survey.");
            }

        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnChangePrimaryPolicyCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Add Plan Objective

    void btnAddPlanObjective_Click(object sender, EventArgs e)
    {
        try
        {
            if ((ddlAvailablePlanObjectives.Items.Count > 0 && ddlAvailablePlanObjectives.SelectedValue == ""))
            {
                string strBuilder = "Please select a valid plan objective:\n";

                throw new FieldValidationException(strBuilder);
            }
            
            // add the plan objective to this survey
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
            manager.AddPlanObjective(ddlAvailablePlanObjectives.SelectedValue.ToString());
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    protected bool IsFeeConsultant()
    {
        return (_eSurvey.StatusCode == SurveyStatus.AssignedSurvey.Code && _eSurvey.AssignedUser.IsFeeCompany) || 
            (_eSurvey.StatusCode == SurveyStatus.AssignedReview.Code && _eSurvey.ConsultantUser != null && _eSurvey.ConsultantUser.IsFeeCompany);
    }

    //WA-855 Doug Bruce 2015-08-25 - Override mimetype given by HttpPostedFile with 
    // mimetype from our table, based on file extension.
    protected string OverrideMimeType(string defaultMimeType, string fileExtension)
    {
        //NOTE: this expects lowercase extension WITH the dot (i.e.:  ".csv", ".txt", etc)

        string correctMimeType = "";
        string mimeType = defaultMimeType.Trim().ToLower();

        //Look up defaultMimeType to get DocumentExtension
        MimeType mimeTypeObject = MimeType.GetOne("Name = ?", mimeType);

        //If extensions don't match, lookup file extension to get MimeTypeName.
        if (mimeTypeObject != null){
            if (mimeTypeObject.DocumentExtension.ToLower() != fileExtension){
                mimeTypeObject = MimeType.GetOne("DocumentExtension = ?", fileExtension);
                if (mimeTypeObject != null)
                {
                    //Send back OUR correct mime type, based on file extension.
                    correctMimeType = mimeTypeObject.Name;
                }
            }
            else
            {
                //If extensions do match, send back the defaultMimeType with apologies for doubting it.
                correctMimeType = defaultMimeType;
            }
        }
        return correctMimeType;
    }

    protected string GetNoteUserName(object obj)
    {
        string result = string.Empty;
        SurveyNote note = (obj as SurveyNote);

        if (note.User != null)
        {
            result = (note.User.IsUnderwriter && !String.IsNullOrEmpty(note.UserName)) ? note.UserName : note.User.Name;
        }
        else
        {
            result = _eSurvey.CreateByExternalUserName;
        }

        return Server.HtmlEncode(result);
    }

    protected string GetDocUserName(object obj)
    {
        string result = string.Empty;
        SurveyDocument doc = (obj as SurveyDocument);

        if (doc.UploadUser != null)
        {
            result = (doc.UploadUser.IsUnderwriter && !String.IsNullOrEmpty(doc.UploadUserName)) ? doc.UploadUserName: doc.UploadUser.Name;
        }
        else
        {
            result = _eSurvey.CreateByExternalUserName;
        }

        return Server.HtmlEncode(result);
    }

    protected string GetEncodedHistory(object dataItem)
    {
        string history = (dataItem as SurveyHistory).EntryText;

        // html encode the string (must do this first)
        return Server.HtmlEncode(history);
    }

    protected string GetEncodedComment(object dataItem)
    {
        string comment = (dataItem as SurveyNote).Comment;
        
        // html encode the string (must do this first)
        comment = Server.HtmlEncode(comment);

        // convert CR, LF, and TAB characters
        comment = comment.Replace("\r\n", "<br>");
        comment = comment.Replace("\r", "<br>");
        comment = comment.Replace("\n", "<br>");
        comment = comment.Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");

        return comment;
    }

    protected bool isCommentInternalOnly(object dataItem)
    {
        return (dataItem as SurveyNote).InternalOnly;
    }

    protected bool isCommentAdminOnly(object dataItem)
    {
        return (dataItem as SurveyNote).AdminOnly;
    }

    protected string GetCommentPrivacy(object dataItem)
    {
        string privacy = string.Empty;

        if (isCommentInternalOnly(dataItem))
        {
            privacy = "Internal Only";
        }
        else if (isCommentAdminOnly(dataItem)) 
        {
            privacy = "Admins Only";
        }

        return privacy;
    }

    protected string GetCommentClass(object dataItem)
    {
        string cssClass = string.Empty;

        if (isCommentInternalOnly(dataItem))
        {
            cssClass = "comment-int";
        }
        else if (isCommentAdminOnly(dataItem))
        {
            cssClass = "comment-admin";
        }

        return cssClass;
    }

    protected string GetFileNameWithSize(object dataItem)
    {
        SurveyDocument doc = (SurveyDocument)dataItem;

        string result = string.Format("{0} ({1:#,##0} KB)", doc.DocumentName, doc.SizeInBytes / 1024);

        return Server.HtmlEncode(result);
    }

    protected string GetLegacyURL()
    {
        string url = ConfigurationManager.AppSettings["CWGNotesURL"];
        return string.Format(url, _eSurvey.LegacySystemID);
    }

    private string GetReturnUrl()
    {
        return string.Format("survey.aspx{0}", ARMSUtility.GetQueryStringForNav());
    }

    private bool CheckRefresh()
    {
        if (CurrentUser.Company.DaysToAutoRefreshSurvey != Int32.MinValue && _eSurvey.LastRefreshDate < DateTime.Today)
        {
            RefreshData();
            return true;
        }
        return false;
    }

    void RefreshData()
    {
        //Run the importfactory to get the lastest insured data from the policy system
        try
        {
            Common.RefreshSurvey(_eSurvey, CurrentUser, false);

            //Response.Redirect(string.Format("Survey.aspx{0}", ARMSUtility.GetQueryStringForNav()), false);
        }
        catch (Berkley.BLC.Import.BlcNoInsuredException)
        {
            //Do Nothing
        }
        catch (FieldValidationException val)
        {
            this.ShowErrorMessage(val);
            return;
        }
        catch (Exception ex)
        {
            //ExceptionManager.Publish(ex);
            throw (ex);
        }
    }

    public class AjaxMethods
    {
        [Ajax.AjaxMethod()]
        public static string GetLetterSentDate(string surveyID, string docVersionID)
        {
            if (surveyID == string.Empty || docVersionID == string.Empty)
                return string.Empty;

            MergeDocument doc = MergeDocument.GetOne("Versions[ID = ?]", docVersionID);

            SurveyLetter letter = SurveyLetter.GetOne("SurveyID = ? && MergeDocumentID = ?", surveyID, doc.ID);
            return (letter != null) ? letter.SentDate.ToShortDateString() : string.Empty;
        }
    }

    #region Unit Test Methods
    protected void lnkTest_Click(object sender, EventArgs e)
    {
        ImagingHelper imaging = new ImagingHelper(CurrentUser.Company);

        List<TestResponse> trl = new List<TestResponse>();
        trl = TestImaging(trl, txtTest.Text);
        trl = TestStreamDocument(trl, txtTest.Text, txtTest2.Text, txtTest3.Text);
        WriteTestResults(trl);

        //0=No doc
        //1=RetrieveFile
        //2=RetrieveFileImage
        //3=No doc
        //4=No doc
        //5=StreamDocument
        int which = Convert.ToInt16(txtTest4.Text);
        DisplayResults(trl[which]);
    }
    protected List<TestResponse> TestImaging(List<TestResponse> trl, string fileNetReference)
    {
        ImagingHelper imaging = new ImagingHelper(CurrentUser.Company);

        trl = imaging.TestImaging(trl, txtTest.Text, txtTest3.Text);

        return trl;
    }

    public List<TestResponse> TestStreamDocument(List<TestResponse> trl, string fileNetReference, string mimeType, string additionalDescription)
    {

        try
        {
            TestResponse oneTest = new TestResponse();

            oneTest = new TestResponse("StreamDocument(fileNetReference) " + additionalDescription, String.Format("StreamDocument('{0}','{1}',0,company({2})", fileNetReference, mimeType, CurrentUser.Company), "Stream");
            try
            {
                StreamDocument sd = new StreamDocument(fileNetReference, mimeType, 0, CurrentUser.Company);
                oneTest.ResponseStream = sd.DocumentStream;
                oneTest.ResponseType = mimeType;
                oneTest.ContentType = mimeType;
                oneTest.IsSuccessful = true;
                oneTest.Message = "Ok";
                oneTest.InternalResponse = String.Format("Length of stream returned: {0}.", oneTest.ResponseStream.Length);
            }
            catch (Exception ex)
            {
                oneTest.Error = ex;
                oneTest.Message = String.Format("Errors processing {0} {1}.", CurrentUser.Company.FileNetVersion, fileNetReference);
            }
            trl.Add(oneTest);
            oneTest = null;
        }
        catch (Exception)
        {
            //ignore an carry on
        }
        return trl;
    }

    private void WriteTestResults(List<TestResponse> trl)
    {

        string filePath = ConfigurationManager.AppSettings["TempDocPath"];
        string fileOut = filePath + "ARMS.log";

        using (StreamWriter writer = new StreamWriter(fileOut, true))
        {
            foreach (TestResponse tr in trl)
            {
                writer.Write(DateTime.Now);
                if (tr != null)
                {
                    writer.WriteLine((tr.IsSuccessful ? " -- Success" : " -- FAIL"));
                    if (tr.Description != null && tr.Description != "")
                    {
                        writer.WriteLine("Description: " + tr.Description);
                    }
                    if (tr.ContentType != null && tr.ContentType != "")
                    {
                        writer.WriteLine("ContentType: " + tr.ContentType);
                    }
                    if (tr.Command != null && tr.Command != "")
                    {
                        writer.WriteLine("Command: " + tr.Command);
                    }
                    if (tr.Error != null)
                    {
                        Exception thisEx = tr.Error;
                        while (thisEx != null)
                        {
                            writer.WriteLine(thisEx.Message);
                            thisEx = thisEx.InnerException;
                        }
                    }
                    if (tr.InternalResponse != null && tr.InternalResponse != "")
                    {
                        writer.WriteLine("Result: " + tr.InternalResponse);
                    }
                    if (tr.Message != null && tr.Message != "")
                    {
                        writer.WriteLine(tr.Message);
                    }
                }
                else
                {
                    writer.WriteLine("");
                    writer.WriteLine("Test response entry is null.");
                }
                writer.WriteLine("");
            }

            writer.WriteLine("=======================================================================================================================");
            writer.WriteLine("");
        }
    }

    private void DisplayResults(TestResponse tr)
    {
        if (tr.ResponseStream != null)
        {
            tr.ResponseStream.Position = 0;
            Stream _contentStream = tr.ResponseStream;

            // fill a buffer with the data from the stream
            byte[] content;
            using (_contentStream)
            {
                int bufferSize = (int)_contentStream.Length;
                content = new byte[bufferSize];

                int bytesRead = _contentStream.Read(content, 0, bufferSize);
                if (bytesRead != bufferSize)
                {
                    throw new Exception(bytesRead + " bytes were read from the content stream.  Exactly " + bufferSize + " bytes were expected.");
                }
            }

            // remove any content in the response
            Response.Clear();
            Response.Buffer = true;

            // set the MIME type and provide a 'friendly' filename for the client
            Response.ContentType = tr.ResponseType;

            // stream the file content to the user
            Response.BinaryWrite(content);
            Response.Flush();
            Response.End();
        }
    }

    private void DisplayFromFileNet(string fileNetReference)
    {
        ImagingHelper imaging = new ImagingHelper(CurrentUser.Company);
        string mimeType = imaging.GetMimeType(fileNetReference);
        Stream _contentStream = imaging.GetStream(fileNetReference, 1);

        // fill a buffer with the data from the stream
        byte[] content;
        using (_contentStream)
        {
            int bufferSize = (int)_contentStream.Length;
            content = new byte[bufferSize];

            int bytesRead = _contentStream.Read(content, 0, bufferSize);
            if (bytesRead != bufferSize)
            {
                throw new Exception(bytesRead + " bytes were read from the content stream.  Exactly " + bufferSize + " bytes were expected.");
            }
        }

        // remove any content in the response
        Response.Clear();
        Response.Buffer = true;

        // set the MIME type and provide a 'friendly' filename for the client
        Response.ContentType = mimeType;

        // stream the file content to the user
        Response.BinaryWrite(content);
        Response.Flush();
        Response.End();
    }

    #endregion Unit Test Methods
}
