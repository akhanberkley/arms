<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminLinks.aspx.cs" Inherits="AdminLinksAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <asp:datagrid ID="grdLinks" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
		    <asp:BoundColumn HeaderText="Order #" DataField="PriorityIndex" />
		     <asp:TemplateColumn HeaderText="Edit">
			    <ItemTemplate>
				    <a href="AdminLinkEdit.aspx?linkid=<%# HtmlEncode(Container.DataItem, "ID")%>">&nbsp;Edit&nbsp;</a>
			    </ItemTemplate>
		    </asp:TemplateColumn>
		    <asp:BoundColumn HeaderText="Description" DataField="Description" />
		    <asp:BoundColumn HeaderText="URL" DataField="URL" />
		    <asp:TemplateColumn HeaderText="Move">
			    <ItemTemplate>
				    <asp:LinkButton ID="btnUp" Runat="server" CommandName="MoveUp">Up</asp:LinkButton> /
				    <asp:LinkButton ID="btnDown" Runat="server" CommandName="MoveDown">Down</asp:LinkButton>
			    </ItemTemplate>
		    </asp:TemplateColumn>
		    <asp:TemplateColumn HeaderText="Remove">
			    <ItemTemplate>
				    <asp:LinkButton ID="btnDelete" Runat="server" CommandName="Remove" OnPreRender="btnDelete_PreRender">Remove</asp:LinkButton>
			    </ItemTemplate>
		    </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>

    <a href="AdminLinkEdit.aspx">Add a New Link</a>

    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
