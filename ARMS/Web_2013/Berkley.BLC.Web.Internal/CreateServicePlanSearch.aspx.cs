using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using System.Data.SqlClient;
using Berkley.BLC.Entities;
using Berkley.BLC.Import;
using Berkley.BLC.Business;
using Wilson.ORMapper;
using QCI.Web.Validation;
using QCI.Web;
using System.Linq;
using System.Text;

public partial class CreateServicePlanSearchAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(CreateServicePlanSearchAspx_Load);
        this.btnSearch.Click += new EventHandler(btnSearch_Click);
        this.btnCreateServicePlan.Click += new EventHandler(btnCreateServicePlan_Click);
    }
    #endregion

    protected string _surveyRequestByInsuredName;
    protected bool _defaultpolicysystem;

    void CreateServicePlanSearchAspx_Load(object sender, EventArgs e)
    {
        boxInsured.Visible = false;
        _surveyRequestByInsuredName = Berkley.BLC.Business.Utility.GetCompanyParameter("SurveyRequestByInsuredName", CurrentUser.Company).ToLower();
        _defaultpolicysystem = (Berkley.BLC.Business.Utility.GetCompanyParameter("DefaultPolicySystem", CurrentUser.Company).ToLower() == "true") ? true : false;

        if (!this.IsPostBack)
        {
            Guid id = ARMSUtility.GetGuidFromQueryString("surveyid", false);
            Survey eSurvey = (id != Guid.Empty) ? Survey.Get(id) : null;

            if (eSurvey != null)
            {
                numSurveyNumber.Text = eSurvey.Number.ToString();
                btnSearch_Click(null, null);
            }

            // populate the survey type list with company specific types
            ServicePlanType[] types = ServicePlanType.GetSortedArray("CompanyID = ? && Disabled = False", "DisplayOrder ASC", CurrentUser.CompanyID);
            cboServicePlanType.DataBind(types, "ID", "Name");
            cboServicePlanType.Visible = types.Length > 0;

            // populate the policy system dropdown
            CompanyPolicySystem[] policySystems = CompanyPolicySystem.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            cboPolicySystem.DataBind(policySystems, "PolicySystemCompanyID", "PolicySystemCompany.LongAbbreviation");
            cboPolicySystem.Visible = (cboPolicySystem.Items.Count > 1);

            if (_defaultpolicysystem && cboPolicySystem.Items.Count > 1)
                cboPolicySystem.SelectedIndex = 1;

            //clear out the cache
            this.UserIdentity.DeletePageSortSetting("createserviceplan");

            numPolicyNumber.MaxLength = (CurrentUser.Company.ID == Company.CarolinaCasualtyInsuranceGroup.ID) ? 20 : 7;
            valPolicyNumber.Type = (CurrentUser.Company.ID == Company.CarolinaCasualtyInsuranceGroup.ID) ? ValidationDataType.String : ValidationDataType.Integer;

            if (_surveyRequestByInsuredName == "true")
            {
                trPolicyNumber.Visible = false;
                cboPolicySystem.Visible = false;
                numSubmissionNumber.Visible = false;
                trInsured.Visible = true;
            }
        }
    }

    void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if ((numSurveyNumber.Text.Trim().Length > 0 && numPolicyNumber.Text.Trim().Length > 0) ||
                (numSurveyNumber.Text.Trim().Length > 0 && numSubmissionNumber.Text.Trim().Length > 0) ||
                (numPolicyNumber.Text.Trim().Length > 0 && numSubmissionNumber.Text.Trim().Length > 0) ||
                (numSurveyNumber.Text.Trim().Length > 0 && numPolicyNumber.Text.Trim().Length > 0 && numSubmissionNumber.Text.Trim().Length > 0))
            {
                if (CurrentUser.Company.CreateServicePlansFromProspects)
                    throw new FieldValidationException(numSurveyNumber, "Only enter the survey, policy, or submission number when creating a service plan.");
                else
                    throw new FieldValidationException(numSurveyNumber, "Only enter the survey or policy number when creating a service plan.");
            }

            Insured eInsured;

            if (numSurveyNumber.Text.Trim().Length > 0)
            {
                //Check if this is migrated
                Survey eSurvey = Survey.GetOne("Number = ?", numSurveyNumber.Text.Trim());
                if (eSurvey != null && eSurvey.CreateState == CreateState.Migrated)
                {
                    throw new FieldValidationException(numSurveyNumber, string.Format("ARMS is unable to create service plans from migrated or manually created surveys. Try creating a plan by entering a policy number for the account."));
                }

                //Business rule, survey cannot be a prospect
                if (!CurrentUser.Company.CreateServicePlansFromProspects)
                {
                    if (eSurvey != null && (SurveyTypeType.IsNotWrittenBusiness(eSurvey.Type.Type) || eSurvey.WorkflowSubmissionNumber.Length > 0))
                    {
                        throw new FieldValidationException(numSurveyNumber, string.Format("ARMS is unable to create service plans from prospect surveys. Try creating a plan by entering a policy number for the account."));
                    }
                }

                //Check by survey number
                eInsured = Insured.GetOne("CompanyID = ? && Surveys[Number = ? && CreateStateCode != ?]",
                    this.CurrentUser.CompanyID, numSurveyNumber.Text.Trim(), CreateState.InProgress.Code);

                if (eInsured == null)
                {
                    throw new FieldValidationException(numSurveyNumber, string.Format("Insured not found with survey number: {0}", numSurveyNumber.Text.Trim()));
                }
                else
                {
                    this.ViewState["SurveyID"] = Survey.GetOne("Number = ?", numSurveyNumber.Text.Trim()).ID;
                }
            }
            else if (numPolicyNumber.Text.Trim().Length > 0)
            {
                //Run the importfactory to get the insured data from the policy system
                ImportFactory factory = new ImportFactory(CurrentUser.Company);

                //Check if this company imports from other policy systems (i.e. national accounts company)
                BlcInsured oBlcInsured;
                if (cboPolicySystem.Items.Count > 1)
                {
                    Company selectedCompany = Company.Get(new Guid(cboPolicySystem.SelectedValue));
                    oBlcInsured = factory.ImportInsuredByPolicy(selectedCompany.Number + numPolicyNumber.Text.Trim(), CurrentUser.Company.YearsOfLossHistory > 0);

                    if (oBlcInsured.AgencyEntity == null)
                    {
                        ImportFactory agencyFactory = new ImportFactory(selectedCompany);
                        oBlcInsured.AgencyEntity = agencyFactory.ImportAgency(string.Empty, numPolicyNumber.Text.Trim());
                    }
                }
                else
                {
                    oBlcInsured = factory.ImportInsuredByPolicy(numPolicyNumber.Text.Trim(), CurrentUser.Company.YearsOfLossHistory > 0);
                }

                ReconcileInsured oReconcileInsured = new ReconcileInsured(oBlcInsured, this.CurrentUser.Company);
                oReconcileInsured.NewInsert();
                eInsured = Insured.Get(oBlcInsured.InsuredEntity.ID);

                this.ViewState["PolicyID"] = Policy.GetOne("Number = ?", numPolicyNumber.Text.Trim()).ID;
            }
            else if (CurrentUser.Company.CreateServicePlansFromProspects && numSubmissionNumber.Text.Trim().Length > 0)
            {
                //Check if we can pull any data from Clearance
                BlcInsured oBlcInsured;

                ProspectBuilder oBuilder = new ProspectBuilder();
                oBlcInsured = oBuilder.BuildFromSubmissionNumber(numSubmissionNumber.Text.Trim(), CurrentUser.Company);

                ReconcileInsured oReconcileInsured = new ReconcileInsured(oBlcInsured, this.CurrentUser.Company);
                oReconcileInsured.NewInsert();
                eInsured = Insured.Get(oBlcInsured.InsuredEntity.ID);

                this.ViewState["SubmissionNumber"] = numSubmissionNumber.Text.Trim();
            }
            else if (_surveyRequestByInsuredName == "true")
            {
                if (txtInsured.Text.Trim().Length <= 0)
                {
                    throw new FieldValidationException(txtInsured, "The Insured must be entered.");
                }

                if (txtInsured.Text.Trim() != hdName.Value)
                {
                    throw new FieldValidationException(txtInsured, "Invalid Insured name. Please select Insured name from presented list.");
                }

                //Run the importfactory to get the insured data from the policy system
                ImportFactory factory = new ImportFactory(CurrentUser.Company);
                BlcInsured oBlcInsured;
                numPolicyNumber.Text = hdPolicyNumber.Value.Trim();
                oBlcInsured = factory.ImportInsuredByPolicy(numPolicyNumber.Text.Trim(), CurrentUser.Company.YearsOfLossHistory > 0);

                ReconcileInsured oReconcileInsured = new ReconcileInsured(oBlcInsured, this.CurrentUser.Company);
                oReconcileInsured.NewInsert();
                eInsured = Insured.Get(oBlcInsured.InsuredEntity.ID);
            }
            else
            {
                if (CurrentUser.Company.CreateServicePlansFromProspects)
                    throw new FieldValidationException(numPolicyNumber, "A survey, policy, or submission number must be entered.");
                else
                    throw new FieldValidationException(numPolicyNumber, "A survey or policy number must be entered.");
            }

            Button btn = (Button)this.FindControl("btnCreateServicePlan");
            btn.Attributes.Add("InsuredID", eInsured.ID.ToString());

            //Check if the insured is non-renewed and display warning
            if (eInsured.NonrenewedDate != DateTime.MinValue)
            {
                btn.Attributes.Add("onclick", "javascript:return confirm('Account was non-renewed. Do you want to continue creating a service plan?');");
            }

            boxInsured.Visible = true;
            this.PopulateFields(eInsured);
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (BlcNoInsuredException ex)
        {
            JavaScript.ShowMessage(this, ex.Message);
            return;
        }
        catch (Berkley.BLC.Import.BlcProspectWebServiceException ex)
        {
            JavaScript.ShowMessage(this, ex.Message);
            return;
        }
        catch (Berkley.BLC.Import.Prospect.BlcProspectWebServiceException ex)
        {
            JavaScript.ShowMessage(this, ex.Message);
            return;
        }
    }

    void btnCreateServicePlan_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        Guid gInsuredID = new Guid(btn.Attributes["InsuredID"]);

        //Stub out new service plan
        ServicePlan eServicePlan = new ServicePlan(new Guid());

        //commit changes
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            eServicePlan.CompanyID = this.CurrentUser.Company.ID;
            eServicePlan.InsuredID = gInsuredID;
            eServicePlan.PrimarySurveyID = (this.ViewState["SurveyID"] != null) ? (Guid)this.ViewState["SurveyID"] : Guid.Empty;
            eServicePlan.PrimaryPolicyID = (this.ViewState["PolicyID"] != null) ? (Guid)this.ViewState["PolicyID"] : Guid.Empty;
            eServicePlan.WorkflowSubmissionNumber = (this.ViewState["SubmissionNumber"] != null) ? this.ViewState["SubmissionNumber"].ToString() : String.Empty;
            eServicePlan.TypeID = (cboServicePlanType.Visible) ? new Guid(cboServicePlanType.SelectedValue) : Guid.Empty;
            eServicePlan.StatusCode = ServicePlanStatus.UnassignedPlan.Code;
            eServicePlan.CreateStateCode = CreateState.InProgress.Code;
            eServicePlan.CreateDate = DateTime.Now;
            eServicePlan.CreatedByUserID = CurrentUser.ID;
            eServicePlan.Save(trans);

            trans.Commit();
        }

        Response.Redirect(string.Format("CreateServicePlan.aspx?planid={0}", eServicePlan.ID), true);
    }

    //Added By Vilas Meshram: 03/12/2013: Squish# 21338: When the user begins typing the name in the new insured name field, the system will present a list of options as 
    //                                    the user continues to type the name (similar to auto completing in Google search).
    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetInsured(string prefixText)
    {
        if (prefixText == string.Empty)
            return null;
        prefixText = prefixText.Replace("'", "''");

        DataTable dt = StoredProcedures.CreateSurvey_GetSearchedInsuredNameBySurveyType(prefixText, "WR");

        List<string> InsuredNames = new List<string>();
        string InsuredItem = string.Empty;

        foreach (DataRow row in dt.Rows)
        {
            InsuredItem = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row["Name"].ToString(), row["Number"].ToString());
            InsuredNames.Add(InsuredItem);
        }

        return InsuredNames;
    }
}