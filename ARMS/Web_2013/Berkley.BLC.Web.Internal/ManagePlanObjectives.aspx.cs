using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using QCI.Web.Validation;
using QCI.ExceptionManagement;

public partial class ManagePlanObjectives : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(SurveyAspx_Load);
        this.PreRender += new EventHandler(SurveyAspx_PreRender);
        this.grdObjectives.ItemCommand += new DataGridCommandEventHandler(grdObjectives_ItemCommand);
       
    }
    #endregion

    protected Survey _eSurvey;
    protected ServicePlan _eServicePlan;

    void SurveyAspx_Load(object sender, EventArgs e)
    {
        // register our AJAX class for use in client script
        //Ajax.Utility.RegisterTypeForAjax(typeof(AjaxMethods));

        Guid id = ARMSUtility.GetGuidFromQueryString("surveyid", false);
        Guid pId = ARMSUtility.GetGuidFromQueryString("planid", true);
        
        if (id != null && id != Guid.Empty)
            _eSurvey = Survey.Get(id);

        if (pId != null && pId != Guid.Empty)
            _eServicePlan = ServicePlan.Get(pId);

        if (!this.IsPostBack)
        {
            lnkAddObjective.HRef = string.Format("ObjectiveEdit.aspx{0}", ARMSUtility.GetQueryStringForNav("objectiveid"));
        }
    }

    void SurveyAspx_PreRender(object sender, EventArgs e)
    {
        if (this.IsPostBack)
        {
            
        }

        // our date picker needs this script
        ARMSUtility.AddJavaScriptToPage(this, ARMSUtility.AppPath + "/AutoFormat.js");

        // put the insured and survey number in the header
        if (_eSurvey != null)
            base.PageHeading = string.Format((_eSurvey.ServiceType == ServiceType.ServiceVisit) ? "Service Visit #{0} ({1})" : "Survey #{0} ({1})", _eSurvey.Number, ARMSUtility.GetSubstring(_eSurvey.Insured.Name, 80));
        else if (_eServicePlan != null)
            base.PageHeading = string.Format("Service Plan #{0} ({1})", _eServicePlan.Number, ARMSUtility.GetSubstring(_eServicePlan.Insured.Name, 80));
        // populate all our static fields
        //this.PopulateFields(_eSurvey);
        //this.PopulateFields(_eSurvey.Status);
        //this.PopulateFields(_eSurvey.Company);

       // string command = string.Format("EXEC Recs_GetCompletedCount @SurveyID = '{0}'", _eSurvey.ID);
        //string recRount = DB.Engine.GetDataSet(command).Tables[0].Rows[0][0].ToString();


        // get the objectives for this survey
        if (_eSurvey != null)
        {
            Objective[] eObjectives = Objective.GetSortedArray("SurveyID = ?", "Number ASC", _eSurvey.ID);
            DataGridHelper.BindGrid(grdObjectives, eObjectives, "ID", "There are no objectives for this service visit.");
            ///repObjectives.DataSource = eObjectives;
            ///repObjectives.DataBind();
            boxObjectives.Title = string.Format("Objectives ({0})", eObjectives.Length);

        }

        if (_eServicePlan != null)
        {
            Objective[] eObjectives = Objective.GetSortedArray("ServicePlanID = ? && isNull(SurveyID)", "Number ASC", _eServicePlan.ID);
            DataGridHelper.BindGrid(grdObjectives, eObjectives, "ID", "There are no objectives for this service plan.");
            //repObjectives.DataSource = eObjectives;
            //repObjectives.DataBind();
            boxObjectives.Title = string.Format("Plan Objectives ({0})", eObjectives.Length);

        }

        // determine available actions the user can take on this survey
        //SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
        //SurveyAction[] actions = manager.GetAvailableActions();

        // turn on (or show) the actions available
        //EnableActions(actions);
    }

    private void EnableActions(SurveyAction[] eActions)
    {
        
        grdObjectives.Visible = false;
               
    }

    protected void btnRemoveObjective_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this objective?");
    }

   
    void repActions_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // we only care about repeater items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // see if this action has a confirm prompt specified
            SurveyAction action = (SurveyAction)e.Item.DataItem;

            // attach the message to the button control
            LinkButton btn = (LinkButton)e.Item.FindControl("btnAction");

            //if (action.Type == SurveyActionType.CloseAndNotify && _eSurvey.MailReceivedDate == DateTime.MinValue && !SurveyTypeType.IsNotWrittenBusiness(_eSurvey.Type.Type) && _eSurvey.Status != SurveyStatus.AssignedSurvey && _eSurvey.ServiceType == ServiceType.SurveyRequest)
            //{
            //    JavaScript.AddConfirmationNoticeToWebControl(btn, "Are you sure you want to close this survey without setting the mail received date?");
            //}
            //else if (action.Type == SurveyActionType.ReleaseOwnership && _eSurvey.Status == SurveyStatus.AssignedSurvey && _eSurvey.AssignedUser.IsFeeCompany)
            //{
            //    JavaScript.AddConfirmationNoticeToWebControl(btn, "Please note that the survey will be in 'Returning to Company' status awaiting for the vendor to acknowledge the release of ownership.");
            //}
            //else if (action.ConfirmMessage != null && action.ConfirmMessage.Trim().Length > 0)
            //{
            //    JavaScript.AddConfirmationNoticeToWebControl(btn, action.ConfirmMessage);
            //}
        }
    }

   
    void grdObjectives_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            // get the ID of the territory selected
            Guid id = (Guid)grdObjectives.DataKeys[e.Item.ItemIndex];

            // remove the objective
            Objective eObjective = Objective.Get(id);
            if (eObjective != null)
            {

                //Validate that no service visits have this objective
                Objective[] ojs = Objective.GetArray("ServicePlanID = ? && !isNull(SurveyID)", eObjective.ServicePlanID);

                if (ojs.Length != null & ojs.Length > 0)
                {
                    string msg = "This objective could not be removed because it's assigned to another service visit.  Please remove from service visit before removing from plan.";

                    JavaScript.ShowMessage(this, msg);
                    return;
                }

                AttributeHelper.DeleteObjectiveAttributes(eObjective);
                eObjective.Delete();

                //Update the survey history
                //SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
                //manager.ObjectiveManagement(eObjective, SurveyManager.UIAction.Remove);
            }
        }

        //Response.Redirect(string.Format("ServicePlan.aspx{0}", ARMSUtility.GetQueryStringForNav()));
    }

}
