<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminDocuments.aspx.cs" Inherits="AdminDocumentsAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <asp:CheckBox ID="chkShowDisabled" Runat="server" CssClass="chk" AutoPostBack="True" Text="Show disabled service plan documents." />
    <asp:datagrid ID="grdDocs" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
		    <asp:hyperlinkcolumn HeaderText="Name" DataTextField="Name" DataNavigateUrlField="ID"
			    DataNavigateUrlFormatString="AdminDocumentEdit.aspx?mergedocid={0}&typeflag=3" SortExpression="Name ASC" />
		    <asp:TemplateColumn HeaderText="Disabled" SortExpression="Disabled ASC">
                <ItemTemplate>
	                <%# GetBoolLabel(Container.DataItem) %>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Move">
	            <ItemTemplate>
		            &nbsp;<asp:LinkButton ID="btnUp" Runat="server" CommandName="MoveUp">Up</asp:LinkButton>&nbsp;/
		            &nbsp;<asp:LinkButton ID="btnDown" Runat="server" CommandName="MoveDown">Down</asp:LinkButton>&nbsp;
	            </ItemTemplate>
            </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>

    <a href="AdminDocumentEdit.aspx?typeflag=3">Create New Service Plan Document Template</a>
    
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
