﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AgencyVisitEdit.aspx.cs" Inherits="AgencyVisitEdit" %>

<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Date" Src="~/Fields/DateField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Counter" Src="~/Controls/Counter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxActivity" runat="server" Title="Activity" width="600">
        <table class="fields">
            <uc:Combo id="cboActivityType" runat="server" field="ActivityType.ID" Name="Activity Type" topItemText="" isRequired="True" AutoPostback="true" />
            <uc:Combo id="cboAllocatedTo" runat="server" field="Activity.AllocatedTo" Name="Allocated To" topItemText="" isRequired="True" />
            <uc:Combo id="cboAgencyVisited" runat="server" field="Activity.AgencyVisitedID" Name="Agency Visited" topItemText="" isRequired="True" />
            <uc:Date id="dtActivityDate" runat="server" field="Activity.Date" Name="Date" IsRequired="True" />
            <uc:Number id="numHours" runat="server" field="Activity.Hours" Name="Hours" IsRequired="False" />
            <uc:Number id="numDays" runat="server" field="Activity.Days" Name="Days" IsRequired="False" />
            <tr id="rowCallCount" runat="server">
                <td class="label" nowrap>Call Count *</td>
                <td><uc:Counter id="counterCallCount" Name="Call Count" runat="server" IsRequired="True" /></td>
            </tr>
            <uc:Text id="txtComments" runat="server" field="Activity.Comments" Name="Comments" Rows="5" style="width: 450px" EnableCheckSpelling="true" />
        </table>
    </cc:Box>
    
    <cc:Box id="boxDocuments" runat="server"  width="600">
        <div id="divAddDoc" runat="server" style="MARGIN-TOP: 5px; margin-bottom: 5px">
            <% if( repDocs.Items.Count > 0 ) { %>
            <asp:Repeater ID="repDocs" Runat="server">
                <HeaderTemplate>
                    <table cellspacing="0" cellpadding="0" border="0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td style="PADDING-BOTTOM: 3px">
                            <a href='<%# DataBinder.Eval(Container.DataItem, "ID", "Document.aspx?activitydocid={0}") %>' target="_blank"><%# GetFileNameWithSize(Container.DataItem) %></a>
                        </td>
                        <td style="PADDING-LEFT: 10px">
                            From <%# HtmlEncode(Container.DataItem, "UploadUser.Name")%> on <%# HtmlEncode(Container.DataItem, "UploadedOn", "{0:g}") %>
                        </td>
                        <td class="tdRemoveDoc" style="PADDING-LEFT: 15px" valign="top" runat="server">
                            <asp:LinkButton ID="lnkRemoveDoc" runat="server" OnClick="lnkRemoveDoc_OnClick">Remove</asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <% } else { %>
            <div style="PADDING-BOTTOM: 10px">
                (none)
            </div>
            <% } %>
        </div>
        
        <div id="divUploadDoc" runat="server">
            Attach Document<br>
            <input type="file" id="fileUploader" runat="server" name="fileDocument" class="txt" size="80"><br>
            <asp:Button ID="btnAttachDocument" Runat="server" Text="Upload" CssClass="btn" CausesValidation="True" />
        </div>
        
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" />
    <asp:Button ID="btnAddAnother" Runat="server" CssClass="btn" Text="Add Another" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
