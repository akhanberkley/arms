<%@ Page Language="C#" AutoEventWireup="false" CodeFile="CompanySummary.aspx.cs" Inherits="CompanySummaryAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <cc:Box id="boxTeam" runat="server" title="Current Team Status" width="550">
                <span class="subheading">Company Consultants</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="surveys.aspx?filter=US"><%= GetUnassignedCount(SurveyStatusType.Survey) - _returnedCount %> Unassigned</a>
                &nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="surveys.aspx?filter=RS"><%= _returnedCount %> Returned</a>
                &nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="surveys.aspx?filter=OH"><%= _holdCount %> On Hold</a>
                <br>
                <asp:Repeater ID="repCCs" Runat="server">
	                <HeaderTemplate>
		                <table class="grid">
		                <tr class="header">
			                <td width="150">Name</td>
			                <td>Total Assigned</td>
                            <% if (_showSurveyDaysCountColumnHeader.ToString() == "true") { %>
                            <td><%=_surveyDaysCountColumnHeader%></td>
                            <% } %>
			                <td>Survey Request</td>
			                <td>Service Visit</td>
			                <td>Awaiting Acceptance</td>
			                <td>Returned&nbsp;For Correction</td>
			                <td>Near Due</td>
			                <td>Past Due</td>
		                </tr>
		                <% _itemClass = null; %>
	                </HeaderTemplate>
	                <ItemTemplate>
		                <% _itemClass = (_itemClass != "item" ? "item" : "altItem"); %>
		                <tr class="<%= _itemClass %>">
			                <td nowrap><a href='<%# DataBinder.Eval(Container.DataItem, "ID", "mysurveys.aspx?userid={0}") %>'><%# HtmlEncode(Container.DataItem, "Name") %></a></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Survey.Code, Container.DataItem, "Total")%></td>
                            <% if (_showSurveyDaysCountColumnHeader.ToString() == "true") { %>
                            <td align="right"><%# GetCountForUser(SurveyStatusType.Survey.Code, Container.DataItem, "SurveyDaysCount")%></td>
                            <% } %>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Survey.Code, Container.DataItem, "SurveyRequest")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Survey.Code, Container.DataItem, "ServiceVisit")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatus.AwaitingAcceptance.Code, Container.DataItem, "AwaitingAcceptance")%></td>
			                <td align="right"><%# GetCountForUser("C", Container.DataItem, "Correction")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Survey.Code, Container.DataItem, "SurveyNearDue")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Survey.Code, Container.DataItem, "PastDue")%></td>
		                </tr>
	                </ItemTemplate>
	                <FooterTemplate>
		                <% _itemClass = (_itemClass != "item" ? "item" : "altItem"); %>
		                <tr class="<%= _itemClass %>">
			                <td><b>Totals</b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Survey.Code, "Total", false)%></b></td>
                            <% if (_showSurveyDaysCountColumnHeader.ToString() == "true") { %>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Survey.Code, "SurveyDaysCount", false)%></b></td>
                            <% } %>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Survey.Code, "SurveyRequest", false)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Survey.Code, "ServiceVisit", false)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatus.AwaitingAcceptance.Code, "AwaitingAcceptance", false)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam("C", "Correction", false)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Survey.Code, "SurveyNearDue", false)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Survey.Code, "PastDue", false)%></b></td>
		                </tr>
	                </table>
	                </FooterTemplate>
                </asp:Repeater>

                <br>
                <span class="subheading">Vendors</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Repeater ID="repVs" Runat="server">
	                <HeaderTemplate>
		                <table class="grid">
		                <tr class="header">
			                <td width="150">Name</td>
			                <td>Total Assigned</td>
			                <td>Survey Request</td>
			                <td>Service Visit</td>
			                <td>Awaiting Acceptance</td>
			                <td>Returned&nbsp;For Correction</td>
			                <td>Near Due</td>
			                <td>Past Due</td>
		                </tr>
		                <% _itemClass = null; %>
	                </HeaderTemplate>
	                <ItemTemplate>
		                <% _itemClass = (_itemClass != "item" ? "item" : "altItem"); %>
		                <tr class="<%= _itemClass %>">
			                <td nowrap><a href='<%# DataBinder.Eval(Container.DataItem, "ID", "surveys.aspx?userid={0}") %>'><%# HtmlEncode(Container.DataItem, "Name") %></a></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Survey.Code, Container.DataItem, "Total")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Survey.Code, Container.DataItem, "SurveyRequest")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Survey.Code, Container.DataItem, "ServiceVisit")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatus.AwaitingAcceptance.Code, Container.DataItem, "AwaitingAcceptance")%></td>
			                <td align="right"><%# GetCountForUser("C", Container.DataItem, "Correction")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Survey.Code, Container.DataItem, "SurveyNearDue")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Survey.Code, Container.DataItem, "PastDue")%></td>
		                </tr>
	                </ItemTemplate>
	                <FooterTemplate>
		                <% _itemClass = (_itemClass != "item" ? "item" : "altItem"); %>
		                <tr class="<%= _itemClass %>">
			                <td><b>Totals</b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Survey.Code, "Total", true)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Survey.Code, "SurveyRequest", true)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Survey.Code, "ServiceVisit", true)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatus.AwaitingAcceptance.Code, "AwaitingAcceptance", true)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam("C", "Correction", true)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Survey.Code, "SurveyNearDue", true)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Survey.Code, "PastDue", true)%></b></td>
		                </tr>
	                </table>
	                </FooterTemplate>
                </asp:Repeater>

                <br>
                <span class="subheading">Reviewers</span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="surveys.aspx?filter=UR"><%= _unassignedReviewCount%> Unassigned Reviews</a>
                <br>
                <asp:Repeater ID="repAFs" Runat="server">
	                <HeaderTemplate>
		                <table class="grid">
		                <tr class="header">
			                <td width="150">Name</td>
			                <td>Total Assigned</td>
			                <td>Survey Request</td>
			                <td>Service Visit</td>
			                <td>Near Due</td>
			                <td>Past Due</td>
		                </tr>
		                <% _itemClass = null; %>
	                </HeaderTemplate>
	                <ItemTemplate>
		                <% _itemClass = (_itemClass != "item" ? "item" : "altItem"); %>
		                <tr class="<%= _itemClass %>">
			                <td nowrap><a href='<%# DataBinder.Eval(Container.DataItem, "ID", "mysurveys.aspx?userid={0}") %>'><%# HtmlEncode(Container.DataItem, "Name") %></a></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Review.Code, Container.DataItem, "Total")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Review.Code, Container.DataItem, "SurveyRequest")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Review.Code, Container.DataItem, "ServiceVisit")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Review.Code, Container.DataItem, "ReviewNearDue")%></td>
			                <td align="right"><%# GetCountForUser(SurveyStatusType.Review.Code, Container.DataItem, "PastDue")%></td>
		                </tr>
	                </ItemTemplate>
	                <FooterTemplate>
		                <% _itemClass = (_itemClass != "item" ? "item" : "altItem"); %>
		                <tr class="<%= _itemClass %>">
			                <td><b>Totals</b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Review.Code, "Total")%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Review.Code, "SurveyRequest", false)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Review.Code, "ServiceVisit", false)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Review.Code, "ReviewNearDue", false)%></b></td>
			                <td align="right"><b><%# GetTotalForTeam(SurveyStatusType.Review.Code, "PastDue", false)%></b></td>
		                </tr>
	                </table>
	                </FooterTemplate>
                </asp:Repeater>
                </cc:Box>
    
                <%--<cc:Box id="boxImport" runat="server" title="Survey Creation History" width="500">
                <asp:Repeater ID="repImport" Runat="server">
	                <HeaderTemplate>
		                <table class="grid">
		                <tr class="header">
			                <td>Date</td>
			                <td width="16%">Assigned to Company Consultants</td>
			                <td width="16%">Assigned to Vendors</td>
			                <td width="16%">On Hold</td>
			                <td width="16%">Not Assigned</td>
			                <td width="20%">Total</td>
		                </tr>
		                <% _itemClass = null; %>
	                </HeaderTemplate>
	                <ItemTemplate>
		                <% _itemClass = (_itemClass != "item" ? "item" : "altItem"); %>
		                <tr class="<%= _itemClass %>">
			                <td align="right"><%# HtmlEncode(Container.DataItem, "Date", "{0:d}") %></a></td>
			                <td align="right"><%# HtmlEncode(Container.DataItem, "AssignedToCompanyConsultant") %></a></td>
			                <td align="right"><%# HtmlEncode(Container.DataItem, "AssignedToVendor") %></a></td>
			                <td align="right"><%# HtmlEncode(Container.DataItem, "OnHold") %></a></td>
			                <td align="right"><%# HtmlEncode(Container.DataItem, "NotAssigned") %></a></td>
			                <td align="right"><%# HtmlEncode(Container.DataItem, "Total") %></a></td>
		                </tr>
	                </ItemTemplate>
	                <FooterTemplate>
		                </table>
	                </FooterTemplate>
                </asp:Repeater>
                </cc:Box>--%>
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td valign="top">
                <cc:Box id="boxNotification" runat="server" title="Site Notification" width="300" Visible="false">
                    <div id="divNotificationMessage" runat="server"></div>
                    <a id="lnkEnhancementList" href="" runat="server" target="_blank">Squish List</a>
                </cc:Box>
            </td>
        </tr>
    </table>

    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
