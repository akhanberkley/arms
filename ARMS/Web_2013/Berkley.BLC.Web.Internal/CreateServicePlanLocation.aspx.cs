using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web.Validation;
using QCI.Web;

public partial class CreateServicePlanLocationAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(CreateServicePlanLocationAspx_Load);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
    }
    #endregion

    protected ServicePlan _servicePlan;

    void CreateServicePlanLocationAspx_Load(object sender, EventArgs e)
    {
        //Guids to navigate back
        Guid servicePlanID = ARMSUtility.GetGuidFromQueryString("planid", true);
        _servicePlan = ServicePlan.Get(servicePlanID);

        //Get the highest location number and incriment by one for the new location
        if (!this.IsPostBack)
        {
            Location[] locations = Location.GetSortedArray("InsuredID = ?", "Number DESC", _servicePlan.InsuredID);
            int locationNumber = (locations.Length > 0) ? locations[0].Number + 1 : 1;
            numLocationNumber.Text = locationNumber.ToString();
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            Location location = new Location(Guid.NewGuid());
            location.InsuredID = _servicePlan.InsuredID;
            location.IsActive = true;

            this.PopulateEntity(location);

            // commit changes
            location.Save();
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        if (btnSubmit.Text.Contains("Add"))
        {
            JavaScript.ConfirmAndRedirect(this, "Add another location?", string.Format("CreateServicePlanLocation.aspx?planid={0}", _servicePlan.ID), GetReturnURL());
        }
        else
        {
            Response.Redirect(GetReturnURL(), true);
        }
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnURL(), true);
    }

    private string GetReturnURL()
    {
        if (_servicePlan.CreateState != CreateState.InProgress)
        {
            return string.Format("ServicePlanDetails.aspx{0}", ARMSUtility.GetQueryStringForNav());
        }
        else
        {
            return string.Format("CreateServicePlan.aspx?planid={0}", _servicePlan.ID);
        }
    }
}
