using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web.Validation;

public partial class AdminRecEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminRecEditAspx_Load);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected Recommendation _eRec;

    void AdminRecEditAspx_Load(object sender, EventArgs e)
    {
        Guid gRecID = ARMSUtility.GetGuidFromQueryString("recid", false);
        _eRec = (gRecID != Guid.Empty) ? Recommendation.Get(gRecID) : null;

        if (!this.IsPostBack)
        {
            if (_eRec != null) // edit
            {
                this.PopulateFields(_eRec);
            }
        }

        btnSubmit.Text = (_eRec == null) ? "Add" : "Update";
        base.PageHeading = (_eRec == null) ? "Add Rec" : "Update Rec";
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            Recommendation eRec;
            if (_eRec == null) // add
            {
                eRec = new Recommendation(Guid.NewGuid());
                eRec.CompanyID = this.UserIdentity.CompanyID;
            }
            else // edit
            {
                eRec = _eRec.GetWritableInstance();
            }

            this.PopulateEntity(eRec);

            // commit all changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                eRec.Save(trans);
                trans.Commit();
            }

            // update our member var so the redirect will work correctly
            _eRec = eRec;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl());
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        return "AdminRecs.aspx";
    }
}
