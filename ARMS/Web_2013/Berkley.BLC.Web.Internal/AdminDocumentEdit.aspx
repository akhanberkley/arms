<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" CodeFile="AdminDocumentEdit.aspx.cs" Inherits="AdminDocumentEditAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"> <%= base.PageHeading %></span><a href="<%= ARMSUtility.AppPath %>/Docs/<%= (mergeType.Name != "Document") ? "merge_fields.doc" : "merge_fields_ServicePlan.doc" %>" target="_blank" style="margin-left:20px;">(Available Merge Fields.doc)</a><br><br>
    
    <cc:Box id="boxDocument" runat="server" title="Details" width="500">
        <table class="fields">
        <uc:Text id="txtDocumentName" runat="server" field="MergeDocument.Name" Columns="50" Name="Name" />
        <uc:Number id="numReminderDays" runat="server" field="MergeDocument.DaysUntilReminder" Name="Reminder" />
       <%-- <uc:Combo id="cboReminderDate" runat="server" field="MergeDocument.ReminderDateTypeCode" Name="Code" topItemText="" />--%>
        <uc:Check id="chkDisabled" runat="server" field="MergeDocument.Disabled" />
        <uc:Check id="chkIsCritical" runat="server" field="MergeDocument.IsCritical" Name="Critical" />
        </table>
    </cc:Box>

    <cc:Box id="boxReporting" runat="server" title="Reporting" width="500">
        <table>
            <tr>
                <td style="padding-right: 10px"><img src="<%= ARMSUtility.AppPath %>/images/document_add_32.png" border="0"></td>
                <td><asp:LinkButton ID="lnkManageField" runat="server">Manage Fields to Report On >></asp:LinkButton></td>
            </tr>
        </table>
    </cc:Box>
    
    <cc:Box id="boxVersions" runat="server" title="Template History" width="500">
        <asp:datagrid ID="grdVersions" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="30" AllowSorting="False">
            <headerstyle CssClass="header" />
            <itemstyle CssClass="item" />
            <alternatingitemstyle CssClass="altItem" />
            <footerstyle CssClass="footer" />
            <pagerstyle CssClass="pager" Mode="NumericPages" />
            <columns>
                <asp:BoundColumn HeaderText="Version" DataField="VersionNumber" SortExpression="VersionNumber ASC" />
                <asp:hyperlinkcolumn HeaderText="Reference" DataTextField="FileNetReference" DataNavigateUrlField="ID" Target="_blank"
                    DataNavigateUrlFormatString="Document.aspx?templateid={0}" SortExpression="FileNetReference ASC" />
                <asp:TemplateColumn HeaderText="Date Uploaded" ItemStyle-HorizontalAlign="Right" SortExpression="UploadedOn ASC">
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "UploadedOn", "{0:G}", DateTime.MinValue, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Active" SortExpression="IsActive ASC">
                    <ItemTemplate>
                        <%# GetBoolLabel(Container.DataItem) %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Preview">
                    <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                            <a href="document.aspx?reportid=<%# HtmlEncode(Container.DataItem, "ID")%>&surveyid=<%= _surveyID %>" target="_blank">Preview</a>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                            <asp:LinkButton ID="lnkMakeActive" runat="server" CommandName="MAKEACTIVE" CausesValidation="false">Make Active</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Remove">
                <ItemTemplate>
                    <asp:LinkButton ID="btnDelete" Runat="server" CommandName="Remove" OnPreRender="btnDelete_PreRender">Remove</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateColumn>
            </columns>
        </asp:datagrid>
        
        <%if (_eMergeDoc != null){%>
            <a href="javascript:Toggle('divUploader')">Add New Template Version</a>
        <%} %>
    </cc:Box>
    
    <div id="divUploader" style="DISPLAY: none">
        <cc:Box id="boxUploader" runat="server" title="Uploader" width="500">
            Upload Template<br>
            <input type="file" id="fileUploader" runat="server" name="fileDocument" class="txt" size="70"><br>
            <asp:Button ID="btnAttachDocument" Runat="server" Text="Upload" CssClass="btn" CausesValidation="False" />
        </cc:Box>
    </div>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Add/Update" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:Footer ID="ucFooter" runat="server" />
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
    function Toggle(id)
    {
        ToggleControlDisplay(id);
    }
    </script>
    
    </form>
</body>
</html>
