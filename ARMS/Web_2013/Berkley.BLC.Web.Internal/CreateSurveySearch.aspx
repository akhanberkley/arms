<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" CodeFile="CreateSurveySearch.aspx.cs" Inherits="CreateSurveySearchAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Date" Src="~/Fields/DateField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= base.PageTitle %></title>
    <script type="text/javascript" language="javascript">var counter = 0;</script>
</head>

<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css"/>
    <form id="frm" method="post" runat="server" defaultbutton="btnSearch">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager >
 
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lnkCreateManualSurvey" CausesValidation="false" runat="server"></asp:LinkButton><br><br>

    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxSearch" runat="server" Title="Search Criteria" width="510">
        <table class="fields">
            <uc:Combo id="cboSurveyType" runat="server" field="" Name="Survey Type" topItemText="" isRequired="true" />
            <uc:Combo id="cboSurveyReasonType" runat="server" field="" Name="Survey Reasons" topItemText="" isRequired="true" />
            <uc:Combo id="cboTransactionType" runat="server" field="" Name="Transaction Type" topItemText="" isRequired="true" />
            <uc:Combo id="cboPolicySystem" runat="server" field="" Name="Company System" topItemText="" isRequired="true" />
            <tr id="trPolicyNumber" runat="server">
                <td class="label" nowrap="nowrap">
                    <span>Policy Number *</span>
                </td>
                <td>
                    <asp:TextBox id="numPolicyNumber" runat="server" Columns="15" MaxLength="15" CssClass="txt"></asp:TextBox>
                    <asp:CompareValidator ID="valPolicyNumber" Runat="server" CssClass="val" ControlToValidate="numPolicyNumber" Display="Dynamic" Operator="DataTypeCheck" Type="Integer" Text="*" ErrorMessage="Policy Number is not a valid value." />
                </td>
            </tr>
            <uc:Number id="numSubmissionNumber" runat="server" field="LineOfBusiness.DisplayOrder" Name="Submission Number *" IsRequired="false" Columns="15" />
            <uc:Text id="txtQuoteNumber" runat="server" field="Policy.Number" Name="Quote Number" IsRequired="false" Columns="15" />
            
            <tr id="trInsured" valign="top" runat="server">
                 <td class="label" nowrap="nowrap">
                    <span>Insured *</span>
                </td>
                <td><asp:TextBox ID="txtInsured" runat="server" Width="300" CssClass="txt" ></asp:TextBox> 

                    <asp:Panel runat="server" ID="pnlInsured" 
                         ScrollBars="Auto" style="overflow:auto;width:300px;text-overflow:ellipsis;display:none;height:inherit">
                    </asp:Panel>

                    <ajax:AutoCompleteExtender 
                                ID="aceInsured" 
                                TargetControlID="txtInsured" MinimumPrefixLength="1" 
                                EnableCaching="true" 
                                CompletionSetCount="1" CompletionInterval="1000" 
                                CompletionListElementID="pnlInsured"
                                OnClientItemSelected="aceInsured_itemSelected"
                                ServiceMethod="GetInsured"
                                runat="server">
                    </ajax:AutoCompleteExtender>

                    <asp:HiddenField ID="hdPolicyNumber" runat="server"/>
                    <asp:HiddenField ID="hdName" runat="server"/>
                </td>

           </tr>
                        
            <tr id="trSurveyReasons" valign="top" runat="server">
                <td class="label" nowrap="nowrap">
                    <span>Survey Reasons *</span>
                </td>
                <td>
                    <asp:CheckBoxList ID="chkListSurveyReasons" DataTextField="Name" DataValueField="ID" runat="server"></asp:CheckBoxList>
                </td>
            </tr>
            
            <!--<tr id="trSurveyRequestedDate" valign="top" runat="server">
                <td class="label" nowrap="nowrap">
                    <span>Requested Date *</span>
                </td>
                <td>
                    {datePickerField} defaults to today.
                </td>
            </tr>-->

            <% if (_showSurveyRequestedDateField) { %>
            <uc:Date ID="dtRequestedDate" runat="server" Field="Survey.RequestedDate" Name="Requested Date" IsRequired="False" />
            <% } %>

            <tr>
                <td>&nbsp;</td>
                <td class="buttons">
                    <asp:Button id="btnSearch" Runat="server" CssClass="btn" Text="Search" Visible="false" />
                    <asp:Button id="btnCreateManualSurvey" Runat="server" CssClass="btn" Text="Create Survey" Visible="false" />
                </td>
            </tr>
        </table>
    </cc:Box>
    
    <cc:Box id="boxInsured" runat="server" Title="Results" width="500">
        <table class="fields">
            <uc:Label id="lblClientID" runat="server" field="Insured.ClientID" />
            <% if (CurrentUser.Company.SupportsPortfolioID) {%>
            <uc:Label id="lblPortfolioID" runat="server" field="Insured.PortfolioID" />
            <% } %>
            <uc:Label id="lblName" runat="server" field="Insured.Name" />
            <uc:Label id="lblName2" runat="server" field="Insured.Name2" />
            <uc:Label id="lblAddress" runat="server" field="Insured.HtmlAddress" Name="Address" />
            <uc:Label id="lblBusinessOperations" runat="server" field="Insured.BusinessOperations" />
            <% if (CurrentUser.Company.SupportsBusinessCategory) {%>
            <uc:Label id="lblBusinessCategory" runat="server" field="Insured.BusinessCategory" />
            <% } %>
            <% if (CurrentUser.Company.SupportsDOTNumber) {%>
            <uc:Label id="lblDOTNumber" runat="server" field="Insured.DOTNumber" Name="DOT Number" />
            <% } %>
            <% if (!_hideSICCode) {%>
            <uc:Label id="lblSIC" runat="server" field="Insured.HtmlSIC" Name="SIC" />
            <% } %>
            <% if (CurrentUser.Company.SupportsNAICS) {%>
            <uc:Label id="lblNAICS" runat="server" field="Insured.NAICSCode" Name="NAICS" />
            <% } %>
            <uc:Label id="lblUnderwriter" runat="server" field="Insured.HtmlUnderwriterName" Name="Underwriter" />
            <uc:Label id="lblUnderwriterPhone" runat="server" field="Insured.HtmlUnderwriterPhone" Name="Underwriter Phone" />                       
            <% if (_showOpenSurveysCount) {%>
            <tr>
                <% if (_openSurveysCount == 0) {%>
                    <td class="label" valign="top" nowrap="nowrap">Active Requests</td>
                    <td> 
                        <asp:Label  CssClass="lbl" ID="lblOpenRequest" runat="server">none</asp:Label>
                    </td>
                <% } else {%>
                    <td class="label" valign="top" nowrap="nowrap" style="font-size:medium;font:bold;color:red">Active Requests</td>
                    <td> 
                        <asp:Button id="btnView" Runat="server" CssClass="btn" Text="View Active Requests" Visible="true" OnClientClick="frm.target ='_blank';"/>
                    </td>
                <% } %>
            </tr>
            <% } %>

            <tr>
                <td>&nbsp;</td>
                <td class="buttons">
                    <asp:Button id="btnCreateSurvey" Runat="server" CssClass="btn" Text="Create Survey" />
                    <% if (CurrentUser.Company.SupportsQuickSurveyCreation) {%>
                    <asp:Button id="btnExpressRequest" Runat="server" CssClass="btn" Text="Express Request"/>
                    <% } %>
                </td>
            </tr>
        </table>
    </cc:Box>
    
    <cc:Box id="boxInProgressSurveys" runat="server" Title="Incomplete Survey Requests" width="500">
        <asp:datagrid ID="grdSurveys" Runat="server" CssClass="grid" AutoGenerateColumns="False" PageSize="500" AllowPaging="False" AllowSorting="False">
            <headerstyle CssClass="header" />
            <itemstyle CssClass="item" />
            <alternatingitemstyle CssClass="altItem" />
            <footerstyle CssClass="footer" />
            <pagerstyle CssClass="pager" Mode="NumericPages" />
            <columns>
                <asp:hyperlinkcolumn HeaderText="Temp&nbsp;#" DataTextField="Number" DataNavigateUrlField="ID"
                    DataNavigateUrlFormatString="CreateSurvey.aspx?surveyid={0}" />
                <asp:TemplateColumn ItemStyle-Wrap="false" HeaderText="Date/Time Started">
                    <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "CreateDate", DateTime.MinValue, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Survey Type">
                    <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Type.Type.Name", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Client ID">
                    <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Insured.ClientID", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Insured">
                    <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Insured.Name", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Delete">
                    <ItemTemplate>
                            <asp:LinkButton ID="lnkDelete" Text="Delete" CommandName="DELETE" runat="server" CausesValidation="false"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </columns>
        </asp:datagrid>
    </cc:Box>
   
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <div style="display: none;"><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" alt="" /></div>
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function() { 
            $.blockUI.defaults.css = {};
            $.blockUI.defaults.overlayCSS.opacity = .2;  
 
            $('#btnSearch', this).click(function() { 
                if (Page_IsValid) {
                    $.blockUI({ message: '<h3><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" />&nbsp;&nbsp;&nbsp;Processing, one moment please...</h3>' });
                }
            });
            
            $('#btnCreateSurvey', this).click(function() { 
                if (Page_IsValid) {
                    $.blockUI({ message: '<h3><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" />&nbsp;&nbsp;&nbsp;Processing, one moment please...</h3>' });
                }
            });

            //focus on first form field
            $(function () { 
                $(window).load(function () { 
                    $("#numPolicyNumber_txt").focus();
                    $("#numSubmissionNumber_txt").focus();
                }); 
             })
        });

        function ToggleBox(id) {
            ToggleControlDisplay(id);
        }

        function aceInsured_itemSelected(sender, e) {
            var hdPolicyNumber = $get('<%= hdPolicyNumber.ClientID %>');
        hdPolicyNumber.value = e.get_value();

        var hdName = $get('<%= hdName.ClientID %>');
        hdName.value = e.get_text();
        }

        function Toggle(cboValue) {
        if (cboValue == "") {
            document.getElementById('numPolicyNumber_lbl').style.display = 'none';
            document.getElementById('numPolicyNumber_txt').style.display = 'none';
            document.getElementById('numClientID_lbl').style.display = 'none';
            document.getElementById('numClientID_txt').style.display = 'none';
        }
        if (cboValue == "PR") {
            document.getElementById('numPolicyNumber_lbl').style.display = 'none';
            document.getElementById('numPolicyNumber_txt').style.display = 'none';
            document.getElementById('numClientID_lbl').style.display = 'inline';
            document.getElementById('numClientID_txt').style.display = 'inline';
        }
        else {
            document.getElementById('numClientID_lbl').style.display = 'none';
            document.getElementById('numClientID_txt').style.display = 'none';
            document.getElementById('numPolicyNumber_lbl').style.display = 'inline';
            document.getElementById('numPolicyNumber_txt').style.display = 'inline';
        }
    }
   </script>
    </form>
</body>
</html>
