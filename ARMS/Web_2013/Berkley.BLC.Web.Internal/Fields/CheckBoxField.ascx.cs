using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.ComponentModel;
using Berkley.BLC.Entities;
using QCI.Web.Validation;

namespace Berkley.BLC.Web.Internal.Fields
{
    public partial class CheckBoxField : MappedFieldControl
    {
        protected bool noEndRow;

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Control_Load);
            this.PreRender += new EventHandler(this.Control_PreRender);

        }
        #endregion

        private void Control_Load(object sender, System.EventArgs e)
        {
            // put the radio buttons into the same group
            if (!this.IsPostBack)
            {
                rdoYes.GroupName = this.UniqueID;
                rdoNo.GroupName = this.UniqueID;
            }
        }

        private void Control_PreRender(object sender, EventArgs e)
        {
            if (lbl.Text.Length == 0)
            {
                lbl.Text = this.GetDefaultLabelText();
            }
            
            // enable the custom radio button validator as needed
            bool enabled = (this.Type == CheckBoxFieldType.RadioButton);
            val.Visible = enabled;
            if (enabled)
            {
                val.Text = "*";
                val.ErrorMessage = string.Format("{0} is required.", base.Name);
            }
        }


        /// <summary>
        /// Gets the internal control responsible for capturing user input.
        /// </summary>
        public override Control InputControl
        {
            get { return chk; }
        }


        /// <summary>
        /// Gets or sets a value indicating whether a postback to the server automatically occurs when the user changes the list selection.
        /// </summary>
        public bool AutoPostback
        {
            get { return chk.AutoPostBack; }
            set { chk.AutoPostBack = value; }
        }


        /// <summary>
        /// Gets or sets a value indicating whether the control is checked.
        /// </summary>
        public bool Checked
        {
            get
            {
                if (this.Type == CheckBoxFieldType.CheckBox)
                {
                    return chk.Checked;
                }
                else // radio
                {
                    return rdoYes.Checked;
                }
            }
            set
            {
                chk.Checked = value;
                rdoYes.Checked = value;
                rdoNo.Checked = !value;
            }
        }

        /// <summary>
        /// Gets or sets the label value for this control.  This value will appear to the left of the input field.
        /// </summary>
        public string Label
        {
            get { return this.lbl.Text; }
            set { this.lbl.Text = value; }
        }

        /// <summary>
        /// Gets or sets a value that determines whether the control is enabled.
        /// </summary>
        public bool Enabled
        {
            get { return chk.Enabled; }
            set { chk.Enabled = value; }
        }

        /// <summary>
        /// Gets or sets the text label associated to the control. This text will appear to the right of the check box.
        /// </summary>
        public string Text
        {
            get { return chk.Text; }
            set { chk.Text = value; }
        }

        /// <summary>
        /// Gets or sets a flag indicating to suspend injecting a end row tag.
        /// </summary>
        public bool NoEndTR
        {
            get { return noEndRow; }
            set { noEndRow = (bool)value; }
        }

        /// <summary>
        /// Gets or sets the type of check box (CheckBox or RadioButton).
        /// </summary>
        public CheckBoxFieldType Type
        {
            get
            {
                object value = this.ViewState["Type"];
                return (value != null) ? (CheckBoxFieldType)value : CheckBoxFieldType.CheckBox;
            }
            set
            {
                this.ViewState["Type"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the optional directions after the text box.
        /// </summary>
        public string Directions
        {
            get { return lblDirections.Text; }
            set { lblDirections.Text = value; }
        }


        /// <summary>
        /// Gets the user input value from the control.
        /// </summary>
        /// <returns>Value from the control.</returns>
        public override object GetValue()
        {
            return this.Checked;
        }


        /// <summary>
        /// Sets the user input value of the control.
        /// </summary>
        /// <param name="value">Value to be set.</param>
        public override void SetValue(object value)
        {
            this.Checked = Convert.ToBoolean(value);
        }


        protected override string GetDefaultLabelText()
        {
            if (chk.Text.Length > 0)
            {
                return string.Empty;
            }
            return base.GetDefaultLabelText();
        }

    }

    public enum CheckBoxFieldType
    {
        CheckBox,
        RadioButton
    }
}
