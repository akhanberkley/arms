<%@ Control Language="C#" AutoEventWireup="false" CodeFile="TextField.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Fields.TextField" %>

<% if (this.EnableCheckSpelling) { %>
<script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/Keyoti_RapidSpell_Web_Common/RapidSpell-DIALOG.js"></script>
<script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/javascripts/csshttprequest.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="<%=ARMSUtility.TemplatePath %>/atd.css" />

 <script type="text/javascript">
     function check<%=this.ID %>(id) {         
         rapidSpell.dialog_spellCheck(true, '<%=this.ID %>_txt');
     }
 </script>
 <% } %>

<% if (base.TableRow) {%><tr><% } %>
<td class="label" valign="<%= (txt.Rows > 1) ? "top" : "middle" %>" nowrap>
	<asp:Label ID="lbl" Runat="server" /><%= (this.IsRequired) ? " *" : "" %>
</td>
<td>
	<asp:TextBox ID="txt" Runat="server" CssClass="txt" />
	<asp:RequiredFieldValidator ID="val" Runat="server" CssClass="val" ControlToValidate="txt" Display="Dynamic" />
	<asp:RegularExpressionValidator ID="valRegex" Runat="server" CssClass="val" ControlToValidate="txt" Display="Dynamic" />
    <% if (Directions.Length > 0) {%>
    &nbsp;<asp:Label ID="lblDirections" Runat="server" />
    <% } %>

    <% if (this.EnableCheckSpelling) { %>
    <td valign="top">
        <a href="javascript:check<%=this.ID %>('<%=this.ID %>_txt')" id="checkLink<%=this.ID %>"><img alt="Check Spelling" style="border:none;" src="<%=ARMSUtility.TemplatePath %>/javascripts/Images/atdbuttontr.gif"></a>
    </td>
    <% } %>
</td>
<% if (base.TableRow) {%></tr><% } %>


