using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using QCI.Web.Validation;
using Berkley.BLC.Entities;

namespace Berkley.BLC.Web.Internal.Fields
{
    public partial class NumberField : MappedFieldControl
    {
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Control_Load);
            this.PreRender += new System.EventHandler(this.Control_PreRender);
            this.Registered += new EventHandler(this.Control_Registered);
        }
        #endregion

        private void Control_Registered(object sender, EventArgs e)
        {
            Type type = this.FieldInfo.DataType;

            if (type == typeof(int) || type == typeof(long) || type == typeof(short))
            {
                this.DataType = ValidationDataType.Integer;
            }
            else if (type == typeof(float) || type == typeof(double) || type == typeof(decimal))
            {
                this.DataType = ValidationDataType.Double;
            }
            else
            {
                throw new InvalidOperationException("Control '" + this.ID + "' cannot be mapped to a field of type '" + type + ".");
            }
        }

        private void Control_Load(object sender, System.EventArgs e)
        {
            // nothing to initalize
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            if (lbl.Text.Length == 0)
            {
                lbl.Text = this.GetDefaultLabelText();
            }

            bool enabled;

            enabled = this.IsRequired;
            val.Visible = enabled;
            if (enabled)
            {
                val.Text = "*";
                val.ErrorMessage = string.Format("{0} is required.", base.Name);
            }

            enabled = (valRegex.ValidationExpression.Length > 0);
            valRegex.Visible = enabled;
            if (enabled)
            {
                valRegex.Text = "*";
                valRegex.ErrorMessage = string.Format("{0} is not correctly formatted.", base.Name);
            }

            switch (valType.Type)
            {
                case ValidationDataType.Date:
                case ValidationDataType.String:
                    {
                        throw new InvalidOperationException("DataType property must be set to one of the numeric types.");
                    }
            }
            valType.Text = "*";
            valType.ErrorMessage = string.Format("{0} is not a valid value.", base.Name);
        }


        /// <summary>
        /// Gets the internal control responsible for capturing user input.
        /// </summary>
        public override Control InputControl
        {
            get { return this.txt; }
        }


        /// <summary>
        /// Gets or sets the text content of the control.
        /// </summary>
        public string Text
        {
            get { return this.txt.Text; }
            set
            {
                if (value != null)
                {
                    value = value.Trim();
                }
                txt.Text = value;
            }
        }


        /// <summary>
        /// Gets or sets the maximum number of characters allowed in the text box.
        /// </summary>
        public int MaxLength
        {
            get { return txt.MaxLength; }
            set { txt.MaxLength = value; }
        }

        /// <summary>
        /// Gets or sets the optional directions after the text box.
        /// </summary>
        public string Directions
        {
            get { return lblDirections.Text; }
            set { lblDirections.Text = value; }
        }


        /// <summary>
        /// Gets or sets the display width of the text box in characters.
        /// </summary>
        public int Columns
        {
            get { return txt.Columns; }
            set { txt.Columns = value; }
        }


        /// <summary>
        /// Gets or sets the regular expression that determines the pattern used to validate the user's input.
        /// </summary>
        public string RegEx
        {
            get { return valRegex.ValidationExpression; }
            set { valRegex.ValidationExpression = value; }
        }

        /// <summary>
        /// Gets or sets the label value for this control.  This value will appear to the left of the input field.
        /// </summary>
        public string Label
        {
            get { return this.lbl.Text; }
            set { this.lbl.Text = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the the control is enabled.
        /// </summary>
        public bool Enabled
        {
            get { return txt.Enabled; }
            set { txt.Enabled = value; }
        }


        /// <summary>
        /// Gets or sets the data type allowed in the input field.
        /// </summary>
        public ValidationDataType DataType
        {
            get
            {
                return valType.Type;
            }
            set
            {
                switch (value)
                {
                    case ValidationDataType.Date:
                    case ValidationDataType.String:
                        {
                            throw new ArgumentOutOfRangeException("value", value, "Value must be one of the numeric types.");
                        }
                }
                valType.Type = value;
            }
        }


        /// <summary>
        /// Gets the user input value from the control.
        /// </summary>
        /// <returns>Value from the control.</returns>
        public override object GetValue()
        {
            Type targetType;
            object value;
            switch (this.DataType)
            {
                case ValidationDataType.Integer:
                    {
                        targetType = typeof(int);
                        value = (txt.Text.Length > 0) ? Convert.ChangeType(txt.Text, targetType) : Int32.MinValue;
                        break;
                    }
                case ValidationDataType.Double:
                case ValidationDataType.Currency:
                    {
                        targetType = typeof(decimal);
                        value = (txt.Text.Length > 0) ? Convert.ChangeType(txt.Text, targetType) : Decimal.MinValue;
                        break;
                    }
                default:
                    {
                        throw new InvalidOperationException("DataType property is not set to one of the numeric types.");
                    }
            }

            return value;
        }

        /// <summary>
        /// Sets the user input value of the control.
        /// </summary>
        /// <param name="value">Value to be set.</param>
        public override void SetValue(object value)
        {
            if (this.DataType == ValidationDataType.Integer)
            {
                this.Text = (value != null && Convert.ToInt32(value) != Int32.MinValue) ? value.ToString() : string.Empty;
            }
            else if (this.DataType == ValidationDataType.Double || this.DataType == ValidationDataType.Currency)
            {
                this.Text = (value != null && Convert.ToDecimal(value) != Decimal.MinValue) ? value.ToString() : string.Empty;
            }
            else
            {
                this.Text = (value != null) ? value.ToString() : null;
            }
        }
    }
}
