<%@ Control Language="C#" AutoEventWireup="false" CodeFile="LabelField.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Fields.LabelField" %>
<% if (base.TableRow) {%><tr><% } %>
<td class="label" valign="top" nowrap>
	<asp:Label ID="lbl" Runat="server" />
</td>
<td>
	<asp:Label ID="lblValue" Runat="server" CssClass="lbl"></asp:Label>
</td>
<% if (base.TableRow) {%></tr><% } %>
