<%@ Control Language="C#" AutoEventWireup="false" CodeFile="MultiSelectComboField.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Fields.MultiSelectComboField" %>
<% if (base.TableRow) {%><tr><% } %>
<td class="label" nowrap>
	<asp:Label ID="lbl" Runat="server" /><%= (this.IsRequired) ? " *" : "" %>
</td>
<td>
    <asp:HiddenField ID="hdnMultiValues" runat="server" />
	<asp:DropDownList ID="cboMulti" Runat="server" CssClass="cbo" />
	<asp:TextBox ID="txt" Runat="server" CssClass="txt" />
	<asp:RequiredFieldValidator ID="val" Runat="server" CssClass="val" ControlToValidate="cboMulti" Display="Dynamic" />
	<asp:RequiredFieldValidator ID="valTxt" Runat="server" CssClass="val" ControlToValidate="txt" Display="Dynamic" Visible="False" />
<% if (!noEndRow) {%>
</td>
<% if (base.TableRow) {%></tr><% } %>
<% } %>
