<%@ Control Language="C#" AutoEventWireup="false" CodeFile="ComboFieldSearch.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Fields.ComboFieldSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<% if (base.TableRow) {%><tr><% } %>
<td class="label" nowrap>
	<asp:Label ID="lbl" Runat="server" /><%= (this.IsRequired) ? " *" : "" %>
</td>
<td style="padding-top:10px;">
	<asp:DropDownList ID="cbo" Runat="server" CssClass="cbo" />
	<ajaxToolkit:ListSearchExtender ID="ListSearchExtender2" runat="server" TargetControlID="cbo" PromptCssClass="ListSearchExtenderPrompt"></ajaxToolkit:ListSearchExtender>
	<asp:TextBox ID="txt" Runat="server" CssClass="txt" />
	<asp:RequiredFieldValidator ID="val" Runat="server" CssClass="val" ControlToValidate="cbo" Display="Dynamic" />
	<asp:RequiredFieldValidator ID="valTxt" Runat="server" CssClass="val" ControlToValidate="txt" Display="Dynamic" Visible="False" />
<% if (!noEndRow) {%>
</td>
<% if (base.TableRow) {%></tr><% } %>
<% } %>
