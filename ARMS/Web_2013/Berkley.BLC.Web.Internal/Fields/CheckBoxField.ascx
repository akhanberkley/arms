<%@ Control Language="C#" AutoEventWireup="false" CodeFile="CheckBoxField.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Fields.CheckBoxField" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Fields" %>
<%@ Import Namespace="Berkley.BLC.Web.Internal.Fields" %>
<% if (base.TableRow) {%><tr><% } %>
<td class="label" valign="middle" nowrap>
	<asp:Label ID="lbl" Runat="server" />
</td>
<td>
	<% if( this.Type == CheckBoxFieldType.CheckBox ) { %>
	<asp:CheckBox ID="chk" Runat="server" CssClass="chk" />
    <% if (Directions.Length > 0) {%>
    &nbsp;<asp:Label ID="lblDirections" Runat="server" ForeColor="Red" />
    <% } %>
	<% } else { %>
	<table>
	<tr>
		<td nowrap>
			<asp:RadioButton id="rdoYes" runat="server" Text="Yes" TextAlign="Left" />
			<asp:RadioButton id="rdoNo" runat="server" Text="No" TextAlign="Left" />
			<cc:RadioButtonRequiredFieldValidator id="val" runat="server" CssClass="val" ControlToValidate="rdoYes" Display="Dynamic" />
		</td>
		<td>&nbsp;</td>
		<td width="400"><%= chk.Text %></td>
	</tr>
	</table>
	<% } %>
<% if (!noEndRow) {%>
</td>
<% if (base.TableRow) {%></tr><% } %>
<% } %>
