using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using QCI.Web.Validation;
using Berkley.BLC.Entities;

namespace Berkley.BLC.Web.Internal.Fields
{
    public partial class TextField : MappedFieldControl
    {
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Control_Load);
            this.PreRender += new System.EventHandler(this.Control_PreRender);
            this.Registered += new EventHandler(this.Control_Registered);
        }
        #endregion

        private void Control_Registered(object sender, EventArgs e)
        {
            if (this.FieldInfo.MaxLength > 0 && this.MaxLength == 0)
            {
                this.MaxLength = this.FieldInfo.MaxLength;
            }
            if (this.FieldInfo.Columns > 0 && this.Columns == 0)
            {
                this.Columns = this.FieldInfo.Columns;
            }
            if (this.FieldInfo.Rows > 0 && this.Rows == 0)
            {
                this.Rows = this.FieldInfo.Rows;
            }
            if (this.FieldInfo.Format != null && this.Format.Length == 0)
            {
                this.Format = this.FieldInfo.Format;
            }
            if (this.FieldInfo.InputMask != null && this.InputMask.Length == 0)
            {
                this.InputMask = this.FieldInfo.InputMask;
            }
        }

        private void Control_Load(object sender, System.EventArgs e)
        {
            // nothing to initalize
        }

        private void Control_PreRender(object sender, System.EventArgs e)
        {
            if (lbl.Text.Length == 0)
            {
                lbl.Text = this.GetDefaultLabelText();
            }
            
            bool enabled;

            enabled = this.IsRequired;
            val.Visible = enabled;
            if (enabled)
            {
                val.Text = "*";
                val.ErrorMessage = string.Format("{0} is required.", base.Name);
            }

            enabled = (valRegex.ValidationExpression.Length > 0);
            valRegex.Visible = enabled;
            if (enabled)
            {
                valRegex.Text = "*";
                valRegex.ErrorMessage = string.Format("{0} is not correctly formatted.", base.Name);
            }

            if (this.InputMask.Length > 0)
            {
                ARMSUtility.AddJavaScriptToPage(this.Page, ARMSUtility.AppPath + "/AutoFormat.js");
                string script = string.Format("StartAutoFormat(this, '{0}')", this.InputMask);
                txt.Attributes.Add("onfocus", script);
            }
        }


        /// <summary>
        /// Gets the internal control responsible for capturing user input.
        /// </summary>
        public override Control InputControl
        {
            get { return this.txt; }
        }


        /// <summary>
        /// Gets or sets the text content of the control.
        /// </summary>
        public string Text
        {
            get { return this.txt.Text; }
            set
            {
                if (value != null)
                {
                    value = value.Trim();
                }
                txt.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the behavior mode (single-line, multiline, or password) of the control.
        /// </summary>
        public TextBoxMode TextMode
        {
            get { return txt.TextMode; }
            set { txt.TextMode = value; }
        }

        /// <summary>
        /// Gets or sets the maximum number of characters allowed in the text box.
        /// </summary>
        public int MaxLength
        {
            get { return txt.MaxLength; }
            set { txt.MaxLength = value; }
        }

        /// <summary>
        /// Gets or sets the optional directions after the text box.
        /// </summary>
        public string Directions
        {
            get { return lblDirections.Text; }
            set { lblDirections.Text = value; }
        }

        /// <summary>
        /// Gets or sets the display width of the text box in characters.
        /// </summary>
        public int Columns
        {
            get { return txt.Columns; }
            set { txt.Columns = value; }
        }

        /// <summary>
        /// Gets or sets the number of rows to display.
        /// </summary>
        public int Rows
        {
            get { return this.txt.Rows; }
            set
            {
                if (txt.Rows != value)
                {
                    txt.Rows = value;
                    txt.TextMode = (value > 1) ? TextBoxMode.MultiLine : TextBoxMode.SingleLine;
                }
            }
        }

        /// <summary>
        /// Gets or sets the regular expression that determines the pattern used to validate the user's input.
        /// </summary>
        public string Format
        {
            get { return valRegex.ValidationExpression; }
            set { valRegex.ValidationExpression = (value != null) ? value : string.Empty; }
        }

        /// <summary>
        /// Gets or sets the name of the client-side input mask to use to auto-correct the user's input.
        /// </summary>
        public string InputMask
        {
            get
            {
                object value = this.ViewState["InputMask"];
                return (value != null) ? (string)value : string.Empty;
            }
            set { this.ViewState["InputMask"] = value; }
        }

        /// <summary>
        /// Gets or sets the label value for this control.  This value will appear to the left of the input field.
        /// </summary>
        public string Label
        {
            get { return this.lbl.Text; }
            set { this.lbl.Text = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the the control is enabled.
        /// </summary>
        public bool Enabled
        {
            get { return txt.Enabled; }
            set { txt.Enabled = value; }
        }

        private bool _checkSpellingEnabled = false;

        /// <summary>
        /// Gets or sets a value indicating whether the the control is enabled.
        /// </summary>
        public bool EnableCheckSpelling
        {
            get { return _checkSpellingEnabled; }
            set { _checkSpellingEnabled = value; }
        }

        /// <summary>
        /// Gets the user input value from the control.
        /// </summary>
        /// <returns>Value from the control.</returns>
        public override object GetValue()
        {
            int maxLength = this.MaxLength;
            if (maxLength == 0) maxLength = int.MaxValue;

            return FieldValidation.ValidateStringField(txt, base.Name, maxLength, this.IsRequired);
        }

        /// <summary>
        /// Sets the user input value of the control.
        /// </summary>
        /// <param name="value">Value to be set.</param>
        public override void SetValue(object value)
        {
            this.Text = (value != null) ? value.ToString() : null;
        }
    }
}
