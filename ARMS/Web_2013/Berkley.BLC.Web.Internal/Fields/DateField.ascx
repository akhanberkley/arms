<%@ Control Language="C#" AutoEventWireup="false" CodeFile="DateField.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Fields.DateField" %>
<%@ Register TagPrefix="cc" Namespace="QCI.Web.Controls" %>
<% if (base.TableRow) {%><tr><% } %>
<td class="label" nowrap>
	<asp:Label ID="lbl" Runat="server" /><%= (this.IsRequired) ? " *" : "" %>
</td>
<td>
	<asp:TextBox ID="txt" Runat="server" CssClass="txt" Columns="15" Visible="False" MaxLength="11" />
	<cc:DatePicker id="dp" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" />
	<asp:RequiredFieldValidator ID="val" Runat="server" CssClass="val" ControlToValidate="dp" Display="Dynamic" />
	<asp:CompareValidator ID="valType" Runat="server" CssClass="val" ControlToValidate="dp" Display="Dynamic" Type="Date" Operator="DataTypeCheck" />
	<asp:CompareValidator ID="valMin" Runat="server" CssClass="val" ControlToValidate="dp" Display="Dynamic" Type="Date" Operator="GreaterThanEqual" />
	<asp:CompareValidator ID="valMax" Runat="server" CssClass="val" ControlToValidate="dp" Display="Dynamic" Type="Date" Operator="LessThanEqual" />
<% if (!noEndRow) {%>
</td>
<% if (base.TableRow) {%></tr><% } %>
<% } %>
