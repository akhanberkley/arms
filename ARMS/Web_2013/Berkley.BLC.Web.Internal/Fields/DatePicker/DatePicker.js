// ----------------------------------------------------------------------------
// Copyright (c) 2003-2005 QCI, Inc.  All rights reserved.
// ----------------------------------------------------------------------------

//TODO: add databinding and a nice shadow effect
//TODO: strike out the today link if min/max do not include it
//TODO: upgrade the hiding/showing of combos and applets

var _dpMinDate, _dpMaxDate;
var _dpFirstDayOfWeek;
var _dpDateFormat;
var _dpTargetTextbox;

var _dpMainDiv, _dpPickListDiv;
var _dpYear, _dpMonth;
var _dpDate, _dpToday;
var _dpIntervalID, _dpTimeoutID;
var _dpYearInList;

var _dpIsLoaded = false;
var _dpShow = false;

// preload the images
var _dpImageNames = new Array("drop.gif", "drop_on.gif", "left.gif", "left_on.gif", "right.gif", "right_on.gif");
var _dpImages = new Array();
for( i = 0; i < _dpImageNames.length; i++ )
{
	_dpImages[i] = new Image();
	_dpImages[i].src = _dpResPath + _dpImageNames[i];
}


function DP_Init()
{
	_dpMainDiv = document.getElementById("DP_MainDiv");
	_dpPickListDiv = document.getElementById("DP_PickListDiv");

	HideDatePicker();

	_dpMainDiv.onclick = function() { _dpShow = true; }
	_dpPickListDiv.onclick = function() { _dpShow = true; }
	_dpPickListDiv.onmouseover = function() { clearTimeout(_dpTimeoutID); }
	_dpPickListDiv.onmouseout = function() { clearTimeout(_dpTimeoutID); _dpTimeoutID=setTimeout("DP_HidePickList()", 100); }

	//TODO: attach to these events only when the picker is visible??
	document.onkeypress = function dp_keypress(evt)
	{
		if( !evt ) evt = window.event;
		if( !evt ) return;
		var charCode = (evt.charCode) ? evt.charCode : (evt.which) ? evt.which : evt.keyCode;
		if( charCode == 27 ) HideDatePicker();
	}
	document.onclick = function()
	{
		if( !_dpShow && _dpMainDiv.style.visibility != "hidden" )
		{
			HideDatePicker();
		}
		_dpShow = false;
	}

	_dpIsLoaded = true;
}


// ----------------------------------------------------------------------------
// Calendar Methods
// ----------------------------------------------------------------------------

function HideDatePicker()
{
	DP_HidePickList();
	_dpMainDiv.style.visibility = "hidden";

	DP_ShowHiddenCombos();
	//DP_ShowElements('SELECT');
	//DP_ShowElements('APPLET');

	_dpShow = false;
}

function ShowDatePicker(txt_id, format, firstDow, minDate, maxDate)
{
	if( !_dpIsLoaded )
	{
		DP_Init();
		if( !_dpIsLoaded ) return;
	}

	var txt = document.getElementById(txt_id);

	if( _dpMainDiv.style.visibility != "hidden" )
	{
		HideDatePicker();
		if( txt == _dpTargetTextbox ) return;
	}

	// at this point the calendar is hidden

	// set member vars
	_dpTargetTextbox = txt;
	_dpDateFormat = format;
	_dpFirstDayOfWeek = firstDow;
	_dpMinDate = minDate;
	_dpMaxDate = maxDate;
	_dpToday = DP_GetDateOnly(new Date());

	// parse the format string
	_dpDate = DP_StringToDate(txt.value, format);
	if( _dpDate == null )
	{
		_dpDate = _dpToday;
	}

	_dpYear = _dpDate.getFullYear();
	_dpMonth = _dpDate.getMonth();

	// build/refresh the picker
	DP_RefreshCalendar();

	// position and show the picker
	var box = DP_GetBoundingBox(txt);
	_dpMainDiv.style.left = box.left;
	_dpMainDiv.style.top = box.bottom + 2;
	_dpMainDiv.style.visibility = "visible";

	// hide overlapping elements that might appear on top of this our picker
	DP_HideOverlappingCombos(_dpMainDiv);
	//DP_HideElements('SELECT', _dpMainDiv);
	//DP_HideElements('APPLET', _dpMainDiv);

	_dpShow = true;
}


function DP_RefreshCalendar()
{
	var startDate = new Date(_dpYear, _dpMonth, 1);
	var endDate = new Date(_dpYear, _dpMonth + 1, 0);
	var startDow = startDate.getDay();

	// get the days in this month
	var daysInMonth = endDate.getDate();

	var html = new Array();
	html.push( "<table>\n<tr>\n" );

	// create day of week header row
	for( var i = 0; i < 7; i++ )
	{
		html.push( "<td width=27 align=right><b>" + _dpDayNames[(i + _dpFirstDayOfWeek) % 7] + "</b></td>\n" );
	}
	html.push( "</tr>\n<tr>\n" );

	var offset = (startDow - _dpFirstDayOfWeek + 7) % 7;
	if( offset == 0 ) offset = 7;
	var endDay = 42 - offset;

	for( var d = 1 - offset; d <= endDay; d++ )
	{
		var date = new Date(_dpYear, _dpMonth, d);
		var dow = (startDow + d + 13) % 7;

		var cssClass = (date.getMonth() == _dpMonth ? "In" : "Out");
		if( date.getTime() == _dpToday.getTime() ) // day is today
		{
			cssClass = "Today";
		}
		if( date.getTime() == _dpDate.getTime() ) // selected day
		{
			cssClass += " SelectedDay";
		}

		if( (!_dpMinDate || date >= _dpMinDate) && (!_dpMaxDate || date <= _dpMaxDate) )
		{
			html.push( "<td align=right><a class='" + cssClass + "' href='javascript:DP_PickDay(" + d + ");'>&nbsp;" + date.getDate() + "&nbsp;</a></td>\n" );
		}
		else // date out of bounds
		{
			html.push( "<td align=right><a class='" + cssClass + " Invalid'>&nbsp;" + date.getDate() + "&nbsp;</a></td>\n" );
		}

		// see if we need to start a new week row
		if( (dow + 1) % 7 == _dpFirstDayOfWeek )
		{
			if( d < endDay )
			{
				html.push( "</tr>\n<tr>\n" );
			}
		}
	}

	html.push( "</tr>\n</table>\n" );

	document.getElementById("DP_CalendarDiv").innerHTML = html.join('');
	document.getElementById("DP_MonthSpan").innerHTML = "&nbsp;" + _dpMonthNames[_dpMonth] + "&nbsp;<img id='DP_MonthImg' src='" + _dpResPath + "drop.gif' width='12' height='10' border=0>";
	document.getElementById("DP_YearSpan").innerHTML = "&nbsp;" + _dpYear + "&nbsp;<img id='DP_YearImg' src='" + _dpResPath + "drop.gif' width='12' height='10' border=0>";
}

function DP_PickDay(day)
{
	// get the date selected
	var date;
	if( day == 'none' ) date = null;
	else if( day == 'today' ) date = new Date(_dpToday.getTime());
	else date = new Date(_dpYear, _dpMonth, day);

	// prevent the user from picking a date that is out of bounds
	if( date && ((_dpMinDate && date < _dpMinDate) || (_dpMaxDate && date > _dpMaxDate)) )
	{
		_dpYear = date.getFullYear();
		_dpMonth = date.getMonth();

		DP_RefreshCalendar();
		return;
	}

	_dpDate = date;

	// update the textbox
	var oldValue = _dpTargetTextbox.value;
	var newValue = (date ? DP_DateToString(date, _dpDateFormat) : "");
	_dpTargetTextbox.value = newValue;

	HideDatePicker();

	// raise the onchange event if a handler is attached
	if( newValue != oldValue && _dpTargetTextbox.onchange )
	{
		if( _dpTargetTextbox.fireEvent )
		{
			_dpTargetTextbox.fireEvent('onchange');
		}
		else
		{
			_dpTargetTextbox.onchange();
		}
	}
}



// ----------------------------------------------------------------------------
// General Pick List Handlers
// ----------------------------------------------------------------------------
function DP_PickListOver(elem)
{
	switch(elem.id)
	{
		case 'DP_LeftSpan': DP_SwapImage("DP_LeftImg", "left_on.gif"); break;
		case 'DP_RightSpan': DP_SwapImage("DP_RightImg", "right_on.gif"); break;
		case 'DP_MonthSpan': DP_SwapImage("DP_MonthImg", "drop_on.gif"); break;
		case 'DP_YearSpan': DP_SwapImage("DP_YearImg", "drop_on.gif"); break;
	}
	elem.className = "DP_controlBoxOn";
}

function DP_PickListOut(elem)
{
	clearInterval(_dpIntervalID);
	_dpTimeoutID = setTimeout("DP_HidePickList()", 100)

	switch(elem.id)
	{
		case 'DP_LeftSpan': DP_SwapImage("DP_LeftImg", "left.gif"); break;
		case 'DP_RightSpan': DP_SwapImage("DP_RightImg", "right.gif"); break;
		case 'DP_MonthSpan': DP_SwapImage("DP_MonthImg", "drop.gif"); break;
		case 'DP_YearSpan': DP_SwapImage("DP_YearImg", "drop.gif"); break;
	}
	elem.className = "DP_controlBox";
}


// ----------------------------------------------------------------------------
// Month Pick List Methods
// ----------------------------------------------------------------------------

function DP_OnMonthArrowDown(delta)
{
	clearTimeout(_dpTimeoutID);
	var func = (delta < 0 ? "DP_StartDecMonth()" : "DP_StartIncMonth()");
	_dpTimeoutID = setTimeout(func, 250);
}
function DP_OnMonthArrowUp(delta)
{
	clearTimeout(_dpTimeoutID);
	clearInterval(_dpIntervalID);
}

function DP_StartDecMonth()
{
	_dpIntervalID = setInterval("DP_DecMonth()", 100);
}
function DP_StartIncMonth()
{
	_dpIntervalID = setInterval("DP_IncMonth()", 100);
}

function DP_IncMonth()
{
	_dpMonth++;
	if( _dpMonth > 11 )
	{
		_dpMonth = 0;
		_dpYear++
	}
	DP_RefreshCalendar();
}

function DP_DecMonth()
{
	_dpMonth--;
	if( _dpMonth < 0 )
	{
		_dpMonth = 11;
		_dpYear--;
	}
	DP_RefreshCalendar();
}

function DP_ShowMonthList(elem)
{
	DP_HidePickList();

	var html = new Array();
	html.push( "<table cellspacing=0 cellpadding=0>" );
	for( m = 0; m < 12; m++ )
	{
		var name = _dpMonthNames[m];
		if( m == _dpMonth )
		{
			name = "<b>" + name + "</b>";
		}
		html.push( "<tr><td onclick='DP_PickMonth(" + m + ", event)' onmouseover='this.className=\"Hover\"' onmouseout='this.className=\"\"'>" + name + "</td></tr>" );
	}
	html.push("</table>");

	_dpPickListDiv.innerHTML = html.join('');

	var box = DP_GetBoundingBox(elem);
	_dpPickListDiv.style.left = box.left;
	_dpPickListDiv.style.top = box.bottom;
	_dpPickListDiv.style.visibility = "visible";

	DP_HideOverlappingCombos(_dpPickListDiv);
	//DP_HideElements('SELECT', _dpPickListDiv);
	//DP_HideElements('APPLET', _dpPickListDiv);
}

function DP_PickMonth(m, evt)
{
	if( !evt ) evt = window.event;

	DP_HidePickList();

	_dpMonth = m;
	DP_RefreshCalendar();

	//TODO: not cross-broswer line
	evt.cancelBubble = true;
}


// ----------------------------------------------------------------------------
// Year Pick List Methods
// ----------------------------------------------------------------------------

function DP_IncYear()
{
	DP_ShiftYearList(1);
}

function DP_DecYear()
{
	DP_ShiftYearList(-1);
}

function DP_ShiftYearList(delta)
{
	_dpYearInList += delta;
	for( i = 0; i < 7; i++ )
	{
		var year = _dpYearInList + i;
		if( year == _dpYear )
		{
			year = "<b>" + year + "</b>";
		}
		document.getElementById("y" + i).innerHTML = year;
	}
}

function DP_PickYear(year)
{
	DP_HidePickList();
	_dpYear = parseInt(year + _dpYearInList);
	DP_RefreshCalendar();
}


function DP_ShowYearList(elem)
{
	DP_HidePickList();

	var html = new Array();
	html.push( "<table cellspacing=0 cellpadding=0>" );
	html.push( "<tr><td align='center' onmouseover='this.className=\"Hover\"' onmouseout='this.className=\"\"' onmousedown='clearInterval(_dpIntervalID);_dpIntervalID=setInterval(\"DP_DecYear()\", 30)' onmouseup='clearInterval(_dpIntervalID)'>-</td></tr>" );

	_dpYearInList = _dpYear - 3;
	for( var i = 0; i < 7; i++ )
	{
		var year = _dpYearInList + i;
		if( year == _dpYear )
		{
			year = "<B>" + year + "</B>";
		}

		html.push( "<tr><td id='y" + i + "' onclick='DP_PickYear(" + i + ", event)' onmouseover='this.className=\"Hover\"' onmouseout='this.className=\"\"'>" + year + "</td></tr>" );
	}

	html.push( "<tr><td align='center' onmouseover='this.className=\"Hover\"' onmouseout='this.className=\"\"' onmousedown='clearInterval(_dpIntervalID);_dpIntervalID=setInterval(\"DP_IncYear()\", 30)' onmouseup='clearInterval(_dpIntervalID)'>+</td></tr>" );
	html.push( "</table>" );

	_dpPickListDiv.innerHTML = html.join('');

	var box = DP_GetBoundingBox(elem);
	_dpPickListDiv.style.left = box.left;
	_dpPickListDiv.style.top = box.bottom;
	_dpPickListDiv.style.visibility = "visible";

	DP_HideOverlappingCombos(_dpPickListDiv);
	//DP_HideElements('SELECT', _dpPickListDiv);
	//DP_HideElements('APPLET', _dpPickListDiv);
}

function DP_HidePickList()
{
	clearInterval(_dpIntervalID);
	clearTimeout(_dpTimeoutID);
	_dpPickListDiv.style.visibility = "hidden";
}


// ----------------------------------------------------------------------------
// Helper Methods
// ----------------------------------------------------------------------------

// hides every combo box (select tags) that overlaps the passed box (needed for IE only)
function DP_HideOverlappingCombos(elem)
{
	if( !document.all ) return;

	var box = DP_GetBoundingBox(elem);

	// examine every combo on the page, hiding ones that overlap
	var combos = document.getElementsByTagName("select");
	var count = combos.length;
	for( var i = 0; i < count; i++ )
	{
		var combo = combos[i];
		if( !combo ) continue;

		// skip this combo if it is already hidden
		if( combo.style.visibility == 'hidden' ) continue;

		// hind the combo if it overlaps the menu
		// note: adding a flag to the combo so we know it should be restored
		var comboBox = DP_GetBoundingBox(combo);
		if( DP_DoBoxesOverlap(comboBox, box) ) // overlap
		{
			combo.style.visibility = 'hidden';
			combo.hiddenByPicker = true;
		}
	}
}

// makes all hidden combos (select tags) visible again (needed for IE only)
function DP_ShowHiddenCombos()
{
	if( !document.all ) return;

	// show all the combo boxes that where hidden by a menu
	var combos = document.getElementsByTagName("select");
	for( var i = 0; i < combos.length; i++ )
	{
		var combo = combos[i];
		if( !combo ) continue;

		if( combo.hiddenByPicker )
		{
			combo.style.visibility = 'visible';
			combo.hiddenByPicker = null;
		}
	}
}

function DP_SwapImage(elemID, srcImg)
{
	var elem = document.getElementById(elemID);
	if( !elem ) return;
	elem.src = _dpResPath + srcImg;
}

function DP_GetDateOnly(date)
{
	return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

function DP_DateToString(date, format)
{
	var y = date.getFullYear()
	var m = date.getMonth() + 1;
	var d = date.getDate();

	var str = format;
	str = str.replace("mmm", _dpMonthNames[m - 1]);
	str = str.replace("mm", DP_PadZero(m));
	str = str.replace("m", m);
	str = str.replace("dd", DP_PadZero(d));
	str = str.replace("d", d);
	str = str.replace("yyyy", y)
	str = str.replace("yy", DP_PadZero(y % 100));

	return str;
}

function DP_StringToDate(str, format)
{
	// parse the format string
	var formatChar = "/";
	var tokens = format.split(formatChar);
	if( tokens.length < 3 )
	{
		formatChar = ".";
		tokens = format.split(formatChar);
		if( tokens.length < 3 )
		{
			formatChar = "-";
			tokens = format.split(formatChar);
			if( tokens.length < 3 )
			{
				formatChar = " ";
				tokens = format.split(formatChar);
				if( tokens.length < 3 )
				{
					// invalid date format
					return null;
				}
			}
		}
	}

	// process the passed date string
	//TODO: add support for 2-digit year?  Have to do it here and in the DP_DateToString() as well
	var values = str.split(formatChar);
	var y = NaN;
	var m = NaN;
	var d = NaN;
	for( i = 0; i < 3; i++ )
	{
		switch( tokens[i] )
		{
			case "d":
			case "dd":
			{
				d = parseInt(values[i], 10);
				break;
			}
			case "m":
			case "mm":
			{
				m = parseInt(values[i], 10) - 1;
				break;
			}
			case "yy":
			case "yyyy":
			{
				y = parseInt(values[i], 10);
				break;
			}
			case "mmmm":
			{
				for( j = 0; j < 12; j++ )
				{
					if( values[i] == _dpMonthNames[j] ) m = j;
				}
				break;
			}
		}
	}

	if( isNaN(y) || isNaN(m) || isNaN(d) )
	{
		return null;
	}

	// fix 2-digit years
	if( y < 100 )
	{
		y += (y < 30) ? 2000 : 1900;
	}

	return new Date(y, m, d);
}

function DP_PadZero(num)
{
	return (num < 10) ? '0' + num : num;
}

function DP_GetBoundingBox(elem)
{
	// determine the top and left values of the element
	var top = 0;
	var left = 0;
	var e = elem;
	while (e.offsetParent)
	{
		top += e.offsetTop;
		left += e.offsetLeft;
		e = e.offsetParent;
	}

	// determine bottom and right values of the element
	var bottom = top + elem.offsetHeight;
	var right = left + elem.offsetWidth;

	// return the box corners
	return {left:left, top:top, right:right, bottom:bottom};
}

// returns true if the two boxes specified overlap
function DP_DoBoxesOverlap(box1, box2) //TODO: put to use
{
	if ((box1.left <= box2.right) && (box1.right >= box2.left))
	{
		if ((box1.top <= box2.bottom) && (box1.bottom >= box2.top))
		{
			return true;
		}
	}
	return false;
}