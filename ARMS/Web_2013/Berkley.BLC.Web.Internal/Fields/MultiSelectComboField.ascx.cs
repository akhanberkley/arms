using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Wilson.ORMapper;
using Berkley.BLC.Entities;
using QCI.Web.Validation;
using QCI.Web;

namespace Berkley.BLC.Web.Internal.Fields
{
    public partial class MultiSelectComboField : MappedFieldControl
    {
        protected bool noEndRow;
        protected bool ignoreMapping;

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Control_Load);
            this.PreRender += new EventHandler(this.Control_PreRender);
            this.cboMulti.SelectedIndexChanged += new EventHandler(cboMulti_SelectedIndexChanged);
        }
        #endregion

        private EventHandler _selectedIndexChanged;
        private const string FREETEXT_ONCHANGE_FUNCTION_NAME = "FreeTextCombo_OnChange";

        private void Control_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack && this.FreeTextEnabled)
            {
                valTxt.Visible = (cboMulti.SelectedValue == this.FreeTextItemValue);
            }
        }

        private void Control_PreRender(object sender, EventArgs e)
        {
            // one final refresh to ensure list is populated and setting changes are reflected
            RefreshList();

            if (lbl.Text.Length == 0)
            {
                lbl.Text = this.GetDefaultLabelText();
            }

            AddMultiSelectJQueryToPage();

            // deal with the free-text field visibility and client script
            if (this.FreeTextEnabled)
            {
                txt.Visible = true;
                string display = (this.SelectedValue == this.FreeTextItemValue) ? "" : "none";
                txt.Style.Add("display", display);

                AddFreeTextScriptsToPage();

                string script = string.Format("{0}('{1}', '{2}', '{3}')",
                    FREETEXT_ONCHANGE_FUNCTION_NAME, cboMulti.ClientID, txt.ClientID,
                    JavaScript.EncodeStringForJavaScript(this.FreeTextItemValue));

                cboMulti.Attributes.Add("onchange", script);

                ScriptHelper.RegisterStartupScript(this.Page, this.ClientID, script, false);

                if (this.IsRequired)
                {
                    valTxt.Visible = true;
                    valTxt.Text = "*";
                    valTxt.ErrorMessage = string.Format("An entry for {0} is required.", base.Name);
                }
            }
            else
            {
                txt.Visible = false;
                valTxt.Visible = false;
            }

            // deal with validator configuration
            if (this.IsRequired)
            {
                val.Visible = true;
                val.Text = "*";
                val.ErrorMessage = string.Format("{0} is required.", base.Name);
            }
            else
            {
                val.Visible = false;
            }
        }


        /// <summary>
        /// Gets the internal control responsible for capturing user input.
        /// </summary>
        public override Control InputControl
        {
            get { return cboMulti; }
        }


        /// <summary>
        /// Gets or sets a value indicating whether a postback to the server automatically occurs when the user changes the list selection.
        /// </summary>
        public bool AutoPostback
        {
            get { return cboMulti.AutoPostBack; }
            set { cboMulti.AutoPostBack = value; }
        }

        /// <summary>
        /// Gets or sets the data source that populates the items of the list control.
        /// </summary>
        public object DataSource
        {
            get { return cboMulti.DataSource; }
            set { cboMulti.DataSource = value; }
        }

        /// <summary>
        /// Gets or sets the field of the data source that provides the text content of the list items.
        /// </summary>
        public string DataTextField
        {
            get { return cboMulti.DataTextField; }
            set { cboMulti.DataTextField = value; }
        }

        /// <summary>
        /// Gets or sets the field of the data source that provides the value of each list item.
        /// </summary>
        public string DataValueField
        {
            get { return cboMulti.DataValueField; }
            set { cboMulti.DataValueField = value; }
        }

        /// <summary>
        /// Gets or sets the label value for this control.  This value will appear to the left of the input field.
        /// </summary>
        public string Label
        {
            get { return this.lbl.Text; }
            set { this.lbl.Text = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the control is enabled.
        /// </summary>
        public bool Enabled
        {
            get { return cboMulti.Enabled; }
            set
            {
                cboMulti.Enabled = value;
                txt.Enabled = value;
            }
        }

        /// <summary>
        /// Gets the collection of items in the list control.
        /// </summary>
        public ListItemCollection Items
        {
            get
            {
                RefreshList();
                return cboMulti.Items;
            }
        }

        /// <summary>
        /// Gets or sets the index of the selected item in the DropDownList control.
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                RefreshList();
                return cboMulti.SelectedIndex;
            }
            set
            {
                RefreshList();
                cboMulti.SelectedIndex = value;
            }
        }

        /// <summary>
        /// Gets the selected item with the lowest index in the list control.
        /// </summary>
        public ListItem SelectedItem
        {
            get
            {
                RefreshList();
                return cboMulti.SelectedItem;
            }
        }

        /// <summary>
        /// Gets the value of the selected item in the list control, or selects the item in the list control that contains the specified value.
        /// </summary>
        public string SelectedValue
        {
            get
            {
                RefreshList();
                return cboMulti.SelectedValue;
            }
            set
            {
                RefreshList();
                cboMulti.SelectedValue = value;
            }
        }

        /// <summary>
        /// Gets or sets the inital value of the control for use during validation.
        /// When IsRequired is true, this value determines which item is not not a valid selection.
        /// </summary>
        public string InitalValue
        {
            get { return val.InitialValue; }
            set { val.InitialValue = value; }
        }

        /// <summary>
        /// Gets or sets the entity type of items in the list.  This type is used by OPath to query items from the database.
        /// </summary>
        public string DataType
        {
            get { return (string)this.ViewState["DataType"]; }
            set { this.ViewState["DataType"] = value; }
        }

        /// <summary>
        /// Gets or sets the OPath query expression used to filter the items in the list.
        /// </summary>
        public string DataFilter
        {
            get { return (string)this.ViewState["DataFilter"]; }
            set { this.ViewState["DataFilter"] = value; }
        }

        /// <summary>
        /// Gets or sets the OPath sort expression used to order the items in the list.
        /// </summary>
        public string DataOrder
        {
            get { return (string)this.ViewState["DataOrder"]; }
            set { this.ViewState["DataOrder"] = value; }
        }

        /// <summary>
        /// Gets or sets the text value for the default item at the top of the list (e.g., "-- select type --".
        /// This item is not consider a non-selection and the user must select another item when IsRequired is set to true.
        /// </summary>
        public string TopItemText
        {
            get { return (string)this.ViewState["TopItemText"]; }
            set { this.ViewState["TopItemText"] = value; }
        }


        /// <summary>
        /// Gets a value indicating if free-text entry is enabled for this control.
        /// </summary>
        public bool FreeTextEnabled
        {
            get { return (this.FreeTextItemValue != null); }
        }

        /// <summary>
        /// Gets or sets the item value that will cause the free-text entry field to appear.
        /// Use 'null' to indicate no free-text entry is allowed regardless of the selected item.
        /// </summary>
        public string FreeTextItemValue
        {
            get { return (string)this.ViewState["FreeTextItemValue"]; }
            set { this.ViewState["FreeTextItemValue"] = value; }
        }

        /// <summary>
        /// Gets or sets the display width of the free-text entry field when visible.
        /// </summary>
        public int FreeTextColumns
        {
            get { return (int)txt.Columns; }
            set { txt.Columns = value; }
        }

        /// <summary>
        /// Gets or sets the maximum length of the free-text entry field when visible.
        /// </summary>
        public int FreeTextMaxLength
        {
            get { return (int)txt.MaxLength; }
            set { txt.MaxLength = value; }
        }

        /// <summary>
        /// Gets or sets a flag indicating to suspend injecting a end row tag.
        /// </summary>
        public bool NoEndTR
        {
            get { return noEndRow; }
            set { noEndRow = (bool)value; }
        }

        /// <summary>
        /// Gets or sets a flag indicating to ignore the mapping field.
        /// </summary>
        public bool IgnoreMapping
        {
            get { return ignoreMapping; }
            set { ignoreMapping = (bool)value; }
        }

        /// <summary>
        /// Occurs when the selection from the list control changes between posts to the server.
        /// </summary>
        public event EventHandler SelectedIndexChanged
        {
            add { _selectedIndexChanged += value; }
            remove { _selectedIndexChanged -= value; }
        }


        /// <summary>
        /// Binds the data source to this control.
        /// </summary>
        public override void DataBind()
        {
            base.DataBind();
            RefreshList();
        }

        /// <summary>
        /// Populates the control with items from the specified data source.
        /// </summary>
        /// <param name="dataSource">Data source that provides data for populating the control.</param>
        /// <param name="valueExpression">Expression that provides the value of each list item.</param>
        /// <param name="textExpression">Expression that provide the text caption value for the list item.</param>
        public void DataBind(object dataSource, string valueExpression, string textExpression)
        {
            ComboHelper.BindCombo(cboMulti, dataSource, valueExpression, textExpression);
            RefreshList();
        }

        /// <summary>
        /// Populates the control with items from the specified data source, formatted using the specifed text format and text fields.
        /// </summary>
        /// <param name="dataSource">Data source that provides data for populating the control.</param>
        /// <param name="valueExpression">Expression that provides the value of each list item.</param>
        /// <param name="textFormat">Formatting string used to control how items are displayed.</param>
        /// <param name="textExpressions">Expressions that provide text caption values for the list item.</param>
        public void DataBind(object dataSource, string valueExpression, string textFormat, params string[] textExpressions)
        {
            ComboHelper.BindCombo(cboMulti, dataSource, valueExpression, textFormat, textExpressions);
            RefreshList();
        }


        /// <summary>
        /// Selects the item in the list having the specified value.  If no item is found, the current selection is not modified.
        /// </summary>
        /// <param name="itemValue">Value of the item to select.</param>
        /// <returns>True if the item was found and selected; otherwise, false.</returns>
        public bool SelectItem(string itemValue)
        {
            RefreshList();
            return ComboHelper.SelectComboItem(cboMulti, itemValue);
        }


        /// <summary>
        /// Gets the user input value from the control.
        /// </summary>
        /// <returns>Value from the control.</returns>
        public override object GetValue()
        {
            string itemValue = this.SelectedValue;

            // the item value is the free-text entry if feature is enabled and free-text item is selected
            if (this.FreeTextItemValue != null && itemValue == this.FreeTextItemValue)
            {
                itemValue = txt.Text.Trim();
            }

            return itemValue;
        }

        /// <summary>
        /// Sets the user input value of the control.
        /// </summary>
        /// <param name="value">Value to be set.</param>
        public override void SetValue(object value)
        {
            RefreshList();

            // get string the value if the list item that should be selected
            string itemValue;
            if (value is Guid)
            {
                itemValue = ((Guid)value != Guid.Empty) ? value.ToString() : string.Empty;
            }
            else
            {
                itemValue = (value != null) ? value.ToString() : string.Empty;
            }

            // try to select the specified item
            bool itemSelected = this.SelectItem(itemValue);
            if (!itemSelected)
            {
                // the item value is the free-text entry if feature is enabled
                if (itemValue != null && this.FreeTextItemValue != null)
                {
                    // select the free-text item and put the passed value in the free text field
                    itemSelected = this.SelectItem(this.FreeTextItemValue);
                    if (!itemSelected)
                    {
                        throw new Exception("Control '" + this.ID + "' does not contain an item with a value matching the FreeTextItemValue property of '" + this.FreeTextItemValue + "'.");
                    }
                    txt.Text = itemValue;
                }
                else if (IgnoreMapping)
                {
                    txt.Text = string.Empty;
                }
                else // free-text not enabled
                {
                    throw new Exception("Control '" + this.ID + "' does not contain an item with a value of '" + itemValue + "'.");
                }
            }
        }



        private void cboMulti_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_selectedIndexChanged != null)
            {
                _selectedIndexChanged(sender, e);
            }
        }


        private void RefreshList()
        {
            // populate the list if auto binding into provided and list is still empty
            string typeName = this.DataType;
            if (cboMulti.Items.Count == 0 && typeName != null && typeName.Length > 0)
            {
                Type entityType = UIMapper.Instance.GetEntityType(typeName);
                if (entityType == null)
                {
                    throw new Exception("Entity type '" + typeName + "' could not be located.");
                }

                string filter = this.DataFilter;
                string order = this.DataOrder;

                OPathQuery query = new OPathQuery(entityType, filter, order);
                ObjectSet list = DB.Engine.GetObjectSet(query);

                cboMulti.DataSource = list;
                cboMulti.DataBind();
            }

            // add/update the top item if one is specified
            string topText = this.TopItemText;
            if (topText != null)
            {
                ListItem topItem = cboMulti.Items.FindByValue("");
                if (topItem != null) // already there
                {
                    topItem.Text = topText;
                }
                else // not in list
                {
                    cboMulti.Items.Insert(0, new ListItem(topText, ""));
                }

                if (this.IsRequired)
                {
                    this.InitalValue = "";
                }
            }
        }

        private void AddFreeTextScriptsToPage()
        {
            ARMSUtility.AddJavaScriptToPage(this.Page, ARMSUtility.AppPath + "/common.js");

            if (!ScriptHelper.IsClientScriptRegistered(this.Page, FREETEXT_ONCHANGE_FUNCTION_NAME))
            {
                StringBuilder script = new StringBuilder();
                script.AppendFormat("function {0}(cboID, txtID, showValue)\r\n", FREETEXT_ONCHANGE_FUNCTION_NAME);
                script.Append("{\r\n");
                script.Append("  var cboMulti = document.getElementById(cboID);\r\n");
                script.Append("  if( !cboMulti ) return;\r\n");
                script.Append("  ChangeControlDisplay(txtID, (cboMulti.value == showValue));\r\n");
                script.Append("}\r\n");

                ScriptHelper.RegisterClientScriptBlock(this.Page, FREETEXT_ONCHANGE_FUNCTION_NAME, script.ToString(), false);
            }
        }

        private void AddMultiSelectJQueryToPage()
        {
            ARMSUtility.AddJavaScriptToPage(this.Page, ARMSUtility.TemplatePath + "/javascripts/jquery.bgiframe.min.js");
            ARMSUtility.AddJavaScriptToPage(this.Page, ARMSUtility.TemplatePath + "/javascripts/jquery.multiSelect.js");

            StringBuilder jscript = new StringBuilder();
            jscript.Append("<script type=\"text/javascript\">\r\n");
            jscript.Append("$(document).ready( function() {\r\n");
            jscript.AppendFormat("$(\"#{0}\").multiSelect({{ noneSelected: '', oneOrMoreSelected: '% Items Selected' }});\r\n", cboMulti.ClientID);
            jscript.Append("});\r\n");
            jscript.Append("</script>");


            ScriptHelper.RegisterClientScriptBlock(this.Page, Guid.NewGuid().ToString(), jscript.ToString());
        }
    }
}