using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Berkley.BLC.Entities;

namespace Berkley.BLC.Web.Internal.Fields
{
    public partial class LabelField : MappedFieldControl
    {
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Control_Load);

        }
        #endregion

        private void Control_Load(object sender, System.EventArgs e)
        {
            if (lbl.Text.Length == 0)
            {
                lbl.Text = this.GetDefaultLabelText();
            }
        }


        /// <summary>
        /// Gets the internal control responsible for capturing user input.
        /// </summary>
        public override Control InputControl
        {
            get { return this.lblValue; }
        }

        /// <summary>
        /// Gets or sets the text content of the control.
        /// </summary>
        public string Text
        {
            get { return this.lblValue.Text; }
            set
            {
                lblValue.Text = (value != null) ? value.Trim() : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the value to display.
        /// </summary>
        public object Value
        {
            get
            {
                return (this.IsValueSet) ? this.ViewState["Value"] : string.Empty;
            }
            set
            {
                this.ViewState["Value"] = value;
                this.IsValueSet = true;

                // determine the text to display based on this value
                string text;
                if (value == null)
                {
                    text = this.NullValueText;
                }
                else if (value is bool)
                {
                    text = ((bool)value) ? "Yes" : "No";
                }
                else if (this.FieldInfo != null && value.Equals(this.FieldInfo.NullValue))
                {
                    text = this.NullValueText;
                }
                else // not null or bool
                {
                    string format = this.ValueFormat;
                    if (format != null && format.Length > 0)
                    {
                        text = string.Format(format, value);
                    }
                    else // no format
                    {
                        if (value is DateTime)
                        {
                            text = ((DateTime)value).ToShortDateString();
                        }
                        else
                        {
                            text = value.ToString();
                        }
                    }
                }

                this.Text = text;
            }
        }

        /// <summary>
        /// Gets or sets the label value for this control.  This value will appear to the left of the input field.
        /// </summary>
        public string Label
        {
            get { return this.lbl.Text; }
            set { this.lbl.Text = value; }
        }

        /// <summary>
        /// Gets or sets a flag indicating if the value to display as been set.
        /// </summary>
        private bool IsValueSet
        {
            get
            {
                object value = this.ViewState["IsValueSet"];
                return (value != null) ? (bool)value : false;
            }
            set { this.ViewState["IsValueSet"] = value; }
        }


        /// <summary>
        /// Gets or sets the horizontal alignment of the text value.
        /// </summary>
        public string Align
        {
            get
            {
                throw new NotImplementedException("This feature is not currently implemented.");
                //object value = this.ViewState["Align"];
                //return (value != null) ? (string)value : "left";
            }
            set
            {
                throw new NotImplementedException("This feature is not currently implemented.");
                //this.ViewState["Align"] = (value != null) ? value.Trim() : null;
            }
        }

        /// <summary>
        /// Gets or sets the width of the text value in pixels.
        /// </summary>
        public int Width
        {
            get { return (int)lblValue.Width.Value; }
            set { lblValue.Width = value; }
        }


        /// <summary>
        /// Gets or sets the string format to apply to the value.
        /// </summary>
        public string ValueFormat
        {
            get { return (string)this.ViewState["ValueFormat"]; }
            set { this.ViewState["ValueFormat"] = value; }
        }

        /// <summary>
        /// Gets or sets the text that is displayed when the value is null.
        /// </summary>
        public string NullValueText
        {
            get
            {
                string value = (string)this.ViewState["NullValueText"];
                return (value != null) ? value : "(not specified)";
            }
            set { this.ViewState["NullValueText"] = value; }
        }

        /// <summary>
        /// Gets the user input value from the control.
        /// </summary>
        /// <returns>Value from the control.</returns>
        public override object GetValue()
        {
            return this.Value;
        }

        /// <summary>
        /// Sets the user input value of the control.
        /// </summary>
        /// <param name="value">Value to be set.</param>
        public override void SetValue(object value)
        {
            this.Value = value;
        }
    }
}
