<%@ Control Language="C#" AutoEventWireup="false" CodeFile="NumberField.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Fields.NumberField" %>
<% if (base.TableRow) {%><tr><% } %>
<td class="label" nowrap>
	<asp:Label ID="lbl" Runat="server" /><%= (this.IsRequired) ? " *" : "" %>
</td>
<td>
	<asp:TextBox ID="txt" Runat="server" CssClass="txt" Columns="10" MaxLength="10" />
	<asp:RequiredFieldValidator ID="val" Runat="server" CssClass="val" ControlToValidate="txt" Display="Dynamic" />
	<asp:RegularExpressionValidator ID="valRegex" Runat="server" CssClass="val" ControlToValidate="txt" Display="Dynamic" />
	<asp:CompareValidator ID="valType" Runat="server" CssClass="val" ControlToValidate="txt" Display="Dynamic" Operator="DataTypeCheck" />
	<% if (Directions.Length > 0) {%>
    &nbsp;<asp:Label ID="lblDirections" Runat="server" />
    <% } %>
</td>
<% if (base.TableRow) {%></tr><% } %>
