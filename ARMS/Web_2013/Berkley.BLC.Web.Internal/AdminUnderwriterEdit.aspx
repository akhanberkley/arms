<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminUnderwriterEdit.aspx.cs" Inherits="AdminUnderwriterEditAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <cc:Box id="boxLink" runat="server" title="Underwriter Information" width="600">
        <table class="fields">
        <uc:Text id="txtCode" runat="server" field="Underwriter.Code" Columns="10" MaxLength="3" Name="Initials" />
        <uc:Text id="txtName" runat="server" field="Underwriter.Name" Name="Name"/>
        <uc:Text id="txtDomainName" runat="server" field="Underwriter.DomainName" IsRequired="true" />
        <uc:Text id="txtUsername" runat="server" field="Underwriter.Username" IsRequired="true" />
        <uc:Text id="txtPhoneNumber" runat="server" field="Underwriter.PhoneNumber" Directions="(ex. 5551234567)"/>
        <uc:Text id="txtPhoneExt" runat="server" field="Underwriter.PhoneExt"/>
        <uc:Text id="txtEmailAddress" runat="server" field="Underwriter.EmailAddress" Columns="50"/>
        <uc:Text id="txtTitle" runat="server" field="Underwriter.Title"/>
        <uc:Combo id="cboProfitCenter" runat="server" field="Underwriter.ProfitCenterCode" Name="Profit Center" topItemText="" />
        <uc:Check id="chkUnderwriterDisabled" runat="server" field="Underwriter.Disabled" />
        </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Add/Update" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
