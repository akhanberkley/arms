<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminTerritoryEdit.aspx.cs" Inherits="AdminTerritoryEditAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="CheckBox" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span>
    <span style="MARGIN-LEFT: 20px">
	    <a class="nav" href="AdminTerritories.aspx?type=<%= _typeCode %>"><< Back to Territory List</a>
    </span><br>
    <br>

    <cc:Box id="boxTerritory" runat="server" title="Territory Information" width="500">
	    <table class="fields">
	    <uc:Text id="txtName" runat="server" field="Territory.Name" Label="Name" Columns="70" />
	    <uc:Text id="txtDescription" runat="server" field="Territory.Description" Label="Description" Columns="70" Rows="6" />
        <uc:CheckBox ID="chkInHouse" runat="server" Field="Territory.InHouseSelection" Label="Internal Staff Territory" />
	    <% if( _terra != null ) { %>
	    <tr>
		    <td></td>
		    <td class="buttons">
			    <a href="AdminTerritoryEditArea.aspx?territoryid=<%= _terra.ID %>">Edit Territory Area</a>
		    </td>
	    </tr>
	    <% } %>
	    </table>
    </cc:Box>

    <cc:Box id="boxUsers" runat="server" title="Assigned Users" width="500">
    <div style="MARGIN-LEFT: 20px" id="users">

    <asp:Repeater ID="repUsers" Runat="server">
    <HeaderTemplate>
	    <table>
    </HeaderTemplate>
    <ItemTemplate>
	    <tr>
		    <td><%# HtmlEncode(Container.DataItem, "Name") %></td>
		    <td width="20">&nbsp;</td>
		    <td>
			    <span id="divPercentage" name="a" runat="server"><asp:TextBox ID="txtPercentage" Runat="server" CssClass="txt" Columns="5" Text='<%# HtmlEncodeNullable(Container.DataItem, "PercentAllocationValue", Int32.MinValue, "") %>' /> %</span>
			    <span id="divPremium" name="b" runat="server">
				    <asp:TextBox ID="txtMALMin" Runat="server" CssClass="txt" Columns="10" Text='<%# HtmlEncodeNullable(Container.DataItem, "MinPremiumAmountValue", "{0:F0}", decimal.MinValue, "") %>' />
				    &nbsp;to&nbsp;
				    <asp:TextBox ID="txtMALMax" Runat="server" CssClass="txt" Columns="10" Text='<%# HtmlEncodeNullable(Container.DataItem, "MaxPremiumAmountValue", "{0:F0}", decimal.MinValue, "") %>' />
			    </span>
		    </td>
		    <td width="10">&nbsp;</td>
		    <td>
			    <asp:LinkButton ID="btnRemoveUser" Runat="server" 
				    CommandName="Remove" 
				    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID") %>'
				    OnPreRender="btnRemoveUser_PreRender"
				    CausesValidation="False">Remove</asp:LinkButton>
		    </td>
	    </tr>
    </ItemTemplate>
    <FooterTemplate>
	    </table>
    </FooterTemplate>
    </asp:Repeater>

    <hr size="1">

	    <div style="MARGIN-TOP:5px">
		    Add User <asp:DropDownList ID="cboUsers" Runat="server" CssClass="cbo" />
		    <asp:Button ID="btnAddUser" Runat="server" CssClass="btn" Text="Add" /><br><br>
		    <asp:RadioButton ID="rdoPercent" Runat="server" CssClass="rdo" GroupName="Allocation" Text="Percentage" Checked="True" OnClick="ToggleDisplay(this)" /><br>
		    <asp:RadioButton ID="rdoPremium" Runat="server" CssClass="rdo" GroupName="Allocation" Text="Premium Limit" OnClick="ToggleDisplay(this)" />
	    </div>
    </div>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Add/Update" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
        
    <uc:Footer ID="ucFooter" runat="server" />
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
    function ToggleDisplay(flag)
    {
	    var grp = new Array();
		    grp = document.getElementById('users').getElementsByTagName('span'); 
    	
	    if (flag.value == 'rdoPercent')
	    {
		    if (grp.length > 0 && grp[0].style.display == 'none')
		    {
			    for (i=0; i<grp.length-2; i++) { 
				    ToggleControlDisplay(grp[i].id);
			    }
		    }
	    }
	    else if (flag.value == 'rdoPremium')
	    {
		    if (grp.length > 0 && grp[1].style.display == 'none')
		    {
			    for (i=0; i<grp.length-2; i++) { 
				    ToggleControlDisplay(grp[i].id);
			    }
		    }
	    }
    }
    </script>
    </form>
</body>
</html>
