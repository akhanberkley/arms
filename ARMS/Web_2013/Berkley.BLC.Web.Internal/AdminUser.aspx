<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminUser.aspx.cs" Inherits="AdminUserAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span>
    <span style="MARGIN-LEFT: 20px">
	    <% if( !_isFeeCompany ) { %>
	    <a class="nav" href="AdminUsers.aspx" style="MARGIN-LEFT: 20px"><< Back to User List</a>
	    <% } else { %>
	    <a class="nav" href="AdminFeeCompanies.aspx"><< Back to Fee Company List</a>
	    <% } %>
    </span><br>
    <br>

    <cc:Box id="boxUser" runat="server" title="User Summary" width="500">
    <table class="fields">
    <uc:Label id="lblName" runat="server" field="User.Name" name="Name" Group="lbl" />
    <uc:Label id="lblTitle" runat="server" field="User.Title" Group="lbl" />
    <uc:Label id="lblCertifications" runat="server" field="User.Certifications" Group="lbl" />
    <uc:Label id="lblUsername" runat="server" field="User.Username" Group="lbl" />
    <uc:Label id="lblEmailAddress" runat="server" field="User.EmailAddress" Group="lbl" />
    <uc:Label id="lblWorkPhone" runat="server" field="User.WorkPhone" Group="lbl" />
    <uc:Label id="lblHomePhone" runat="server" field="User.HomePhone" Group="lbl" />
    <uc:Label id="lblMobilePhone" runat="server" field="User.MobilePhone" Group="lbl" />
    <uc:Label id="lblFaxNumber" runat="server" field="User.FaxNumber" Group="lbl" />
    <uc:Label id="lblManager" runat="server" field="User.City" Name="Manager" Group="lbl" />
    <uc:Label id="lblProfitCenter" runat="server" field="ProfitCenter.Name" Group="lbl" />
    <uc:Label id="lblAddress" runat="server" field="User.HtmlAddress" Name="Address" Group="lbl" />
    <uc:Label id="lblRole" runat="server" field="UserRole.Name" Name="Security Role" Group="lbl" />
    <uc:Label id="lblAccountDisabled" runat="server" field="User.AccountDisabled" Group="lbl" />
    <uc:Label id="lblConsultantCount" runat="server" field="" Name="Consultants" Group="lbl" />
    <tr>
	    <td>&nbsp;</td>
	    <td>
		    <br>
		    <a href="AdminUserEdit.aspx?id=<%= _user.ID %>">Edit Details</a>
		    <% if( _isFeeCompany ) { %>
		    <a href="AdminFeeUsers.aspx?id=<%= _user.ID %>" style="MARGIN-LEFT: 20px">Manage Fee Consultants</a>
		    <% } %>
	    </td>
    </tr>
    </table>
    </cc:Box>

    <a id="lnkCopier" href="javascript:Toggle('divCopier'); javascript:Toggle('lnkCopier')">Copy Saved Searches From Another User</a>

    <div id="divCopier" style="DISPLAY: none">
        <cc:Box id="boxUserSearches" runat="server" title="Copy Saved Searches" width="500">
        <span>Note: This feature will allow you to copy over an existing active user's saved searches over to this user's
        saved searches.</span>
        <table class="fields" width="100%">
        <tr><td colspan="2"></td></tr>
        <uc:Combo id="cboActiveUsers" runat="server" field="User.Name" Name="Existing User" topItemText="" IsRequired="true" Group="txt" />
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr>
	        <td style="width:1%" nowrap>&nbsp;</td>
            <td><asp:Button ID="btnCopy" Runat="server" CssClass="btn" Text="Copy" CausesValidation="true" />
                <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
            </td>
        </tr>
        </table>
        </cc:Box>
    </div>
    
    <uc:Footer ID="ucFooter" runat="server" />
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
        function Toggle(id) {
            ToggleControlDisplay(id);
        }
    </script>
    </form>
</body>
</html>
