﻿<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" CodeFile="CreateServicePlanPolicy.aspx.cs" Inherits="CreateServicePlanPolicyAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Date" Src="~/Fields/DateField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><% = base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="form1" runat="server">

    <uc:Header ID="ucHeader" runat="server" />
    <span class="heading"><%= base.PageHeading %></span><br /><br />
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />

    <cc:Box id="boxPolicy" runat="server" Title="Policy" width="725">
        <table class="fields">
            <%if (_action == Action.Add || _action == Action.Edit){%>
            <%----%>
                <uc:Text id="txtPolicyNumber" runat="server" field="Policy.Number" Columns="15" Group="txt" />
                <uc:Number id="numPolicyMod" runat="server" field="Policy.Mod" Group="txt" />
                <uc:Combo id="cboLOB" runat="server" field="Policy.LineOfBusinessCode" Name="Line Of Business" topItemText="" isRequired="True" Group="txt" />
                <uc:Text id="txtPolicySymbol" runat="server" field="Policy.Symbol" Columns="5" Group="txt" />
                <uc:Date id="dtEffectiveDate" runat="server" field="Policy.EffectiveDate" Name="Effective Date" IsRequired="True" Group="txt" OnChange="AutoFillExpiration(this.value);" />
                <uc:Date id="dtExpirationDate" runat="server" field="Policy.ExpireDate" Name="Expiration Date" IsRequired="True" Group="txt" />
                <uc:Text id="txtPremium" runat="server" field="Policy.Premium" Columns="15" IsRequired="True" Group="txt" />
                <uc:Text id="txtBranchCode" runat="server" field="Policy.BranchCode" Columns="30" Group="txt" />
                <uc:Combo id="cboProfitCenter" runat="server" field="Policy.ProfitCenter" Name="Profit Center" topItemText="" Group="txt" />                               
                <uc:Combo id="cboCompanyCarrier" runat="server" field="Policy.Carrier" Name="Carrier" topItemText="" isRequired="True" Group="txt" />
                <uc:Combo id="cboPolicyState" runat="server" field="Policy.StateCode" Name="Primary State" topItemText="" isRequired="False" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" Group="txt" />
            <%}else{%>
            <%----%>
                <uc:Label id="lblPolicyNumber" runat="server" field="Policy.Number" Group="lbl" />
                <uc:Label id="lblMod" runat="server" field="Policy.Mod" Group="lbl" />
                <uc:Label id="lblLineOfBusiness" runat="server" field="LineOfBusiness.Name" Group="lbl" />
                <uc:Label id="lblEffectiveDate" runat="server" field="Policy.EffectiveDate" Group="lbl" />
                <uc:Label id="lblExpirationDate" runat="server" field="Policy.ExpireDate" Name="ExpirationDate" Group="lbl" />
                <uc:Label id="lblPremium" runat="server" field="Policy.Premium" valueFormat="{0:c}" Group="lbl" />
                <uc:Label id="lblBranchCode" runat="server" field="Policy.BranchCode" Group="lbl" />
                <uc:Label id="lblProfitCenter" runat="server" field="ProfitCenter.Name" Group="lbl" />
                <uc:Label id="lblCarrier" runat="server" field="Policy.Carrier" Group="lbl" />
                <uc:Label id="lblPolicyState" runat="server" Name="Primary State" field="Policy.StateCode" Group="lbl" />
            <%} %>
            
            <%----%>
            <% if (_hideHazardGrade != "true"){ %>
            <uc:Text id="txtHazardGrade" runat="server" field="Policy.HazardGrade" Columns="10" IsRequired="false" Group="txt" />
            <%} %>
            
        </table>
    </cc:Box>

    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Add" />
    <asp:Button ID="btnAddAnother" Runat="server" CssClass="btn" Text="Add Another" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />

    <uc:HintFooter ID="ucHintFooter" runat="server" VerticalOffset="130" />
    <uc:Footer ID="ucFooter" runat="server" />

    </form>
</body>
</html>
