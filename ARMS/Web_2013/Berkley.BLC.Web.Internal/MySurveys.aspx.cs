using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using Entities = Berkley.BLC.Entities;
using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web;

public partial class MySurveysAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(MySurveysAspx_Load);
        this.PreRender += new EventHandler(MySurveysAspx_PreRender);
        this.grdSavedSearches.ItemCommand += new DataGridCommandEventHandler(grdSavedSearches_ItemCommand);
        this.grdSavedSearches.ItemDataBound += new DataGridItemEventHandler(grdSavedSearches_ItemDataBound);
    }
    #endregion

    protected Entities.User _eActiveUser;
    protected bool _showPastYearSurvey = false;
   

    void MySurveysAspx_Load(object sender, EventArgs e)
    {
        Guid userID = ARMSUtility.GetGuidFromQueryString("userid", false);
        _eActiveUser = (userID != Guid.Empty) ? Entities.User.Get(userID) : this.CurrentUser;
        

        string title = (_eActiveUser.ID != this.CurrentUser.ID) ? string.Format("{0}'s Saved Searches", _eActiveUser.Name) : "My Saved Searches";
        boxSearches.Title = title;

        boxSearches.Visible = !CurrentUser.IsUnderwriter;

        int count = ARMSUtility.GetIntFromQueryString("surveycount", false);
        if (count != int.MinValue)
        {
            JavaScript.ShowMessage(this, "Your survey request(s) has been sucessfully created.");
        }
    }

    void MySurveysAspx_PreRender(object sender, EventArgs e)
    {
        UserSearch[] searches = UserSearch.GetSortedArray("UserID = ?", "DisplayOrder ASC", _eActiveUser.ID);
        DataGridHelper.BindGrid(grdSavedSearches, searches, "ID", "There are no saved searches.");
        grdSavedSearches.Columns[(grdSavedSearches.Columns.Count - 1)].Visible = CurrentUser.Permissions.CanRequireSavedSearch;

        if (CurrentUser.Permissions.CanViewSurveyReviewQueues)
        {
            SurveyReviewQueue[] reviewQueues = SurveyReviewQueue.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            DataGridHelper.BindPagingGrid(grdReviewQueues, reviewQueues, null, "There are no survey review queues.");
        }
        
        if (PermissionSet.CanViewLetterQueue(_eActiveUser))
        {
            MergeDocument[] docs = MergeDocument.GetSortedArray("Disabled = false && Type = ? && !ISNULL(DaysUntilReminder) && CompanyID = ?", "PriorityIndex ASC", MergeDocumentType.Letter.ID, _eActiveUser.CompanyID);
            DataGridHelper.BindPagingGrid(grdLetters, docs, null, "There are no letter reminders.");
        }
        else
        {
            boxLetterReminders.Visible = false;
        }
    }

    protected string GetSurveyReviewsCount(Object dataItem)
    {
        SurveyReviewQueue reviewQueue = (SurveyReviewQueue)dataItem;
        object count = DB.Engine.ExecuteScalar(reviewQueue.CountQuery);

        return count.ToString();
    }

    protected MergeDocument priorDoc;
    protected string GetPriorLetter()
    {
        return (priorDoc != null) ? priorDoc.ID.ToString() : Guid.Empty.ToString();
    }

    protected string GetLetterCount(Object dataItem)
    {
        MergeDocument currentDoc = (MergeDocument)dataItem;
        string command = string.Format("EXEC Letter_GetReminders @CompanyID = '{0}', @UserID = '{1}', @PriorLetterID = '{2}', @CurrentLetterID = '{3}', @ReturnType = '0'", _eActiveUser.CompanyID, _eActiveUser.ID, (priorDoc != null) ? priorDoc.ID : Guid.Empty, currentDoc.ID);

        priorDoc = currentDoc;
        return DB.Engine.GetDataSet(command).Tables[0].Rows[0][0].ToString();
    }

    protected string GetCount(SurveyStatus status)
    {
        return Survey.GetArray("StatusCode = ? && CompanyID = ? && CreateStateCode != ? && (CreateByExternalUserName = ? || Insured.Underwriter = ?)",
            status.Code,
            CurrentUser.CompanyID,
            CreateState.InProgress.Code,
            UserIdentity.Name,
            UserIdentity.Initials).Length.ToString();
    }

    protected string GetClosedCount()
    {
        return Survey.GetArray("StatusCode = ? && CompanyID = ? && CreateStateCode != ? && (CreateByExternalUserName = ? || Insured.Underwriter = ?) && CompleteDate >= ?",
            SurveyStatus.ClosedSurvey.Code,
            CurrentUser.CompanyID,
            CreateState.InProgress.Code,
            UserIdentity.Name,
            UserIdentity.Initials,
            DateTime.Today.AddMonths(-12)).Length.ToString();
    }

    protected string GetCanceledCount()
    {
        return Survey.GetArray("StatusCode = ? && CompanyID = ? && CreateStateCode != ? && (CreateByExternalUserName = ? || Insured.Underwriter = ?) && CanceledDate >= ?",
            SurveyStatus.CanceledSurvey.Code,
            CurrentUser.CompanyID,
            CreateState.InProgress.Code,
            UserIdentity.Name,
            UserIdentity.Initials,
            DateTime.Today.AddMonths(-12)).Length.ToString();
    }

    protected string GetUnacknowledgedCount()
    {
        return Survey.GetArray("(StatusCode = ? || StatusCode = ? || StatusCode = ?) && CompanyID = ? && CreateStateCode != ? && (CreateByExternalUserName = ? || Insured.Underwriter = ?) && ISNULL(UnderwritingAcceptDate) && ReportCompleteDate >= ?",
            SurveyStatus.AssignedReview.Code,
            SurveyStatus.UnassignedReview.Code,
            SurveyStatus.ClosedSurvey.Code,
            CurrentUser.CompanyID,
            CreateState.InProgress.Code,
            UserIdentity.Name,
            UserIdentity.Initials,
            DateTime.Today.AddMonths(-12)).Length.ToString();
    }

    protected string GetUnassignedCount()
    {
        return Survey.GetArray("(StatusCode = ? || StatusCode = ? || StatusCode = ?) && CompanyID = ? && CreateStateCode != ? && (CreateByExternalUserName = ? || Insured.Underwriter = ?) ",
            SurveyStatus.UnassignedSurvey.Code,
            SurveyStatus.OnHoldSurvey.Code,
            SurveyStatus.AwaitingAcceptance.Code,
            CurrentUser.CompanyID,
            CreateState.InProgress.Code,
            UserIdentity.Name,
            UserIdentity.Initials).Length.ToString();
    }

    protected string GetCountAll()
    {
        DateTime surveyCreateDate = DateTime.Today;
        _showPastYearSurvey = (Berkley.BLC.Business.Utility.GetCompanyParameter("ShowPastYearSurvey", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;

        if (_showPastYearSurvey && this.CurrentUser.IsUnderwriter )
        {
            surveyCreateDate = surveyCreateDate.AddYears(-1);
        }        

        return Survey.GetArray("CompanyID = ? && CreateStateCode != ? && (CreateByExternalUserName = ? || Insured.Underwriter = ?) && (CreateDate > ? ) ",
            CurrentUser.CompanyID,
            CreateState.InProgress.Code,
            UserIdentity.Name,
            UserIdentity.Initials,
            surveyCreateDate).Length.ToString();
    }

    protected string GetOverdue()
    {
        int count = SpecialQueries.GetCount("ISNULL(ReportCompleteDate) && SurveyStatusCode != ? && SurveyStatusCode != ? && (CreateByExternalUserName = ? || UnderwriterCode = ?) && CompanyID = ? && CreateStateCode != ? && DueDate < TodaysDate",
            string.Format("{0}~{1}~{2}~{3}~{4}~{5}", SurveyStatus.ClosedSurvey.Code, SurveyStatus.CanceledSurvey.Code, UserIdentity.Name, UserIdentity.Initials, CurrentUser.CompanyID, CreateState.InProgress.Code), 
            typeof(VWSurvey), 
            "dbo.VW_Survey");
        return Server.HtmlEncode(count.ToString());
    }

    protected string GetResultCountHtml(object dataItem)
    {
        Entities.UserSearch search = (Entities.UserSearch)dataItem;

        int count = SpecialQueries.GetCount(search.FilterExpression, search.FilterParameters, typeof(VWSurvey), "dbo.VW_Survey");
        return Server.HtmlEncode(count.ToString());
    }

    protected string GetRequiredText(Object dataItem)
    {
        UserSearch search = (UserSearch)dataItem;
        return (search.IsRequired) ? "Is Required" : "Not Required";
    }

    protected bool SupportsUWAcknowledgement()
    {
        SurveyAction action = SurveyAction.GetOne("CompanyID = ? && SurveyStatusCode = ? && TypeCode = ?", CurrentUser.CompanyID, SurveyStatus.ClosedSurvey.Code, SurveyActionType.AcknowledgeByUW.Code);
        return (action != null) ? true : false;
    }

    void grdSavedSearches_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            UserSearch search = (UserSearch)e.Item.DataItem;
            LinkButton lnk = (LinkButton)e.Item.FindControl("btnDelete");

            lnk.Enabled = !search.IsRequired;
            if (lnk.Enabled)
            {
                JavaScript.AddConfirmationNoticeToWebControl(lnk, "Are you sure you want to delete this search?");
            }
        }
    }

    void grdSavedSearches_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid searchID = (Guid)grdSavedSearches.DataKeys[e.Item.ItemIndex];

        // get the current searches from the DB and find the index of the target search
        Entities.UserSearch[] searches = Entities.UserSearch.GetSortedArray("UserID = ?", "DisplayOrder ASC", _eActiveUser.ID);
        int searchIndex = int.MinValue;
        for (int i = 0; i < searches.Length; i++)
        {
            if (searches[i].ID == searchID)
            {
                searchIndex = i;
                break;
            }
        }
        if (searchIndex == int.MinValue) // not found
        {
            return;
        }

        Entities.UserSearch search = searches[searchIndex];

        // execute the requested transformation on the searches
        using (Transaction trans = Entities.DB.Engine.BeginTransaction())
        {
            ArrayList searchList = new ArrayList(searches);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEUP":
                    {
                        searchList.RemoveAt(searchIndex);
                        if (searchIndex > 0)
                        {
                            searchList.Insert(searchIndex - 1, search);
                        }
                        else // top item (move to bottom)
                        {
                            searchList.Add(search);
                        }
                        break;
                    }
                case "MOVEDOWN":
                    {
                        searchList.RemoveAt(searchIndex);
                        if (searchIndex < searches.Length - 1)
                        {
                            searchList.Insert(searchIndex + 1, search);
                        }
                        else // bottom item (move to top)
                        {
                            searchList.Insert(0, search);
                        }
                        break;
                    }
                case "DELETE":
                    {
                        searchList.RemoveAt(searchIndex);
                        searches[searchIndex].Delete(trans);
                        break;
                    }
                case "REQUIRED":
                    {
                        UserSearch requireSearch = searches[searchIndex].GetWritableInstance();
                        requireSearch.IsRequired = !requireSearch.IsRequired;
                        requireSearch.Save(trans);
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the searches still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < searchList.Count; i++)
            {
                Entities.UserSearch searchUpdate = (searchList[i] as Entities.UserSearch).GetWritableInstance();
                searchUpdate.DisplayOrder = (i + 1);
                searchUpdate.Save(trans);
            }

            trans.Commit();
        }
    }
}
