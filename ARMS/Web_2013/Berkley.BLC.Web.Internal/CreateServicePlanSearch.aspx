<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" CodeFile="CreateServicePlanSearch.aspx.cs" Inherits="CreateServicePlanSearchAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
    <script language="javascript">var counter = 0;</script>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager >
        
        <uc:Header ID="ucHeader" runat="server" />
    

    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    
    <cc:Box id="boxSearch" runat="server" Title="Search Criteria" width="500">
        <table class="fields">
            <uc:Combo id="cboServicePlanType" runat="server" field="" Name="Service Plan Type" topItemText="" isRequired="true" />
            <uc:Combo id="cboPolicySystem" runat="server" field="" Name="Policy System" topItemText="" isRequired="true" />
            <uc:Number id="numSurveyNumber" runat="server" field="Survey.Number" Name="Survey Number" IsRequired="false" Columns="15" />
            <tr id="trPolicyNumber" runat="server">
                <td class="label" nowrap="nowrap">
                    <span>Policy Number</span>
                </td>
                <td>
                    <asp:TextBox id="numPolicyNumber" runat="server" Columns="15" MaxLength="7" CssClass="txt"></asp:TextBox>
                    <asp:CompareValidator ID="valPolicyNumber" Runat="server" CssClass="val" ControlToValidate="numPolicyNumber" Display="Dynamic" Operator="DataTypeCheck" Type="Integer" Text="*" ErrorMessage="Policy Number is not a valid value." />
                </td>
            </tr>
            <% if (CurrentUser.Company.CreateServicePlansFromProspects) { %>
            <uc:Number id="numSubmissionNumber" runat="server" field="LineOfBusiness.DisplayOrder" Name="Submission Number" IsRequired="false" Columns="15" />
            <% } %>
            <tr id="trInsured" valign="top" runat="server" visible = "false">
                 <td class="label" nowrap="nowrap">
                    <span>Insured *</span>
                </td>
                <td><asp:TextBox ID="txtInsured" runat="server" Width="300" CssClass="txt" ></asp:TextBox> 

                    <asp:Panel runat="server" ID="pnlInsured" 
                         ScrollBars="Auto" style="overflow:auto;width:300px;text-overflow:ellipsis;display:none;height:inherit">
                    </asp:Panel>

                    <ajax:AutoCompleteExtender 
                                ID="aceInsured" 
                                TargetControlID="txtInsured" MinimumPrefixLength="1" 
                                EnableCaching="true" 
                                CompletionSetCount="1" CompletionInterval="1000" 
                                CompletionListElementID="pnlInsured"
                                OnClientItemSelected="aceInsured_itemSelected"
                                ServiceMethod="GetInsured"
                                runat="server">
                    </ajax:AutoCompleteExtender>

                    <asp:HiddenField ID="hdPolicyNumber" runat="server"/>
                    <asp:HiddenField ID="hdName" runat="server"/>
                </td>

           </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="buttons">
                    <asp:Button id="btnSearch" Runat="server" CssClass="btn" Text="Search" />
                </td>
            </tr>
        </table>
    </cc:Box>
    
    <cc:Box id="boxInsured" runat="server" Title="Results" width="500">
        <table class="fields">
            <uc:Label id="lblClientID" runat="server" field="Insured.ClientID" />
            <% if (CurrentUser.Company.SupportsPortfolioID) {%>
            <uc:Label id="lblPortfolioID" runat="server" field="Insured.PortfolioID" />
            <% } %>
            <uc:Label id="lblName" runat="server" field="Insured.Name" />
            <uc:Label id="lblName2" runat="server" field="Insured.Name2" />
            <uc:Label id="lblAddress" runat="server" field="Insured.HtmlAddress" Name="Address" />
            <uc:Label id="lblBusinessOperations" runat="server" field="Insured.BusinessOperations" />
            <% if (CurrentUser.Company.SupportsBusinessCategory) {%>
            <uc:Label id="lblBusinessCategory" runat="server" field="Insured.BusinessCategory" />
            <% } %>
            <% if (CurrentUser.Company.SupportsDOTNumber) {%>
            <uc:Label id="lblDOTNumber" runat="server" field="Insured.DOTNumber" Name="DOT Number" />
            <% } %>
            <uc:Label id="lblSIC" runat="server" field="Insured.HtmlSIC" Name="SIC" />
            <% if (CurrentUser.Company.SupportsNAICS) {%>
            <uc:Label id="lblNAICS" runat="server" field="Insured.NAICSCode" Name="NAICS" />
            <% } %>
            <uc:Label id="lblUnderwriter" runat="server" field="Insured.HtmlUnderwriterName" Name="Underwriter" />
            <uc:Label id="lblUnderwriterPhone" runat="server" field="Insured.HtmlUnderwriterPhone" Name="Underwriter Phone" />
            <tr>
                <td>&nbsp;</td>
                <td class="buttons">
                    <asp:Button id="btnCreateServicePlan" Runat="server" CssClass="btn" Text="Create Service Plan" />
                </td>
            </tr>
        </table>
    </cc:Box>
     
    <uc:HintFooter ID="ucHintFooter" runat="server" /> 
    <uc:Footer ID="ucFooter" runat="server" />
    
    <div style="display: none;"><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" alt="" /></div>
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.blockUI.defaults.css = {};
            $.blockUI.defaults.overlayCSS.opacity = .2;

            $('input[type=submit]', this).click(function () {
                if (Page_IsValid) {
                    $.blockUI({ message: '<h3><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" />&nbsp;&nbsp;&nbsp;Processing, one moment please...</h3>' });
                }
            });

            //focus on first form field
            $(function () {
                $(window).load(function () {
                    $("#numPolicyNumber_txt").focus();
                    $("#numSubmissionNumber_txt").focus();
                });
            })
        });

        function ToggleBox(id) {
            ToggleControlDisplay(id);
        }

        function aceInsured_itemSelected(sender, e) {
            var hdPolicyNumber = $get('<%= hdPolicyNumber.ClientID %>');
            hdPolicyNumber.value = e.get_value();

            var hdName = $get('<%= hdName.ClientID %>');
            hdName.value = e.get_text();
        }
    </script>

    </form>
</body>
</html>