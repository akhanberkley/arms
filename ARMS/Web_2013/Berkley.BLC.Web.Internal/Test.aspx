<%@ Page Language="C#" AutoEventWireup="false" CodeFile="Test.aspx.cs" Inherits="TestAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px; background-color: #F5E8C5;">
<script type="text/javascript">
/**
 *
 * @access public
 * @return void
 **/
function closeParentWin(flag){
    //var cool = flag;
	
	top.$('#modalWindow').jqmHide();
	return false;
}
	</script>

    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">

    <div style="border: 1px solid black; width: 600px; padding: 10px;">
    <cc:Box id="boxBuildings" runat="server" Title="Building" width="600">
        <table class="fields">
            <uc:Number id="numBuildingNumber" DataType="Integer" runat="server" field="Building.Number" MaxLength="4" IsRequired="true" Columns="10" />
            <uc:Number id="txtYearBuilt" DataType="Integer" runat="server" MaxLength="4" field="Building.YearBuilt" Columns="10" />
            <uc:Combo id="cboHasSprinklerSystem" runat="server" field="Building.HasSprinklerSystem" Name="Sprinkler System" isRequired="false" />
            <uc:Number id="txtPublicProtection" DataType="Integer" runat="server" field="Building.PublicProtection" Columns="10" />
            <uc:Number id="txtBuildingValue" DataType="Currency" runat="server" field="Building.Value" Columns="30" />
            <uc:Number id="txtContents" DataType="Currency" runat="server" field="Building.Contents" Columns="30" />
            <uc:Number id="txtStockValues" DataType="Currency" runat="server" field="Building.StockValues" Columns="30" />
            <uc:Number id="txtBusinessInterruption" runat="server" field="Building.BusinessInterruption" Columns="30" />
            <uc:Text id="txtLocationOccupancy" runat="server" field="Building.LocationOccupancy" Columns="80" Rows="3" />
            

            <uc:Combo id="cboValuationType" Visible="false" runat="server" field="Building.ValuationTypeCode" Name="Valuation Type" topItemText="" isRequired="false"
		        DataType="BuildingValuationType" DataValueField="Code" DataTextField="Description" DataOrder="DisplayOrder ASC" />
		    <uc:Number id="txtCoinsurance" Visible="false" DataType="Integer" runat="server" field="Building.CoinsurancePercentage" Name="Coinsurance %" Columns="10" Directions="(ex. 80)" />
        </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" />
    <asp:Button ID="btnCancel" Runat="server"  Text="Cancel" CausesValidation="False"  CssClass="closeClass" />
    
    </div>
    
    </form>
</body>
</html>
