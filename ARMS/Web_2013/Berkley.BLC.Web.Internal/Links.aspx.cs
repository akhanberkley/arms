using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;

public partial class LinksAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(LinksAspx_Load);
    }
    #endregion

    void LinksAspx_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            //Get the links based on the company
            Link[] eLinks = Link.GetSortedArray("CompanyID = ?", "PriorityIndex ASC", UserIdentity.Current.CompanyID);
            repLinks.DataSource = eLinks;
            repLinks.DataBind();
        }
    }
}
