using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web.Validation;

public partial class AdminServicePlanFieldEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminServicePlanFieldEditAspx_Load);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected DynamicField _eField;

    void AdminServicePlanFieldEditAspx_Load(object sender, EventArgs e)
    {
        Guid gFieldID = ARMSUtility.GetGuidFromQueryString("fieldid", false);
        _eField = (gFieldID != Guid.Empty) ? DynamicField.Get(gFieldID) : null;

        if (!this.IsPostBack)
        {
            if (_eField != null) // edit
            {
                this.PopulateFields(_eField);
            }
        }

        btnSubmit.Text = (_eField == null) ? "Add" : "Update";
        base.PageHeading = (_eField == null) ? "Add Field" : "Update Field";
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            DynamicField eField;
            if (_eField == null) // add
            {
                eField = new DynamicField(Guid.NewGuid());
                eField.CompanyID = this.UserIdentity.CompanyID;
            }
            else // edit
            {
                eField = _eField.GetWritableInstance();
            }

            this.PopulateEntity(eField);

            // commit all changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                eField.Save(trans);
                trans.Commit();
            }

            // update our member var so the redirect will work correctly
            _eField = eField;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl());
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        return "AdminServicePlanFields.aspx";
    }
}
