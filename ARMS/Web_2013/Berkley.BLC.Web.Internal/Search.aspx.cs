using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web;
using QCI.Web.Validation;

public partial class SearchAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(SearchAspx_Load);
        this.PreRender += new EventHandler(SearchAspx_PreRender);
        this.btnSearch.Click += new EventHandler(btnSearch_Click);
        this.btnLucky.Click += new EventHandler(btnLucky_Click);        
    }
    #endregion

    protected bool feelingLuckyInvoked = false;
    protected bool _displayRecStatusFilter = false;
    protected bool _showPolicySystem;
    void SearchAspx_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            ARMSUtility.SetInputFocus(numSurveyNumber);


            _displayRecStatusFilter = (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplayRecStatusFilter", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
            _showPolicySystem = (Berkley.BLC.Business.Utility.GetCompanyParameter("ShowPolicySystem", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;

            // populate the survey type list with company specific types
            SurveyType[] eTypes = SurveyType.GetSortedArray("CompanyID = ? && Disabled = False", "DisplayOrder ASC", this.CurrentUser.CompanyID);
            cboSurveyType.DataBind(eTypes, "ID", "Type.Name");

            //cboPolicySystem.Items.Insert(1, new ListItem("BMAG", "77C82BD6-0BD9-4D79-92FD-C133788A3B0E"));
            //cboPolicySystem.Items.Insert(2, new ListItem("BSIG", "87C82BD6-0BD9-4D79-92FD-C133788A3B02"));
            //cboPolicySystem.Items.Insert(3, new ListItem("USIG", "25F08C73-E8A3-4475-AF1A-91882ABCDB76"));
            CompanyPolicySystem[] policySystems = CompanyPolicySystem.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            cboPolicySystem.DataBind(policySystems, "PolicySystemCompanyID", "PolicySystemCompany.LongAbbreviation");

            if (_showPolicySystem && cboPolicySystem.Items.Count > 1)
                cboPolicySystem.SelectedIndex = 0;
            	
	
	
            // populate the user list with all users (exclude underwriters and sort disabled users to the bottom)
            User[] eUsers = Berkley.BLC.Entities.User.GetSortedArray("CompanyID = ? && IsUnderwriter = false", "AccountDisabled ASC, Name ASC", this.CurrentUser.CompanyID);
            cboUser.DataBind(eUsers, "ID", "Name");
            cboRecommendedUser.DataBind(eUsers, "ID", "Name");
            cboConsultant.DataBind(eUsers, "ID", "Name");

            // populate the user list with all internal users in the company (exclude fee companies and underwriters)
            User[] eInternalUsers = Berkley.BLC.Entities.User.GetSortedArray("CompanyID = ? && IsFeeCompany = false && IsUnderwriter = false", "AccountDisabled ASC, Name ASC", this.CurrentUser.CompanyID);
            cboReviewer.DataBind(eInternalUsers, "ID", "Name");

            cboCreatedByStaff.DataBind(eInternalUsers, "ID", "Name");
            cboCreatedByStaff.Items.Insert(1, new ListItem("(All)", "B3EDF5CC-F499-4a12-ABAD-4899EBB1CAA4"));

            // populate the sic list with all sic codes and descriptions
            SIC[] codes = AppCache.GetSICCodes(this, CurrentUser.Company, "SICCodes40", 40);
            string sicDisplayOrder = CurrentUser.Company.SICSortedByAlpha ? "Description|Code" : "Code|Description";
            cboSIC.DataBind(codes, "Code", "{0} - {1}", sicDisplayOrder.Split('|'));


            // populate the underwriter list with company specific underwriter names and initials
            Underwriter[] underwriters = AppCache.GetUnderwriters(this, CurrentUser.Company, "Underwriters", false);
            cboUnderwriter.DataBind(underwriters, "Code", "Name");

            //populate the company specific profit centers
            ProfitCenter[] eProfitCenters = ProfitCenter.GetSortedArray("CompanyID = ?", "Name ASC", CurrentUser.CompanyID);
            cboProfitCenterByUser.DataBind(eProfitCenters, "Code", "Name");
            cboProfitCenterByRegion.DataBind(eProfitCenters, "Code", "Name");

            // Get all the external (non-staff) user names that created a survey
            DataTable dt = AppCache.GetCreatedByNonLCStaff(this, CurrentUser.Company, "CreatedByNonStaff");
            cboCreatedByNonStaff.DataBind(dt, "Code", "Name");
            cboCreatedByNonStaff.Items.Insert(1, new ListItem("(All)", "(All)"));

            //Populate the bools
            ComboForBooleans[] cboData = ComboForBooleans.GetValues();
            cboCorrection.DataBind(cboData, "ID", "DisplayText");
            cboOpenRecs.DataBind(cboData, "ID", "DisplayText");
            cboCriticalRecs.DataBind(cboData, "ID", "DisplayText");
            

            cboData = ComboForBooleans.GetValuesNoBits();
            cboPACEAccount.DataBind(cboData, "ID", "DisplayText");
            cboKeyAccount.DataBind(cboData, "ID", "DisplayText");
            cboLossSensitive.DataBind(cboData, "ID", "DisplayText");
            cboUnderwritingAccepted.DataBind(cboData, "ID", "DisplayText");

            // populate the QA dropdwon
            SurveyQuality[] qualities = SurveyQuality.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", this.CurrentUser.CompanyID);
            cboQA.DataBind(qualities, "ID", "Type.Name");
            cboQA.Visible = qualities.Length > 0;

            // populate the letters dropdown
            MergeDocument[] letters = MergeDocument.GetSortedArray("CompanyID = ? && Disabled = false && Type = ? && !ISNULL(DaysUntilReminder)",
            "PriorityIndex ASC", this.CurrentUser.CompanyID, MergeDocumentType.Letter.ID);
            cboLettersSent.DataBind(letters, "ID", "Name");
            cboLettersSent.Visible = letters.Length > 0;

            SurveyReleaseOwnershipType[] releaseTypes = SurveyReleaseOwnershipType.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", this.CurrentUser.CompanyID);
            cboReleaseOwnershipReason.DataBind(releaseTypes, "ID", "Name");
            cboReleaseOwnershipReason.Visible = qualities.Length > 0;

            //get Critical Combo
            RecClassification recClassification = RecClassification.GetOne("CompanyID = ? && TypeCode = ?", CurrentUser.CompanyID, RecClassificationType.Critical.Code);
            cboCriticalRecs.Visible = (recClassification != null) ? true : false;


        }
    }

    void SearchAspx_PreRender(object sender, EventArgs e)
    {
        if (!this.IsPostBack && HttpContext.Current.Request.QueryString.AllKeys.Length > 0)
        {
            // populate the search fields with any values provided on the query string
            SearchCriteria c = SearchCriteria.Create(HttpContext.Current.Request.QueryString);

            // survey search fields
            if (c.SurveyNumber != int.MinValue)
            {
                numSurveyNumber.SetValue(c.SurveyNumber);
            }
            if (c.SurveyTypeID != Guid.Empty)
            {
                cboSurveyType.SelectItem(c.SurveyTypeID.ToString());
            }
            if (c.PolicySystemID != Guid.Empty)
            {
                cboPolicySystem.SelectItem(c.PolicySystemID.ToString());
            }
            if (c.ServiceTypeCode != null)
            {
                cboServiceType.SelectItem(c.ServiceTypeCode);
            }
            if (c.UserID != Guid.Empty)
            {
                cboUser.SelectItem(c.UserID.ToString());
            }
            if (c.RecommendedUserID != Guid.Empty)
            {
                cboRecommendedUser.SelectItem(c.RecommendedUserID.ToString());
            }
            if (c.ConsultantID != Guid.Empty)
            {
                cboConsultant.SelectItem(c.ConsultantID.ToString());
            }
            if (c.ReviewerID != Guid.Empty)
            {
                cboReviewer.SelectItem(c.ReviewerID.ToString());
            }
            if (c.CreatedByStaffID != Guid.Empty)
            {
                cboCreatedByStaff.SelectItem(c.CreatedByStaffID.ToString());
            }
            if (c.CreatedByNonStaff != null)
            {
                cboCreatedByNonStaff.SelectItem(c.CreatedByNonStaff.ToString());
            }
            if (c.SurveyStatusCode != null)
            {
                cboStatus.SelectItem(c.SurveyStatusCode);
            }
            if (c.ProfitCenterByUser != null)
            {
                cboProfitCenterByUser.SelectItem(c.ProfitCenterByUser);
            }
            if (c.UnderwritingAccepted != null)
            {
                cboUnderwritingAccepted.SelectItem(c.UnderwritingAccepted);
            }
            if (c.Correction != null)
            {
                cboCorrection.SelectItem(c.Correction);
            }
            if (c.OpenRecs != null)
            {
                cboOpenRecs.SelectItem(c.OpenRecs);
            }
            if (c.CriticalRecs != null)
            {
                cboCriticalRecs.SelectItem(c.CriticalRecs);
                if (c.CriticalRecs == "1")
                {
                    divOpenCriticalRecs.Attributes.Clear();
                    divOpenCriticalRecs.Attributes.Add("style", "VISIBILITY: visible");
                }
            }
            if (c.OpenCriticalRecs)
            {
                chkOpenCriticalRecs.Checked = true;
            }
            if (c.PACEAccount != null)
            {
                cboPACEAccount.SelectItem(c.PACEAccount);
            }
            if (c.KeyAccount != null)
            {
                cboKeyAccount.SelectItem(c.KeyAccount);
            }
            if (c.LossSensitive != null)
            {
                cboLossSensitive.SelectItem(c.LossSensitive);
            }
            if (c.SurveyQualityID != Guid.Empty)
            {
                cboQA.SelectItem(c.SurveyQualityID.ToString());
            }
            if (c.LastModifiedStart != DateTime.MinValue)
            {
                drLastModified.StartDate = c.LastModifiedStart;
            }
            if (c.LastModifiedEnd != DateTime.MaxValue)
            {
                drLastModified.EndDate = c.LastModifiedEnd;
            }
            if (c.CreatedOnStart != DateTime.MinValue)
            {
                drCreatedOn.StartDate = c.CreatedOnStart;
            }
            if (c.CreatedOnEnd != DateTime.MaxValue)
            {
                drCreatedOn.EndDate = c.CreatedOnEnd;
            }
            if (c.DueDateStart != DateTime.MinValue)
            {
                drDueDate.StartDate = c.DueDateStart;
            }
            if (c.DueDateEnd != DateTime.MaxValue)
            {
                drDueDate.EndDate = c.DueDateEnd;
            }
            if (c.AssignDateStart != DateTime.MinValue)
            {
                drAssignDate.StartDate = c.AssignDateStart;
            }
            if (c.AssignDateEnd != DateTime.MaxValue)
            {
                drAssignDate.EndDate = c.AssignDateEnd;
            }
            if (c.AcceptDateStart != DateTime.MinValue)
            {
                drAcceptDate.StartDate = c.AcceptDateStart;
            }
            if (c.AcceptDateEnd != DateTime.MaxValue)
            {
                drAcceptDate.EndDate = c.AcceptDateEnd;
            }
            if (c.UnderwritingAcceptDateStart != DateTime.MinValue)
            {
                drUnderwritingAcceptDate.StartDate = c.UnderwritingAcceptDateStart;
            }
            if (c.UnderwritingAcceptDateEnd != DateTime.MaxValue)
            {
                drUnderwritingAcceptDate.EndDate = c.UnderwritingAcceptDateEnd;
            }
            if (c.DateSurveyedStart != DateTime.MinValue)
            {
                drDateSurveyed.StartDate = c.DateSurveyedStart;
            }
            if (c.DateSurveyedEnd != DateTime.MaxValue)
            {
                drDateSurveyed.EndDate = c.DateSurveyedEnd;
            }
            if (c.CompletedReportDateStart != DateTime.MinValue)
            {
                drCompletedReportDate.StartDate = c.CompletedReportDateStart;
            }
            if (c.CompletedReportDateEnd != DateTime.MaxValue)
            {
                drCompletedReportDate.EndDate = c.CompletedReportDateEnd;
            }
            if (c.CanceledDateStart != DateTime.MinValue)
            {
                drCanceledDate.StartDate = c.CanceledDateStart;
            }
            if (c.CanceledDateEnd != DateTime.MaxValue)
            {
                drCanceledDate.EndDate = c.CanceledDateEnd;
            }
            if (c.CompletedDateStart != DateTime.MinValue)
            {
                drCompletedDate.StartDate = c.CompletedDateStart;
            }
            if (c.CompletedDateEnd != DateTime.MaxValue)
            {
                drCompletedDate.EndDate = c.CompletedDateEnd;
            }
            if (c.ReviewerCompletedDateStart != DateTime.MinValue)
            {
                drReviewerCompletedDate.StartDate = c.ReviewerCompletedDateStart;
            }
            if (c.ReviewerCompletedDateEnd != DateTime.MaxValue)
            {
                drReviewerCompletedDate.EndDate = c.ReviewerCompletedDateEnd;
            }
            if (c.SurveyLetterID != Guid.Empty)
            {
                cboLettersSent.SelectItem(c.SurveyLetterID.ToString());
            }
            if (c.SurveyReleaseOwnershipTypeID != Guid.Empty)
            {
                cboReleaseOwnershipReason.SelectItem(c.SurveyReleaseOwnershipTypeID.ToString());
            }

            // insured fields
            if (c.InsuredClientID != null)
            {
                txtClientID.Text = c.InsuredClientID;
            }
            if (c.InsuredPortfolioID != null)
            {
                txtPortfolioID.Text = c.InsuredPortfolioID;
            }
            if (c.InsuredName != null)
            {
                txtInsuredName.Text = c.InsuredName;
            }
            if (c.InsuredDBA != null)
            {
                txtInsuredDBA.Text = c.InsuredDBA;
            }
            if (c.InsuredCity != null)
            {
                txtInsuredCity.Text = c.InsuredCity;
            }
            if (c.InsuredStateCode != null)
            {
                cboInsuredState.SelectItem(c.InsuredStateCode);
            }
            if (c.InsuredZipCode != null)
            {
                txtInsuredZipCode.Text = c.InsuredZipCode;
            }
            if (c.InsuredNonRenewedDateStart != DateTime.MinValue)
            {
                drNonRenewedDate.StartDate = c.InsuredNonRenewedDateStart;
            }
            if (c.InsuredNonRenewedDateEnd != DateTime.MaxValue)
            {
                drNonRenewedDate.EndDate = c.InsuredNonRenewedDateEnd;
            }
            if (c.InsuredBusinessOperations != null)
            {
                txtBusinessOperations.Text = c.InsuredBusinessOperations;
            }
            if (c.InsuredSICCode != null)
            {
                cboSIC.SelectItem(c.InsuredSICCode);
            }
            if (c.PrimaryPolicyProfitCenter != null)
            {
                cboProfitCenterByRegion.SelectItem(c.PrimaryPolicyProfitCenter);
            }
            if (c.Underwriter != null)
            {
                cboUnderwriter.SelectItem(c.Underwriter);
            }

            // location search fields
            if (c.LocationCity != null)
            {
                txtLocationCity.Text = c.LocationCity;
            }
            if (c.LocationStateCode != null)
            {
                cboLocationState.SelectItem(c.LocationStateCode);
            }
            if (c.LocationZipCode != null)
            {
                txtLocationZipCode.Text = c.LocationZipCode;
            }

            // policy search fields
            if (c.PolicyNumber != null)
            {
                txtPolicyNumber.Text = c.PolicyNumber;
            }
            if (c.PolicyEffectiveDateStart != DateTime.MinValue)
            {
                drPolicyEffectiveDate.StartDate = c.PolicyEffectiveDateStart;
            }
            if (c.PolicyEffectiveDateEnd != DateTime.MaxValue)
            {
                drPolicyEffectiveDate.EndDate = c.PolicyEffectiveDateEnd;
            }
            if (c.PolicyExpirationDateStart != DateTime.MinValue)
            {
                drPolicyExpireDate.StartDate = c.PolicyExpirationDateStart;
            }
            if (c.PolicyExpirationDateEnd != DateTime.MaxValue)
            {
                drPolicyExpireDate.EndDate = c.PolicyExpirationDateEnd;
            }
            if (c.TotalPremiumMin != decimal.MinValue)
            {
                numTotalPremium.StartValue = c.TotalPremiumMin;
            }
            if (c.TotalPremiumMax != decimal.MaxValue)
            {
                numTotalPremium.EndValue = c.TotalPremiumMax;
            }

            // agency fields
            if (c.AgencyNumber != null)
            {
                txtAgencyNumber.Text = c.AgencyNumber;
            }
            if (c.AgencyName != null)
            {
                txtAgencyName.Text = c.AgencyName;
            }
            if (c.AgencyStateCode != null)
            {
                cboAgencyState.SelectItem(c.AgencyStateCode);
            }
        }
    }

    void btnLucky_Click(object sender, EventArgs e)
    {
        feelingLuckyInvoked = true;
        btnSearch_Click(null, null);
    }

    void btnSearch_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        //little pre-work if searching by policy number
        string queryByPolicy = SearchCriteria.BuildPolicyQuery(CurrentUser.CompanyID, txtPolicyNumber.Text);


        SearchCriteria c = new SearchCriteria();
        string qs;

        // fill the search criteria object
        try
        {
            // survey search fields
            if (numSurveyNumber.Text.Length > 0)
            {
                c.SurveyNumber = (int)numSurveyNumber.GetValue();
            }
            if (cboSurveyType.SelectedValue.Length > 0)
            {
                c.SurveyTypeID = new Guid(cboSurveyType.SelectedValue);
            }
            if (cboPolicySystem.SelectedValue.Length > 0)
            {
                c.PolicySystemID = new Guid(cboPolicySystem.SelectedValue);
            }
            if (cboServiceType.SelectedValue.Length > 0)
            {
                c.ServiceTypeCode = cboServiceType.SelectedValue;
            }
            if (cboUser.SelectedValue.Length > 0)
            {
                c.UserID = new Guid(cboUser.SelectedValue);
            }
            if (cboRecommendedUser.SelectedValue.Length > 0)
            {
                c.RecommendedUserID = new Guid(cboRecommendedUser.SelectedValue);
            }
            if (cboConsultant.SelectedValue.Length > 0)
            {
                c.ConsultantID = new Guid(cboConsultant.SelectedValue);
            }
            if (cboReviewer.SelectedValue.Length > 0)
            {
                c.ReviewerID = new Guid(cboReviewer.SelectedValue);
            }
            if (cboCreatedByStaff.SelectedValue.Length > 0)
            {
                c.CreatedByStaffID = new Guid(cboCreatedByStaff.SelectedValue);
            }
            if (cboCreatedByNonStaff.SelectedValue.Length > 0)
            {
                c.CreatedByNonStaff = cboCreatedByNonStaff.SelectedValue;
            }
            if (cboStatus.SelectedValue.Length > 0)
            {
                c.SurveyStatusCode = cboStatus.SelectedValue;
            }
            if (cboProfitCenterByUser.SelectedValue.Length > 0)
            {
                c.ProfitCenterByUser = cboProfitCenterByUser.SelectedValue;
            }
            if (cboUnderwritingAccepted.SelectedValue.Length > 0)
            {
                c.UnderwritingAccepted = cboUnderwritingAccepted.SelectedValue;
            }
            if (cboCorrection.SelectedValue.Length > 0)
            {
                c.Correction = cboCorrection.SelectedValue;
            }
            if (cboOpenRecs.SelectedValue.Length > 0)
            {
                c.OpenRecs = cboOpenRecs.SelectedValue;
            }
            if (cboCriticalRecs.SelectedValue.Length > 0)
            {
                c.CriticalRecs = cboCriticalRecs.SelectedValue;
            }
            if (chkOpenCriticalRecs.Checked)
            {
                c.OpenCriticalRecs = chkOpenCriticalRecs.Checked;
            }
            if (cboPACEAccount.SelectedValue.Length > 0)
            {
                c.PACEAccount = cboPACEAccount.SelectedValue;
            }
            if (cboKeyAccount.SelectedValue.Length > 0)
            {
                c.KeyAccount = cboKeyAccount.SelectedValue;
            }
            if (cboLossSensitive.SelectedValue.Length > 0)
            {
                c.LossSensitive = cboLossSensitive.SelectedValue;
            }
            if (cboQA.SelectedValue.Length > 0)
            {
                c.SurveyQualityID = new Guid(cboQA.SelectedValue);
            }
            if (drLastModified.StartDate != DateTime.MinValue)
            {
                c.LastModifiedStart = drLastModified.StartDate;
            }
            if (drLastModified.EndDate != DateTime.MaxValue)
            {
                c.LastModifiedEnd = drLastModified.EndDate;
            }
            if (drCreatedOn.StartDate != DateTime.MinValue)
            {
                c.CreatedOnStart = drCreatedOn.StartDate;
            }
            if (drCreatedOn.EndDate != DateTime.MaxValue)
            {
                c.CreatedOnEnd = drCreatedOn.EndDate;
            }
            if (drDueDate.StartDate != DateTime.MinValue)
            {
                c.DueDateStart = drDueDate.StartDate;
            }
            if (drDueDate.EndDate != DateTime.MaxValue)
            {
                c.DueDateEnd = drDueDate.EndDate;
            }
            if (drAssignDate.StartDate != DateTime.MinValue)
            {
                c.AssignDateStart = drAssignDate.StartDate;
            }
            if (drAssignDate.EndDate != DateTime.MaxValue)
            {
                c.AssignDateEnd = drAssignDate.EndDate;
            }
            if (drAcceptDate.StartDate != DateTime.MinValue)
            {
                c.AcceptDateStart = drAcceptDate.StartDate;
            }
            if (drAcceptDate.EndDate != DateTime.MaxValue)
            {
                c.AcceptDateEnd = drAcceptDate.EndDate;
            }
            if (drUnderwritingAcceptDate.StartDate != DateTime.MinValue)
            {
                c.UnderwritingAcceptDateStart = drUnderwritingAcceptDate.StartDate;
            }
            if (drUnderwritingAcceptDate.EndDate != DateTime.MaxValue)
            {
                c.UnderwritingAcceptDateEnd = drUnderwritingAcceptDate.EndDate;
            }
            if (drDateSurveyed.StartDate != DateTime.MinValue)
            {
                c.DateSurveyedStart = drDateSurveyed.StartDate;
            }
            if (drDateSurveyed.EndDate != DateTime.MaxValue)
            {
                c.DateSurveyedEnd = drDateSurveyed.EndDate;
            }
            if (drCompletedReportDate.StartDate != DateTime.MinValue)
            {
                c.CompletedReportDateStart = drCompletedReportDate.StartDate;
            }
            if (drCompletedReportDate.EndDate != DateTime.MaxValue)
            {
                c.CompletedReportDateEnd = drCompletedReportDate.EndDate;
            }
            if (drCanceledDate.StartDate != DateTime.MinValue)
            {
                c.CanceledDateStart = drCanceledDate.StartDate;
            }
            if (drCanceledDate.EndDate != DateTime.MaxValue)
            {
                c.CanceledDateEnd = drCanceledDate.EndDate;
            }
            if (drCompletedDate.StartDate != DateTime.MinValue)
            {
                c.CompletedDateStart = drCompletedDate.StartDate;
            }
            if (drCompletedDate.EndDate != DateTime.MaxValue)
            {
                c.CompletedDateEnd = drCompletedDate.EndDate;
            }
            if (drReviewerCompletedDate.StartDate != DateTime.MinValue)
            {
                c.ReviewerCompletedDateStart = drReviewerCompletedDate.StartDate;
            }
            if (drReviewerCompletedDate.EndDate != DateTime.MaxValue)
            {
                c.ReviewerCompletedDateEnd = drReviewerCompletedDate.EndDate;
            }
            if (cboLettersSent.SelectedValue.Length > 0)
            {
                c.SurveyLetterID = new Guid(cboLettersSent.SelectedValue);
            }
            if (cboReleaseOwnershipReason.SelectedValue.Length > 0)
            {
                c.SurveyReleaseOwnershipTypeID = new Guid(cboReleaseOwnershipReason.SelectedValue);
            }

            // insured fields
            if (txtClientID.Text.Length > 0)
            {
                c.InsuredClientID = txtClientID.Text;
            }
            if (txtPortfolioID.Text.Length > 0)
            {
                c.InsuredPortfolioID = txtPortfolioID.Text;
            }
            if (txtInsuredName.Text.Length > 0)
            {
                c.InsuredName = txtInsuredName.Text;
            }
            if (txtInsuredDBA.Text.Length > 0)
            {
                c.InsuredDBA = txtInsuredDBA.Text;
            }
            if (txtInsuredCity.Text.Length > 0)
            {
                c.InsuredCity = txtInsuredCity.Text;
            }
            if (cboInsuredState.SelectedValue.Length > 0)
            {
                c.InsuredStateCode = cboInsuredState.SelectedValue;
            }
            if (txtInsuredZipCode.Text.Length > 0)
            {
                c.InsuredZipCode = txtInsuredZipCode.Text;
            }
            if (drNonRenewedDate.StartDate != DateTime.MinValue)
            {
                c.InsuredNonRenewedDateStart = drNonRenewedDate.StartDate;
            }
            if (drNonRenewedDate.EndDate != DateTime.MaxValue)
            {
                c.InsuredNonRenewedDateEnd = drNonRenewedDate.EndDate;
            }
            if (txtBusinessOperations.Text.Length > 0)
            {
                c.InsuredBusinessOperations = txtBusinessOperations.Text;
            }
            if (cboSIC.SelectedValue.Length > 0)
            {
                c.InsuredSICCode = cboSIC.SelectedValue;
            }
            if (cboProfitCenterByRegion.SelectedValue.Length > 0)
            {
                c.PrimaryPolicyProfitCenter = cboProfitCenterByRegion.SelectedValue;
            }
            if (cboUnderwriter.SelectedValue.Length > 0)
            {
                c.Underwriter = cboUnderwriter.SelectedValue;
            }

            // location search fields
            if (txtLocationCity.Text.Length > 0)
            {
                c.LocationCity = txtLocationCity.Text;
            }
            if (cboLocationState.SelectedValue.Length > 0)
            {
                c.LocationStateCode = cboLocationState.SelectedValue;
            }
            if (txtLocationZipCode.Text.Length > 0)
            {
                c.LocationZipCode = txtLocationZipCode.Text;
            }

            // policy search fields
            if (txtPolicyNumber.Text.Length > 0)
            {
                c.PolicyNumber = txtPolicyNumber.Text;
            }
            if (drPolicyEffectiveDate.StartDate != DateTime.MinValue)
            {
                c.PolicyEffectiveDateStart = drPolicyEffectiveDate.StartDate;
            }
            if (drPolicyEffectiveDate.EndDate != DateTime.MaxValue)
            {
                c.PolicyEffectiveDateEnd = drPolicyEffectiveDate.EndDate;
            }
            if (drPolicyExpireDate.StartDate != DateTime.MinValue)
            {
                c.PolicyExpirationDateStart = drPolicyExpireDate.StartDate;
            }
            if (drPolicyExpireDate.EndDate != DateTime.MaxValue)
            {
                c.PolicyExpirationDateEnd = drPolicyExpireDate.EndDate;
            }
            if (numTotalPremium.StartValue != decimal.MinValue)
            {
                c.TotalPremiumMin = numTotalPremium.StartValue;
            }
            if (numTotalPremium.EndValue != decimal.MaxValue)
            {
                c.TotalPremiumMax = numTotalPremium.EndValue;
            }

            // agency fields
            if (txtAgencyNumber.Text.Length > 0)
            {
                c.AgencyNumber = txtAgencyNumber.Text;
            }
            if (txtAgencyName.Text.Length > 0)
            {
                c.AgencyName = txtAgencyName.Text;
            }
            if (cboAgencyState.SelectedValue.Length > 0)
            {
                c.AgencyStateCode = cboAgencyState.SelectedValue;
            }

            // build the query string containing all the criteria
            qs = c.ToQueryString();
            if (qs == null || qs.Length == 0)
            {
                throw new FieldValidationException("At least one criteria field must be specified.");
            }
            else if (qs.Contains("%7e"))
            {
                throw new FieldValidationException("Search criteria is not permitted to contain a tilde (~).");
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        object[] args;
        string query = c.GetOPathQuery(this.UserIdentity.CompanyID, out args) + queryByPolicy;

        //check if the "feeling lucky" button was clicked and need to redirect to survey screen
        if (feelingLuckyInvoked)
        {
            Guid surveyID = SpecialQueries.GetTopSurveyID(query.Trim(), ArrayToString(args), typeof(VWSurvey), "dbo.VW_Survey");

            if (surveyID != Guid.Empty)
            {
                Response.Redirect(string.Format("Survey.aspx?surveyid={0}&NavFromSearch=1&{1}", surveyID, qs), true);
            }
        }

        //check the size of the query
        int count = SpecialQueries.GetCount(query.Trim(), ArrayToString(args), typeof(VWSurvey), "dbo.VW_Survey");

        // redirect to the results page with the criteria in the query string
        string url = "SearchResults.aspx?" + qs;
        if (count > int.Parse(ConfigurationManager.AppSettings["SearchResultThreshold"]))
        {
            JavaScript.ConfirmAndRedirect(this, string.Format("Your search will return {0} records.  This large of a result set could impact the performance of the system.  Are you sure you want to continue?", count), url, "Search.aspx?" + qs);
        }
        else
        {
            Response.Redirect(url, true);
        }
    }

    
    private string ArrayToString(object[] args)
    {
        string result = string.Empty;
        foreach (object param in args)
        {
            //if (param.ToString().ToUpper() == "TRUE")
            //    result += "1,";
            //else if (param.ToString().ToUpper() == "FALSE")
            //    result += "0,";
            //else
                result += string.Format("{0}~", param);
        }

        return (result.Length > 0) ? result.Remove(result.Length - 1, 1) : result;
    }

    private class ComboForBooleans
    {
        private string id = "0";
        private string displayText = string.Empty;

        public ComboForBooleans() { }

        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        public string DisplayText
        {
            get { return displayText; }
            set { displayText = value; }
        }

        public static ComboForBooleans[] GetValues()
        {
            ArrayList list = new ArrayList();

            for (int i = 1; i >= 0; i--)
            {
                ComboForBooleans cbo = new ComboForBooleans();

                cbo.DisplayText = (i == 0) ? "No" : "Yes";
                cbo.ID = i.ToString();
                list.Add(cbo);
            }

            return list.ToArray(typeof(ComboForBooleans)) as ComboForBooleans[];
        }

        public static ComboForBooleans[] GetValuesNoBits()
        {
            ArrayList list = new ArrayList();

            for (int i = 1; i >= 0; i--)
            {
                ComboForBooleans cbo = new ComboForBooleans();

                cbo.DisplayText = (i == 0) ? "No" : "Yes";
                cbo.ID = (i == 0) ? "NO" : "YES";
                list.Add(cbo);
            }

            return list.ToArray(typeof(ComboForBooleans)) as ComboForBooleans[];
        }
    }
}
