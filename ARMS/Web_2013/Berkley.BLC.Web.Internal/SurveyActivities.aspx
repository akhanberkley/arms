<%@ Page Language="C#" AutoEventWireup="false" CodeFile="SurveyActivities.aspx.cs" Inherits="SurveyActivitiesAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Date" Src="~/Fields/DateField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><a class="nav" href="Survey.aspx<%= ARMSUtility.GetQueryStringForNav() %>"><< Back to Survey</a><br /><br />
    
    <asp:datagrid ID="grdActivities" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="20" AllowSorting="True">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
            <asp:TemplateColumn HeaderText="Activity Type" SortExpression="ActivityType.Name ASC">
	            <ItemTemplate>
                    <a href="SurveyActivities.aspx<%= ARMSUtility.GetQueryStringForNav("activityid") %>&activityid=<%# HtmlEncode(Container.DataItem, "ID")%>">
		                <%# HtmlEncodeNullable(Container.DataItem, "ActivityType.Name", string.Empty, "(none)")%>
		            </a>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Hours" SortExpression="ActivityHours ASC">
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "ActivityHours", decimal.MinValue, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
		    <asp:TemplateColumn HeaderText="Date" SortExpression="ActivityDate DESC">
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "ActivityDate", "{0:d}", DateTime.MinValue, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Allocated To" SortExpression="User.Name ASC">
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "User.Name", string.Empty, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn>
	            <ItemTemplate>
		            &nbsp;<asp:LinkButton ID="btnRemoveRec" Runat="server" CommandName="Remove" OnPreRender="btnRemoveActivity_PreRender">Remove</asp:LinkButton>&nbsp;
	            </ItemTemplate>
            </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>

    <asp:LinkButton ID="btnAdd" runat="server">Add New Activity Time</asp:LinkButton>

    <div id="divAddActivity" runat="server" visible="false">
        <br /><br />
        <cc:Box id="boxActivity" runat="server" Title="Activity" width="625">
            <table class="fields">
                <uc:Combo id="cboActivityType" runat="server" field="ActivityType.ID" Name="Activity Type" topItemText="" isRequired="True" />
                <uc:Date id="dtActivityDate" runat="server" field="SurveyActivity.ActivityDate" Name="Date" IsRequired="True" />
                <uc:Number id="numHours" runat="server" field="SurveyActivity.ActivityHours" Name="Hours" IsRequired="True" />
                <uc:Text id="txtComments" runat="server" field="SurveyActivity.Comments" Name="Comments" Rows="5" Columns="88" />
            </table>
        </cc:Box>
    
        <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" />
        <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    </div>
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
