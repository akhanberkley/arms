<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" CodeFile="ServicePlan.aspx.cs" ValidateRequest="false" Inherits="ServicePlanAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Date" Src="~/Fields/DateField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="dp" Namespace="QCI.Web.Controls" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <link rel="stylesheet" media="print" type="text/css" href="<%= base.TemplatePath %>/Print-Survey.css" />
    <style type="text/css">a img {border-style:none;}</style>

    <script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/Keyoti_RapidSpell_Web_Common/RapidSpell-DIALOG.js"></script>
    <script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/javascripts/csshttprequest.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="<%=ARMSUtility.TemplatePath %>/atd.css" />
 

    <form id="frm" method="post" runat="server">
         <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/AjaxMethodsService.asmx" />
        </Services>
    </asp:ScriptManager>
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span>&nbsp;&nbsp;<input type="button" id="btnPrintPage" value="Print Plan" class="btn" onClick="window.print()" /><a class="nav" href="ServicePlans.aspx"><< Back to Service Plans</a><br><br>

    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <table cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td valign="top" width="550">
        <cc:Box id="boxServicePlan" runat="server" title="Service Plan Information" width="100%">
            <table class="fields">
            <uc:Label id="lblNumber" runat="server" field="ServicePlan.Number" />
            <uc:Label id="lblCreateDate" runat="server" field="ServicePlan.CreateDate" Name="Created On" />
            <% if (_hideSPCreatedBy.ToString() != "true") { %>
                <uc:Label id="lblCreatedBy" runat="server" field="ServicePlan.HtmlCreatedBy" Name="Created By" />
            <% } %>
            <uc:Label id="lblType" runat="server" field="ServicePlanType.Name" />
            <uc:Label id="lblStatus" runat="server" field="ServicePlanStatus.Name" />
            <uc:Label id="lblAssignedUser" runat="server" field="ServicePlan.HtmlAssignedTo" Name="Assigned User" />
            <% if (_eServicePlan.CompleteDate != DateTime.MinValue) { %>
            <uc:label id="lblCompleteDate" runat="server" field="ServicePlan.CompleteDate" />
            <% } %>
            <% if (_usesPlanGrading) { %>
                <uc:Label id="lblGrading" runat="server" field="SurveyGrading.Name" Name="Overall Plan Grading" />
            <% } %>
            <uc:Label id="lblLastModifiedOn" runat="server" field="ServicePlan.LastModifiedOn" ValueFormat="{0:g}" />
            </table>
        </cc:Box>
        
        <cc:Box id="boxPlanDetails" runat="server" title="Additional Plan Information" width="100%">
            <table class="fields">
                <% if (!_isEditMode) { %>
                    <%if (repFields.Items.Count > 0) {%>
                        <asp:Repeater ID="repFields" Runat="server">
                        <ItemTemplate>
                            <tr>
                                <td class="label" valign="top" style="white-space:nowrap"><span id="lblAttributeReadOnly" runat="server"></span></td>
                                <td>
                                    <span id="lblAttributeValueReadOnly" runat="server"></span>
                                </td>
                            </tr>
                        </ItemTemplate>

                        </asp:Repeater>
                    <%} else {%>
                        <div style="PADDING-LEFT: 200px">
                            (none)
                        </div>
                    <%} %>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <br><asp:LinkButton ID="lnkEditInstructions" runat="server" CausesValidation="false">Edit Details</asp:LinkButton>
                        </td>
                    </tr>
                <% } else { %>
                    <asp:Repeater ID="repServicePlanAttributes" Runat="server">
                        <ItemTemplate>
                            <tr>
                                <td class="label" valign="top" style="white-space:nowrap;"><span id="lblAttributeLabel" runat="server"></span></td>

                                <td ><cc:DynamicControlsPlaceholder id="pHolder" Runat="Server" /></td>
                            </tr>			
                        </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                            <td>&nbsp;</td>
                            <td><br><asp:Button ID="btnSaveDetails" Runat="server" CssClass="btn" Text="Save Details" /></td>
                        </tr>
                <% } %>
           </table>
        </cc:Box>
        
        <cc:Box id="boxVisits" runat="server" title="Service Visits" width="100%">
            <a id="lnkShowVisits" runat="server" href="javascript:Toggle('divVisits')">Show/Hide</a>
            <div id="divVisits" style="MARGIN-TOP: 10px;" runat="server">
            <asp:datagrid ID="grdVisits" Runat="server" CssClass="grid" AutoGenerateColumns="False" PageSize="100" AllowSorting="True" AllowPaging="True" Width="100%">
                <headerstyle CssClass="header" />
                <itemstyle CssClass="item" />
                <alternatingitemstyle CssClass="altItem" />
                <footerstyle CssClass="footer" />
                <pagerstyle CssClass="pager" Mode="NumericPages" />
                <columns>
                    <asp:TemplateColumn HeaderText="Service Visit&nbsp;#" SortExpression="Number ASC">
                        <ItemTemplate>
                            <a href="Survey.aspx?NavFromPlan=1&planid=<%= _eServicePlan.ID %>&surveyid=<%# HtmlEncode(Container.DataItem, "ID")%>"><%# HtmlEncode(Container.DataItem, "Number")%></a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Due Date" SortExpression="DueDate ASC" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "DueDate", "{0:d}", DateTime.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="Type" SortExpression="Type.Type.Name ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Type.Type.Name", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" SortExpression="Status.Name ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Status.Name", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Location">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Location.SingleLineAddressWithNumber", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Consultant" SortExpression="ConsultantUser.Name ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "HtmlConsultantGrid", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </columns>
            </asp:datagrid>
            </div>
        </cc:Box>

            <cc:Box id="boxPlanVisits" runat="server" title="Service Visits" width="100%" Visible="false">
            <a id="lnkShowPlanVisits" runat="server" href="javascript:Toggle('divPlanVisits')">Show/Hide</a>
            <div id="divPlanVisits" style="MARGIN-TOP: 10px;" runat="server">
            <asp:datagrid ID="grdPlanVisits" Runat="server" CssClass="grid" AutoGenerateColumns="False" PageSize="100" AllowSorting="True" AllowPaging="True" Width="100%">
                <headerstyle CssClass="header" />
                <itemstyle CssClass="item" />
                <alternatingitemstyle CssClass="altItem" />
                <footerstyle CssClass="header" />
                <pagerstyle CssClass="pager" Mode="NumericPages" />
                <columns>
                    <asp:TemplateColumn HeaderText="Service Visit&nbsp;#" SortExpression="Number ASC">
                        <ItemTemplate>
                            <a href="Survey.aspx?NavFromPlan=1&planid=<%= _eServicePlan.ID %>&surveyid=<%# HtmlEncode(Container.DataItem, "ID")%>"><%# HtmlEncode(Container.DataItem, "Number")%></a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Date Surveyed" SortExpression="DateSurveyed ASC" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "SurveyedDate", "{0:d}", DateTime.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" SortExpression="Status.Name ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Status.Name", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Location">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Location.SingleLineAddressWithNumber", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Grading" SortExpression="Grading.Name ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Grading.Name", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Consultant" SortExpression="ConsultantUser.Name ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "HtmlConsultantGrid", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Total Hours" SortExpression="TotalActivityHours ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "TotalActivityHours", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </columns>
            </asp:datagrid>
            </div>
        </cc:Box>
        
        <div id="divObjectivesNotForPrint">
        <cc:Box id="boxObjectives" runat="server" title="Service Visit Objectives" width="100%">
        <a id="lnkShowObjectives" runat="server" href="javascript:Toggle('divObjectives')">Show/Hide</a>
        <div id="divObjectives" style="MARGIN-TOP: 10px;">
            <asp:Repeater ID="repServiceVisitsObjectives" Runat="server">
                <HeaderTemplate>
                    <table>
                </HeaderTemplate>
                <FooterTemplate>
                   </table> 
                </FooterTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" nowrap>
                            <b><%# GetSurveyDetails(Container.DataItem) %></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <% if (1 > 0) { %>
                            <asp:Repeater ID="repBuildings" DataSource="<%# GetObjectivesForSurvey(Container.DataItem) %>" runat="server">
                                <HeaderTemplate>
                                    <table style="margin: 5px 0px 0px 20px" cellspacing="0">
                                        <tr>
                                            <td><u>Number</u></td>
                                            <asp:Repeater ID="repObjectiveAttributeLabels" DataSource="<%# _companyObjectiveAttributes %>" runat="server">
                                                <ItemTemplate>
                                                    <td style="padding-left: 20px"><u><%# HtmlEncode(Container.DataItem, "Label")%></u></td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <td style="padding-left: 20px"><u>Status</u></td>
                                             <% if (_displayPrintSelection) { %>
                                            <td style="padding-left: 20px"><u>Print</u></td>
                                               <% } %>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td valign="top"><%# HtmlEncodeNullable(Container.DataItem, "Number", Int32.MinValue, "(none)")%></td>
                                        <asp:Repeater ID="repObjectiveAttributeValues" DataSource="<%# GetObjectiveAttributeValues(Container.DataItem) %>" runat="server">
                                            <ItemTemplate>
                                                <%# GetEncodedAttributeValue(Container.DataItem)%>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <td valign="top" style="padding-left: 20px"><%# HtmlEncodeNullable(Container.DataItem, "Status.Name", string.Empty, "(none)")%></td>
                                             <% if (_displayPrintSelection ) { %>
                                        <td valign="top" style="padding-left: 20px">
                                            <asp:CheckBox ID="IsPrint" runat="server" CssClass="chk" Checked='<%#(bool)DataBinder.Eval(Container.DataItem, "IsPrint")%>' OnClick='<%# String.Format("SetPrint(\"{0}\", \"{1}\");", Eval("ID"),Eval("IsPrint")) %>'/></td>
                                        <% } %>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <%} else {%>
                            <div style="PADDING-LEFT: 10px">
                                (none)
                            </div>
                            <% } %>
                        </td>
                        
                    </tr>			
                </ItemTemplate>
            </asp:Repeater>
        </div>
        </cc:Box>
        </div>
        
        <div id="divObjectivesForPrint">
        <cc:Box id="boxObjectivesForPrint" runat="server" title="Service Visit Objectives" width="100%">
            <asp:Repeater ID="repServiceVisitsObjectivesForPrint" Runat="server">
                <HeaderTemplate>
                    <table>
                </HeaderTemplate>
                <FooterTemplate>
                   </table> 
                </FooterTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" nowrap>
                            <b><%# GetSurveyDetails(Container.DataItem) %></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <% if (1 > 0) { %>
                            <asp:Repeater ID="repBuildings" DataSource="<%# GetObjectivesForSurvey(Container.DataItem) %>" Runat="server">
                                <HeaderTemplate>
                                    <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0">
                                        <tr>
                                            <td nowrap><u>Number</u></td>
                                            <td style="PADDING-LEFT: 20px" nowrap><u>Objective</u></td>
                                            <td style="PADDING-LEFT: 20px" nowrap><u>Comments</u></td>
                                            <td style="PADDING-LEFT: 20px" nowrap><u>Target Date</u></td>
                                            <td style="PADDING-LEFT: 20px" nowrap><u>Status</u></td>
                                            <td style="PADDING-LEFT: 20px" nowrap><u>Print</u></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Number", Int32.MinValue, "(none)")%></td>
                                        <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "Text", string.Empty, "(none)")%></td>
                                        <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "CommentsText", string.Empty, "(none)")%></td>
                                        <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "TargetDate", "{0:d}", DateTime.MinValue, "(none)")%></td>
                                        <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "Status", string.Empty, "(none)")%></td>
                                        <td valign="top" style="PADDING-LEFT: 20px">
                                            <asp:CheckBox ID="IsPrint1" runat="server" CssClass="chk" Checked='<%#(bool)DataBinder.Eval(Container.DataItem, "IsPrint") %>' AutoPostBack="true" /></td>
                                    </tr>			
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <%} else {%>
                            <div style="PADDING-LEFT: 10px">
                                (none)
                            </div>
                            <% } %>
                        </td>
                    </tr>			
                </ItemTemplate>
            </asp:Repeater>
        </cc:Box>
        </div>

        <cc:Box id="boxNotes" runat="server" title="Comments" width="100%">
        <a id="lnkAddNote" runat="server" href="javascript:Toggle('divNotes')">Show/Hide</a>
        <div id="divNotes" style="MARGIN-TOP: 10px">
        <% if( repNotes.Items.Count > 0 ) { %>
        <asp:Repeater ID="repNotes" Runat="server">
            <HeaderTemplate>
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td colspan="2" style="BORDER-BOTTOM: solid 1px black">
                        <%# GetUserName(DataBinder.Eval(Container.DataItem, "User")) %> - 
                        <%# HtmlEncode(Container.DataItem, "EntryDate", "{0:g}") %>
                    </td>
                </tr>
                <tr>
                    <td style="PADDING-BOTTOM: 10px">
                        <%# GetEncodedComment(Container.DataItem) %>
                    </td>
                    <td id="tdRemoveNote" align="right" valign="top" runat="server">
                        <asp:LinkButton ID="lnkRemoveNote" runat="server" OnClick="lnkRemoveNote_OnClick">Remove</asp:LinkButton>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <% } else { %>
        <div style="PADDING-BOTTOM: 10px">
            (none)
        </div>
        <% } %>
        </div>
        <div id="divAddNote" runat="server" style="MARGIN-TOP: 10px;">
            <table>
                <tr>
                    <td>
                        Comment<br>
                        <asp:TextBox id="txtNote" runat="server" CssClass="txt" TextMode="MultiLine" style="width: 475px" Rows="5" />
                    </td>
                    <td valign="top" style="padding-top: 10px;">
                        <a href="javascript:rapidSpell.dialog_spellCheck(true, 'txtNote')" id="checkLink"><img alt="Check Spelling" style="border:none;" src="<%=ARMSUtility.TemplatePath %>/javascripts/Images/atdbuttontr.gif" /></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnAddNote" Runat="server" Text="Add Comment" CssClass="btn" CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </div>
        </cc:Box>

        <cc:Box id="boxDocs" runat="server" title="Documents" width="100%">
        <a id="lnkAddDoc" href="javascript:Toggle('divAddDoc')">Show/Hide</a>
        <div id="divAddDoc" runat="server" style="MARGIN-TOP: 10px">
        <% if( repDocs.Items.Count > 0 ) { %>
        <asp:Repeater ID="repDocs" Runat="server">
            <HeaderTemplate>
                <table cellspacing="0" cellpadding="0" border="0">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="PADDING-BOTTOM: 3px">
                        <a id="lnkDownloadDocInNewWindow" href='<%# DataBinder.Eval(Container.DataItem, "ID", "Document.aspx?plandocid={0}") %>' target="_blank" runat="server"><%# GetFileNameWithSize(Container.DataItem) %></a>
                        <asp:LinkButton ID="lnkDownloadDoc" runat="server" OnClick="lnkDownloadDoc_OnClick"><%# GetFileNameWithSize(Container.DataItem) %></asp:LinkButton>
                    </td>
                    <td style="PADDING-LEFT: 10px" valign="top">
                        <%# GetUserName(DataBinder.Eval(Container.DataItem, "UploadedUser")) %> on <%# HtmlEncode(Container.DataItem, "UploadedOn", "{0:g}") %>
                    </td>
                    <td id="tdRemoveDoc" style="PADDING-LEFT: 15px" valign="top" runat="server">
                        <asp:LinkButton ID="lnkRemoveDoc" runat="server" OnClick="lnkRemoveDoc_OnClick">Remove</asp:LinkButton>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <% } else { %>
        <div style="PADDING-BOTTOM: 10px">
            (none)
        </div>
        <% } %>
            <div id="divUploadDoc" style="padding-top:5px" runat="server">
                Attach Document<br>
                <input type="file" id="fileUploader" runat="server" name="fileDocument" class="txt" size="70"><br>
                <%if (cboDocType.Items.Count > 0) { %>
                Doc Type<br>
                <asp:DropDownList ID="cboDocType" runat="server" CssClass="cbo"></asp:DropDownList><br>
                <%if (CurrentUser.Company.SupportsDocRemarks) { %>
                    Doc Remarks<br>
                    <asp:TextBox ID="txtDocRemarks" runat="server" Columns="35" CssClass="txt"></asp:TextBox>
                 <% } %>
                <% } %>
                <div style="padding-top:5px">
                    <asp:Button ID="btnAttachDocument" Runat="server" Text="Upload" CssClass="btn" CausesValidation="False" />
                </div>
            </div>
        </div>
        </cc:Box>
        
        <cc:Box id="boxHistory" runat="server" title="History" width="100%">
        <a id="lnkHistory" href="javascript:Toggle('divHistory')">Show/Hide</a><br>
        <div id="divHistory" style="MARGIN-TOP: 10px">
        <% if( repHistory.Items.Count > 0 ) { %>
            <asp:Repeater ID="repHistory" Runat="server">
                <HeaderTemplate>
                    <table cellspacing="0" cellpadding="0" border="0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" nowrap>
                            <%# HtmlEncode(Container.DataItem, "EntryDate", "{0:g}") %>
                        </td>
                        <td style="PADDING-LEFT: 10px; PADDING-BOTTOM: 4px">
                            <%# GetEncodedHistory(Container.DataItem)%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        <% } else { %>
            (none)
        <% } %>
        </div>
        </cc:Box>

        </td>
        <td width="15">&nbsp;</td>
        <td valign="top">
        
        <cc:Box id="boxLinks" runat="server" title="Insured Details" width="180">
        <ul>
            <li><a href="serviceplandetails.aspx<%= ARMSUtility.GetQueryStringForNav() %>">Insured Details</a></li> 
        </ul>
        </cc:Box>
            
        <cc:Box id="boxActions" runat="server" title="Available Actions" width="180">
            <asp:Repeater ID="repActions" Runat="server">
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <li>
                        <asp:LinkButton id="btnAction" runat="server" CommandName='<%# HtmlEncode(Container.DataItem, "ID") %>'><%# HtmlEncode(Container.DataItem, "DisplayName") %></asp:LinkButton>
                    </li>
                </ItemTemplate>
                <FooterTemplate>
                    <% if( repActions.Items.Count == 0 ) { %>
                    <li>None</li>
                    <% } %>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>

            <div id="divAssign" runat="server">
                <asp:Label ID="lblAssign" Runat="server"></asp:Label>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:DropDownList ID="cboUsers" runat="server" CssClass="cbo" />
                </div>
                <asp:Button ID="btnAssign" Runat="server" Text="Assign" CssClass="btn" />
                <asp:Button ID="btnAssignCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            
            <div id="divType" runat="server">
                <span>New Service Plan Type:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:DropDownList ID="cboTypes" runat="server" CssClass="cbo" />
                </div>
                <asp:Button ID="btnTypeUpdate" Runat="server" Text="Update" CssClass="btn" />
                <asp:Button ID="btnTypeCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>

            <div id="divPlanGrading" runat="server">
                <span>Overall Plan Grading:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:DropDownList ID="cboOverallPlanGrading" runat="server" CssClass="cbo" />
                </div>
                <asp:Button ID="btnGradingUpdate" Runat="server" Text="Update" CssClass="btn" />
                <asp:Button ID="btnGradingCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>  
            
            <div id="divClosePlan" runat="server">

                <% if (_usesPlanGrading) { %>
                <span>Overall Plan Grading:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:DropDownList ID="cboOverallGrading" runat="server" CssClass="cbo" />
                </div>
                <% } %>

                <span>Future Plan Needed:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:RadioButton id="rdoPlanYes" GroupName="FutureVisit" Text="Yes" CssClass="rdo" runat="server" />
                    <asp:RadioButton id="rdoPlanNo" GroupName="FutureVisit" Text="No" CssClass="rdo" runat="server" />
                </div>

                <div id="divFuturePlan" runat="server">
                    <span><% if (string.IsNullOrWhiteSpace(_fugureAssignedUserLabelName)) { %> Future Assigned User <% } else {  %> <%=_fugureAssignedUserLabelName %> <% } %>:</span>
                    <div style="margin: 1px 0px 20px 0px">
                        <asp:DropDownList ID="cboFutureUsers" runat="server" CssClass="cbo" />
                    </div>
                </div>
                                
                <asp:Button ID="btnCloseSubmit" Runat="server" Text="Submit" CssClass="btn" />
                <asp:Button ID="btnCloseCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divDocuments" runat="server" visible="false">
                <span>Generate:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:DropDownList ID="cboDocuments" runat="server" CssClass="cbo" />
                </div>
                <input type="button" id="btnDocumentSelect" value="Generate" class="btn" OnClick="<%= string.Format("GenerateDocument('{0}');", _eServicePlan.ID) %>" />
                <asp:Button ID="btnDocumentCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
        </cc:Box>

        </td>
    </tr>
    </table>
    
    <uc:HintFooter ID="ucHintFooter" runat="server" VerticalOffset="130" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
    function Toggle(id)
    {
        ToggleControlDisplay(id);
    }
    function NewPlan()
    {
        var chk = document.getElementById('rdoPlanYes');
        if (chk.checked) {
            document.getElementById('divFuturePlan').style.display = 'inline';
        }
        else {
            document.getElementById('divFuturePlan').style.display = 'none';
        }
    }
    function SetPrint(objectiveid,object)
    {
    
        AjaxMethodsService.SetPrint(objectiveid, function (result, eventArgs) {});

    }
    function GenerateDocument(servicePlanID) {
        var cboDocuments = document.getElementById('cboDocuments');

        if (cboDocuments.value != null && cboDocuments.value != "") {
            window.open('document.aspx?serviceplanid=' + servicePlanID + '&documentid=' + cboDocuments.value);
            document.location = document.URL;
        }
        else {
            alert("Select a Document to generate.");
        }
    }
    </script>

    <div style="display: none;"><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" alt="" /></div>
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.blockUI.defaults.css = {};
            $.blockUI.defaults.overlayCSS.opacity = .2;

            $('#btnCloseSubmit', this).click(function () {
                if (Page_IsValid) {
                    $.blockUI({ message: '<h3><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" />&nbsp;&nbsp;&nbsp;Processing, one moment please...</h3>' });
                }
            });
        });
    </script>
    </form>
</body>
</html>
