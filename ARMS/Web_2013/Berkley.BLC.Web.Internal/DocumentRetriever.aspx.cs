using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using QCI.Web;
using QCI.ExceptionManagement;
using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using Berkley.Imaging;
using System.IO;

public partial class DocumentRetrieverAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(DocumentRetrieverAspx_Load);
        this.btnSearch.Click += new EventHandler(btnSearch_Click);
        this.grdDocs.ItemDataBound += new DataGridItemEventHandler(grdDocs_ItemDataBound);
        this.btnAttach.Click += new EventHandler(btnAttach_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected Survey _eSurvey;
    protected bool _isExistingSurvey = false;
    protected string _showFileName;
    void DocumentRetrieverAspx_Load(object sender, EventArgs e)
    {
        //Load the survey
        _eSurvey = Survey.Get(ARMSUtility.GetGuidFromQueryString("surveyid", true));
        _isExistingSurvey = (_eSurvey.Location != null && _eSurvey.CreateState != CreateState.InProgress);
         _showFileName = Berkley.BLC.Business.Utility.GetCompanyParameter("ShowFileName", CurrentUser.Company).ToLower();
        if (!this.IsPostBack)
        {
            // populate the policy system dropdown
            CompanyPolicySystem[] policySystems = CompanyPolicySystem.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            cboPolicySystem.DataBind(policySystems, "PolicySystemCompanyID", "PolicySystemCompany.LongAbbreviation");
            cboPolicySystem.Visible = (policySystems.Length > 0);

            FileNetSearchCriteria[] filnetSearchCriteria = null;
            if (_eSurvey.PrimaryPolicy == null)
                filnetSearchCriteria = FileNetSearchCriteria.GetArray("CompanyID = ? ", CurrentUser.CompanyID);
            else
                filnetSearchCriteria = FileNetSearchCriteria.GetArray("CompanyID = ?  && PolicySymbol = ?", CurrentUser.CompanyID,_eSurvey.PrimaryPolicy.Symbol);
            
            
            //pre-populate the client id
            txtClientID.Text = _eSurvey.Insured.ClientID;
            //Default the date to a range of one year
            int duration = 1;

            if (filnetSearchCriteria.Length > 0)
            {
                txtDocType.Text = filnetSearchCriteria[0].DocType;
                duration = filnetSearchCriteria[0].Duration;
            }
            
            drEntryDate.StartDate = DateTime.Today.AddYears(-duration);
            drEntryDate.EndDate = DateTime.Today;
        }
    }

    string Scrub(string strIn)
    {
        return strIn.Replace("'", "").Trim();     //WA-3121, WA-881 Doug Bruce 2015-08-31 - Remove single quote so it won't mess up IBM SQL if user accidentally enters it.
    }

    void btnSearch_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        //WA-881 Doug Bruce 2015-08-31 - Remove single quote so it won't mess up IBM SQL if user accidentally enters it.
        txtPolicyNumber.Text = Scrub(txtPolicyNumber.Text);
        txtClientID.Text = Scrub(txtClientID.Text);
        txtDocID.Text = Scrub(txtDocID.Text);
        txtDocType.Text = Scrub(txtDocType.Text.ToUpper()); //WA-881 Doug Bruce 2015-08-31 - also compare UPPER case doc type

        if (txtPolicyNumber.Text.Trim().Length <= 0 && txtClientID.Text.Trim().Length <= 0 && txtDocID.Text.Trim().Length <= 0)
        {
            JavaScript.ShowMessage(this, "The document id, policy number, or client id must be entered.");
            return;
        }

        boxResults.Visible = true;
        btnAttach.Visible = true;
        btnCancel.Visible = true;

        //It was discovered that not many documents have client ids, therefore if the client id is passed
        //a lookup is performed of all the associated policies and those are used to query the documents
        Company company = (cboPolicySystem.Items.Count > 1) ? Company.Get(new Guid(cboPolicySystem.SelectedValue)) : CurrentUser.Company;
        ImagingHelper imaging = new ImagingHelper(company);
        List<IDocument> docs = new List<IDocument>();

        if (txtDocID.Text.Length > 0)
        {
            //Get the indexes based on the doc id only
            IDocument[] docsRetrieved = imaging.RetrieveFileIndexes(new string[] { }, string.Empty, txtDocID.Text.Trim(), txtDocType.Text.Trim().TrimEnd(',').TrimStart(',').Split(','), drEntryDate.StartDate, drEntryDate.EndDate);
            docs.AddRange(docsRetrieved);
        }
        else if (txtPolicyNumber.Text.Length > 0)
        {
            //Get the indexes based on the policy number(s) only
            IDocument[] docsRetrieved = imaging.RetrieveFileIndexes(txtPolicyNumber.Text.Trim().TrimEnd(',').TrimStart(',').Split(','), string.Empty, string.Empty, txtDocType.Text.Trim().TrimEnd(',').TrimStart(',').Split(','), drEntryDate.StartDate, drEntryDate.EndDate);
            docs.AddRange(docsRetrieved);
        }
        else if (txtClientID.Text.Length > 0)
        {
            //Get the docs based on the client id only
            IDocument[] docsRetrieved = imaging.RetrieveFileIndexes(new string[] { }, txtClientID.Text.Trim(), string.Empty, txtDocType.Text.Trim().TrimEnd(',').TrimStart(',').Split(','), drEntryDate.StartDate, drEntryDate.EndDate);
            docs.AddRange(docsRetrieved);

            //Now check the policy numbers tied to the client
            Survey[] surveys = Survey.GetSortedArray("Insured.ClientID = ? && CompanyID = ? && CreateDate > ?", "CreateDate DESC", txtClientID.Text, CurrentUser.CompanyID, DateTime.Now.AddYears(-2));
            if (surveys.Length > 0)
            {
                Policy[] policies = Policy.GetArray("InsuredID = ?", surveys[0].InsuredID);
                if (policies.Length > 0)
                {
                    List<string> policyNumberList = new List<string>();
                    foreach (Policy policy in policies)
                    {
                        //get the unique list of policies
                        if (!String.IsNullOrEmpty(policy.Number) && !policyNumberList.Contains(policy.Number) && policy.Number.Length > 0)
                        {
                            policyNumberList.Add(policy.Number);
                        }
                    }

                    if (policyNumberList.Count > 0)
                    {
                        docsRetrieved = imaging.RetrieveFileIndexes(policyNumberList.ToArray(), string.Empty, string.Empty, txtDocType.Text.Trim().TrimEnd(',').TrimStart(',').Split(','), drEntryDate.StartDate, drEntryDate.EndDate);
                        docs.AddRange(docsRetrieved);
                    }
                }
            }
        }

        //Finally, make sure the docs are not duplicated
        SortedList docList = new SortedList();
        foreach (IDocument doc in docs)
        {
            //get the unique list of docs
            if (!docList.Contains(doc.DocNumber))
            {
                docList.Add(doc.DocNumber, doc);
            }
        }

        IDocument[] trimmedDocList = new IDocument[docList.Count];
        docList.Values.CopyTo(trimmedDocList, 0);

        //Don't display if over 100 docs were returned
        if (docList.Count > 100)
        {
            JavaScript.ShowMessage(this, "Over 100 documents were found using your search criteria.  Please refine your search.");
            boxResults.Visible = false;
            return;
        }

        string noRecordsMessage = "No documents where found using the search criteria.";
        DataGridHelper.BindGrid(grdDocs, trimmedDocList, "DocNumber", noRecordsMessage);

        boxResults.Title = string.Format("Search Results ({0})", docList.Count);
    }

    void grdDocs_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the document from the grid
            IDocument doc = (IDocument)e.Item.DataItem;

            //Controls
            CheckBox chk = (CheckBox)e.Item.FindControl("chkAttach");
            TextBox txt = (TextBox)e.Item.FindControl("txtFileName");
            LinkButton lnkDownload = (LinkButton)e.Item.FindControl("lnkDownloadDoc");

            chk.Attributes.Add("OnClick", string.Format("AddRemove(this,'{0}','{1}')", doc.DocNumber, txt.ClientID));
            lnkDownload.Attributes.Add("DocNumber", doc.DocNumber);
            lnkDownload.Attributes.Add("MimeType", doc.MimeType);
            lnkDownload.Attributes.Add("PageCount", doc.PageCount.ToString());

            TextBox txtDocRemarks = (TextBox)e.Item.FindControl("txtDocRemarks");
            HiddenField hdnDocType = (HiddenField)e.Item.FindControl("hdnDocType");
            txtDocRemarks.Text = Server.HtmlDecode(doc.DocRemarks);

            //Try to provide a good suggested filename for attaching to Audit.
            //Priority:
            //1 - DocRemarks
            //2 - FileName
            //3 - ID (guid)
            if (doc.DocRemarks != null)
            {
                txt.Text = Server.HtmlDecode(doc.DocRemarks);
            }
            else
            {
                if (doc.FileName != null)
                {
                    txt.Text = Path.GetFileNameWithoutExtension(Server.HtmlDecode(doc.FileName));
                }
                else
                {
                    txt.Text = Server.HtmlDecode(doc.Id);
                }
            }

            if (_showFileName == "true")
            {
                string pattern = "[\\*/:<>?|\"-]";
                string replacement = " ";
                Regex regEx = new Regex(pattern);
                txt.Text = Regex.Replace(regEx.Replace(txt.Text, replacement), @"\s+", " ");
            }
            hdnDocType.Value = doc.DocType;
        }
    }

    protected void lnkDownloadDoc_OnClick(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            StreamDocument docRetriever = new StreamDocument(lnk.Attributes["DocNumber"], lnk.Attributes["MimeType"], int.Parse(lnk.Attributes["PageCount"]), CurrentUser.Company);
            docRetriever.AttachDocToResponse(this, docRetriever.DocMimeType);   //WA-621 Doug Bruce 2015-05-05 - Added MimeType for correct display.
        }
        catch (System.Web.HttpException ex)
        {
            //ignore http exceptions for when the user quits a doc download and the stream doesn't get flushed
            if (ex.Message.Contains("The remote host closed the connection"))
            {
                //swallow exception
            }
            else
            {
                throw new Exception("See inner exception for details.", ex);
            }
        }
    }

    void btnAttach_Click(object sender, EventArgs e)
    {
        //Do a quick check to make sure at least on doc was selected
        string[] docNumbers = hdnDocNumbers.Value.TrimEnd('|').Split('|');
        if (docNumbers.Length <= 0 || docNumbers[0].Length <= 0)
        {
            JavaScript.ShowMessage(this, "No documents were selected.");
            return;
        }

        //Do a quick check to make sure all the docs selected have a name
        foreach (DataGridItem item in grdDocs.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox chk = (CheckBox)item.FindControl("chkAttach");
                TextBox txt = (TextBox)item.FindControl("txtFileName");

                if (chk.Checked && txt.Text.Trim().Length <= 0)
                {
                    JavaScript.ShowMessage(this, "A name must be entered for each selected document.");
                    return;
                }
            }
        }

        try
        {
            //Got this far, good to go
            foreach (DataGridItem item in grdDocs.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chk = (CheckBox)item.FindControl("chkAttach");
                    TextBox txt = (TextBox)item.FindControl("txtFileName");
                    TextBox txtDocRemarks = (TextBox)item.FindControl("txtDocRemarks");
                    HiddenField hdnDocType = (HiddenField)item.FindControl("hdnDocType");

                    if (chk.Checked)
                    {
                        if (txt.Text.Trim().Length <= 0)
                        {
                            JavaScript.ShowMessage(this, "A name must be entered for each selected document.");
                            return;
                        }

                        if (txt.Text.Contains("\\") || txt.Text.Contains("/") || txt.Text.Contains(":") || 
                            txt.Text.Contains("*") || txt.Text.Contains("?") || txt.Text.Contains("\"") || 
                            txt.Text.Contains("<") || txt.Text.Contains(">") || txt.Text.Contains("|"))
                        {
                            JavaScript.ShowMessage(this, "A name cannot contain any of the following characters:\r \\\\ / : * ? \" < > |");
                            return;
                        }

                        LinkButton lnk = (LinkButton)item.FindControl("lnkDownloadDoc");
                        string docNumber = lnk.Attributes["DocNumber"];
                        string mimeType = lnk.Attributes["MimeType"];
                        int pageCount = int.Parse(lnk.Attributes["PageCount"]);

                        StreamDocument docRetriever = new StreamDocument(docNumber, mimeType, pageCount, CurrentUser.Company);
                        System.IO.Stream stream = docRetriever.DocumentStream;
                        mimeType = docRetriever.DocMimeType;

                        //set the name of the file with the extension
                        string fileName = txt.Text + StreamDocument.GetExtension(mimeType, false, string.Empty);

                        SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser, !_isExistingSurvey);
                        //manager.AttachDocument(fileName, mimeType, stream, hdnDocType.Value, txtDocRemarks.Text, true);

                        manager.AttachDocument(docNumber, fileName, mimeType, stream, txtDocRemarks.Text);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //ExceptionManager.Publish(ex);
            throw (ex);
        }

        Response.Redirect(GetReturnURL(), true);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnURL(), true);
    }

    private string GetReturnURL()
    {
        if (_isExistingSurvey)
        {
            return string.Format("Survey.aspx{0}", ARMSUtility.GetQueryStringForNav());
        }
        else
        {
            return string.Format("CreateSurvey.aspx?surveyid={0}", _eSurvey.ID);
        }
    }
}
