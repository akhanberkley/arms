<%@ Page Language="C#" AutoEventWireup="false" CodeFile="Document.aspx.cs" Inherits="DocumentAspx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body onload="document.frm.submit(); ResetSrc('<%= ARMSUtility.AppPath %>/images/rotation.gif');">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    
    <p style="margin-top:200px" align="center">
        <span>Retrieving Document...</span><br />
        <img id="imgRetrievingDoc" visible="false" />
    </p>
    
    </form>
    <script language="javascript">
    function ResetSrc(path)
    {
	    document.getElementById("imgRetrievingDoc").src = path;
    }
    </script>
</body>
</html>
