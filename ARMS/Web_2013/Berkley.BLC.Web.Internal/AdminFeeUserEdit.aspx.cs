using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web;
using QCI.Web.Validation;

public partial class AdminFeeUserEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminFeeUserEditAspx_Load);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected FeeUser _user;
    protected Guid _feeCompanyUserID;

    void AdminFeeUserEditAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("id", false);
        _user = (id != Guid.Empty) ? FeeUser.Get(id) : null;

        // get the company id, we'll need it during save and cancel
        _feeCompanyUserID = (_user != null) ? _user.FeeCompanyUserID : ARMSUtility.GetGuidFromQueryString("cid", true);

        if (!this.IsPostBack)
        {
            if (_user != null) // edit
            {
                this.PopulateFields(_user);
                txtPassword.Label = (_user == null) ? "Password" : "New Password";
                txtPassword.IsRequired = (_user == null);
            }

            btnSubmit.Text = (_user == null) ? "Add" : "Update";
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            FeeUser user;
            if (_user == null) // add
            {
                user = new FeeUser(Guid.NewGuid());
                user.FeeCompanyUserID = _feeCompanyUserID;
            }
            else
            {
                user = _user.GetWritableInstance();
            }

            this.PopulateEntity(user);

            // username must be unique across the entire database
            FeeUser dupTest = FeeUser.GetOne("Username = ? && ID != ?", user.Username, user.ID);
            if (dupTest != null)
            {
                throw new FieldValidationException(txtUsername.InputControl, "The username entered is already being used by another user.  Please enter a different one.");
            }

            // set the user's password if one is provided (required during add; optional during edit)
            string newPassword = txtPassword.Text;
            if (newPassword.Length > 0)
            {
                // passwords must be at least six chars
                if (newPassword.Length < 6)
                {
                    throw new FieldValidationException(txtPassword.InputControl, "Passwords must be at least six characters in length.");
                }

                user.PasswordHash = user.CreatePasswordHash(txtPassword.Text);
            }

            // commit changes
            user.Save();

            // update our member var so the redirect will work correctly
            _user = user;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl());
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        return "AdminFeeUsers.aspx?id=" + _feeCompanyUserID;
    }

}
