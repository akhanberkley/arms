using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.ExceptionManagement;
using QCI.Web;

public partial class SurveysAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
        BuildDataGrid();
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(SurveysAspx_Load);
        this.PreRender += new EventHandler(SurveysAspx_PreRender);
        this.grdItems.PageIndexChanged += new DataGridPageChangedEventHandler(grdItems_PageIndexChanged);
        this.grdItems.SortCommand += new DataGridSortCommandEventHandler(grdItems_SortCommand);
        this.grdItems.ItemDataBound += new DataGridItemEventHandler(grdItems_ItemDataBound);
        this.btnUpdate.Command += new CommandEventHandler(btnUpdate_Command);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.ddlItemsPerPage.SelectedIndexChanged += new EventHandler(ddlItemsPerPage_SelectedIndexChanged);
        this.btnExport.Click += new EventHandler(btnExport_Click);
        this.cboDueDateDays.SelectedIndexChanged += new EventHandler(cboDueDateDays_SelectedIndexChanged);
        this.lnkMapSelectedLocations.Click += new EventHandler(lnkMapSelectedLocations_Click); 
    }
    #endregion

    protected string _itemClass; // used during HTML rendering
    private string strKey;
    private UserSearch _eUserSearch;
    private User _eFeeCompany;
    private MergeDocument _eLetter;
    private SurveyReviewQueue _reviewQueue;
    protected int _recordCount = 0;
    protected bool _mapSelectedLocations;

    void SurveysAspx_Load(object sender, EventArgs e)
    {
        /* 
         * This page could be invoked from the following options:
         * 1) User defined search from MySurveys.aspx,
         * 2) A letter reminder queue from MySurveys.aspx,
         * 3) A queue from CompanySummary.aspx
         * 4) Returning from a survey, Survey.aspx
         * 5) Underwriter queues, MySurveys.aspx
        */
        Guid gSearchID = ARMSUtility.GetGuidFromQueryString("searchid", false);
        Guid gFeeCompanyID = ARMSUtility.GetGuidFromQueryString("userid", false);
        Guid gLetterID = ARMSUtility.GetGuidFromQueryString("letterid", false);
        Guid reviewQueueID = ARMSUtility.GetGuidFromQueryString("reviewqueueid", false);
        string strFilter = ARMSUtility.GetStringFromQueryString("filter", false);
        string strUWFilter = ARMSUtility.GetStringFromQueryString("uwfilter", false);

        if (gSearchID != Guid.Empty)
        {
            _eUserSearch = UserSearch.Get(gSearchID);
            strKey = gSearchID.ToString();
        }
        else if (gFeeCompanyID != Guid.Empty)
        {
            _eFeeCompany = Berkley.BLC.Entities.User.Get(gFeeCompanyID);
            strKey = gFeeCompanyID.ToString();
        }
        else if (gLetterID != Guid.Empty)
        {
            _eLetter = MergeDocument.Get(gLetterID);
            strKey = gLetterID.ToString();
        }
        else if (reviewQueueID != Guid.Empty)
        {
            _reviewQueue = SurveyReviewQueue.Get(reviewQueueID);
            strKey = _reviewQueue.ToString();
        }
        else if (strFilter != null && strFilter.Length > 0)
        {
            strKey = strFilter;
        }
        else if (strUWFilter != null && strUWFilter.Length > 0)
        {
            strKey = strUWFilter;
        }
        else
        {
            //If we get here, redirect their default page.
            string page = ARMSUtility.GetDefaultPageUrl(CurrentUser);
            Response.Redirect(page, true);
        }

        if (!this.IsPostBack)
        {
            divReminder.Visible = false;

            // restore sort expression, reverse state, and page index settings
            this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting(strKey + "SortExpression", "DueDate");
            this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting(strKey + "SortReversed", true);
            this.grdItems.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting(strKey + "PageIndex", 0);

        }
    }

    void SurveysAspx_PreRender(object sender, EventArgs e)
    {
        this.grdItems.PageSize = (int)this.UserIdentity.GetPageSetting(strKey + "PageCount", 20);
        this.ddlItemsPerPage.SelectedValue = this.grdItems.PageSize.ToString();
        this.cboDueDateDays.SelectedValue = (string)this.UserIdentity.GetPageSetting(strKey + "DueDateDays", string.Empty);

        string noRecordsMessage = "No surveys found.";
        string sortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sortExp = sortExp.Replace(" DESC", " TEMP");
            sortExp = sortExp.Replace(" ASC", " DESC");
            sortExp = sortExp.Replace(" TEMP", " ASC");
        }

        VWSurvey[] eSurveys;
        string strQuery = string.Empty;
        ArrayList args = new ArrayList();

        if (_eUserSearch != null) //User Search from MySummary.aspx
        {
            string searchFilter = _eUserSearch.FilterExpression;
            string[] strUserSearchArgs = _eUserSearch.FilterParameters.Split('~');
            if (cboDueDateDays.SelectedValue != string.Empty)
            {
                searchFilter += " && DueDate <= ?";
                strUserSearchArgs = string.Format("{0}~{1}", _eUserSearch.FilterParameters, DateTime.Today.AddDays(Convert.ToDouble(cboDueDateDays.SelectedValue))).Split('~');
            }

            eSurveys = VWSurvey.GetSortedArray(searchFilter, sortExp, strUserSearchArgs);
            base.PageHeading = _eUserSearch.Name;

            //log large viewd saved searches
            int maxCount = int.Parse(ConfigurationManager.AppSettings["SearchResultThreshold"]);
            if (eSurveys.Length > maxCount)
            {
                ExceptionManager.Publish(new Exception(string.Format("WARNING: A user viewed a saved search that contained over {0} results. \nUser: {1} \nCompany: {2} \nResult Count: {3} \nSaved Search ID: {4}", maxCount, CurrentUser.Name, CurrentUser.Company.Abbreviation, eSurveys.Length, _eUserSearch.ID)));
            }
        }
        else if (_eFeeCompany != null) //Vendor Queue from CompanySummary.aspx
        {
            strQuery = "ISNULL(CompleteDate) && CreateStateCode != ? && CompanyID = ? && AssignedUserID = ? && (SurveyStatusCode = ? || SurveyStatusCode = ? || SurveyStatusCode = ?)";
            args.Add(CreateState.InProgress.Code);
            args.Add(CurrentUser.CompanyID);
            args.Add(_eFeeCompany.ID);
            args.Add(SurveyStatus.AssignedSurvey.Code);
            args.Add(SurveyStatus.AwaitingAcceptance.Code);
            args.Add(SurveyStatus.ReturningToCompany.Code);

            if (cboDueDateDays.SelectedValue != string.Empty)
            {
                strQuery += " && DueDate <= ?";
                args.Add(DateTime.Today.AddDays(Convert.ToDouble(cboDueDateDays.SelectedValue)));
            }

            eSurveys = VWSurvey.GetSortedArray(strQuery, sortExp, args.ToArray());
            noRecordsMessage = "No assigned surveys currently exist.";
            base.PageHeading = _eFeeCompany.Name;
        }
        else if (_eLetter != null) //Letter Reminder from MySummary.aspx
        {
            Guid gUserID = ARMSUtility.GetGuidFromQueryString("letteruserid", true);
            Guid gPriorLetterID = ARMSUtility.GetGuidFromQueryString("priorletterid", true);

            SelectProcedure sp = new SelectProcedure(typeof(VWSurvey), "Letter_GetReminders");
            sp.AddParameter("@CompanyID", CurrentUser.CompanyID);
            sp.AddParameter("@UserID", gUserID);
            sp.AddParameter("@PriorLetterID", gPriorLetterID);
            sp.AddParameter("@CurrentLetterID", _eLetter.ID);
            sp.AddParameter("@ReturnType", 1);

            IList list = DB.Engine.GetObjectSet(sp);
            Array result = Array.CreateInstance(sp.ObjectType, list.Count);
            list.CopyTo(result, 0);

            eSurveys = result as VWSurvey[];
            noRecordsMessage = "No surveys with letters needing to be generated currently exist.";
            base.PageHeading = _eLetter.Name;
        }
        else if (_reviewQueue != null)
        {
            IList list = DB.Engine.GetObjectSet(typeof(VWSurvey), _reviewQueue.ResultSetQuery);
            Array result = Array.CreateInstance(typeof(VWSurvey), list.Count);
            list.CopyTo(result, 0);

            eSurveys = result as VWSurvey[];

            noRecordsMessage = "No surveys.";
            base.PageHeading = _reviewQueue.Name;
        }
        else if (strKey != null && strKey.Length > 0 && CurrentUser.IsUnderwriter)//Underwriter queues
        {
            strQuery = "CreateStateCode != ? && CompanyID = ? && (CreateByExternalUserName = ? || UnderwriterCode = ?)";
            args.Add(CreateState.InProgress.Code);
            args.Add(CurrentUser.CompanyID);
            args.Add(UserIdentity.Name);
            args.Add(UserIdentity.Initials);

            switch (strKey)
            {
                case "OHSU": // On Hold/ Unassigned Surveys
                    {
                        strQuery += " && (SurveyStatusCode = ? || SurveyStatusCode = ? || SurveyStatusCode = ?)";
                        args.Add(SurveyStatus.OnHoldSurvey.Code);
                        args.Add(SurveyStatus.UnassignedSurvey.Code);
                        args.Add(SurveyStatus.AwaitingAcceptance.Code);
                        noRecordsMessage = "No surveys currently on hold or unassigned.";
                        base.PageHeading = "On Hold / Unassigned";
                        break;
                    }
                case "SA": // Assigned To Consultant Surveys
                    {
                        strQuery += " && SurveyStatusCode = ?";
                        args.Add(SurveyStatus.AssignedSurvey.Code);
                        noRecordsMessage = "No surveys currently assigned to a consultant.";
                        base.PageHeading = "Assigned To Consulant";
                        break;
                    }
                case "FA": // Assigned To Reviewer Surveys
                    {
                        strQuery += " && SurveyStatusCode = ?";
                        args.Add(SurveyStatus.AssignedReview.Code);
                        noRecordsMessage = "No surveys currently assigned to a reviewer.";
                        base.PageHeading = "Assigned To Reviewer";
                        break;
                    }
                case "SC": // Closed Surveys
                    {
                        strQuery += " && SurveyStatusCode = ? && CompleteDate >= ?";
                        args.Add(SurveyStatus.ClosedSurvey.Code);
                        args.Add(DateTime.Today.AddMonths(-12));
                        noRecordsMessage = "No surveys currently closed.";
                        base.PageHeading = "Complete";
                        break;
                    }
                case "CC": // Canceled Surveys
                    {
                        strQuery += " && SurveyStatusCode = ? && CanceledDate >= ?";
                        args.Add(SurveyStatus.CanceledSurvey.Code);
                        args.Add(DateTime.Today.AddMonths(-12));
                        noRecordsMessage = "No surveys currently canceled.";
                        base.PageHeading = "Canceled";
                        break;
                    }
                case "UK": // Unacknowledged Surveys
                    {
                        strQuery += " && (SurveyStatusCode = ? || SurveyStatusCode = ? || SurveyStatusCode = ?) && ReportCompleteDate >= ? && ISNULL(UnderwritingAcceptDate)";
                        args.Add(SurveyStatus.AssignedReview.Code);
                        args.Add(SurveyStatus.UnassignedReview.Code);
                        args.Add(SurveyStatus.ClosedSurvey.Code);
                        args.Add(DateTime.Today.AddMonths(-12));
                        noRecordsMessage = "No surveys currently unacknowledged.";
                        base.PageHeading = "Unacknowledged";
                        break;
                    }
                case "PD": // Past Due
                    {
                        strQuery += " && ISNULL(ReportCompleteDate) && (SurveyStatusCode != ? && SurveyStatusCode != ?) && DueDate < ?";
                        args.Add(SurveyStatus.ClosedSurvey.Code);
                        args.Add(SurveyStatus.CanceledSurvey.Code);
                        args.Add(DateTime.Today);
                        noRecordsMessage = "No surveys currently open and past due.";
                        base.PageHeading = "Open & Past Due";
                        break;
                    }
                case "ALL": // All Surveys
                    {
                        noRecordsMessage = "No surveys.";
                        base.PageHeading = "All Surveys";
                        break;
                    }
                default:
                    {
                        throw new Exception("Not expecting key '" + strKey + "'.");
                    }
            }
            eSurveys = VWSurvey.GetSortedArray(strQuery, sortExp, args.ToArray());

        }
        else if (strKey != null && strKey.Length > 0)//Other Queues from CompanySummary.aspx
        {
            strQuery = "CreateStateCode != ? && CompanyID = ?";
            args.Add(CreateState.InProgress.Code);
            args.Add(CurrentUser.CompanyID);

            if (cboDueDateDays.SelectedValue != string.Empty)
            {
                strQuery += " && DueDate <= ?";
                args.Add(DateTime.Today.AddDays(Convert.ToDouble(cboDueDateDays.SelectedValue)));
            }

            switch (strKey)
            {
                case "US": // Unassigned Surveys
                    {
                        strQuery += " && ISNULL(CompleteDate) && ISNULL(AssignedUserID) && ISNULL(AssignDate) && SurveyStatusTypeCode = ? && SurveyStatusCode = ?";
                        args.Add(SurveyStatusType.Survey.Code);
                        args.Add(SurveyStatus.UnassignedSurvey.Code);
                        noRecordsMessage = "No unassigned surveys currently exist.";
                        base.PageHeading = "Unassigned Surveys";
                        break;
                    }
                case "RS": // Returned Surveys
                    {
                        strQuery += " && ISNULL(CompleteDate) && ISNULL(AssignedUserID) && (!ISNULL(AssignDate) || !ISNULL(ReassignDate))  && SurveyStatusTypeCode = ? && SurveyStatusCode = ?";
                        args.Add(SurveyStatusType.Survey.Code);
                        args.Add(SurveyStatus.UnassignedSurvey.Code);
                        noRecordsMessage = "No returned surveys currently exist.";
                        base.PageHeading = "Returned Surveys";
                        break;
                    }
                case "UR": // Unassigned Review
                    {
                        strQuery += " && ISNULL(AssignedUserID) && SurveyStatusTypeCode = ? && SurveyStatusCode = ?";
                        args.Add(SurveyStatusType.Review.Code);
                        args.Add(SurveyStatus.UnassignedReview.Code);
                        noRecordsMessage = "No unassigned reviews currently exist.";
                        base.PageHeading = "Unassigned Reviews";
                        break;
                    }
                case "OH": // On Hold
                    {
                        strQuery += " && ISNULL(CompleteDate) && SurveyStatusCode = ?";
                        args.Add(SurveyStatus.OnHoldSurvey.Code);
                        noRecordsMessage = "No surveys currently on hold.";
                        base.PageHeading = "On Hold";
                        break;
                    }
                default:
                    {
                        throw new Exception("Not expecting key '" + strKey + "'.");
                    }
            }
            eSurveys = VWSurvey.GetSortedArray(strQuery, sortExp, args.ToArray());
        }
        else
        {
            throw new Exception("No specifiers in the querystring.");
        }

        _recordCount = eSurveys.Length;
        grdItems.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindPagingGrid(grdItems, eSurveys, null, noRecordsMessage);
        this.ViewState["SearchKey"] = strKey;
        this.ViewState["SearchResults"] = eSurveys;

        SetMappingURL(eSurveys);
    }

    private void SetMappingURL(VWSurvey[] surveys)
    {
        //get the list of unique location zip codes by consultant
        List<string> surveyNumbers = new List<string>();
        foreach (VWSurvey survey in surveys)
        {
            surveyNumbers.Add(survey.SurveyNumber.ToString());
        }

        //Modified By Vilas Meshram: 05/07/2013 : Quish #21376: Use OVI maps : 
        lnkMapLocations.HRef = string.Format("mapOvi.aspx?name={0}&surveys={1}", HttpUtility.UrlEncode(base.PageHeading), String.Join(",", surveyNumbers.ToArray()));
        //lnkMapLocations.HRef = string.Format("map.aspx?name={0}&surveys={1}", HttpUtility.UrlEncode(base.PageHeading), String.Join(",", surveyNumbers.ToArray()));
        lnkMapLocations.Visible = (surveys.Length > 0);

        int maxSurveyCount = int.Parse(ConfigurationManager.AppSettings["MaxSurveyCountToMap"]);
        if (surveys.Length > maxSurveyCount)
        {
            lnkMapLocations.Disabled = true;
            lnkMapLocations.HRef = string.Empty;
        }
        else
        {
            lnkMapLocations.Disabled = false;
        }

    }

    void grdItems_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdItems.CurrentPageIndex = e.NewPageIndex;
        divReminder.Visible = false;

        this.UserIdentity.SavePageSetting(ViewState["SearchKey"] + "PageIndex", e.NewPageIndex);
    }

    void cboDueDateDays_SelectedIndexChanged(object sender, EventArgs e)
    {
        divReminder.Visible = false;

        this.UserIdentity.SavePageSetting(ViewState["SearchKey"] + "DueDateDays", cboDueDateDays.SelectedValue);
    }

    void grdItems_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        divReminder.Visible = false;

        this.UserIdentity.SavePageSetting(ViewState["SearchKey"] + "SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting(ViewState["SearchKey"] + "SortReversed", sortRev);
    }

    void ddlItemsPerPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList list = (DropDownList)sender;
        this.UserIdentity.SavePageSetting(ViewState["SearchKey"] + "PageCount", Convert.ToInt32(list.SelectedValue));
    }

    void grdItems_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // check the survey's reminder status
            VWSurvey eSurvey = (VWSurvey)e.Item.DataItem;
            ImageButton btn = (ImageButton)e.Item.FindControl("btnReminder");

            if (eSurvey.ReminderDate > DateTime.MinValue)
            {
                //determine which icon to display
                btn.ImageUrl = (eSurvey.ReminderDate > DateTime.Today) ? ARMSUtility.AppPath + "/images/clock.gif" : ARMSUtility.AppPath + "/images/xmark.gif";
                btn.AlternateText = string.Format("{0}: {1}", eSurvey.ReminderDate.ToShortDateString(), (eSurvey.ReminderComment.Length > 0) ? eSurvey.ReminderComment : "(no comments)");
            }
            else
                btn.ImageUrl = ARMSUtility.AppPath + "/images/dash.gif";
        }
    }

    protected void btnUpdate_Command(object sender, CommandEventArgs e)
    {
        //validation
        if (txtReminderComment.Text.Length > 500)
        {
            JavaScript.ShowMessage(this, "Reminder comment cannot be more than 500 characters.");
            return;
        }
        else
        {
            //save the reminder date
            Guid id = new Guid(e.CommandName);
            Survey eSurvey = Survey.Get(id).GetWritableInstance();

            eSurvey.ReminderDate = dateReminder.Date;
            eSurvey.ReminderComment = txtReminderComment.Text;

            // commit all changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                eSurvey.Save(trans);
                trans.Commit();
            }

            divReminder.Visible = false;
        }
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        divReminder.Visible = false;
    }

    void btnExport_Click(object sender, EventArgs e)
    {
        VWSurvey[] surveys = (VWSurvey[])this.ViewState["SearchResults"];
        ExportItemList oExportDataList = (ExportItemList)this.ViewState["Exports"];

        if (surveys == null || oExportDataList == null)
        {
            JavaScript.ShowMessage(this, "The session has timed out.  Please try again.");
            return;
        }

        string strErrorMessage = "No Surveys Found";
        object oSavedText = this.ViewState["EmptyExportText"];
        if (oSavedText != null)
        {
            strErrorMessage = oSavedText.ToString();
        }

        // Get the CSV data
        string strCsv = oExportDataList.GetCsv(surveys, oExportDataList, strErrorMessage);

        Response.ContentType = "application/x-msexcel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=survey.csv");

        // Write out the CSV data
        System.Text.ASCIIEncoding oEncoding = new System.Text.ASCIIEncoding(); ;
        byte[] rbBytes = oEncoding.GetBytes(strCsv);
        Response.OutputStream.Write(rbBytes, 0, rbBytes.Length);

        Response.End();
    }

    void lnkMapSelectedLocations_Click(object sender, EventArgs e)
    {
        List<string> surveyNumbers = new List<string>();

        foreach (DataGridItem dataGridItem in grdItems.Items)
        {
            if (((CheckBox)dataGridItem.FindControl("ChkMap")).Checked)
            {
                string sSurveyNumber = ((HyperLink)dataGridItem.FindControl("lnkSurvey")).Text.ToString();
                surveyNumbers.Add(sSurveyNumber);
            }
        }

        Response.Redirect(string.Format("mapOvi.aspx?name={0}&surveys={1}", HttpUtility.UrlEncode(base.PageHeading), String.Join(",", surveyNumbers.ToArray())));

    }
  

    private void BuildDataGrid()
    {
        ExportItemList oExportList = new ExportItemList(this);

        //Get the columns by order from the DB
        GridColumnFilterCompany[] filterCols = GridColumnFilterCompany.GetSortedArray("CompanyID = ? && IsExternal = 0", "Display DESC, DisplayOrder ASC", CurrentUser.CompanyID);

        foreach (GridColumnFilterCompany column in filterCols)
        {
            string columnName = (column.GridColumnFilter.Name == "Call<br>Count") ? CurrentUser.Company.CallCountLabel : column.GridColumnFilter.Name;
            if (column.Display)
            {
                TemplateColumn template = new TemplateColumn();
                template.HeaderText = columnName;
                template.SortExpression = column.GridColumnFilter.SortExpression;
                template.ItemTemplate = new MyTemplate(column.GridColumnFilter);
                template.ItemStyle.HorizontalAlign = (column.GridColumnFilter.ID == GridColumnFilter.Reminder.ID) ? HorizontalAlign.Center : HorizontalAlign.Left;

                grdItems.Columns.Add(template);

                if (columnName.ToLower().Trim() == "map")
                    _mapSelectedLocations = true;

            }

            if (this.IsPostBack)
            {
                if (columnName.ToLower().Trim() != "map")
                {
                    // Grab the field names for Excel export
                    string strFilterName = column.GridColumnFilter.FilterExpression == null || column.GridColumnFilter.FilterExpression.Length == 0 ? "" : column.GridColumnFilter.FilterExpression.Replace("\r\n", " ").Trim();
                    string strSortName = column.GridColumnFilter.SortExpression == null || column.GridColumnFilter.SortExpression.Length == 0 ? "" : column.GridColumnFilter.SortExpression.Substring(0, column.GridColumnFilter.SortExpression.IndexOf(" "));
                    string strDataType = column.GridColumnFilter.DotNetDataType == null || column.GridColumnFilter.DotNetDataType.Length == 0 ? "" : column.GridColumnFilter.DotNetDataType.Replace("\r\n", " ").Trim();
                    oExportList.Add(new ExportItem(column.GridColumnFilter.ID, strSortName, strFilterName, strDataType, columnName, column.DisplayOrder));
                }
            }
        }

        //oExportList.Sort();
        this.ViewState["Exports"] = oExportList;
    }

    private class MyTemplate : ITemplate
    {
        private string expression;
        private GridColumnFilter filter;
        public MyTemplate(GridColumnFilter filter)
        {
            this.filter = filter;
            this.expression = filter.FilterExpression;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            //figure out the type of control to bind
            if (filter.ID == GridColumnFilter.Reminder.ID)
            {
                ImageButton btnReminder = new ImageButton();
                btnReminder.ID = "btnReminder";
                btnReminder.DataBinding += new EventHandler(this.BindImageButton);
                btnReminder.Command += new CommandEventHandler(btnReminder_Command);
                container.Controls.Add(btnReminder);
            }
            else if (filter.ID == GridColumnFilter.SurveyLink.ID)
            {
                HyperLink link = new HyperLink();
                link.ID = "lnkSurvey";

                link.DataBinding += new EventHandler(this.BindHyperLink);
                container.Controls.Add(link);
            }
            else if (filter.ID == GridColumnFilter.InsuredLink.ID)
            {
                HyperLink link = new HyperLink();
                link.ID = "lnkInsured";
                link.DataBinding += new EventHandler(this.BindHyperLink);
                container.Controls.Add(link);
            }
            else if (filter.ID == GridColumnFilter.MapCheckBox.ID)
            {
                CheckBox chkMap = new CheckBox();
                chkMap.ID = "chkMap";
                container.Controls.Add(chkMap);
            }
            else
            {
                Literal l = new Literal();
                l.DataBinding += new EventHandler(this.BindData);
                container.Controls.Add(l);
            }
        }

        public void BindData(object sender, EventArgs e)
        {
            Literal l = (Literal)sender;
            DataGridItem container = (DataGridItem)l.NamingContainer;

            switch (filter.SystemTypeCode)
            {
                case TypeCode.DateTime:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:d}", DateTime.MinValue, "(none)");
                    break;
                case TypeCode.Decimal:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:C}", Decimal.MinValue, "(none)");
                    break;
                case TypeCode.Int64:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:N2}", Decimal.MinValue, "(none)");
                    break;
                case TypeCode.Int32:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:N0}", Int32.MinValue, "(none)");
                    break;
                default:
                    if (expression.ToLower().Equals("priority"))
                    {
                        var d = (VWSurvey)container.DataItem;
                        string imgName = !string.IsNullOrEmpty(d.Priority) ? d.Priority : "./Images/colors/none.png";
                        var imgHtml =
                            string.Format(
                                " <img id='imgPriority' src='{0}'  height='20' width='20' />",
                                imgName);
                        l.Text = imgHtml;
                    }
                    else
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, string.Empty, "(none)");
                    break;
            }
        }

        public void BindImageButton(object sender, EventArgs e)
        {
            ImageButton btnReminder = (ImageButton)sender;
            DataGridItem container = (DataGridItem)btnReminder.NamingContainer;
            btnReminder.CommandName = HtmlEncode(container.DataItem, "SurveyID");
        }

        public void BindHyperLink(object sender, EventArgs e)
        {
            HyperLink link = (HyperLink)sender;
            DataGridItem container = (DataGridItem)link.NamingContainer;
            link.Text = HtmlEncodeNullable(container.DataItem, expression, string.Empty, "(none)");

            if (link.ID == "lnkSurvey")
            {
                link.NavigateUrl = string.Format("Survey.aspx?surveyid={0}&{1}", HtmlEncode(container.DataItem, "SurveyID"), HttpContext.Current.Request.QueryString.ToString());
            }
            else
            {
                link.NavigateUrl = string.Format("Insured.aspx?surveyid={0}&locationid={1}&{2}", HtmlEncode(container.DataItem, "SurveyID"), HtmlEncode(container.DataItem, "LocationID"), HttpContext.Current.Request.QueryString.ToString());
            }
        }

        private void btnReminder_Command(object sender, CommandEventArgs e)
        {
            //Get instances of the controls
            ImageButton btn = (ImageButton)sender;
            HtmlGenericControl divReminder = (HtmlGenericControl)btn.Page.FindControl("divReminder");

            Guid gSurveyID = new Guid(e.CommandName);
            Survey eSurvey = Survey.Get(gSurveyID).GetWritableInstance();

            //check if this reminder has already expired
            //if so, clear out the date, otherwise diplay the reminder field
            if (eSurvey.ReminderDate > DateTime.MinValue && eSurvey.ReminderDate <= DateTime.Today)
            {
                eSurvey.ReminderDate = DateTime.MinValue;
                eSurvey.ReminderComment = string.Empty;

                // commit all changes
                using (Transaction trans = DB.Engine.BeginTransaction())
                {
                    eSurvey.Save(trans);
                    trans.Commit();
                }

                divReminder.Visible = false;
            }
            else
            {
                //Get instances of the controls
                Berkley.BLC.Web.Internal.Controls.Box boxReminder = (Berkley.BLC.Web.Internal.Controls.Box)btn.Page.FindControl("boxReminder");
                Button btnUpdate = (Button)btn.Page.FindControl("btnUpdate");
                Berkley.BLC.Web.Internal.Fields.DateField dateReminder = (Berkley.BLC.Web.Internal.Fields.DateField)btn.Page.FindControl("dateReminder");
                Berkley.BLC.Web.Internal.Fields.TextField txtReminderComment = (Berkley.BLC.Web.Internal.Fields.TextField)btn.Page.FindControl("txtReminderComment");

                //Set the values
                boxReminder.Title = string.Format("Reminder for Survey #{0}", eSurvey.Number);
                divReminder.Visible = true;
                btnUpdate.CommandName = e.CommandName;
                dateReminder.Date = eSurvey.ReminderDate;
                txtReminderComment.Text = eSurvey.ReminderComment;

                JavaScript.SetInputFocus(btnUpdate);
            }
        }

    }
}
