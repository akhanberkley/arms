using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web;

public partial class AdminServicePlanFieldsAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminServicePlanFieldsAspx_Load);
        this.PreRender += new EventHandler(AdminServicePlanFieldsAspx_PreRender);
        this.grdFields.ItemCommand += new DataGridCommandEventHandler(grdFields_ItemCommand);
    }
    #endregion

    void AdminServicePlanFieldsAspx_Load(object sender, EventArgs e)
    {
    }

    void AdminServicePlanFieldsAspx_PreRender(object sender, EventArgs e)
    {
        // get the company specific recs
        DynamicField[] eFields = DynamicField.GetSortedArray("CompanyID = ? && Disabled = ?", "PriorityIndex ASC", this.CurrentUser.CompanyID, chkShowDisabled.Checked);

        string noRecordsMessage = string.Format("There are no {0} service plan fields for your company.", (!chkShowDisabled.Checked) ? "active" : "disabled");
        DataGridHelper.BindPagingGrid(grdFields, eFields, "ID", noRecordsMessage);

        if (chkShowDisabled.Checked)
        {
            for (int i = 0; i < grdFields.Columns.Count; i++)
            {
                DataGridColumn col = grdFields.Columns[i];
                col.Visible = (col.HeaderText.ToUpper() != "MOVE");
            }
        }
        else
        {
            for (int i = 0; i < grdFields.Columns.Count; i++)
            {
                DataGridColumn col = grdFields.Columns[i];
                col.Visible = true;
            }
        }
    }

    void grdFields_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid fieldID = (Guid)grdFields.DataKeys[e.Item.ItemIndex];

        // get the current fields from the DB and find the index of the target columns
        DynamicField[] fields = DynamicField.GetSortedArray("CompanyID = ? && Disabled = false", "PriorityIndex ASC", this.UserIdentity.CompanyID);
        int targetIndex = int.MinValue;
        for (int i = 0; i < fields.Length; i++)
        {
            if (fields[i].ID == fieldID)
            {
                targetIndex = i;
                break;
            }
        }
        if (targetIndex == int.MinValue) // not found
        {
            return;
        }

        DynamicField targetField = fields[targetIndex];

        // execute the requested transformation on the rules
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            ArrayList fieldList = new ArrayList(fields);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEUP":
                    {
                        fieldList.RemoveAt(targetIndex);
                        if (targetIndex > 0)
                        {
                            fieldList.Insert(targetIndex - 1, targetField);
                        }
                        else // top item (move to bottom)
                        {
                            fieldList.Add(targetField);
                        }
                        break;
                    }
                case "MOVEDOWN":
                    {
                        fieldList.RemoveAt(targetIndex);
                        if (targetIndex < fields.Length - 1)
                        {
                            fieldList.Insert(targetIndex + 1, targetField);
                        }
                        else // bottom item (move to top)
                        {
                            fieldList.Insert(0, targetField);
                        }
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the fields still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < fieldList.Count; i++)
            {
                DynamicField field = (fieldList[i] as DynamicField).GetWritableInstance();
                field.PriorityIndex = (i + 1);
                field.Save(trans);
            }

            trans.Commit();
        }
    }

    protected string GetBooleanString(object dataItem)
    {
        DynamicField eField = (DynamicField)dataItem;
        return (eField.IsRequired) ? "Yes" : "No";
    }
}
