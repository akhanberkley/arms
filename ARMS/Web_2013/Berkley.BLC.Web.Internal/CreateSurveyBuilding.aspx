<%@ Page Language="C#" AutoEventWireup="false" CodeFile="CreateSurveyBuilding.aspx.cs" Inherits="CreateSurveyBuildingAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><br><br>

    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxBuildings" runat="server" Title="Building" width="600">
        <table class="fields">
            <uc:Number id="numBuildingNumber" DataType="Integer" runat="server" field="Building.Number" MaxLength="4" IsRequired="true" Columns="10" />
            
            <% if (CurrentUser.Company.ID != Company.BerkleyLifeSciences.ID) { %>
            <uc:Number id="txtYearBuilt" DataType="Integer" runat="server" MaxLength="4" field="Building.YearBuilt" Columns="10" />
            <uc:Combo id="cboHasSprinklerSystem" runat="server" field="Building.HasSprinklerSystem" Name="Sprinkler System" isRequired="false" />
            <uc:Combo id="cboHasSprinklerSystemCredit" runat="server" field="Building.HasSprinklerSystemCredit" Name="Sprinkler System Credit" isRequired="false" />
            <uc:Combo id="cboITV" runat="server" field="Building.ITV" Name="ITV" isRequired="false" />
            <uc:Number id="txtPublicProtection" DataType="Integer" runat="server" field="Building.PublicProtection" Columns="10" />
            <% } %>

            <uc:Number id="txtBuildingValue" DataType="Currency" runat="server" field="Building.Value" Columns="30" />
            <uc:Number id="txtContents" DataType="Currency" runat="server" field="Building.Contents" Columns="30" />
            <uc:Number id="txtStockValues" DataType="Currency" runat="server" field="Building.StockValues" Columns="30" />
            <uc:Number id="txtBusinessInterruption" runat="server" field="Building.BusinessInterruption" Columns="30" />
            <uc:Text id="txtLocationOccupancy" runat="server" field="Building.LocationOccupancy" Columns="80" Rows="3" />
            
            <% if (CurrentUser.Company.SupportsBuildingValuation) { %>
            <uc:Combo id="cboValuationType" runat="server" field="Building.ValuationTypeCode" Name="Valuation Type" topItemText="" isRequired="false"
		        DataType="BuildingValuationType" DataValueField="Code" DataTextField="Description" DataOrder="DisplayOrder ASC" />
		    <uc:Number id="txtCoinsurance" DataType="Integer" runat="server" field="Building.CoinsurancePercentage" Name="Coinsurance %" Columns="10" Directions="(ex. 80)" />
		    <% } %>

            <% if (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID) { %>
            <uc:Number id="txtChangeInEnvControl" DataType="Currency" runat="server" field="Building.ChangeInEnvironmentalControl" Columns="30" />
            <uc:Number id="txtScientificAnimals" DataType="Currency" runat="server" field="Building.ScientificAnimals" Columns="30" />
            <uc:Number id="txtContamination" DataType="Currency" runat="server" field="Building.Contamination" Columns="30" />
            <uc:Number id="txtRadioActiveContamination" DataType="Currency" runat="server" field="Building.RadioActiveContamination" Columns="30" />
            <uc:Number id="txtFlood" DataType="Currency" runat="server" field="Building.Flood" Columns="30" />
            <uc:Number id="txtEarthquake" DataType="Currency" runat="server" field="Building.Earthquake" Columns="30" />
		    <% } %>
        </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Save" />
    <asp:Button ID="btnAddAnother" Runat="server" CssClass="btn" Text="Add Another" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />

    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery-1.4.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('select').keypress(function (event)
            { return cancelBackspace(event) });
            $('select').keydown(function (event)
            { return cancelBackspace(event) });
        });

        function cancelBackspace(event) {
            if (event.keyCode == 8) {
                return false;
            }
        }

    </script>

    </form>
</body>
</html>
