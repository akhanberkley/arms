<%@ Page Language="C#" AutoEventWireup="false" CodeFile="SearchResults.aspx.cs" Inherits="SearchResultsAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span>

    <a href="Search.aspx?<%= _criteria.ToQueryString() %>" style="MARGIN-LEFT: 20px"><b><< Modify Search Criteria</b></a><br>
    
    <table width="1px">
        <tr>
            <td>
                <table>
                    <tr>
                        <td class="label" valign="middle" nowrap>
                            <% if( _recordCount == 1 ) { %>
                            (1 survey found)<br>
                            <% } else { %>
                            (<%= _recordCount %> surveys found)<br>
                            <% } %>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td nowrap align="left">
                            Filter By:
                            <asp:dropdownlist id="cboFilterBy" runat="server" AutoPostBack="True" CssClass="cbo">
                                <asp:ListItem Value="" Selected="True">(All)</asp:ListItem>
                                <asp:ListItem Value="Assigned">Only Assigned</asp:ListItem>
                                <asp:ListItem Value="TwoYears">Past 2 Years</asp:ListItem>
                            </asp:dropdownlist>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="lnkMapLocations" runat="server" target="_blank" visible="true">(Map All Locations)</a>
                            <% if(_mapSelectedLocations) { %>
                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lnkMapSelectedLocations" CausesValidation="false" runat="server" Text="(Map Selected Locations)" OnClientClick="frm.target ='_blank';"/> 
                            <% } %>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:Button ID="btnExport" Runat="server" CssClass="btn" Text="Export Results to Excel" CausesValidation="false" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:datagrid ID="grdItems" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="20" AllowSorting="True">
	                <headerstyle CssClass="header" VerticalAlign="top" />
	                <itemstyle CssClass="item" />
	                <alternatingitemstyle CssClass="altItem" />
	                <footerstyle CssClass="footer" />
	                <pagerstyle CssClass="pager" Mode="NumericPages" />
                </asp:datagrid>
            </td>
        </tr>
        <tr>
            <td class="label" valign="top" nowrap width="1%">Items Per Page:</td>
		    <td align="left">
		        <asp:dropdownlist id="ddlItemsPerPage" runat="server" AutoPostBack="True" CssClass="cbo">
	                <asp:ListItem Value="20" Selected="True">20</asp:ListItem>
	                <asp:ListItem Value="50">50</asp:ListItem>
	                <asp:ListItem Value="100">100</asp:ListItem>
			    </asp:dropdownlist></td>
        </tr>
    </table>
    
    <%if (!CurrentUser.IsUnderwriter){%>
    <br>
    <a href="javascript:Toggle('divSave')">Save this Search</a>
    <div id="divSave" style="DISPLAY: none">
    <br>
    <cc:Box id="boxSaveSearch" runat="server" title="Save Search" width="500">
    <table class="fields">
        <uc:Text id="lblSearchName" runat="server" field="UserSearch.Name" name="Search Name" Columns="40" />
        <uc:Combo id="cboUser" runat="server" field="" Name="For User" isRequired="true" Visible="false" />
    </table>
    </cc:Box>

    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Save" />&nbsp;
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    <%} %>

    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
        function Toggle(id) {
            ToggleControlDisplay(id);
        }

        $(document).ready(function () {
            $('#lnkMapSelectedLocations', this).click(function () {
                var chkboxrowcount = $("#<%=grdItems.ClientID%> input[id*='chkMap']:checkbox:checked").size();

            if (chkboxrowcount == 0) {
                alert("Please select at least 1 Survey Location");
                return false;
            }
            return true;
        });
    });
    </script>
    </form>
</body>
</html>
