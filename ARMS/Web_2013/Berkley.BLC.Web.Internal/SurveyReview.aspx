<%@ Page Language="C#" AutoEventWireup="false" CodeFile="SurveyReview.aspx.cs" Inherits="SurveyReviewAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server" defaultbutton="btnSubmit">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />

        <cc:Box id="boxDefinitions" runat="server" title="Performance Rating Definitions" Width="575">
            <asp:Repeater ID="repRatings" Runat="server">
                <HeaderTemplate>
                    <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0" cellpadding="0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" nowrap><b><%# GetRatingName(Container.DataItem) %></b></td>
                        <td valign="top" style="padding-left: 15px; padding-bottom: 10px;"><%# HtmlEncode(Container.DataItem, "RatingDescription")%></td>
                    </tr>	
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </cc:Box>

        <cc:Box id="boxCategories" runat="server" title="Survey" Width="575">
            <asp:Repeater ID="repCategories" Runat="server">
                <HeaderTemplate>
                    <table class="fields" cellspacing="0" cellpadding="0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
	                    <td valign="top" colspan="2" style="padding-bottom:5px"><strong><%# GetCategoryDescription(Container.DataItem) %></strong></td>
                    </tr>
                    <tr>
                        <td class="label" nowrap="nowrap" valign="top">Rating</td>
	                    <td valign="top" style="padding-bottom:5px; width:100%;">
	                        <asp:DropDownList ID="cboRating" CssClass="cbo" runat="Server"></asp:DropDownList>
	                        <asp:RequiredFieldValidator ID="valRating" Runat="server" CssClass="val" ControlToValidate="cboRating" Display="Dynamic" Enabled="false" />
	                    </td>
                    </tr>
                    <tr>
                        <td class="label" nowrap="nowrap" valign="top">Comments</td>
                        <td valign="top" style="padding-bottom:10px">
                            <asp:TextBox ID="txtComments" runat="server" Columns="75" TextMode="MultiLine" Rows="2" CssClass="txt" MaxLength="2000"></asp:TextBox>
                            <asp:Label ID="lblComments" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><hr style="width:100%" /></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </cc:Box>

    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
