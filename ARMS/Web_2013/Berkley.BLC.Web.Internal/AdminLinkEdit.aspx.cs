using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web.Validation;

public partial class AdminLinkEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminLinkEditAspx_Load);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected Link _eLink;

    void AdminLinkEditAspx_Load(object sender, EventArgs e)
    {
        Guid gLinkID = ARMSUtility.GetGuidFromQueryString("linkid", false);
        _eLink = (gLinkID != Guid.Empty) ? Link.Get(gLinkID) : null;

        if (!this.IsPostBack)
        {
            if (_eLink != null) // edit
            {
                this.PopulateFields(_eLink);
            }
        }

        btnSubmit.Text = (_eLink == null) ? "Add" : "Update";
        base.PageHeading = (_eLink == null) ? "Add Link" : "Update Link";
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            Link eLink;
            if (_eLink == null) // add
            {
                eLink = new Link(Guid.NewGuid());
                eLink.CompanyID = this.UserIdentity.CompanyID;
            }
            else // edit
            {
                eLink = _eLink.GetWritableInstance();
            }

            this.PopulateEntity(eLink);

            //get the highest priority index number for the company
            Link[] eLinks = Link.GetSortedArray("CompanyID = ?", "PriorityIndex DESC", this.CurrentUser.CompanyID);
            eLink.PriorityIndex = (eLinks.Length > 0) ? eLinks[0].PriorityIndex + 1 : 1;

            // commit all changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                eLink.Save(trans);
                trans.Commit();
            }

            // update our member var so the redirect will work correctly
            _eLink = eLink;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl());
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        return "AdminLinks.aspx";
    }
}
