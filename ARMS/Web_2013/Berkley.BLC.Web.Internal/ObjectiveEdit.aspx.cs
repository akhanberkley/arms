using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web.Validation;
using Wilson.ORMapper;

public partial class ObjectiveEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(ObjectiveEditAspx_Load);
        this.PreRender += new EventHandler(ObjectiveEditAspx_PreRender);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.repObjectiveAttributes.ItemDataBound += new RepeaterItemEventHandler(repObjectiveAttributes_ItemDataBound);
    }
    #endregion

    protected Objective _eObjective;
    protected Survey _eSurvey;
    protected ServicePlan _eServicePlan;
    protected Boolean _bServicePlan;
    protected Boolean _bCSP;

    void ObjectiveEditAspx_Load(object sender, EventArgs e)
    {
        Guid gObjectiveID = ARMSUtility.GetGuidFromQueryString("objectiveid", false);
        //Guid gPlanObjectiveID = ARMSUtility.GetGuidFromQueryString("planobjectiveid", false);
        

        _eObjective = (gObjectiveID != Guid.Empty) ? Objective.Get(gObjectiveID) : null;
        //_ePlanObjective = (gPlanObjectiveID != Guid.Empty) ? PlanObjective.Get(gPlanObjectiveID) : null;

        Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", false);
        Guid gPlanID = ARMSUtility.GetGuidFromQueryString("planid", false);

        _bCSP = ARMSUtility.GetStringFromQueryString("csp", false) == "1" ? true : false;

        if (gSurveyID != null && gSurveyID != Guid.Empty)
            _eSurvey = Survey.Get(gSurveyID);
        else
            _bServicePlan = true;

        if (gPlanID != null && gPlanID != Guid.Empty)
            _eServicePlan = ServicePlan.Get(gPlanID);

        if (!this.IsPostBack)
        {
            if (_eObjective != null) // edit
            {
                this.PopulateFields(_eObjective);
            }
            else
            {
                cboObjectiveStatus.SelectedValue = ObjectiveStatus.Open.Code;
            }
        }

        btnSubmit.Text = (_eObjective == null) ? "Add" : "Update";
        base.PageHeading = (_eObjective == null) ? "Add Objective" : "Update Objective";
    }

    void ObjectiveEditAspx_PreRender(object sender, EventArgs e)
    {
        // get any attributes
        this.repObjectiveAttributes.DataSource = AttributeHelper.GetCompanyAttributes(CurrentUser.Company, EntityType.ServiceVisitObjective);
        this.repObjectiveAttributes.DataBind();
    }

    void repObjectiveAttributes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Berkley.BLC.Web.Internal.Controls.DynamicControlsPlaceholder pl = (Berkley.BLC.Web.Internal.Controls.DynamicControlsPlaceholder)e.Item.FindControl("pHolder");
        CompanyAttribute companyAttribute = (CompanyAttribute)e.Item.DataItem;
        Guid objectiveID = _eObjective != null ? _eObjective.ID : Guid.Empty;       

        string attributeValue = string.Empty;

        if (!string.IsNullOrEmpty(companyAttribute.MappedField))
        {
            Objective objective = Objective.GetOne("ID = ?", objectiveID);
            if (companyAttribute.MappedField == "Objective.Text")
            {
                attributeValue = (objective != null) ? objective.Text : string.Empty;
            }
            else if (companyAttribute.MappedField == "Objective.CommentsText")
            {
                attributeValue = (objective != null) ? objective.CommentsText : string.Empty;
            }
            else if (companyAttribute.MappedField == "Objective.TargetDate")
            {
                if (_eSurvey != null)
                    attributeValue = (objective != null) ? objective.TargetDate.ToShortDateString() : _eSurvey.DueDate.ToShortDateString();
                else
                    attributeValue = (objective != null) ? objective.TargetDate.ToShortDateString() : DateTime.MinValue.ToShortDateString();
            }
        }
        else
        {
            attributeValue = AttributeHelper.GetAttributeValue(objectiveID, companyAttribute);
        }
        

        //set the label
        HtmlGenericControl lblAttributeLabel = (HtmlGenericControl)e.Item.FindControl("lblAttributeLabel");
        lblAttributeLabel.InnerText = companyAttribute.Label + (companyAttribute.IsRequired ? " *" : string.Empty);

        AttributeHelper.LoadWebControls(companyAttribute, attributeValue, ref pl, e.Item.ItemIndex);
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
                Objective eObjective;
                if (_eObjective == null) // add
                {
                    eObjective = new Objective(Guid.NewGuid());

                    if (_eSurvey != null)
                    {
                        eObjective.SurveyID = _eSurvey.ID;

                        //get the highest number and incriment by one
                        Objective[] eObjectives = Objective.GetSortedArray("SurveyID = ?", "Number DESC", _eSurvey.ID);
                        eObjective.Number = (eObjectives.Length > 0) ? eObjectives[0].Number + 1 : 1;
                    }
                    else if (_eServicePlan != null)
                    {
                        eObjective.ServicePlanID = _eServicePlan.ID;

                        //get the highest number and incriment by one
                        Objective[] eObjectives = Objective.GetSortedArray("ServicePlanID = ?", "Number DESC", _eServicePlan.ID);
                        eObjective.Number = (eObjectives.Length > 0) ? eObjectives[0].Number + 1 : 1;
                    }
                }
                else // edit
                {
                    eObjective = _eObjective.GetWritableInstance();
                }

                //stub out the objective details, the actuals will be populated in the SaveObjectiveAttributes method
                eObjective.Text = ".";
                if (!_bServicePlan)
                    eObjective.TargetDate = _eSurvey.DueDate;
                eObjective.StatusCode = cboObjectiveStatus.SelectedValue;
                

                // commit all changes
                using (Transaction trans = DB.Engine.BeginTransaction())
                {
                    eObjective.Save(trans);

                    // update any objective attributes
                    AttributeHelper.SaveObjectiveAttributes(repObjectiveAttributes, eObjective, CurrentUser, trans);

                    trans.Commit();
                }

                //Update the survey history
                if (!_bServicePlan)
                {
                    SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
                    manager.ObjectiveManagement(eObjective, (_eObjective == null) ? SurveyManager.UIAction.Add : SurveyManager.UIAction.Update);
                }

                // update our member var so the redirect will work correctly
                _eObjective = eObjective;
            }
            
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl());
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        if (_bCSP)
            return string.Format("CreateServicePlanVisit.aspx{0}", ARMSUtility.GetQueryStringForNav());
        else if (_bServicePlan)
            return string.Format("ManagePlanObjectives.aspx{0}", ARMSUtility.GetQueryStringForNav("objectiveid"));
        else
            return string.Format("Survey.aspx{0}", ARMSUtility.GetQueryStringForNav("objectiveid"));
    }
}
