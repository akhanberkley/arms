using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Entity = Berkley.BLC.Entities;
using QCI.Web;

public partial class AdminUsersAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminUsersAspx_Load);
        this.PreRender += new EventHandler(AdminUsersAspx_PreRender);
        this.chkShowDisabled.CheckedChanged += new EventHandler(chkShowDisabled_CheckedChanged);
        this.grdUsers.PageIndexChanged += new DataGridPageChangedEventHandler(grdUsers_PageIndexChanged);
        this.grdUsers.SortCommand += new DataGridSortCommandEventHandler(grdUsers_SortCommand);
    }
    #endregion

    void AdminUsersAspx_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // restore sort expression, reverse state, and page index settings
            this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", grdUsers.Columns[0].SortExpression);
            this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", false);
            grdUsers.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);
        }
    }

    void AdminUsersAspx_PreRender(object sender, EventArgs e)
    {
        string sortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sortExp = sortExp.Replace(" DESC", " TEMP");
            sortExp = sortExp.Replace(" ASC", " DESC");
            sortExp = sortExp.Replace(" TEMP", " ASC");
        }

        // get non-fee company users to display
        Entity.User[] users = Entity.User.GetSortedArray("Company.ID = ? && AccountDisabled = ? && IsFeeCompany = false && IsUnderwriter = false", sortExp,
            this.UserIdentity.CompanyID, chkShowDisabled.Checked);

        string noRecordsMessage = string.Format("There are no {0} users in your company.", (!chkShowDisabled.Checked) ? "active" : "disabled");

        grdUsers.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindPagingGrid(grdUsers, users, null, noRecordsMessage);
    }

    void chkShowDisabled_CheckedChanged(object sender, EventArgs e)
    {
        grdUsers.CurrentPageIndex = 0;
        this.UserIdentity.SavePageSetting("PageIndex", grdUsers.CurrentPageIndex);
    }

    void grdUsers_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdUsers.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);
    }

    void grdUsers_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sortRev);
    }
}
