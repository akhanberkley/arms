<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" ValidateRequest="false" CodeFile="CreateServicePlanVisit.aspx.cs" Inherits="CreateServicePlanVisitAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Date" Src="~/Fields/DateField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <style type="text/css">a img {border-style:none;}</style>

    <script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/javascripts/csshttprequest.js"></script>
    <script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/Keyoti_RapidSpell_Web_Common/RapidSpell-DIALOG.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="<%=ARMSUtility.TemplatePath %>/atd.css" />
 
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxVisit" runat="server" Title="Visit Details" width="700">
        <table class="fields">
            <uc:Date id="dtDueDate" runat="server" IsRequired="True" field="Survey.DueDate" />            
            <uc:Combo id="cboSurveyType" runat="server" field="Survey.TypeID" Name="Survey Type" topItemText="" isRequired="true" />
            <uc:Combo id="cboSurveyReasonType" runat="server" field="SurveyReasonType.ID" Name="Survey Reason" topItemText="" isRequired="true" />
            <uc:Combo id="cboLocations" runat="server" field="Survey.LocationID" Name="Location" topItemText="" isRequired="True" AutoPostback="True" />
            <uc:Combo id="cboAssignedUsers" runat="server" field="Survey.AssignedUserID" Name="Assigned To" topItemText="" isRequired="True" />
       </table>
    </cc:Box>
    
    <cc:Box id="boxContacts" runat="server" Title="Contacts" width="700">
        <% if (cboLocations.SelectedValue != string.Empty) { %>
        * Note: Make sure to rank contacts by importance - #1 being primary.<br><br>
        <asp:datagrid ID="grdContacts" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
            <headerstyle CssClass="header" />
            <itemstyle CssClass="item" />
            <alternatingitemstyle CssClass="altItem" />
            <footerstyle CssClass="footer" />
            <columns>
                <asp:BoundColumn HeaderText="Rank" DataField="PriorityIndex" SortExpression="PriorityIndex ASC" />
                <asp:TemplateColumn HeaderText="Name" SortExpression="Name ASC">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkContactName" runat="server" OnClick="lnkContactName_Click" CausesValidation="true">
                                <%# HtmlEncodeNullable(Container.DataItem, "Name", string.Empty, "(none)") %>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Title">
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "Title", string.Empty, "(none)") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Phone">
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "HtmlPhoneNumber", string.Empty, "(none)") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Email">
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "EmailAddress", string.Empty, "(none)") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Mail Options">
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "ContactReasonType.ShortDescription", null, "(none)") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Move">
                    <ItemTemplate>
                        &nbsp;<asp:LinkButton ID="btnUp" Runat="server" CommandName="MoveUp">Up</asp:LinkButton>&nbsp;/
                        &nbsp;<asp:LinkButton ID="btnDown" Runat="server" CommandName="MoveDown">Down</asp:LinkButton>&nbsp;
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        &nbsp;<asp:LinkButton ID="btnRemoveContact" Runat="server" CommandName="Remove" OnPreRender="btnRemoveContact_PreRender" CausesValidation="false">Remove</asp:LinkButton>&nbsp;
                    </ItemTemplate>
                </asp:TemplateColumn>
            </columns>
        </asp:datagrid>
        <asp:LinkButton ID="lnkAddContact" runat="server" CausesValidation="True">Add Contact</asp:LinkButton>
        <%} else {%>
        <div style="PADDING-LEFT: 10px">
            Note: Contact information will not appear here until a location to be setup for service is selected.
        </div>
        <% } %>
    </cc:Box>

    <cc:Box id="boxNotes" runat="server" title="Comments" width="700">
        <a id="lnkAddNote" runat="server" href="javascript:Toggle('divNotes')">Show/Hide</a>
        <div id="divNotes" style="MARGIN-TOP: 10px">
        <% if( repNotes.Items.Count > 0 ) { %>
        <asp:Repeater ID="repNotes" Runat="server">
            <HeaderTemplate>
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td colspan="2" style="BORDER-BOTTOM: solid 1px black">
                        <%# GetUserName(DataBinder.Eval(Container.DataItem, "User")) %> - 
                        <%# HtmlEncode(Container.DataItem, "EntryDate", "{0:g}") %>
                        <span style="float:right;"><em><%# GetCommentPrivacy(Container.DataItem) %></em></span>
                    </td>
                </tr>
                <tr class="comment <%# GetCommentClass(Container.DataItem) %>">
                    <td>
                        <%# GetEncodedComment(Container.DataItem) %>
                    </td>
                    <td id="tdRemoveNote" align="right" valign="top" runat="server">
                        <asp:LinkButton ID="lnkRemoveNote" runat="server" OnClick="lnkRemoveNote_OnClick">Remove</asp:LinkButton>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <% } else { %>
        <div class="comment">
            (none)
        </div>
        <% } %>
        </div>
        <div id="divAddNote" runat="server" style="MARGIN-TOP: 10px;">
            <table>
                <tr>
                    <td>
                        Service Visit Instructions / Comments<br>
                        <asp:TextBox id="txtNote" runat="server" CssClass="txt" TextMode="MultiLine" style="width: 475px" Rows="5" />
                    </td>
                    <td valign="top" style="padding-top: 10px;">
                        <a href="javascript:rapidSpell.dialog_spellCheck(true, 'txtNote')" id="checkLink"><img alt="Check Spelling" style="border:none;" src="<%=ARMSUtility.TemplatePath %>/javascripts/Images/atdbuttontr.gif" /></a>
                    </td>
                </tr>
                <!--
                <tr>
                    <td colspan="2">
                        <asp:Panel runat="server" ID="NotePrivacyDiv">
                            Visible to: 
                            <asp:RadioButton ID="rdoPrivacyAll" runat="server" Text="All" GroupName="rdoNotePrivacy" />
                            <asp:RadioButton ID="rdoPrivacyInternal" runat="server" Text="Internal Only" GroupName="rdoNotePrivacy" />
                            <asp:RadioButton ID="rdoPrivacyAdministrator" runat="server" Text="Administrators Only" GroupName="rdoNotePrivacy" />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnAddNote" Runat="server" Text="Add Comment" CssClass="btn" CausesValidation="False" />
                    </td>
                </tr>
                -->
            </table>
        </div>
    </cc:Box>
    
    <% if (!_usesPlanObjectives){ %>
    <cc:Box id="boxObjectives" runat="server" Title="Service Visit Objectives" width="700">
        <asp:datagrid ID="grdObjectives" Runat="server" CssClass="grid" AutoGenerateColumns="False" PageSize="10" AllowPaging="True" AllowSorting="True">
            <headerstyle CssClass="header" />
            <itemstyle CssClass="item" />
            <alternatingitemstyle CssClass="altItem" />
            <footerstyle CssClass="footer" />
            <pagerstyle CssClass="pager" Mode="NumericPages" />
        </asp:datagrid>
        
        <a id="A1" href="javascript:Toggle('boxObjective');" runat="server" visible="false">Add an Objective</a>

        <asp:LinkButton ID="lnkAddObjective" runat="server" CausesValidation="false">Add an Objective</asp:LinkButton>
    </cc:Box>
    
    <cc:Box id="boxObjectiveDetails" runat="server" Title="Objective Details" width="600" Visible="false">
        <table class="fields">
            <asp:Repeater ID="repObjectiveAttributes" Runat="server">
                <ItemTemplate>
                    <tr>
                        <td class="label" valign="top" nowrap><span id="lblAttributeLabel" runat="server"></span></td>
                        <td nowrap><cc:DynamicControlsPlaceholder id="pHolder" Runat="Server" /></td>
                    </tr>			
                </ItemTemplate>
            </asp:Repeater>
            <uc:Combo id="cboObjectiveStatus" runat="server" field="Objective.StatusCode" Name="Objective Status" topItemText="" isRequired="true"
                DataType="ObjectiveStatus" DataValueField="Code" DataTextField="Name" DataOrder="DisplayOrder ASC" />
            <tr>
                <td>&nbsp;</td>
                <td class="buttons">
                    <asp:Button ID="btnSaveObjective" Runat="server" CssClass="btn" Text="Save Objective" />
                </td>
            </tr>
        </table>
    </cc:Box>
    <% } %>

    <% if (_usesPlanObjectives){ %>      
        <cc:Box id="boxServicePlanObjectives" runat="server" title="Service Visit Objectives" width="100%">
            Available Plan Objectives<br>
            <asp:DropDownList ID="ddlAvailablePlanObjectives" runat="server" CssClass="cbo" align="left" valign="top" Width="600px"></asp:DropDownList>
             <asp:Button ID="btnAddPlanObjective" Runat="server" Text="Add" CssClass="btn" CausesValidation="False" /><br>
            <asp:datagrid ID="grdPlanObjectives" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
                <headerstyle CssClass="header" />
                <itemstyle CssClass="item" />
                <alternatingitemstyle CssClass="altItem" />
                <footerstyle CssClass="footer" />
                <pagerstyle CssClass="pager" Mode="NumericPages" />
                <columns>
                    <asp:TemplateColumn HeaderText="Number" SortExpression="Number ASC">
                        <ItemTemplate>
                            <a href="ObjectiveEdit.aspx<%= ARMSUtility.GetQueryStringForNav() %>&objectiveid=<%# HtmlEncode(Container.DataItem, "ID")%>"><%# HtmlEncode(Container.DataItem, "Number").PadLeft(3,'0')%></a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Objective" SortExpression="Text ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Text", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="Comments" SortExpression="CommentsText ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "CommentsText", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Target Date" SortExpression="TargetDate ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "TargetDate", "{0:d}", DateTime.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" SortExpression="Status.Name ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Status.Name", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            &nbsp;<asp:LinkButton ID="btnRemoveObjective" Runat="server" CommandName="Remove" OnPreRender="btnRemoveObjective_PreRender" CausesValidation="false">Remove</asp:LinkButton>&nbsp;
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </columns>
            </asp:datagrid>
            
            <a id="lnkAddPlanObjective" href="ObjectiveEdit.aspx" visible="false" runat="server">Create New Plan Objective</a>
            
        </cc:Box>
        <% } %>
<% if (_displayAttachDocs)   { %>  
    <cc:Box id="boxDocuments" runat="server" width="700" Visible="true">
        <a id="lnkAddDocument" href="javascript:Toggle('divAddDocument')">Show/Hide</a>
        <div id="divAddDocument" runat="server" style="MARGIN-TOP: 10px">
            <% if( repDocs.Items.Count > 0 ) { %>
            <asp:Repeater ID="repDocs" Runat="server">
                <HeaderTemplate>
                    <table cellspacing="0" cellpadding="0" border="0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td style="PADDING-BOTTOM: 3px">
                            <a id="lnkDownloadDocInNewWindow" href='<%# DataBinder.Eval(Container.DataItem, "ID", "Document.aspx?docid={0}") %>' target="_blank" runat="server"><%# GetFileNameWithSize(Container.DataItem) %></a>
                            <asp:LinkButton ID="lnkDownloadDoc" runat="server" OnClick="lnkDownloadDoc_OnClick"><%# GetFileNameWithSize(Container.DataItem) %></asp:LinkButton>
                        </td>
                        <td style="PADDING-LEFT: 10px">
                            From <%# GetUserName(DataBinder.Eval(Container.DataItem, "UploadUser")) %> on <%# HtmlEncode(Container.DataItem, "UploadedOn", "{0:g}") %>
                        </td>
                        <td id="Td1" style="PADDING-LEFT: 15px" valign="top" runat="server">
                        <asp:LinkButton ID="lnkRemoveDoc" runat="server" OnClick="lnkRemoveDoc_OnClick">Remove</asp:LinkButton>
                    </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <% } else { %>
            <div style="PADDING-BOTTOM: 10px">
                (none)
            </div>
            <% } %>
        </div>
        
        <div id="divUploadDoc" runat="server">
            Attach Document<br>
            <input type="file" id="fileUploader" runat="server" name="fileDocument" class="txt" size="80"><br>
            <%if (cboDocType.Items.Count > 0 && CurrentUser.Company.RequireDocTypeOnCreateSurvey) { %>
                Doc Type<br>
                <asp:DropDownList ID="cboDocType" runat="server" CssClass="cbo"></asp:DropDownList><br>
                <%if (CurrentUser.Company.SupportsDocRemarks) { %>
                    Doc Remarks&nbsp;<span style="font-size:10px">(optional)</span><br>
                    <asp:TextBox ID="txtDocRemarks" runat="server" Columns="35" CssClass="txt"></asp:TextBox>
                <% } %>
            <% } %>
            <div style="padding-top:5px">
                <asp:Button ID="btnAttachDocument" Runat="server" Text="Attach Document" CssClass="btn" CausesValidation="False" />
                &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lnkDocumentRetriever" runat="server"><span style="font-size:12px">...Search & Retrieve Additional Documents from FileNet >></span></asp:LinkButton>
            </div>
        </div>
    </cc:Box>
 <% } %>
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <div style="display: none;"><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" alt="" /></div>
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.blockUI.defaults.css = {};
            $.blockUI.defaults.overlayCSS.opacity = .2;

            $('#btnSubmit', this).click(function () {
                if (Page_IsValid) {
                    $.blockUI({ message: '<h3><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" />&nbsp;&nbsp;&nbsp;Processing, one moment please...</h3>' });
                }
            });
        });
    </script>
    
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
        function Toggle(id) {
            ToggleControlDisplay(id);
        }
    </script>
    </form>
</body>
</html>
