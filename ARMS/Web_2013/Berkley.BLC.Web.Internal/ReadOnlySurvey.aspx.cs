using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using QCI.Web.Validation;
using QCI.ExceptionManagement;

public partial class ReadOnlySurveyAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(ReadOnlySurveyAspx_Load);
        this.PreRender += new EventHandler(ReadOnlySurveyAspx_PreRender);
        this.repDocs.ItemDataBound += new RepeaterItemEventHandler(repDocs_ItemDataBound);
    }
    #endregion

    protected Survey _eSurvey;

    void ReadOnlySurveyAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        _eSurvey = Survey.GetOne("ID = ?", id);

        if (_eSurvey == null)
        {
            Response.Redirect("NotFound.htm");
        }
    }

    void ReadOnlySurveyAspx_PreRender(object sender, EventArgs e)
    {
        // put the insured and survey number in the header
        base.PageHeading = string.Format("Survey #{0} ({1})", _eSurvey.Number, _eSurvey.Insured.Name);

        // populate all our static fields
        this.PopulateFields(_eSurvey);

        this.PopulateFields(_eSurvey.ServiceType);
        this.PopulateFields(_eSurvey.Status);

        // populate statics that could be null
        if (_eSurvey.AssignedUser != null)
        {
            this.PopulateFields(_eSurvey.AssignedUser);
        }
        else
        {
            lblAssignedUser.Text = "(not specified)";
        }
        if (_eSurvey.Type != null)
        {
            this.PopulateFields(_eSurvey.Type.Type);
        }
        else
        {
            this.lblType.Text = "(not specified)";
        }
        if (_eSurvey.Grading != null)
        {
            this.PopulateFields(_eSurvey.Grading);
        }
        else
        {
            lblGrading.Text = "(not specified)";
        }

        //populate the dynamic bool fields
        lblNonProductive.Text = (_eSurvey.NonProductive) ? "Yes" : "No";
        lblFlagForReview.Text = (_eSurvey.Review) ? "Yes" : "No";

        // get all documents associated to this survey
        SurveyDocument[] docs;
        if (_eSurvey.Status.Type == SurveyStatusType.Survey)
        {
            docs = SurveyDocument.GetSortedArray("SurveyID = ? && ISNULL(UploadUserID) && IsRemoved = false", "UploadedOn DESC", _eSurvey.ID);
        }
        else
        {
            docs = SurveyDocument.GetSortedArray("SurveyID = ? && IsRemoved = false", "UploadedOn DESC", _eSurvey.ID);
        }

        repDocs.DataSource = docs;
        repDocs.DataBind();
        boxDocs.Title = string.Format("Documents ({0})", docs.Length);

        // get all notes associated to this survey
        SurveyNote[] notes = SurveyNote.GetSortedArray("SurveyID = ?", "EntryDate DESC", _eSurvey.ID);
        repNotes.DataSource = notes;
        repNotes.DataBind();
        boxNotes.Title = string.Format("Comments ({0})", notes.Length);

        // get the objectives for this survey
        Objective[] eObjectives = Objective.GetSortedArray("SurveyID = ?", "Number ASC", _eSurvey.ID);
        repObjectives.DataSource = eObjectives;
        repObjectives.DataBind();
        boxObjectives.Title = string.Format("Objectives ({0})", eObjectives.Length);

        // get all recs associated to this survey
        SurveyRecommendation[] eRecs = SurveyRecommendation.GetSortedArray(_eSurvey.GetAllSurveysForAccount, "PriorityIndex ASC");
        repRecs.DataSource = eRecs;
        repRecs.DataBind();
        boxRecs.Title = string.Format("Recommendations ({0})", eRecs.Length);

        // get all history associated to this survey
        SurveyHistory[] history = SurveyHistory.GetSortedArray("SurveyID = ?", "EntryDate DESC", _eSurvey.ID);
        repHistory.DataSource = history;
        repHistory.DataBind();
        boxHistory.Title = string.Format("History ({0})", history.Length);
    }

    void repDocs_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            SurveyDocument eDoc = (SurveyDocument)e.Item.DataItem;
            LinkButton lnkDownload = (LinkButton)e.Item.FindControl("lnkDownloadDoc");
            HtmlAnchor lnkDownloadDocInNewWindow = (HtmlAnchor)e.Item.FindControl("lnkDownloadDocInNewWindow");
            lnkDownload.Attributes.Add("DocumentID", eDoc.ID.ToString());

            //determine which link to display
            lnkDownload.Visible = !eDoc.MimeType.Contains("htm");
            lnkDownloadDocInNewWindow.Visible = eDoc.MimeType.Contains("htm");
        }
    }

    protected void lnkDownloadDoc_OnClick(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            SurveyDocument doc = SurveyDocument.Get(new Guid(lnk.Attributes["DocumentID"]));

            StreamDocument docRetriever = new StreamDocument(doc, doc.Survey.Company);
            docRetriever.AttachDocToResponse(this);
        }
        catch (System.Web.HttpException ex)
        {
            //ignore http exceptions for when the user quits a doc download and the stream doesn't get flushed
            if (ex.Message.Contains("The remote host closed the connection"))
            {
                //swallow exception
            }
            else
            {
                throw new Exception("See inner exception for details.", ex);
            }
        }
    }

    protected string GetUserName(object obj)
    {
        User user = (obj as User);
        return Server.HtmlEncode((user != null) ? user.Name : _eSurvey.CreateByExternalUserName);
    }

    protected string GetEncodedHistory(object dataItem)
    {
        string history = (dataItem as SurveyHistory).EntryText;

        // html encode the string (must do this first)
        return Server.HtmlEncode(history);
    }

    protected string GetEncodedComment(object dataItem)
    {
        string comment = (dataItem as SurveyNote).Comment;
        bool internalOnly = (dataItem as SurveyNote).InternalOnly;

        // html encode the string (must do this first)
        comment = Server.HtmlEncode(comment);

        // convert CR, LF, and TAB characters
        comment = comment.Replace("\r\n", "<br>");
        comment = comment.Replace("\r", "<br>");
        comment = comment.Replace("\n", "<br>");
        comment = comment.Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");

        //make vendor notes/comments bolded
        if (internalOnly)
        {
            comment = "<b>" + comment + "</b>";
        }

        return comment;
    }

    protected string GetFileNameWithSize(object dataItem)
    {
        SurveyDocument doc = (SurveyDocument)dataItem;

        string result = string.Format("{0} ({1:#,##0} KB)", doc.DocumentName, doc.SizeInBytes / 1024);

        return Server.HtmlEncode(result);
    }
}
