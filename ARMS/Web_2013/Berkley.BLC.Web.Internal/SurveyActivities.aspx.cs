using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using Wilson.ORMapper;
using QCI.Web.Validation;

public partial class SurveyActivitiesAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(SurveyActivitiesAspx_Load);
        this.PreRender += new EventHandler(SurveyActivitiesAspx_PreRender);
        this.grdActivities.ItemCommand += new DataGridCommandEventHandler(grdActivities_ItemCommand);
        this.grdActivities.SortCommand += new DataGridSortCommandEventHandler(grdActivities_SortCommand);
        this.grdActivities.PageIndexChanged += new DataGridPageChangedEventHandler(grdActivities_PageIndexChanged);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.btnAdd.Click += new EventHandler(btnAdd_Click);
    }
    #endregion

    protected SurveyActivity _eActivity;
    protected Survey _eSurvey;

    void SurveyActivitiesAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        _eSurvey = Survey.Get(id);

        Guid gActivityID = ARMSUtility.GetGuidFromQueryString("activityid", false);
        _eActivity = (gActivityID != Guid.Empty) ? SurveyActivity.Get(gActivityID) : null;

        // restore sort expression, reverse state, and page index settings
        this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", grdActivities.Columns[0].SortExpression);
        this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", false);
        grdActivities.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);

        if (!this.IsPostBack)
        {
            // populate the company specific activity types
            ActivityType[] eTypes = ActivityType.GetSortedArray("CompanyID = ? && (LevelCode = ? || LevelCode = ? || LevelCode = ?) && Disabled != True", "PriorityIndex ASC", this.CurrentUser.CompanyID, ActivityTypeLevel.Survey.Code, ActivityTypeLevel.SurveyAndServicePlan.Code, ActivityTypeLevel.All.Code);
            cboActivityType.DataBind(eTypes, "ID", "Name");

            if (_eActivity != null) // edit
            {
                divAddActivity.Visible = true;
                this.PopulateFields(_eActivity);

                //Check if the selected items exist in the list
                if (cboActivityType.Items.Contains(new ListItem(_eActivity.ActivityType.Name, _eActivity.ActivityTypeID.ToString())))
                {
                    this.PopulateFields(_eActivity.ActivityType);
                }
                else
                {
                    //add it to the list and select it
                    cboActivityType.Items.Add(new ListItem(_eActivity.ActivityType.Name, _eActivity.ActivityTypeID.ToString()));
                    cboActivityType.SelectedValue = _eActivity.ActivityTypeID.ToString();
                }
            }
            else
            {
                //default
                dtActivityDate.Date = DateTime.Today;
            }

            btnSubmit.Text = (_eActivity == null) ? "Add" : "Update";
            base.PageHeading = (_eActivity == null) ? "Add Activity Time" : "Update Activity Time";
        }
    }

    void SurveyActivitiesAspx_PreRender(object sender, EventArgs e)
    {
        string sortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sortExp = sortExp.Replace(" DESC", " TEMP");
            sortExp = sortExp.Replace(" ASC", " DESC");
            sortExp = sortExp.Replace(" TEMP", " ASC");
        }
        
        // get the activities for this survey
        SurveyActivity[] eActivities = SurveyActivity.GetSortedArray("SurveyID = ?", sortExp, _eSurvey.ID);
        DataGridHelper.BindGrid(grdActivities, eActivities, "ID", "There are no activity times for this survey.");
    }

    protected void btnRemoveActivity_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this activity?");
    }

    void grdActivities_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            // get the ID of the territory selected
            Guid id = (Guid)grdActivities.DataKeys[e.Item.ItemIndex];

            // remove the objective
            SurveyActivity eActivity = SurveyActivity.Get(id);
            if (eActivity != null)
            {
                eActivity.Delete();

                //Update the survey history
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
                manager.ActivityManagement(eActivity, SurveyManager.UIAction.Remove);
            }
        }
    }

    void grdActivities_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdActivities.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);
    }

    void grdActivities_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sortRev);
    }

    void btnAdd_Click(object sender, EventArgs e)
    {
        divAddActivity.Visible = true;
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            SurveyActivity eActivity;
            if (_eActivity == null) // add
            {
                eActivity = new SurveyActivity(Guid.NewGuid());
            }
            else
            {
                eActivity = _eActivity.GetWritableInstance();
            }

            eActivity.SurveyID = _eSurvey.ID;
            eActivity.ActivityTypeID = new Guid(cboActivityType.SelectedValue);
            eActivity.AllocatedTo = CurrentUser.ID;
            eActivity.ActivityDate = dtActivityDate.Date;
            eActivity.ActivityHours = Convert.ToDecimal(numHours.Text);
            eActivity.Comments = txtComments.Text;

            // commit changes
            eActivity.Save();

            //Update the survey history
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
            manager.ActivityManagement(eActivity, (_eActivity == null) ? SurveyManager.UIAction.Add : SurveyManager.UIAction.Update);

            // update our member var so the redirect will work correctly
            _eActivity = eActivity;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(string.Format("SurveyActivities.aspx{0}", ARMSUtility.GetQueryStringForNav("activityid")), true);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("SurveyActivities.aspx{0}", ARMSUtility.GetQueryStringForNav("activityid")), true);
    }
}
