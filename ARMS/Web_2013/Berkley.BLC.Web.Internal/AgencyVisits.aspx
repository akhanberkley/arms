﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AgencyVisits.aspx.cs" Inherits="AgencyVisits" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    <span class="heading"><%= base.PageHeading %></span><br><br>

    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>Year:</td>
                        <td><asp:DropDownList ID="cboYear" Runat="server" CssClass="cbo" AutoPostBack="True" /></td>
                        <td>&nbsp;&nbsp;Month:</td>
                        <td><asp:DropDownList ID="cboMonth" Runat="server" CssClass="cbo" AutoPostBack="True" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td>Agency:</td>
                        <td><asp:dropdownlist id="cboAgency" runat="server" AutoPostBack="True" CssClass="cbo"></asp:dropdownlist></td>                        
                    </tr>
                </table>
            </td>

            <td align="right">
                <table>
                    <tr id="rowUserFilter" runat="server">
                        <td>User:</td>
                        <td><asp:dropdownlist id="cboUser" runat="server" AutoPostBack="True" CssClass="cbo"></asp:dropdownlist></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <asp:datagrid ID="grdActivities" Runat="server" CssClass="grid" Width="750" AutoGenerateColumns="False" AllowPaging="True" PageSize="20" AllowSorting="True">
                <headerstyle CssClass="header" />
                <itemstyle CssClass="item" />
                <alternatingitemstyle CssClass="altItem" />
                <footerstyle CssClass="footer" />
                <pagerstyle CssClass="pager" Mode="NumericPages" />
                <columns>
                    <asp:TemplateColumn HeaderText="Date" SortExpression="Date DESC">
                        <ItemTemplate>
                            <a href="AgencyVisitEdit.aspx?activityid=<%# HtmlEncode(Container.DataItem, "ID")%>">
                                <%# HtmlEncodeNullable(Container.DataItem, "Date", "{0:d}", DateTime.MinValue, "(none)")%>
                            </a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Hours" SortExpression="Hours ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Hours", decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Days" SortExpression="Days ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Days", decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Call Count" HeaderStyle-Wrap="false" SortExpression="CallCount ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "CallCount", decimal.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Activity Type" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" sortExpression="Type.Name ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Type.Name", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Agency Visited" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" sortExpression="AgencyVisited.Name ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "AgencyVisited.Name", null, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Allocated To" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" SortExpression="User.Name ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "User.Name", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Comments" SortExpression="Comments ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Comments", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            &nbsp;<asp:LinkButton ID="btnRemoveRec" Runat="server" CommandName="Remove" OnPreRender="btnRemoveActivity_PreRender">Remove</asp:LinkButton>&nbsp;
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </columns>
            </asp:datagrid>
            </td>
        </tr>
        <tr>
            <td><a href="AgencyVisitEdit.aspx?userid=<%=CurrentUser.ID%>">Add New Agency Visit</a></td>
            <td align="right"><asp:Button ID="btnExport" Runat="server" CssClass="btn" Text="Export to Excel" CausesValidation="false" /></td>
        </tr>
    </table>
    
    <uc:HintFooter ID="ucHintFooter" runat="server" VerticalOffset="135" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
