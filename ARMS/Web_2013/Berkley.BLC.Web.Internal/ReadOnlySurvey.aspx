<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" CodeFile="ReadOnlySurvey.aspx.cs" Inherits="ReadOnlySurveyAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register TagPrefix="dp" Namespace="QCI.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="Counter" Src="~/Controls/Counter.ascx" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" media="screen" type="text/css" href="<%= base.TemplatePath %>/styles.css" />
    <link rel="stylesheet" media="print" type="text/css" href="<%= base.TemplatePath %>/Print-Survey.css" />

    <form id="frm" method="post" runat="server">
    
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
	    <td class="pageBody" width="100%" height="400" valign="top">
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <table cellspacing="0" cellpadding="0" border="0">
    <tr>
	    <td valign="top" width="700">
        <cc:Box id="boxSurvey" runat="server" title="Survey Information" width="100%">
	        <table class="fields">
	        <uc:Label id="lblNumber" runat="server" field="Survey.Number" />
	        <% if (_eSurvey.Status != SurveyStatus.AssignedSurvey) { %>
	        <uc:Label id="lblConsultant" runat="server" field = "Survey.HtmlConsultant" Name= "Consultant" />
	        <% } %>
	        <uc:Label id="lblType" runat="server" field="SurveyTypeType.Name" Name="Survey Type" />
	        <uc:Label id="lblServiceType" runat="server" field="ServiceType.Name" />
	        <uc:Label id="lblDueDate" runat="server" field="Survey.DueDate" />
	        <uc:Label id="lblCreateDate" runat="server" field="Survey.CreateDate" Name="Created On" />
	        <uc:Label id="lblCreatedBy" runat="server" field="Survey.HtmlCreatedBy" Name="Created By" />
	        <uc:Label id="lblStatus" runat="server" field="SurveyStatus.Name" />
	        <% if (_eSurvey.Status == SurveyStatus.OnHoldSurvey) { %>
	        <uc:Label id="lblRecommendedUser" runat="server" field="Survey.HtmlRecommendedConsultant" Name="Recommended User" />
	        <%} else {%>
	        <uc:Label id="lblAssignedUser" runat="server" field="User.Name" Name="Assigned User" />
	        <% } %>
	        <uc:Label id="lblDateSurveyed" runat="server" field="Survey.SurveyedDate" Name="Date Surveyed" />
	        <uc:Label id="lblSurveyHours" runat="server" field="Survey.Hours" ValueFormat="{0:n2}"/>
	        <uc:Label id="lblGrading" runat="server" field="SurveyGrading.Name" Name="Overall Grading" />
	        <uc:Label id="lblCallCount" runat="server" field="Survey.CallCount" ValueFormat="{0:n2}"/>
	        <tr>
		        <td class="label" valign="top" nowrap>Non-Productive</td>
		        <td>
		            <asp:LinkButton id="lnkNonProductive" runat="server"></asp:LinkButton>
		            <asp:Label id="lblNonProductive" runat="server"></asp:Label>
		        </td>
	        </tr>
	        <tr>
		        <td class="label" valign="top" nowrap>Flag For Review</td>
		        <td>
		            <asp:LinkButton id="lnkFlagForReview" runat="server"></asp:LinkButton>
		            <asp:Label id="lblFlagForReview" runat="server"></asp:Label>
		        </td>
	        </tr>
	        <% if (_eSurvey.AssignDate != DateTime.MinValue) { %>
	        <uc:label id="lblAssignDate" runat="server" field="Survey.AssignDate" Name="Assigned Date" />
	        <% } %>
	        <% if (_eSurvey.ReassignDate != DateTime.MinValue) { %>
	        <uc:label id="lblReassignDate" runat="server" field="Survey.ReassignDate" Name="Reassigned Date" />
	        <% } %>
	        <% if (_eSurvey.AcceptDate != DateTime.MinValue) { %>
	        <uc:label id="lblAcceptDate" runat="server" field="Survey.AcceptDate" Name="Accepted Date" />
	        <% } %>
	        <% if (_eSurvey.ReportCompleteDate != DateTime.MinValue) { %>
	        <uc:label id="lblReportCompleteDate" runat="server" field="Survey.ReportCompleteDate" Name="Report Completed Date" />
	        <% } %>
	        <% if (_eSurvey.FeeConsultantCost != decimal.MinValue) { %>
	        <uc:Label id="lblFeeConsultantCost" runat="server" field="Survey.FeeConsultantCost" ValueFormat="{0:c}" />
	        <% } %>
	        <% if (_eSurvey.CompleteDate != DateTime.MinValue) { %>
	        <uc:label id="lblCompleteDate" runat="server" field="Survey.CompleteDate" />
	        <% } %>
	        <% if (_eSurvey.CanceledDate != DateTime.MinValue) { %>
	        <uc:label id="lblCanceledDate" runat="server" field="Survey.CanceledDate" />
	        <% } %>
	        <uc:Label id="lblLastModifiedOn" runat="server" field="Survey.LastModifiedOn" ValueFormat="{0:g}" />
	        </table>
        </cc:Box>

        <cc:Box id="boxNotes" runat="server" title="Comments" width="100%">
        <a id="lnkAddNote" runat="server" href="javascript:Toggle('divNotes')">Show/Hide</a>
        <div id="divNotes" style="MARGIN-TOP: 10px">
        <% if( repNotes.Items.Count > 0 ) { %>
        <asp:Repeater ID="repNotes" Runat="server">
	        <HeaderTemplate>
		        <table width="100%" cellspacing="0" cellpadding="0" border="0">
	        </HeaderTemplate>
	        <ItemTemplate>
		        <tr>
			        <td style="BORDER-BOTTOM: solid 1px black">
				        <%# GetUserName(DataBinder.Eval(Container.DataItem, "User")) %> - 
				        <%# HtmlEncode(Container.DataItem, "EntryDate", "{0:g}") %>
			        </td>
		        </tr>
		        <tr>
			        <td style="PADDING-BOTTOM: 10px">
				        <%# GetEncodedComment(Container.DataItem) %>
			        </td>
		        </tr>
	        </ItemTemplate>
	        <FooterTemplate>
		        </table>
	        </FooterTemplate>
        </asp:Repeater>
        <% } else { %>
        <div style="PADDING-BOTTOM: 10px">
	        (none)
        </div>
        <% } %>
        </div>
        </cc:Box>

        <cc:Box id="boxDocs" runat="server" title="Documents" width="100%">
        <a id="lnkAddDoc" href="javascript:Toggle('divAddDoc')">Show/Hide</a>
        <div id="divAddDoc" runat="server" style="MARGIN-TOP: 10px">
        <% if( repDocs.Items.Count > 0 ) { %>
        <asp:Repeater ID="repDocs" Runat="server">
	        <HeaderTemplate>
		        <table cellspacing="0" cellpadding="0" border="0">
	        </HeaderTemplate>
	        <ItemTemplate>
		        <tr>
			        <td style="PADDING-BOTTOM: 3px">
			            <a id="lnkDownloadDocInNewWindow" href='<%# DataBinder.Eval(Container.DataItem, "ID", "Document.aspx?docid={0}") %>' target="_blank" runat="server"><%# GetFileNameWithSize(Container.DataItem) %></a>
				        <asp:LinkButton ID="lnkDownloadDoc" runat="server" OnClick="lnkDownloadDoc_OnClick"><%# GetFileNameWithSize(Container.DataItem) %></asp:LinkButton>
			        </td>
			        <td style="PADDING-LEFT: 10px">
				        From <%# GetUserName(DataBinder.Eval(Container.DataItem, "UploadUser")) %> on <%# HtmlEncode(Container.DataItem, "UploadedOn", "{0:g}") %>
			        </td>
		        </tr>
	        </ItemTemplate>
	        <FooterTemplate>
		        </table>
	        </FooterTemplate>
        </asp:Repeater>
        <% } else { %>
        <div style="PADDING-BOTTOM: 10px">
	        (none)
        </div>
        <% } %>
        </div>
        </cc:Box>
        
        <% if (_eSurvey.ServiceType == ServiceType.ServiceVisit) { %>
        <cc:Box id="boxObjectives" runat="server" title="Objectives" width="100%">
            <% if( repObjectives.Items.Count > 0 ) { %>
            <asp:Repeater ID="repObjectives" Runat="server">
	            <HeaderTemplate>
			        <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0" cellspacing="0">
				        <tr>
					        <td nowrap><u>Number</u></td>
					        <td style="PADDING-LEFT: 20px" nowrap><u>Objective</u></td>
					        <td style="PADDING-LEFT: 20px" nowrap><u>Comments</u></td>
					        <td style="PADDING-LEFT: 20px" nowrap><u>Target Date</u></td>
					        <td style="PADDING-LEFT: 20px" nowrap><u>Status</u></td>
				        </tr>
		        </HeaderTemplate>
		        <ItemTemplate>
			        <tr>
				        <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Number", Int32.MinValue, "(none)")%></td>
				        <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "Text", string.Empty, "(none)")%></td>
				        <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "CommentsText", string.Empty, "(none)")%></td>
				        <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "TargetDate", "{0:d}", DateTime.MinValue, "(none)")%></td>
				        <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "Status.Name", string.Empty, "(none)")%></td>
			        </tr>			
		        </ItemTemplate>
		        <FooterTemplate>
			        </table>
		        </FooterTemplate>
	        </asp:Repeater>
	        <% } else { %>
            <div style="PADDING-BOTTOM: 10px">
                (none)
            </div>
            <% } %>
        </cc:Box>
        <% } %>
        
        <cc:Box id="boxRecs" runat="server" title="Recommendations" width="100%">
            <% if( repRecs.Items.Count > 0 ) { %>
            <asp:Repeater ID="repRecs" Runat="server">
	            <HeaderTemplate>
			        <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0" cellspacing="0">
				        <tr>
					        <td nowrap><u>Number</u></td>
					        <td style="PADDING-LEFT: 10px" nowrap><u>Rec Code</u></td>
					        <td style="PADDING-LEFT: 10px" nowrap><u>Rec Title</u></td>
					        <td style="PADDING-LEFT: 10px" nowrap><u>Classification</u></td>
					        <td style="PADDING-LEFT: 10px" nowrap><u>Date Created</u></td>
					        <td style="PADDING-LEFT: 10px" nowrap><u>Date Completed</u></td>
					        <td style="PADDING-LEFT: 10px" nowrap><u>Status</u></td>
				        </tr>
		        </HeaderTemplate>
		        <ItemTemplate>
			        <tr>
				        <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "RecommendationNumber", string.Empty, "(none)")%></td>
				        <td valign="top" style="PADDING-LEFT: 10px"><%# HtmlEncodeNullable(Container.DataItem, "Recommendation.Code", string.Empty, "(none)")%></td>
				        <td valign="top" style="PADDING-LEFT: 10px"><%# HtmlEncodeNullable(Container.DataItem, "Recommendation.Title", string.Empty, "(none)")%></td>
				        <td valign="top" style="PADDING-LEFT: 10px"><%# HtmlEncodeNullable(Container.DataItem, "RecClassification.Type.Name", null, "(none)")%></td>
				        <td valign="top" style="PADDING-LEFT: 10px"><%# HtmlEncodeNullable(Container.DataItem, "DateCreated", "{0:d}", DateTime.MinValue, "(none)")%></td>
				        <td valign="top" style="PADDING-LEFT: 10px"><%# HtmlEncodeNullable(Container.DataItem, "DateCompleted", "{0:d}", DateTime.MinValue, "(none)")%></td>
				        <td valign="top" style="PADDING-LEFT: 10px"><%# HtmlEncodeNullable(Container.DataItem, "RecStatus.Type.Name", null, "(none)")%></td>
			        </tr>			
		        </ItemTemplate>
		        <FooterTemplate>
			        </table>
		        </FooterTemplate>
	        </asp:Repeater>
	        <% } else { %>
            <div style="PADDING-BOTTOM: 10px">
                (none)
            </div>
            <% } %>
        </cc:Box>

        <cc:Box id="boxHistory" runat="server" title="History" width="100%">
        <a id="lnkHistory" href="javascript:Toggle('divHistory')">Show/Hide</a><br>
        <div id="divHistory" style="MARGIN-TOP: 10px">
        <% if( repHistory.Items.Count > 0 ) { %>
	        <asp:Repeater ID="repHistory" Runat="server">
		        <HeaderTemplate>
			        <table cellspacing="0" cellpadding="0" border="0">
		        </HeaderTemplate>
		        <ItemTemplate>
			        <tr>
				        <td valign="top" nowrap>
					        <%# HtmlEncode(Container.DataItem, "EntryDate", "{0:g}") %>
				        </td>
				        <td style="PADDING-LEFT: 10px; PADDING-BOTTOM: 4px">
					        <%# GetEncodedHistory(Container.DataItem)%>
				        </td>
			        </tr>
		        </ItemTemplate>
		        <FooterTemplate>
			        </table>
		        </FooterTemplate>
	        </asp:Repeater>
        <% } else { %>
	        (none)
        <% } %>
        </div>
        </cc:Box>

        </td>
    </tr>
    </table>
        	
    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
    function Toggle(id)
    {
	    ToggleControlDisplay(id);
    }
    </script>
    </form>
</body>
</html>
