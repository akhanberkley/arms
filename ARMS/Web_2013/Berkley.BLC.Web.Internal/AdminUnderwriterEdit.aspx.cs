using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web.Validation;

public partial class AdminUnderwriterEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminUnderwriterEditAspx_Load);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected Underwriter _eUnderwriter;

    void AdminUnderwriterEditAspx_Load(object sender, EventArgs e)
    {
        string code = ARMSUtility.GetStringFromQueryString("underwriterid", false);
        _eUnderwriter = (code != string.Empty) ? Underwriter.GetOne("Code = ? && CompanyID = ?", code, CurrentUser.CompanyID) : null;

        if (!this.IsPostBack)
        {
            //populate the company specific profit centers
            ProfitCenter[] eProfitCenters = ProfitCenter.GetSortedArray("CompanyID = ?", "Name ASC", CurrentUser.CompanyID);
            cboProfitCenter.DataBind(eProfitCenters, "Code", "Name");
            
            if (_eUnderwriter != null) // edit
            {
                this.PopulateFields(_eUnderwriter);
            }
            else
            {
                txtDomainName.Text = this.CurrentUser.Company.PrimaryDomainName;
            }
        }

        btnSubmit.Text = (_eUnderwriter == null) ? "Add" : "Update";
        base.PageHeading = (_eUnderwriter == null) ? "Add Underwriter" : "Update Underwriter";
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        txtCode.Text = txtCode.Text.ToUpper();

        try
        {
            Underwriter eUnderwriter;
            if (_eUnderwriter == null) // add
            {
                //First check if these initials already belong to another underwriter
                Underwriter uw = Underwriter.GetOne("Code = ? && CompanyID = ?", txtCode.Text, CurrentUser.CompanyID);
                if (uw != null)
                {
                    throw new FieldValidationException(txtCode, string.Format("These intials are already associate with {0}.", uw.Name)); 
                }

                eUnderwriter = new Underwriter(txtCode.Text, CurrentUser.CompanyID);
            }
            else // edit
            {
                //Check if the intials have changed
                if (_eUnderwriter.Code != txtCode.Text.Trim())
                {
                    _eUnderwriter.Delete();
                    eUnderwriter = new Underwriter(txtCode.Text, CurrentUser.CompanyID);
                }
                else
                {
                    eUnderwriter = _eUnderwriter.GetWritableInstance();
                }
            }

            this.PopulateEntity(eUnderwriter);

            // commit all changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                eUnderwriter.Save(trans);
                trans.Commit();
            }

            //reset the cache lists
            AppCache.ResetUnderwriters(this, CurrentUser.Company, "Underwriters");

            // update our member var so the redirect will work correctly
            _eUnderwriter = eUnderwriter;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl());
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        return "AdminUnderwriters.aspx";
    }
}
