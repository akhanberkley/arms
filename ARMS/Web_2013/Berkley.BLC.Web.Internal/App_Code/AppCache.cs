using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Berkley.BLC.Entities;

/// <summary>
/// Summary description for AppCache
/// </summary>
public class AppCache
{
    private static DateTime expiration = DateTime.Today.AddHours(22);
    
    public static Underwriter[] GetUnderwriters(Page page, Company company, string key, bool enabledOnly)
    {
        string cacheKey = BuildKey(key, company);
        
        Underwriter[] underwriters;
        if (page.Cache.Get(cacheKey) == null)
        {
            underwriters = Underwriter.GetArray(company, enabledOnly);
            AddToCache(page, cacheKey, underwriters);
        }
        else
        {
            underwriters = page.Cache[cacheKey] as Underwriter[];
        }

        return underwriters;
    }

    public static void ResetUnderwriters(Page page, Company company, string key)
    {
        string cacheKey = BuildKey(key, company);

        if (page.Cache.Get(cacheKey) != null)
        {
            page.Cache.Remove(cacheKey);
        }
    }

    public static SIC[] GetSICCodes(Page page, Company company, string key, int descriptionLength)
    {
        string cacheKey = BuildKey(key, company);

        SIC[] codes;
        if (page.Cache.Get(cacheKey) == null)
        {
            codes = SIC.GetArray(descriptionLength, company.SICSortedByAlpha);
            AddToCache(page, cacheKey, codes);
        }
        else
        {
            codes = page.Cache[cacheKey] as SIC[];
        }

        return codes;
    }

    public static NAIC[] GetNAICCodes(Page page, Company company, string key, int descriptionLength)
    {
        string cacheKey = BuildKey(key, company);

        NAIC[] codes;
        if (page.Cache.Get(cacheKey) == null)
        {
            codes = NAIC.GetArray(descriptionLength, company.NAICSortedByAlpha);
            AddToCache(page, cacheKey, codes);
        }
        else
        {
            codes = page.Cache[cacheKey] as NAIC[];
        }

        return codes;
    }

    public static DataTable GetCreatedByNonLCStaff(Page page, Company company, string key)
    {
        string cacheKey = BuildKey(key, company);

        DataTable dt;
        if (page.Cache.Get(cacheKey) == null)
        {
            dt = DB.Engine.GetDataSet(string.Format("EXEC CreatedBy_Get @CompanyID = '{0}'", company.ID)).Tables[0];
            AddToCache(page, cacheKey, dt);
        }
        else
        {
            dt = (DataTable)page.Cache[cacheKey];
        }

        return dt;
    }

    private static void AddToCache(Page page, string cacheKey, object data)
    {
        page.Cache.Add(cacheKey, data, null, expiration, System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null);
    }

    private static string BuildKey(string key, Company company)
    {
        return string.Format("{0}For{1}", key, company.Abbreviation);
    }
}
