using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Specialized;
using System.Text;

using Aspose.Words;
using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.DataAccess;

/// <summary>
/// Summary description for MailMerge
/// </summary>
public class MailMerge
{
    private Document doc;
    private DataSet ds;
    private string docExtension;

    #region Enumerations

    public enum MergeTables
    {
        InsuredData = 0,
        Recs = 1,
        PrimaryContact = 2,
        Policies = 3,
        NonPrimaryContacts = 4,
        Locations = 5
    }

    public enum MergeTablesServicePlan
    {
        ServicePlanDetails = 0,
        ServiceVisits = 1,
        Comments = 2,
        Objectives = 3,
    }

    #endregion

    /// <summary>
    /// Uses Apose.Word to merge the predefined mail merge fields with the passed survey.
    /// </summary>
    /// <param name="stream">The document stream.</param>
    /// <param name="docuementName">The document name.</param>
    public MailMerge(Stream stream, string docuementName)
    {
        Aspose.Words.License license = new Aspose.Words.License();
        license.SetLicense("Aspose.Words.lic");

        doc = new Document(stream);
        docExtension = System.IO.Path.GetExtension(docuementName).ToUpper();
    }

    /// <summary>
    /// Uses Apose.Word to merge the predefined mail merge fields with the passed survey.
    /// </summary>
    /// <param name="survey">Survey to be used to merge the fields.</param>
    public void Merge(Survey survey)
    {
        //execute the stored procedure
        string command = string.Format("EXEC Document_GetMergeFields @SurveyID = '{0}'", survey.ID);
        ds = DB.Engine.GetDataSet(command);

        //turn on the remove empty paragraphs call
        doc.MailMerge.RemoveEmptyParagraphs = true;

        //Insured Data and the primary contact
        doc.MailMerge.Execute(ds.Tables[(int)MergeTables.InsuredData]);
        doc.MailMerge.Execute(ds.Tables[(int)MergeTables.PrimaryContact]);

        //NOTE: Repeatable Items must have a table name
        DataTable dtRecs = ds.Tables[(int)MergeTables.Recs];
        dtRecs.TableName = "Recs";
        doc.MailMerge.ExecuteWithRegions(dtRecs);

        //NOTE: Repeatable Items must have a table name (3 sets of differently names policy tables)
        DataTable dtPolicies = ds.Tables[(int)MergeTables.Policies];
        dtPolicies.TableName = "Policies";
        doc.MailMerge.ExecuteWithRegions(dtPolicies);

        dtPolicies.TableName = "Policies2";
        doc.MailMerge.ExecuteWithRegions(dtPolicies);

        dtPolicies.TableName = "Policies3";
        doc.MailMerge.ExecuteWithRegions(dtPolicies);

        //NOTE: Repeatable Items must have a table name
        DataTable dtContacts = ds.Tables[(int)MergeTables.NonPrimaryContacts];
        dtContacts.TableName = "NonPrimaryContacts";
        doc.MailMerge.ExecuteWithRegions(dtContacts);

        //NOTE: Repeatable Items must have a table name
        DataTable dtLocations = ds.Tables[(int)MergeTables.Locations];
        dtLocations.TableName = "Locations";
        doc.MailMerge.ExecuteWithRegions(dtLocations);
    }

    /// <summary>
    /// Uses Apose.Word to merge the predefined mail merge fields with the passed ServicePlan.
    /// </summary>
    /// <param name="survey">ServicePlan to be used to merge the fields.</param>
    public void Merge(ServicePlan servicePlan)
    {
        //execute the stored procedure
        string command = string.Format("EXEC Document_GetMergeFields_ServicePlan @ServicePlanID = '{0}'", servicePlan.ID);
        ds = DB.Engine.GetDataSet(command);

        //turn on the remove empty paragraphs call
        doc.MailMerge.RemoveEmptyParagraphs = true;

        //service plan details
        doc.MailMerge.Execute(ds.Tables[(int)MergeTablesServicePlan.ServicePlanDetails]);

        //service plan attributes
        CompanyAttribute[] companyAttributes2 = AttributeHelper.GetCompanyAttributes(servicePlan.Company, EntityType.ServicePlan);
        DataTable dtServicePlan = AttributeHelper.BuildServicePlanDataTable(servicePlan, companyAttributes2);
        doc.MailMerge.Execute(dtServicePlan);

        //NOTE: Repeatable Items must have a table name
        DataTable dtServiceVisits = ds.Tables[(int)MergeTablesServicePlan.ServiceVisits];
        dtServiceVisits.TableName = "ServiceVisits";
        doc.MailMerge.ExecuteWithRegions(dtServiceVisits);

        //Modified by Vilas Meshram: 05/21/2013 : Squish# 21329 : Add Additional Merge Fields for Service Plan and Survey Templates

        //NOTE: Repeatable Items must have a table name
        CompanyAttribute[] companyAttributes = AttributeHelper.GetCompanyAttributes(servicePlan.Company, EntityType.ServiceVisitObjective);
        //Objective[] objectives = Objective.GetSortedArray("Survey[CompanyID = ? && CreateStateCode != ? && CreateStateCode != ? && ServicePlanSurveys[ServicePlanID = ?]]", "Survey.DueDate ASC", servicePlan.CompanyID, CreateState.InProgress.Code, CreateState.ErrorCreating.Code, servicePlan.ID);
        //DataTable dtObjectives = AttributeHelper.BuildObjectivesDataTable(objectives, companyAttributes, true);
        DataTable dtObjectives = ds.Tables[(int)MergeTablesServicePlan.Objectives];
        dtObjectives.TableName = "Objectives";
        doc.MailMerge.ExecuteWithRegions(dtObjectives);

        //NOTE: Repeatable Items must have a table name
        //DataTable dtObjectives1 = ds.Tables[(int)MergeTablesServicePlan.Objectives];
        //dtObjectives1.TableName = "Objectives";
        //doc.MailMerge.ExecuteWithRegions(dtObjectives1);
        //Modified by Vilas Meshram: 05/21/2013 : Squish# 21329 : Add Additional Merge Fields for Service Plan and Survey Templates

        //NOTE: Repeatable Items must have a table name 
        DataTable dtComments = ds.Tables[(int)MergeTablesServicePlan.Comments];
        dtComments.TableName = "PlanComments";
        doc.MailMerge.ExecuteWithRegions(dtComments);



    }

    /// <summary>
    /// Uses Apose.Word to extract the predefined form fields from the merge doc.
    /// </summary>
    /// <param name="survey">Survey to be used to merge the fields.</param>
    public string Extract(Survey survey)
    {
        //first, identify the document being uploaded
        try
        {
            MergeDocument mergeDoc = MergeDocument.GetOne("ID = ?", doc.BuiltInDocumentProperties.Category);
            StringBuilder sb = new StringBuilder();

            if (mergeDoc != null)
            {
                //next do some validation
                foreach (Aspose.Words.Fields.FormField formField in doc.Range.FormFields)
                {
                    MergeDocumentField docField = MergeDocumentField.GetOne("MergeDocumentID = ? && FieldName = ? && IsReported = true", mergeDoc.ID, formField.Name);
                    if (docField != null)
                    {
                        if (docField.IsRequired)
                        {
                            if (String.IsNullOrEmpty(formField.Result) ||
                                (formField.Type == Aspose.Words.Fields.FieldType.FieldFormDropDown && formField.Result.ToUpper().Contains("PLEASE SELECT")) ||
                                (formField.Type == Aspose.Words.Fields.FieldType.FieldFormDropDown && formField.Result.ToUpper().Contains("SELECT ONE")))
                            {
                                sb.AppendFormat("- '{0}' is a required field.\r\n", docField.DisplayName);
                            }
                        }

                        if (!String.IsNullOrEmpty(formField.Result))
                        {
                            switch (docField.FormFieldDataType.DisplayName)
                            {
                                case "Date":
                                    try
                                    {
                                        DateTime.Parse(formField.Result);
                                    }
                                    catch
                                    {
                                        sb.AppendFormat("- '{0}' does not a valid date value.\r\n", docField.DisplayName);
                                    }
                                    break;

                                case "Money":
                                    try
                                    {
                                        string moneyValue = formField.Result.Replace("$", "");
                                        Decimal.Parse(moneyValue);
                                    }
                                    catch
                                    {
                                        sb.AppendFormat("- '{0}' does not a valid currency value.\r\n", docField.DisplayName);
                                    }
                                    break;

                                case "Number":
                                    try
                                    {
                                        Decimal.Parse(formField.Result);
                                    }
                                    catch
                                    {
                                        sb.AppendFormat("- '{0}' does not a valid number value.\r\n", docField.DisplayName);
                                    }
                                    break;
                            }
                        }
                    }
                }

                if (sb.Length > 0)
                {
                    sb.Insert(0, "Please correct the following and attach your document again:\r\n\r\n");
                    return sb.ToString();
                }

                //next, delete any prior saved responses for this document/survey
                //biz rule is that only one version of a document can be uploaded per survey
                StoredProcedures.Document_DeleteSurveyMergeDocumentFields(survey.ID, mergeDoc.ID);


                //last, save the field values
                foreach (Aspose.Words.Fields.FormField formField in doc.Range.FormFields)
                {
                    MergeDocumentField docField = MergeDocumentField.GetOne("MergeDocumentID = ? && FieldName = ? && IsReported = true", mergeDoc.ID, formField.Name);
                    if (docField != null)
                    {
                        string fieldValue = (formField.Result.Length >= 2000) ? formField.Result.Substring(0, 1999) : formField.Result;

                        StoredProcedures.Document_InsertSurveyMergeDocumentFields(
                            survey.ID,
                            mergeDoc.ID,
                            survey.CompanyID,
                            formField.Name,
                            fieldValue,
                            DateTime.Now);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error occured when extracting values from document form fields.  See inner exception for details.", ex);
        }

        return null;
    }

    public bool IsExtractable
    {
        get
        {
            //check if a Guid is stored in the category field of the doc
            try
            {
                Guid docID = new Guid(doc.BuiltInDocumentProperties.Category.Trim());
                return true;
            }
            catch
            {
                return false;
            }
        }
    }

    public Stream GetMergedDocument
    {
        get
        {
            MemoryStream result = new MemoryStream();
            doc.Save(result, (docExtension == ".DOCX") ? SaveFormat.Docx : SaveFormat.Doc);

            return result;
        }
    }
}
