using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Berkley.BLC.Web.Internal.Fields
{
    /// <summary>
    /// Validates if at least one radio button in a group is checked.
    /// </summary>
    public class RadioButtonRequiredFieldValidator : BaseValidator
    {
        private const string ClientEvalFunctionName = "RadioButtonRequiredFieldValidatorEvaluateIsValid";

        /// <summary>
        /// Validator Requirement
        /// </summary>
        /// <returns>True if dependencies are valid.</returns>
        protected override bool ControlPropertiesValid()
        {
            string controlID = this.ControlToValidate;
            if (controlID.Length == 0)
            {
                throw new HttpException(string.Format("The ControlToValidate property of '{0}' cannot be blank.", this.ID));
            }
            Control control = this.NamingContainer.FindControl(controlID);
            if (control == null)
            {
                throw new HttpException(string.Format("Unable to find control id '{0}' referenced by the 'ControlToValidate' property of '{1}'.", controlID, this.ID));
            }
            if (!(control is RadioButton))
            {
                throw new HttpException(string.Format("Control '{0}' referenced by the ControlToValidate property of '{1}' cannot be validated. Control must be RadioButton.", controlID, this.ID));
            }

            return true;
        }


        /// <summary>
        /// Validator Requirement
        /// </summary>
        /// <returns>True if ControlToValidate has one item or more selected.</returns>
        protected override bool EvaluateIsValid()
        {
            return this.EvaluateIsChecked();
        }


        /// <summary>
        /// Return true if an item in the list is selected.
        /// </summary>
        /// <returns>True if ControlToValidate has one item or more selected.</returns>
        protected bool EvaluateIsChecked()
        {
            RadioButton rdo = (RadioButton)this.NamingContainer.FindControl(this.ControlToValidate);
            if (rdo.Checked)
            {
                return true;
            }

            // see if another radio in the same group is checked
            return EvaluateIsCheckedRecursive(this.NamingContainer, rdo.GroupName);
        }


        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An EventArgs object that contains the event data.</param>
        /// <remarks>This method notifies the server control to perform any necessary prerendering steps prior to saving view state and rendering content.</remarks>
        protected override void OnPreRender(EventArgs e)
        {
            if (this.DetermineRenderUplevel() && this.EnableClientScript)
            {
                this.Attributes["evaluationfunction"] = ClientEvalFunctionName;
                this.RegisterClientScript();
            }
            else
            {
                this.Attributes.Remove("evaluationfunction");
            }

            base.OnPreRender(e);
        }


        /// <summary>
        /// Register the client script.
        /// </summary>
        protected void RegisterClientScript()
        {
            if (this.Page.ClientScript.IsClientScriptBlockRegistered(ClientEvalFunctionName))
            {
                return;
            }

            string script = @"
            <script language=""javascript"">
            function " + ClientEvalFunctionName + @"(val)
			{
				var rdo = document.getElementById(val.controltovalidate);
				var radioGroup = document.getElementsByName(rdo.name);
				if (radioGroup[0]) // radio group is an array (one radio is not an array)
				{
					if (!rdo.GroupHookup)
					{
						for (var i = 0; i < radioGroup.length; i++)
						{
							if (rdo != radioGroup[i]) ValidatorHookupControl(radioGroup[i], val);
						}
						rdo.GroupHookup = true;
					}
					for (var i = 0; i < radioGroup.length; i++)
					{
						if (radioGroup[i].checked)
						{
							return true;
						}
					}
				}
				else // only one radio in group
				{
					if (radioGroup.checked)
					{
						return true;
					}
				}
				return false;
			}
            </script>";

            this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), ClientEvalFunctionName, script);
        }


        private bool EvaluateIsCheckedRecursive(Control container, string groupName)
        {
            // traverse all child controls in the passed container looking for radio buttons in the specified group
            foreach (Control control in container.Controls)
            {
                if (control is RadioButton)
                {
                    RadioButton rdo = (RadioButton)control;
                    if (string.Compare(rdo.GroupName, groupName, true) == 0) // radio in group
                    {
                        if (rdo.Checked)
                        {
                            return true;
                        }
                    }
                }
                else // not a radio button
                {
                    if (control.Controls.Count > 0)
                    {
                        bool isChecked = EvaluateIsCheckedRecursive(control, groupName);
                        if (isChecked)
                        {
                            return true;
                        }
                    }
                }
            }

            // no checked radio found in the control hierarchy
            return false;
        }
    }
}
