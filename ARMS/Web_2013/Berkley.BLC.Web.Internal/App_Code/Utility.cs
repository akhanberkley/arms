using System;
using System.Collections;
using System.Collections.Specialized;
//using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;

using QCI.Web;
using QCI.ExceptionManagement;
using Berkley.BLC.Entities;

/// <summary>
/// Container for a number of utility methods used by the presentation layer.
/// </summary>
public class ARMSUtility
{
    private static string _appPath = null;

    /// <summary>
    /// Private constructor to prevent object instantiation of this class.
    /// </summary>
    private ARMSUtility() { }


    /// <summary>
    /// Returns the virtual path to this application, relative to the host web server, without a trailing slash (/).
    /// If the application is at the root of the web server, an empty string is returned.
    /// </summary>
    /// <returns>Virtual path to this application without a trailing slash.</returns>
    public static string AppPath
    {
        get
        {
            if (_appPath == null)
            {
                // get the virtual path to this web application
                _appPath = HttpContext.Current.Request.ApplicationPath.TrimEnd('/');
            }
            return _appPath;
        }
    }

    /// <summary>
    /// Gets the full URL path to the template folder.
    /// </summary>
    public static string TemplatePath
    {
        get { return AppPath + "/template"; }
    }

    /// <summary>
    /// Rebuilds the passed query string for navigating purposes.
    /// </summary>
    /// <returns>The query string, or empty string if none.</returns>
    public static string GetQueryStringForNav()
    {
        return GetQueryStringForNav(null);
    }

    /// <summary>
    /// Rebuilds the passed query string for navigating purposes.
    /// </summary>
    /// <returns>The query string, or empty string if none.</returns>
    public static string GetQueryStringForNav(string keyToRemove)
    {
        string result = string.Empty;

        System.Collections.Specialized.NameValueCollection collection = HttpContext.Current.Request.QueryString;
        String[] keyArray = collection.AllKeys;
        for (int i = 0; i < keyArray.Length; i++)
        {
            string key = keyArray[i];
            if (i <= 0)
            {
                result = "?";
            }

            if (key != null && key.Length > 0 && key != keyToRemove)
            {
                result += string.Format("{0}={1}&", key, HttpUtility.UrlEncode(collection.Get(key)));
            }

        }

        // strip any '&' off the end
        result = result.TrimEnd('&');

        return result;
    }

    public static string GetQueryStringForNav(string keyToRemove, string keyToRemove2)
    {
        string result = string.Empty;

        System.Collections.Specialized.NameValueCollection collection = HttpContext.Current.Request.QueryString;
        String[] keyArray = collection.AllKeys;
        for (int i = 0; i < keyArray.Length; i++)
        {
            string key = keyArray[i];
            if (i <= 0)
            {
                result = "?";
            }

            if (key != null && key.Length > 0 && key != keyToRemove && key != keyToRemove2)
            {
                result += string.Format("{0}={1}&", key, HttpUtility.UrlEncode(collection.Get(key)));
            }
        }

        // strip any '&' off the end
        result = result.TrimEnd('&');

        return result;
    }

    ///// <summary>
    ///// Rebuilds the passed query string for navigating purposes.
    ///// </summary>
    ///// <returns>The query string, or empty string if none.</returns>
    //public static string GetQueryStringForNav(List<string> keysToRemove)
    //{
    //    string result = string.Empty;
        
    //    foreach (string key in keysToRemove)
    //    {
    //        result = GetQueryStringForNav(key);
    //    }

    //    return result;
    //}


    /// <summary>
    /// Gets a string value from the query string of the current HttpRequest object.
    /// </summary>
    /// <param name="name">Name of the query string value to fetch.</param>
    /// <param name="required">Flag value indicating whether the value is required.</param>
    /// <returns>Value found on the query string, or null if none was specified.</returns>
    public static string GetStringFromQueryString(string name, bool required)
    {
        string value = HttpContext.Current.Request.QueryString[name];
        if (value == null || value.Length == 0)
        {
            if (required)
            {
                throw new InvalidQueryStringException(name, "Value must be specified.");
            }
            else
            {
                return null;
            }
        }
        else // value specified
        {
            return value;
        }
    }

    /// <summary>
    /// Gets a Guid value from the query string of the current HttpRequest object.
    /// </summary>
    /// <param name="name">Name of the query string value to fetch.</param>
    /// <param name="required">Flag value indicating whether the value is required.</param>
    /// <returns>Value found on the query string, or Guid.empty if none was specified.</returns>
    public static Guid GetGuidFromQueryString(string name, bool required)
    {
        string value = HttpContext.Current.Request.QueryString[name];
        if (value == null || value.Length == 0)
        {
            if (required)
            {
                throw new InvalidQueryStringException(name, "Value must be specified.");
            }
            else
            {
                return Guid.Empty;
            }
        }
        else // value specified
        {
            try
            {
                return new Guid(value);
            }
            catch
            {
                throw new InvalidQueryStringException(name, value, "Failed to parse value.");
            }
        }
    }

    /// <summary>
    /// Gets an integer value from the query string of the current HttpRequest object.
    /// </summary>
    /// <param name="name">Name of the query string value to fetch.</param>
    /// <param name="required">Flag value indicating whether the value is required.</param>
    /// <returns>Value found on the query string, or int.MinValue if none was specified.</returns>
    public static int GetIntFromQueryString(string name, bool required)
    {
        string value = HttpContext.Current.Request.QueryString[name];
        if (value == null || value.Length == 0)
        {
            if (required)
            {
                throw new InvalidQueryStringException(name, "Value must be specified.");
            }
            else
            {
                return int.MinValue;
            }
        }
        else // value specified
        {
            try
            {
                return int.Parse(value);
            }
            catch
            {
                throw new InvalidQueryStringException(name, value, "Failed to parse value.");
            }
        }
    }

    /// <summary>
    /// Gets a boolean value from the query string of the current HttpRequest object.
    /// </summary>
    /// <param name="name">Name of the query string value to fetch.</param>
    /// <param name="required">Flag value indicating whether the value is required.</param>
    /// <returns>Value found on the query string, or int.MinValue if none was specified.</returns>
    public static bool GetBoolFromQueryString(string name, bool required)
    {
        string value = HttpContext.Current.Request.QueryString[name];
        if (value == null || value.Length == 0)
        {
            if (required)
            {
                throw new InvalidQueryStringException(name, "Value must be specified.");
            }
            else
            {
                return false;
            }
        }
        else // value specified
        {
            try
            {
                int tmp = int.Parse(value);
                return (tmp == 1) ? true : false;
            }
            catch
            {
                throw new InvalidQueryStringException(name, value, "Failed to parse value.");
            }
        }
    }

    /// <summary>
    /// Gets an date value (without time) from the query string of the current HttpRequest object.
    /// </summary>
    /// <param name="name">Name of the query string value to fetch.</param>
    /// <param name="required">Flag value indicating whether the value is required.</param>
    /// <returns>Value found on the query string, or DateTime.MinValue if none was specified.</returns>
    public static DateTime GetDateFromQueryString(string name, bool required)
    {
        string value = HttpContext.Current.Request.QueryString[name];
        if (value == null || value.Length == 0)
        {
            if (required)
            {
                throw new InvalidQueryStringException(name, "Value must be specified.");
            }
            else
            {
                return DateTime.MinValue;
            }
        }
        else // value specified
        {
            try
            {
                return DateTime.Parse(value).Date;
            }
            catch
            {
                throw new InvalidQueryStringException(name, value, "Failed to parse value.");
            }
        }
    }


    /// <summary>
    /// Custom DataGrid event handler which formats the page links on the Pager row.
    /// </summary>
    /// <param name="sender">Reference to the object calling this event.</param>
    /// <param name="e">A DataGridItemEventArgs that contains event data.</param>
    public static void PagingGrid_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        // only process the pager row
        if (e.Item.ItemType != ListItemType.Pager)
        {
            return;
        }

        // The pager row has the following layout:
        // <tr><td colspan=x> ... links... </td></tr>

        // get the <td> tag/control containing the page links
        // and modify the pager links so they have the format of "[ 1 ] Page 2 [ 3 ] [ 4 ] [ 5 ] [...]"
        TableCell pagerCell = (TableCell)e.Item.Controls[0];
        int count = pagerCell.Controls.Count;
        for (int i = 0; i < count; i += 2)
        {
            LinkButton pageBtn = (pagerCell.Controls[i] as LinkButton);
            if (pageBtn != null) // page link
            {
                pageBtn.Text = "[" + pageBtn.Text + "]";
            }
            else // not a page link, has to be the static page label
            {
                Label pageLabel = (Label)pagerCell.Controls[i];
                pageLabel.Text = "Page " + pageLabel.Text;
            }
        }
    }


    /// <summary>
    /// Combines an array of values into a single comma-delimited string.
    /// </summary>
    /// <param name="values">Values to be combined.</param>
    /// <returns>Comma-delimited string of values.</returns>
    public static string ArrayToString(object[] values)
    {
        return ArrayToString(values, ",");
    }

    /// <summary>
    /// Combines an array of values into a single string, delimited with the specified seperator.
    /// </summary>
    /// <param name="values">Values to be combined.</param>
    /// <param name="seperator">Delimiter to use between values.</param>
    /// <returns>Delimited string of values.</returns>
    public static string ArrayToString(object[] values, string seperator)
    {
        StringBuilder sb = new StringBuilder(100);
        int count = values.Length;
        for (int i = 0; i < count; i++)
        {
            if (i > 0) sb.Append(seperator);
            sb.Append(values[i]);
        }

        return sb.ToString();
    }


    /// <summary>
    /// Converts a column in a DataTable to an array of values of the specified type.
    /// </summary>
    /// <param name="dt">DataTable containing the column to evaluate.</param>
    /// <param name="columnName">Name of the column.</param>
    /// <param name="type">Type of the elements in the return array.</param>
    /// <returns>An array of the specified type, filled with values taken from the DataTable.</returns>
    public static Array DataColumnToArray(DataTable dt, string columnName, Type type)
    {
        int colIndex = dt.Columns.IndexOf(columnName);

        int count = dt.Rows.Count;
        ArrayList items = new ArrayList(count);
        for (int i = 0; i < count; i++)
        {
            items.Add(dt.Rows[i][colIndex]);
        }

        return items.ToArray(type);
    }


    /// <summary>
    /// Evaluates data-binding expressions at runtime (using Databinder.Eval method) and returns
    /// an image tag setup to display a graphic of the boolean result from the evaluation.
    /// </summary>
    /// <param name="container">The object reference against which the expression is evaluated.</param>
    /// <param name="expression">The navigation path from the container to the property value to be used to determine the graphic used. This must be a string type of property or field names separated by dots, such as Tables[0].DefaultView.[0].Price.</param>
    /// <param name="useXMark">A value indicating if an X-Mark should be used for 'false' values, rather than the default clear graphic.</param>
    /// <returns>An HTML image tag that will display a graphic of the result from the evaluation of the data-binding expression.</returns>
    public static string GetCheckImageTag(object container, string expression, bool useXMark)
    {
        return string.Format("<img src=\"{0}\" />", GetCheckboxImageUrl(container, expression, useXMark));
    }

    /// <summary>
    /// Evaluates data-binding expressions at runtime (using Databinder.Eval method) and returns
    /// an image tag setup to display a graphic of the boolean result from the evaluation.
    /// </summary>
    /// <param name="isChecked">Value indicating which type of graphic to return.</param>
    /// <param name="useXMark">A value indicating if an X-Mark should be used for 'false' values, rather than the default clear graphic.</param>
    /// <returns>An HTML image tag that will display a graphic of the result from the evaluation of the data-binding expression.</returns>
    public static string GetCheckImageTag(bool isChecked, bool useXMark)
    {
        return string.Format("<img src=\"{0}\" />", GetCheckboxImageUrl(isChecked, useXMark));
    }


    /// <summary>
    /// Evaluates data-binding expressions at runtime (using Databinder.Eval method) and returns
    /// the url of a graphic that represents the boolean result from the evaluation.
    /// </summary>
    /// <param name="container">The object reference against which the expression is evaluated.</param>
    /// <param name="expression">The navigation path from the container to the property value to be used to determine the graphic used. This must be a string type of property or field names separated by dots, such as Tables[0].DefaultView.[0].Price.</param>
    /// <returns>An HTML image tag that will display a graphic of the result from the evaluation of the data-binding expression.</returns>
    public static string GetCheckboxImageUrl(object container, string expression)
    {
        return GetCheckboxImageUrl(container, expression, false);
    }

    /// <summary>
    /// Evaluates data-binding expressions at runtime (using Databinder.Eval method) and returns
    /// the url of a graphic that represents the boolean result from the evaluation.
    /// </summary>
    /// <param name="container">The object reference against which the expression is evaluated.</param>
    /// <param name="expression">The navigation path from the container to the property value to be used to determine the graphic used. This must be a string type of property or field names separated by dots, such as Tables[0].DefaultView.[0].Price.</param>
    /// <param name="useXMark">A value indicating if an X-Mark should be used for 'false' values, rather than the default clear graphic.</param>
    /// <returns>An HTML image tag that will display a graphic of the result from the evaluation of the data-binding expression.</returns>
    /// <returns>A URL to the graphic to use.</returns>
    public static string GetCheckboxImageUrl(object container, string expression, bool useXMark)
    {
        object value = DataBinder.Eval(container, expression);
        bool isChecked = (value != null && value != DBNull.Value && (bool)value);
        return GetCheckboxImageUrl(isChecked, useXMark);
    }

    /// <summary>
    /// Returns the url of a graphic that represents the boolean value passed.
    /// </summary>
    /// <param name="isChecked">Value indicating which type of graphic to return.</param>
    /// <returns>A URL to the graphic to use.</returns>
    public static string GetCheckboxImageUrl(bool isChecked)
    {
        return GetCheckboxImageUrl(isChecked, false);
    }

    /// <summary>
    /// Returns the url of a graphic that represents the boolean value passed.
    /// </summary>
    /// <param name="isChecked">Value indicating which type of graphic to return.</param>
    /// <param name="useXMark">Value indicating if an X-Mark should be used for 'false' values, rather than the default clear graphic.</param>
    /// <returns>A URL to the graphic to use.</returns>
    public static string GetCheckboxImageUrl(bool isChecked, bool useXMark)
    {
        string url;
        if (isChecked)
        {
            url = "/images/check.gif";
        }
        else
        {
            url = (useXMark) ? "/images/xmark.gif" : "/images/check_blank.gif";
        }
        return AppPath + url;
    }


    /// <summary>
    /// Adds a script file registration line to the page.
    /// </summary>
    /// <param name="page">Instance of the page to modify.</param>
    /// <param name="fileUrl">URL to the script file.</param>
    public static void AddJavaScriptToPage(Page page, string fileUrl)
    {
        fileUrl = fileUrl.ToLower();
        if (!ScriptHelper.IsClientScriptRegistered(page, fileUrl))
        {
            string script = string.Format("<script language=\"javascript\" type=\"text/javascript\" src=\"{0}\"></script>", fileUrl);
            ScriptHelper.RegisterClientScriptBlock(page, fileUrl, script, true);
        }
    }


    /// <summary>
    /// Adds a javascript to the passed page to cause a report to load in a new window when the page is loaded.
    /// WARNING: This script will likely get blocked by pop-up blockers.  Have a backup plan.
    /// </summary>
    /// <param name="page">Page to which the script will be added.</param>
    /// <param name="url">URL to the report to load in the new window.</param>
    public static void AddScriptToOpenReport(Page page, string url)
    {
        url = JavaScript.EncodeStringForJavaScript(url);

        string script = @"
			<script language=""javascript"">
			function ShowReport()
			{
				var reportWindow = window.open('" + url + @"', 'ReportViewer', 'resizable=1,location=0,menubar=0,toolbar=0,status=0');
				if (reportWindow) reportWindow.focus();
			}
			ShowReport();
			</script>";

        page.ClientScript.RegisterStartupScript(page.GetType(), "OpenReport", script);
    }



    /// <summary>
    /// Returns the URL to the default (home) page for the security role passed.
    /// </summary>
    /// <param name="securityRoleCode">Security role code to evaluate.</param>
    /// <returns>URL to default page.</returns>
    public static string GetDefaultPageUrl(User user)
    {
        string url = null;

        if (user.Role.PermissionSet.CanViewCompanySummary)
            url = "/CompanySummary.aspx";
        else
            url = "/MySurveys.aspx";

        return AppPath + url;
    }


    /// <summary>
    /// Sends the specified email using the configured SMTP server.
    /// This message traps any errors and publishes a detailed exception.
    /// </summary>
    /// <param name="msg">Mail message to be sent.</param>
    public static void SendEmail(MailMessage msg)
    {
        string smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
        if (smtpServer == null) throw new Exception("Configuration setting 'SmtpServer' must be specified in the application config file.");

        try
        {
            SmtpClient smtp = new SmtpClient(smtpServer);
            smtp.Send(msg);
        }
        catch (Exception ex)
        {
            // dump contents of email into collection for inclusion in published exception
            NameValueCollection emailInfo = new NameValueCollection();
            emailInfo.Add("From", msg.From.Address);
            emailInfo.Add("To", msg.To[0].ToString());
            emailInfo.Add("Subject", msg.Subject);
            emailInfo.Add("Body", msg.Body);

            // log the exception
            //ExceptionManager.Publish(ex, emailInfo);
            throw (ex);
        }
    }


    /// <summary>
    /// Tests the passed IP and determines if it falls within the specified range.
    /// </summary>
    /// <param name="ipAddress">Address to be tested.</param>
    /// <param name="minIPAddress">Address at the beginning of the IP range.</param>
    /// <param name="maxIPAddress">Address at the end of the IP range.</param>
    /// <returns>True if the address is in the specified range.</returns>
    public static bool IsAddressInRange(string ipAddress, string minIPAddress, string maxIPAddress)
    {
        if (ipAddress == null) throw new ArgumentNullException("ipAddress");
        if (minIPAddress == null) throw new ArgumentNullException("minIPAddress");
        if (maxIPAddress == null) throw new ArgumentNullException("maxIPAddress");

        long minIP = ConvertIPAddressToInt64(minIPAddress);
        long maxIP = ConvertIPAddressToInt64(maxIPAddress);
        long ip = ConvertIPAddressToInt64(ipAddress);

        return (minIP <= ip && ip <= maxIP);
    }

    /// <summary>
    /// Converts an IP Address to a long integer value to allow simple comparisons.
    /// </summary>
    /// <param name="ip">Value of the IP Address to convert.</param>
    /// <returns>Integer value of the specified IP.</returns>
    public static long ConvertIPAddressToInt64(string ip)
    {
        if (!Regex.IsMatch(ip, @"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"))
        {
            throw new ArgumentException("Value '" + ip + "' is not a valid IP Address.");
        }

        string[] segments = ip.Split('.');
        long result = 0;
        for (int i = 0; i < segments.Length; i++)
        {
            result <<= 8;
            result += int.Parse(segments[i]);
        }

        return result;
    }

    /// <summary>
    /// Sets the cursor to the specified field.
    /// </summary>
    /// <param name="field">The field to set the focus to.</param>
    public static void SetInputFocus(Control control)
    {
        string id = control.ID;

        control.ID = string.Format("{0}_txt", id);
        JavaScript.SetInputFocus(control);
        control.ID = id;
    }

    public static bool EmailRegex(string emailAddress)
    {
        string patternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
              + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
              + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
              + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
              + @"[a-zA-Z]{2,}))$";
        Regex reg = new Regex(patternStrict);

        return reg.IsMatch(emailAddress);
    }

    /// <summary>
    /// Returns a substring of the text provided with a trailing "...".
    /// </summary>
    /// <param name="text">The text to trim.</param>
    /// <param name="maxCharCount">The max character count.</param>
    public static string GetSubstring(string text, int maxCharCount)
    {
        string result = string.Empty;

        if (!string.IsNullOrEmpty(text) && text.Length > maxCharCount)
        {
            result = text.Substring(0, maxCharCount) + "...";
        }
        else
        {
            result = text;
        }

        return result;
    }
}
