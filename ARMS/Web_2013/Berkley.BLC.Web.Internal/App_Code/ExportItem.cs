using System;
using System.Reflection;
using Berkley.BLC.Entities;

/// <summary>
/// Summary description for ExportItem.
/// </summary>
public class ExportItem : IComparable
{
    private string mstrSortExpression;
    private string mstrFilterName;
    private string mstrDataType;
    private string mstrHeaderName;
    private bool mfIsReminder = false;
    private PropertyInfo moPropertyInfo;
    private object moReferencedObject;
    private int miDisplayOrder = -1;

    public ExportItem(System.Guid oColumnId, string strSortExpression, string strFilterName, string strDataType, string strHeaderName, int iDisplayOrder)
    {
        mstrSortExpression = strSortExpression;
        mstrFilterName = strFilterName;
        mstrDataType = strDataType;
        mstrHeaderName = strHeaderName;
        miDisplayOrder = iDisplayOrder;
        if (oColumnId == GridColumnFilter.Reminder.ID)
        {
            mfIsReminder = true;
        }
    }

    public bool IsReminder
    {
        get { return mfIsReminder; }
    }

    public int DisplayOrder
    {
        get { return miDisplayOrder; }
        set { miDisplayOrder = value; }
    }

    public string SortExpression
    {
        get { return mstrSortExpression; }
        set { mstrSortExpression = value; }
    }

    public string FilterName
    {
        get { return mstrFilterName; }
        set { mstrFilterName = value; }
    }

    public string HeaderName
    {
        get { return mstrHeaderName; }
        set { mstrHeaderName = value; }
    }

    public string DataType
    {
        get { return mstrDataType; }
        set { mstrDataType = value; }
    }

    public object ReferencedObject
    {
        get { return moReferencedObject; }
        set { moReferencedObject = value; }
    }

    public PropertyInfo PropertyInfo
    {
        get { return moPropertyInfo; }
        set { moPropertyInfo = value; }
    }

    #region IComparable Members

    public int CompareTo(object oThat)
    {
        // TODO:  Add ExportItem.CompareTo implementation
        if (oThat != null && oThat.GetType() == typeof(ExportItem))
        {
            ExportItem oThatExport = oThat as ExportItem;
            return this.miDisplayOrder.CompareTo(oThatExport.miDisplayOrder);
        }
        return 0;
    }

    #endregion
}
