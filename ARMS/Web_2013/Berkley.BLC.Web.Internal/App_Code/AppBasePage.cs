using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using QCI.Web;
using QCI.Web.Validation;
using Berkley.BLC.Entities;
using Entities = Berkley.BLC.Entities;

/// <summary>
/// Summary description for AppBasePage
/// </summary>
public class AppBasePage : UIMapperBasePage
{
    private static string _appPath = null;
    private static string _copyright = null;
    private static Hashtable _pages = Hashtable.Synchronized(new Hashtable());

    private string _pagePath = null;
    private string _pageHeader = null;
    private User _currentUser = null;

    protected override void OnLoad(EventArgs e)
    {
        WebPage page = this.WebPage;

        // see if user has permission to access the page
        if (!CanUserAccessPage(this.UserIdentity, page))
        {
            Security.RedirectToLoginPage();
        }

        // set the page header
        _pageHeader = page.Name;
        if (page.IsAddEditPage)
        {
            string id = this.Request["id"];
            bool isAdd = (id == null || id.Length == 0);
            _pageHeader = _pageHeader.Replace("Add/Edit", (isAdd ? "Add" : "Edit"));
        }

        if (this.Page.Header != null)
        {
            //Inject Reference to JQuery
            System.Web.UI.HtmlControls.HtmlGenericControl script = new System.Web.UI.HtmlControls.HtmlGenericControl("script");
            script.Attributes.Add("type", "text/javascript");
            script.Attributes.Add("src", string.Format("{0}/javascripts/jquery-1.4.2.js", ARMSUtility.TemplatePath));
            this.Page.Header.Controls.Add(script);
        }
        
        // now let the base run it's normal OnLoad
        base.OnLoad(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (this.WebPage.NoCachingOnBrowser)
        {
            //Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
        }

        // now let the base run it's normal OnPreRender
        base.OnPreRender(e);
    }

    protected Dictionary<string, SortDirection> ColumnSortDirection
    {
        get
        {
            if (ViewState["columnsortdirection"] == null)
                ViewState["columnsortdirection"] = new Dictionary<string, SortDirection>();

            return (Dictionary<string, SortDirection>)ViewState["columnsortdirection"];
        }
        set
        {
            ViewState["columnsortdirection"] = value;
        }
    }

    /// <summary>
    /// HTML-encodes a string for display to a user on a web page.
    /// </summary>
    /// <param name="value">Value to be HTML-encoded.</param>
    /// <returns>An HTML-encoded string.</returns>
    public static string HtmlEncode(string value)
    {
        return HttpUtility.HtmlEncode(value);
    }

    /// <summary>
    /// Evaluates data-binding expressions at runtime (using Databinder.Eval method) and returns the result as HTML-encoded text to be displayed in the requesting browser.
    /// </summary>
    /// <param name="container">The object reference against which the expression is evaluated.</param>
    /// <param name="expression">The navigation path from the container to the property value to be placed in the bound control property. This must be a string type of property or field names separated by dots, such as Tables[0].DefaultView.[0].Price.</param>
    /// <returns>An HTML-encoded string that results from the evaluation of the data-binding expression.</returns>
    public static string HtmlEncode(object container, string expression)
    {
        return HtmlEncode(container, expression, "{0}");
    }

    /// <summary>
    /// Evaluates data-binding expressions at runtime (using Databinder.Eval method) and formats the result as HTML-encoded text to be displayed in the requesting browser.
    /// </summary>
    /// <param name="container">The object reference against which the expression is evaluated.</param>
    /// <param name="expression">The navigation path from the container to the property value to be placed in the bound control property. This must be a string type of property or field names separated by dots, such as Tables[0].DefaultView.[0].Price.</param>
    /// <param name="format">A .NET Framework format string, similar to those used by String.Format, that converts the Object (which results from the evaluation of the data-binding expression) to a String that can be displayed by the requesting browser.</param>
    /// <returns>An HTML-encoded string that results from the evaluation of the data-binding expression.</returns>
    public static string HtmlEncode(object container, string expression, string format)
    {
        string value = DataBinder.Eval(container, expression, format);
        return HtmlEncode(value);
    }


    /// <summary>
    /// HTML-encodes a string for display to a user on a web page.
    /// </summary>
    /// <param name="value">Value to be HTML-encoded.</param>
    /// <param name="nullValue">The value that represents a null value.</param>
    /// <param name="nullText">The text value to return if the expression value matches the nullValue provided.</param>
    /// <returns>An HTML-encoded string.</returns>
    public static string HtmlEncodeNullable(object value, object nullValue, string nullText)
    {
        string result;
        if (value == null && nullValue == null || value != null && value.Equals(nullValue))
        {
            result = nullText;
        }
        else
        {
            result = (value != null) ? value.ToString() : string.Empty;
        }
        return HtmlEncode(result);
    }

    /// <summary>
    /// Evaluates data-binding expressions at runtime (using Databinder.Eval method) and returns the result as HTML-encoded text to be displayed in the requesting browser.
    /// </summary>
    /// <param name="container">The object reference against which the expression is evaluated.</param>
    /// <param name="expression">The navigation path from the container to the property value to be placed in the bound control property. This must be a string type of property or field names separated by dots, such as Tables[0].DefaultView.[0].Price.</param>
    /// <param name="nullValue">The value that represents a null value.</param>
    /// <param name="nullText">The text value to return if the expression value matches the nullValue provided.</param>
    /// <returns>An HTML-encoded string that results from the evaluation of the data-binding expression.</returns>
    public static string HtmlEncodeNullable(object container, string expression, object nullValue, string nullText)
    {
        object value = DataBinder.Eval(container, expression);
        return HtmlEncodeNullable(value, nullValue, nullText);
    }

    /// <summary>
    /// Evaluates data-binding expressions at runtime (using Databinder.Eval method) and returns the result as HTML-encoded text to be displayed in the requesting browser.
    /// </summary>
    /// <param name="container">The object reference against which the expression is evaluated.</param>
    /// <param name="expression">The navigation path from the container to the property value to be placed in the bound control property. This must be a string type of property or field names separated by dots, such as Tables[0].DefaultView.[0].Price.</param>
    /// <param name="format">A .NET Framework format string, similar to those used by String.Format, that converts the Object (which results from the evaluation of the data-binding expression) to a String that can be displayed by the requesting browser.</param>
    /// <param name="nullValue">The value that represents a null value.</param>
    /// <param name="nullText">The text value to return if the expression value matches the nullValue provided.</param>
    /// <returns>An HTML-encoded string that results from the evaluation of the data-binding expression.</returns>
    public static string HtmlEncodeNullable(object container, string expression, string format, object nullValue, string nullText)
    {
        object value = DataBinder.Eval(container, expression);
        string result;
        if (value == null && nullValue == null || value != null && value.Equals(nullValue))
        {
            result = nullText;
        }
        else // non-null
        {
            result = (value != null) ? string.Format(format, value) : string.Empty;
        }
        return HtmlEncode(result);
    }


    /// <summary>
    /// Gets the full URL path to the application root, relative to the server root.
    /// </summary>
    public static string AppPath
    {
        get
        {
            if (_appPath == null)
            {
                // get the root virtual path to this web application
                _appPath = HttpContext.Current.Request.ApplicationPath;
                if (_appPath == "/") _appPath = string.Empty;
            }
            return _appPath;
        }
    }

    /// <summary>
    /// Gets the copyright information from the assembly.
    /// The value returned is HTML-encoded and is intented to be added to the metadata for the page.
    /// </summary>
    public static string AppCopyright
    {
        get
        {
            if (_copyright == null)
            {
                // get the copyright information from this assembly
                Assembly assembly = Assembly.Load("App_Code");
                AssemblyCopyrightAttribute copyrightAttrib = (AssemblyCopyrightAttribute)Attribute.GetCustomAttribute(assembly, typeof(AssemblyCopyrightAttribute));
                _copyright = HttpUtility.HtmlEncode(copyrightAttrib.Copyright);
            }
            return _copyright;
        }
    }


    /// <summary>
    /// Gets the full URL path to the template folder.
    /// </summary>
    public string TemplatePath
    {
        get { return AppPath + "/template"; }
    }

    /// <summary>
    /// Gets the path to this page, relative to the application root.
    /// </summary>
    public string PagePath
    {
        get
        {
            if (_pagePath == null)
            {
                _pagePath = Request.Path.Substring(AppPath.Length);
            }
            return _pagePath;
        }
    }

    /// <summary>
    /// Gets the WebPage business entity that represents this page.
    /// </summary>
    public WebPage WebPage
    {
        get
        {
            WebPage page = (WebPage)_pages[this.PagePath];
            if (page == null)
            {
                page = WebPage.Get(this.PagePath);
                _pages[this.PagePath] = page;
            }
            return page;
        }
    }

    /// <summary>
    /// Gets the HTML-encoded title of this page.
    /// </summary>
    public string PageTitle
    {
        get
        {
            return Server.HtmlEncode("ARMS - ") + this.PageHeading;
        }
    }

    /// <summary>
    /// Gets or sets the HTML-encoded heading of this page.
    /// NOTE: When setting this property do not HTML-encode the value.
    /// </summary>
    public string PageHeading
    {
        get { return Server.HtmlEncode(_pageHeader); }
        set { _pageHeader = value; }
    }

    /// <summary>
    /// Gets the current user identity making the request.
    /// </summary>
    public UserIdentity UserIdentity
    {
        get { return (this.User.Identity as UserIdentity); }
    }

    /// <summary>
    /// Gets the current user making the request.
    /// </summary>
    public Entities.User CurrentUser
    {
        get
        {
            if (_currentUser == null)
            {
                _currentUser = Entities.User.GetOne("ID = ?", this.UserIdentity.UserID);
            }
            return _currentUser;
        }
    }


    /// <summary>
    /// Builds the same validation summary message presented to clients on IE
    /// and adds a JavaScript to the page so it is displayed when the page is loaded.
    /// </summary>
    public void ShowValidatorErrors()
    {
        ValidationSummary summary = FindValidationSummary(this.Controls);
        if (summary == null)
        {
            throw new Exception("Page does not contain a ValidationSummary control.");
        }
        JavaScript.ShowValidationSummaryMessage(summary);
    }


    private ValidationSummary FindValidationSummary(ControlCollection controls)
    {
        // do a post-tree traversal of the control hierarchy
        ValidationSummary result = null;

        for (int i = controls.Count - 1; i >= 0; i--)
        {
            Control control = controls[i];
            if (control is ValidationSummary)
            {
                result = (ValidationSummary)control;
                break;
            }
            else
            {
                // traverse into the child controls
                result = FindValidationSummary(control.Controls);
                if (result != null)
                {
                    break;
                }
            }
        }

        return result;
    }


    /// <summary>
    /// Verifies the specified user is assigned a securty role that can access this page based on the roles assigned to the page.
    /// </summary>
    private static bool CanUserAccessPage(UserIdentity user, WebPage page)
    {
        if (!page.IsLoginRequired)
        {
            return true;
        }

        switch (page.Path)
        {
            case "/AdminAssignmentRules.aspx":
            case "/AdminAssignmentRuleEdit.aspx":
                {
                    return user.Permissions.CanAdminRules;
                }
            case "/Administration.aspx":
                {
                    return user.Permissions.CanViewAdminPage;
                }
            case "/AdminClients.aspx":
                {
                    return user.Permissions.CanAdminClients;
                }
            case "/AdminLetters.aspx":
                {
                    return user.Permissions.CanAdminLetters;
                }
            case "/AdminReports.aspx":
                {
                    return user.Permissions.CanAdminReports;
                }
            case "/AdminDocuments.aspx":
                {
                    return user.Permissions.CanAdminDocuments;
                }
            case "/AdminDocumentEdit.aspx":
            case "/AdminDocumentExtraction.aspx":
                {
                    return (user.Permissions.CanAdminLetters || user.Permissions.CanAdminReports || user.Permissions.CanAdminDocuments);
                }
            case "/AdminHelpfulHint.aspx":
                {
                    return user.Permissions.CanAdminHelpfulHints;
                }
            case "/AdminLinks.aspx":
            case "/AdminLinkEdit.aspx":
                {
                    return user.Permissions.CanAdminLinks;
                }
            case "/AdminActivityTimes.aspx":
            case "/AdminActivityTimeEdit.aspx":
                {
                    return user.Permissions.CanAdminActivityTimes;
                }
            case "/AdminRecs.aspx":
            case "/AdminRecEdit.aspx":
                {
                    return user.Permissions.CanAdminRecommendations;
                }
            case "/AdminServicePlanFields.aspx":
            case "/AdminServicePlanFieldEdit.aspx":
                {
                    return user.Permissions.CanAdminServicePlanFields;
                }
            case "/AdminUser.aspx":
            case "/AdminUserEdit.aspx":
            case "/AdminUsers.aspx":
                {
                    return user.Permissions.CanAdminUserAccounts;
                }
            case "/AdminFeeCompanies.aspx":
            case "/AdminFeeUserEdit.aspx":
            case "/AdminFeeUsers.aspx":
                {
                    return user.Permissions.CanAdminFeeCompanies;
                }
            case "/AdminResultsDisplay.aspx":
                {
                    return user.Permissions.CanAdminResultsDisplay;
                }
            case "/AdminRoleEdit.aspx":
            case "/AdminRoles.aspx":
                {
                    return user.Permissions.CanAdminUserRoles;
                }
            case "/AdminTerritories.aspx":
            case "/AdminTerritoryEdit.aspx":
            case "/AdminTerritoryEditArea.aspx":
                {
                    return user.Permissions.CanAdminTerritories;
                }
            case "/AdminUnderwriterEdit.aspx":
            case "/AdminUnderwriters.aspx":
                {
                    return user.Permissions.CanAdminUnderwriters;
                }
            case "/CompanySummary.aspx":
                {
                    return user.Permissions.CanViewCompanySummary;
                }
            case "/CreateServicePlan.aspx":
            case "/CreateServicePlanSearch.aspx":
            case "/CreateServicePlanObjective.aspx":
                {
                    return user.Permissions.CanCreateServicePlans;
                }
            case "/CreateServicePlanVisit.aspx":
            case "/CreateServicePlanLocation.aspx":
            case "/CreateServicePlanPolicy.aspx":
                {
                  return (user.Permissions.CanCreateServicePlans || user.Permissions.CanViewServicePlansPage);
                }
            case "/CreateSurvey.aspx":
            case "/CreateSurveyBuilding.aspx":
            case "/CreateSurveyCoverage.aspx":
            case "/CreateSurveyLocation.aspx":
            case "/CreateSurveyPolicy.aspx":
            case "/CreateSurveySearch.aspx":
            case "/Contact.aspx":
            case "/Test.aspx":
                {
                    return true; // all users can create survey requests
                }
            case "/ContactManager.aspx":
                {
                    return (user.Permissions.CanWorkSurveys || user.Permissions.CanWorkReviews);
                }
            case "/Document.aspx":
                {
                    return true; // all users can view docs
                }
            case "/DocumentRetriever.aspx":
                {
                    return user.Permissions.CanViewMySurveysPage;
                }
            case "/Insured.aspx":
                {
                    return true; // allow all logged in users
                }
            case "/Links.aspx":            
                {
                    return user.Permissions.CanViewLinks;
                }
            case "/AgencyVisits.aspx":
            case "/AgencyVisitEdit.aspx":
                {
                    return user.Permissions.CanWorkAgencyVisits;
                }
            case "/Locations.aspx":
                {
                    return (user.Permissions.CanWorkSurveys || user.Permissions.CanWorkReviews);
                }
            case "/Login.aspx":
            case "/Error.aspx":
                {
                    return true; // login not required.
                }
            case "/Logout.aspx":
                {
                    return true; // login not required.
                }
            case "/Map.aspx":
                {
                    return user.Permissions.CanViewMySurveysPage;
                }
            case "/MapOvi.aspx":
                {
                   return user.Permissions.CanViewMySurveysPage;
                }
            case "/MySurveys.aspx":
                {
                    return user.Permissions.CanViewMySurveysPage;
                }
            case "/Objectives.aspx":
            case "/ManagePlanObjectives.aspx":
            case "/ObjectiveEdit.aspx":
                {
                    return (user.Permissions.CanWorkSurveys || user.Permissions.CanWorkReviews);
                }
            case "/Recommendations.aspx":
            case "/RecommendationEdit.aspx":
                {
                    return (user.Permissions.CanWorkSurveys || user.Permissions.CanWorkReviews || user.IsUnderwriter);
                }
            case "/ReadOnlySurvey.aspx":
                {
                    return true; // login not required.
                }
            case "/Search.aspx":
                {
                    return user.Permissions.CanSearchSurveys;
                }
            case "/SearchResults.aspx":
                {
                    return user.Permissions.CanSearchSurveys;
                }
            case "/ServicePlans.aspx":
            case "/ServicePlan.aspx":
            case "/ServicePlanDetails.aspx":
                {
                    return user.Permissions.CanViewServicePlansPage;
                }
            case "/ServicePlanActivities.aspx":
            case "/ServicePlanActivityEdit.aspx":
                {
                    return user.Permissions.CanWorkServicePlans;
                }
            case "/Survey.aspx":
            case "/SurveyReview.aspx":
            case "/SurveyPrintView.aspx":
            case "/Surveys.aspx":
                {
                    return user.Permissions.CanViewMySurveysPage;
                }
            case "/SurveyActivities.aspx":
            case "/SurveyActivityEdit.aspx":
                {
                    return (user.Permissions.CanWorkSurveys || user.Permissions.CanWorkReviews);
                }
            default:
                {
                    throw new NotSupportedException("Access is not defined for page '" + page.Path + "'.");
                }
        }
    }
}
