﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Berkley.BLC.Web.Internal.Controls;
using Berkley.BLC.Entities;
using System.Web.UI.WebControls;
using System.Data;

/// <summary>
/// Summary description for AttributeHelper
/// </summary>
public class AttributeHelper
{
	public AttributeHelper()
	{
	}

    #region Common

    public static CompanyAttribute[] GetCompanyAttributes(Company company, EntityType entityType)
    {
        return CompanyAttribute.GetSortedArray("CompanyID = ? && EntityTypeCode = ?", "DisplayOrder ASC", company.ID, entityType.Code);
    }

    public static string GetAttributeValue(Guid entityID, CompanyAttribute companyAttribute)
    {
        string result = string.Empty;

        if (companyAttribute.EntityTypeCode == EntityType.ServiceVisitObjective.Code)
        {
            ObjectiveAttribute objectiveAttribute = ObjectiveAttribute.GetOne("ObjectiveID = ? && CompanyAttributeID = ?", entityID, companyAttribute.ID);
            result = (objectiveAttribute != null) ? objectiveAttribute.AttributeValue : string.Empty;
        }
        else if (companyAttribute.EntityTypeCode == EntityType.ServiceVisitObjective.Code ||
            companyAttribute.EntityTypeCode == EntityType.Survey.Code)
        {
            SurveyAttribute surveyAttribute = SurveyAttribute.GetOne("SurveyID = ? && CompanyAttributeID = ?", entityID, companyAttribute.ID);
            result = (surveyAttribute != null) ? surveyAttribute.AttributeValue : string.Empty;
        }
        else if (companyAttribute.EntityTypeCode == EntityType.ServicePlan.Code)
        {
            ServicePlanAttribute servicePlanAttribute = ServicePlanAttribute.GetOne("ServicePlanID = ? && CompanyAttributeID = ?", entityID, companyAttribute.ID);
            result = (servicePlanAttribute != null) ? servicePlanAttribute.AttributeValue : string.Empty;
        }

        return result;
    }

    public static string GetEncodedAttributeValueForGrid(object attribute, EntityType entityType, string nullValue)
    {
        string resultValue = string.Empty;
        AttributeDataType dataType = null;

        if (attribute != null)
        {
            if (entityType.Code == EntityType.ServiceVisitObjective.Code)
            {
                ObjectiveAttribute objectiveAttribute = (ObjectiveAttribute)attribute;
                resultValue = objectiveAttribute.AttributeValue;
                dataType = objectiveAttribute.CompanyAttribute.AttributeDataType;
            }
            else if (entityType.Code == EntityType.ServicePlan.Code)
            {
                ServicePlanAttribute servicePlanAttribute = (ServicePlanAttribute)attribute;
                resultValue = servicePlanAttribute.AttributeValue;
                dataType = servicePlanAttribute.CompanyAttribute.AttributeDataType;
            }
            else if (entityType.Code == EntityType.Survey.Code)
            {
                SurveyAttribute surveyAttribute = (SurveyAttribute)attribute;
                resultValue = surveyAttribute.AttributeValue;
                dataType = surveyAttribute.CompanyAttribute.AttributeDataType;
            }
            else
            {
                throw new NotSupportedException(string.Format("Not expected entity type of '{0}' to be passed.", entityType.Code));
            }

            if (!string.IsNullOrEmpty(resultValue))
            {
                if (dataType.Code == AttributeDataType.Date.Code)
                {
                    try
                    {
                        resultValue = DateTime.Parse(resultValue).ToShortDateString();
                    }
                    catch (Exception)
                    {
                        //swallow this exception and just display whatever was returned
                    }
                }
                else if (dataType.Code == AttributeDataType.Bit.Code)
                {
                    resultValue = (resultValue == "1") ? "Yes" : "No";
                }
                else if (dataType.Code == AttributeDataType.Money.Code)
                {
                    try
                    {
                        Decimal currency = Decimal.Parse(resultValue.Replace("$", ""));
                        resultValue = string.Format("{0:C}", currency);
                    }
                    catch (Exception)
                    {
                        //swallow this exception and just display whatever was returned
                    }
                }
            }
            else
            {
                resultValue = nullValue;
            }
        }
        else
        {
            resultValue = nullValue;
        }

        return resultValue;
    }

    public static void LoadWebControls(CompanyAttribute attribute, string attributeValue, ref DynamicControlsPlaceholder pl, int itemIndex)
    {
        string controlID = string.Empty;

        switch (attribute.AttributeDataTypeCode)
        {
            case "B":
                CheckBox chk = new CheckBox();
                chk.Checked = (attributeValue == "1") ? true : false;
                chk.ID = attribute.ID.ToString();
                chk.CssClass = "chk";
                controlID = chk.ID;
                pl.Controls.Add(chk);
                controlID = chk.ID;
                break;
            case "D":
                TextBox txtDate = new TextBox();
                txtDate.CssClass = "txt";
                txtDate.Visible = false;
                pl.Controls.Add(txtDate);

                QCI.Web.Controls.DatePicker dp = new QCI.Web.Controls.DatePicker();
                dp.Date = (attributeValue != string.Empty) ? DateTime.Parse(attributeValue) : DateTime.MinValue;
                dp.ID = attribute.ID.ToString();
                dp.Attributes.Add("onfocus", "StartAutoFormat(this, 'Date')");
                dp.CssClass = "txt";
                dp.Columns = 10;
                pl.Controls.Add(dp);
                controlID = dp.ID;

                RegularExpressionValidator revDate = new RegularExpressionValidator();
                revDate.ValidationExpression = "^(((((((0?[13578])|(1[02]))[\\.\\-/]?((0?[1-9])|([12]\\d)|(3[01])))|(((0?[469])|(11))[\\.\\-/]?((0?[1-9])|([12]\\d)|(30)))|((0?2)[\\.\\-/]?((0?[1-9])|(1\\d)|(2[0-8]))))[\\.\\-/]?(((19)|(20))?([\\d][\\d]))))|((0?2)[\\.\\-/]?(29)[\\.\\-/]?(((19)|(20))?(([02468][048])|([13579][26])))))$";
                revDate.CssClass = "val";
                revDate.ErrorMessage = string.Format("{0} does not have a valid date.", attribute.Label);
                revDate.Text = " *";
                revDate.ControlToValidate = controlID;
                revDate.Display = ValidatorDisplay.Dynamic;
                pl.Controls.Add(revDate);

                break;
            case "M":
                TextBox txtCurrency = new TextBox();
                txtCurrency.ID = attribute.ID.ToString();
                txtCurrency.Text = attributeValue;
                txtCurrency.CssClass = "txt";
                txtCurrency.MaxLength = 3000;
                pl.Controls.Add(txtCurrency);
                controlID = txtCurrency.ID;

                RegularExpressionValidator rev = new RegularExpressionValidator();
                rev.ValidationExpression = "^\\$?\\-?([1-9]{1}[0-9]{0,2}(\\,\\d{3})*(\\.\\d{0,2})?|[1-9]{1}\\d{0,}(\\.\\d{0,2})?|0(\\.\\d{0,2})?|(\\.\\d{1,2}))$|^\\-?\\$?([1-9]{1}\\d{0,2}(\\,\\d{3})*(\\.\\d{0,2})?|[1-9]{1}\\d{0,}(\\.\\d{0,2})?|0(\\.\\d{0,2})?|(\\.\\d{1,2}))$|^\\(\\$?([1-9]{1}\\d{0,2}(\\,\\d{3})*(\\.\\d{0,2})?|[1-9]{1}\\d{0,}(\\.\\d{0,2})?|0(\\.\\d{0,2})?|(\\.\\d{1,2}))\\)$";
                rev.CssClass = "val";
                rev.ErrorMessage = string.Format("{0} does not have a valid currency value.", attribute.Label);
                rev.Text = " *";
                rev.ControlToValidate = controlID;
                rev.Display = ValidatorDisplay.Dynamic;
                pl.Controls.Add(rev);
                break;
            case "T":
                TextBox txt = new TextBox();
                txt.ID = attribute.ID.ToString();
                txt.Text = attributeValue;
                txt.CssClass = "txt";
                txt.MaxLength = 3000;
                txt.Columns = (attribute.ControlColumnCount != Int32.MinValue) ? attribute.ControlColumnCount : 10;
                if (attribute.ControlRowCount != Int32.MinValue && attribute.ControlRowCount > 1)
                {
                    txt.Rows = attribute.ControlRowCount;
                    txt.TextMode = TextBoxMode.MultiLine;
                }
                pl.Controls.Add(txt);
                controlID = txt.ID;

                if (attribute.ControlRowCount != Int32.MinValue && attribute.ControlRowCount > 1)
                {
                    HyperLink lnk = new HyperLink();
                    lnk.ImageUrl = ARMSUtility.TemplatePath + "/javascripts/Images/atdbuttontr.gif";
                    lnk.ToolTip = "Check Spelling";
                    lnk.Style.Add("vertical-align", "top");
                    string controlIdToEdit = pl.Parent.ClientID + "_" + attribute.ID.ToString();
                    lnk.ID = "checkLink" + attribute.ID.ToString();

                    //lnk.NavigateUrl = string.Format("javascript:checkSpelling('{0}_{2}', '{1}_{2}')", controlIdToEdit, pl.Parent.ClientID + "_" + lnk.ID, itemIndex);
                    lnk.NavigateUrl = string.Format("javascript:rapidSpell.dialog_spellCheck(true,'{0}')", controlIdToEdit, pl.Parent.ClientID + "_" + lnk.ID, itemIndex);
                    pl.Controls.Add(lnk);
                }
                break;
            default:
                throw new NotSupportedException(string.Format("Not expecting attribute data value type of '{0}'.", attribute.AttributeDataTypeCode));
        }

        if (attribute.IsRequired && attribute.AttributeDataTypeCode != AttributeDataType.Bit.Code)
        {
            RequiredFieldValidator rfv = new RequiredFieldValidator();
            rfv.CssClass = "val";
            rfv.ErrorMessage = string.Format("{0} is required.", attribute.Label);
            rfv.Text = " *";
            rfv.ControlToValidate = controlID;
            rfv.Display = ValidatorDisplay.Dynamic;
            pl.Controls.Add(rfv);
        }
    }

    private static string GetWebControlResult(System.Web.UI.Control control)
    {
        string result = string.Empty;
        if (control.GetType() == typeof(QCI.Web.Controls.DatePicker))
        {
            QCI.Web.Controls.DatePicker dp = (QCI.Web.Controls.DatePicker)control;
            result = (dp.Date != DateTime.MinValue) ? dp.Date.ToShortDateString() : string.Empty;
        }
        else if (control.GetType() == typeof(TextBox))
        {
            TextBox txt = (TextBox)control;
            result = txt.Text;
        }
        else if (control.GetType() == typeof(CheckBox))
        {
            CheckBox chk = (CheckBox)control;
            result = (chk.Checked) ? "1" : "0";
        }

        return result;
    }

    public static string FormatLabelName(string labelName)
    {
        return labelName.Replace(" ", "").Replace(".", "").Replace("(", "").Replace(")", "").Replace("#", "").Replace("*", "").Replace("[", "").Replace("]", "").Replace("?", "").Replace(",", "").Replace("<", "").Replace(">", "").Replace("@", "").Replace("$", "").Replace("&", "").Replace("-", "");
    }

    #endregion

    #region Objective Attribute Section

    public static void DeleteObjectiveAttributes(Objective objective)
    {
        ObjectiveAttribute[] objectiveAttributes = ObjectiveAttribute.GetArray("ObjectiveID = ?", objective.ID);
        foreach (ObjectiveAttribute objectiveAttribute in objectiveAttributes)
        {
            objectiveAttribute.Delete();
        }
    }

    public static void SaveObjectiveAttributes(Repeater rep, Objective objective, User currentUser, Wilson.ORMapper.Transaction trans)
    {
        // update any objective attributes
        if (objective.IsReadOnly)
        {
            objective = objective.GetWritableInstance();
        }

        foreach (RepeaterItem item in rep.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                Berkley.BLC.Web.Internal.Controls.DynamicControlsPlaceholder pHolder = (Berkley.BLC.Web.Internal.Controls.DynamicControlsPlaceholder)item.FindControl("pHolder");
                foreach (System.Web.UI.Control control in pHolder.Controls)
                {
                    Guid dataKey = Guid.Empty;
                    if (Guid.TryParse(control.ID, out dataKey))
                    {
                        CompanyAttribute companyAttribute = CompanyAttribute.Get(dataKey);

                        if (!string.IsNullOrEmpty(companyAttribute.MappedField))
                        {
                            if (companyAttribute.MappedField == "Objective.Text")
                            {
                                objective.Text = GetWebControlResult(control);
                            }
                            else if (companyAttribute.MappedField == "Objective.CommentsText")
                            {
                                objective.CommentsText = GetWebControlResult(control);
                            }
                            else if (companyAttribute.MappedField == "Objective.TargetDate")
                            {
                                string targetDate = GetWebControlResult(control);
                                objective.TargetDate = (targetDate == string.Empty) ? DateTime.MinValue : DateTime.Parse(GetWebControlResult(control));
                            }
                        }
                        else
                        {
                            ObjectiveAttribute objectiveAttribute = ObjectiveAttribute.GetOne("ObjectiveID = ? && CompanyAttributeID = ?", objective.ID, companyAttribute.ID);
                            if (objectiveAttribute == null) // add
                            {
                                objectiveAttribute = new ObjectiveAttribute(objective.ID, dataKey);
                            }
                            else //edit
                            {
                                objectiveAttribute = objectiveAttribute.GetWritableInstance();
                            }

                            objectiveAttribute.AttributeValue = GetWebControlResult(control);
                            objectiveAttribute.LastModifiedDate = DateTime.Now;
                            objectiveAttribute.LastModifiedBy = currentUser.Username;
                            objectiveAttribute.Save(trans);
                        }
                    }
                }
            }

        }

        
        objective.Save(trans);
    }

    

    public static DataTable BuildObjectivesDataTable(Objective[] objectives, CompanyAttribute[] companyAttributes, bool exportToDoc)
    {
        //combine the static properties with the dynamic attributes
        DataTable dt = new DataTable();

        //need to capture the labels of the static fields for that company
        string columnNameObjective = string.Empty;
        string columnNameComments = string.Empty;
        string columnNameTargetDate = string.Empty;

        if (exportToDoc)
        {
            dt.Columns.Add("Objective.ID").DataType = typeof(Guid);
            dt.Columns.Add("Objective.SurveyNumber");
            dt.Columns.Add("Objective.Number");
            dt.Columns.Add("Objective.Status");
        }
        else
        {
            dt.Columns.Add("ObjectiveID").DataType = typeof(Guid);
            dt.Columns.Add("SurveyNumber");
            dt.Columns.Add("Number");
            dt.Columns.Add("Status");
        }

        //this is where we add the dynamic attributes
        foreach (CompanyAttribute companyAttribute in companyAttributes)
        {
            string formattedLabelName = (exportToDoc) ? string.Format("Objective.{0}", FormatLabelName(companyAttribute.Label)) : FormatLabelName(companyAttribute.Label);
            dt.Columns.Add(formattedLabelName);

            if (!string.IsNullOrEmpty(companyAttribute.MappedField))
            {
                if (companyAttribute.MappedField == "Objective.Text")
                {
                    columnNameObjective = formattedLabelName;
                }
                else if (companyAttribute.MappedField == "Objective.CommentsText")
                {
                    columnNameComments = formattedLabelName;
                }
                else if (companyAttribute.MappedField == "Objective.TargetDate")
                {
                    columnNameTargetDate = formattedLabelName;
                }
            }
        }

        //now add all of the records
        for (int i = 0; i < objectives.Length; i++)
        {
            Objective objective = objectives[i];
            ObjectiveAttribute[] objectiveAttributes = ObjectiveAttribute.GetSortedArray("ObjectiveID = ?", "CompanyAttribute.DisplayOrder ASC", objective.ID);

            DataRow dr = dt.NewRow();
            dr[0] = objectives[i].ID;
            dr[1] = objectives[i].Survey.Number;
            dr[2] = objectives[i].Number;
            dr[3] = objectives[i].Status.Name;
            dr[columnNameObjective] = objectives[i].Text;
            dr[columnNameComments] = objectives[i].CommentsText;
            dr[columnNameTargetDate] = objectives[i].TargetDate.ToShortDateString();

            for (int j = 0; j < objectiveAttributes.Length; j++)
            {
                string formattedLabelName = (exportToDoc) ? string.Format("Objective.{0}", FormatLabelName(objectiveAttributes[j].CompanyAttribute.Label)) : FormatLabelName(objectiveAttributes[j].CompanyAttribute.Label);
                dr[formattedLabelName] = GetEncodedAttributeValueForGrid(objectiveAttributes[j], EntityType.ServiceVisitObjective, string.Empty);
            }

            dt.Rows.Add(dr);
        }


        return dt;
    }

    #endregion

    #region ServicePlan Attribute Section

    public static void SaveServicePlanAttributes(Repeater rep, Guid servicePlanID, User currentUser, Wilson.ORMapper.Transaction trans)
    {
        // update any objective attributes
        foreach (RepeaterItem item in rep.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                Berkley.BLC.Web.Internal.Controls.DynamicControlsPlaceholder pHolder = (Berkley.BLC.Web.Internal.Controls.DynamicControlsPlaceholder)item.FindControl("pHolder");
                foreach (System.Web.UI.Control control in pHolder.Controls)
                {
                    Guid dataKey = Guid.Empty;
                    if (Guid.TryParse(control.ID, out dataKey))
                    {
                        ServicePlanAttribute servicePlanAttribute = ServicePlanAttribute.GetOne("ServicePlanID = ? && CompanyAttributeID = ?", servicePlanID, dataKey);
                        if (servicePlanAttribute == null) // add
                        {
                            servicePlanAttribute = new ServicePlanAttribute(servicePlanID, dataKey);
                        }
                        else //edit
                        {
                            servicePlanAttribute = servicePlanAttribute.GetWritableInstance();
                        }

                        servicePlanAttribute.AttributeValue = GetWebControlResult(control);
                        servicePlanAttribute.LastModifiedDate = DateTime.Now;
                        servicePlanAttribute.LastModifiedBy = currentUser.Username;
                        servicePlanAttribute.Save(trans);
                    }
                }
            }

        }
    }

    public static DataTable BuildServicePlanDataTable(ServicePlan servicePlan, CompanyAttribute[] companyAttributes)
    {
        //combine the static properties with the dynamic attributes
        DataTable dt = new DataTable();

        //this is where we add the dynamic attributes
        foreach (CompanyAttribute companyAttribute in companyAttributes)
        {
            dt.Columns.Add(string.Format("ServicePlan.{0}", FormatLabelName(companyAttribute.Label)));
        }

        //now add all of the records
        ServicePlanAttribute[] servicePlanAttributes = ServicePlanAttribute.GetSortedArray("ServicePlanID = ?", "CompanyAttribute.DisplayOrder ASC", servicePlan.ID);

        DataRow dr = dt.NewRow();
        for (int j = 0; j < servicePlanAttributes.Length; j++)
        {
            dr[j] = GetEncodedAttributeValueForGrid(servicePlanAttributes[j], EntityType.ServicePlan, string.Empty);
        }

        dt.Rows.Add(dr);

        return dt;
    }
    
    #endregion

    #region Survey Attribute Section

    public static void SaveSurveyAttributes(Repeater rep, Guid surveyID, User currentUser, Wilson.ORMapper.Transaction trans)
    {
        // update any objective attributes
        foreach (RepeaterItem item in rep.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                Berkley.BLC.Web.Internal.Controls.DynamicControlsPlaceholder pHolder = (Berkley.BLC.Web.Internal.Controls.DynamicControlsPlaceholder)item.FindControl("pHolder");
                foreach (System.Web.UI.Control control in pHolder.Controls)
                {
                    Guid dataKey = Guid.Empty;
                    if (Guid.TryParse(control.ID, out dataKey))
                    {
                        SurveyAttribute surveyAttribute = SurveyAttribute.GetOne("SurveyID = ? && CompanyAttributeID = ?", surveyID, dataKey);
                        if (surveyAttribute == null) // add
                        {
                            surveyAttribute = new SurveyAttribute(surveyID, dataKey);
                        }
                        else //edit
                        {
                            surveyAttribute = surveyAttribute.GetWritableInstance();
                        }

                        surveyAttribute.AttributeValue = GetWebControlResult(control);
                        surveyAttribute.LastModifiedDate = DateTime.Now;
                        surveyAttribute.LastModifiedBy = currentUser.Username;
                        surveyAttribute.Save(trans);
                    }
                }
            }

        }
    }

    #endregion
}