﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Berkley.BLC.Business;
using Berkley.BLC.Entities;
using Berkley.BLC.Import;

[ScriptService]
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class AjaxMethodsService : System.Web.Services.WebService 
{
    [WebMethod]
    public string ExclusionCheck(bool isChecked, string surveyID, string locationID, string companyID)
    {
        //Inserts into the LocationPolicy Exclusion
        Guid gLocationID = new Guid(locationID);

        //Determine if we are inserting
        string imgSrc = string.Empty;
        if (!isChecked)
        {
            //set all the policies for this location as excluded
            DB.Engine.ExecuteScalar(string.Format("UPDATE Survey_LocationPolicyExclusion SET LocationExcluded = 1 WHERE LocationID = '{0}'", gLocationID));
            imgSrc = ARMSUtility.AppPath + "/images/dash.gif";
        }
        else
        {
            //set all the policies for this location as included
            DB.Engine.ExecuteScalar(string.Format("UPDATE Survey_LocationPolicyExclusion SET LocationExcluded = 0 WHERE LocationID = '{0}'", gLocationID));

            VWLocation eLocation = VWLocation.Get(gLocationID);

            if (eLocation.NoActivePolicies ||
                eLocation.AllPoliciesExcluded ||
                eLocation.LocationExcluded ||
                eLocation.NoContactsAdded ||
                eLocation.NotAllPolicyReportsSelected)
            {
                imgSrc = ARMSUtility.AppPath + "/images/xmark.gif";
            }
            else
            {
                imgSrc = ARMSUtility.AppPath + "/images/check.gif";
            }
        }

        return imgSrc;
    }

    [WebMethod]
    public void SetPrint(string objectiveID)
    {
        
        Guid gobjectiveID = new Guid(objectiveID);        
        //Toggle Print checkbox in Database 
        DB.Engine.ExecuteScalar(string.Format("UPDATE Objective SET IsPrint=Isnull(IsPrint^1,0) WHERE ObjectiveID = '{0}'", gobjectiveID));         
      
    }

    [WebMethod]
    public string[] GetAgency(string agencyNumber, string insuredID, string companyNumber)
    {
        string[] arrResult = new string[1];
        Survey survey = Survey.GetOne("InsuredID = ?", insuredID);

        //Validate that the value is digits only
        //TODO: Remove company hard coding and make feature table driven
        if (companyNumber != Company.BerkleyLifeSciences.ID.ToString() && companyNumber != Company.BerkleyOilGas.ID.ToString())
        {
            try { Convert.ToDecimal(agencyNumber); }
            catch
            {
                arrResult = new string[1];
                arrResult[0] = "Agency Number must be a numeric value.";

                return arrResult;
            }
        }

        //First check if the agency already exist in the ARMS db
        Agency eAgency = Agency.GetOne("CompanyID = ? && (Number = ? || Number = ? || Number = ?)", companyNumber, agencyNumber.PadLeft(5, '0'), agencyNumber.TrimStart('0'), agencyNumber);
        Company company = Company.Get(new Guid(companyNumber));

        if (eAgency == null)
        {
            try
            {
                //Check the policy system
                ImportFactory oFactory = new ImportFactory(company);
                Agency oAgency = oFactory.ImportAgency(agencyNumber, string.Empty); //01981 

                ReconcileAgency oReconcileAgency = new ReconcileAgency(company);
                oReconcileAgency.Reconcile(oAgency);

                eAgency = Agency.Get(oReconcileAgency.Entity.ID);

            }
            catch (BlcImportException ex)
            {
                if (survey != null && (survey.PrimaryPolicy != null || !String.IsNullOrEmpty(survey.WorkflowSubmissionNumber)))
                {
                    arrResult = new string[1];
                    arrResult[0] = ex.Message;
                }
                else
                {
                    arrResult = new string[8];
                    for (int i = 0; i < 8; i++)
                    {
                        arrResult[i] = String.Empty;
                    }
                }
            }
            catch (Berkley.BLC.Import.Prospect.BlcProspectWebServiceException ex)
            {
                if (survey != null && (survey.PrimaryPolicy != null || !String.IsNullOrEmpty(survey.WorkflowSubmissionNumber)))
                {
                    arrResult = new string[1];
                    arrResult[0] = ex.Message;
                }
                else
                {
                    arrResult = new string[8];
                    for (int i = 0; i < 8; i++)
                    {
                        arrResult[i] = String.Empty;
                    }
                }
            }
        }

        if (eAgency != null)
        {
            if (survey != null && (survey.PrimaryPolicy != null || !String.IsNullOrEmpty(survey.WorkflowSubmissionNumber)))
            {
                arrResult = new string[4];
                arrResult[0] = eAgency.Name;
                arrResult[1] = eAgency.HtmlAddress;
                arrResult[2] = eAgency.HtmlPhoneNumber;
                arrResult[3] = eAgency.HtmlFaxNumber;
            }
            else
            {
                arrResult = new string[9];
                arrResult[0] = eAgency.Name;
                arrResult[1] = eAgency.StreetLine1;
                arrResult[2] = eAgency.StreetLine2;
                arrResult[3] = eAgency.City;
                arrResult[4] = eAgency.StateCode;
                arrResult[5] = eAgency.ZipCode;
                arrResult[6] = eAgency.PhoneNumber;
                arrResult[7] = eAgency.FaxNumber;
                arrResult[8] = eAgency.Producer;
            }

            //Update the agency tied to the insured
            Insured insured = Insured.Get(new Guid(insuredID)).GetWritableInstance();
            insured.AgencyID = eAgency.ID;
            insured.Save();
        }

        return arrResult;
    }

    [WebMethod]
    public string[] GetAgencyEmails(string agencyNumber, string insuredID, string companyID)
    {
        //Since the agency has changed, null out the agent email
        Insured insured = Insured.Get(new Guid(insuredID)).GetWritableInstance();
        insured.AgentEmailID = Guid.Empty;
        insured.Save();

        AgencyEmail[] emails = AgencyEmail.GetSortedArray("Agency[Number = ? && CompanyID = ?]", "EmailAddress ASC", agencyNumber, companyID);
        List<String> emailList = new List<String>();

        //by default, always add the first line
        emailList.Add("-- select or add email --|");

        foreach (AgencyEmail email in emails)
        {
            emailList.Add(email.EmailAddress.Trim() + "|" + email.ID.ToString());
        }

        return emailList.ToArray();
    }
}