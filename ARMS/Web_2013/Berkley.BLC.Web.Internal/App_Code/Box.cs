using System;
using System.Web;
using System.Web.UI;

namespace Berkley.BLC.Web.Internal.Controls
{
    /// <summary>
    /// Container control that renders as a stylized box which can be used to group other controls.
    /// </summary>
    public class Box : Control
    {
        /// <summary>
        /// Gets or sets the text value of the title to be shown at the top of the container.
        /// </summary>
        public string Title
        {
            get
            {
                object value = this.ViewState["Title"];
                return (value != null) ? (string)value : string.Empty;
            }
            set
            {
                this.ViewState["Title"] = value;
            }
        }

        public string TitleRightCorner
        {
            get
            {
                object value = this.ViewState["TitleRightCorner"];
                return (value != null) ? (string)value : string.Empty;
            }
            set
            {
                this.ViewState["TitleRightCorner"] = value;
            }
        }

        public bool EncodeTitle
        {
            get
            {
                object value = this.ViewState["EncodeTitle"];
                return (value != null) ? (bool)value : true;
            }
            set
            {
                this.ViewState["EncodeTitle"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the width of this container.
        /// </summary>
        public string Width
        {
            get
            {
                object value = this.ViewState["Width"];
                return (value != null) ? (string)value : string.Empty;
            }
            set
            {
                this.ViewState["Width"] = value;
            }
        }


        public bool Hidden
        {
            get
            {
                object value = this.ViewState["Hidden"];
                return (value != null) ? (bool)value : false;
            }
            set
            {
                this.ViewState["Hidden"] = value;
            }
        }

        /// <summary>
        /// Sends server control content to a provided HtmlTextWriter object, which writes the content to be rendered on the client.
        /// </summary>
        /// <param name="w">The HtmlTextWriter object that receives the server control content.</param>
        protected override void Render(HtmlTextWriter w)
        {
            //foreach( string key in this.Attributes.Keys )
            //{
            //	control.Attributes.Add(key, this.Attributes[key]);
            //}

            // start table
            w.WriteBeginTag("table");
            w.WriteAttribute("id", this.UniqueID);
            w.WriteAttribute("class", "box");
            if (this.Width.Length > 0)
            {
                w.WriteAttribute("width", this.Width);
            }
            w.WriteAttribute("border", "0");
            w.WriteAttribute("cellspacing", "0");
            w.WriteAttribute("cellpadding", "0");
            if (this.Hidden)
            {
                w.WriteAttribute("style", "DISPLAY: none");
            }
            w.WriteLine('>');

            // header row
            w.WriteLine("<tr>");
            w.Indent++;
            w.WriteLine("<td class=\"TL\"><div></div></td>");
            w.Write("<td class=\"TC\" nowrap>");
            w.Write((EncodeTitle) ? HttpUtility.HtmlEncode(this.Title) : this.Title);
            w.WriteLine("</td>");
            w.Write("<td class=\"TCRight\" alrign=\"right\" nowrap>");
            w.Write((EncodeTitle) ? HttpUtility.HtmlEncode(this.TitleRightCorner) : this.TitleRightCorner);
            w.WriteLine("</td>");
            w.WriteLine("<td class=\"TR\"><div></div></td>");
            w.Indent--;
            w.WriteLine("</tr>");

            // content row
            w.WriteLine("<tr>");
            w.Indent++;
            w.WriteLine("<td class=\"ML\"><div></div></td>");
            w.Write("<td colspan=2 class=\"MC\">");
            this.RenderChildren(w);
            w.WriteLine("</td>");
            w.WriteLine("<td class=\"MR\"><div></div></td>");
            w.Indent--;
            w.WriteLine("</tr>");

            // footer row
            w.WriteLine("<tr>");
            w.Indent++;
            w.WriteLine("<td class=\"BL\"><div></div></td>");
            w.WriteLine("<td colspan=2 class=\"BC\"><div></div></td>");
            w.WriteLine("<td class=\"BR\"><div></div></td>");
            w.Indent--;
            w.WriteLine("</tr>");

            // end table
            w.WriteEndTag("table");
        }
    }
}
