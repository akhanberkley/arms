using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Berkley.BLC.Entities;
using Berkley.BLC.Import;
using Berkley.BLC.Business;
using System.Collections;

/// <summary>
/// Summary description for Common
/// </summary>
public class Common
{
    public Common()
    {
        //
        // TODO: Add constructor logic here
        //
    }

        public static string RefreshSurvey(Survey eSurvey, User currentUser, bool refreshClaims)
        {
            //Run the importfactory to get the lastest insured data from the policy system

            //remove the characters after and including the last dash
            string number = ParsePolicyNumber(eSurvey.PrimaryPolicy.Number);

            //IMPORTANT: before anything else, update the policy system key in the primary location
            //note: purposely avoided the mapper in this update
            if (eSurvey.LocationID != null && eSurvey.PrimaryPolicyID != null)
            {
                string policySystemKey = string.Format("{0}-{1}", eSurvey.Location.Number, eSurvey.PrimaryPolicy.Number);
                string sqlUpdate = string.Format("UPDATE Location SET PolicySystemKey = '{0}' WHERE LocationID = '{1}'", policySystemKey, eSurvey.Location.ID);
                DB.Engine.ExecuteScalar(sqlUpdate);
            }

            ImportFactory factory = new ImportFactory(currentUser.Company);
            BlcInsured oBlcInsured = factory.ImportInsuredByPolicy(number, refreshClaims);
            oBlcInsured.PreviousClientId = eSurvey.Insured.ClientID;
            bool _enableNonRenewedAccountStatus = (Berkley.BLC.Business.Utility.GetCompanyParameter("EnableNonRenewedAccountStatus", currentUser.Company).ToUpper() == "TRUE") ? true : false;
          
            Insured importedInsured = factory.ImportOnlyInsuredForAccountStatus("", eSurvey.Insured.ClientID.ToString());

            DateTime nonrenewedDate = DateTime.MinValue;

            if (importedInsured != null && importedInsured.NonrenewedDate != DateTime.MinValue && _enableNonRenewedAccountStatus == true)
                {
                    //update the non-renewed field based on whatever was returned from the policy system
                    nonrenewedDate = importedInsured.NonrenewedDate;
                }
          
            ReconcileInsured oReconcileInsured = new ReconcileInsured(oBlcInsured, eSurvey, currentUser.Company);
            oReconcileInsured.Reconcile();

            //If any of the active policies don't exist for this survey, 
            //then attach them to the survey via the Survey_PolicyCoverage table
            Policy[] existingPolicies = Policy.GetArray("SurveyPolicyCoverages.SurveyID = ?", eSurvey.ID);
            Policy[] activePolicies = Policy.GetArray("InsuredID = ? && IsActive = true", eSurvey.InsuredID);

            ArrayList keys = new ArrayList();
            foreach (Policy existingPolicy in existingPolicies)
            {
                keys.Add(existingPolicy.ID);
            }

            //Add the covereges to the newly added policies
            string newPolicyMessage = string.Empty;
            int newPolicyCount = 0;
            foreach (Policy activePolicy in activePolicies)
            {
                if (!keys.Contains(activePolicy.ID))
                {
                    newPolicyCount = newPolicyCount + 1;
                    Location[] locations = Location.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && Active = true]", "Number ASC", eSurvey.ID);
                    foreach (Location location in locations)
                    {
                        CoverageName[] coverages = CoverageName.GetArray("Coverages[InsuredID = ? && PolicyID = ?]  || (IsRequired = true && CompanyID = ? && LineOfBusinessCode = ?)", eSurvey.InsuredID, activePolicy.ID, currentUser.CompanyID, activePolicy.LineOfBusinessCode);
                        foreach (CoverageName coverage in coverages)
                        {
                            SurveyPolicyCoverage surveyCoverage = new SurveyPolicyCoverage(eSurvey.ID, location.ID, activePolicy.ID, coverage.ID);
                            surveyCoverage.ReportTypeCode = ReportType.NoReport.Code;
                            surveyCoverage.Active = false;

                            surveyCoverage.Save();
                        }
                    }
                }
            }

            if (newPolicyCount > 0)
            {
                if (newPolicyCount == 1)
                {
                    newPolicyMessage = "1 new policy was returned from the policy system.\r\n\r\nTo include the policy on this survey request, go the policies/coverages section of this screen and click on the appropriate 'Include' checkbox for this policy record.\r\n\r\n* Note: you must be in 'Edit' mode to perform this action.";
                }
                else
                {
                    newPolicyMessage = string.Format("{0} new policies were returned from the policy system.\r\n\r\nTo include these policies on this survey request, go the policies/coverages section of this screen and click on the appropriate 'Include' checkbox for each desired policy.\r\n\r\n* Note: you must be in 'Edit' mode to perform this action.", newPolicyCount);
                }
            }

            //group both list together
            List<Policy> policyList = new List<Policy>();
            policyList.AddRange(existingPolicies);
            policyList.AddRange(activePolicies);

            if (policyList.Count > 0)
            {
                //check for unique policies by policy number
                Hashtable uniquePolicies = new Hashtable();
                foreach (Policy policy in policyList)
                {
                    if (!uniquePolicies.ContainsKey(policy.Number))
                    {
                        uniquePolicies.Add(policy.Number, policy);
                    }
                }

                //Refresh the list of claims
                Policy[] policies = new Policy[uniquePolicies.Count];
                uniquePolicies.Values.CopyTo(policies, 0);

                BlcClaim[] blcClaims = factory.ImportClaims(policies);
                foreach (BlcClaim blcClaim in blcClaims)
                {
                    foreach (Policy policy in policies)
                    {
                        if (policy.Number == blcClaim.PolicyNumber)
                        {
                            Claim claim = blcClaim.Entity;
                            claim.PolicyID = policy.ID;
                            claim.Save();
                        }
                    }
                }
            }

         



            Survey survey = eSurvey.GetWritableInstance();
            survey.LastModifiedOn = DateTime.Now;
            survey.LastRefreshDate = DateTime.Now;
            Insured insuredToUpdate = survey.Insured.GetWritableInstance();


            if (nonrenewedDate != DateTime.MinValue && _enableNonRenewedAccountStatus == true)
            { 
                insuredToUpdate.NonrenewedDate = nonrenewedDate;
                insuredToUpdate.Save();
            }

            survey.Save();
            return newPolicyMessage;
        }

        public static string ParsePolicyNumber(string number)
        {
            if (number.Contains("-"))
            {
                int length = number.Length - number.LastIndexOf("-");
                number = number.Remove((number.Length - length), length);
            }

            return number;
        }

        public static string ReadString(object obj)
        {
            return (obj != null) ? obj.ToString().Trim() : string.Empty;
        }
}
