﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Berkley.BLC.Web.Internal.Controls
{

    public class CheckBoxForRepeater : CheckBox
    {
        public string CommandName
        {
            get
            {
                if (this.ViewState["CommandName"] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return this.ViewState["CommandName"] as string;
                }
            }
            set
            {
                this.ViewState["CommandName"] = value;
            }
        }

        public string CommandArgument
        {
            get
            {
                if (this.ViewState["CommandArgument"] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return this.ViewState["CommandArgument"] as string;
                }
            }
            set
            {
                this.ViewState["CommandArgument"] = value;
            }
        }


        protected override void OnCheckedChanged(EventArgs e)
        {
            //create a new event args of type command event args
            CommandEventArgs ce = new CommandEventArgs(this.CommandName, this.CommandArgument);

            //allow the base checkbox to handle the event as normal 
            base.OnCheckedChanged(e);

            //raise the contorls method RaiseBubbleEvent
            base.RaiseBubbleEvent(this, ce);
        }
    }
}