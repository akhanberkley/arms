using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Specialized;
using System.Collections.Generic;

using Berkley.BLC.Entities;
using QCI.Web.Validation;
using Berkley.BLC.Business;
using Berkley.Imaging;


/// <summary>
/// Summary description for Document
/// </summary>
public class StreamDocument
{
    #region Enumerations

    public enum DocType
    {
        Survey,
        ServicePlan,
        Template,
        Letter,
        Report
    }

    #endregion

    #region Properties

    public System.IO.Stream DocumentStream
    {
        get
        {
            return _contentStream;
        }
        set
        {
            _contentStream = value;
        }
    }

    public string DocMimeType
    {
        get
        {
            return _docMimeType;
        }
        set
        {
            _docMimeType = value;
        }
    }

    #endregion

    private string _fileNetReference = string.Empty;
    private string _docMimeType = string.Empty;
    private string _docName = string.Empty;
    private System.IO.Stream _contentStream = null;

    /// <summary>
    /// Performs the necessary operations to stream back a document and attach to the http response.
    /// </summary>
    /// <param name="doc">The document.</param>
    /// <param name="user">The current user.</param>
    public StreamDocument(string fileNetReference, string mimeType, int pageCount, Company company)
    {
        if (fileNetReference == null || fileNetReference.Length <= 0)
        {
            throw new NotSupportedException("The FileNet reference cannot be null.");
        }

        ImagingHelper imaging = new ImagingHelper(company);
        string docExtension = GetExtension(mimeType, true, string.Empty);

        //--------------------------------------------------------------------------------
        //Check CompanyParameters to see if PDF Conversion should be done (default yes)
        //--------------------------------------------------------------------------------
        bool doPDFConversion = !(Berkley.BLC.Business.Utility.GetCompanyParameter("P8BypassPDFConversion", company).ToLower() == "true");   //DO CONVERSION if no company parameter exists for this

        //--------------------------------------------------------------------------------
        //IF NOT BYPASSED, images needed to be converted into pdfs (especially multi-paged tiffs)
        //--------------------------------------------------------------------------------
        pageCount = imaging.RetrievePageCount(fileNetReference);	//WA-756, Doug Bruce, 2015-06-23 - P8 - Used for finding/counting pages in a multipage document (split over multiple ContentList entries).
        if (doPDFConversion && (pageCount > 1 || docExtension.Contains("tif")))  //WA-905 Doug Bruce 2015-09-22 - merge tifs to pdf
        {
            IFile[] files = imaging.RetrieveFile(fileNetReference, pageCount);  //WA-756, Doug Bruce, 2015-06-23 - P8 - Used for finding/counting pages in a multipage document (split over multiple ContentList entries).

            _contentStream = ConvertPDF(files, docExtension);
            if (_contentStream != null)
            {
                //Conversion successful; override the type information to pdfs
                _docMimeType = "application/pdf";
                docExtension = ".pdf";
            }
            else
            {
                //Conversion failed; revert to TIF file.
                _contentStream = imaging.RetrieveFileImage(fileNetReference);
                _docName = fileNetReference + docExtension;
                _docMimeType = mimeType;
            }
        }
        else
        {
            _contentStream = imaging.RetrieveFileImage(fileNetReference);
            _docName = fileNetReference + docExtension;
            _docMimeType = mimeType;
        }
    }

    /// <summary>
    /// Performs the necessary operations to stream back a document and attach to the http response.
    /// </summary>
    /// <param name="doc">The document.</param>
    /// <param name="user">The current user.</param>
    public StreamDocument(SurveyDocument doc, Company company)
    {
        if (doc == null)
        {
            throw new NotSupportedException("The document cannot be null.");
        }

        ImagingHelper imaging = new ImagingHelper(company);
        string docExtension = GetExtension(doc.MimeType, true, doc.DocumentName);

        //--------------------------------------------------------------------------------
        //Check CompanyParameters to see if PDF Conversion should be bypassed (default no)
        //--------------------------------------------------------------------------------
        bool isBypassPDFConversion = false;
        if (Berkley.BLC.Business.Utility.GetCompanyParameter("P8BypassPDFConversion", company).ToLower() == "true")
            isBypassPDFConversion = true;

        //--------------------------------------------------------------------------------
        //IF NOT BYPASSED, images needed to be converted into pdfs (especially multi-paged tiffs)
        //--------------------------------------------------------------------------------
        int pageCount = imaging.RetrievePageCount(doc.FileNetReference);	//WA-756, Doug Bruce, 2015-06-23 - P8 - Used for finding/counting pages in a multipage document (split over multiple ContentList entries).
        if (!isBypassPDFConversion && (pageCount > 1 || docExtension.Contains("tif")))  //WA-905 Doug Bruce 2015-09-22 - merge tifs to pdf
        {
            IFile[] files = imaging.RetrieveFile(doc.FileNetReference, pageCount);

            _contentStream = ConvertPDF(files,docExtension);
            if (_contentStream != null)
            {
                //Conversion successful; override the type information to pdfs
                _docMimeType = "application/pdf";
                docExtension = ".pdf";
            }
            else
            {
                //Conversion failed; revert to TIF file.
                _contentStream = imaging.RetrieveFileImage(doc.FileNetReference);
                _docMimeType = doc.MimeType;
            }
        }
        else
        {
            _contentStream = imaging.RetrieveFileImage(doc.FileNetReference);
            _docMimeType = doc.MimeType;
        }

        //Give the user the most recognizable filename, if possible.
        string documentName = doc.DocumentName;
        if (documentName == "")
        {
            documentName = doc.FileNetReference;
        }

        //WA-881 Doug Bruce 2015-08-31 - Handle case with no file extension (should not happen except with converted P8 files - filenet extensions required by ARMS UI).
        int extPos = documentName.LastIndexOf('.');
        if (extPos >= 0)
        {
            //strip off the original extension
            _docName = documentName.Substring(0, extPos);
            //then add our new one
            _docName += docExtension;
        }
        else
        {
            //Look up extension based on mime type
            docExtension = GetExtension(_docMimeType, false, "");
            _docName = documentName + docExtension;
        }

        //always replace tiffs with pdfs
        //_docName = doc.DocumentName.Replace(".tiff", ".pdf").Replace(".tif", ".pdf");     //WA-855 - Removed because adobe won't read tifs renamed to pdfs.
    }

    /// <summary>
    /// Performs the necessary operations to stream back a document and attach to the http response.
    /// </summary>
    /// <param name="doc">The document.</param>
    /// <param name="user">The current user.</param>
    public StreamDocument(ServicePlanDocument doc, Company company)
    {
        if (doc == null)
        {
            throw new NotSupportedException("The document cannot be null.");
        }

        _docMimeType = doc.MimeType;
        _docName = doc.DocumentName;

        ImagingHelper imaging = new ImagingHelper(company);
        _contentStream = imaging.RetrieveFileImage(doc.FileNetReference);
    }

    /// <summary>
    /// Performs the necessary operations to stream back a document and attach to the http response.
    /// </summary>
    /// <param name="doc">The document.</param>
    /// <param name="user">The current user.</param>
    public StreamDocument(MergeDocumentVersion doc, Company company)
    {
        if (doc == null)
        {
            throw new NotSupportedException("The document cannot be null.");
        }

        _docMimeType = doc.MimeType;
        _docName = doc.DocumentName;

        ImagingHelper imaging = new ImagingHelper(company);
        _contentStream = imaging.RetrieveFileImage(doc.FileNetReference);
    }

    //public void Merge(Survey survey, User user)
    //{
    //    if (survey == null)
    //    {
    //        throw new Exception("Survey to be used to mail merge is null.");
    //    }

    //    if (_contentStream == null)
    //    {
    //        throw new Exception("The content stream to be used to mail merge is null.");
    //    }

    //    MailMerge mergedDoc = new MailMerge(_contentStream);
    //    mergedDoc.Merge(survey);
    //    _contentStream = mergedDoc.GetMergedDocument;
    //    _contentStream.Position = 0;
    //}

    public void AttachDocToResponse(Page page)           //WA=621 Doug Bruce 2015-05-05 - Created this stub to preserve previous compatible functionality.
    {
        AttachDocToResponse(page, "application/pdf");
    }

    public void AttachDocToResponse(Page page, string mimeType)     //WA=621 Doug Bruce 2015-05-05 - Added mimeType to allow passing in so stream is displayed correctly
    {
        // fill a buffer with the data from the stream
        byte[] content;
        using (_contentStream)
        {
            int bufferSize = (int)_contentStream.Length;
            content = new byte[bufferSize];

            int bytesRead = _contentStream.Read(content, 0, bufferSize);
            if (bytesRead != bufferSize)
            {
                throw new Exception(bytesRead + " bytes were read from the content stream.  Exactly " + bufferSize + " bytes were expected.");
            }
        }

        // remove any content in the response
        page.Response.Clear();
        page.Response.Buffer = true;

        // set the MIME type and provide a 'friendly' filename for the client
        if (mimeType != null && mimeType != String.Empty)
        {
            page.Response.ContentType = mimeType;   //WA=621 Doug Bruce 2015-05-05 - Added mimeType to allow passing in so stream is displayed correctly
        }
        else
        {
            page.Response.ContentType = "application/pdf";
        }

        string contentDisposition;

        if (page.Request.Browser.Browser == "IE" && (page.Request.Browser.Version == "7.0" || page.Request.Browser.Version == "8.0"))
            contentDisposition = "attachment; filename=" + Uri.EscapeDataString(_docName);
        else
            contentDisposition = "attachment; filename=\"" + _docName + "\"; filename*=UTF-8''" + Uri.EscapeDataString(_docName);

        page.Response.AddHeader("Content-Disposition", contentDisposition);

        // stream the file content to the user
        if (page.Response.IsClientConnected)
        {
            page.Response.BinaryWrite(content);
            page.Response.Flush();
            page.Response.End();
        }
    }

    /// <summary>
    /// Returns a document's extension based on 1) file extension, and 2) the MIME type passed.
    /// WA-855 Doug Bruce 2015-08-25 - If they conflict, filename extension wins, except for 
    /// .tif case below.
    /// </summary>
    /// <param name="mimeType">The MIME Type.</param>
    public static string GetExtension(string mimeType, bool returnImageExt, string fileName)
    {
        mimeType = mimeType.Trim().ToLower();
        string fileExtension = System.IO.Path.GetExtension(fileName).ToLower();
        string result = fileExtension;

        MimeType mimeTypeObject = MimeType.GetOne("Name = ?", mimeType);

        if (mimeTypeObject != null)
        {
            if (mimeTypeObject.DocumentExtension == ".tif")
            {
                result = (returnImageExt) ? mimeTypeObject.DocumentExtension : ".pdf";
            }
            else if (System.IO.Path.GetExtension(fileName) == ".dcd")
            {
                result = ".dcd";
            }
            else
            {
                //WA-881 Doug Bruce 2015-08-31 - Added to support generic extension retrieval from mime type
                result = mimeTypeObject.DocumentExtension;
            }
        }
        else
        {
            throw new NotSupportedException(string.Format("Was not expecting a MIME type of: '{0}'.", mimeType));
        }

        return result;
    }

    /// <summary>
    /// Expose PDF Conversion routine so it can be used outside of StreamDocument.
    /// At this point, there should be only ONE file in the Files array, but this will
    /// handle it even if there are more than one. All files will be merged together
    /// into a pdf (via writing/reading files) and that pdf stream will REPLACE the
    /// Files array in the first (0) position in the returned document.
    /// </summary>
    /// <param name="mimeType">The MIME Type.</param>
    public System.IO.Stream ConvertPDF(IFile[] filesIn, string docExtension)
    {
        List<String> fileList = new List<string>(filesIn.Length + 1);
        System.IO.Stream contentOut = new MemoryStream();

        //Write each image document out to the specified location.
        foreach (Berkley.Imaging.IFile file in filesIn)
        {
            string filePath = ConfigurationManager.AppSettings["TempDocPath"] + Guid.NewGuid().ToString() + docExtension;
            fileList.Add(filePath);

            MemoryStream ms = (MemoryStream)file.Content;
            FileStream fs = new FileStream(filePath, FileMode.Create);

            fs.Write(ms.ToArray(), 0, (int)ms.Length);
            fs.Flush();
            fs.Close();
        }

        //build our new path
        string pdfFilePath = ConfigurationManager.AppSettings["TempDocPath"] + Guid.NewGuid().ToString() + ".pdf";

        //convert the image documents into a pdf
        WRBCConvert.Converter pdfConverter = new WRBCConvert.Converter();
        try
        {
            contentOut = pdfConverter.MakePDF(fileList.ToArray(), pdfFilePath, true);
        }
        catch (Exception)
        {
            contentOut = null;  //If this comes back null, revert to the TIF file.
        }

        return contentOut;
    }

}
