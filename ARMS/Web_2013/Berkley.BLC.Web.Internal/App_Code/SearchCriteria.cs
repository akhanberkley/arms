using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Data;
using Berkley.BLC.Entities;

/// <summary>
/// Container for survey search criteria, which utility methods to convert the values to and from the query string.
/// </summary>
public class SearchCriteria
{
    #region --- Public Members ---

    // survey search fields
    public int SurveyNumber = int.MinValue;
    public const string SurveyNumberQS = "sn";

    public Guid SurveyTypeID = Guid.Empty;
    public const string SurveyTypeIDQS = "st";

    public Guid PolicySystemID = Guid.Empty;
    public const string PolicySystemIDQS = "ps";
        
    public string ServiceTypeCode = null;
    public const string ServiceTypeCodeQS = "servt";

    public Guid UserID = Guid.Empty;
    public const string UserIDQS = "au";

    public Guid RecommendedUserID = Guid.Empty;
    public const string RecommendedUserIDQS = "rau";

    public Guid ConsultantID = Guid.Empty;
    public const string ConsultantIDQS = "conu";

    public Guid ReviewerID = Guid.Empty;
    public const string ReviewerIDQS = "tu";

    public Guid CreatedByStaffID = Guid.Empty;
    public const string CreatedByStaffIDQS = "cbs";

    public string CreatedByNonStaff = null;
    public const string CreatedByNonStaffQS = "cbns";

    public string SurveyStatusCode = null;
    public const string SurveyStatusCodeQS = "as";

    public string ProfitCenterByUser = null;
    public const string ProfitCenterByUserQS = "pcbu";

    public string UnderwritingAccepted = null;
    public const string UnderwritingAcceptedQS = "uwa";
    
    public string Correction = null;
    public const string CorrectionQS = "cc";

    public string OpenRecs = null;
    public const string OpenRecsQS = "or";

    public string CriticalRecs = null;
    public const string CriticalRecsQS = "cr";
    
    public bool OpenCriticalRecs;
    public const string OpenCriticalRecsQS = "ocr";

    public string PACEAccount = null;
    public const string PACEAccountQS = "pacc";

    public string KeyAccount = null;
    public const string KeyAccountQS = "kacc";

    public string LossSensitive = null;
    public const string LossSensitiveQS = "lsen";

    public Guid SurveyQualityID = Guid.Empty;
    public const string SurveyQualityIDQS = "sq";

    public DateTime LastModifiedStart = DateTime.MinValue;
    public const string LastModifiedStartQS = "lms";

    public DateTime LastModifiedEnd = DateTime.MaxValue;
    public const string LastModifiedEndQS = "lme";

    public DateTime CreatedOnStart = DateTime.MinValue;
    public const string CreatedOnStartQS = "cos";

    public DateTime CreatedOnEnd = DateTime.MaxValue;
    public const string CreatedOnEndQS = "coe";

    public DateTime DueDateStart = DateTime.MinValue;
    public const string DueDateStartQS = "dds";

    public DateTime DueDateEnd = DateTime.MaxValue;
    public const string DueDateEndQS = "dde";

    public DateTime AssignDateStart = DateTime.MinValue;
    public const string AssignDateStartQS = "ands";

    public DateTime AssignDateEnd = DateTime.MaxValue;
    public const string AssignDateEndQS = "ande";

    public DateTime AcceptDateStart = DateTime.MinValue;
    public const string AcceptDateStartQS = "acds";

    public DateTime AcceptDateEnd = DateTime.MaxValue;
    public const string AcceptDateEndQS = "acde";

    public DateTime UnderwritingAcceptDateStart = DateTime.MinValue;
    public const string UnderwritingAcceptDateStartQS = "uwads";

    public DateTime UnderwritingAcceptDateEnd = DateTime.MaxValue;
    public const string UnderwritingAcceptDateEndQS = "uwade";

    public DateTime DateSurveyedStart = DateTime.MinValue;
    public const string DateSurveyedStartQS = "dss";

    public DateTime DateSurveyedEnd = DateTime.MaxValue;
    public const string DateSurveyedEndQS = "dese";

    public DateTime CompletedReportDateStart = DateTime.MinValue;
    public const string CompletedReportDateStartQS = "crds";

    public DateTime CompletedReportDateEnd = DateTime.MaxValue;
    public const string CompletedReportDateEndQS = "crde";

    public DateTime CanceledDateStart = DateTime.MinValue;
    public const string CanceledDateStartQS = "cds";

    public DateTime CanceledDateEnd = DateTime.MaxValue;
    public const string CanceledDateEndQS = "cde";

    public DateTime CompletedDateStart = DateTime.MinValue;
    public const string CompletedDateStartQS = "cods";

    public DateTime CompletedDateEnd = DateTime.MaxValue;
    public const string CompletedDateEndQS = "code";

    public DateTime ReviewerCompletedDateStart = DateTime.MinValue;
    public const string ReviewerCompletedDateStartQS = "tods";

    public DateTime ReviewerCompletedDateEnd = DateTime.MaxValue;
    public const string ReviewerCompletedDateEndQS = "tode";

    public Guid SurveyLetterID = Guid.Empty;
    public const string SurveyLetterIDQS = "slet";

    public Guid SurveyReleaseOwnershipTypeID = Guid.Empty;
    public const string SurveyReleaseOwnershipTypeIDQS = "sroti";

    // insured search fields
    public string InsuredClientID = null;
    public const string InsuredClientIDQS = "icid";

    public string InsuredPortfolioID = null;
    public const string InsuredPortfolioIDQS = "ipid";

    public string InsuredName = null;
    public const string InsuredNameQS = "in";

    public string InsuredDBA = null;
    public const string InsuredDBAQS = "idba";

    public string InsuredCity = null;
    public const string InsuredCityQS = "ic";

    public string InsuredStateCode = null;
    public const string InsuredStateCodeQS = "is";

    public string InsuredZipCode = null;
    public const string InsuredZipCodeQS = "iz";

    public DateTime InsuredNonRenewedDateStart = DateTime.MinValue;
    public const string InsuredNonRenewedDateStartQS = "inrd";

    public DateTime InsuredNonRenewedDateEnd = DateTime.MaxValue;
    public const string InsuredNonRenewedDateEndQS = "inrs";

    public string InsuredBusinessOperations = null;
    public const string InsuredBusinessOperationsQS = "ibo";

    public string InsuredSICCode = null;
    public const string InsuredSICCodeQS = "isic";

    public string PrimaryPolicyProfitCenter = null;
    public const string PrimaryPolicyProfitCenterQS = "pc";

    public string Underwriter = null;
    public const string UnderwriterQS = "uw";

    // location search fields
    public string LocationCity = null;
    public const string LocationCityQS = "lc";

    public string LocationStateCode = null;
    public const string LocationStateCodeQS = "ls";

    public string LocationZipCode = null;
    public const string LocationZipCodeQS = "lz";

    // policy search fields
    public string PolicyNumber = null;
    public const string PolicyNumberQS = "pnum";

    public DateTime PolicyEffectiveDateStart = DateTime.MinValue;
    public const string PolicyEffectiveDateStartQS = "peffs";

    public DateTime PolicyEffectiveDateEnd = DateTime.MaxValue;
    public const string PolicyEffectiveDateEndQS = "peffe";

    public DateTime PolicyExpirationDateStart = DateTime.MinValue;
    public const string PolicyExpirationDateStartQS = "pexps";

    public DateTime PolicyExpirationDateEnd = DateTime.MaxValue;
    public const string PolicyExpirationDateEndQS = "pexpe";

    public decimal TotalPremiumMin = decimal.MinValue;
    public const string TotalPremiumMinQS = "tps";

    public decimal TotalPremiumMax = decimal.MaxValue;
    public const string TotalPremiumMaxQS = "tpe";

    // agency search fields
    public string AgencyNumber = null;
    public const string AgencyNumberQS = "gnm";

    public string AgencyName = null;
    public const string AgencyNameQS = "gna";

    public string AgencyStateCode = null;
    public const string AgencyStateCodeQS = "ast";

    #endregion

    public static SearchCriteria Create(NameValueCollection queryString)
    {
        SearchCriteria c = new SearchCriteria();

        foreach (string key in queryString.AllKeys)
        {
            // skip empty keys (yes, it is actually possible)
            if (key == null || key.Length == 0)
            {
                continue;
            }   

            string value = queryString[key];

            // skip empty values
            if (value == null || value.Length == 0)
            {
                continue;
            }

            switch (key.ToLower())
            {
                case SurveyNumberQS: c.SurveyNumber = TryParseInt(value, int.MinValue); break;
                case SurveyTypeIDQS: c.SurveyTypeID = TryParseGuid(value, Guid.Empty); break;
                case PolicySystemIDQS: c.PolicySystemID = TryParseGuid(value, Guid.Empty); break;                                    
                case ServiceTypeCodeQS: c.ServiceTypeCode = value.ToUpper(); break;
                case UserIDQS: c.UserID = TryParseGuid(value, Guid.Empty); break;
                case RecommendedUserIDQS: c.RecommendedUserID = TryParseGuid(value, Guid.Empty); break;
                case ConsultantIDQS: c.ConsultantID = TryParseGuid(value, Guid.Empty); break;
                case ReviewerIDQS: c.ReviewerID = TryParseGuid(value, Guid.Empty); break;
                case CreatedByStaffIDQS: c.CreatedByStaffID = TryParseGuid(value, Guid.Empty); break;
                case CreatedByNonStaffQS: c.CreatedByNonStaff = value; break;
                case SurveyStatusCodeQS: c.SurveyStatusCode = value.ToUpper(); break;
                case ProfitCenterByUserQS: c.ProfitCenterByUser = value.ToUpper(); break;
                case UnderwritingAcceptedQS: c.UnderwritingAccepted = value.ToUpper(); break;
                case CorrectionQS: c.Correction = value.ToUpper(); break;
                case OpenRecsQS: c.OpenRecs = value.ToUpper(); break;
                case CriticalRecsQS: c.CriticalRecs = value.ToUpper(); break;
                case OpenCriticalRecsQS: c.OpenCriticalRecs = TryParseBool(value, false);  break;                
                case PACEAccountQS: c.PACEAccount = value.ToUpper(); break;
                case KeyAccountQS: c.KeyAccount = value.ToUpper(); break;
                case LossSensitiveQS: c.LossSensitive = value.ToUpper(); break;
                case SurveyQualityIDQS: c.SurveyQualityID = TryParseGuid(value, Guid.Empty); break;
                case LastModifiedStartQS: c.LastModifiedStart = TryParseDate(value, DateTime.MinValue); break;
                case LastModifiedEndQS: c.LastModifiedEnd = TryParseDate(value, DateTime.MaxValue); break;
                case CreatedOnStartQS: c.CreatedOnStart = TryParseDate(value, DateTime.MinValue); break;
                case CreatedOnEndQS: c.CreatedOnEnd = TryParseDate(value, DateTime.MaxValue); break;
                case DueDateStartQS: c.DueDateStart = TryParseDate(value, DateTime.MinValue); break;
                case DueDateEndQS: c.DueDateEnd = TryParseDate(value, DateTime.MaxValue); break;
                case AssignDateStartQS: c.AssignDateStart = TryParseDate(value, DateTime.MinValue); break;
                case AssignDateEndQS: c.AssignDateEnd = TryParseDate(value, DateTime.MaxValue); break;
                case AcceptDateStartQS: c.AcceptDateStart = TryParseDate(value, DateTime.MinValue); break;
                case AcceptDateEndQS: c.AcceptDateEnd = TryParseDate(value, DateTime.MaxValue); break;
                case UnderwritingAcceptDateStartQS: c.UnderwritingAcceptDateStart = TryParseDate(value, DateTime.MinValue); break;
                case UnderwritingAcceptDateEndQS: c.UnderwritingAcceptDateEnd = TryParseDate(value, DateTime.MaxValue); break;
                case DateSurveyedStartQS: c.DateSurveyedStart = TryParseDate(value, DateTime.MinValue); break;
                case DateSurveyedEndQS: c.DateSurveyedEnd = TryParseDate(value, DateTime.MaxValue); break;
                case CompletedReportDateStartQS: c.CompletedReportDateStart = TryParseDate(value, DateTime.MinValue); break;
                case CompletedReportDateEndQS: c.CompletedReportDateEnd = TryParseDate(value, DateTime.MaxValue); break;
                case CanceledDateStartQS: c.CanceledDateStart = TryParseDate(value, DateTime.MinValue); break;
                case CanceledDateEndQS: c.CanceledDateEnd = TryParseDate(value, DateTime.MaxValue); break;
                case CompletedDateEndQS: c.CompletedDateEnd = TryParseDate(value, DateTime.MaxValue); break;
                case CompletedDateStartQS: c.CompletedDateStart = TryParseDate(value, DateTime.MaxValue); break;
                case ReviewerCompletedDateEndQS: c.ReviewerCompletedDateEnd = TryParseDate(value, DateTime.MaxValue); break;
                case ReviewerCompletedDateStartQS: c.ReviewerCompletedDateStart = TryParseDate(value, DateTime.MaxValue); break;
                case SurveyLetterIDQS: c.SurveyLetterID = TryParseGuid(value, Guid.Empty); break;
                case SurveyReleaseOwnershipTypeIDQS: c.SurveyReleaseOwnershipTypeID = TryParseGuid(value, Guid.Empty); break;

                case InsuredClientIDQS: c.InsuredClientID = value; break;
                case InsuredPortfolioIDQS: c.InsuredPortfolioID = value; break;
                case InsuredNameQS: c.InsuredName = value; break;
                case InsuredDBAQS: c.InsuredDBA = value; break;
                case InsuredCityQS: c.InsuredCity = value; break;
                case InsuredStateCodeQS: c.InsuredStateCode = value.ToUpper(); break;
                case InsuredZipCodeQS: c.InsuredZipCode = value; break;
                case InsuredNonRenewedDateStartQS: c.InsuredNonRenewedDateStart = TryParseDate(value, DateTime.MinValue); break;
                case InsuredNonRenewedDateEndQS: c.InsuredNonRenewedDateEnd = TryParseDate(value, DateTime.MaxValue); break;
                case InsuredBusinessOperationsQS: c.InsuredBusinessOperations = value; break;
                case InsuredSICCodeQS: c.InsuredSICCode = value.ToUpper(); break;
                case PrimaryPolicyProfitCenterQS: c.PrimaryPolicyProfitCenter = value.ToUpper(); break;
                case UnderwriterQS: c.Underwriter = value; break;

                case LocationCityQS: c.LocationCity = value; break;
                case LocationStateCodeQS: c.LocationStateCode = value.ToUpper(); break;
                case LocationZipCodeQS: c.LocationZipCode = value; break;

                case PolicyNumberQS: c.PolicyNumber = value; break;
                case PolicyEffectiveDateStartQS: c.PolicyEffectiveDateStart = TryParseDate(value, DateTime.MinValue); break;
                case PolicyEffectiveDateEndQS: c.PolicyEffectiveDateEnd = TryParseDate(value, DateTime.MaxValue); break;
                case PolicyExpirationDateStartQS: c.PolicyExpirationDateStart = TryParseDate(value, DateTime.MinValue); break;
                case PolicyExpirationDateEndQS: c.PolicyExpirationDateEnd = TryParseDate(value, DateTime.MaxValue); break;
                case TotalPremiumMinQS: c.TotalPremiumMin = TryParseDecimal(value, decimal.MinValue); break;
                case TotalPremiumMaxQS: c.TotalPremiumMax = TryParseDecimal(value, decimal.MaxValue); break;

                case AgencyNumberQS: c.AgencyNumber = value; break;
                case AgencyNameQS: c.AgencyName = value; break;
                case AgencyStateCodeQS: c.AgencyStateCode = value.ToUpper(); break;
            }
        }

        return c;
    }

    public static string BuildPolicyQuery(Guid companyID, string policyNumber)
    {
        string result = string.Empty;

        //if policy number is searched on, first get surveys that have this policy for this company
        if (!string.IsNullOrEmpty(policyNumber))
        {
            DataTable dt = StoredProcedures.SurveyIDsByPolicy_Get(policyNumber, companyID);

            result += " && ( ";
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    result += string.Format(" SurveyID = '{0}' || ", dt.Rows[i][0].ToString());
                }
            }
            else
            {
                result += string.Format("SurveyID = '{0}'", Guid.Empty);
            }

            result = result.Trim().TrimEnd('|');
            result += " ) ";
        }

        return result;
    }

    public string ToQueryString()
    {
        StringBuilder sb = new StringBuilder();

        // survey search fields
        if (SurveyNumber != int.MinValue)
        {
            sb.AppendFormat("&{0}=", SurveyNumberQS);
            sb.Append(HttpUtility.UrlEncode(SurveyNumber.ToString()));
        }
        if (SurveyTypeID != Guid.Empty)
        {
            sb.AppendFormat("&{0}=", SurveyTypeIDQS);
            sb.Append(HttpUtility.UrlEncode(SurveyTypeID.ToString()));
        }
        if (PolicySystemID != Guid.Empty)
        {
            sb.AppendFormat("&{0}=", PolicySystemIDQS);
            sb.Append(HttpUtility.UrlEncode(PolicySystemID.ToString()));
        }    
        if (ServiceTypeCode != null)
        {
            sb.AppendFormat("&{0}=", ServiceTypeCodeQS);
            sb.Append(HttpUtility.UrlEncode(ServiceTypeCode));
        }
        if (UserID != Guid.Empty)
        {
            sb.AppendFormat("&{0}=", UserIDQS);
            sb.Append(HttpUtility.UrlEncode(UserID.ToString()));
        }
        if (RecommendedUserID != Guid.Empty)
        {
            sb.AppendFormat("&{0}=", RecommendedUserIDQS);
            sb.Append(HttpUtility.UrlEncode(RecommendedUserID.ToString()));
        }
        if (ConsultantID != Guid.Empty)
        {
            sb.AppendFormat("&{0}=", ConsultantIDQS);
            sb.Append(HttpUtility.UrlEncode(ConsultantID.ToString()));
        }
        if (ReviewerID != Guid.Empty)
        {
            sb.AppendFormat("&{0}=", ReviewerIDQS);
            sb.Append(HttpUtility.UrlEncode(ReviewerID.ToString()));
        }
        if (CreatedByStaffID != Guid.Empty)
        {
            sb.AppendFormat("&{0}=", CreatedByStaffIDQS);
            sb.Append(HttpUtility.UrlEncode(CreatedByStaffID.ToString()));
        }
        if (CreatedByNonStaff != null)
        {
            sb.AppendFormat("&{0}=", CreatedByNonStaffQS);
            sb.Append(HttpUtility.UrlEncode(CreatedByNonStaff.ToString()));
        }
        if (SurveyStatusCode != null)
        {
            sb.AppendFormat("&{0}=", SurveyStatusCodeQS);
            sb.Append(HttpUtility.UrlEncode(SurveyStatusCode));
        }
        if (ProfitCenterByUser != null)
        {
            sb.AppendFormat("&{0}=", ProfitCenterByUserQS);
            sb.Append(HttpUtility.UrlEncode(ProfitCenterByUser));
        }
        if (UnderwritingAccepted != null)
        {
            sb.AppendFormat("&{0}=", UnderwritingAcceptedQS);
            sb.Append(HttpUtility.UrlEncode(UnderwritingAccepted.ToString()));
        }
        if (Correction != null)
        {
            sb.AppendFormat("&{0}=", CorrectionQS);
            sb.Append(HttpUtility.UrlEncode(Correction.ToString()));
        }
        if (OpenRecs != null)
        {
            sb.AppendFormat("&{0}=", OpenRecsQS);
            sb.Append(HttpUtility.UrlEncode(OpenRecs.ToString()));
        }
        if (CriticalRecs != null)
        {
            sb.AppendFormat("&{0}=", CriticalRecsQS);
            sb.Append(HttpUtility.UrlEncode(CriticalRecs.ToString()));
        }
        if (OpenCriticalRecs)
        {
            sb.AppendFormat("&{0}=", OpenCriticalRecsQS);
            sb.Append(HttpUtility.UrlEncode(OpenCriticalRecs.ToString()));
        }
        if (PACEAccount != null)
        {
            sb.AppendFormat("&{0}=", PACEAccountQS);
            sb.Append(HttpUtility.UrlEncode(PACEAccount.ToString()));
        }
        if (KeyAccount != null)
        {
            sb.AppendFormat("&{0}=", KeyAccountQS);
            sb.Append(HttpUtility.UrlEncode(KeyAccount.ToString()));
        }
        if (LossSensitive != null)
        {
            sb.AppendFormat("&{0}=", LossSensitiveQS);
            sb.Append(HttpUtility.UrlEncode(LossSensitive.ToString()));
        }
        if (SurveyQualityID != Guid.Empty)
        {
            sb.AppendFormat("&{0}=", SurveyQualityIDQS);
            sb.Append(HttpUtility.UrlEncode(SurveyQualityID.ToString()));
        }
        if (LastModifiedStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", LastModifiedStartQS);
            sb.Append(HttpUtility.UrlEncode(LastModifiedStart.ToShortDateString()));
        }
        if (LastModifiedEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", LastModifiedEndQS);
            sb.Append(HttpUtility.UrlEncode(LastModifiedEnd.ToShortDateString()));
        }
        if (CreatedOnStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", CreatedOnStartQS);
            sb.Append(HttpUtility.UrlEncode(CreatedOnStart.ToShortDateString()));
        }
        if (CreatedOnEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", CreatedOnEndQS);
            sb.Append(HttpUtility.UrlEncode(CreatedOnEnd.ToShortDateString()));
        }
        if (DueDateStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", DueDateStartQS);
            sb.Append(HttpUtility.UrlEncode(DueDateStart.ToShortDateString()));
        }
        if (DueDateEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", DueDateEndQS);
            sb.Append(HttpUtility.UrlEncode(DueDateEnd.ToShortDateString()));
        }
        if (AssignDateStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", AssignDateStartQS);
            sb.Append(HttpUtility.UrlEncode(AssignDateStart.ToShortDateString()));
        }
        if (AssignDateEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", AssignDateEndQS);
            sb.Append(HttpUtility.UrlEncode(AssignDateEnd.ToShortDateString()));
        }
        if (AcceptDateStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", AcceptDateStartQS);
            sb.Append(HttpUtility.UrlEncode(AcceptDateStart.ToShortDateString()));
        }
        if (AcceptDateEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", AcceptDateEndQS);
            sb.Append(HttpUtility.UrlEncode(AcceptDateEnd.ToShortDateString()));
        }
        if (UnderwritingAcceptDateStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", UnderwritingAcceptDateStartQS);
            sb.Append(HttpUtility.UrlEncode(UnderwritingAcceptDateStart.ToShortDateString()));
        }
        if (UnderwritingAcceptDateEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", UnderwritingAcceptDateEndQS);
            sb.Append(HttpUtility.UrlEncode(UnderwritingAcceptDateEnd.ToShortDateString()));
        }
        if (DateSurveyedStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", DateSurveyedStartQS);
            sb.Append(HttpUtility.UrlEncode(DateSurveyedStart.ToShortDateString()));
        }
        if (DateSurveyedEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", DateSurveyedEndQS);
            sb.Append(HttpUtility.UrlEncode(DateSurveyedEnd.ToShortDateString()));
        }
        if (CompletedReportDateStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", CompletedReportDateStartQS);
            sb.Append(HttpUtility.UrlEncode(CompletedReportDateStart.ToShortDateString()));
        }
        if (CompletedReportDateEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", CompletedReportDateEndQS);
            sb.Append(HttpUtility.UrlEncode(CompletedReportDateEnd.ToShortDateString()));
        }
        if (CanceledDateStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", CanceledDateStartQS);
            sb.Append(HttpUtility.UrlEncode(CanceledDateStart.ToShortDateString()));
        }
        if (CanceledDateEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", CanceledDateEndQS);
            sb.Append(HttpUtility.UrlEncode(CanceledDateEnd.ToShortDateString()));
        }
        if (CompletedDateStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", CompletedDateStartQS);
            sb.Append(HttpUtility.UrlEncode(CompletedDateStart.ToShortDateString()));
        }
        if (CompletedDateEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", CompletedDateEndQS);
            sb.Append(HttpUtility.UrlEncode(CompletedDateEnd.ToShortDateString()));
        }
        if (ReviewerCompletedDateStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", ReviewerCompletedDateStartQS);
            sb.Append(HttpUtility.UrlEncode(ReviewerCompletedDateStart.ToShortDateString()));
        }
        if (ReviewerCompletedDateEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", ReviewerCompletedDateEndQS);
            sb.Append(HttpUtility.UrlEncode(ReviewerCompletedDateEnd.ToShortDateString()));
        }
        if (SurveyLetterID != Guid.Empty)
        {
            sb.AppendFormat("&{0}=", SurveyLetterIDQS);
            sb.Append(HttpUtility.UrlEncode(SurveyLetterID.ToString()));
        }
        if (SurveyReleaseOwnershipTypeID != Guid.Empty)
        {
            sb.AppendFormat("&{0}=", SurveyReleaseOwnershipTypeIDQS);
            sb.Append(HttpUtility.UrlEncode(SurveyReleaseOwnershipTypeID.ToString()));
        }

        // insured fields
        if (InsuredClientID != null)
        {
            sb.AppendFormat("&{0}=", InsuredClientIDQS);
            sb.Append(HttpUtility.UrlEncode(InsuredClientID));
        }
        if (InsuredPortfolioID != null)
        {
            sb.AppendFormat("&{0}=", InsuredPortfolioIDQS);
            sb.Append(HttpUtility.UrlEncode(InsuredPortfolioID));
        }
        if (InsuredName != null)
        {
            sb.AppendFormat("&{0}=", InsuredNameQS);
            sb.Append(HttpUtility.UrlEncode(InsuredName));
        }
        if (InsuredDBA != null)
        {
            sb.AppendFormat("&{0}=", InsuredDBAQS);
            sb.Append(HttpUtility.UrlEncode(InsuredDBA));
        }
        if (InsuredCity != null)
        {
            sb.AppendFormat("&{0}=", InsuredCityQS);
            sb.Append(HttpUtility.UrlEncode(InsuredCity));
        }
        if (InsuredStateCode != null)
        {
            sb.AppendFormat("&{0}=", InsuredStateCodeQS);
            sb.Append(HttpUtility.UrlEncode(InsuredStateCode));
        }
        if (InsuredZipCode != null)
        {
            sb.AppendFormat("&{0}=", InsuredZipCodeQS);
            sb.Append(HttpUtility.UrlEncode(InsuredZipCode));
        }
        if (InsuredNonRenewedDateStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", InsuredNonRenewedDateStartQS);
            sb.Append(HttpUtility.UrlEncode(InsuredNonRenewedDateStart.ToShortDateString()));
        }
        if (InsuredNonRenewedDateEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", InsuredNonRenewedDateEndQS);
            sb.Append(HttpUtility.UrlEncode(InsuredNonRenewedDateEnd.ToShortDateString()));
        }
        if (InsuredBusinessOperations != null)
        {
            sb.AppendFormat("&{0}=", InsuredBusinessOperationsQS);
            sb.Append(HttpUtility.UrlEncode(InsuredBusinessOperations));
        }
        if (InsuredSICCode != null)
        {
            sb.AppendFormat("&{0}=", InsuredSICCodeQS);
            sb.Append(HttpUtility.UrlEncode(InsuredSICCode));
        }
        if (PrimaryPolicyProfitCenter != null)
        {
            sb.AppendFormat("&{0}=", PrimaryPolicyProfitCenterQS);
            sb.Append(HttpUtility.UrlEncode(PrimaryPolicyProfitCenter));
        }
        if (Underwriter != null)
        {
            sb.AppendFormat("&{0}=", UnderwriterQS);
            sb.Append(HttpUtility.UrlEncode(Underwriter.ToString()));
        }

        // location search fields
        if (LocationCity != null)
        {
            sb.AppendFormat("&{0}=", LocationCityQS);
            sb.Append(HttpUtility.UrlEncode(LocationCity));
        }
        if (LocationStateCode != null)
        {
            sb.AppendFormat("&{0}=", LocationStateCodeQS);
            sb.Append(HttpUtility.UrlEncode(LocationStateCode));
        }
        if (LocationZipCode != null)
        {
            sb.AppendFormat("&{0}=", LocationZipCodeQS);
            sb.Append(HttpUtility.UrlEncode(LocationZipCode));
        }

        // policy search fields
        if (PolicyNumber != null)
        {
            sb.AppendFormat("&{0}=", PolicyNumberQS);
            sb.Append(HttpUtility.UrlEncode(PolicyNumber));
        }
        if (PolicyEffectiveDateStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", PolicyEffectiveDateStartQS);
            sb.Append(HttpUtility.UrlEncode(PolicyEffectiveDateStart.ToShortDateString()));
        }
        if (PolicyEffectiveDateEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", PolicyEffectiveDateEndQS);
            sb.Append(HttpUtility.UrlEncode(PolicyEffectiveDateEnd.ToShortDateString()));
        }
        if (PolicyExpirationDateStart != DateTime.MinValue)
        {
            sb.AppendFormat("&{0}=", PolicyExpirationDateStartQS);
            sb.Append(HttpUtility.UrlEncode(PolicyExpirationDateStart.ToShortDateString()));
        }
        if (PolicyExpirationDateEnd != DateTime.MaxValue)
        {
            sb.AppendFormat("&{0}=", PolicyExpirationDateEndQS);
            sb.Append(HttpUtility.UrlEncode(PolicyExpirationDateEnd.ToShortDateString()));
        }
        if (TotalPremiumMin != decimal.MinValue)
        {
            sb.AppendFormat("&{0}=", TotalPremiumMinQS);
            sb.Append(HttpUtility.UrlEncode(TotalPremiumMin.ToString()));
        }
        if (TotalPremiumMax != decimal.MaxValue)
        {
            sb.AppendFormat("&{0}=", TotalPremiumMaxQS);
            sb.Append(HttpUtility.UrlEncode(TotalPremiumMax.ToString()));
        }

        // agency fields
        if (AgencyNumber != null)
        {
            sb.AppendFormat("&{0}=", AgencyNumberQS);
            sb.Append(HttpUtility.UrlEncode(AgencyNumber));
        }
        if (AgencyName != null)
        {
            sb.AppendFormat("&{0}=", AgencyNameQS);
            sb.Append(HttpUtility.UrlEncode(AgencyName));
        }
        if (AgencyStateCode != null)
        {
            sb.AppendFormat("&{0}=", AgencyStateCodeQS);
            sb.Append(HttpUtility.UrlEncode(AgencyStateCode));
        }

        // strip any leading '&' off the front
        string url = sb.ToString();
        url.TrimStart('&');

        return url;
    }


    public string GetOPathQuery(Guid companyID, out object[] parameters)
    {
        string surveyClause = string.Empty;
        ArrayList surveyArgs = new ArrayList();

        // survey search fields
        if (this.SurveyNumber != int.MinValue)
        {
            surveyClause += "&& SurveyNumber = ?";
            surveyArgs.Add(this.SurveyNumber);
        }
        if (this.SurveyTypeID != Guid.Empty)
        {
            surveyClause += "&& SurveyTypeID = ?";
            surveyArgs.Add(this.SurveyTypeID);
        }
        if (this.PolicySystemID != Guid.Empty)
        {
            surveyClause += "&& PolicySystemID = ?";
            surveyArgs.Add(this.PolicySystemID);
        }
        if (this.ServiceTypeCode != null)
        {
            surveyClause += "&& ServiceTypeCode = ?";
            surveyArgs.Add(this.ServiceTypeCode);
        }
        if (this.UserID != Guid.Empty)
        {
            surveyClause += "&& AssignedUserID = ?";
            surveyArgs.Add(this.UserID);
        }
        if (this.RecommendedUserID != Guid.Empty)
        {
            surveyClause += "&& RecommendedUserID = ?";
            surveyArgs.Add(this.RecommendedUserID);
        }
        if (this.ConsultantID != Guid.Empty)
        {
            surveyClause += "&& ConsultantUserID = ?";
            surveyArgs.Add(this.ConsultantID);
        }
        if (this.ReviewerID != Guid.Empty)
        {
            surveyClause += "&& ReviewerUserID = ?";
            surveyArgs.Add(this.ReviewerID);
        }
        if (this.CreatedByStaffID != Guid.Empty)
        {
            if (this.CreatedByStaffID == new Guid("B3EDF5CC-F499-4a12-ABAD-4899EBB1CAA4"))
            {
                surveyClause += "&& NOT ISNULL(CreateByInternalUserID)";
            }
            else
            {
                surveyClause += "&& CreateByInternalUserID = ?";
                surveyArgs.Add(this.CreatedByStaffID);
            }
        }
        if (this.CreatedByNonStaff != null)
        {
            if (this.CreatedByNonStaff.ToUpper() == "(ALL)")
            {
                surveyClause += "&& NOT ISNULL(CreateByExternalUserName)";
            }
            else
            {
                surveyClause += "&& CreateByExternalUserName = ?";
                surveyArgs.Add(this.CreatedByNonStaff);
            }
        }
        if (this.SurveyStatusCode != null)
        {
            surveyClause += "&& SurveyStatusCode = ?";
            surveyArgs.Add(this.SurveyStatusCode);
        }
        if (this.ProfitCenterByUser != null)
        {
            surveyClause += "&& ProfitCenterByUser LIKE ?";
            surveyArgs.Add("%" + this.ProfitCenterByUser + "%");
        }
        if (this.UnderwritingAccepted != null)
        {
            if (this.UnderwritingAccepted.ToUpper() == "YES")
            {
                surveyClause += "&& NOT ISNULL(UnderwritingAcceptDate)";
            }
            else
            {
                surveyClause += "&& ISNULL(UnderwritingAcceptDate)";
            }
        }
        if (this.Correction != null)
        {
            surveyClause += "&& Correction = ?";
            surveyArgs.Add(this.Correction);
        }
        if (this.OpenRecs != null)
        {
            if (this.OpenRecs == "1")
            {
                surveyClause += "&& RecCount LIKE ?";
                surveyArgs.Add("[1-9]%");
            }
            else 
            {
                surveyClause += "&& RecCount LIKE ?";
                surveyArgs.Add("0/%");
            }
        }
        if (this.CriticalRecs != null)
        {
            if (this.CriticalRecs == "1")
            {
                surveyClause += "&& ContainsCriticalRec = ?";
                surveyArgs.Add(this.CriticalRecs);
            }
        }
        if (this.OpenCriticalRecs != null)
        {
            if (this.OpenCriticalRecs)
            {
                surveyClause += "&& ContainsOpenCriticalRec = ?";
                surveyArgs.Add(this.OpenCriticalRecs);
            }
        }
        if (this.PACEAccount != null)
        {
            surveyClause += "&& PaceAccount = ?";
            surveyArgs.Add(this.PACEAccount);
        }
        if (this.KeyAccount != null)
        {
            surveyClause += "&& KeyAccount = ?";
            surveyArgs.Add(this.KeyAccount);
        }
        if (this.LossSensitive != null)
        {
            surveyClause += "&& LossSensitive = ?";
            surveyArgs.Add(this.LossSensitive);
        }
        if (this.SurveyQualityID != Guid.Empty)
        {
            surveyClause += "&& SurveyQualityID = ?";
            surveyArgs.Add(this.SurveyQualityID);
        }
        if (this.LastModifiedStart != DateTime.MinValue)
        {
            surveyClause += "&& LastModifiedOn >= ?";
            surveyArgs.Add(this.LastModifiedStart);
        }
        if (this.LastModifiedEnd != DateTime.MaxValue)
        {
            surveyClause += "&& LastModifiedOn < ?";
            surveyArgs.Add(this.LastModifiedEnd.AddDays(1));
        }
        if (this.CreatedOnStart != DateTime.MinValue)
        {
            surveyClause += "&& CreateDate >= ?";
            surveyArgs.Add(this.CreatedOnStart);
        }
        if (this.CreatedOnEnd != DateTime.MaxValue)
        {
            surveyClause += "&& CreateDate < ?";
            surveyArgs.Add(this.CreatedOnEnd.AddDays(1));
        }
        if (this.DueDateStart != DateTime.MinValue)
        {
            surveyClause += "&& DueDate >= ?";
            surveyArgs.Add(this.DueDateStart);
        }
        if (this.DueDateEnd != DateTime.MaxValue)
        {
            surveyClause += "&& DueDate < ?";
            surveyArgs.Add(this.DueDateEnd.AddDays(1));
        }
        if (this.AssignDateStart != DateTime.MinValue)
        {
            surveyClause += "&& AssignDate >= ?";
            surveyArgs.Add(this.AssignDateStart);
        }
        if (this.AssignDateEnd != DateTime.MaxValue)
        {
            surveyClause += "&& AssignDate < ?";
            surveyArgs.Add(this.AssignDateEnd.AddDays(1));
        }
        if (this.AcceptDateStart != DateTime.MinValue)
        {
            surveyClause += "&& AcceptDate >= ?";
            surveyArgs.Add(this.AcceptDateStart);
        }
        if (this.AcceptDateEnd != DateTime.MaxValue)
        {
            surveyClause += "&& AcceptDate < ?";
            surveyArgs.Add(this.AcceptDateEnd.AddDays(1));
        }
        if (this.UnderwritingAcceptDateStart != DateTime.MinValue)
        {
            surveyClause += "&& UnderwritingAcceptDate >= ?";
            surveyArgs.Add(this.UnderwritingAcceptDateStart);
        }
        if (this.UnderwritingAcceptDateEnd != DateTime.MaxValue)
        {
            surveyClause += "&& UnderwritingAcceptDate < ?";
            surveyArgs.Add(this.UnderwritingAcceptDateEnd.AddDays(1));
        }
        if (this.DateSurveyedStart != DateTime.MinValue)
        {
            surveyClause += "&& SurveyedDate >= ?";
            surveyArgs.Add(this.DateSurveyedStart);
        }
        if (this.DateSurveyedEnd != DateTime.MaxValue)
        {
            surveyClause += "&& SurveyedDate < ?";
            surveyArgs.Add(this.DateSurveyedEnd.AddDays(1));
        }
        if (this.CompletedReportDateStart != DateTime.MinValue)
        {
            surveyClause += "&& ReportCompleteDate >= ?";
            surveyArgs.Add(this.CompletedReportDateStart);
        }
        if (this.CompletedReportDateEnd != DateTime.MaxValue)
        {
            surveyClause += "&& ReportCompleteDate < ?";
            surveyArgs.Add(this.CompletedReportDateEnd.AddDays(1));
        }
        if (this.CanceledDateStart != DateTime.MinValue)
        {
            surveyClause += "&& CanceledDate >= ?";
            surveyArgs.Add(this.CanceledDateStart);
        }
        if (this.CanceledDateEnd != DateTime.MaxValue)
        {
            surveyClause += "&& CanceledDate < ?";
            surveyArgs.Add(this.CanceledDateEnd.AddDays(1));
        }
        if (this.CompletedDateStart != DateTime.MinValue)
        {
            surveyClause += "&& CompleteDate >= ?";
            surveyArgs.Add(this.CompletedDateStart);
        }
        if (this.CompletedDateEnd != DateTime.MaxValue)
        {
            surveyClause += "&& CompleteDate < ?";
            surveyArgs.Add(this.CompletedDateEnd.AddDays(1));
        }
        if (this.ReviewerCompletedDateStart != DateTime.MinValue)
        {
            surveyClause += "&& ReviewerCompleteDate >= ?";
            surveyArgs.Add(this.ReviewerCompletedDateStart);
        }
        if (this.ReviewerCompletedDateEnd != DateTime.MaxValue)
        {
            surveyClause += "&& ReviewerCompleteDate < ?";
            surveyArgs.Add(this.ReviewerCompletedDateEnd.AddDays(1));
        }
        if (this.SurveyLetterID != Guid.Empty)
        {
            surveyClause += "&& SurveyLetters LIKE ?";
            surveyArgs.Add("%" + this.SurveyLetterID + "%");
        }
        if (this.SurveyReleaseOwnershipTypeID != Guid.Empty)
        {
            surveyClause += "&& SurveyReleaseOwnershipTypeID = ?";
            surveyArgs.Add(this.SurveyReleaseOwnershipTypeID);
        }

        // insured fields
        if (this.InsuredClientID != null)
        {
            surveyClause += "&& ClientID = ?";
            surveyArgs.Add(this.InsuredClientID);
        }
        if (this.InsuredPortfolioID != null)
        {
            surveyClause += "&& PortfolioID = ?";
            surveyArgs.Add(this.InsuredPortfolioID);
        }
        if (this.InsuredName != null)
        {
            surveyClause += "&& InsuredName LIKE ?";
            surveyArgs.Add("%" + this.InsuredName + "%");
        }
        if (this.InsuredDBA != null)
        {
            surveyClause += "&& InsuredName2 LIKE ?";
            surveyArgs.Add("%" + this.InsuredDBA + "%");
        }
        if (this.InsuredCity != null)
        {
            surveyClause += "&& InsuredCity LIKE ?";
            surveyArgs.Add("%" + this.InsuredCity + "%");
        }
        if (this.InsuredStateCode != null)
        {
            surveyClause += "&& InsuredStateCode = ?";
            surveyArgs.Add(this.InsuredStateCode);
        }
        if (this.InsuredZipCode != null)
        {
            surveyClause += "&& InsuredZipCode LIKE ?";
            surveyArgs.Add(this.InsuredZipCode + "%");
        }
        if (this.InsuredNonRenewedDateStart != DateTime.MinValue)
        {
            surveyClause += "&& InsuredNonRenewedDate >= ?";
            surveyArgs.Add(this.InsuredNonRenewedDateStart);
        }
        if (this.InsuredNonRenewedDateEnd != DateTime.MaxValue)
        {
            surveyClause += "&& InsuredNonRenewedDate < ?";
            surveyArgs.Add(this.InsuredNonRenewedDateEnd.AddDays(1));
        }
        if (this.InsuredBusinessOperations != null)
        {
            surveyClause += "&& BusinessOperations LIKE ?";
            surveyArgs.Add(this.InsuredBusinessOperations + "%");
        }
        if (this.InsuredSICCode != null)
        {
            surveyClause += "&& SICCode = ?";
            surveyArgs.Add(this.InsuredSICCode);
        }
        if (this.PrimaryPolicyProfitCenter != null)
        {
            surveyClause += "&& ProfitCenter LIKE ?";
            surveyArgs.Add("%" + this.PrimaryPolicyProfitCenter + "%");
        }
        if (this.Underwriter != null)
        {
            surveyClause += "&& UnderwriterCode = ?";
            surveyArgs.Add(this.Underwriter);
        }

        // location fields
        if (this.LocationCity != null)
        {
            surveyClause += "&& LocationCity LIKE ?";
            surveyArgs.Add("%" + this.LocationCity + "%");
        }
        if (this.LocationStateCode != null)
        {
            surveyClause += "&& LocationStateCode = ?";
            surveyArgs.Add(this.LocationStateCode);
        }
        if (this.LocationZipCode != null)
        {
            surveyClause += "&& LocationZipCode LIKE ?";
            surveyArgs.Add(this.LocationZipCode + "%");
        }

        //policy search fields
        if (this.PolicyNumber != null)
        {
            surveyClause += "&& CompanyID = ?";
            surveyArgs.Add(companyID);
        }
        if (this.PolicyEffectiveDateStart != DateTime.MinValue)
        {
            surveyClause += "&& PolicyEffectiveDate >= ?";
            surveyArgs.Add(this.PolicyEffectiveDateStart);
        }
        if (this.PolicyEffectiveDateEnd != DateTime.MaxValue)
        {
            surveyClause += "&& PolicyEffectiveDate < ?";
            surveyArgs.Add(this.PolicyEffectiveDateEnd.AddDays(1));
        }
        if (this.PolicyExpirationDateStart != DateTime.MinValue)
        {
            surveyClause += "&& PolicyExpireDate >= ?";
            surveyArgs.Add(this.PolicyExpirationDateStart);
        }
        if (this.PolicyExpirationDateEnd != DateTime.MaxValue)
        {
            surveyClause += "&& PolicyExpireDate < ?";
            surveyArgs.Add(this.PolicyExpirationDateEnd.AddDays(1));
        }
        if (this.TotalPremiumMin != decimal.MinValue)
        {
            surveyClause += "&& PolicyPremium >= ?";
            surveyArgs.Add(this.TotalPremiumMin);
        }
        if (this.TotalPremiumMax != decimal.MaxValue)
        {
            surveyClause += "&& PolicyPremium <= ?";
            surveyArgs.Add(this.TotalPremiumMax);
        }

        // agency fields
        if (this.AgencyNumber != null)
        {
            surveyClause += "&& AgencyNumber = ?";
            surveyArgs.Add(this.AgencyNumber);
        }
        if (this.AgencyName != null)
        {
            surveyClause += "&& AgencyName LIKE ?";
            surveyArgs.Add("%" + this.AgencyName + "%");
        }
        if (this.AgencyStateCode != null)
        {
            surveyClause += "&& AgencyStateCode = ?";
            surveyArgs.Add(this.AgencyStateCode);
        }

        // strip leading '&&' from the front of the clauses
        if (surveyClause.Length > 3) surveyClause = surveyClause.Remove(0, 3);

        // add survey clause
        if (surveyClause.Length > 0)
        {
            surveyClause += " && CreateStateCode != ?";
            surveyArgs.Add(Berkley.BLC.Entities.CreateState.InProgress.Code);

            if (companyID != Berkley.BLC.Entities.Company.WRBerkleyCorporation.ID)
            {
                surveyClause += " && CompanyID = ?";
                surveyArgs.Add(companyID);
            }
        }

        // return the parameters and query
        parameters = surveyArgs.ToArray();
        return surveyClause;
    }

    private static int TryParseInt(string value, int errorValue)
    {
        try
        {
            return int.Parse(value);
        }
        catch
        {
            return errorValue;
        }
    }

    private static decimal TryParseDecimal(string value, decimal errorValue)
    {
        try
        {
            return decimal.Parse(value);
        }
        catch
        {
            return errorValue;
        }
    }

    private static DateTime TryParseDate(string value, DateTime errorValue)
    {
        try
        {
            return DateTime.Parse(value);
        }
        catch
        {
            return errorValue;
        }
    }

    private static Guid TryParseGuid(string value, Guid errorValue)
    {
        try
        {
            return new Guid(value);
        }
        catch
        {
            return errorValue;
        }
    }

    private static bool TryParseBool(string value, bool errorValue)
    {
        try
        {
            return bool.Parse(value);
        }
        catch
        {
            return errorValue;
        }
    }
}
