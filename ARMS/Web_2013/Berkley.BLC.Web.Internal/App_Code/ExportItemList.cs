using System;
using System.Web;
using System.Web.UI;
using System.Collections;
using System.Reflection;
using System.Text;
using System.Threading;

using Berkley.BLC.Entities;

/// <summary>
/// Summary description for ExportData.
/// </summary>
public class ExportItemList
{
    private ArrayList moList = new ArrayList();
    private ExportItem[] roCache;
    private bool mfDirty = true;
    private Page moPage;

    public ExportItemList(Page oPage)
    {
        moPage = oPage;
    }

    public void Add(ExportItem oExportItem)
    {
        moList.Add(oExportItem);
        mfDirty = true;
    }

    public void Sort()
    {
        moList.Sort();
    }

    public ExportItem[] ExportItems
    {
        get
        {
            if (mfDirty || roCache == null)
            {
                roCache = (ExportItem[])moList.ToArray(typeof(ExportItem));
                mfDirty = false;
            }
            return roCache;
        }
    }

    private bool GetProperty(object oRootObject, string strClassPropertyName, ref ExportItem oExportItem)
    {
        PropertyInfo oProperty;
        int iCount = 0;
        System.Type oType = oRootObject.GetType();
        string[] rstrMethods = strClassPropertyName.Split('.');

        oExportItem.PropertyInfo = null;

        foreach (string strMethod in rstrMethods)
        {
            iCount++;
            oProperty = oType.GetProperty(strMethod);
            if (oProperty != null)
            {
                object oValue = oProperty.GetValue(oRootObject, null);
                if (oValue != null)
                {
                    if (iCount == rstrMethods.Length)
                    {
                        oExportItem.PropertyInfo = oProperty;
                        oExportItem.ReferencedObject = oRootObject;
                        return true;
                    }
                    else
                    {
                        oRootObject = oProperty.GetValue(oRootObject, null);
                        oType = oRootObject.GetType();
                    }
                }
            }
        }
        return false;
    }

    public void BuildReflectionProperties(Object oObject)
    {
        ExportItem[] roExportItems = ExportItems;

        // Build up the property info
        for (int iExportItem = 0; iExportItem < roExportItems.Length; iExportItem++)
        {
            // Try the filter name first - this will hold the Survey Property Name
            if (!GetProperty(oObject, roExportItems[iExportItem].FilterName, ref roExportItems[iExportItem]))
            {
                GetProperty(oObject, roExportItems[iExportItem].SortExpression, ref roExportItems[iExportItem]);
            }
        }
    }

    public string GetCsv(object[] objects, ExportItemList oExportList, string strErrorMessage)
    {
        StringBuilder oBuilder = new StringBuilder();
        PropertyInfo oProperty;
        ExportItem oExportItem;
        object oValue;
        DateTime oDateTime;
        int iCount = 0;

        // Write out the Header
        foreach (ExportItem oItem in oExportList.ExportItems)
        {
            // Use quotes around all cell data so that the commas in the $ will not be considered a delimiter 
            oBuilder.Append("\"");
            oBuilder.Append(oItem.HeaderName.Replace("<br>", " ").Replace("&nbsp;", " "));
            oBuilder.Append("\"");
            oBuilder.Append(",");
        }
        oBuilder.Append("\r\n");
        if (objects.Length == 0)
        {
            oBuilder.Append("\"");
            oBuilder.Append(strErrorMessage);
            oBuilder.Append("\"");
        }
        else
        {
            // Write out the survey data
            foreach (Object oObject in objects)
            {
                iCount++;
                oExportList.BuildReflectionProperties(oObject);
                for (int iExportItem = 0; iExportItem < oExportList.ExportItems.Length; iExportItem++)
                {
                    oExportItem = oExportList.ExportItems[iExportItem];
                    oProperty = oExportItem.PropertyInfo;
                    if (oProperty != null)
                    {
                        oBuilder.Append("\"");

                        oValue = oProperty.GetValue(oExportItem.ReferencedObject, null);
                        if (oExportItem.IsReminder)
                        {
                            #region Reminder
                            try
                            {
                                oDateTime = Convert.ToDateTime(oValue);
                                if (oDateTime > DateTime.MinValue)
                                {
                                    if (oDateTime > DateTime.Today)
                                    {
                                        oBuilder.Append(oDateTime.ToShortDateString());
                                    }
                                    else
                                    {
                                        oBuilder.Append(" !! " + oDateTime.ToShortDateString());
                                    }
                                }
                                else
                                {
                                    oBuilder.Append("-");
                                }
                            }
                            catch (Exception)
                            {
                                oBuilder.Append("?");
                            }
                            #endregion
                        }
                        else
                        {
                            #region DateTime, String, Decimal
                            switch (oExportItem.DataType)
                            {
                                case "Decimal":
                                    Decimal oDecimal = Convert.ToDecimal(oValue);
                                    if (oDecimal != Decimal.MinValue)
                                    {
                                        oBuilder.Append(oDecimal.ToString("C"));
                                    }
                                    else
                                    {
                                        oBuilder.Append("-");
                                    }
                                    break;

                                case "Int32":
                                case "Int64":
                                    Decimal oNonCurrencyDecimal = Convert.ToDecimal(oValue);
                                    if (oNonCurrencyDecimal != Decimal.MinValue && oNonCurrencyDecimal != Int32.MinValue)
                                    {
                                        oBuilder.Append(oNonCurrencyDecimal.ToString("N2"));
                                    }
                                    else
                                    {
                                        oBuilder.Append("-");
                                    }
                                    break;

                                case "DateTime":
                                    oDateTime = Convert.ToDateTime(oValue);
                                    if (oDateTime != DateTime.MinValue)
                                    {
                                        oBuilder.Append(oDateTime.ToShortDateString());
                                    }
                                    else
                                    {
                                        oBuilder.Append("-");
                                    }
                                    break;

                                case "String":
                                default:
                                    if (oValue.ToString().Length > 0)
                                    {
                                        oBuilder.Append(oValue.ToString());
                                    }
                                    else
                                    {
                                        oBuilder.Append("-");
                                    }
                                    break;
                            }
                            #endregion
                        }
                        oBuilder.Append("\"");
                        oBuilder.Append(",");
                    }
                }
                oBuilder.Append("\r\n");
            }
        }
        return oBuilder.ToString();
    }

}
