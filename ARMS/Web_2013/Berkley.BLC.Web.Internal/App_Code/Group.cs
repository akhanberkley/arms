using System;
using System.Web;
using System.Web.UI;

namespace Berkley.BLC.Web.Internal.Controls
{
    /// <summary>
    /// Container control used to group other control into a region to better control visibility.
    /// </summary>
    public class Group : Control
    {
        /// <summary>
        /// Gets or sets the text value of the label to be shown.
        /// </summary>
        public string Label
        {
            get
            {
                object value = this.ViewState["Label"];
                return (value != null) ? (string)value : string.Empty;
            }
            set
            {
                this.ViewState["Label"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating if a HTML table row tag should be injected into the page by this control.
        /// </summary>
        public bool TableRow
        {
            get
            {
                object value = this.ViewState["TableRow"];
                return (value != null) ? (bool)value : true;
            }
            set
            {
                this.ViewState["TableRow"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating if the label should be shown.
        /// </summary>
        public bool ShowLabel
        {
            get
            {
                object value = this.ViewState["ShowLabel"];
                return (value != null) ? (bool)value : true;
            }
            set
            {
                this.ViewState["ShowLabel"] = value;
            }
        }


        /// <summary>
        /// Sends server control content to a provided HtmlTextWriter object, which writes the content to be rendered on the client.
        /// </summary>
        /// <param name="output">The HtmlTextWriter object that receives the server control content.</param>
        protected override void Render(HtmlTextWriter output)
        {
            if (this.ShowLabel)
            {
                if (this.TableRow)
                {
                    output.WriteFullBeginTag("tr");
                }

                output.WriteLine("<td class=\"label\">&nbsp;</td>");
                output.Write("<td><br><b>");
                HttpUtility.HtmlEncode(this.Label, output);
                output.Write("</b></td>");

                if (this.TableRow)
                {
                    output.WriteEndTag("tr");
                }
            }

            base.Render(output);
        }
    }
}
