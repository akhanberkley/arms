<%@ Page Language="C#" AutoEventWireup="false" CodeFile="ServicePlans.aspx.cs" Inherits="ServicePlansAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    <span class="heading"><%= base.PageHeading %></span><br><br>

    <uc:HintHeader ID="ucHintHeader" runat="server" />

    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>Status:</td>
                        <td><asp:dropdownlist id="cboStatus" runat="server" AutoPostBack="True" CssClass="cbo"></asp:dropdownlist></td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <table>
                    <tr>
                        <% if (!_hideManagedByDropDown) { %>
                        <td>Managed By:</td>
                        <td><asp:dropdownlist id="cboCreatedBy" runat="server" AutoPostBack="True" CssClass="cbo"></asp:dropdownlist></td>
                        <% } %>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:datagrid ID="grdItems" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="30" AllowSorting="True">
	                <headerstyle CssClass="header" VerticalAlign="Top" />
	                <itemstyle CssClass="item" />
	                <alternatingitemstyle CssClass="altItem" />
	                <footerstyle CssClass="footer" />
	                <pagerstyle CssClass="pager" Mode="NumericPages" />
                </asp:datagrid>
            </td>
        </tr>
        <tr>
            <td><a id="lnkCreatePlan" href="CreateServicePlanSearch.aspx" runat="server">Create New Service Plan</a></td>
            <td align="right"><asp:Button ID="btnExport" runat="Server" CssClass="btn" Text="Export to Excel" /></td>
        </tr>
    </table> 
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
