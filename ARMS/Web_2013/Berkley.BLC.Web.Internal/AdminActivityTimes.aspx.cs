using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using Wilson.ORMapper;

public partial class AdminActivityTimesAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminActivityTimesAspx_Load);
        this.PreRender += new EventHandler(AdminActivityTimesAspx_PreRender);
        this.grdActivities.ItemCommand += new DataGridCommandEventHandler(grdActivities_ItemCommand);
        this.grdActivities.SortCommand += new DataGridSortCommandEventHandler(grdActivities_SortCommand);
        this.grdActivities.PageIndexChanged += new DataGridPageChangedEventHandler(grdActivities_PageIndexChanged);
        this.cboYear.SelectedIndexChanged += new EventHandler(cboYear_SelectedIndexChanged);
        this.cboMonth.SelectedIndexChanged += new EventHandler(cboMonth_SelectedIndexChanged);
        this.cboUser.SelectedIndexChanged += new EventHandler(cboUser_SelectedIndexChanged);
        this.btnExport.Click += new EventHandler(btnExport_Click);
    }
    #endregion

    private static string[] monthNames = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
    protected string _displayAgencyVisitedCol;

    void AdminActivityTimesAspx_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // populate the year list
            for (int i = 2007; i <= DateTime.Today.Year; i++)
            {
                cboYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            cboYear.Items.Insert(0, new ListItem("(All)", ""));

            // populate the month list
            for (int m = 1; m <= 12; m++)
            {
                cboMonth.Items.Add(new ListItem(monthNames[m - 1], m.ToString()));
            }
            cboMonth.Items.Insert(0, new ListItem("(All)", ""));

            // populate the user list
            User[] users = Berkley.BLC.Entities.User.GetSortedArray("CompanyID = ? && IsFeeCompany = false && IsUnderwriter = false && AccountDisabled = false", "Name ASC", CurrentUser.CompanyID);
            ComboHelper.BindCombo(cboUser, users, "ID", "Name");
            cboUser.Items.Insert(0, new ListItem("(All)", ""));

            //Only management can see other users times
            rowUserFilter.Visible = CurrentUser.Role.PermissionSet.CanWorkReviews;

            // restore sort expression, reverse state, and page index settings
            this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", grdActivities.Columns[0].SortExpression);
            this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", false);
            grdActivities.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);
            cboUser.SelectedValue = (string)this.UserIdentity.GetPageSetting("SelectedUserID", CurrentUser.ID.ToString());
            cboYear.SelectedValue = (string)this.UserIdentity.GetPageSetting("SelectedYear", DateTime.Today.Year.ToString());
            cboMonth.SelectedValue = (string)this.UserIdentity.GetPageSetting("SelectedMonth", DateTime.Today.Month.ToString());
            cboMonth.SelectedValue = (cboYear.SelectedValue.Length > 0) ? cboMonth.SelectedValue : string.Empty;
            cboMonth.Enabled = (cboYear.SelectedValue.Length > 0);
        }

        base.PageHeading = string.Format("Administrative Times for {0}", CurrentUser.Name);
    }

    void AdminActivityTimesAspx_PreRender(object sender, EventArgs e)
    {
        string sortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sortExp = sortExp.Replace(" DESC", " TEMP");
            sortExp = sortExp.Replace(" ASC", " DESC");
            sortExp = sortExp.Replace(" TEMP", " ASC");
        }

        // get the activities for this user
        Activity[] activities = GetActivities();

        string noRecordsText;
        if (cboUser.SelectedValue.Length > 0)
        {
            User user = Berkley.BLC.Entities.User.GetOne("ID = ?", cboUser.SelectedValue);
            noRecordsText = string.Format("{0} has no administrative times for the selected time frame.", user.Name);
        }
        else
        {
            noRecordsText = "There are no administrative times for the selected year.";
        }

        // Hide Agency Visited Col if not applicable based upon company parameter
        _displayAgencyVisitedCol = (Berkley.BLC.Business.Utility.GetCompanyParameter("DispAgencyListOnActivityType", CurrentUser.Company).ToUpper() == "AGENCY VISIT") ? "true" : "false";
        if (_displayAgencyVisitedCol == "false")
        {
            grdActivities.Columns[5].Visible = false;
        }

        DataGridHelper.BindGrid(grdActivities, activities, "ID", noRecordsText);
    }

    void cboUser_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.UserIdentity.SavePageSetting("SelectedUserID", cboUser.SelectedValue);
    }

    void cboYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.UserIdentity.SavePageSetting("SelectedYear", cboYear.SelectedValue);
        cboMonth.SelectedValue = (cboYear.SelectedValue.Length > 0) ? cboMonth.SelectedValue : string.Empty;
        cboMonth.Enabled = (cboYear.SelectedValue.Length > 0);
    }

    void cboMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.UserIdentity.SavePageSetting("SelectedMonth", cboMonth.SelectedValue);
    }

    protected void btnRemoveActivity_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this activity?");
    }

    void grdActivities_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            // get the ID of the activity selected
            Guid id = (Guid)grdActivities.DataKeys[e.Item.ItemIndex];

            // remove the activity
            Activity eActivity = Activity.Get(id);
            if (eActivity != null)
            {
                //first remove any children
                ActivityDocument[] docs = ActivityDocument.GetArray("ActivityID = ?", eActivity.ID);
                foreach (ActivityDocument doc in docs)
                {
                    doc.Delete();
                }

                eActivity.Delete();
            }
        }
    }

    void grdActivities_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdActivities.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);
    }

    void grdActivities_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sortRev);
    }

    void btnExport_Click(object sender, EventArgs e)
    {
        Activity[] activities = GetActivities();

        //Get the columns for the CSV export
        ExportItemList oExportDataList = new ExportItemList(this);
        for (int i = 0; i < grdActivities.Columns.Count; i++)
        {
            DataGridColumn column = grdActivities.Columns[i];
            string filterName = column.SortExpression.Replace("ASC", "").Replace("DESC", "").Trim();

            if (column.Visible && !string.IsNullOrEmpty(column.HeaderText))
            {
                string dataType;
                switch (i)
                {
                    case 0:
                        dataType = "DateTime";
                        break;
                    case 1:
                        dataType = "Int32";
                        break;
                    case 2:
                        dataType = "Int32";
                        break;
                    case 3:
                        dataType = "Int32";
                        break;
                    case 4:
                        dataType = "String";
                        break;
                    case 5:
                        dataType = "String";
                        break;
                    case 6:
                        dataType = "String";
                        break;
                    case 7:
                        dataType = "String";
                        break;
                    default:
                        throw new NotSupportedException("Not expecting this many columns in the grid.");
                }

                oExportDataList.Add(new ExportItem(Guid.NewGuid(), column.SortExpression, filterName, dataType, column.HeaderText, i));
            }
        }

        // Get the CSV data
        string strCsv = oExportDataList.GetCsv(activities, oExportDataList, "No Activity Times Found.");

        Response.ContentType = "application/x-msexcel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=ARMS_AdministrativeTimeExport.csv");

        // Write out the CSV data
        System.Text.ASCIIEncoding oEncoding = new System.Text.ASCIIEncoding(); ;
        byte[] rbBytes = oEncoding.GetBytes(strCsv);
        Response.OutputStream.Write(rbBytes, 0, rbBytes.Length);

        Response.End();
    }

    private Activity[] GetActivities()
    {
        // get the activities for this user
        StringBuilder filter = new StringBuilder();
        ArrayList args = new ArrayList();

        //Set the date filter
        if (cboYear.SelectedValue.Length > 0)
        {
            DateTime selectedDate;
            filter.Append("Date >= ? && Date < ?");

            if (cboMonth.SelectedValue.Length > 0)
            {
                selectedDate = new DateTime(Convert.ToInt32(cboYear.SelectedValue), Convert.ToInt32(cboMonth.SelectedValue), 1);
                args.Add(selectedDate);
                args.Add(selectedDate.AddMonths(1));
            }
            else
            {
                selectedDate = new DateTime(Convert.ToInt32(cboYear.SelectedValue), 1, 1);
                args.Add(selectedDate);
                args.Add(selectedDate.AddYears(1));
            }
        }
        else
        {
            filter.Append("!ISNULL(Date)");
        }

        //Set the user filter
        if (cboUser.SelectedValue.Length > 0)
        {
            filter.Append(" && AllocatedTo = ?");
            args.Add(cboUser.SelectedValue);
        }
        else
        {
            filter.Append(" && User.CompanyID = ?");
            args.Add(CurrentUser.CompanyID);
        }

        return Activity.GetSortedArray(filter.ToString(), this.ViewState["SortExpression"].ToString(), args.ToArray());
    }
}
