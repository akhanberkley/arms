﻿<%@ Page Language="C#" AutoEventWireup="false" ValidateRequest="false" CodeFile="ContactManager.aspx.cs" Inherits="ContactManagerAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register TagPrefix="cf" Namespace="Berkley.BLC.Web.Internal.Fields" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span>
    <% if (_plan != null) { %>
        <a class="nav" href="ServicePlanDetails.aspx<%= ARMSUtility.GetQueryStringForNav("sortby", "sortreversed") %>"><< Back to Service Plan Details</a><br><br>
    <% } else { %>
        <a class="nav" href="Insured.aspx<%= ARMSUtility.GetQueryStringForNav("sortby", "sortreversed") %>"><< Back to Insured Details</a><br><br>
    <% } %>

    <uc:HintHeader ID="ucHintHeader" runat="server" />

    <cc:Box id="boxActionType" runat="server" Title="Step #1:&nbsp;&nbsp;Select Action" width="625">
        <table>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:RadioButton ID="rdoUpdate" Text="<b>Update</b> Existing Contact(s)" runat="server" GroupName="Action" AutoPostBack="true" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RadioButton ID="rdoDelete" Text="<b>Delete</b> Existing Contact(s)" runat="server" GroupName="Action" AutoPostBack="true" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RadioButton ID="rdoInsert" Text="<b>Create</b> New or <b>Add</b> Existing Contact" runat="server" GroupName="Action" AutoPostBack="true" />
                </td>
            </tr>
        </table>
    </cc:Box>

    <cc:Box id="boxContacts" runat="server" Title="">
        <table>
            <tr>
                <td>
                    <span>Filter By:</span>
                    <asp:dropdownlist id="cboSurveyStatus" runat="server" AutoPostBack="True" CssClass="cbo"></asp:dropdownlist>
                </td>
                <td align="right">
                    <asp:LinkButton ID="btnExport" runat="server">Export Contacts to Excel</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:datagrid ID="grdContacts" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="True">
	                    <headerstyle CssClass="header" />
	                    <itemstyle CssClass="item" />
	                    <alternatingitemstyle CssClass="altItem" />
	                    <footerstyle CssClass="footer" />
	                    <columns>
	                        <asp:BoundColumn HeaderText="Rank" SortExpression="ContactPriorityIndex" DataField="ContactPriorityIndex" />
	                        <asp:BoundColumn HeaderText="Name" SortExpression="ContactName" DataField="ContactName" />
		                    <asp:BoundColumn HeaderText="Title" SortExpression="ContactTitle" DataField="ContactTitle" />
                            <asp:BoundColumn HeaderText="Phone" SortExpression="ContactPhone" DataField="ContactPhone" />
                            <asp:BoundColumn HeaderText="Email" SortExpression="ContactEmailAddress" DataField="ContactEmailAddress" />
                            <asp:BoundColumn HeaderText="Mail Options" SortExpression="ContactReasonType" DataField="ContactReasonType" />
                            <asp:BoundColumn HeaderText="Surveys" SortExpression="SurveyListEncoded" DataField="SurveyListEncoded" />
                            <asp:BoundColumn HeaderText="Location #" SortExpression="LocationNumber" DataField="LocationNumber" />
                            <asp:BoundColumn HeaderText="Location Address" SortExpression="SurveyLocation" DataField="SurveyLocation" />
                            <asp:TemplateColumn>
	                                <ItemTemplate>
		                                <asp:CheckBox ID="chkContact" runat="server" CssClass="chkGrid" />
	                                </ItemTemplate>
                            </asp:TemplateColumn>
	                    </columns>
                    </asp:datagrid>
                </td>
            </tr>
        </table>
    </cc:Box>

    <script type="text/javascript">
        $(document).ready(function () {
            $("[class=chkGrid]").live("click", function () {
                $(this).closest("tr").toggleClass('selectedItem');
            });
        });
    </script>

    <cc:Box id="boxContactToCopy" runat="server" Title="Optional Step #3:&nbsp;&nbsp;Select Existing Contact to Copy From" width="600">
        <table class="fields">
            <uc:Combo id="cboContacts" runat="server" Name="Existing Contacts" Field="LocationContact.ID" OnChange="PopulateContactFields(this.value);" IsRequired="false" topItemText="" />
        </table>
    </cc:Box>

    <cc:Box id="boxContact" runat="server" Title="Step #4:&nbsp;&nbsp;Enter Information to Update the Selected Contacts Above" width="600">
        <table class="fields">
            <uc:Check ID="chkMakePrimary" Field="Location.IsActive" runat="server" Name="Make Primary" />
            <uc:Text id="txtName" runat="server" field="LocationContact.Name" Columns="50" />
            <uc:Text id="txtTitle" runat="server" field="LocationContact.Title" Columns="60" />
            <uc:Text id="txtPhoneNumber" runat="server" field="LocationContact.PhoneNumber" IsRequired="true" Directions="(ex. 5551234567)" Name="Phone" /> 
            <uc:Text id="txtAltPhoneNumber" runat="server" field="LocationContact.AlternatePhoneNumber" Directions="(ex. 5551234567)" Name="Alt Phone" />
            <uc:Text id="txtFaxNumber" runat="server" field="LocationContact.FaxNumber" Directions="(ex. 5551234567)" />
            <uc:Text id="txtEmailAddress" runat="server" field="LocationContact.EmailAddress" Name="Email" Columns="70" />
            <uc:Combo id="cboContactOption" runat="server" Name="Mail Option" Field="LocationContact.ContactReasonTypeCode" IsRequired="true" topItemText="" />
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr><td align="right"><asp:CheckBox ID="chkSameAsCorporateAddress" runat="server" /></td><td> Same as Corporate Mailing Address</td></tr>
            <uc:Text id="txtCompanyName" runat="server" field="LocationContact.CompanyName" Name="Company" Columns="70" />
            <uc:Text id="txtStreetLine1" runat="server" field="LocationContact.StreetLine1" Columns="80" />
            <uc:Text id="txtStreetLine2" runat="server" field="LocationContact.StreetLine2" Columns="80" />
            <uc:Text id="txtCity" runat="server" field="LocationContact.City" />
            <uc:Combo id="cboState" runat="server" field="LocationContact.StateCode" topItemText="" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" />
            <uc:Text id="txtZipCode" runat="server" field="LocationContact.ZipCode" />
        </table>
    </cc:Box>

    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" CausesValidation="True" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />

    <uc:HintFooter ID="ucHintFooter" runat="server" VerticalOffset="130" />
    <uc:Footer ID="ucFooter" runat="server" />

    <script language="javascript">
        function SameAsCorporateAddress(isChecked, insuredID) {
            var arrFields = AjaxMethods.SameAsCorporateAddress(isChecked, insuredID).value;

            document.getElementById("txtCompanyName_txt").value = arrFields[0];
            document.getElementById("txtStreetLine1_txt").value = arrFields[1];
            document.getElementById("txtStreetLine2_txt").value = arrFields[2];
            document.getElementById("txtCity_txt").value = arrFields[3];
            document.getElementById("cboState_cbo").value = arrFields[4];
            document.getElementById("txtZipCode_txt").value = arrFields[5];
        }

        function PopulateContactFields(contactID) {
            var arrFields = AjaxMethods.PopulateContactFields(contactID).value;

            document.getElementById("txtName_txt").value = arrFields[0];
            document.getElementById("txtTitle_txt").value = arrFields[1];
            document.getElementById("txtPhoneNumber_txt").value = arrFields[2];
            document.getElementById("txtAltPhoneNumber_txt").value = arrFields[3];
            document.getElementById("txtFaxNumber_txt").value = arrFields[4];
            document.getElementById("txtEmailAddress_txt").value = arrFields[5];
            document.getElementById("cboContactOption_cbo").value = arrFields[6];
            
            document.getElementById("txtCompanyName_txt").value = arrFields[7];
            document.getElementById("txtStreetLine1_txt").value = arrFields[8];
            document.getElementById("txtStreetLine2_txt").value = arrFields[9];
            document.getElementById("txtCity_txt").value = arrFields[10];
            document.getElementById("cboState_cbo").value = arrFields[11];
            document.getElementById("txtZipCode_txt").value = arrFields[12];
        }
    </script>
    
    </form>
</body>
</html>
