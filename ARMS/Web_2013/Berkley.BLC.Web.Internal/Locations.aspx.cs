using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using Wilson.ORMapper;

public partial class LocationsAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(LocationsAspx_Load);
        this.PreRender += new EventHandler(LocationsAspx_PreRender);
        this.grdAttachedLocations.ItemCommand += new DataGridCommandEventHandler(grdAttachedLocations_ItemCommand);
        this.grdAttachedLocations.ItemDataBound += new DataGridItemEventHandler(grdAttachedLocations_ItemDataBound);
        this.grdNonAttachedLocations.ItemCommand += new DataGridCommandEventHandler(grdNonAttachedLocations_ItemCommand);
        this.grdLocationsForSeperation.ItemDataBound += new DataGridItemEventHandler(grdLocationsForSeperation_ItemDataBound);
        this.lnkAddLocation.Click += new EventHandler(lnkAddLocation_Click);
        this.btnSeperate.Click += new EventHandler(btnSeperate_Click);
    }
    #endregion

    protected Survey _eSurvey;
    protected Location[] _eLocations;

    void LocationsAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        _eSurvey = Survey.Get(id);

        //Query the locations
        _eLocations = Location.GetSortedArray("SurveyPolicyCoverages[SurveyID = ?]", "Number ASC", _eSurvey.ID);

        this.PageHeading = string.Format("Locations: {0}", _eSurvey.Insured.Name);
        this.boxAttachedLocations.Title = string.Format("Locations for Survey #{0}", _eSurvey.Number);
    }

    void LocationsAspx_PreRender(object sender, EventArgs e)
    {
        DataGridHelper.BindGrid(grdAttachedLocations, _eLocations, "ID", "There are no locations being surveyed.");
        DataGridHelper.BindGrid(grdLocationsForSeperation, _eLocations, "ID", "There are no locations being surveyed.");

        //Filter out the surveyed locations
        ArrayList args = new ArrayList();
        args.Add(_eSurvey.InsuredID);

        StringBuilder sb = new StringBuilder();
        sb.Append("InsuredID = ? &&");

        foreach (Location location in _eLocations)
        {
            sb.Append(" ID != ? &&");
            args.Add(location.ID);
        }

        //Trim any trailing "&&"
        sb.Remove(sb.Length - 2, 2);

        //Query the locations that aren't attached to the survey
        Location[] notAttachedLocations = Location.GetSortedArray(sb.ToString(), "Number ASC", args.ToArray());
        DataGridHelper.BindGrid(grdNonAttachedLocations, notAttachedLocations, "ID", "All locations are being surveyed.");
    }

    void grdAttachedLocations_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the control
            Berkley.BLC.Web.Internal.Controls.RadioButtonForRepeater rdoPrimary = (Berkley.BLC.Web.Internal.Controls.RadioButtonForRepeater)e.Item.FindControl("rdoPrimary");

            Location location = (Location)e.Item.DataItem;
            rdoPrimary.Checked = (location.ID == _eSurvey.LocationID);
        }
    }

    void grdAttachedLocations_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            // get the ID of the location selected
            Guid id = (Guid)grdAttachedLocations.DataKeys[e.Item.ItemIndex];
            Location eLocation = Location.Get(id);
            
            if (e.CommandName.ToUpper() == "REMOVE")
            {

                // remove the location
                if (eLocation != null)
                {
                    //Check if any other locations exists
                    if (_eLocations.Length <= 1)
                    {
                        JavaScript.ShowMessage(this, "At least one location must remain for the survey.");
                        return;
                    }

                    //Check if this is the primary location of the survey
                    if (_eSurvey.LocationID == eLocation.ID)
                    {
                        Survey updateSurvey = _eSurvey.GetWritableInstance();

                        //determine which location to make primary
                        if (_eSurvey.LocationID == _eLocations[0].ID)
                        {
                            updateSurvey.LocationID = _eLocations[1].ID;
                        }
                        else
                        {
                            updateSurvey.LocationID = _eLocations[0].ID;
                        }

                        updateSurvey.Save(trans);
                    }

                    //Remove any policy/coverages tied to this location
                    SurveyPolicyCoverage[] eCoverages = SurveyPolicyCoverage.GetArray("SurveyID = ? && LocationID = ?", _eSurvey.ID, eLocation.ID);
                    foreach (SurveyPolicyCoverage eCoverage in eCoverages)
                    {
                        eCoverage.Delete(trans);
                    }

                    ////Finally, kill the location
                    //eLocation.Delete(trans);

                    trans.Commit();
                }
            }
            else if (e.CommandName.ToUpper() == "MAKEPRIMARY")
            {
                //update this location as the primary for the survey
                _eSurvey = (_eSurvey.IsReadOnly) ? _eSurvey.GetWritableInstance() : _eSurvey;
                _eSurvey.LocationID = eLocation.ID;
                _eSurvey.Save(trans);
                trans.Commit();
            }
        }

        //Refresh grid
        Response.Redirect(string.Format("Locations.aspx{0}", ARMSUtility.GetQueryStringForNav("locationid")), true);
    }

    void grdNonAttachedLocations_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "ADD")
        {
            // get the ID of the location selected
            Guid id = (Guid)grdNonAttachedLocations.DataKeys[e.Item.ItemIndex];

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                // remove the location
                Location eLocation = Location.Get(id);
                if (eLocation != null)
                {
                    //Add the location and policy/coverages using the primary location
                    SurveyPolicyCoverage[] eCoverages = SurveyPolicyCoverage.GetArray("SurveyID = ? && LocationID = ?", _eSurvey.ID, _eSurvey.LocationID);
                    foreach (SurveyPolicyCoverage existingCoverage in eCoverages)
                    {
                        SurveyPolicyCoverage newLocationCoverage = new SurveyPolicyCoverage(_eSurvey.ID, eLocation.ID, existingCoverage.PolicyID, existingCoverage.CoverageNameID);
                        newLocationCoverage.ReportTypeCode = existingCoverage.ReportTypeCode;
                        newLocationCoverage.Active = true;

                        newLocationCoverage.Save(trans);
                    }

                    trans.Commit();
                }
            }
        }

        //Refresh grid
        Response.Redirect(string.Format("Locations.aspx{0}", ARMSUtility.GetQueryStringForNav("locationid")), true);
    }

    protected string GetBuildingCount(object dataItem)
    {
        Location eLocation = (dataItem as Location);
        return Building.GetArray("LocationID = ?", eLocation.ID).Length.ToString();
    }

    void grdLocationsForSeperation_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Dropdown
            DropDownList cbo = (DropDownList)e.Item.FindControl("cboSurveyNumber");
            
            //Add the location id to the control
            Location location = (Location)e.Item.DataItem;
            cbo.Attributes.Add("LocationID", location.ID.ToString());
            
            //Add the number of surveys as there are locations
            for (int i = 1; i < _eLocations.Length+1; i++)
            {
                cbo.Items.Add(new ListItem(string.Format("Survey #{0}", i), i.ToString()));
            }
        }
    }

    void lnkAddLocation_Click(object sender, EventArgs e)
    {
        Location eLocation = new Location(Guid.NewGuid());

        //commit changes
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            //Create a new location and save to the db
            eLocation.InsuredID = _eSurvey.InsuredID;

            //Query the db to determine the appropriate default location number
            Location[] eLocations = Location.GetSortedArray("InsuredID = ?", "Number DESC", _eSurvey.InsuredID);
            eLocation.Number = (eLocations.Length > 0) ? eLocations[0].Number + 1 : 1;

            //By default, try and add a primary contact from a previous location
            LocationContact eContact = LocationContact.GetOne("PriorityIndex = ? && Location.InsuredID = ?", 1, _eSurvey.InsuredID);
            if (eContact != null)
            {
                LocationContact newContact = new LocationContact(Guid.NewGuid());
                newContact.LocationID = eLocation.ID;
                newContact.Name = eContact.Name;
                newContact.Title = eContact.Title;
                newContact.PhoneNumber = eContact.PhoneNumber;
                newContact.AlternatePhoneNumber = eContact.AlternatePhoneNumber;
                newContact.FaxNumber = eContact.FaxNumber;
                newContact.EmailAddress = eContact.EmailAddress;
                newContact.StreetLine1 = eContact.StreetLine1;
                newContact.StreetLine2 = eContact.StreetLine2;
                newContact.City = eContact.City;
                newContact.StateCode = eContact.StateCode;
                newContact.ZipCode = eContact.ZipCode;
                newContact.PriorityIndex = eContact.PriorityIndex;

                newContact.Save(trans);
            }

            //Save location
            eLocation.Save(trans);

            //Add all the policy/coverages from the primary location
            SurveyPolicyCoverage[] eCoverages = SurveyPolicyCoverage.GetArray("SurveyID = ? && LocationID = ?", _eSurvey.ID, _eSurvey.LocationID);
            foreach (SurveyPolicyCoverage existingCoverage in eCoverages)
            {
                SurveyPolicyCoverage newLocationCoverage = new SurveyPolicyCoverage(_eSurvey.ID, eLocation.ID, existingCoverage.PolicyID, existingCoverage.CoverageNameID);
                newLocationCoverage.ReportTypeCode = existingCoverage.ReportTypeCode;
                newLocationCoverage.Active = true;

                newLocationCoverage.Save(trans);
            }

            // commit changes
            trans.Commit();
        }

        Response.Redirect(string.Format("Insured.aspx{0}&locationid={1}&locations=1", ARMSUtility.GetQueryStringForNav("locationid"), eLocation.ID), true);
    }

    void btnSeperate_Click(object sender, EventArgs e)
    {
        Hashtable list = new Hashtable();
        
        foreach (DataGridItem item in grdLocationsForSeperation.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                //Dropdown
                DropDownList cbo = (DropDownList)item.FindControl("cboSurveyNumber");
                list.Add(cbo.Attributes["LocationID"], cbo.SelectedValue);
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < list.Count+1; i++)
        {
            foreach (Location location in _eLocations)
            {
                if (Convert.ToInt32(list[location.ID.ToString()]) == i)
                {
                    sb.Append(location.ID);
                    sb.Append(",");
                }
            }

            if (sb.Length > 1)
            {
                sb.Remove(sb.Length - 1, 1);
                sb.Append("|");
            }
        }

        //Remove any trailing pipes
        sb.Remove(sb.Length - 1, 1);

        //Check to see if we need to seprate
        if (sb.ToString().Split('|').Length <= 1)
        {
            JavaScript.ShowMessage(this, "There are no locations to separate.");
            return;
        }

        //get the existing docs and notes first
        SurveyDocument[] documents = SurveyDocument.GetArray("SurveyID = ?", _eSurvey.ID);
        SurveyNote[] notes = SurveyNote.GetArray("SurveyID = ?", _eSurvey.ID);

        //loop thru in order to create new surveys with the matching locations
        foreach (string strGroup in sb.ToString().Split('|'))
        {
            //Check if this group contains the "main" location of the survey
            bool isMainGroup = (strGroup.Contains(_eSurvey.LocationID.ToString()));
            
            if (!isMainGroup)
            {
                int counter = 0;
                using (Transaction trans = DB.Engine.BeginTransaction())
                {
                    Survey newSurvey = null;
                    List<SurveyPolicyCoverage> newLocationCoverages = new List<SurveyPolicyCoverage>();
                    List<SurveyPolicyCoverage> deleteCoverages = new List<SurveyPolicyCoverage>();
                    foreach (string strLocationID in strGroup.Split(','))
                    {
                        Location location = Location.Get(new Guid(strLocationID));

                        //Create new survey
                        if (counter <= 0)
                        {
                            newSurvey = new Survey(Guid.NewGuid());
                            newSurvey.CompanyID = _eSurvey.CompanyID;
                            newSurvey.InsuredID = _eSurvey.InsuredID;
                            newSurvey.PrimaryPolicyID = _eSurvey.PrimaryPolicyID;
                            newSurvey.LocationID = location.ID;
                            newSurvey.PreviousSurveyID = _eSurvey.PreviousSurveyID;
                            newSurvey.TypeID = _eSurvey.TypeID;
                            newSurvey.AssignedUserID = Guid.Empty;
                            newSurvey.RecommendedUserID = _eSurvey.RecommendedUserID;
                            newSurvey.ServiceTypeCode = _eSurvey.ServiceTypeCode;
                            newSurvey.StatusCode = SurveyStatus.OnHoldSurvey.Code;
                            newSurvey.CreateStateCode = CreateState.InProgress.Code;
                            newSurvey.CreateByInternalUserID = _eSurvey.CreateByInternalUserID;
                            newSurvey.CreateByExternalUserName = _eSurvey.CreateByExternalUserName;
                            newSurvey.CreateDate = DateTime.Now;
                            newSurvey.DueDate = _eSurvey.DueDate;
                            newSurvey.WorkflowSubmissionNumber = _eSurvey.WorkflowSubmissionNumber;
                            newSurvey.UnderwriterCode = _eSurvey.UnderwriterCode;
                            newSurvey.Priority = _eSurvey.Priority;
                            newSurvey.Save(trans);

                            //Add history entries
                            SurveyHistory history1 = new SurveyHistory(Guid.NewGuid());
                            history1.SurveyID = newSurvey.ID;
                            history1.EntryDate = DateTime.Now;
                            history1.EntryText = string.Format("Survey created in ARMS by {0} from survey #{1} by seperating locations.", CurrentUser.Name, _eSurvey.Number);
                            history1.Save(trans);

                            SurveyHistory history2 = new SurveyHistory(Guid.NewGuid());
                            history2.SurveyID = history1.SurveyID;
                            history2.EntryDate = history1.EntryDate.AddSeconds(1);
                            history2.EntryText = "Survey put on hold.";
                            history2.Save(trans);
                        }

                        //Move the policy/coverages to the new survey
                        SurveyPolicyCoverage[] coverages = SurveyPolicyCoverage.GetArray("SurveyID = ? && LocationID = ?", _eSurvey.ID, location.ID);
                        foreach (SurveyPolicyCoverage coverage in coverages)
                        {
                            SurveyPolicyCoverage newLocationCoverage = new SurveyPolicyCoverage(newSurvey.ID, location.ID, coverage.PolicyID, coverage.CoverageNameID);
                            newLocationCoverage.ReportTypeCode = coverage.ReportTypeCode;
                            newLocationCoverage.Active = coverage.Active;
                            
                            newLocationCoverages.Add(newLocationCoverage);
                            deleteCoverages.Add(coverage);
                            //newLocationCoverage.Save(trans);
                            //coverage.Delete(trans);
                        }

                        counter++;
                    }

                    foreach (SurveyPolicyCoverage newLocationCoverage in newLocationCoverages)
                    {
                        newLocationCoverage.Save(trans);
                    }

                    foreach (SurveyPolicyCoverage deleteCoverage in deleteCoverages)
                    {
                        deleteCoverage.Delete(trans);
                    }

                    //copy over the documents to the new survey
                    foreach (SurveyDocument tempDoc in documents)
                    {
                        // build and save the document
                        SurveyDocument doc = new SurveyDocument(Guid.NewGuid());
                        doc.SurveyID = newSurvey.ID;
                        doc.DocumentName = tempDoc.DocumentName;
                        doc.MimeType = tempDoc.MimeType;
                        doc.SizeInBytes = tempDoc.SizeInBytes;
                        doc.FileNetReference = tempDoc.FileNetReference;
                        doc.UploadedOn = tempDoc.UploadedOn;
                        doc.UploadUserID = tempDoc.UploadUserID;
                        doc.FileNetRemovable = tempDoc.FileNetRemovable;
                        doc.IsRemoved = false;
                        doc.Save(trans);
                    }

                    //copy over the comments to the new survey
                    foreach (SurveyNote tempNote in notes)
                    {
                        // build and save the comment
                        SurveyNote note = new SurveyNote(Guid.NewGuid());
                        note.SurveyID = newSurvey.ID;
                        note.UserID = tempNote.UserID;
                        note.EntryDate = tempNote.EntryDate;
                        note.InternalOnly = tempNote.InternalOnly;
                        note.Comment = tempNote.Comment;
                        note.Save(trans);
                    }

                    // try and create a BPM instance with the new survey(s)
                    SurveyManager manager = SurveyManager.GetInstance(newSurvey, CurrentUser);
                    newSurvey = manager.CreateSurvey();
                    newSurvey.Save(trans);

                    trans.Commit();
                }
            }
        }

        //Refresh the screen
        JavaScript.ShowMessageAndRedirect(this, "Your new survey(s) will be in the On Hold queue.", string.Format("Locations.aspx{0}", ARMSUtility.GetQueryStringForNav("locationid")));
    }
}
