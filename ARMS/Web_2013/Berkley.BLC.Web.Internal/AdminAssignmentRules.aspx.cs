using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web;

public partial class AdminAssignmentRulesAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminAssignmentRulesAspx_Load);
        this.PreRender += new EventHandler(AdminAssignmentRulesAspx_PreRender);
        this.grdRules.ItemCommand += new DataGridCommandEventHandler(grdRules_ItemCommand);
        this.cboFilter.SelectedIndexChanged += new EventHandler(cboFilter_SelectedIndexChanged);
    }
    #endregion

    void AdminAssignmentRulesAspx_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // populate the filter list
            cboFilter.Items.Add(new ListItem("Survey Assignment Rules", SurveyStatusType.Survey.Code));
            cboFilter.Items.Add(new ListItem("Review Assignment Rules", SurveyStatusType.Review.Code));

            string typeCode = ARMSUtility.GetStringFromQueryString("type", false);
            if (typeCode != null && typeCode.Length > 0)
            {
                ComboHelper.SelectComboItem(cboFilter, typeCode);
            }
            else // no value on query string
            {
                string filter = (string)this.UserIdentity.GetPageSetting("Filter", null);
                if (filter != null)
                {
                    ComboHelper.SelectComboItem(cboFilter, filter);
                    cboFilter_SelectedIndexChanged(null, null);
                }
            }
        }
    }

    void AdminAssignmentRulesAspx_PreRender(object sender, EventArgs e)
    {
        AssignmentRule[] rules = AssignmentRule.GetSortedArray("CompanyID = ? && SurveyStatusTypeCode = ?", "PriorityIndex ASC", this.UserIdentity.CompanyID, cboFilter.SelectedValue);
        DataGridHelper.BindGrid(grdRules, rules, "ID", "There are no assignment rules defined.");
    }

    protected void btnDelete_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to delete this assignment rule?");
    }

    void cboFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.UserIdentity.SavePageSetting("Filter", cboFilter.SelectedValue);
    }

    protected string GetRuleExpressionHtml(object dataItem)
    {
        AssignmentRule rule = (AssignmentRule)dataItem;

        FilterCondition[] conditions = FilterCondition.GetSortedArray("FilterID = ?", "OrderIndex ASC", rule.FilterID);

        StringBuilder sb = new StringBuilder(500);
        sb.Append("If ");
        for (int i = 0; i < conditions.Length; i++)
        {
            FilterCondition condition = conditions[i];
            if (i > 0) sb.Append("and ");
            sb.AppendFormat("<font color=\"#993300\">{0}</font> ", condition.FilterField.Name);
            sb.Append(condition.FilterOperator.DisplayName.ToLower());
            sb.AppendFormat(" <font color=\"#008000\">{0}</font>", Server.HtmlEncode(condition.DisplayValue));
            sb.Append("<br>");
        }
        sb.Append("then the survey ");
        sb.AppendFormat("<font color=\"#0000A0\">{0}</font>.", rule.Action.DisplayName);

        return sb.ToString();
    }

    void grdRules_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid ruleID = (Guid)grdRules.DataKeys[e.Item.ItemIndex];

        // get the current rules from the DB and find the index of the target rule
        AssignmentRule[] rules = AssignmentRule.GetSortedArray("CompanyID = ? && SurveyStatusTypeCode = ?", "PriorityIndex ASC", this.UserIdentity.CompanyID, cboFilter.SelectedValue);
        int targetIndex = int.MinValue;
        for (int i = 0; i < rules.Length; i++)
        {
            if (rules[i].ID == ruleID)
            {
                targetIndex = i;
                break;
            }
        }
        if (targetIndex == int.MinValue) // not found
        {
            return;
        }

        AssignmentRule targetRule = rules[targetIndex];

        // execute the requested transformation on the rules
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            ArrayList ruleList = new ArrayList(rules);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEUP":
                    {
                        ruleList.RemoveAt(targetIndex);
                        if (targetIndex > 0)
                        {
                            ruleList.Insert(targetIndex - 1, targetRule);
                        }
                        else // top item (move to bottom)
                        {
                            ruleList.Add(targetRule);
                        }
                        break;
                    }
                case "MOVEDOWN":
                    {
                        ruleList.RemoveAt(targetIndex);
                        if (targetIndex < rules.Length - 1)
                        {
                            ruleList.Insert(targetIndex + 1, targetRule);
                        }
                        else // bottom item (move to top)
                        {
                            ruleList.Insert(0, targetRule);
                        }
                        break;
                    }
                case "DELETE":
                    {
                        // note: killing the filter kills the rule since the filter is the parent (cascade delete is enabled)
                        ruleList.RemoveAt(targetIndex);
                        rules[targetIndex].Filter.Delete(trans);
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the rules still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < ruleList.Count; i++)
            {
                AssignmentRule rule = (ruleList[i] as AssignmentRule).GetWritableInstance();
                rule.PriorityIndex = (i + 1);
                rule.Save(trans);
            }

            trans.Commit();
        }
    }
}
