<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminUsers.aspx.cs" Inherits="AdminUsersAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><br><br>

    <asp:CheckBox ID="chkShowDisabled" Runat="server" CssClass="chk" AutoPostBack="True" Text="Show users with disabled accounts." />

    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <asp:datagrid ID="grdUsers" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="30" AllowSorting="True">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
		    <asp:hyperlinkcolumn HeaderText="Name" DataTextField="Name" DataNavigateUrlField="ID"
			    DataNavigateUrlFormatString="AdminUser.aspx?id={0}" SortExpression="Name ASC" />
		    <asp:BoundColumn HeaderText="Username" DataField="Username" SortExpression="Username ASC" />
		    <asp:BoundColumn HeaderText="Work Phone" DataField="WorkPhone" SortExpression="WorkPhone ASC, Name ASC" />
            <asp:BoundColumn HeaderText="Fax Number" DataField="FaxNumber" SortExpression="FaxNumber ASC" />
		    <asp:hyperlinkcolumn HeaderText="Email Address" DataTextField="EmailAddress" 
			    DataNavigateUrlField="EmailAddress" DataNavigateUrlFormatString="mailto:{0}" 
			    SortExpression="EmailAddress ASC, Name ASC" />
		    <asp:TemplateColumn HeaderText="Security Role" SortExpression="Role.Name ASC, Name ASC">
			    <ItemTemplate>
				    <%# HtmlEncode(Container.DataItem, "Role.Name") %>
			    </ItemTemplate>
		    </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>

    <a href="AdminUserEdit.aspx">Create New User Account</a>
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
