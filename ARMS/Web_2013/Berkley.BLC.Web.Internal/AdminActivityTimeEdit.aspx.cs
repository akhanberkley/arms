using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using QCI.Web.Validation;
using QCI.ExceptionManagement;

public partial class AdminActivityTimeEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminActivityTimeEditAspx_Load);
        this.PreRender += new EventHandler(AdminActivityTimeEditAspx_PreRender);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.cboActivityType.SelectedIndexChanged += new EventHandler(cboActivityType_SelectedIndexChanged);
        this.btnAttachDocument.Click += new EventHandler(btnAttachDocument_Click);
        this.repDocs.ItemDataBound += new RepeaterItemEventHandler(repDocs_ItemDataBound);
        this.btnAddAnother.Click += new EventHandler(btnAddAnother_Click);
    }
    #endregion

    protected Activity _eActivity;

    void AdminActivityTimeEditAspx_Load(object sender, EventArgs e)
    {
        Guid gActivityID = ARMSUtility.GetGuidFromQueryString("activityid", false);
        _eActivity = (gActivityID != Guid.Empty) ? Activity.Get(gActivityID) : null;

        // Parameters
        bool _dispAgencyListOnActivityType = (Berkley.BLC.Business.Utility.GetCompanyParameter("DispAgencyListOnActivityType", CurrentUser.Company).ToUpper() == "AGENCY VISIT") ? true : false;

        if (!this.IsPostBack)
        {
            // populate the company specific activity types
            ActivityType[] eTypes = ActivityType.GetSortedArray("CompanyID = ? && (LevelCode = ? || LevelCode = ?) && Disabled != True", "PriorityIndex ASC", this.CurrentUser.CompanyID, ActivityTypeLevel.User.Code, ActivityTypeLevel.All.Code);
            cboActivityType.DataBind(eTypes, "ID", "Name");

            // populate the user combo
            User[] eUsers = Berkley.BLC.Entities.User.GetSortedArray("CompanyID = ? && AccountDisabled = false && IsFeeCompany = false && IsUnderwriter = false", "Name ASC", this.CurrentUser.CompanyID);
            cboAllocatedTo.DataBind(eUsers, "ID", "Name");
            cboAllocatedTo.Enabled = CurrentUser.Role.PermissionSet.CanWorkReviews;

            // populate the agency combo (if applicable and enabled by company param.)
            if (_dispAgencyListOnActivityType)
            {
                Agency[] eAgencies = Agency.GetSortedArray("CompanyID = ?", "Name ASC", this.CurrentUser.CompanyID);
                cboAgencyVisited.DataBind(eAgencies, "ID", "{0} - {1}", new string[] { "Name", "Number" });
            }

            if (_eActivity != null) // edit
            {
                this.PopulateFields(_eActivity);
                this.counterCallCount.Text = (_eActivity.CallCount != Decimal.MinValue) ? _eActivity.CallCount.ToString("N2") : string.Empty;

                //Check if the selected items exist in the list
                if (cboActivityType.Items.Contains(new ListItem(_eActivity.Type.Name, _eActivity.TypeID.ToString())))
                {
                    this.PopulateFields(_eActivity.Type);
                }
                else
                {
                    //add it to the list and select it
                    cboActivityType.Items.Add(new ListItem(_eActivity.Type.Name, _eActivity.TypeID.ToString()));
                    cboActivityType.SelectedValue = _eActivity.TypeID.ToString();
                }
            }
            else
            {
                //default user to the current user
                Guid userID = ARMSUtility.GetGuidFromQueryString("userid", false);
                cboAllocatedTo.SelectedValue = (userID != Guid.Empty) ? userID.ToString() : CurrentUser.ID.ToString();
            }

            //visibility
            DetermineVisibility();

            btnSubmit.Text = (_eActivity == null) ? "Add" : "Update";
            base.PageHeading = (_eActivity == null) ? "Add Administrative Time" : "Update Administrative Time";
        }
    }

    void AdminActivityTimeEditAspx_PreRender(object sender, EventArgs e)
    {
        // get all documents associated to this survey request
        ActivityDocument[] docs = ActivityDocument.GetSortedArray("ActivityID = ?", "UploadedOn DESC", (_eActivity != null ? _eActivity.ID : Guid.Empty));
        repDocs.DataSource = docs;
        repDocs.DataBind();
        boxDocuments.Title = string.Format("Supporting Documents ({0})", docs.Length);
    }

    void cboActivityType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DetermineVisibility();
    }

    private void DetermineVisibility()
    {
        cboAllocatedTo.Visible = false;
        cboAgencyVisited.Visible = false;
        dtActivityDate.Visible = false;
        numHours.Visible = false;
        numDays.Visible = false;
        rowCallCount.Visible = false;
        txtComments.Visible = false;
        btnSubmit.Visible = false;
        btnAddAnother.Visible = false;
        boxDocuments.Visible = false;

        if (cboActivityType.SelectedValue.Length <= 0)
        {
            return;
        }
        
        ActivityType activityType = ActivityType.GetOne("ID = ? && CompanyID = ?", cboActivityType.SelectedValue, CurrentUser.CompanyID);
        if (activityType != null)
        {
            btnSubmit.Visible = true;
            btnAddAnother.Visible = (_eActivity == null);

            if (activityType.IsProductive)
            {
                cboAllocatedTo.Visible = true;
                dtActivityDate.Visible = true;
                numHours.Visible = true;
                rowCallCount.Visible = true;
                txtComments.Visible = true;
                boxDocuments.Visible = true;
            }
            else
            {
                cboAllocatedTo.Visible = true;
                dtActivityDate.Visible = true;
                numHours.Visible = true;
                numDays.Visible = true;
                txtComments.Visible = true;
            }

            // Parameters
            bool _dispAgencyListOnActivityType = (Berkley.BLC.Business.Utility.GetCompanyParameter("DispAgencyListOnActivityType", CurrentUser.Company).ToUpper() == "AGENCY VISIT") ? true : false;

            if (activityType.Name == "Agency Visit" && _dispAgencyListOnActivityType)
            {
                cboAgencyVisited.Visible = true;
            }
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        //Save the form data first
        if (SaveActivity())
        {
            if (sender == bool.TrueString)
            {
                Response.Redirect("AdminActivityTimeEdit.aspx", true);
            }
            else
            {
                Response.Redirect("AdminActivityTimes.aspx", true);
            }
        }
    }

    void btnAddAnother_Click(object sender, EventArgs e)
    {
        btnSubmit_Click(bool.TrueString, null);
    }

    private bool SaveActivity()
    {
        try
        {
            if (numHours.Text.Trim().Length <= 0 && numDays.Text.Trim().Length <= 0)
            {
                throw new FieldValidationException("The number of hours or days must be entered.");
            }

            if (counterCallCount.Text.Trim().Length > 0)
            {
                try
                {
                    Convert.ToDecimal(counterCallCount.Text);
                }
                catch (FormatException)
                {
                    throw new FieldValidationException("The call count is not a valid value.");
                }
            }

            Activity eActivity;
            if (_eActivity == null) // add
            {
                eActivity = new Activity(Guid.NewGuid());
            }
            else
            {
                eActivity = _eActivity.GetWritableInstance();
            }

            eActivity.TypeID = new Guid(cboActivityType.SelectedValue);
            eActivity.AllocatedTo = new Guid(cboAllocatedTo.SelectedValue);
            eActivity.AgencyVisitedID = (!string.IsNullOrWhiteSpace(cboAgencyVisited.SelectedValue)) ? new Guid(cboAgencyVisited.SelectedValue) : Guid.Empty;
            eActivity.Date = dtActivityDate.Date;
            eActivity.Hours = (numHours.Text.Trim().Length > 0 && numHours.Visible) ? Convert.ToDecimal(numHours.Text) : decimal.MinValue;
            eActivity.Days = (numDays.Text.Trim().Length > 0 && numDays.Visible) ? Convert.ToDecimal(numDays.Text) : decimal.MinValue;
            eActivity.CallCount = (counterCallCount.Text.Trim().Length > 0 && rowCallCount.Visible) ? Convert.ToDecimal(counterCallCount.Text) : decimal.MinValue;
            eActivity.Comments = txtComments.Text;

            // commit changes
            eActivity.Save();

            // update our member var so the redirect will work correctly
            _eActivity = eActivity;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return false;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return false;
        }

        return true;
    }


    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("AdminActivityTimes.aspx"), true);
    }

    protected string GetFileNameWithSize(object oDataItem)
    {
        ActivityDocument eDoc = (ActivityDocument)oDataItem;
        string result = string.Format("{0} ({1:#,##0} KB)", eDoc.DocumentName, eDoc.SizeInBytes / 1024);

        return Server.HtmlEncode(result);
    }

    void btnAttachDocument_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }
        
        HttpPostedFile postedFile = fileUploader.PostedFile;

        try
        {
            // make sure a filename was uploaded
            if (postedFile == null || postedFile.FileName.Length == 0) // no filename was specified
            {
                throw new FieldValidationException(fileUploader, "Please select a file to submit.");
            }

            // make sure the file name is not larger the max allowed
            string fileName = System.IO.Path.GetFileName(postedFile.FileName);
            if (fileName.Length > 200)
            {
                throw new FieldValidationException(fileUploader, "The file name cannot exceed 200 characters.");
            }

            // check for an extesion
            if (System.IO.Path.GetExtension(fileName).Length <= 0)
            {
                throw new FieldValidationException(fileUploader, "The file name must contain a file extension.");
            }

            // make sure the file has content
            int max = int.Parse(ConfigurationManager.AppSettings["MaxRequestLength"]);
            if (postedFile.ContentLength <= 0) // the file is empty
            {
                throw new FieldValidationException(fileUploader, "The file submitted is empty.");
            }
            else if (postedFile.ContentLength > max)
            {
                // the maximum file size is 10 Megabytes
                throw new FieldValidationException(fileUploader, "The file size cannot be larger than 10 Megabytes.");
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        //Save the form data first
        if (SaveActivity())
        {
            // attach this file to the activity
            try
            {
                // remove any pathing info from the specified filename and get the extension
                string fileName = System.IO.Path.GetFileName(postedFile.FileName);
                string extension = System.IO.Path.GetExtension(fileName);

                // save the doc to the imaging server and get the FileNet reference
                ImagingHelper imaging = new ImagingHelper(this.CurrentUser.Company);
                string fileNetRef = imaging.StoreFileImage(extension, postedFile.ContentType, postedFile.InputStream);

                // create a record of this file with the survey 
                ActivityDocument doc = new ActivityDocument(Guid.NewGuid());
                doc.ActivityID = _eActivity.ID;
                doc.DocumentName = fileName;
                doc.MimeType = postedFile.ContentType;
                doc.SizeInBytes = postedFile.InputStream.Length;
                doc.FileNetReference = fileNetRef;
                doc.UploadUserID = this.CurrentUser.ID;
                doc.UploadedOn = DateTime.Now;
                doc.Save();
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);
                throw (ex);
            }

            Response.Redirect(string.Format(string.Format("AdminActivityTimeEdit.aspx?activityid={0}", _eActivity.ID)), true);
        }
    }

    void repDocs_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ActivityDocument eDoc = (ActivityDocument)e.Item.DataItem;
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkRemoveDoc");
            lnk.Attributes.Add("DocumentID", eDoc.ID.ToString());

            JavaScript.AddConfirmationNoticeToWebControl(lnk, string.Format("Are your sure you want to remove '{0}'?", eDoc.DocumentName));
        }
    }

    protected void lnkRemoveDoc_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        ActivityDocument doc = ActivityDocument.Get(new Guid(lnk.Attributes["DocumentID"]));

        // remove the file
        try
        {
            //delete the doc
            doc.Delete();
        }
        catch (Exception ex)
        {
            //ExceptionManager.Publish(ex);
            throw (ex);
        }
    }
}
