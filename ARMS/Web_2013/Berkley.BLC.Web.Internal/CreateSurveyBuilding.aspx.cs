using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web.Validation;
using QCI.Web;

public partial class CreateSurveyBuildingAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(CreateSurveyBuildingAspx_Load);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnAddAnother.Click += new EventHandler(btnAddAnother_Click);
    }
    #endregion

    protected Location _eLocation;
    protected Survey _eSurvey;
    protected Building _eBuilding;
    protected bool _isExistingSurvey = false;

    void CreateSurveyBuildingAspx_Load(object sender, EventArgs e)
    {
        Guid gBuildingID = ARMSUtility.GetGuidFromQueryString("buildingid", false);
        _eBuilding = (gBuildingID != Guid.Empty) ? Building.Get(gBuildingID) : null;

        //Guids to navigate back
        Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        try
        {
            _eSurvey = Survey.Get(gSurveyID);
        }
        catch
        {
            //survey id no longer exists
            Security.RequestExpired();
        }
        _isExistingSurvey = (_eSurvey.Location != null && _eSurvey.CreateState != CreateState.InProgress);

        Guid gLocationID = ARMSUtility.GetGuidFromQueryString("locationid", false);
        _eLocation = (gLocationID != Guid.Empty) ? Location.Get(gLocationID) : _eSurvey.Location;

        if (!this.IsPostBack)
        {
            cboHasSprinklerSystem.Name = CurrentUser.Company.BuildingSprinklerSystemLabel;
            cboHasSprinklerSystemCredit.Name = CurrentUser.Company.BuildingSprinklerSystemLabel + " Credit";
            cboITV.Visible = (Berkley.BLC.Business.Utility.GetCompanyParameter("UsesBuildingITV", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
            
            //Populate the booleans
            ComboForBooleans[] cboData = ComboForBooleans.GetValues();
            cboHasSprinklerSystem.DataBind(cboData, "ID", "DisplayText");
            cboHasSprinklerSystemCredit.DataBind(cboData, "ID", "DisplayText");
            cboITV.DataBind(cboData, "ID", "DisplayText");

            if (_eBuilding != null) // edit
            {
                this.PopulateFields( _eBuilding);
                if (_eBuilding.CoinsurancePercentage != Decimal.MinValue)
                {
                    txtCoinsurance.Text = (_eBuilding.CoinsurancePercentage * 100).ToString("N0");
                }
            }

            btnSubmit.Text = (_eBuilding == null) ? "Add" : "Update";
            btnAddAnother.Visible = (_eBuilding == null);
            base.PageHeading = (_eBuilding == null) ? "Add Building" : "Update Building";
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            //Check that the coinsurance is a valid percentage
            decimal coinsPercent = decimal.MinValue;
            if (txtCoinsurance.Text != string.Empty)
            {
                coinsPercent = Convert.ToDecimal(txtCoinsurance.Text);
                if (coinsPercent > 100 || coinsPercent < 1)
                {
                    throw new FieldValidationException(txtCoinsurance, "The coinsurance percentage has to be between 1 and 100.");
                }
                else
                {
                    coinsPercent = coinsPercent / 100;
                }
            }

            Building eBuilding;
            if (_eBuilding == null) // add
            {
                eBuilding = new Building(Guid.NewGuid());
                eBuilding.LocationID = _eLocation.ID;
            }
            else
            {
                eBuilding = _eBuilding.GetWritableInstance();
            }

            this.PopulateEntity(eBuilding);
            eBuilding.CoinsurancePercentage = coinsPercent;

            // commit changes
            eBuilding.Save();

            // update our member var so the redirect will work correctly
            _eBuilding = eBuilding;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        if (sender == bool.TrueString)
        {
            Response.Redirect(string.Format("CreateSurveyBuilding.aspx{0}", ARMSUtility.GetQueryStringForNav("buildingid")));
        }
        else
        {
            Response.Redirect(GetReturnURL(), true);
        }
    }

    void btnAddAnother_Click(object sender, EventArgs e)
    {
        btnSubmit_Click(bool.TrueString, null);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnURL(), true);
    }

    private string GetReturnURL()
    {
        if (_isExistingSurvey)
        {
            return string.Format("Insured.aspx{0}", ARMSUtility.GetQueryStringForNav("buildingid"));
        }
        else
        {
            return UrlBuilder("CreateSurveyLocation.aspx");
        }
    }

    protected string UrlBuilder(string sWebPage)
    {
        return UrlBuilder(sWebPage, null, null);
    }

    protected string UrlBuilder(string sWebPage, string sKey, object oValue)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat(sWebPage);
        sb.AppendFormat("?surveyid={0}", _eSurvey.ID);
        sb.AppendFormat("&locationid={0}", _eLocation.ID);

        if (sKey != null && sKey.Length > 0)
        {
            sb.AppendFormat("&{0}={1}", sKey, oValue);
        }

        return sb.ToString();
    }

    private class ComboForBooleans
    {
        private int id = 0;
        private string displayText = string.Empty;

        public ComboForBooleans() { }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public string DisplayText
        {
            get { return displayText; }
            set { displayText = value; }
        }

        public static ComboForBooleans[] GetValues()
        {
            ArrayList list = new ArrayList();

            for (int i = 1; i >= -1; i--)
            {
                ComboForBooleans cbo = new ComboForBooleans();

                if (i == -1)
                {
                    cbo.DisplayText = string.Empty;
                    cbo.ID = Int16.MinValue;
                    list.Insert(0, cbo);
                }
                else
                {
                    cbo.DisplayText = (i == 0) ? "No" : "Yes";
                    cbo.ID = i;
                    list.Add(cbo);
                }
            }

            return list.ToArray(typeof(ComboForBooleans)) as ComboForBooleans[];
        }
    }
}
