using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using Wilson.ORMapper;
using QCI.Web.Validation;

public partial class SurveyReviewAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(SurveyReviewAspx_Load);
        this.PreRender += new EventHandler(SurveyReviewAspx_PreRender);
        this.repCategories.ItemDataBound += new RepeaterItemEventHandler(repCategories_ItemDataBound);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected Survey _survey;
    protected ReviewerType _reviewerType;
    protected ReviewRatingType[] _ratingTypes;
    protected SurveyReview _surveyReview;
    protected bool _isReadOnly;

    void SurveyReviewAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        _survey = Survey.Get(id);

        string code = ARMSUtility.GetStringFromQueryString("reviewertype", true);
        _reviewerType = ReviewerType.Get(code);

        //based on the survey and reviewer type, check it this is a new or existing review
        _surveyReview = SurveyReview.GetOne("SurveyID = ? && ReviewerTypeCode = ?", _survey.ID, _reviewerType.Code);

        //check if this user has only read-only permissions
        SurveyManager manager = SurveyManager.GetInstance(_survey, CurrentUser);
        _isReadOnly = (_surveyReview != null && _surveyReview.ReviewerTypeCode == ReviewerType.Manager.Code && !manager.VerifyUserCanTakeThisAction(SurveyActionType.PerformQCR) || (_surveyReview != null && !CurrentUser.IsUnderwriter && _surveyReview.ReviewerTypeCode == ReviewerType.Underwriter.Code));
        btnCancel.Visible = !_isReadOnly;
        btnSubmit.Text = (_isReadOnly) ? "Back to Survey" : "Submit";
        btnSubmit.CausesValidation = !_isReadOnly;

        base.PageHeading = _reviewerType.Name;
    }

    void SurveyReviewAspx_PreRender(object sender, EventArgs e)
    {
        _ratingTypes = ReviewRatingType.GetSortedArray("CompanyID = ? && ReviewerTypeCode = ?", "DisplayOrder ASC", CurrentUser.CompanyID, _reviewerType.Code);
        repRatings.DataSource = _ratingTypes;
        repRatings.DataBind();

        ReviewCategoryType[] ratingCategories = ReviewCategoryType.GetSortedArray("CompanyID = ? && ReviewerTypeCode = ?", "DisplayOrder ASC", CurrentUser.CompanyID, _reviewerType.Code);
        repCategories.DataSource = ratingCategories;
        repCategories.DataBind();
    }

    protected string GetRatingName(object dataItem)
    {
        ReviewRatingType ratingType = dataItem as ReviewRatingType;
        return string.Format("{0} - {1}", ratingType.RatingNumber, ratingType.RatingName);
    }

    protected string GetCategoryDescription(object dataItem)
    {
        ReviewCategoryType category = dataItem as ReviewCategoryType;
        return string.Format("{0}. {1}", category.DisplayOrder + 1, category.Description);
    }

    void repCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //get controls
            DropDownList cboRating = (DropDownList)e.Item.FindControl("cboRating");
            TextBox txtComments = (TextBox)e.Item.FindControl("txtComments");
            Label lblComments = (Label)e.Item.FindControl("lblComments");
            RequiredFieldValidator valRating = (RequiredFieldValidator)e.Item.FindControl("valRating");

            txtComments.Visible = !_isReadOnly;
            lblComments.Visible = _isReadOnly;
            cboRating.Enabled = !_isReadOnly;
            
            ReviewRatingType[] ratingTypes = _ratingTypes;
            QCI.Web.ComboHelper.BindCombo(cboRating, ratingTypes, "ID", "RatingNumber");
            cboRating.Items.Insert(0, new ListItem("", ""));

            //add the id of the category for retrieving later
            ReviewCategoryType category = (ReviewCategoryType)e.Item.DataItem;
            cboRating.Attributes.Add("ReviewCategoryTypeID", category.ID.ToString());

            //check if anything is required
            if (category.IsRequired)
            {
                valRating.Enabled = true;
                valRating.Text = "*";
                valRating.ErrorMessage = string.Format("Rating for question #{0} is required.", category.DisplayOrder + 1);
            }

            //populate the selections for existing reviews
            if (_surveyReview != null)
            {
                SurveyReviewRating surveyReviewRating = SurveyReviewRating.GetOne("SurveyReviewID = ? && ReviewCategoryTypeID = ?", _surveyReview.ID, category.ID);
                if (surveyReviewRating != null)
                {
                    cboRating.SelectedValue = surveyReviewRating.ReviewRatingTypeID.ToString();
                    txtComments.Text = surveyReviewRating.Comments;
                    lblComments.Text = (surveyReviewRating.Comments.Length > 0) ? surveyReviewRating.Comments : "(not specified)";
                }
            }
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {

            if (!_isReadOnly)
            {
                decimal scoreTotal = 0;
                decimal questionsAnswered = 0;

                using (Transaction trans = DB.Engine.BeginTransaction())
                {
                    SurveyReview surveyReview;

                    if (_surveyReview == null) //new
                    {
                        surveyReview = new SurveyReview(Guid.NewGuid());
                        surveyReview.SurveyID = _survey.ID;
                        surveyReview.ReviewerTypeCode = _reviewerType.Code;
                        surveyReview.CreateDate = DateTime.Now;

                    }
                    else //updating
                    {
                        surveyReview = _surveyReview.GetWritableInstance();

                        //remove all the previous ratings
                        SurveyReviewRating[] surveyReviewRatings = SurveyReviewRating.GetArray("SurveyReviewID = ?", _surveyReview.ID);
                        foreach (SurveyReviewRating surveyReviewRating in surveyReviewRatings)
                        {
                            surveyReviewRating.Delete(trans);
                        }
                    }

                    surveyReview.ReviewedBy = CurrentUser.Username;
                    surveyReview.ReviewedDate = DateTime.Now;
                    surveyReview.Save(trans);

                    foreach (RepeaterItem item in repCategories.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            DropDownList cboRating = (DropDownList)item.FindControl("cboRating");
                            TextBox txtComments = (TextBox)item.FindControl("txtComments");
                            Guid categoryID = new Guid(cboRating.Attributes["ReviewCategoryTypeID"]);

                            ReviewRatingType rating = null;
                            if (!String.IsNullOrEmpty(cboRating.SelectedValue))
                            {
                                rating = ReviewRatingType.GetOne("ID = ?", cboRating.SelectedValue);
                                scoreTotal += rating.RatingNumber;
                                questionsAnswered += 1;
                            }

                            //save the selections
                            SurveyReviewRating review = new SurveyReviewRating(Guid.NewGuid());
                            review.SurveyReviewID = surveyReview.ID;
                            review.ReviewCategoryTypeID = categoryID;
                            review.ReviewRatingTypeID = (rating != null) ? rating.ID : Guid.Empty;
                            review.Comments = txtComments.Text;
                            review.Save(trans);
                        }
                    }

                    trans.Commit();

                    if (_surveyReview == null && surveyReview.ReviewerType == ReviewerType.Manager && _survey.ConsultantUser != null)
                    {
                        //email the consultant
                        SurveyManager manager = SurveyManager.GetInstance(_survey, CurrentUser);
                        manager.SurveyReview();
                    }
                }

                QCI.Web.JavaScript.ShowMessageAndRedirect(this, string.Format("The following are the overall results:\n\nTotal Score: {0}\nAverage Score: {1:N2}", scoreTotal, scoreTotal / questionsAnswered), string.Format("Survey.aspx{0}", ARMSUtility.GetQueryStringForNav("reviewertype")));
            }
            else
            {
                Response.Redirect(string.Format("Survey.aspx{0}", ARMSUtility.GetQueryStringForNav("reviewertype")), true);
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("Survey.aspx{0}", ARMSUtility.GetQueryStringForNav("reviewertype")), true);
    }
}
