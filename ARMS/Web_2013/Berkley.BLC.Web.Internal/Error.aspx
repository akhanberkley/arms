<%@ Page Language="C#" AutoEventWireup="false" CodeFile="Error.aspx.cs" Inherits="ErrorAspx" ValidateRequest="false" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" TabsVisible="false" />
    
    <span class="heading" style="color:maroon; font-weight: bold;"><%= base.PageHeading %></span><br /><br />
    
    <cc:Box id="boxError" runat="server" width="900">
        <p style="padding:0px 20px 0px 20px;">
            <b><u>Details:</u></b>&nbsp;&nbsp;
            <asp:Label ID="lblErrorDetails" runat="server">An application error occurred on the server while processing your request.</asp:Label><br /><br />
        </p>
            <hr style="margin:0px 20px 0px 20px;" />
        <p style="padding:0px 20px 0px 20px;">
            If this problem persists, please report the incident to the <a href="mailto:btsprod@service-now.com">BTS Service Desk</a>. 
        </p>
    </cc:Box>

    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
