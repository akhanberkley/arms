<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" ValidateRequest="false" CodeFile="ManagePlanObjectives.aspx.cs" Inherits="ManagePlanObjectives" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register TagPrefix="dp" Namespace="QCI.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="Counter" Src="~/Controls/Counter.ascx" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" media="screen" type="text/css" href="<%= base.TemplatePath %>/styles.css" />
    <link rel="stylesheet" media="print" type="text/css" href="<%= base.TemplatePath %>/Print-Survey.css" />
    
    <script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/Keyoti_RapidSpell_Web_Common/RapidSpell-DIALOG.js"></script>
    <script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/javascripts/csshttprequest.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="<%=ARMSUtility.TemplatePath %>/atd.css" />

     
    <div class="surveyscreen">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span>

    <a class="nav" href="ServicePlan.aspx<%= ARMSUtility.GetQueryStringForNav() %>"><< Back to Service Plan</a>
    
    
    <br /><br />
    
        
   
    <table cellspacing="0" cellpadding="0" border="0">
    <tr>
	    <td valign="top" width="500">
        
        
        <cc:Box id="boxObjectives" runat="server" title="Objectives" width="100%">
            
            <asp:datagrid ID="grdObjectives" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
	            <headerstyle CssClass="header" />
	            <itemstyle CssClass="item" />
	            <alternatingitemstyle CssClass="altItem" />
	            <footerstyle CssClass="footer" />
	            <pagerstyle CssClass="pager" Mode="NumericPages" />
	            <columns>
                    <asp:TemplateColumn HeaderText="Number" SortExpression="Number ASC">
                        <ItemTemplate>
                            <a href="ObjectiveEdit.aspx<%= ARMSUtility.GetQueryStringForNav() %>&objectiveid=<%# HtmlEncode(Container.DataItem, "ID")%>"><%# HtmlEncode(Container.DataItem, "Number").PadLeft(3,'0')%></a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Objective" SortExpression="Text ASC">
                        <ItemTemplate>
	                        <%# HtmlEncodeNullable(Container.DataItem, "Text", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="Comments" SortExpression="CommentsText ASC">
                        <ItemTemplate>
	                        <%# HtmlEncodeNullable(Container.DataItem, "CommentsText", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Target Date" SortExpression="TargetDate ASC">
                        <ItemTemplate>
	                        <%# HtmlEncodeNullable(Container.DataItem, "TargetDate", "{0:d}", DateTime.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" SortExpression="Status.Name ASC">
                        <ItemTemplate>
	                        <%# HtmlEncodeNullable(Container.DataItem, "Status.Name", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
	                        &nbsp;<asp:LinkButton ID="btnRemoveObjective" Runat="server" CommandName="Remove" OnPreRender="btnRemoveObjective_PreRender" CausesValidation="false">Remove</asp:LinkButton>&nbsp;
                        </ItemTemplate>
                    </asp:TemplateColumn>
	            </columns>
            </asp:datagrid>
            
            <a id="lnkAddObjective" href="ObjectiveEdit.aspx" visible="true" runat="server">Create New Plan Objective</a>
            
        </cc:Box>

        
        
	    </td>
    </tr>
    </table>

 
    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
    function Toggle(id)
    {
	    ToggleControlDisplay(id);
    }
    function GetLetterSentDate(surveyID, docID)
    {
	    var date = AjaxMethods.GetLetterSentDate(surveyID, docID).value;
	    document.getElementById('dpMailSentDate').value = date;
    }
    function Visit()
    {
	    var chk = document.getElementById('rdoVisitYes');
	    if (chk.checked) {
	        document.getElementById('divFutureVisit').style.display = 'inline';
	    }
	    else {
	        document.getElementById('divFutureVisit').style.display = 'none';
	    }
    }
    function GenerateLetter(surveyID)
    {
	    var cboLetters = document.getElementById('cboLetters');
	    
	    if (cboLetters.value != null && cboLetters.value != "") {
	        window.open('document.aspx?letterid=' + cboLetters.value + '&surveyid=' + surveyID);
	        document.location = document.URL;
	    }
	    else {
	        alert("Select a letter to generate.");
	    }
    }
    function GenerateReport(surveyID)
    {
	    var cboReports = document.getElementById('cboReports');
	    
	    if (cboReports.value != null && cboReports.value != "") {
	        window.open('document.aspx?reportid=' + cboReports.value + '&surveyid=' + surveyID);
	        document.location = document.URL;
	    }
	    else {
	        alert("Select a report to generate.");
	    }
    }
    </script>
    </form>
    </div>
</body>
</html>
