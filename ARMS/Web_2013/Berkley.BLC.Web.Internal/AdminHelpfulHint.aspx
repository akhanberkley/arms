<%@ Page Language="C#" AutoEventWireup="false" ValidateRequest="false" CodeFile="AdminHelpfulHint.aspx.cs" Inherits="AdminHelpfulHintAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="ftb" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    
    
    Screen: <asp:DropDownList ID="cboWebPage" Runat="server" CssClass="cbo" AutoPostBack="True" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:CheckBox ID="chkAlwaysVisible" CssClass="chk" runat="server" Text="Hint Always Visible when Screen is Scrolled Up and Down" /><br /><br />
    <cc:Box id="boxTask" runat="server" title="Enter Help Text">
	    <table>
	        <tr>
	            <td>
	                <ftb:FreeTextBox id="txtHelpfulHintText" EnableHtmlMode="false" Height="200" ToolbarLayout="FontForeColorsMenu, Bold, Italic, Underline, Strikethrough, Superscript, Subscript, JustifyLeft, JustifyRight, JustifyCenter, JustifyFull, BulletedList, NumberedList, Indent, Outdent, Cut, Copy, Paste, Delete, SymbolsMenu" runat="Server" />
	            </td>
	        </tr>
        </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Update" />&nbsp;
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
