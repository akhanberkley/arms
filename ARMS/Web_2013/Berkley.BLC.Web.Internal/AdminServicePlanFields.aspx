<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminServicePlanFields.aspx.cs" Inherits="AdminServicePlanFieldsAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <asp:CheckBox ID="chkShowDisabled" Runat="server" CssClass="chk" AutoPostBack="True" Text="Show disabled recs." />
    <asp:datagrid ID="grdFields" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="false" AllowSorting="false">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
		    <asp:hyperlinkcolumn HeaderText="Description" DataTextField="Description" DataNavigateUrlField="ID"
			    DataNavigateUrlFormatString="AdminServicePlanFieldEdit.aspx?fieldid={0}" />
			<asp:TemplateColumn HeaderText="Required">
                <ItemTemplate>
	                <%# GetBooleanString(Container.DataItem)%>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Move">
			    <ItemTemplate>
				    <asp:LinkButton ID="btnUp" Runat="server" CommandName="MoveUp">Up</asp:LinkButton> /
				    <asp:LinkButton ID="btnDown" Runat="server" CommandName="MoveDown">Down</asp:LinkButton>
			    </ItemTemplate>
		    </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>

    <a href="AdminServicePlanFieldEdit.aspx">Add a New Field</a>
    
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
