<%@ Page Language="C#" AutoEventWireup="false" CodeFile="Recommendations.aspx.cs" Inherits="RecommendationsAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading">Current Recs</span><a class="nav" href="Survey.aspx<%= ARMSUtility.GetQueryStringForNav("surveyrecid") %>"><%if (_eSurvey.ServiceType == Berkley.BLC.Entities.ServiceType.ServiceVisit) {%><< Back to Service Visit<%} else {%><< Back to Survey<%}%></a><br><br>

    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <asp:datagrid ID="grdRecs" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
		    <asp:BoundColumn HeaderText="Order" DataField="PriorityIndex" />
		    <asp:TemplateColumn HeaderText="Number">
	            <ItemTemplate>
		            <a href="RecommendationEdit.aspx<%= ARMSUtility.GetQueryStringForNav() %>&surveyrecid=<%# HtmlEncode(Container.DataItem, "ID")%>">
		                <%# HtmlEncodeNullable(Container.DataItem, "RecommendationNumber", string.Empty, "(none)")%>
		            </a>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Survey #">
	            <ItemTemplate>
		                <%# HtmlEncode(Container.DataItem, "Survey.Number")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Surveyed Date">
	            <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "Survey.SurveyedDate", "{0:d}", DateTime.MinValue, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
		    <asp:TemplateColumn HeaderText="Rec Code">
	            <ItemTemplate>
		                <%# HtmlEncode(Container.DataItem, "Recommendation.Code")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
		    <asp:TemplateColumn HeaderText="Rec Title">
	            <ItemTemplate>
		            <span title='Description: <%# HtmlEncodeNullable(Container.DataItem, "Recommendation.Description", string.Empty, "(none)")%>'>
                    <%# HtmlEncodeNullable(Container.DataItem, "Recommendation.Title", string.Empty, "(none)")%></span>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Classification">
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "RecClassification.Type.Name", null, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Date Created">
            <HeaderStyle Wrap="false" />
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "DateCreated", "{0:d}", DateTime.MinValue, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Date Completed">
            <HeaderStyle Wrap="false" />
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "DateCompleted", "{0:d}", DateTime.MinValue, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Status">
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "RecStatus.Type.Name", null, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Move">
		            <ItemTemplate>
			            &nbsp;<asp:LinkButton ID="btnUp" Runat="server" CommandName="MoveUp">Up</asp:LinkButton>&nbsp;/
			            &nbsp;<asp:LinkButton ID="btnDown" Runat="server" CommandName="MoveDown">Down</asp:LinkButton>&nbsp;
		            </ItemTemplate>
	            </asp:TemplateColumn>
	            <asp:TemplateColumn>
		            <ItemTemplate>
			            &nbsp;<asp:LinkButton ID="btnRemoveRec" Runat="server" CommandName="Remove">Remove</asp:LinkButton>&nbsp;
		            </ItemTemplate>
	            </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>
    
    <% if (!CurrentUser.IsUnderwriter || (CurrentUser.IsUnderwriter && CurrentUser.Company.AllowUnderwritersToEditRecs)){ %>
    <a href="RecommendationEdit.aspx<%= ARMSUtility.GetQueryStringForNav("surveyrecid") %>">Create New Recommendation</a>
    
    <br /><br /><br />
    <span class="heading">Previous Recs</span><br />
    <span><b>* Note: You can add a previous rec for this client's location(s) by clicking on the 'Add To Current Survey' link.</b></span><br /><br />
    Show: <asp:DropDownList ID="cboFilter" Runat="server" CssClass="cbo" AutoPostBack="True">
        <asp:ListItem Text="Prior Open Recs" Value="O" Selected="True"></asp:ListItem>
        <asp:ListItem Text="All Prior Recs" Value=""></asp:ListItem>
        </asp:DropDownList>
    <asp:datagrid ID="grdOldRecs" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
		    <asp:TemplateColumn HeaderText="Number">
	            <ItemTemplate>
		            <a href="RecommendationEdit.aspx<%= ARMSUtility.GetQueryStringForNav() %>&surveyrecid=<%# HtmlEncode(Container.DataItem, "ID")%>">
		                <%# HtmlEncodeNullable(Container.DataItem, "RecommendationNumber", string.Empty, "(none)")%>
		            </a>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Survey #">
	            <ItemTemplate>
		                <%# HtmlEncode(Container.DataItem, "Survey.Number")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Surveyed Date">
	            <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "Survey.SurveyedDate", "{0:d}", DateTime.MinValue, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Location&nbsp;#(s)">
	            <ItemTemplate>
		                <%# GetLocationNumbers(DataBinder.Eval(Container.DataItem, "Survey")) %>
	            </ItemTemplate>
            </asp:TemplateColumn>
		    <asp:TemplateColumn HeaderText="Rec Code">
	            <ItemTemplate>
		                <%# HtmlEncode(Container.DataItem, "Recommendation.Code")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
		    <asp:TemplateColumn HeaderText="Rec Title">
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "Recommendation.Title", string.Empty, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Classification">
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "RecClassification.Type.Name", null, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Date Created">
            <HeaderStyle Wrap="false" />
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "DateCreated", "{0:d}", DateTime.MinValue, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Date Completed">
            <HeaderStyle Wrap="false" />
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "DateCompleted", "{0:d}", DateTime.MinValue, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Status">
	            <ItemTemplate>
		            <%# HtmlEncodeNullable(Container.DataItem, "RecStatus.Type.Name", null, "(none)")%>
	            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn>
	            <ItemTemplate>
		            &nbsp;<asp:LinkButton ID="btnRemoveRec" Runat="server" CommandName="Remove">Add To Current Survey</asp:LinkButton>&nbsp;
	            </ItemTemplate>
            </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>
    <% } %>

    <uc:HintFooter ID="ucHintFooter" runat="server" VerticalOffset="130" />
    <uc:Footer ID="ucFooter" runat="server" />

    </form>
</body>
</html>
