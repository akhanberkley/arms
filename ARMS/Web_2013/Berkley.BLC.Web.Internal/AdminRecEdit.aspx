<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminRecEdit.aspx.cs" Inherits="AdminRecEditAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <cc:Box id="boxRec" runat="server" title="Rec Information" width="600">
        <table class="fields">
        <uc:Text id="txtCode" runat="server" field="Recommendation.Code" Name="Code" Columns="30" />
        <uc:Text id="txtTitle" runat="server" field="Recommendation.Title" Name="Title" Columns="90" />
        <uc:Text id="txtDescription" runat="server" field="Recommendation.Description" Name="Description" Rows="25" Columns="90" />
        <uc:Check id="chkRecDisabled" runat="server" field="Recommendation.Disabled" />
        </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Add/Update" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
