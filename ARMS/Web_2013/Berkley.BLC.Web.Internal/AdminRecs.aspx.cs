using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web;

public partial class AdminRecsAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminRecsAspx_Load);
        this.PreRender += new EventHandler(AdminRecsAspx_PreRender);
        this.grdRecs.SortCommand += new DataGridSortCommandEventHandler(grdRecs_SortCommand);
        this.grdRecs.PageIndexChanged += new DataGridPageChangedEventHandler(grdRecs_PageIndexChanged);
    }
    #endregion

    void AdminRecsAspx_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // restore sort expression, reverse state, and page index settings
            this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", grdRecs.Columns[0].SortExpression);
            this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", false);
            grdRecs.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);
        }
    }

    void AdminRecsAspx_PreRender(object sender, EventArgs e)
    {
        string sortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sortExp = sortExp.Replace(" DESC", " TEMP");
            sortExp = sortExp.Replace(" ASC", " DESC");
            sortExp = sortExp.Replace(" TEMP", " ASC");
        }
        
        // get the company specific recs
        Recommendation[] eRecs = Recommendation.GetSortedArray("CompanyID = ? && Disabled = ?", sortExp, this.CurrentUser.CompanyID, chkShowDisabled.Checked);

        string noRecordsMessage = string.Format("There are no {0} recs for your company.", (!chkShowDisabled.Checked) ? "active" : "disabled");
        grdRecs.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindPagingGrid(grdRecs, eRecs, null, noRecordsMessage);
    }

    void grdRecs_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdRecs.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);
    }

    void grdRecs_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sortRev);
    }
}
