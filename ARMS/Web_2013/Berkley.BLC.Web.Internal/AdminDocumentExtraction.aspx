<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminDocumentExtraction.aspx.cs" Inherits="AdminDocumentExtractionAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <script language="javascript">
        function setValidationState(sender, validator) {
            ValidatorEnable(document.getElementById(validator), sender.checked);
        }
    </script>
    
    <span class="heading"><%= base.PageHeading %></span>
    <span style="MARGIN-LEFT: 20px">
	    <a class="nav" href="<%= GetReturnUrl() %>" style="MARGIN-LEFT: 20px"><< Back to Manage Document</a>
    </span><br>
    <br>
    
    <cc:Box id="boxResults" runat="server" Title="Reportable Fields">
        <span id="lblNoResults" runat="server" visible="false">There are no form fields for this document.</span>
        <asp:datagrid ID="grdFormFields" Runat="server" CssClass="grid" AutoGenerateColumns="False" PageSize="500" AllowPaging="False" AllowSorting="False">
	        <headerstyle CssClass="header" />
	        <itemstyle CssClass="item" />
	        <alternatingitemstyle CssClass="altItem" />
	        <footerstyle CssClass="footer" />
	        <pagerstyle CssClass="pager" Mode="NumericPages" />
	        <columns>
	            <asp:TemplateColumn HeaderText="Reportable" ItemStyle-HorizontalAlign="center">
	                <ItemTemplate>
			                <asp:CheckBox ID="chkInclude" CssClass="chkReportable" runat="server" />
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Field ID">
	                <ItemTemplate>
			                <%# HtmlEncodeNullable(Container.DataItem, "Name", string.Empty, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Display Name" ItemStyle-HorizontalAlign="Center">
	                <ItemTemplate>
                            <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName" Display="Static" Text="*" Enabled="false"></asp:RequiredFieldValidator>
			                <asp:TextBox ID="txtDisplayName" runat="server" CssClass="txt" Columns="50" MaxLength="500"></asp:TextBox>&nbsp;&nbsp;
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Field Type">
	                <ItemTemplate>
			                <%# HtmlEncodeNullable(Container.DataItem, "Type", string.Empty, "(none)").Replace("FieldForm", "")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Data Type" ItemStyle-HorizontalAlign="Center">
	                <ItemTemplate>
			                <asp:RequiredFieldValidator ID="rfvDataType" runat="server" ControlToValidate="cboDataType" Display="Static" Text="*" Enabled="false"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="cboDataType" runat="server" CssClass="cbo" />&nbsp;&nbsp;
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Required" ItemStyle-HorizontalAlign="center">
	                <ItemTemplate>
	                        <asp:CheckBox ID="chkRequired" CssClass="chk" runat="server" />
	                </ItemTemplate>
                </asp:TemplateColumn>
	        </columns>
        </asp:datagrid>
    </cc:Box>

    <% if (grdMissingFormFields.Items.Count > 0) { %>
    <cc:Box id="boxMissingResults" runat="server" Title="Missing Reportable Fields">
        <asp:datagrid ID="grdMissingFormFields" Runat="server" CssClass="grid" AutoGenerateColumns="False" PageSize="500" AllowPaging="False" AllowSorting="False">
	        <headerstyle CssClass="header" />
	        <itemstyle CssClass="item" />
	        <alternatingitemstyle CssClass="altItem" />
	        <footerstyle CssClass="footer" />
	        <pagerstyle CssClass="pager" Mode="NumericPages" />
	        <columns>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Field ID">
	                <ItemTemplate>
			                <%# HtmlEncodeNullable(Container.DataItem, "FieldName", string.Empty, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Display Name" ItemStyle-HorizontalAlign="Center">
	                <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "DisplayName", string.Empty, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Field Type">
	                <ItemTemplate>
			                <%# HtmlEncodeNullable(Container.DataItem, "FieldType", string.Empty, "(none)").Replace("FieldForm", "")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Data Type" ItemStyle-HorizontalAlign="Center">
	                <ItemTemplate>
			                <%# HtmlEncodeNullable(Container.DataItem, "FormFieldDataType.DisplayName", string.Empty, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Required" ItemStyle-HorizontalAlign="center">
	                <ItemTemplate>
                            <%# GetBooleanString(Container.DataItem)%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Last Reported" ItemStyle-HorizontalAlign="Center">
	                <ItemTemplate>
			                <%# HtmlEncodeNullable(Container.DataItem, "LastReportedOn", "{0:d}", DateTime.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
	        </columns>
        </asp:datagrid>
    </cc:Box>
    <% } %>
    <script type="text/javascript">
        $(document).ready(function () {
            $("[class=chkReportable]").live("click", function () {
                $(this).closest("tr").toggleClass('selectedItem');
            });
        });
    </script>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" CausesValidation="true" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:Footer ID="ucFooter" runat="server" />
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script type="text/javascript">
    function Toggle(id)
    {
	    ToggleControlDisplay(id);
	}
    </script>

    </form>
</body>
</html>
