using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web;
using QCI.Web.Validation;
using Wilson.ORMapper;

public partial class CreateSurveyLocationAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(CreateSurveyLocationAspx_Load);
        this.PreRender += new EventHandler(CreateSurveyLocationAspx_PreRender);
        this.grdPolicies.ItemDataBound += new DataGridItemEventHandler(grdPolicies_ItemDataBound);
        this.grdPolicies.SortCommand += new DataGridSortCommandEventHandler(grdPolicies_SortCommand);
        this.grdPolicies.ItemCommand += new DataGridCommandEventHandler(grdPolicies_ItemCommand);
        this.grdPolicies.ItemCreated += new DataGridItemEventHandler(grdPolicies_ItemCreated);
        this.lnkAddLob.Click += new EventHandler(lnkAddLob_Click);
        this.grdBuildings.SortCommand += new DataGridSortCommandEventHandler(grdBuildings_SortCommand);
        this.grdBuildings.ItemDataBound += new DataGridItemEventHandler(grdBuildings_ItemDataBound);
        this.grdBuildings.ItemCreated += new DataGridItemEventHandler(grdBuildings_ItemCreated);
        this.grdBuildings.ItemCommand += new DataGridCommandEventHandler(grdBuildings_ItemCommand);
        this.grdContacts.ItemDataBound += new DataGridItemEventHandler(grdContacts_ItemDataBound);
        this.grdContacts.ItemCreated += new DataGridItemEventHandler(grdContacts_ItemCreated);
        this.lnkAddBuilding.Click += new EventHandler(lnkAddBuilding_Click);
        this.lnkAddContact.Click += new EventHandler(lnkAddContact_Click);
        this.grdContacts.ItemCommand += new DataGridCommandEventHandler(grdContacts_ItemCommand);
        //this.btnCancel.Click += new EventHandler(btnCancel_Click);
        //this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.lnkBackToInsured.Click += new EventHandler(lnkBackToInsured_Click);
        this.lnkBackToInsured2.Click += new EventHandler(lnkBackToInsured2_Click);
        this.lnkBackToInsuredCancel2.Click += new EventHandler(lnkBackToInsuredCancel2_Click);
        this.lnkBackToInsuredCancel.Click += new EventHandler(lnkBackToInsuredCancel_Click);
    }
    #endregion

    protected Location _eLocation;
    protected Guid _gSurveyID;
    protected Survey _eSurvey;
    private const string sPolicySortExpression = "PolicySortExpression";
    private const string sPolicySortReversed = "PolicySortReversed";
    private const string sBuildingSortExpression = "BuildingSortExpression";
    private const string sBuildingSortReversed = "BuildingSortReversed";
    private string _hideHazardGrade;
    private string _usesBuildingITV;
    protected bool _validatePolicyEffectiveDate;
    void CreateSurveyLocationAspx_Load(object sender, EventArgs e)
    {
        // register our AJAX class for use in client script
        Ajax.Utility.RegisterTypeForAjax(typeof(AjaxMethods));
        
        Guid glocationID = ARMSUtility.GetGuidFromQueryString("locationid", true);
        _eLocation = Location.Get(glocationID);

        Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", true);

        try
        {
            _eSurvey = Survey.Get(gSurveyID);
        }
        catch
        {
            //survey id no longer exists
            Security.RequestExpired();
        }

        //Determine what is visible
        chkSameAsInsuredAddress.Attributes.Add("onclick", string.Format("SameAsInsuredAddress(this.checked, '{0}')", _eSurvey.InsuredID));

        //Hide HazardGrade
        _hideHazardGrade = Berkley.BLC.Business.Utility.GetCompanyParameter("HideHazardGrade", CurrentUser.Company).ToString().ToLower();

        _usesBuildingITV = Berkley.BLC.Business.Utility.GetCompanyParameter("UsesBuildingITV", CurrentUser.Company).ToString().ToLower();
        _validatePolicyEffectiveDate = (Berkley.BLC.Business.Utility.GetCompanyParameter("ValidatePolicyEffectiveDate", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;

        if (!this.IsPostBack)
        {
            this.PopulateFields(_eLocation);
            
            string sAddress = _eLocation.SingleLineAddress;
            base.PageHeading = string.Format("Location: ({0})", (sAddress.Length > 0) ? sAddress : _eLocation.Number.ToString());

            // restore sort expression, reverse state
            this.ViewState[sPolicySortExpression] = this.UserIdentity.GetPageSetting(sPolicySortExpression, grdPolicies.Columns[0].SortExpression);
            this.ViewState[sPolicySortReversed] = this.UserIdentity.GetPageSetting(sPolicySortReversed, false);

            this.ViewState[sBuildingSortExpression] = this.UserIdentity.GetPageSetting(sBuildingSortExpression, grdBuildings.Columns[0].SortExpression);
            this.ViewState[sBuildingSortReversed] = this.UserIdentity.GetPageSetting(sBuildingSortReversed, false);

            JavaScript.AddConfirmationNoticeToWebControl(lnkBackToInsuredCancel, "Are you sure you want to cancel updating this location?\r\rPress OK to continue, or Cancel to stay on the current page.");
            JavaScript.AddConfirmationNoticeToWebControl(lnkBackToInsuredCancel2, "Are you sure you want to cancel updating this location?\r\rPress OK to continue, or Cancel to stay on the current page.");

            //lnkTest.HRef = string.Format("test.aspx?surveyid={0}&locationid={1}&width=622px&height=316&jqmRefresh=true", _eSurvey.ID, _eLocation.ID);
            
            if (_hideHazardGrade == "true")
                grdPolicies.Columns[grdPolicies.Columns.Count - 8].Visible = false;    
     
        }

        //determine if the valuation columns are displayed
        //Modified by Vilas Meshram: 04/19/2013
        grdBuildings.Columns[11].Visible = CurrentUser.Company.SupportsBuildingValuation;
        grdBuildings.Columns[10].Visible = CurrentUser.Company.SupportsBuildingValuation;
        //grdBuildings.Columns[grdBuildings.Columns.Count - 8].Visible = CurrentUser.Company.SupportsBuildingValuation;
        //grdBuildings.Columns[grdBuildings.Columns.Count - 9].Visible = CurrentUser.Company.SupportsBuildingValuation;

        grdBuildings.Columns[2].HeaderText = CurrentUser.Company.BuildingSprinklerSystemLabel;
        grdBuildings.Columns[3].HeaderText = CurrentUser.Company.BuildingSprinklerSystemLabel + " Credit";

        //hardcoding this one for BLS
        grdBuildings.Columns[17].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
        grdBuildings.Columns[16].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
        grdBuildings.Columns[15].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
        grdBuildings.Columns[14].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
        grdBuildings.Columns[13].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
        grdBuildings.Columns[12].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
        //grdBuildings.Columns[grdBuildings.Columns.Count - 2].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
        //grdBuildings.Columns[grdBuildings.Columns.Count - 3].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
        //grdBuildings.Columns[grdBuildings.Columns.Count - 4].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
        //grdBuildings.Columns[grdBuildings.Columns.Count - 5].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
        //grdBuildings.Columns[grdBuildings.Columns.Count - 6].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
        //grdBuildings.Columns[grdBuildings.Columns.Count - 7].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);

        //Modified by Vilas Meshram: 04/19/2013

        grdBuildings.Columns[1].Visible = (CurrentUser.Company.ID != Company.BerkleyLifeSciences.ID);
        grdBuildings.Columns[2].Visible = (CurrentUser.Company.ID != Company.BerkleyLifeSciences.ID);
        grdBuildings.Columns[3].Visible = (CurrentUser.Company.ID != Company.BerkleyLifeSciences.ID);

        if (CurrentUser.Company.ID != Company.BerkleyLifeSciences.ID)
            grdBuildings.Columns[18].Visible =(_usesBuildingITV == "true") ? true : false;
        
    }

    void CreateSurveyLocationAspx_PreRender(object sender, EventArgs e)
    {
        string sSortPolicyExp = (string)this.ViewState[sPolicySortExpression];
        if ((bool)this.ViewState[sPolicySortReversed])
        {
            sSortPolicyExp = sSortPolicyExp.Replace(" DESC", " TEMP");
            sSortPolicyExp = sSortPolicyExp.Replace(" ASC", " DESC");
            sSortPolicyExp = sSortPolicyExp.Replace(" TEMP", " ASC");
        }

        string sSortBuildingExp = (string)this.ViewState[sBuildingSortExpression];
        if ((bool)this.ViewState[sBuildingSortReversed])
        {
            sSortBuildingExp = sSortBuildingExp.Replace(" DESC", " TEMP");
            sSortBuildingExp = sSortBuildingExp.Replace(" ASC", " DESC");
            sSortBuildingExp = sSortBuildingExp.Replace(" TEMP", " ASC");
        }
        
        // get all the policies for this location
        Policy[] ePolicies = Policy.GetSortedArray("InsuredID = ? && IsActive = true", sSortPolicyExp, _eSurvey.InsuredID);
        DataGridHelper.BindGrid(grdPolicies, ePolicies, "ID", "There are no policies for this location");

        // get all the buildings for this location
        Building[] eBuildings = Building.GetSortedArray("LocationID = ?", sSortBuildingExp, _eLocation.ID);
        DataGridHelper.BindGrid(grdBuildings, eBuildings, "ID", "There are no buildings for this location.");
       
        // get all the contacts for this location
        List<LocationContact> eContacts = new List<LocationContact>();
        eContacts.AddRange(LocationContact.GetSortedArray("LocationID = ?", "PriorityIndex", _eLocation.ID));
        
        //check if a default contact needs to be added
        if (!String.IsNullOrEmpty(CurrentUser.Company.DefaultInsuredContact) && eContacts.Count == 0)
        {
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //Add the contact to this location first
                LocationContact contact = new LocationContact(Guid.NewGuid());
                contact.LocationID = _eLocation.ID;
                contact.PriorityIndex = 1;
                contact.Name = CurrentUser.Company.DefaultInsuredContact;
                contact.Save(trans);

                eContacts.Add(contact);
                
                //Now add a primary contact to all other locations
                Location[] eLocations = Location.GetArray("ID != ? && InsuredID = ? && IsActive = true", _eLocation.ID, _eSurvey.InsuredID);
                foreach (Location eLocation in eLocations)
                {
                    LocationContact newContact = new LocationContact(Guid.NewGuid());
                    newContact.LocationID = eLocation.ID;
                    newContact.PriorityIndex = 1;
                    newContact.Name = CurrentUser.Company.DefaultInsuredContact;
                    newContact.Save(trans);
                }

                trans.Commit();
            }
        }

        DataGridHelper.BindGrid(grdContacts, eContacts.ToArray(), "ID", "There are no contacts for this location.");

        // get the comments for this location
        SurveyLocationNote eNote = SurveyLocationNote.GetOne("SurveyID = ? && LocationID = ?", _eSurvey.ID, _eLocation.ID);
        txtComments.Text = (eNote != null) ? eNote.Comment : string.Empty;
    }

    #region Policy Section

    void grdPolicies_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the LocationPolicy from the policy
            Policy ePolicy = (Policy)e.Item.DataItem;

            //CheckBox
            CheckBox chk = (CheckBox)e.Item.FindControl("chkPolicy");
            SurveyLocationPolicyExclusion surveyLocationPolicy = SurveyLocationPolicyExclusion.Get(_eSurvey.ID, _eLocation.ID, ePolicy.ID);

            //Add attribute as an id for retrieving via oncheckedchange event
            chk.Attributes.Add("PolicyID", ePolicy.ID.ToString());

            //Determine which status image to display
            HtmlImage img = (HtmlImage)e.Item.FindControl("imgReportStatus");
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkReportStatus");
            lnk.Attributes.Add("PolicyID", ePolicy.ID.ToString());

            //LinkButton for the policy details
            LinkButton lnkPolicyDetails = (LinkButton)e.Item.FindControl("lnkPolicyDetails");
            lnkPolicyDetails.Attributes.Add("PolicyID", ePolicy.ID.ToString());

            if (surveyLocationPolicy.PolicyExcluded)
            {
                chk.Checked = false;
                img.Src = ARMSUtility.AppPath + "/images/dash.gif";
                lnk.Attributes.Add("flagHref", "1");
            }
            else
            {
                chk.Checked = true;
                if (surveyLocationPolicy.PolicyViewed)
                {
                    img.Src = ARMSUtility.AppPath + "/images/check.gif";
                }
                else
                {
                    img.Alt = "Must select a report type for each coverage for this policy.";
                    img.Src = ARMSUtility.AppPath + "/images/xmark.gif";
                }
            }

            chk.Attributes.Add("onclick", string.Format("SetExclusion(this.checked, '{0}', '{1}', '{2}', '{3}', '{4}', '{5}','{6}');",
                _eSurvey.ID.ToString(), _eLocation.ID.ToString(), ePolicy.EffectiveDate,
                this.CurrentUser.CompanyID.ToString(), lnk.ClientID, img.ClientID,chk.ClientID));
        }
    }

    void grdPolicies_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Event Handler for linkbutton
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkReportStatus");
            lnk.Click += new EventHandler(lnkReportStatus_Click);

            //Event Handler for LinkButton
            LinkButton lnkPolicyDetails = (LinkButton)e.Item.FindControl("lnkPolicyDetails");
            lnkPolicyDetails.Click += new EventHandler(lnkPolicyDetails_Click);
        }
    }

    void lnkPolicyDetails_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        Guid gPolicyID = new Guid(lnk.Attributes["PolicyID"]);

        if (SaveLocation())
        {
            Response.Redirect(UrlBuilder("CreateSurveyPolicy.aspx", "policyid", gPolicyID), true);
        }
    }

    void lnkReportStatus_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        Guid gPolicyID = new Guid(lnk.Attributes["PolicyID"]);

        //commit changes
        if (SaveLocation())
        {
            Response.Redirect(UrlBuilder("CreateSurveyCoverage.aspx", "policyid", gPolicyID), true);
        }
    }

    void grdPolicies_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sSortExp = (string)this.ViewState[sPolicySortExpression];
        bool sSortRev = (bool)this.ViewState[sPolicySortReversed];
        sSortRev = !sSortRev && (sSortExp == e.SortExpression);

        this.ViewState[sPolicySortExpression] = e.SortExpression;
        this.ViewState[sPolicySortReversed] = sSortRev;

        this.UserIdentity.SavePageSetting(sPolicySortExpression, e.SortExpression);
        this.UserIdentity.SavePageSetting(sPolicySortReversed, sSortRev);
    }

    void lnkAddLob_Click(object sender, EventArgs e)
    {
        if (SaveLocation())
        {
            Response.Redirect(UrlBuilder("CreateSurveyPolicy.aspx"), true);
        }
    }

    protected void btnRemovePolicy_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "WARNING: This action will remove this policy for all the locations of this survey request. Are you sure you want to remove this policy?");
    }

    void grdPolicies_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            Guid gPolicyID = (Guid)grdPolicies.DataKeys[e.Item.ItemIndex];

            //Check if this is the primary policy of the survey
            if (_eSurvey.PrimaryPolicyID == gPolicyID)
            {
                JavaScript.ShowMessage(this, "The primary policy of the survey cannot be removed.  You may choose to not include the policy by selecting the apppropriate checkbox.");
                return;
            }

            //Call an SP to do the persistence
            DB.Engine.ExecuteScalar(string.Format("EXEC CreateSurvey_DeletePolicy '{0}', '{1}'", _eSurvey.ID, gPolicyID));
        }

        Response.Redirect(UrlBuilder("CreateSurveyLocation.aspx"), true);
    }

    #endregion

    #region Building Section

    void grdBuildings_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the LocationPolicy from the policy
            Building eBuilding = (Building)e.Item.DataItem;

            //LinkButton
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkBuildingDetails");

            //Add attribute as an id for retrieving via oncheckedchange event
            lnk.Attributes.Add("BuildingID", eBuilding.ID.ToString());
        }
    }

    void grdBuildings_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Event Handler for LinkButton
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkBuildingDetails");
            lnk.Click += new EventHandler(lnkBuildingDetails_Click);
        }
    }

    void lnkBuildingDetails_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        Guid gBuildingID = new Guid(lnk.Attributes["BuildingID"]);

        if (SaveLocation())
        {
            Response.Redirect(UrlBuilder("CreateSurveyBuilding.aspx", "buildingid", gBuildingID), true);
        }
    }

    void lnkAddBuilding_Click(object sender, EventArgs e)
    {
        if (SaveLocation())
        {
            Response.Redirect(UrlBuilder("CreateSurveyBuilding.aspx"), true);
        }
    }

    protected void btnRemoveBuilding_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this building?");
    }

    void grdBuildings_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            Guid gBuildingID = (Guid)grdBuildings.DataKeys[e.Item.ItemIndex];
            Building eBuilding = Building.Get(gBuildingID);

            // execute the requested transformation on the buildings
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                eBuilding.Delete(trans);
                trans.Commit();
            }
        }

        Response.Redirect(UrlBuilder("CreateSurveyLocation.aspx"), true);
    }

    void grdBuildings_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sSortExp = (string)this.ViewState[sBuildingSortExpression];
        bool sSortRev = (bool)this.ViewState[sBuildingSortReversed];
        sSortRev = !sSortRev && (sSortExp == e.SortExpression);

        this.ViewState[sBuildingSortExpression] = e.SortExpression;
        this.ViewState[sBuildingSortReversed] = sSortRev;

        this.UserIdentity.SavePageSetting(sBuildingSortExpression, e.SortExpression);
        this.UserIdentity.SavePageSetting(sBuildingSortReversed, sSortRev);
    }

    #endregion

    #region Contact Section

    void grdContacts_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the LocationPolicy from the policy
            LocationContact eContact = (LocationContact)e.Item.DataItem;

            //LinkButton
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkContactName");

            //Add attribute as an id for retrieving via oncheckedchange event
            lnk.Attributes.Add("ContactID", eContact.ID.ToString());
        }
    }

    void grdContacts_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Event Handler for LinkButton
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkContactName");
            lnk.Click += new EventHandler(lnkContactName_Click);
        }
    }

    void lnkContactName_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        Guid gContactID = new Guid(lnk.Attributes["ContactID"]);

        if (SaveLocation())
        {
            Response.Redirect(UrlBuilder("Contact.aspx", "contactid", gContactID), true);
        }
    }

    void lnkAddContact_Click(object sender, EventArgs e)
    {
        if (SaveLocation())
        {
            Response.Redirect(UrlBuilder("Contact.aspx"), true);
        }
    }

    protected void btnRemoveContact_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this contact?");
    }

    void grdContacts_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid gContactID = (Guid)grdContacts.DataKeys[e.Item.ItemIndex];

        // get the current contacts from the DB and find the index of the primary contact
        LocationContact[] eContacts = LocationContact.GetSortedArray("LocationID = ?", "PriorityIndex ASC", _eLocation.ID);
        int iContactIndex = int.MinValue;
        for (int i = 0; i < eContacts.Length; i++)
        {
            if (eContacts[i].ID == gContactID)
            {
                iContactIndex = i;
                break;
            }
        }
        if (iContactIndex == int.MinValue) // not found
        {
            return;
        }

        LocationContact contact = eContacts[iContactIndex];

        // execute the requested transformation on the contacts
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            ArrayList contactList = new ArrayList(eContacts);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEUP":
                    {
                        contactList.RemoveAt(iContactIndex);
                        if (iContactIndex > 0)
                        {
                            contactList.Insert(iContactIndex - 1, contact);
                        }
                        else // top item (move to bottom)
                        {
                            contactList.Add(contact);
                        }
                        break;
                    }
                case "MOVEDOWN":
                    {
                        contactList.RemoveAt(iContactIndex);
                        if (iContactIndex < eContacts.Length - 1)
                        {
                            contactList.Insert(iContactIndex + 1, contact);
                        }
                        else // bottom item (move to top)
                        {
                            contactList.Insert(0, contact);
                        }
                        break;
                    }
                case "REMOVE":
                    {
                        contactList.RemoveAt(iContactIndex);
                        eContacts[iContactIndex].Delete(trans);
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the contacts still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < contactList.Count; i++)
            {
                LocationContact eContactUpdate = (contactList[i] as LocationContact).GetWritableInstance();
                eContactUpdate.PriorityIndex = (i + 1);
                eContactUpdate.Save(trans);
            }

            trans.Commit();
        }
    }

    #endregion

    private string  ValidatePolicies()
    {
        Policy[] includedPolicies = Policy.GetArray("SurveyLocationPolicyCoverageNames[SurveyID = ? && LocationID = ?]", _eSurvey.ID, _eLocation.ID);
        Policy[] excludedPolicies = Policy.GetArray("SurveyLocationPolicyExclusions[SurveyID = ? && LocationID = ? && (LocationExcluded = true || PolicyExcluded = true)]", _eSurvey.ID, _eLocation.ID);

        List<String> excludedPolicyNumbers = new List<String>();
        foreach (Policy excludedPolicy in excludedPolicies)
        {
            excludedPolicyNumbers.Add(excludedPolicy.Number);
        }

        // build up a validation message if not all the required fields are entered
        string strBuilder = string.Empty;
        for (int i = 0; i < includedPolicies.Length; i++)
        {
            Policy policy = includedPolicies[i];

            if (!excludedPolicyNumbers.Contains(policy.Number))
            {
                if (policy.EffectiveDate == DateTime.MinValue || policy.ExpireDate == DateTime.MinValue ||
                    (CurrentUser.Company.RequirePolicyPremium && policy.Premium == Decimal.MinValue) ||
                    ((policy.HazardGrade == string.Empty || policy.HazardGrade.ToLower() == "none" || policy.HazardGrade.ToLower() == "0") && 
                      !CurrentUser.Company.NonRequiredLOBHazardGrades.Contains(policy.LineOfBusinessCode) && _hideHazardGrade != "true"))
                {
                    if (i >= 1)
                    {
                        strBuilder += "\n";
                    }

                    if (policy.Number != string.Empty)
                    {
                        strBuilder += string.Format("The following fields must be entered for policy #{0}:\n", policy.Number);
                    }
                    else
                    {
                        strBuilder += string.Format("The following fields must be entered for the {0} policy:\n", policy.LineOfBusiness.Name);
                    }

                    if (policy.EffectiveDate == DateTime.MinValue)
                    {
                        strBuilder += "- The effective date.\n";
                    }

                    if (policy.ExpireDate == DateTime.MinValue)
                    {
                        strBuilder += "- The expiration date.\n";
                    }

                    if (CurrentUser.Company.RequirePolicyPremium && policy.Premium == Decimal.MinValue)
                    {
                        strBuilder += "- The premium amount.\n";
                    }

                    if ((policy.HazardGrade == string.Empty || policy.HazardGrade.ToLower() == "none" || policy.HazardGrade.ToLower() == "0") &&
                        !CurrentUser.Company.NonRequiredLOBHazardGrades.Contains(policy.LineOfBusinessCode))
                    {
                        strBuilder += "- The hazard grade.\n";
                    }

                }
            }
        }

        return (strBuilder.Length > 0) ? strBuilder : string.Empty;
    }

    void lnkBackToInsured_Click(object sender, EventArgs e)
    {
        if (SaveLocation())
        {
            string msg = ValidatePolicies();
            if (msg.Length > 0)
            {
                JavaScript.ShowMessage(this, msg);
                return;
            }

            Response.Redirect(UrlBuilder("CreateSurvey.aspx"), true);
        }
    }

    void lnkBackToInsured2_Click(object sender, EventArgs e)
    {
        if (SaveLocation())
        {
            string msg = ValidatePolicies();
            if (msg.Length > 0)
            {
                JavaScript.ShowMessage(this, msg);
                return;
            }

            Response.Redirect(UrlBuilder("CreateSurvey.aspx"), true);
        }
    }

    void lnkBackToInsuredCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(UrlBuilder("CreateSurvey.aspx"), true);
    }

    void lnkBackToInsuredCancel2_Click(object sender, EventArgs e)
    {
        Response.Redirect(UrlBuilder("CreateSurvey.aspx"), true);
    }

    protected string UrlBuilder(string sWebPage)
    {
        return UrlBuilder(sWebPage, null, null);
    }

    protected string UrlBuilder(string sWebPage, string sKey, object oValue)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat(sWebPage);
        sb.AppendFormat("?surveyid={0}", _eSurvey.ID);
        sb.AppendFormat("&locationid={0}", _eLocation.ID);

        if (sKey != null && sKey.Length > 0)
        {
            sb.AppendFormat("&{0}={1}", sKey, oValue);
        }

        return sb.ToString();
    }

    protected string GetBoolLabel(string value)
    {
        Int16 i = Convert.ToInt16(value);
        string result;

        if (i == 0)
            result = "No";
        else if (i == 1)
            result = "Yes";
        else
            result = "(none)";

        return result;
    }

    private bool SaveLocation()
    {
        //Add or Update comments
        SurveyLocationNote eNote = SurveyLocationNote.GetOne("SurveyID = ? && LocationID = ?", _eSurvey.ID, _eLocation.ID);
        if (txtComments.Text.Length > 0 || eNote != null)
        {
            if (eNote == null) //Insert
            {
                eNote = new SurveyLocationNote(_eSurvey.ID, _eLocation.ID);
                eNote.UserID = (!this.CurrentUser.IsUnderwriter) ? this.CurrentUser.ID : Guid.Empty;
                eNote.UserName = (this.CurrentUser.IsUnderwriter && UserIdentity != null) ? UserIdentity.Name : string.Empty;
            }
            else //Update
            {
                eNote = eNote.GetWritableInstance();
            }

            eNote.EntryDate = DateTime.Now;
            eNote.Comment = txtComments.Text;
            eNote.Save();
        }
        
        Location eLocation;
        try
        {
            eLocation = _eLocation.GetWritableInstance();

            //save location data
            this.PopulateEntity(eLocation);
            eLocation.IsActive = true;
            eLocation.Save();
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return false;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return false;
        }

        //update our member var
        _eLocation = eLocation;

        return true;
    }

    //void btnSubmit_Click(object sender, EventArgs e)
    //{
    //    if (SaveLocation())
    //    {
    //        Response.Redirect(UrlBuilder("CreateSurvey.aspx"), true);
    //    }
    //}

    //void btnCancel_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect(UrlBuilder("CreateSurvey.aspx"), true);
    //}

    public class AjaxMethods
    {
        [Ajax.AjaxMethod()]
        public static string ExclusionCheck(bool isChecked,  string surveyID, string locationID, string policyID, string companyID)
        {
            string imgSrc = string.Empty;
            
            SurveyLocationPolicyExclusion surveyLocationPolicy = SurveyLocationPolicyExclusion.GetOne("SurveyID = ? && LocationID = ? && PolicyID = ?", surveyID, locationID, policyID).GetWritableInstance();
            if (isChecked)
            {
                surveyLocationPolicy.PolicyExcluded = false;
                imgSrc = (surveyLocationPolicy.PolicyViewed) ? ARMSUtility.AppPath + "/images/check.gif" : ARMSUtility.AppPath + "/images/xmark.gif";
            }
            else
            {
                surveyLocationPolicy.PolicyExcluded = true;
                imgSrc = ARMSUtility.AppPath + "/images/dash.gif";
            }

            surveyLocationPolicy.Save();

            return imgSrc;
        }

        [Ajax.AjaxMethod()]
        public static string[] SameAsInsuredAddress(bool isChecked, string insuredID)
        {
            string[] arrResult = new string[5];

            if (isChecked)
            {
                Insured eInsured = Insured.Get(new Guid(insuredID));
                arrResult[0] = eInsured.StreetLine1;
                arrResult[1] = eInsured.StreetLine2;
                arrResult[2] = eInsured.City;
                arrResult[3] = eInsured.StateCode;
                arrResult[4] = eInsured.ZipCode;
            }
            else
            {
                for (int i = 0; i < arrResult.Length; i++)
                {
                    arrResult[i] = string.Empty;
                }
            }

            return arrResult;
        }
    }
}
