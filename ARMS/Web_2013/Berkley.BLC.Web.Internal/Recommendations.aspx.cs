using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Web.Internal;
using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using QCI.Web.Validation;
using Wilson.ORMapper;

public partial class RecommendationsAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(RecommendationsAspx_Load);
        this.PreRender += new EventHandler(RecommendationsAspx_PreRender);
        this.grdRecs.ItemCommand += new DataGridCommandEventHandler(grdRecs_ItemCommand);
        this.grdRecs.ItemCreated += new DataGridItemEventHandler(grdRecs_ItemCreated);
        this.grdOldRecs.ItemCommand += new DataGridCommandEventHandler(grdOldRecs_ItemCommand);
        this.cboFilter.SelectedIndexChanged += new EventHandler(cboFilter_SelectedIndexChanged);
    }
    #endregion

    protected Survey _eSurvey;
    protected string _defaultRecFilter;

    void RecommendationsAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        _eSurvey = Survey.Get(id);

        // Default Rec Filter
        _defaultRecFilter = Berkley.BLC.Business.Utility.GetCompanyParameter("DefaultRecFilter", CurrentUser.Company).ToLower();

        //determine if this company utilizes rec classifications
        foreach (DataGridColumn column in grdRecs.Columns)
        {
            if (column.HeaderText == "Classification")
            {
                column.Visible = (RecClassification.GetOne("CompanyID = ?", CurrentUser.CompanyID) != null);
            }
        }

        if (!this.IsPostBack)
        {
            //Modified by Vilas Meshram: 05/09/2013 : Squish# 21323 : Default filer
            if (_defaultRecFilter == "all")
                cboFilter.SelectedValue = string.Empty;
            else
                cboFilter.SelectedValue = (string)this.UserIdentity.GetPageSetting(string.Format("{0}Filter", _eSurvey.ID), "O");
        }
    }

    void RecommendationsAspx_PreRender(object sender, EventArgs e)
    {
        SurveyRecommendation[] eRecs = SurveyRecommendation.GetSortedArray("SurveyID = ?", "PriorityIndex ASC", _eSurvey.ID);
        DataGridHelper.BindGrid(grdRecs, eRecs, "ID", "There are no recs for this survey.");

        SelectProcedure sp = new SelectProcedure(typeof(SurveyRecommendation), "Recs_GetPrior");
        sp.AddParameter("@SurveyID", _eSurvey.ID);
        sp.AddParameter("@CompanyID", CurrentUser.CompanyID);
        sp.AddParameter("@SurveyNumber", _eSurvey.Number);
        sp.AddParameter("@ClientID", _eSurvey.Insured.ClientID);
        sp.AddParameter("@RecStatus", cboFilter.SelectedValue);

        IList list = DB.Engine.GetObjectSet(sp);
        Array result = Array.CreateInstance(sp.ObjectType, list.Count);
        list.CopyTo(result, 0);
        SurveyRecommendation[] oldRecs = result as SurveyRecommendation[];

        DataGridHelper.BindGrid(grdOldRecs, oldRecs, "ID", "There are no previous recs for this client/location(s).");
    }

    void grdRecs_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (!String.IsNullOrEmpty(e.CommandName))
        {
            Guid gRecID = (Guid)grdRecs.DataKeys[e.Item.ItemIndex];

            // get the current recs from the DB and find the index of the primary rec
            SurveyRecommendation[] eRecs = SurveyRecommendation.GetSortedArray(_eSurvey.GetAllSurveysForAccount, "PriorityIndex ASC");
            int iRecIndex = int.MinValue;
            for (int i = 0; i < eRecs.Length; i++)
            {
                if (eRecs[i].ID == gRecID)
                {
                    iRecIndex = i;
                    break;
                }
            }
            if (iRecIndex == int.MinValue) // not found
            {
                return;
            }

            SurveyRecommendation rec = eRecs[iRecIndex];

            // execute the requested transformation on the recs
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                ArrayList recList = new ArrayList(eRecs);
                switch (e.CommandName.ToUpper())
                {
                    case "MOVEUP":
                        {
                            recList.RemoveAt(iRecIndex);
                            if (iRecIndex > 0)
                            {
                                recList.Insert(iRecIndex - 1, rec);
                            }
                            else // top item (move to bottom)
                            {
                                recList.Add(rec);
                            }
                            break;
                        }
                    case "MOVEDOWN":
                        {
                            recList.RemoveAt(iRecIndex);
                            if (iRecIndex < eRecs.Length - 1)
                            {
                                recList.Insert(iRecIndex + 1, rec);
                            }
                            else // bottom item (move to top)
                            {
                                recList.Insert(0, rec);
                            }
                            break;
                        }
                    case "REMOVE":
                        {
                            recList.RemoveAt(iRecIndex);
                            eRecs[iRecIndex].Delete(trans);

                            //Update the survey history
                            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
                            manager.RecManagement(eRecs[iRecIndex], SurveyManager.UIAction.Remove);

                            break;
                        }
                    default:
                        {
                            throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                        }
                }

                // save all the recs still in our list
                // note: we are updated the priority number as we save to make sure they are always sequential from 1
                for (int i = 0; i < recList.Count; i++)
                {
                    SurveyRecommendation eRecUpdate = (recList[i] as SurveyRecommendation).GetWritableInstance();
                    eRecUpdate.PriorityIndex = (i + 1);
                    eRecUpdate.Save(trans);
                }

                trans.Commit();
            }
        }
    }

    void cboFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.UserIdentity.SavePageSetting(string.Format("{0}Filter", _eSurvey.ID), cboFilter.SelectedValue);
    }

    void grdRecs_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (!this.IsPostBack)
            {
                //determine if the user has permission to remove recs
                LinkButton lnk = (LinkButton)e.Item.FindControl("btnRemoveRec");
                lnk.Enabled = CurrentUser.Permissions.CanTakeAction(SurveyActionType.RemoveRec, _eSurvey, CurrentUser);
                if (lnk.Enabled)
                {
                    JavaScript.AddConfirmationNoticeToWebControl(lnk, "Are you sure you want to remove this recommendation?");
                }

                LinkButton btnUp = (LinkButton)e.Item.FindControl("btnUp");
                btnUp.Enabled = !CurrentUser.IsUnderwriter;

                LinkButton btnDown = (LinkButton)e.Item.FindControl("btnDown");
                btnDown.Enabled = !CurrentUser.IsUnderwriter;
            }
        }
    }

    void grdOldRecs_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid gRecID = (Guid)grdOldRecs.DataKeys[e.Item.ItemIndex];
        SurveyRecommendation oldRec = SurveyRecommendation.Get(gRecID).GetWritableInstance();

        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            //Bring the old to the new
            SurveyRecommendation newRec = new SurveyRecommendation(Guid.NewGuid());
            newRec.SurveyID = _eSurvey.ID;
            newRec.RecommendationID = oldRec.RecommendationID;
            newRec.RecommendationNumber = oldRec.RecommendationNumber;
            newRec.EntryText = oldRec.EntryText;
            newRec.Comments = oldRec.Comments;
            newRec.DateCreated = oldRec.DateCreated;
            newRec.DateCompleted = DateTime.MinValue;
            newRec.RecClassificationID = oldRec.RecClassificationID;
            newRec.RecStatusID = RecStatus.GetOne("CompanyID = ? && TypeCode = ?", CurrentUser.CompanyID, RecStatusType.Open.Code).ID;

            SurveyRecommendation[] eRecs = SurveyRecommendation.GetSortedArray("SurveyID = ?", "PriorityIndex DESC", _eSurvey.ID);
            newRec.PriorityIndex = (eRecs.Length > 0 ? eRecs[0].PriorityIndex + 1 : 1);

            newRec.Save(trans);

            oldRec.RecStatusID = RecStatus.GetOne("CompanyID = ? && TypeCode = ?", CurrentUser.CompanyID, RecStatusType.Completed.Code).ID;
            oldRec.DateCompleted = (oldRec.DateCompleted == DateTime.MinValue) ? DateTime.Today : oldRec.DateCompleted;
            oldRec.Comments += string.Format(" {0} moved rec {1} from this survey to the current survey #{2}.", CurrentUser.Name, oldRec.RecommendationNumber, newRec.Survey.Number);
            oldRec.Save(trans);

            //Add history entry for the old survey
            SurveyHistory history1 = new SurveyHistory(Guid.NewGuid());
            history1.SurveyID = oldRec.SurveyID;
            history1.EntryDate = DateTime.Now;
            history1.EntryText = string.Format("{0} copied rec {1} from this survey to the current survey #{2}.", CurrentUser.Name, oldRec.RecommendationNumber, newRec.Survey.Number);
            history1.Save(trans);

            //Add history entry for the new survey
            SurveyHistory history2 = new SurveyHistory(Guid.NewGuid());
            history2.SurveyID = _eSurvey.ID;
            history2.EntryDate = DateTime.Now;
            history2.EntryText = string.Format("{0} added rec {1} from prior survey #{2}.", CurrentUser.Name, oldRec.RecommendationNumber, oldRec.Survey.Number);
            history2.Save(trans);

            trans.Commit();
        }

    }

    protected string GetLocationNumbers(object obj)
    {
        Survey survey = (obj as Survey);

        Location[] locations = Location.GetSortedArray("SurveyPolicyCoverages.SurveyID = ?", "Number ASC", survey.ID);

        string result = "(none)";

        if (locations.Length > 0)
        {
            result = string.Empty;
            foreach (Location location in locations)
            {
                result += location.Number + ", ";
            }

            //trim off the last comma
            result = result.Trim().TrimEnd(',');
        }

        return result;
    }
}
