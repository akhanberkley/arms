<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminAssignmentRuleEdit.aspx.cs" Inherits="AdminAssignmentRuleEditAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="FilterEditor" Src="~/Controls/FilterEditor.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <cc:Box id="boxEditor" runat="server" title="Filter Criteria">
    <table class="fields">
    <uc:FilterEditor id="ucFilterEditor" runat="server" />
    <tr>
	    <td colspan="5">
		    then the survey&nbsp;
		    <asp:DropDownList ID="cboMethod" Runat="server" CssClass="cbo" />
		    <asp:RequiredFieldValidator ID="valMethod" Runat="server" CssClass="val" ControlToValidate="cboMethod" Display="Dynamic" Text="*" ErrorMessage="An action must be selected." />
	    </td>
    </tr>
    </table>
    </cc:Box>

    <asp:Button id="btnSave" Text="Save" CssClass="btn" Runat="Server" />
    <asp:Button id="btnCancel" Text="Cancel" CssClass="btn" Runat="Server" CausesValidation="False" />

    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
