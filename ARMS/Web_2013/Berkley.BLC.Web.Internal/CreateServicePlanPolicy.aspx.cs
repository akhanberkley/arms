﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

using Berkley.BLC.Entities;
using Berkley.BLC.Import;
using Berkley.BLC.Business;
using QCI.Web;
using QCI.Web.Validation;
using Wilson.ORMapper;

public partial class CreateServicePlanPolicyAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(CreateServicePlanPolicyAspx_Load);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        //this.btnSubmitNonProspect.Click += new EventHandler(btnSubmitNonProspect_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        //this.btnAddAnother.Click += new EventHandler(btnAddAnother_Click);
    }
    #endregion

    #region Enum
    /// <summary>
    /// Enum for action.
    /// </summary>
    public enum Action
    {
        Add,
        Edit,
        ReadOnly
    }
    #endregion

    //protected Location _eLocation;
    protected Guid gPlanID;
    protected ServicePlan _eServicePlan;
    //protected Policy _ePolicy;
    //protected bool _isExistingSurvey = false;
    protected Action _action = Action.Add;
    protected string _hideHazardGrade;
    protected bool _requiresCarrier;
    protected bool _isDefaultEffDate;

    void CreateServicePlanPolicyAspx_Load(object sender, EventArgs e)
    {
        // register our AJAX class for use in client script
        Ajax.Utility.RegisterTypeForAjax(typeof(AjaxMethods));

        gPlanID = ARMSUtility.GetGuidFromQueryString("planid", false);
        _eServicePlan = (gPlanID != Guid.Empty) ? ServicePlan.Get(gPlanID) : null;

        _hideHazardGrade = Berkley.BLC.Business.Utility.GetCompanyParameter("HideHazardGrade", CurrentUser.Company).ToString().ToLower();
        _requiresCarrier = (Berkley.BLC.Business.Utility.GetCompanyParameter("RequiresCarrier", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _isDefaultEffDate = (Berkley.BLC.Business.Utility.GetCompanyParameter("IsDefaultEffDate", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        
        cboCompanyCarrier.IsRequired = _requiresCarrier;

        if (!this.IsPostBack)
        {
            btnAddAnother.Visible = false;
            
            // populate the company specific profit centers
            ProfitCenter[] eProfitCenters = ProfitCenter.GetSortedArray("CompanyID = ?", "Name ASC", CurrentUser.CompanyID);
            cboProfitCenter.DataBind(eProfitCenters, "Code", "Name");

            CompanyCarrier[] companyCarrier = CompanyCarrier.GetSortedArray("CompanyID = ?", "Name ASC", CurrentUser.CompanyID);
            cboCompanyCarrier.DataBind(companyCarrier, "Name", "Name");

                   // populate the seleciton list of the lobs
            Berkley.BLC.Entities.LineOfBusiness[] lobs = Berkley.BLC.Entities.LineOfBusiness.GetSortedArray("CompanyLineOfBusinesses.CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            cboLOB.DataBind(lobs, "Code", "Name");            
            txtPremium.IsRequired = CurrentUser.Company.RequirePolicyPremium;

            Policy[] ePolicies = Policy.GetSortedArray("InsuredID = ? && IsActive = true", "Number ASC", _eServicePlan.InsuredID);
            if (ePolicies[0] != null && _isDefaultEffDate)
            { 
                dtEffectiveDate.Date = ePolicies[0].EffectiveDate;
            }

            if (_hideHazardGrade != "true")
                txtHazardGrade.IsRequired = CurrentUser.Company.NonRequiredLOBHazardGrades.Length <= 10;
            txtBranchCode.Name = CurrentUser.Company.PolicyBranchCodeLabel;
            lblBranchCode.Name = CurrentUser.Company.PolicyBranchCodeLabel;

        }

        /*
        // register our AJAX class for use in client script
        Ajax.Utility.RegisterTypeForAjax(typeof(AjaxMethods));
        
        Guid gPolicyID = ARMSUtility.GetGuidFromQueryString("policyid", false);
        _ePolicy = (gPolicyID != Guid.Empty) ? Policy.Get(gPolicyID) : null;

        //navigate back
        Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        try
        {
            _eSurvey = Survey.Get(gSurveyID);
        }
        catch
        {
            //survey id no longer exists
            Security.RequestExpired();
        }
        _isExistingSurvey = (_eSurvey.Location != null && _eSurvey.CreateState != CreateState.InProgress);

        Guid gLocationID = ARMSUtility.GetGuidFromQueryString("locationid", false);
        _eLocation = (gLocationID != Guid.Empty) ? Location.Get(gLocationID) : null;

        //Hide HazardGrade
        _hideHazardGrade = Berkley.BLC.Business.Utility.GetCompanyParameter("HideHazardGrade", CurrentUser.Company).ToString().ToLower();

        if (!this.IsPostBack)
        {
            //populate the company specific profit centers
            ProfitCenter[] eProfitCenters = ProfitCenter.GetSortedArray("CompanyID = ?", "Name ASC", CurrentUser.CompanyID);
            cboProfitCenter.DataBind(eProfitCenters, "Code", "Name");

            //Hide HazardGrade
            _hideHazardGrade = Berkley.BLC.Business.Utility.GetCompanyParameter("HideHazardGrade", CurrentUser.Company).ToString().ToLower();
             
            
            //Determine the action
            _action = DetermineAction();

            txtPremium.IsRequired = CurrentUser.Company.RequirePolicyPremium;
            if (_hideHazardGrade != "true")
                txtHazardGrade.IsRequired = CurrentUser.Company.NonRequiredLOBHazardGrades.Length <= 10;
            txtBranchCode.Name = CurrentUser.Company.PolicyBranchCodeLabel;
            lblBranchCode.Name = CurrentUser.Company.PolicyBranchCodeLabel;

            if (_ePolicy != null)
            {
                if (cboProfitCenter.Items.FindByValue(_ePolicy.ProfitCenter) == null)
                {
                    cboProfitCenter.Items.Add(new ListItem(_ePolicy.ProfitCenter, _ePolicy.ProfitCenter));
                }

                //Now populate the combo with the entered lob
                cboLOB.Items.Add(new ListItem(_ePolicy.LineOfBusiness.Name, _ePolicy.LineOfBusinessCode));
                cboLOB.Enabled = false;
                txtPolicyNumber.Enabled = false;
                //numPolicyMod.Enabled = false;

                this.PopulateFields("txt", _ePolicy);
                this.PopulateFields("lbl", _ePolicy);
                this.PopulateFields("lbl", _ePolicy.LineOfBusiness);
                txtPremium.Text = (_ePolicy.Premium != Decimal.MinValue) ? _ePolicy.Premium.ToString("N0") : string.Empty;
                lblProfitCenter.Text = (_ePolicy.ProfitCenter != string.Empty) ? _ePolicy.ProfitCenter : "(not specified)";

                base.PageHeading = _ePolicy.LineOfBusiness.Name;
            }
            else
            {
                // get the lobs
                Berkley.BLC.Entities.LineOfBusiness[] lobs = Berkley.BLC.Entities.LineOfBusiness.GetSortedArray("CompanyLineOfBusinesses.CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
                cboLOB.DataBind(lobs, "Code", "Name");

                base.PageHeading = "Add Line of Business";
            }

            btnSubmit.Text = (_ePolicy == null) ? "Add" : "Update";
            btnAddAnother.Visible = (_ePolicy == null);
        } 
        */
    }

    /*
    void btnSubmitNonProspect_Click(object sender, EventArgs e)
    {
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            Policy ePolicy = _ePolicy.GetWritableInstance();

            if (_hideHazardGrade != "true")
                ePolicy.HazardGrade = txtHazardGrade.Text;

            ePolicy.Save(trans);

            //Commit
            trans.Commit();
        }

        Response.Redirect(GetReturnURL(), true);
    }
    */

    void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                try
                {
                    Convert.ToDecimal(txtPremium.Text);
                }
                catch
                {
                    if (CurrentUser.Company.RequirePolicyPremium)
                    {
                        throw new FieldValidationException("Estimated Premium is not a valid currency value.");
                    }
                    else
                    {
                        txtPremium.Text = "0";
                    }
                }

                // LOGIC
                Policy ePolicy = new Policy(Guid.NewGuid());

                ePolicy.InsuredID = _eServicePlan.InsuredID;
                this.PopulateEntity("txt", ePolicy);
                ePolicy.IsActive = true;

                ePolicy.Save(trans);

                //stub out the coverages for the newly created lob
                ImportFactory oFactory = new ImportFactory(this.CurrentUser.Company);
                BlcInsured oBlcInsured = oFactory.GetEmptyCoverages(ePolicy.LineOfBusiness);
                foreach (BlcCoverage blcCoverage in oBlcInsured.CoverageList)
                {
                    Coverage eCoverage = new Coverage(new Guid());
                    eCoverage.InsuredID = _eServicePlan.InsuredID;
                    eCoverage.PolicyID = ePolicy.ID;
                    eCoverage.TypeCode = blcCoverage.CoverageValueType;

                    //get the coverage name
                    CoverageNameTable tbl = new CoverageNameTable();
                    tbl.Load(this.CurrentUser.Company);
                    eCoverage.NameID = tbl.GetId(blcCoverage);

                    eCoverage.Save(trans);
                }

                //Set our Survey_PolicyCoverage Table if this is an already existing survey
                /*
                if (_isExistingSurvey)
                {
                    CoverageName[] coverageNames = CoverageName.GetArray("CompanyID = ? && LineOfBusinessCode = ? && MigrationOnly = false", CurrentUser.CompanyID, ePolicy.LineOfBusinessCode);
                    foreach (CoverageName coverageName in coverageNames)
                    {
                        SurveyPolicyCoverage coverage = new SurveyPolicyCoverage(_eSurvey.ID, _eLocation.ID, ePolicy.ID, coverageName.ID);
                        coverage.ReportTypeCode = (CurrentUser.Company.SupportsQuickSurveyCreation) ? ReportType.Report.Code : ReportType.NoReport.Code;
                        coverage.Active = true;

                        coverage.Save(trans);
                    }
                }
                else
                {
                    Location[] locations = Location.GetArray("InsuredID = ? && IsActive = true", _eSurvey.InsuredID);
                    foreach (Location location in locations)
                    {
                        SurveyLocationPolicyExclusion locationPolicy = new SurveyLocationPolicyExclusion(_eSurvey.ID, location.ID, ePolicy.ID);
                        locationPolicy.LocationExcluded = false;
                        locationPolicy.PolicyExcluded = false;
                        locationPolicy.PolicyViewed = (ePolicy.LineOfBusinessCode == Berkley.BLC.Entities.LineOfBusiness.CommercialUmbrella.Code);
                        locationPolicy.Save(trans);
                    }
                }
                */




                /*
                Policy ePolicy;
                if (_ePolicy == null)
                {
                    ePolicy = new Policy(Guid.NewGuid());
                }
                else
                {
                    ePolicy = _ePolicy.GetWritableInstance();
                }

                ePolicy.InsuredID = _eSurvey.InsuredID;
                this.PopulateEntity("txt", ePolicy);
                ePolicy.IsActive = true;

                ePolicy.Save(trans);

                if (_ePolicy == null)
                {
                    //stub out the coverages for the newly created lob
                    ImportFactory oFactory = new ImportFactory(this.CurrentUser.Company);
                    BlcInsured oBlcInsured = oFactory.GetEmptyCoverages(ePolicy.LineOfBusiness);
                    foreach (BlcCoverage blcCoverage in oBlcInsured.CoverageList)
                    {
                        Coverage eCoverage = new Coverage(new Guid());
                        eCoverage.InsuredID = _eSurvey.InsuredID;
                        eCoverage.PolicyID = ePolicy.ID;
                        eCoverage.TypeCode = blcCoverage.CoverageValueType;

                        //get the coverage name
                        CoverageNameTable tbl = new CoverageNameTable();
                        tbl.Load(this.CurrentUser.Company);
                        eCoverage.NameID = tbl.GetId(blcCoverage);

                        eCoverage.Save(trans);
                    }

                    //Set our Survey_PolicyCoverage Table if this is an already existing survey
                    if (_isExistingSurvey)
                    {
                        CoverageName[] coverageNames = CoverageName.GetArray("CompanyID = ? && LineOfBusinessCode = ? && MigrationOnly = false", CurrentUser.CompanyID, ePolicy.LineOfBusinessCode);
                        foreach (CoverageName coverageName in coverageNames)
                        {
                            SurveyPolicyCoverage coverage = new SurveyPolicyCoverage(_eSurvey.ID, _eLocation.ID, ePolicy.ID, coverageName.ID);
                            coverage.ReportTypeCode = (CurrentUser.Company.SupportsQuickSurveyCreation) ? ReportType.Report.Code : ReportType.NoReport.Code;
                            coverage.Active = true;

                            coverage.Save(trans);
                        }
                    }
                    else
                    {
                        Location[] locations = Location.GetArray("InsuredID = ? && IsActive = true", _eSurvey.InsuredID);
                        foreach (Location location in locations)
                        {
                            SurveyLocationPolicyExclusion locationPolicy = new SurveyLocationPolicyExclusion(_eSurvey.ID, location.ID, ePolicy.ID);
                            locationPolicy.LocationExcluded = false;
                            locationPolicy.PolicyExcluded = false;
                            locationPolicy.PolicyViewed = (ePolicy.LineOfBusinessCode == Berkley.BLC.Entities.LineOfBusiness.CommercialUmbrella.Code);
                            locationPolicy.Save(trans);
                        }
                    }
                } 
                */

                //Commit
                trans.Commit();

                // update our member var so the redirect will work correctly
                //_ePolicy = ePolicy;
            }
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (Exception ex)
        {
            if (ex.Message.ToUpper().Contains("COVERAGENAME NOT FOUND") || ex.Message.ToUpper().Contains("UNKNOWN LINE"))
            {
                QCI.Web.JavaScript.ShowMessage(this, string.Format("{0} is not a supported line of business.  Please choose a different line of business and try again.", cboLOB.SelectedItem.Text));
                return;
            }
            else
            {
                throw new Exception("", ex);
            }
        }

        if (sender == bool.TrueString)
        {
            Response.Redirect(string.Format("CreateServicePlanPolicy.aspx{0}", ARMSUtility.GetQueryStringForNav("planid")));
        }
        else
        {
            Response.Redirect(GetReturnURL(), true);
        }
    }

    /*
    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //some validation
                
                try
                {
                    Convert.ToDecimal(txtPremium.Text);
                }
                catch
                {
                    if (CurrentUser.Company.RequirePolicyPremium)
                    {
                        throw new FieldValidationException("Estimated Premium is not a valid currency value.");
                    }
                    else
                    {
                        txtPremium.Text = "0";
                    }
                }

                Policy ePolicy;
                if (_ePolicy == null)
                {
                    ePolicy = new Policy(Guid.NewGuid());
                }
                else
                {
                    ePolicy = _ePolicy.GetWritableInstance();
                }

                ePolicy.InsuredID = _eSurvey.InsuredID;
                this.PopulateEntity("txt", ePolicy);
                ePolicy.IsActive = true;

                ePolicy.Save(trans);

                if (_ePolicy == null)
                {
                    //stub out the coverages for the newly created lob
                    ImportFactory oFactory = new ImportFactory(this.CurrentUser.Company);
                    BlcInsured oBlcInsured = oFactory.GetEmptyCoverages(ePolicy.LineOfBusiness);
                    foreach (BlcCoverage blcCoverage in oBlcInsured.CoverageList)
                    {
                        Coverage eCoverage = new Coverage(new Guid());
                        eCoverage.InsuredID = _eSurvey.InsuredID;
                        eCoverage.PolicyID = ePolicy.ID;
                        eCoverage.TypeCode = blcCoverage.CoverageValueType;

                        //get the coverage name
                        CoverageNameTable tbl = new CoverageNameTable();
                        tbl.Load(this.CurrentUser.Company);
                        eCoverage.NameID = tbl.GetId(blcCoverage);

                        eCoverage.Save(trans);
                    }

                    //Set our Survey_PolicyCoverage Table if this is an already existing survey
                    if (_isExistingSurvey)
                    {
                        CoverageName[] coverageNames = CoverageName.GetArray("CompanyID = ? && LineOfBusinessCode = ? && MigrationOnly = false", CurrentUser.CompanyID, ePolicy.LineOfBusinessCode);
                        foreach (CoverageName coverageName in coverageNames)
                        {
                            SurveyPolicyCoverage coverage = new SurveyPolicyCoverage(_eSurvey.ID, _eLocation.ID, ePolicy.ID, coverageName.ID);
                            coverage.ReportTypeCode = (CurrentUser.Company.SupportsQuickSurveyCreation) ? ReportType.Report.Code : ReportType.NoReport.Code;
                            coverage.Active = true;

                            coverage.Save(trans);
                        }
                    }
                    else
                    {
                        Location[] locations = Location.GetArray("InsuredID = ? && IsActive = true", _eSurvey.InsuredID);
                        foreach (Location location in locations)
                        {
                            SurveyLocationPolicyExclusion locationPolicy = new SurveyLocationPolicyExclusion(_eSurvey.ID, location.ID, ePolicy.ID);
                            locationPolicy.LocationExcluded = false;
                            locationPolicy.PolicyExcluded = false;
                            locationPolicy.PolicyViewed = (ePolicy.LineOfBusinessCode == Berkley.BLC.Entities.LineOfBusiness.CommercialUmbrella.Code);
                            locationPolicy.Save(trans);
                        }
                    }
                }

                //Commit
                trans.Commit();

                // update our member var so the redirect will work correctly
                _ePolicy = ePolicy;
            }
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (Exception ex)
        {
            if (ex.Message.ToUpper().Contains("COVERAGENAME NOT FOUND") || ex.Message.ToUpper().Contains("UNKNOWN LINE"))
            {
                QCI.Web.JavaScript.ShowMessage(this, string.Format("{0} is not a supported line of business.  Please choose a different line of business and try again.", cboLOB.SelectedItem.Text));
                return;
            }
            else
            {
                throw new Exception("", ex);
            }
        }


        if (sender == bool.TrueString)
        {
            Response.Redirect(string.Format("CreateSurveyPolicy.aspx{0}", ARMSUtility.GetQueryStringForNav("policyid")));
        }
        else
        {
            Response.Redirect(GetReturnURL(), true);
        }
    }
    */

    void btnAddAnother_Click(object sender, EventArgs e)
    {
        btnSubmit_Click(bool.TrueString, null);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnURL(), true);
    }

    private string GetReturnURL()
    {
        string destinationURL = UrlBuilder("ServicePlanDetails.aspx");
        return destinationURL;
    }

    protected string UrlBuilder(string sWebPage)
    {
        return UrlBuilder(sWebPage, null, null);
    }

    protected string UrlBuilder(string sWebPage, string sKey, object oValue)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat(sWebPage);
        sb.AppendFormat("?planid={0}", gPlanID);

        if (sKey != null && sKey.Length > 0)
        {
            sb.AppendFormat("&{0}={1}", sKey, oValue);
        }

        return sb.ToString();
    }
    

    public class AjaxMethods
    {
        [Ajax.AjaxMethod()]
        public static string GetNewDate(string oldDate)
        {
            DateTime date;
            
            try
            {
                date = DateTime.Parse(oldDate);
            }
            catch
            {
                return string.Empty;
            }

            return date.AddYears(1).ToShortDateString();
        }
    }
}