<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" ValidateRequest="false" CodeFile="Survey.aspx.cs" Inherits="SurveyAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register TagPrefix="dp" Namespace="QCI.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="Counter" Src="~/Controls/Counter.ascx" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" media="screen" type="text/css" href="<%= base.TemplatePath %>/styles.css" />
    <link rel="stylesheet" media="print" type="text/css" href="<%= base.TemplatePath %>/Print-Survey.css" />
    
    <script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/javascripts/csshttprequest.js"></script>
    <script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/Keyoti_RapidSpell_Web_Common/RapidSpell-DIALOG.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="<%=ARMSUtility.TemplatePath %>/atd.css" />

    <div class="surveyscreen">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <% if (!string.IsNullOrEmpty(_eSurvey.Priority) && _eSurvey.Priority != "./Images/colors/none.png"){%>
    <asp:Image ID="imgPriority" runat="server" ImageUrl="./Images/arrow_forward.png" />
    <% } %>

    <span class="heading"><%= base.PageHeading %></span>
    <% if( HttpContext.Current.Request.QueryString.Get("NavFromSearch") != null ) { %>
        <% if( HttpContext.Current.Request.QueryString.Get("NavFromSearch") == "1" ) { %>
        <a class="nav" href="Search.aspx<%= ARMSUtility.GetQueryStringForNav("surveyid") %>"><< Back to Search</a>
        <% } else {%>
        <a class="nav" href="SearchResults.aspx<%= ARMSUtility.GetQueryStringForNav("surveyid") %>"><< Back to Search Results</a>
        <% } %>
    <% } else if( HttpContext.Current.Request.QueryString.Get("NavFromPlan") != null ) { %>
    <a class="nav" href="ServicePlan.aspx<%= ARMSUtility.GetQueryStringForNav("surveyid") %>"><< Back to Service Plan</a>
    <% } else {%>
    <a class="nav" id="BackToQueue" href="Surveys.aspx<%= ARMSUtility.GetQueryStringForNav("surveyid") %>"><< Back to Queue</a>
    <% } %>
 
    <% if (_displayInsuredCount){%>
    <a class="nav" id="TotalNumberOfServeys" href="SearchResults.aspx?<%=_urlParam %>"> 
        <% if (_serveysRecordCount == 1)
            { %>
            Total 1 Request <br>
        <% } else { %>
            Total <%= _serveysRecordCount %> Requests <br>
        <% } %>
    </a>
    <% } %>

    <br /><br />

    <% if(_eSurvey.LegacySystemID != string.Empty) { %>
    <span><a href="<%= GetLegacyURL() %>">Click here</a> to view additional details from the prior system.</span>
    <br />
    <% } %>
    
    <!--Test Controls: Begin - Hide or Remove the following block before going to Test! -->
    <div runat="server" style="visibility:visible;">
        <asp:TextBox ID="txtTest" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtTest2" runat="server">image/tiff</asp:TextBox>
        <asp:TextBox ID="txtTest3" runat="server">Test</asp:TextBox>
        <asp:TextBox ID="txtTest4" runat="server">5</asp:TextBox>
        <asp:LinkButton ID="lnkTest" runat="server" OnClick="lnkTest_Click">Test</asp:LinkButton>
        <br>
        <br>
    </div>
    <!--Test Controls: End-->
    <br />

    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <table cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td valign="top" width="500">
        <cc:Box id="boxSurvey" runat="server" title="Survey Information" width="100%">
          <table>
            <tr>
            <td valign="top" width="250px">
            <table class="fields">          
            <uc:Label id="lblNumber" runat="server" field="Survey.Number" />
            <uc:Label id="lblPolicyNumbers" runat="server" field="Survey.LegacySystemID" Name="Policy Numbers" />
            <% if (CurrentUser.Company.ID == Company.WRBerkleyCorporation.ID) { %>
            <uc:Label id="lblCompany" runat="server" field="Company.Name" Name="Company" />
            <% } %>
            <% if (_eSurvey.Status != SurveyStatus.AssignedSurvey) { %>
            <uc:Label id="lblConsultant" runat="server" field = "Survey.HtmlConsultant" Name= "Consultant" />
            <% } %>
            <uc:Label id="lblType" runat="server" field="SurveyTypeType.Name" Name="Survey Type" />
            <% if (TransactionType.GetOne("CompanyID = ?", CurrentUser.CompanyID) != null) { %>
            <uc:Label id="lblTransactionType" runat="server" field="TransactionType.Description" />
            <% } %>
            <uc:Label id="lblReasons" runat="server" field="SurveyTypeType.Code" Name="Survey Reasons" Visible="false" />
            <% if (_showSurveyRequestedDateField && (_eSurvey.RequestedDate != DateTime.MinValue)) { %>
            <uc:Label id="lblRequestedDate" runat="server" field="Survey.RequestedDate" />
            <% } %>
            <uc:Label id="lblDueDate" runat="server" field="Survey.DueDate" />
            <uc:Label id="lblCreateDate" runat="server" field="Survey.CreateDate" Name="Created On" />
            <uc:Label id="lblCreatedBy" runat="server" field="Survey.HtmlCreatedBy" Name="Created By" />
            <uc:Label id="lblCreatedWith" runat="server" field="Survey.PrimaryPolicyID" Name="Created With" />
            <uc:Label id="lblStatus" runat="server" field="SurveyStatus.Name" />
            <% if (_eSurvey.Status == SurveyStatus.OnHoldSurvey) { %>
            <uc:Label id="lblRecommendedUser" runat="server" field="Survey.HtmlRecommendedConsultant" Name="Recommended User" />
            <%}
            else if (_hideUWAssignedUser =="true" && !CurrentUser.IsUnderwriter) {%>
            <uc:Label id="lblAssignedUser" runat="server" field="User.Name" Name="Assigned User" />
            <% } %>
                <uc:Label id="lblDateSurveyed" runat="server" field="Survey.SurveyedDate" Name="Date Surveyed" />
            <uc:Label id="lblSurveyHours" runat="server" field="Survey.Hours" ValueFormat="{0:n2}"/>
                  <% if (CurrentUser.Company.SupportsOverallGrading) { %>
            <uc:Label id="lblGrading" runat="server" field="SurveyGrading.Name" Name="Overall Grading" />
            <% } %>
            <% if (CurrentUser.Company.SupportsSeverityRating) { %>
            <uc:Label id="lblSeverityRating" runat="server" field="SeverityRating.Name" Name="Severity Rating" />
            <% } %>
            <% if (CurrentUser.Company.SupportsInvoiceFields && IsFeeConsultant()) { %>
            <uc:Label id="lblInvoiceNumber" runat="server" field="Survey.InvoiceNumber" />
            <uc:Label id="lblInvoiceDate" runat="server" field="Survey.InvoiceDate" />
            <% } %>
            <% if (CurrentUser.Company.SupportsCallCount) { %>
            <uc:Label id="lblCallCount" runat="server" field="Survey.CallCount" ValueFormat="{0:n2}"/>
            <% } %>
            <% if (CurrentUser.Company.SupportsNonDisclosure) { %>
                <% if (_eSurvey.NonDisclosureRequired) { %>
                <tr>
                    <td class="label" valign="top" style="color:Red" nowrap>Non Disclosure Required</td>
                    <td style="color:Red">Yes</td>
                </tr>
                <% } else { %>
                <uc:Label id="Label1" runat="server" field="Survey.NonDisclosureRequired" />
                <% } %>
            <% } %>
            <tr>
                <td class="label" valign="top" nowrap>Non-Productive</td>
                <td>
                    <asp:LinkButton id="lnkNonProductive" runat="server"></asp:LinkButton>
                    <asp:Label id="lblNonProductive" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label" valign="top" nowrap>Flag For Review</td>
                <td>
                    <asp:LinkButton id="lnkFlagForReview" runat="server"></asp:LinkButton>
                    <asp:Label id="lblFlagForReview" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="label" valign="top" nowrap>Open Recs</td>
                <td>
                    <asp:Label id="lblOpenRecs" runat="server"></asp:Label>
                    <asp:Image ID="imgRecsArrow" AlternateText="Open recs exist for this survey request." Visible="false" BorderWidth="0" runat="server" />
                </td>
            </tr>
            <% if (_eSurvey.Quality != null) { %>
            <uc:Label id="lblQA" runat="server" field="SurveyQualityType.Name" Name="Quality Assessment" />
            <% } %>
            <% if (_eSurvey.AssignDate != DateTime.MinValue) { %>
            <uc:label id="lblAssignDate" runat="server" field="Survey.AssignDate" Name="Assigned Date" />
            <% } %>
            <% if (_eSurvey.ReassignDate != DateTime.MinValue) { %>
            <uc:label id="lblReassignDate" runat="server" field="Survey.ReassignDate" Name="Reassigned Date" />
            <% } %>
            <% if (_eSurvey.AcceptDate != DateTime.MinValue) { %>
            <uc:label id="lblAcceptDate" runat="server" field="Survey.AcceptDate" Name="Accepted Date" />
            <% } %>
            <% if (_eSurvey.ReportCompleteDate != DateTime.MinValue) { %>
            <uc:label id="lblReportCompleteDate" runat="server" field="Survey.ReportCompleteDate" Name="Report Submitted Date" />
            <% } %>
            <% if (_eSurvey.MailReceivedDate != DateTime.MinValue) { %>
            <uc:label id="lblMailReceivedDate" runat="server" field="Survey.MailReceivedDate" Name="Mail Received Date" />
            <% } %>
            <% if (_eSurvey.FeeConsultantCost != decimal.MinValue && CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID) { %>
            <uc:Label id="lblFeeConsultantCost" runat="server" field="Survey.FeeConsultantCost" ValueFormat="{0:c}" />
            <% } %>
            <% if (_eSurvey.ReviewerCompleteDate != DateTime.MinValue) { %>
                <uc:label id="lblReviewerCompleteDate" runat="server" field="Survey.ReviewerCompleteDate" Name="Reviewer Completed Date" />
            <% } %>
            <% if (_eSurvey.CompleteDate != DateTime.MinValue) { %>
            <uc:label id="lblCompleteDate" runat="server" field="Survey.CompleteDate" Name="Closed Date" />
            <% } %>
            <% if (_eSurvey.CanceledDate != DateTime.MinValue) { %>
            <uc:label id="lblCanceledDate" runat="server" field="Survey.CanceledDate" />
            <% } %>
            <uc:Label id="lblLastModifiedOn" runat="server" field="Survey.LastModifiedOn" ValueFormat="{0:g}" />
                </table>
             </td>
                <% if (_displayUndAgencyInfo)
                   { %>
            <td width="10px" align="center"><img class="sep" src="<%= AppPath %>/images/black_pixel.gif" height="110px" width="1px"></td>
            <td valign="top">
            <table class="fields">
            <uc:Label id="lblClientID" runat="server" field="Insured.ClientID" Columns="25" />
            <uc:Label id="lblHtmlUnderwriterName" runat="server" field="Survey.HtmlUnderwriterName" Name="Underwriter" />
            <uc:Label id="lblHtmlUnderwriterPhone" runat="server" field="Survey.HtmlUnderwriterPhone" Name="Underwriter Phone" />            
            <uc:Label id="lblAgencyNumber" runat="server" field="Agency.Number" />
            <uc:Label id="lblAgencyName" runat="server" field="Agency.Name" />
            <uc:Label id="lblAgencyPhone" runat="server" field="Agency.HtmlPhoneNumber"  Directions="(ex. 5551234567)" Name="Phone Number"  /> 
         
                 </table></td>
                    <% } %>
            </tr>
            </table>
        </cc:Box>
    

        <cc:Box id="boxNotes" runat="server" title="Comments" width="100%">
        <a id="lnkAddNote" runat="server" href="javascript:Toggle('divNotes')">Show/Hide</a>
        <div id="divNotes" style="MARGIN-TOP: 10px">
        <% if( repNotes.Items.Count > 0 ) { %>
        <asp:Repeater ID="repNotes" Runat="server">
            <HeaderTemplate>
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td colspan="2" style="BORDER-BOTTOM: solid 1px black">
                        <%# GetNoteUserName(Container.DataItem) %> - 
                        <%# HtmlEncode(Container.DataItem, "EntryDate", "{0:g}") %>
                        <span style="float:right;"><em><%# GetCommentPrivacy(Container.DataItem) %></em></span>
                    </td>
                </tr>
                <tr class="comment <%# GetCommentClass(Container.DataItem) %>">
                    <td>
                        <%# GetEncodedComment(Container.DataItem) %>
                    </td>
                    <td id="tdRemoveNote" align="right" valign="top" runat="server">
                        <asp:LinkButton ID="lnkRemoveNote" runat="server" OnClick="lnkRemoveNote_OnClick">Remove</asp:LinkButton>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <% } else { %>
        <div class="comment">
            (none)
        </div>
        <% } %>
        </div>
        <div id="divAddNote" runat="server" style="MARGIN-TOP: 10px;">
            <table>
                <tr>
                    <td>
                        <asp:TextBox id="txtNote" runat="server" CssClass="txt" TextMode="MultiLine" style="width: 425px" Rows="5" />
                    </td>
                    <td valign="top" style="padding-top: 10px;">
                        <a href="javascript:rapidSpell.dialog_spellCheck(true, 'txtNote')" id="checkLink"><img alt="Check Spelling" style="border:none;" src="<%=ARMSUtility.TemplatePath %>/javascripts/Images/atdbuttontr.gif" /></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Panel runat="server" ID="NotePrivacyDiv">
                            Visible to: 
                            <asp:RadioButton ID="rdoPrivacyAll" runat="server" Text="All" GroupName="rdoNotePrivacy" />
                            <asp:RadioButton ID="rdoPrivacyInternal" runat="server" Text="Internal Only" GroupName="rdoNotePrivacy" />
                            <asp:RadioButton ID="rdoPrivacyAdministrator" runat="server" Text="Administrators Only" GroupName="rdoNotePrivacy" />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!--<asp:CheckBox ID="chkInternalOnly" Runat="server" CssClass="chk" Text="Internal Only" />&nbsp;&nbsp;-->
                        <asp:Button ID="btnAddNote" Runat="server" Text="Add Comment" CssClass="btn" CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </div>
        </cc:Box>

        <% if (CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID) { %> 
        <cc:Box id="boxDocs" runat="server" title="Documents" width="100%">
        <a id="lnkAddDoc" href="javascript:Toggle('divAddDoc')">Show/Hide</a>
        <div id="divAddDoc" runat="server" style="MARGIN-TOP: 10px">
        <% if( repDocs.Items.Count > 0 ) { %>
        <asp:Repeater ID="repDocs" Runat="server">
            <HeaderTemplate>
                <table cellspacing="0" cellpadding="0" border="0">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="PADDING-BOTTOM: 3px">
                        <a id="lnkDownloadDocInNewWindow" href='<%# DataBinder.Eval(Container.DataItem, "ID", "Document.aspx?docid={0}") %>' target="_blank" runat="server"><%# GetFileNameWithSize(Container.DataItem) %></a>
                        <asp:LinkButton ID="lnkDownloadDoc" runat="server" OnClick="lnkDownloadDoc_OnClick"><%# GetFileNameWithSize(Container.DataItem) %></asp:LinkButton>
                    </td>
                    <td nowrap style="PADDING-LEFT: 10px" valign="top">
                        <%# GetDocUserName(Container.DataItem) %> on <%# HtmlEncode(Container.DataItem, "UploadedOn", "{0:g}") %>
                    </td>
                    <td id="tdRemoveDoc" class="tdRemoveDoc" style="PADDING-LEFT: 15px" valign="top" runat="server">
                        <asp:LinkButton ID="lnkRemoveDoc" runat="server" OnClick="lnkRemoveDoc_OnClick">Remove</asp:LinkButton>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <% } else { %>
        <div style="PADDING-BOTTOM: 10px">
            (none)
        </div>
        <% } %>
            <div id="divUploadDoc" style="padding-top:5px" runat="server">
                Attach Document<br>
                <input type="file" id="fileUploader" runat="server" name="fileDocument" class="txt" size="70"><br>
                <%if (cboDocType.Items.Count > 0) { %>
                Doc Type<br>
                <asp:DropDownList ID="cboDocType" runat="server" CssClass="cbo"></asp:DropDownList><br>
                <%if (CurrentUser.Company.SupportsDocRemarks) { %>
                    Doc Remarks <small>(225 character limit)</small>&nbsp;
                    <%if (!CurrentUser.Company.RequireDocRemarks) { %>
                        <span style="font-size:10px">(optional)</span>
                        <% } %>
                        <br>
                        <asp:TextBox ID="txtDocRemarks" runat="server" Columns="70" CssClass="txt" MaxLength="225"></asp:TextBox>
                    <% } %>
                <% } %>
                <div style="padding-top:5px">
                    <asp:Button ID="btnAttachDocument" Runat="server" Text="Attach Document" CssClass="btn" CausesValidation="False" />
                    &nbsp;&nbsp;&nbsp;<a href="DocumentRetriever.aspx<%= ARMSUtility.GetQueryStringForNav() %>"><span style="font-size:12px">...Search & Retrieve Additional Documents from FileNet >></span></a>
                </div>
            </div>
        </div>
        </cc:Box>
        
        <% if (_eSurvey.ServiceType == ServiceType.ServiceVisit && !_usesPlanObjectives){ %>
        <cc:Box id="boxObjectives" runat="server" title="Objectives" width="100%">
            <% if( repObjectives.Items.Count > 0 ) { %>
            <asp:Repeater ID="repObjectives" Runat="server">
                <HeaderTemplate>
                    <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0" cellpadding="0">
                        <tr>
                            <td nowrap><u>Number</u></td>
                            <td style="PADDING-LEFT: 20px" nowrap><u>Objective</u></td>
                            <td style="PADDING-LEFT: 20px" nowrap><u>Comments</u></td>
                            <td style="PADDING-LEFT: 20px" nowrap><u>Target Date</u></td>
                            <td style="PADDING-LEFT: 20px" nowrap><u>Status</u></td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Number", Int32.MinValue, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "Text", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "CommentsText", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "TargetDate", "{0:d}", DateTime.MinValue, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "Status.Name", string.Empty, "(none)")%></td>
                    </tr>			
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <% } else if (repObjectives.Visible) { %>
            <div style="PADDING-BOTTOM: 10px">
                (none)
            </div>
            <% } %>
            <asp:datagrid ID="grdObjectives" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
                <headerstyle CssClass="header" />
                <itemstyle CssClass="item" />
                <alternatingitemstyle CssClass="altItem" />
                <footerstyle CssClass="footer" />
                <pagerstyle CssClass="pager" Mode="NumericPages" />
                <columns>
                    <asp:TemplateColumn HeaderText="Number" SortExpression="Number ASC">
                        <ItemTemplate>
                            <a href="ObjectiveEdit.aspx<%= ARMSUtility.GetQueryStringForNav() %>&objectiveid=<%# HtmlEncode(Container.DataItem, "ID")%>"><%# HtmlEncode(Container.DataItem, "Number").PadLeft(3,'0')%></a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Objective" SortExpression="Text ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Text", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="Comments" SortExpression="CommentsText ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "CommentsText", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Target Date" SortExpression="TargetDate ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "TargetDate", "{0:d}", DateTime.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" SortExpression="Status.Name ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Status.Name", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            &nbsp;<asp:LinkButton ID="btnRemoveObjective" Runat="server" CommandName="Remove" OnPreRender="btnRemoveObjective_PreRender" CausesValidation="false">Remove</asp:LinkButton>&nbsp;
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </columns>
            </asp:datagrid>
            
            <a id="lnkAddObjective" href="ObjectiveEdit.aspx" visible="false" runat="server">Create New Objective</a>
            
        </cc:Box>
        <% } %>

         <% if (_eSurvey.ServiceType == ServiceType.ServiceVisit && _usesPlanObjectives){ %>      
        <cc:Box id="boxServicePlanObjectives" runat="server" title="Objectives" width="100%">
            Available Plan Objectives<br></br>
            <asp:DropDownList ID="ddlAvailablePlanObjectives" runat="server" CssClass="cbo" align="right" valign="top" Width="400px"></asp:DropDownList>
             <asp:Button ID="btnAddPlanObjective" Runat="server" Text="Add" CssClass="btn" CausesValidation="False" />
            <br></br>
            <asp:datagrid ID="grdPlanObjectives" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
                <headerstyle CssClass="header" />
                <itemstyle CssClass="item" />
                <alternatingitemstyle CssClass="altItem" />
                <footerstyle CssClass="footer" />
                <pagerstyle CssClass="pager" Mode="NumericPages" />
                <columns>
                    <asp:TemplateColumn HeaderText="Number" SortExpression="Number ASC">
                        <ItemTemplate>
                            <a href="ObjectiveEdit.aspx<%= ARMSUtility.GetQueryStringForNav() %>&objectiveid=<%# HtmlEncode(Container.DataItem, "ID")%>"><%# HtmlEncode(Container.DataItem, "Number").PadLeft(3,'0')%></a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Objective" SortExpression="Text ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Text", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="Comments" SortExpression="CommentsText ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "CommentsText", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Target Date" SortExpression="TargetDate ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "TargetDate", "{0:d}", DateTime.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" SortExpression="Status.Name ASC">
                        <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Status.Name", string.Empty, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            &nbsp;<asp:LinkButton ID="btnRemoveObjective" Runat="server" CommandName="Remove" OnPreRender="btnRemoveObjective_PreRender" CausesValidation="false">Remove</asp:LinkButton>&nbsp;
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </columns>
            </asp:datagrid>
            
            <a id="A1" href="ObjectiveEdit.aspx" visible="false" runat="server">Create New Objective</a>
            
        </cc:Box>
        <% } %>
        
        <%if (CurrentUser.IsUnderwriter || CurrentUser.Company.DisplayRecsOnSurveyScreen) { %>
        <cc:Box id="boxRecs" runat="server" title="Recommendations" width="100%">
            <% if( repRecs.Items.Count > 0 ) { %>
            <asp:Repeater ID="repRecs" Runat="server">
                <HeaderTemplate>
                    <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0" cellspacing="0">
                        <tr>
                            <td nowrap><u>Number</u></td>
                            <% if(_customRecommendationSnapShot=="true") { %>
                            <td style="PADDING-LEFT: 10px" nowrap><u>Recommendations</u></td>
                            <td style="PADDING-LEFT: 10px" nowrap><u>Created</u></td>
                            <td style="PADDING-LEFT: 10px" nowrap><u>Completed</u></td>
                            <% } else { %>
                            <td style="PADDING-LEFT: 10px" nowrap><u>Rec Code</u></td>
                            <td style="PADDING-LEFT: 10px" nowrap><u>Rec Title</u></td>
                            <td style="PADDING-LEFT: 10px" nowrap><u>Date Created</u></td>
                            <td style="PADDING-LEFT: 10px" nowrap><u>Date Completed</u></td>
                            <% } %>
                            <td style="PADDING-LEFT: 10px" nowrap><u>Status</u></td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>

                        <% if(_customRecommendationSnapShot=="true") { %>
                        <td valign="top" nowrap>
                            <a href="RecommendationEdit.aspx<%= ARMSUtility.GetQueryStringForNav() %>&surveyrecid=<%# HtmlEncode(Container.DataItem, "ID")%><%=_requestedByPage %>">
                            <%# HtmlEncodeNullable(Container.DataItem, "RecommendationNumber", string.Empty, "(none)")%>
                            </a>                       
                        </td>
                        <td valign="top" style="PADDING-LEFT: 10px">
                            <%# (HtmlEncodeNullable(Container.DataItem, "EntryText", string.Empty, "(none)").Length > 100) ? 
                                 HtmlEncodeNullable(Container.DataItem, "EntryText", string.Empty, "(none)").Substring(0,100):
                                 HtmlEncodeNullable(Container.DataItem, "EntryText", string.Empty, "(none)") %>
                        </td>
                        <% } else { %>
                        <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "RecommendationNumber", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 10px"><%# HtmlEncodeNullable(Container.DataItem, "Recommendation.Code", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 10px"><%# HtmlEncodeNullable(Container.DataItem, "Recommendation.Title", string.Empty, "(none)")%></td>
                        <% } %>
                        <td valign="top" style="PADDING-LEFT: 10px"><%# HtmlEncodeNullable(Container.DataItem, "DateCreated", "{0:d}", DateTime.MinValue, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 10px"><%# HtmlEncodeNullable(Container.DataItem, "DateCompleted", "{0:d}", DateTime.MinValue, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 10px"><%# HtmlEncodeNullable(Container.DataItem, "RecStatus.Type.Name", null, "(none)")%></td>
                    </tr>			
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <% } else { %>
            <div style="PADDING-BOTTOM: 10px">
                (none)
            </div>
            <% } %>
        </cc:Box>
        <% } %>

        <cc:Box id="boxHistory" runat="server" title="History" width="100%">
        <a id="lnkHistory" href="javascript:Toggle('divHistory')">Show/Hide</a><br>
        <div id="divHistory" style="MARGIN-TOP: 10px">
        <% if( repHistory.Items.Count > 0 ) { %>
            <asp:Repeater ID="repHistory" Runat="server">
                <HeaderTemplate>
                    <table cellspacing="0" cellpadding="0" border="0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" nowrap>
                            <%# HtmlEncode(Container.DataItem, "EntryDate", "{0:g}") %>
                        </td>
                        <td style="PADDING-LEFT: 10px; PADDING-BOTTOM: 4px">
                            <%# GetEncodedHistory(Container.DataItem)%>
                        </td>
                        <td id="tdRemoveHistory" class="tdRemoveDoc" style="PADDING-LEFT: 15px" valign="top" runat="server">
                            <asp:LinkButton ID="lnkRemoveHistory" runat="server" OnClick="lnkRemoveHistory_OnClick">Remove</asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        <% } else { %>
            (none)
        <% } %>
        </div>
            </cc:Box>
        <% } %>

        </td>
        <td width="15">&nbsp;</td>
        <td valign="top">
        
        <cc:Box id="boxLinks" runat="server" title="Insured Info" width="185">
        <ul>
            <li><a href="insured.aspx<%= ARMSUtility.GetQueryStringForNav() %>&locationid=<%= _eSurvey.LocationID %>">Insured Details</a></li>
            <li id="listItemInsuredLocations" runat="server"><a href="locations.aspx<%= ARMSUtility.GetQueryStringForNav() %>">Insured Location(s)</a></li>
        </ul>
        </cc:Box>
            
        <cc:Box id="boxActions" runat="server" title="Available Actions" width="185">
            <asp:Repeater ID="repActions" Runat="server">
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <li>
                        <asp:LinkButton id="btnAction" runat="server" CommandName='<%# HtmlEncode(Container.DataItem, "ID") %>'><%# HtmlEncode(Container.DataItem, "DisplayName") %></asp:LinkButton>
                    </li>
                </ItemTemplate>
                <FooterTemplate>
                    <% if( repActions.Items.Count == 0 ) { %>
                    <li>None</li>
                    <% } %>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>

            <div id="divAssign" runat="server">
                <asp:Label ID="lblAssign" Runat="server"></asp:Label>
                <div style="margin: 5px 0px 5px 0px">
                     <% if( _showUserOption ) { %>
                      <asp:RadioButtonList ID="rdlUserType" runat="server" AutoPostBack="True">
                        <asp:ListItem selected="true">Users</asp:ListItem>
                        <asp:ListItem>Reviewers</asp:ListItem>
                    </asp:RadioButtonList>   
                      <% } %>
                    <asp:DropDownList ID="cboUsers" runat="server" CssClass="cbo" />
                </div>
                <asp:Button ID="btnAssign" Runat="server" Text="Assign" CssClass="btn" />
                <asp:Button ID="btnAssignCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divType" runat="server">
                <span>New Type:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:DropDownList ID="cboTypes" runat="server" CssClass="cbo" />
                </div>
                <asp:Button ID="btnTypeUpdate" Runat="server" Text="Update" CssClass="btn" />
                <asp:Button ID="btnTypeCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divDueDate" runat="server">
                <span>New Due Date:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <dp:DatePicker id="dpDueDate" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
                </div>
                <asp:Button ID="btnDueDateUpdate" Runat="server" Text="Update" CssClass="btn" />
                <asp:Button ID="btnDueDateCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divApproveDueDate" runat="server">
                 <asp:Label id="lblChangeDateApprove" runat="server" Visible="true"></asp:Label>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:RadioButton ID="rdoApprove"  Runat="server" CssClass="rdo" GroupName="ChangeDateApprove" Text="Approve" AutoPostBack="true" OnCheckedChanged="ChangeDateApprove_CheckedChanged"/><br />
                    <asp:RadioButton ID="rdoDeny"  Runat="server" CssClass="rdo" GroupName="ChangeDateApprove" Text="Deny" AutoPostBack="true" OnCheckedChanged="ChangeDateApprove_CheckedChanged"/><br />
                    <asp:RadioButton ID="rdoNewDate"  Runat="server" CssClass="rdo" GroupName="ChangeDateApprove" Text="New Date" AutoPostBack="true" OnCheckedChanged="ChangeDateApprove_CheckedChanged"/><br>
                </div>

                <div id="dvApproveNewDate"  runat ="server" visible="false">
                    <dp:DatePicker id="dpApproveNewDate" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
                </div> 

                <span>Comments:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:TextBox ID="txtApprovDenyReason" CssClass="txt" runat="server" Rows="4" Columns="25" TextMode="MultiLine"></asp:TextBox>
                </div>

                <asp:Button ID="btnApproveDueDateSubmit" Runat="server" Text="Submit" CssClass="btn" />
                <asp:Button ID="btnApproveDueDateCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divAssignedDate" runat="server">
                <span>New Assigned Date:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <dp:DatePicker id="dpAssignedDate" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
                </div>
                <asp:Button ID="btnAssignedDateUpdate" Runat="server" Text="Update" CssClass="btn" />
                <asp:Button ID="btnAssignedDateCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divAcceptedDate" runat="server">
                <span>New Accepted Date:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <dp:DatePicker id="dpAcceptedDate" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
                </div>
                <asp:Button ID="btnAcceptedDateUpdate" Runat="server" Text="Update" CssClass="btn" />
                <asp:Button ID="btnAcceptedDateCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divReportSubmittedDate" runat="server">
                <span>New Report Submitted Date:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <dp:DatePicker id="dpReportSubmittedDate" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
                </div>
                <asp:Button ID="btnReportSubmittedDateUpdate" Runat="server" Text="Update" CssClass="btn" />
                <asp:Button ID="btnReportSubmittedDateCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divCompleteDate" runat="server">
                <span>New Closed Date:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <dp:DatePicker id="dpCompleteDate" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
                </div>
                <asp:Button ID="btnCompleteDateUpdate" Runat="server" Text="Update" CssClass="btn" />
                <asp:Button ID="btnCompleteDateCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divChangePrimaryPolicy" runat="server">
                <span id="lblPrimaryPolicy" runat="server">Primary Policy:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:TextBox ID="txtPrimaryPolicy" runat="server" CssClass="txt" Columns="15"></asp:TextBox><br>
                </div>
                <asp:Button ID="btnChangePrimaryPolicyUpdate" Runat="server" Text="Update" CssClass="btn" />
                <asp:Button ID="btnChangePrimaryPolicyCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divLetters" runat="server">
                <span>Generate:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:DropDownList ID="cboLetters" runat="server" CssClass="cbo" />
                </div>
                <input type="button" id="btnLetterSelect" value="Generate" class="btn" OnClick="<%= string.Format("GenerateLetter('{0}');", _eSurvey.ID) %>" />
                <asp:Button ID="btnLetterCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divReports" runat="server">
                <span>Generate:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:DropDownList ID="cboReports" runat="server" CssClass="cbo" />
                </div>
                <!--Modified By Vilas Meshram: 05/16/2013 : Squish# 21329 : Add Additional Merge Fields for Service Plan and Survey Templates -->
                <input type="button" id="btnReportSelect" value="Generate" class="btn" OnClick="<%= string.Format("GenerateReport('{0}','{1}');", _eSurvey.ID, _servicePlanid) %>" /> 
                <%--<input type="button" id="btnReportSelect" value="Generate" class="btn" OnClick="<%= string.Format("GenerateReport('{0}');", _eSurvey.ID) %>" />--%>
                <asp:Button ID="btnReportCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divQA" runat="server">
                <span>QA of Survey Report & Letter:</span>
                <div style="margin: 5px 0px 13px 0px">
                    <asp:DropDownList ID="cboQualities" runat="server" CssClass="cbo" />
                </div>
                <span>Comments:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:TextBox ID="txtQAComments" CssClass="txt" runat="server" Rows="4" Columns="25" TextMode="MultiLine"></asp:TextBox>
                </div>
                <asp:Button ID="btnQAUpdate" Runat="server" Text="Update" CssClass="btn" />
                <asp:Button ID="btnQACancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divMailReceivedDate" runat="server">
                <span>Mail Received Date:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <dp:DatePicker id="dpMailReceivedDate" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
                </div>
                <asp:Button ID="btnMailReceivedDateUpdate" Runat="server" Text="Update" CssClass="btn" />
                <asp:Button ID="btnMailReceivedDateCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divMailSentDate" runat="server">
                <span>Letter Sent:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:DropDownList ID="cboSentDateLetters" runat="server" CssClass="cbo" />
                </div>
                
                <span>Date Sent:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <dp:DatePicker id="dpMailSentDate" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
                </div>
                <asp:Button ID="btnMailSentDateUpdate" Runat="server" Text="Update" CssClass="btn" />
                <asp:Button ID="btnMailSentDateCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divCancel" runat="server">
                <span>Reason for Cancellation:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:TextBox ID="txtCancellationExplanation" CssClass="txt" runat="server" Rows="4" Columns="25" TextMode="MultiLine"></asp:TextBox>
                </div>
                <asp:Button ID="btnCancellation" Runat="server" Text="Submit" CssClass="btn" />
                <asp:Button ID="btnCancellationCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divCloseAndNotify" runat="server">
                <span>UW Instructions: (optional)</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:TextBox ID="txtNotifyInstructions" CssClass="txt" runat="server" Rows="4" Columns="25" TextMode="MultiLine"></asp:TextBox>
                </div>
                <asp:Button ID="btnCloseAndNotify" Runat="server" Text="Submit" CssClass="btn" />
                <asp:Button ID="btnCloseAndNotifyCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divReleaseOwnership" runat="server">
                <% if (_eSurvey.Status.Type == SurveyStatusType.Survey) { %>
                <span>Reason:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:DropDownList ID="cboReleaseReasons" runat="server" CssClass="cbo"   AutoPostBack="true" OnSelectedIndexChanged="cboReleaseReasons_SelectedIndexChanged" />
                </div>
                <% } %>
                <div id="newDueDate" style="margin: 1px 0px 10px 0px" runat="server" visible="false"  >
                <span>New Date:</span>
                    <div style="margin: 5px 0px 5px 0px">
                        <dp:DatePicker id="dtpNewDueDate" runat="server" CssClass="txt" Columns="10" AutoPostBack="false" onfocus="StartAutoFormat(this, 'Date')" /><br>
                    </div>
                </div>
                <span>Comments:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:TextBox ID="txtReasonForReleaseOwnership" CssClass="txt" runat="server" Rows="4" Columns="25" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div>                
                            Visible to: 
                     <div style="margin: 5px 0px 5px 0px">
                            <asp:RadioButton ID="rdoAll" CssClass="rdo" runat="server" Text="All" GroupName="rdoCommentPrivacy" /><br />
                            <asp:RadioButton ID="rdoInternal" CssClass="rdo" runat="server" Text="Internal Only" GroupName="rdoCommentPrivacy" /><br />
                            <asp:RadioButton ID="rdoAdministrator" CssClass="rdo" runat="server" Text="Administrators Only" GroupName="rdoCommentPrivacy" /><br />
                         </div>                 
                 </div>
                <asp:Button ID="btnReleaseOwnership" Runat="server" Text="Submit" CssClass="btn" />
                <asp:Button ID="btnReleaseOwnershipCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divEmailUW" runat="server">
                <span>Email To:</span>
                <div style="margin: 1px 0px 10px 0px">
                        <% if (_displayUserType) { %>
                    <asp:RadioButtonList ID="rdoUserType" runat="server" AutoPostBack="True">
                        <asp:ListItem selected="true">Underwriter</asp:ListItem>
                        <asp:ListItem>Users</asp:ListItem>
                    </asp:RadioButtonList>                
                        <% } %>
                    <asp:DropDownList ID="cboUnderwriters" runat="server" CssClass="cbo" />
                </div>
                
                <span>Email Text:</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:TextBox ID="txtEmailForUW" CssClass="txt" runat="server" Rows="4" Columns="25" TextMode="MultiLine"></asp:TextBox>
                </div>
                <asp:Button ID="btnEmailUW" Runat="server" Text="Submit" CssClass="btn" />
                <asp:Button ID="btnEmailUWCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>      
            <div id="divCreateNewSurvey" runat="server">
                <span>New Survey Type:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:DropDownList ID="cboNewSurveySurveyTypes" runat="server" CssClass="cbo" />
                </div>
                <span>New Assigned User:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:DropDownList ID="cboNewSurveyUsers" runat="server" CssClass="cbo" />
                </div>
                <span>New Due Date:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <dp:DatePicker id="dpNewSurveyDueDate" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
                </div>
                <asp:Button ID="btnCreateNewSurvey" Runat="server" Text="Submit" CssClass="btn" />
                <asp:Button ID="btnCreateNewSurveyCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>

            <div id="divChangeSurveyPriority" runat="server">
                <span>Change Survey Priority:</span>
            <!-- Modified By Vilas Meshram: 03/04/2013: Squish# 21365: Dropdown for priority -->
                <asp:DropDownList ID="ddlSurveyPriority" runat ="server" Width="100" > </asp:DropDownList> 
            <!-- 
                <div runat="server" id="imgRptr">
                <asp:RadioButtonList runat="server" ID="rblPriority" RepeatLayout="Flow" RepeatDirection="Horizontal" RepeatColumns="3" >
                </asp:RadioButtonList>
                </div>
             Modified By Vilas Meshram: 03/04/2013: Squish# 21365: Dropdown for priority -->
            <br />
            <br />
            <br />

            <asp:Button ID="btnSavePriority" runat="server" Text="Submit" CssClass="btn" />
            <asp:Button ID="btnCancelPriority" runat="server" Text="Cancel" CssClass="btn" />
            </div>
            <div id="divNotifyUW" runat="server">
                <span>UW Instructions: (optional)</span>
                <div style="margin: 5px 0px 5px 0px">
                    <asp:TextBox ID="txtNotifyUWInstructions" CssClass="txt" runat="server" Rows="4" Columns="25" TextMode="MultiLine"></asp:TextBox>
                </div>
                <asp:Button ID="btnNotifyUW" Runat="server" Text="Submit" CssClass="btn" />
                <asp:Button ID="btnNotifyUWCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
        </cc:Box>
        
        <% if( repCompleteActions.Items.Count > 0 ) { %>
        <cc:Box id="boxCompletedCall" runat="server" title="Handle Completed Call" width="190">
            <asp:Repeater ID="repCompleteActions" Runat="server">
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <li><asp:LinkButton id="btnCompleteAction" runat="server" CommandName='<%# HtmlEncode(Container.DataItem, "ID") %>'><%# HtmlEncode(Container.DataItem, "DisplayName") %></asp:LinkButton>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
            
            <div id="divSetDetails" runat="server">
                <span>Date Surveyed:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <dp:DatePicker id="dpDateSurveyed" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
                </div>
                
                <% if ((CurrentUser.Company.SupportsSimplifiedAcctMgmt || _displayActivityTimes) && !IsFeeConsultant()) { %>
                <asp:Repeater ID="repSurveyActivities" Runat="server">
                    <ItemTemplate>
                        <span><%# HtmlEncode(Container.DataItem, "Name") %>:</span>
                        <div style="margin: 1px 0px 10px 0px">
                            <asp:TextBox ID="txtActivity" CssClass="txt" Columns="10" runat="server"></asp:TextBox>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <% } %>
                
                <% if (((!CurrentUser.Role.PermissionSet.CanTakeAction(SurveyActionType.AccountManagement, _eSurvey, CurrentUser) || _displayActivityTimes) && !CurrentUser.Company.SupportsSimplifiedAcctMgmt) || IsFeeConsultant()) { %>
                <span>Total Hours:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:TextBox ID="txtTotalHours" CssClass="txt" Columns="10" runat="server"></asp:TextBox>
                </div>
                <% } %>
                
                <% if (IsFeeConsultant() && _eSurvey.Company.SupportsInvoiceFields) { %>
                <span>Invoice Number:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:TextBox ID="txtInvoiceNumber" CssClass="txt" Columns="10" runat="server"></asp:TextBox>
                </div>
                
                <span>Invoice Date:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <dp:DatePicker id="dpInvoiceDate" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
                </div>
                <% } %>
                
                <% if (_eSurvey.Company.SupportsOverallGrading) { %>
                <span>Overall Grading:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:DropDownList ID="cboOverallGrading" runat="server" CssClass="cbo" />
                </div>
                <% } %>
                
                <% if (_eSurvey.Company.SupportsSeverityRating) { %>
                <span>Severity Rating:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:DropDownList ID="cboSeverityRating" runat="server" CssClass="cbo" />
                </div>
                <% } %>
                
                <% if ((_eSurvey.Status == SurveyStatus.AssignedSurvey && _eSurvey.AssignedUser.IsFeeCompany) || (_eSurvey.ConsultantUser != null && _eSurvey.ConsultantUser.IsFeeCompany)) { %>
                <span>Cost:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:TextBox ID="txtCost" CssClass="txt" Columns="10" runat="server"></asp:TextBox>
                </div>
                <% } %>
                
                <% if (_eSurvey.Company.SupportsCallCount) { %>
                <span><%=CurrentUser.Company.CallCountLabel %>:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <uc:Counter id="cCallCount" runat="server" IsRequired="false" />
                </div>
                <% } %>
                
                <span>Recommendations:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:RadioButton id="rdoRecYes" GroupName="Recs" Text="Yes" CssClass="rdo" runat="server" />
                    <asp:RadioButton id="rdoRecNo" GroupName="Recs" Text="No" CssClass="rdo" runat="server" />
                </div>
                  <% if (Berkley.BLC.Business.Utility.GetCompanyParameter("ShowUWNotified", CurrentUser.Company) == "true"){%>
                 <span>Was Underwriter Notified:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:RadioButton id="rdoUWNotifiedYes" GroupName="UWNotified" Text="Yes" CssClass="rdo" runat="server" />
                    <asp:RadioButton id="rdoUWNotifiedNo" GroupName="UWNotified" Text="No" CssClass="rdo" runat="server" />
                </div>
                <% } %>
                <asp:Button ID="btnSetDetailsSubmit" Runat="server" Text="Submit" CssClass="btn" />
                <asp:Button ID="btnSetDetailsCancel" Runat="server" Text="Cancel" CssClass="btn" />
            </div>
            
            <div id="divCompleteRequest" runat="server">
                <% if (_eSurvey.ServiceType == ServiceType.SurveyRequest) { %>
                <span>Future Visits Needed:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <%if (_eSurvey.CreateStateCode != CreateState.Migrated.Code) { %>
                    <asp:RadioButton id="rdoVisitYes" GroupName="FutureVisit" Text="Yes" CssClass="rdo" runat="server" />
                    <%} %>
                    <asp:RadioButton id="rdoVisitNo" GroupName="FutureVisit" Text="No" CssClass="rdo" runat="server" />
                </div>
                <%} %>
                
                <% if (CurrentUser.Company.OptionToReviewIsVisible) { %>
                <span>Send for Review:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:RadioButton id="rdoReviewYes" GroupName="Review" Text="Yes" CssClass="rdo" runat="server" />
                    <asp:RadioButton id="rdoReviewNo" GroupName="Review" Text="No" CssClass="rdo" runat="server" />
                </div>

                <div id="divUWInstructionsOnCompletion">
                    <span>* UW Instructions:</span>
                    <div style="margin: 5px 0px 5px 0px">
                        <asp:TextBox ID="txtNotifyInstructionsOnCompletion" CssClass="txt" runat="server" Rows="4" Columns="25" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <%} %>
                
                <% if (_eSurvey.ServiceType == ServiceType.SurveyRequest) { %>
                <div id="divFutureVisit" runat="server">
                    <span>* Survey Type:</span>
                    <div style="margin: 1px 0px 10px 0px">
                        <asp:DropDownList ID="cboSurveyType" runat="server" CssClass="cbo" />
                    </div>
                     <% if (_isCWGSurvey) { %>
                      <span>* Survey Reason:</span>
                    <div style="margin: 1px 0px 10px 0px">
                        <asp:DropDownList ID="cboSurveyReasonType" runat="server" CssClass="cbo" />
                    </div>
                    <%}%>
                    <span>Future Assigned User:</span>
                    <div style="margin: 1px 0px 10px 0px">
                        <asp:DropDownList ID="cboFutureUsers" runat="server" CssClass="cbo" />
                    </div>
                    <% if (_isCWGSurvey)
                       { %>
                    <span>* Due Date:</span>
                    <%}else {%>
                    <span>* Future Survey Date:</span>
                    <%}%>
                    <div style="margin: 1px 0px 10px 0px">
                        <dp:DatePicker id="dpFutureDateSurveyed" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
                    </div>
                      <% if (_isCWGSurvey)
                         { %>
                    <span>* Comments:</span>
                    <%}else {%>
                    <span>* Purpose of Future Survey:</span>
                    <%}%>                    
                    <div style="margin: 1px 0px 20px 0px">
                        <asp:TextBox ID="txtFutureSurveyPurpose" CssClass="txt" runat="server" Rows="3" Columns="25" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <%} %>
                
                <asp:Button ID="btnCompleteSubmit" Runat="server" Text="Submit" CssClass="btn" />
                <asp:Button ID="btnCompleteCancel" Runat="server" Text="Cancel" CssClass="btn" CausesValidation="false" />
            </div>
        </cc:Box>
        <% } %>
        </td>
    </tr>
    </table>

    <uc:HintFooter ID="ucHintFooter" runat="server" VerticalOffset="130" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
    function Toggle(id)
    {
        ToggleControlDisplay(id);
    }
    function GetLetterSentDate(surveyID, docID)
    {
        var date = AjaxMethods.GetLetterSentDate(surveyID, docID).value;
        document.getElementById('dpMailSentDate').value = date;
    }
    function Visit()
    {
        var chk = document.getElementById('rdoVisitYes');
        if (chk.checked) {
            document.getElementById('divFutureVisit').style.display = 'inline';
        }
        else {
            document.getElementById('divFutureVisit').style.display = 'none';
        }
    }
    function GenerateLetter(surveyID)
    {
        var cboLetters = document.getElementById('cboLetters');
        
        if (cboLetters.value != null && cboLetters.value != "") {
            window.open('document.aspx?letterid=' + cboLetters.value + '&surveyid=' + surveyID);
            document.location = document.URL;
        }
        else {
            alert("Select a letter to generate.");
        }
    }

    //Modified By Vilas Meshram: 05/16/2013 : Squish# 21329 : Add Additional Merge Fields for Service Plan and Survey Templates 
    function GenerateReport(surveyID, planID) {
        var cboReports = document.getElementById('cboReports');

        if (cboReports.value != null && cboReports.value != "") {
            window.open('document.aspx?reportid=' + cboReports.value + '&surveyid=' + surveyID + '&serviceplanid=' + planID);
            document.location = document.URL;
        }
        else {
            alert("Select a report to generate.");
        }
    }

    </script>

    <!-- Added By Vilas Meshram: 03/04/2013: Squish# 21365: Dropdown for priority -->
    <link rel="stylesheet" type="text/css" href="<%= ARMSUtility.TemplatePath %>/MsDropdown/dd.css" />
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery-1.6.1.min.js"></script>
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery.dd.js"></script>

    <!-- Script is used to call the JQuery for dropdown -->
    <script type="text/javascript" language="javascript">
        $(document).ready(function (e) {
            try {
                $("#ddlSurveyPriority").msDropDown();
            } catch (e) {
                alert(e.message);
            }
        });
    </script>
    <!-- Added By Vilas Meshram: 03/04/2013: Squish# 21365: Dropdown for priority -->

    </form>
    </div>
</body>
</html>
