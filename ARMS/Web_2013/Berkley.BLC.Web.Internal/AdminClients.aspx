<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" CodeFile="AdminClients.aspx.cs" Inherits="AdminClientsAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxSearch" runat="server" Title="Search Criteria" width="500">
        <table class="fields">
            <tr>
                <td class="label" valign="middle" nowrap>Search By *</td>
                <td>
                    <asp:dropdownlist id="cboSearchType" runat="server" CssClass="cbo" onchange="Toggle(this.value)">
                        <asp:ListItem Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="C">Client ID</asp:ListItem>
                        <asp:ListItem Value="P">Policy Number</asp:ListItem>
                    </asp:dropdownlist>
                </td>
            </tr>
            <tr id="trClientID" runat="server" Style="display:none;">
                <td class="label" valign="middle" nowrap>Client ID *</td>
                <td><asp:TextBox ID="txtClientID" runat="Server" CssClass="txt" Columns="15"></asp:TextBox></td>
            </tr>
            <tr id="trPolicyNumber" runat="server" Style="display:none;">
                <td class="label" valign="middle" nowrap>Policy Number *</td>
                <td><asp:TextBox ID="txtPolicyNumber" runat="Server" CssClass="txt" Columns="15"></asp:TextBox></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="buttons">
                    <asp:Button id="btnSearch" Runat="server" CssClass="btn" Text="Search" />
                </td>
            </tr>
        </table>
    </cc:Box>
    
    <cc:Box id="boxResults" runat="server" Title="Results" width="500" Visible="false">
        <table class="fields">
            <uc:Label id="lblClientID" runat="server" field="Insured.ClientID" Group="View" />
            <uc:Label id="lblName" runat="server" field="Insured.Name" Group="View" />
            <uc:Label id="lblName2" runat="server" field="Insured.Name2" Group="View" />
            <uc:Label id="lblServiceAccount" runat="server" Name="Service Account" field="Agency.StreetLine1" Group="View" />
            <tr><td colspan="2">&nbsp;</td></tr>
            <asp:Repeater ID="repAttributes" Runat="server">
                <ItemTemplate>
                    <tr>
                        <td class="label" valign="top" nowrap><span><%# HtmlEncodeNullable(Container.DataItem, "Attribute.Type.Name", string.Empty, "(none)")%></span></td>
                        <td><span class="lbl"><%# GetAttributeValue(Container.DataItem) %></span></td>
                    </tr>			
                </ItemTemplate>
            </asp:Repeater>
            <tr id="trEditAttributes" runat="server">
                <td>&nbsp;</td>
                <td>
                    <br>
                    <asp:LinkButton ID="lnkEditAttributes" runat="server">Edit Client Attributes</asp:LinkButton>
                </td>
            </tr>
        </table>
    </cc:Box>
    
    <cc:Box id="boxEditResults" runat="server" Title="Results" width="500" Visible="false">
        <table class="fields">
            <uc:Label id="lblClientIDEdit" runat="server" field="Insured.ClientID" Group="Edit" />
            <uc:Label id="lblNameEdit" runat="server" field="Insured.Name" Group="Edit" />
            <uc:Label id="lblName2Edit" runat="server" field="Insured.Name2" Group="Edit" />
            <uc:Label id="lblServiceAccountEdit" runat="server" Name="Service Account" field="Agency.StreetLine1" Group="Edit" />
            <tr><td colspan="2">&nbsp;</td></tr>
            <asp:Repeater ID="repEditAttributes" Runat="server">
                <ItemTemplate>
                    <tr>
                        <td class="label" valign="top" nowrap><span><%# HtmlEncodeNullable(Container.DataItem, "Attribute.Type.Name", string.Empty, "(none)")%></span></td>
                        <td><cc:DynamicControlsPlaceholder id="pHolder" Runat="Server" /></td>
                    </tr>			
                </ItemTemplate>
            </asp:Repeater>
            <tr id="trBtns" runat="server">
                <td>&nbsp;</td>
                <td class="buttons">
                    <asp:Button id="btnSubmit" Runat="server" CssClass="btn" Text="Submit" />
                    <asp:Button id="btnCancel" Runat="server" CssClass="btn" Text="Cancel" />
                </td>
            </tr>
        </table>
    </cc:Box>
    
    <cc:Box id="boxHistory" runat="server" title="History" width="500">
        <a id="lnkHistory" href="javascript:Toggle('divHistory')">Show/Hide</a><br>
        <div id="divHistory" style="MARGIN-TOP: 10px">
        <% if( repHistory.Items.Count > 0 ) { %>
            <asp:Repeater ID="repHistory" Runat="server">
                <HeaderTemplate>
                    <table cellspacing="0" cellpadding="0" border="0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" nowrap>
                            <%# HtmlEncode(Container.DataItem, "EntryDate", "{0:g}") %>
                        </td>
                        <td style="PADDING-LEFT: 10px; PADDING-BOTTOM: 4px">
                            <%# GetEncodedHistory(Container.DataItem)%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        <% } else { %>
            (none)
        <% } %>
        </div>
        </cc:Box>
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript">
    function Toggle(cboValue)
    {
        var box = document.getElementById('boxResults');
        if (box != null) {
            box.style.display = 'none';
        }
        
        if (cboValue == 'C') {
            document.getElementById('trPolicyNumber').style.display = 'none';
            document.getElementById('trClientID').style.display = 'inline';
            document.getElementById('btnSearch').style.display = 'inline';
            document.getElementById('txtPolicyNumber').value = '';
        }
        else if (cboValue == 'P') {
            document.getElementById('trClientID').style.display = 'none';
            document.getElementById('trPolicyNumber').style.display = 'inline';
            document.getElementById('btnSearch').style.display = 'inline';
            document.getElementById('txtClientID').value = '';
        }
        else {
            document.getElementById('trPolicyNumber').style.display = 'none';
            document.getElementById('trClientID').style.display = 'none';
            document.getElementById('btnSearch').style.display = 'none';
            
            document.getElementById('txtPolicyNumber').value = '';
            document.getElementById('txtClientID').value = '';
        }
    }
    </script>
    
    </form>
</body>
</html>
