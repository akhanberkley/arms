using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web.Validation;
using QCI.Web;

public partial class ContactAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(ContactAspx_Load);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnAddAnother.Click += new EventHandler(btnAddAnother_Click);
    }
    #endregion

    protected Location _eLocation;
    protected Survey _eSurvey;
    protected LocationContact _eContact;

    void ContactAspx_Load(object sender, EventArgs e)
    {
        // register our AJAX class for use in client script
        Ajax.Utility.RegisterTypeForAjax(typeof(AjaxMethods));

        Guid gContactID = ARMSUtility.GetGuidFromQueryString("contactid", false);
        _eContact = (gContactID != Guid.Empty) ? LocationContact.Get(gContactID) : null;

        //navigate back
        Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        try
        {
            _eSurvey = Survey.Get(gSurveyID);
        }
        catch
        {
            //survey id no longer exists
            Security.RequestExpired();
        }

        Guid gLocationID = ARMSUtility.GetGuidFromQueryString("locationid", false);
        _eLocation = (gLocationID == Guid.Empty) ? _eSurvey.Location : Location.Get(gLocationID);

        chkSameAsCorporateAddress.Attributes.Add("onclick", string.Format("SameAsCorporateAddress(this.checked, '{0}')", _eSurvey.InsuredID));

        if (!this.IsPostBack)
        {
            // populate the list with company specific mail options
            ContactReasonType[] contactTypes = ContactReasonType.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            cboContactOption.DataBind(contactTypes, "Code", "LongDescription");
            cboContactOption.Visible = contactTypes.Length > 0;

            if (_eContact != null) // edit
            {
                this.PopulateFields(_eContact);
                valContactType.Enabled = false;
            }

            btnSubmit.Text = (_eContact == null) ? "Add" : "Update";
            btnAddAnother.Visible = (_eContact == null);
            base.PageHeading = (_eContact == null) ? "Add Contact" : "Update Contact";
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            LocationContact eContact;
            if (_eContact == null) // add
            {
                eContact = new LocationContact(Guid.NewGuid());

                //get the highest number and incriment by one
                LocationContact[] eContacts = LocationContact.GetSortedArray("LocationID = ?", "PriorityIndex DESC", _eLocation.ID);
                eContact.PriorityIndex = (eContacts.Length > 0) ? eContacts[0].PriorityIndex + 1 : 1;
            }
            else
            {
                eContact = _eContact.GetWritableInstance();
            }

            //keep the original name
            string contactName = eContact.Name;

            this.PopulateEntity(eContact);
            eContact.LocationID = _eLocation.ID;

            // commit all changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                if (rdoAllClientLocations.Checked) //INSERT
                {
                    //Add the new primary contact to all other locations
                    Location[] eLocations = Location.GetArray("ID != ? && InsuredID = ? && IsActive = true", _eLocation.ID, _eSurvey.InsuredID);

                    foreach (Location eLocation in eLocations)
                    {
                        LocationContact newContact = new LocationContact(Guid.NewGuid());

                        //set the defaults
                        newContact.LocationID = eLocation.ID;
                        newContact.PriorityIndex = eContact.PriorityIndex;

                        this.PopulateEntity(newContact);
                        newContact.Save(trans);
                    }
                }

                //Check if any other locations need to have this contact updated
                if (_eContact != null)
                {
                    LocationContact[] locationContacts = LocationContact.GetArray("ID != ? && Location[InsuredID = ?] && Name = ?",
                            eContact.ID, _eSurvey.InsuredID, contactName);

                    for (int i = 0; i < locationContacts.Length; i++)
                    {
                        LocationContact locationContact = locationContacts[i].GetWritableInstance();
                        int currentPriority = locationContact.PriorityIndex;
                        this.PopulateEntity(locationContact);
                        locationContact.PriorityIndex = currentPriority;
                        locationContact.Save(trans);
                    }
                }

                eContact.Save(trans);
                trans.Commit();
            }

            // update our member var so the redirect will work correctly
            _eContact = eContact;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        if (sender == bool.TrueString)
        {
            Response.Redirect(string.Format("Contact.aspx{0}", ARMSUtility.GetQueryStringForNav("contactid")));
        }
        else
        {
            Response.Redirect(GetReturnURL(), true);
        }
    }

    void btnAddAnother_Click(object sender, EventArgs e)
    {
        btnSubmit_Click(bool.TrueString, null);
    }

    protected string GetReturnURL()
    {
        Guid planID = ARMSUtility.GetGuidFromQueryString("planid", false);

        if (planID != Guid.Empty)
        {
            //Return to CreateServicePlanVisit.aspx
            return string.Format("CreateServicePlanVisit.aspx{0}", ARMSUtility.GetQueryStringForNav("contactid"));
        }
        else if (_eSurvey.Location != null && _eSurvey.CreateState != CreateState.InProgress)
        {
            //Return to Insured.aspx
            return string.Format("Insured.aspx{0}", ARMSUtility.GetQueryStringForNav("contactid"));
        }
        else
        {
            //Return to CreateSurveyLocation.aspx
            return string.Format("CreateSurveyLocation.aspx?surveyid={0}&locationid={1}", _eSurvey.ID, _eLocation.ID);
        }
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnURL(), true);
    }

    public class AjaxMethods
    {
        [Ajax.AjaxMethod()]
        public static string[] SameAsCorporateAddress(bool isChecked, string insuredID)
        {
            string[] arrResult = new string[6];

            if (isChecked)
            {
                Insured eInsured = Insured.Get(new Guid(insuredID));
                arrResult[0] = eInsured.Name;
                arrResult[1] = eInsured.StreetLine1;
                arrResult[2] = eInsured.StreetLine2;
                arrResult[3] = eInsured.City;
                arrResult[4] = eInsured.StateCode;
                arrResult[5] = eInsured.ZipCode;
            }
            else
            {
                for (int i = 0; i < arrResult.Length; i++)
                {
                    arrResult[i] = string.Empty;
                }
            }

            return arrResult;
        }
    }
}
