<%@ Page Language="C#" AutoEventWireup="false" CodeFile="RecommendationEdit.aspx.cs" ValidateRequest="false" Inherits="RecommendationEditAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Date" Src="~/Fields/DateField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><br><br>

    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxRec" runat="server" Title="Recommendation">
        <table class="fields">
            <uc:Label id="lblDateCreated" runat="server" field="SurveyRecommendation.DateCreated" />
            <uc:Text id="txtRecNumber" runat="server" field="SurveyRecommendation.RecommendationNumber" Name="Rec Number" IsRequired="true" />
            <uc:Combo id="cboRecCode" runat="server" field="Recommendation.ID" Name="Rec Code" topItemText="" isRequired="True"/>
            <uc:Text id="txtRecDescription" runat="server" field="SurveyRecommendation.EntryText" Name="Rec Description" Rows="15" style="width: 450px" IsRequired="True" EnableCheckSpelling="true" />
            <asp:HiddenField ID="hdRecDescription" runat="server" />
            <uc:Text id="txtComments" runat="server" field="SurveyRecommendation.Comments" Name="Comments" Rows="5" style="width: 450px" EnableCheckSpelling="true" />
            <uc:Combo id="cboRecClass" runat="server" field="SurveyRecommendation.RecClassificationID" Name="Rec Classification" topItemText="" isRequired="true" />
            <uc:Combo id="cboRecStatus" runat="server" field="SurveyRecommendation.RecStatusID" Name="Rec Status" topItemText="" isRequired="true" />
            <uc:Label id="lblDateCompleted" runat="server" field="SurveyRecommendation.DateCompleted" />
        </table>
    </cc:Box>
    
    
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" />
    <asp:Button ID="btnAddAnother" Runat="server" CssClass="btn" Text="Add Another" Visible="false" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />


    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript">
    function GetDescription(recID)
    {
        var result = AjaxMethods.GetDescription(recID).value;
        var currRecDescription = document.getElementById("txtRecDescription_txt").value;
        var recDescription = document.getElementById("hdRecDescription").value;

        if (currRecDescription && recDescription != currRecDescription)
        {
            var resultYesNo = confirm('Do you want to keep existing rec description?  Click OK to keep or Cancel to remove')
            if (resultYesNo == true)
                return;
        }
        document.getElementById("hdRecDescription").value = result;
        document.getElementById("txtRecDescription_txt").value = result;
        
    }

    </script>
    
    </form>
</body>
</html>
