<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" ValidateRequest="false" CodeFile="CreateSurveyLocation.aspx.cs" Inherits="CreateSurveyLocationAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px" onload="RemoveHrefs();">

<%--<link rel="stylesheet" type="text/css" media="screen" href="Template/jqModal.css"/>
<script type="text/javascript" src="Template/Javascripts/jqModal.js"></script>
<script type="text/javascript" src="Template/Javascripts/jqModalCustom.js"></script>--%>

    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
         <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/AjaxMethodsService.asmx" />
        </Services>
    </asp:ScriptManager>
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span>
    <asp:LinkButton ID="lnkBackToInsured" runat="server" CausesValidation="true" CssClass="nav"><< Back to Insured</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|
    <asp:LinkButton ID="lnkBackToInsuredCancel" runat="server" CausesValidation="false" CssClass="nav">Cancel</asp:LinkButton><br><br>

    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxLocation" runat="server" Title="Location" width="700">
        <table class="fields">
            <uc:Check id="chkSameAsInsuredAddress" Name="Same as Insured Address" runat="server" field="" />
            <uc:Number id="numLocationNumber" runat="server" field="Location.Number" Columns="10" IsRequired="True" />
            <uc:Text id="txtStreetLine1" runat="server" field="Location.StreetLine1" Name="Address 1" Columns="70" IsRequired="True" />
            <uc:Text id="txtStreetLine2" runat="server" field="Location.StreetLine2" Name="Address 2" Columns="70" />
            <uc:Text id="txtCity" runat="server" field="Location.City" IsRequired="True" />
            <uc:Combo id="cboInsuredState" runat="server" field="Location.StateCode" Name="State" topItemText="" isRequired="True" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" />
            <uc:Text id="txtZipCode" runat="server" field="Location.ZipCode" IsRequired="True" />
            <uc:Text id="txtLocationDescription" runat="server" field="Location.Description" Name="Location Description" Rows="1" Directions="(ex. 'Apartment Complex')" Columns="63" />
        </table>
    </cc:Box>

    <cc:Box id="boxPolicies" runat="server" Title="Policies / Coverages" width="700">
        <asp:datagrid ID="grdPolicies" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="True">
	        <headerstyle CssClass="header" />
	        <itemstyle CssClass="item" />
	        <alternatingitemstyle CssClass="altItem" />
	        <footerstyle CssClass="footer" />
	        <columns>
	            <asp:TemplateColumn HeaderText="Policy #" SortExpression="Number ASC">
			        <ItemTemplate>
				        <asp:LinkButton ID="lnkPolicyDetails" runat="server" CausesValidation="true">
                                <%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(none)") %>
                        </asp:LinkButton>
			        </ItemTemplate>
		        </asp:TemplateColumn>
		        <asp:TemplateColumn HeaderText="Mod" SortExpression="Mod ASC">
			        <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "Mod", Int32.MinValue, "(none)") %>
			        </ItemTemplate>
		        </asp:TemplateColumn>
		        <asp:TemplateColumn HeaderText="Symbol" SortExpression="Symbol ASC">
			        <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "Symbol", string.Empty, "(none)") %>
			        </ItemTemplate>
		        </asp:TemplateColumn>
		        <asp:TemplateColumn HeaderText="LOB" SortExpression="LineOfBusinessCode ASC">
			        <ItemTemplate>
                        <%# HtmlEncode(Container.DataItem, "LineOfBusiness.Name") %>
			        </ItemTemplate>
		        </asp:TemplateColumn>
		        <asp:TemplateColumn HeaderText="Hazard Grade" SortExpression="HazardGrade ASC">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "HazardGrade", string.Empty, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
		        <asp:TemplateColumn HeaderText="Effective" ItemStyle-HorizontalAlign="Right" SortExpression="EffectiveDate ASC">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "EffectiveDate", "{0:d}", DateTime.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
		        <asp:TemplateColumn HeaderText="Expiration" ItemStyle-HorizontalAlign="Right" SortExpression="ExpireDate ASC">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "ExpireDate", "{0:d}", DateTime.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
		        <asp:TemplateColumn HeaderText="Premium" SortExpression="Premium ASC">
			        <ItemTemplate>
                    <% if (CurrentUser.Company.DisplayPolicyPremium) { %>
				        <%# HtmlEncodeNullable(Container.DataItem, "Premium", "{0:C}", Decimal.MinValue, "(none)")%>
                    <% } else { %>
                        NA
                    <% } %>
			        </ItemTemplate>
		        </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Primary State" SortExpression="StateCode ASC">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "StateCode", string.Empty, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
		        <asp:TemplateColumn HeaderText="Reporting">
			        <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
			        <ItemTemplate>
			                <asp:LinkButton ID="lnkReportStatus" runat="server" CausesValidation="true">
                                <img ID="imgReportStatus" border="0" runat="server" />
                            </asp:LinkButton>
				    </ItemTemplate>
		        </asp:TemplateColumn>
		        <asp:TemplateColumn HeaderText="Include" >
			        <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
			        <ItemTemplate>
			            <asp:CheckBox ID="chkPolicy" runat="server"></asp:CheckBox> 
				    </ItemTemplate>
		        </asp:TemplateColumn>
		        <asp:TemplateColumn>
		            <ItemTemplate>
			            &nbsp;<asp:LinkButton ID="btnRemovePolicy" Runat="server" CommandName="Remove" OnPreRender="btnRemovePolicy_PreRender" CausesValidation="true">Remove</asp:LinkButton>&nbsp;
		            </ItemTemplate>
	            </asp:TemplateColumn>
	        </columns>
        </asp:datagrid>
        
        <asp:LinkButton ID="lnkAddLob" runat="server">Add Policy</asp:LinkButton>
    </cc:Box>
    
    <% if (CurrentUser.Company.SupportsBuildings) { %>
    <cc:Box id="boxBuildings" runat="server" Title="Buildings" width="700">
        <asp:datagrid ID="grdBuildings" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="True">
	        <headerstyle CssClass="header" />
	        <itemstyle CssClass="item" />
	        <alternatingitemstyle CssClass="altItem" />
	        <footerstyle CssClass="footer" />
	        <columns>
		        <asp:TemplateColumn HeaderText="Building Number" SortExpression="Number ASC">
	                <ItemTemplate>
		                <asp:LinkButton ID="lnkBuildingDetails" runat="server">
                                <%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(none)").PadLeft(3,'0')%>
                        </asp:LinkButton>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Year Built" SortExpression="YearBuilt ASC">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "YearBuilt", Int32.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Sprinkler System" SortExpression="HasSprinklerSystem ASC">
	                <ItemTemplate>
		                <%# GetBoolLabel(HtmlEncode(Container.DataItem, "HasSprinklerSystem"))%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Sprinkler System Credit" SortExpression="HasSprinklerSystemCredit ASC">
	                <ItemTemplate>
		                <%# GetBoolLabel(HtmlEncode(Container.DataItem, "HasSprinklerSystemCredit"))%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Public Protection" SortExpression="PublicProtection ASC">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "PublicProtection", Int32.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Building Value" SortExpression="Value ASC">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "Value", "{0:C0}", Decimal.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Building Contents" SortExpression="Contents ASC">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "Contents", "{0:C0}", Decimal.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Stock Values" SortExpression="StockValues ASC">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "StockValues", "{0:C0}", Decimal.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Business Interruption" SortExpression="BusinessInterruption ASC">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "BusinessInterruption", "{0:C0}", Decimal.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Location Occupancy" SortExpression="LocationOccupancy ASC">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "LocationOccupancy", string.Empty, "(none)") %>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Valuation Type" SortExpression="ValuationTypeCode ASC">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "ValuationTypeCode", string.Empty, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Coinsurance" SortExpression="CoinsurancePercentage ASC" HeaderStyle-VerticalAlign="top">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "CoinsurancePercentage", "{0:P1}", Decimal.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Change in Env. Control" SortExpression="ChangeInEnvironmentalControl ASC" HeaderStyle-VerticalAlign="top">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "ChangeInEnvironmentalControl", "{0:C0}", Decimal.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Scientific Animals" SortExpression="ScientificAnimals ASC" HeaderStyle-VerticalAlign="top">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "ScientificAnimals", "{0:C0}", Decimal.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Contamination" SortExpression="Contamination ASC" HeaderStyle-VerticalAlign="top">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "Contamination", "{0:C0}", Decimal.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Radio Active Contamination" SortExpression="RadioActiveContamination ASC" HeaderStyle-VerticalAlign="top">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "RadioActiveContamination", "{0:C0}", Decimal.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Flood" SortExpression="Flood ASC" HeaderStyle-VerticalAlign="top">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "Flood", "{0:C0}", Decimal.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Earthquake" SortExpression="Earthquake ASC" HeaderStyle-VerticalAlign="top">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "Earthquake", "{0:C0}", Decimal.MinValue, "(none)")%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="ITV" SortExpression="ITV ASC">
	                <ItemTemplate>
		                <%# GetBoolLabel(HtmlEncode(Container.DataItem, "ITV"))%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
		            <ItemTemplate>
			            &nbsp;<asp:LinkButton ID="btnRemoveBuilding" Runat="server" CommandName="Remove" OnPreRender="btnRemoveBuilding_PreRender" CausesValidation="True">Remove</asp:LinkButton>&nbsp;
		            </ItemTemplate>
	            </asp:TemplateColumn>
	        </columns>
        </asp:datagrid>
        
        <asp:LinkButton ID="lnkAddBuilding" runat="server">Add Building</asp:LinkButton>
        
        <%--<a id="lnkTest" runat="server" class="thickbox" title="test">Add Building</a>
	
	<div id="modalWindow" class="jqmWindow">
        <iframe id="jqmContent" src="" frameborder="0"></iframe>
    </div>--%>
    </cc:Box>
    <% } %>
    
    <cc:Box id="boxContacts" runat="server" Title="Contacts" width="700">
        * Note: Make sure to rank contacts by importance - #1 being primary.<br><br>
        <asp:datagrid ID="grdContacts" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
	        <headerstyle CssClass="header" />
	        <itemstyle CssClass="item" />
	        <alternatingitemstyle CssClass="altItem" />
	        <footerstyle CssClass="footer" />
	        <columns>
	            <asp:BoundColumn HeaderText="Rank" DataField="PriorityIndex" SortExpression="PriorityIndex ASC" />
	            <asp:TemplateColumn HeaderText="Name" SortExpression="Name ASC">
			        <ItemTemplate>
				        <asp:LinkButton ID="lnkContactName" runat="server">
                                <%# HtmlEncodeNullable(Container.DataItem, "Name", string.Empty, "(none)") %>
                        </asp:LinkButton>
			        </ItemTemplate>
		        </asp:TemplateColumn>
		        <asp:TemplateColumn HeaderText="Title">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "Title", string.Empty, "(none)") %>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Phone">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "HtmlPhoneNumber", string.Empty, "(none)") %>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Email">
	                <ItemTemplate>
		                <%# HtmlEncodeNullable(Container.DataItem, "EmailAddress", string.Empty, "(none)") %>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Mail Options">
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "ContactReasonType.ShortDescription", null, "(none)") %>
	                    </ItemTemplate>
                </asp:TemplateColumn>
		        <asp:TemplateColumn HeaderText="Move">
		            <ItemTemplate>
			            &nbsp;<asp:LinkButton ID="btnUp" Runat="server" CommandName="MoveUp">Up</asp:LinkButton>&nbsp;/
			            &nbsp;<asp:LinkButton ID="btnDown" Runat="server" CommandName="MoveDown">Down</asp:LinkButton>&nbsp;
		            </ItemTemplate>
	            </asp:TemplateColumn>
	            <asp:TemplateColumn>
		            <ItemTemplate>
			            &nbsp;<asp:LinkButton ID="btnRemoveContact" Runat="server" CommandName="Remove" OnPreRender="btnRemoveContact_PreRender" CausesValidation="true">Remove</asp:LinkButton>&nbsp;
		            </ItemTemplate>
	            </asp:TemplateColumn>
	        </columns>
        </asp:datagrid>
        <asp:LinkButton ID="lnkAddContact" runat="server">Add Contact</asp:LinkButton>
    </cc:Box>
    
    <cc:Box id="boxComments" runat="server" Title="Location Specific Comments" width="700">
        <table class="fields">
            <uc:Text id="txtComments" runat="server" field="Agency.StreetLine1" Name="Comments" style="width: 500px" Rows="4" EnableCheckSpelling="true" />
       </table>
    </cc:Box>
    
    <span class="heading"><%= base.PageHeading %></span><asp:LinkButton ID="lnkBackToInsured2" runat="server" CausesValidation="true" CssClass="nav"><< Back to Insured</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|
    <asp:LinkButton ID="lnkBackToInsuredCancel2" runat="server" CausesValidation="false" CssClass="nav">Cancel</asp:LinkButton>
    
    <uc:HintFooter ID="ucHintFooter" runat="server" VerticalOffset="130" />
    <uc:Footer ID="ucFooter" runat="server" />
    
        <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery-1.4.2.js"></script>
    <script type="text/javascript">
   
        function SetExclusion(isChecked, surveyID, locationID, policyEffectiveDate,companyID, lnkID, imgID,chkID) {

            var today = new Date();
            var effectiveDate = new Date(policyEffectiveDate);
            
            
         
            if ((isChecked == true) && (effectiveDate > today) && ("<%= _validatePolicyEffectiveDate %>"))
            {
                alert("Cannot include this Policy on Survey as it is not effective yet.");
                document.getElementById(chkID).checked = false;
                return;
            }
            var lnk = document.getElementById(lnkID);
            var img = document.getElementById(imgID);
           

            AjaxMethodsService.ExclusionCheck(isChecked, surveyID, locationID, companyID, function (result, eventArgs) {
                img.setAttribute('src', result);

                if (!isChecked) {
                    var href = lnk.getAttribute("href");
                    if (href && href != "" && href != null) {
                        lnk.setAttribute('href_bak', href);
                    }
                    lnk.removeAttribute('href');
                }
                else {
                    var href_bak = lnk.getAttribute("href_bak");
                    if (href_bak && href_bak != "" && href_bak != null) {
                        lnk.setAttribute('href', lnk.attributes['href_bak'].nodeValue);
                    }
                }
            });
        }
    function RemoveHrefs() { 
		arrLinks = new Array();
		arrLinks = frm.getElementsByTagName('a');
		
		for (i=0; i<arrLinks.length; i++) { 
			var att = arrLinks[i].getAttribute("flagHref");
			if (att != null && att == "1"){
			    var href = arrLinks[i].getAttribute("href");
			    if(href && href != "" && href != null){
			        arrLinks[i].setAttribute('href_bak', href);
			    }
			    arrLinks[i].removeAttribute('href');
			}
		}
	}
	function SameAsInsuredAddress(isChecked, insuredID) {
	    var arrFields = AjaxMethods.SameAsInsuredAddress(isChecked, insuredID).value;
	   
	    document.getElementById("txtStreetLine1_txt").value = arrFields[0];
        document.getElementById("txtStreetLine2_txt").value = arrFields[1];
        document.getElementById("txtCity_txt").value = arrFields[2];
        document.getElementById("cboInsuredState_cbo").value = arrFields[3];
        document.getElementById("txtZipCode_txt").value = arrFields[4];
	}

	$(document).ready(function () {
	   
	    $('select').keypress(function (event)
	    { return cancelBackspace(event) });
	    $('select').keydown(function (event)
	    { return cancelBackspace(event) });
         });

    function cancelBackspace(event) {
        if (event.keyCode == 8) {
            return false;
        }
    }
    </script>
    </form>
</body>
</html>
