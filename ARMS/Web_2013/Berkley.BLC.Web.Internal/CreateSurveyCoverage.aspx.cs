using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web.Validation;
using Berkley.BLC.Web.Internal.Controls;

public partial class CreateSurveyCoverageAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(CreateSurveyCoverageAspx_Load);
        this.PreRender += new EventHandler(CreateSurveyCoverageAspx_PreRender);
        this.repCoverageNames.ItemDataBound += new RepeaterItemEventHandler(repCoverageNames_ItemDataBound);
        this.repOptionalCoverageNames.ItemDataBound += new RepeaterItemEventHandler(repOptionalCoverageNames_ItemDataBound);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
    }
    #endregion

    protected Policy _ePolicy;
    protected Location _eLocation;
    protected Survey _eSurvey;
    protected bool _isExistingSurvey = false;
    protected ReportTypeCompany[] _reportTypes;

    protected bool _isCoverageDetails = false;


    void CreateSurveyCoverageAspx_Load(object sender, EventArgs e)
    {
        Guid gPolicyID = ARMSUtility.GetGuidFromQueryString("policyid", true);
        _ePolicy = Policy.Get(gPolicyID);

        Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        try
        {
            _eSurvey = Survey.Get(gSurveyID);
        }
        catch
        {
            //survey id no longer exists
            Security.RequestExpired();
        }

        _isExistingSurvey = (_eSurvey.Location != null && _eSurvey.CreateState != CreateState.InProgress);

        Guid gLocationID = ARMSUtility.GetGuidFromQueryString("locationid", false);
        _eLocation = (gLocationID != Guid.Empty) ? Location.Get(gLocationID) : _eSurvey.Location;

        if (_ePolicy.Number.Length > 0 && _ePolicy.Mod > 0)
        {
            base.PageHeading = string.Format("{0}: ({1}-{2})", _ePolicy.LineOfBusiness.Name, _ePolicy.Number, _ePolicy.Mod);
        }
        else
        {
            base.PageHeading = _ePolicy.LineOfBusiness.Name;
        }
    }

    void CreateSurveyCoverageAspx_PreRender(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if ((Berkley.BLC.Business.Utility.GetCompanyParameter("UsesCompanyReportTypes", CurrentUser.Company).ToUpper() == "TRUE") ? true : false)
                _reportTypes = ReportTypeCompany.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            else
                _reportTypes = ReportTypeCompany.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", Guid.Empty);
            
            //Get the coverage names based on the lob and company
            //CoverageName[] eCoverageNames;
            CoverageName[] eOptionalCoverageNames;
            if (_isExistingSurvey) //Updating from the insured.aspx screen
            {
                eOptionalCoverageNames = CoverageName.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && LocationID = ? && PolicyID = ?] && (IsRequired = true && TypeCode != ?)", "DisplayOrder ASC", _eSurvey.ID, _eLocation.ID, _ePolicy.ID, CoverageNameType.Property.Code);
            }
            else //From creating a new survey request
            {
                eOptionalCoverageNames = CoverageName.GetSortedArray("LineOfBusinessCode = ? && CompanyID = ? && (IsRequired = true && TypeCode != ?)  && MigrationOnly = false", "DisplayOrder ASC", _ePolicy.LineOfBusinessCode, UserIdentity.Current.CompanyID, CoverageNameType.Property.Code);
            }

            repCoverageNames.DataSource = GetCoveragesForPolicy();
            repCoverageNames.DataBind();

            repOptionalCoverageNames.DataSource = eOptionalCoverageNames;
            repOptionalCoverageNames.DataBind();

        }
    }

    protected CustomCoverageName[] GetCoveragesForPolicy()
    {
        //Gets a little tricky here because we are using one screen that points to two seperate tables and two different instances of workflow
        VWExistingCoverage[] existingCoverages = null;
        VWNewCoverage[] newCoverages = null;
        int count = 0;
        
        if (_isExistingSurvey)
        {
            existingCoverages = VWExistingCoverage.GetSortedArray("SurveyID = ? && LocationID = ? && PolicyID = ? && (CoverageNameIsRequired = False || CoverageNameTypeCode = ?)", "CoverageNameDisplayOrder DESC, CoverageValueDisplayOrder ASC", _eSurvey.ID, _eLocation.ID, _ePolicy.ID, CoverageNameType.Property.Code);
            count = existingCoverages.Length;
        }
        else
        {
            newCoverages = VWNewCoverage.GetSortedArray("SurveyID = ? && LocationID = ? && PolicyID = ?", "CoverageNameDisplayOrder DESC, CoverageValueDisplayOrder ASC", _eSurvey.ID, _eLocation.ID, _ePolicy.ID);
            count = newCoverages.Length;
        }

        SortedList list = new SortedList();
        for (int i = 0; i < count; i++ )
        {
            Guid coverageNameID = (_isExistingSurvey) ? existingCoverages[i].CoverageNameID : newCoverages[i].CoverageNameID;
            string coverageNameTypeName = (_isExistingSurvey) ? existingCoverages[i].CoverageNameTypeName : newCoverages[i].CoverageNameTypeName;
            string coverageNameTypeCode = (_isExistingSurvey) ? existingCoverages[i].CoverageNameTypeCode : newCoverages[i].CoverageNameTypeCode;
            string reportTypeCode = (_isExistingSurvey) ? existingCoverages[i].ReportTypeCode : newCoverages[i].ReportTypeCode;
            int coverageNameDisplayOrder = (_isExistingSurvey) ? existingCoverages[i].CoverageNameDisplayOrder : newCoverages[i].CoverageNameDisplayOrder;
            
            CustomCoverageName customCoverageName;
            if (!list.ContainsKey(coverageNameDisplayOrder))
            {
                customCoverageName = new CustomCoverageName(coverageNameID, coverageNameTypeName, coverageNameTypeCode, reportTypeCode);
            }
            else
            {
                customCoverageName = list[coverageNameDisplayOrder] as CustomCoverageName;
            }

            string coverageTypeName = (_isExistingSurvey) ? existingCoverages[i].CoverageTypeName : newCoverages[i].CoverageTypeName;
            if (!string.IsNullOrEmpty(coverageTypeName))
            {
                Guid coverageID = (_isExistingSurvey) ? existingCoverages[i].CoverageID : newCoverages[i].CoverageID;
                string coverageValue = (_isExistingSurvey) ? existingCoverages[i].CoverageValue : newCoverages[i].CoverageValue;
                string dotNetDataType = (_isExistingSurvey) ? existingCoverages[i].DotNetDataType : newCoverages[i].DotNetDataType;
                
                CustomCoverage customCoverage = new CustomCoverage(coverageID, coverageTypeName, coverageValue, dotNetDataType);
                customCoverageName.CustomCoverages.Add(customCoverage);
            }

            list.Remove(coverageNameDisplayOrder);
            list.Add(coverageNameDisplayOrder, customCoverageName);
        }

        CustomCoverageName[] results = new CustomCoverageName[list.Count];
        list.Values.CopyTo(results, 0);

        return results;
    }

    protected CustomCoverage[] GetValuesForCoverage(object dataItem)
    {
        return (dataItem as CustomCoverageName).CustomCoverages.ToArray();
    }

    protected CustomCoverageValue[] GetRowsForCoverageValue(object dataItem)
    {
        CustomCoverage eCoverage = (CustomCoverage)dataItem;
        ArrayList list = new ArrayList();

        //Break out the concatenated coverage names and values
        string[] arrNames = eCoverage.DisplayCoverageType.Split('|');
        string[] arrValues = eCoverage.DisplayCoverageValue.Split('|');
        for (int i = 0; i < arrNames.Length; i++)
        {
            CustomCoverageValue eValue = new CustomCoverageValue(eCoverage.CoverageID, arrNames[i], (arrValues.Length == 1) ? arrValues[0] : arrValues[i], eCoverage.DotNetDataType);
            list.Add(eValue);
        }

        //Added By Vilas Meshrram: 04/10/2013 : Squish# #21360 Vehical Schedule
        Coverage eCov = Coverage.Get(eCoverage.CoverageID).GetWritableInstance();
        //if (eCov.isCoverageDetails)
        //    _isCoverageDetails = true;
        //else
        //    _isCoverageDetails = false;

        return list.ToArray(typeof(CustomCoverageValue)) as CustomCoverageValue[];
    }

    void repCoverageNames_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the controls
            DropDownList cboReportType = (DropDownList)e.Item.FindControl("cboReportType");

            QCI.Web.ComboHelper.BindCombo(cboReportType, _reportTypes, "Code", "Name");
            //cboReportType.Items.Insert(0, new ListItem("", ""));

            // select the selected value
            CustomCoverageName coverageName = (CustomCoverageName)e.Item.DataItem;
            cboReportType.Attributes.Add("CoverageNameID", coverageName.CoverageNameID.ToString());

            //Business rule, Umbrellas are not reportable
            HtmlTableRow rowReport = (HtmlTableRow)e.Item.FindControl("rowReportValue");
            rowReport.Visible = (coverageName.CoverageNameTypeCode != CoverageNameType.CommercialUmbrella.Code);
            cboReportType.SelectedValue = coverageName.ReportTypeCode;
        }
    }

    void repOptionalCoverageNames_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the controls
            DropDownList cboReportType = (DropDownList)e.Item.FindControl("cboReportType");
            ReportType[] eReportTypes = ReportType.GetSortedArray("", "DisplayOrder ASC");

            QCI.Web.ComboHelper.BindCombo(cboReportType, eReportTypes, "Code", "Name");

            // select the selected value
            CoverageName eCoverageName = (CoverageName)e.Item.DataItem;
            cboReportType.Attributes.Add("CoverageNameID", eCoverageName.ID.ToString());

            // get the selected value
            if (_isExistingSurvey)
            {
                SurveyPolicyCoverage ePolicyCoverage = SurveyPolicyCoverage.GetOne("SurveyID = ? && LocationID = ? && PolicyID = ? && CoverageNameID = ?", _eSurvey.ID, _eLocation.ID, _ePolicy.ID, eCoverageName.ID);
                cboReportType.SelectedValue = (ePolicyCoverage != null) ? ePolicyCoverage.ReportTypeCode : ReportType.NoReport.Code;
            }
            else
            {
                SurveyLocationPolicyCoverageName eLocationPolicyCoverage = SurveyLocationPolicyCoverageName.GetOne("SurveyID = ? && LocationID = ? && PolicyID = ? && CoverageNameID = ?", _eSurvey.ID, _eLocation.ID, _ePolicy.ID, eCoverageName.ID);
                cboReportType.SelectedValue = (eLocationPolicyCoverage != null) ? eLocationPolicyCoverage.ReportTypeCode : ReportType.NoReport.Code;
            }
        }
    }

    protected void repCoverageValueRows_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            CustomCoverageValue coverage = (CustomCoverageValue)e.Item.DataItem;

            TextBox txtProspectValue = (TextBox)e.Item.FindControl("txtProspectValue");
            txtProspectValue.Attributes.Add("CoverageID", coverage.ID.ToString());
            txtProspectValue.Text = coverage.Value;

            //enforce the required value type
            if (coverage.DotNetDataType == "Decimal")
            {
                CompareValidator valValueType = (CompareValidator)e.Item.FindControl("valProspectValueType");
                valValueType.Type = ValidationDataType.Currency;
                valValueType.ErrorMessage = string.Format("{0} needs to be in a currency format.", coverage.Name);
            }
            else if (coverage.DotNetDataType == "Int32")
            {
                CompareValidator valValueType = (CompareValidator)e.Item.FindControl("valProspectValueType");
                valValueType.Type = ValidationDataType.Integer;
                valValueType.ErrorMessage = string.Format("{0} needs to be in a valid number.", coverage.Name);
            }

        }
    }

    protected string GetCoverageName(object dataItem)
    {
        CoverageName eCoverageName = (dataItem as CoverageName);

        string result;
        if (eCoverageName.SubLineOfBusiness != null)
        {
            result = string.Format("{0} ({1})", eCoverageName.Type.Name, eCoverageName.SubLineOfBusiness.Name);
        }
        else
        {
            result = eCoverageName.Type.Name;
        }

        return result;
    }

    protected string UrlBuilder(string sWebPage)
    {
        return UrlBuilder(sWebPage, null, null);
    }

    protected string UrlBuilder(string sWebPage, string sKey, object oValue)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat(sWebPage);
        sb.AppendFormat("?surveyid={0}", _eSurvey.ID);
        sb.AppendFormat("&locationid={0}", _eLocation.ID);

        if (sKey != null && sKey.Length > 0)
        {
            sb.AppendFormat("&{0}={1}", sKey, oValue);
        }

        return sb.ToString();
    }

    //Added By Vilas Meshrram: 04/15/2013 : Squish# #21360 Vehical Schedule
    protected string GetCoverageDetails(object dataItem)
    {
        CustomCoverageValue myCustomeCoverage = (CustomCoverageValue)dataItem;
        CoverageDetail[] mydetails = GetCoverageDetailsForPolicy(dataItem);

        if (mydetails.Length > 0)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("    <td class=\"label\" valign=\"top\">");

            string sLink = string.Empty;
            //sLink = string.Format("            <a class=\"lnkPolicy\" href=\"javascript:Toggle('{0}')\"> Coverage Details </a>", "row" + myCustomeCoverage.CoverageID);
            sLink = string.Format("           <img src=\"./Images/plus.png\" OnClick=\"ToggleExpandCollaspe(this,'{0}')\" />", "row" + myCustomeCoverage.ID);
            sb.AppendLine(sLink);

            sb.AppendLine("    </td>");
            sb.AppendLine("     <tr id=\"" + "row" + myCustomeCoverage.ID + "\" runat=\"server\" style=\"DISPLAY: none\">");
            sb.AppendLine("        <td></td>");
            sb.AppendLine("        <td>");
            sb.AppendLine("           <table");
            sb.AppendLine("             <asp:Repeater ID=\"repCoverageDetails\" runat=\"server\"  DataSource='" + mydetails + "'>");
            sb.AppendLine("                 <ItemTemplate>");
            sb.AppendLine("                     <tr id=\"rowCoverageDetailsValue\"> ");
            sb.AppendLine("                         <td> </td>");
            sb.AppendLine("                         <td class=\"lbl\" valign=\"top\"  nowrap style=\"PADDING-RIGHT: 1px\">");

            for (int i = 0; i < mydetails.Length; i++)
            {
                sb.Append(GetEncodedCoverageDetailValue(mydetails[i]) + "<br/>");
            }

            sb.AppendLine("                         </td>");
            sb.AppendLine("                     </tr>");
            sb.AppendLine("                 </ItemTemplate>");
            sb.AppendLine("             </asp:Repeater>");
            sb.AppendLine("           </table>");
            sb.AppendLine("        </td>");
            sb.AppendLine("     </tr>");

            return sb.ToString();
        }
        else
        {
            return string.Empty;
        }


    }

    //Added By Vilas Meshrram: 04/10/2013 : Squish# 21360 Vehical Schedule
    protected CoverageDetail[] GetCoverageDetailsForPolicy(object dataItem)
    {
        CustomCoverageValue eCoverage = (CustomCoverageValue)dataItem;

        CoverageDetail[] coverageDetails = CoverageDetail.GetSortedArray("CoverageID = ?", "Value ASC", eCoverage.ID);
        return coverageDetails;
    }

    //Added By Vilas Meshrram: 04/10/2013 : Squish# 21360 Vehical Schedule
    protected string GetEncodedCoverageDetailValue(object dataItem)
    {

        string result = (dataItem as CoverageDetail).Value;

        // html encode the string (must do this first)

        result = Server.HtmlEncode(result);

        // convert '|' characters
        result = result.Replace("|", "<br>");

        return result;
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        List<Coverage> list1 = new List<Coverage>();
        List<SurveyPolicyCoverage> list2 = new List<SurveyPolicyCoverage>();
        List<SurveyLocationPolicyCoverageName> list3 = new List<SurveyLocationPolicyCoverageName>();

        //only save updates for non umbrella lines of business
        if (_ePolicy.LineOfBusiness != LineOfBusiness.CommercialUmbrella)
        {
            try
            {
                //Update Coverages
                foreach (RepeaterItem coverageNameItem in repCoverageNames.Items)
                {
                    if (coverageNameItem.ItemType == ListItemType.Item || coverageNameItem.ItemType == ListItemType.AlternatingItem)
                    {
                        DropDownList cboReportType = (DropDownList)coverageNameItem.FindControl("cboReportType");
                        Repeater repCoverageValues = (Repeater)coverageNameItem.FindControl("repCoverageValues");
                        Guid gCoverageNameID = new Guid(cboReportType.Attributes["CoverageNameID"]);
                        CoverageName eCoverageName = CoverageName.Get(gCoverageNameID);

                        //update the values first
                        foreach (RepeaterItem coverageItem in repCoverageValues.Items)
                        {
                            if (coverageItem.ItemType == ListItemType.Item || coverageItem.ItemType == ListItemType.AlternatingItem)
                            {
                                Repeater repCoverageValueRows = (Repeater)coverageItem.FindControl("repCoverageValueRows");
                                Coverage eCoverage = null;
                                Guid gCoverageID = Guid.Empty;
                                string result = string.Empty;

                                foreach (RepeaterItem coverageRowItem in repCoverageValueRows.Items)
                                {
                                    if (coverageRowItem.ItemType == ListItemType.Item || coverageRowItem.ItemType == ListItemType.AlternatingItem)
                                    {
                                        TextBox txtProspectValue = (TextBox)coverageRowItem.FindControl("txtProspectValue");
                                        gCoverageID = new Guid(txtProspectValue.Attributes["CoverageID"]);
                                        result += txtProspectValue.Text + "|";
                                    }
                                }

                                eCoverage = Coverage.Get(gCoverageID).GetWritableInstance();
                                if (result.EndsWith("|"))
                                {
                                    eCoverage.Value = result.Remove(result.Length - 1);
                                }

                                //commit
                                //eCoverage.Save();

                                list1.Add(eCoverage);
                            }
                        }

                        if (cboReportType.Visible || eCoverageName.Type == CoverageNameType.CommercialUmbrella)
                        {
                            if (_isExistingSurvey)
                            {
                                SurveyPolicyCoverage ePolicyCoverage = SurveyPolicyCoverage.GetOne("SurveyID = ? && LocationID = ? && PolicyID = ? && CoverageNameID = ?", _eSurvey.ID, _eLocation.ID, _ePolicy.ID, gCoverageNameID);
                                ePolicyCoverage = ePolicyCoverage.GetWritableInstance();
                                ePolicyCoverage.ReportTypeCode = cboReportType.SelectedValue;
                                //ePolicyCoverage.Save();

                                list2.Add(ePolicyCoverage);
                            }
                            else
                            {
                                SurveyLocationPolicyCoverageName eLocationPolicyCoverage = SurveyLocationPolicyCoverageName.GetOne("SurveyID = ? && LocationID = ? && PolicyID = ? && CoverageNameID = ?", _eSurvey.ID, _eLocation.ID, _ePolicy.ID, gCoverageNameID);
                                if (eLocationPolicyCoverage == null) //insert
                                {
                                    eLocationPolicyCoverage = new SurveyLocationPolicyCoverageName(_eSurvey.ID, _eLocation.ID, _ePolicy.ID, gCoverageNameID);
                                }
                                else //update
                                {
                                    eLocationPolicyCoverage = eLocationPolicyCoverage.GetWritableInstance();
                                }

                                eLocationPolicyCoverage.ReportTypeCode = cboReportType.SelectedValue;

                                //commit
                                //eLocationPolicyCoverage.Save();

                                list3.Add(eLocationPolicyCoverage);
                            }
                        }
                    }
                }

                //optional report items
                foreach (RepeaterItem optionalCoverageNameItem in repOptionalCoverageNames.Items)
                {
                    if (optionalCoverageNameItem.ItemType == ListItemType.Item || optionalCoverageNameItem.ItemType == ListItemType.AlternatingItem)
                    {
                        DropDownList cboReportType = (DropDownList)optionalCoverageNameItem.FindControl("cboReportType");
                        Repeater repCoverageValues = (Repeater)optionalCoverageNameItem.FindControl("repCoverageValues");
                        Guid gCoverageNameID = new Guid(cboReportType.Attributes["CoverageNameID"]);
                        CoverageName eCoverageName = CoverageName.Get(gCoverageNameID);

                        if (cboReportType.Visible || eCoverageName.Type == CoverageNameType.CommercialUmbrella)
                        {
                            if (_isExistingSurvey)
                            {
                                SurveyPolicyCoverage ePolicyCoverage = SurveyPolicyCoverage.GetOne("SurveyID = ? && LocationID = ? && PolicyID = ? && CoverageNameID = ?", _eSurvey.ID, _eLocation.ID, _ePolicy.ID, gCoverageNameID);
                                ePolicyCoverage = ePolicyCoverage.GetWritableInstance();
                                ePolicyCoverage.ReportTypeCode = cboReportType.SelectedValue;
                                //ePolicyCoverage.Save();

                                list2.Add(ePolicyCoverage);
                            }
                            else
                            {
                                SurveyLocationPolicyCoverageName eLocationPolicyCoverage = SurveyLocationPolicyCoverageName.GetOne("SurveyID = ? && LocationID = ? && PolicyID = ? && CoverageNameID = ?", _eSurvey.ID, _eLocation.ID, _ePolicy.ID, gCoverageNameID);
                                if (eLocationPolicyCoverage == null) //insert
                                {
                                    eLocationPolicyCoverage = new SurveyLocationPolicyCoverageName(_eSurvey.ID, _eLocation.ID, _ePolicy.ID, gCoverageNameID);
                                }
                                else //update
                                {
                                    eLocationPolicyCoverage = eLocationPolicyCoverage.GetWritableInstance();
                                }

                                eLocationPolicyCoverage.ReportTypeCode = cboReportType.SelectedValue;

                                //commit
                                //eLocationPolicyCoverage.Save();

                                list3.Add(eLocationPolicyCoverage);
                            }
                        }
                    }
                }

                using (Transaction trans = DB.Engine.BeginTransaction())
                {
                    //Now that we have all the lists to be saved, persist the changes
                    foreach (Coverage coverage in list1)
                    {
                        coverage.Save(trans);
                    }

                    foreach (SurveyPolicyCoverage spc in list2)
                    {
                        spc.Save(trans);
                    }

                    foreach (SurveyLocationPolicyCoverageName slpcn in list3)
                    {
                        slpcn.Save(trans);
                    }

                    if (!_isExistingSurvey)
                    {
                        //flag this policy as being viewed
                        SurveyLocationPolicyExclusion surveyPolicy = SurveyLocationPolicyExclusion.Get(_eSurvey.ID, _eLocation.ID, _ePolicy.ID).GetWritableInstance();
                        surveyPolicy.PolicyViewed = true;
                        surveyPolicy.Save(trans);
                    }

                    trans.Commit();
                }
            }
            catch (ValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }
            catch (FieldValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }
        }

        // return the user to the location
        Response.Redirect(GetReturnURL(), true);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnURL(), true);
    }

    private string GetReturnURL()
    {
        if (_isExistingSurvey)
        {
            return string.Format("Insured.aspx{0}", ARMSUtility.GetQueryStringForNav("policyid"));
        }
        else
        {
            return UrlBuilder("CreateSurveyLocation.aspx");
        }
    }

    protected class CustomCoverageValue
    {
        private Guid _id = Guid.Empty;
        private string _name = string.Empty;
        private string _value = string.Empty;
        private string _dotNetDataType = string.Empty;

        public CustomCoverageValue(Guid id, string name, string value, string dotNetDataType)
        {
            _id = id;
            _name = name;
            _value = value;
            _dotNetDataType = dotNetDataType;
        }

        public Guid ID
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }

        public string Value
        {
            get { return _value; }
        }

        public string DotNetDataType
        {
            get { return _dotNetDataType; }
        }
    }

    protected class CustomCoverage
    {
        private Guid _coverageID = Guid.Empty;
        private string _displayCoverageType = string.Empty;
        private string _displayCoverageValue = string.Empty;
        private string _dotNetDataType = string.Empty;

        public CustomCoverage(Guid coverageID, string displayCoverageType, string displayCoverageValue, string dotNetDataType)
        {
            _coverageID = coverageID;
            _displayCoverageType = displayCoverageType;
            _displayCoverageValue = displayCoverageValue;
            _dotNetDataType = dotNetDataType;
        }

        public Guid CoverageID
        {
            get { return _coverageID; }
        }

        public string DisplayCoverageType
        {
            get { return _displayCoverageType; }
        }

        public string DisplayCoverageValue
        {
            get { return _displayCoverageValue; }
        }

        public string DotNetDataType
        {
            get { return _dotNetDataType; }
        }
    }

    protected class CustomCoverageName
    {
        private Guid _coverageNameID = Guid.Empty;
        private string _displayCoverageName = string.Empty;
        private string _coverageNameTypeCode = string.Empty;
        private string _reportTypeCode = string.Empty;
        private List<CustomCoverage> _customCoverages = new List<CustomCoverage>();

        public CustomCoverageName(Guid coverageNameID, string displayCoverageName, string coverageNameTypeCode, string reportTypeCode)
        {
            _coverageNameID = coverageNameID;
            _displayCoverageName = displayCoverageName;
            _coverageNameTypeCode = coverageNameTypeCode;
            _reportTypeCode = reportTypeCode;
        }

        public Guid CoverageNameID
        {
            get { return _coverageNameID; }
        }
        
        public string DisplayCoverageName
        {
            get { return _displayCoverageName; }
        }

        public string CoverageNameTypeCode
        {
            get { return _coverageNameTypeCode; }
        }

        public string ReportTypeCode
        {
            get { return _reportTypeCode; }
        }

        public List<CustomCoverage> CustomCoverages
        {
            get { return _customCoverages; }
        }
    }
}
