<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminRoles.aspx.cs" Inherits="AdminRolesAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <asp:datagrid ID="grdRoles" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="30" AllowSorting="True">
	    <headerstyle CssClass="header" />
	    <itemstyle CssClass="item" />
	    <alternatingitemstyle CssClass="altItem" />
	    <footerstyle CssClass="footer" />
	    <pagerstyle CssClass="pager" Mode="NumericPages" />
	    <columns>
		    <asp:hyperlinkcolumn HeaderText="Name" DataTextField="Name" DataNavigateUrlField="ID"
			    DataNavigateUrlFormatString="AdminRoleEdit.aspx?roleid={0}" SortExpression="Name ASC" ItemStyle-Wrap="false" />
		    <asp:BoundColumn HeaderText="Description" DataField="Description" Visible="False" SortExpression="Description ASC, Name ASC" />
		    <asp:TemplateColumn HeaderText="Users"> 
			    <ItemTemplate>
				    <%# GetUserListForRole(Container.DataItem) %>
			    </ItemTemplate>
		    </asp:TemplateColumn>
	    </columns>
    </asp:datagrid>

    <a href="AdminRoleEdit.aspx">Create New User Role</a>
    
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
