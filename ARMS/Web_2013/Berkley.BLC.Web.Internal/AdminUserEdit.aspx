<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminUserEdit.aspx.cs" Inherits="AdminUserEditAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><br>
    <br>

    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxUser" runat="server" title="User Information" width="500">
    <table class="fields">
    <uc:Text id="txtName" runat="server" field="User.Name" />
    <uc:Text id="txtDomainName" runat="server" field="User.DomainName" IsRequired="true" />
    <uc:Text id="txtUsername" runat="server" field="User.Username" IsRequired="true" />
    <uc:Text id="txtTitle" runat="server" field="User.Title" Columns="50" />
    <uc:Text id="txtCertifications" runat="server" field="User.Certifications" Columns="60" />
    <uc:Text id="txtEmailAddress" runat="server" field="User.EmailAddress" Columns="60" />
    <uc:Text id="txtWorkPhone" runat="server" field="User.WorkPhone" />
    <uc:Text id="txtHomePhone" runat="server" field="User.HomePhone" />
    <uc:Text id="txtMobilePhone" runat="server" field="User.MobilePhone" />
    <uc:Text id="txtFaxNumber" runat="server" field="User.FaxNumber" />
    <uc:Combo id="cboManager" runat="server" field="User.ManagerID" Name="Manager" topItemText="" />
    <uc:Combo id="cboProfitCenter" runat="server" field="User.ProfitCenterCode" Name="Profit Center" topItemText="" />
    <uc:Text id="txtStreetLine1" runat="server" field="User.StreetLine1" />
    <uc:Text id="txtStreetLine2" runat="server" field="User.StreetLine2" />
    <uc:Text id="txtCity" runat="server" field="User.City" />
    <uc:Combo id="cboState" runat="server" field="User.StateCode" topItemText="" 
	    DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" />
    <uc:Text id="txtZipCode" runat="server" field="User.ZipCode" />
    <uc:Check id="chkAccountDisabled" runat="server" field="User.AccountDisabled" />
    </table>
    </cc:Box>

    <cc:Box id="boxPermissions" runat="server" title="Permissions" width="500">
    <table class="fields" width="100%">
    <tr>
	    <td class="label" nowrap>Security Role</td>
	    <td width="100%">
		    <asp:DropDownList ID="cboRoles" Runat="server" CssClass="cbo" /><br>
	    </td>
    </tr>
    </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Add/Update" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>

    </form>
</body>
</html>
