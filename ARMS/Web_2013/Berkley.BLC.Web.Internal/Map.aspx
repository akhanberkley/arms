﻿<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" ValidateRequest="false" CodeFile="Map.aspx.cs" Inherits="MapAspx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>


<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= base.PageTitle %></title>

    <style type="text/css"> 
#map{
	height: 600px;
	width: 800px;
	border: 1px solid black; 
}
</style>

</head>
<body style="margin:0px;">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server"> 

    <table width="100%" cellspacing="0" cellpadding="0">
    <tr>
	    <td class="pageBody" width="100%" height="100%" valign="top">
    <!-- END PAGE HEADER -->
    
    <span class="heading"><%= base.PageHeading %></span>

    <table>
        <tr>
            <td>
                <div id="map"></div>
                <script type="text/javascript" src="http://api.maps.yahoo.com/ajaxymap?v=3.8&appid=<%= _yahooID %>"></script>
            </td>
            <td>
                <asp:Repeater ID="repAssignedUsers" Runat="server">
	                <HeaderTemplate>
			            <table style="MARGIN: 5px 0px 0px 20px;">
                            <tr>
                                <td style="font-size:15px;">
                                    <b>Key:</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="border: 1px solid black; background-color: white; " cellspacing="0" cellpadding="0">
		            </HeaderTemplate>
		            <ItemTemplate>
			            <tr>
				            <td valign="top" style="PADDING: 5px 0px 5px 10px; font-size:15px;"><%# HtmlEncode(Container.DataItem, "OrderIndex")%>)</td>
				            <td valign="top" style="PADDING: 5px 0px 5px 10px; font-size:15px;"><%# HtmlEncode(Container.DataItem, "UserName")%></td>
				            <td valign="top" style="PADDING: 5px 10px 5px 15px;">
                                <table style="border: 1px solid black; background-color:<%# HtmlEncode(Container.DataItem, "CSSColor")%>;">
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
			            </tr>			
		            </ItemTemplate>
		            <FooterTemplate>
			            </table>
                        </td>
                            </tr>
                        </table>
		            </FooterTemplate>
	            </asp:Repeater>
            </td>
        </tr>
    </table>

    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
