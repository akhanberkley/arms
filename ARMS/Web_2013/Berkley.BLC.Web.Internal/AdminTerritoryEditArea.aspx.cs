using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web;
using QCI.Web.Validation;
using QCI.ExceptionManagement;

public partial class AdminTerritoryEditAreaAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminTerritoryEditAreaAspx_Load);
        this.btnCopy.Click += new EventHandler(btnCopy_Click);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected Territory _terra;

    void AdminTerritoryEditAreaAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("territoryid", true);
        _terra = Territory.Get(id);

        // register our AJAX class for use in client script
        Ajax.Utility.RegisterTypeForAjax(typeof(AjaxMethods));

        if (!this.IsPostBack)
        {
            lblTerritory.Text = _terra.Name;

            // build strings for hidden values driving page
            StringBuilder sbState = new StringBuilder();
            StringBuilder sbZip3 = new StringBuilder();
            StringBuilder sbZip5 = new StringBuilder();
            StringBuilder sbProfitCenter = new StringBuilder();

            GeographicArea[] areas = GeographicArea.GetSortedArray("TerritoryID = ?", "AreaValue ASC", _terra.ID);
            foreach (GeographicArea area in areas)
            {
                StringBuilder sb;
                if (area.GeographicUnitCode == GeographicUnit.State.Code)
                {
                    sb = sbState;
                }
                else if (area.GeographicUnitCode == GeographicUnit.Zip3.Code)
                {
                    sb = sbZip3;
                }
                else if (area.GeographicUnitCode == GeographicUnit.Zip5.Code)
                {
                    sb = sbZip5;
                }
                else if (area.GeographicUnitCode == GeographicUnit.ProfitCenter.Code)
                {
                    sb = sbProfitCenter;
                }
                else
                {
                    throw new NotSupportedException("GeographicUnit '" + area.GeographicUnitCode + "' was not expected.");
                }

                // append the value to the string builder selected above
                if (sb.Length > 0) sb.Append(',');
                sb.Append(area.AreaValue);
            }

            // set the hidden values
            hdnListST.Value = sbState.ToString();
            hdnListZ3.Value = sbZip3.ToString();
            hdnListZ5.Value = sbZip5.ToString();
            hdnListPC.Value = sbProfitCenter.ToString();

            // populate the copy territory combo
            Territory[] items = Territory.GetSortedArray("CompanyID = ? && ID != ?", "Name ASC", this.CurrentUser.CompanyID, _terra.ID);
            cboTerritory.Items.Add(new ListItem("-- select territory --", ""));
            foreach (Territory item in items)
            {
                string text = string.Format("{0}", item.Name);
                cboTerritory.Items.Add(new ListItem(text, item.ID.ToString()));
            }
        }
    }

    void btnCopy_Click(object sender, EventArgs e)
    {
        try
        {
            string idStr = cboTerritory.SelectedValue;
            if (idStr == null || idStr.Length == 0)
            {
                throw new FieldValidationException(cboTerritory, "A territory must be selected.");
            }
            Guid sourceID = new Guid(idStr);

            // create the containers we will use to store our new lists
            ArrayList stateList = new ArrayList();
            ArrayList zip3List = new ArrayList();
            ArrayList zip5List = new ArrayList();
            ArrayList profitCenterList = new ArrayList();
            ArrayList errors = new ArrayList();

            // load the current areas if we are not starting from scratch
            if (!chkClearAllFirst.Checked)
            {
                stateList.AddRange(SplitDelimitedString(hdnListST.Value));
                zip3List.AddRange(SplitDelimitedString(hdnListZ3.Value));
                zip5List.AddRange(SplitDelimitedString(hdnListZ5.Value));
                profitCenterList.AddRange(SplitDelimitedString(hdnListPC.Value));
            }

            // load the areas for each  unit type from the source territory 
            //  and copy them to our lists, tracking any overlap errors along the way
            GeographicArea[] areas;
            areas = GeographicArea.GetSortedArray("TerritoryID = ? && GeographicUnitCode = ?", "AreaValue ASC", sourceID, GeographicUnit.State.Code);
            CopyAreas(_terra, areas, stateList, errors);

            areas = GeographicArea.GetSortedArray("TerritoryID = ? && GeographicUnitCode = ?", "AreaValue ASC", sourceID, GeographicUnit.Zip3.Code);
            CopyAreas(_terra, areas, zip3List, errors);

            areas = GeographicArea.GetSortedArray("TerritoryID = ? && GeographicUnitCode = ?", "AreaValue ASC", sourceID, GeographicUnit.Zip5.Code);
            CopyAreas(_terra, areas, zip5List, errors);

            areas = GeographicArea.GetSortedArray("TerritoryID = ? && GeographicUnitCode = ?", "AreaValue ASC", sourceID, GeographicUnit.ProfitCenter.Code);
            CopyAreas(_terra, areas, profitCenterList, errors);

            // determine if overlap errors are fatal to this process
            if (errors.Count > 0 && !chkSkipConflicts.Checked) // these conflicts are fatal
            {
                // build an error message that includes the first few overlaps
                string msg = "Conflicts were found while trying to copy the areas from the territory you selected:";

                int count = errors.Count;
                if (count == 1)
                {
                    msg += "\n\n There is one conflict:";
                }
                else // multiple
                {
                    msg += "\n\n There are " + count + " conflicts:";
                }

                // only show the first few conflicts
                if (count > 10) count = 10;
                for (int i = 0; i < count; i++)
                {
                    msg += string.Format("\n - {0}", errors[i]);
                }
                if (count < errors.Count)
                {
                    msg += "\n - ...";
                }

                msg += "\n\nYou can load the remaining areas by checking the approperiate box and press Copy again.";

                throw new FieldValidationException(msg);
            }

            // if we make it here, there were no conflicts or conflicts were non-fatal

            // update our hidden fields
            hdnListST.Value = string.Join(",", (string[])stateList.ToArray(typeof(string)));
            hdnListZ3.Value = string.Join(",", (string[])zip3List.ToArray(typeof(string)));
            hdnListZ5.Value = string.Join(",", (string[])zip5List.ToArray(typeof(string)));
            hdnListPC.Value = string.Join(",", (string[])profitCenterList.ToArray(typeof(string)));
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }
    }

    private void CopyAreas(Territory terra, GeographicArea[] areas, ArrayList list, ArrayList errors)
    {
        foreach (GeographicArea area in areas)
        {
            // skip this area if it's already in our list
            if (list.IndexOf(area.AreaValue) >= 0)
            {
                continue;
            }

            // add this area if it won't overlap any others
            // note: reuse our ajax overlap check to look for conflicts with other territories
            Territory owner;
            if (AjaxMethods.WouldAreaCauseOverlap(terra, area.GeographicUnitCode, area.AreaValue, out owner))
            {
                string msg = "Area '" + area.AreaValue + "' cannot be copied as it would cause an overlap with territory '" + owner.Name + "'.";
                errors.Add(msg);
            }
            else // no overlap
            {
                list.Add(area.AreaValue);
            }
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            //NOTE: Intentionally not validating the values again.
            //		We are assuming they are still OK since the last time the AJAX check was run.

            // build an xml document that contains all the values on the page
            StringBuilder xmldoc = new StringBuilder();
            string[] values;
            xmldoc.Append("<root>");
            values = SplitDelimitedString(hdnListST.Value);
            foreach (string value in values)
            {
                xmldoc.AppendFormat("<area unit=\"{0}\" value=\"{1}\"/>", GeographicUnit.State.Code, value);
            }
            values = SplitDelimitedString(hdnListZ3.Value);
            foreach (string value in values)

            {
                xmldoc.AppendFormat("<area unit=\"{0}\" value=\"{1}\"/>", GeographicUnit.Zip3.Code, value);
            }
            values = SplitDelimitedString(hdnListZ5.Value);
            foreach (string value in values)
            {
                xmldoc.AppendFormat("<area unit=\"{0}\" value=\"{1}\"/>", GeographicUnit.Zip5.Code, value);
            }
            values = SplitDelimitedString(hdnListPC.Value);

            
            foreach (string value in values)
            {
                //Added By Vilas Meshram: 05/08/2013: Squish# 21389: To Handle any '&' in string
                string replaceValue = value.Replace("&", "&amp;");
                xmldoc.AppendFormat("<area unit=\"{0}\" value=\"{1}\"/>", GeographicUnit.ProfitCenter.Code, replaceValue);
            }
            xmldoc.Append("</root>");

            // use our helper sproc to bulk update the entire territory
            using (SqlConnection conn = DB.GetNewConnection(true))
            {
                SqlTransaction trans = conn.BeginTransaction();
                StoredProcedures.GeographicArea_BulkUpdateTerritory(trans, _terra.ID, xmldoc.ToString());
                trans.Commit();
            }
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // return user to master page
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        return "AdminTerritoryEdit.aspx?territoryid=" + _terra.ID;
    }

    private string[] SplitDelimitedString(string str)
    {
        if (str == null || str.Length == 0)
        {
            return new string[0];
        }
        else
        {
            return str.Split(',');
        }
    }

    public class AjaxMethods
    {
        [Ajax.AjaxMethod()]
        public static string CheckValue(string territoryID, string code, string value)
        {
            try
            {
                // do code-specific validations
                if (code == GeographicUnit.State.Code) // state
                {
                    if (!Regex.IsMatch(value, @"^[A-Z]{2}$"))
                    {
                        return "Value '" + value + "' must be a two-letter state abbreviation.";
                    }

                    // verify: make sure this is a real state code
                    State state = State.GetOne("Code = ?", value);
                    if (state == null)
                    {
                        return "Value '" + value + "' is not a known two-letter state abbreviation.";
                    }
                }
                else if (code == GeographicUnit.Zip3.Code) // Zip 3
                {
                    if (!Regex.IsMatch(value, @"^\d{3}$"))
                    {
                        return "Value '" + value + "' must be the first three digits of a zip code.";
                    }
                }
                else if (code == GeographicUnit.Zip5.Code) // Zip 5
                {
                    if (!Regex.IsMatch(value, @"^\d{5}$"))
                    {
                        return "Value '" + value + "' must be a five digit zip code.";
                    }
                }
                else if (code == GeographicUnit.ProfitCenter.Code) // Zip 5
                {
                    //do nothing
                }
                else
                {
                    throw new NotSupportedException("Unit type '" + code + "' was not expected.");
                }

                // get the territory we are dealing with (can't use the page's reference since this is an async AJAX method)
                Territory terra = Territory.Get(new Guid(territoryID));

                // determine if adding this area would cause an overlap
                Territory owner;
                if (WouldAreaCauseOverlap(terra, code, value, out owner))
                {
                    return "Area '" + value + "' cannot be added as it would cause an overlap with territory '" + owner.Name + "'.";
                }

                // all clear - return no error message
                return null;
            }
            catch (Exception ex)
            {
                // publish any exception since they are non-fatal to AJAX and we 
                // will never find out about a problem unless we do.
                //ExceptionManager.Publish(ex);
                throw ex;
            }
        }


        public static bool WouldAreaCauseOverlap(Territory territory, string unitCode, string areaValue, out Territory ownerTerritory)
        {
            // verify: no other territory with the same methods are using this unit/value combo
            string queryFormat = "GeographicUnitCode = ? && AreaValue = ? && Territory[ID != ? && CompanyID = ? && SurveyStatusTypeCode = ? && InHouseSelection = ?]";
            ArrayList args = new ArrayList();
            args.Add(unitCode);
            args.Add(areaValue);
            args.Add(territory.ID);
            args.Add(territory.CompanyID);
            args.Add(territory.SurveyStatusTypeCode);
            args.Add(territory.InHouseSelection);

            // execute the query
            GeographicArea dupArea = GeographicArea.GetOne(queryFormat, args.ToArray());

            // return the results of our query
            ownerTerritory = (dupArea != null) ? dupArea.Territory : null;
            return (dupArea != null);
        }
    }
}
