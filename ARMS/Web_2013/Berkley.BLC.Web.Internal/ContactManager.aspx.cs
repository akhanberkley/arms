﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities.Extensions;
using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web.Validation;
using QCI.Web;

public partial class ContactManagerAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(ContactManagerAspx_Load);
        this.PreRender += new EventHandler(ContactManagerAspx_PreRender);
        this.grdContacts.ItemDataBound += new DataGridItemEventHandler(grdContacts_ItemDataBound);
        this.grdContacts.SortCommand += new DataGridSortCommandEventHandler(grdContacts_SortCommand);
        this.cboSurveyStatus.SelectedIndexChanged += new EventHandler(cboSurveyStatus_SelectedIndexChanged);
        this.btnExport.Click += new EventHandler(btnExport_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
    }
    #endregion

    #region Enum
    /// <summary>
    /// Action Types
    /// </summary>
    protected enum Action
    {
        Update,
        Insert,
        Delete,
        Nothing
    }
    #endregion

    private Insured _insured;
    protected ServicePlan _plan;
    protected Survey _survey;
    protected Action _action;

    void ContactManagerAspx_Load(object sender, EventArgs e)
    {
        // register our AJAX class for use in client script
        Ajax.Utility.RegisterTypeForAjax(typeof(AjaxMethods));
        
        //navigate back
        Guid surveyID = ARMSUtility.GetGuidFromQueryString("surveyid", false);
        Guid planID = ARMSUtility.GetGuidFromQueryString("planid", false);

        if (surveyID != Guid.Empty)
        {
            _survey = Survey.Get(surveyID);
            _insured = _survey.Insured;
        }
        else if (planID != Guid.Empty)
        {
            _plan = ServicePlan.Get(planID);
            _insured = _plan.Insured;
        }
        else
        {
            //survey id no longer exists
            Security.RequestExpired();
        }

        DetermineAction();

        //set the client script for this checkbox
        chkSameAsCorporateAddress.Attributes.Add("onclick", string.Format("SameAsCorporateAddress(this.checked, '{0}')", _insured.ID));

        if (!this.IsPostBack)
        {
            // populate the list with company specific mail options
            ContactReasonType[] contactTypes = ContactReasonType.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            cboContactOption.DataBind(contactTypes, "Code", "LongDescription");
            cboContactOption.Visible = contactTypes.Length > 0;

            // populate the list of statuses
            SurveyStatus[] statuses = SurveyStatus.GetSortedArray("", "DisplayOrder ASC");
            QCI.Web.ComboHelper.BindCombo(cboSurveyStatus, statuses, "Code", "Name");
            cboSurveyStatus.Items.Insert(0, new ListItem("(All)", ""));
        }

        this.PageHeading = string.Format("Contacts for Client {0}", _insured.ClientID);
    }

    void ContactManagerAspx_PreRender(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            LoadContacts();
        }

        SurveyLocationContact[] surveyLocationContacts = this.ViewState["SurveyLocationContactResults"] as SurveyLocationContact[];
        DataGridHelper.BindGrid(grdContacts, surveyLocationContacts, "ContactID", "There are no contacts for this insured.");

        cboContacts.DataBind(GetUniqueContactNames(surveyLocationContacts), "ContactID", "ContactName");
        //cboContacts.DataBind(surveyLocationContacts, "ContactID", "{0}  (Location #{1})", "ContactName|LocationNumber".Split('|'));

        if (_action == Action.Update)
        {
            boxContacts.Title = Server.HtmlDecode("Step #2:&nbsp;&nbsp;Select Contacts to Update");
            boxContact.Title = Server.HtmlDecode("Step #4:&nbsp;&nbsp;Enter Information to Update the Selected Contacts Above");
        }
        else
        {
            if (_action == Action.Delete)
            {
                boxContacts.Title = Server.HtmlDecode("Step #2:&nbsp;&nbsp;Select Contacts to Delete");
            }
            else if (_action == Action.Insert)
            {
                boxContacts.Title = Server.HtmlDecode("Step #2:&nbsp;&nbsp;Select Survey Locations for the New Contact");
            }
            boxContact.Title = Server.HtmlDecode("Step #4:&nbsp;&nbsp;Enter Information for the New Contact");
        }

        btnSubmit.Visible = (_action != Action.Nothing);
        btnCancel.Visible = (_action != Action.Nothing);
        boxContact.Visible = (_action == Action.Update || _action == Action.Insert);
        boxContactToCopy.Visible = (_action == Action.Update || _action == Action.Insert);

        grdContacts.Columns[grdContacts.Columns.Count - 1].Visible = (_action != Action.Nothing);
        grdContacts.Columns[0].Visible = (_action != Action.Insert);
        grdContacts.Columns[1].Visible = (_action != Action.Insert);
        grdContacts.Columns[2].Visible = (_action != Action.Insert);
        grdContacts.Columns[3].Visible = (_action != Action.Insert);
        grdContacts.Columns[4].Visible = (_action != Action.Insert);
        grdContacts.Columns[5].Visible = (_action != Action.Insert);
        boxContacts.Width = (surveyLocationContacts.Length == 0) ? "500" : "";
    }

    void cboSurveyStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadContacts();
    }

    private void LoadContacts()
    {
        string command = string.Format("EXEC Insured_GetAllContacts @ClientID = '{0}', @SurveyStatusCode = '{1}'", _insured.ClientID, cboSurveyStatus.SelectedValue);
        DataSet ds = DB.Engine.GetDataSet(command);

        List<SurveyLocationContact> results = new List<SurveyLocationContact>();
        List<string> uniqueSurveysLocations = new List<string>();
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            SurveyLocationContact result = new SurveyLocationContact();
            result.ContactID = Guid.Parse(dr["LocationContactID"].ToString());
            result.ContactPriorityIndex = Int32.Parse(dr["ContactPriorityIndex"].ToString());
            result.ContactName = dr["ContactName"].ToString();
            result.ContactTitle = dr["ContactTitle"].ToString();
            result.ContactPhone = Formatter.Phone(dr["ContactPhoneNumber"].ToString(), "(none)");
            result.ContactEmailAddress = dr["ContactEmailAddress"].ToString();
            result.ContactReasonType = dr["ContactReasonType"].ToString();
            result.LocationID = Guid.Parse(dr["LocationID"].ToString());
            result.LocationNumber = Int32.Parse(dr["LocationNumber"].ToString());

            //Now compile the surveys tied to the contact
            result.SurveyListEncoded = BuildSurveyList(dr, uniqueSurveysLocations, result);
            result.SurveyLocation = GetFormattedLocation(dr);

            results.Add(result);
        }

        btnSubmit.Enabled = (results.Count > 0);

        string sortExpression = ARMSUtility.GetStringFromQueryString("sortby", false);
        string sortReversed = ARMSUtility.GetStringFromQueryString("sortReversed", false);
        if (!String.IsNullOrEmpty(sortExpression))
        {
            if (!String.IsNullOrEmpty(sortReversed) && sortReversed == "1")
            {
                results = results.SortDESC(sortExpression);
            }
            else
            {
                results = results.Sort(sortExpression);
            }
        }

        this.ViewState.Add("SurveyLocationContactResults", results.ToArray());
        this.ViewState["SortReversed"] = 0;
    }

    private void DetermineAction()
    {
        if (rdoUpdate.Checked)
        {
            _action = Action.Update;
        }
        else if (rdoDelete.Checked)
        {
            _action = Action.Delete;
        }
        else if (rdoInsert.Checked)
        {
            _action = Action.Insert;
        }
        else
        {
            _action = Action.Nothing;
        }
    }

    void grdContacts_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        SortDirection sortDirection = new SortDirection();
        List<SurveyLocationContact> contacts = new List<SurveyLocationContact>();
        contacts.AddRange(this.ViewState["SurveyLocationContactResults"] as SurveyLocationContact[]);

        if (ColumnSortDirection.ContainsKey(e.SortExpression))
        {
            sortDirection = ColumnSortDirection[e.SortExpression];
        }
        else
        {
            ColumnSortDirection.Add(e.SortExpression, sortDirection);
        }

        //determine if the sort has been reversed and do the sorting
        if (sortDirection == SortDirection.Ascending)
        {
            contacts = contacts.Sort(e.SortExpression);
            ColumnSortDirection[e.SortExpression] = SortDirection.Descending;

            this.ViewState["SortReversed"] = 0;
        }
        else
        {
            contacts = contacts.SortDESC(e.SortExpression);
            ColumnSortDirection[e.SortExpression] = SortDirection.Ascending;

            this.ViewState["SortReversed"] = 1;
        }

        //save this back to the view state
        this.ViewState.Add("SurveyLocationContactResults", contacts.ToArray());
        this.ViewState.Add("SortExpression", e.SortExpression);
    }

    void grdContacts_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the control
            CheckBox chkAction = (CheckBox)e.Item.FindControl("chkContact");

            // set the contact id as an attribute for each control
            SurveyLocationContact contact = (SurveyLocationContact)e.Item.DataItem;
            chkAction.Attributes.Add("ContactID", contact.ContactID.ToString());

            if (_action == Action.Insert)
            {
                //only display unique survey locations
                e.Item.Visible = contact.DisplayInGrid;
                e.Item.CssClass = "item";
            }
        }
    }

    protected string BuildSurveyList(DataRow dr, List<string> uniqueSurveysLocations, SurveyLocationContact contact)
    {
        string resultWithHTML = string.Empty;
        string result = string.Empty;

        string command = string.Format("EXEC dbo.Insured_GetAllContactSurveys @LocationContactID = '{0}'", dr["LocationContactID"]);
        DataSet ds = DB.Engine.GetDataSet(command);
        if (ds.Tables.Count > 0)
        {
            foreach (DataRow drSurvey in ds.Tables[0].Rows)
            {
                resultWithHTML += string.Format("<div style='white-space:nowrap;'>#{0} - {1}</span><br>", drSurvey["SurveyNumber"], drSurvey["SurveyStatusName"]);
                result += string.Format("#{0} - {1}", drSurvey["SurveyNumber"], drSurvey["SurveyStatusName"]);

                string key = drSurvey["SurveyNumber"].ToString() + dr["LocationID"].ToString();
                if (!uniqueSurveysLocations.Contains(key))
                {
                    contact.DisplayInGrid = true;
                    uniqueSurveysLocations.Add(key);
                }
            }
        }

        contact.SurveyList = result;

        return (resultWithHTML.Length > 0) ? resultWithHTML : "(none)";
    }


    protected string GetFormattedLocation(DataRow dr)
    {
        return Formatter.Address(dr["LocationStreetLine1"].ToString(),
            dr["LocationStreetLine2"].ToString(),
            dr["LocationCity"].ToString(),
            dr["LocationStateCode"].ToString(),
            dr["LocationZipCode"].ToString());
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        List<LocationContact> contacts = new List<LocationContact>();

        foreach (DataGridItem gridItem in grdContacts.Items)
        {
            if (gridItem.ItemType == ListItemType.Item || gridItem.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox chkContact = (CheckBox)gridItem.FindControl("chkContact");

                if (chkContact.Checked)
                {
                    Guid id = new Guid(chkContact.Attributes["ContactID"]);
                    contacts.Add(LocationContact.Get(id));
                }
            }
        }

        if (contacts.Count <= 0)
        {
            if (_action == Action.Insert)
            {
                JavaScript.ShowMessage(this, "Please select a survey location or locations.");
            }
            else
            {
                JavaScript.ShowMessage(this, "Please select a contact or contacts.");
            }
            return;
        }

        //update or delete
        for (int i = 0; i < contacts.Count; i++)
        {
            LocationContact contact = contacts[i].GetWritableInstance();
            int rank = GetRank(contact);

            if (_action == Action.Delete)
            {
                contact.Delete();
            }
            else if (_action == Action.Update)
            {
                this.PopulateEntity(contact);
                contact.PriorityIndex = rank;
                contact.Save();
            }
            else if (_action == Action.Insert)
            {
                LocationContact newContact = new LocationContact(Guid.NewGuid());
                this.PopulateEntity(newContact);
                newContact.LocationID = contact.LocationID;
                newContact.PriorityIndex = rank;

                newContact.Save();
            }
        }

        Response.Redirect(string.Format("ContactManager.aspx{0}&sortby={1}&sortreversed={2}", ARMSUtility.GetQueryStringForNav("sortby", "sortreversed"), this.ViewState["SortExpression"], this.ViewState["SortReversed"]), true);
    }

    private List<SurveyLocationContact> GetUniqueContactNames(SurveyLocationContact[] surveyLocationContacts)
    {
        List<SurveyLocationContact> list = new List<SurveyLocationContact>();
        List<string> uniqueNames = new List<string>();

        foreach (SurveyLocationContact surveyLocationContact in surveyLocationContacts)
        {
            if (!uniqueNames.Contains(surveyLocationContact.ContactName))
            {
                list.Add(surveyLocationContact);
                uniqueNames.Add(surveyLocationContact.ContactName);
            }
        }

        return list;
    }

    private int GetRank(LocationContact newContact)
    {
        int result = int.MinValue;

        LocationContact[] contacts;
        if (_action == Action.Update)
        {
            contacts = LocationContact.GetSortedArray("LocationID = ? && ID != ?", "PriorityIndex ASC", newContact.LocationID, newContact.ID);
        }
        else
        {
            contacts = LocationContact.GetSortedArray("LocationID = ?", "PriorityIndex ASC", newContact.LocationID);
        }

        if (chkMakePrimary.Checked || _action == Action.Delete)
        {
            //set the contact as primary
            result = 1;

            //now see if the others for that location need to be incrimented
            for (int i = 0; i < contacts.Length; i++)
            {
                LocationContact otherContact = contacts[i].GetWritableInstance();
                otherContact.PriorityIndex = i + ((chkMakePrimary.Checked) ? 2 : 1);
                otherContact.Save();
            }
        }
        else if (_action == Action.Insert)
        {
            //get the highest number and incriment by one
            result = (contacts.Length > 0) ? contacts[contacts.Length-1].PriorityIndex + 1 : 1;
        }
        else if (_action == Action.Update)
        {
            //keep the same rank
            result = newContact.PriorityIndex;
        }

        return result;
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("ContactManager.aspx{0}", ARMSUtility.GetQueryStringForNav()), true);
    }

    void btnExport_Click(object sender, EventArgs e)
    {
        List<SurveyLocationContact> contacts = new List<SurveyLocationContact>();
        contacts.AddRange(this.ViewState["SurveyLocationContactResults"] as SurveyLocationContact[]);

        //Get the columns for the CSV export
        ExportItemList oExportDataList = new ExportItemList(this);
        for (int i = 0; i < grdContacts.Columns.Count -1 ; i++)
        {
            string dataType;
            switch (i)
            {
                case 0:
                    dataType = "Int32";
                    break;
                case 1:
                    dataType = "String";
                    break;
                case 2:
                    dataType = "String";
                    break;
                case 3:
                    dataType = "String";
                    break;
                case 4:
                    dataType = "String";
                    break;
                case 5:
                    dataType = "String";
                    break;
                case 6:
                    dataType = "String";
                    break;
                case 7:
                    dataType = "String";
                    break;
                case 8:
                    dataType = "String";
                    break;
                default:
                    throw new NotSupportedException("Not expecting this many columns in the contacts grid.");
            }

            DataGridColumn column = grdContacts.Columns[i];
            oExportDataList.Add(new ExportItem(Guid.NewGuid(), column.SortExpression, column.SortExpression.Replace("Encoded", ""), dataType, column.HeaderText, i));
        }

        // Get the CSV data
        string strCsv = oExportDataList.GetCsv(contacts.ToArray(), oExportDataList, "No Contacts Found.");

        Response.ContentType = "application/x-msexcel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=contacts.csv");

        // Write out the CSV data
        System.Text.ASCIIEncoding oEncoding = new System.Text.ASCIIEncoding(); ;
        byte[] rbBytes = oEncoding.GetBytes(strCsv);
        Response.OutputStream.Write(rbBytes, 0, rbBytes.Length);

        Response.End();
    }

    [Serializable]
    public class SurveyLocationContact
    {
        Guid _contactID = Guid.Empty;
        int _contactPriorityIndex = int.MinValue;
        string _contactName = string.Empty;
        string _contactTitle = string.Empty;
        string _contactPhone = string.Empty;
        string _contactEmailAddress = string.Empty;
        string _contactReasonType = string.Empty;
        string _surveyList = string.Empty;
        string _surveyListEncoded = string.Empty;
        string _surveyLocation = string.Empty;
        Guid _locationID = Guid.Empty;
        int _locationNumber = int.MinValue;
        bool _displayInGrid = false;

        public Guid ContactID
        {
            get { return _contactID; }
            set { _contactID = value; }
        }
        
        public int ContactPriorityIndex
        {
            get {return _contactPriorityIndex; }
            set {_contactPriorityIndex = value;}
        }

        public string ContactName
        {
            get { return _contactName; }
            set { _contactName = value; }
        }

        public string ContactTitle
        {
            get { return _contactTitle; }
            set { _contactTitle = value; }
        }

        public string ContactPhone
        {
            get { return _contactPhone; }
            set { _contactPhone = value; }
        }

        public string ContactEmailAddress
        {
            get { return _contactEmailAddress; }
            set { _contactEmailAddress = value; }
        }

        public string ContactReasonType
        {
            get { return _contactReasonType; }
            set { _contactReasonType = value; }
        }

        public string SurveyList
        {
            get { return _surveyList; }
            set { _surveyList = value; }
        }

        public string SurveyListEncoded
        {
            get { return _surveyListEncoded; }
            set { _surveyListEncoded = value; }
        }

        public string SurveyLocation
        {
            get { return _surveyLocation; }
            set { _surveyLocation = value; }
        }

        public Guid LocationID
        {
            get { return _locationID; }
            set { _locationID = value; }
        }

        public int LocationNumber
        {
            get { return _locationNumber; }
            set { _locationNumber = value; }
        }

        public bool DisplayInGrid
        {
            get { return _displayInGrid; }
            set { _displayInGrid = value; }
        }
    }

    public class AjaxMethods
    {
        [Ajax.AjaxMethod()]
        public static string[] SameAsCorporateAddress(bool isChecked, string insuredID)
        {
            string[] arrResult = new string[6];

            if (isChecked)
            {
                Insured eInsured = Insured.Get(new Guid(insuredID));
                arrResult[0] = eInsured.Name;
                arrResult[1] = eInsured.StreetLine1;
                arrResult[2] = eInsured.StreetLine2;
                arrResult[3] = eInsured.City;
                arrResult[4] = eInsured.StateCode;
                arrResult[5] = eInsured.ZipCode;
            }
            else
            {
                for (int i = 0; i < arrResult.Length; i++)
                {
                    arrResult[i] = string.Empty;
                }
            }

            return arrResult;
        }

        [Ajax.AjaxMethod()]
        public static string[] PopulateContactFields(string contactID)
        {
            string[] arrResult = new string[13];

            if (!string.IsNullOrEmpty(contactID))
            {
                LocationContact contact = LocationContact.Get(new Guid(contactID));
                arrResult[0] = contact.Name;
                arrResult[1] = contact.Title;
                arrResult[2] = contact.PhoneNumber;
                arrResult[3] = contact.AlternatePhoneNumber;
                arrResult[4] = contact.FaxNumber;
                arrResult[5] = contact.EmailAddress;
                arrResult[6] = contact.ContactReasonTypeCode;
                arrResult[7] = contact.CompanyName;
                arrResult[8] = contact.StreetLine1;
                arrResult[9] = contact.StreetLine2;
                arrResult[10] = contact.City;
                arrResult[11] = contact.StateCode;
                arrResult[12] = contact.ZipCode;
            }
            else
            {
                for (int i = 0; i < arrResult.Length; i++)
                {
                    arrResult[i] = string.Empty;
                }
            }

            return arrResult;
        }
    }
}
