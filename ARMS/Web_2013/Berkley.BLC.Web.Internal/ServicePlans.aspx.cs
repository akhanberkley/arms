using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web;

public partial class ServicePlansAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
        BuildDataGrid();
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(ServicePlansAspx_Load);
        this.PreRender += new EventHandler(ServicePlansAspx_PreRender);
        this.grdItems.PageIndexChanged += new DataGridPageChangedEventHandler(grdItems_PageIndexChanged);
        this.grdItems.SortCommand += new DataGridSortCommandEventHandler(grdItems_SortCommand);
        this.cboCreatedBy.SelectedIndexChanged += new EventHandler(cboCreatedBy_SelectedIndexChanged);
        this.cboStatus.SelectedIndexChanged += new EventHandler(cboStatus_SelectedIndexChanged);
        this.btnExport.Click += new EventHandler(btnExport_Click);
    }
    #endregion

    protected bool _hideManagedByDropDown;
    void ServicePlansAspx_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            ServicePlanStatus[] statuses = ServicePlanStatus.GetSortedArray("", "DisplayOrder ASC", null);
            ComboHelper.BindCombo(cboStatus, statuses, "Code", "Name");
            cboStatus.SelectedValue = (string)this.UserIdentity.GetPageSetting("SelectedStatusCode", ServicePlanStatus.AssignedPlan.Code);

            User[] users = Berkley.BLC.Entities.User.GetSortedArray("CompanyID = ? && Role[PermissionSet[CanCreateServicePlans = true]]", "Name ASC", CurrentUser.CompanyID);
            ComboHelper.BindCombo(cboCreatedBy, users, "ID", "Name");
            cboCreatedBy.Items.Insert(0, new ListItem("(Any User)", ""));
            cboCreatedBy.SelectedValue = (string)this.UserIdentity.GetPageSetting("SelectedUserID", CurrentUser.ID.ToString());
            
            // restore sort expression, reverse state, and page index settings
            this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", grdItems.Columns[0].SortExpression);
            this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", false);
            grdItems.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);

            lnkCreatePlan.Visible = CurrentUser.Permissions.CanCreateServicePlans;
        }
    }

    void ServicePlansAspx_PreRender(object sender, EventArgs e)
    {
        string sortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sortExp = sortExp.Replace(" DESC", " TEMP");
            sortExp = sortExp.Replace(" ASC", " DESC");
            sortExp = sortExp.Replace(" TEMP", " ASC");
        }

        // get the service plans
        VWServicePlan[] ePlans;
        if (cboCreatedBy.SelectedValue != string.Empty)
        {
            ePlans = VWServicePlan.GetSortedArray("CompanyID = ? && CreateStateCode != ? && ServicePlanStatusCode = ? && AssignedUserID = ?", sortExp, this.CurrentUser.CompanyID, CreateState.InProgress.Code, cboStatus.SelectedValue, cboCreatedBy.SelectedValue);
        }
        else
        {
            ePlans = VWServicePlan.GetSortedArray("CompanyID = ? && CreateStateCode != ? && ServicePlanStatusCode = ?", sortExp, this.CurrentUser.CompanyID, CreateState.InProgress.Code, cboStatus.SelectedValue);
        }

        string noRecordsMessage = string.Format("There are currently no {0} service plans created by {1}.", cboStatus.SelectedItem.ToString().ToLower(), cboCreatedBy.SelectedItem.ToString());
        grdItems.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindPagingGrid(grdItems, ePlans, null, noRecordsMessage);
        this.ViewState["SearchResults"] = ePlans;

        if (Berkley.BLC.Business.Utility.GetCompanyParameter("HideSPManagedByLCConsultant", this.CurrentUser.Company).ToLower() == "true" && !this.CurrentUser.Permissions.CanViewCompanySummary)
            _hideManagedByDropDown = true;
    }

    void cboCreatedBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.UserIdentity.SavePageSetting("SelectedUserID", cboCreatedBy.SelectedValue);
    }

    void cboStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.UserIdentity.SavePageSetting("SelectedStatusCode", cboStatus.SelectedValue);
    }

    void grdItems_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdItems.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);
    }

    void grdItems_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sortRev);
    }

    void btnExport_Click(object sender, EventArgs e)
    {
        VWServicePlan[] plans = (VWServicePlan[])this.ViewState["SearchResults"];
        ExportItemList oExportDataList = (ExportItemList)this.ViewState["Exports"];

        if (plans == null || oExportDataList == null)
        {
            JavaScript.ShowMessage(this, "The session has timed out.  Please try again.");
            return;
        }

        string strErrorMessage = "No Service Plans Found";
        object oSavedText = this.ViewState["EmptyExportText"];
        if (oSavedText != null)
        {
            strErrorMessage = oSavedText.ToString();
        }

        // Get the CSV data
        string strCsv = oExportDataList.GetCsv(plans, oExportDataList, strErrorMessage);

        Response.ContentType = "application/x-msexcel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=serviceplan.csv");

        // Write out the CSV data
        System.Text.ASCIIEncoding oEncoding = new System.Text.ASCIIEncoding(); ;
        byte[] rbBytes = oEncoding.GetBytes(strCsv);
        Response.OutputStream.Write(rbBytes, 0, rbBytes.Length);

        Response.End();
    }

    protected string GetPolicyExpDate(Object dataItem)
    {
        string result = "(none)";
        
        ServicePlan servicePlan = (ServicePlan)dataItem;
        
        Policy[] policies = Policy.GetSortedArray("InsuredID = ?", "ExpireDate DESC", servicePlan.InsuredID);
        if (policies.Length > 0)
        {
            result = policies[0].ExpireDate.ToShortDateString();
        }
        
        return result;
    }

    private void BuildDataGrid()
    {
        ExportItemList oExportList = new ExportItemList(this);

        //Get the columns by order from the DB
        ServicePlanGridColumnFilterCompany[] filterCols = ServicePlanGridColumnFilterCompany.GetSortedArray("CompanyID = ?", "Display DESC, DisplayOrder ASC", CurrentUser.CompanyID);

        foreach (ServicePlanGridColumnFilterCompany column in filterCols)
        {
            if (column.Display)
            {
                TemplateColumn template = new TemplateColumn();
                template.HeaderText = column.ServicePlanGridColumnFilter.Name;
                template.SortExpression = column.ServicePlanGridColumnFilter.SortExpression;
                template.ItemTemplate = new MyTemplate(column.ServicePlanGridColumnFilter);
                template.ItemStyle.HorizontalAlign = (column.ServicePlanGridColumnFilter.ID == GridColumnFilter.Reminder.ID) ? HorizontalAlign.Center : HorizontalAlign.Left;

                grdItems.Columns.Add(template);
            }

            if (this.IsPostBack)
            {
                // Grab the field names for Excel export
                string strFilterName = column.ServicePlanGridColumnFilter.FilterExpression == null || column.ServicePlanGridColumnFilter.FilterExpression.Length == 0 ? "" : column.ServicePlanGridColumnFilter.FilterExpression.Replace("\r\n", " ").Trim();
                string strSortName = column.ServicePlanGridColumnFilter.SortExpression == null || column.ServicePlanGridColumnFilter.SortExpression.Length == 0 ? "" : column.ServicePlanGridColumnFilter.SortExpression.Substring(0, column.ServicePlanGridColumnFilter.SortExpression.IndexOf(" "));
                string strDataType = column.ServicePlanGridColumnFilter.DotNetDataType == null || column.ServicePlanGridColumnFilter.DotNetDataType.Length == 0 ? "" : column.ServicePlanGridColumnFilter.DotNetDataType.Replace("\r\n", " ").Trim();
                oExportList.Add(new ExportItem(column.ServicePlanGridColumnFilter.ID, strSortName, strFilterName, strDataType, column.ServicePlanGridColumnFilter.Name, column.DisplayOrder));
            }
        }

        //oExportList.Sort();
        this.ViewState["Exports"] = oExportList;
    }

    private class MyTemplate : ITemplate
    {
        private string expression;
        private ServicePlanGridColumnFilter filter;
        public MyTemplate(ServicePlanGridColumnFilter filter)
        {
            this.filter = filter;
            this.expression = filter.FilterExpression;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            //figure out the type of control to bind
            if (filter.ID == ServicePlanGridColumnFilter.ServicePlanLink.ID)
            {
                HyperLink link = new HyperLink();
                link.ID = "lnkServicePlan";
                link.DataBinding += new EventHandler(this.BindHyperLink);
                container.Controls.Add(link);
            }
            else
            {
                Literal l = new Literal();
                l.DataBinding += new EventHandler(this.BindData);
                container.Controls.Add(l);
            }
        }

        public void BindData(object sender, EventArgs e)
        {
            Literal l = (Literal)sender;
            DataGridItem container = (DataGridItem)l.NamingContainer;

            switch (filter.SystemTypeCode)
            {
                case TypeCode.DateTime:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:d}", DateTime.MinValue, "(none)");
                    break;
                case TypeCode.Decimal:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:C}", Decimal.MinValue, "(none)");
                    break;
                case TypeCode.Int64:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:N2}", Decimal.MinValue, "(none)");
                    break;
                case TypeCode.Int32:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:N0}", Int32.MinValue, "(none)");
                    break;
                default:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, string.Empty, "(none)");
                    break;
            }
        }

        public void BindHyperLink(object sender, EventArgs e)
        {
            HyperLink link = (HyperLink)sender;
            DataGridItem container = (DataGridItem)link.NamingContainer;
            link.Text = HtmlEncodeNullable(container.DataItem, expression, string.Empty, "(none)");
            link.NavigateUrl = string.Format("ServicePlan.aspx?planid={0}", HtmlEncode(container.DataItem, "ServicePlanID"));
        }
    }
}
