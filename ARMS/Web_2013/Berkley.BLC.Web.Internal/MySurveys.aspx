<%@ Page Language="C#" AutoEventWireup="false" CodeFile="MySurveys.aspx.cs" Inherits="MySurveysAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    <uc:HintHeader ID="ucHintHeader" runat="server" />

    <cc:Box id="boxSearches" runat="server" title="Saved Searches" width="375">
    <asp:datagrid ID="grdSavedSearches" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="false">
    <headerstyle CssClass="header" />
    <itemstyle CssClass="item" />
    <alternatingitemstyle CssClass="altItem" />
    <footerstyle CssClass="footer" />
    <pagerstyle CssClass="pager" Mode="NumericPages" />
    <columns>
	    <asp:hyperlinkcolumn ItemStyle-Wrap="false" HeaderText="Name" DataTextField="Name" DataNavigateUrlField="ID"
		    DataNavigateUrlFormatString="surveys.aspx?searchid={0}" />
	    <asp:TemplateColumn ItemStyle-Wrap="false" HeaderText="Total&nbsp;Results" ItemStyle-HorizontalAlign="Right">
		    <ItemTemplate>
			    <%# GetResultCountHtml(Container.DataItem) %>
		    </ItemTemplate>
	    </asp:TemplateColumn>
	    <asp:TemplateColumn HeaderText="Move" ItemStyle-Wrap="false">
		    <ItemTemplate>
			    &nbsp;<asp:LinkButton ID="btnUp" Runat="server" CommandName="MoveUp">Up</asp:LinkButton>&nbsp;/
			    &nbsp;<asp:LinkButton ID="btnDown" Runat="server" CommandName="MoveDown">Down</asp:LinkButton>&nbsp;
		    </ItemTemplate>
	    </asp:TemplateColumn>
	    <asp:TemplateColumn HeaderText="Delete">
		    <ItemTemplate>
			    &nbsp;<asp:LinkButton ID="btnDelete" Runat="server" CommandName="Delete">Delete</asp:LinkButton>&nbsp;
		    </ItemTemplate>
	    </asp:TemplateColumn>
	    <asp:TemplateColumn HeaderText="Required">
            <ItemStyle Wrap="False" HorizontalAlign="Center"></ItemStyle>
            <ItemTemplate>
                    <asp:LinkButton ID="lnkMakeActive" runat="server" CommandName="Required"><%# GetRequiredText(Container.DataItem) %></asp:LinkButton>
	        </ItemTemplate>
        </asp:TemplateColumn>
    </columns>
    </asp:datagrid>
    </cc:Box>
    
    <%if (CurrentUser.IsUnderwriter){%>
    <cc:Box id="boxUWQueues" runat="server" Title="My Survey Requests" width="200">
		<table class="fields">
			<tr class="header">
				<td style="white-space:nowrap"><a href="surveys.aspx?uwfilter=OHSU" style="MARGIN-RIGHT: 20px">On Hold / Unassigned</a></td>
				<td style="white-space:nowrap"><%= GetUnassignedCount()%></td>
			</tr>
			<tr class="header">
				<td style="white-space:nowrap"><a href="surveys.aspx?uwfilter=SA" style="MARGIN-RIGHT: 20px">Assigned To Consultant</a></td>
				<td style="white-space:nowrap"><%= GetCount(SurveyStatus.AssignedSurvey)%></td>
			</tr>
			<tr class="header">
				<td style="white-space:nowrap"><a href="surveys.aspx?uwfilter=FA" style="MARGIN-RIGHT: 20px">Assigned To Reviewer</a></td>
				<td style="white-space:nowrap"><%= GetCount(SurveyStatus.AssignedReview)%></td>
			</tr>
			<tr class="header">
				<td style="white-space:nowrap"><a href="surveys.aspx?uwfilter=PD" style="MARGIN-RIGHT: 20px">Open & Past Due</a></td>
				<td style="white-space:nowrap"><%= GetOverdue()%></td>
			</tr>
			<tr class="header">
				<td style="white-space:nowrap"><a href="surveys.aspx?uwfilter=SC" style="MARGIN-RIGHT: 20px">Closed (Past Year)</a></td>
				<td style="white-space:nowrap"><%= GetClosedCount()%></td>
			</tr>
			<tr class="header">
				<td style="white-space:nowrap"><a href="surveys.aspx?uwfilter=CC" style="MARGIN-RIGHT: 20px">Canceled (Past Year)</a></td>
				<td style="white-space:nowrap"><%= GetCanceledCount()%></td>
			</tr>
            <%if (SupportsUWAcknowledgement()) { %>
            <tr class="header">
				<td style="white-space:nowrap"><a href="surveys.aspx?uwfilter=UK" style="MARGIN-RIGHT: 20px">Unacknowledged (Past Year)</a></td>
				<td style="white-space:nowrap"><%= GetUnacknowledgedCount()%></td>
			</tr>
            <%} %>
			<tr class="header">
				<td style="white-space:nowrap"><a href="surveys.aspx?uwfilter=ALL" style="MARGIN-RIGHT: 20px">All My Surveys</a></td>
				<td style="white-space:nowrap"><%= GetCountAll()%></td>
			</tr>
		</table>
	</cc:Box>
	<%} %>
	
	<%if (_eActiveUser.Permissions.CanViewSurveyReviewQueues && grdReviewQueues.Items.Count > 0){%>
	<cc:Box id="boxSurveyReviewQueues" runat="server" title="Review Queues" width="250">
	    <asp:datagrid ID="grdReviewQueues" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" PageSize="30" AllowSorting="false">
        <headerstyle CssClass="header" />
        <itemstyle CssClass="item" />
        <alternatingitemstyle CssClass="altItem" />
        <footerstyle CssClass="footer" />
        <pagerstyle CssClass="pager" Mode="NumericPages" />
        <columns>
		    <asp:TemplateColumn HeaderText="Name" ItemStyle-Wrap="false">
			    <ItemTemplate>
				    <a href="surveys.aspx?reviewqueueid=<%# HtmlEncode(Container.DataItem, "ID") %>">
				        <%# HtmlEncode(Container.DataItem, "Name") %>
				    </a>
			    </ItemTemplate>
		    </asp:TemplateColumn>
	        <asp:TemplateColumn HeaderText="Total"  ItemStyle-HorizontalAlign="Right">
			    <ItemTemplate>
				        <%# GetSurveyReviewsCount(Container.DataItem) %>
			    </ItemTemplate>
		    </asp:TemplateColumn>
        </columns>
        </asp:datagrid>
	</cc:Box>
	<%} %>
	
	<cc:Box id="boxLetterReminders" runat="server" title="Letter Reminders" width="250">
	    <asp:datagrid ID="grdLetters" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" PageSize="30" AllowSorting="false">
        <headerstyle CssClass="header" />
        <itemstyle CssClass="item" />
        <alternatingitemstyle CssClass="altItem" />
        <footerstyle CssClass="footer" />
        <pagerstyle CssClass="pager" Mode="NumericPages" />
        <columns>
		    <asp:TemplateColumn HeaderText="Name" ItemStyle-Wrap="false">
			    <ItemTemplate>
				    <a href="surveys.aspx?letterid=<%# HtmlEncode(Container.DataItem, "ID") %>&letteruserid=<%= _eActiveUser.ID %>&priorletterid=<%# GetPriorLetter() %>">
				        <%# HtmlEncode(Container.DataItem, "Name") %>
				    </a>
			    </ItemTemplate>
		    </asp:TemplateColumn>
	        <asp:TemplateColumn HeaderText="Total"  ItemStyle-HorizontalAlign="Right">
			    <ItemTemplate>
				        <%# GetLetterCount(Container.DataItem) %>
			    </ItemTemplate>
		    </asp:TemplateColumn>
        </columns>
        </asp:datagrid>
	</cc:Box>
	
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
