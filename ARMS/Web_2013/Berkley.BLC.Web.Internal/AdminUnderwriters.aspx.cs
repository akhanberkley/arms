using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web;
using Wilson.ORMapper;

public partial class AdminUnderwritersAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminUnderwritersAspx_Load);
        this.PreRender += new EventHandler(AdminUnderwritersAspx_PreRender);
        this.grdUnderwriters.SortCommand += new DataGridSortCommandEventHandler(grdUnderwriters_SortCommand);
        this.grdUnderwriters.PageIndexChanged += new DataGridPageChangedEventHandler(grdUnderwriters_PageIndexChanged);
    }
    #endregion

    void AdminUnderwritersAspx_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // restore sort expression, reverse state, and page index settings
            this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", grdUnderwriters.Columns[0].SortExpression);
            this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", false);
            grdUnderwriters.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);
        }
    }

    void AdminUnderwritersAspx_PreRender(object sender, EventArgs e)
    {
        string sortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sortExp = sortExp.Replace(" DESC", " TEMP");
            sortExp = sortExp.Replace(" ASC", " DESC");
            sortExp = sortExp.Replace(" TEMP", " ASC");
        }

        // get the company specific underwriters
        Underwriter[] eUnderwriters = Underwriter.GetSortedArray("CompanyID = ? && Disabled = ?", sortExp, this.CurrentUser.CompanyID, chkShowDisabled.Checked);

        string noRecordsMessage = string.Format("There are no {0} underwriters listed for your company.", (!chkShowDisabled.Checked) ? "active" : "disabled");
        grdUnderwriters.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindPagingGrid(grdUnderwriters, eUnderwriters, "Code", noRecordsMessage);
    }

    void grdUnderwriters_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sortRev);
    }

    void grdUnderwriters_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdUnderwriters.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);
    }
}
