using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using Wilson.ORMapper;
using QCI.Web;
using QCI.Web.Validation;
using QCI.ExceptionManagement;

public partial class CreateServicePlanAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(CreateServicePlanAspx_Load);
        this.PreRender += new EventHandler(CreateServicePlanAspx_PreRender);
        this.btnAddEmail.Click += new EventHandler(btnAddEmail_Click);
        this.lnkCreatePlan.Click += new EventHandler(lnkCreatePlan_Click);
        this.grdLocations.SortCommand += new DataGridSortCommandEventHandler(grdLocations_SortCommand);
        this.grdLocations.PageIndexChanged += new DataGridPageChangedEventHandler(grdLocations_PageIndexChanged);
        this.grdClaims.SortCommand += new DataGridSortCommandEventHandler(grdClaims_SortCommand);
        this.grdClaims.PageIndexChanged += new DataGridPageChangedEventHandler(grdClaims_PageIndexChanged);
        this.btnExport.Click += new EventHandler(btnExport_Click);
    }
    #endregion

    protected ServicePlan _eServicePlan;
    private const string claimsSortExpression = "ClaimsSortExpression";
    private const string claimsSortReversed = "ClaimsSortReversed";
    private const string claimsPageIndex = "ClaimsPageIndex";
    private const string locationsSortExpression = "LocationsSortExpression";
    private const string locationsSortReversed = "LocationsSortReversed";
    private const string locationsPageIndex = "LocationsPageIndex";

    void CreateServicePlanAspx_Load(object sender, EventArgs e)
    {
        Guid gServicePlanID = ARMSUtility.GetGuidFromQueryString("planid", true);
        _eServicePlan = ServicePlan.Get(gServicePlanID);
        
        base.PageHeading = string.Format("Insured: ({0})", _eServicePlan.Insured.Name);

        if (!this.IsPostBack)
        {
            // get all the agency email addresses
            AgencyEmail[] eEmails = AgencyEmail.GetSortedArray("AgencyID = ?", "EmailAddress ASC", _eServicePlan.Insured.AgencyID);
            cboAgentEmail.DataBind(eEmails, "ID", "EmailAddress");

            if (_eServicePlan.Insured.AgentEmailID != Guid.Empty)
            {
                ListItem item = new ListItem(_eServicePlan.Insured.AgencyEmail.EmailAddress, _eServicePlan.Insured.AgentEmailID.ToString());
                if (!cboAgentEmail.Items.Contains(item))
                {
                    cboAgentEmail.Items.Add(item);
                }
            }

            // get the policies of the insured
            Policy[] ePolicies = Policy.GetSortedArray("InsuredID = ? && IsActive = true", "Number ASC", _eServicePlan.InsuredID);
            DataGridHelper.BindGrid(grdPolicies, ePolicies, null, "There are no policies for this insured.");

            // get the primary contact for the first location
            LocationContact eContact = LocationContact.GetOne("Location[InsuredID = ? && Number = 1] && PriorityIndex = 1", _eServicePlan.InsuredID);

            this.PopulateFields(_eServicePlan.Insured);
            this.PopulateFields(_eServicePlan.Insured.Agency);

            if (eContact != null)
            {
                this.PopulateFields(eContact);
            }
            else
            {
                lblContactName.Text = "(not specified)";
                lblContactTitle.Text = "(not specified)";
                lblContactPhone.Text = "(not specified)";
                lblContactAltPhone.Text = "(not specified)";
                lblContactFax.Text = "(not specified)";
                lblContactEmail.Text = "(not specified)";
                lblContactAddress.Text = "(not specified)";
            }

            // restore sort expression, reverse state, and page index settings
            this.ViewState[locationsSortExpression] = this.UserIdentity.GetPageSetting(locationsSortExpression, grdLocations.Columns[0].SortExpression);
            this.ViewState[locationsSortReversed] = this.UserIdentity.GetPageSetting(locationsSortReversed, false);
            grdLocations.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting(locationsPageIndex, 0);

            this.ViewState[claimsSortExpression] = this.UserIdentity.GetPageSetting(claimsSortExpression, grdClaims.Columns[0].SortExpression);
            this.ViewState[claimsSortReversed] = this.UserIdentity.GetPageSetting(claimsSortReversed, false);
            grdClaims.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting(claimsPageIndex, 0);
        }
    }

    void CreateServicePlanAspx_PreRender(object sender, EventArgs e)
    {
        string sortLocationsExp = (string)this.ViewState[locationsSortExpression];
        if ((bool)this.ViewState[locationsSortReversed])
        {
            sortLocationsExp = sortLocationsExp.Replace(" DESC", " TEMP");
            sortLocationsExp = sortLocationsExp.Replace(" ASC", " DESC");
            sortLocationsExp = sortLocationsExp.Replace(" TEMP", " ASC");
        }

        string sortClaimsExp = (string)this.ViewState[claimsSortExpression];
        if ((bool)this.ViewState[claimsSortReversed])
        {
            sortClaimsExp = sortClaimsExp.Replace(" DESC", " TEMP");
            sortClaimsExp = sortClaimsExp.Replace(" ASC", " DESC");
            sortClaimsExp = sortClaimsExp.Replace(" TEMP", " ASC");
        }
        
        // get all the locations for this insured
        grdLocations.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        Location[] eLocations = Location.GetSortedArray("InsuredID = ? && IsActive = true", sortLocationsExp, _eServicePlan.InsuredID);
        DataGridHelper.BindPagingGrid(grdLocations, eLocations, "ID", "There are no locations for this insured.");

        grdClaims.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        Claim[] eClaims = Claim.GetSortedArray("Policy[InsuredID = ?]", sortClaimsExp, _eServicePlan.InsuredID);
        DataGridHelper.BindPagingGrid(grdClaims, eClaims, "ID", "There are no claims for this insured.");

        // get all the rate mod factors associated with this survey, if any
        RateModificationFactor[] rateModFactors = RateModificationFactor.GetSortedArray("Policy[InsuredID = ?]", "Policy.Number ASC, Policy.Mod ASC, StateCode ASC, TypeCode ASC", _eServicePlan.InsuredID);
        DataGridHelper.BindGrid(grdRateModFactors, rateModFactors, null, "There are no rate modification factors for this insured.");
    }

    void grdLocations_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdLocations.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting(locationsPageIndex, e.NewPageIndex);

        JavaScript.SetInputFocus(boxLocations);
    }

    void grdLocations_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState[locationsSortExpression];
        bool sortRev = (bool)this.ViewState[locationsSortReversed];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState[locationsSortExpression] = e.SortExpression;
        this.ViewState[locationsSortReversed] = sortRev;

        this.UserIdentity.SavePageSetting(locationsSortExpression, e.SortExpression);
        this.UserIdentity.SavePageSetting(locationsSortReversed, sortRev);

        JavaScript.SetInputFocus(boxLocations);
    }

    void grdClaims_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdClaims.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting(claimsPageIndex, e.NewPageIndex);

        JavaScript.SetInputFocus(boxClaims);
    }

    void grdClaims_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState[claimsSortExpression];
        bool sortRev = (bool)this.ViewState[claimsSortReversed];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState[claimsSortExpression] = e.SortExpression;
        this.ViewState[claimsSortReversed] = sortRev;

        this.UserIdentity.SavePageSetting(claimsSortExpression, e.SortExpression);
        this.UserIdentity.SavePageSetting(claimsSortReversed, sortRev);

        JavaScript.SetInputFocus(boxClaims);
    }

    protected string GetBuildingCount(object dataItem)
    {
        Location eLocation = (dataItem as Location);
        return Building.GetArray("LocationID = ?", eLocation.ID).Length.ToString();
    }

    protected string GetProfitCenterName(object obj)
    {
        Policy policy = (obj as Policy);
        ProfitCenter pr = ProfitCenter.GetOne("Code = ? && CompanyID = ?", policy.ProfitCenter, CurrentUser.CompanyID);

        if (pr != null)
        {
            return pr.Name;
        }
        else
        {
            return "(none)";
        }
    }

    void btnAddEmail_Click(object sender, EventArgs e)
    {
        //commit changes
        AgencyEmail eEmail;
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            // Insert new email
            eEmail = new AgencyEmail(Guid.NewGuid());
            eEmail.AgencyID = _eServicePlan.Insured.AgencyID;
            eEmail.EmailAddress = txtNewAgencyEmail.Text;
            eEmail.Save(trans);

            // Tie the new email to the insured
            Insured eInsured = _eServicePlan.Insured.GetWritableInstance();
            eInsured.AgentContactName = txtAgentContactName.Text;
            eInsured.AgentContactPhoneNumber = txtAgentContactPhone.Text;
            eInsured.AgentEmailID = eEmail.ID;
            eInsured.Save(trans);

            trans.Commit();
        }

        // Update the email list
        AgencyEmail[] eEmails = AgencyEmail.GetSortedArray("AgencyID = ?", "EmailAddress ASC", _eServicePlan.Insured.AgencyID);
        cboAgentEmail.DataBind(eEmails, "ID", "EmailAddress");

        // Set the new as selected by default
        cboAgentEmail.SelectedValue = eEmail.ID.ToString();
        txtNewAgencyEmail.Text = string.Empty;
    }

    void lnkCreatePlan_Click(object sender, EventArgs e)
    {
        if (SavePlan())
        {
            try
            {
                //Create the service plan
                ServicePlanManager manager = new ServicePlanManager(_eServicePlan, this.CurrentUser, false);
                manager.CreatePlan();
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);
                throw (ex);
            }

            Response.Redirect(string.Format("ServicePlan.aspx?planid={0}&edit=1", _eServicePlan.ID), true);
        }
    }

    private bool SavePlan()
    {
        try
        {
            //commit changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //save insured data
                Insured eInsured = _eServicePlan.Insured.GetWritableInstance();
                this.PopulateEntity(eInsured);
                eInsured.Save(trans);

                trans.Commit();
            }
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return false;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return false;
        }

        return true;
    }

    void btnExport_Click(object sender, EventArgs e)
    {
        Claim[] claims = Claim.GetSortedArray("Policy[InsuredID = ?]", this.ViewState[claimsSortExpression].ToString(), _eServicePlan.InsuredID);

        //Get the columns for the CSV export
        ExportItemList oExportDataList = new ExportItemList(this);
        for (int i = 0; i < grdClaims.Columns.Count; i++)
        {
            string dataType;
            switch (i)
            {
                case 0:
                    dataType = "String";
                    break;
                case 1:
                    dataType = "String";
                    break;
                case 2:
                    dataType = "DateTime";
                    break;
                case 3:
                    dataType = "DateTime";
                    break;
                case 4:
                    dataType = "Decimal";
                    break;
                case 5:
                    dataType = "String";
                    break;
                case 6:
                    dataType = "String";
                    break;
                case 7:
                    dataType = "String";
                    break;
                case 8:
                    dataType = "String";
                    break;
                default:
                    throw new NotSupportedException("Not expecting this many columns in the claims grid.");
            }

            DataGridColumn column = grdClaims.Columns[i];
            string filterName = column.SortExpression.Replace("ASC", "").Replace("DESC", "").Trim();
            oExportDataList.Add(new ExportItem(Guid.NewGuid(), column.SortExpression, filterName, dataType, column.HeaderText, i));
        }

        // Get the CSV data
        string strCsv = oExportDataList.GetCsv(claims, oExportDataList, "No Claims Found.");

        Response.ContentType = "application/x-msexcel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=claims.csv");

        // Write out the CSV data
        System.Text.ASCIIEncoding oEncoding = new System.Text.ASCIIEncoding(); ;
        byte[] rbBytes = oEncoding.GetBytes(strCsv);
        Response.OutputStream.Write(rbBytes, 0, rbBytes.Length);

        Response.End();
    }
}
