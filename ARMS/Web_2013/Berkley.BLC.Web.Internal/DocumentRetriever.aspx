<%@ Page Language="C#" AutoEventWireup="false" CodeFile="DocumentRetriever.aspx.cs" ValidateRequest="false" Inherits="DocumentRetrieverAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="DateRange" Src="~/Controls/DateRange.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px" onload="javascript:Enable()">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    <asp:HiddenField ID="hdnDocNumbers" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span>
    
    <% if (!_isExistingSurvey) {%>
        <a class="nav" href="CreateSurvey.aspx<%= ARMSUtility.GetQueryStringForNav() %>"><< Back to Insured</a>
    <%} else {%>
        <a class="nav" href="Survey.aspx<%= ARMSUtility.GetQueryStringForNav() %>"><%if (_eSurvey.ServiceType == Berkley.BLC.Entities.ServiceType.ServiceVisit) {%><< Back to Service Visit<%} else {%><< Back to Survey<%}%></a>
    <%} %>
    <br><br>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />

    <cc:Box id="boxSearch" runat="server" title="Document Search">
        <table class="fields">
            <uc:Combo id="cboPolicySystem" runat="server" field="" Name="Policy System" topItemText="" isRequired="true" />
            <uc:Text id="txtDocID" runat="server" field="Location.Number" Name="Document ID" IsRequired="false" MaxLength="40" Columns="35" />	
            <uc:Text id="txtPolicyNumber" runat="server" field="Policy.Number" Name="Policy Number(s)" IsRequired="false" Columns="35" Directions="(ex. 0123456, 6543210)" />
            <uc:Text id="txtClientID" runat="server" field="Insured.ClientID" Name="Client ID" IsRequired="false" MaxLength="12" Columns="15" />
            <uc:Text id="txtDocType" runat="server" field="Insured.StreetLine1" Name="Doc Type(s)" IsRequired="false" Columns="25" Directions="(ex. APP, END, LCC)" />
            <uc:DateRange id="drEntryDate" runat="server" Label="Entry Date" IsRequired="true" />
            <tr>
			    <td>&nbsp;</td>
			    <td class="buttons">
				    <asp:Button id="btnSearch" Runat="server" CssClass="btn" Text="Search" />
			    </td>
		    </tr>
        </table>
    </cc:Box>
    
    <cc:Box id="boxResults" runat="server" Title="Search Results" Visible="false">
        <div>Note: Please be patient when viewing or attaching documents from FileNet due to the size of documents.</div>
        <asp:datagrid ID="grdDocs" Runat="server" CssClass="grid" AutoGenerateColumns="False" PageSize="500" AllowPaging="False" AllowSorting="False">
	        <headerstyle CssClass="header" />
	        <itemstyle CssClass="item" />
	        <alternatingitemstyle CssClass="altItem" />
	        <footerstyle CssClass="footer" />
	        <pagerstyle CssClass="pager" Mode="NumericPages" />
	        <columns>
	            <asp:TemplateColumn HeaderText="Attach">
	                <ItemTemplate>
			                <asp:CheckBox ID="chkAttach" CssClass="chk" runat="server" />
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Name">
	                <ItemTemplate>
			                <asp:TextBox ID="txtFileName" CssClass="txt" Columns="25" MaxLength="100" Text="(none)" Enabled="False" runat="server"></asp:TextBox>
			                <%# StreamDocument.GetExtension(HtmlEncode(Container.DataItem, "MimeType"), true, string.Empty)%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Doc Number">
	                <ItemTemplate>
			                <asp:LinkButton ID="lnkDownloadDoc" runat="server" OnClick="lnkDownloadDoc_OnClick"><%# HtmlEncodeNullable(Container.DataItem, "DocNumber", string.Empty, "(none)")%></asp:LinkButton>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Doc Type">
	                <ItemTemplate>
			                <%# HtmlEncodeNullable(Container.DataItem, "DocType", string.Empty, string.Empty)%>
			                <asp:HiddenField ID="hdnDocType" runat="server" />
	                </ItemTemplate>
                </asp:TemplateColumn>
		        <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Policy Number">
	                <ItemTemplate>
			                <%# HtmlEncodeNullable(Container.DataItem, "PolicyNumber", string.Empty, string.Empty)%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Policy Type">
	                <ItemTemplate>
			                <%# HtmlEncodeNullable(Container.DataItem, "PolicySymbol", string.Empty, string.Empty)%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Entry Date">
	                <ItemTemplate>
			                <%# HtmlEncodeNullable(Container.DataItem, "EntryDate", string.Empty, string.Empty)%>
	                </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-Wrap="false" HeaderText="Doc Remarks">
	                <ItemTemplate>
			                <asp:TextBox ID="txtDocRemarks" CssClass="txt" Columns="50" runat="server"></asp:TextBox>
	                </ItemTemplate>
                </asp:TemplateColumn>
	        </columns>
        </asp:datagrid>
    </cc:Box>
    
    <% if (grdDocs.Items.Count > 0) { %>
        <asp:Button ID="btnAttach" Runat="server" CssClass="btn" Text="Attach To Survey" Visible="false"/>&nbsp;
        <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" Visible="false" />
    <% } %>
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <div style="display: none;"><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" alt="" /></div>
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function() { 
            $.blockUI.defaults.css = {}; 
            $.blockUI.defaults.overlayCSS.opacity = .2; 
            
            $('#btnAttach', this).click(function() { 
                $.blockUI({ message: '<h3><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" />&nbsp;&nbsp;&nbsp;Processing, one moment please...</h3>' });
            });
        });
    </script>
    
    <script language="javascript">
    function AddRemove(chk, docNumber, txtID)
    {
	    var hdn = document.getElementById('hdnDocNumbers');
	    var txt = document.getElementById(txtID);
	    
	    if (chk.checked) {
	        hdn.value = hdn.value + docNumber + '|';
	        txt.disabled = false;
	        if (txt.value == "(none)") txt.value = "";
	       
	    }
	    else {
	        hdn.value = hdn.value.replace(docNumber + '|', "");
	        txt.disabled = true;
	        if (txt.value == "") txt.value = "(none)";
	    }
    }
    function Enable()
    {
	    arrTxts = new Array();
		arrTxts = frm.getElementsByTagName('input');
		
		for (i=0; i<arrTxts.length; i++) { 
			var txt = arrTxts[i];
			if (txt != null && txt.value != "(none)"){
			    txt.disabled = false;
			}
			else {
			    txt.disabled = true;
			}
		}
    }
    </script>
    </form>
</body>
</html>
