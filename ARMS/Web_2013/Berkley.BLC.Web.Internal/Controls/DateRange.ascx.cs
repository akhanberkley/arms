using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Berkley.BLC.Web.Internal.Controls
{
    public partial class DateRange : System.Web.UI.UserControl
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(DateRange_Load);
            this.PreRender += new EventHandler(DateRange_PreRender);
        }
        #endregion

        void DateRange_Load(object sender, EventArgs e)
        {
        }

        void DateRange_PreRender(object sender, EventArgs e)
        {
            ARMSUtility.AddJavaScriptToPage(this.Page, ARMSUtility.AppPath + "/AutoFormat.js");

            bool required = this.IsRequired;
            string label = this.Label;

            valStart.Visible = required;
            valEnd.Visible = required;
            if (required)
            {
                valStart.Text = "*";
                valEnd.Text = "*";
                valStart.ErrorMessage = string.Format("Start date of {0} is required.", label);
                valEnd.ErrorMessage = string.Format("End date of {0} is required.", label);
            }

            valStartType.Text = "*";
            valStartType.ErrorMessage = string.Format("Start date of {0} is not a valid date.", label);

            valEndType.Text = "*";
            valEndType.ErrorMessage = string.Format("End date of {0} is not a valid date.", label);

            valBetween.Text = "*";
            valBetween.ErrorMessage = string.Format("Start date of {0} must be on or before end date.", label);

            valMin.Visible = (this.MinDate != DateTime.MinValue);
            if (valMin.Visible)
            {
                valMin.ValueToCompare = this.MinDate.ToShortDateString();
                valMin.Text = "*";
                valMin.ErrorMessage = string.Format("Start date of {0} must be on or after {1:d}.", label, this.MinDate);
            }

            valMax.Visible = (this.MaxDate != DateTime.MaxValue);
            if (valMax.Visible)
            {
                valMax.ValueToCompare = this.MaxDate.ToShortDateString();
                valMax.Text = "*";
                valMax.ErrorMessage = string.Format("End date of {0} must be on or before {1:d}.", label, this.MaxDate);
            }
        }

        /// <summary>
        /// Gets or sets the text value of the label to be shown.
        /// </summary>
        public string Label
        {
            get { return lbl.Text; }
            set { lbl.Text = value; }
        }

        /// <summary>
        /// Gets or sets the minimum date that can be selected by the user.
        /// </summary>
        public DateTime MinDate
        {
            get { return dpStart.MinDate; }
            set
            {
                dpStart.MinDate = value;
                dpEnd.MinDate = value;
            }
        }


        /// <summary>
        /// Gets or sets the maximum date that can be selected by the user.
        /// </summary>
        public DateTime MaxDate
        {
            get { return dpStart.MaxDate; }
            set
            {
                dpStart.MaxDate = value;
                dpEnd.MaxDate = value;
            }
        }


        /// <summary>
        /// Gets or sets a value indicating if this field is required.
        /// </summary>
        public bool IsRequired
        {
            get
            {
                object value = this.ViewState["IsRequired"];
                return (value != null) ? (bool)value : false;
            }
            set
            {
                this.ViewState["IsRequired"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether a postback to the server automatically occurs when the user changes the selected date.
        /// </summary>
        public bool AutoPostBack
        {
            get { return dpStart.AutoPostBack; }
            set
            {
                dpStart.AutoPostBack = value;
                dpEnd.AutoPostBack = value;
            }
        }


        /// <summary>
        /// Gets or sets the start of the date range.  MinValue indicates no date.
        /// </summary>
        public DateTime StartDate
        {
            get { return dpStart.Date; }
            set { dpStart.Date = value; }
        }


        /// <summary>
        /// Gets or sets the end of the date range.  MaxValue indicates no date.
        /// </summary>
        public DateTime EndDate
        {
            get { return (dpEnd.Date != DateTime.MinValue) ? dpEnd.Date : DateTime.MaxValue; }
            set { dpEnd.Date = value; }
        }


        /// <summary>
        /// Gets or sets a value indicating if a HTML table row tag should be injected into the page by this control.
        /// </summary>
        public bool TableRow
        {
            get
            {
                object value = this.ViewState["TableRow"];
                return (value != null) ? (bool)value : true;
            }
            set
            {
                this.ViewState["TableRow"] = value;
            }
        }
    }
}