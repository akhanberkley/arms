using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using Wilson.ORMapper;
using QCI.Web;

namespace Berkley.BLC.Web.Internal.Controls
{
    public partial class PermissionSetEditor : System.Web.UI.UserControl
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(PermissionSetEditor_Load);
            this.PreRender += new EventHandler(PermissionSetEditor_PreRender);
        }

        #endregion

        private SurveyAction[] _actions;
        private ServicePlanAction[] _planActions;
        protected string _itemStyle = null; // used during HTML rendering

        void PermissionSetEditor_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                // get all the available actions for this company
                // note: must be sorted by status code, than display order to be processed correctly later
                _actions = SurveyAction.GetSortedArray("CompanyID = ?", "SurveyStatus.DisplayOrder ASC, DisplayOrder ASC", UserIdentity.Current.CompanyID);
                _planActions = ServicePlanAction.GetSortedArray("CompanyID = ?", "ServicePlanStatus.DisplayOrder ASC, DisplayOrder ASC", UserIdentity.Current.CompanyID);

                // extract all unique status codes (that utilize methods)
                ArrayList statusList = new ArrayList();

                // no method status list
                ArrayList statusNoServiceList = new ArrayList();

                string lastStatusCode = null;
                foreach (SurveyAction action in _actions)
                {
                    if (action.SurveyStatusCode != lastStatusCode)
                    {
                        if (action.SurveyStatusCode == SurveyStatus.UnassignedSurvey.Code ||
                            action.SurveyStatusCode == SurveyStatus.OnHoldSurvey.Code ||
                            action.SurveyStatusCode == SurveyStatus.CanceledSurvey.Code)
                        {
                            statusNoServiceList.Add(action.SurveyStatus);
                        }
                        else
                        {
                            statusList.Add(action.SurveyStatus);
                        }
                        lastStatusCode = action.SurveyStatusCode;
                    }
                }

                // bind the status list to the repeater
                // note: this will cause the nested repeater to bind the actions for each status
                repStatus.DataSource = statusList;
                repStatus.DataBind();

                repStatusNoService.DataSource = statusNoServiceList;
                repStatusNoService.DataBind();

                repStatusServicePlan.DataSource = ServicePlanStatus.GetSortedArray("ServicePlanActions[!IsNull(ServicePlanStatusCode)]", "DisplayOrder ASC", null);
                repStatusServicePlan.DataBind();

                PopulateActionCombos();
            }
        }

        void PermissionSetEditor_PreRender(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                // nop
            }

            // Parameters
            bool _enableAdministrativeNotes = (Utility.GetCompanyParameter("EnableAdministrativeNotes", Company.Get(UserIdentity.Current.CompanyID)).ToUpper() == "TRUE") ? true : false;
            
            // Hide View Administrative Note option based on it being enabled via company parameter
            rdoCanViewAdministrativeNotes.Visible = _enableAdministrativeNotes;
        }

        private void PopulateActionCombos()
        {
            // traverse the nested repeaters, filling the combos with the correct set of items based on status type
            foreach (RepeaterItem statusItem in repStatus.Items)
            {
                // skip non-items
                if (statusItem.ItemType != ListItemType.Item && statusItem.ItemType != ListItemType.AlternatingItem)
                {
                    continue;
                }

                string statusCode = (statusItem.FindControl("hdnStatusCode") as HtmlInputHidden).Value;

                // add the items to each action's combos
                Repeater repActions = (Repeater)statusItem.FindControl("repActions");
                foreach (RepeaterItem actionItem in repActions.Items)
                {
                    // skip non-items
                    if (actionItem.ItemType != ListItemType.Item && actionItem.ItemType != ListItemType.AlternatingItem)
                    {
                        continue;
                    }

                    DropDownList cboRequest = (DropDownList)actionItem.FindControl("cboRequest");
                    cboRequest.Items.AddRange(GetListItems(statusCode));

                    DropDownList cboService = (DropDownList)actionItem.FindControl("cboService");
                    cboService.Items.AddRange(GetListItems(statusCode));
                }
            }

            // traverse the nested repeaters, filling the combos with the correct set of items based on status type
            foreach (RepeaterItem statusItem in repStatusNoService.Items)
            {
                // skip non-items
                if (statusItem.ItemType != ListItemType.Item && statusItem.ItemType != ListItemType.AlternatingItem)
                {
                    continue;
                }

                string statusCode = (statusItem.FindControl("hdnStatusCodeNoService") as HtmlInputHidden).Value;

                // add the items to each action's combos
                Repeater repActions = (Repeater)statusItem.FindControl("repActionsNoService");
                foreach (RepeaterItem actionItem in repActions.Items)
                {
                    // skip non-items
                    if (actionItem.ItemType != ListItemType.Item && actionItem.ItemType != ListItemType.AlternatingItem)
                    {
                        continue;
                    }

                    DropDownList cboNoService = (DropDownList)actionItem.FindControl("cboNoService");
                    cboNoService.Items.AddRange(GetListItems(statusCode));
                }
            }

            // traverse the nested repeaters, filling the combos with the correct set of items based on status type
            foreach (RepeaterItem statusItem in repStatusServicePlan.Items)
            {
                // skip non-items
                if (statusItem.ItemType != ListItemType.Item && statusItem.ItemType != ListItemType.AlternatingItem)
                {
                    continue;
                }

                string statusCode = (statusItem.FindControl("hdnStatusCodeServicePlan") as HtmlInputHidden).Value;

                // add the items to each action's combos
                Repeater repActions = (Repeater)statusItem.FindControl("repActionsServicePlan");
                foreach (RepeaterItem actionItem in repActions.Items)
                {
                    // skip non-items
                    if (actionItem.ItemType != ListItemType.Item && actionItem.ItemType != ListItemType.AlternatingItem)
                    {
                        continue;
                    }

                    ListItem[] items;
                    if (statusCode == ServicePlanStatus.AssignedPlan.Code)
                    {
                        items = new ListItem[4];
                        items[0] = new ListItem("None", "0");
                        items[1] = new ListItem("Own", "1");
                        items[2] = new ListItem("Others", "2");
                        items[3] = new ListItem("Own and Others", "4");
                    }
                    else
                    {
                        items = new ListItem[2];
                        items[0] = new ListItem("No", "0");
                        items[1] = new ListItem("Yes", "7");
                    }

                    DropDownList cboServicePlan = (DropDownList)actionItem.FindControl("cboServicePlan");
                    cboServicePlan.Items.AddRange(items);
                }
            }
        }

        private ListItem[] GetListItems(string statusCode)
        {
            // get the item set based on the status type
            ListItem[] items;
            if (statusCode == SurveyStatus.UnassignedSurvey.Code
                || statusCode == SurveyStatus.OnHoldSurvey.Code
                || statusCode == SurveyStatus.UnassignedReview.Code)
            {
                items = new ListItem[2];
                items[0] = new ListItem("No", "0");
                items[1] = new ListItem("Yes", "7");
            }
            else if (statusCode == SurveyStatus.AssignedSurvey.Code
                || statusCode == SurveyStatus.AwaitingAcceptance.Code)
            {
                items = new ListItem[8];
                items[0] = new ListItem("None", "0");
                items[1] = new ListItem("Own", "1");
                items[2] = new ListItem("Others", "2");
                items[3] = new ListItem("Vendors", "3");
                items[4] = new ListItem("Own and Others", "4");
                items[5] = new ListItem("Others and Vendors", "5");
                items[6] = new ListItem("Own and Vendors", "6");
                items[7] = new ListItem("Own, Others and Vendors", "7");
            }
            else
            {
                items = new ListItem[4];
                items[0] = new ListItem("None", "0");
                items[1] = new ListItem("Own", "1");
                items[2] = new ListItem("Others", "2");
                items[3] = new ListItem("Own and Others", "4");
            }

            return items;
        }

        /// <summary>
        /// Loads action permissions for a PermissionSet into the control for editing by the user.
        /// </summary>
        /// <param name="permissionSet">PermissionSet to be loaded.</param>
        public void LoadPermissionSet(PermissionSet permissionSet)
        {
            // populate the general permisison checkboxes
            (this.Page as AppBasePage).PopulateFields(permissionSet);

            // get the action permissions settings for this set
            SurveyActionPermission[] permissions = SurveyActionPermission.GetArray("PermissionSetID = ?", permissionSet.ID);

            // update the radio buttons based on the permissions
            SetActionPermissions(permissions);

            // get the action permissions settings for this set
            ServicePlanActionPermission[] planPermissions = ServicePlanActionPermission.GetArray("PermissionSetID = ?", permissionSet.ID);

            // update the radio buttons based on the permissions
            SetActionPermissions(planPermissions);
        }


        protected SurveyAction[] GetActionsForStatus(object dataItem)
        {
            SurveyStatus status = (SurveyStatus)dataItem;

            ArrayList list = new ArrayList();
            foreach (SurveyAction action in _actions)
            {
                if (action.SurveyStatusCode == status.Code)
                {
                    list.Add(action);
                }
            }

            return (SurveyAction[])list.ToArray(typeof(SurveyAction));
        }

        protected ServicePlanAction[] GetActionsForPlanStatus(object dataItem)
        {
            ServicePlanStatus status = (ServicePlanStatus)dataItem;

            ArrayList list = new ArrayList();
            foreach (ServicePlanAction action in _planActions)
            {
                if (action.ServicePlanStatusCode == status.Code)
                {
                    list.Add(action);
                }
            }

            return (ServicePlanAction[])list.ToArray(typeof(ServicePlanAction));
        }

        public void Save(Transaction trans, PermissionSet permissionSet)
        {
            // get the array of new action permissions
            SurveyActionPermission[] newPermissions = GetActionPermissions(permissionSet);

            // get the array of new action plan permissions
            ServicePlanActionPermission[] newPlanPermissions = GetPlanActionPermissions(permissionSet);

            // populate the permission set
            (this.Page as AppBasePage).PopulateEntity(permissionSet);

            // update the CanWorkXXX flags based on action settings
            permissionSet.CanWorkSurveys = false;
            permissionSet.CanWorkReviews = false;
            permissionSet.CanWorkServicePlans = false;
            foreach (SurveyActionPermission permission in newPermissions)
            {
                // skip non-owner related settings
                if (permission.Setting != 1 && permission.Setting != 4 && permission.Setting != 6 && permission.Setting != 7)
                {
                    continue;
                }

                string code = permission.SurveyAction.SurveyStatusCode;
                if (code == SurveyStatus.AssignedSurvey.Code && permission.SurveyAction.Type == SurveyActionType.SendForReview)
                {
                    permissionSet.CanWorkSurveys = true;
                }
                else if (code == SurveyStatus.UnassignedReview.Code && permission.SurveyAction.Type == SurveyActionType.TakeOwnership)
                {
                    permissionSet.CanWorkReviews = true;
                }
            }

            foreach (ServicePlanActionPermission permission in newPlanPermissions)
            {
                if (permission.Setting == 1 || permission.Setting == 4 || permission.Setting == 7)
                {
                    if (permission.ServicePlanAction.ServicePlanStatusCode == ServicePlanStatus.AssignedPlan.Code)
                    {
                        permissionSet.CanWorkServicePlans = true;
                    }
                }
            }

            // save the permission set
            permissionSet.Save(trans);

            // refresh the action permissions by deleteing the old and adding the new
            SurveyActionPermission[] oldPermissions = SurveyActionPermission.GetArray("PermissionSetID = ?", permissionSet.ID);
            foreach (SurveyActionPermission permission in oldPermissions)
            {
                permission.Delete(trans);
            }
            foreach (SurveyActionPermission permission in newPermissions)
            {
                permission.Save(trans);
            }

            // refresh the action permissions by deleteing the old and adding the new
            ServicePlanActionPermission[] oldPlanPermissions = ServicePlanActionPermission.GetArray("PermissionSetID = ?", permissionSet.ID);
            foreach (ServicePlanActionPermission planPermission in oldPlanPermissions)
            {
                planPermission.Delete(trans);
            }
            foreach (ServicePlanActionPermission planPermission in newPlanPermissions)
            {
                planPermission.Save(trans);
            }
        }


        private void SetActionPermissions(SurveyActionPermission[] permissions)
        {
            Hashtable actionMap = new Hashtable(50);

            // traverse the nested repeaters and build a hashtable mapping action ID to repeater item
            foreach (RepeaterItem statusItem in repStatus.Items)
            {
                if (statusItem.ItemType == ListItemType.Item || statusItem.ItemType == ListItemType.AlternatingItem)
                {
                    Repeater repActions = (Repeater)statusItem.FindControl("repActions");
                    foreach (RepeaterItem actionItem in repActions.Items)
                    {
                        if (actionItem.ItemType == ListItemType.Item || actionItem.ItemType == ListItemType.AlternatingItem)
                        {
                            HtmlInputHidden hdnActionID = (HtmlInputHidden)actionItem.FindControl("hdnActionID");
                            Guid actionID = new Guid(hdnActionID.Value);

                            actionMap.Add(actionID, actionItem);
                        }
                    }
                }
            }

            foreach (RepeaterItem statusItem in repStatusNoService.Items)
            {
                if (statusItem.ItemType == ListItemType.Item || statusItem.ItemType == ListItemType.AlternatingItem)
                {
                    Repeater repActionsNoService = (Repeater)statusItem.FindControl("repActionsNoService");
                    foreach (RepeaterItem actionItem in repActionsNoService.Items)
                    {
                        if (actionItem.ItemType == ListItemType.Item || actionItem.ItemType == ListItemType.AlternatingItem)
                        {
                            HtmlInputHidden hdnActionID = (HtmlInputHidden)actionItem.FindControl("hdnActionIDNoService");
                            Guid actionID = new Guid(hdnActionID.Value);

                            actionMap.Add(actionID, actionItem);
                        }
                    }
                }
            }

            // process all the passed permissions
            foreach (SurveyActionPermission permission in permissions)
            {
                RepeaterItem actionItem = (RepeaterItem)actionMap[permission.SurveyActionID];
                SetComboValue(actionItem, permission);
            }
        }

        private void SetActionPermissions(ServicePlanActionPermission[] permissions)
        {
            Hashtable actionMap = new Hashtable(50);

            // traverse the nested repeaters and build a hashtable mapping action ID to repeater item
            foreach (RepeaterItem statusItem in repStatusServicePlan.Items)
            {
                if (statusItem.ItemType == ListItemType.Item || statusItem.ItemType == ListItemType.AlternatingItem)
                {
                    Repeater repActionsServicePlan = (Repeater)statusItem.FindControl("repActionsServicePlan");
                    foreach (RepeaterItem actionItem in repActionsServicePlan.Items)
                    {
                        if (actionItem.ItemType == ListItemType.Item || actionItem.ItemType == ListItemType.AlternatingItem)
                        {
                            HtmlInputHidden hdnActionID = (HtmlInputHidden)actionItem.FindControl("hdnActionIDServicePlan");
                            Guid actionID = new Guid(hdnActionID.Value);

                            actionMap.Add(actionID, actionItem);
                        }
                    }
                }
            }

            // process all the passed permissions
            foreach (ServicePlanActionPermission permission in permissions)
            {
                RepeaterItem actionItem = (RepeaterItem)actionMap[permission.ServicePlanActionID];
                SetComboValue(actionItem, permission);
            }
        }

        private void SetComboValue(RepeaterItem item, SurveyActionPermission permission)
        {
            if (item.Parent.ID == "repActions")
            {

                // determine the control ID based on method type
                string controlID;
                if (permission.ServiceTypeCode == ServiceType.SurveyRequest.Code)
                {
                    controlID = "cboRequest";
                }
                else if (permission.ServiceTypeCode == ServiceType.ServiceVisit.Code)
                {
                    controlID = "cboService";
                }
                else
                {
                    throw new NotSupportedException("Survey service code '" + permission.ServiceTypeCode + "' was not expected.");
                }

                // set the checked state for each radio buttons
                DropDownList cbo = (DropDownList)item.FindControl(controlID);
                ComboHelper.SelectComboItem(cbo, permission.Setting.ToString());
            }
            else if (item.Parent.ID == "repActionsNoService")
            {
                // set the checked state for each radio buttons
                DropDownList cbo = (DropDownList)item.FindControl("cboNoService");
                ComboHelper.SelectComboItem(cbo, permission.Setting.ToString());
            }
        }

        private void SetComboValue(RepeaterItem item, ServicePlanActionPermission permission)
        {
            if (item.Parent.ID == "repActionsServicePlan")
            {
                // set the checked state for each radio buttons
                DropDownList cbo = (DropDownList)item.FindControl("cboServicePlan");
                ComboHelper.SelectComboItem(cbo, permission.Setting.ToString());
            }
        }


        private SurveyActionPermission[] GetActionPermissions(PermissionSet permissionSet)
        {
            ArrayList list = new ArrayList(50);

            // traverse the nested repeaters extracting the selected settings
            // and building SurveyActionPermission objects
            foreach (RepeaterItem statusItem in repStatus.Items)
            {
                // skip non-items
                if (statusItem.ItemType != ListItemType.Item && statusItem.ItemType != ListItemType.AlternatingItem)
                {
                    continue;
                }

                HtmlInputHidden hdnStatusCode = (HtmlInputHidden)statusItem.FindControl("hdnStatusCode");
                Repeater repActions = (Repeater)statusItem.FindControl("repActions");

                foreach (RepeaterItem actionItem in repActions.Items)
                {
                    // skip non-items
                    if (actionItem.ItemType != ListItemType.Item && actionItem.ItemType != ListItemType.AlternatingItem)
                    {
                        continue;
                    }

                    HtmlInputHidden hdnActionID = (HtmlInputHidden)actionItem.FindControl("hdnActionID");
                    Guid actionID = new Guid(hdnActionID.Value);

                    int setting;

                    // survey requests
                    setting = GetComboValue(actionItem, "cboRequest");
                    if (setting > 0)
                    {
                        SurveyActionPermission permission = new SurveyActionPermission(Guid.NewGuid());
                        permission.PermissionSetID = permissionSet.ID;
                        permission.SurveyActionID = actionID;
                        permission.ServiceTypeCode = ServiceType.SurveyRequest.Code;
                        permission.Setting = setting;

                        list.Add(permission);
                    }

                    // service visits
                    setting = GetComboValue(actionItem, "cboService");
                    if (setting > 0)
                    {
                        SurveyActionPermission permission = new SurveyActionPermission(Guid.NewGuid());
                        permission.PermissionSetID = permissionSet.ID;
                        permission.SurveyActionID = actionID;
                        permission.ServiceTypeCode = ServiceType.ServiceVisit.Code;
                        permission.Setting = setting;

                        list.Add(permission);
                    }
                }
            }

            foreach (RepeaterItem statusItem in repStatusNoService.Items)
            {
                // skip non-items
                if (statusItem.ItemType != ListItemType.Item && statusItem.ItemType != ListItemType.AlternatingItem)
                {
                    continue;
                }

                HtmlInputHidden hdnStatusCode = (HtmlInputHidden)statusItem.FindControl("hdnStatusCodeNoService");
                Repeater repActions = (Repeater)statusItem.FindControl("repActionsNoService");

                foreach (RepeaterItem actionItem in repActions.Items)
                {
                    // skip non-items
                    if (actionItem.ItemType != ListItemType.Item && actionItem.ItemType != ListItemType.AlternatingItem)
                    {
                        continue;
                    }

                    HtmlInputHidden hdnActionID = (HtmlInputHidden)actionItem.FindControl("hdnActionIDNoService");
                    Guid actionID = new Guid(hdnActionID.Value);

                    int setting;

                    // No Service Surveys
                    setting = GetComboValue(actionItem, "cboNoService");
                    if (setting > 0)
                    {
                        SurveyActionPermission permission = new SurveyActionPermission(Guid.NewGuid());
                        permission.PermissionSetID = permissionSet.ID;
                        permission.SurveyActionID = actionID;
                        permission.Setting = setting;

                        list.Add(permission);
                    }
                }
            }

            return (SurveyActionPermission[])list.ToArray(typeof(SurveyActionPermission));
        }

        private ServicePlanActionPermission[] GetPlanActionPermissions(PermissionSet permissionSet)
        {
            ArrayList list = new ArrayList(50);

            // traverse the nested repeaters extracting the selected settings
            // and building ServicePlanActionPermission objects
            foreach (RepeaterItem statusItem in repStatusServicePlan.Items)
            {
                // skip non-items
                if (statusItem.ItemType != ListItemType.Item && statusItem.ItemType != ListItemType.AlternatingItem)
                {
                    continue;
                }

                HtmlInputHidden hdnStatusCode = (HtmlInputHidden)statusItem.FindControl("hdnStatusCodeServicePlan");
                Repeater repActions = (Repeater)statusItem.FindControl("repActionsServicePlan");

                foreach (RepeaterItem actionItem in repActions.Items)
                {
                    // skip non-items
                    if (actionItem.ItemType != ListItemType.Item && actionItem.ItemType != ListItemType.AlternatingItem)
                    {
                        continue;
                    }

                    HtmlInputHidden hdnActionID = (HtmlInputHidden)actionItem.FindControl("hdnActionIDServicePlan");
                    Guid actionID = new Guid(hdnActionID.Value);

                    int setting;

                    // No Service Surveys
                    setting = GetComboValue(actionItem, "cboServicePlan");
                    if (setting > 0)
                    {
                        ServicePlanActionPermission permission = new ServicePlanActionPermission(Guid.NewGuid());
                        permission.PermissionSetID = permissionSet.ID;
                        permission.ServicePlanActionID = actionID;
                        permission.Setting = setting;

                        list.Add(permission);
                    }
                }
            }

            return (ServicePlanActionPermission[])list.ToArray(typeof(ServicePlanActionPermission));
        }

        private int GetComboValue(RepeaterItem item, string controlID)
        {
            DropDownList cbo = (DropDownList)item.FindControl(controlID);

            string value = cbo.SelectedValue;
            if (value != null && value.Length > 0)
            {
                return int.Parse(value);
            }
            else
            {
                return 0;
            }
        }
    }
}
