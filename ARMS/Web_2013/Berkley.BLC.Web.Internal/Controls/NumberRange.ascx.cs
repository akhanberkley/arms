using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Berkley.BLC.Web.Internal.Controls
{
    public partial class NumberRange : System.Web.UI.UserControl
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(NumberRange_Load);
            this.PreRender += new EventHandler(NumberRange_PreRender);
        }
        #endregion

        void NumberRange_Load(object sender, EventArgs e)
        {
        }

        void NumberRange_PreRender(object sender, EventArgs e)
        {
            bool required = this.IsRequired;
            string label = this.Label;

            valStart.Visible = required;
            valEnd.Visible = required;
            if (required)
            {
                valStart.Text = "*";
                valStart.ErrorMessage = string.Format("Minimum value of {0} is required.", label);
                valEnd.Text = "*";
                valEnd.ErrorMessage = string.Format("Maximum value of {0} is required.", label);
            }

            valStartType.Text = "*";
            valStartType.ErrorMessage = string.Format("Minimum value of {0} is not a valid number.", label);

            valEndType.Text = "*";
            valEndType.ErrorMessage = string.Format("Maximum value of {0} is not a valid number.", label);

            valBetween.Text = "*";
            valBetween.ErrorMessage = string.Format("Minimum value of {0} must be less than or equal to maximum value.", label);

            valMin.Visible = (this.MinValue != decimal.MinValue);
            if (valMin.Visible)
            {
                valMin.ValueToCompare = this.MinValue.ToString();
                valMin.Text = "*";
                valMin.ErrorMessage = string.Format("Minimum value of {0} cannot be less than {1}.", label, this.MinValue);
            }

            valMax.Visible = (this.MaxValue != decimal.MaxValue);
            if (valMax.Visible)
            {
                valMax.ValueToCompare = this.MaxValue.ToString();
                valMax.Text = "*";
                valMax.ErrorMessage = string.Format("Maximum value of {0} cannot be greater than {1}.", label, this.MaxValue);
            }
        }

        /// <summary>
        /// Gets or sets the text value of the label to be shown.
        /// </summary>
        public string Label
        {
            get { return lbl.Text; }
            set { lbl.Text = value; }
        }


        /// <summary>
        /// Gets or sets the minimum value that can be entered by the user.  Decimal.MinValue indicates no minimum value.
        /// </summary>
        public decimal MinValue
        {
            get
            {
                object value = this.ViewState["MinValue"];
                return (value != null) ? (decimal)value : decimal.MinValue;
            }
            set
            {
                this.ViewState["MinValue"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the maximum value that can be entered by the user. Decimal.MaxValue indicates no maximum value.
        /// </summary>
        public decimal MaxValue
        {
            get
            {
                object value = this.ViewState["MaxValue"];
                return (value != null) ? (decimal)value : decimal.MaxValue;
            }
            set
            {
                this.ViewState["MaxValue"] = value;
            }
        }


        /// <summary>
        /// Gets or sets a value indicating if this field is required.
        /// </summary>
        public bool IsRequired
        {
            get
            {
                object value = this.ViewState["IsRequired"];
                return (value != null) ? (bool)value : false;
            }
            set
            {
                this.ViewState["IsRequired"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the start value in the range.  MinValue indicates no value.
        /// </summary>
        public decimal StartValue
        {
            get
            {
                string text = txtStart.Text.Trim();
                return (text.Length > 0) ? decimal.Parse(text) : decimal.MinValue;
            }
            set
            {
                string format = this.ValueFormat;
                txtStart.Text = (format != null && format.Length > 0) ? string.Format(format, value) : value.ToString();
            }
        }


        /// <summary>
        /// Gets or sets the end value in the range.  MaxValue indicates no value.
        /// </summary>
        public decimal EndValue
        {
            get
            {
                string text = txtEnd.Text.Trim();
                return (text.Length > 0) ? decimal.Parse(text) : decimal.MaxValue;
            }
            set
            {
                string format = this.ValueFormat;
                txtEnd.Text = (format != null && format.Length > 0) ? string.Format(format, value) : value.ToString();
            }
        }


        /// <summary>
        /// Gets or sets the string format to apply when presenting values to the user.
        /// </summary>
        public string ValueFormat
        {
            get { return (string)this.ViewState["ValueFormat"]; }
            set { this.ViewState["ValueFormat"] = value; }
        }


        /// <summary>
        /// Gets or sets a value indicating if a HTML table row tag should be injected into the page by this control.
        /// </summary>
        public bool TableRow
        {
            get
            {
                object value = this.ViewState["TableRow"];
                return (value != null) ? (bool)value : true;
            }
            set
            {
                this.ViewState["TableRow"] = value;
            }
        }
    }
}