<%@ Control Language="C#" AutoEventWireup="false" CodeFile="PermissionSetEditor.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Controls.PermissionSetEditor" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>

<cc:Box id="boxGeneral" runat="server" title="General Permissions" width="515">
    <table class="fields" cellpadding="0" cellspacing="0">
    <uc:Check id="rdoCanViewCompanySummary" runat="server" field="PermissionSet.CanViewCompanySummary" Type="RadioButton" />
    <uc:Check id="rdoCanSearchSurveys" runat="server" field="PermissionSet.CanSearchSurveys" Type="RadioButton" />
    <uc:Check id="rdoCanCreateSurvey" runat="server" field="PermissionSet.CanCreateSurveys" Type="RadioButton" />
    <uc:Check id="rdoCanCreateServicePlans" runat="server" field="PermissionSet.CanCreateServicePlans" Type="RadioButton" />
    <uc:Check id="rdoCanViewSurveyReviewQueues" runat="server" field="PermissionSet.CanViewSurveyReviewQueues" Type="RadioButton" />
    <uc:Check id="rdoCanViewLinks" runat="server" field="PermissionSet.CanViewLinks" Type="RadioButton" />
    <uc:Check id="rdoCanViewSurveyHistory" runat="server" field="PermissionSet.CanViewSurveyHistory" Type="RadioButton" />
    <uc:Check id="rdoCanRequireSavedSearch" runat="server" field="PermissionSet.CanRequireSavedSearch" Type="RadioButton" />
    <uc:Check id="rdoCanAdminUserRoles" runat="server" field="PermissionSet.CanAdminUserRoles" Type="RadioButton" />
    <uc:Check id="rdoCanAdminUserAccounts" runat="server" field="PermissionSet.CanAdminUserAccounts" Type="RadioButton" />
    <uc:Check id="rdoCanAdminFeeCompanies" runat="server" field="PermissionSet.CanAdminFeeCompanies" Type="RadioButton" />
    <uc:Check id="rdoCanAdminTerritories" runat="server" field="PermissionSet.CanAdminTerritories" Type="RadioButton" />
    <uc:Check id="rdoCanAdminRules" runat="server" field="PermissionSet.CanAdminRules" Type="RadioButton" />
    <uc:Check id="rdoCanAdminLetters" runat="server" field="PermissionSet.CanAdminLetters" Type="RadioButton" />
    <uc:Check id="rdoCanAdminReports" runat="server" field="PermissionSet.CanAdminReports" Type="RadioButton" />
    <uc:Check id="rdoCanAdminDocuments" runat="server" field="PermissionSet.CanAdminDocuments" Name="Can Admin Service Plan Templates" Type="RadioButton" />
    <uc:Check id="rdoCanAdminResultsDisplay" runat="server" field="PermissionSet.CanAdminResultsDisplay" Type="RadioButton" />
    <uc:Check id="rdoCanAdminRecommendations" runat="server" field="PermissionSet.CanAdminRecommendations" Type="RadioButton" />
    <uc:Check id="rdoCanAdminServicePlanFields" runat="server" field="PermissionSet.CanAdminServicePlanFields" Type="RadioButton" Visible="false" />
    <uc:Check id="rdoCanAdminLinks" runat="server" field="PermissionSet.CanAdminLinks" Type="RadioButton" />
    <uc:Check id="rdoCanAdminUnderwriters" runat="server" field="PermissionSet.CanAdminUnderwriters" Type="RadioButton" />
    <uc:Check id="rdoCanAdminClients" runat="server" field="PermissionSet.CanAdminClients" Type="RadioButton" />
    <uc:Check id="rdoCanAdminActivityTimes" runat="server" field="PermissionSet.CanAdminActivityTimes" Type="RadioButton" />
    <uc:Check id="rdoCanAdminHelpfulHints" runat="server" field="PermissionSet.CanAdminHelpfulHints" Type="RadioButton" />
    <uc:Check id="rdoCanWorkAgencyVisits" runat="server" field="PermissionSet.CanWorkAgencyVisits" Type="RadioButton" />
    <uc:Check id="rdoCanViewAdministrativeNotes" runat="server" field="PermissionSet.CanViewAdministrativeNotes" Type="RadioButton" />
    </table>
</cc:Box>

<cc:Box id="boxActions" runat="server" title="Action Permissions" width="515">

    <asp:Repeater ID="repStatusNoService" Runat="server">
    <ItemTemplate>
        <div style="MARGIN-TOP: 10px">
            <b><%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem, "Name", "{0}")) %></b>
            <input type="hidden" id="hdnStatusCodeNoService" runat="server" value='<%# DataBinder.Eval(Container.DataItem, "Code") %>'/>
        </div>
        <table class="grid" cellspacing="0" border="0" cellpadding="0">
        <tr class="header">
            <td>Action</td>
            <td>All Surveys</td>
        </tr>
        
            <% _itemStyle = null; %>
            <asp:Repeater ID="repActionsNoService" Runat="server" DataSource="<%# GetActionsForStatus(Container.DataItem) %>">
                <ItemTemplate>
        <% _itemStyle = (_itemStyle != "item") ? "item" : "altItem"; %>
        <tr class="<%= _itemStyle %>">
            <td width="145" nowrap>
                <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem, "DisplayName", "{0}")) %>
                <input type="hidden" id="hdnActionIDNoService" runat="server" value='<%# DataBinder.Eval(Container.DataItem, "ID") %>'/>
            </td>
            <td style="PADDING: 4px 10px 4px 10px" nowrap>
                <asp:DropDownList ID="cboNoService" Runat="server" CssClass="cbo" Width="150" />
            </td>
        </tr>
                </ItemTemplate>
            </asp:Repeater>
            
        </table>
    </ItemTemplate>
    </asp:Repeater>

</cc:Box>

<cc:Box id="boxActionsWithService" runat="server" title="Action Permissions by Service Type" width="515">

    <asp:Repeater ID="repStatus" Runat="server">
    <ItemTemplate>
        <div style="MARGIN-TOP: 10px">
            <b><%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem, "Name", "{0}")) %></b>
            <input type="hidden" id="hdnStatusCode" runat="server" value='<%# DataBinder.Eval(Container.DataItem, "Code") %>' />
        </div>
        <table class="grid" cellspacing="0" border="0" cellpadding="0">
        <tr class="header">
            <td>Action</td>
            <td>Survey Requests</td>
            <td>Service Visits</td>
        </tr>
        
            <% _itemStyle = null; %>
            <asp:Repeater ID="repActions" Runat="server" DataSource="<%# GetActionsForStatus(Container.DataItem) %>">
                <ItemTemplate>
        <% _itemStyle = (_itemStyle != "item") ? "item" : "altItem"; %>
        <tr class="<%= _itemStyle %>">
            <td width="145" nowrap>
                <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem, "DisplayName", "{0}")) %>
                <input type="hidden" id="hdnActionID" runat="server" value='<%# DataBinder.Eval(Container.DataItem, "ID") %>' />
            </td>
            <td style="PADDING: 4px 10px 4px 10px" nowrap>
                <asp:DropDownList ID="cboRequest" Runat="server" CssClass="cbo" Width="150" />
            </td>
            <td style="PADDING: 4px 10px 4px 10px" nowrap>
                <asp:DropDownList ID="cboService" Runat="server" CssClass="cbo" Width="150" />
            </td>
        </tr>
                </ItemTemplate>
            </asp:Repeater>
            
        </table>
    </ItemTemplate>
    </asp:Repeater>

</cc:Box>

<cc:Box id="boxServicePlanActions" runat="server" title="Action Permissions for Service Plans" width="515">

    <asp:Repeater ID="repStatusServicePlan" Runat="server">
    <ItemTemplate>
        <div style="MARGIN-TOP: 10px">
            <b><%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem, "Name", "{0}")) %></b>
            <input type="hidden" id="hdnStatusCodeServicePlan" runat="server" value='<%# DataBinder.Eval(Container.DataItem, "Code") %>'/>
        </div>
        <table class="grid" cellspacing="0" border="0" cellpadding="0">
        <tr class="header">
            <td>Action</td>
            <td>All Service Plans</td>
        </tr>
        
            <% _itemStyle = null; %>
            <asp:Repeater ID="repActionsServicePlan" Runat="server" DataSource="<%# GetActionsForPlanStatus(Container.DataItem) %>">
                <ItemTemplate>
        <% _itemStyle = (_itemStyle != "item") ? "item" : "altItem"; %>
        <tr class="<%= _itemStyle %>">
            <td width="145" nowrap>
                <%# HttpUtility.HtmlEncode(DataBinder.Eval(Container.DataItem, "DisplayName", "{0}")) %>
                <input type="hidden" id="hdnActionIDServicePlan" runat="server" value='<%# DataBinder.Eval(Container.DataItem, "ID") %>'/>
            </td>
            <td style="PADDING: 4px 10px 4px 10px" nowrap>
                <asp:DropDownList ID="cboServicePlan" Runat="server" CssClass="cbo" Width="150" />
            </td>
        </tr>
                </ItemTemplate>
            </asp:Repeater>
            
        </table>
    </ItemTemplate>
    </asp:Repeater>

</cc:Box>
