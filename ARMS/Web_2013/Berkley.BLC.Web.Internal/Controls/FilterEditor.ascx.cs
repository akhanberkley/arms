using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Berkley.BLC.Entities;
using QCI.Web;
using QCI.Web.Validation;
using Wilson.ORMapper;

namespace Berkley.BLC.Web.Internal.Controls
{
    public partial class FilterEditor : System.Web.UI.UserControl
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(FilterEditor_Load);
            this.PreRender += new EventHandler(FilterEditor_PreRender);
            this.btnAdd.Click += new EventHandler(btnAdd_Click);
            this.phConditions.ControlRestored += new DynamicControlEventHandler(phConditions_ControlRestored);
        }
        #endregion

        private Guid companyID;

        void FilterEditor_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (this.FilterID == Guid.Empty)
                {
                    // start page off with one empty condition if no filter was loaded
                    AddConditionToPlaceHolder(null);
                }
            }
        }

        void FilterEditor_PreRender(object sender, EventArgs e)
        {
            int count = this.phConditions.Controls.Count;
            for (int i = 0; i < count; i++)
            {
                (phConditions.Controls[i] as FilterConditionControl).IsFirstCondition = (i == 0);
                (phConditions.Controls[i] as FilterConditionControl).DeleteEnabled = (count > 1);
                (phConditions.Controls[i] as FilterConditionControl).CompanyID = CompanyID;
            }
        }

        /// <summary>
        /// Sets the companyid.
        /// </summary>
        public Guid CompanyID
        {
            get
            {
                return companyID;
            }
            set
            {
                companyID = value;
            }
        }

        /// <summary>
        /// Loads the specified Filter into the control for editing by the user.
        /// </summary>
        /// <param name="filter">Filter to be loaded.</param>
        public void LoadFilter(Filter filter)
        {
            FilterCondition[] conditions = FilterCondition.GetSortedArray("FilterID = ?", "OrderIndex ASC", filter.ID);
            foreach (FilterCondition condition in conditions)
            {
                AddConditionToPlaceHolder(condition);
            }

            this.FilterID = filter.ID;
        }

        /// <summary>
        /// Saves the filter defined within this control and returns the ID.
        /// </summary>
        /// <param name="trans">Transaction under which this operation will performed.</param>
        /// <returns>ID of the filter that was saved.</returns>
        public Guid SaveFilter(Transaction trans)
        {
            // create and save the root filter if one doesn't already exist
            if (this.FilterID == Guid.Empty)
            {
                Filter filter = new Filter(Guid.NewGuid());
                filter.Save(trans);

                this.FilterID = filter.ID;
            }

            // build an array of the conditions
            FilterCondition[] conditions = new FilterCondition[this.phConditions.Controls.Count];
            for (int i = 0; i < conditions.Length; i++)
            {
                FilterCondition condition = (phConditions.Controls[i] as FilterConditionControl).GetCondition();
                condition.FilterID = this.FilterID;
                condition.OrderIndex = i + 1;
                conditions[i] = condition;
            }

            // refresh the condition list by deleteing the old and adding the new
            FilterCondition[] oldConditions = FilterCondition.GetArray("FilterID = ?", this.FilterID);
            foreach (FilterCondition condition in oldConditions)
            {
                condition.Delete(trans);
            }
            foreach (FilterCondition condition in conditions)
            {
                condition.Save(trans);
            }

            return this.FilterID;
        }

        private Guid FilterID
        {
            get
            {
                object value = ViewState["FilterID"];
                return (value != null) ? (Guid)value : Guid.Empty;
            }
            set
            {
                ViewState["FilterID"] = value;
            }
        }

        void AddConditionToPlaceHolder(FilterCondition condition)
        {
            FilterConditionControl control = (FilterConditionControl)Page.LoadControl("~/Controls/FilterCondition.ascx");
            control.DeleteRequested += new EventHandler(Condition_DeleteRequested);
            control.ID = "ucCondition" + phConditions.Controls.Count;

            // we must add the control to the place holder so TrackViewState is called before we initalize/load
            phConditions.Controls.Add(control);
            control.CompanyID = companyID;

            if (condition != null)
            {
                control.LoadCondition(condition);
            }
            else // no condition
            {
                control.InitalizeFields();
            }
        }

        private void phConditions_ControlRestored(object sender, DynamicControlEventArgs e)
        {
            if (e.DynamicControl is FilterConditionControl)
            {
                (e.DynamicControl as FilterConditionControl).DeleteRequested += new EventHandler(Condition_DeleteRequested);
            }
        }

        void btnAdd_Click(object sender, EventArgs e)
        {
            AddConditionToPlaceHolder(null);
        }

        private void Condition_DeleteRequested(object sender, EventArgs e)
        {
            if (phConditions.Controls.Count <= 1)
            {
                throw new InvalidOperationException("Condition cannot be deleted.  At least one must always be defined.");
            }

            FilterConditionControl targetControl = (FilterConditionControl)sender;
            phConditions.Controls.Remove(targetControl);
        }

    }
}
