<%@ Control Language="C#" AutoEventWireup="false" CodeFile="Counter.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Controls.Counter" %>

<asp:TextBox ID="txt" Runat="server" CssClass="txt" Columns="5" MaxLength="5" />
<asp:RequiredFieldValidator ID="val" Runat="server" CssClass="val" ControlToValidate="txt" Display="Dynamic" />
<input type="button" value="<" id="btnDown" runat="server" class="counter" />
<input type="button" value=">" id="btnUp" runat="server" class="counter" />

 <script language="javascript">
function Incriment(id, incrimentBy)
{
    var txt = document.getElementById(id);
    var floatValue = parseFloat(txt.value);
    
    if( txt.value.length > 0 && !isNaN(floatValue) ) {
		txt.value = floatValue + incrimentBy;
	}
	else {
	    txt.value = 0;
	}  
}
</script>
