<%@ Control Language="C#" AutoEventWireup="false" CodeFile="DateRange.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Controls.DateRange" %>
<%@ Register TagPrefix="cc" Namespace="QCI.Web.Controls" %>
<% if (base.TableRow) {%><tr><% } %>
<td class="label" nowrap>
	<asp:Label ID="lbl" Runat="server" /><%= (this.IsRequired) ? " *" : "" %>
</td>
<td nowrap>
	<cc:DatePicker id="dpStart" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" />
	<asp:RequiredFieldValidator ID="valStart" Runat="server" CssClass="val" ControlToValidate="dpStart" Display="Dynamic" />
	<asp:CompareValidator ID="valStartType" Runat="server" CssClass="val" ControlToValidate="dpStart" Display="Dynamic" Type="Date" Operator="DataTypeCheck" />
	<asp:CompareValidator ID="valMin" Runat="server" CssClass="val" ControlToValidate="dpStart" Display="Dynamic" Type="Date" Operator="LessThan" />
	to&nbsp;
	<cc:DatePicker id="dpEnd" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" />
	<asp:RequiredFieldValidator ID="valEnd" Runat="server" CssClass="val" ControlToValidate="dpEnd" Display="Dynamic" />
	<asp:CompareValidator ID="valEndType" Runat="server" CssClass="val" ControlToValidate="dpEnd" Display="Dynamic" Type="Date" Operator="DataTypeCheck" />
	<asp:CompareValidator ID="valMax" Runat="server" CssClass="val" ControlToValidate="dpEnd" Display="Dynamic" Type="Date" Operator="GreaterThan" />
	<asp:comparevalidator ID="valBetween" Runat="server" CssClass="val" ControlToValidate="dpStart" ControlToCompare="dpEnd" Display="Dynamic" Type="Date" Operator="LessThanEqual" />
</td>
<% if (base.TableRow) {%></tr><% } %>