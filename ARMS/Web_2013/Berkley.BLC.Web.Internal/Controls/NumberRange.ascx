<%@ Control Language="C#" AutoEventWireup="false" CodeFile="NumberRange.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Controls.NumberRange" %>
<%@ Register TagPrefix="cc" Namespace="QCI.Web.Controls" %>
<% if (base.TableRow) {%><tr><% } %>
<td class="label" nowrap>
	<asp:Label ID="lbl" Runat="server" /><%= (this.IsRequired) ? " *" : "" %>
</td>
<td nowrap>
	<asp:TextBox ID="txtStart" Runat="server" CssClass="txt" Columns="10" MaxLength="10" />
	<asp:RequiredFieldValidator ID="valStart" Runat="server" CssClass="val" ControlToValidate="txtStart" Display="Dynamic" />
	<asp:CompareValidator ID="valStartType" Runat="server" CssClass="val" ControlToValidate="txtStart" Display="Dynamic" Type="Double" Operator="DataTypeCheck" />
	<asp:CompareValidator ID="valMin" Runat="server" CssClass="val" ControlToValidate="txtStart" Display="Dynamic" Type="Double" Operator="LessThan" />
	to&nbsp;
	<asp:TextBox ID="txtEnd" Runat="server" CssClass="txt" Columns="10" MaxLength="10" />
	<asp:RequiredFieldValidator ID="valEnd" Runat="server" CssClass="val" ControlToValidate="txtEnd" Display="Dynamic" />
	<asp:CompareValidator ID="valEndType" Runat="server" CssClass="val" ControlToValidate="txtEnd" Display="Dynamic" Type="Double" Operator="DataTypeCheck" />
	<asp:CompareValidator ID="valMax" Runat="server" CssClass="val" ControlToValidate="txtEnd" Display="Dynamic" Type="Double" Operator="GreaterThan" />
	<asp:comparevalidator ID="valBetween" Runat="server" CssClass="val" ControlToValidate="txtStart" ControlToCompare="txtEnd" Display="Dynamic" Type="Double" Operator="LessThanEqual" />
</td>
<% if (base.TableRow) {%></tr><% } %>