using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Berkley.BLC.Web.Internal.Controls
{
    public partial class Counter : System.Web.UI.UserControl
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(Counter_Load);
            this.PreRender += new EventHandler(Counter_PreRender);
        }
        #endregion

        protected string _appPath; // used during HTML rendering

        void Counter_Load(object sender, EventArgs e)
        {
            _appPath = ARMSUtility.AppPath;
        }

        void Counter_PreRender(object sender, EventArgs e)
        {
            string strTxtID = this.ID + "_txt";
            btnUp.Attributes.Add("onclick", "Incriment('" + strTxtID + "', 1)");
            btnDown.Attributes.Add("onclick", "Incriment('" + strTxtID + "', -1)");

            bool enabled;

            enabled = this.IsRequired;
            val.Visible = enabled;
            if (enabled)
            {
                val.Text = "*";
                val.ErrorMessage = string.Format("{0} is required.", this.Name);
            }
        }

        /// <summary>
        /// Gets or sets the text content of the control.
        /// </summary>
        public string Text
        {
            get { return this.txt.Text; }
            set
            {
                if (value != null)
                {
                    value = value.Trim();
                }
                txt.Text = value;
            }
        }


        /// <summary>
        /// Gets or sets the maximum number of characters allowed in the text box.
        /// </summary>
        public int MaxLength
        {
            get { return txt.MaxLength; }
            set { txt.MaxLength = value; }
        }


        /// <summary>
        /// Gets or sets the display width of the text box in characters.
        /// </summary>
        public int Columns
        {
            get { return txt.Columns; }
            set { txt.Columns = value; }
        }


        /// <summary>
        /// Gets or sets a value indicating if this field is required.
        /// </summary>
        public bool IsRequired
        {
            get
            {
                object value = this.ViewState["IsRequired"];
                return (value != null) ? (bool)value : false;
            }
            set
            {
                this.ViewState["IsRequired"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of this control, which is suitable to present to the user.
        /// </summary>
        public string Name
        {
            get
            {
                object value = this.ViewState["Name"];
                return (value != null) ? (string)value : null;
            }
            set
            {
                this.ViewState["Name"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the the control is enabled.
        /// </summary>
        public bool Enabled
        {
            get { return txt.Enabled; }
            set { txt.Enabled = value; }
        }
    }
}