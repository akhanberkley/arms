using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using System.Web.Caching;
using Berkley.BLC.Entities;
using QCI.Web;
using QCI.Web.Validation;
using Wilson.ORMapper;
using System.Linq;

namespace Berkley.BLC.Web.Internal.Controls
{
    public partial class FilterConditionControl : System.Web.UI.UserControl
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(FilterCondition_Load);
            this.PreRender += new EventHandler(FilterCondition_PreRender);
            this.btnDelete.Click += new EventHandler(btnDelete_Click);
            this.cboField.SelectedIndexChanged +=new EventHandler(cboField_SelectedIndexChanged);
        }
        #endregion
        
        private const string NULL_VALUE = "##null##";

		private EventHandler _deleteRequested;

        private FilterField[] _fields;
        private FilterOperator[] _operators;

        public FilterConditionControl()
		{
			string typeCode = ARMSUtility.GetStringFromQueryString("type", true);
            SurveyStatusType type = SurveyStatusType.Get(typeCode);

            // initalize our member variables
			_fields = (FilterField[])HttpContext.Current.Cache["FilterFields"];
			if( _fields == null )
			{
                if (type == SurveyStatusType.Survey)
                {
                    _fields = FilterField.GetSortedArray("SurveyStatusTypeCode = ?", "DisplayOrder ASC", SurveyStatusType.Survey.Code);
                }
                else
                {
                    _fields = FilterField.GetSortedArray("", "DisplayOrder ASC");
                }
				//HttpContext.Current.Cache.Insert("FilterFields", _fields, null, DateTime.Now.AddMinutes(30), TimeSpan.Zero);
			}

            _operators = (FilterOperator[])HttpContext.Current.Cache["FilterOperators"];
			if( _operators == null )
			{
				_operators = FilterOperator.GetSortedArray("", "DisplayOrder ASC");
				HttpContext.Current.Cache.Insert("FilterOperators", _operators, null, DateTime.Now.AddMinutes(30), TimeSpan.Zero);
			}
		}

        void  FilterCondition_PreRender(object sender, EventArgs e)
        {
        }

        void  FilterCondition_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Gets or sets a value indicating if this is the first condition in the list. 
        /// </summary>
        public bool IsFirstCondition
        {
            get
            {
                object value = this.ViewState["IsFirstCondition"];
                return (value != null) ? (bool)value : false;
            }
            set
            {
                this.ViewState["IsFirstCondition"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating if the condition can be deleted by the user.
        /// </summary>
        public bool DeleteEnabled
        {
            get
            {
                object value = this.ViewState["DeleteEnabled"];
                return (value != null) ? (bool)value : false;
            }
            set
            {
                this.ViewState["DeleteEnabled"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the company id.
        /// </summary>
        public Guid CompanyID
        {
            get
            {
                object value = this.ViewState["CompanyID"];
                return (value != null) ? (Guid)value : Guid.Empty;
            }
            set
            {
                this.ViewState["CompanyID"] = value;
            }
        }


        /// <summary>
        /// Occurs when the delete link for this condition is clicked by the user.
        /// </summary>
        public event EventHandler DeleteRequested
        {
            add { _deleteRequested += value; }
            remove { _deleteRequested -= value; }
        }


        void cboField_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get the selected field
            string fieldCode = cboField.SelectedValue;
            FilterField field = (fieldCode.Length > 0) ? FastGetField(fieldCode) : null;

            // update the operator and value controls
            RefreshOperatorField(field);
            InitValueField(field);
        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            if (_deleteRequested != null)
            {
                _deleteRequested(this, e);
            }
        }


        public void InitalizeFields()
        {
            if (cboField.Items.Count == 0)
            {
                // load the field list
                cboField.Items.Clear();
                ComboHelper.BindCombo(cboField, _fields, "Code", "Name");
                cboField.Items.Insert(0, new ListItem("-- select field --", ""));
                cboField_SelectedIndexChanged(null, null);

                valField.ErrorMessage = "A field must be selected for one of the conditions.";
            }
        }


        public void LoadCondition(FilterCondition condition)
        {
            InitalizeFields();

            cboField.SelectedValue = condition.FilterFieldCode;
            cboField_SelectedIndexChanged(null, null);

            cboOperator.SelectedValue = condition.FilterOperatorCode;

            if (txtValue.Visible)
            {
                txtValue.Text = condition.CompareValue;
            }
            else
            {
                string value = condition.CompareValue;
                if (value == null || value.Length == 0)
                {
                    value = NULL_VALUE;
                }
                cboValue.SelectedValue = value;
            }
        }

        public FilterCondition GetCondition()
        {
            string fieldCode = cboField.SelectedValue;
            FilterField field = FastGetField(fieldCode);

            FilterCondition condition = new FilterCondition(Guid.NewGuid());
            condition.FilterFieldCode = field.Code;
            condition.FilterOperatorCode = cboOperator.SelectedValue;
            if (txtValue.Visible)
            {
                string text = txtValue.Text.Trim();
                string value = txtValue.Text.Trim();

                // convert the value string to the correct type and get the ToString() value
                if (value.Length > 0)
                {
                    object realValue;
                    try
                    {
                        realValue = Convert.ChangeType(value, field.SystemTypeCode);
                    }
                    catch
                    {
                        throw new FieldValidationException(valTextValueType.ErrorMessage);
                    }
                    value = realValue.ToString();
                }

                condition.CompareValue = value;
                condition.DisplayValue = (text.Length > 0) ? text : "<blank>";
            }
            else
            {
                ListItem item = cboValue.SelectedItem;
                string value = (item.Value != NULL_VALUE) ? item.Value : string.Empty;

                condition.CompareValue = value;
                condition.DisplayValue = (value.Length > 0) ? item.Text : "<blank>";
            }

            return condition;
        }


        private void RefreshOperatorField(FilterField field)
        {
            string oldValue = cboOperator.SelectedValue;

            // reset and hide the combo
            cboOperator.Items.Clear();
            cboOperator.Visible = false;

            // we done if no filter was passed
            if (field == null)
            {
                return;
            }

            cboOperator.Visible = true;

            // only Equals and Not Equals can be used for combo fields
            ArrayList items = new ArrayList();
            if (field.ComboEntity != null && field.ComboEntity.Length > 0)
            {
                items.Add(FilterOperator.Equal);
                items.Add(FilterOperator.NotEqual);
            }
            else // free-text value field
            {
                // add all operators to the list
                items.AddRange(_operators);

                // remove string-only operators if field is not of type string
                if (field.DotNetDataType != "String")
                {
                    items.Remove(FilterOperator.StartsWith);
                    items.Remove(FilterOperator.EndsWith);
                    items.Remove(FilterOperator.Contains);
                }

                //remove int-only operators if field is not of type int
                if (field.DotNetDataType != "Int32")
                {
                    items.Remove(FilterOperator.IsDivisibleBy);
                    items.Remove(FilterOperator.NotDivisibleBy);
                }
            }

            // populate our combo and try to restore the selection
            if (items.Count > 0)
            {
                ComboHelper.BindCombo(cboOperator, items, "Code", "DisplayName");
                ComboHelper.SelectComboItem(cboOperator, oldValue);
            }
        }

        private void InitValueField(FilterField field)
        {
            // reset the value fields
            txtValue.Text = null;
            cboValue.Items.Clear();

            // hide all the controls (we'll turn the right ones on below)
            txtValue.Visible = false;
            cboValue.Visible = false;
            valTextValue.Visible = false;
            valTextValueType.Visible = false;
            valComboValue.Visible = false;

            // we done if no filter was passed
            if (field == null)
            {
                return;
            }

            // show the text or combo control depending on this type of field
            if (field.ComboEntity == null || field.ComboEntity.Length == 0) // free-text entry
            {
                txtValue.Visible = true;
                if (!field.NullAllowed)
                {
                    valTextValue.Visible = true;
                    valTextValue.ErrorMessage = string.Format("A comparision value must be entered for {0} condition.", field.Name);
                }

                // determine the validation data type
                ValidationDataType dataType;
                switch (field.SystemTypeCode)
                {
                    case TypeCode.DateTime: dataType = ValidationDataType.Date; break;
                    case TypeCode.Single:
                    case TypeCode.Double:
                    case TypeCode.Decimal: dataType = ValidationDataType.Double; break;
                    case TypeCode.Byte:
                    case TypeCode.SByte:
                    case TypeCode.Int16:
                    case TypeCode.Int32:
                    case TypeCode.Int64:
                    case TypeCode.UInt16:
                    case TypeCode.UInt32:
                    case TypeCode.UInt64: dataType = ValidationDataType.Integer; break;
                    default:
                        {
                            dataType = ValidationDataType.String;
                            break;
                        }
                }
                valTextValueType.Type = dataType;
                valTextValueType.Visible = true;
                valTextValueType.ErrorMessage = string.Format("The value entered for {0} condition is not valid.", field.Name);
            }
            else // limited values
            {
                cboValue.Visible = true;

                string filter = field.FilterExpression.Replace("#Company.ID#", CompanyID.ToString());

                // build the query that will return the correct set of values
                Type entityType = UIMapper.Instance.GetEntityType(field.ComboEntity);
                OPathQuery query = new OPathQuery(entityType, filter, field.SortExpression);

                // execute the query and load the combo with results
                ObjectSet items = DB.Engine.GetObjectSet(query);

                //Added By Vilas Meshram: 03/19/2013 :Squish# 21367: Grouping of SIC Codes in 2 digits
                if (field.ComboEntity == "SIC" && field.FlexibleDisplayWidth > 0 )
                {
                    List<SIC> results = new List<SIC>();
                    for (int i = 0; i < items.Count; i++)
                    {
                        string sCode = ((SIC)(items[i])).Code.Substring(0, field.FlexibleDisplayWidth);
                        string sDescription = ((SIC)(items[i])).Description;

                        results.Add(new SIC(sCode, sDescription));
                    }
                    //results.Distinct(); 
                    SIC[] groupResultItems = results.Distinct().ToArray();
                    ComboHelper.BindCombo(cboValue, groupResultItems, field.ValueField, field.TextField);
                }
                //Added By Vilas Meshram: 03/19/2013 :Squish# 21367: Grouping of SIC Codes in 2 digits
                else
                {
                    ComboHelper.BindCombo(cboValue, items, field.ValueField, field.TextField);
                }

                // add a "null" entry if null is allowed
                if (field.NullAllowed)
                {
                    cboValue.Items.Insert(0, new ListItem("<blank>", NULL_VALUE));
                }

                // add the "choose" entry to the top of the list
                cboValue.Items.Insert(0, new ListItem("-- select value --", ""));

                valComboValue.Visible = true;
                valTextValue.ErrorMessage = string.Format("A comparision value must be selected for {0} condition.", field.Name);
            }
        }


        private FilterField FastGetField(string code)
        {
            for (int i = 0; i < _fields.Length; i++)
            {
                if (_fields[i].Code == code)
                {
                    return _fields[i];
                }
            }
            throw new ArgumentException("Could not find a field with code '" + code + "'.");
        }
    }
}
