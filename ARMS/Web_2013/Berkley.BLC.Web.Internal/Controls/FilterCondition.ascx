<%@ Control Language="C#" AutoEventWireup="false" CodeFile="FilterCondition.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Controls.FilterConditionControl" %>
<tr>
	<td>
		<%= (this.IsFirstCondition) ? "If" : "and" %>
	</td>
	<td>
		<asp:DropDownList id="cboField" Runat="Server" CssClass="cbo" AutoPostBack="True" />
		<asp:RequiredFieldValidator ID="valField" Runat="server" CssClass="val" ControlToValidate="cboField" Display="Dynamic" Text="*" />
	</td>
	<td>
		<asp:DropDownList id="cboOperator" CssClass="cbo" runat="Server" />
	</td>
	<td>
		<asp:TextBox id="txtValue" Runat="Server" CssClass="txt" />
		<asp:RequiredFieldValidator ID="valTextValue" Runat="server" CssClass="val" ControlToValidate="txtValue" Display="Dynamic" Text="*" />
		<asp:CompareValidator ID="valTextValueType" Runat="server" CssClass="val" ControlToValidate="txtValue" Operator="DataTypeCheck" Display="Dynamic" Text="*" /> 
		<asp:DropDownList id="cboValue" CssClass="cbo" Runat="Server" Visible="false" />
		<asp:RequiredFieldValidator ID="valComboValue" Runat="server" CssClass="val" ControlToValidate="cboValue" Display="Dynamic" Text="*" />
	</td>
	<td style="PADDING-LEFT: 5px" valign="center">
		<% if( this.DeleteEnabled ) { %>
		<asp:LinkButton ID="btnDelete" Runat="server" CausesValidation="false">Remove</asp:LinkButton>
		<% } %>
	</td>
</tr>