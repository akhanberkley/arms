using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web;
using Wilson.ORMapper;

public partial class AdminResultsDisplayAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }
    /// <summary>
    /// Toggles between internal and external mode
    /// added 20121127 Dustin Thostenson
    /// </summary>
    protected bool isExternal
    {
        get { return cboFilter.SelectedValue.Contains("E"); ; }
    }
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminResultsDisplayAspx_Load);
        this.PreRender += new EventHandler(AdminResultsDisplayAspx_PreRender);
        this.grdCols.ItemCreated += new DataGridItemEventHandler(grdCols_ItemCreated);
        this.grdPlanCols.ItemCreated += new DataGridItemEventHandler(grdPlanCols_ItemCreated);
        this.grdCols.ItemCommand += new DataGridCommandEventHandler(grdCols_ItemCommand);
        this.grdColsNotIncluded.ItemCommand += new DataGridCommandEventHandler(grdColsNotIncluded_ItemCommand);
        this.grdPlanCols.ItemCommand += new DataGridCommandEventHandler(grdPlanCols_ItemCommand);
        this.grdPlanColsNotIncluded.ItemCommand += new DataGridCommandEventHandler(grdPlanColsNotIncluded_ItemCommand);
    }
    #endregion

    void AdminResultsDisplayAspx_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // populate the filter list
            cboFilter.Items.Add(new ListItem("Survey Grid Columns", "S"));
            cboFilter.Items.Add(new ListItem("Service Plan Grid Columns", "P"));
            //20121031 Dustin: testing out adding new colums here just to see what it will look like.
            //TODO: identify the table this gets saved to. I'm assuming the field is a single col, so we need a decent code.
            cboFilter.Items.Add(new ListItem("External Survey Grid Columns", "ES"));
            //cboFilter.Items.Add(new ListItem("External Service Plan Grid Columns", "EP"));
        }
    }

    void AdminResultsDisplayAspx_PreRender(object sender, EventArgs e)
    {
        if (cboFilter.SelectedValue.Contains("S"))
        {
            GridColumnFilterCompany[] colsDisplayed = GridColumnFilterCompany.GetSortedArray("CompanyID = ? && Display = ?  && IsExternal = ?", "DisplayOrder ASC", this.UserIdentity.CompanyID, true, isExternal);
            DataGridHelper.BindGrid(grdCols, colsDisplayed, "GridColumnFilterID", "There are no columns defined for the results to display.");

            GridColumnFilterCompany[] colsNotDisplayed = GridColumnFilterCompany.GetSortedArray("CompanyID = ? && Display = ? && IsExternal = ?", "DisplayOrder ASC", this.UserIdentity.CompanyID, false, isExternal);
            DataGridHelper.BindGrid(grdColsNotIncluded, colsNotDisplayed, "GridColumnFilterID", "All columns are currently displayed.");
        }
        else
        {
            ServicePlanGridColumnFilterCompany[] colsDisplayed = ServicePlanGridColumnFilterCompany.GetSortedArray("CompanyID = ? && Display = ?", "DisplayOrder ASC", this.UserIdentity.CompanyID, true);
            DataGridHelper.BindGrid(grdPlanCols, colsDisplayed, "ServicePlanGridColumnFilterID", "There are no columns defined for the results to display.");

            ServicePlanGridColumnFilterCompany[] colsNotDisplayed = ServicePlanGridColumnFilterCompany.GetSortedArray("CompanyID = ? && Display = ?", "DisplayOrder ASC", this.UserIdentity.CompanyID, false);
            DataGridHelper.BindGrid(grdPlanColsNotIncluded, colsNotDisplayed, "ServicePlanGridColumnFilterID", "All columns are currently displayed.");
        }
    }

    void grdCols_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            GridColumnFilterCompany col = (GridColumnFilterCompany)e.Item.DataItem;

            //Do not allow the removal of required columns
            if (col != null && col.Required)
            {
                LinkButton btn = e.Item.Controls[3].FindControl("btnDelete") as LinkButton;
                btn.Enabled = false;
            }
        }
    }

    void grdPlanCols_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ServicePlanGridColumnFilterCompany col = (ServicePlanGridColumnFilterCompany)e.Item.DataItem;

            //Do not allow the removal of required columns
            if (col != null && col.Required)
            {
                LinkButton btn = e.Item.Controls[3].FindControl("btnDelete") as LinkButton;
                btn.Enabled = false;
            }
        }
    }

    void grdCols_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid colID = (Guid)grdCols.DataKeys[e.Item.ItemIndex];

        // get the current columns from the DB and find the index of the target columns
        GridColumnFilterCompany[] cols = GridColumnFilterCompany.GetSortedArray("CompanyID = ? && Display = ? && IsExternal = ?", "DisplayOrder ASC", this.UserIdentity.CompanyID, true, isExternal);
        int targetIndex = int.MinValue;
        for (int i = 0; i < cols.Length; i++)
        {
            if (cols[i].GridColumnFilterID == colID && cols[i].CompanyID == CurrentUser.CompanyID)
            {
                targetIndex = i;
                break;
            }
        }
        if (targetIndex == int.MinValue) // not found
        {
            return;
        }

        GridColumnFilterCompany targetCol = cols[targetIndex];

        // execute the requested transformation on the rules
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            ArrayList colList = new ArrayList(cols);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEUP":
                    {
                        colList.RemoveAt(targetIndex);
                        if (targetIndex > 0)
                        {
                            colList.Insert(targetIndex - 1, targetCol);
                        }
                        else // top item (move to bottom)
                        {
                            colList.Add(targetCol);
                        }
                        break;
                    }
                case "MOVEDOWN":
                    {
                        colList.RemoveAt(targetIndex);
                        if (targetIndex < cols.Length - 1)
                        {
                            colList.Insert(targetIndex + 1, targetCol);
                        }
                        else // bottom item (move to top)
                        {
                            colList.Insert(0, targetCol);
                        }
                        break;
                    }
                case "REMOVE":
                    {
                        colList.RemoveAt(targetIndex);
                        GridColumnFilterCompany col = GridColumnFilterCompany.Get(colID, CurrentUser.CompanyID, isExternal).GetWritableInstance();
                        col.IsExternal = isExternal;
                        col.Display = false;
                        col.DisplayOrder = 0;
                        col.Save(trans);
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the rules still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < colList.Count; i++)
            {
                GridColumnFilterCompany col = (colList[i] as GridColumnFilterCompany).GetWritableInstance();
                col.DisplayOrder = (i + 1);
                col.Save(trans);
            }

            trans.Commit();
        }
    }

    void grdColsNotIncluded_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid colID = (Guid)grdColsNotIncluded.DataKeys[e.Item.ItemIndex];

        // get the current columns from the DB and find the index of the target columns
        GridColumnFilterCompany[] cols = GridColumnFilterCompany.GetSortedArray("CompanyID = ? && Display = false && IsExternal = ?", "DisplayOrder ASC", this.UserIdentity.CompanyID, isExternal);
        int targetIndex = int.MinValue;
        for (int i = 0; i < cols.Length; i++)
        {
            if (cols[i].GridColumnFilterID == colID && cols[i].CompanyID == CurrentUser.CompanyID)
            {
                targetIndex = i;
                break;
            }
        }
        if (targetIndex == int.MinValue) // not found
        {
            return;
        }

        GridColumnFilterCompany targetCol = cols[targetIndex];

        // execute the requested transformation on the rules
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            ArrayList colList = new ArrayList(cols);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEAVAILABLEUP":
                    {
                        colList.RemoveAt(targetIndex);
                        if (targetIndex > 0)
                        {
                            colList.Insert(targetIndex - 1, targetCol);
                        }
                        else // top item (move to bottom)
                        {
                            colList.Add(targetCol);
                        }
                        break;
                    }
                case "MOVEAVAILABLEDOWN":
                    {
                        colList.RemoveAt(targetIndex);
                        if (targetIndex < cols.Length - 1)
                        {
                            colList.Insert(targetIndex + 1, targetCol);
                        }
                        else // bottom item (move to top)
                        {
                            colList.Insert(0, targetCol);
                        }
                        break;
                    }
                case "ADD":
                    {
                        colList.RemoveAt(targetIndex);
                        GridColumnFilterCompany col = GridColumnFilterCompany.Get(colID, CurrentUser.CompanyID, isExternal).GetWritableInstance();
                        col.IsExternal = isExternal;
                        col.Display = true;
                        col.DisplayOrder = grdCols.DataKeys.Count + 1;
                        col.Save(trans);
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the rules still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < colList.Count; i++)
            {
                GridColumnFilterCompany col = (colList[i] as GridColumnFilterCompany).GetWritableInstance();
                col.DisplayOrder = (i + 1);
                col.Save(trans);
            }

            trans.Commit();
        }
    }

    void grdPlanCols_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid colID = (Guid)grdPlanCols.DataKeys[e.Item.ItemIndex];

        // get the current columns from the DB and find the index of the target columns
        ServicePlanGridColumnFilterCompany[] cols = ServicePlanGridColumnFilterCompany.GetSortedArray("CompanyID = ? && Display = ?", "DisplayOrder ASC", this.UserIdentity.CompanyID, true);
        int targetIndex = int.MinValue;
        for (int i = 0; i < cols.Length; i++)
        {
            if (cols[i].ServicePlanGridColumnFilterID == colID && cols[i].CompanyID == CurrentUser.CompanyID)
            {
                targetIndex = i;
                break;
            }
        }
        if (targetIndex == int.MinValue) // not found
        {
            return;
        }

        ServicePlanGridColumnFilterCompany targetCol = cols[targetIndex];

        // execute the requested transformation on the rules
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            ArrayList colList = new ArrayList(cols);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEUP":
                    {
                        colList.RemoveAt(targetIndex);
                        if (targetIndex > 0)
                        {
                            colList.Insert(targetIndex - 1, targetCol);
                        }
                        else // top item (move to bottom)
                        {
                            colList.Add(targetCol);
                        }
                        break;
                    }
                case "MOVEDOWN":
                    {
                        colList.RemoveAt(targetIndex);
                        if (targetIndex < cols.Length - 1)
                        {
                            colList.Insert(targetIndex + 1, targetCol);
                        }
                        else // bottom item (move to top)
                        {
                            colList.Insert(0, targetCol);
                        }
                        break;
                    }
                case "REMOVE":
                    {
                        colList.RemoveAt(targetIndex);

                        ServicePlanGridColumnFilterCompany col = ServicePlanGridColumnFilterCompany.Get(colID, CurrentUser.CompanyID).GetWritableInstance();
                        col.Display = false;
                        col.DisplayOrder = 0;
                        col.Save(trans);
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the rules still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < colList.Count; i++)
            {
                ServicePlanGridColumnFilterCompany col = (colList[i] as ServicePlanGridColumnFilterCompany).GetWritableInstance();
                col.DisplayOrder = (i + 1);
                col.Save(trans);
            }

            trans.Commit();
        }
    }

    void grdPlanColsNotIncluded_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid colID = (Guid)grdPlanColsNotIncluded.DataKeys[e.Item.ItemIndex];

        // get the current columns from the DB and find the index of the target columns
        ServicePlanGridColumnFilterCompany[] cols = ServicePlanGridColumnFilterCompany.GetSortedArray("CompanyID = ? && Display = false", "DisplayOrder ASC", this.UserIdentity.CompanyID);
        int targetIndex = int.MinValue;
        for (int i = 0; i < cols.Length; i++)
        {
            if (cols[i].ServicePlanGridColumnFilterID == colID && cols[i].CompanyID == CurrentUser.CompanyID)
            {
                targetIndex = i;
                break;
            }
        }
        if (targetIndex == int.MinValue) // not found
        {
            return;
        }

        ServicePlanGridColumnFilterCompany targetCol = cols[targetIndex];

        // execute the requested transformation on the rules
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            ArrayList colList = new ArrayList(cols);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEAVAILABLEUP":
                    {
                        colList.RemoveAt(targetIndex);
                        if (targetIndex > 0)
                        {
                            colList.Insert(targetIndex - 1, targetCol);
                        }
                        else // top item (move to bottom)
                        {
                            colList.Add(targetCol);
                        }
                        break;
                    }
                case "MOVEAVAILABLEDOWN":
                    {
                        colList.RemoveAt(targetIndex);
                        if (targetIndex < cols.Length - 1)
                        {
                            colList.Insert(targetIndex + 1, targetCol);
                        }
                        else // bottom item (move to top)
                        {
                            colList.Insert(0, targetCol);
                        }
                        break;
                    }
                case "ADD":
                    {
                        colList.RemoveAt(targetIndex);

                        ServicePlanGridColumnFilterCompany col = ServicePlanGridColumnFilterCompany.Get(colID, CurrentUser.CompanyID).GetWritableInstance();
                        col.Display = true;
                        col.DisplayOrder = grdPlanCols.DataKeys.Count + 1;
                        col.Save(trans);
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the rules still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < colList.Count; i++)
            {
                ServicePlanGridColumnFilterCompany col = (colList[i] as ServicePlanGridColumnFilterCompany).GetWritableInstance();
                col.DisplayOrder = (i + 1);
                col.Save(trans);
            }

            trans.Commit();
        }
    }
}
