using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

using Berkley.BLC.Entities;
using Berkley.BLC.Web.Internal.Controls;
using QCI.Web.Validation;
using QCI.Web;
using Wilson.ORMapper;
using BCS = Berkley.BLC.Import.Prospect;

public partial class AdminClientsAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminClientsAspx_Load);
        this.btnSearch.Click += new EventHandler(btnSearch_Click);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.lnkEditAttributes.Click += new EventHandler(lnkEditAttributes_Click);
        this.repEditAttributes.ItemDataBound += new RepeaterItemEventHandler(repEditAttributes_ItemDataBound);
    }
    #endregion

    protected Insured _insured = null;

    void AdminClientsAspx_Load(object sender, EventArgs e)
    {
        boxHistory.Visible = false;

        //this is a test.
    }

    void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (cboSearchType.SelectedValue == "C")
            {
                //validate
                trClientID.Attributes.Clear();
                trClientID.Attributes.Add("Style", "display:inline;");
                trPolicyNumber.Attributes.Clear();
                trPolicyNumber.Attributes.Add("Style", "display:none;");
                if (txtClientID.Text.Length <= 0)
                {
                    throw new FieldValidationException(txtClientID, "A client id must be entered.");
                }

                Survey[] surveys = Survey.GetSortedArray("Insured.ClientID = ? && CompanyID = ? && CreateDate > ?", "CreateDate DESC", txtClientID.Text, CurrentUser.CompanyID, DateTime.Now.AddYears(-10));
                if (surveys.Length > 0)
                {
                    _insured = surveys[0].Insured;
                }
            }
            else if (cboSearchType.SelectedValue == "P")
            {
                //validate
                trPolicyNumber.Attributes.Clear();
                trPolicyNumber.Attributes.Add("Style", "display:inline;");
                trClientID.Attributes.Clear();
                trClientID.Attributes.Add("Style", "display:none;");
                if (txtPolicyNumber.Text.Length <= 0)
                {
                    throw new FieldValidationException(txtPolicyNumber, "A policy number must be entered.");
                }

                Survey[] surveys = Survey.GetSortedArray("Insured.Policies[Number = ?] && CompanyID = ? && Insured.ClientID != ? && CreateDate > ?", "CreateDate DESC", txtPolicyNumber.Text, CurrentUser.CompanyID, "REMOVE", DateTime.Now.AddYears(-10));
                if (surveys.Length > 0)
                {
                    _insured = surveys[0].Insured;
                }
            }
            else
            {
                throw new NotSupportedException("The user some how selected nothing from the dropdown.");
            }

            //check to populate
            try
            {
                if (_insured != null)
                {
                    Client[] clientValues = GetClientValues(_insured.ClientID);

                    //check if existing attributes exist for the client, if not stub them out
                    BCS.ProspectWebService service = new BCS.ProspectWebService();
                    if (clientValues.Length <= 0)
                    {
                        ClientAttribute[] clientAttributes = ClientAttribute.GetArray("CompanyID = ?", CurrentUser.CompanyID);
                        foreach (ClientAttribute clientAttribute in clientAttributes)
                        {
                            Client client = new Client(_insured.ClientID, clientAttribute.ID);

                            if (!String.IsNullOrEmpty(clientAttribute.Type.ClientCoreGroupName))
                            {
                                client.AttributeValue = service.GetClientMembershipInfo(CurrentUser.Company, client.ID, clientAttribute.Type.ClientCoreGroupName, "0");
                            }

                            client.Save();
                        }
                    }

                    this.repAttributes.DataSource = clientValues;
                    ViewState["attributes"] = AdminClientClient.GetAdminClientClients(clientValues);

                    this.repAttributes.DataBind();

                    this.boxResults.Visible = true;
                    this.boxEditResults.Visible = false;
                    this.PopulateFields("Edit", _insured);
                    this.PopulateFields("View", _insured);

                    //Display if the account is currently being serviced
                    Survey survey = Survey.GetOne("Insured.ClientID = ? && CompanyID = ? && ISNULL(SurveyedDate) && StatusCode = ?", _insured.ClientID, CurrentUser.CompanyID, SurveyStatus.AssignedSurvey.Code);
                    lblServiceAccount.Text = (survey != null) ? "Yes" : "No";
                    lblServiceAccountEdit.Text = (survey != null) ? "Yes" : "No";

                    //retain the client id as an attribute
                    lnkEditAttributes.Attributes.Add("ClientID", _insured.ClientID);

                    // get all history associated to this client
                    ClientHistory[] history = ClientHistory.GetSortedArray("ClientID = ?", "EntryDate DESC", _insured.ClientID);
                    repHistory.DataSource = history;
                    repHistory.DataBind();
                    boxHistory.Title = string.Format("History ({0})", history.Length);
                    boxHistory.Visible = true;
                }
                else
                {
                    this.boxResults.Visible = false;
                    JavaScript.ShowMessage(this, "No client was found matching the search criteria entered.");
                    return;
                }
            }
            catch (BCS.BlcProspectWebServiceException bpwse)
            {
                JavaScript.ShowMessage(this, bpwse.Message);
                QCI.ExceptionManagement.ExceptionManager.Publish(bpwse);
                return;
            }
        }
        catch (FieldValidationException ex)
        {
            this.boxResults.Visible = false;
            this.ShowErrorMessage(ex);
            return;
        }
    }

    protected string GetEncodedHistory(object dataItem)
    {
        string history = (dataItem as ClientHistory).EntryText;

        // html encode the string (must do this first)
        return Server.HtmlEncode(history);
    }

    protected string GetAttributeValue(object dataItem)
    {
        string result = "(not specified)";
        Client client = (Client)dataItem;

        if (client.AttributeValue.Length > 0)
        {
            if (client.Attribute.Type.SourceTable.Length > 0)
            {
                // build the query that will return the correct set of values
                Type entityType = UIMapper.Instance.GetEntityType(client.Attribute.Type.SourceTable);
                OPathQuery query = new OPathQuery(entityType, string.Format("ID = '{0}'", client.AttributeValue));

                // execute the query and return the column/row value
                ObjectSet items = DB.Engine.GetObjectSet(query);
                result = HtmlEncodeNullable(items[0], client.Attribute.Type.SourceColumn, string.Empty, "(not specified)");
            }
            else
            {
                switch (client.Attribute.Type.DotNetDataType)
                {
                    case "Boolean":
                        result = (client.AttributeValue == "1") ? "Yes" : "No";
                        break;
                    default:
                        result = client.AttributeValue;
                        break;
                }
            }
        }

        return result;
    }

    private Client[] GetClientValues(string clientID)
    {
        BCS.ProspectWebService service = new BCS.ProspectWebService();
        
        List<Client> results = new List<Client>();
        Client[] clientValues = Client.GetSortedArray("ID = ? && Attribute.CompanyID = ?", "Attribute.DisplayOrder ASC", clientID, CurrentUser.CompanyID);
                
        //check if any of these attributes should be returning its value from Client Core
        for (int i = 0; i < clientValues.Length; i++)
        {
            Client clientValue = clientValues[i].GetWritableInstance();

            if (!String.IsNullOrEmpty(clientValue.Attribute.Type.ClientCoreGroupName))
            {
                clientValue.AttributeValue = service.GetClientMembershipInfo(CurrentUser.Company, clientValue.ID, clientValue.Attribute.Type.ClientCoreGroupName, "0");
                
            }

            results.Add(clientValue);
        }
        
        return results.ToArray();
    }

    void lnkEditAttributes_Click(object sender, EventArgs e)
    {
        boxHistory.Visible = true;
        LinkButton lnk = (LinkButton)sender;
        Client[] attributes = GetClientValues(lnk.Attributes["ClientID"]);

        if (attributes.Length > 0)
        {
            this.boxResults.Visible = false;
            this.boxEditResults.Visible = true;

            this.repEditAttributes.DataSource = attributes;
            this.repEditAttributes.DataBind();
        }
    }

    void repEditAttributes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Client client = (Client)e.Item.DataItem;
            string clientID = client.ID + "|" + client.AttributeID.ToString();
            DynamicControlsPlaceholder pl = (DynamicControlsPlaceholder)e.Item.FindControl("pHolder");
            switch (client.Attribute.Type.DotNetDataType)
            {
                case "Boolean":
                    CheckBox chk = new CheckBox();
                    chk.ID = clientID;
                    chk.CssClass = "chk";
                    chk.Checked = (client.AttributeValue == "1");
                    pl.Controls.Add(chk);
                    break;
                case "Guid":
                    //For Guids, we assume it is a dropdown
                    DropDownList cbo = new DropDownList();
                    cbo.ID = clientID;
                    cbo.CssClass = "cbo";

                    //Add an attribute to set the hidden value onchange
                    //cbo.Attributes.Add("onchange", string.Format("document.getElementById('repEditAttributes$ctl{0}${1}').value = this.value", e.Item.ItemIndex.ToString().PadLeft(2,'0'), client.AttributeID.ToString()));
                    cbo.Attributes.Add("onchange", string.Format("document.getElementById('repEditAttributes_{0}_{1}').value = this.value", client.AttributeID.ToString(), e.Item.ItemIndex));

                    //Check if the filter is using any reservered keys
                    Guid guidValue = !String.IsNullOrEmpty(client.AttributeValue) ? Guid.Parse(client.AttributeValue) : Guid.Empty ;
                    string filter = client.Attribute.Type.FilterExpression.Replace("#CompanyID#", "'" + CurrentUser.CompanyID.ToString() + "'").Replace("#AssignedUserID#", "'" + guidValue + "'");
                
                    //populate the combo
                    Type entityType = UIMapper.Instance.GetEntityType(client.Attribute.Type.SourceTable);
                    OPathQuery query = new OPathQuery(entityType, filter, client.Attribute.Type.SortExpression);
                    ObjectSet items = DB.Engine.GetObjectSet(query);
                    ComboHelper.BindCombo(cbo, items, "ID", client.Attribute.Type.SourceColumn);
                    
                    // add the empty entry to the top of the list
                    cbo.Items.Insert(0, new ListItem("", ""));
                    cbo.SelectedValue = client.AttributeValue.ToLower();
                    pl.Controls.Add(cbo);

                    //also add a hidden field to populate with the selected value
                    HtmlInputHidden hidden = new HtmlInputHidden();
                    hidden.ID = client.AttributeID.ToString();
                    hidden.Value = client.AttributeValue;
                    pl.Controls.Add(hidden);
                    break;
                default:
                    TextBox txt = new TextBox();
                    txt.ID = clientID;
                    txt.CssClass = "txt";
                    txt.Text = client.AttributeValue;
                    pl.Controls.Add(txt);
                    break;
            }
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in repEditAttributes.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                DynamicControlsPlaceholder pHolder = (DynamicControlsPlaceholder)item.FindControl("pHolder");
                string[] keys = pHolder.Controls[0].ID.Split('|');

                //Get the control via the id
                Client client = Client.Get(keys[0], new Guid(keys[1]));
                string result = string.Empty;
                string resultForDisplay = "nothing";
                string previousResultForDisplay = "nothing";
                Client previousClient = Client.GetOne("ID = ? && AttributeID = ?", client.ID, client.AttributeID);

                switch (client.Attribute.Type.DotNetDataType)
                {
                    case "Boolean":
                        CheckBox chk = (CheckBox)pHolder.Controls[0];
                        result = (chk.Checked) ? "1" : "0";
                        resultForDisplay = (chk.Checked) ? "Yes" : "No";

                        if (previousClient != null && previousClient.AttributeValue != String.Empty)
                        {
                            previousResultForDisplay = (previousClient.AttributeValue == "1") ? "Yes" : "No";
                        }
                        break;
                    case "Guid":
                        HtmlInputHidden hidden = (HtmlInputHidden)pHolder.Controls[1];
                        result = hidden.Value;

                        if (client != null && !string.IsNullOrEmpty(result))
                        {
                            Berkley.BLC.Entities.User user = Berkley.BLC.Entities.User.GetOne("ID = ?", result);
                            if (user != null)
                            {
                                resultForDisplay = user.Name;
                            }
                        }

                        if (previousClient != null && !string.IsNullOrEmpty(previousClient.AttributeValue))
                        {
                            Berkley.BLC.Entities.User user = Berkley.BLC.Entities.User.GetOne("ID = ?", previousClient.AttributeValue);
                            if (user != null)
                            {
                                previousResultForDisplay = user.Name;
                            }
                        }
                        break;
                    default:
                        TextBox txt = (TextBox)pHolder.Controls[0];
                        result = txt.Text;
                        resultForDisplay = txt.Text;
                        previousResultForDisplay = (previousClient != null) ? txt.Text : string.Empty;
                        break;
                }

                bool update = false;
                if (previousClient != null)
                {
                    if (previousClient.AttributeValue != result)
                    {
                        ClientHistory history = new ClientHistory(Guid.NewGuid());
                        history.ClientID = client.ID;
                        history.EntryText = string.Format("{0} changed {1} from {2} to {3}.", CurrentUser.Name, previousClient.Attribute.Type.Name, previousResultForDisplay, resultForDisplay);
                        history.EntryDate = DateTime.Now;
                        history.Save();

                        update = true;
                    }
                }
                else
                {
                    ClientHistory history = new ClientHistory(Guid.NewGuid());
                    history.ClientID = client.ID;
                    history.EntryText = string.Format("{0} set {1} to {2}.", CurrentUser.Name, client.Attribute.Type.Name, resultForDisplay);
                    history.EntryDate = DateTime.Now;
                    history.Save();

                    update = true;
                }

                //save the changes
                if (update)
                {
                    //check if we need to save any changes to Client Core (Enterprise Client Repository)
                    if (!String.IsNullOrEmpty(client.Attribute.Type.ClientCoreGroupName))
                    {
                        try
                        {
                            BCS.ProspectWebService service = new BCS.ProspectWebService();
                            service.SubmitClientMembershipInfo(CurrentUser.Company, client.ID, client.Attribute.Type.ClientCoreGroupName, result);
                        }
                        catch (BCS.BlcProspectWebServiceException bpwse)
                        {
                            JavaScript.ShowMessage(this, bpwse.Message);
                            QCI.ExceptionManagement.ExceptionManager.Publish(bpwse);
                            return;
                        }
                    }
                    
                    client = client.GetWritableInstance();
                    client.AttributeValue = result;
                    client.DateModified = DateTime.Now;
                    client.Save();
                }
            }
        }

        //post back with the read-only version
        this.boxResults.Visible = true;
        this.boxEditResults.Visible = false;
        btnSearch_Click(null, null);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        //post back with the read-only version
        this.boxResults.Visible = true;
        this.boxEditResults.Visible = false;
        boxHistory.Visible = true;
    }

    [Serializable]
    private class AdminClientClient
    {
        private string _id;
        private Guid _attributeID;
        private string _attributeValue = String.Empty;

        public AdminClientClient(string id, Guid attributeID, string attributeValue)
        {
            _id = id;
            _attributeID = attributeID;
            _attributeValue = attributeValue;
}

        public string Id
        {
            get {return _id;}
        }
        public Guid AttributeID
        {
            get {return _attributeID;}
        }
        public string AttributeValue
        {
            get {return _attributeValue;}
        }

        public static List<AdminClientClient> GetAdminClientClients(Client[] clients)
        {
            if (clients == null)
                return null;

            List<AdminClientClient> aClients = new List<AdminClientClient>();

            foreach (Client client in clients)
            {
                aClients.Add(new AdminClientClient(client.ID, client.AttributeID, client.AttributeValue));
            }

            return aClients;
        }
    }
}
