using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Import;
using Berkley.BLC.Business;
using QCI.Web;
using QCI.Web.Validation;
using QCI.ExceptionManagement;
using Wilson.ORMapper;

public partial class ServicePlanDetailsAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(ServicePlanDetailsAspx_Load);
        this.PreRender += new EventHandler(ServicePlanDetailsAspx_PreRender);
        this.grdLocations.SortCommand += new DataGridSortCommandEventHandler(grdLocations_SortCommand);
        this.grdLocations.PageIndexChanged += new DataGridPageChangedEventHandler(grdLocations_PageIndexChanged);
        this.grdClaims.SortCommand += new DataGridSortCommandEventHandler(grdClaims_SortCommand);
        this.grdClaims.PageIndexChanged += new DataGridPageChangedEventHandler(grdClaims_PageIndexChanged);
        this.btnExport.Click += new EventHandler(btnExport_Click);

        this.btnUpdateInsured.Click += new EventHandler(btnUpdateInsured_Click);
        this.btnUpdateAgency.Click += new EventHandler(btnUpdateAgency_Click);
        this.btnUpdateLocationContact.Click += new EventHandler(btnUpdateLocationContact_Click);
    }
    #endregion

    #region Enum
    /// <summary>
    /// Screen Mode Types
    /// </summary>
    protected struct ScreenMode
    {
        public const string ReadOnly = "R";
        public const string EditInsured = "EI";
        public const string EditPolicy = "EP";
        public const string EditInsuredAndPolicy = "EIP";
        public const string Print = "P";
    }
    #endregion

    protected ServicePlan _eServicePlan;
    private const string claimsSortExpression = "ClaimsSortExpression";
    private const string claimsSortReversed = "ClaimsSortReversed";
    private const string claimsPageIndex = "ClaimsPageIndex";
    private const string locationsSortExpression = "LocationsSortExpression";
    private const string locationsSortReversed = "LocationsSortReversed";
    private const string locationsPageIndex = "LocationsPageIndex";

    //Parameter configurations
    protected bool _sicDisplyCodeDescription;
    protected bool _naicDisplyCodeDescription;
    protected bool _requiresNAICCode;
    protected bool _hideSICCode;
    protected bool canEditInsured = false;
    protected bool canEditPolicy = false;
    protected string fieldGroup = string.Empty;

    void ServicePlanDetailsAspx_Load(object sender, EventArgs e)
    {
        Guid gPlanID = ARMSUtility.GetGuidFromQueryString("planid", true);
        _eServicePlan = ServicePlan.Get(gPlanID);

        // put the insured and service plan number in the header
        base.PageHeading = string.Format("{0}", _eServicePlan.Insured.Name);

        //Parameters
        _sicDisplyCodeDescription = (Berkley.BLC.Business.Utility.GetCompanyParameter("SICDisplyCodeDescription", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _naicDisplyCodeDescription = (Berkley.BLC.Business.Utility.GetCompanyParameter("NASICDisplyCodeDescription", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _requiresNAICCode = (Berkley.BLC.Business.Utility.GetCompanyParameter("RequiresNAICCode", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _hideSICCode = (Berkley.BLC.Business.Utility.GetCompanyParameter("HideSICCode", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;

        if (!this.IsPostBack)
        {
            PopulateViewList();

            // get all the agency email addresses
            AgencyEmail[] eEmails = AgencyEmail.GetSortedArray("AgencyID = ?", "EmailAddress ASC", _eServicePlan.Insured.AgencyID);
            cboAgentContactEmail.DataBind(eEmails, "ID", "EmailAddress");
            
            if (_eServicePlan.Insured.AgentEmailID != Guid.Empty)
            {
                ListItem item = new ListItem(_eServicePlan.Insured.AgencyEmail.EmailAddress, _eServicePlan.Insured.AgentEmailID.ToString());
                if (!cboAgentContactEmail.Items.Contains(item))
                {
                    cboAgentContactEmail.Items.Add(item);
                }
            }

            // populate the sic list with all sic codes and descriptions
            if (!_hideSICCode)
            {
                SIC[] eSICCodes = SIC.GetArray(70, CurrentUser.Company.SICSortedByAlpha);
                string sicDisplayOrder = CurrentUser.Company.SICSortedByAlpha ? "Description|Code" : "Code|Description";

                if (_sicDisplyCodeDescription)
                    cboSIC.DataBind(eSICCodes, "Code", "{1} - {0}", sicDisplayOrder.Split('|'));
                else
                    cboSIC.DataBind(eSICCodes, "Code", "{0} - {1}", sicDisplayOrder.Split('|'));
                cboSIC.IsRequired = CurrentUser.Company.RequireSICCode;
            }

            // populate the NAIC list with all codes and descriptions
            NAIC[] eNAICcodes = NAIC.GetArray(70, CurrentUser.Company.NAICSortedByAlpha);
            string naicDisplayOrder = CurrentUser.Company.NAICSortedByAlpha ? "Description|Code" : "Code|Description";

            if (_naicDisplyCodeDescription)
                cboNAIC.DataBind(eNAICcodes, "Code", "{0} - {1}", naicDisplayOrder.Split('|'));
            else
                cboNAIC.DataBind(eNAICcodes, "Code", "{1} - {0}", naicDisplayOrder.Split('|'));
            cboNAIC.IsRequired = _requiresNAICCode;

            // populate the underwriter list
            Underwriter[] eUnderwriters = AppCache.GetUnderwriters(this, CurrentUser.Company, "ActiveUnderwriters", true);
            cboUnderwriter.DataBind(eUnderwriters, "Code", "Name");
            cboUnderwriter2.DataBind(eUnderwriters, "Code", "Name");            

            if (!string.IsNullOrEmpty(_eServicePlan.Insured.Underwriter))
            {
                Underwriter underwriter = Underwriter.GetOne("Code = ? && CompanyID = ?", _eServicePlan.Insured.Underwriter, CurrentUser.CompanyID);
                ListItem item = new ListItem((underwriter != null) ? underwriter.Name : _eServicePlan.Insured.Underwriter, _eServicePlan.Insured.Underwriter);
                if (!cboUnderwriter.Items.Contains(item))
                {
                    cboUnderwriter.Items.Add(item);
                }
            }

            if (!string.IsNullOrEmpty(_eServicePlan.Insured.Underwriter2))
            {
                Underwriter underwritersec = Underwriter.GetOne("Code = ? && CompanyID = ?", _eServicePlan.Insured.Underwriter2, CurrentUser.CompanyID);
                ListItem item = new ListItem((underwritersec != null) ? underwritersec.Name : _eServicePlan.Insured.Underwriter2, _eServicePlan.Insured.Underwriter2);
                if (!cboUnderwriter2.Items.Contains(item))
                {
                    cboUnderwriter2.Items.Add(item);
                }
            }     

            // restore sort expression, reverse state, and page index settings
            this.ViewState[locationsSortExpression] = this.UserIdentity.GetPageSetting(locationsSortExpression, grdLocations.Columns[0].SortExpression);
            this.ViewState[locationsSortReversed] = this.UserIdentity.GetPageSetting(locationsSortReversed, false);
            grdLocations.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting(locationsPageIndex, 0);

            this.ViewState[claimsSortExpression] = this.UserIdentity.GetPageSetting(claimsSortExpression, grdClaims.Columns[0].SortExpression);
            this.ViewState[claimsSortReversed] = this.UserIdentity.GetPageSetting(claimsSortReversed, false);
            grdClaims.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting(claimsPageIndex, 0);
        }
    }

    void ServicePlanDetailsAspx_PreRender(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.cboMode.SelectedValue = this.UserIdentity.GetPageSetting("PageMode", ScreenMode.ReadOnly).ToString();
        }

        string sortLocationsExp = (string)this.ViewState[locationsSortExpression];
        if ((bool)this.ViewState[locationsSortReversed])
        {
            sortLocationsExp = sortLocationsExp.Replace(" DESC", " TEMP");
            sortLocationsExp = sortLocationsExp.Replace(" ASC", " DESC");
            sortLocationsExp = sortLocationsExp.Replace(" TEMP", " ASC");
        }

        string sortClaimsExp = (string)this.ViewState[claimsSortExpression];
        if ((bool)this.ViewState[claimsSortReversed])
        {
            sortClaimsExp = sortClaimsExp.Replace(" DESC", " TEMP");
            sortClaimsExp = sortClaimsExp.Replace(" ASC", " DESC");
            sortClaimsExp = sortClaimsExp.Replace(" TEMP", " ASC");
        }

        // target the appropriate control group (txt for edit fields, lbl for display labels)
        fieldGroup = (cboMode.SelectedValue != ScreenMode.ReadOnly && string.IsNullOrEmpty(fieldGroup)) ? "txt" : "lbl";

        this.PopulateFields(fieldGroup, _eServicePlan.Insured);
        this.PopulateFields(fieldGroup, _eServicePlan.Insured.Agency);

        // get the primary contact for the first location
        LocationContact eContact = LocationContact.GetOne("Location[InsuredID = ? && Number = 1 && IsActive = true] && PriorityIndex = 1", _eServicePlan.InsuredID);
       
        //this.PopulateFields(fieldGroup, eContact);

        if (eContact != null)
        {
            this.PopulateFields("lbl", eContact);
        }
        else
        {
            lblLocationContactName.Text = "(not specified)";
            lblLocationContactTitle.Text = "(not specified)";
            lblLocationContactPhone.Text = "(not specified)";
            lblLocationContactAltPhone.Text = "(not specified)";
            lblLocationContactFax.Text = "(not specified)";
            lblLocationContactEmail.Text = "(not specified)";
            lblLocationContactAddress.Text = "(not specified)";
        }
        

        // get the policies of the insured
        Policy[] ePolicies = Policy.GetSortedArray("InsuredID = ? && IsActive = true", "Number ASC", _eServicePlan.InsuredID);
        DataGridHelper.BindGrid(grdPolicies, ePolicies, null, "There are no policies for this insured.");

        // get all the locations for this insured
        Location[] eLocations = Location.GetSortedArray("InsuredID = ? && IsActive = true", sortLocationsExp, _eServicePlan.InsuredID);
        grdLocations.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindPagingGrid(grdLocations, eLocations, "ID", "There are no locations for this insured.");

        Claim[] eClaims = Claim.GetSortedArray("Policy[InsuredID = ?]", sortClaimsExp, _eServicePlan.InsuredID);
        grdClaims.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindPagingGrid(grdClaims, eClaims, "ID", "There are no claims for this insured.");

        // get all the rate mod factors associated with this survey, if any
        RateModificationFactor[] rateModFactors = RateModificationFactor.GetSortedArray("Policy[InsuredID = ?]", "Policy.Number ASC, Policy.Mod ASC, StateCode ASC, TypeCode ASC", _eServicePlan.InsuredID);
        DataGridHelper.BindGrid(grdRateModFactors, rateModFactors, null, "There are no rate modification factors for this insured.");
        
    }

    private void PopulateViewList()
    {
        canEditInsured = CurrentUser.Permissions.CanTakeAction(ServicePlanActionType.EditInsured, _eServicePlan, CurrentUser);
        canEditPolicy = CurrentUser.Permissions.CanTakeAction(ServicePlanActionType.EditPolicy, _eServicePlan, CurrentUser);
        bool showScreenMode = (canEditInsured || canEditPolicy) ? true : false;

        ListItem[] items;
        if (canEditInsured || canEditPolicy)
        {
            items = new ListItem[2];
            items[0] = new ListItem("Read-Only View", ScreenMode.ReadOnly);
            items[1] = new ListItem("Edit View", ScreenMode.EditInsuredAndPolicy);
        }
        else if (CurrentUser.Company.ID == Company.WRBerkleyCorporation.ID)
        {
            items = new ListItem[1];
            items[0] = new ListItem("Read-Only View", ScreenMode.ReadOnly);
        }
        else
        {
            items = new ListItem[1];
            items[0] = new ListItem("Read-Only View", ScreenMode.ReadOnly);
        }

        cboMode.Items.AddRange(items);
        cboMode.Visible = false;
        if (showScreenMode)
        {
            cboMode.Visible = true;
        }
    }

    void grdLocations_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdLocations.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting(locationsPageIndex, e.NewPageIndex);

        JavaScript.SetInputFocus(boxLocations);
    }

    void grdLocations_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState[locationsSortExpression];
        bool sortRev = (bool)this.ViewState[locationsSortReversed];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState[locationsSortExpression] = e.SortExpression;
        this.ViewState[locationsSortReversed] = sortRev;

        this.UserIdentity.SavePageSetting(locationsSortExpression, e.SortExpression);
        this.UserIdentity.SavePageSetting(locationsSortReversed, sortRev);

        JavaScript.SetInputFocus(boxLocations);
    }

    void grdClaims_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdClaims.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting(claimsPageIndex, e.NewPageIndex);

        JavaScript.SetInputFocus(boxClaims);
    }

    void grdClaims_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState[claimsSortExpression];
        bool sortRev = (bool)this.ViewState[claimsSortReversed];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState[claimsSortExpression] = e.SortExpression;
        this.ViewState[claimsSortReversed] = sortRev;

        this.UserIdentity.SavePageSetting(claimsSortExpression, e.SortExpression);
        this.UserIdentity.SavePageSetting(claimsSortReversed, sortRev);

        JavaScript.SetInputFocus(boxClaims);
    }

    protected string GetBuildingCount(object dataItem)
    {
        Location eLocation = (dataItem as Location);
        return Building.GetArray("LocationID = ?", eLocation.ID).Length.ToString();
    }

    protected string GetProfitCenterName(object obj)
    {
        Policy policy = (obj as Policy);
        ProfitCenter pr = ProfitCenter.GetOne("Code = ? && CompanyID = ?", policy.ProfitCenter, CurrentUser.CompanyID);

        if (pr != null)
        {
            return pr.Name;
        }
        else
        {
            return "(none)";
        }
    }

    void btnExport_Click(object sender, EventArgs e)
    {
        Claim[] claims = Claim.GetSortedArray("Policy[InsuredID = ?]", this.ViewState[claimsSortExpression].ToString(), _eServicePlan.InsuredID);

        //Get the columns for the CSV export
        ExportItemList oExportDataList = new ExportItemList(this);
        for (int i = 0; i < grdClaims.Columns.Count; i++)
        {
            string dataType;
            switch (i)
            {
                case 0:
                    dataType = "String";
                    break;
                case 1:
                    dataType = "String";
                    break;
                case 2:
                    dataType = "DateTime";
                    break;
                case 3:
                    dataType = "DateTime";
                    break;
                case 4:
                    dataType = "Decimal";
                    break;
                case 5:
                    dataType = "String";
                    break;
                case 6:
                    dataType = "String";
                    break;
                case 7:
                    dataType = "String";
                    break;
                case 8:
                    dataType = "String";
                    break;
                default:
                    throw new NotSupportedException("Not expecting this many columns in the claims grid.");
            }

            DataGridColumn column = grdClaims.Columns[i];
            string filterName = column.SortExpression.Replace("ASC", "").Replace("DESC", "").Trim();
            oExportDataList.Add(new ExportItem(Guid.NewGuid(), column.SortExpression, filterName, dataType, column.HeaderText, i));
        }

        // Get the CSV data
        string strCsv = oExportDataList.GetCsv(claims, oExportDataList, "No Claims Found.");

        Response.ContentType = "application/x-msexcel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=claims.csv");

        // Write out the CSV data
        System.Text.ASCIIEncoding oEncoding = new System.Text.ASCIIEncoding(); ;
        byte[] rbBytes = oEncoding.GetBytes(strCsv);
        Response.OutputStream.Write(rbBytes, 0, rbBytes.Length);

        Response.End();
    }

    void btnUpdateInsured_Click(object sender, EventArgs e)
    {
        try
        {
            //commit changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //save insured data
                Insured eInsured = _eServicePlan.Insured.GetWritableInstance();

                if (txtName.Text.Trim().CompareTo(eInsured.Name.Trim()) != 0)
                {
                    eInsured.ExcludeName = true;
                }
                if (txtName2.Text.Trim().CompareTo(eInsured.Name2.Trim()) != 0)
                {
                    eInsured.ExcludeName2 = true;
                }
                if (txtStreetLine1.Text.Trim().CompareTo(eInsured.StreetLine1.Trim()) != 0)
                {
                    eInsured.ExcludeStreetLine1 = true;
                }
                if (txtStreetLine2.Text.Trim().CompareTo(eInsured.StreetLine2.Trim()) != 0)
                {
                    eInsured.ExcludeStreetLine2 = true;
                }
                if (txtStreetLine3.Text.Trim().CompareTo(eInsured.StreetLine3.Trim()) != 0)
                {
                    eInsured.ExcludeStreetLine3 = true;
                }
                if (txtCity.Text.CompareTo(eInsured.City.Trim()) != 0)
                {
                    eInsured.ExcludeCity = true;
                }
                if (cboState.SelectedValue.CompareTo(eInsured.StateCode) != 0)
                {
                    eInsured.ExcludeState = true;
                }
                if (txtZipCode.Text.Trim().CompareTo(eInsured.ZipCode.Trim()) != 0)
                {
                    eInsured.ExcludeZip = true;
                }
                if (txtBusinessOperations.Text.Trim().CompareTo(eInsured.BusinessOperations.Trim()) != 0)
                {
                    eInsured.ExcludeBusiness = true;
                }
                if (cboSIC.SelectedValue.CompareTo(eInsured.SICCode) != 0)
                {
                    eInsured.ExcludeSIC = true;
                }
                if (cboUnderwriter.SelectedValue.CompareTo(eInsured.Underwriter) != 0)
                {
                    eInsured.ExcludeUnderWriter = true;
                }

                this.PopulateEntity("txt", eInsured);
                eInsured.Save(trans);

                trans.Commit();
            }
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }

        //refresh the service plan instance
        _eServicePlan = ServicePlan.Get(_eServicePlan.ID);
    }

    void btnUpdateAgency_Click(object sender, EventArgs e)
    {
        try
        {
            //commit changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //save insured data
                Insured eInsured = _eServicePlan.Insured.GetWritableInstance();
                this.PopulateEntity("txt", eInsured);

                //Check if agency has changed and update the Insured's associated value
                Agency eAgency;
                if (txtAgencyNumber.Text.Trim().Length > 0 && txtAgencyNumber.Text.Trim() != _eServicePlan.Insured.Agency.Number)
                {
                    eAgency = Agency.GetOne("CompanyID = ? && (Number = ? || Number = ? || Number = ?)", this.CurrentUser.CompanyID, txtAgencyNumber.Text.PadLeft(5, '0'), txtAgencyNumber.Text.TrimStart('0'), txtAgencyNumber.Text);
                    eInsured.AgencyID = eAgency.ID;
                }
                else
                {
                    eAgency = _eServicePlan.Insured.Agency;
                }

                //save agency data
                eAgency = eAgency.GetWritableInstance();
                this.PopulateEntity("txt", eAgency);

                eInsured.Save(trans);
                eAgency.Save(trans);
                
                trans.Commit();
            }
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }

        //refresh the service plan instance
        _eServicePlan = ServicePlan.Get(_eServicePlan.ID);

        //JavaScript.SetInputFocus(boxAgency);
        
    }

    void btnUpdateLocationContact_Click(object sender, EventArgs e)
    {
        try
        {
            //commit changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //save location contact data
                LocationContact eContact = LocationContact.GetOne("Location[InsuredID = ? && Number = 1 && IsActive = true] && PriorityIndex = 1", _eServicePlan.InsuredID).GetWritableInstance();

                this.PopulateEntity("txt", eContact);
                eContact.Save(trans);

                trans.Commit();
            }
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }

        //refresh the service plan instance
        _eServicePlan = ServicePlan.Get(_eServicePlan.ID);
    }

    void btnAddEmail_Click(object sender, EventArgs e)
    {
        //commit changes
        AgencyEmail eEmail;
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            // Insert new email
            eEmail = new AgencyEmail(Guid.NewGuid());
            eEmail.AgencyID = _eServicePlan.Insured.AgencyID;
            eEmail.EmailAddress = txtNewAgencyEmail.Text;
            eEmail.Save(trans);

            // Tie the new email to the insured
            Insured eInsured = _eServicePlan.Insured.GetWritableInstance();
            eInsured.AgentContactName = txtAgentContactName.Text;
            eInsured.AgentContactPhoneNumber = txtAgentContactPhoneNumber.Text;
            eInsured.AgentEmailID = eEmail.ID;
            eInsured.Save(trans);

            trans.Commit();
        }

        // Update the email list
        AgencyEmail[] eEmails = AgencyEmail.GetSortedArray("AgencyID = ?", "EmailAddress ASC", _eServicePlan.Insured.AgencyID);
        cboAgentContactEmail.DataBind(eEmails, "ID", "EmailAddress");

        // Set the new as selected by default
        cboAgentContactEmail.SelectedValue = eEmail.ID.ToString();
        txtNewAgencyEmail.Text = string.Empty;

        //refresh the survey instance
        _eServicePlan = ServicePlan.Get(_eServicePlan.ID);
    }

    void cboMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList cbo = (DropDownList)sender;
        this.UserIdentity.SavePageSetting("PageMode", cbo.SelectedValue);
    }
}
