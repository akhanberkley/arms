using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web.Validation;
using QCI.Web;

public partial class AdminAssignmentRuleEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminAssignmentRuleEditAspx_Load);
        this.btnSave.Click += new EventHandler(btnSave_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.ucFilterEditor.Load += new EventHandler(ucFilterEditor_Load);
    }
    #endregion

    private AssignmentRule _rule;
    private SurveyStatusType _type;

    void AdminAssignmentRuleEditAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("id", false);
        _rule = (id != Guid.Empty) ? AssignmentRule.Get(id) : null;

        string typeCode = ARMSUtility.GetStringFromQueryString("type", true);
        _type = SurveyStatusType.Get(typeCode);

        if (!this.IsPostBack)
        {
            AssignmentRuleAction[] actionTypes;

            if ((Berkley.BLC.Business.Utility.GetCompanyParameter("UsesInHouseSelection", CurrentUser.Company).ToUpper() == "TRUE") ? true : false)
                actionTypes = AssignmentRuleAction.GetSortedArray("TypeCode = ?", "DisplayOrder ASC", (_type == SurveyStatusType.Survey) ? "H" : "A");
            else if (_type == SurveyStatusType.Survey)
                actionTypes = AssignmentRuleAction.GetSortedArray("TypeCode = ? && Code = ?", "DisplayOrder ASC", "H", "HH");
            else          
                actionTypes = AssignmentRuleAction.GetSortedArray("TypeCode = ?", "DisplayOrder ASC", "A");

            ComboHelper.BindCombo(cboMethod, actionTypes, "Code", "DisplayName");

            if (_rule != null)
            {
                ucFilterEditor.CompanyID = CurrentUser.CompanyID;
                ucFilterEditor.LoadFilter(_rule.Filter);
                cboMethod.SelectedValue = _rule.Action.Code;
            }
            else
            {
                cboMethod.Items.Insert(0, new ListItem("-- select action --", ""));
            }

            btnSave.Text = (_rule == null) ? "Add" : "Update";
        }
    }

    void ucFilterEditor_Load(object sender, EventArgs e)
    {
        ucFilterEditor.CompanyID = CurrentUser.CompanyID;
    }

    void btnSave_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            if (_rule == null)
            {
                _rule = new AssignmentRule(Guid.NewGuid());
                _rule.CompanyID = this.UserIdentity.CompanyID;
                _rule.PriorityIndex = 1 + AssignmentRule.GetArray("CompanyID = ? && SurveyStatusTypeCode = ?", _rule.CompanyID, _type.Code).Length;
            }
            else
            {
                _rule = _rule.GetWritableInstance();
            }

            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                Guid filterID = ucFilterEditor.SaveFilter(trans);

                _rule.FilterID = filterID;
                _rule.ActionCode = cboMethod.SelectedValue;
                _rule.SurveyStatusTypeCode = _type.Code;
                _rule.Save(trans);

                trans.Commit();
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl(), true);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        return "AdminAssignmentRules.aspx";
    }
}
