using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web.Validation;

public partial class AdminRoleEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminRoleEditAspx_Load);
        this.PreRender += new EventHandler(AdminRoleEditAspx_PreRender);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected UserRole _role;

    void AdminRoleEditAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("roleid", false);
        _role = (id != Guid.Empty) ? UserRole.Get(id) : null;
    }

    void AdminRoleEditAspx_PreRender(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (_role != null) // edit
            {
                this.PopulateFields(_role);

                // load the permission set editor
                ucPermissions.LoadPermissionSet(_role.PermissionSet);
            }

            btnSubmit.Text = (_role == null) ? "Add" : "Update";
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            UserRole role;
            PermissionSet permissionSet;
            if (_role == null) // add
            {
                role = new UserRole(Guid.NewGuid());
                role.CompanyID = this.CurrentUser.CompanyID;

                permissionSet = new PermissionSet(Guid.NewGuid());
                role.PermissionSetID = permissionSet.ID;
            }
            else // edit
            {
                role = _role.GetWritableInstance();
                permissionSet = role.PermissionSet.GetWritableInstance();
            }

            this.PopulateEntity(role);

            // commit all changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                ucPermissions.Save(trans, permissionSet);

                role.Save(trans);

                trans.Commit();
            }

            // remove role from cache to force refresh
            PermissionSetCache.Clear(role);

            // update our member var so redirect will work correctly
            _role = role;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // return the user
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        return "AdminRoles.aspx";
    }
}
