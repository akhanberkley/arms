using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web;

public partial class AdminLettersAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminLettersAspx_Load);
        this.PreRender += new EventHandler(AdminLettersAspx_PreRender);
        this.grdDocs.ItemCommand += new DataGridCommandEventHandler(grdDocs_ItemCommand);
    }
    #endregion

    void AdminLettersAspx_Load(object sender, EventArgs e)
    {
    }

    void AdminLettersAspx_PreRender(object sender, EventArgs e)
    {
        // get letters
        MergeDocument[] eDocs = MergeDocument.GetSortedArray("CompanyID = ? && Type = ? && Disabled = ?", "PriorityIndex ASC", this.UserIdentity.CompanyID, MergeDocumentType.Letter.ID, chkShowDisabled.Checked);
        
        string noRecordsMessage = string.Format("There are no {0} letter templates for your company.", (!chkShowDisabled.Checked) ? "active" : "disabled");

        grdDocs.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindGrid(grdDocs, eDocs, "ID", noRecordsMessage);

        if (chkShowDisabled.Checked)
        {
            for (int i = 0; i < grdDocs.Columns.Count; i++)
            {
                DataGridColumn col = grdDocs.Columns[i];
                col.Visible = (col.HeaderText.ToUpper() != "MOVE");
            }
        }
        else
        {
            for (int i = 0; i < grdDocs.Columns.Count; i++)
            {
                DataGridColumn col = grdDocs.Columns[i];
                col.Visible = true;
            }
        }
    }

    protected string GetBoolLabel(object dataItem)
    {
        MergeDocument eDoc= (dataItem as MergeDocument);
        return (eDoc.Disabled) ? "Yes" : "No";
    }

    void grdDocs_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid gLetterID = (Guid)grdDocs.DataKeys[e.Item.ItemIndex];

        // get the current letters from the DB and find the index of the primary letter
        MergeDocument[] eLetters = MergeDocument.GetSortedArray("CompanyID = ? && Type = 2 && Disabled = false", "PriorityIndex ASC", this.UserIdentity.CompanyID);
        int iLetterIndex = int.MinValue;
        for (int i = 0; i < eLetters.Length; i++)
        {
            if (eLetters[i].ID == gLetterID)
            {
                iLetterIndex = i;
                break;
            }
        }
        if (iLetterIndex == int.MinValue) // not found
        {
            return;
        }

        MergeDocument letter = eLetters[iLetterIndex];

        // execute the requested transformation on the letters
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            ArrayList letterList = new ArrayList(eLetters);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEUP":
                    {
                        letterList.RemoveAt(iLetterIndex);
                        if (iLetterIndex > 0)
                        {
                            letterList.Insert(iLetterIndex - 1, letter);
                        }
                        else // top item (move to bottom)
                        {
                            letterList.Add(letter);
                        }
                        break;
                    }
                case "MOVEDOWN":
                    {
                        letterList.RemoveAt(iLetterIndex);
                        if (iLetterIndex < eLetters.Length - 1)
                        {
                            letterList.Insert(iLetterIndex + 1, letter);
                        }
                        else // bottom item (move to top)
                        {
                            letterList.Insert(0, letter);
                        }
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the letters still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < letterList.Count; i++)
            {
                MergeDocument eLetterUpdate = (letterList[i] as MergeDocument).GetWritableInstance();
                eLetterUpdate.PriorityIndex = (i + 1);
                eLetterUpdate.Save(trans);
            }

            trans.Commit();
        }
    }
}
