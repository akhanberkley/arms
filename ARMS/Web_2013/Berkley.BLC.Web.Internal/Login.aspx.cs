using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using Berkley.BLC.Entities;
using Entities = Berkley.BLC.Entities;
using QCI.Web;
using QCI.Web.Validation;

namespace Berkley.BLC.Web.Internal
{
    public partial class LoginAspx : AppBasePage
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(LoginAspx_Load);
            this.btnLogin.Click += new EventHandler(btnLogin_Click);
            this.cboCompany.SelectedIndexChanged += new EventHandler(cboCompany_SelectedIndexChanged);
        }
        #endregion

        void LoginAspx_Load(object sender, EventArgs e)
        {
            // get user's domain account, if any
            string userNameAndDomain = this.Request.ServerVariables["LOGON_USER"];
            if (userNameAndDomain == null) userNameAndDomain = string.Empty;

            // see if this user is a system admin
            string sysAdminList = ConfigurationManager.AppSettings["SystemAdminAccounts"];
            bool isSystemAdmin = false;
            if (userNameAndDomain.Length > 0 && sysAdminList != null && sysAdminList.Length > 0)
            {
                string[] accounts = sysAdminList.Split(';');
                foreach (string account in accounts)
                {
                    if (string.Compare(account, userNameAndDomain, true) == 0) // match
                    {
                        isSystemAdmin = true;
                        break;
                    }
                }
            }

            // get the integrated security flag
            string integratedSecurity = ConfigurationManager.AppSettings["LoginUsesIntegratedSecurity"];
            if (integratedSecurity == null) throw new Exception("Configuration setting 'LoginUseintegratedSecurity' must be specified in the application config file.");

            // integrated security is required when the flag is anything but zero
            if (!isSystemAdmin && integratedSecurity != "0")
            {
                // try to find the user by username (including domain)
                Entities.User eUser;
                int separatorIndex = userNameAndDomain.IndexOf('\\');
                string username = userNameAndDomain.Substring(separatorIndex + 1);
                string domain=string.Empty;
                if (separatorIndex >= 0)
                    domain = userNameAndDomain.Substring(0, separatorIndex);
                eUser = Entities.User.GetOne("Username = ? || Username = ?", username, userNameAndDomain);

                //check if the user is an underwriter by checking if the company number is passed
                string companyNumber = ARMSUtility.GetStringFromQueryString("companynumber", false);
                if (eUser == null && companyNumber != null && domain.ToUpper() == "WRBTS")
                {
                    //The domain user is defaulted to an underwriter role
                    eUser = Entities.User.GetOne("Company[Number = ?] && IsUnderwriter = true", companyNumber);
                }

                if (eUser != null)
                {
                    Entities.LoginHistory login = new Entities.LoginHistory(username, Request.UserHostAddress, true);
                    login.Save();

                    //Clear the user's page index settings
                    DB.Engine.GetDataSet(string.Format("EXEC UserSetting_DeletePageIndex @UserID='{0}'", eUser.ID));

                    // use the Login page's method to log the user in and redirect them
                    Security.LoginUser(eUser, false, ARMSUtility.GetDefaultPageUrl(eUser));
                    return;
                }
                else
                {
                    // record the login failure
                    Entities.LoginHistory eLogin = new Entities.LoginHistory(userNameAndDomain, Request.UserHostAddress, false);
                    eLogin.Save();

                    // send them a 403 (request denied) error
                    Security.RequestForbidden();
                    return;
                }
            }
            else // integrated security disabled (or a sys admin user)
            {
                // give the username field the focus on first page hit
                if (!this.IsPostBack)
                {
                    JavaScript.SetInputFocus(cboCompany.InputControl);

                    bool bIsLocal = Security.IsHttpRequestLocal();
                    //chkPersist.Visible = bIsLocal;
                    //chkPersist.Checked = bIsLocal;

                    // populate the user list
                    cboCompany_SelectedIndexChanged(null, null);
                }
            }
        }

        void btnLogin_Click(object sender, EventArgs e)
        {
            // handle non-IE browsers and disabled javascript
            if (!this.IsValid)
            {
                ShowValidatorErrors();
                return;
            }

            string userID = cboUser.SelectedValue;

            // try to find the user
            Entities.User eAuthedUser = Entities.User.GetOne("ID = ?", new Guid(userID));
            if (eAuthedUser == null)
            {
                JavaScript.ShowMessage(this, "The selected user could not be found.");
                JavaScript.SetInputFocus(cboUser.InputControl);
                return;
            }

            // log the user in and take them to the default/return page
            Security.LoginUser(eAuthedUser, false, ARMSUtility.GetDefaultPageUrl(eAuthedUser));
        }

        void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboUser.Items.Clear();

            string companyID = cboCompany.SelectedValue;
            if (companyID != string.Empty)
            {
                Entities.User[] eUsers = Entities.User.GetSortedArray("CompanyID = ? && AccountDisabled = false && IsFeeCompany = false", "Name ASC", new Guid(companyID));
                cboUser.DataBind(eUsers, "ID", "Name");
            }
        }
    }
}
