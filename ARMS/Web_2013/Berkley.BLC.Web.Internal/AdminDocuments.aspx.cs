using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Wilson.ORMapper;
using QCI.Web;

public partial class AdminDocumentsAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminDocumentAspx_Load);
        this.PreRender += new EventHandler(AdminDocumentAspx_PreRender);
        this.grdDocs.ItemCommand += new DataGridCommandEventHandler(grdDocs_ItemCommand);
    }
    #endregion

    void AdminDocumentAspx_Load(object sender, EventArgs e)
    {
    }

    void AdminDocumentAspx_PreRender(object sender, EventArgs e)
    {
        // get Documents 
        MergeDocument[] eDocs = MergeDocument.GetSortedArray("CompanyID = ? && Type = ? && Disabled = ?", "PriorityIndex ASC",
            this.UserIdentity.CompanyID, MergeDocumentType.Document.ID, chkShowDisabled.Checked);

        string noRecordsMessage = string.Format("There are no {0} document templates for your company.", (!chkShowDisabled.Checked) ? "active" : "disabled");

        grdDocs.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindGrid(grdDocs, eDocs, "ID", noRecordsMessage);

        if (chkShowDisabled.Checked)
        {
            for (int i = 0; i < grdDocs.Columns.Count; i++)
            {
                DataGridColumn col = grdDocs.Columns[i];
                col.Visible = (col.HeaderText.ToUpper() != "MOVE");
            }
        }
        else
        {
            for (int i = 0; i < grdDocs.Columns.Count; i++)
            {
                DataGridColumn col = grdDocs.Columns[i];
                col.Visible = true;
            }
        }
    }

    protected string GetBoolLabel(object dataItem)
    {
        MergeDocument eDoc = (dataItem as MergeDocument);
        return (eDoc.Disabled) ? "Yes" : "No";
    }

    void grdDocs_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid gReportID = (Guid)grdDocs.DataKeys[e.Item.ItemIndex];

        // get the current documents from the DB and find the index of the primary report
        MergeDocument[] eReports = MergeDocument.GetSortedArray("CompanyID = ? && Type = ? && Disabled = false", "PriorityIndex ASC", this.UserIdentity.CompanyID, MergeDocumentType.Document.ID);
        int iReportIndex = int.MinValue;
        for (int i = 0; i < eReports.Length; i++)
        {
            if (eReports[i].ID == gReportID)
            {
                iReportIndex = i;
                break;
            }
        }
        if (iReportIndex == int.MinValue) // not found
        {
            return;
        }

        MergeDocument report = eReports[iReportIndex];

        // execute the requested transformation on the reports
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            ArrayList reportList = new ArrayList(eReports);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEUP":
                    {
                        reportList.RemoveAt(iReportIndex);
                        if (iReportIndex > 0)
                        {
                            reportList.Insert(iReportIndex - 1, report);
                        }
                        else // top item (move to bottom)
                        {
                            reportList.Add(report);
                        }
                        break;
                    }
                case "MOVEDOWN":
                    {
                        reportList.RemoveAt(iReportIndex);
                        if (iReportIndex < eReports.Length - 1)
                        {
                            reportList.Insert(iReportIndex + 1, report);
                        }
                        else // bottom item (move to top)
                        {
                            reportList.Insert(0, report);
                        }
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the reports still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < reportList.Count; i++)
            {
                MergeDocument eReportUpdate = (reportList[i] as MergeDocument).GetWritableInstance();
                eReportUpdate.PriorityIndex = (i + 1);
                eReportUpdate.Save(trans);
            }

            trans.Commit();
        }
    }
}
