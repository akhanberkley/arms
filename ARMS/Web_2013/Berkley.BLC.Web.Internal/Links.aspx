<%@ Page Language="C#" AutoEventWireup="false" CodeFile="Links.aspx.cs" Inherits="LinksAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <cc:Box id="boxLinks" runat="server" title="Links" Width="600">
        <asp:Repeater ID="repLinks" Runat="server">
        <HeaderTemplate>
            <table>
        </HeaderTemplate>
        <ItemTemplate>
	        <tr>
		        <td class="label" valign="middle" nowrap>
		            <span><%# HtmlEncode(Container.DataItem, "Description")%></span>
		        </td>
	        </tr>
	        <tr>
	            <td class="label" valign="middle" nowrap>
		            <span><a href='<%# HtmlEncode(Container.DataItem, "URL")%>' target="_blank"><%# HtmlEncode(Container.DataItem, "URL")%></a></span>
		        </td>
	        </tr>
        </ItemTemplate>
        <SeparatorTemplate>
            <tr><td>&nbsp;</td></tr>
        </SeparatorTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
        </asp:Repeater>
    </cc:Box>
    
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
