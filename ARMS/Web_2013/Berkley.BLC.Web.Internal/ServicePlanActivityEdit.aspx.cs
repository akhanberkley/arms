using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web.Validation;

public partial class ServicePlanActivityEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(ServicePlanActivityEditAspx_Load);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected ServicePlanActivity _eActivity;
    protected ServicePlan _eServicePlan;

    void ServicePlanActivityEditAspx_Load(object sender, EventArgs e)
    {
        Guid gActivityID = ARMSUtility.GetGuidFromQueryString("activityid", false);
        _eActivity = (gActivityID != Guid.Empty) ? ServicePlanActivity.Get(gActivityID) : null;

        Guid gServicePlanID = ARMSUtility.GetGuidFromQueryString("planid", true);
        _eServicePlan = ServicePlan.Get(gServicePlanID);

        if (!this.IsPostBack)
        {
            // populate the company specific activity types
            ActivityType[] eTypes = ActivityType.GetSortedArray("CompanyID = ? && (LevelCode = ? || LevelCode = ? || LevelCode = ?) && Disabled != True", "PriorityIndex ASC", this.CurrentUser.CompanyID, ActivityTypeLevel.ServicePlan.Code, ActivityTypeLevel.SurveyAndServicePlan.Code, ActivityTypeLevel.All.Code);
            cboActivityType.DataBind(eTypes, "ID", "Name");

            // populate the user combo
            User[] eUsers = Berkley.BLC.Entities.User.GetSortedArray("CompanyID = ? && IsFeeCompany = false && IsUnderwriter = false && AccountDisabled = false", "Name ASC", CurrentUser.CompanyID);
            cboAllocatedTo.DataBind(eUsers, "ID", "Name");

            if (_eActivity != null) // edit
            {
                this.PopulateFields(_eActivity);
                this.PopulateFields(_eActivity.User);

                //Check if the selected items exist in the list
                if (cboActivityType.Items.Contains(new ListItem(_eActivity.ActivityType.Name, _eActivity.ActivityTypeID.ToString())))
                {
                    this.PopulateFields(_eActivity.ActivityType);
                }
                else
                {
                    //add it to the list and select it
                    cboActivityType.Items.Add(new ListItem(_eActivity.ActivityType.Name, _eActivity.ActivityTypeID.ToString()));
                    cboActivityType.SelectedValue = _eActivity.ActivityTypeID.ToString();
                }
            }
            else
            {
                //Default the selected user to the current one
                cboAllocatedTo.SelectedValue = CurrentUser.ID.ToString();
            }

            btnSubmit.Text = (_eActivity == null) ? "Add" : "Update";
            base.PageHeading = (_eActivity == null) ? "Add Activity Time" : "Update Activity Time";
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            ServicePlanActivity eActivity;
            if (_eActivity == null) // add
            {
                eActivity = new ServicePlanActivity(Guid.NewGuid());
            }
            else
            {
                eActivity = _eActivity.GetWritableInstance();
            }

            eActivity.ServicePlanID = _eServicePlan.ID;
            eActivity.ActivityTypeID = new Guid(cboActivityType.SelectedValue);
            eActivity.AllocatedTo = new Guid(cboAllocatedTo.SelectedValue);
            eActivity.ActivityDate = dtActivityDate.Date;
            eActivity.ActivityHours = Convert.ToDecimal(numHours.Text);
            eActivity.Comments = txtComments.Text;

            // commit changes
            eActivity.Save();

            //Update the service plan history
            ServicePlanManager manager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
            manager.ActivityManagement(eActivity, (_eActivity == null) ? ServicePlanManager.UIAction.Add : ServicePlanManager.UIAction.Update);

            // update our member var so the redirect will work correctly
            _eActivity = eActivity;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(string.Format("ServicePlanActivities.aspx{0}", ARMSUtility.GetQueryStringForNav("activityid")), true);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("ServicePlanActivities.aspx{0}", ARMSUtility.GetQueryStringForNav("activityid")), true);
    }
}
