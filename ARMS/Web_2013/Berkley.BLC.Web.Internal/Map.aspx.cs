using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using System.Text;
using Microsoft.Http;

using Berkley.BLC.Entities;
using Entities = Berkley.BLC.Entities;
using QCI.Web;
using QCI.Web.Validation;

public partial class MapAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(MapAspx_Load);
    }
    #endregion

    private List<Pair> _colorList = new List<Pair>();
    protected const string _yahooID = "iJEQERzV34EScVQSjhbQ8Yu3GZCeg_Sg2RymqH4agCPUSIAsQctkHwJovMcTCiKWVynSe3TxTs8fx4QWoEgsXmI8ylQUvHw";
    private const string MappingFunction = "MappingFunction";
    private List<Triplet> _geoLocations = new List<Triplet>();
    private List<MapLocation> _invalidLocations = new List<MapLocation>();

    void MapAspx_Load(object sender, EventArgs e)
    {
        LoadColors();
        List<MapLocation> mapLocations = new List<MapLocation>();

        //set the page heading
        base.PageHeading = HttpUtility.UrlDecode(ARMSUtility.GetStringFromQueryString("name", true));

        string surveyNumbers = ARMSUtility.GetStringFromQueryString("surveys", true);
        DataSet ds = DB.Engine.GetDataSet(BuildQuery(surveyNumbers));

        //get the list of locations by consultant
        List<System.Threading.Thread> threads = new List<System.Threading.Thread>();
        Dictionary<string, MapGroup> locationsByUser = new Dictionary<string, MapGroup>();
        int i = 0;
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            string key = Common.ReadString(dr[(int)Column.ConsultantName]);
            if (locationsByUser.ContainsKey(key))
            {
                MapLocation mapLocation = BuildMapLocation(dr);
                GetCoordinates(ref mapLocation, locationsByUser[key].NamedColor);

                locationsByUser[key].MapLocations.Add(mapLocation);
            }
            else
            {
                MapGroup mapGroup = BuildMapGroup(dr, i);
                MapLocation mapLocation = BuildMapLocation(dr);
                mapGroup.MapLocations.Add(mapLocation);
                GetCoordinates(ref mapLocation, mapGroup.NamedColor);

                locationsByUser.Add(key, mapGroup);
                i = (i < _colorList.Count - 1) ? i + 1 : 0;
            }
        }

        //display a message stating which survey has an invalid zip code
        if (_invalidLocations.Count > 0)
        {
            string message = string.Empty;
            foreach (MapLocation invalidLocation in _invalidLocations)
            {
                message += string.Format("Survey #{0} - Location #{1}\r\n", invalidLocation.SurveyNumber, invalidLocation.LocationNumber);
            }

            JavaScript.ShowMessage(this, string.Format("The following survey(s) could not be mapped due to an invalid address for the survey location:\r\n\r\n{0}", message));
        }

        repAssignedUsers.DataSource = locationsByUser.Values;
        repAssignedUsers.DataBind();

        AddScriptsToPage(locationsByUser);
    }

    private void GetCoordinates(ref MapLocation location, string color)
    {
        MapLocation mapLocation = location as MapLocation;

        HttpQueryString vars = new HttpQueryString();
        
        //only by zip
        //vars.Add("q", mapLocation.ZipCode.Substring(0, 5));

        //by full address
        vars.Add("street", mapLocation.StreetLine1);
        vars.Add("city", mapLocation.City);
        vars.Add("state", mapLocation.StateCode);
        vars.Add("zip", mapLocation.ZipCode.Substring(0, 5));

        vars.Add("flags", "c");
        vars.Add("appid", _yahooID);

        HttpClient http = new HttpClient("http://where.yahooapis.com/geocode");
        HttpResponseMessage resp = http.Get(new Uri("http://where.yahooapis.com/geocode"), vars);
        resp.EnsureStatusIsSuccessful();

        XmlDocument doc = new XmlDocument();
        doc.Load(resp.Content.ReadAsStream());
        XmlNode latitude = doc.SelectSingleNode("/ResultSet/Result/latitude");
        XmlNode longitude = doc.SelectSingleNode("/ResultSet/Result/longitude");

        if (longitude != null && latitude != null)
        {

            mapLocation.Longitude = longitude.InnerXml;
            mapLocation.Latitude = latitude.InnerXml;
            mapLocation.Color = color;
        }
        else
        {
            _invalidLocations.Add(mapLocation);
        }
    }

    private void AddScriptsToPage(Dictionary<string, MapGroup> locationsByUser)
    {
        if (!ScriptHelper.IsClientScriptRegistered(this.Page, MappingFunction))
        {
            StringBuilder script = new StringBuilder();
            script.Append("var map = new YMap(document.getElementById('map'));\r\n");
            script.Append("function startMap() {\r\n");
            script.Append("  map.addTypeControl();\r\n");
            script.Append("  map.addZoomLong();\r\n");
            script.Append("  map.addPanControl();\r\n");

            int i = 0;
            List<String> listNames = new List<string>();
            foreach (var pair in locationsByUser)
            {
                MapGroup mapGroup = pair.Value as MapGroup;

                foreach (MapLocation mapLocation in mapGroup.MapLocations)
                {
                    if (!string.IsNullOrEmpty(mapLocation.Longitude) && !string.IsNullOrEmpty(mapLocation.Latitude))
                    {
                        string geoName = string.Format("geoPoint{0}", i);
                        string markerName = string.Format("newMarker{0}", i);
                        script.AppendFormat("  var {0} = new YGeoPoint({1}, {2});\r\n", geoName, mapLocation.Latitude, mapLocation.Longitude);
                        script.AppendFormat("  var {0} = new YMarker({1}, createCustomMarkerImage('{2}'));\r\n", markerName, geoName, mapLocation.Color);

                        script.AppendFormat("  {0}.addAutoExpand('<b>Survey #:&nbsp;</b>{1} <br>  <b>Location #:&nbsp;</b>{2} <br> <b>Insured:&nbsp;</b>{3}');\r\n", markerName, mapLocation.SurveyNumber, mapLocation.LocationNumber, mapLocation.InsuredName);

                        //script.AppendFormat("  YEvent.Capture({0}, EventsList.MouseClick, function(){{{0}.openSmartWindow({1});}});\r\n", markerName, markerupName);
                        script.AppendFormat("  map.addOverlay({0});\r\n", markerName);
                        listNames.Add(geoName);
                        i++;
                    }
                }
            }

            //function for creating the custom marker
            script.Append("  function createCustomMarkerImage(color){\r\n");
            script.Append("  var myImage = new YImage();\r\n");
            script.AppendFormat("  myImage.src = '{0}';\r\n", ARMSUtility.AppPath + "/images/colors/'" + " + color + " + "'.png");
            script.Append("  myImage.offsetSmartWindow = new YCoordPoint(15,15);\r\n");
            script.Append("  return myImage;	\r\n");
            script.Append("  }\r\n");

            //set the zoom level and where the map should be centered based on the locations
            script.AppendFormat("  var listPoints = new Array({0});\r\n", String.Join(",", listNames.ToArray()));
            script.Append("  var locations = map.getZoomLevel(listPoints);\r\n");
            script.Append("  map.drawZoomAndCenter(map.getCenterLatLon(), locations);\r\n");
            script.Append("}\r\n");
            script.Append("window.onload = startMap;");

            this.ClientScript.RegisterStartupScript(this.GetType(), MappingFunction, script.ToString(), true);
        }
    }

    private void LoadColors()
    {
        _colorList.Add(new Pair("red", "#FF0000"));
        _colorList.Add(new Pair("yellow", "#FFFF00"));
        _colorList.Add(new Pair("green", "#008000"));
        _colorList.Add(new Pair("blue", "#0000FF"));
        _colorList.Add(new Pair("purple", "#800080"));
        _colorList.Add(new Pair("orange", "#FFA500"));
        _colorList.Add(new Pair("tourquoise", "#40E0D0"));
        _colorList.Add(new Pair("olive", "#808000"));
        _colorList.Add(new Pair("teal", "#008080"));
        _colorList.Add(new Pair("lime", "#00FF00"));
        _colorList.Add(new Pair("gray", "#808080"));
        _colorList.Add(new Pair("salmon", "#FFA07A"));
        _colorList.Add(new Pair("yellowgreen", "#9ACD32"));
        _colorList.Add(new Pair("Violet", "#C71585"));
        _colorList.Add(new Pair("darkred", "#8B0000"));
        _colorList.Add(new Pair("cornsilk", "#FFF8DC"));
        _colorList.Add(new Pair("pink", "#FFC0CB"));
        _colorList.Add(new Pair("chocolate", "#D2691E"));
        _colorList.Add(new Pair("rosybrown", "#BC8F8F"));
        _colorList.Add(new Pair("steelblue", "#4682B4"));
    }

    private string BuildQuery(string surveyNumbers)
    {
        StringBuilder sql = new StringBuilder();

        sql.Append("SELECT DISTINCT ");
        sql.Append("s.SurveyNumber, ");
        sql.Append("i.Name,  ");
        sql.Append("l.LocationNumber, ");
        sql.Append("l.StreetLine1,  ");
        sql.Append("l.City,  ");
        sql.Append("l.StateCode, ");
        sql.Append("l.ZipCode, ");
        sql.Append("CASE WHEN (s.SurveyStatusCode = 'SA' OR s.SurveyStatusCode = 'SW' OR s.SurveyStatusCode = 'SR') THEN u1.Name  ");
        sql.Append("WHEN ((s.SurveyStatusCode = 'OH' OR s.SurveyStatusCode = 'SU') AND u3.Name IS NOT NULL) THEN u3.Name  ");
        sql.Append("WHEN u2.Name IS NOT NULL THEN u2.Name ELSE 'Not Assigned' END AS ConsultantUserName ");
        sql.Append("FROM Survey s ");
        sql.Append("INNER JOIN Survey_PolicyCoverage spc ON spc.SurveyID = s.SurveyID  ");
        sql.Append("INNER JOIN Location l ON l.LocationID = spc.LocationID  ");
        sql.Append("INNER JOIN Insured i ON i.InsuredID = l.InsuredID  ");
        sql.Append("LEFT JOIN dbo.[User] AS u1 ON u1.UserID = s.AssignedUserID  ");
        sql.Append("LEFT JOIN dbo.[User] AS u2 ON u2.UserID = s.ConsultantUserID  ");
        sql.Append("LEFT JOIN dbo.[User] AS u3 ON u3.UserID = s.RecommendedUserID  ");
        sql.AppendFormat("WHERE spc.Active = 1 AND LEN(l.ZipCode) >= 5 AND s.SurveyNumber IN ({0})  ", surveyNumbers);

        return sql.ToString();
    }

    private MapGroup BuildMapGroup(DataRow dr, int orderIndex)
    {
        MapGroup mapGroup = new MapGroup();
        mapGroup.UserName = Common.ReadString(dr[(int)Column.ConsultantName]);
        mapGroup.CSSColor = _colorList[orderIndex].Second.ToString();
        mapGroup.NamedColor = _colorList[orderIndex].First.ToString();
        mapGroup.OrderIndex = orderIndex + 1;

        return mapGroup;
    }

    private MapLocation BuildMapLocation(DataRow dr)
    {
        MapLocation mapLocation = new MapLocation();
        mapLocation.LocationNumber = Common.ReadString(dr[(int)Column.LocationNumber]);
        mapLocation.StreetLine1 = Common.ReadString(dr[(int)Column.LocationStreetLine1]);
        mapLocation.City = Common.ReadString(dr[(int)Column.LocationCity]);
        mapLocation.StateCode = Common.ReadString(dr[(int)Column.LocationStateCode]);
        mapLocation.ZipCode = Common.ReadString(dr[(int)Column.LocationZipCode]);
        mapLocation.SurveyNumber = Common.ReadString(dr[(int)Column.SurveyNumber]);
        mapLocation.InsuredName = Common.ReadString(dr[(int)Column.InsuredName]).Replace("'", "");

        return mapLocation;
    }

    public enum Column
    {
        SurveyNumber = 0,
        InsuredName = 1,
        LocationNumber = 2,
        LocationStreetLine1 = 3,
        LocationCity = 4,
        LocationStateCode = 5,
        LocationZipCode = 6,
        ConsultantName = 7
    }

    protected class MapGroup
    {
        private string _userName = string.Empty;
        private string _cssColor = string.Empty;
        private string _namedColor = string.Empty;
        private int _orderIndex = Int32.MinValue;
        private List<MapLocation> _mapLocation = new List<MapLocation>();

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
            }
        }

        public string CSSColor
        {
            get { return _cssColor; }
            set
            {
                _cssColor = value;
            }
        }

        public string NamedColor
        {
            get { return _namedColor; }
            set
            {
                _namedColor = value;
            }
        }

        public int OrderIndex
        {
            get { return _orderIndex; }
            set
            {
                _orderIndex = value;
            }
        }

        public List<MapLocation> MapLocations
        {
            get { return _mapLocation; }
            set
            {
                _mapLocation = value;
            }
        }
    }

    protected class MapLocation
    {
        private string _surveyNumber = string.Empty;
        private string _insuredName = string.Empty;
        private string _locationNumber = string.Empty;
        private string _streetLine1 = string.Empty;
        private string _city = string.Empty;
        private string _stateCode = string.Empty;
        private string _zipCode = string.Empty;
        private string _color = string.Empty;
        private string _longitude = string.Empty;
        private string _latitude = string.Empty;

        public string SurveyNumber
        {
            get { return _surveyNumber; }
            set
            {
                _surveyNumber = value;
            }
        }

        public string InsuredName
        {
            get { return _insuredName; }
            set
            {
                _insuredName = value;
            }
        }

        public string LocationNumber
        {
            get { return _locationNumber; }
            set
            {
                _locationNumber = value;
            }
        }

        public string StreetLine1
        {
            get { return _streetLine1; }
            set
            {
                _streetLine1 = value;
            }
        }

        public string City
        {
            get { return _city; }
            set
            {
                _city = value;
            }
        }

        public string StateCode
        {
            get { return _stateCode; }
            set
            {
                _stateCode = value;
            }
        }

        public string ZipCode
        {
            get { return _zipCode; }
            set
            {
                _zipCode = value;
            }
        }

        public string Color
        {
            get { return _color; }
            set
            {
                _color = value;
            }
        }

        public string Longitude
        {
            get { return _longitude; }
            set
            {
                _longitude = value;
            }
        }

        public string Latitude
        {
            get { return _latitude; }
            set
            {
                _latitude = value;
            }
        }
    }

}

