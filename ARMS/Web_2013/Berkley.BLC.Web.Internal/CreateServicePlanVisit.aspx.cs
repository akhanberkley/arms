using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using Wilson.ORMapper;
using QCI.Web.Validation;
using QCI.Web;
using QCI.ExceptionManagement;
using System.Collections.Generic;
using System.Linq;

public partial class CreateServicePlanVisitAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
        BuildDataGrid();
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(CreateServicePlanVisitAspx_Load);
        this.PreRender += new EventHandler(CreateServicePlanVisitAspx_PreRender);
        this.grdObjectives.SortCommand += new DataGridSortCommandEventHandler(grdObjectives_SortCommand);
        this.grdObjectives.PageIndexChanged += new DataGridPageChangedEventHandler(grdObjectives_PageIndexChanged);
        this.grdObjectives.ItemDataBound += new DataGridItemEventHandler(grdObjectives_ItemDataBound);
        this.grdObjectives.ItemCreated += new DataGridItemEventHandler(grdObjectives_ItemCreated);
        this.grdObjectives.ItemCommand += new DataGridCommandEventHandler(grdObjectives_ItemCommand);
        this.lnkAddObjective.Click += new EventHandler(lnkAddObjective_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnSaveObjective.Click += new EventHandler(btnSaveObjective_Click);
        this.grdContacts.ItemCommand += new DataGridCommandEventHandler(grdContacts_ItemCommand);
        this.grdContacts.ItemDataBound += new DataGridItemEventHandler(grdContacts_ItemDataBound);
        this.lnkAddContact.Click += new EventHandler(lnkAddContact_Click);
        this.btnAttachDocument.Click += new EventHandler(btnAttachDocument_Click);
        this.repObjectiveAttributes.ItemDataBound += new RepeaterItemEventHandler(repObjectiveAttributes_ItemDataBound);
        this.repNotes.ItemDataBound += new RepeaterItemEventHandler(repNotes_ItemDataBound);
        this.btnAddPlanObjective.Click += new EventHandler(btnAddPlanObjective_Click);
        this.grdPlanObjectives.ItemCommand += new DataGridCommandEventHandler(grdPlanObjectives_ItemCommand);
    }
    #endregion

    protected ServicePlan _eServicePlan;
    protected Survey _eSurvey;
    protected bool _usesPlanObjectives;
    protected bool _displayAttachDocs = false;
    protected bool _isCWGSurvey;

    void CreateServicePlanVisitAspx_Load(object sender, EventArgs e)
    {
        Guid gPlanID = ARMSUtility.GetGuidFromQueryString("planid", true);
        _eServicePlan = ServicePlan.Get(gPlanID);

        Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", false);
        _eSurvey = (gSurveyID != Guid.Empty) ? Survey.Get(gSurveyID) : null;
        _isCWGSurvey = (Berkley.BLC.Business.Utility.GetCompanyParameter("IsCWGSurvey", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;

        base.PageHeading = "Add Service Visit";
        this.btnSubmit.Enabled = (Objective.GetArray("SurveyID = ?", (_eSurvey != null ? _eSurvey.ID : Guid.Empty)).Length > 0 || !CurrentUser.Company.RequireServiceVisitObjective);
        this.btnSubmit.ToolTip = "At least one objective must be added before submitting.";

        if (_isCWGSurvey)
        {
            cboSurveyType.Name = "Survey Type";
            cboSurveyReasonType.Visible = true;           
        }
        else
        {
            cboSurveyType.Name = "Visit Type";
            cboSurveyReasonType.Visible = false;
        }

        if (!this.IsPostBack)
        {
            //filter
            StringBuilder sb = new StringBuilder();
            sb.Append("AccountDisabled = false && ");
            sb.Append("(Role[PermissionSet.SurveyActionPermissions[");
            sb.Append("(Setting = ? || Setting = ? || Setting = ? || Setting = ?) && ");
            sb.Append("ServiceTypeCode = ? && SurveyAction.SurveyStatusCode = ?]]) ");
            sb.Append("|| (IsFeeCompany = true && CompanyID = ?)");

            //params
            ArrayList args = new ArrayList();
            args.Add(PermissionSet.SETTING_OWNER);
            args.Add(PermissionSet.SETTING_OWNER_AND_OTHERS);
            args.Add(PermissionSet.SETTING_OWNER_AND_VENDORS);
            args.Add(PermissionSet.SETTING_ALL);
            args.Add(ServiceType.ServiceVisit.Code);
            args.Add(SurveyStatus.AssignedSurvey.Code);
            args.Add(CurrentUser.CompanyID);
            
            User[] eUsers = this.CurrentUser.Company.GetConsultants(sb.ToString(), args.ToArray());
            cboAssignedUsers.DataBind(eUsers, "ID", "Name");
                        
            // populate the survey type list with company specific types
            SurveyType[] eTypes = SurveyType.GetServiceSurveyTypes(CurrentUser.Company);
            cboSurveyType.DataBind(eTypes, "ID", "Type.Name");
            if (_isCWGSurvey)
            { 
                cboSurveyType.SelectedValue = SurveyType.GetOne("TypeCode = ? && CompanyID = ?", SurveyTypeType.Service.Code, CurrentUser.Company.ID).ID.ToString();
                cboSurveyType.Enabled = false;
                SurveyReasonType[] eReasonTypes = SurveyReasonType.GetServiceSurveyReasonTypes(CurrentUser.Company);
                cboSurveyReasonType.DataBind(eReasonTypes, "ID", "Name");
            }

            // populate the locations drop down
            Guid existingLocationID = (_eSurvey != null) ? _eSurvey.LocationID : Guid.Empty;
            List<Location> locList = new List<Location>();
            Location[] aLocations = Location.GetSortedArray("(InsuredID = ? && IsActive = true) || ID = ?", "Number ASC", _eServicePlan.InsuredID, existingLocationID);
            foreach (Location loc in aLocations) {
                locList.Add(loc);
            }
            // If eLocations is empty, and no survey location exists, insert a new location with the 
            // Insured's address to ensure the location drop down is not empty as it is required
            if (existingLocationID == Guid.Empty && locList.Count() == 0)
            {
                // Default the location value to that of the Insured's address
                //var insLocation = Location.GetOne("(InsuredID = ? && IsActive = true)", _eServicePlan.InsuredID);
                Location newLoc = new Location(new Guid());

                newLoc.InsuredID = _eServicePlan.InsuredID;
                newLoc.PolicySymbol = _eServicePlan.PrimaryPolicy.Symbol;
                newLoc.PolicySystemKey = _eServicePlan.PrimaryPolicy.PolicySystemKey;
                newLoc.Number = 1;
                newLoc.StreetLine1 = _eServicePlan.Insured.StreetLine1;
                newLoc.StreetLine2 = _eServicePlan.Insured.StreetLine2;
                newLoc.City = _eServicePlan.Insured.City;
                newLoc.StateCode = _eServicePlan.Insured.StateCode;
                newLoc.ZipCode = _eServicePlan.Insured.ZipCode;
                //newLoc.Description = "";
                newLoc.IsActive = true;

                newLoc.InsertInsuredLocation(newLoc);
                locList.Add(newLoc);
            }
            Location[] eLocations = locList.ToArray();
            cboLocations.DataBind(eLocations, "ID", "SingleLineAddressWithNumber");

            if (_eSurvey != null) // edit
            {
                this.PopulateFields(_eSurvey);
            }
            if (_eSurvey != null && _isCWGSurvey)
            {
                if (SurveyReason.GetOne("SurveyID = ?", _eSurvey.ID) != null)
                {
                    SurveyReason surveyReason = SurveyReason.GetOne("SurveyID = ?", _eSurvey.ID);
                    cboSurveyReasonType.SelectedValue = SurveyReasonType.GetOne("ID = ?", surveyReason.TypeID).ID.ToString();
                }
            }
            lnkAddPlanObjective.HRef = string.Format("ObjectiveEdit.aspx{0}{1}", ARMSUtility.GetQueryStringForNav(),"&CSP=1");

            bool _bCSP = ARMSUtility.GetStringFromQueryString("csp", false) == "1" ? true : false;

            if (_bCSP)
            {
                //get most recent objective and add to grid
            }
            
            // restore sort expression, reverse state, and page index settings
            this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", grdObjectives.Columns[0].SortExpression);
            this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", false);
            grdObjectives.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);
        }
    }

    void CreateServicePlanVisitAspx_PreRender(object sender, EventArgs e)
    {
        string sortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sortExp = sortExp.Replace(" DESC", " TEMP");
            sortExp = sortExp.Replace(" ASC", " DESC");
            sortExp = sortExp.Replace(" TEMP", " ASC");
        }
    
        // get all the objectives for this visit
        Objective[] eObjectives = Objective.GetSortedArray("SurveyID = ?", "Number ASC", (_eSurvey != null ? _eSurvey.ID : Guid.Empty));

        // get any attributes
        CompanyAttribute[] companyAttributes = AttributeHelper.GetCompanyAttributes(CurrentUser.Company, EntityType.ServiceVisitObjective);
        this.repObjectiveAttributes.DataSource = companyAttributes;
        this.repObjectiveAttributes.DataBind();

        DataTable dt = AttributeHelper.BuildObjectivesDataTable(eObjectives, companyAttributes, false);
        DataGridHelper.BindPagingGrid(grdObjectives, dt, "ObjectiveID", "There are no objectives for this service visit.");

        // get all the contacts for the selected location
        LocationContact[] eContacts = LocationContact.GetSortedArray("LocationID = ?", "PriorityIndex", (cboLocations.SelectedValue.Length > 0) ? cboLocations.SelectedValue : Guid.Empty.ToString());
        DataGridHelper.BindGrid(grdContacts, eContacts, "ID", "There are no contacts for this location.");

        // get all documents associated to this survey request
        SurveyDocument[] eDocs = SurveyDocument.GetSortedArray("SurveyID = ? && IsRemoved = False", "UploadedOn DESC", (_eSurvey != null ? _eSurvey.ID : Guid.Empty));
        repDocs.DataSource = eDocs;
        repDocs.DataBind();
        boxDocuments.Title = string.Format("Supporting Documents ({0})", eDocs.Length);

        // get all the doc types for the company
        DocType[] docTypes = DocType.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", this.CurrentUser.CompanyID);
        ComboHelper.BindCombo(cboDocType, docTypes, "Code", "{0} - {1}", "Code|Description".Split('|'));
        if (docTypes.Length > 0)
        {
            cboDocType.Items.Insert(0, new ListItem("-- select doc type --", ""));
        }

        // get all notes associated to this service plan
        bool _userCanViewAdministrativeNotes = CurrentUser.Permissions.CanViewAdministrativeNotes;

        SurveyNote[] notes = SurveyNote.GetSortedArray("SurveyID = ?", "EntryDate DESC", (_eSurvey != null ? _eSurvey.ID : Guid.Empty));

        // Filter out the Administrative Notes if the user is not pemritted to see them
        if (!_userCanViewAdministrativeNotes)
        {
            notes = notes.Where(x => x.AdminOnly != true).ToArray();
        }

        repNotes.DataSource = notes;
        repNotes.DataBind();
        boxNotes.Title = string.Format("Comments ({0})", notes.Length);

        _usesPlanObjectives = (Utility.GetCompanyParameter("UsesPlanObjectives", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _displayAttachDocs = (Utility.GetCompanyParameter("DisplayAttachDocs", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;

        if (_usesPlanObjectives)
        {
            // get Plan objectives
            Guid planId = ARMSUtility.GetGuidFromQueryString("planid", false);
            Guid surveyId = ARMSUtility.GetGuidFromQueryString("surveyid", false);
            List<Objective> planobjectives = Objective.GetSortedArray("ServicePlanID = ? && isNull(SurveyID)", "Number ASC", planId).ToList();
            List<Objective> surveyObjectives = Objective.GetSortedArray("SurveyID = ?", "Number ASC", surveyId).ToList();

            //Objective[] cd = from c in planobjectives
            //            where !(from o in surveyObjectives
            //           select o.ServicePlanID).Contains(c.ServicePlanID) select c;

            //Objective[] bb = planobjectives.Where(i => !surveyObjectives.Contains(i)).ToArray();

            //Objective[] res = planobjectives.Where(item => (surveyObjectives.Where(sp => sp.ServicePlanID == item.ServicePlanID)).ToArray();
            //List<Objective> ls = planobjectives.RemoveAll(item => surveyObjectives.Select(c => c.ServicePlanID.ToString()) = item.ServicePlanID.ToString());

            ComboHelper.BindCombo(ddlAvailablePlanObjectives, planobjectives, "ID", "Text");
            if (planobjectives != null && planobjectives.Count > 0)
            {
                ddlAvailablePlanObjectives.Items.Insert(0, new ListItem("-- select plan objective --", ""));
            }

            DataGridHelper.BindGrid(grdPlanObjectives, eObjectives, "ID", "There are no objectives for this service visit.");
            boxServicePlanObjectives.Title = string.Format("Service Visit Objectives ({0})", eObjectives.Length);
            // turn on (or show) the actions available
       
        }
    }
       
    private void BuildDataGrid()
    {
        TemplateColumn template = new TemplateColumn();
        template.HeaderText = "Number";
        template.ItemTemplate = new MyTemplate("Number", "Link");
        grdObjectives.Columns.Add(template);

        //this is where we add the dynamic attributes
        CompanyAttribute[] companyAttributes = AttributeHelper.GetCompanyAttributes(CurrentUser.Company, EntityType.ServiceVisitObjective);
        foreach (CompanyAttribute companyAttribute in companyAttributes)
        {
            if (companyAttribute.DisplayInGrid)
            {
                TemplateColumn newTemplate = new TemplateColumn();
                newTemplate.HeaderText = companyAttribute.Label;
                newTemplate.ItemTemplate = new MyTemplate(AttributeHelper.FormatLabelName(companyAttribute.Label), companyAttribute.AttributeDataTypeCode);
                grdObjectives.Columns.Add(newTemplate);
            }
        }

        TemplateColumn template5 = new TemplateColumn();
        template5.HeaderText = "Status";
        template5.ItemTemplate = new MyTemplate("Status", "T");
        grdObjectives.Columns.Add(template5);

        TemplateColumn template6 = new TemplateColumn();
        template6.ItemTemplate = new MyTemplate("", "RemoveLink");
        grdObjectives.Columns.Add(template6);
    }
    
    protected void btnRemoveContact_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this contact?");
    }

    void grdContacts_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the LocationPolicy from the policy
            LocationContact eContact = (LocationContact)e.Item.DataItem;

            //LinkButton
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkContactName");
            lnk.Attributes.Add("ContactID", eContact.ID.ToString());
        }
    }

    void repObjectiveAttributes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Berkley.BLC.Web.Internal.Controls.DynamicControlsPlaceholder pl = (Berkley.BLC.Web.Internal.Controls.DynamicControlsPlaceholder)e.Item.FindControl("pHolder");
        CompanyAttribute companyAttribute = (CompanyAttribute)e.Item.DataItem;
        Guid objectiveID = this.ViewState["ObjectiveID"] != null ? Guid.Parse(this.ViewState["ObjectiveID"].ToString()) : Guid.Empty;

        string attributeValue = string.Empty;
        if (!string.IsNullOrEmpty(companyAttribute.MappedField))
        {
            Objective objective = Objective.GetOne("ID = ?", objectiveID);
            if (companyAttribute.MappedField == "Objective.Text")
            {
                attributeValue = (objective != null) ? objective.Text : string.Empty;
            }
            else if (companyAttribute.MappedField == "Objective.CommentsText")
            {
                attributeValue = (objective != null) ? objective.CommentsText : string.Empty;
            }
            else if (companyAttribute.MappedField == "Objective.TargetDate")
            {
                attributeValue = (objective != null) ? objective.TargetDate.ToShortDateString() : dtDueDate.Date.ToShortDateString();
            }
            
        }
        else
        {
            attributeValue = AttributeHelper.GetAttributeValue(objectiveID, companyAttribute);
        }

        //set the label
        HtmlGenericControl lblAttributeLabel = (HtmlGenericControl)e.Item.FindControl("lblAttributeLabel");
        lblAttributeLabel.InnerText = companyAttribute.Label + (companyAttribute.IsRequired ? " *" : string.Empty);

        AttributeHelper.LoadWebControls(companyAttribute, attributeValue, ref pl, e.Item.ItemIndex);
    }

    protected void lnkContactName_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        Guid gContactID = new Guid(lnk.Attributes["ContactID"]);
        
        if (SaveVisit())
        {
            Response.Redirect(string.Format("Contact.aspx?planid={0}&surveyid={1}&contactid={2}", _eServicePlan.ID, _eSurvey.ID, gContactID), true);
        }
    }

    void lnkAddContact_Click(object sender, EventArgs e)
    {
        if (SaveVisit())
        {
            Response.Redirect(string.Format("Contact.aspx?planid={0}&surveyid={1}", _eServicePlan.ID, _eSurvey.ID), true);
        }
    }

    void grdContacts_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid gContactID = (Guid)grdContacts.DataKeys[e.Item.ItemIndex];

        // get the current contacts from the DB and find the index of the primary contact
        LocationContact[] eContacts = LocationContact.GetSortedArray("LocationID = ?", "PriorityIndex ASC", (cboLocations.SelectedValue.Length > 0) ? cboLocations.SelectedValue : Guid.Empty.ToString());
        int iContactIndex = int.MinValue;
        for (int i = 0; i < eContacts.Length; i++)
        {
            if (eContacts[i].ID == gContactID)
            {
                iContactIndex = i;
                break;
            }
        }
        if (iContactIndex == int.MinValue) // not found
        {
            return;
        }

        LocationContact contact = eContacts[iContactIndex];

        // execute the requested transformation on the contacts
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            ArrayList contactList = new ArrayList(eContacts);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEUP":
                    {
                        contactList.RemoveAt(iContactIndex);
                        if (iContactIndex > 0)
                        {
                            contactList.Insert(iContactIndex - 1, contact);
                        }
                        else // top item (move to bottom)
                        {
                            contactList.Add(contact);
                        }
                        break;
                    }
                case "MOVEDOWN":
                    {
                        contactList.RemoveAt(iContactIndex);
                        if (iContactIndex < eContacts.Length - 1)
                        {
                            contactList.Insert(iContactIndex + 1, contact);
                        }
                        else // bottom item (move to top)
                        {
                            contactList.Insert(0, contact);
                        }
                        break;
                    }
                case "REMOVE":
                    {
                        contactList.RemoveAt(iContactIndex);
                        eContacts[iContactIndex].Delete(trans);
                        break;
                    }
                case "":
                    {
                        return;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the contacts still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < contactList.Count; i++)
            {
                LocationContact eContactUpdate = (contactList[i] as LocationContact).GetWritableInstance();
                eContactUpdate.PriorityIndex = (i + 1);
                eContactUpdate.Save(trans);
            }

            trans.Commit();
        }
    }

    void grdObjectives_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdObjectives.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);
    }

    void grdObjectives_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sortRev);
    }

    void grdObjectives_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            Guid gObjectiveID = (Guid)grdObjectives.DataKeys[e.Item.ItemIndex];
            Objective eObjective = Objective.Get(gObjectiveID);

            // execute the requested transformation on the objectives
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                AttributeHelper.DeleteObjectiveAttributes(eObjective);
                eObjective.Delete(trans);
                trans.Commit();
            }

            Response.Redirect(string.Format("CreateServicePlanVisit.aspx?planid={0}&surveyid={1}", _eServicePlan.ID, _eSurvey.ID), true);
        }
    }

    void grdObjectives_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the LocationPolicy from the policy
            DataRowView drv = (DataRowView)e.Item.DataItem;

            //LinkButton
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkObjectiveDetails");

            //Add attribute as an id for retrieving via oncheckedchange event
            lnk.Attributes.Add("ObjectiveID", drv["ObjectiveID"].ToString());
        }
    }

    void grdObjectives_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Event Handler for LinkButton
            if (e.Item.Controls.Count > 0)
            {
                LinkButton lnk = (LinkButton)e.Item.Controls[0].FindControl("lnkObjectiveDetails");
                lnk.Click += new EventHandler(lnkObjectiveDetails_Click);
            }
        }
    }

    void lnkObjectiveDetails_Click(object sender, EventArgs e)
    {
        boxObjectiveDetails.Visible = true;
        
        LinkButton lnk = (LinkButton)sender;
        Objective eObjective = Objective.Get(new Guid(lnk.Attributes["ObjectiveID"]));

        this.ViewState["ObjectiveID"] = eObjective.ID.ToString();
        this.PopulateFields(eObjective);
    }

    void lnkAddObjective_Click(object sender, EventArgs e)
    {
        //Display the Objective Details and set the defaults
        boxObjectiveDetails.Visible = !boxObjectiveDetails.Visible;
        //dtTargetDate.Date = dtDueDate.Date;
        cboObjectiveStatus.SelectedValue = ObjectiveStatus.Open.Code;

        this.ViewState["ObjectiveID"] = Guid.Empty.ToString();
    }

    void btnSaveObjective_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        if (SaveVisit())
        {
            Response.Redirect(string.Format("CreateServicePlanVisit.aspx{0}&surveyid={1}", ARMSUtility.GetQueryStringForNav("surveyid"), _eSurvey.ID), true);
        }
    }

    void grdPlanObjectives_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdObjectives.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);
    }

    void grdPlanObjectives_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sortRev);
    }

    void grdPlanObjectives_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            Guid gObjectiveID = (Guid)grdObjectives.DataKeys[e.Item.ItemIndex];
            Objective eObjective = Objective.Get(gObjectiveID);

            // execute the requested transformation on the objectives
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                AttributeHelper.DeleteObjectiveAttributes(eObjective);
                eObjective.Delete(trans);
                trans.Commit();
            }

            Response.Redirect(string.Format("CreateServicePlanVisit.aspx?planid={0}&surveyid={1}", _eServicePlan.ID, _eSurvey.ID), true);
        }
    }

    //void grdPlanObjectives_ItemDataBound(object sender, DataGridItemEventArgs e)
    //{
    //    // we only care about datagrid items
    //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //    {
    //        // get the LocationPolicy from the policy
    //        DataRowView drv = (DataRowView)e.Item.DataItem;

    //        //LinkButton
    //        LinkButton lnk = (LinkButton)e.Item.FindControl("lnkObjectiveDetails");

    //        //Add attribute as an id for retrieving via oncheckedchange event
    //        lnk.Attributes.Add("ObjectiveID", drv["ObjectiveID"].ToString());
    //    }
    //}

    //void grdPlanObjectives_ItemCreated(object sender, DataGridItemEventArgs e)
    //{
    //    // we only care about datagrid items
    //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //    {
    //        //Event Handler for LinkButton
    //        if (e.Item.Controls.Count > 0)
    //        {
    //            LinkButton lnk = (LinkButton)e.Item.Controls[0].FindControl("lnkObjectiveDetails");
    //            lnk.Click += new EventHandler(lnkObjectiveDetails_Click);
    //        }
    //    }
    //}

    void lnkObjectivePlanDetails_Click(object sender, EventArgs e)
    {
        boxObjectiveDetails.Visible = true;

        LinkButton lnk = (LinkButton)sender;
        Objective eObjective = Objective.Get(new Guid(lnk.Attributes["ObjectiveID"]));

        this.ViewState["ObjectiveID"] = eObjective.ID.ToString();
        this.PopulateFields(eObjective);
    }

    void lnkAddPlanObjective_Click(object sender, EventArgs e)
    {
        //Display the Objective Details and set the defaults
        boxObjectiveDetails.Visible = !boxObjectiveDetails.Visible;
        //dtTargetDate.Date = dtDueDate.Date;
        cboObjectiveStatus.SelectedValue = ObjectiveStatus.Open.Code;

        this.ViewState["ObjectiveID"] = Guid.Empty.ToString();
    }

    protected void lnkDownloadDoc_OnClick(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            SurveyDocument doc = SurveyDocument.Get(new Guid(lnk.Attributes["DocumentID"]));

            StreamDocument docRetriever = new StreamDocument(doc, CurrentUser.Company);
            docRetriever.AttachDocToResponse(this);
        }
        catch (System.Web.HttpException ex)
        {
            //ignore http exceptions for when the user quits a doc download and the stream doesn't get flushed
            if (ex.Message.Contains("The remote host closed the connection"))
            {
                //swallow exception
            }
            else
            {
                throw new Exception("See inner exception for details.", ex);
            }
        }
    }

    protected void lnkRemoveDoc_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        SurveyDocument doc = SurveyDocument.Get(new Guid(lnk.Attributes["DocumentID"]));

        //try and remove the document from FileNet
        if (doc.FileNetRemovable)
        {
            ImagingHelper imaging = new ImagingHelper(CurrentUser.Company);
            imaging.RemoveFileImage(doc.FileNetReference);
        }

        // mark the doc as deleted in ARMS
        SurveyDocument removedDoc = doc.GetWritableInstance();
        removedDoc.IsRemoved = true;
        removedDoc.Save();

        JavaScript.SetInputFocus(boxDocuments);
    }

    #region ACTION: Remove Note

    void repNotes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            SurveyNote note = (SurveyNote)e.Item.DataItem;
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkRemoveNote");
            lnk.Attributes.Add("NoteID", note.ID.ToString());

            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdRemoveNote");
            td.Visible = (CurrentUser.Permissions.CanTakeAction(SurveyActionType.RemoveNote, _eSurvey, CurrentUser));

            JavaScript.AddConfirmationNoticeToWebControl(lnk, "Are your sure you want to remove this comment?");
        }
    }

    protected void lnkRemoveNote_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        SurveyNote note = SurveyNote.Get(new Guid(lnk.Attributes["NoteID"]));

        SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
        manager.RemoveNote(note);
    }

    #endregion


    void btnAttachDocument_Click(object sender, EventArgs e)
    {
        if (SaveVisit())
        {
            HttpPostedFile postedFile = fileUploader.PostedFile;

            try
            {
                // make sure a filename was uploaded
                if (postedFile == null || postedFile.FileName.Length == 0) // no filename was specified
                {
                    throw new FieldValidationException(fileUploader, "Please select a file to submit.");
                }

                // make sure the file name is not larger the max allowed
                string fileName = System.IO.Path.GetFileName(postedFile.FileName);
                if (fileName.Length > 200)
                {
                    throw new FieldValidationException(fileUploader, "The file name cannot exceed 200 characters.");
                }

                // check for an extesion
                if (System.IO.Path.GetExtension(fileName).Length <= 0)
                {
                    throw new FieldValidationException(fileUploader, "The file name must contain a file extension.");
                }

                // make sure the file has content
                int max = int.Parse(ConfigurationManager.AppSettings["MaxRequestLength"]);
                if (postedFile.ContentLength <= 0) // the file is empty
                {
                    throw new FieldValidationException(fileUploader, "The file submitted is empty.");
                }
                else if (postedFile.ContentLength > max)
                {
                    // the maximum file size is 10 Megabytes
                    throw new FieldValidationException(fileUploader, "The file size cannot be larger than 10 Megabytes.");
                }

                // check if the company has doc types and if one was selected
                if (cboDocType.Items.Count > 0 && cboDocType.SelectedValue == "" && CurrentUser.Company.RequireDocTypeOnCreateSurvey)
                {
                    throw new FieldValidationException("Please select a doc type.");
                }
            }
            catch (FieldValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }

            // attach this file to the survey
            try
            {
                DocType docType = DocType.GetOne("Code = ?", cboDocType.SelectedValue);
                
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser, true);
                manager.AttachDocument(postedFile.FileName, postedFile.ContentType, postedFile.InputStream, docType, txtDocRemarks.Text, true);
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);
                throw (ex);
            }

            JavaScript.SetInputFocus(boxDocuments);
        }
    }

    protected string GetFileNameWithSize(object oDataItem)
    {
        SurveyDocument eDoc = (SurveyDocument)oDataItem;
        string result = string.Format("{0} ({1:#,##0} KB)", eDoc.DocumentName, eDoc.SizeInBytes / 1024);

        return Server.HtmlEncode(result);
    }

    protected string GetUserName(object obj)
    {
        User user = (obj as User);
        return Server.HtmlEncode((user != null) ? user.Name : "System");
    }

    protected string GetEncodedComment(object dataItem)
    {
        string comment = (dataItem as SurveyNote).Comment;

        // html encode the string (must do this first)
        comment = Server.HtmlEncode(comment);

        // convert CR, LF, and TAB characters
        comment = comment.Replace("\r\n", "<br>");
        comment = comment.Replace("\r", "<br>");
        comment = comment.Replace("\n", "<br>");
        comment = comment.Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");

        return comment;
    }

    protected bool isCommentInternalOnly(object dataItem)
    {
        return (dataItem as SurveyNote).InternalOnly;
    }

    protected bool isCommentAdminOnly(object dataItem)
    {
        return (dataItem as SurveyNote).AdminOnly;
    }

    protected string GetCommentPrivacy(object dataItem)
    {
        string privacy = string.Empty;

        if (isCommentInternalOnly(dataItem))
        {
            privacy = "Internal Only";
        }
        else if (isCommentAdminOnly(dataItem))
        {
            privacy = "Admins Only";
        }

        return privacy;
    }

    protected string GetCommentClass(object dataItem)
    {
        string cssClass = string.Empty;

        if (isCommentInternalOnly(dataItem))
        {
            cssClass = "comment-int";
        }
        else if (isCommentAdminOnly(dataItem))
        {
            cssClass = "comment-admin";
        }

        return cssClass;
    }

    private bool SaveVisit()
    {
        try
        {
            //Commit service visit details
            Survey eSurvey;
            if (_eSurvey == null)
            {
                //determine if a submission number was used
                string submissionNumber = String.Empty;
                if (!String.IsNullOrEmpty(_eServicePlan.WorkflowSubmissionNumber))
                {
                    submissionNumber = _eServicePlan.WorkflowSubmissionNumber;
                }
                else if (_eServicePlan.PrimarySurvey != null)
                {
                    submissionNumber = _eServicePlan.PrimarySurvey.WorkflowSubmissionNumber;
                }

                eSurvey = new Survey(Guid.NewGuid());
                eSurvey.CompanyID = this.CurrentUser.CompanyID;
                eSurvey.PrimaryPolicyID = (_eServicePlan.PrimarySurvey != null) ? _eServicePlan.PrimarySurvey.PrimaryPolicyID : _eServicePlan.PrimaryPolicyID;
                eSurvey.WorkflowSubmissionNumber = submissionNumber;
                eSurvey.InsuredID = _eServicePlan.InsuredID;
                eSurvey.ServiceTypeCode = ServiceType.ServiceVisit.Code;                
                eSurvey.StatusCode = SurveyStatus.AssignedSurvey.Code;
                eSurvey.CreateStateCode = CreateState.InProgress.Code; // Default to InProgress
                eSurvey.CreateDate = DateTime.Now;
                eSurvey.CreateByInternalUserID = CurrentUser.ID;
                eSurvey.RecsRequired = true;
            } 
            else
            {
                eSurvey = _eSurvey.GetWritableInstance();
            }

            //commit changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                this.PopulateEntity(eSurvey);
                eSurvey.LocationID = (!string.IsNullOrEmpty(cboLocations.SelectedValue)) ? new Guid(cboLocations.SelectedValue) : Guid.Empty;
                eSurvey.AssignedUserID = new Guid(cboAssignedUsers.SelectedValue);
                eSurvey.Save(trans);
             
                if (_eSurvey == null)
                {
                    //tie the service plan with the visit
                    ServicePlanSurvey eServicePlanSurvey = new ServicePlanSurvey(_eServicePlan.ID, eSurvey.ID);
                    eServicePlanSurvey.Save(trans);
                }

                if (eSurvey != null && _isCWGSurvey && cboSurveyReasonType.SelectedValue != "")
                {
                    SurveyReason surveyReason;
                    if (SurveyReason.GetOne("SurveyID = ?", eSurvey.ID) != null)
                    {
                         surveyReason = SurveyReason.GetOne("SurveyID = ?", eSurvey.ID);
                        if(surveyReason.TypeID.ToString() != cboSurveyReasonType.SelectedValue.ToString())
                        { 
                         surveyReason.Delete();
                         surveyReason = new SurveyReason(new Guid(cboSurveyReasonType.SelectedValue), eSurvey.ID);
                         surveyReason.Save(trans);
                        }
                    }
                    else
                    {
                        surveyReason = new SurveyReason(new Guid(cboSurveyReasonType.SelectedValue), eSurvey.ID);
                        surveyReason.Save(trans);
                    }

                    //SurveyReason surveyReason = (SurveyReason.GetOne("SurveyID = ?", eSurvey.ID) != null) ? SurveyReason.GetOne("SurveyID = ?", eSurvey.ID) : new SurveyReason(new Guid(cboSurveyReasonType.SelectedValue), _eSurvey.ID);
                    
                    cboSurveyReasonType.SelectedValue = SurveyReasonType.GetOne("ID = ?", surveyReason.TypeID).ID.ToString();
                }

                trans.Commit();
            }

            //Add or Updates objectives
            if (boxObjectiveDetails.Visible)
            {
                Objective eObjective = Objective.GetOne("ID = ?", this.ViewState["ObjectiveID"]);
                if (eObjective == null) // add
                {
                    eObjective = new Objective(Guid.NewGuid());
                    eObjective.SurveyID = eSurvey.ID;

                    //get the highest number and incriment by one
                    Objective[] eObjectives = Objective.GetSortedArray("SurveyID = ?", "Number DESC", eSurvey.ID);
                    eObjective.Number = (eObjectives.Length > 0) ? eObjectives[0].Number + 1 : 1;
                }
                else // edit
                {
                    eObjective = eObjective.GetWritableInstance();
                }

                //this.PopulateEntity(eObjective);
                //stub out the objective details, the actuals will be populated in the SaveObjectiveAttributes method
                eObjective.Text = ".";
                eObjective.TargetDate = eSurvey.DueDate;
                eObjective.StatusCode = cboObjectiveStatus.SelectedValue;

                //commit changes
                using (Transaction trans = DB.Engine.BeginTransaction())
                {
                    eObjective.Save(trans);

                    // update any objective attributes
                    AttributeHelper.SaveObjectiveAttributes(repObjectiveAttributes, eObjective, CurrentUser, trans);
                    trans.Commit();
                }

                // update our member var so the redirect will work correctly
                this.ViewState["ObjectiveID"] = eObjective.ID.ToString();       
            }


            // Attach the comments/instructions
            // validate comments / instructions
            string comment = FieldValidation.ValidateStringField(txtNote, "Comment", 8000, false);

            // Save the instruction / comments
            if (!string.IsNullOrEmpty(comment))
            {
                using (Transaction trans = DB.Engine.BeginTransaction())
                {
                    //Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", false);
                    //_eSurvey = (gSurveyID != Guid.Empty) ? Survey.Get(gSurveyID) : null;
                    SurveyManager surveyManager = SurveyManager.GetInstance(eSurvey, this.CurrentUser);
                    surveyManager.AddNote(comment, false, false, DateTime.Now, string.Empty);

                    trans.Commit();
                }
            }
            _eSurvey = eSurvey;

        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return false;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return false;
        }

        return true;
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        if (SaveVisit())
        {
            if (_eSurvey.CreateState == CreateState.InProgress)
            {
                ServicePlanManager servicePlanManager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
                servicePlanManager.AddVisitToExistingPlan(_eSurvey);
            }

            Response.Redirect(GetReturnUrl(), true);
        }
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        return string.Format("ServicePlan.aspx{0}", ARMSUtility.GetQueryStringForNav("surveyid"));
    }

    protected void btnRemoveObjective_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this objective?");
    }

    void btnAddPlanObjective_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try   
        {
            if (SaveVisit())
            {
                if ((ddlAvailablePlanObjectives.Items.Count > 0 && ddlAvailablePlanObjectives.SelectedValue == ""))
                {
                    string strBuilder = "Please select a valid plan objective:\n";

                    throw new FieldValidationException(strBuilder);
                }

                // add the plan objective to this survey
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                manager.AddPlanObjective(ddlAvailablePlanObjectives.SelectedValue.ToString());

                Response.Redirect(string.Format("CreateServicePlanVisit.aspx{0}&surveyid={1}", ARMSUtility.GetQueryStringForNav("surveyid"), _eSurvey.ID), true);
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
    }

    private class MyTemplate : ITemplate
    {
        private string expression;
        private string dataType;
        public MyTemplate(string expression, string dataType)
        {
            this.expression = expression;
            this.dataType = dataType;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            //figure out the type of control to bind
            if (dataType == "Link")
            {
                LinkButton link = new LinkButton();
                link.ID = "lnkObjectiveDetails";
                link.DataBinding += new EventHandler(this.BindLinkButton);
                container.Controls.Add(link);
            }
            else if (dataType == "RemoveLink")
            {
                LinkButton linkRemove = new LinkButton();
                linkRemove.ID = "btnRemoveObjective";
                linkRemove.CommandName = "REMOVE";
                linkRemove.CausesValidation = false;
                linkRemove.Text = "Remove";
                container.Controls.Add(linkRemove);
            }
            else
            {
                Literal l = new Literal();
                l.DataBinding += new EventHandler(this.BindData);
                container.Controls.Add(l);
            }
        }

        public void BindData(object sender, EventArgs e)
        {
            Literal l = (Literal)sender;
            DataGridItem container = (DataGridItem)l.NamingContainer;

            switch (dataType)
            {
                case "D":
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:d}", string.Empty, "(none)");
                    break;
                case "M":
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:C}", Decimal.MinValue, "(none)");
                    break;
                case "N":
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:N0}", Int32.MinValue, "(none)");
                    break;
                default:
                    l.Text = HtmlEncodeNullable(container.DataItem, expression, string.Empty, "(none)");
                    break;
            }
        }

        public void BindLinkButton(object sender, EventArgs e)
        {
            LinkButton link = (LinkButton)sender;
            DataGridItem container = (DataGridItem)link.NamingContainer;
            link.Text = HtmlEncodeNullable(container.DataItem, expression, string.Empty, "(none)").PadLeft(3, '0');
        }

    }
}
