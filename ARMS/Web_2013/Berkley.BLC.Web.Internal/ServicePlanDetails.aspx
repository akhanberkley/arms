<%@ Page Language="C#" AutoEventWireup="false" CodeFile="ServicePlanDetails.aspx.cs" Inherits="ServicePlanDetailsAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Number" Src="~/Fields/NumberField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="ComboSearch" Src="~/Fields/ComboFieldSearch.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">

    <div class="serviceplan-insuredscreen">
    <form id="frm" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span>
    &nbsp;&nbsp;<a class="nav" href="ServicePlan.aspx<%= ARMSUtility.GetQueryStringForNav() %>"><< Back to Service Plan</a>

    <%-- // Place a table containing a drop down selection for the view type: Print, Read-Only, Edit --%>
    <div class="viewModeSelector" style="display:inline-block;margin-bottom: 10px;">&nbsp;&nbsp;<asp:dropdownlist id="cboMode" runat="server" AutoPostBack="True" CssClass="cbo"></asp:dropdownlist></div>
    
    <br />&nbsp;
    <uc:HintHeader ID="ucHintHeader" runat="server" />

    <cc:Box id="boxInsured" runat="server" title="Insured" width="725" EncodeTitle="false">
    <div id="boxInsuredContent">
        <%if (cboMode.SelectedValue == ScreenMode.EditInsured || cboMode.SelectedValue == ScreenMode.EditInsuredAndPolicy) {%>
        <table class="fields">
            <uc:Text id="txtClientID" runat="server" field="Insured.ClientID" Columns="25" IsRequired="True" Group="txt" />
            <% if (CurrentUser.Company.SupportsPortfolioID) {%>
            <uc:Text id="txtPortfolioID" runat="server" field="Insured.PortfolioID" Columns="25" IsRequired="False" Group="txt" />
            <% } %>
            <uc:Text id="txtName" runat="server" field="Insured.Name" Columns="60" IsRequired="True" Group="txt" />
            <uc:Text id="txtName2" runat="server" field="Insured.Name2" Columns="60" IsRequired="False" Group="txt" />
            <uc:Text id="txtStreetLine1" runat="server" field="Insured.StreetLine1" Name="Mailing Address 1" Columns="70" IsRequired="True" Group="txt" />
            <uc:Text id="txtStreetLine2" runat="server" field="Insured.StreetLine2" Name="Mailing Address 2" Columns="70" IsRequired="False" Group="txt" />
            <uc:Text id="txtStreetLine3" runat="server" field="Insured.StreetLine3" Name="Mailing Address 3" Columns="70" IsRequired="False" Group="txt" />
            <uc:Text id="txtCity" runat="server" field="Insured.City" IsRequired="True" Group="txt" />
            <uc:Combo id="cboState" runat="server" field="Insured.StateCode" Name="State" topItemText="" isRequired="True" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" Group="txt" />
            <uc:Text id="txtZipCode" runat="server" field="Insured.ZipCode" IsRequired="True" Group="txt" />
            <uc:Text id="txtBusinessOperations" runat="server" field="Insured.BusinessOperations" Columns="75" Rows="1" Name="Business Ops." IsRequired="True" Group="txt" />
            <% if (CurrentUser.Company.SupportsBusinessCategory) {%>
            <uc:Text id="txtBusinessCategory" runat="server" field="Insured.BusinessCategory" Columns="15" Name="Business Category" IsRequired="False" Group="txt" />
            <% } %>
            <% if (CurrentUser.Company.SupportsDOTNumber) {%>
            <uc:Text id="txtDOTNumber" runat="server" field="Insured.DOTNumber" Name="DOT Number" IsRequired="False" Group="txt" />
            <% } %>
            <uc:Text id="txtCompanyWebsite" runat="server" field="Insured.CompanyWebsite" Columns="75" Rows="1" IsRequired="False" Group="txt" />
            <% if (!_hideSICCode) {%>
            <uc:ComboSearch id="cboSIC" runat="server" field="Insured.SICCode" Name="SIC" topItemText="" isRequired="True" Group="txt" />
            <% } %>
            <% if (CurrentUser.Company.SupportsNAICS) {%>
            <uc:ComboSearch id="cboNAIC" runat="server" field="Insured.NAICSCode" Name="NAICS" topItemText="" isRequired="True" Group="txt" />
            <% } %>
            <uc:Combo id="cboUnderwriter" runat="server" field="Insured.Underwriter" Name="Underwriter" topItemText="" isRequired="True" IgnoreMapping="true" Group="txt" />            
            <%if (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplaySecUnderwriter", CurrentUser.Company).ToUpper() == "TRUE"){%>
            <uc:Combo id="cboUnderwriter2" runat="server" field="Insured.Underwriter2" Name="Underwriter2" topItemText=""  IgnoreMapping="true" Group="txt" />            
            <% } %>
            <tr>
                <td>&nbsp;</td>
                <td class="buttons">
                    <asp:Button id="btnUpdateInsured" Runat="server" CssClass="btn" Text="Save Insured Updates" />
                </td>
            </tr>
        </table>
        <%}else{%>
        <table class="fields">
            <uc:Label id="lblClientID" runat="server" field="Insured.ClientID" Group="lbl" />
            <% if (CurrentUser.Company.SupportsPortfolioID) {%>
            <uc:Label id="lblPortfolioID" runat="server" field="Insured.PortfolioID" Group="lbl" />
            <% } %>
            <uc:Label id="lblName" runat="server" field="Insured.Name" Group="lbl" />
            <uc:Label id="lblName2" runat="server" field="Insured.Name2" Group="lbl" />
            <uc:Label id="lblAddress" runat="server" field="Insured.HtmlAddress" Name="Address" Group="lbl" />
            <uc:Label id="lblBusinessOperations" runat="server" field="Insured.BusinessOperations" Group="lbl" />
            <% if (CurrentUser.Company.SupportsBusinessCategory) {%>
            <uc:Label id="lblBusinessCategory" runat="server" field="Insured.BusinessCategory" Group="lbl" />
            <% } %>
            <% if (CurrentUser.Company.SupportsDOTNumber) {%>
            <uc:Label id="lblDOTNumber" runat="server" field="Insured.DOTNumber" Name="DOT Number" Group="lbl" />
            <% } %>
            <uc:Label id="lblCompanyWebsite" runat="server" field="Insured.CompanyWebsite" Group="lbl" />
            <% if (!_hideSICCode) {%>
            <uc:Label id="lblSIC" runat="server" field="Insured.HtmlSIC" Name="SIC" Group="lbl" />
            <% } %>
            <% if (CurrentUser.Company.SupportsNAICS) {%>
            <uc:Label id="lblNAICS" runat="server" field="Insured.NAICSCode" Name="NAICS" Group="lbl" />
            <% } %>
            <uc:Label id="lblUnderwriter" runat="server" field="Insured.HtmlUnderwriterName" Name="Underwriter" Group="lbl" />
            <uc:Label id="lblUnderwriterPhone" runat="server" field="Insured.HtmlUnderwriterPhone" Name="Underwriter Phone" Group="lbl" />
            <%if (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplaySecUnderwriter", CurrentUser.Company).ToUpper() == "TRUE") {%>
             <uc:Label id="lblUnderwriter2" runat="server" field="Insured.HtmlUnderwriter2Name" Name="Underwriter2" Group="lbl" />
            <uc:Label id="lblUnderwriter2Phone" runat="server" field="Insured.HtmlUnderwriter2Phone" Name="Underwriter2 Phone" Group="lbl" />
            <%} %>
        </table>
        <%} %>
        <%--
        <table>
            <uc:Label id="lblClientID" runat="server" field="Insured.ClientID" Group="lbl" />
            <% if (CurrentUser.Company.SupportsPortfolioID) {%>
            <uc:Label id="lblPortfolioID" runat="server" field="Insured.PortfolioID" Group="lbl" />
            <% } %>
            <uc:Label id="lblName" runat="server" field="Insured.Name" Group="lbl" />
            <uc:Label id="lblName2" runat="server" field="Insured.Name2" Group="lbl" />
            <uc:Label id="lblAddress" runat="server" field="Insured.HtmlAddress" Name="Address" Group="lbl" />
            <uc:Label id="lblBusinessOperations" runat="server" field="Insured.BusinessOperations" Group="lbl" />
            <% if (CurrentUser.Company.SupportsBusinessCategory) {%>
            <uc:Label id="lblBusinessCategory" runat="server" field="Insured.BusinessCategory" Group="lbl" />
            <% } %>
            <% if (CurrentUser.Company.SupportsDOTNumber) {%>
            <uc:Label id="lblDOTNumber" runat="server" field="Insured.DOTNumber" Name="DOT Number" Group="lbl" />
            <% } %>
            <uc:Label id="lblCompanyWebsite" runat="server" field="Insured.CompanyWebsite" Group="lbl" />
            <uc:Label id="lblSIC" runat="server" field="Insured.HtmlSIC" Name="SIC" Group="lbl" />
            <% if (CurrentUser.Company.SupportsNAICS) {%>
            <uc:Label id="lblNAICS" runat="server" field="Insured.NAICSCode" Name="NAICS" Group="lbl" />
            <% } %>
            <uc:Label id="lblUnderwriter" runat="server" field="Insured.HtmlUnderwriterName" Name="Underwriter" Group="lbl" />
                <uc:Label id="lblUnderwriterPhone" runat="server" field="Insured.HtmlUnderwriterPhone" Name="Underwriter Phone" Group="lbl" />
        </table>
        --%>
    </div>
    </cc:Box>

    <cc:Box id="boxContacts" runat="server" Title="Primary Contact" width="725" EncodeTitle="false">
    <div id="boxContactsContent">
        <%if (cboMode.SelectedValue == ScreenMode.EditInsured || cboMode.SelectedValue == ScreenMode.EditInsuredAndPolicy) {%>
        <table class="fields">
            <uc:Text id="txtLocationContactName" runat="server" field="LocationContact.Name" Columns="50" IsRequired="False" Group="txt" />
            <uc:Text id="txtLocationContactTitle" runat="server" field="LocationContact.Title" Columns="40" IsRequired="False" Group="txt" />
            <uc:Text id="txtLocationContactPhone" runat="server" field="LocationContact.PhoneNumber" Columns="40" IsRequired="false" Directions="(ex. 5551234567)" Name="Phone Number" Group="txt" /> 
            <uc:Text id="txtLocationContactAltPhone" runat="server" field="LocationContact.AlternatePhoneNumber" Columns="40" IsRequired="false" Directions="(ex. 5551234567)" Name="Alt Phone Number" Group="txt" />
            <uc:Text id="txtLocationContactFax" runat="server" field="LocationContact.FaxNumber" Columns="40" IsRequired="false" Directions="(ex. 5551234567)" Name="Fax Number" Group="txt" />
            <uc:Text id="txtLocationContactEmail" runat="server" field="LocationContact.EmailAddress" Columns="40" IsRequired="false" Name="Email Address" Group="txt" />
            <uc:Text id="txtLocationStreetLine1" runat="server" field="LocationContact.StreetLine1" Columns="50" IsRequired="True" Name="Address 1" Group="txt" />
            <uc:Text id="txtLocationStreetLine2" runat="server" field="LocationContact.StreetLine2" Columns="50" IsRequired="False" Name="Address 2" Group="txt" />
            <uc:Text id="txtLocationCity" runat="server" field="LocationContact.City" IsRequired="True" Group="txt" />
            <uc:Combo id="cboLocationState" runat="server" field="LocationContact.StateCode" topItemText="" isRequired="True" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" Name="State" Group="txt" />
            <uc:Text id="txtLocationZipCode" runat="server" field="LocationContact.ZipCode" IsRequired="True" Group="txt" />
            <tr>
                <td>&nbsp;</td>
                <td class="buttons">
                    <asp:Button id="btnUpdateLocationContact" Runat="server" CssClass="btn" Text="Save Location Contact Updates" />
                </td>
            </tr>
        </table>
        <%}else{%>
        <table class="fields">
            <uc:Label id="lblLocationContactName" runat="server" field="LocationContact.Name" Group="lbl" />
            <uc:Label id="lblLocationContactTitle" runat="server" field="LocationContact.Title" Group="lbl" />
            <uc:Label id="lblLocationContactPhone" runat="server" field="LocationContact.HtmlPhoneNumberSingle" Name="Phone Number" Group="lbl" />
            <uc:Label id="lblLocationContactAltPhone" runat="server" field="LocationContact.HtmlAltPhoneNumberSingle" Name="Alt Phone Number" Group="lbl" />
            <uc:Label id="lblLocationContactFax" runat="server" field="LocationContact.HtmlFaxNumberSingle" Name="Fax Number" Group="lbl" />
            <uc:Label id="lblLocationContactEmail" runat="server" field="LocationContact.EmailAddress" Group="lbl" />
            <uc:Label id="lblLocationContactAddress" runat="server" field="LocationContact.HtmlAddress" Name="Address" Group="lbl" />
        </table>
        <%} %>
    </div>
    </cc:Box>

    <cc:Box id="boxAgency" runat="server" Title="Agency" width="725" EncodeTitle="false">
    <div id="boxAgencyContent">
        <%if (cboMode.SelectedValue == ScreenMode.EditInsured || cboMode.SelectedValue == ScreenMode.EditInsuredAndPolicy) {%>
        <table class="fields">
            <uc:Text id="txtAgencyNumber" runat="server" field="Agency.Number" IsRequired="True" Group="txt" />
            <uc:Text id="txtAgencyName" runat="server" field="Agency.Name" IsRequired="True" Group="txt" />
            <uc:Text id="txtAgencyStreetLine1" runat="server" field="Agency.StreetLine1" Name="Street Address 1" Columns="70" IsRequired="True" Group="txt" />
            <uc:Text id="txtAgencyStreetLine2" runat="server" field="Agency.StreetLine2" Name="Street Address 2" Columns="70" Group="txt" />
            <uc:Text id="txtAgencyCity" runat="server" field="Agency.City" IsRequired="True" Group="txt" />
            <uc:Combo id="cboAgencyState" runat="server" field="Agency.StateCode" Name="State" topItemText="" isRequired="True" DataType="State" DataValueField="Code" DataTextField="Name" DataOrder="Name ASC" Group="txt" />
            <uc:Text id="txtAgencyZip" runat="server" field="Agency.ZipCode" IsRequired="True" Group="txt" />
            <uc:Text id="txtAgencyPhone" runat="server" field="Agency.PhoneNumber" IsRequired="true" Directions="(ex. 5551234567)" Name="Phone Number" Group="txt" /> 
            <uc:Text id="txtAgencyFax" runat="server" field="Agency.FaxNumber" IsRequired="false" Directions="(ex. 5551234567)" Name="Fax Number" Group="txt" />
            <uc:Text id="txtAgentContactName" runat="server" field="Insured.AgentContactName" Name="Agent Contact Name" IsRequired="false" Group="txt" />
            <uc:Text id="txtAgentContactPhoneNumber" runat="server" field="Insured.AgentContactPhoneNumber" Name="Agent Phone" IsRequired="false" Directions="(ex. 5551234567)" Group="txt" />
            <uc:Combo id="cboAgentContactEmail" runat="server" field="Insured.AgentEmailID" Name="Agent Email" topItemText="-- select or add an email --" isRequired="False" Group="txt" />
            <tr id="rowLnkAddAgencyEmail">
                <td>&nbsp;</td>
                <td>
                    <a href="javascript:Toggle('rowAddAgencyEmail'); Toggle('rowLnkAddAgencyEmail');">Add Email to List</a>
                </td>
            </tr>
            <tr id="rowAddAgencyEmail" style="display: none" runat="Server">
                <td>&nbsp;</td>
                <td>
                    New Email<br />
                    <asp:TextBox ID="txtNewAgencyEmail" runat="server" Columns="60" MaxLength="100" CssClass="txt"></asp:TextBox><br /><br />
                    <asp:Button ID="btnAddEmail" Runat="server" CssClass="btn" Text="Add" CausesValidation="False" />&nbsp;
                    <input type="button" value="Cancel" class="btn" onclick="javascript: Toggle('rowAddAgencyEmail'); Toggle('rowLnkAddAgencyEmail'); document.getElementById('txtNewAgencyEmail').innerText = ''">
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="buttons">
                    <asp:Button id="btnUpdateAgency" Runat="server" CssClass="btn" Text="Save Agency Updates" />
                </td>
            </tr>
        </table>
        <%}else{%>
        <table class="fields">
            <uc:Label id="lblAgencyNumber" runat="server" field="Agency.Number" Group="lbl" />
            <uc:Label id="lblAgencyName" runat="server" field="Agency.Name" Group="lbl" />
            <uc:Label id="lblAgencyAddress" runat="server" field="Agency.HtmlAddress" Name="Agent Address" Group="lbl" />
            <uc:Label id="lblAgencyPhone" runat="server" field="Agency.HtmlPhoneNumber" Name="Phone Number" Group="lbl" />
            <uc:Label id="lblAgencyFax" runat="server" field="Agency.HtmlFaxNumber" Name="Fax Number" Group="lbl" />
            <uc:Label id="lblAgentContactName" runat="server" field="Insured.AgentContactName" Name="Agent Contact Name" Group="lbl" />
            <uc:Label id="lblAgentContactPhoneNumber" runat="server" field="Insured.AgentContactPhoneNumber" Name="Agent Phone" Group="lbl" />
            <uc:Label id="lblAgentContactEmail" runat="server" field="Insured.HtmlAgentContactEmail" Name="Agent Email" Group="lbl" />
        </table>
        <%} %>
    </div>
    </cc:Box>

    <cc:Box id="boxPolicies" runat="server" Title="Policies" width="725" EncodeTitle="false">
    <div id="boxPoliciesContent">
        <asp:datagrid ID="grdPolicies" Runat="server" Width="100%" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
            <headerstyle CssClass="header" />
            <itemstyle CssClass="item" />
            <alternatingitemstyle CssClass="altItem" />
            <footerstyle CssClass="footer" />
            <columns>
                <asp:TemplateColumn HeaderText="Policy #">
                    <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Mod">
                    <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Mod", Int32.MinValue, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Symbol">
                    <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "Symbol", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="LOB">
                    <ItemTemplate>
                        <%# HtmlEncode(Container.DataItem, "LineOfBusiness.Name") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Effective" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "EffectiveDate", "{0:d}", DateTime.MinValue, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Expiration" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "ExpireDate", "{0:d}", DateTime.MinValue, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Premium">
                    <ItemTemplate>
                    <% if (CurrentUser.Company.DisplayPolicyPremium) { %>
                        <%# HtmlEncodeNullable(Container.DataItem, "Premium", "{0:C}", Decimal.MinValue, "(none)")%>
                    <% } else { %>
                        NA
                    <% } %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Profit Center">
                    <ItemTemplate>
                        <%# GetProfitCenterName(Container.DataItem) %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Primary State">
                    <ItemTemplate>
                            <%# HtmlEncodeNullable(Container.DataItem, "StateCode", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </columns>
        </asp:datagrid>

        <%if (cboMode.SelectedValue == ScreenMode.EditPolicy || cboMode.SelectedValue == ScreenMode.EditInsuredAndPolicy) {%>
             <a href="CreateServicePlanPolicy.aspx<%= ARMSUtility.GetQueryStringForNav() %>">Add Line of Business</a>
        <%} %>
    </div>
    </cc:Box>

    <cc:Box id="boxLocations" runat="server" Title="Locations" width="725" EncodeTitle="false">
    <div id="boxLocationsContent">
        <asp:datagrid ID="grdLocations" Runat="server" Width="100%" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="25" AllowSorting="True">
            <headerstyle CssClass="header" />
            <itemstyle CssClass="item" />
            <alternatingitemstyle CssClass="altItem" />
            <footerstyle CssClass="footer" />
            <pagerstyle CssClass="pager" Mode="NumericPages" />
            <columns>
                <asp:BoundColumn HeaderText="Location&nbsp;#" DataField="Number" SortExpression="Number ASC" />
                <asp:TemplateColumn HeaderText="Address Line 1" SortExpression="StreetLine1 ASC">
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "StreetLine1", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Address Line 2" SortExpression="StreetLine2 ASC">
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "StreetLine2", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="City" SortExpression="City ASC">
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "City", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="State" SortExpression="StateCode ASC">
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "StateCode", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Zip" SortExpression="ZipCode ASC">
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "ZipCode", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Buildings">
                    <ItemTemplate>
                        <%# GetBuildingCount(Container.DataItem) %>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </columns>
        </asp:datagrid>
        <%if (cboMode.SelectedValue == ScreenMode.EditInsured || cboMode.SelectedValue == ScreenMode.EditInsuredAndPolicy) {%>
            <a href="CreateServicePlanLocation.aspx?planid=<%= _eServicePlan.ID %>">Add Location</a>
        <%}%>
    </div>
    </cc:Box>

<% if (CurrentUser.Company.ImportRateModFactors) { %>
    <cc:Box id="boxRateModFactors" runat="server" Title="Rate Modification Factors" width="725" EncodeTitle="false">
    <div id="boxRateModFactorsContent">
        <asp:datagrid ID="grdRateModFactors" Runat="server" Width="100%" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
            <headerstyle CssClass="header" />
            <itemstyle CssClass="item" />
            <alternatingitemstyle CssClass="altItem" />
            <footerstyle CssClass="footer" />
            <pagerstyle CssClass="pager" Mode="NumericPages" />
            <columns>
                <asp:TemplateColumn HeaderText="Policy #">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "Policy.Number", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Mod">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "Policy.Mod", Int32.MinValue, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="State">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "State.Name", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Description">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "Type.Description", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Rate">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "Rate", "{0:N3}", decimal.MinValue, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </columns>
        </asp:datagrid>
        
    </div>
    </cc:Box>
<% } %>

<% if (CurrentUser.Company.YearsOfLossHistory > 0) { %>
    <cc:Box id="boxClaims" runat="server" Title="Claims" width="725" EncodeTitle="false">
    
    <% if (grdClaims.Items.Count > 0) { %>
    <asp:LinkButton ID="btnExport" runat="server">Export Claims to Excel</asp:LinkButton>
    <% } %>
        
    <div id="boxClaimsContent">
        <asp:datagrid ID="grdClaims" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="10" AllowSorting="True">
            <headerstyle CssClass="header" />
            <itemstyle CssClass="item" />
            <alternatingitemstyle CssClass="altItem" />
            <footerstyle CssClass="footer" />
            <pagerstyle CssClass="pager" Mode="NumericPages" />
            <columns>
                <asp:TemplateColumn HeaderText="Policy #" SortExpression="PolicyNumber ASC">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "PolicyNumber", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Sym" SortExpression="PolicySymbol ASC">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "PolicySymbol", string.Empty, "")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Policy Exp" SortExpression="PolicyExpireDate ASC">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "PolicyExpireDate", "{0:d}", DateTime.MinValue, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Loss Date" SortExpression="LossDate ASC">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "LossDate", "{0:d}", DateTime.MinValue, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Amount" SortExpression="Amount ASC">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "Amount", "{0:C}", decimal.MinValue, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Claimant" SortExpression="Claimant ASC">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "Claimant", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Status" SortExpression="Status ASC">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "Status", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Loss Type" SortExpression="LossType ASC">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "LossType", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Claim Comment" SortExpression="Comment ASC">
                    <ItemStyle VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <%# HtmlEncodeNullable(Container.DataItem, "Comment", string.Empty, "(none)")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </columns>
        </asp:datagrid>
    </div>
    </cc:Box>
<% } %>

    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />

    </form>
    </div>
</body>
</html>
