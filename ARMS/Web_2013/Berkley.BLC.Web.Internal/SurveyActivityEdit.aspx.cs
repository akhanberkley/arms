using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web.Validation;
using QCI.Web;

public partial class SurveyActivityEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(SurveyActivityEditAspx_Load);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected SurveyActivity _eActivity;
    protected Survey _eSurvey;

    void SurveyActivityEditAspx_Load(object sender, EventArgs e)
    {
        Guid gActivityID = ARMSUtility.GetGuidFromQueryString("activityid", false);
        _eActivity = (gActivityID != Guid.Empty) ? SurveyActivity.Get(gActivityID) : null;

        Guid id = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        _eSurvey = Survey.Get(id);

        if (!this.IsPostBack)
        {
            // populate the company specific activity types
            ActivityType[] eTypes = ActivityType.GetSortedArray("CompanyID = ? && (LevelCode = ? || LevelCode = ? || LevelCode = ?) && Disabled != True", "PriorityIndex ASC", this.CurrentUser.CompanyID, ActivityTypeLevel.Survey.Code, ActivityTypeLevel.SurveyAndServicePlan.Code, ActivityTypeLevel.All.Code);
            cboActivityType.DataBind(eTypes, "ID", "Name");

            if (_eActivity != null) // edit
            {
                this.PopulateFields(_eActivity);

                //Check if the selected items exist in the list
                if (cboActivityType.Items.Contains(new ListItem(_eActivity.ActivityType.Name, _eActivity.ActivityTypeID.ToString())))
                {
                    this.PopulateFields(_eActivity.ActivityType);
                }
                else
                {
                    //add it to the list and select it
                    cboActivityType.Items.Add(new ListItem(_eActivity.ActivityType.Name, _eActivity.ActivityTypeID.ToString()));
                    cboActivityType.SelectedValue = _eActivity.ActivityTypeID.ToString();
                }
            }
            else
            {
                //default
                dtActivityDate.Date = DateTime.Today;
            }

            btnSubmit.Text = (_eActivity == null) ? "Add" : "Update";
            base.PageHeading = (_eActivity == null) ? "Add Activity Time" : "Update Activity Time";
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            SurveyActivity eActivity;
            if (_eActivity == null) // add
            {
                eActivity = new SurveyActivity(Guid.NewGuid());
            }
            else
            {
                eActivity = _eActivity.GetWritableInstance();
            }

            eActivity.SurveyID = _eSurvey.ID;
            eActivity.ActivityTypeID = new Guid(cboActivityType.SelectedValue);
            eActivity.AllocatedTo = CurrentUser.ID;
            eActivity.ActivityDate = dtActivityDate.Date;
            eActivity.ActivityHours = Convert.ToDecimal(numHours.Text);
            eActivity.Comments = txtComments.Text;

            // commit changes
            eActivity.Save();

            //Update the survey history
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
            manager.ActivityManagement(eActivity, (_eActivity == null) ? SurveyManager.UIAction.Add : SurveyManager.UIAction.Update);

            // update our member var so the redirect will work correctly
            _eActivity = eActivity;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(string.Format("SurveyActivities.aspx{0}", ARMSUtility.GetQueryStringForNav("activityid")), true);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("SurveyActivities.aspx{0}", ARMSUtility.GetQueryStringForNav("activityid")), true);
    }
}
