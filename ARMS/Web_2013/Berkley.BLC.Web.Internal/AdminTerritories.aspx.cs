using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web;

public partial class AdminTerritoriesAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminTerritoriesAspx_Load);
        this.PreRender += new EventHandler(AdminTerritoriesAspx_PreRender);
        this.cboFilter.SelectedIndexChanged += new EventHandler(cboFilter_SelectedIndexChanged);
        this.grdTerritories.PageIndexChanged += new DataGridPageChangedEventHandler(grdTerritories_PageIndexChanged);
        this.grdTerritories.ItemCommand += new DataGridCommandEventHandler(grdTerritories_ItemCommand);
    }
    #endregion

    void AdminTerritoriesAspx_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // populate the filter list
            cboFilter.Items.Add(new ListItem("Consultant Territories", SurveyStatusType.Survey.Code));
            cboFilter.Items.Add(new ListItem("Reviewer Territories", SurveyStatusType.Review.Code));

            string typeCode = ARMSUtility.GetStringFromQueryString("type", false);
            if (typeCode != null && typeCode.Length > 0)
            {
                ComboHelper.SelectComboItem(cboFilter, typeCode);
            }
            else // no value on query string
            {
                string filter = (string)this.UserIdentity.GetPageSetting("Filter", null);
                if (filter != null)
                {
                    ComboHelper.SelectComboItem(cboFilter, filter);
                    cboFilter_SelectedIndexChanged(null, null);
                }
            }
        }
    }

    void AdminTerritoriesAspx_PreRender(object sender, EventArgs e)
    {
        // get the selected filter
        string filterCode = cboFilter.SelectedValue;

        // get the selected territories
        Territory[] territories = Territory.GetSortedArray("CompanyID = ? && SurveyStatusTypeCode = ?", "Name ASC, ID ASC", this.CurrentUser.CompanyID, filterCode);
        string noRecordsMessage = "No territories are defined for this group.";

        // bind the grid
        grdTerritories.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindPagingGrid(grdTerritories, territories, "ID", noRecordsMessage);

        // update the URL for the create link
        lnkNew.NavigateUrl = string.Format("AdminTerritoryEdit.aspx?type={0}", filterCode);
    }

    protected void btnDelete_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to delete this territory definition?");
    }

    protected string GetAssignedUsersHtml(object dataItem)
    {
        Territory terra = (Territory)dataItem;

        UserTerritory[] list = UserTerritory.GetSortedArray("TerritoryID = ?", "PercentAllocated DESC, User.Name ASC", terra.ID);

        string result = null;
        foreach (UserTerritory userTerra in list)
        {
            if (result != null) result += ", ";

            if (userTerra.PercentAllocated > Int32.MinValue)
                result += string.Format("{0} ({1}%)", userTerra.User.Name, userTerra.PercentAllocated);
            else
                result += string.Format("{0} ({1} - {2})", userTerra.User.Name, userTerra.MinPremiumAmount.ToString("C0"), userTerra.MaxPremiumAmount.ToString("C0"));
        }
        if (result == null)
        {
            result = "(none)";
        }

        return Server.HtmlEncode(result);
    }

    void cboFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.UserIdentity.SavePageSetting("Filter", cboFilter.SelectedValue);
    }

    void grdTerritories_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdTerritories.CurrentPageIndex = e.NewPageIndex;
    }

    void grdTerritories_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() != "DELETE")
        {
            throw new NotSupportedException("Command of '" + e.CommandName + "' was not expected.");
        }

        // get the ID of the territory selected
        Guid id = (Guid)grdTerritories.DataKeys[e.Item.ItemIndex];

        // delete the territory (cascade delete will kill all child records)
        Territory terra = Territory.GetOne("ID = ?", id);
        if (terra != null)
        {
            terra.Delete();
        }
    }
}
