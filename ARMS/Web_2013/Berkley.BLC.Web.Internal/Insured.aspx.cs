using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Import;
using Berkley.BLC.Business;
using QCI.Web;
using QCI.Web.Validation;
using QCI.ExceptionManagement;
using Wilson.ORMapper;

public partial class InsuredAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(InsuredAspx_Load);
        this.PreRender += new EventHandler(InsuredAspx_PreRender);
        this.grdContacts.ItemCommand += new DataGridCommandEventHandler(grdContacts_ItemCommand);
        this.grdClaims.SortCommand += new DataGridSortCommandEventHandler(grdClaims_SortCommand);
        this.grdClaims.PageIndexChanged += new DataGridPageChangedEventHandler(grdClaims_PageIndexChanged);
        this.grdBuildings.SortCommand += new DataGridSortCommandEventHandler(grdBuildings_SortCommand);
        this.grdBuildings.ItemCommand += new DataGridCommandEventHandler(grdBuildings_ItemCommand);
        this.grdPolicies.ItemDataBound += new DataGridItemEventHandler(grdPolicies_ItemDataBound);
        this.grdPolicies.ItemCommand += new DataGridCommandEventHandler(grdPolicies_ItemCommand);
        this.cboMode.SelectedIndexChanged += new EventHandler(cboMode_SelectedIndexChanged);
        this.cboLocations.SelectedIndexChanged += new EventHandler(cboLocations_SelectedIndexChanged);
        this.btnAddEmail.Click += new EventHandler(btnAddEmail_Click);
        this.btnUpdateLocation.Click += new EventHandler(btnUpdateLocation_Click);
        this.btnUpdateInsured.Click += new EventHandler(btnUpdateInsured_Click);
        this.btnUpdateAgency.Click += new EventHandler(btnUpdateAgency_Click);
        this.btnRefreshData.Click += new EventHandler(btnRefreshData_Click);
        this.btnExport.Click += new EventHandler(btnExport_Click);
        this.repLocations.ItemCreated += new RepeaterItemEventHandler(repLocations_ItemCreated);
        this.chkDisplayClaims.CheckedChanged += new EventHandler(chkDisplayClaims_CheckedChanged);
        
    }
    #endregion

    #region Enum
    /// <summary>
    /// Screen Mode Types
    /// </summary>
    protected struct ScreenMode
    {
        public const string ReadOnly = "R";
        public const string EditInsured = "EI";
        public const string EditPolicy = "EP";
        public const string EditInsuredAndPolicy = "EIP";
        public const string Print = "P";
    }
    #endregion

    protected Survey _eSurvey;
    protected Location _eLocation;
    private const string claimsSortExpression = "ClaimsSortExpression";
    private const string claimsSortReversed = "ClaimsSortReversed";
    private const string buildingsSortExpression = "BuildingsSortExpression";
    private const string buildingsSortReversed = "BuildingsSortReversed";
    protected const string _claimsProspectMsg = "* Claim information is not provided on prospect requests or surveys created from prospect requests.";
    protected string _hideHazardGrade;
    protected string _hideExportClaimsToExcel;
    protected string _disclaimerMessage;

    //Parameter configurations
    protected bool _sicDisplyCodeDescription;
    protected bool _naicDisplyCodeDescription;
    protected bool _requiresNAICCode;
    protected bool _hideSICCode;
    protected bool _displaySurveyInfo;
    protected bool _displayProducer;
    protected bool _isRefreshClaims;
    void InsuredAspx_Load(object sender, EventArgs e)
    {
        Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        _eSurvey = Survey.Get(gSurveyID);
        
        // register our AJAX class for use in client script
        AjaxMethods.AjaxSurvey = _eSurvey;
        Ajax.Utility.RegisterTypeForAjax(typeof(AjaxMethods));
        txtAgencyNumber.Attributes.Add("OnChange", string.Format("GetAgency(this.value, '{0}');", this.CurrentUser.CompanyID));
        
        Guid gLocationID = ARMSUtility.GetGuidFromQueryString("locationid", false);
        _eLocation = (gLocationID != Guid.Empty) ? Location.Get(gLocationID) : _eSurvey.Location;

        //base.PageHeading = string.Format("Survey Location {0}: {1}", _eLocation.Number, _eLocation.SingleLineAddress);

        //Parameters
        _sicDisplyCodeDescription = (Berkley.BLC.Business.Utility.GetCompanyParameter("SICDisplyCodeDescription", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _naicDisplyCodeDescription = (Berkley.BLC.Business.Utility.GetCompanyParameter("NASICDisplyCodeDescription", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _requiresNAICCode = (Berkley.BLC.Business.Utility.GetCompanyParameter("RequiresNAICCode", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _hideSICCode = (Berkley.BLC.Business.Utility.GetCompanyParameter("HideSICCode", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _displaySurveyInfo = (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplaySurveyInfo", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _displayProducer = (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplayProducer", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _isRefreshClaims = (Berkley.BLC.Business.Utility.GetCompanyParameter("IsRefreshClaims", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        if (_displaySurveyInfo)
        {          
            divSurvey.Style.Add("Display", "block");
        }

        if (_displayProducer)
        {
            lblProducer.Visible = true;
            txtProducer.Visible = true;
        }
        else
        {
            lblProducer.Visible = false;
            txtProducer.Visible = false;
        }

        if (!this.IsPostBack)
        {
            PopulateViewList();

            // populate the locations
            Location[] locations = Location.GetSortedArray("SurveyPolicyCoverages[SurveyID = ?]", "Number ASC", _eSurvey.ID);
            ComboHelper.BindCombo(cboLocations, locations, "ID", "SingleLineAddressWithNumber");
            cboLocations.SelectedValue = _eLocation.ID.ToString();
            lblHeadingForPrint.Visible = !CurrentUser.Company.DefaultLocationsToOneSurvey;

            // get all the agency email addresses
            AgencyEmail[] eEmails = AgencyEmail.GetSortedArray("AgencyID = ?", "EmailAddress ASC", _eSurvey.Insured.AgencyID);
            cboAgentEmail.DataBind(eEmails, "ID", "EmailAddress");

            if (_eSurvey.Insured.AgentEmailID != Guid.Empty)
            {
                ListItem item = new ListItem(_eSurvey.Insured.AgencyEmail.EmailAddress, _eSurvey.Insured.AgentEmailID.ToString());
                if (!cboAgentEmail.Items.Contains(item))
                {
                    cboAgentEmail.Items.Add(item);
                }
            }

            // populate the sic list with all sic codes and descriptions
            if (!_hideSICCode)
            {
                SIC[] eSICCodes = SIC.GetArray(70, CurrentUser.Company.SICSortedByAlpha);
                string sicDisplayOrder = CurrentUser.Company.SICSortedByAlpha ? "Description|Code" : "Code|Description";

                if (_sicDisplyCodeDescription)
                    cboSIC.DataBind(eSICCodes, "Code", "{1} - {0}", sicDisplayOrder.Split('|'));
                else
                    cboSIC.DataBind(eSICCodes, "Code", "{0} - {1}", sicDisplayOrder.Split('|'));
                cboSIC.IsRequired = CurrentUser.Company.RequireSICCode;
            }

            // populate the NAIC list with all codes and descriptions
            NAIC[] eNAICcodes = NAIC.GetArray(70, CurrentUser.Company.NAICSortedByAlpha);
            string naicDisplayOrder = CurrentUser.Company.NAICSortedByAlpha ? "Description|Code" : "Code|Description";

            if (_naicDisplyCodeDescription)
                cboNAIC.DataBind(eNAICcodes, "Code", "{0} - {1}", naicDisplayOrder.Split('|'));
            else
                cboNAIC.DataBind(eNAICcodes, "Code", "{1} - {0}", naicDisplayOrder.Split('|'));
            cboNAIC.IsRequired = _requiresNAICCode;

            // populate the underwriter list
            Underwriter[] eUnderwriters = AppCache.GetUnderwriters(this, CurrentUser.Company, "ActiveUnderwriters", true);
            cboUnderwriter.DataBind(eUnderwriters, "Code", "Name");
            cboUnderwriter2.DataBind(eUnderwriters, "Code", "Name");
            if (!string.IsNullOrEmpty(_eSurvey.UnderwriterCode))
            {
                Underwriter underwriter = Underwriter.GetOne("Code = ? && CompanyID = ?", _eSurvey.UnderwriterCode, CurrentUser.CompanyID);
                ListItem item = new ListItem((underwriter != null) ? underwriter.Name : _eSurvey.UnderwriterCode, _eSurvey.UnderwriterCode);
                if (!cboUnderwriter.Items.Contains(item))
                {
                    cboUnderwriter.Items.Add(item);
                }
            }

            if (!string.IsNullOrEmpty(_eSurvey.Underwriter2Code))
            {
                Underwriter underwriter2 = Underwriter.GetOne("Code = ? && CompanyID = ?", _eSurvey.Underwriter2Code, CurrentUser.CompanyID);
                ListItem item = new ListItem((underwriter2 != null) ? underwriter2.Name : _eSurvey.UnderwriterCode, _eSurvey.Underwriter2Code);
                if (!cboUnderwriter2.Items.Contains(item))
                {
                    cboUnderwriter2.Items.Add(item);
                }
            }

            // restore sort expression, reverse state, and page index settings
            this.ViewState[claimsSortExpression] = this.UserIdentity.GetPageSetting(claimsSortExpression, "LossDate DESC, Policy.Number ASC");
            this.ViewState[claimsSortReversed] = this.UserIdentity.GetPageSetting(claimsSortReversed, false);
            grdClaims.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);

            this.ViewState[buildingsSortExpression] = this.UserIdentity.GetPageSetting(buildingsSortExpression, grdBuildings.Columns[0].SortExpression);
            this.ViewState[buildingsSortReversed] = this.UserIdentity.GetPageSetting(buildingsSortReversed, false);

            //determine if the valuation columns are displayed
            grdBuildings.Columns[grdBuildings.Columns.Count - 8].Visible = CurrentUser.Company.SupportsBuildingValuation;
            grdBuildings.Columns[grdBuildings.Columns.Count - 9].Visible = CurrentUser.Company.SupportsBuildingValuation;
            grdBuildings.Columns[2].HeaderText = CurrentUser.Company.BuildingSprinklerSystemLabel;
            grdBuildings.Columns[3].HeaderText = CurrentUser.Company.BuildingSprinklerSystemLabel + " Credit";

            //hardcoding this one for BLS
            grdBuildings.Columns[grdBuildings.Columns.Count - 2].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
            grdBuildings.Columns[grdBuildings.Columns.Count - 3].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
            grdBuildings.Columns[grdBuildings.Columns.Count - 4].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
            grdBuildings.Columns[grdBuildings.Columns.Count - 5].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
            grdBuildings.Columns[grdBuildings.Columns.Count - 6].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
            grdBuildings.Columns[grdBuildings.Columns.Count - 7].Visible = (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID);
            grdBuildings.Columns[1].Visible = (CurrentUser.Company.ID != Company.BerkleyLifeSciences.ID);
            grdBuildings.Columns[2].Visible = (CurrentUser.Company.ID != Company.BerkleyLifeSciences.ID);
            grdBuildings.Columns[3].Visible = (CurrentUser.Company.ID != Company.BerkleyLifeSciences.ID);

        }
    }

    void InsuredAspx_PreRender(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.cboMode.SelectedValue = this.UserIdentity.GetPageSetting("PageMode", ScreenMode.ReadOnly).ToString();
        }

        string sortClaimsExp = (string)this.ViewState[claimsSortExpression];
        if ((bool)this.ViewState[claimsSortReversed])
        {
            sortClaimsExp = sortClaimsExp.Replace(" DESC", " TEMP");
            sortClaimsExp = sortClaimsExp.Replace(" ASC", " DESC");
            sortClaimsExp = sortClaimsExp.Replace(" TEMP", " ASC");
        }

        string sortBuildingsExp = (string)this.ViewState[buildingsSortExpression];
        if ((bool)this.ViewState[buildingsSortReversed])
        {
            sortBuildingsExp = sortBuildingsExp.Replace(" DESC", " TEMP");
            sortBuildingsExp = sortBuildingsExp.Replace(" ASC", " DESC");
            sortBuildingsExp = sortBuildingsExp.Replace(" TEMP", " ASC");
        }
    
        this.PopulateFields("txt", _eLocation);
        this.PopulateFields("txt", _eSurvey.Insured);
        this.PopulateFields("txtAgent", _eSurvey.Insured);
        this.PopulateFields("lbl", _eSurvey.Insured);
        this.PopulateFields("lbl", _eSurvey);
        this.PopulateFields("txt", _eSurvey);
        this.PopulateFields("txt", _eSurvey.Insured.Agency);
        this.PopulateFields("lbl", _eSurvey.Insured.Agency);

        LocationContact[] eContacts = LocationContact.GetSortedArray("LocationID = ?", "PriorityIndex ASC", _eLocation.ID);
        Building[] eBuildings = Building.GetSortedArray("LocationID = ?", sortBuildingsExp, _eLocation.ID);
        Claim[] eClaims = Claim.GetSortedArray("Policy[InsuredID = ?]", sortClaimsExp, _eSurvey.InsuredID);

        chkDisplayClaims.Text = (_eSurvey.DisplayClaims) ? "Hide Claims" : "Display Claims";
        chkDisplayClaims.Checked = false;
        grdClaims.Visible = (CurrentUser.Company.DisplayClaimsByDefault) ? true : _eSurvey.DisplayClaims;
        repClaims.Visible = (CurrentUser.Company.DisplayClaimsByDefault) ? true : _eSurvey.DisplayClaims;

        // clear the print checkbox options for each section
        boxInsured.TitleRightCorner = string.Empty;
        boxContacts.TitleRightCorner = string.Empty;
        boxAgency.TitleRightCorner = string.Empty;
        boxPolicies.TitleRightCorner = string.Empty;
        boxPolicySummary.TitleRightCorner = string.Empty;
        boxBuildings.TitleRightCorner = string.Empty;
        boxRateModFactors.TitleRightCorner = string.Empty;
        boxClaim.TitleRightCorner = string.Empty;
        boxSurvey.TitleRightCorner = string.Empty;

        //Hide HazardGrade
        _hideHazardGrade = Berkley.BLC.Business.Utility.GetCompanyParameter("HideHazardGrade", CurrentUser.Company).ToString().ToLower();

        // Hide Export Claims to Excel
        _hideExportClaimsToExcel = Berkley.BLC.Business.Utility.GetCompanyParameter("HideExportClaimsToExcel", CurrentUser.Company).ToString().ToLower();

        // Retrive Disclaimer message
        _disclaimerMessage = Berkley.BLC.Business.Utility.GetCompanyParameter("DisclaimerMessage", CurrentUser.Company).ToString();

        if (_disclaimerMessage.Length > 0)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= 8; i++)
                sb.Append("&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp;&nbsp;");
            boxClaim.Title = "Claims " + string.Format(sb.ToString() + " Last Refreshed: {0}", GetRefreshDate().ToShortDateString());
        }

        if (cboMode.SelectedValue == ScreenMode.ReadOnly || cboMode.SelectedValue == ScreenMode.Print) //read-only & printing
        {
            //disabled editable grid
            grdPolicies.Enabled = false;
            grdContacts.Enabled = false;
            grdBuildings.Enabled = false;
            grdClaims.Enabled = false;

            Policy[] activePolicies = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && LocationID = ? && Active = true]", "Number ASC", _eSurvey.ID, _eLocation.ID);
            repPolicies.DataSource = activePolicies;
            repPolicies.DataBind();

            repContacts.DataSource = eContacts;
            repContacts.DataBind();

            string buildingTitle = (CurrentUser.Company.SupportsBuildings) ? "Locations/Buildings" : "Locations";
            boxBuildings.Title = buildingTitle + string.Format("&nbsp;&nbsp;-&nbsp;&nbsp;<a href='mapOvi.aspx?name=Survey+%23{0}&surveys={0}' target='_blank' style='color: white'>(Map All Locations)</a>", _eSurvey.Number);
            Location[] activeLocations = Location.GetSortedArray("SurveyPolicyCoverages[SurveyID = ?]", "Number ASC", _eSurvey.ID);
            repLocations.DataSource = activeLocations;
            repLocations.DataBind();

            repClaims.DataSource = eClaims;
            repClaims.DataBind();

            repPolicySummary.DataSource = activePolicies;
            repPolicySummary.DataBind();

            // populate all our static fields
            this.PopulateFields(_eSurvey);
            this.PopulateFields(_eSurvey.Status);
            this.PopulateFields(VWSurvey.GetOne("SurveyID = ?", _eSurvey.ID));

            // populate statics that could be null
            if (_eSurvey.AssignedUser != null)
                this.PopulateFields(_eSurvey.AssignedUser);
            else
                lblAssignedUser.Text = "(not specified)";

            if (_eSurvey.Type != null)
                this.PopulateFields(_eSurvey.Type.Type);
            else
                this.lblType.Text = "(not specified)";

            if (cboMode.SelectedValue == ScreenMode.Print)
            {
                // add the print checkbox options for each section
                boxInsured.TitleRightCorner = BuildPrintCheckbox("boxInsuredContent", "boxInsuredPrintMessage");
                boxContacts.TitleRightCorner = BuildPrintCheckbox("boxContactsContent", "boxContactsPrintMessage");
                boxAgency.TitleRightCorner = BuildPrintCheckbox("boxAgencyContent", "boxAgencyPrintMessage");
                boxPolicies.TitleRightCorner = BuildPrintCheckbox("boxPoliciesContent", "boxPoliciesPrintMessage");
                boxPolicySummary.TitleRightCorner = BuildPrintCheckbox("boxPolicySummaryContent", "boxPolicySummaryPrintMessage");
                boxBuildings.TitleRightCorner = BuildPrintCheckbox("boxBuildingsContent", "boxBuildingsPrintMessage");
                boxRateModFactors.TitleRightCorner = BuildPrintCheckbox("boxRateModFactorsContent", "boxRateModFactorsPrintMessage");
                boxClaim.TitleRightCorner = BuildPrintCheckbox("boxClaimsContent", "boxClaimsPrintMessage");
                boxSurvey.TitleRightCorner = BuildPrintCheckbox("boxSurveyContent", "boxSurveyPrintMessage");               
          
            }
        }
        else //editable
        {
            //enable editable grid
            grdPolicies.Enabled = true;
            grdContacts.Enabled = true;
            grdBuildings.Enabled = true;
            grdClaims.Enabled = true;

            Policy[] allPolicies = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && LocationID = ?]", "Number ASC", _eSurvey.ID, _eLocation.ID);
            DataGridHelper.BindGrid(grdPolicies, allPolicies, "ID", "There are no policies for this survey.");

            DataGridHelper.BindGrid(grdContacts, eContacts, "ID", "There are no contacts for this survey.");

            boxBuildings.Title = string.Format("Buildings for Location: {0}&nbsp;&nbsp;-&nbsp;&nbsp;<a href='{1}' target='_blank' style='color: white'>(Map It)</a>", _eLocation.SingleLineAddressWithNumber, GetMappingURL(_eLocation));
            DataGridHelper.BindGrid(grdBuildings, eBuildings, "ID", "There are no buildings for this survey.");

            grdClaims.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
            string noClaimsMsg = (_eSurvey.PrimaryPolicy != null && !SurveyTypeType.IsNotWrittenBusiness(_eSurvey.Type.Type) && CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID && String.IsNullOrEmpty(_eSurvey.WorkflowSubmissionNumber)) ? "There are no claims for this survey." : _claimsProspectMsg;
            DataGridHelper.BindPagingGrid(grdClaims, eClaims, "ID", noClaimsMsg);

            //Hide HazardGrade
            if (_hideHazardGrade == "true")
                grdPolicies.Columns[grdPolicies.Columns.Count - 8].Visible = false;    

        }

        // get all notes associated to this survey
        bool _userCanViewAdministrativeNotes = CurrentUser.Permissions.CanViewAdministrativeNotes;
        
        SurveyNote[] notes = SurveyNote.GetSortedArray("SurveyID = ?", "EntryDate DESC", _eSurvey.ID);

        // Filter out the Administrative Notes if the user is not pemritted to see them
        if (!_userCanViewAdministrativeNotes)
        {
            notes = notes.Where(x => x.AdminOnly != true).ToArray();
        }

        repNotes.DataSource = notes;
        repNotes.DataBind();

        // get all the objectives associated with the service visit, if any
        Objective[] objectives = Objective.GetSortedArray("SurveyID = ?", "TargetDate DESC", _eSurvey.ID);
        repObjectives.DataSource = objectives;
        repObjectives.DataBind();

        // get all the rate mod factors associated with this survey, if any
        RateModificationFactor[] rateModFactors = RateModificationFactor.GetSortedArray("Policy[InsuredID = ?]", "Policy.Number ASC, Policy.Mod ASC, StateCode ASC, TypeCode ASC", _eSurvey.InsuredID);
        repRateModFactors.DataSource = rateModFactors;
        repRateModFactors.DataBind();
    }

    private string BuildPrintCheckbox(string controlToToggle, string otherControlToToggle)
    {
        return string.Format("<input type=\"CheckBox\" Checked=\"true\" OnClick=\"Toggle('{0}'); Toggle('{1}');\" class=\"chk\">Print", controlToToggle, otherControlToToggle);
    }

    void cboLocations_SelectedIndexChanged(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("Insured.aspx{0}&locationid={1}", ARMSUtility.GetQueryStringForNav("locationid"), cboLocations.SelectedValue), true);
    }

    private void PopulateViewList()
    {
        bool canEditInsured = CurrentUser.Permissions.CanTakeAction(SurveyActionType.EditInsured, _eSurvey, CurrentUser);
        bool canEditPolicy = CurrentUser.Permissions.CanTakeAction(SurveyActionType.EditPolicy, _eSurvey, CurrentUser);

        ListItem[] items;
        if (canEditInsured || canEditPolicy)
        {
            string mode;
            if (canEditInsured && canEditPolicy)
            {
                mode = ScreenMode.EditInsuredAndPolicy;
            }
            else if (!canEditInsured && canEditPolicy)
            {
                mode = ScreenMode.EditPolicy;
            }
            else
            {
                mode = ScreenMode.EditInsured;
            }

            items = new ListItem[3];
            items[0] = new ListItem("Print View", ScreenMode.Print);
            items[1] = new ListItem("Read-Only View", ScreenMode.ReadOnly);
            items[2] = new ListItem("Edit View", mode);

        }
        else if (CurrentUser.Company.ID == Company.WRBerkleyCorporation.ID)
        {
            items = new ListItem[1];
            items[0] = new ListItem("Print View", ScreenMode.Print);
        }
        else
        {
            items = new ListItem[2];
            items[0] = new ListItem("Print View", ScreenMode.Print);
            items[1] = new ListItem("Read-Only View", ScreenMode.ReadOnly);
        }

        cboMode.Items.AddRange(items);
    }

    #region Mode: Read-Only

    protected string GetSinglePolicyLine(object dataItem)
    {
        Policy ePolicy = (Policy)dataItem;
        if (SurveyTypeType.IsNotWrittenBusiness(_eSurvey.Type.Type) || _eSurvey.WorkflowSubmissionNumber.Length > 0)
        {
            return string.Format("Policy: {0}", ePolicy.LineOfBusiness.Name);
        }
        else
        {
            string mod = (ePolicy.Mod != int.MinValue) ? "-" + ePolicy.Mod.ToString() : string.Empty;
            return string.Format("Policy #{0}{1}: {2}, {3}", ePolicy.Number, mod, ePolicy.LineOfBusiness.Name, ePolicy.Premium.ToString("C"));
        } 
    }

    protected Policy _currentPolicy;
    protected CustomCoverageName[] GetCoveragesForPolicy(object dataItem)
    {
        Policy ePolicy = (Policy)dataItem;
        _currentPolicy = ePolicy;

        VWExistingCoverage[] coverages;
        if (cboMode.SelectedValue != ScreenMode.Print)
        {
            coverages = VWExistingCoverage.GetSortedArray("SurveyID = ? && LocationID = ? && PolicyID = ?", "CoverageNameDisplayOrder DESC, CoverageValueDisplayOrder ASC", _eSurvey.ID, _eLocation.ID, ePolicy.ID);
        }
        else
        {
            coverages = VWExistingCoverage.GetSortedArray("SurveyID = ? && LocationID = ? && PolicyID = ? && ReportTypeCode = ?", "CoverageNameDisplayOrder DESC, CoverageValueDisplayOrder ASC", _eSurvey.ID, _eLocation.ID, ePolicy.ID, ReportType.Report.Code);
        }

        SortedList list = new SortedList();
        foreach (VWExistingCoverage coverage in coverages)
        {
            CustomCoverageName customCoverageName;
            if (!list.ContainsKey(coverage.CoverageNameDisplayOrder))
            {
                customCoverageName = new CustomCoverageName(coverage.CoverageNameTypeName, coverage.ReportTypeName);
            }
            else
            {
                customCoverageName = list[coverage.CoverageNameDisplayOrder] as CustomCoverageName;
            }

            if (!coverage.CoverageNameIsRequired)
            {
                CustomCoverage customCoverage = new CustomCoverage(coverage.CoverageID, coverage.CoverageTypeName, coverage.CoverageValue, coverage.DotNetDataType);
                customCoverageName.CustomCoverages.Add(customCoverage);
            }

            list.Remove(coverage.CoverageNameDisplayOrder);
            list.Add(coverage.CoverageNameDisplayOrder, customCoverageName);
        }

        CustomCoverageName[] results = new CustomCoverageName[list.Count];
        list.Values.CopyTo(results, 0);

        return results;
    }

    protected CustomCoverage[] GetValuesForCoverage(object dataItem)
    {
        return (dataItem as CustomCoverageName).CustomCoverages.ToArray();
    }

    private HtmlTableRow rowCoverageName;

    protected void repCoverages_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        // we only care about datagrid items
        if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) && e.Item.DataItem != null)
        {
            //Event Handler
            rowCoverageName = (HtmlTableRow)e.Item.FindControl("rowCoverageName");

            //Determine if we should gray out the non reported coverage
            CustomCoverageName customCoverageName = (CustomCoverageName)e.Item.DataItem;
            if (customCoverageName.DisplayReport.ToUpper() == "NO REPORT")
            {
                rowCoverageName.Style.Add("color", "gray");
            }

            Repeater repCoverageValues = (Repeater)e.Item.FindControl("repCoverageValues");
            repCoverageValues.ItemCreated += new RepeaterItemEventHandler(repCoverageValues_ItemCreated);
        }
    }

    void repCoverageValues_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Event Handler
            HtmlTableRow rowCoverageValue = (HtmlTableRow)e.Item.FindControl("rowCoverageValue");
            rowCoverageValue.Style.Value = rowCoverageName.Style.Value;
        }
    }

    protected string GetEncodedName(object dataItem)
    {
        string result = (dataItem as CustomCoverage).DisplayCoverageType;

        // html encode the string (must do this first)
        result = Server.HtmlEncode(result);

        // convert '|' characters
        result = result.Replace("|", "<br>");

        return result;
    }

    protected string GetEncodedValue(object dataItem)
    {
        string result = "(not specified)";
        CustomCoverage eCoverage = (dataItem as CustomCoverage);
        string sValue = eCoverage.DisplayCoverageValue;

        if (sValue != null && sValue.Length > 0)
        {
            // covert to type

            try
            {
                switch (eCoverage.DotNetDataType)
                {
                    case "DateTime":
                        sValue = Convert.ToDateTime(sValue).ToString("d");
                        break;
                    case "Decimal":
                        sValue = Convert.ToDecimal(sValue).ToString("C0");
                        break;
                    case "Int32":
                        sValue = Convert.ToInt32(sValue).ToString("N0");
                        break;
                    default:
                        //do nothing
                        break;
                }
            }
            catch (FormatException)
            {
                sValue = sValue.Trim();
            }

            // html encode the string (must do this first)
            result = Server.HtmlEncode(sValue);

            // convert '|' characters
            if (result.Contains("|"))
            {
                string[] strArray = result.Split('|');
                string strBuilder = string.Empty;
                foreach (string s in strArray)
                {
                    strBuilder += (s != null && s.Length > 0) ? s + "<br>" : "(not specified)<br>";
                }
                result = strBuilder + "<br>";
            }
        }

        return result;
    }

    protected string GetBoolLabel(string value)
    {
        Int16 i = Convert.ToInt16(value);
        string result;

        if (i == 0)
            result = "No";
        else if (i == 1)
            result = "Yes";
        else
            result = "(none)";

        return result;
    }

    protected string GetUserName(object obj)
    {
        User user = (obj as User);
        return Server.HtmlEncode((user != null) ? user.Name : _eSurvey.CreateByExternalUserName);
    }

    protected string GetProfitCenterName(object obj)
    {
        Policy policy = (obj as Policy);
        ProfitCenter pr = ProfitCenter.GetOne("Code = ? && CompanyID = ?", policy.ProfitCenter, CurrentUser.CompanyID);

        if (pr != null)
        {
            return pr.Name;
        }
        else
        {
            return "(not specified)";
        }
    }

    protected string GetEncodedComment(object dataItem)
    {
        string comment = (dataItem as SurveyNote).Comment;

        // html encode the string (must do this first)
        comment = Server.HtmlEncode(comment);

        // convert CR, LF, and TAB characters
        comment = comment.Replace("\r\n", "<br>");
        comment = comment.Replace("\r", "<br>");
        comment = comment.Replace("\n", "<br>");
        comment = comment.Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");

        return comment;
    }

    protected bool isCommentInternalOnly(object dataItem)
    {
        return (dataItem as SurveyNote).InternalOnly;
    }

    protected bool isCommentAdminOnly(object dataItem)
    {
        return (dataItem as SurveyNote).AdminOnly;
    }

    protected string GetCommentPrivacy(object dataItem)
    {
        string privacy = string.Empty;

        if (isCommentInternalOnly(dataItem))
        {
            privacy = "Internal Only";
        }
        else if (isCommentAdminOnly(dataItem))
        {
            privacy = "Admins Only";
        }

        return privacy;
    }

    protected string GetCommentClass(object dataItem)
    {
        string cssClass = string.Empty;

        if (isCommentInternalOnly(dataItem))
        {
            cssClass = "comment-int";
        }
        else if (isCommentAdminOnly(dataItem))
        {
            cssClass = "comment-admin";
        }

        return cssClass;
    }

    //Added By Vilas Meshrram: 04/15/2013 : Squish# #21360 Vehical Schedule
    protected string GetCoverageDetails(object dataItem)
    {
        CustomCoverage myCustomeCoverage = (CustomCoverage)dataItem;
        CoverageDetail[] mydetails = GetCoverageDetailsForPolicy(dataItem);

        if (mydetails.Length > 0)
        {
            StringBuilder sb = new StringBuilder();

            if (cboMode.SelectedValue != ScreenMode.Print)
            {
                sb.AppendLine("    <td class=\"label\" valign=\"top\">");

                string sLink = string.Empty;
                //sLink = string.Format("            <a class=\"lnkPolicy\" href=\"javascript:Toggle('{0}')\"> Coverage Details </a>", "row" + myCustomeCoverage.CoverageID);
                sLink = string.Format("           <img src=\"./Images/plus.png\" OnClick=\"ToggleExpandCollaspe(this,'{0}')\" />", "row" + myCustomeCoverage.CoverageID);
                sb.AppendLine(sLink);

                sb.AppendLine("    </td>");
                sb.AppendLine("<tr id=\"" + "row" + myCustomeCoverage.CoverageID + "\" runat=\"server\" style=\"DISPLAY: none\">");
            }
            else
            {
                sb.AppendLine("<tr id='rowCoverageDetails' runat='server'>");
            }

            sb.AppendLine("        <td></td>");
            sb.AppendLine("        <td>");
            sb.AppendLine("           <table");
            sb.AppendLine("             <asp:Repeater ID=\"repCoverageDetails\" runat=\"server\"  DataSource='" + mydetails + "'>");
            sb.AppendLine("                 <ItemTemplate>");
            sb.AppendLine("                     <tr id=\"rowCoverageDetailsValue\"> ");
            sb.AppendLine("                         <td> </td>");
            sb.AppendLine("                         <td class=\"lbl\" valign=\"top\"  nowrap style=\"PADDING-RIGHT: 1px\">");

            for (int i = 0; i < mydetails.Length; i++)
            {
                sb.Append(GetEncodedCoverageDetailValue(mydetails[i]) + "<br/>");
            }

            sb.AppendLine("                         </td>");
            sb.AppendLine("                     </tr>");
            sb.AppendLine("                 </ItemTemplate>");
            sb.AppendLine("             </asp:Repeater>");
            sb.AppendLine("           </table>");
            sb.AppendLine("        </td>");
            sb.AppendLine("     </tr>");

            return sb.ToString();
        }
        else
        {
            return string.Empty; 
        }


    }

    //Added By Vilas Meshrram: 04/05/2013 : Squish# 21360 Vehical Schedule
    protected CoverageDetail[] GetCoverageDetailsForPolicy(object dataItem)
    {
        CustomCoverage eCoverage = (CustomCoverage)dataItem;

        CoverageDetail[] coverageDetails = CoverageDetail.GetSortedArray("CoverageID = ?", "Value ASC", eCoverage.CoverageID);
        return coverageDetails;
    }
    
    //Added By Vilas Meshrram: 04/05/2013 : Squish# 21360 Vehical Schedule
    protected string GetEncodedCoverageDetailValue(object dataItem)
    {

        string result = (dataItem as CoverageDetail).Value;

        // html encode the string (must do this first)

        result = Server.HtmlEncode(result);

        // convert '|' characters
        result = result.Replace("|", "<br>");

        return result;
    }

    #endregion

    #region Mode: Edit

    void grdPolicies_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the location
            Policy ePolicy = (Policy)e.Item.DataItem;
            CheckBox chk = (CheckBox)e.Item.FindControl("chkPolicy");
            chk.Attributes.Add("PolicyID", ePolicy.ID.ToString());

            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkEditReports");
            lnk.Attributes.Add("PolicyID", ePolicy.ID.ToString());

            chk.Checked = SurveyPolicyCoverage.GetOne("SurveyID = ? && LocationID = ? && PolicyID = ?", _eSurvey.ID, _eLocation.ID, ePolicy.ID).Active;
        }
    }

    protected void chkPolicy_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chk = (CheckBox)sender;
        Guid gPolicyID = new Guid(chk.Attributes["PolicyID"]);

        //commit changes
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            SurveyPolicyCoverage[] coverages = SurveyPolicyCoverage.GetArray("SurveyID = ? && LocationID = ? && PolicyID = ?", _eSurvey.ID, _eLocation.ID, gPolicyID);
            for (int i = 0; i < coverages.Length; i++)
            {
                SurveyPolicyCoverage coverage = coverages[i].GetWritableInstance();
                coverage.Active = chk.Checked;
                coverage.Save(trans);
            }

            trans.Commit();
        }

        return;
    }

    protected void lnkEditReports_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        Response.Redirect(string.Format("CreateSurveyCoverage.aspx{0}&policyid={1}", ARMSUtility.GetQueryStringForNav(), lnk.Attributes["PolicyID"]), true);
    }


    protected void btnRemoveContact_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this contact?");
    }

    void grdContacts_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        Guid gContactID = (Guid)grdContacts.DataKeys[e.Item.ItemIndex];

        // get the current contacts from the DB and find the index of the primary contact
        LocationContact[] eContacts = LocationContact.GetSortedArray("LocationID = ?", "PriorityIndex ASC", _eLocation.ID);
        int iContactIndex = int.MinValue;
        for (int i = 0; i < eContacts.Length; i++)
        {
            if (eContacts[i].ID == gContactID)
            {
                iContactIndex = i;
                break;
            }
        }
        if (iContactIndex == int.MinValue) // not found
        {
            return;
        }

        LocationContact contact = eContacts[iContactIndex];

        // execute the requested transformation on the contacts
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            ArrayList contactList = new ArrayList(eContacts);
            switch (e.CommandName.ToUpper())
            {
                case "MOVEUP":
                    {
                        contactList.RemoveAt(iContactIndex);
                        if (iContactIndex > 0)
                        {
                            contactList.Insert(iContactIndex - 1, contact);
                        }
                        else // top item (move to bottom)
                        {
                            contactList.Add(contact);
                        }
                        break;
                    }
                case "MOVEDOWN":
                    {
                        contactList.RemoveAt(iContactIndex);
                        if (iContactIndex < eContacts.Length - 1)
                        {
                            contactList.Insert(iContactIndex + 1, contact);
                        }
                        else // bottom item (move to top)
                        {
                            contactList.Insert(0, contact);
                        }
                        break;
                    }
                case "REMOVE":
                    {
                        contactList.RemoveAt(iContactIndex);
                        eContacts[iContactIndex].Delete(trans);
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                    }
            }

            // save all the contacts still in our list
            // note: we are updated the priority number as we save to make sure they are always sequential from 1
            for (int i = 0; i < contactList.Count; i++)
            {
                LocationContact eContactUpdate = (contactList[i] as LocationContact).GetWritableInstance();
                eContactUpdate.PriorityIndex = (i + 1);
                eContactUpdate.Save(trans);
            }

            trans.Commit();
        }
    }

    protected Building[] GetBuildingsForLocation(object dataItem)
    {
        Location location = (Location)dataItem;
        return Building.GetSortedArray("LocationID = ?", "Number ASC", location.ID);
    }

    void grdBuildings_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState[buildingsSortExpression];
        bool sortRev = (bool)this.ViewState[buildingsSortReversed];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState[buildingsSortExpression] = e.SortExpression;
        this.ViewState[buildingsSortReversed] = sortRev;

        this.UserIdentity.SavePageSetting(buildingsSortExpression, e.SortExpression);
        this.UserIdentity.SavePageSetting(buildingsSortReversed, sortRev);

        JavaScript.SetInputFocus(boxBuildings);
    }

    protected void btnRemoveBuilding_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this building?");
    }

    void grdBuildings_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            Guid gBuildingID = (Guid)grdBuildings.DataKeys[e.Item.ItemIndex];
            Building eBuilding = Building.Get(gBuildingID);

            // execute the requested transformation on the buildings
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                eBuilding.Delete(trans);
                trans.Commit();
            }
        }

        Response.Redirect(string.Format("Insured.aspx{0}", ARMSUtility.GetQueryStringForNav()), true);
    }

    void grdClaims_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState[claimsSortExpression];
        bool sortRev = (bool)this.ViewState[claimsSortReversed];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState[claimsSortExpression] = e.SortExpression;
        this.ViewState[claimsSortReversed] = sortRev;

        this.UserIdentity.SavePageSetting(claimsSortExpression, e.SortExpression);
        this.UserIdentity.SavePageSetting(claimsSortReversed, sortRev);

        JavaScript.SetInputFocus(boxClaim);
    }

    void grdClaims_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdClaims.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);

        JavaScript.SetInputFocus(boxClaim);
    }

    void btnAddEmail_Click(object sender, EventArgs e)
    {
        //commit changes
        AgencyEmail eEmail;
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            // Insert new email
            eEmail = new AgencyEmail(Guid.NewGuid());
            eEmail.AgencyID = _eSurvey.Insured.AgencyID;
            eEmail.EmailAddress = txtNewAgencyEmail.Text;
            eEmail.Save(trans);

            // Tie the new email to the insured
            Insured eInsured = _eSurvey.Insured.GetWritableInstance();
            eInsured.AgentContactName = txtAgentContactName.Text;
            eInsured.AgentContactPhoneNumber = txtAgentContactPhone.Text;
            eInsured.AgentEmailID = eEmail.ID;
            eInsured.Save(trans);

            trans.Commit();
        }

        // Update the email list
        AgencyEmail[] eEmails = AgencyEmail.GetSortedArray("AgencyID = ?", "EmailAddress ASC", _eSurvey.Insured.AgencyID);
        cboAgentEmail.DataBind(eEmails, "ID", "EmailAddress");

        // Set the new as selected by default
        cboAgentEmail.SelectedValue = eEmail.ID.ToString();
        txtNewAgencyEmail.Text = string.Empty;

        //refresh the survey instance
        _eSurvey = Survey.Get(_eSurvey.ID);
    }

    void btnUpdateLocation_Click(object sender, EventArgs e)
    {
        try
        {
            //commit changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //save location data
                Location eLocation = _eLocation.GetWritableInstance();

                this.PopulateEntity("txt", eLocation);
                eLocation.Save(trans);

                trans.Commit();
            }
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }

        //refresh the location instance
        Response.Redirect(string.Format("Insured.aspx{0}&locationid={1}", ARMSUtility.GetQueryStringForNav("locationid"), _eLocation.ID), true);

    }

    void btnUpdateInsured_Click(object sender, EventArgs e)
    {
        try
        {
            //commit changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //save insured data
                Insured eInsured = _eSurvey.Insured.GetWritableInstance();

                if (txtName.Text.Trim().CompareTo(eInsured.Name.Trim()) != 0)
                {
                    eInsured.ExcludeName = true;
                }
                if (txtName2.Text.Trim().CompareTo(eInsured.Name2.Trim()) != 0)
                {
                    eInsured.ExcludeName2 = true;
                }
                if (txtStreetLine1.Text.Trim().CompareTo(eInsured.StreetLine1.Trim()) != 0)
                {
                    eInsured.ExcludeStreetLine1 = true;
                }
                if (txtStreetLine2.Text.Trim().CompareTo(eInsured.StreetLine2.Trim()) != 0)
                {
                    eInsured.ExcludeStreetLine2 = true;
                }
                if (txtStreetLine3.Text.Trim().CompareTo(eInsured.StreetLine3.Trim()) != 0)
                {
                    eInsured.ExcludeStreetLine3 = true;
                }
                if (txtCity.Text.CompareTo(eInsured.City.Trim()) != 0)
                {
                    eInsured.ExcludeCity = true;
                }
                if (cboState.SelectedValue.CompareTo(eInsured.StateCode) != 0)
                {
                    eInsured.ExcludeState = true;
                }
                if (txtZipCode.Text.Trim().CompareTo(eInsured.ZipCode.Trim()) != 0)
                {
                    eInsured.ExcludeZip = true;
                }
                if (txtBusinessOperations.Text.Trim().CompareTo(eInsured.BusinessOperations.Trim()) != 0)
                {
                    eInsured.ExcludeBusiness = true;
                }
                if (cboSIC.SelectedValue.CompareTo(eInsured.SICCode) != 0)
                {
                    eInsured.ExcludeSIC = true;
                }
                if (cboUnderwriter.SelectedValue.CompareTo(eInsured.Underwriter) != 0)
                {
                    eInsured.ExcludeUnderWriter = true;
                }

                //save the underwriter in both locations
                Survey writeableSurvey = _eSurvey.GetWritableInstance();
                writeableSurvey.UnderwriterCode = cboUnderwriter.SelectedValue;
                writeableSurvey.Underwriter2Code = cboUnderwriter2.SelectedValue;
                //eInsured.Underwriter = cboUnderwriter.SelectedValue;

                this.PopulateEntity("txt", eInsured);
                eInsured.Save(trans);
                writeableSurvey.Save(trans);

                trans.Commit();
            }
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }

        //refresh the survey instance
        _eSurvey = Survey.Get(_eSurvey.ID);
    }

    void btnUpdateAgency_Click(object sender, EventArgs e)
    {
        try
        {
            //commit changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //save insured data
                Insured eInsured = _eSurvey.Insured.GetWritableInstance();
                this.PopulateEntity("txtAgent", eInsured);

                //Check if agency has changed
                Agency eAgency;
                if (txtAgencyNumber.Text.Trim().Length > 0 && txtAgencyNumber.Text.Trim() != _eSurvey.Insured.Agency.Number)
                {
                    eAgency = Agency.GetOne("CompanyID = ? && (Number = ? || Number = ? || Number = ?)", this.CurrentUser.CompanyID, txtAgencyNumber.Text.PadLeft(5, '0'), txtAgencyNumber.Text.TrimStart('0'), txtAgencyNumber.Text);
                    eInsured.AgencyID = eAgency.ID;
                }
                else
                {
                    eAgency = _eSurvey.Insured.Agency;
                }

                //save agency data
                eAgency = eAgency.GetWritableInstance();
                this.PopulateEntity("txt", eAgency);

                eInsured.Save(trans);
                eAgency.Save(trans);

                trans.Commit();
            }
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
        }

        //refresh the survey instance
        _eSurvey = Survey.Get(_eSurvey.ID);

        JavaScript.SetInputFocus(boxAgency);
    }

    protected void lnkRemovePolicy_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this policy?");
    }

    void grdPolicies_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            Guid policyID = (Guid)grdPolicies.DataKeys[e.Item.ItemIndex];
            SurveyPolicyCoverage[] policyCoverages = SurveyPolicyCoverage.GetArray("SurveyID = ? && PolicyID = ?", _eSurvey.ID, policyID);

            //remove the policy reference to this survey
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                foreach (SurveyPolicyCoverage policyCoverage in policyCoverages)
                {
                    policyCoverage.Delete(trans);
                }

                trans.Commit();
            }
        }

        Response.Redirect(string.Format("Insured.aspx{0}", ARMSUtility.GetQueryStringForNav()), true);
    }

    #endregion

    void repLocations_ItemCreated(object sender, RepeaterItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Event Handler nested repeater
            Repeater repBuildings = (Repeater)e.Item.FindControl("repBuildings");
            repBuildings.ItemDataBound += new RepeaterItemEventHandler(repBuildings_ItemDataBound);
        }
    }

    void repBuildings_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater repBuildings = (Repeater)sender;
        Building[] buildings = repBuildings.DataSource as Building[];
        repBuildings.FooterTemplate = new MyTemplate(buildings.Length > 0);

        if (buildings.Length <= 0 && e.Item.ItemType != ListItemType.Footer)
        {
            e.Item.Visible = false;
        }
    }

    private class MyTemplate : ITemplate
    {
        private bool _showDefaultFooter;
        public MyTemplate(bool showDefaultFooter)
        {
            _showDefaultFooter = showDefaultFooter;
        }

        public void InstantiateIn(System.Web.UI.Control container)
        {
            Literal l = new Literal();
            if (!_showDefaultFooter)
            {
                l.Text = "<div style='PADDING-LEFT: 15px'>(no buildings)</div>";
            }
            else
            {
                l.Text = "</table>";
            }
            container.Controls.Add(l);
        }
    }

    void btnRefreshData_Click(object sender, EventArgs e)
    {
        //Run the importfactory to get the lastest insured data from the policy system
        try
        {
           string msg = Common.RefreshSurvey(_eSurvey, CurrentUser, _isRefreshClaims); 

            if (msg.Length > 0)
            {
                JavaScript.ShowMessageAndRedirect(this, msg, string.Format("Insured.aspx{0}", ARMSUtility.GetQueryStringForNav()));
            }
            else
            {
                Response.Redirect(string.Format("Insured.aspx{0}", ARMSUtility.GetQueryStringForNav()), false);
            }
        }
        catch (Berkley.BLC.Import.BlcNoInsuredException)
        {
            JavaScript.ShowMessage(this, string.Format("No refresh has occured. Policy #{0} used to create this survey is no longer in the policy system or is no longer an active policy.", _eSurvey.PrimaryPolicy.Number));
            return;
        }
        catch (FieldValidationException val)
        {
            this.ShowErrorMessage(val);
            return;
        }
        catch (Exception ex)
        {
            //ExceptionManager.Publish(ex);
            throw (ex);
        }
    }

    private string ParsePolicyNumber(string number)
    {
        if (number.Contains("-"))
        {
            int length = number.Length - number.LastIndexOf("-");
            number = number.Remove((number.Length - length), length);
        }

        return number;
    }

    protected string GetAccountStatus(Survey survey)
    {
        return (survey.Insured.NonrenewedDate == DateTime.MinValue) ? "Active" : "Non-Renewed";
    }

    //protected string GetLocationDetails(object dataItem)
    //{
    //    Location location = (Location)dataItem;

    //    //attempt to add the policy number tied to the location
    //    string policyNumber = string.Empty;
    //    if (!String.IsNullOrEmpty(location.PolicySystemKey) && location.PolicySystemKey.Split('-').Length > 1)
    //    {
    //        policyNumber = string.Format(" Policy #{0}", location.PolicySystemKey.Split('-')[1]);
    //    }

    //    string result = HtmlEncodeNullable(dataItem, "SingleLineAddressWithNumber", string.Empty, "(none)");
    //    if (location.Description.Length > 0)
    //    {
    //        result += string.Format(" - ({0})", location.Description);
    //        result += policyNumber;
    //    }

    //    return result;
    //}

    protected string GetLocationDetails(object dataItem)
    {
        Location location = (Location)dataItem;

        string result = HtmlEncodeNullable(dataItem, "SingleLineAddressWithNumber", string.Empty, "(none)");
        if (location.Description.Length > 0)
        {
            result += string.Format(" - ({0})", location.Description);
        }

        return result;
    }

    protected string GetMappingURL(object dataItem)
    {
        Location location = (Location)dataItem;
        return string.Format("http://maps.google.com?q={0}", Server.UrlPathEncode(location.SingleLineAddress));
    }

    protected DateTime GetRefreshDate()
    {
        return (_eSurvey.LastRefreshDate != DateTime.MinValue) ? _eSurvey.LastRefreshDate : _eSurvey.CreateDate;
    }

    void cboMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList cbo = (DropDownList)sender;
        this.UserIdentity.SavePageSetting("PageMode", cbo.SelectedValue);
    }
     
    protected bool CheckRefresh()
    {
        bool result = false;

        if (CurrentUser.Company.DaysToAutoRefreshSurvey != Int32.MinValue &&
            GetRefreshDate() < DateTime.Today.AddDays(-CurrentUser.Company.DaysToAutoRefreshSurvey) &&
            _eSurvey.CancelRefreshDate != DateTime.Today &&
            _eSurvey.PrimaryPolicy != null && !SurveyTypeType.IsNotWrittenBusiness(_eSurvey.Type.Type) &&
            CurrentUser.Company.ID != Company.WRBerkleyCorporation.ID && _eSurvey.Status.TypeCode != SurveyStatusType.Completed.Code && String.IsNullOrEmpty(_eSurvey.WorkflowSubmissionNumber))            
        {
            result = true;
        }

        return result;
    }

    void chkDisplayClaims_CheckedChanged(object sender, EventArgs e)
    {
        Survey survey = _eSurvey.GetWritableInstance();
        survey.DisplayClaims = !survey.DisplayClaims;
        survey.Save();

        _eSurvey = survey;
    }

    void btnExport_Click(object sender, EventArgs e)
    {
        Claim[] claims = Claim.GetSortedArray("Policy[InsuredID = ?]", this.ViewState[claimsSortExpression].ToString(), _eSurvey.InsuredID);

        //Get the columns for the CSV export
        ExportItemList oExportDataList = new ExportItemList(this);
        for (int i = 0; i < grdClaims.Columns.Count; i++)
        {
            string dataType;
            switch (i)
            {
                case 0:
                    dataType = "String";
                    break;
                case 1:
                    dataType = "String";
                    break;
                case 2:
                    dataType = "DateTime";
                    break;
                case 3:
                    dataType = "DateTime";
                    break;
                case 4:
                    dataType = "Decimal";
                    break;
                case 5:
                    dataType = "String";
                    break;
                case 6:
                    dataType = "String";
                    break;
                case 7:
                    dataType = "String";
                    break;
                case 8:
                    dataType = "String";
                    break;
                default:
                    throw new NotSupportedException("Not expecting this many columns in the claims grid.");
            }

            DataGridColumn column = grdClaims.Columns[i];
            string filterName = column.SortExpression.Replace("ASC", "").Replace("DESC", "").Trim();
            oExportDataList.Add(new ExportItem(Guid.NewGuid(), column.SortExpression, filterName, dataType, column.HeaderText, i));
        }

        // Get the CSV data
        string strCsv = oExportDataList.GetCsv(claims, oExportDataList, "No Claims Found.");

        Response.ContentType = "application/x-msexcel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=claims.csv");

        // Write out the CSV data
        System.Text.ASCIIEncoding oEncoding = new System.Text.ASCIIEncoding(); ;
        byte[] rbBytes = oEncoding.GetBytes(strCsv);
        Response.OutputStream.Write(rbBytes, 0, rbBytes.Length);

        Response.End();
    }

    protected class CustomCoverage
    {
        private string _displayCoverageType = string.Empty;
        private string _displayCoverageValue = string.Empty;
        private string _dotNetDataType = string.Empty;
        private Guid _coverageID = Guid.Empty;
        
        public CustomCoverage(Guid coverageID, string displayCoverageType, string displayCoverageValue, string dotNetDataType)
        {
            _coverageID = coverageID;
            _displayCoverageType = displayCoverageType;
            _displayCoverageValue = displayCoverageValue;
            _dotNetDataType = dotNetDataType;
        }
        
        public Guid CoverageID
        {
            get { return _coverageID; }
        }

        public string DisplayCoverageType
        {
            get { return _displayCoverageType; }
        }

        public string DisplayCoverageValue
        {
            get { return _displayCoverageValue; }
        }

        public string DotNetDataType
        {
            get { return _dotNetDataType; }
        }
    }

    protected class CustomCoverageName
    {
        private string _displayCoverageName = string.Empty;
        private string _displayReport = string.Empty;
        private List<CustomCoverage> _customCoverages = new List<CustomCoverage>();

        public CustomCoverageName(string displayCoverageName, string displayReport)
        {
            _displayCoverageName = displayCoverageName;
            _displayReport = displayReport;
        }

        public string DisplayCoverageName
        {
            get { return _displayCoverageName; }
        }

        public string DisplayReport
        {
            get { return _displayReport; }
        }

        public List<CustomCoverage> CustomCoverages
        {
            get { return _customCoverages; }
        }
    }

    public class AjaxMethods
    {
        private static Survey _survey;
        public static Survey AjaxSurvey
        {
            set
            {
                _survey = value;
            }
        }

        [Ajax.AjaxMethod()]
        public static string[] GetAgency(string agencyNumber, string companyNumber)
        {
            string[] arrResult = new string[8];

            //Validate that the value is digits only
            if (companyNumber != Company.BerkleyLifeSciences.ID.ToString() && companyNumber != Company.BerkleyOilGas.ID.ToString())
            {
                try { Convert.ToDecimal(agencyNumber); }
                catch
                {
                    arrResult = new string[1];
                    arrResult[0] = "Agency Number must be a numeric value.";

                    return arrResult;
                }

                if (agencyNumber.Contains(","))
                {
                    arrResult = new string[1];
                    arrResult[0] = "Agency Number must be a numeric value.";

                    return arrResult;
                }
            }

            //First check if the agency already exist in the ARMS db
            Agency eAgency = Agency.GetOne("CompanyID = ? && (Number = ? || Number = ? || Number = ?)", companyNumber, agencyNumber.PadLeft(5, '0'), agencyNumber.TrimStart('0'), agencyNumber);

            if (eAgency == null)
            {
                try
                {
                    //Check the policy system
                    Company eCompany = Company.Get(new Guid(companyNumber));
                    ImportFactory oFactory = new ImportFactory(eCompany);
                    Agency oAgency = oFactory.ImportAgency(agencyNumber, string.Empty); //01981 

                    ReconcileAgency oReconcileAgency = new ReconcileAgency(eCompany);
                    oReconcileAgency.Reconcile(oAgency);

                    eAgency = Agency.Get(oReconcileAgency.Entity.ID);
                }
                catch (BlcImportException ex)
                {
                    arrResult = new string[1];
                    arrResult[0] = ex.Message;
                }
            }

            if (eAgency != null)
            {
                arrResult[0] = eAgency.Name;
                arrResult[1] = eAgency.StreetLine1;
                arrResult[2] = eAgency.StreetLine2;
                arrResult[3] = eAgency.City;
                arrResult[4] = eAgency.StateCode;
                arrResult[5] = eAgency.ZipCode;
                arrResult[6] = eAgency.PhoneNumber;
                arrResult[7] = eAgency.FaxNumber;
            }

            return arrResult;
        }

        [Ajax.AjaxMethod()]
        public static void SetRefreshCancelDate()
        {
            Survey updateSurvey = _survey.GetWritableInstance();
            updateSurvey.CancelRefreshDate = DateTime.Today;
            updateSurvey.Save();
        }
    }
}
