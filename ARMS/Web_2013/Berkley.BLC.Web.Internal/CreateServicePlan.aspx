<%@ Page Language="C#" AutoEventWireup="false" CodeFile="CreateServicePlan.aspx.cs" Inherits="CreateServicePlanAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Date" Src="~/Fields/DateField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <table cellspacing="0" cellpadding="0" border="0">
    <tr>
	    <td valign="top" width="600">
        <cc:Box id="boxInsured" runat="server" Title="Insured" width="600">
            <a id="lnkInsured" href="javascript:Toggle('divInsured')">Show/Hide</a><br>
		    <div id="divInsured" style="MARGIN-TOP: 5px">
                <table class="fields">
                    <% if (CurrentUser.Company.SupportsPortfolioID) {%>
                    <uc:Label id="lblPortfolioID" runat="server" field="Insured.PortfolioID" />
                    <% } %>
	                <uc:Label id="lblClientID" runat="server" field="Insured.ClientID" />
	                <uc:Label id="lblName" runat="server" field="Insured.Name" />
	                <uc:Label id="lblName2" runat="server" field="Insured.Name2" />
	                <uc:Label id="lblAddress" runat="server" field="Insured.HtmlAddress" Name="Address" />
	                <uc:Label id="lblBusinessOperations" runat="server" field="Insured.BusinessOperations" />
	                <% if (CurrentUser.Company.SupportsBusinessCategory) {%>
	                <uc:Label id="lblBusinessCategory" runat="server" field="Insured.BusinessCategory" />
	                <% } %>
	                <% if (CurrentUser.Company.SupportsDOTNumber) {%>
                    <uc:Label id="lblDOTNumber" runat="server" field="Insured.DOTNumber" Name="DOT Number" />
                    <% } %>
                    <uc:Label id="lblCompanyWebsite" runat="server" field="Insured.CompanyWebsite" />
	                <uc:Label id="lblSIC" runat="server" field="Insured.HtmlSIC" Name="SIC" />
	                <% if (CurrentUser.Company.SupportsNAICS) {%>
	                <uc:Label id="lblNAICS" runat="server" field="Insured.NAICSCode" Name="NAICS" />
	                <% } %>
	                <uc:Label id="lblUnderwriter" runat="server" field="Insured.HtmlUnderwriterName" Name="Underwriter" />
	                <uc:Label id="lblUnderwriterPhone" runat="server" field="Insured.HtmlUnderwriterPhone" Name="Underwriter Phone" />
	            </table>
	        </div>
        </cc:Box>
        
        <cc:Box id="boxContacts" runat="server" Title="Primary Contact" width="600">
            <a id="lnkContact" href="javascript:Toggle('divInsured')">Show/Hide</a><br>
		    <div id="divContact" style="MARGIN-TOP: 5px">
                <table class="fields">
	                <uc:Label id="lblContactName" runat="server" field="LocationContact.Name" />
	                <uc:Label id="lblContactTitle" runat="server" field="LocationContact.Title" />
	                <uc:Label id="lblContactPhone" runat="server" field="LocationContact.HtmlPhoneNumberSingle" Name="Phone Number" />
	                <uc:Label id="lblContactAltPhone" runat="server" field="LocationContact.HtmlAltPhoneNumberSingle" Name="Alt Phone Number" />
	                <uc:Label id="lblContactFax" runat="server" field="LocationContact.HtmlFaxNumberSingle" Name="Fax Number" />
	                <uc:Label id="lblContactEmail" runat="server" field="LocationContact.EmailAddress" />
	                <uc:Label id="lblContactAddress" runat="server" field="LocationContact.HtmlAddress" Name="Address" />
	            </table>
	        </div>
        </cc:Box>
        
        <cc:Box id="boxAgency" runat="server" Title="Agency" width="600">
            <a id="lnkAgency" href="javascript:Toggle('divAgency')">Show/Hide</a><br>
		    <div id="divAgency" style="MARGIN-TOP: 5px">
                <table class="fields">
                    <uc:Label id="lblAgencyNumber" runat="server" field="Agency.Number" />
	                <uc:Label id="lblAgencyName" runat="server" field="Agency.Name" />
	                <uc:Label id="lblAgencyAddress" runat="server" field="Agency.HtmlAddress" Name="Address" />
	                <uc:Label id="lblAgencyPhone" runat="server" field="Agency.HtmlPhoneNumber" Name="Phone Number" />
	                <uc:Label id="lblAgencyFax" runat="server" field="Agency.HtmlFaxNumber" Name="Fax Number" />
	                <uc:Text id="txtAgentContactName" runat="server" field="Insured.AgentContactName" Name="Agent Name" IsRequired="False" />
	                <uc:Text id="txtAgentContactPhone" runat="server" field="Insured.AgentContactPhoneNumber" Name="Agent Phone" IsRequired="False" />
	                <uc:Combo id="cboAgentEmail" runat="server" field="Insured.AgentEmailID" Name="Agent Email" topItemText="-- select or add email --" isRequired="False" />
	                <tr id="rowLnkAddAgencyEmail">
		                <td>&nbsp;</td>
		                <td>
			                <a href="javascript:Toggle('rowAddAgencyEmail'); Toggle('rowLnkAddAgencyEmail');">Add Email to List</a>
		                </td>
	                </tr>
	                <tr id="rowAddAgencyEmail" style="DISPLAY: none" runat="Server">
					    <td>&nbsp;</td>
					    <td>
					        New Email<br />
					        <asp:TextBox ID="txtNewAgencyEmail" runat="server" Columns="60" MaxLength="100" CssClass="txt"></asp:TextBox><br /><br />
						    <asp:Button ID="btnAddEmail" Runat="server" CssClass="btn" Text="Add" CausesValidation="False" />&nbsp;
                            <input type="button" value="Cancel" class="btn" onclick="javascript:Toggle('rowAddAgencyEmail'); Toggle('rowLnkAddAgencyEmail'); document.getElementById('txtNewAgencyEmail').innerText = ''">
					    </td>
				    </tr>
	            </table>
	        </div>
        </cc:Box>
        
        <cc:Box id="boxPolicies" runat="server" Title="Policies" width="600">
            <asp:datagrid ID="grdPolicies" Runat="server" Width="100%" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
                <headerstyle CssClass="header" />
                <itemstyle CssClass="item" />
                <alternatingitemstyle CssClass="altItem" />
                <footerstyle CssClass="footer" />
                <columns>
                    <asp:TemplateColumn HeaderText="Policy #">
		                <ItemTemplate>
				                <%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(none)")%>
		                </ItemTemplate>
	                </asp:TemplateColumn>
	                <asp:TemplateColumn HeaderText="Mod">
		                <ItemTemplate>
				                <%# HtmlEncodeNullable(Container.DataItem, "Mod", Int32.MinValue, "(none)")%>
		                </ItemTemplate>
	                </asp:TemplateColumn>
	                <asp:TemplateColumn HeaderText="Symbol">
		                <ItemTemplate>
				                <%# HtmlEncodeNullable(Container.DataItem, "Symbol", string.Empty, "(none)")%>
		                </ItemTemplate>
	                </asp:TemplateColumn>
	                <asp:TemplateColumn HeaderText="LOB">
		                <ItemTemplate>
                            <%# HtmlEncode(Container.DataItem, "LineOfBusiness.Name") %>
		                </ItemTemplate>
	                </asp:TemplateColumn>
	                <asp:TemplateColumn HeaderText="Hazard Grade">
			            <ItemTemplate>
					            <%# HtmlEncodeNullable(Container.DataItem, "HazardGrade", string.Empty, "(none)")%>
			            </ItemTemplate>
		            </asp:TemplateColumn>
	                <asp:TemplateColumn HeaderText="Effective" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
	                        <%# HtmlEncodeNullable(Container.DataItem, "EffectiveDate", "{0:d}", DateTime.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
	                <asp:TemplateColumn HeaderText="Expiration" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
	                        <%# HtmlEncodeNullable(Container.DataItem, "ExpireDate", "{0:d}", DateTime.MinValue, "(none)")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
	                <asp:TemplateColumn HeaderText="Premium">
		                <ItemTemplate>
                        <% if (CurrentUser.Company.DisplayPolicyPremium) { %>
			                <%# HtmlEncodeNullable(Container.DataItem, "Premium", "{0:C}", Decimal.MinValue, "(none)")%>
                        <% } else { %>
                            NA
                        <% } %>
		                </ItemTemplate>
	                </asp:TemplateColumn>
	                <asp:TemplateColumn HeaderText="Profit Center">
		                <ItemTemplate>
			                <%# GetProfitCenterName(Container.DataItem) %>
		                </ItemTemplate>
	                </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Primary State">
		                <ItemTemplate>
			                <%# HtmlEncodeNullable(Container.DataItem, "StateCode", string.Empty, "(none)")%>
		                </ItemTemplate>
	                </asp:TemplateColumn>
                </columns>
            </asp:datagrid>
        </cc:Box>
        
        <cc:Box id="boxLocations" runat="server" Title="Locations" width="600">
            <asp:datagrid ID="grdLocations" Runat="server" Width="100%" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="25" AllowSorting="True">
	            <headerstyle CssClass="header" />
	            <itemstyle CssClass="item" />
	            <alternatingitemstyle CssClass="altItem" />
	            <footerstyle CssClass="footer" />
	            <pagerstyle CssClass="pager" Mode="NumericPages" />
	            <columns>
		            <asp:BoundColumn HeaderText="Location&nbsp;#" DataField="Number" SortExpression="Number ASC" />
		            <asp:TemplateColumn HeaderText="Address Line 1" SortExpression="StreetLine1 ASC">
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "StreetLine1", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Address Line 2" SortExpression="StreetLine2 ASC">
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "StreetLine2", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
		            <asp:TemplateColumn HeaderText="City" SortExpression="City ASC">
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "City", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
		            <asp:TemplateColumn HeaderText="State" SortExpression="StateCode ASC">
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "StateCode", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Zip" SortExpression="ZipCode ASC">
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "ZipCode", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Buildings">
	                    <ItemTemplate>
		                    <%# GetBuildingCount(Container.DataItem) %>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
	            </columns>
            </asp:datagrid>
            <a href="CreateServicePlanLocation.aspx?planid=<%= _eServicePlan.ID %>">Add Location</a>
        </cc:Box>
        
        <% if (CurrentUser.Company.ImportRateModFactors) { %>
        <cc:Box id="boxRateModFactors" runat="server" title="Rate Modification Factors" width="600">
            <asp:datagrid ID="grdRateModFactors" Runat="server" Width="100%" CssClass="grid" AutoGenerateColumns="False" AllowPaging="False" AllowSorting="False">
	            <headerstyle CssClass="header" />
	            <itemstyle CssClass="item" />
	            <alternatingitemstyle CssClass="altItem" />
	            <footerstyle CssClass="footer" />
	            <pagerstyle CssClass="pager" Mode="NumericPages" />
	            <columns>
                    <asp:TemplateColumn HeaderText="Policy #">
	                    <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "Policy.Number", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Mod">
	                    <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "Policy.Mod", Int32.MinValue, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="State">
	                    <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "State.Name", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Description">
	                    <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "Type.Description", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Rate">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "Rate", "{0:N3}", decimal.MinValue, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
	            </columns>
            </asp:datagrid>
        </cc:Box>
        <% } %>
        
        <% if (CurrentUser.Company.YearsOfLossHistory > 0) { %>
        <cc:Box id="boxClaims" runat="server" title="Claims" width="600">
            <% if (grdClaims.Items.Count > 0) { %>
            <asp:LinkButton ID="btnExport" runat="server">Export Claims to Excel</asp:LinkButton>
            <% } %>
            
            <asp:datagrid ID="grdClaims" Runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="10" AllowSorting="True">
	            <headerstyle CssClass="header" />
	            <itemstyle CssClass="item" />
	            <alternatingitemstyle CssClass="altItem" />
	            <footerstyle CssClass="footer" />
	            <pagerstyle CssClass="pager" Mode="NumericPages" />
	            <columns>
                    <asp:TemplateColumn HeaderText="Policy #" SortExpression="PolicyNumber ASC">
	                    <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "PolicyNumber", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Sym" SortExpression="PolicySymbol ASC">
	                    <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "PolicySymbol", string.Empty, "")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Policy Exp" SortExpression="PolicyExpireDate ASC">
	                    <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "PolicyExpireDate", "{0:d}", DateTime.MinValue, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Loss Date" SortExpression="LossDate ASC">
	                    <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "LossDate", "{0:d}", DateTime.MinValue, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Amount" SortExpression="Amount ASC">
	                    <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "Amount", "{0:C}", decimal.MinValue, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Claimant" SortExpression="Claimant ASC">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "Claimant", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" SortExpression="Status ASC">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "Status", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Loss Type" SortExpression="LossType ASC">
                        <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "LossType", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Claim Comment" SortExpression="Comment ASC">
	                    <ItemStyle VerticalAlign="Top"></ItemStyle>
	                    <ItemTemplate>
		                    <%# HtmlEncodeNullable(Container.DataItem, "Comment", string.Empty, "(none)")%>
	                    </ItemTemplate>
                    </asp:TemplateColumn>
	            </columns>
            </asp:datagrid>
        </cc:Box>
        <% } %>
        
    </td>
        <td width="15">&nbsp;</td>
        <td valign="top">
        	
        <cc:Box id="boxActions" runat="server" title="Available Actions" width="180">
            <ul><li><asp:LinkButton ID="lnkCreatePlan" runat="server">Create Service Plan</asp:LinkButton></li></ul>
        </cc:Box>
	    </td>
    </tr>
    </table>
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
    function Toggle(id)
    {
	    ToggleControlDisplay(id);
    }
    </script>
    
    </form>
</body>
</html>
