using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Entity = Berkley.BLC.Entities;
using QCI.Web;

public partial class AdminUserAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminUserAspx_Load);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.btnCopy.Click += new EventHandler(btnCopy_Click);
    }
    #endregion

    protected Entity.User _user; // used during HTML rendering
    protected bool _isFeeCompany; // used during HTML rendering

    void AdminUserAspx_Load(object sender, EventArgs e)
    {
        Guid id = ARMSUtility.GetGuidFromQueryString("id", true);
        _user = Entity.User.Get(id);
        _isFeeCompany = _user.IsFeeCompany;

        if (!this.IsPostBack)
        {
            // populate the user combo
            Berkley.BLC.Entities.User[] users = Berkley.BLC.Entities.User.GetSortedArray("CompanyID = ? && IsFeeCompany = false && IsUnderwriter = false && AccountDisabled = false && ID != ?", "Name ASC", this.CurrentUser.CompanyID, (_user != null) ? _user.ID : Guid.Empty);
            cboActiveUsers.DataBind(users, "ID", "Name");
            //cboActiveUsers.Items.Insert(0, new ListItem("-- select user --", ""));

            //boxUserSearches.Title = string.Format("Copy Saved Searches From Another User For {0}.", _user.Name);
        }

        // initalize page controls
        this.PageHeading = (!_isFeeCompany ? "User " : "Fee Company ") + "Details";
        boxUser.Title = (!_isFeeCompany ? "User " : "Company ") + "Summary";
        lblName.Name = (!_isFeeCompany) ? "Name " : "Company Name";
        lblUsername.Visible = (!_isFeeCompany);
        lblWorkPhone.Name = (!_isFeeCompany) ? "Work Phone" : "Phone Number";
        lblRole.Visible = (!_isFeeCompany);
        lblConsultantCount.Visible = (_isFeeCompany);

        // populate summary fields
        this.PopulateFields("lbl", _user);
        this.PopulateFields("lbl", _user.Role);
        lblManager.Text = (_user.Manager != null) ? _user.Manager.Name : "(not specified)";

        if (_user.ProfitCenter != null)
        {
            this.PopulateFields("lbl", _user.ProfitCenter);
        }
        else
        {
            lblProfitCenter.Text = "(not specified)";
        }

        if (_isFeeCompany)
        {
            Entity.FeeUser[] consultants = Entity.FeeUser.GetArray("FeeCompanyUserID = ?", _user.ID);
            lblConsultantCount.Text = consultants.Length.ToString();
        }

        // hide fields with no value
        lblWorkPhone.Visible = (lblWorkPhone.Value != null && (string)lblWorkPhone.Value != string.Empty);
        lblHomePhone.Visible = (lblHomePhone.Value != null && (string)lblHomePhone.Value != string.Empty);
        lblMobilePhone.Visible = (lblMobilePhone.Value != null && (string)lblMobilePhone.Value != string.Empty);
        lblAddress.Visible = (lblAddress.Value != null && (string)lblAddress.Value != string.Empty);

    }

    void btnCopy_Click(object sender, EventArgs e)
    {
        Entity.User user = Entity.User.Get(Guid.Parse(cboActiveUsers.SelectedValue));
        Entity.UserSearch[] existingSearches = Entity.UserSearch.GetArray("UserID = ?", user.ID);

        if (existingSearches.Length == 0)
        {
            JavaScript.ShowMessage(this, string.Format("{0} does not have any existing saved searches to copy from.  Please select a different user and try again", user.Name));
            return;
        }
        
        foreach (Entity.UserSearch oldSearch in existingSearches)
        {
            Entity.UserSearch newSearch = new Entity.UserSearch(Guid.NewGuid());
            newSearch.UserID = _user.ID;
            newSearch.Name = oldSearch.Name;
            newSearch.FilterExpression = oldSearch.FilterExpression;
            newSearch.FilterParameters = oldSearch.FilterParameters.ToUpper().Replace(user.ID.ToString().ToUpper(), _user.ID.ToString().ToUpper());
            newSearch.DisplayOrder = oldSearch.DisplayOrder;
            newSearch.IsRequired = oldSearch.IsRequired;

            newSearch.Save();
        }
        
        string message = String.Format("{0} saved searches have now been copied over from {1} to {2}.", existingSearches.Length, user.Name, _user.Name);
        JavaScript.ShowMessageAndRedirect(this, message, "AdminUsers.aspx");
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("AdminUser.aspx?id=" + _user.ID, true);
    }
}
