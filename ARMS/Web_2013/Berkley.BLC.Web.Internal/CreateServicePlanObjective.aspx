<%@ Page Language="C#" AutoEventWireup="false" CodeFile="CreateServicePlanObjective.aspx.cs" Inherits="CreateServicePlanObjectiveAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Date" Src="~/Fields/DateField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <cc:Box id="boxObjective" runat="server" Title="Objective Details" width="500">
        <table class="fields">
            <uc:Text id="txtObjective" runat="server" field="Objective.Text" IsRequired="True" Name="Objective" Columns="68" Rows="5" />
            <uc:Text id="txtComments" runat="server" field="Objective.CommentsText" Name="Comments" Columns="68" Rows="5" />
            <uc:Date id="dtTargetDate" runat="server" IsRequired="True" field="Objective.TargetDate" />
            <uc:Combo id="cboObjectiveStatus" runat="server" field="Objective.StatusCode" Name="Objective Status" topItemText="" isRequired="true"
		        DataType="ObjectiveStatus" DataValueField="Code" DataTextField="Name" DataOrder="DisplayOrder ASC" />
        </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
