using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using Wilson.ORMapper;
using QCI.Web.Validation;
using QCI.Web;
using QCI.ExceptionManagement;
using Aspose.Words;
using System.Data.SqlClient;

public partial class AdminDocumentExtractionAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(AdminDocumentExtractionAspx_Load);
        this.PreRender += new EventHandler(AdminDocumentExtractionAspx_PreRender);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.grdFormFields.ItemDataBound += new DataGridItemEventHandler(grdFormFields_ItemDataBound);
    }
    #endregion

    protected MergeDocument _mergeDoc;
    //protected bool isLettersAdmin = false;
    protected MergeDocumentType mergeType;
    protected FormFieldDataType[] formFieldDataTypes;

    void AdminDocumentExtractionAspx_Load(object sender, EventArgs e)
    {
        Guid mergeDocID = ARMSUtility.GetGuidFromQueryString("mergedocid", true);
        _mergeDoc = MergeDocument.Get(mergeDocID);

        //isLettersAdmin = ARMSUtility.GetBoolFromQueryString("letterflag", true);

        mergeType = MergeDocumentType.Get(ARMSUtility.GetIntFromQueryString("typeflag", true));

        //load the values for the data type dropdown
        formFieldDataTypes = FormFieldDataType.GetSortedArray("IsBooleanField = false", "DisplayOrder");
    }

    void AdminDocumentExtractionAspx_PreRender(object sender, EventArgs e)
    {
        // get
        MergeDocumentVersion version = MergeDocumentVersion.GetOne("MergeDocumentID = ? && IsActive = true", _mergeDoc.ID);
        System.IO.Stream stream = null;

        try
        {
            ImagingHelper imaging = new ImagingHelper(CurrentUser.Company);
            stream = imaging.RetrieveFileImage(version.FileNetReference);
        }
        catch (Exception)
        {
            JavaScript.ShowMessageAndRedirect(this, string.Format("Error retrieving document from FileNet.  This document version may not exist in the '{0}' environment.  If that is the case, please upload a new document version and then attempt to scan for form fields again.", Berkley.BLC.Core.CoreSettings.ConfigurationType), GetReturnUrl());
            return;
        }

        Aspose.Words.License license = new Aspose.Words.License();
        license.SetLicense("Aspose.Words.lic");

        Document doc = new Document(stream);

        //Check if the field names are unique
        Hashtable list = new Hashtable();
        bool duplicateFound = false;
        //List<string> list = new List<string>(doc.Range.FormFields.Count);
        foreach (Aspose.Words.Fields.FormField formField in doc.Range.FormFields)
        {
            if (list.Contains(formField.Name))
            {
                duplicateFound = true;
                
                //get the current occurance count and incriment by 1
                int count = Int32.Parse(list[formField.Name].ToString());
                list[formField.Name] = count + 1;
            }
            else
            {
                list.Add(formField.Name, 1);
            }
        }

        if (duplicateFound)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in list.Keys)
            {
                int count = Int32.Parse(list[key].ToString());
                if (count > 1)
                {
                    sb.AppendFormat("- '{0}' is used {1} times.\r\n", key, count);
                }
            }

            JavaScript.ShowMessageAndRedirect(this, "All form field names must be unique.\r\n\r\nThe following fields had duplicate names and must be corrected:\r\n\r\n" + sb.ToString(), GetReturnUrl());
            return;
        }


        grdFormFields.DataSource = doc.Range.FormFields;
        grdFormFields.DataBind();

        if (doc.Range.FormFields.Count <= 0)
        {
            grdFormFields.Visible = false;
            btnSubmit.Visible = false;
            btnCancel.Visible = false;
            lblNoResults.Visible = true;
        }

        
        //look for fields that used to be reporatable but are no longer contained in the document
        List<MergeDocumentField> missingReportableFields = new List<MergeDocumentField>();
        MergeDocumentField[] docFields = MergeDocumentField.GetArray("MergeDocumentID = ?", _mergeDoc.ID);
        foreach (MergeDocumentField docField in docFields)
        {
            if (!list.Contains(docField.FieldName))
            {
                missingReportableFields.Add(docField);
            }
        }

        grdMissingFormFields.DataSource = missingReportableFields.ToArray();
        grdMissingFormFields.DataBind();
    }

    void grdFormFields_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the form field
            Aspose.Words.Fields.FormField formField = (Aspose.Words.Fields.FormField)e.Item.DataItem;

            // get our controls
            HtmlImage img = (HtmlImage)e.Item.FindControl("imgFieldStatus");
            CheckBox chkInclude = (CheckBox)e.Item.FindControl("chkInclude");
            CheckBox chkRequired = (CheckBox)e.Item.FindControl("chkRequired");
            TextBox  txtDisplayName = (TextBox)e.Item.FindControl("txtDisplayName");
            DropDownList cboDataType = (DropDownList)e.Item.FindControl("cboDataType");

            //set the validation up
            RequiredFieldValidator rfvDisplayName = (RequiredFieldValidator)e.Item.FindControl("rfvDisplayName");
            RequiredFieldValidator rfvDataType = (RequiredFieldValidator)e.Item.FindControl("rfvDataType");
            rfvDisplayName.ErrorMessage = string.Format("'{0}' display name is required.", formField.Name);
            rfvDataType.ErrorMessage = string.Format("'{0}' data type selection is required.", formField.Name);
            chkInclude.Attributes.Add("onclick", string.Format("setValidationState(this, '{0}'); setValidationState(this, '{1}');", rfvDisplayName.ClientID, rfvDataType.ClientID));

            //add attributes for later retrieval
            chkInclude.Attributes.Add("FieldName", formField.Name);
            chkInclude.Attributes.Add("FieldType", formField.Type.ToString());

            MergeDocumentField docField = MergeDocumentField.GetOne("MergeDocumentID = ? && FieldName = ? && IsReported = true", _mergeDoc.ID, formField.Name);
            if (docField != null)
            {
                chkInclude.Checked = true;
                txtDisplayName.Text = docField.DisplayName;
                chkRequired.Checked = docField.IsRequired;
                e.Item.CssClass = "selectedItem";
            }

            // load and default the DataType, if possible
            if (formField.Type == Aspose.Words.Fields.FieldType.FieldFormCheckBox)
            {
                cboDataType.Items.Add(new ListItem("N/A", FormFieldDataType.YesNo.Code));
            }
            else if (formField.Type == Aspose.Words.Fields.FieldType.FieldFormTextInput)
            {

                QCI.Web.ComboHelper.BindCombo(cboDataType, formFieldDataTypes, "Code", "DisplayName");
                cboDataType.Items.Insert(0, new ListItem(string.Empty, string.Empty));

                cboDataType.SelectedValue = (docField != null) ? docField.FormFieldDataTypeCode : string.Empty;
            }
            else
            {
                cboDataType.Items.Add(new ListItem("N/A", FormFieldDataType.Text.Code));
            }
        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        List<string> reportableFields = new List<string>();
        
        foreach (DataGridItem item in grdFormFields.Items)
        {
            if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox chkInclude = (CheckBox)item.FindControl("chkInclude");
                CheckBox chkRequired = (CheckBox)item.FindControl("chkRequired");
                TextBox txtDisplayName = (TextBox)item.FindControl("txtDisplayName");
                DropDownList cboDataType = (DropDownList)item.FindControl("cboDataType");
                string fieldName = chkInclude.Attributes["FieldName"].ToString();
                MergeDocumentField docField = MergeDocumentField.GetOne("MergeDocumentID = ? && FieldName = ?", _mergeDoc.ID, fieldName);
                
                if (chkInclude.Checked && docField == null)
                {
                    MergeDocumentField newDocField = new MergeDocumentField(_mergeDoc.ID, fieldName);
                    newDocField.FieldType = chkInclude.Attributes["FieldType"].ToString();
                    newDocField.IsReported = true;
                    newDocField.IsRequired = chkRequired.Checked;
                    newDocField.DisplayName = txtDisplayName.Text;
                    newDocField.FormFieldDataTypeCode = cboDataType.SelectedValue;
                    newDocField.LastReportedOn = DateTime.Now;

                    //commit
                    newDocField.Save();
                }
                else if (docField != null)
                {
                    docField = docField.GetWritableInstance();
                    docField.IsReported = chkInclude.Checked;
                    docField.IsRequired = chkRequired.Checked;
                    docField.DisplayName = txtDisplayName.Text;
                    docField.FormFieldDataTypeCode = cboDataType.SelectedValue;
                    docField.LastReportedOn = DateTime.Now;

                    //commit
                    docField.Save();
                }

                if (chkInclude.Checked)
                {
                    reportableFields.Add(fieldName);
                }
            }
        }

        //now check to see if any prior fields were removed from the document and need to be set as not reportable
        MergeDocumentField[] docFields = MergeDocumentField.GetArray("MergeDocumentID = ? && IsReported = true", _mergeDoc.ID);
        foreach (MergeDocumentField docField in docFields)
        {
            if (!reportableFields.Contains(docField.FieldName))
            {
                MergeDocumentField updateDocField = docField.GetWritableInstance();
                updateDocField.IsReported = false;
                updateDocField.Save();
            }
        }
        
        Response.Redirect(GetReturnUrl(), true);
    }

    protected string GetBooleanString(object dataItem)
    {
        MergeDocumentField eField = (MergeDocumentField)dataItem;
        return (eField.IsRequired) ? "Yes" : "No";
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    protected string GetReturnUrl()
    {
        return string.Format("AdminDocumentEdit.aspx?mergedocid={0}&typeflag={1}", _mergeDoc.ID, mergeType.ID);
    }
}
