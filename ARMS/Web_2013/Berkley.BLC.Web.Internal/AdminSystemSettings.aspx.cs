﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminSystemSettingsAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        //this.Load += new EventHandler(AdminRolesAspx_Load);
        //this.PreRender += new EventHandler(AdminRolesAspx_PreRender);
        //this.grdRoles.PageIndexChanged += new DataGridPageChangedEventHandler(grdRoles_PageIndexChanged);
        //this.grdRoles.SortCommand += new DataGridSortCommandEventHandler(grdRoles_SortCommand);
    }
    #endregion

    void AdminSystemSettingsAspx_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            // restore sort expression, reverse state, and page index settings
            //this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", grdRoles.Columns[0].SortExpression);
            //this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", false);
            //grdRoles.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);
        }
    }

    void AdminSystemSettingsAspx_PreRender(object sender, EventArgs e)
    {
        //string sortExp = (string)this.ViewState["SortExpression"];
        //if ((bool)this.ViewState["SortReversed"])
        //{
        //    sortExp = sortExp.Replace(" DESC", " TEMP");
        //    sortExp = sortExp.Replace(" ASC", " DESC");
        //    sortExp = sortExp.Replace(" TEMP", " ASC");
        //}

        //// get all roles for this company except the one used for Fee Companies
        //Entities.UserRole[] roles = Entities.UserRole.GetSortedArray("CompanyID = ? && ID != Company.FeeCompanyUserRoleID", sortExp, this.UserIdentity.CompanyID);
        //string noRecordsMessage = "There are no roles defined for your company.";

        //grdRoles.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        //DataGridHelper.BindPagingGrid(grdRoles, roles, null, noRecordsMessage);
    }
}