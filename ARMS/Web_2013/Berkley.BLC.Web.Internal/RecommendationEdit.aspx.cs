using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web.Validation;
using Wilson.ORMapper;
using QCI.Web;
using System.Linq;

public partial class RecommendationEditAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(RecommendationEditAspx_Load);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.btnAddAnother.Click += new EventHandler(btnAddAnother_Click);
    }
    #endregion

    protected SurveyRecommendation _eRec;
    protected Survey _eSurvey;
    protected string _autoRECNumbering;
    protected string _defaultRecClassification;
    protected string _recCodeDisplayFormat;
    protected string _requestedByPage;

    void RecommendationEditAspx_Load(object sender, EventArgs e)
    {
        // register our AJAX class for use in client script
        
        Ajax.Utility.RegisterTypeForAjax(typeof(AjaxMethods));
        
        cboRecCode.Attributes.Add("OnChange", "GetDescription(this.value);");
        
        Guid gSurveyRecID = ARMSUtility.GetGuidFromQueryString("surveyrecid", false);
        _eRec = (gSurveyRecID != Guid.Empty) ? SurveyRecommendation.Get(gSurveyRecID) : null;

        Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        _eSurvey = Survey.Get(gSurveyID);

        // Auto Rec Number
        _autoRECNumbering = Berkley.BLC.Business.Utility.GetCompanyParameter("AutoRECNumbering", CurrentUser.Company).ToLower();

        // Parameters
        _defaultRecClassification = Berkley.BLC.Business.Utility.GetCompanyParameter("DefaultRecClassification", CurrentUser.Company);
        _recCodeDisplayFormat = (!string.IsNullOrWhiteSpace(Berkley.BLC.Business.Utility.GetCompanyParameter("RecCodeDisplayFormat", CurrentUser.Company))) ? Berkley.BLC.Business.Utility.GetCompanyParameter("RecCodeDisplayFormat", CurrentUser.Company) : null;

        //Added By Vilas Meshram: 05/23/2013: Squish# 21331 : Modification of Recommendation Snap Shot.
        _requestedByPage = ARMSUtility.GetStringFromQueryString("requestedByPage", false);

        if (!this.IsPostBack)
        {
            // populate the rec list with all rec codes and descriptions
            List<Recommendation> eRecs = new List<Recommendation>();
            eRecs = Recommendation.GetSortedArray("CompanyID = ? && Disabled = false", "Code ASC", this.CurrentUser.CompanyID).ToList();
            
            //Check if Rec being populated is disabled and add to list
            if (_eRec != null && _eRec.Recommendation != null && _eRec.Recommendation.Disabled)
                eRecs.Add(_eRec.Recommendation);

            if (_recCodeDisplayFormat != null)
                cboRecCode.DataBind(eRecs, "ID", _recCodeDisplayFormat, "Code|Title".Split('|'));
            else
                cboRecCode.DataBind(eRecs, "ID", "{0} - {1}", "Code|Title".Split('|'));

            // populate the company specific types
            RecClassification[] recClassifications = RecClassification.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            cboRecClass.DataBind(recClassifications, "ID", "Type.Name");
            cboRecClass.Visible = recClassifications.Length > 0;

            RecStatus[] recStatuses = RecStatus.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            cboRecStatus.DataBind(recStatuses, "ID", "Type.Name");

            if (_eRec != null) // edit
            {
                this.PopulateFields(_eRec);
                this.PopulateFields(_eRec.Recommendation);
            }
            else
            {
                lblDateCreated.Text = DateTime.Today.ToShortDateString();
                lblDateCompleted.Text = "(not specified)";

                RecStatus recStatus = RecStatus.GetOne("CompanyID = ? && TypeCode = ?", CurrentUser.CompanyID, RecStatusType.Open.Code);
                cboRecStatus.SelectedValue = recStatus.ID.ToString();  
                //cboRecStatus.SelectedValue = RecStatusType.Open.Code;

                // Generate Auto Recommendation Number
                if (_autoRECNumbering == "true")
                    txtRecNumber.Text = GetNextRecommendationNumber(_eSurvey);
                
                // Select default Rec Classification type as per Company configuration
                if (_defaultRecClassification.Length > 0)
                {
                        RecClassification recClassification = RecClassification.GetOne("CompanyID = ? && Type.Name = ?", CurrentUser.CompanyID, _defaultRecClassification);
                        if (recClassification != null)
                            cboRecClass.SelectedValue = recClassification.ID.ToString(); 
                }

            }

            if (!CurrentUser.IsUnderwriter || (CurrentUser.IsUnderwriter && CurrentUser.Company.AllowUnderwritersToEditRecs))
            {
                btnSubmit.Text = (_eRec == null) ? "Add" : "Update";
                btnAddAnother.Visible = (_eRec == null);
                base.PageHeading = (_eRec == null) ? "Add Recommendation" : "Update Recommendation";
            }
            else
            {
                btnSubmit.Text = "Back to List";
                base.PageHeading = "View Recommendation";
                btnCancel.Visible = false;
            }

        }
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        if (!CurrentUser.IsUnderwriter || (CurrentUser.IsUnderwriter && CurrentUser.Company.AllowUnderwritersToEditRecs))
        {
            try
            {
                SurveyRecommendation eRec;
                if (_eRec == null) // add
                {
                    eRec = new SurveyRecommendation(Guid.NewGuid());
                    eRec.SurveyID = _eSurvey.ID;

                    //get the highest number and incriment by one
                    SurveyRecommendation[] eRecs = SurveyRecommendation.GetSortedArray(_eSurvey.GetAllSurveysForAccount, "PriorityIndex DESC");
                    eRec.PriorityIndex = (eRecs.Length > 0) ? eRecs[0].PriorityIndex + 1 : 1;
                }
                else
                {
                    eRec = _eRec.GetWritableInstance();
                }

                eRec.RecommendationID = new Guid(cboRecCode.SelectedValue);
                eRec.RecommendationNumber = txtRecNumber.Text;
                eRec.EntryText = txtRecDescription.Text;
                eRec.Comments = txtComments.Text;
                eRec.RecClassificationID = (cboRecClass.SelectedValue != string.Empty) ? new Guid(cboRecClass.SelectedValue) : Guid.Empty;
                eRec.DateCreated = (eRec.DateCreated == DateTime.MinValue) ? DateTime.Today : eRec.DateCreated;
                eRec.RecStatusID = (cboRecStatus.SelectedValue != string.Empty) ? new Guid(cboRecStatus.SelectedValue) : Guid.Empty;

                //Set the complete date
                if (eRec.DateCompleted == DateTime.MinValue && eRec.RecStatus.TypeCode == RecStatusType.Completed.Code)
                {
                    eRec.DateCompleted = DateTime.Today;
                }
                else if (eRec.DateCompleted != DateTime.MinValue && eRec.RecStatus.TypeCode != RecStatusType.Completed.Code)
                {
                    eRec.DateCompleted = DateTime.MinValue;
                }
                else
                {
                    eRec.DateCompleted = eRec.DateCompleted;
                }

                // commit changes
                eRec.Save();


                //get Critical Rec Classification 
                RecClassification recClassification = RecClassification.GetOne("CompanyID = ? && TypeCode = ?", CurrentUser.CompanyID, RecClassificationType.Critical.Code);

                RecStatus recStatus = RecStatus.GetOne("CompanyID = ? && TypeCode = ?", CurrentUser.CompanyID, RecStatusType.Open.Code);

                //Update Survey column ContainsCriticalRec = true if there is any Critical Rec in Survey_Recommandation
                if (recClassification != null)
                {
                    SurveyRecommendation[] eSurveyRecs = SurveyRecommendation.GetArray("SurveyID = ? && RecClassificationID = ?", eRec.SurveyID, recClassification.ID);

                    Survey eSurvey;
                    eSurvey = _eSurvey.GetWritableInstance();
                    eSurvey.ContainsCriticalRec = (eSurveyRecs.Length > 0) ? true : false;
                    eSurvey.Save();
                }
                if (recClassification != null && recStatus != null)
                {
                    SurveyRecommendation[] eSurveyRecs = SurveyRecommendation.GetArray("SurveyID = ? && RecClassificationID = ? && RecStatusID = ?", eRec.SurveyID, recClassification.ID, recStatus.ID);

                    Survey eSurvey;
                    eSurvey = _eSurvey.GetWritableInstance();
                    eSurvey.ContainsOpenCriticalRec = (eSurveyRecs.Length > 0) ? true : false;
                    eSurvey.Save();
                }

                //Update the survey history
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser);
                manager.RecManagement(eRec, (_eRec == null) ? SurveyManager.UIAction.Add : SurveyManager.UIAction.Update);

                // update our member var so the redirect will work correctly
                _eRec = eRec;
            }
            catch (ValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }
            catch (FieldValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }

        }
        

        if (_requestedByPage != null && _requestedByPage=="Survey.aspx" )
        {
            Response.Redirect(string.Format("Survey.aspx{0}", ARMSUtility.GetQueryStringForNav("surveyrecid")), true);
        }
        else if (sender == bool.TrueString)
        {
            Response.Redirect(string.Format("RecommendationEdit.aspx{0}", ARMSUtility.GetQueryStringForNav("surveyrecid")), true);
        }
        else
        {
            Response.Redirect(string.Format("Recommendations.aspx{0}", ARMSUtility.GetQueryStringForNav("surveyrecid")), true);
        }
    }

    void btnAddAnother_Click(object sender, EventArgs e)
    {
        btnSubmit_Click(bool.TrueString, null);
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        if (_requestedByPage != null && _requestedByPage == "Survey.aspx")
            Response.Redirect(string.Format("Survey.aspx{0}", ARMSUtility.GetQueryStringForNav("surveyrecid")), true);
        else
            Response.Redirect(string.Format("Recommendations.aspx{0}", ARMSUtility.GetQueryStringForNav("surveyrecid")), true);
    }

     #region --- Methods ---
    /// <summary>
    /// Generate next Recommendation Number'.
    /// </summary>
    /// <remarks>Created 03/22/2013 Vilas Meshram</remarks>
    /// <param name="Name">Value for parameter_eSurvey </param>
    public string GetNextRecommendationNumber(Survey _eSurvey)
    {
        try
        {
            string YearMonth = (_eSurvey.SurveyedDate != DateTime.MinValue) ? _eSurvey.SurveyedDate.ToString("yyyy-MM") : DateTime.Now.ToString("yyyy-MM");

            //Current Records
            SurveyRecommendation[] CurrentRecs = SurveyRecommendation.GetSortedArray("SurveyID = ?", "PriorityIndex ASC", _eSurvey.ID);

            //All PriorRecs
            SelectProcedure sp = new SelectProcedure(typeof(SurveyRecommendation), "Recs_GetPrior");
            sp.AddParameter("@SurveyID", _eSurvey.ID);
            sp.AddParameter("@CompanyID", CurrentUser.CompanyID);
            sp.AddParameter("@SurveyNumber", _eSurvey.Number);
            sp.AddParameter("@ClientID", _eSurvey.Insured.ClientID);
            sp.AddParameter("@RecStatus", ""); //For All Prior Recs

            IList list = DB.Engine.GetObjectSet(sp);
            Array result = Array.CreateInstance(sp.ObjectType, list.Count);
            list.CopyTo(result, 0);
            SurveyRecommendation[] oldRecs = result as SurveyRecommendation[];

            // All Prior Recs + Currrent Recs + 1
            int NextRecommendationNumber = CurrentRecs.Length + oldRecs.Length + 1;
            string sNextNumber = YearMonth + "-" + NextRecommendationNumber;

            return sNextNumber;
        }

        catch (Exception ex)
        {
            QCI.Web.JavaScript.ShowMessage(this, ex.Message);
            return string.Empty; 
        }

    }
    

    #endregion
    protected class AjaxMethods
    {
        [Ajax.AjaxMethod()]
        public static string GetDescription(string id)
        {
            Recommendation eRec = Recommendation.Get(new Guid(id));
            return eRec.Description;
        }

    }

}
