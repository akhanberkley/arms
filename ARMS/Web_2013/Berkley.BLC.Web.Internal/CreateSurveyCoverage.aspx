<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" CodeFile="CreateSurveyCoverage.aspx.cs" Inherits="CreateSurveyCoverageAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>


<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <link rel="stylesheet" media="print" type="text/css" href="<%= base.TemplatePath %>/Print-Insured.css" />

    <form id="frm" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server" />

    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><br /><br />
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxCoverages" runat="server" title="Reportable Coverages" width="600">

    <asp:Repeater ID="repCoverageNames" Runat="server">
    <HeaderTemplate>
        <table class="fields">
    </HeaderTemplate>
    <ItemTemplate>
	    <tr>
		    <td class="label" valign="middle" nowrap>
		        <span><b><%# HtmlEncode(Container.DataItem, "DisplayCoverageName")%></b></span>
		    </td>
		    <td>
		        &nbsp;
		    </td>
	    </tr>
	    <tr id="rowReportValue" runat="Server">
	        <td class="label" valign="middle" nowrap>
	            <span>Reporting Format *</span>
	        </td>
	        <td>
	            <asp:DropDownList ID="cboReportType" CssClass="cbo" runat="server"></asp:DropDownList>
	        </td>
	    </tr>
		    <asp:Repeater ID="repCoverageValues" Runat="server" DataSource="<%# GetValuesForCoverage(Container.DataItem) %>">
			    <ItemTemplate>
			        <asp:Repeater ID="repCoverageValueRows" Runat="server" DataSource="<%# GetRowsForCoverageValue(Container.DataItem) %>" OnItemDataBound="repCoverageValueRows_ItemDataBound">
			            <ItemTemplate>
	                        <tr>
		                        <td class="label" valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Name", string.Empty, "(not specified)")%></td>
                                <td>
                                    <asp:TextBox ID="txtProspectValue" CssClass="txt" runat="server" Columns="40"></asp:TextBox>
                                    <asp:CompareValidator ID="valProspectValueType" Runat="server" CssClass="val" ControlToValidate="txtProspectValue" Text="*" Display="Dynamic" Operator="DataTypeCheck" />
                                </td>
                                <!-- Added By Vilas Meshrram: 04/14/2013 : Squish# #21360 Vehical Schedule--->
                                <%# GetCoverageDetails(Container.DataItem)%>
                                
                              </tr>
                                                </ItemTemplate>
                                        </asp:Repeater>
	                        </ItemTemplate>
			    <SeparatorTemplate><tr height="5"><td colspan="2"></td></tr></SeparatorTemplate>
		    </asp:Repeater>
	    <tr>
	        <td colspan="2">&nbsp;</td>
	    </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
    </asp:Repeater>
    </cc:Box>
    
    <% if (repOptionalCoverageNames.Items.Count > 0) { %>
    <cc:Box id="boxOptionalReports" runat="server" title="Optional Report Specifications" width="600">

    <asp:Repeater ID="repOptionalCoverageNames" Runat="server">
    <HeaderTemplate>
        <table class="fields">
    </HeaderTemplate>
    <ItemTemplate>
	    <tr>
		    <td class="label" valign="middle" nowrap>
		        <span><b><%# GetCoverageName(Container.DataItem) %></b></span>
		    </td>
		    <td>
		        &nbsp;
		    </td>
	    </tr>
	    <tr>
	        <td class="label" valign="middle" nowrap>
	            <span>Reporting Format *</span>
	        </td>
	        <td>
	            <asp:DropDownList ID="cboReportType" CssClass="cbo" runat="server"></asp:DropDownList>
	        </td>
	    </tr>
    </ItemTemplate>
    <SeparatorTemplate>
        <tr>
	        <td colspan="2">&nbsp;</td>
	    </tr>
    </SeparatorTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
    </asp:Repeater>
    </cc:Box>
    <% } %>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />

    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    
    <div style="display: none;"><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" alt="" /></div>
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery-1.4.2.js"></script>
    <script type="text/javascript" src="<%= ARMSUtility.TemplatePath %>/javascripts/jquery.blockUI.js"></script>
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>

    <script type="text/javascript">
        $(document).ready(function() { 
            $.blockUI.defaults.css = {};
            $.blockUI.defaults.overlayCSS.opacity = .2;

            $('select').keypress(function (event)
            { return cancelBackspace(event) });
            $('select').keydown(function (event)
            { return cancelBackspace(event) });

            $('#frm').submit(function() {
                if (Page_IsValid) {
                    $.blockUI({ message: '<h3><img src="<%= ARMSUtility.AppPath %>/images/rotation.gif" />&nbsp;&nbsp;&nbsp;Processing, one moment please...</h3>' });
                }
            });
        });

        function cancelBackspace(event) {
            if (event.keyCode == 8) {
                return false;
            }
        }
 
        var imgA = "<%= ARMSUtility.AppPath %>/Images/plus.png";
        var imgB = "<%= ARMSUtility.AppPath %>/Images/minus.png";

        function ToggleExpandCollaspe(obj, id) {

            ToggleControlDisplay(id);

            obj.src = obj.src.match(imgA) ?
                      obj.src.replace(imgA, imgB) :
                      obj.src.replace(imgB, imgA);
        }

        function Toggle(id) {
                ToggleControlDisplay(id);
            }


    </script>
    
    </form>
</body>
</html>
