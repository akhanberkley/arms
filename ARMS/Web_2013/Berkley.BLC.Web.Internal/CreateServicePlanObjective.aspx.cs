using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web.Validation;
using Wilson.ORMapper;

public partial class CreateServicePlanObjectiveAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(CreateServicePlanObjectiveAspx_Load);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
    }
    #endregion

    protected Objective _eObjective;
    protected ServicePlan _eServicePlan;
    protected Survey _eSurvey;

    void CreateServicePlanObjectiveAspx_Load(object sender, EventArgs e)
    {
        Guid gObjectiveID = ARMSUtility.GetGuidFromQueryString("objectiveid", false);
        _eObjective = (gObjectiveID != Guid.Empty) ? Objective.Get(gObjectiveID) : null;

        //for nav back
        Guid gServicePlanID = ARMSUtility.GetGuidFromQueryString("planid", true);
        _eServicePlan = ServicePlan.Get(gServicePlanID);

        Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", true);
        _eSurvey = Survey.Get(gSurveyID);

        if (!this.IsPostBack)
        {
            if (_eObjective != null) // edit
            {
                this.PopulateFields(_eObjective);
            }
            else
            {
                //default target date to the visit's due date
                dtTargetDate.Date = _eSurvey.DueDate;
            }
        }

        btnSubmit.Text = (_eObjective == null) ? "Add" : "Update";
        base.PageHeading = (_eObjective == null) ? "Add Objective" : "Update Objective";
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        // double check state of validators (in case browser is not uplevel)
        if (!this.IsValid)
        {
            ShowValidatorErrors();
            return;
        }

        try
        {
            Objective eObjective;
            if (_eObjective == null) // add
            {
                eObjective = new Objective(Guid.NewGuid());
                eObjective.SurveyID = _eSurvey.ID;

                //get the highest number and incriment by one
                Objective[] eObjectives = Objective.GetSortedArray("SurveyID = ?", "Number DESC", _eSurvey.ID);
                eObjective.Number = (eObjectives.Length > 0) ? eObjectives[0].Number + 1 : 1;
            }
            else // edit
            {
                eObjective = _eObjective.GetWritableInstance();
            }

            this.PopulateEntity(eObjective);

            // commit all changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                eObjective.Save(trans);
                trans.Commit();
            }

            // update our member var so the redirect will work correctly
            _eObjective = eObjective;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl());
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    private string GetReturnUrl()
    {
        return string.Format("CreateServicePlanVisit.aspx?planid={0}&surveyid={1}", _eServicePlan.ID, _eSurvey.ID);
    }
}
