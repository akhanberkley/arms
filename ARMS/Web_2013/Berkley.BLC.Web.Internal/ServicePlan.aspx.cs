using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using QCI.Web.Validation;
using QCI.ExceptionManagement;
using Wilson.ORMapper;

public partial class ServicePlanAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(ServicePlanAspx_Load);
        this.PreRender += new EventHandler(ServicePlanAspx_PreRender);
        this.repActions.ItemCommand += new RepeaterCommandEventHandler(repActions_ItemCommand);
        this.repActions.ItemDataBound += new RepeaterItemEventHandler(repActions_ItemDataBound);
        this.repDocs.ItemDataBound += new RepeaterItemEventHandler(repDocs_ItemDataBound);
        this.repNotes.ItemDataBound +=new RepeaterItemEventHandler(repNotes_ItemDataBound);
        this.btnAddNote.Click += new EventHandler(btnAddNote_Click);
        this.btnAttachDocument.Click += new EventHandler(btnAttachDocument_Click);
        this.btnAssign.Click += new EventHandler(btnAssign_Click);
        this.grdVisits.PageIndexChanged += new DataGridPageChangedEventHandler(grdVisits_PageIndexChanged);
        this.grdVisits.SortCommand += new DataGridSortCommandEventHandler(grdVisits_SortCommand);
        this.lnkEditInstructions.Click += new EventHandler(lnkEditInstructions_Click);
        this.btnSaveDetails.Click += new EventHandler(btnSaveDetails_Click);
        this.btnCloseSubmit.Click += new EventHandler(btnCloseSubmit_Click);
        this.btnCloseCancel.Click += new EventHandler(btnCloseCancel_Click);
        this.btnTypeUpdate.Click += new EventHandler(btnTypeUpdate_Click);
        this.btnTypeCancel.Click += new EventHandler(btnTypeCancel_Click);
        this.btnGradingUpdate.Click += new EventHandler(btnGradingUpdate_Click);
        this.btnGradingCancel.Click += new EventHandler(btnGradingCancel_Click);
        this.btnDocumentCancel.Click += new EventHandler(btnDocumentCancel_Click);
        this.repServicePlanAttributes.ItemDataBound += new RepeaterItemEventHandler(repServicePlanAttributes_ItemDataBound);                
        this.repFields.ItemDataBound += new RepeaterItemEventHandler(repFields_ItemDataBound);
       }
    #endregion

    protected ServicePlan _eServicePlan;
    protected CompanyAttribute[] _companyObjectiveAttributes;
    protected bool _isEditMode;
    protected string _hideSPCreatedBy;
    protected string _assignedUserLabelName;
    protected string _fugureAssignedUserLabelName;
    protected string _servicePlanExpirationMsg;
    protected bool _usesPlanObjectives;
    protected bool _usesPlanGrading;
    protected bool _usesPlanVisitDetail;
    protected bool _displayPrintSelection;
    void ServicePlanAspx_Load(object sender, EventArgs e)
    {
        Guid gPlanID = ARMSUtility.GetGuidFromQueryString("planid", true);
        _isEditMode = ARMSUtility.GetBoolFromQueryString("edit", false);
        _eServicePlan = ServicePlan.Get(gPlanID);      
  
        if (!this.IsPostBack)
        {
            ShowActionControl(this.repActions);
            grdVisits.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);

            // restore sort expression, reverse state, and page index settings
            this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", grdVisits.Columns[1].SortExpression);
            this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", false);

        }

        if (this.Page.Request.UrlReferrer !=null && this.Page.Request.UrlReferrer.AbsolutePath.Contains("CreateServicePlan.aspx"))
        {
            JavaScript.ShowMessage(this, "Your service plan has been sucessfully created.");
        }

        _companyObjectiveAttributes = CompanyAttribute.GetSortedArray("CompanyID = ? && EntityTypeCode = ? && DisplayInGrid = true", "DisplayOrder ASC", CurrentUser.CompanyID, EntityType.ServiceVisitObjective.Code);

        //Set parameter values
        _hideSPCreatedBy = Berkley.BLC.Business.Utility.GetCompanyParameter("HideSPCreatedBy", CurrentUser.Company).ToLower();
        _assignedUserLabelName = Berkley.BLC.Business.Utility.GetCompanyParameter("LabelSPAssignedUser", CurrentUser.Company);
        if (!string.IsNullOrWhiteSpace(_assignedUserLabelName))
            lblAssignedUser.Name = _assignedUserLabelName;
        _fugureAssignedUserLabelName = Berkley.BLC.Business.Utility.GetCompanyParameter("LabelSPFutureAssignedUser", CurrentUser.Company);
        _servicePlanExpirationMsg = Berkley.BLC.Business.Utility.GetCompanyParameter("ServicePlanExpirationMsg", CurrentUser.Company);
        _usesPlanObjectives = (Utility.GetCompanyParameter("UsesPlanObjectives", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _usesPlanGrading = (Utility.GetCompanyParameter("UsesPlanGrading", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _usesPlanVisitDetail = (Utility.GetCompanyParameter("UsesPlanVisitDetail", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _displayPrintSelection = (Utility.GetCompanyParameter("DisplayPrintSelection", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
    }

    void ServicePlanAspx_PreRender(object sender, EventArgs e)
    {
        string sortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sortExp = sortExp.Replace(" DESC", " TEMP");
            sortExp = sortExp.Replace(" ASC", " DESC");
            sortExp = sortExp.Replace(" TEMP", " ASC");
        }
        
        if (this.IsPostBack)
        {
            // re-load the service plan to avoid dirty reads after postbacks
            _eServicePlan = ServicePlan.Get(_eServicePlan.ID);
        }

        // our date picker needs this script
        ARMSUtility.AddJavaScriptToPage(this, ARMSUtility.AppPath + "/AutoFormat.js");

        // put the insured and service plan number in the header
        base.PageHeading = string.Format("Service Plan #{0} ({1})", _eServicePlan.Number, ARMSUtility.GetSubstring(_eServicePlan.Insured.Name, 70));

        // populate all our static fields
        this.PopulateFields(_eServicePlan);
        this.PopulateFields(_eServicePlan.Status);
        lblType.Text = (_eServicePlan.Type != null) ? _eServicePlan.Type.Name : "(not specified)";
        lblGrading.Text = (!string.IsNullOrWhiteSpace(_eServicePlan.GradingCode)) ? _eServicePlan.SurveyGrading.Name : "(not specified)";

        // get all the visits (i.e. surveys)
        Survey[] eSurveys = Survey.GetSortedArray("CompanyID = ? && CreateStateCode != ? && CreateStateCode != ? && ServicePlanSurveys[ServicePlanID = ?]", sortExp, this.CurrentUser.CompanyID, CreateState.InProgress.Code, CreateState.ErrorCreating.Code, _eServicePlan.ID);
        string noRecordsMessage = "There are no scheduled visits for this service plan.";
        grdVisits.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
        DataGridHelper.BindPagingGrid(grdVisits, eSurveys, null, noRecordsMessage);
        boxVisits.Title = string.Format("Service Visits ({0})", eSurveys.Length);

        if (_usesPlanVisitDetail)
        {
            grdPlanVisits.ItemCreated += new DataGridItemEventHandler(ARMSUtility.PagingGrid_ItemCreated);
            grdPlanVisits.ShowFooter = true;
            DataGridHelper.BindPagingGrid(grdPlanVisits, eSurveys, null, noRecordsMessage);
            boxPlanVisits.Title = string.Format("Service Visits ({0})", eSurveys.Length);
            boxVisits.Visible = false;
            boxPlanVisits.Visible = true;
        }
            
        repServiceVisitsObjectives.DataSource = eSurveys;
        repServiceVisitsObjectives.DataBind();

        repServiceVisitsObjectivesForPrint.DataSource = eSurveys;
        repServiceVisitsObjectivesForPrint.DataBind();

        // get all documents associated to this service plan
        ServicePlanDocument[] docs = ServicePlanDocument.GetSortedArray("ServicePlanID = ? && IsRemoved = false", "UploadedOn DESC", _eServicePlan.ID);
        repDocs.DataSource = docs;
        repDocs.DataBind();
        boxDocs.Title = string.Format("Documents ({0})", docs.Length);

        // get all notes associated to this service plan
        ServicePlanNote[] notes = ServicePlanNote.GetSortedArray("ServicePlanID = ?", "EntryDate DESC", _eServicePlan.ID);
        repNotes.DataSource = notes;
        repNotes.DataBind();
        boxNotes.Title = string.Format("Comments ({0})", notes.Length);

        // get all history associated to this survey
        ServicePlanHistory[] history = ServicePlanHistory.GetSortedArray("ServicePlanID = ?", "EntryDate DESC", _eServicePlan.ID);
        repHistory.DataSource = history;
        repHistory.DataBind();
        boxHistory.Title = string.Format("History ({0})", history.Length);

        //Populate the dynamic fields (if any)
        CompanyAttribute[] attributes = AttributeHelper.GetCompanyAttributes(CurrentUser.Company, EntityType.ServicePlan);
        repFields.DataSource = attributes;
        repFields.DataBind();

        repServicePlanAttributes.DataSource = attributes;
        repServicePlanAttributes.DataBind();

        // get all the doc types for the company
        DocType[] docTypes = DocType.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", this.CurrentUser.CompanyID);
        ComboHelper.BindCombo(cboDocType, docTypes, "Code", "{0} - {1}", "Code|Description".Split('|'));
        if (docTypes.Length > 0)
        {
            cboDocType.Items.Insert(0, new ListItem("-- select doc type --", ""));
        }

        if (CurrentUser.Company.HideServicePlanVisitsByDefault)
        {
            divVisits.Attributes.Add("Style", "display:none;");
        }

        // determine available actions the user can take on this survey
        ServicePlanManager manager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
        ServicePlanAction[] actions = manager.GetAvailableActions();

        if (!this.IsPostBack)
        {
            //Check for policy expiration on plan
            if (!string.IsNullOrWhiteSpace(_servicePlanExpirationMsg) && manager.CheckServicePlanPolicyforExpiration(_eServicePlan))
            {
                //Display message
                JavaScript.ShowMessage(this, _servicePlanExpirationMsg);
            }
        }

        // turn on (or show) the actions available
        EnableActions(actions); 
        
    }

    void grdVisits_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sortExp = (string)this.ViewState["SortExpression"];
        bool sortRev = (bool)this.ViewState["SortReversed"];
        sortRev = !sortRev && (sortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sortRev);
    }

    protected string GetSurveyDetails(object dataItem)
    {
        Survey survey = (Survey)dataItem;
        return string.Format("<a href='Survey.aspx?NavFromPlan=1&planid={0}&surveyid={1}'>Service Visit #{2} ({3}) - {4}</a>", _eServicePlan.ID, survey.ID, survey.Number, survey.Type.Type.Name, survey.Location.SingleLineAddress);
    }

    protected Objective[] GetObjectivesForSurvey(object dataItem)
    {
        Survey survey = (Survey)dataItem;
        return Objective.GetSortedArray("SurveyID = ?", "Number ASC", survey.ID);
    }

    protected ObjectiveAttribute[] GetObjectiveAttributeValues(object dataItem)
    {
        Objective objective = (Objective)dataItem;
        //ObjectiveAttribute[] objectiveAttributes = ObjectiveAttribute.GetSortedArray("ObjectiveID = ? && CompanyAttribute.DisplayInGrid = true", "CompanyAttribute.DisplayOrder ASC", objective.ID);

        //look for any companyattributes that are missing from the objective attribute list
        List<ObjectiveAttribute> list = new List<ObjectiveAttribute>();
        List<Guid> listKeys = new List<Guid>();
        foreach(CompanyAttribute companyAttribute in _companyObjectiveAttributes)
        {
            if (!listKeys.Contains(companyAttribute.ID))
            {
                listKeys.Add(companyAttribute.ID);

                ObjectiveAttribute objectiveAttribute = ObjectiveAttribute.GetOne("ObjectiveID = ? && CompanyAttributeID = ?", objective.ID, companyAttribute.ID);
                if (objectiveAttribute != null)
                {
                    list.Add(objectiveAttribute);
                }
                else
                {
                    ObjectiveAttribute missingAttribute = new ObjectiveAttribute(objective.ID, companyAttribute.ID);

                    if (companyAttribute.MappedField == "Objective.Text")
                    {
                        missingAttribute.AttributeValue = objective.Text;
                    }
                    else if (companyAttribute.MappedField == "Objective.CommentsText")
                    {
                        missingAttribute.AttributeValue = objective.CommentsText;
                    }
                    else if (companyAttribute.MappedField == "Objective.TargetDate")
                    {
                        missingAttribute.AttributeValue = objective.TargetDate.ToShortDateString();
                    }

                    list.Add(missingAttribute);
                }
            }
        }

        return list.ToArray();
    }

    protected string GetEncodedAttributeValue(object dataItem)
    {
        ObjectiveAttribute attribute = (ObjectiveAttribute)dataItem;
        string result = AttributeHelper.GetEncodedAttributeValueForGrid(attribute, EntityType.ServiceVisitObjective, "(none)");

        int minWidth = (attribute.CompanyAttribute.MinWidthInGrid != Int32.MinValue) ? attribute.CompanyAttribute.MinWidthInGrid : 0;
        return string.Format("<td valign=\"top\" style=\"PADDING-LEFT: 20px; min-width: {0}px;\">{1}</td>", minWidth, result);
    }

    void lnkEditInstructions_Click(object sender, EventArgs e)
    {
        _isEditMode = true;
    }

    void btnSaveDetails_Click(object sender, EventArgs e)
    {
        _isEditMode = false;

        //commit changes
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            // update any objective attributes
            AttributeHelper.SaveServicePlanAttributes(repServicePlanAttributes, _eServicePlan.ID, CurrentUser, trans);

            trans.Commit();
        }

        Response.Redirect(string.Format("ServicePlan.aspx{0}", ARMSUtility.GetQueryStringForNav("edit")));
    }

    void repFields_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        CompanyAttribute companyAttribute = (CompanyAttribute)e.Item.DataItem;
        ServicePlanAttribute servicePlanAttribute = ServicePlanAttribute.GetOne("ServicePlanID = ? && CompanyAttributeID = ?", _eServicePlan.ID, companyAttribute.ID);

        HtmlGenericControl lblAttributeReadOnly = (HtmlGenericControl)e.Item.FindControl("lblAttributeReadOnly");
        lblAttributeReadOnly.InnerText = companyAttribute.Label;

        HtmlGenericControl lblAttributeValueReadOnly = (HtmlGenericControl)e.Item.FindControl("lblAttributeValueReadOnly");
        lblAttributeValueReadOnly.InnerText = AttributeHelper.GetEncodedAttributeValueForGrid(servicePlanAttribute, EntityType.ServicePlan, "(not specified)");
    }

   void repServicePlanAttributes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Berkley.BLC.Web.Internal.Controls.DynamicControlsPlaceholder pl = (Berkley.BLC.Web.Internal.Controls.DynamicControlsPlaceholder)e.Item.FindControl("pHolder");
        CompanyAttribute companyAttribute = (CompanyAttribute)e.Item.DataItem;
        string attributeValue = AttributeHelper.GetAttributeValue(_eServicePlan.ID, companyAttribute);

        //set the label
        HtmlGenericControl lblAttributeLabel = (HtmlGenericControl)e.Item.FindControl("lblAttributeLabel");
        lblAttributeLabel.InnerText = companyAttribute.Label + (companyAttribute.IsRequired ? " *" : string.Empty);

        AttributeHelper.LoadWebControls(companyAttribute, attributeValue, ref pl, e.Item.ItemIndex);
    }

    private void EnableActions(ServicePlanAction[] eActions)
    {
        //// reset integrated actions
        divAddNote.Visible = false;
        divUploadDoc.Visible = false;
       
        //// configure page for each integrated action in list
        ArrayList nonIntegrated = new ArrayList(eActions.Length);
        foreach (ServicePlanAction eAction in eActions)
        {
            if (eAction.TypeCode == ServicePlanActionType.AddNote.Code)
            {
                divAddNote.Visible = true;
            }
            else if (eAction.TypeCode == ServicePlanActionType.AddDocument.Code)
            {
                divUploadDoc.Visible = true;
            }
            else if (eAction.TypeCode == ServicePlanActionType.RemoveDocument.Code ||
                    eAction.TypeCode == ServicePlanActionType.RemoveNote.Code)
            {
                //do nothing, action is not 
            }
            else // not integrated
            {
                nonIntegrated.Add(eAction);
            }
        }

        // bind the actions repeater with what is left
        repActions.DataSource = nonIntegrated;
        repActions.DataBind();
    }

    private void ShowActionControl(Control ctrl)
    {
        this.repActions.Visible = (ctrl == this.repActions);
        this.divAssign.Visible = (ctrl == this.divAssign);
        this.divClosePlan.Visible = (ctrl == this.divClosePlan);
        this.divType.Visible = (ctrl == this.divType);
        this.divDocuments.Visible = (ctrl == this.divDocuments);
        this.divPlanGrading.Visible = (ctrl == this.divPlanGrading);
    }

    void repActions_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // we only care about repeater items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // see if this action has a confirm prompt specified
            ServicePlanAction action = (ServicePlanAction)e.Item.DataItem;

            // attach the message to the button control
            LinkButton btn = (LinkButton)e.Item.FindControl("btnAction");

            if (action.Type == ServicePlanActionType.Close)
            {
                //Check if there are any open visits
                Survey[] surveys = Survey.GetArray("ServicePlanSurveys[ServicePlanID = ?] && Status.TypeCode = ?", _eServicePlan.ID, SurveyStatusType.Survey.Code);
                if (surveys.Length > 0)
                {
                    string txt;
                    if (surveys.Length == 1)
                    {
                        txt = "There is still 1 survey that is not complete.  Are you sure you want to continue closing this service plan?";
                    }
                    else
                    {
                        txt = string.Format("There are still {0} surveys that are not complete.  Are you sure you want to continue closing this service plan?", surveys.Length);
                    }

                    JavaScript.AddConfirmationNoticeToWebControl(btn, txt);
                }
            }
            else if (action.ConfirmMessage != null && action.ConfirmMessage.Trim().Length > 0)
            {
                if (btn != null)
                {
                    JavaScript.AddConfirmationNoticeToWebControl(btn, action.ConfirmMessage);
                }
            }
        }
    }

    void repActions_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string actionID = e.CommandName;
        ServicePlanAction action = ServicePlanAction.Get(new Guid(actionID));
        TakeAction(action);
    }

    private void TakeAction(ServicePlanAction action)
    {
        try
        {
            //Don't allow the user to peform any actions until the required fields are entered
            if (action.Type != ServicePlanActionType.Cancel)
            {
                string strBuilder = string.Empty;
                bool foundOneEmptyRequiredField = false;
                CompanyAttribute[] companyAttributes = CompanyAttribute.GetArray("CompanyID = ? && EntityTypeCode = ? && IsRequired = True", CurrentUser.CompanyID, EntityType.ServicePlan.Code);
                foreach (CompanyAttribute companyAttribute in companyAttributes)
                {
                    ServicePlanAttribute field = ServicePlanAttribute.GetOne("ServicePlanID = ? && CompanyAttributeID = ?", _eServicePlan.ID, companyAttribute.ID);
                    if (field == null || field.AttributeValue == string.Empty)
                    {
                        if (!foundOneEmptyRequiredField)
                        {
                            foundOneEmptyRequiredField = true;
                            strBuilder += "The following field(s) are required and must be filled out under the additional plan information before any action can be taken:\n";
                        }

                        strBuilder += string.Format("- {0}\n", companyAttribute.Label);
                    }
                }

                if (!string.IsNullOrEmpty(strBuilder))
                {
                    throw new FieldValidationException(strBuilder);
                }
            }

            // determine what action to take based on the type
            ServicePlanActionType actionType = action.Type;

            if (actionType == ServicePlanActionType.Assign)
            {
                PrepForAssignment();
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divAssign);
            }
            else if (actionType == ServicePlanActionType.ReleaseOwnership)
            {
                ServicePlanManager manager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
                manager.ReleaseOwnership();
                Response.Redirect(GetReturnUrl(), true);
            }
            else if (actionType == ServicePlanActionType.ManageAccount)
            {
                Response.Redirect(string.Format("ServicePlanActivities.aspx{0}", ARMSUtility.GetQueryStringForNav()), true);
            }
            else if (actionType == ServicePlanActionType.AddVisit)
            {
                if (!_usesPlanObjectives)
                    Response.Redirect(string.Format("CreateServicePlanVisit.aspx{0}", ARMSUtility.GetQueryStringForNav("surveyid")), true);
                else
                {
                    Objective[] ojs = Objective.GetArray("ServicePlanID = ?", _eServicePlan.ID);

                    if (ojs.Length != null & ojs.Length > 0)
                        Response.Redirect(string.Format("CreateServicePlanVisit.aspx{0}", ARMSUtility.GetQueryStringForNav("surveyid")), true);
                    else
                        {
                        string msg = "You must add a Plan Objective before creating your service visit";

                        JavaScript.ShowMessage(this, msg);
                        return;
                        }

                }
            }
            else if (actionType == ServicePlanActionType.ManagePlanObjectives)
            {
                Response.Redirect(string.Format("ManagePlanObjectives.aspx{0}", ARMSUtility.GetQueryStringForNav("surveyid")), true);
            }
            else if (actionType == ServicePlanActionType.Reopen)
            {
                PrepForAssignment();
                this.ViewState["ActionTypeCode"] = actionType.Code;
                ShowActionControl(this.divAssign);
            }
            else if (actionType == ServicePlanActionType.Close)
            {
                PrepForClosePlan();
                ShowActionControl(this.divClosePlan);
            }
            else if (actionType == ServicePlanActionType.ChangePlanType)
            {
                PrepForTypeChange();
                ShowActionControl(this.divType);
            }
            else if (actionType == ServicePlanActionType.Cancel)
            {
                ServicePlanManager manager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
                manager.Cancel();
                Response.Redirect(GetReturnUrl(), true);
            }
            else if (actionType == ServicePlanActionType.DocumentReport)
            {
                PrepForGenerateDocuments();
                ShowActionControl(this.divDocuments);
            }
            else if (actionType == ServicePlanActionType.AssignPlanGrading)
            {
                PrepForGradingChange();
                ShowActionControl(this.divPlanGrading);
            }
            else if (actionType == ServicePlanActionType.EditInsured || actionType == ServicePlanActionType.EditPolicy)
            {
                Response.Redirect(string.Format("ServicePlanDetails.aspx{0}", ARMSUtility.GetQueryStringForNav()), true);
            }
            else
            {
                throw new NotSupportedException("Action Type '" + actionType.Code + "' was not expected.");
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
    }

    void grdVisits_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdVisits.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);
    }

    protected void lnkDownloadDoc_OnClick(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            ServicePlanDocument doc = ServicePlanDocument.Get(new Guid(lnk.Attributes["DocumentID"]));

            StreamDocument docRetriever = new StreamDocument(doc, CurrentUser.Company);
            docRetriever.AttachDocToResponse(this);
        }
        catch (System.Web.HttpException ex)
        {
            //ignore http exceptions for when the user quits a doc download and the stream doesn't get flushed
            if (ex.Message.Contains("The remote host closed the connection"))
            {
                //swallow exception
            }
            else
            {
                throw new Exception("See inner exception for details.", ex);
            }
        }
    }

    #region ACTION: Assignment

    private void PrepForAssignment()
    {
        // get all available users based on permissions
        User[] eUsers = Berkley.BLC.Entities.User.GetSortedArray("CompanyID = ? && AccountDisabled = false && Role.PermissionSet.CanWorkServicePlans = true", "Name ASC", CurrentUser.CompanyID);

        // populate the combo
        ComboHelper.BindCombo(cboUsers, eUsers, "ID", "Name");
        cboUsers.Items.Insert(0, new ListItem("-- select user --", ""));

        // set the assign label
        lblAssign.Text = "Assign to:";
    }

    void btnAssign_Click(object sender, EventArgs e)
    {
        try
        {
            if (cboUsers.SelectedValue.Length == 0)
            {
                throw new FieldValidationException(cboUsers, "Please select a user to assign.");
            }

            Guid userID = new Guid(cboUsers.SelectedValue);
            User targetUser = Berkley.BLC.Entities.User.Get(userID);

            string code = (string)this.ViewState["ActionTypeCode"];
            ServicePlanActionType actionType = ServicePlanActionType.Get(code);

            if (actionType == ServicePlanActionType.Assign)
            {
                ServicePlanManager manager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
                manager.Assign(targetUser);
            }
            else if (actionType == ServicePlanActionType.Reopen)
            {
                ServicePlanManager manager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
                manager.Reopen(targetUser);
            }
            else
            {
                throw new NotSupportedException("Action Type '" + actionType.Code + "' was not expected.");
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnAssignCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Add Note

    void btnAddNote_Click(object sender, EventArgs e)
    {
        try
        {
            string comment = FieldValidation.ValidateStringField(txtNote, "Comment", 8000, true);

            // add the note to this service plan
            ServicePlanManager manager = new ServicePlanManager(_eServicePlan, this.CurrentUser, false);
            manager.AddNote(comment, DateTime.Now);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Remove Note

    void repNotes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ServicePlanNote note = (ServicePlanNote)e.Item.DataItem;
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkRemoveNote");
            lnk.Attributes.Add("NoteID", note.ID.ToString());

            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdRemoveNote");
            td.Visible = (CurrentUser.Permissions.CanTakeAction(ServicePlanActionType.RemoveNote, _eServicePlan, CurrentUser));

            JavaScript.AddConfirmationNoticeToWebControl(lnk, "Are your sure you want to remove this comment?");
        }
    }

    protected void lnkRemoveNote_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        ServicePlanNote note = ServicePlanNote.Get(new Guid(lnk.Attributes["NoteID"]));

        ServicePlanManager manager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
        manager.RemoveNote(note);
    }

    #endregion

    #region ACTION: Close Plan

    private void PrepForClosePlan()
    {
        // get all available users based on permissions
        User[] eUsers = Berkley.BLC.Entities.User.GetSortedArray("CompanyID = ? && (AccountDisabled = false || ID = ?) && Role.PermissionSet.CanWorkServicePlans = true && IsUnderwriter = false", "Name ASC", CurrentUser.CompanyID, _eServicePlan.AssignedUserID);

        // populate the combo
        ComboHelper.BindCombo(cboFutureUsers, eUsers, "ID", "Name");
        cboFutureUsers.SelectedValue = _eServicePlan.AssignedUserID.ToString();
        
        rdoPlanYes.Attributes.Add("OnClick", "NewPlan()");
        rdoPlanNo.Attributes.Add("OnClick", "NewPlan()");
        rdoPlanNo.Checked = true;
        
        divFuturePlan.Attributes.Add("style", "DISPLAY: none");

        SurveyGradingCompany[] eGradings = SurveyGradingCompany.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
        ComboHelper.BindCombo(cboOverallGrading, eGradings, "SurveyGrading.Code", "SurveyGrading.Name");
        cboOverallGrading.Items.Insert(0, new ListItem("-- select grading --", ""));

        cboOverallGrading.SelectedValue = _eServicePlan.GradingCode;
    }

    void btnCloseSubmit_Click(object sender, EventArgs e)
    {
        if (_usesPlanGrading && cboOverallGrading.SelectedIndex < 1)
        {
            try
            {            
                    foreach (Survey s in _eServicePlan.Surveys2)
                    {
                        SurveyRecommendation recs = SurveyRecommendation.GetOne("SurveyID = ?", s.ID);
                        if (recs != null && recs.ID != Guid.Empty)
                            throw new FieldValidationException(cboOverallGrading, "Please select a Overall Plan Grading.");
                    }
                }
            catch (FieldValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }
            
        }
        
        //Run the importfactory to get the lastest insured data from the policy system
        if (rdoPlanYes.Checked)
        {
            try
            {
                Survey[] planSurveys = Survey.GetSortedArray("ServicePlanSurveys.ServicePlanID = ?", "CreateDate DESC", _eServicePlan.ID);

                if (planSurveys.Length > 0)
                {
                    Common.RefreshSurvey(planSurveys[0], CurrentUser, true);
                }
            }
            catch (Exception)
            {
                //swallow any exceptions from the refresh
            }
        }
        
        
        User user = Berkley.BLC.Entities.User.Get(new Guid(cboFutureUsers.SelectedValue));
        
        ServicePlanManager manager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
        ServicePlan newPlan = manager.Close(rdoPlanYes.Checked, user, cboOverallGrading.SelectedValue);

        if (newPlan != null && _usesPlanObjectives)
        {
            manager.CopyActivePlanObjectives(_eServicePlan, newPlan);
            JavaScript.ShowMessageAndRedirect(this.Page, "You have successfully closed this service plan.  You will now be redirected in order to create the service visits for your new plan.", string.Format("ServicePlan.aspx?planid={0}", newPlan.ID));
        }

        if (rdoPlanYes.Checked && newPlan != null)
        {
            JavaScript.ShowMessageAndRedirect(this.Page, "You have successfully closed this service plan.  You will now be redirected in order to create the service visits for your new plan.", string.Format("CreateServicePlanVisit.aspx?planid={0}", newPlan.ID));
        }
        else
        {
            Response.Redirect(GetReturnUrl(), true);
        }
    }

    void btnCloseCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Attach Document

    void btnAttachDocument_Click(object sender, EventArgs e)
    {
        HttpPostedFile postedFile = fileUploader.PostedFile;

        try
        {
            // make sure a filename was uploaded
            if (postedFile == null || postedFile.FileName.Length == 0) // no filename was specified
            {
                throw new FieldValidationException("Please select a file to submit.");
            }

            // make sure the file name is not larger the max allowed
            string fileName = System.IO.Path.GetFileName(postedFile.FileName);
            if (fileName.Length > 200)
            {
                throw new FieldValidationException("The file name cannot exceed 200 characters.");
            }

            // check for an extesion
            if (System.IO.Path.GetExtension(fileName).Length <= 0)
            {
                throw new FieldValidationException("The file name must contain a file extension.");
            }

            // make sure the file has content
            int max = int.Parse(ConfigurationManager.AppSettings["MaxRequestLength"]);
            if (postedFile.ContentLength <= 0) // the file is empty
            {
                throw new FieldValidationException("The file submitted is empty.");
            }
            else if (postedFile.ContentLength > max)
            {
                // the maximum file size is 10 Megabytes
                throw new FieldValidationException("The file size cannot be larger than 10 Megabytes.");
            }

            // check if the company has doc types and if one was selected
            if (cboDocType.Items.Count > 0 && cboDocType.SelectedValue == "")
            {
                throw new FieldValidationException("Please select a doc type.");
            }
        }
        catch (FieldValidationException ex)
        {
            JavaScript.SetInputFocus(boxDocs);
            this.ShowErrorMessage(ex);
            return;
        }

        // attach this file to the service plan
        try
        {
            ServicePlanManager manager = new ServicePlanManager(_eServicePlan, this.CurrentUser, false);
            manager.AttachDocument(postedFile.FileName, postedFile.ContentType, postedFile.InputStream, cboDocType.SelectedValue, txtDocRemarks.Text);

            Response.Redirect(GetReturnUrl(), true);
        }
        catch (System.Threading.ThreadAbortException)
        {
            //Do nothing, let the request redirect
        }
        catch (Exception ex)
        {
            //ExceptionManager.Publish(ex);
            throw (ex);
        }
    }

    #endregion

    #region ACTION: Remove Document

    void repDocs_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ServicePlanDocument eDoc = (ServicePlanDocument)e.Item.DataItem;
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkRemoveDoc");
            LinkButton lnkDownload = (LinkButton)e.Item.FindControl("lnkDownloadDoc");
            HtmlAnchor lnkDownloadDocInNewWindow = (HtmlAnchor)e.Item.FindControl("lnkDownloadDocInNewWindow");
            lnk.Attributes.Add("DocumentID", eDoc.ID.ToString());
            lnkDownload.Attributes.Add("DocumentID", eDoc.ID.ToString());

            //determine which link to display
            lnkDownload.Visible = !eDoc.MimeType.Contains("htm");
            lnkDownloadDocInNewWindow.Visible = eDoc.MimeType.Contains("htm");

            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdRemoveDoc");
            td.Visible = (CurrentUser.Permissions.CanTakeAction(ServicePlanActionType.RemoveDocument, _eServicePlan, CurrentUser));

            JavaScript.AddConfirmationNoticeToWebControl(lnk, string.Format("Are your sure you want to remove '{0}'?", eDoc.DocumentName));
        }
    }

    protected void lnkRemoveDoc_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        ServicePlanDocument doc = ServicePlanDocument.Get(new Guid(lnk.Attributes["DocumentID"]));

        ServicePlanManager manager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
        manager.RemoveDocument(doc);
    }

    #endregion

    #region ACTION: Document Report

    private void PrepForGenerateDocuments()
    {
        MergeDocumentVersion[] eReports = MergeDocumentVersion.GetSortedArray("MergeDocument[CompanyID = ? && Disabled = false && Type = ?] && IsActive = true",
            "MergeDocument.PriorityIndex ASC", this.CurrentUser.CompanyID, MergeDocumentType.Document.ID);

        // populate the combo
        ComboHelper.BindCombo(cboDocuments, eReports, "ID", "MergeDocument.Name");
        cboDocuments.Items.Insert(0, new ListItem("-- select document --", ""));
    }

    void btnDocumentCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        //divDocuments.Visible = false;
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Change Service Plan Type

    private void PrepForTypeChange()
    {
        ServicePlanType[] types = ServicePlanType.GetSortedArray("CompanyID = ? && Disabled = False", "DisplayOrder ASC", CurrentUser.CompanyID);

        ComboHelper.BindCombo(cboTypes, types, "ID", "Name");
        cboTypes.Items.Insert(0, new ListItem("-- select type --", ""));

        JavaScript.SetInputFocus(cboTypes);
    }

    void btnTypeUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            string id = cboTypes.SelectedValue;
            if (id.Length == 0)
            {
                throw new FieldValidationException(cboTypes, "Please select a new service plan type first.");
            }

            // get the selected type
            ServicePlanType type = ServicePlanType.Get(new Guid(id));

            // change the service plan type
            ServicePlanManager manager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
            manager.ChangeServicePlanType(type);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnTypeCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    #region ACTION: Change Plan Grading

    private void PrepForGradingChange()
    {
        SurveyGradingCompany[] eGradings = SurveyGradingCompany.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
        ComboHelper.BindCombo(cboOverallPlanGrading, eGradings, "SurveyGrading.Code", "SurveyGrading.Name");
        cboOverallPlanGrading.Items.Insert(0, new ListItem("-- select grading --", ""));

        cboOverallPlanGrading.SelectedValue = _eServicePlan.GradingCode;

        JavaScript.SetInputFocus(cboOverallPlanGrading);
    }

    void btnGradingUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            string id = cboOverallPlanGrading.SelectedValue;
            if (id.Length == 0)
            {
                throw new FieldValidationException(cboTypes, "Please select a Overall Plan Grading first.");
            }

            // change the service plan type
            ServicePlanManager manager = new ServicePlanManager(_eServicePlan, CurrentUser, false);
            manager.UpdatePlanGrading(id);
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    void btnGradingCancel_Click(object sender, EventArgs e)
    {
        // show the actions list again
        Response.Redirect(GetReturnUrl(), true);
    }

    #endregion

    protected string GetUserName(object obj)
    {
        User user = (obj as User);
        return Server.HtmlEncode((user != null) ? user.Name : "System");
    }

    protected string GetEncodedHistory(object dataItem)
    {
        string history = (dataItem as ServicePlanHistory).EntryText;

        // html encode the string (must do this first)
        return Server.HtmlEncode(history);
    }

    protected string GetEncodedComment(object dataItem)
    {
        string comment = (dataItem as ServicePlanNote).Comment;

        // html encode the string (must do this first)
        comment = Server.HtmlEncode(comment);

        // convert CR, LF, and TAB characters
        comment = comment.Replace("\r\n", "<br>");
        comment = comment.Replace("\r", "<br>");
        comment = comment.Replace("\n", "<br>");
        comment = comment.Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");

        return comment;
    }

    protected string GetFileNameWithSize(object dataItem)
    {
        ServicePlanDocument doc = (ServicePlanDocument)dataItem;

        string result = string.Format("{0} ({1:#,##0} KB)", doc.DocumentName, doc.SizeInBytes / 1024);

        return Server.HtmlEncode(result);
    }

    private string GetReturnUrl()
    {
        return string.Format("serviceplan.aspx{0}", ARMSUtility.GetQueryStringForNav());
    }
}
