using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Threading;
using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using Berkley.BLC.Import;
using Wilson.ORMapper;
using QCI.Web;
using QCI.Web.Validation;
using QCI.ExceptionManagement;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Data.SqlClient;


public partial class CreateSurveyAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(CreateSurveyAspx_Load);
        this.PreRender += new EventHandler(CreateSurveyAspx_PreRender);
        this.btnAddEmail.Click += new EventHandler(btnAddEmail_Click);
        this.grdLocations.ItemDataBound += new DataGridItemEventHandler(grdLocations_ItemDataBound);
        this.grdLocations.ItemCreated += new DataGridItemEventHandler(grdLocations_ItemCreated);
        this.grdLocations.SortCommand += new DataGridSortCommandEventHandler(grdLocations_SortCommand);
        this.grdLocations.ItemCommand += new DataGridCommandEventHandler(grdLocations_ItemCommand);
        this.grdLocations.PageIndexChanged += new DataGridPageChangedEventHandler(grdLocations_PageIndexChanged);
        this.lnkAddLocation.Click += new EventHandler(lnkAddLocation_Click);
        this.btnAttachDocument.Click += new EventHandler(btnAttachDocument_Click);
        this.lnkDocumentRetriever.Click += new EventHandler(lnkDocumentRetriever_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);
        this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
        this.lnkSelectAll.Click += new EventHandler(lnkSelectAll_Click);
        this.lnkUnselectAll.Click += new EventHandler(lnkUnselectAll_Click);
        this.lnkCopyCoverages.Click += new EventHandler(lnkCopyCoverages_Click);
        this.repDocs.ItemDataBound += new RepeaterItemEventHandler(repDocs_ItemDataBound);
    }
    #endregion

    protected Survey _eSurvey;

    //Parameter configurations
    protected string SurveyAssignPriority;
    protected bool _usesInHouseSelection;
    protected bool _agentEmailRequired;
    protected bool _sicDisplyCodeDescription;
    protected bool _naicDisplyCodeDescription;
    protected bool _dispSurveyRequestAgencyLookup;
    protected bool _requiresNAICCode;
    protected bool _hideSICCode;
    protected bool _displayProducer = false;
    protected bool _requireInsuredContact;
    protected bool _validatePolicyEffectiveDate;
    protected bool _canAssignConsultant = false;
    protected bool _setCommentsInternalOnly;
    protected bool _enableAdministrativeNotes;
    protected bool _userCanViewAdministrativeNotes;

    void CreateSurveyAspx_Load(object sender, EventArgs e)
    {
        Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", false);

        try
        {
            _eSurvey = (gSurveyID != Guid.Empty) ? Survey.Get(gSurveyID) : null;
            // try to auto-populate the Agency details from the agency number value if present
            // this is setting a OnChange event on the input.
            cboAgencyLookup.Attributes.Add("OnChange", string.Format("GetAgency(this.value, '{0}', '{1}');", _eSurvey.InsuredID.ToString(), this.CurrentUser.CompanyID));
            txtAgencyNumber.Attributes.Add("OnChange", string.Format("GetAgency(this.value, '{0}', '{1}');", _eSurvey.InsuredID.ToString(), this.CurrentUser.CompanyID));
            
        }
        catch
        {
            //survey id no longer exists
            Security.RequestExpired();
        }

        //Parameters
        SurveyAssignPriority = Utility.GetCompanyParameter("SurveyAssignPriority", CurrentUser.Company).ToLower();
        _usesInHouseSelection = (Berkley.BLC.Business.Utility.GetCompanyParameter("UsesInHouseSelection", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _agentEmailRequired = (Berkley.BLC.Business.Utility.GetCompanyParameter("AgentEmailRequired", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _sicDisplyCodeDescription = (Berkley.BLC.Business.Utility.GetCompanyParameter("SICDisplyCodeDescription", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _naicDisplyCodeDescription = (Berkley.BLC.Business.Utility.GetCompanyParameter("NASICDisplyCodeDescription", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _dispSurveyRequestAgencyLookup = (Berkley.BLC.Business.Utility.GetCompanyParameter("DispSurveyRequestAgencyLookup", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _requiresNAICCode = (Berkley.BLC.Business.Utility.GetCompanyParameter("RequiresNAICCode", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _hideSICCode = (Berkley.BLC.Business.Utility.GetCompanyParameter("HideSICCode", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _displayProducer = (Berkley.BLC.Business.Utility.GetCompanyParameter("DisplayProducer", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _requireInsuredContact = (Berkley.BLC.Business.Utility.GetCompanyParameter("RequireInsuredContact", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _validatePolicyEffectiveDate = (Berkley.BLC.Business.Utility.GetCompanyParameter("ValidatePolicyEffectiveDate", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _canAssignConsultant = (Berkley.BLC.Business.Utility.GetCompanyParameter("CanAssignConsultant", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _setCommentsInternalOnly = (Utility.GetCompanyParameter("defaultCommentsInternalOnly", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _enableAdministrativeNotes = (Utility.GetCompanyParameter("EnableAdministrativeNotes", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
        _userCanViewAdministrativeNotes = CurrentUser.Permissions.CanViewAdministrativeNotes;
        //Determine what is visible
        SetRequirements();

        base.PageHeading = (_eSurvey.Insured.Name.Length > 0) ? string.Format("Insured: ({0})", _eSurvey.Insured.Name) : "Create Survey Request";
        
        if (_displayProducer)
        {
            txtProducer.Visible = true;
        }
        else
        {
            txtProducer.Visible = false;
        }

        if (!this.IsPostBack)
        {
            // Set state of InHouse Consultant radio group
            if (_usesInHouseSelection)
            {
                rdoInHouseSelectionInHouse.Visible = true;
                rdoInHouseSelectionSystem.Visible = true;
                rdoInHouseSelectionSystem.Checked = true;
            }

            // populate the agency combo (if applicable and enabled by company param.)
            if (_dispSurveyRequestAgencyLookup)
            {
                Agency[] eAgencies = Agency.GetSortedArray("CompanyID = ?", "Name ASC", this.CurrentUser.CompanyID);
                cboAgencyLookup.DataBind(eAgencies, "Number", "{0} ({1})", new string[] { "Name", "Number" });
            }

            // populate the sic list with all sic codes and descriptions
            if (!_hideSICCode)
            {
                SIC[] SICcodes = AppCache.GetSICCodes(this, CurrentUser.Company, "SICCodes70", 70);
                string sicDisplayOrder = CurrentUser.Company.SICSortedByAlpha ? "Description|Code" : "Code|Description";

                if (_sicDisplyCodeDescription)
                    cboSIC.DataBind(SICcodes, "Code", "{1} - {0}", sicDisplayOrder.Split('|'));
                else
                    cboSIC.DataBind(SICcodes, "Code", "{0} - {1}", sicDisplayOrder.Split('|'));    
            }
            cboSIC.IsRequired = (CurrentUser.Company.RequireSICCode && !_hideSICCode) ? true : false;

            // populate the NAIC list with all codes and descriptions
            NAIC[] NAICcodes = AppCache.GetNAICCodes(this, CurrentUser.Company, "NAICCodes70", 70);
            string naicDisplayOrder = CurrentUser.Company.NAICSortedByAlpha ? "Description|Code" : "Code|Description";

            if (_naicDisplyCodeDescription)
                cboNAIC.DataBind(NAICcodes, "Code", "{0} - {1}", naicDisplayOrder.Split('|'));
            else
                cboNAIC.DataBind(NAICcodes, "Code", "{1} - {0}", naicDisplayOrder.Split('|'));

            cboNAIC.IsRequired = _requiresNAICCode;

            Underwriter[] underwriters = AppCache.GetUnderwriters(this, CurrentUser.Company, "ActiveUnderwriters", true);
            cboUnderwriter.DataBind(underwriters, "Code", "Name");                        
            cboUnderwriter2.DataBind(underwriters, "Code", "Name");

            if (!string.IsNullOrEmpty(_eSurvey.UnderwriterCode))
            {
                Underwriter underwriter = Underwriter.GetOne("Code = ? && CompanyID = ?", _eSurvey.UnderwriterCode, CurrentUser.CompanyID);
                ListItem item = new ListItem((underwriter != null) ? underwriter.Name : _eSurvey.UnderwriterCode, _eSurvey.UnderwriterCode);
                if (!cboUnderwriter.Items.Contains(item))
                {
                    cboUnderwriter.Items.Add(item);
                }
            }

            if (!string.IsNullOrEmpty(_eSurvey.Underwriter2Code))
            {
                Underwriter underwriter2 = Underwriter.GetOne("Code = ? && CompanyID = ?", _eSurvey.Underwriter2Code, CurrentUser.CompanyID);
                ListItem item = new ListItem((underwriter2 != null) ? underwriter2.Name : _eSurvey.Underwriter2Code, _eSurvey.Underwriter2Code);
                if (!cboUnderwriter2.Items.Contains(item))
                {
                    cboUnderwriter2.Items.Add(item);
                }
            }

            // get all the agency email addresses
            AgencyEmail[] eEmails = AgencyEmail.GetSortedArray("AgencyID = ?", "EmailAddress ASC", _eSurvey.Insured.AgencyID);
            cboAgentEmail.DataBind(eEmails, "ID", "EmailAddress");

            if (_eSurvey.Insured.AgentEmailID != Guid.Empty)
            {
                ListItem item = new ListItem(_eSurvey.Insured.AgencyEmail.EmailAddress, _eSurvey.Insured.AgentEmailID.ToString());
                if (!cboAgentEmail.Items.Contains(item))
                {
                    cboAgentEmail.Items.Add(item);
                }
            }

            //Determine if certain fields need to be required
            txtClientID.IsRequired = (_eSurvey.PrimaryPolicy != null || !String.IsNullOrEmpty(_eSurvey.WorkflowSubmissionNumber));
            cboSIC.IsRequired = CurrentUser.Company.RequireSICCode;
             

            this.PopulateFields(_eSurvey.Insured);
            this.PopulateFields(_eSurvey);

            if (_eSurvey.Insured.Agency != null)
            {
                this.PopulateFields("txt", _eSurvey.Insured.Agency);
                this.PopulateFields("lbl", _eSurvey.Insured.Agency);
            }
            else
            {
                this.lblAgencyName.Text = "(not specified)";
                this.lblAgencyAddress.Text = "(not specified)";
                this.lblAgencyPhone.Text = "(not specified)";
                this.lblAgencyFax.Text = "(not specified)";
            }

            rdoPrivacyAll.Checked = true;
            rdoPrivacyAdministrator.Visible = false;
            if (_setCommentsInternalOnly && CurrentUser.Company.LongAbbreviation != "USIG")
            {
                rdoPrivacyInternal.Checked = true;
            }
            if (_enableAdministrativeNotes && _userCanViewAdministrativeNotes)
            {
                rdoPrivacyAdministrator.Visible = true;
            }
            //Display any notes
            SurveyNote eNote = SurveyNote.GetOne("SurveyID = ?", _eSurvey.ID);
            if (eNote != null)
            {
                txtComments.Text = eNote.Comment;
                rdoPrivacyInternal.Checked = eNote.InternalOnly;
                rdoPrivacyAdministrator.Checked = eNote.AdminOnly;
            }
          

            // restore sort expression, reverse state
            this.ViewState["SortExpression"] = this.UserIdentity.GetPageSetting("SortExpression", grdLocations.Columns[0].SortExpression);
            this.ViewState["SortReversed"] = this.UserIdentity.GetPageSetting("SortReversed", false);
            grdLocations.CurrentPageIndex = (int)this.UserIdentity.GetPageSetting("PageIndex", 0);
            chkOneSurveyForAllLocations.Checked = (bool)this.UserIdentity.GetPageSetting(_eSurvey.ID.ToString(), CurrentUser.Company.DefaultLocationsToOneSurvey);

            //Added By Vilas Meshram: 03/01/2013: Squish# 21365: Dropdown for priority
            PopulatePriorityChange();

            if (_canAssignConsultant) {
                // Populate a list of Consultants
                GetListOfConsultants();
            }
            
        }
        //Added By Vilas Meshram: 03/01/2013: Squish# 21365: Dropdown for priority, Bind Priority Images with combo box
        BindImagePriority();

        //PopulatePriorityChange();
    }

    void CreateSurveyAspx_PreRender(object sender, EventArgs e)
    {
        string sSortExp = (string)this.ViewState["SortExpression"];
        if ((bool)this.ViewState["SortReversed"])
        {
            sSortExp = sSortExp.Replace(" DESC", " TEMP");
            sSortExp = sSortExp.Replace(" ASC", " DESC");
            sSortExp = sSortExp.Replace(" TEMP", " ASC");
        }

        // get all the locations for this insured
        VWLocation[] eLocations = VWLocation.GetSortedArray("InsuredID = ? && IsActive = true", sSortExp, (_eSurvey != null ? _eSurvey.InsuredID : Guid.Empty));
        DataGridHelper.BindPagingGrid(grdLocations, eLocations, "LocationID", "There are no locations for this insured.");

        // Determine if the copy coverages features is ready/enabled
        VWLocation[] completedLocations = VWLocation.GetArray("InsuredID = ? && LocationExclusionCount = 0 && ActivePolicyCount > 0 && ActivePolicyCount != PolicyExclusionCount && ActivePolicyCount = (PolicyExclusionCount + PolicyBeenViewedCount)", _eSurvey.InsuredID);
        divCopyCoverages.Visible = (completedLocations.Length == 1 && eLocations.Length > 1);

        // get all documents associated to this survey request
        SurveyDocument[] eDocs = SurveyDocument.GetSortedArray("SurveyID = ? && IsRemoved = False", "UploadedOn DESC", (_eSurvey != null ? _eSurvey.ID : Guid.Empty));
        repDocs.DataSource = eDocs;
        repDocs.DataBind();
        boxDocuments.Title = string.Format("Supporting Documents ({0})", eDocs.Length);

        // get all the doc types for the company
        DocType[] docTypes = DocType.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", this.CurrentUser.CompanyID);
        ComboHelper.BindCombo(cboDocType, docTypes, "Code", "{1} - {0}", "Code|Description".Split('|'));
        if (docTypes.Length > 0)
        {
            cboDocType.Items.Insert(0, new ListItem("-- select doc type --", ""));
        }

        //Check company parameters
        if (Utility.GetCompanyParameter("HideOneSurveyAllLocations", this.CurrentUser.Company).ToLower() == "true")
            chkOneSurveyForAllLocations.Visible = false;
    }

    private void SetRequirements()
    {
        //txtAgentContactName.IsRequired = CurrentUser.Company.AgentDataIsRequied;
        if (_requireInsuredContact)    
        {
            txtContactName.IsRequired = true;
            txtContactPhone.IsRequired = true;
        }
        
    }

    void grdLocations_SortCommand(object source, DataGridSortCommandEventArgs e)
    {
        // get current sort expression and reverse the sort if same as new one
        string sSortExp = (string)this.ViewState["SortExpression"];
        bool sSortRev = (bool)this.ViewState["SortReversed"];
        sSortRev = !sSortRev && (sSortExp == e.SortExpression);

        this.ViewState["SortExpression"] = e.SortExpression;
        this.ViewState["SortReversed"] = sSortRev;

        this.UserIdentity.SavePageSetting("SortExpression", e.SortExpression);
        this.UserIdentity.SavePageSetting("SortReversed", sSortRev);

        JavaScript.SetInputFocus(boxLocations);
    }

    void grdLocations_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        grdLocations.CurrentPageIndex = e.NewPageIndex;
        this.UserIdentity.SavePageSetting("PageIndex", e.NewPageIndex);

        JavaScript.SetInputFocus(boxLocations);
    }

    void repDocs_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            SurveyDocument eDoc = (SurveyDocument)e.Item.DataItem;
            LinkButton lnkDownload = (LinkButton)e.Item.FindControl("lnkDownloadDoc");
            LinkButton lnkRemoveDoc = (LinkButton)e.Item.FindControl("lnkRemoveDoc");
            HtmlAnchor lnkDownloadDocInNewWindow = (HtmlAnchor)e.Item.FindControl("lnkDownloadDocInNewWindow");
            lnkDownload.Attributes.Add("DocumentID", eDoc.ID.ToString());
            lnkRemoveDoc.Attributes.Add("DocumentID", eDoc.ID.ToString());

            //determine which link to display
            lnkDownload.Visible = !eDoc.MimeType.Contains("htm");
            lnkDownloadDocInNewWindow.Visible = eDoc.MimeType.Contains("htm");

            JavaScript.AddConfirmationNoticeToWebControl(lnkRemoveDoc, string.Format("Are your sure you want to remove '{0}'?", eDoc.DocumentName));
        }
    }

    void grdLocations_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "REMOVE")
        {
            Guid gLocationID = (Guid)grdLocations.DataKeys[e.Item.ItemIndex];
            Location eLocation = Location.Get(gLocationID).GetWritableInstance();

            // execute the requested transformation on the policy
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //first remove any reference to the policy in our cache tables
                SurveyLocationPolicyCoverageName[] reports = SurveyLocationPolicyCoverageName.GetArray("SurveyID = ? && LocationID = ?", _eSurvey.ID, eLocation.ID);
                foreach (SurveyLocationPolicyCoverageName report in reports)
                {
                    report.Delete(trans);
                }

                SurveyLocationPolicyExclusion[] exclusions = SurveyLocationPolicyExclusion.GetArray("SurveyID = ? && LocationID = ?", _eSurvey.ID, eLocation.ID);
                foreach (SurveyLocationPolicyExclusion exclusion in exclusions)
                {
                    exclusion.Delete(trans);
                }

                eLocation.IsActive = false;
                eLocation.Save(trans);

                trans.Commit();
            }

            JavaScript.SetInputFocus(boxLocations);
        }

        //Response.Redirect(UrlBuilder("CreateSurvey.aspx"), true);
    }

    void grdLocations_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            // get the location
            VWLocation eLocation = (VWLocation)e.Item.DataItem;

            //CheckBox
            CheckBox chk = (CheckBox)e.Item.FindControl("chkLocation");
            chk.Checked = true;

            //LinkButton
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkCompletionStatus");

            //Add attribute as an id for retrieving via oncheckedchange event
            chk.Attributes.Add("LocationID", eLocation.LocationID.ToString());
            lnk.Attributes.Add("LocationID", eLocation.LocationID.ToString());

            //Determine which status image to display
            HtmlImage img = (HtmlImage)e.Item.FindControl("imgCompletionStatus");

            if (eLocation.LocationExcluded)
            {
                img.Src = ARMSUtility.AppPath + "/images/dash.gif";
                lnk.Attributes.Add("flagHref", "1");
                chk.Checked = false;
            }
            else if (eLocation.NoActivePolicies)
            {
                img.Alt = "Must enter at least one policy for this location.";
                img.Src = ARMSUtility.AppPath + "/images/xmark.gif";
            }
            else if (eLocation.AllPoliciesExcluded)
            {
                img.Alt = "Must include at least one policy for this location.";
                img.Src = ARMSUtility.AppPath + "/images/xmark.gif";
            }
            else if (eLocation.NoContactsAdded)
            {
                img.Alt = "Must enter at least one contact for this location.";
                img.Src = ARMSUtility.AppPath + "/images/xmark.gif";
            }
            else if (eLocation.NotAllPolicyReportsSelected)
            {
                img.Alt = "Must select your reports for each policy in this location.";
                img.Src = ARMSUtility.AppPath + "/images/xmark.gif";
            }
            else
            {
                img.Src = ARMSUtility.AppPath + "/images/check.gif";
            }

            chk.Attributes.Add("onclick", string.Format("SetExclusion(this.checked, '{0}', '{1}', '{2}', '{3}', '{4}');", 
                _eSurvey.ID.ToString(), eLocation.LocationID.ToString(), this.CurrentUser.CompanyID.ToString(), 
                lnk.ClientID, img.ClientID));

        }
    }

    void grdLocations_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        // we only care about datagrid items
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //Event Handler for linkbutton
            LinkButton lnk = (LinkButton)e.Item.FindControl("lnkCompletionStatus");
            lnk.Click += new EventHandler(lnkCompletionStatus_Click);
        }
        else if (e.Item.ItemType == ListItemType.Pager)
        {
            ARMSUtility.PagingGrid_ItemCreated(sender, e);
        }
    }

    void lnkSelectAll_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        DB.Engine.ExecuteScalar(string.Format("UPDATE Survey_LocationPolicyExclusion SET LocationExcluded = 0 WHERE SurveyID = '{0}'", _eSurvey.ID));
        JavaScript.SetInputFocus(boxLocations);
    }

    void lnkUnselectAll_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        DB.Engine.ExecuteScalar(string.Format("UPDATE Survey_LocationPolicyExclusion SET LocationExcluded = 1 WHERE SurveyID = '{0}'", _eSurvey.ID));
        JavaScript.SetInputFocus(boxLocations);
    }

    void lnkCompletionStatus_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        Guid gLocationID = new Guid(lnk.Attributes["LocationID"]);

        //commit changes
        if (SaveInsured())
        {
            Response.Redirect(UrlBuilder("CreateSurveyLocation.aspx", "locationid", gLocationID), true);
        }
    }

    protected string GetFileNameWithSize(object oDataItem)
    {
        SurveyDocument eDoc = (SurveyDocument)oDataItem;
        string result = string.Format("{0} ({1:#,##0} KB)", eDoc.DocumentName, eDoc.SizeInBytes / 1024);

        return Server.HtmlEncode(result);
    }

    protected string GetUserName(object obj)
    {
        User user = (obj as User);
        return Server.HtmlEncode((user != null) ? user.Name : "System");
    }

    protected string UrlBuilder(string webPage)
    {
        return UrlBuilder(webPage, null, null);
    }

    protected string UrlBuilder(string sWebPage, string sKey, object oValue)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat(sWebPage);
        sb.AppendFormat("?surveyid={0}", _eSurvey.ID);

        if (sKey != null && sKey.Length > 0)
        {
            sb.AppendFormat("&{0}={1}", sKey, oValue);
        }

        return sb.ToString();
    }

    void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(GetReturnUrl(), true);
    }

    void lnkAddLocation_Click(object sender, EventArgs e)
    {
        if (SaveInsured())
        {
            Location eLocation = new Location(Guid.NewGuid());
            
            //commit changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //Create a new location and save to the db
                eLocation.InsuredID = _eSurvey.InsuredID;

                //Query the db to determine the appropriate default location number
                object tempNumber = DB.Engine.ExecuteScalar(string.Format("SELECT TOP 1 LocationNumber FROM VW_Location WHERE InsuredID = '{0}' AND IsActive = 1 ORDER BY LocationNumber DESC", _eSurvey.InsuredID));
                int number = (tempNumber != null) ? (Int32)tempNumber : 0;

                //Location[] eLocations = Location.GetSortedArray("InsuredID = ?", "Number DESC", _eSurvey.InsuredID);
                eLocation.Number = number + 1;

                //By default, try and add a primary contact from a previous location
                LocationContact eContact = LocationContact.GetOne("PriorityIndex = ? && Location.InsuredID = ?", 1, _eSurvey.InsuredID);
                if (eContact != null)
                {
                    LocationContact newContact = new LocationContact(Guid.NewGuid());
                    newContact.LocationID = eLocation.ID;
                    newContact.Name = eContact.Name;
                    newContact.Title = eContact.Title;
                    newContact.PhoneNumber = eContact.PhoneNumber;
                    newContact.AlternatePhoneNumber = eContact.AlternatePhoneNumber;
                    newContact.FaxNumber = eContact.FaxNumber;
                    newContact.EmailAddress = eContact.EmailAddress;
                    newContact.StreetLine1 = eContact.StreetLine1;
                    newContact.StreetLine2 = eContact.StreetLine2;
                    newContact.City = eContact.City;
                    newContact.StateCode = eContact.StateCode;
                    newContact.ZipCode = eContact.ZipCode;
                    newContact.PriorityIndex = eContact.PriorityIndex;

                    newContact.Save(trans);
                }

                // commit changes
                eLocation.Save(trans);

                Policy[] policies = Policy.GetArray("InsuredID = ? AND IsActive = True", _eSurvey.InsuredID);
                foreach (Policy policy in policies)
                {
                    SurveyLocationPolicyExclusion locationPolicy = new SurveyLocationPolicyExclusion(_eSurvey.ID, eLocation.ID, policy.ID);
                    locationPolicy.LocationExcluded = false;
                    locationPolicy.PolicyExcluded = false;
                    locationPolicy.PolicyViewed = (policy.LineOfBusiness == Berkley.BLC.Entities.LineOfBusiness.CommercialUmbrella);

                    locationPolicy.Save(trans);
                }

                trans.Commit();
            }

            Response.Redirect(UrlBuilder("CreateSurveyLocation.aspx", "locationid", eLocation.ID), true);
        }
    }

    void lnkDocumentRetriever_Click(object sender, EventArgs e)
    {
        if (SaveInsured())
        {
            Response.Redirect(UrlBuilder("DocumentRetriever.aspx"), true);
        }
    }

    protected void btnRemoveLocation_PreRender(object sender, EventArgs e)
    {
        JavaScript.AddConfirmationNoticeToWebControl((WebControl)sender, "Are you sure you want to remove this location?");
    }

    private bool SaveInsured()
    {
        try
        {
            //commit changes
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                //save insured data
                Insured eInsured = _eSurvey.Insured.GetWritableInstance();

                //Tie the agency id to the insured
                Agency eAgency = Agency.GetOne("CompanyID = ? && (Number = ? || Number = ? || Number = ?)", this.CurrentUser.CompanyID, txtAgencyNumber.Text.PadLeft(5, '0'), txtAgencyNumber.Text.TrimStart('0'), txtAgencyNumber.Text);
                if (eAgency == null)
                {
                    eAgency = new Agency(Guid.NewGuid());
                    eAgency.CompanyID = CurrentUser.CompanyID;
                    
                    this.PopulateEntity("txt", eAgency);
                    eAgency.Save(trans);
                }

                if (string.IsNullOrWhiteSpace(eAgency.PhoneNumber.ToString()) && txtAgencyPhone.Text.Length > 0)
                {
                    if (eAgency.IsReadOnly)
                        eAgency = eAgency.GetWritableInstance();
                    eAgency.PhoneNumber = txtAgencyPhone.Text;
                    eAgency.Save(trans);
                }

                eInsured.AgencyID = eAgency.ID;

                this.PopulateEntity(eInsured);
                if (string.IsNullOrWhiteSpace(eInsured.Underwriter) && !string.IsNullOrWhiteSpace(this.cboUnderwriter.SelectedValue))
                    eInsured.Underwriter = this.cboUnderwriter.SelectedValue;
                eInsured.Save(trans);

                //Commit due date, if entered
                Survey eSurvey = _eSurvey.GetWritableInstance();
                this.PopulateEntity(eSurvey);

                //save the Priority
                eSurvey.Priority= GetSelectedPriority();

                // Save the Assined Consultant to the _eSurvey at this point.
                if (_canAssignConsultant)
                {
                    if (cboConsultants.SelectedValue != "")
                    {
                        eSurvey.AssignedUserID = new Guid(cboConsultants.SelectedValue);
                    }
                }

                //set InHouseSelection value
                if (_usesInHouseSelection)
                {
                    eSurvey.InHouseSelection = GetInHouseSelection();
                }
                
                eSurvey.Save(trans);

                _eSurvey = eSurvey;

                //Add or Update comments
                SurveyNote eNote = SurveyNote.GetOne("SurveyID = ?", _eSurvey.ID);
                if (txtComments.Text.Length > 0 || eNote != null)
                {
                    if (eNote == null) //Insert
                    {
                        eNote = new SurveyNote(new Guid());
                        eNote.SurveyID = _eSurvey.ID;
                        eNote.UserID = (!this.CurrentUser.IsUnderwriter) ? this.CurrentUser.ID : Guid.Empty;
                        eNote.UserName = (this.CurrentUser.IsUnderwriter && UserIdentity != null) ? UserIdentity.Name : string.Empty;
                        //eNote.InternalOnly = false;
                        //eNote.AdminOnly = false;
                        eNote.InternalOnly = rdoPrivacyInternal.Checked; ;
                        eNote.AdminOnly = rdoPrivacyAdministrator.Checked;
                        eNote.NoteBySurveyCreator = true;
                    }
                    else //Update
                    {
                        eNote = eNote.GetWritableInstance();
                        eNote.InternalOnly = rdoPrivacyInternal.Checked; ;
                        eNote.AdminOnly = rdoPrivacyAdministrator.Checked;
                    }

                    eNote.EntryDate = DateTime.Now;
                    eNote.Comment = txtComments.Text;
                    eNote.Save(trans);
                }

                trans.Commit();

                //save the check for locations
                this.UserIdentity.SavePageSetting(_eSurvey.ID.ToString(), chkOneSurveyForAllLocations.Checked);
            }
        }
        catch (ValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return false;
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return false;
        }

        return true;
    }

    private string GetSelectedPriority()
    {
        //Modified By Vilas Meshram: 03/01/2013: Squish# 21365: Dropdown for priority
        if (SurveyAssignPriority == "true" && ddlSurveyPriority.Items.Count > 0)
            return ddlSurveyPriority.SelectedValue;
        return string.Empty;
        
        /*
        foreach (ListItem item in rblPriority.Items.Cast<ListItem>().Where(item => item.Selected))
            return item.Value;

        if (SurveyAssignPriority == "true" && rblPriority.Items.Count > 0)
            return rblPriority.Items[0].Value;
        return string.Empty;
       */

        //Modified By Vilas Meshram: 03/01/2013: Squish# 21365: Dropdown for priority
      
    }

    private bool GetInHouseSelection()
    {
        if (rdoInHouseSelectionInHouse.Checked == true)
            return true;

        return false;
    }

    void btnSubmit_Click(object sender, EventArgs e)
    {
        //Check if a due date was entered or agent name
        if (dtDateDue.Date == DateTime.MinValue || (txtAgentContactName.Text.Length <= 0 && CurrentUser.Company.AgentDataIsRequied) 
            || (cboAgentEmail.SelectedIndex == 0 && CurrentUser.Company.AgentDataIsRequied && _agentEmailRequired) 
            || (string.IsNullOrEmpty(txtComments.Text) && CurrentUser.Company.RequireCommentOnCreateSurvey))
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Please correct the following:");

            if (txtAgentContactName.Text.Length <= 0 && CurrentUser.Company.AgentDataIsRequied)
            {
                sb.AppendLine("- Agent Name is required.");
            }

            if (cboAgentEmail.SelectedIndex == 0 && CurrentUser.Company.AgentDataIsRequied && _agentEmailRequired)
            {
                sb.AppendLine("- Agent Email is required.");
            }

            if (dtDateDue.Date == DateTime.MinValue)
            {
                sb.AppendLine("- Due Date is required.");
            }

            if (string.IsNullOrEmpty(txtComments.Text) && CurrentUser.Company.RequireCommentOnCreateSurvey)
            {
                sb.AppendLine("- Comments are required.");
            }



            JavaScript.ShowMessage(this, sb.ToString());
            JavaScript.SetInputFocus(btnSubmit);
            return;
        }

        //if (!this.IsValid)
        //{
        //    return;
        //}
        
        //Make sure at least one location is selected and all the coverages have been reported
        bool oneSelected = false;
        foreach (DataGridItem locationItem in grdLocations.Items)
        {
            if (locationItem.ItemType == ListItemType.Item || locationItem.ItemType == ListItemType.AlternatingItem)
            {
                HtmlImage img = (HtmlImage)locationItem.FindControl("imgCompletionStatus");
                CheckBox chk = (CheckBox)locationItem.FindControl("chkLocation");

                if (chk.Checked)
                {
                    VWLocation eLocation = VWLocation.Get(new Guid(chk.Attributes["LocationID"]));

                    if (eLocation.NoActivePolicies)
                    {
                        JavaScript.ShowMessage(this, string.Format("Location #{0} must have at least one policy entered if the location is to be included.", eLocation.LocationNumber));
                        return;
                    }
                    else if (eLocation.AllPoliciesExcluded)
                    {
                        JavaScript.ShowMessage(this, string.Format("Location #{0} must include at least one policy if the location is to be included.", eLocation.LocationNumber));
                        return;
                    }
                    else if (eLocation.LocationExcluded || eLocation.NotAllPolicyReportsSelected)
                    {
                        JavaScript.ShowMessage(this, string.Format("Location #{0} has policies that need to have reports selected if this location is to be included.", eLocation.LocationNumber));
                        return;
                    }
                    else if (eLocation.NoContactsAdded)
                    {
                        JavaScript.ShowMessage(this, string.Format("Location #{0} must have a contact entered if the location is to be included.", eLocation.LocationNumber));
                        return;
                    }
                    else
                    {
                        oneSelected = true;
                    }
                }
            }
        }
        Policy[] allPolicies = Policy.GetArray("InsuredID = ? && IsActive = true", _eSurvey.InsuredID);

        foreach (Policy policy in allPolicies)
        {
            if ((policy.EffectiveDate > DateTime.Now) && _validatePolicyEffectiveDate)
            {

                JavaScript.ShowMessage(this, "Cannot include Policy " + policy.LineOfBusinessCode + policy.Number + " on this survey until the policy's effective date of  " + policy.EffectiveDate + ".");
                return;
            }
        }

        //Short circuit if one location hasn't been selected
        if (!oneSelected)
        {
            JavaScript.ShowMessage(this, "At least one location must be selected and complete.");
            return;
        }

        if (SaveInsured())
        {
            try
            {
                //Create the survey
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
                var priority = GetSelectedPriority();
                manager.CreateSurvey(dtDateDue.Date, chkOneSurveyForAllLocations.Checked, (_eSurvey.PrimaryPolicy == null && string.IsNullOrEmpty(_eSurvey.WorkflowSubmissionNumber)), priority);
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);
                throw (ex);
            }

            //if we got this far, then remove the cached survey request
            DeleteSurvey(0);

            Response.Redirect("MySurveys.aspx?surveycount=1", true);
        }
    }

    private string GetReturnUrl()
    {
        return "CreateSurveySearch.aspx";
    }

    private void DeleteSurvey(int attempts)
    {
        if (attempts < 5)
        {
            try
            {
                DB.Engine.ExecuteCommand(string.Format("EXEC Survey_delete @SurveyID='{0}'", _eSurvey.ID));
            }
            catch
            {
                Thread.Sleep(1000);
                DeleteSurvey(attempts + 1);
            }
        }
        else
        {
            throw new Exception("Attempted to remove cached survey 5 times without success.");
        }
    }

    void btnAddEmail_Click(object sender, EventArgs e)
    {
        //commit changes
        AgencyEmail eEmail;
        using (Transaction trans = DB.Engine.BeginTransaction())
        {
            // Insert new email
            eEmail = new AgencyEmail(Guid.NewGuid());
            eEmail.AgencyID = _eSurvey.Insured.AgencyID;
            eEmail.EmailAddress = txtNewAgencyEmail.Text;
            eEmail.Save(trans);

            // Tie the new email to the insured
            Insured eInsured = _eSurvey.Insured.GetWritableInstance();
            eInsured.AgentContactName = txtAgentContactName.Text;
            eInsured.AgentContactPhoneNumber = txtAgentContactPhone.Text;
            eInsured.AgentEmailID = eEmail.ID;
            eInsured.Save(trans);

            trans.Commit();
        }

        // Update the email list
        AgencyEmail[] eEmails = AgencyEmail.GetSortedArray("AgencyID = ?", "EmailAddress ASC", _eSurvey.Insured.AgencyID);
        cboAgentEmail.DataBind(eEmails, "ID", "EmailAddress");

        // Set the new as selected by default
        cboAgentEmail.SelectedValue = eEmail.ID.ToString();
        txtNewAgencyEmail.Text = string.Empty;
    }

    protected void lnkDownloadDoc_OnClick(object sender, EventArgs e)
    {
        try
        {
            LinkButton lnk = (LinkButton)sender;
            SurveyDocument doc = SurveyDocument.Get(new Guid(lnk.Attributes["DocumentID"]));

            StreamDocument docRetriever = new StreamDocument(doc, CurrentUser.Company);
            docRetriever.AttachDocToResponse(this);
        }
        catch (System.Web.HttpException ex)
        {
            //ignore http exceptions for when the user quits a doc download and the stream doesn't get flushed
            if (ex.Message.Contains("The remote host closed the connection"))
            {
                //swallow exception
            }
            else
            {
                throw new Exception("See inner exception for details.", ex);
            }
        }
    }

    protected void lnkRemoveDoc_OnClick(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        SurveyDocument doc = SurveyDocument.Get(new Guid(lnk.Attributes["DocumentID"]));

        //try and remove the document from FileNet
        if (doc.FileNetRemovable)
        {
            ImagingHelper imaging = new ImagingHelper(CurrentUser.Company);
            imaging.RemoveFileImage(doc.FileNetReference);
        }

        // mark the doc as deleted in ARMS
        SurveyDocument removedDoc = doc.GetWritableInstance();
        removedDoc.IsRemoved = true;
        removedDoc.Save();

        JavaScript.SetInputFocus(boxDocuments);
    }

    void btnAttachDocument_Click(object sender, EventArgs e)
    {
        HttpPostedFile postedFile = fileUploader.PostedFile;

        try
        {
            // make sure a filename was uploaded
            if (postedFile == null || postedFile.FileName.Length == 0) // no filename was specified
            {
                throw new FieldValidationException(fileUploader, "Please select a file to submit.");
            }

            // make sure the file name is not larger the max allowed
            string fileName = System.IO.Path.GetFileName(postedFile.FileName);
            if (fileName.Length > 200)
            {
                throw new FieldValidationException(fileUploader, "The file name cannot exceed 200 characters.");
            }

            // check for an extesion
            if (System.IO.Path.GetExtension(fileName).Length <= 0)
            {
                throw new FieldValidationException(fileUploader, "The file name must contain a file extension.");
            }

            // make sure the file has content
            int max = int.Parse(ConfigurationManager.AppSettings["MaxRequestLength"]);
            if (postedFile.ContentLength <= 0) // the file is empty
            {
                throw new FieldValidationException(fileUploader, "The file submitted is empty.");
            }
            else if (postedFile.ContentLength > max)
            {
                // the maximum file size is 10 Megabytes
                throw new FieldValidationException(fileUploader, "The file size cannot be larger than 10 Megabytes.");
            }

            // check if the company has doc types and if one was selected
            if (cboDocType.Items.Count > 0 && cboDocType.SelectedValue == "" && CurrentUser.Company.RequireDocTypeOnCreateSurvey)
            {
                throw new FieldValidationException("Please select a doc type.");
            }
        }
        catch (FieldValidationException ex)
        {
            this.ShowErrorMessage(ex);
            return;
        }

        // attach this file to the survey
        try
        {
            DocType docType = DocType.GetOne("Code = ?", cboDocType.SelectedValue);
            
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser, true);
            manager.AttachDocument(postedFile.FileName, postedFile.ContentType, postedFile.InputStream, docType, txtDocRemarks.Text, true);
        }
        catch (Exception ex)
        {
            //ExceptionManager.Publish(ex);
            throw (ex);
        }

        JavaScript.SetInputFocus(boxDocuments);
    }

    void lnkCopyCoverages_Click(object sender, EventArgs e)
    {
        VWLocation[] completedLocations = VWLocation.GetArray("InsuredID = ? && ActivePolicyCount > 0 && ActivePolicyCount != PolicyExclusionCount && ActivePolicyCount = (PolicyExclusionCount + PolicyBeenViewedCount)", _eSurvey.InsuredID);
        if (completedLocations.Length == 1)
        {
            //copy over the selected coverages
            DB.Engine.ExecuteCommand(string.Format("EXEC CreateSurvey_CopyCoverages @SurveyID='{0}', @LocationID='{1}'", _eSurvey.ID, completedLocations[0].LocationID));

            //Then update all of the policy locations as being viewed based on the completed location
            SurveyLocationPolicyExclusion[] policyLocations = SurveyLocationPolicyExclusion.GetArray("LocationID = ?", completedLocations[0].LocationID);
            foreach (SurveyLocationPolicyExclusion policyLocation in policyLocations)
            {
                Guid policyID = policyLocation.PolicyID;
                int policyExcluded = (policyLocation.PolicyExcluded) ? 1 : 0;
                int policyViewed = (policyLocation.PolicyViewed) ? 1 : 0;

                DB.Engine.ExecuteScalar(string.Format("UPDATE Survey_LocationPolicyExclusion SET PolicyExcluded = {0},  PolicyViewed = {1} WHERE PolicyID = '{2}'", policyExcluded, policyViewed, policyID));
            }

            //Redirect at the end so the user doesn't post-back with the refresh button
            Response.Redirect(UrlBuilder("CreateSurvey.aspx"), true);
        }
        else
        {
            JavaScript.ShowMessage(this, "Only one location can be complete to perform this action.");
        }
    }
    
    //Added By Vilas Meshram: 03/01/2013: Squish# 21365: Dropdown for priority:
    private void PopulatePriorityChange()
    {
        var selectedPriority = _eSurvey.Priority;
        PriorityRating[] priorities = PriorityRating.GetSortedArray("CompanyID = ?", "DisplayOrder ASC, Name ASC", CurrentUser.CompanyID);

        ddlSurveyPriority.DataTextField = "Description";
        ddlSurveyPriority.DataValueField = "Name";
        ddlSurveyPriority.DataSource = priorities;
        ddlSurveyPriority.DataBind();

        foreach (ListItem item in ddlSurveyPriority.Items)
            if (item.Value == selectedPriority)
            {
                ddlSurveyPriority.SelectedValue = selectedPriority;
                break; 
            }
    }


    /// Commented By Vilas Meshram: 03/01/2013: Squish# 21365: Dropdown for priority:
    //private void PopulatePriorityChange()
    //{
    //    var selectedPriority = _eSurvey.Priority;
    //    foreach (ListItem item in rblPriority.Items.Cast<ListItem>().Where(item => item.Selected))
    //        selectedPriority = item.Value;//persist previously selected item
    //    PriorityRating[] priorities = PriorityRating.GetSortedArray("CompanyID = ?", "DisplayOrder ASC, Name ASC", CurrentUser.CompanyID);
    //    var colors = (from p in priorities select p.Name).ToArray();
    //    rblPriority.DataSource = colors;
    //    rblPriority.DataBind();
    //    /*20121113 Dustin: displaying a list of images and allowing a user to select 1 was challenging.
    //     * while in a repeater control, radio buttons would not work properly, even if you applied the GroupName attribute you could select more than 1.
    //     * A radio button list using a background image was the best thing I could find with a little looking.  It's a hack, but it's effective.
    //     * I'm trying to be mindful of the time I spend on this problem.
    //   */
    //    foreach (ListItem item in rblPriority.Items)
    //    {
    //        var url = item.Text;
    //        item.Text = "..";//this was needed to make sure the image wasn't behind the selector button
    //        item.Selected = selectedPriority.Equals(url);

    //        item.Attributes.CssStyle.Add("background-image", "url(" + url + ")");
    //        item.Attributes.CssStyle.Add("background-repeat", "no-repeat");
    //        item.Attributes.CssStyle.Add("background-position", "right top");
    //        item.Attributes.CssStyle.Add("font-size", "300%");//another hack, to make sure the images have enough height and are not covered with the text.
    //        item.Attributes.CssStyle.Add("color", "white");
    //    }
    //}

    private void GetListOfConsultants()
    {
        // populate combo with all consultants
        User[] eUsers;

        //filter
        StringBuilder sb = new StringBuilder();
        sb.Append("AccountDisabled = false && ");
        sb.Append("(Role[PermissionSet.SurveyActionPermissions[");
        sb.Append("(Setting = ? || Setting = ? || Setting = ? || Setting = ?) ]]) ");
        
        //params
        ArrayList args = new ArrayList();
        args.Add(PermissionSet.SETTING_OWNER);
        args.Add(PermissionSet.SETTING_OWNER_AND_OTHERS);
        args.Add(PermissionSet.SETTING_OWNER_AND_VENDORS);
        args.Add(PermissionSet.SETTING_ALL);
        eUsers = this.CurrentUser.Company.GetConsultants(sb.ToString(), args.ToArray());

        ComboHelper.BindCombo(cboConsultants, eUsers, "ID", "Name");
        cboConsultants.Items.Insert(0, new ListItem("System Selection", ""));

    }

    /// <summary>
    /// This Method is used to bind titles to each element of dropdown
    /// Added By Vilas Meshram: 03/01/2013: Squish# 21365: Dropdown for priority:
    /// </summary>
    protected void BindImagePriority()
    {
        if (ddlSurveyPriority != null)
        {
            foreach (ListItem li in ddlSurveyPriority.Items)
            {
                li.Attributes["title"] = li.Value.Substring(2); // setting text value of item as tooltip
            }
        }
    }
}
