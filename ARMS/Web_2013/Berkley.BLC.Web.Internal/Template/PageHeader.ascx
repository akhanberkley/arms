<%@ Control Language="C#" AutoEventWireup="false" CodeFile="PageHeader.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Template.PageHeader" %>
<%@ Register TagPrefix="uc" TagName="Tabs" Src="Tabs.ascx" %>
<!-- BEGIN PAGE HEADER -->
 <div id="divFontResizer" runat="server" style="float:right; position:relative; padding-top:59px; padding-right:10px; z-index:100">
    <script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/javascripts/jquery-1.4.2.js"></script>
    <script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/javascripts/jquery.cookie.js"></script>
    <a style="color:White" class="jfontsize-button" id="jfontsize-small" href="#">A-</a>&nbsp;|&nbsp;
    <a style="color:White"  class="jfontsize-button" id="jfontsize-default" href="#">Reset</a>&nbsp;|&nbsp;
    <a style="color:White"  class="jfontsize-button" id="jfontsize-increase" href="#">A+</a>
 
    <script type="text/javascript" language="javascript">

        $("#jfontsize-small").click(function () {
            var min = 9;
            var reset = $('.pageBody TABLE').css('fontSize');
            var size = str_replace(reset, 'px', '');
            size--;

            if (size >= min) {
                resizeMe(size);
                $.cookie('ARMSWebsiteFontSize', size, { expires: 365 });
            }

            return false;
        });

        $("#jfontsize-default").click(function () {
            var size = 11;
            resizeMe(size);
            $.cookie('ARMSWebsiteFontSize', size, { expires: 365 });

            return false;
        });

        $("#jfontsize-increase").click(function () {
            var max = 14;
            var reset = $('.pageBody TABLE').css('fontSize');
            var size = str_replace(reset, 'px', '');
            size++;

            if (size <= max) {
                resizeMe(size);
                $.cookie('ARMSWebsiteFontSize', size, { expires: 365 });
            }

            return false;
        });

        //tags/classes to resizes
        function resizeMe(size) {
            $(".pageBody TABLE").css({ "font-size": size });
            $("INPUT").css({ "font-size": size });
            $(".cbo").css({ "font-size": size });
            $(".txt").css({ "font-size": size });
            $(".lbl").css({ "font-size": size });
            $(".chk").css({ "font-size": size });
            $(".logoCompany").css({ "font-size": size });
            $(".instructions").css({ "font-size": size });
            $(".logout").css({ "font-size": size });
            $(".loggedUser").css({ "font-size": size });
            $(".tabControl TD").css({ "font-size": size });
            $(".pageBody .subheading").css({ "font-size": size });
        } 

        //a string replace function 
        function str_replace(haystack, needle, replacement) {
            var temp = haystack.split(needle);
            return temp.join(replacement);
        } 
    </script>
</div>

<div id="divDefaultFontSize" runat="server"></div>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="position:absolute;">
<tr>
	<td class="logoLine">
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
            <td valign="top" nowrap><img src="<%= _appPath %>/template/images/title.gif" border="0" alt="Account Reporting Management System"><span class="surveyHeader">Loss Control Survey Request</span></td>
			<td width="100%" style="font-size:large"><%= _env %></td>
			<td style="PADDING-RIGHT: 10px" align="right" nowrap>
				<span class="logoCompany"><%= (_user != null) ? _user.CompanyName : "&nbsp;" %></span><br>
				<span class="loggedUser"><%= (_user != null) ? _user.Name : "&nbsp;" %></span><br>
				<% if( _user != null && _user.CanLogout ) { %>
				<div class="logout"><a href="<%= _appPath %>/logout.aspx">Logout</a></div>
				<% } %>
 			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="tabsLine" valign="bottom">
    

<uc:Tabs id="ucTabs" runat="server" />

	</td>
</tr>
<tr>
	<td class="pageBody" width="100%" height="400" valign="top">
<!-- END PAGE HEADER -->
