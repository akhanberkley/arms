/*--Keyoti js--*/
//Version 1.5 (RapidSpell Web assembly version 3.4.3 onwards)
//Copyright Keyoti Inc. 2005-2014
//This code is not to be modified, copied or used without a license in any form.

var rsS85=new Array("",
"popup",
"<style></style>",
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
"if (openerWindow!=null && !openerWindow.closed && openerWindow.",
") openerWindow.",
"(spellCheckFinished)",
"undefined",
"<",
"&lt;",
">",
"&gt;",
"<br>",
"function getPageCoords (el) {\n",
" var coords = {x: 0, y: 0};\n var dd='';",
" do {\ndd+=el.tagName+' '+el.offsetLeft+' '+coords.x+'\\r\\n';",
" coords.x += el.offsetLeft;\n",
" coords.y += el.offsetTop;\n",
"dd+='2 '+el.tagName+' '+el.offsetLeft+' '+coords.x+'\\r\\n'; }\n",
" while ((el = el.offsetParent));\n",
" return coords;\n",
"}\n",
"function scrollIntoView(el) {\n",
" var coords = getPageCoords(el);\n",
" window.scrollTo (coords.x, coords.y);\n",
"</span>",
"style",
"text/css",
"<style>",
"</style>",
"script",
"text/javascript",
"string",
"LINK",
"head",
"href",
"rel",
"stylesheet",
"marginwidth",
"marginheight",
"topmargin",
"leftmargin",
"highlight",
"function",
"?",
"&",
"fMessage=addWord&UserDictionaryFile=",
"&word=",
"&GuiLanguage=",
"<\\s*script([^<])*<([ ])*/([ ])*script([ ])*",
"i",
"<\\s*script([^<])*/([ ])*>",
"<\\s*script([^<])*>",
"<\\s*embed([^<])*>",
"(<([^>])*)on(([^ |^=)*([ |=])*=([ |=])*([^ |^>])*(([^>])*>))",
"$1$3",
"(<([^>])*)([ |=])*=([ |=])*([^ |^>])*javascript:([^ |^>])*(([^>])*>)",
"$1$7",
"<PUBLIC([ ])*:([^<])*<([ ])*/PUBLIC([ ])*",
"<PUBLIC([ ])*:([^<])*/([ ])*>",
"<PUBLIC([ ])*:([^<])*>");																																				var rs_s2=window;var rs_s3=document; var iAed=new Array(), cAed=new Array(); var spellCheckFinished=false; var duplicateWord=false; var showXMLTags=true;
																																						 var userDicFile=rsS85[0]; var mode=rsS85[1]; var callBack=rsS85[0]; var currentWordIndex=0; var ignoreWordIndexes=new Array(); var documentTextBoxStyle=rsS85[2]; var hist = new Array();
																																						 var histPtr=0; var addingWord = false; var refreshResetUnload=false; var keyStr = rsS85[3]; function decode64(input) { var output = rsS85[0]; var chr1, chr2, chr3;
																																						 var enc1, enc2, enc3, enc4; var i = 0; do { enc1 = keyStr.indexOf(input.charAt(i++)); enc2 = keyStr.indexOf(input.charAt(i++)); enc3 = keyStr.indexOf(input.charAt(i++));
																																						 enc4 = keyStr.indexOf(input.charAt(i++)); chr1 = (enc1 << 2) | (enc2 >> 4); chr2 = ((enc2 & 15) << 4) | (enc3 >> 2); chr3 = ((enc3 & 3) << 6) | enc4; output = output + String.fromCharCode(chr1);
																																						 if (enc3 != 64) { output = output + String.fromCharCode(chr2); } if (enc4 != 64) { output = output + String.fromCharCode(chr3); } } while (i < input.length); return output;
																																						 } function rsw_callCorrectionNotifyListener(listener, a, b, c){ var f = decode64(listener); openerWindow[f](a, b, c, interfaceObject.tbName) } function rsw_callAddNotifyListener(listener, a, b, c) { var f = decode64(listener);
																																						 openerWindow[f](a, b, c, interfaceObject.tbName) } function rsw_callFinishListener(listener, openerWindow){ eval(rsS85[4]+decode64(listener)+rsS85[5]+decode64(listener)+rsS85[6]);
																																						 } function onWordAdded(){ addingWord = false; } function updateHistory() { if(enableUndo){ var curChecking = -1; if(openerWindow != null && !openerWindow.closed && typeof (openerWindow.currentlyChecking) != rsS85[7]) curChecking = openerWindow.currentlyChecking;
																																						 var fieldDisEl = rs_s3.getElementById(rsw_fieldDisplayTextLabelID); var elHTML = rsS85[0]; if (fieldDisEl) elHTML = fieldDisEl.innerHTML; if (badWords.length > 0) { hist[histPtr] = [cp(ignoreWordIndexes), currentWordIndex, text, cp(badWords), interfaceObject, curChecking, getIndices(badWords), elHTML, cp(iAed), cp(cAed)];
																																						 histPtr++; } } } function undoChange(){ activateButtons(); if(histPtr>0){ histPtr--; if(hist[histPtr][4]!=interfaceObject)interfaceObject.setText(text); ignoreWordIndexes = hist[histPtr][0];
																																						 currentWordIndex = hist[histPtr][1]; text = hist[histPtr][2]; badWords = hist[histPtr][3]; interfaceObject = hist[histPtr][4]; var curChecking = hist[histPtr][5];
																																						 if(typeof (openerWindow != null && openerWindow.currentlyChecking) != rsS85[7] && curChecking>-1)openerWindow.currentlyChecking = curChecking; setIndices(badWords, hist[histPtr][6]);
																																						 var elHTML = hist[histPtr][7]; var fieldDisEl = rs_s3.getElementById(rsw_fieldDisplayTextLabelID); if(fieldDisEl) fieldDisEl.innerHTML = elHTML; iAed = hist[histPtr][8];
																																						 cAed = hist[histPtr][9]; hist.length = histPtr+1; if(currentWordIndex<0) undoChange(); else refresh(); } } function cp(ary){ var bry = new Array(); for(var i=0; i<ary.length;
																																						 i++) bry[i] = ary[i]; return bry; } function getIndices(bw){ var a = new Array(); for(var i=0; i<bw.length; i++) a[i] = [bw[i].start, bw[i].end]; return a; } function setIndices(bw, a){ for(var i=0;
																																						 i<bw.length; i++){ bw[i].start=a[i][0]; bw[i].end=a[i][1]; } return a; } function change(){ updateHistory(); if(currentWordIndex<badWords.length){ changeWord(currentWordIndex);
																																						 nextWord(); } } function changeAll() { updateHistory(); if (currentWordIndex < badWords.length) { var currentWord = badWords[currentWordIndex].text; var n=changeWord(currentWordIndex);
																																						 cAed[cAed.length]={text:currentWord, r:n}; for (var i = currentWordIndex + 1; i < badWords.length; i++) { if (!ignoreWordIndexes[i] && badWords[i].text == currentWord) { changeWord(i);
																																						 ignoreWordIndexes[i] = true; } } nextWord(); } } function ignoreCurrent(){ updateHistory(); if(currentWordIndex<badWords.length){ nextWord(); } } function ignoreAll(){ updateHistory();
																																						 if (currentWordIndex < badWords.length) { var currentWord = badWords[currentWordIndex].text; iAed[iAed.length]=currentWord; for (var i = currentWordIndex + 1; i < badWords.length;
																																						 i++) { if (!ignoreWordIndexes[i] && badWords[i].text == currentWord) { ignoreWordIndexes[i] = true; } } nextWord(); } } function changeSuggestions(){ var suggestion=document.forms[0].suggestions.options[document.forms[0].suggestions.selectedIndex].text;
																																						 if(suggestion!=noSuggestionsText){ rs_s3.forms[0].word.value=suggestion; } } function deactivateButtons(){ if(rs_s3.forms[0].changeButton!=null) rs_s3.forms[0].changeButton.disabled=true;
																																						 if(rs_s3.forms[0].changeAllButton!=null) rs_s3.forms[0].changeAllButton.disabled=true; if(rs_s3.forms[0].ignoreButton!=null) rs_s3.forms[0].ignoreButton.disabled=true;
																																						 if(rs_s3.forms[0].ignoreAllButton!=null) rs_s3.forms[0].ignoreAllButton.disabled=true; if(rs_s3.forms[0].addButton!=null) rs_s3.forms[0].addButton.disabled=true; } function activateButtons(){ if(rs_s3.forms[0].undoButton!=null) rs_s3.forms[0].undoButton.disabled = histPtr <= 0;
																																						 if(rs_s3.forms[0].changeButton!=null) rs_s3.forms[0].changeButton.disabled=false; if(rs_s3.forms[0].changeAllButton!=null) rs_s3.forms[0].changeAllButton.disabled=false;
																																						 if(rs_s3.forms[0].ignoreButton!=null) rs_s3.forms[0].ignoreButton.disabled=false; if(rs_s3.forms[0].ignoreAllButton!=null) rs_s3.forms[0].ignoreAllButton.disabled=false;
																																						 if(rs_s3.forms[0].addButton!=null) rs_s3.forms[0].addButton.disabled=false; } function nextWord(){ var f=true; while(currentWordIndex++<badWords.length&&ignoreWordIndexes[currentWordIndex]);
																																						 if(currentWordIndex>=badWords.length){ deactivateButtons(); } else { } refresh(); } function textToHtml(t){ if(typeof(rsw_textToHtml)!=rsS85[7]) return rsw_textToHtml(t);
																																						 if(showXMLTags){ var ltexp = new RegExp(rsS85[8]); while(ltexp.test(t)) t = t.replace(ltexp, rsS85[9]); var gtexp = new RegExp(rsS85[10]); while(gtexp.test(t)) t = t.replace(gtexp, rsS85[11]);
																																						 } else { } var newlineexp = new RegExp(newlineRule); while(newlineexp.test(t)) t = t.replace(newlineexp, rsS85[12]); return t; } var rsw_haveCopiedCSS = false; function refresh() { rs_s3.forms[0].undoButton.disabled = histPtr<=0;
																																						 if (refreshResetUnload) { onunload = windowClosing; refreshResetUnload = false; } if(currentWordIndex<badWords.length){ spellCheckFinished=false; var html = rsS85[0];
																																						 var script = rsS85[0]; script += rsS85[13]; script += rsS85[14]; script += rsS85[15]; script += rsS85[16]; script += rsS85[17]; script += rsS85[18]; script += rsS85[19];
																																						 script += rsS85[20]; script += rsS85[21]; script += rsS85[22]; script += rsS85[23]; script += rsS85[24]; script += rsS85[21]; html+=ftr(textToHtml(text.substring(0,badWords[currentWordIndex].start)),scriptFilterLevel);
																																						 html+='<span id="highlight" class="badWordHighlight">'; html+=ftr(text.substring(badWords[currentWordIndex].start,badWords[currentWordIndex].end),scriptFilterLevel);
																																						 html+=rsS85[25]; html+=ftr(textToHtml(text.substring(badWords[currentWordIndex].end,text.length)),scriptFilterLevel); if (!rsw_haveCopiedCSS) { var documentTextBoxStyleEl = documentTextPanel.document.createElement(rsS85[26]);
																																						 documentTextBoxStyleEl.type = rsS85[27]; if (documentTextBoxStyleEl.styleSheet) { documentTextBoxStyleEl.styleSheet.cssText = documentTextBoxStyle.replace(rsS85[28], rsS85[0]).replace(rsS85[29], rsS85[0]);
																																						 } else { documentTextBoxStyleEl.appendChild(documentTextPanel.document.createTextNode(documentTextBoxStyle.replace(rsS85[28], rsS85[0]).replace(rsS85[29], rsS85[0])));
																																						 } rsw_haveCopiedCSS = true; var scr = documentTextPanel.document.createElement(rsS85[30]); scr.type = rsS85[31]; if (typeof (scr.text) == rsS85[32]) { scr.text = script;
																																						 } else { scr.appendChild(documentTextPanel.document.createTextNode(script)); } try{ var linkElement = documentTextPanel.document.createElement(rsS85[33]); linkElement.type = rsS85[27];
																																						 if (documentTextPanel.document.getElementsByTagName(rsS85[34])[0].childNodes.length < 10) { documentTextPanel.document.getElementsByTagName(rsS85[34])[0].appendChild(linkElement);
																																						 linkElement.setAttribute(rsS85[35], cssLinkURL); linkElement.setAttribute(rsS85[36], rsS85[37]); } } catch (x) { console.log(x); } documentTextPanel.document.getElementsByTagName(rsS85[34])[0].appendChild(documentTextBoxStyleEl);
																																						 documentTextPanel.document.getElementsByTagName(rsS85[34])[0].appendChild(scr); } documentTextPanel.document.body.innerHTML = html; documentTextPanel.document.body.setAttribute(rsS85[38], 4);
																																						 documentTextPanel.document.body.setAttribute(rsS85[39], 4); documentTextPanel.document.body.setAttribute(rsS85[40], 4); documentTextPanel.document.body.setAttribute(rsS85[41], 4);
																																						 try { if (documentTextPanel.scrollIntoView) documentTextPanel.scrollIntoView(documentTextPanel.document.getElementById(rsS85[42])); } catch (EE) { } rs_s3.forms[0].suggestions.options.length=0;
																																						 var n=badWords[currentWordIndex].suggestions.length; if(n==0){ rs_s3.forms[0].word.value=badWords[currentWordIndex].text; rs_s3.forms[0].suggestions.options[0]=new Option(noSuggestionsText);
																																						 } else if (badWords[currentWordIndex].suggestions[0]==removeDuplicateWordText){ rs_s3.forms[0].suggestions.options[0]=new Option(badWords[currentWordIndex].suggestions[0]);
																																						 rs_s3.forms[0].suggestions.selectedIndex=0; rs_s3.forms[0].word.value=rsS85[0]; duplicateWord=true; } else{ rs_s3.forms[0].word.value=badWords[currentWordIndex].suggestions[0];
																																						 for(var i=0;i<n;i++){ rs_s3.forms[0].suggestions.options[i]=new Option(badWords[currentWordIndex].suggestions[i]); } rs_s3.forms[0].suggestions.selectedIndex=0; duplicateWord=false;
																																						 } rs_s3.forms[0].word.select(); } else{ var html=rsS85[0]; html+=ftr(textToHtml(text),scriptFilterLevel); var documentTextBoxStyleEl = documentTextPanel.document.createElement(rsS85[26]);
																																						 documentTextBoxStyleEl.type = rsS85[27]; if (documentTextBoxStyleEl.styleSheet) { documentTextBoxStyleEl.styleSheet.cssText = documentTextBoxStyle.replace(rsS85[28], rsS85[0]).replace(rsS85[29], rsS85[0]);
																																						 } else { documentTextBoxStyleEl.appendChild(documentTextPanel.document.createTextNode(documentTextBoxStyle.replace(rsS85[28], rsS85[0]).replace(rsS85[29], rsS85[0])));
																																						 } try { var linkElement = documentTextPanel.document.createElement(rsS85[33]); linkElement.type = rsS85[27]; if (documentTextPanel.document.getElementsByTagName(rsS85[34])[0].childNodes.length < 10) { documentTextPanel.document.getElementsByTagName(rsS85[34])[0].appendChild(linkElement);
																																						 linkElement.setAttribute(rsS85[35], cssLinkURL); linkElement.setAttribute(rsS85[36], rsS85[37]); } } catch (x) { console.log(x); } documentTextPanel.document.getElementsByTagName(rsS85[34])[0].appendChild(documentTextBoxStyleEl);
																																						 documentTextPanel.document.body.innerHTML = html; documentTextPanel.document.body.setAttribute(rsS85[38], 4); documentTextPanel.document.body.setAttribute(rsS85[39], 4);
																																						 documentTextPanel.document.body.setAttribute(rsS85[40], 4); documentTextPanel.document.body.setAttribute(rsS85[41], 4); rs_s3.forms[0].word.value=rsS85[0]; rs_s3.forms[0].suggestions.options.length=0;
																																						 rs_s3.forms[0].suggestions.options[0]=new Option(noSuggestionsText); if(badWords.length>0){ finishedMessageJS(); } else { noErrorsMessageJS(); } spellCheckFinished = true;
																																						 duplicateWord = false; finish(); } if(typeof(onRefreshed)==rsS85[43])onRefreshed(); } function changeWord(index, replacement){ if(replacement) replacement=replacement;
																																						 else replacement=document.forms[0].word.value; changeNotifyCode(index); var newText=rsS85[0]; if(duplicateWord && rs_s3.forms[0].word.value==rsS85[0]){ badWords[index].start--;
																																						 } newText+=text.substring(0,badWords[index].start); newText+=replacement; newText+=text.substring(badWords[index].end,text.length); moveWordOffsets(replacement.length - (badWords[index].end - badWords[index].start), index + 1);
																																						 text=newText; return replacement; } function moveWordOffsets(delta,start){ for(i=start;i<badWords.length;i++){ badWords[i].start+=delta; badWords[i].end+=delta; } } function addCurrent(){ addingWord = true;
																																						 addNotifyCode(currentWordIndex); var currentWord = badWords[currentWordIndex].text; if(rs_s3.forms[0].addButton){ rs_s3.forms[0].addButton.value=addingText; rs_s3.forms[0].addButton.disabled=true;
																																						 } addWordFrame.location.href = rsw_popupLocation + (rsw_popupLocation.indexOf(rsS85[44]) > -1 ? rsS85[45] : rsS85[44]) + rsS85[46] + escape(userDicFile) + rsS85[47] + badWords[currentWordIndex].e + rsS85[48] + guiLanguage + rsS85[0];
																																						 ignoreAll(); } function ftr(tt, level){ switch(level){ case 1: return fsb( tt ); break; case 2: return feh( tt ); break; case 3: return ftr(ftr(tt,2),1); break; case 4: return fu( tt );
																																						 break; case 5: return ftr(ftr(tt,4),1); break; case 6: return ftr(ftr(tt,4),2); break; case 7: return ftr(ftr(tt,6),1); break; case 8: return fb(tt); break; case 9: return ftr(ftr(tt,8),1);
																																						 break; case 10: return ftr(ftr(tt,8),2); break; case 11: return ftr(ftr(tt,10),1); break; case 12: return ftr(ftr(tt,8),4); break; case 13: return ftr(ftr(tt,12),1);
																																						 break; case 14: return ftr(ftr(tt,12),2); break; case 15: return fsb( feh( fu( fb(tt) ) ) ); break; default: return tt; break; } } function fsb(tt){ var exp = new RegExp(rsS85[49] + rsS85[10], rsS85[50]);
																																						 while(exp.test(tt)) tt = tt.replace(exp, rsS85[0]); var exp = new RegExp(rsS85[51], rsS85[50]); while(exp.test(tt)) tt = tt.replace(exp, rsS85[0]); var exp = new RegExp(rsS85[52], rsS85[50]);
																																						 while(exp.test(tt)) tt = tt.replace(exp, rsS85[0]); var exp = new RegExp(rsS85[53], rsS85[50]); while(exp.test(tt)) tt = tt.replace(exp, rsS85[0]); return tt; } function feh(tt){ var exp = new RegExp(rsS85[54], rsS85[50]);
																																						 while(exp.test(tt)) tt = tt.replace(exp, rsS85[55]); return tt; } function fu(tt){ var exp = new RegExp(rsS85[56], rsS85[50]); while(exp.test(tt)) tt = tt.replace(exp, rsS85[57]);
																																						 return tt; } function fb(tt){ var exp = new RegExp(rsS85[58]+rsS85[10], rsS85[50]); while(exp.test(tt)) tt = tt.replace(exp, rsS85[0]); var exp = new RegExp(rsS85[59], rsS85[50]);
																																						 while(exp.test(tt)) tt = tt.replace(exp, rsS85[0]); var exp = new RegExp(rsS85[60], rsS85[50]); while(exp.test(tt)) tt = tt.replace(exp, rsS85[0]); return tt } if( typeof(Sys)!=rsS85[7] && typeof(Sys.Application)!=rsS85[7]) Sys.Application.notifyScriptLoaded();
																																						 //-------------------------
