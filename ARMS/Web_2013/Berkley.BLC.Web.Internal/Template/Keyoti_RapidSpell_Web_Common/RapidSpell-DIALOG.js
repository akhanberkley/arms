/*--Keyoti js--*/
/*<![CDATA[*/
//Version 1.56 (RapidSpell Web assembly version 3.5 onwards)
//Copyright Keyoti Inc. 2005-2013
//This code is not to be modified, copied or used without a license in any form.

var rsS19=new Array("Microsoft Internet Explorer",
"MSIE ([0-9]{1,}[\.0-9]{0,})",
"rsTCInt",
"undefined",
"IgnoreXML",
"True",
"popUpCheckSpelling",
"('rsTCInt",
"')",
"g",
"&#34;",
"&",
"&amp;",
"Error: element ",
" does not exist, check TextComponentName.",
"",
"Sorry, a textbox with ID=",
" couldn't be found - please check the TextComponentID or TextComponentName property.",
"_IF",
"&lt;",
"&gt;",
"TEXTAREA",
"INPUT",
" EDITABLE",
"\r\n",
" ",
"contentEditable",
"true",
"designMode",
"on",
"IFRAME",
" --has contentWindow.document",
"*",
"Gecko",
"MSIE",
"Chrome",
"AppleWebKit",
"script",
"Sorry, cannot find a script import named ",
".js, please do not rename any RapidSpell scripts.",
"RapidSpell-",
"BackCompat",
"/",
"<",
"blank.html",
"menu.css",
"rs_style.css",
"RapidSpellModalHelper.html",
"UserDictionaryFile",
"SuggestionsMethod",
"SeparateHyphenWords",
"IncludeUserDictionaryInSuggestions",
"IgnoreCapitalizedWords",
"GuiLanguage",
"LanguageParser",
"Modal",
"AllowAnyCase",
"IgnoreWordsWithDigits",
"ShowFinishedMessage",
"ShowNoErrorsMessage",
"ShowXMLTags",
"AllowMixedCase",
"WarnDuplicates",
"DictFile",
"PopUpWindowName",
"CreatePopUpWindow",
"ConsiderationRange",
"LookIntoHyphenatedText",
"CheckCompoundWords",
"EnableUndo",
"LeaveWindowOpenForUndo",
"SSLFriendlyPage",
"IgnoreURLsAndEmailAddresses",
"IgnoreIncorrectSentenceCapitalization",
"IgnoreInEnglishLowerCaseI",
"SuggestSplitWords",
"CorrectionNotifyListener",
"MaximumNumberOfSuggestions",
"ShowAddItemAlways",
"AddNotifyListener",
"~/user-dictionary.txt",
"HASHING_SUGGESTIONS",
"false",
"ENGLISH",
"80",
"rsw_ondialogcorrection",
"8",
"rsw_ondialogadd",
"dialog_correction",
"dialog_add",
"FRENCH",
"SPANISH",
"GERMAN",
"ITALIAN",
"PORTUGUESE",
"DUTCH",
"POLISH",
"Ignore",
"Ignore All",
"Add",
"Edit...",
"All",
"Remove duplicate word",
"No suggestions",
"The spelling check is complete.",
"Ignorer",
"Ignorer Tout",
"Ajouter",
"Éditez...",
"Tout",
"Enlevez le mot double",
"Aucun",
"Fini.",
"Ignorar",
"Ignorar Todas",
"Agregar",
"Corrija...",
"Todas",
"Quite la palabra duplicada",
"Ningunas sugerencias",
"La verificación ortográfica ha finalizado.",
"Ignorieren",
"Alle ignorieren",
"Hinzufügen",
"Redigieren...",
"Alle",
"Doppelte Wörter entfernen",
"Keine vorschläge",
"Die Rechtschreibprüfung ist abgeschlossen.",
"Ignora",
"Ignora tutto",
"Aggiungi",
"Modifica...",
"Tutto",
"Rimuovi la parola duplicata",
"Nessun suggerimento",
"Controllo completato.",
"Ignore Tudo",
"Adicione",
"Edite...",
"Tudo",
"Remova a palavra duplicada",
"Nenhumas sugestões",
"A verificação de soletração está completa.",
"Negeren",
"Alles negeren",
"Toevoegen",
"Corrigeren...",
"Allen",
"Verwijder herhaald woord",
"Geen suggesties",
"De spellingscontrole is voltooid.",
"Ignoruj",
"Ignoruj wszystko",
"Dodaj",
"Edytuj...",
"Wszystko",
"Usuń podwójne wystąpienie słowa",
"Nie ma sugestii",
"Zakończono Sprawdzanie pisowni.",
"ignore",
"ignoreAll",
"add",
"edit",
"changeAll",
"removeDuplicate",
"noSuggestions",
"complete",
"default",
"nospell",
"hidden",
"none",
"visibility",
"display",
"Please ensure that you include the RapidSpell-Core.js file in your page, before the RapidSpell-Launcher.js file is imported.",
"popup.aspx",
"string",
"text",
"function",
"rs",
"dialog",
"textarea",
"input",
"rsw_oldOnLoad",
"&#",
";",
"<input type=hidden name=",
" value='",
"'>",
"typeof(",
"var curState=",
"_SpellChecker.state",
"var cs=",
"overlay",
"_SpellChecker.OnSpellButtonClicked()",
"dialog_startedcheckingtextbox",
"rsTCInt['",
"']",
"dialogHeight: ",
"px; dialogWidth: ",
"px; ",
"dialogLeft:",
"; ",
"dialogTop:",
"edge: Sunken; center: Yes; help: No; resizable: No; status: No;",
"/blank.html",
"docdomain",
"?",
"docdomain=",
"rspellwin",
"resizable=yes,scrollbars=auto,dependent=yes,toolbar=no,left=",
",top=",
",status=no,location=no,menubar=no,width=",
",height=",
"dialog_finishedcheckingtextbox",
"dialog_finishedcheckingall",
"findEditableElements failed: ");																																				var rs_s2=window;var rs_s3=document; var rsw_launcher_script_loaded = true; var rsw_suppressWarnings=false; function rsw_getInternetExplorerVersion() { var rv = -1;
																																						 if (navigator.appName == rsS19[0]) { var ua = navigator.userAgent; var re = new RegExp(rsS19[1]); if (re.exec(ua) != null) rv = parseFloat(RegExp.$1); } return rv;
																																						 } function SpellCheckLauncher(clientID){ this.clientID = clientID; this.OnSpellButtonClicked = OnSpellButtonClicked; this.config; this.hasRunFieldID; this.getParameterValue = getParameterValue;
																																						 this.setParameterValue = setParameterValue; this.tbInterface = null; function getParameterValue(param){ for(var pp=0; pp<this.config.keys.length; pp++){ if(this.config.keys[pp]==param) return this.config.values[pp];
																																						 } } function setParameterValue(param, value){ for(var pp=0; pp<this.config.keys.length; pp++){ if(this.config.keys[pp]==param) this.config.values[pp] = value; } } function OnSpellButtonClicked(){ this.tbInterface = eval(rsS19[2]+this.clientID);
																																						 if(this.tbInterface!=null && typeof(this.tbInterface.findContainer)!=rsS19[3]){ this.textBoxID = this.tbInterface.findContainer().id; if (!this.tbInterface.targetIsPlain){ this.setParameterValue(rsS19[4], rsS19[5]);
																																						 } } eval(rsS19[6]+this.clientID+rsS19[7]+this.clientID+rsS19[8]); } } function RS_writeDoc(toWindow, isSafari){ toWindow.document.open(); toWindow.document.write(spellBoot);
																																						 toWindow.document.close(); if(isSafari) toWindow.document.forms[0].submit(); } function escQuotes(text){ var rx = new RegExp("\"", rsS19[9]); return text.replace(rx,rsS19[10]);
																																						 } function escEntities(text){ var rx = new RegExp(rsS19[11], rsS19[9]); return text.replace(rx,rsS19[12]); } function RSStandardInterface(tbElementName){ this.tbName = tbElementName;
																																						 this.getText = getText; this.setText = setText; function getText(){ if(!document.getElementById(this.tbName) && !rsw_suppressWarnings) { alert(rsS19[13]+this.tbName+rsS19[14]);
																																						 return rsS19[15]; } else return rs_s3.getElementById(this.tbName).value; } function setText(text) { if(rs_s3.getElementById(this.tbName)) rs_s3.getElementById(this.tbName).value = text;
																																						 if(typeof(rsw_tbs)!=rsS19[3]){ for(var i=0; i<rsw_tbs.length; i++){ if(rsw_tbs[i].shadowTB.id==this.tbName){ if(rsw_tbs[i].updateIframe){rsw_tbs[i].updateIframe();
																																						rsw_tbs[i].focus();} } } } } } function RSAutomaticInterface(tbElementName){ this.tbName = tbElementName;this.getText = getText;this.setText = setText; this.identifyTarget = identifyTarget;
																																						 this.target=null; this.targetContainer = null; this.searchedForTarget = false; this.targetIsPlain = true; this.showNoFindError = showNoFindError; this.finder = null;
																																						 this.findContainer = findContainer; function findContainer(){ this.identifyTarget(); return this.targetContainer; } function showNoFindError(){ alert(rsS19[16]+this.tbName+rsS19[17]);
																																						 } function identifyTarget(){ if(!this.searchedForTarget){ this.searchedForTarget = true; if(this.finder == null) this.finder = new RSW_EditableElementFinder(); var plain = this.finder.findPlainTargetElement(this.tbName);
																																						 var richs = this.finder.findRichTargetElements(); if(plain==null && (richs==null || richs.length==0) && !rsw_suppressWarnings) showNoFindError(); else{ if(richs==null || richs.length==0){ this.targetIsPlain = true;
																																						 this.target = plain; this.targetContainer = plain; } else { if(plain==null && richs.length==1){ this.targetIsPlain = false; this.target = this.finder.obtainElementWithInnerHTML(richs[0][0]);
																																						 this.targetContainer = richs[0][1]; } else { for (var rp = 0; rp < richs.length; rp ++){ if(typeof(richs[rp][1].id)!=rsS19[3] && richs[rp][1].id.indexOf(this.tbName)>-1){ if(plain!=null && richs[rp][1].id == plain.id+rsS19[18]){ this.targetIsPlain = true;
																																						 this.target = plain; this.targetContainer = plain; break; } else { this.targetIsPlain = false; this.target = this.finder.obtainElementWithInnerHTML(richs[rp][0]);
																																						 this.targetContainer = richs[rp][1]; break; } } } if(this.target==null){ this.target = plain; this.targetIsPlain = true; this.targetContainer = plain; } } } } } } function getText(){ this.identifyTarget();
																																						 if( this.targetIsPlain ) return this.target.value; else return this.target.innerHTML; } function setText(text){ this.identifyTarget(); if (this.targetIsPlain) { var ver = rsw_getInternetExplorerVersion();
																																						 if (ver > 0 && ver < 9) text = text.replace(/</g, rsS19[19]).replace(/>/g, rsS19[20]); this.target.value = text; } else this.target.innerHTML = text; if(typeof(rsw_tbs)!=rsS19[3]){ for(var i=0;
																																						 i<rsw_tbs.length; i++){ if(rsw_tbs[i].shadowTB.id==this.tbName){ if(rsw_tbs[i].updateIframe){rsw_tbs[i].updateIframe();rsw_tbs[i].focus();} } } } } } function RSW_EditableElementFinder(){ this.findPlainTargetElement = findPlainTargetElement;
																																						 this.findRichTargetElements = findRichTargetElements; this.obtainElementWithInnerHTML = obtainElementWithInnerHTML; this.findEditableElements = findEditableElements;
																																						 this.elementIsEditable = elementIsEditable; this.getEditableContentDocument = getEditableContentDocument; function findPlainTargetElement(elementID){ var rsw_elected = rs_s3.getElementById(elementID);
																																						 if(rsw_elected!=null && rsw_elected.tagName && (rsw_elected.tagName.toUpperCase()==rsS19[21] || rsw_elected.tagName.toUpperCase()==rsS19[22])){ return rsw_elected;
																																						 } else return null; } function findRichTargetElements(debugTextBox){ var editables = new Array(); this.findEditableElements(document, editables, window,rsS19[15], debugTextBox);
																																						 return editables; } function obtainElementWithInnerHTML(editable){ if(typeof(editable.innerHTML)!=rsS19[3]) return editable; else if(typeof(editable.documentElement)!=rsS19[3]) return editable.documentElement;
																																						 return null; } function findEditableElements(node, editables, parent, debugInset, debugTextBox){ var children = node.childNodes; var editableElement; if(debugTextBox) debugTextBox.value += debugInset + node.tagName;
																																						 if( (editableElement=this.elementIsEditable(node))!=null || (editableElement=this.getEditableContentDocument(node, debugTextBox))!=null ){ if(debugTextBox) debugTextBox.value += rsS19[23];
																																						 editables[editables.length] = editableElement; } if(debugTextBox) debugTextBox.value += rsS19[24]; for (var i = 0; i < children.length; i++) { this.findEditableElements(children[i], editables, node, debugInset+rsS19[25], debugTextBox);
																																						 } } function elementIsEditable(element){ if ( ( typeof(element.getAttribute)!=rsS19[3] && ( element.getAttribute(rsS19[26])==rsS19[27] || element.getAttribute(rsS19[28])==rsS19[29] ) ) || ( (element.contentEditable && element.contentEditable==true) || (element.designMode && element.designMode.toLowerCase()==rsS19[29]) ) ) return [element, element];
																																						 else return null; } function getEditableContentDocument(element, debugTextBox){ if(element.tagName && element.tagName==rsS19[30]){ var kids = new Array(); if(element.contentWindow && element.contentWindow.document){ if(debugTextBox) debugTextBox.value += rsS19[31];
																																						 this.findEditableElements(element.contentWindow.document, kids, element, rsS19[32], debugTextBox); if(kids.length>0){ var editable = kids[0][0]; if(typeof(editable.body)!=rsS19[3]) editable = editable.body;
																																						 return [editable, element]; } } } return null; } } if( typeof(Sys)!=rsS19[3] && typeof(Sys.Application)!=rsS19[3]) Sys.Application.notifyScriptLoaded(); var rsw_mozly = navigator.userAgent.indexOf(rsS19[33]) > -1 ;
																																						 var rsw_msie = navigator.userAgent.indexOf(rsS19[34]) > -1 ; var rsw_chrome = navigator.userAgent.indexOf(rsS19[35]) > -1; var rsw_applewebkit = navigator.userAgent.indexOf(rsS19[36]) > -1;
																																						 if(typeof(RapidSpell)==rsS19[3])RapidSpell = function () { }; RapidSpell.prototype.getConfigurationObject = rsw_getConfigurationObject; RapidSpell.prototype.setParameterValue = rsw_setParameterValue;
																																						 RapidSpell.prototype.getParameterValue = rsw_getParameterValue; RapidSpell.prototype.addEventListener = rsw_addEventListener; RapidSpell.prototype.removeEventListener = rsw_removeEventListener;
																																						 var rsw_eventListeners = []; function rswEventListener(eventName, listener) { this.eventName = eventName; this.listener = listener; } function rsw_addEventListener(eventName, listenerFunction) { rsw_eventListeners[rsw_eventListeners.length] = new rswEventListener(eventName, listenerFunction);
																																						 } function rsw_removeEventListener(eventName, listenerFunction) { var foundAt=-1; for (var i = 0; foundAt==-1 && i < rsw_eventListeners.length; i++) { if(rsw_eventListeners[i].listener==listenerFunction && rsw_eventListeners[i].eventName==eventName) foundAt=i;
																																						 } if(foundAt>-1) rsw_eventListeners.splice(foundAt, 1); } function rsw_broadcastEvent(eventName, source, data1, data2, data3, data4, data5) { for (var i = 0; i < rsw_eventListeners.length;
																																						 i++) { if (rsw_eventListeners[i].eventName == eventName) rsw_eventListeners[i].listener(source, data1, data2, data3, data4, data5); } } function rsw_getScriptLocation(name) { var scripts = rs_s3.getElementsByTagName(rsS19[37]);
																																						 var l; for (var i = 0; i < scripts.length; i++) { if ((l = scripts[i].src.indexOf(name)) > -1) { return scripts[i].src.substring(0, l); } } alert(rsS19[38] + name + rsS19[39]);
																																						 } var rsw_scriptLocation = rsw_getScriptLocation(rsS19[40]); var rapidSpell = rapidSpell ? rapidSpell : new RapidSpell(); var rsw_iever = rsw_getInternetExplorerVersion();
																																						 var rsw_thisBrowserOnlyFilename = ((rsw_iever > 0 && rsw_iever < 8) || (rsw_iever > 0 && rsw_iever < 10 && rs_s3.compatMode == rsS19[41])) && rsw_scriptLocation.charAt(0) != rsS19[42];
																																						 var rsw_RapidSpell_Core=true; var rsw_ignoreDisabledTextBoxes = true; var rsw_ignoreReadyOnlyTextBoxes = true; var rsw_blankLocation = '<%= WebResource("blank.html") %>'.indexOf(rsS19[43]) > -1 ? rsw_scriptLocation + rsS19[44] : '<%= WebResource("blank.html") %>';
																																						 if (rsw_thisBrowserOnlyFilename) { rsw_rs_menu_styleURL = '<%= WebResource("menu-net2.css") %>'.indexOf(rsS19[43]) > -1 ? rsS19[45] : '<%= WebResource("menu-net2.css") %>';
																																						 rsw_rs_styleURL = '<%= WebResource("rs_style-net2.css") %>'.indexOf(rsS19[43]) > -1 ? rsS19[46] : '<%= WebResource("rs_style-net2.css") %>'; } else { rsw_rs_menu_styleURL = ('<%= WebResource("menu-net2.css") %>'.indexOf(rsS19[43]) > -1 ? rsw_scriptLocation + rsS19[45] : '<%= WebResource("menu-net2.css") %>');
																																						 rsw_rs_styleURL = ('<%= WebResource("rs_style-net2.css") %>'.indexOf(rsS19[43]) > -1 ? rsw_scriptLocation + rsS19[46] : '<%= WebResource("rs_style-net2.css") %>');
																																						 } rsw_modalHelperURL = '<%= WebResource("RapidSpellModalHelperNET2.html") %>'.indexOf(rsS19[43]) > -1 ? rsw_scriptLocation + rsS19[47] : '<%= WebResource("RapidSpellModalHelperNET2.html") %>';
																																						 var rsw_targetIDs = new Array(); var rsw_rapidSpellControls = new Array(); var rsw_config_textBoxKeys = new Array(); var rsw_config_textBoxValues = new Array(); var rsw_config_defaults = { keys: [rsS19[48], rsS19[49], rsS19[50], rsS19[51], rsS19[4], rsS19[52], rsS19[53], rsS19[54], rsS19[55], rsS19[56], rsS19[57], rsS19[58], rsS19[59], rsS19[60], rsS19[61], rsS19[62], rsS19[63], rsS19[64], rsS19[65], rsS19[66], rsS19[67], rsS19[68], rsS19[69], rsS19[70], rsS19[71], rsS19[72], rsS19[73], rsS19[74], rsS19[75], rsS19[76], rsS19[77], rsS19[78], rsS19[79]], values: [rsS19[80], rsS19[81], rsS19[82], rsS19[27], rsS19[82], rsS19[82], rsS19[83], rsS19[83], rsS19[82], rsS19[82], rsS19[82], rsS19[27], rsS19[82], rsS19[82], rsS19[82], rsS19[27], rsS19[15], rsS19[15], rsS19[82], rsS19[84], rsS19[27], rsS19[82], rsS19[27], rsS19[82], rsw_blankLocation, rsS19[27], rsS19[82], rsS19[82], rsS19[27], rsS19[85], rsS19[86], false, rsS19[87]] };
																																						 function rsw_ondialogcorrection(a, b, c, tbName) { rsw_broadcastEvent(rsS19[88], null, rs_s3.getElementById(tbName), a, b, c); } function rsw_ondialogadd(a, b, c, tbName) { rsw_broadcastEvent(rsS19[89], null, rs_s3.getElementById(tbName), a, b, c);
																																						 } var rsw_menuOptionKeys = [rsS19[83], rsS19[90], rsS19[91], rsS19[92], rsS19[93], rsS19[94], rsS19[95], rsS19[96]]; var rsw_menuOptionValues = [ [rsS19[97], rsS19[98], rsS19[99], rsS19[100], rsS19[101], rsS19[102], rsS19[103], rsS19[104]], [rsS19[105], rsS19[106], rsS19[107], rsS19[108], rsS19[109], rsS19[110], rsS19[111], rsS19[112]], [rsS19[113], rsS19[114], rsS19[115], rsS19[116], rsS19[117], rsS19[118], rsS19[119], rsS19[120]], [rsS19[121], rsS19[122], rsS19[123], rsS19[124], rsS19[125], rsS19[126], rsS19[127], rsS19[128]], [rsS19[129], rsS19[130], rsS19[131], rsS19[132], rsS19[133], rsS19[134], rsS19[135], rsS19[136]], [rsS19[97], rsS19[137], rsS19[138], rsS19[139], rsS19[140], rsS19[141], rsS19[142], rsS19[143]], [rsS19[144], rsS19[145], rsS19[146], rsS19[147], rsS19[148], rsS19[149], rsS19[150], rsS19[151]], [rsS19[152], rsS19[153], rsS19[154], rsS19[155], rsS19[156], rsS19[157], rsS19[158], rsS19[159]] ];
																																						 function SpellCheckUITextItems() { this.ignore; } function rsw_setUIText(language, item, newText) { var a = rsw_getLanguageArray(language); switch (item) { case rsS19[160]: a[0] = newText;
																																						 break; case rsS19[161]: a[1] = newText; break; case rsS19[162]: a[2] = newText; break; case rsS19[163]: a[3] = newText; break; case rsS19[164]: a[4] = newText; break;
																																						 case rsS19[165]: a[5] = newText; break; case rsS19[166]: a[6] = newText; break; case rsS19[167]: a[7] = newText; break; } } function rsw_getLanguageArray(lang) { for (var i = 0;
																																						 i < rsw_menuOptionKeys.length; i++) { if (rsw_menuOptionKeys[i] == lang) return rsw_menuOptionValues[i]; } return null; } function rsw_getParameterValue(textBox, param) { var searchObj;
																																						 var pos = rsw_getTextBoxIndex(textBox); if (pos == -1 && textBox != rsS19[168]) return rsw_getParameterValue(rsS19[168], param); else { if (textBox == rsS19[168]) { searchObj = rsw_config_defaults;
																																						 } else searchObj = rsw_config_textBoxValues[pos]; var found = -1; for (var pp = 0; searchObj.keys && pp < searchObj.keys.length && found == -1; pp++) { if (searchObj.keys[pp] == param) found = pp;
																																						 } if (found == -1) return rsw_getParameterValue(rsS19[168], param); else { return searchObj.values[found]; } } } function rsw_getConfigurationObject(textBox) { var synthObject = {keys:[], values:[]};
																																						 for (var i = 0; i < rsw_config_defaults.keys.length; i++) { synthObject.keys[synthObject.keys.length] = rsw_config_defaults.keys[i]; synthObject.values[synthObject.values.length] = rsw_getParameterValue(textBox, rsw_config_defaults.keys[i]);
																																						 } return synthObject; } function rsw_getTextBoxIndex(textBox) { for (var i = 0; i < rsw_config_textBoxKeys.length; i++) { if (rsw_config_textBoxKeys[i] == textBox) return i;
																																						 } return -1; } function rsw_setParameterValue(textBox, param, value) { var pos; param = param.replace(/^\s+|\s+$/g, rsS19[15]); if (textBox != rsS19[168]) { pos = rsw_getTextBoxIndex(textBox);
																																						 if (pos == -1) { rsw_config_textBoxKeys[rsw_config_textBoxKeys.length] = textBox; pos = rsw_config_textBoxKeys.length - 1; rsw_config_textBoxValues[pos] = {}; } } var targ;
																																						 if (textBox != rsS19[168]) targ = rsw_config_textBoxValues[pos]; else targ = rsw_config_defaults; var found = -1; for (var pp = 0; targ.keys && pp < targ.keys.length && found == -1;
																																						 pp++) { if (targ.keys[pp] == param) found = pp; } if (!targ.keys) { targ.keys = new Array(); targ.values = new Array(); } if (found == -1) found = targ.keys.length;
																																						 targ.keys[found] = param; targ.values[found] = value; if(typeof(rsw_scs)!=rsS19[3]){ for (var i = 0; i < rsw_scs.length; i++) { if (textBox==rsS19[168] || rsw_scs[i].textBoxID == textBox.id) { rsw_scs[i].config = rsw_getConfigurationObject(rs_s3.getElementById(rsw_scs[i].textBoxID));
																																						 if (param == rsS19[53]) { var langArray = rsw_getLanguageArray(value); if (langArray != null) { rsw_setSpellCheckerText(rsw_scs[i], langArray); } } } } } } function rsw_setSpellCheckerText(spellChecker, langArray) { spellChecker.ignoreText = langArray[0];
																																						 spellChecker.ignoreAllText = langArray[1]; spellChecker.addText = langArray[2]; spellChecker.editText = langArray[3]; spellChecker.changeAllText = langArray[4]; spellChecker.removeDuplicateText = langArray[5];
																																						 spellChecker.noSuggestionsText = langArray[6]; } function rsw_isParentIsNoSpell(tb) { if (tb.getAttribute(rsS19[169]) == rsS19[27]) return true; if (tb.parentElement != null) return rsw_isParentIsNoSpell(tb.parentElement);
																																						 else if (typeof (tb.parentElement) == rsS19[3] && tb.parentNode != null) return rsw_isParentIsNoSpell(tb.parentNode); else return false; } function rsw_isElementVisible(tb) { var ifVis = false;
																																						 if (rs_s3.getElementById(tb.id + rsS19[18]) != null) ifVis = rsw_isElementVisible(rs_s3.getElementById(tb.id + rsS19[18])); if ((tb.style && tb.style.visibility && tb.style.visibility == rsS19[170]) || (tb.style && tb.style.display == rsS19[171])) return false || ifVis;
																																						 var compVis = rsw_getStyleProperty(tb, rsS19[172]); var compDis = rsw_getStyleProperty(tb, rsS19[173]); if (compVis == rsS19[170] || compDis == rsS19[171]) return false || ifVis;
																																						 if (tb.parentElement != null) return rsw_isElementVisible(tb.parentElement) || ifVis; else if (typeof (tb.parentElement) == rsS19[3] && tb.parentNode != null) return rsw_isElementVisible(tb.parentNode) || ifVis;
																																						 else return true; } var rsw_RapidSpell_Launcher = true; var rsw_warnNoTextBoxes = true; if (!rsw_RapidSpell_Core) alert(rsS19[174]); rapidSpell.dialog_popupURL = rsw_scriptLocation + rsS19[175];
																																						 rapidSpell.dialog_blankURL = null; rapidSpell.dialog_modal = false; rapidSpell.dialog_width = 370; rapidSpell.dialog_height = navigator.userAgent.indexOf(rsS19[35]) > -1 ? 430 : 400;
																																						 rapidSpell.dialog_left = 100; rapidSpell.dialog_top = 100; rapidSpell.dialog_documentDomain = rsS19[15]; rapidSpell.dialog_ignoreTextBoxIds = new Array(); var rsw_mult_use_update = true;
																																						 var rsTCInt = new Object(); RapidSpell.prototype.dialog_spellCheck = rswm_RunSpellCheck; function rsw_isTextBoxIgnoredDialog(tbId) { for (var i = 0; i < rapidSpell.dialog_ignoreTextBoxIds.length;
																																						 i++) if (tbId == rapidSpell.dialog_ignoreTextBoxIds[i]) return true; return false; } function rsw_getStyleProperty(obj, IEStyleProp) { if (obj.currentStyle) { return obj.currentStyle[IEStyleProp];
																																						 } else if (rs_s3.defaultView.getComputedStyle) { return rs_s3.defaultView.getComputedStyle(obj, null)[IEStyleProp]; } else { return null; } } function rsw_setup_SpellControls(optionalID) { if (typeof (optionalID) == rsS19[176]) rsw_setup_SpellControls_Array([optionalID]);
																																						 else rsw_setup_SpellControls_Array(optionalID); } function rsw_setup_SpellControls_Array(optionalIDs){ rsw_targetIDs = []; var myElements = []; var givenIDs = false;
																																						 if (typeof (optionalIDs) != rsS19[3] && optionalIDs.length > 0) { givenIDs = true; for (var i = 0; i < optionalIDs.length; i++) { var el = rs_s3.getElementById(optionalIDs[i]);
																																						 if (el != null) myElements[myElements.length] = el; } } else { for (var f = 0; f < rs_s3.forms.length; f++) { for (var g = 0; g < rs_s3.forms[f].elements.length; g++) { myElements[myElements.length] = rs_s3.forms[f].elements[g];
																																						 } } } var optionalID; var more; for (var i = 0; i < myElements.length; i++) { var cobj = myElements[i]; var idPtr = 0; if (typeof (optionalIDs) != rsS19[3] && optionalIDs.length > idPtr) optionalID = optionalIDs[idPtr];
																																						 more = true; while (more) { if ((typeof (optionalID) != rsS19[3] && cobj.id == optionalID) || typeof (optionalID) == rsS19[3]) { if ((((cobj.tagName == rsS19[22] && cobj.type.toLowerCase() == rsS19[177]) || (cobj.tagName == rsS19[21]) || (cobj.tagName == rsS19[30])) && (!rsw_ignoreReadyOnlyTextBoxes || !cobj.readOnly) && (!rsw_ignoreDisabledTextBoxes || !cobj.disabled) && cobj.value != rsS19[15]) && cobj.getAttribute(rsS19[169]) != rsS19[27] && !rsw_isParentIsNoSpell(cobj) && rsw_isElementVisible(cobj) && cobj.id!=rsS19[15] && !rsw_isTextBoxIgnoredDialog(cobj.id) ) { rsw_targetIDs[rsw_targetIDs.length] = cobj.id;
																																						 } else if (givenIDs && (!rsw_ignoreReadyOnlyTextBoxes || !cobj.readOnly) && (!rsw_ignoreDisabledTextBoxes || !cobj.disabled) && cobj.value != rsS19[15] && cobj.id != rsS19[15]) { rsw_targetIDs[rsw_targetIDs.length] = cobj.id;
																																						 } } more = (typeof (optionalIDs) != rsS19[3] && optionalIDs.length > ++idPtr); if (more) optionalID = optionalIDs[idPtr]; } } if (rsw_targetIDs.length == 0 && typeof(optionalID)!=rsS19[3]) { rsw_targetIDs[rsw_targetIDs.length] = optionalID;
																																						 } rsw_rapidSpellControls = []; for (var i = 0; i < rsw_targetIDs.length; i++) { if (typeof (rapidSpell.textInterfaceNeeded) == rsS19[178]) rsTCInt[rsS19[179] + i] = new rapidSpell.textInterfaceNeeded(rsw_targetIDs[i], rsS19[180]);
																																						 else rsTCInt[rsS19[179] + i] = new RSAutomaticInterface(rsw_targetIDs[i]); rsw_rapidSpellControls[i] = rsS19[179] + i; } } function rsw_findTextBoxes() { var textareas = rs_s3.getElementsByTagName(rsS19[181]);
																																						 var inputs = rs_s3.getElementsByTagName(rsS19[182]); for (var i = 0; i < textareas.length; i++) { if (!textareas[i].readOnly && !textareas[i].disabled && textareas[i].getAttribute(rsS19[169]) != rsS19[27] && !rsw_isParentIsNoSpell(textareas[i])) rsw_targetIDs[rsw_targetIDs.length] = textareas[i].id;
																																						 } for (var i = 0; i < inputs.length; i++) { if (!inputs[i].readOnly && !inputs[i].disabled && inputs[i].type.toLowerCase() == rsS19[177] && inputs[i].getAttribute(rsS19[169]) != rsS19[27] && !rsw_isParentIsNoSpell(inputs[i])) rsw_targetIDs[rsw_targetIDs.length] = inputs[i].id;
																																						 } for (var i = 0; i < rsw_targetIDs.length; i++) { if (typeof (rapidSpell.textInterfaceNeeded) == rsS19[178]) rsTCInt[rsS19[179] + i] = new rapidSpell.textInterfaceNeeded(rsw_targetIDs[i], rsS19[180]);
																																						 else rsTCInt[rsS19[179] + i] = new RSAutomaticInterface(rsw_targetIDs[i]); rsw_rapidSpellControls[i] = rsS19[179] + i; } if (typeof (rsS19[183]) == rsS19[178]) rsw_oldOnLoad();
																																						 } function RSStandardInterface(tbElementName) { this.tbName = tbElementName; this.getText = getText; this.setText = setText; function getText() { return rs_s3.getElementById(this.tbName).value;
																																						 } function setText(text) { rs_s3.getElementById(this.tbName).value = text; } } function rsw_escQuotes(text) { var rx = new RegExp("\"", rsS19[9]); return text.replace(rx, rsS19[10]);
																																						 } function rsw_escEntities(text) { var rx = new RegExp(rsS19[11], rsS19[9]); text = text.replace(rx, rsS19[12]); for (var i = 161; i <= 255; i++) { rx = new RegExp(String.fromCharCode(i), rsS19[9]);
																																						 text = text.replace(rx, rsS19[184] + i + rsS19[185]); } return text; } function rsw_getrsw_spellBootString(interfaceObject) { var res = new String(); var textBox = rs_s3.getElementById(interfaceObject.tbName);
																																						 for(var i=0; i<rsw_config_defaults.keys.length; i++){ var key = rsw_config_defaults.keys[i]; var value = rsw_config_defaults.values[i]; var cval = rsw_getParameterValue(textBox, key);
																																						 if (cval != null) value = cval; if (key == rsS19[58]) value = false; res += rsS19[186] + key + rsS19[187] + value + rsS19[188]; } return res; } function rsw_createSpellBoot(interfaceObject, interfaceObjectName) { rsw_spellBoot = "<html><head>"+ (!rsw_isDocDomainSpec() ? '' : "<script type='text/javascript'>document.domain='" + rapidSpell.dialog_documentDomain + "';</script>") + "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body onload=\"if(navigator.userAgent.indexOf('MSIE') > -1)docu" + "ment.forms[0].submit();\"><font face='arial, helvetica' size=2>Spell checking docu" + "ment...</font><form action='" + rapidSpell.dialog_popupURL + "' method='post' ACCEPT-CHARSET='UTF-8'>";
																																						 rsw_spellBoot += "<input type='hidden' name='docdomain' value='"+rapidSpell.dialog_documentDomain+"'><input type='hidden' name='FinishedListener' value='rswm_NotifyDone'><input type='hidden' name='InterfaceObject' value=\"" + interfaceObjectName + "\"><input type='hidden' name='textToCheck' value=\"" + rsw_escQuotes(rsw_escEntities(interfaceObject.getText())) + "\"><input type='hidden' name='mode' value='popup'><input type='hidden' name='callBack' value=''><input type='hidden' name='RswlClientID' value='r7a4z4'><input type='hidden' name='UseUpdate' value='" + (typeof (rsw_mult_use_update) != 'undefined' ? rsw_mult_use_update : '') + "'>";
																																						 rsw_spellBoot += rsw_getrsw_spellBootString(interfaceObject); rsw_spellBoot += "<scri" + "pt type='text/javascript'>if(navigator.userAgent.indexOf('MSIE') == -1"+(rsw_isDocDomainSpec()?' || true':'')+")docu" + "ment.forms[0].submit();</scri" + "pt></form></body></html>";
																																						 } function rsw_isDocDomainSpec(){ return (rapidSpell.dialog_documentDomain!=null && rapidSpell.dialog_documentDomain.length>0); } var rsw_spellBoot = rsS19[15]; function rsw_popUpCheckSpelling(interfaceObject, interfaceObjectName) { rsw_createSpellBoot(interfaceObject, interfaceObjectName);
																																						 if (typeof (rsw_mult_use_update) != rsS19[3] && rsw_mult_use_update) RS_writeDoc(rswm_PopUpWin.addWordFrame); else RS_writeDoc(rswm_PopUpWin); rswm_PopUpWin.focus();
																																						 } function RS_writeDoc(toWindow) { if(!rsw_isDocDomainSpec())toWindow.document.open(); toWindow.document.write(rsw_spellBoot); if (!rsw_isDocDomainSpec()) toWindow.document.close();
																																						 } var checking = false; var rswm_PopUpWin; var checkCompleted = true; var currentlyChecking = 0; function rswm_RunSpellCheck(buttonClick, optionalID) { if (typeof (buttonClick) == rsS19[3]) buttonClick = true;
																																						 if (buttonClick) { rsw_setup_SpellControls(optionalID); if (rsw_rapidSpellControls.length == 0) { rswm_NotifyDone(true); return; } } checkComplete = true; if (eval(rsS19[189] + rsw_rapidSpellControls[currentlyChecking] + '_SpellChecker)!="undefined"')) { eval(rsS19[190] + rsw_rapidSpellControls[currentlyChecking] + rsS19[191]);
																																						 if (currentlyChecking == 0) { isOverlayed = false; for (var ch = 0; ch < rsw_rapidSpellControls.length; ch++) { eval(rsS19[192] + rsw_rapidSpellControls[ch] + rsS19[191]);
																																						 if (cs == rsS19[193]) isOverlayed = true; } } if (isOverlayed && curState != rsS19[193]) { rswm_NotifyDone(true); } else { eval(rsw_rapidSpellControls[currentlyChecking] + rsS19[194]);
																																						 } } else { try { if (rswm_PopUpWin != null && !rswm_PopUpWin.closed) { } } catch (e) { rswm_PopUpWin = null; } if (buttonClick && rswm_PopUpWin != null && !rswm_PopUpWin.closed) { rswm_PopUpWin.focus();
																																						 } else { if (buttonClick) { checking = true; currentlyChecking = 0; } if (!(currentlyChecking > 0 && rsTCInt[rsw_rapidSpellControls[currentlyChecking]].getText() == rsS19[15])) { rsw_broadcastEvent(rsS19[195], null, rs_s3.getElementById(rsTCInt[rsw_rapidSpellControls[currentlyChecking]].tbName));
																																						 rsTCInt[rsw_rapidSpellControls[currentlyChecking]].hasRun = true; if (rapidSpell.dialog_modal) { rapidSpell.setParameterValue(rsS19[168], rsS19[65], true); rapidSpell.setParameterValue(rsS19[168], rsS19[55], true);
																																						 rsw_mult_use_update = false; rsw_createSpellBoot(rsTCInt[rsw_rapidSpellControls[currentlyChecking]], rsS19[196] + rsw_rapidSpellControls[currentlyChecking] + rsS19[197]);
																																						 rsw_modal_pair = [window, rsw_spellBoot]; showModalDialog(rsw_modalHelperURL, rsw_modal_pair, rsS19[198] + (rapidSpell.dialog_height + 10) + rsS19[199] + (rapidSpell.dialog_width + 15) + rsS19[200] + (rapidSpell.dialog_left >= 0 ? rsS19[201] + rapidSpell.dialog_left + rsS19[202] : rsS19[15]) + (rapidSpell.dialog_top >= 0 ? rsS19[203] + rapidSpell.dialog_top + rsS19[202] : rsS19[15]) + rsS19[204]);
																																						 if (rsw_chrome) { currentlyChecking = rsw_rapidSpellControls.length; } if (currentlyChecking < rsw_rapidSpellControls.length && checkCompleted) rswm_RunSpellCheck(false);
																																						 } else{ if (checking && (rswm_PopUpWin == null || rswm_PopUpWin.closed)) { if (rapidSpell.dialog_blankURL == null) rapidSpell.dialog_blankURL = rsw_scriptLocation + rsS19[205];
																																						 if (rapidSpell.dialog_documentDomain != null && rapidSpell.dialog_documentDomain != rsS19[15] && rapidSpell.dialog_blankURL.indexOf(rsS19[206]) == -1) rapidSpell.dialog_blankURL += (rapidSpell.dialog_blankURL.indexOf(rsS19[207]) > -1 ? rsS19[11] : rsS19[207]) + rsS19[208] + rapidSpell.dialog_documentDomain;
																																						 if (typeof (rsw_create_popup_window) == rsS19[178]) { rswm_PopUpWin = rsw_create_popup_window((rsw_isDocDomainSpec()?(rapidSpell.dialog_blankURL):rsS19[15]), rsS19[209], rsS19[210]+rapidSpell.dialog_left+rsS19[211]+rapidSpell.dialog_top+rsS19[212]+rapidSpell.dialog_width+rsS19[213]+rapidSpell.dialog_height);
																																						 } else rswm_PopUpWin = rs_s2.open((rsw_isDocDomainSpec() ? (rapidSpell.dialog_blankURL) : rsS19[15]), rsS19[209], rsS19[210] + rapidSpell.dialog_left + rsS19[211] + rapidSpell.dialog_top + rsS19[212] + rapidSpell.dialog_width + rsS19[213] + rapidSpell.dialog_height);
																																						 if(rsw_isDocDomainSpec())rsw_waitForLoad(rswm_PopUpWin); var oldUnload = rs_s2.onunload; rs_s2.onunload = function () { if (typeof (oldUnload) == rsS19[178]) oldUnload();
																																						 if (rswm_PopUpWin != null) rswm_PopUpWin.close(); }; } if (rswm_PopUpWin != null && !rswm_PopUpWin.closed) { rsw_mult_use_update = true && currentlyChecking > 0; rsw_popUpCheckSpelling(rsTCInt[rsw_rapidSpellControls[currentlyChecking]], rsS19[196]+rsw_rapidSpellControls[currentlyChecking]+rsS19[197]);
																																						 rsw_mult_use_update = false; } } } else rswm_NotifyDone(true); } } } function rsw_waitForLoad(win) { var i = 0; while (!win.loaded && i++ < 9900000) { } } function rswm_NotifyDone(spellCheckFinished) { var tb = rsw_rapidSpellControls.length == 0?rsS19[168]: rs_s3.getElementById(rsTCInt[rsw_rapidSpellControls[rsw_rapidSpellControls.length - 1]].tbName);
																																						 var v = rsw_getParameterValue(tb, rsS19[58]); var showMesg = v == rsS19[27] || v==true; currentlyChecking++; checkCompleted = spellCheckFinished; if (rapidSpell.dialog_modal) { if (spellCheckFinished && showMesg) alert(rsw_getLanguageArray(rsw_getParameterValue(tb, rsS19[53]))[7]);
																																						 return; } if (currentlyChecking < rsw_rapidSpellControls.length && spellCheckFinished) { rsw_broadcastEvent(rsS19[214], null, rs_s3.getElementById(rsTCInt[rsw_rapidSpellControls[currentlyChecking-1]].tbName), spellCheckFinished);
																																						 rswm_RunSpellCheck(false); } else { checking = false; if (spellCheckFinished && showMesg) if (rswm_PopUpWin) { try { rswm_PopUpWin.alert(rsw_getLanguageArray(rsw_getParameterValue(tb, rsS19[53]))[7]);
																																						 } catch (x) { alert(rsw_getLanguageArray(rsw_getParameterValue(tb, rsS19[53]))[7]); } } else alert(rsw_getLanguageArray(rsw_getParameterValue(tb, rsS19[53]))[7]);
																																						 if (rswm_PopUpWin) rswm_PopUpWin.close(); if (rsw_rapidSpellControls.length > currentlyChecking - 1) { rsw_broadcastEvent(rsS19[214], null, rs_s3.getElementById(rsTCInt[rsw_rapidSpellControls[currentlyChecking - 1]].tbName), spellCheckFinished);
																																						 rsw_broadcastEvent(rsS19[215], null, null, spellCheckFinished); if (typeof (rsw_refreshUnderlines) == rsS19[178] && !rsw_ie9 && !rsw_msie11) rsw_refreshUnderlines();
																																						 } else { rsw_broadcastEvent(rsS19[214], null, null, spellCheckFinished); rsw_broadcastEvent(rsS19[215], null, null, spellCheckFinished); } currentlyChecking = 0; } } function RSAutomaticInterface(tbElementName) { this.tbName = tbElementName;
																																						 this.getText = getText; this.setText = setText; this.identifyTarget = identifyTarget; this.target = null; this.targetContainer = null; this.searchedForTarget = false;
																																						 this.targetIsPlain = true; this.showNoFindError = showNoFindError; this.finder = null; this.findContainer = findContainer; function findContainer() { this.identifyTarget();
																																						 return this.targetContainer; } function showNoFindError() { alert(rsS19[16] + this.tbName + rsS19[17]); } function identifyTarget() { if (!this.searchedForTarget) { this.searchedForTarget = true;
																																						 if (this.finder == null) this.finder = new RSW_EditableElementFinder(); var plain = this.finder.findPlainTargetElement(this.tbName); var richs = this.finder.findRichTargetElements();
																																						 if (plain == null && (richs == null || richs.length == 0) && !rsw_suppressWarnings) showNoFindError(); else { if (richs == null || richs.length == 0) { this.targetIsPlain = true;
																																						 this.target = plain; this.targetContainer = plain; } else { if (plain == null && richs.length == 1) { this.targetIsPlain = false; this.target = this.finder.obtainElementWithInnerHTML(richs[0][0]);
																																						 this.targetContainer = richs[0][1]; } else { var findIdentical = false; for (var rp = 0; rp < richs.length && !findIdentical; rp++) findIdentical = typeof (richs[rp][1].id) != rsS19[3] && richs[rp][1].id == this.tbName + rsS19[18];
																																						 for (var rp = 0; rp < richs.length; rp++) { if (typeof (richs[rp][1].id) != rsS19[3] && ( (!findIdentical && richs[rp][1].id.indexOf(this.tbName) > -1) || (findIdentical && richs[rp][1].id == this.tbName) )) { if (plain != null && richs[rp][1].id == plain.id + rsS19[18]) { this.targetIsPlain = true;
																																						 this.target = plain; this.targetContainer = plain; break; } else { this.targetIsPlain = false; this.target = this.finder.obtainElementWithInnerHTML(richs[rp][0]);
																																						 this.targetContainer = richs[rp][1]; break; } } } if (this.target == null) { this.target = plain; this.targetIsPlain = true; this.targetContainer = plain; } } } } } } function getText() { this.identifyTarget();
																																						 if (this.targetIsPlain) return this.target.value; else return this.target.innerHTML; } function setText(text) { this.identifyTarget(); if (this.targetIsPlain) { var ver = rsw_getInternetExplorerVersion();
																																						 if (ver > 0 && ver < 9) text = text.replace(/</g, rsS19[19]).replace(/>/g, rsS19[20]); this.target.value = text; } else this.target.innerHTML = text; if (typeof (rsw_tbs) != rsS19[3]) { for (var i = 0;
																																						 i < rsw_tbs.length; i++) { if (rsw_tbs[i].shadowTB.id == this.tbName) { if (rsw_tbs[i].updateIframe) { rsw_tbs[i].updateIframe(); } } } } } } function RSW_EditableElementFinder() { this.findPlainTargetElement = findPlainTargetElement;
																																						 this.findRichTargetElements = findRichTargetElements; this.obtainElementWithInnerHTML = obtainElementWithInnerHTML; this.findEditableElements = findEditableElements;
																																						 this.elementIsEditable = elementIsEditable; this.getEditableContentDocument = getEditableContentDocument; function findPlainTargetElement(elementID) { var rsw_elected = rs_s3.getElementById(elementID);
																																						 if (rsw_elected != null && rsw_elected.tagName && (rsw_elected.tagName.toUpperCase() == rsS19[21] || rsw_elected.tagName.toUpperCase() == rsS19[22])) { return rsw_elected;
																																						 } else return null; } function findRichTargetElements(debugTextBox) { var editables = new Array(); this.findEditableElements(document, editables, window, rsS19[15], debugTextBox);
																																						 return editables; } function obtainElementWithInnerHTML(editable) { if (typeof (editable.innerHTML) != rsS19[3]) return editable; else if (typeof (editable.documentElement) != rsS19[3]) return editable.documentElement;
																																						 return null; } function findEditableElements(node, editables, parent, debugInset, debugTextBox) { try { var children = node.childNodes; var editableElement; if (debugTextBox) debugTextBox.value += debugInset + node.tagName;
																																						 if ((editableElement = this.elementIsEditable(node)) != null || (editableElement = this.getEditableContentDocument(node, debugTextBox)) != null ) { if (debugTextBox) debugTextBox.value += rsS19[23];
																																						 editables[editables.length] = editableElement; } if (debugTextBox) debugTextBox.value += rsS19[24]; for (var i = 0; i < children.length; i++) { this.findEditableElements(children[i], editables, node, debugInset + rsS19[25], debugTextBox);
																																						 } } catch (e) { if (debugTextBox) debugTextBox.value += debugInset + rsS19[216] + e; } } function elementIsEditable(element) { if ( ( typeof (element.getAttribute) != rsS19[3] && ( element.getAttribute(rsS19[26]) == rsS19[27] || element.getAttribute(rsS19[28]) == rsS19[29] ) ) || ( (element.contentEditable && element.contentEditable == true) || (element.designMode && element.designMode.toLowerCase() == rsS19[29]) ) ) return [element, element];
																																						 else return null; } function getEditableContentDocument(element, debugTextBox) { if (element.tagName && element.tagName == rsS19[30]) { var kids = new Array(); if (element.contentWindow && element.contentWindow.document) { if (debugTextBox) debugTextBox.value += rsS19[31];
																																						 this.findEditableElements(element.contentWindow.document, kids, element, rsS19[32], debugTextBox); if (kids.length > 0) { var editable = kids[0][0]; if (typeof (editable.body) != rsS19[3]) editable = editable.body;
																																						 return [editable, element]; } } } return null; } } if (typeof (Sys) != rsS19[3] && typeof (Sys.Application) != rsS19[3]) Sys.Application.notifyScriptLoaded(); 
