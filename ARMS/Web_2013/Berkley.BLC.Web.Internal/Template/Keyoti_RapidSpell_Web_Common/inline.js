/*--Keyoti js--*/
//Version 4.4.0g (RapidSpell Web assembly version 4.0.0 onwards)
//Copyright Keyoti Inc. 2005-2014
//This code is not to be modified, copied or used without a license in any form.

var rsS75=new Array("",
"rswinline",
"INACTIVE",
"RS_ContextMenuTable",
"RS_CMItemSeparator",
"RS_ContextMenuItem",
"RS_ContextMenuItem_AllSubItem",
"RS_ContextMenuItem_Disabled",
"oldBrowserBox",
"undefined",
"Mac",
"Gecko",
"MSIE",
"Trident",
"Chrome",
"AppleWebKit",
"NT 6.1",
"NT 6.2",
"\r",
"\n",
"MSIE 9.",
"MSIE 10.",
"CSS1Compat",
"_IF",
"Microsoft Internet Explorer",
"MSIE ([0-9]{1,}[\.0-9]{0,})",
":",
" ",
"rsw_ignorePropertyChange",
"oldValue",
"_SHD",
"DIV",
"_D",
"class",
"style",
"display:none;width:1px; height:1px;",
"display:none;width:1px; height:1px;position:absolute;",
"none",
"pwHtmlBox",
"div",
"__RSFIX",
"absolute",
"IFRAME",
"textarea",
"string",
"%",
"BackCompat",
"px",
"scroll",
"bottom",
"marginTop",
"marginLeft",
"paddingLeft",
"paddingRight",
"borderLeftWidth",
"borderRightWidth",
"paddingTop",
"paddingBottom",
"borderTopWidth",
"borderBottomWidth",
"BODY",
"webkit",
"zIndex",
"position",
"left",
"right",
"top",
"display",
"ms",
"resize",
"width",
"height",
"scrollbar",
"border",
"css",
"get",
"set",
"Moz",
"whiteSpace",
"word",
"text",
"remove",
"item",
"color",
"margin",
"clip",
"visibility",
"padding",
"line",
"lineHeight",
"table",
"max",
"min",
"background",
"overflow",
"visible",
"backgroundColor",
"transparent",
"float",
"marginRight",
"marginBottom",
"borderBottom",
"borderTop",
"borderLeft",
"borderRight",
"borderBottomColor",
"borderTopColor",
"borderLeftColor",
"borderRightColor",
"borderBottomStyle",
"borderTopStyle",
"borderLeftStyle",
"borderRightStyle",
"borderRadius",
"borderLeftRadius",
"borderTopRadius",
"borderRightRadius",
"borderBottomRadius",
"borderTopLeftRadius",
"borderTopRightRadius",
"borderBottomLeftRadius",
"borderBottomRightRadius",
"-moz-border-radius",
"-webkit-border-radius",
"-khtml-border-radius",
"0px",
"overflowX",
"hidden",
"overflowY",
"text/xml",
"Msxml2.XMLHTTP",
"Microsoft.XMLHTTP",
"debug",
"\r\nMicrosoft.XMLHTTP not available",
"\r\n",
"Sending request to ",
"POST",
"Content-Type",
"text/xml; charset=UTF-8",
"Safari",
"application/xml",
"a",
".",
"b",
"<",
">",
"</",
"<input type=hidden name=",
" value='",
"'>",
"function",
"'",
"','",
"<form accept-charset='UTF-8' action='",
"' method='post'>",
"<input type='hidden' name='textToCheck' value=''><input type='hidden' name='IAW' value=''>",
"</form>",
"|",
"<r><resp>xml</resp><textToCheck>",
"</textToCheck><IAW>",
"</IAW>",
"</r>",
"\r\nerror in rsw_spellCheckText ",
"?fiddlerParam=",
"[",
"]",
"Sorry, a textbox with ID=",
" couldn't be found - please check the TextComponentID or TextComponentName property.",
"&lt;",
"&gt;",
"No suggestions",
"Ignore All",
"All",
"Add",
"Edit...",
"Remove duplicate word",
"Checking...",
"Resume Editing",
"Check Spelling",
"No Spelling Errors In Text.",
"Sorry the server has failed to respond to the spell check request. Please check the URL set in the RapidSpellWebInlinePage property in the RapidSpellWebInline ctrl.",
"Textbox with ID=",
" could not be found, please check the TextComponentID property in the RapidSpell control.",
"regTB",
"rich",
"true",
"IgnoreXML",
"True",
"ACTIVE",
"starting check",
"queued ",
"spellcheckfinish",
"(true,-1)",
"ayt_spellcheckfinish",
"EDITING",
"()",
"TRANSITION-CHECKING",
"before:",
"\r\nafter:",
" **abort",
"setting caret",
"block",
"overlay",
"CHECKING",
"firefox",
"inline",
"inline-block",
"server responded ",
" errors",
" **abort rsw_key_downed_flag. rsw_key_down_timeout=",
" flag=",
" lim=",
"caret at:",
",",
"reset caret2",
"(true,numberOfErrors)",
"Popped queued spell check tb.",
"\r\nCallback, readyState:",
" status:",
"id='resultContent'>",
"id='numberOfErrors'>",
"</div>",
"The page holding the RapidSpellWInlineHelper control couldn't be found, please check the URL in the RapidSpellWInlineHelperPage property, it should be set to the URL of the page holding the RapidSpellWInlineHelper control.",
"The page holding the RapidSpellWInlineHelper control returned a 500 server error - which means the page has an error, please visit the URL specified in the RapidSpellWInlineHelperPage to debug this page. ",
"(HINT: Most likely, you need to add validateRequest='false' to the Page directive if you are spell checking HTML content.)",
"There was a problem with the request, please check the URL set in the RapidSpellWInlineHelperPage property. Http Error Code: ",
"RapidSpell AJAX call was cancelled. Status code 0",
"There was a problem with the request. Http Error Code: ",
"BR",
"P",
"<fo",
"rm accept-charset='UTF-8' action='",
"<input type='hidden' name='action' value='add'>",
"UserDictionaryFile",
"<r><action>add</action><w>",
"</w><UserDictionaryFile>",
"</UserDictionaryFile></r>",
"The page holding the RapidSpellWInlineHelper control couldn't be found, please check the URL in the RapidSpellWInlineHelperPage property, it should be set to the URL of the page holding RapidSpellWInlineHelperPage.",
"The page holding the RapidSpellWInlineHelper control returned a 500 server error - which means the page has an error, please visit the URL specified in the RapidSpellWInlineHelperPage to debug this page. (HINT: Most likely, you need to add validateRequest='false' to the Page directive if you are spell checking HTML content.)",
"id='errorContent'>",
"if (rsw_activeTextbox.recordCaretPos) rsw_activeTextbox.recordCaretPos();",
"ayt_contextmenushowing",
"ayt_contextmenushown",
"LABEL",
"setTimeout( function() { rsw_onFinish(",
", ",
");}, 100 ); ",
"ayt_textboxesinitialized",
"boolean",
"LINK",
"text/css",
"head",
"href",
"rel",
"stylesheet",
"rs_err_hl",
"className",
"onmouseup",
"correction",
"#edit",
"br",
"li",
"HTML",
"input",
"p",
"&",
"&amp;",
"keydown",
"rsw_activeTextbox.rsw_key_downed_within_lim=false;",
" DOWN ",
"textedit",
"character",
"A",
"Subscript",
"span",
"suggestions",
"inside",
"\r\nRECORD A ",
"StartToEnd",
"\r\nRECORD B ",
"EndToEnd",
"sentence",
"\r\nRECORD CARET",
"\r\nSET CARET,",
" has\\r=",
"\r\nnew text=>",
"<== \r\n\r\nOLD=>",
"<==\r\n",
"MSIE 7",
"ayt_correction",
"keypress",
"unlink",
"keyup",
"mousedown",
"mouseup",
" rsw_focused",
"focus",
"MSIE 6",
"contentEditable",
"change",
"false",
"blur",
"\r\nblur",
"\r\nSET CONTENT",
" & fromshadow=",
"<br />",
"<nobr>",
"</nobr>",
"contextmenu",
"rsw_activeTextbox.pasting=true;rsw_activeTextbox.updateShadow();if(rsw_activeTextbox.maxlength>0){",
"rsw_setShadowTB(rsw_activeTextbox.shadowTB, rsw_activeTextbox.shadowTB.value.substring(0,rsw_activeTextbox.maxlength));}rsw_activeTextbox.recordCaretPos();rsw_activeTextbox.updateIframe();rsw_activeTextbox.resetCaretPos();rsw_activeTextbox.pasting=false;",
"paste",
"setTimeout( function() {try{ rsw_getTBSFromID('",
"').initialize(",
");}catch(exc){}}, ",
" ); ",
"spellcheck",
"display:inline-block; padding:0; line-height:1; position:absolute; visibility:hidden; font-size:1em",
"M",
"margin:0;",
"AutoUrlDetect",
"RS_MultiLineTB_Disabled",
"RS_MultiLineTB",
"RS_SingleLineTB_Disabled",
"RS_SingleLineTB",
"g",
"firefox/1.0",
"\r\nInnerHTML: ",
"nobr",
"UIEvents",
"<p>$1</p>",
"<p><BR></p>",
"&nbsp;",
"/",
"off",
"on",
"setTimeout( function() { rsw_getTBSFromID('",
");}, 50 ); ",
"fontSize",
"setTimeout( function() {try{ document.getElementById('",
"').contentDocument.designMode = 'On';} catch (exc){} }, 400 ); ",
"if(rsw_activeTextbox!=null&&rsw_activeTextbox.maxlength>0&&rsw_activeTextbox.shadowTB.value.length>rsw_activeTextbox.maxlength){rsw_activeTextbox.updateShadow();rsw_setShadowTB(rsw_activeTextbox.shadowTB, rsw_activeTextbox.shadowTB.value.substring(0,rsw_activeTextbox.maxlength));rsw_activeTextbox.updateIframe();}",
"click",
"multiline",
"yes",
"iframe.style.height",
"px'",
"id",
"nospell",
"td",
"value",
"inlineTB",
"doubleclick",
"<P style='margin:0px;'>",
"</P>",
"<br>",
"\t",
"<span class='tab'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>",
"#text",
"=",
"SCRIPT",
"Opera 5",
"Opera/5",
"^apos^",
"^qt^",
"#",
"changeall",
"subitem",
"remove duplicate",
"no_suggestions",
"-",
"edit",
"ignore_all",
"ShowAddItemAlways",
"add",
"RS_CM_DIV",
"RS_CM_IF",
"msie",
"<tr><td width='100%' ",
"colspan='1'",
"colspan='2'",
"</td>",
"<td>",
"</tr>",
"</table>",
"standard",
"', '",
"</span>",
"_Over",
"out",
"ayt_ignoringAll",
"ayt_ignoredAll",
"ayt_editingAll",
"ayt_adding",
"ayt_added",
"ayt_removingDuplicate",
"ayt_removedDuplicate",
"ayt_changingAll",
"ayt_changedAll",
"ayt_changing",
"ayt_changed",
"oncontextmenu",
"try{event.cancelBubble=true;event.preventDefault();}catch(e){}return false;",
"body",
"src",
"javascript: false;",
"scrolling",
"no",
"frameborder",
"0",
"position:absolute; top:0px; left:0px; display:none;",
"ayt_finished_initializing",
";",
"&amp",
"&nbsp",
"&lt",
"&gt",
"TEXTAREA",
"INPUT",
"designMode",
"*",
"object",
"rsw__init(true)");																																				var rs_s2=window;var rs_s3=document; if (!window.console) rs_s2.console = {}; if (!window.console.log) rs_s2.console.log = function () { }; var rsw_absolutePositionStaticOverlay = false;
																																						 var rsw_copyFontColor = true; var rsw_updatingShadow = false; var rsw_spellCheckRunning = false; var rsw_useBattleShipStyle = false; var rsw_key_down_timeout = 150;
																																						 var rsw_inline_script_loaded = true; var rsw_rs_styleURL = rsS75[0]; var rsw_rs_menu_styleURL = rsS75[0]; var rsw_config = new Array(); var rsw_tbs = new Array();
																																						 var rsw_scs = new Array(); var rsw_isASPX = true; var rsw_copyLineHeight = true; var rsw_activeTextbox; var rsw_previouslyActiveTextbox; var rsw_ASPNETAJAX_OnHandlersAdded = false;
																																						 var rsw_ayt_check = false; var rsw_ayt_enabled = true; var rsw_key_down_timer = null; var rsw_contextMenu = null; var rsw_lastRightClickedError; var rsw_comIF = rs_s2.frames[rsS75[1]];
																																						 var rsw_inProcessTB; var rsw_inProcessSC; var rsw_spellBoot = rsS75[0]; var rsw_channel_state = rsS75[2]; var rsw_channel_timeout; var RS_ContextMenuTable_Class = rsS75[3];
																																						 var RS_CMItemSeparator_Class = rsS75[4]; var RS_ContextMenuItem_Class = rsS75[5]; var RS_ContextMenuItem_AllSubItem_Class = rsS75[6]; var RS_ContextMenuItem_Disabled_Class = rsS75[7];
																																						 var rsw_debug = false; var rsw_inProcessTBResetCaret = true; var rsw_correctCaret = true; var rsw_reconcileChanges = true; var rsw_id_waitingToInitialize = null; var rsw_overlayCSSClassName = rsS75[8];
																																						 var rsw_yScroll = null; var rsw_isMac = typeof (navigator.userAgent) != rsS75[9] && navigator.userAgent.indexOf(rsS75[10]) > -1; var rsw_spellCheckOnBlur = true; var rsw_mozly = navigator.userAgent.indexOf(rsS75[11]) > -1;
																																						 var rsw_msie = navigator.userAgent.indexOf(rsS75[12]) > -1; var rsw_msie11 = navigator.userAgent.indexOf(rsS75[12]) == -1 && navigator.userAgent.indexOf(rsS75[13])>-1;
																																						 var rsw_chrome = navigator.userAgent.indexOf(rsS75[14]) > -1; var rsw_applewebkit = navigator.userAgent.indexOf(rsS75[15]) > -1; var rsw_compatibleBrowser = rsw_msie || rsw_mozly || rsw_chrome || rsw_applewebkit;
																																						 var rsw_W7 = navigator.userAgent.indexOf(rsS75[16]) > -1; var rsw_W8 = navigator.userAgent.indexOf(rsS75[17]) > -1; var rsw_MenuOnRightClick = true; var rsw_newlineexp = new RegExp(rsw_msie?rsS75[18]:rsS75[19]);
																																						 var rsw_ffMaxLengthChecker; var rsw_haltProcesses = false; var rsw_cancelCall = false; var rsw_suppressWarnings = rsw_suppressWarnings ? rsw_suppressWarnings : false;
																																						 var rsw_aux_oninit_handlers = new Array(); var rsw_ObjsToInit = new Array(); var RSWITextBox_DownLevels = new Array(); var rsw_showHorizScrollBarsInFF = false; var rsw_autoFocusAfterAJAX = true;
																																						 var rsw_recalculateOverlayPosition = true; var rsw_adjustOffsetSizeForStrict = true; var rsw_ie9Standards = false; var rsw_ie9; try { rsw_ie9 = navigator.appVersion.indexOf(rsS75[20]) > -1 || navigator.appVersion.indexOf(rsS75[21]) > -1;
																																						 rsw_ie9Standards = rsw_ie9 && rs_s3.compatMode == rsS75[22]; } catch (e) { } function rsw_addTBConfig(config) { var found = false; for (var i = 0; rsw_config != null && i < rsw_config.length;
																																						 i++) { if (rsw_config[i].values[0] == config.values[0]) { found = true; rsw_config[i] = config; } } if (!found) rsw_config[rsw_config.length] = config; } function rsw_refreshActiveTextbox() { if (rsw_activeTextbox != null && rsw_activeTextbox.iframe != null && !rsw_activeTextbox.isStatic) { rsw_activeTextbox.iframe = rs_s3.getElementById(rsw_activeTextbox.iframe.id);
																																						 } return rsw_activeTextbox; } function rsw_getTBConfig(tbid) { for (var i = 0; i < rsw_config.length; i++) { if (rsw_config[i].values[0] == tbid + rsS75[23]) return rsw_config[i];
																																						 } } function rsw_getInternetExplorerVersion() { var rv = -1; if (navigator.appName == rsS75[24]) { var ua = navigator.userAgent; var re = new RegExp(rsS75[25]); if (re.exec(ua) != null) rv = parseFloat(RegExp.$1);
																																						 } return rv; } function rsw_debug_getTime() { var now = new Date(); return now.getHours() + rsS75[26] + now.getMinutes() + rsS75[26] + now.getSeconds() + rsS75[26] + now.getMilliseconds() + rsS75[27];
																																						 } function rsw_setShadowTB(shadow, value) { if (typeof (rsS75[28]) != rsS75[9]) rsw_ignorePropertyChange = true; shadow.removeAttribute(rsS75[29]); shadow.value = value;
																																						 if (typeof (rsS75[28]) != rsS75[9]) rsw_ignorePropertyChange = false; } function rsw_getTBSFromID(id, copyStyle) { for (var i = 0; i < rsw_tbs.length; i++) { if (rsw_tbs[i].shadowTBID == id || rsw_tbs[i].shadowTBID == id+rsS75[30]) { return rsw_tbs[i];
																																						 } } var tbs = _createTBSForPlainTB(id, copyStyle); if (tbs != null) { rsw_tbs[rsw_tbs.length] = tbs; return rsw_tbs[rsw_tbs.length - 1]; } else return null; } function rsw_createBackUpPlainTBS(id) { var divElement = rs_s3.createElement(rsS75[31]);
																																						 divElement.id = id + rsS75[32]; divElement.setAttribute(rsS75[33], rsw_overlayCSSClassName); if (!rsw_absolutePositionStaticOverlay) { divElement.setAttribute(rsS75[34], rsS75[35]);
																																						 rs_s3.getElementById(id).parentNode.insertBefore(divElement, rs_s3.getElementById(id)); } else { divElement.setAttribute(rsS75[34], rsS75[36]); rs_s3.body.appendChild(divElement);
																																						 } var myIFrame = rs_s3.getElementById(id + rsS75[32]); myIFrame.style.display = rsS75[37]; myIFrame.className = rsw_overlayCSSClassName; return myIFrame; } function _createTBSForPlainTB(id, copyStyle) { if (rsw_haltProcesses) return;
																																						 var myIFrame = rs_s3.getElementById(id + rsS75[32]); if (myIFrame == null && rs_s3.getElementById(id) != null) myIFrame = rsw_createBackUpPlainTBS(id); if (myIFrame == null) return null;
																																						 var ptb = new OldIETB(myIFrame); var theTB = rs_s3.getElementById(id); try { if (theTB.name == rsS75[38] && theTB.parentNode.tagName.toLowerCase() == rsS75[39]) { theTB = theTB.parentNode;
																																						 theTB.id = id + rsS75[40]; } } catch (excep) { } if (rsw_absolutePositionStaticOverlay || (theTB.style.position && theTB.style.position == rsS75[41])) { myIFrame.style.position = rsS75[41];
																																						 rsw_updatePosition(myIFrame, theTB); } if (theTB.tagName.toUpperCase() == rsS75[42]) { rsw_auto_copyStyle(myIFrame.id, theTB.id); } myIFrame.style.backgroundColor = theTB.style.backgroundColor;
																																						 if (theTB.style.fontFamily) myIFrame.style.fontFamily = theTB.style.fontFamily; if (theTB.style.fontSize) myIFrame.style.fontSize = theTB.style.fontSize; ptb.initialize();
																																						 if (copyStyle) rsw_copyComputedStyle(myIFrame, theTB); rsw_resetTBSSize(myIFrame, theTB.id); if (theTB.tagName.toLowerCase() != rsS75[43]) ptb.multiline = false; return ptb;
																																						 } function rsw_resetTBSSize(myIFrame, theTBid, isAYT) { var tbWidth = rsw_getElementWidth(theTBid, myIFrame.id.indexOf(rsS75[32]) > -1); var theTB = rs_s3.getElementById(theTBid);
																																						 if (typeof (tbWidth) == rsS75[44] && tbWidth.indexOf(rsS75[45]) > -1) { myIFrame.style.width = tbWidth; } else { if (((rs_s3.compatMode && rs_s3.compatMode != rsS75[46]) || (rsw_mozly && !rsw_msie11 && !rsw_chrome)) && rsw_adjustOffsetSizeForStrict) { if(theTB.tagName.toUpperCase()==rsS75[42]) tbWidth = rsw_adjustOffsetWidthForStrict(myIFrame, tbWidth);
																																						 else tbWidth = rsw_adjustOffsetWidthForStrict(theTB, tbWidth); } if (tbWidth >= 0) myIFrame.style.width = tbWidth + rsS75[47]; } var tbHeight = rsw_getElementHeight(theTBid, myIFrame.id.indexOf(rsS75[32]) > -1);
																																						 if (typeof (tbHeight) == rsS75[44] && tbHeight.indexOf(rsS75[45]) > -1) { myIFrame.style.height = tbHeight; } else { if (((rs_s3.compatMode && rs_s3.compatMode != rsS75[46]) || (rsw_mozly && !rsw_msie11 && !rsw_chrome)) && rsw_adjustOffsetSizeForStrict) { if (theTB.tagName.toUpperCase() == rsS75[42]) tbHeight = rsw_adjustOffsetHeightForStrict(myIFrame, tbHeight);
																																						 else tbHeight = rsw_adjustOffsetHeightForStrict(theTB, tbHeight); } if (tbHeight < 26 && !isAYT) { tbHeight = 50; myIFrame.style.overflowX = rsS75[48]; } if (tbHeight >= 0) myIFrame.style.height = tbHeight + rsS75[47];
																																						 } } function rsw_updatePosition(targetElement, sourceElement) { if (!rsw_absolutePositionStaticOverlay && !(sourceElement.style.position && sourceElement.style.position == rsS75[41])) { targetElement.style.verticalAlign = rsS75[49];
																																						 } else { var marginDeltaY = 0, marginDeltaX = 0; if (((rs_s3.compatMode && rs_s3.compatMode != rsS75[46]) || rsw_mozly) && rsw_adjustOffsetSizeForStrict) { var tpTop = rsw_getStyleProperty(sourceElement, rsS75[50]);
																																						 marginDeltaY = parseInt(tpTop.substring(0, tpTop.length - 2)); var tpLeft = rsw_getStyleProperty(sourceElement, rsS75[51]); marginDeltaX = parseInt(tpLeft.substring(0, tpLeft.length - 2));
																																						 } if (isNaN(marginDeltaX)) marginDeltaX = 0; if (isNaN(marginDeltaY)) marginDeltaY = 0; targetElement.style.left = (rsw_findPosX(sourceElement) - marginDeltaX) + rsS75[47];
																																						 targetElement.style.top = (rsw_findPosY(sourceElement) - marginDeltaY) + rsS75[47]; } } function rsw_adjustOffsetWidthForStrict(el, width) { try { var tpLeft = rsw_getStyleProperty(el, rsS75[52]);
																																						 var tpRight = rsw_getStyleProperty(el, rsS75[53]); var pX = parseInt(tpLeft.substring(0, tpLeft.length - 2)) + parseInt(tpRight.substring(0, tpRight.length - 2));
																																						 var tbLeft = rsw_getStyleProperty(el, rsS75[54]); var tbRight = rsw_getStyleProperty(el, rsS75[55]); var bX = parseInt(tbLeft.substring(0, tbLeft.length - 2)) + parseInt(tbRight.substring(0, tbRight.length - 2));
																																						 if (isNaN(pX) || isNaN(bX)) return width; else { return width - pX - bX; } } catch (e) { return width; } } function rsw_adjustOffsetHeightForStrict(el, height) { try { var tpTop = rsw_getStyleProperty(el, rsS75[56]);
																																						 var tpBottom = rsw_getStyleProperty(el, rsS75[57]); var pY = parseInt(tpTop.substring(0, tpTop.length - 2)) + parseInt(tpBottom.substring(0, tpBottom.length - 2));
																																						 var tbTop = rsw_getStyleProperty(el, rsS75[58]); var tbBottom = rsw_getStyleProperty(el, rsS75[59]); var bY = parseInt(tbTop.substring(0, tbTop.length - 2)) + parseInt(tbBottom.substring(0, tbBottom.length - 2));
																																						 if (isNaN(pY) || isNaN(bY)) return height; else return height - pY - bY; } catch (e) { return height; } } function rsw_getStyleProperty(obj, IEStyleProp) { if (obj.currentStyle) { return obj.currentStyle[IEStyleProp];
																																						 } else if (rs_s3.defaultView.getComputedStyle) { return rs_s3.defaultView.getComputedStyle(obj, null)[IEStyleProp]; } else { return null; } } function rsw_copyComputedStyle(tEl, sEl) { var col;
																																						 var srcIsIFrame = sEl.tagName.toUpperCase() == rsS75[42]; var tarIsIFrame = tEl.tagName.toUpperCase() == rsS75[42] || tEl.tagName.toUpperCase() == rsS75[60]; var tarIsContentWindowBody = tEl.tagName.toUpperCase() == rsS75[60];
																																						 var staticOverRich = srcIsIFrame && !tarIsIFrame; var ayt = tarIsIFrame; if (sEl.currentStyle) col = sEl.currentStyle; else if (rs_s3.defaultView && rs_s3.defaultView.getComputedStyle) col = rs_s3.defaultView.getComputedStyle(sEl, null);
																																						 else return; for (sp in col) { if (isNaN(parseInt(sp))) { try { if (sp.indexOf(rsS75[61]) == -1 && sp != rsS75[62] && sp != rsS75[63] && sp != rsS75[64] && sp != rsS75[65] && sp != rsS75[66] && sp != rsS75[67] && sp.toLowerCase().indexOf(rsS75[68]) == -1 && sp != rsS75[69] && sp != rsS75[49] && sp != rsS75[70] && sp != rsS75[71] && sp.indexOf(rsS75[72]) == -1 && sp.indexOf(rsS75[73]) == -1 && sp.indexOf(rsS75[74]) == -1 && sp.indexOf(rsS75[33]) == -1 && sp.indexOf(rsS75[75]) == -1 && sp.indexOf(rsS75[76]) == -1 && sp.indexOf(rsS75[77]) == -1 && sp.indexOf(rsS75[78]) == -1 && sp.indexOf(rsS75[79]) == -1 && sp.indexOf(rsS75[80]) == -1 && sp.indexOf(rsS75[81]) == -1 && sp.indexOf(rsS75[82]) == -1 && (rsw_copyFontColor || sp!=rsS75[83]) && ((!staticOverRich && !ayt) || sp.indexOf(rsS75[84]) == -1) && sp.indexOf(rsS75[85]) == -1 && sp.indexOf(rsS75[86]) == -1 && ((!staticOverRich && !ayt) || sp.indexOf(rsS75[87]) == -1) && (sp.indexOf(rsS75[88]) == -1 || sp==rsS75[89]) && sp.indexOf(rsS75[90]) == -1 && sp.indexOf(rsS75[91]) == -1 && sp.indexOf(rsS75[92]) == -1 && (!tarIsContentWindowBody || sp.indexOf(rsS75[93])==-1) ) { var v = rsw_getStyleProperty(sEl, sp);
																																						 if (typeof (v) != rsS75[9] && v != rsS75[0] && v != null && (rsw_copyLineHeight || sp!=rsS75[89]) && (sp.indexOf(rsS75[94]) == -1 || v != rsS75[95]) && (sp.indexOf(rsS75[96]) == -1 || v != rsS75[97]) ) { tEl.style[sp] = v;
																																						 } } } catch (ex) { } } } if (rsw_mozly && !rsw_msie11 && !rsw_chrome && !rsw_applewebkit) tEl.style.overflowY = rsS75[48]; } function rsw_auto_copyStyle(iframeId, tbId) { var shtb = rs_s3.getElementById(tbId);
																																						 var ifr = rs_s3.getElementById(iframeId); var isOverlay = ifr.tagName.toUpperCase() == rsS75[31]; try { if (!isOverlay) { if (shtb.style.zIndex) ifr.style.zIndex = shtb.style.zIndex;
																																						 if (!rsw_mozly && !rsw_chrome && !rsw_applewebkit) { rsw_copyComputedStyle(ifr.contentWindow.document.body, shtb); } else { rsw_copyComputedStyle(ifr, shtb); if (ifr.contentWindow) rsw_copyComputedStyle(ifr.contentWindow.document.body, shtb);
																																						 } ifr.style.position = shtb.style.position; ifr.style.left = shtb.style.left; ifr.style.top = shtb.style.top; } var col = null; if (shtb.tagName.toUpperCase() == rsS75[42] && shtb.contentWindow) { if (shtb.currentStyle) col = shtb.contentWindow.document.body.currentStyle;
																																						 else if (shtb.contentWindow.document.defaultView && shtb.contentWindow.document.defaultView.getComputedStyle) col = shtb.contentWindow.document.defaultView.getComputedStyle(shtb.contentWindow.document.body, null);
																																						 } else { if (shtb.currentStyle) col = shtb.currentStyle; else if (rs_s3.defaultView && rs_s3.defaultView.getComputedStyle) col = rs_s3.defaultView.getComputedStyle(shtb, null);
																																						 } if (col != null && !isOverlay) { var borderStyles = [rsS75[98], rsS75[63], rsS75[64], rsS75[56], rsS75[52], rsS75[53], rsS75[57], rsS75[51], rsS75[50], rsS75[99], rsS75[100], rsS75[66], rsS75[73], rsS75[101], rsS75[102], rsS75[103], rsS75[104], rsS75[59], rsS75[58], rsS75[54], rsS75[55], rsS75[105], rsS75[106], rsS75[107], rsS75[108], rsS75[109], rsS75[110], rsS75[111], rsS75[112], rsS75[113], rsS75[114], rsS75[115], rsS75[116], rsS75[117], rsS75[118], rsS75[119], rsS75[118], rsS75[119], rsS75[120], rsS75[121], rsS75[122], rsS75[123], rsS75[124]];
																																						 for (var v = 0; v < borderStyles.length; v++) { if (col[borderStyles[v]]) { if (!(rsw_msie && rsw_getInternetExplorerVersion() <= 9 && borderStyles[v].indexOf(rsS75[73]) > -1 && col[rsS75[110]] == rsS75[37] ) ) { if (borderStyles[v] != rsS75[64] || col[borderStyles[v]] != rsS75[125]) ifr.style[borderStyles[v]] = col[borderStyles[v]];
																																						 } } } } else if (!isOverlay) { if (shtb.style.border) ifr.style.border = shtb.style.border; } else { ifr.style[rsS75[87]] = rsS75[0]; ifr.style[rsS75[52]] = col[rsS75[52]].substring(0, col[rsS75[52]].length - 2) + col[rsS75[51]].substring(0, col[rsS75[51]].length - 2) + rsS75[47];
																																						 ifr.style[rsS75[53]] = col[rsS75[53]].substring(0, col[rsS75[53]].length - 2) + col[rsS75[99]].substring(0, col[rsS75[99]].length - 2) + rsS75[47]; ifr.style[rsS75[56]] = col[rsS75[56]].substring(0, col[rsS75[56]].length - 2) + col[rsS75[50]].substring(0, col[rsS75[50]].length - 2) + rsS75[47];
																																						 ifr.style[rsS75[57]] = col[rsS75[57]].substring(0, col[rsS75[57]].length - 2) + col[rsS75[100]].substring(0, col[rsS75[100]].length - 2) + rsS75[47]; ifr.style[rsS75[126]] = rsS75[127];
																																						 ifr.style[rsS75[128]] = rsS75[48]; } } catch (e) { alert(e); } } var rsw_http_request = false; function rsw_createRequest() { if (rsw_haltProcesses) return; rsw_http_request = false;
																																						 if (rs_s2.XMLHttpRequest) { rsw_http_request = new XMLHttpRequest(); if (rsw_http_request.overrideMimeType) { rsw_http_request.overrideMimeType(rsS75[129]); } } else if (rs_s2.ActiveXObject) { try { rsw_http_request = new ActiveXObject(rsS75[130]);
																																						 } catch (e) { try { rsw_http_request = new ActiveXObject(rsS75[131]); } catch (e) { if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[133]; } } } return rsw_http_request;
																																						 } function rsw_sendRequest(req, url, paramXML, callBack, synchronous) { if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[134] + rsw_debug_getTime() + rsS75[135]+url+rsS75[27];
																																						 req.onreadystatechange = callBack; req.open(rsS75[136], url, !synchronous); req.setRequestHeader(rsS75[137], rsS75[138]); if (!rsw_ie9Standards && typeof (DOMParser) != rsS75[9] && navigator.userAgent.indexOf(rsS75[139]) == -1) { var domParser = new DOMParser();
																																						 var xmlDocument = domParser.parseFromString(paramXML, rsS75[140]); if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[141]; req.send(xmlDocument); if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[142];
																																						 } else { if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[143]; req.send(paramXML); if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[142];
																																						 } if (synchronous) { } } function RapidSpellCheckerClient(helperPageURL) { this.Check = Check; this.AddWord = AddWord; this.userCallback = null; this.result = null;
																																						 this.generateResult = generateResult; this.createErrorHTML = createErrorHTML; this.createErrorMouseUp = createErrorMouseUp; this.textChecked = null; this.OnSpellCheckCallBack = OnSpellCheckCallBack;
																																						 this.rapidSpellWebPage = helperPageURL; this.getSpellBootString = getSpellBootString; this.config; this.useXMLHTTP = true; this.getParameterValue = getParameterValue;
																																						 this.setParameterValue = setParameterValue; function getParameterValue(param) { for (var pp = 0; pp < this.config.keys.length; pp++) { if (this.config.keys[pp] == param) return this.config.values[pp];
																																						 } } function setParameterValue(param, value) { for (var pp = 0; pp < this.config.keys.length; pp++) { if (this.config.keys[pp] == param) this.config.values[pp] = value;
																																						 } } function getSpellBootString(xml) { var res = new String(); if (xml) { for (var pp = 0; pp < this.config.keys.length; pp++) { var val = rsw_escapeHTML(this.config.values[pp]);
																																						 res += rsS75[144] + this.config.keys[pp] + rsS75[145] + val + rsS75[146] + this.config.keys[pp] + rsS75[145]; } } else { for (var pp = 0; pp < this.config.keys.length;
																																						 pp++) { res += rsS75[147] + this.config.keys[pp] + rsS75[148] + this.config.values[pp] + rsS75[149]; } } return res; } function Check(text, asynchronousCallback) { var blocking = true;
																																						 if (typeof (asynchronousCallback) == rsS75[150]) { blocking = false; this.userCallback = asynchronousCallback; } else this.userCallback = null; rsw_inProcessSC = this;
																																						 this.textChecked = text; rsw_spellCheckText(text, true, blocking); if (blocking) { _rsXMLCallBack(); return this.result; } } function AddWord(word) { rsw_inProcessSC = this;
																																						 rsw_serverAdd(word); } function OnSpellCheckCallBack(response, numberOfErrors) { this.result = this.generateResult(response, numberOfErrors); if (this.userCallback != null) this.userCallback(this.result);
																																						 } function unescapeEntities(text) { return rsw_unescapeHTML(text); } function createErrorMouseUp(suggestions) { var suggestionList = rsS75[0]; if (suggestions.length > 0) { suggestionList = rsS75[151];
																																						 for (var i = 0; i < suggestions.length; i++) { var reg1 = new RegExp("'", "g"); var reg2 = new RegExp("\"", "g"); suggestionList += suggestions[i].replace(/\\/g, "\\\\").replace(reg1, "^apos^").replace(reg2, "^qt^");
																																						 if (i < suggestions.length - 1) suggestionList += rsS75[152]; } suggestionList += rsS75[151]; } return suggestionList; } function createErrorHTML(word, suggestions) { var mouseup = createErrorMouseUp(suggestions);
																																						 var html = "<span class='rs_err_hl' onmouseup=\"" + mouseup + "\" oncontextmenu=\"try{event.cancelBubble=true;event.preventDefault();}catch(e){}return false;\">" + word + "</span>";
																																						 return html; } function generateResult(response, numberOfErrors, forceIgnoreXML) { response = unescapeEntities(response); var result = new RapidSpellChecker_Result();
																																						 result.originalText = this.textChecked; result.numberOfErrors = numberOfErrors; var errorReg = /<span class=[^>]* onmouseup="rsw_showMenu\(([^\]]*\]),this,event\)[^>]*>([^<]*)<\/span>/g; var match; var lengthToDiscard = 0; var wordStart = 0; result.errorPositionArray = new Array(); while ((match = errorReg.exec(response)) != undefined) { var sugs = rsw_getSuggestionsArray(match[1]); for (var s = 0; s < sugs.length; s++) { sugs[s] = rsw_decodeSuggestionItem(sugs[s]); } wordStart = match.index - lengthToDiscard; result.errorPositionArray[result.errorPositionArray.length] = { start: wordStart, end: match[2].length + wordStart, word: match[2], suggestions: sugs }; lengthToDiscard += errorReg.lastIndex - match.index - match[2].length; } return result; } } function RapidSpellChecker_Result() { this.originalText; this.numberOfErrors; this.errorPositionArray; } function rsw_spellCheck() { rsw_spellCheckText(rsw_inProcessSC.tbInterface.getText(), rsw_inProcessSC.useXMLHTTP, false); } function rsw_spellCheckText(textToCheck, useXmlHttp, synchronous) { var rsw_useXMLHttpReq = useXmlHttp; var req = false; if (rsw_haltProcesses) return; else if (rsw_cancelCall) rsw_cancelCall = false; if (rsw_useXMLHttpReq) req = rsw_createRequest(); if (!req) { rsw_comIF = rs_s2.frames[rsS75[1]]; rsw_spellBoot = rsS75[153] + rsw_inProcessSC.rapidSpellWebPage + rsS75[154]; rsw_spellBoot += rsS75[155]; rsw_spellBoot += rsw_inProcessSC.getSpellBootString(false); rsw_spellBoot += rsS75[156]; if (rsw_comIF.document.body) rsw_comIF.document.body.innerHTML = rsw_spellBoot; else { rsw_comIF.document.open(); rsw_comIF.document.write(rsw_spellBoot); } rsw_comIF.document.forms[0].textToCheck.value = textToCheck; rsw_comIF.document.forms[0].IAW.value = rsw_ignoreAllWords.join(rsS75[157]); rsw_comIF.document.forms[0].submit(); } else { var paramString = new String(); var text = rsw_escapeHTML(textToCheck); paramString = rsS75[158] + text + rsS75[159] + rsw_ignoreAllWords.join(rsS75[157]) + rsS75[160] + rsw_inProcessSC.getSpellBootString(true) + rsS75[161]; try { req.rsw_sc = rsw_inProcessSC; } catch (error) { if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[162]+error; } if (typeof(rsw_fiddler_debug)!=rsS75[9] && rsw_fiddler_debug) rsw_sendRequest(req, rsw_inProcessSC.rapidSpellWebPage+rsS75[163]+textToCheck, paramString, _rsXMLCallBack, synchronous); else rsw_sendRequest(req, rsw_inProcessSC.rapidSpellWebPage, paramString, _rsXMLCallBack, synchronous); } } function rsw_getSuggestionsArray(handlerCode) { var suggsClean = handlerCode; if(suggsClean.indexOf(rsS75[164])>-1) suggsClean = suggsClean.substring(suggsClean.indexOf(rsS75[164]) + 1, suggsClean.indexOf(rsS75[165])); if (suggsClean.length > 1) { suggsClean = suggsClean.substring(1, suggsClean.length - 1); } var resArray = suggsClean.split(rsS75[152]); if (resArray.length == 1 && resArray[0] == rsS75[0]) return []; else return resArray; } function RSStandardInterface(tbElementName) { this.tbName = tbElementName; this.getText = getText; this.setText = setText; function getText() { var t = rs_s3.getElementById(tbElementName).value; if (t.indexOf(rsS75[18]) == -1) { var rx = new RegExp("\n", "g"); t = t.replace(rx, "\r\n"); } return t; } function setText(text) { rs_s3.getElementById(tbElementName).value = (text); if (rsw_tbs != null) { for (var i = 0; i < rsw_tbs.length; i++) { if (rsw_tbs[i].shadowTB.id == this.tbName) { if (rsw_tbs[i].updateIframe) { rsw_tbs[i].updateIframe(); rsw_tbs[i].focus(); } } } } } } function RSAutomaticInterface(tbElementName) { this.tbName = tbElementName; this.getText = getText; this.setText = setText; this.identifyTarget = identifyTarget; this.target = null; this.targetContainer = null; this.searchedForTarget = false; this.targetIsPlain = true; this.showNoFindError = showNoFindError; this.finder = null; this.findContainer = findContainer; function findContainer() { this.identifyTarget(); return this.targetContainer; } function showNoFindError() { alert(rsS75[166] + this.tbName + rsS75[167]); } function identifyTarget() { if (!this.searchedForTarget) { this.searchedForTarget = true; if (this.finder == null) this.finder = new RSW_EditableElementFinder(); var plain = this.finder.findPlainTargetElement(this.tbName); var richs = this.finder.findRichTargetElements(); if (plain == null && (richs == null || richs.length == 0) && !rsw_suppressWarnings) showNoFindError(); else { if (richs == null || richs.length == 0) { this.targetIsPlain = true; this.target = plain; this.targetContainer = plain; } else { if (plain == null && richs.length == 1) { this.targetIsPlain = false; this.target = this.finder.obtainElementWithInnerHTML(richs[0][0]); this.targetContainer = richs[0][1]; } else { var findIdentical = false; for (var rp = 0; rp < richs.length && !findIdentical; rp++) findIdentical = typeof (richs[rp][1].id) != rsS75[9] && richs[rp][1].id == this.tbName + rsS75[23]; for (var rp = 0; rp < richs.length; rp++) { if (typeof (richs[rp][1].id) != rsS75[9] && ( (!findIdentical && richs[rp][1].id.indexOf(this.tbName) > -1) || (findIdentical && richs[rp][1].id == this.tbName) )) { if (plain != null && richs[rp][1].id == plain.id + rsS75[23]) { this.targetIsPlain = true; this.target = plain; this.targetContainer = plain; break; } else { this.targetIsPlain = false; this.target = this.finder.obtainElementWithInnerHTML(richs[rp][0]); this.targetContainer = richs[rp][1]; break; } } } if (this.target == null) { this.target = plain; this.targetIsPlain = true; this.targetContainer = plain; } } } } } } function getText() { this.identifyTarget(); if (this.targetIsPlain) return this.target.value; else return this.target.innerHTML; } function setText(text) { this.identifyTarget(); if (this.targetIsPlain) { var ver = rsw_getInternetExplorerVersion(); if (ver > 0 && ver < 9) text = text.replace(/</g, rsS75[168]).replace(/>/g, rsS75[169]); this.target.value = text; } else this.target.innerHTML = text; if (typeof (rsw_tbs) != rsS75[9]) { for (var i = 0; i < rsw_tbs.length; i++) { if (rsw_tbs[i].shadowTB.id == this.tbName) { if (rsw_tbs[i].updateIframe) { rsw_tbs[i].updateIframe(); } } } } } } function SpellChecker(textBoxID) { this.state; this.getTBS = getTBS; this.textBoxID = textBoxID; this.rsw_tbs = null; this.OnSpellButtonClicked = OnSpellButtonClicked; this.OnSpellCheckCallBack = OnSpellCheckCallBack; this.finishedListener; this.leaveStaticSpellCheckListener; this.enterStaticSpellCheckListener; this.tbInterface = new RSStandardInterface(textBoxID); this.config; this.getSpellBootString = getSpellBootString; this.buttonID; this.getParameterValue = getParameterValue; this.setParameterValue = setParameterValue; this.showNoSpellingErrorsMesg = true; this.enterEditModeWhenNoErrors = true; this.noSuggestionsText = rsS75[170]; this.ignoreAllText = rsS75[171]; this.showChangeAllItem = false; this.changeAllText = rsS75[172]; this.addText = rsS75[173]; this.editText = rsS75[174]; this.removeDuplicateText = rsS75[175]; this.buttonTextSpellChecking = rsS75[176]; this.buttonTextSpellMode = rsS75[177]; this.buttonText = rsS75[178]; this.noSpellingErrorsText = rsS75[179]; this.changeButtonTextWithState = true; this.showAddMenuItem = true; this.showIgnoreAllMenuItem = true; this.showEditMenuItem = true; this.responseTimeout = 20; this.responseTimeoutMessage = rsS75[180]; this.hasRunFieldID; this.OnTextBoxDoubleClicked = OnTextBoxDoubleClicked; this.doubleClickSwitchesMode = true; this.onLeaveEdit = onLeaveEdit; this.onEnterEdit = onEnterEdit; this.useXMLHTTP; this.ignoreXML = false; this.copyComputedStyleToOverlay = true; this.overlayCSSClassName = rsS75[8]; this.hasRun = false; function OnTextBoxDoubleClicked() { if (this.doubleClickSwitchesMode) this.OnSpellButtonClicked(true); } function getSpellBootString(xml) { var res = new String(); if (xml) { for (var pp = 0; pp < this.config.keys.length; pp++) { var val; if(typeof(this.config.values[pp])==rsS75[44]) val = rsw_escapeHTML(this.config.values[pp]); else val = this.config.values[pp].toString(); res += rsS75[144] + this.config.keys[pp] + rsS75[145] + val + rsS75[146] + this.config.keys[pp] + rsS75[145]; } } else { for (var pp = 0; pp < this.config.keys.length; pp++) { res += rsS75[147] + this.config.keys[pp] + rsS75[148] + this.config.values[pp] + rsS75[149]; } } return res; } function getParameterValue(param) { for (var pp = 0; pp < this.config.keys.length; pp++) { if (this.config.keys[pp] == param) return this.config.values[pp]; } } function setParameterValue(param, value) { for (var pp = 0; pp < this.config.keys.length; pp++) { if (this.config.keys[pp] == param) this.config.values[pp] = value; } } function getTBS() { if (rsw_haltProcesses) return; if (this.rsw_tbs == null) { var el = rs_s3.getElementById(this.textBoxID); if (el == null && !rsw_suppressWarnings) alert(rsS75[181] + this.textBoxID + rsS75[182]); else if (el != null) { rsw_overlayCSSClassName = this.overlayCSSClassName; this.rsw_tbs = rsw_getTBSFromID(this.textBoxID, this.copyComputedStyleToOverlay); if (this.rsw_tbs == null) return null; this.rsw_tbs.spellChecker = this; if (this.rsw_tbs.isStatic) { this.state = rsS75[183]; } else this.state = rsS75[184]; } } if (this.rsw_tbs != null && this.rsw_tbs.isStatic && rsw_recalculateOverlayPosition) { rsw_updatePosition(this.rsw_tbs.iframe, this.rsw_tbs.shadowTB); } if (this.rsw_tbs != null && this.rsw_tbs.isStatic) this.rsw_tbs.targetIsPlain = !this.ignoreXML; return this.rsw_tbs; } var rsw_spellCheckQueue = []; function OnSpellButtonClicked(quietFinish, dontResetCaretPosition) { rsw_spellCheckRunning = true; if (rsw_haltProcesses) { rsw_spellCheckRunning = false; return; } this.hasRun = true; if (this.hasRunFieldID && rs_s3.getElementById(this.hasRunFieldID)) rs_s3.getElementById(this.hasRunFieldID).value = rsS75[185]; if (typeof (this.tbInterface.findContainer) != rsS75[9]) { this.textBoxID = this.tbInterface.findContainer().id; if (!this.tbInterface.targetIsPlain) { this.setParameterValue(rsS75[186], rsS75[187]); this.ignoreXML = true; } } else if (!this.tbInterface.targetIsPlain) { this.setParameterValue(rsS75[186], rsS75[187]); this.ignoreXML = true; } rsw_inProcessTB = this.getTBS(); if (rsw_inProcessTB == null) { rsw_spellCheckRunning = false; return; } if (!rsw_inProcessTB.enabled && (typeof (rsw_ignoreDisabledBoxes) != rsS75[9] && rsw_ignoreDisabledBoxes)) { rsw_spellCheckRunning = false; return; } rsw_inProcessTB.spellChecker = this; rsw_inProcessSC = this; if (this.state == rsS75[183] || this.state == rsS75[184]) { if (rsw_channel_state == rsS75[2]) { rsw_channel_state = rsS75[188]; clearTimeout(rsw_channel_timeout); var lc_SD6F5S67DF576SD57F6S76F576S576E5R76WE5675WE76R76W567SD5F76SD56F7576E76W5R76EW5757 = rsS75[0]; var timeoutFn = 'if(rsw_channel_state == "ACTIVE" && !rsw_suppressWarnings){alert("' + this.responseTimeoutMessage + '");rsw_channel_state = "INACTIVE";}'; rsw_channel_timeout = setTimeout(timeoutFn, this.responseTimeout * 1000); rsw_inProcessTBResetCaret = !dontResetCaretPosition; if (typeof (rsw_inProcessTB.recordCaretPos) != rsS75[9]) rsw_inProcessTB.recordCaretPos(); if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[134] + rsw_debug_getTime() + rsS75[189]; rsw_spellCheck(); if (this.state == rsS75[183]) { rsw_resetTBSSize(rsw_inProcessTB.iframe, this.textBoxID); this.onLeaveEdit(); } } else { rsw_spellCheckQueue[rsw_spellCheckQueue.length] = [rsw_inProcessTB, rsw_inProcessSC]; console.log(rsS75[190] + rsw_inProcessTB.shadowTBID); } } else { rsw_inProcessTB.updateShadow(); rsw_inProcessTB.iframe.style.display = rsS75[37]; rsw_inProcessTB.shadowTB.style.display = rsw_inProcessTB.shadowTBDisplay; this.state = rsS75[183]; try { if (typeof (rsw_inProcessTB.shadowTB.focus) != rsS75[9]) rsw_inProcessTB.shadowTB.focus(); } catch (ee) { } this.onEnterEdit(); rsw_broadcastToListeners(rsS75[191]); if (this.finishedListener != null && this.finishedListener != rsS75[0] && !quietFinish) { eval(this.finishedListener + rsS75[192]); } if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[193],rsw_inProcessTB, true, -1); if (typeof (rswm_auto_NotifyDone) == rsS75[150] && !quietFinish) rswm_auto_NotifyDone(true, -1); } rsw_hideCM(); } function onEnterEdit() { if (rs_s2.rsw_inline_button_OnStateChanged && this.changeButtonTextWithState) { rsw_inline_button_OnStateChanged(rsS75[194], rsw_inProcessSC.buttonID, this.buttonTextSpellChecking, this.buttonTextSpellMode, this.buttonText); } if (this.leaveStaticSpellCheckListener != null && this.leaveStaticSpellCheckListener != rsS75[0]) eval(this.leaveStaticSpellCheckListener + rsS75[195]); } function onLeaveEdit() { if (rs_s2.rsw_inline_button_OnStateChanged && this.changeButtonTextWithState) { rsw_inline_button_OnStateChanged(rsS75[196], rsw_inProcessSC.buttonID, this.buttonTextSpellChecking, this.buttonTextSpellMode, this.buttonText); } if (this.enterStaticSpellCheckListener != null && this.enterStaticSpellCheckListener != rsS75[0]) eval(this.enterStaticSpellCheckListener + rsS75[195]); } this.reconcileChange = reconcileChange; function reconcileChange(beforeS, afterS) { if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[134] + rsw_debug_getTime() + rsS75[197] + beforeS + rsS75[198] + afterS; var dif = RSW_diff(beforeS, afterS); if (dif.position == -1) return [beforeS, false]; else if (dif.vector > 0) { return [this.insertAtVisible(beforeS, dif.addedText, dif.position), true]; } else if (dif.vector < 0) { return [afterS, true]; } else return [beforeS, false]; } this.insertAtVisible = insertAtVisible; function insertAtVisible(str, addition, pos) { var cs = new RSW_VisibleCharSeq(str); return cs.insertAtVisible(addition, pos); } var rsw_OnSpellCheckCallBack_vars_text; var rsw_OnSpellCheckCallBack_vars_innerHTMLLength; var rsw_OnSpellCheckCallBack_vars_haveResetFocus; var rsw_tempClient = new RapidSpellCheckerClient(); function OnSpellCheckCallBack_Breather() { if (rsw_haltProcesses) return; if (rsw_OnSpellCheckCallBack_vars_innerHTMLLength != rsw_inProcessTB.getContent().length) { if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[134] + rsw_debug_getTime() + rsS75[199]; return; } if (typeof (rsw_inProcessTB.insertErrorHighlights) != rsS75[9]) { rsw_inProcessTB.insertErrorHighlights(rsw_tempClient.generateResult(rsw_OnSpellCheckCallBack_vars_text, rsw_OnSpellCheckCallBack_vars_numErrors, rsw_inProcessSC.ignoreXML), rsw_tempClient); } else rsw_inProcessTB.setContent(rsw_OnSpellCheckCallBack_vars_text); if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[134] + rsw_debug_getTime() + rsS75[200]; if (typeof (rsw_inProcessTB.insertErrorHighlights) == rsS75[9] && typeof (rsw_inProcessTB.resetCaretPos) != rsS75[9] && rsw_inProcessTBResetCaret) rsw_inProcessTB.resetCaretPos(); rsw_OnSpellCheckCallBack_vars_haveResetFocus = true; if (rsw_inProcessTB.isStatic) { rsw_inProcessTB.shadowTBDisplay = rsw_getStyleProperty(rsw_inProcessTB.shadowTB, rsS75[67]); if (rsw_inProcessTB.shadowTB.tagName == rsS75[42]) rsw_inProcessTB.iframe.style.display = rsS75[201]; else rsw_inProcessTB.iframe.style.display = rsw_inProcessTB.shadowTBDisplay; if(!rsw_absolutePositionStaticOverlay) rsw_inProcessTB.shadowTB.style.display = rsS75[37]; if (rsw_inProcessSC.state == rsS75[183]) { rsw_inProcessSC.state = rsS75[202]; if (rs_s2.rsw_inline_button_OnStateChanged && rsw_inProcessSC.changeButtonTextWithState) { rsw_inline_button_OnStateChanged(rsS75[203], rsw_inProcessSC.buttonID, rsw_inProcessSC.buttonTextSpellChecking, rsw_inProcessSC.buttonTextSpellMode, rsw_inProcessSC.buttonText); } } if (navigator.userAgent.toLowerCase().indexOf(rsS75[204]) > -1 && rsw_inProcessTB.iframe.style.display == rsS75[205]) { rsw_inProcessTB.iframe.style.display = rsS75[206]; } } } function OnSpellCheckCallBack(text, numberOfErrors) { var workingTB = this.getTBS(); var workingSC = this; if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[134] + rsw_debug_getTime() + rsS75[207] + numberOfErrors + rsS75[208]; try { rsw_channel_state = rsS75[2]; clearTimeout(rsw_channel_timeout); if (rsw_cancelCall) { rsw_cancelCall = false; return; } workingTB.isDirty = false; rsw_OnSpellCheckCallBack_vars_haveResetFocus = false; workingTB.rsw_key_downed_flag = false; var innerHTMLLength = workingTB.getContent().length; if (numberOfErrors > 0) { if (!workingTB.isStatic && !workingTB.noReconcile) { if (rsw_haltProcesses) return; if (typeof (workingTB.insertErrorHighlights) == rsS75[9]) { var curText = workingSC.tbInterface.getText(); if (rsw_mozly) { curText = curText.replace(/\r\n/g, rsS75[19]); curText = curText.replace(/\r/g, rsS75[19]); text = text.replace(/\r\n/g, rsS75[19]); text = text.replace(/\r/g, rsS75[19]); } var rec = this.reconcileChange(text, curText); if (rsw_reconcileChanges) text = rec[0]; } if (workingTB.rsw_key_downed_flag || workingTB.rsw_key_downed_within_lim) { if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[134] + rsw_debug_getTime() + rsS75[209] + rsw_key_down_timeout+rsS75[210]+workingTB.rsw_key_downed_flag +rsS75[211]+ workingTB.rsw_key_downed_within_lim; return; } if (typeof (workingTB.recordCaretPos) != rsS75[9]) workingTB.recordCaretPos(); if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[134] + rsw_debug_getTime() + rsS75[212] + workingTB.caretBL + rsS75[213] + workingTB.caretBT; } rsw_OnSpellCheckCallBack_vars_text = text; rsw_OnSpellCheckCallBack_vars_innerHTMLLength = innerHTMLLength; rsw_OnSpellCheckCallBack_vars_numErrors = numberOfErrors; if (rsw_ayt_check && !rsw_ayt_initializing) setTimeout(OnSpellCheckCallBack_Breather, 10); else OnSpellCheckCallBack_Breather(); } else { if (workingSC.showNoSpellingErrorsMesg) alert(workingSC.noSpellingErrorsText); if (workingSC.state == rsS75[183]) { workingSC.onEnterEdit(); } } if (!workingTB.isStatic && !rsw_ayt_initializing && rsw_inProcessTBResetCaret) { } if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[134] + rsw_debug_getTime() + rsS75[214]; if (numberOfErrors > 0 && !rsw_OnSpellCheckCallBack_vars_haveResetFocus && typeof (workingTB.resetCaretPos) != rsS75[9] && rsw_inProcessTBResetCaret && !rsw_ayt_check) workingTB.resetCaretPos(); rsw_broadcastToListeners(rsS75[191]); if (workingSC.finishedListener != null && workingSC.finishedListener != rsS75[0]) { eval(workingSC.finishedListener + rsS75[215]); } if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[193], workingTB, true, numberOfErrors); if (typeof (rswm_auto_NotifyDone) == rsS75[150]) rswm_auto_NotifyDone(true, numberOfErrors); } catch (e) { rsw_spellCheckRunning = false; } rsw_spellCheckRunning = false; while (rsw_spellCheckQueue.length > 0 && rsw_spellCheckQueue[0] == null) rsw_spellCheckQueue.splice(0, 1); if (rsw_spellCheckQueue.length > 0) { console.log(rsS75[216]); rsw_inProcessTB = rsw_spellCheckQueue[0][0]; rsw_spellCheckTextBox(rsw_inProcessTB); rsw_spellCheckQueue.splice(0, 1); } } } function _rsXMLCallBack() { if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[217] + rsw_http_request.readyState + (rsw_http_request.readyState<=3?rsS75[0]:rsS75[218] + rsw_http_request.status); if (rsw_http_request.readyState == 4) { if (rsw_http_request.status == 200) { var responseText = rsw_http_request.responseText; rsw_showXMLResponseError(responseText); var rcs = responseText.indexOf(rsS75[219]) + 19; var rns = responseText.indexOf(rsS75[220]) + 20; var rce = responseText.lastIndexOf(rsS75[221], rns); var rne = responseText.indexOf(rsS75[221], rns); var responseContent = responseText.substring(rcs, rce); if (responseContent.indexOf(rsS75[18]) == -1) { var pos = -2; while ((pos = responseContent.indexOf(rsS75[19], pos + 2)) > -1) responseContent = responseContent.substring(0, pos) + rsS75[134] + responseContent.substring(pos + 1); } _rsCallBack(responseContent, responseText.substring(rns, rne), this); } else if (rsw_http_request.status == 404 && !rsw_suppressWarnings) { alert(rsS75[222]); } else if (rsw_http_request.status == 500 && !rsw_suppressWarnings) { alert(rsS75[223] + (rsw_isASPX ? rsS75[224] : rsS75[0])); } else if (rsw_http_request.status == 405 && !rsw_suppressWarnings) { alert(rsS75[225] + rsw_http_request.status); } else if (!rsw_suppressWarnings) { if (rsw_http_request.status == 0) { rs_s2.console.log(rsS75[226]); } else alert(rsS75[227] + rsw_http_request.status); } } } function _rsCallBack(text, numberOfErrors, request) { if (request.rsw_sc) request.rsw_sc.OnSpellCheckCallBack(text, numberOfErrors); else if (rsw_inProcessSC) rsw_inProcessSC.OnSpellCheckCallBack(text, numberOfErrors); } function rsw_countCharactersTo(elements, toElement) { var count = 0; var found = false; for (var i = 0; i < elements.length && !found; i++) { if (elements[i] == toElement || elements[i].parentNode == toElement) { return [count, true]; } else { if (elements[i].childNodes.length == 0) { if (elements[i].nodeValue == null) { if (elements[i].nodeName == rsS75[228] || elements[i].nodeName == rsS75[31] || elements[i].nodeName == rsS75[229]) count += 1; } else count += elements[i].nodeValue.length; } else { res = rsw_countCharactersTo(elements[i].childNodes, toElement); count += res[0]; found = res[1]; if (!found && rsw_tagAddsNewline(elements[i])) count += 1; } } } return [count, found]; } function rsw_getAbsSel(range, len, contentElements, findRangeEnd) { var i; var r = new Array(); r[0] = len; r[1] = false; r[2] = false; var tarContainer = findRangeEnd ? range.endContainer : range.startContainer; var tarOffset = findRangeEnd ? range.endOffset : range.startOffset; var numberOfElementsToCount = contentElements.length; var result = rsw_countCharactersTo(contentElements, tarContainer); if (tarContainer.nodeType == Node.TEXT_NODE) { len = result[0] + tarOffset; } else { len = result[0]; result = rsw_countCharactersTo(tarContainer.childNodes, tarContainer.childNodes[tarOffset]); len += result[0]; } r[1] = true; r[2] = true; r[0] = len; return r; } function rsw_findEl(index, elements, r) { var count = 0; if (index == 0) { r[2] = elements[0]; r[3] = 0; r[4] = false; r[1] = true; } for (var i = 0; i < elements.length && count<index; i++) { if (i > 0 && (elements[i-1].nodeName==rsS75[31] || elements[i-1].nodeName==rsS75[229])) count++; var countBeforeThisElement = count; if (elements[i].childNodes.length == 0) { if (elements[i].nodeValue != null) count += elements[i].nodeValue.length; } else { if (index != count) count += rsw_findEl(index - count, elements[i].childNodes, r); } if (count >= index && r.length == 0) { r[2] = elements[i]; r[3] = index - countBeforeThisElement; r[4] = elements[i].nodeName == rsS75[228]; r[1] = true; return count; } } return count; } function rsw_serverAdd(word) { var rsw_useXMLHttpReq = rsw_inProcessSC.useXMLHTTP; var req = false; if (rsw_useXMLHttpReq) req = rsw_createRequest(); if (!req) { rsw_comIF = rs_s2.frames[rsS75[1]]; var boot = rsS75[0]; boot += rsS75[230] + rsS75[231] + rsw_inProcessSC.rapidSpellWebPage + rsS75[154] + rsS75[232] + "<input type='hidden' name='w' value=''><input type='hidden' name='UserDictionaryFile' value=\"\"></form>";
																																						 if (rsw_comIF.document.body) rsw_comIF.document.body.innerHTML = boot; else { rsw_comIF.document.open(); rsw_comIF.document.write(boot); } rsw_comIF.document.forms[0].w.value = word;
																																						 rsw_comIF.document.forms[0].UserDictionaryFile.value = rsw_inProcessSC.getParameterValue(rsS75[233]); rsw_comIF.document.forms[0].submit(); } else { var paramString = new String();
																																						 paramString = rsS75[234] + rsw_escapeHTML(word) + rsS75[235] + rsw_escapeHTML(rsw_inProcessSC.getParameterValue(rsS75[233])) + rsS75[236]; rsw_sendRequest(req, rsw_inProcessSC.rapidSpellWebPage, paramString, rsw_serverAddCallback, false);
																																						 } } function rsw_serverAddCallback() { if (rsw_http_request.readyState == 4) { if (rsw_http_request.status == 200) { rsw_showXMLResponseError(rsw_http_request.responseText);
																																						 } else if (rsw_http_request.status == 404 && !rsw_suppressWarnings) { alert(rsS75[237]); } else if (rsw_http_request.status == 500 && !rsw_suppressWarnings) { alert(rsS75[238]);
																																						 } else if (rsw_http_request.status == 405 && !rsw_suppressWarnings) { alert(rsS75[225] + rsw_http_request.status); } else if (!rsw_suppressWarnings) { alert(rsS75[227] + rsw_http_request.status);
																																						 } } } function rsw_showXMLResponseError(responseText) { var rcs = responseText.indexOf(rsS75[239]) + 18; if (rcs > 17) { var rce = responseText.indexOf(rsS75[221], rcs);
																																						 alert(responseText.substring(rcs, rce)); } } function rsw_showMenu(menuItems, element, e) { rsw_refreshActiveTextbox(); function isRightClick(e) { var rightclick;
																																						 if (!e) var e = rs_s2.event; if (e.which) rightclick = (e.which == 3); else if (e.button) rightclick = (e.button == 2); return rightclick; } rsw_lastRightClickedError = element;
																																						 var atbs = rsw_getTBSHoldingElement(element); if (atbs.focus && !atbs.isFocused) { var yScroll = null; if (typeof (atbs.iframe.contentWindow) != rsS75[9]) yScroll = rsw_getScrollY(atbs.iframe.contentWindow);
																																						 atbs.focus(); if (yScroll != null) rsw_setScrollY(atbs.iframe.contentWindow, yScroll); } rsw_activeTextbox = atbs; setTimeout(rsS75[240], 200); if (rsw_isMac) rsw_MenuOnRightClick = false;
																																						 try{ if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) rsw_MenuOnRightClick = false; }catch(e){} if (!rsw_MenuOnRightClick && (e.button == 1 || e.button == 0)) { rsw_showCM(element, menuItems, e);
																																						 } else if (rsw_MenuOnRightClick && isRightClick(e)) { rsw_showCM(element, menuItems, e); } return false; } function rsw_getTBSHoldingElement(element) { for (var i = 0;
																																						 i < rsw_tbs.length; i++) if (rsw_tbs[i].containsElement(element)) return rsw_tbs[i]; } function rsw_getScrollX(windowEl) { if (windowEl.pageYOffset) { return windowEl.pageXOffset;
																																						 } else if (windowEl.document.documentElement && windowEl.document.documentElement.scrollTop) { return windowEl.document.documentElement.scrollLeft; } else if (windowEl.document.body) { return windowEl.document.body.scrollLeft;
																																						 } } function rsw_getClientWidth(windowEl) { if (windowEl.innerHeight) return windowEl.innerWidth; else if (windowEl.document.documentElement && rs_s3.documentElement.clientHeight) return windowEl.document.documentElement.clientWidth;
																																						 else if (rs_s3.body) return windowEl.document.body.clientWidth; } function rsw_getClientHeight(windowEl) { if (windowEl.innerHeight) return windowEl.innerHeight; else if (windowEl.document.documentElement && rs_s3.documentElement.clientHeight) return windowEl.document.documentElement.clientHeight;
																																						 else if (rs_s3.body) return windowEl.document.body.clientHeight; } function rsw_getScrollY(windowEl) { if (windowEl.pageYOffset) return windowEl.pageYOffset; else if (windowEl.document.documentElement && windowEl.document.documentElement.scrollTop) return windowEl.document.documentElement.scrollTop;
																																						 else if (windowEl.document.body) { return windowEl.document.body.scrollTop; } } function rsw_setScrollY(windowEl, scrollY) { windowEl.scrollTo(0, scrollY); } function rsw_showCM(element, menuItems, event) { rsw_refreshActiveTextbox();
																																						 rsw_contextMenu = new RS_ContextMenu(element, menuItems, rsw_activeTextbox); rsw_contextMenu.x = rsw_activeTextbox.getAbsX(element, event) + 20; rsw_contextMenu.y = rsw_activeTextbox.getAbsY(element, event) + 20;
																																						 if (typeof (rsw_getContextMenuOffsetX) == rsS75[150]) rsw_contextMenu.x += rsw_getContextMenuOffsetX(rsw_contextMenu.x, element, rsw_activeTextbox, event); if (typeof (rsw_getContextMenuOffsetY) == rsS75[150]) rsw_contextMenu.y += rsw_getContextMenuOffsetY(rsw_contextMenu.y, element, rsw_activeTextbox, event);
																																						 var winWidth = rsw_getClientWidth(this) + rsw_getScrollX(this); if (rsw_contextMenu.x + 220 > winWidth) rsw_contextMenu.x = winWidth - 240 - 10; if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[241], rsw_contextMenu, rsw_activeTextbox, event, element);
																																						 rsw_contextMenu.show(); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[242], rsw_contextMenu, rsw_activeTextbox, event, element); var menuHeight = rsw_getElementHeight(rsw_contextMenu.CMelement.id);
																																						 var winHeight = rsw_getClientHeight(this) + rsw_getScrollY(this); if (rsw_contextMenu.y + menuHeight > winHeight) { rsw_contextMenu.y = winHeight - menuHeight - 10;
																																						 } if (rsw_contextMenu.x <= 0) rsw_contextMenu.x = 1; if (rsw_contextMenu.y <= 0) rsw_contextMenu.y = 1; rsw_contextMenu.moveCMElement(); } function rsw__resize() { for (var i = 0;
																																						 i < rsw_tbs.length; i++) { if (rsw_tbs[i].isStatic) { rsw_updatePosition(rs_s3.getElementById(rsw_tbs[i].iframe.id), rsw_tbs[i].shadowTB); } } } function rsw_setSettings(tbs) { if (typeof (tbs.tbConfig) != rsS75[9] && tbs.tbConfig != null && tbs.tbConfig.keys != null) { for (var pp = 3;
																																						 pp < tbs.tbConfig.keys.length; pp++) { try { if (!rsw_useBattleShipStyle || tbs.tbConfig.keys[pp].indexOf(rsS75[73]) == -1) { var c; var parts = tbs.tbConfig.keys[pp].split(rsS75[142]);
																																						 if (parts.length == 1) { tbs[parts[0]] = tbs.tbConfig.values[pp]; } else if (parts.length == 2) { tbs[parts[0]][parts[1]] = tbs.tbConfig.values[pp]; } else if (parts.length == 3) { tbs[parts[0]][parts[1]][parts[2]] = tbs.tbConfig.values[pp];
																																						 } else if (parts.length == 4) { tbs[parts[0]][parts[1]][parts[2]][parts[3]] = tbs.tbConfig.values[pp]; } } } catch (e) { } } var tbHeight = rsw_getElementHeight(tbs.iframe.id);
																																						 if (tbHeight < 26 && tbs.multiline) { tbHeight = 36; tbs.iframe.style.height = tbHeight + rsS75[47]; } tbs.updateIframe(); tbs.iframe.contentWindow.rsw_showMenu = rsw_showMenu;
																																						 } } function rsw__unhook() { for (var i = 0; rsw_tbs != null && i < rsw_tbs.length; i++) { if (rsw_tbs[i] != null) rsw_tbs[i].unhook(); } } function rsw__init(fromAJAXEnd) { if (rsw_haltProcesses) return;
																																						 for (var ptr = 0; ptr < rsw_config.length; ptr++) { if (rsw_haltProcesses) return; var tbConfig = rsw_config[ptr]; var myIFrame = rs_s3.getElementById(tbConfig.values[0]);
																																						 if (myIFrame == null) { rsw_config.splice(ptr, 1); for (var sp = 0; sp < rsw_scs.length; sp++) { if (rsw_scs[sp].textBoxID + rsS75[23] == tbConfig.values[0]) rsw_scs.splice(sp, 1);
																																						 } ptr--; continue; } if (myIFrame.nodeName.toUpperCase() === rsS75[243]) { rsw_tbs[ptr] = new LabelTB(myIFrame, true); } else { if (rsw_chrome || rsw_applewebkit) rsw_tbs[ptr] = new MozlyTB(myIFrame, true);
																																						 else if (rsw_mozly) rsw_tbs[ptr] = new MozlyTB(myIFrame, true); else rsw_tbs[ptr] = new IETB(myIFrame, true); } rsw_tbs[ptr].enabled = tbConfig.values[1]; rsw_tbs[ptr].CssSheetURL = tbConfig.values[2];
																																						 try { rsw_tbs[ptr].tbConfig = tbConfig; rsw_tbs[ptr].initialize(); } catch (ex) { } } rsw_activeTextbox = rsw_tbs[0]; rsw_onFinish(fromAJAXEnd, 0); } function rsw_onFinish(fromAJAXEnd, attempts) { if (rsw_haltProcesses) return;
																																						 if (!attempts) attempts = 0; if (rsw_id_waitingToInitialize != null && attempts < 100) { attempts++; eval(rsS75[244] + fromAJAXEnd + rsS75[245] + attempts + rsS75[246]);
																																						 return; } if (fromAJAXEnd && rsw_autoFocusAfterAJAX) { if (rsw_tbs.length > 0) { var first = rsw_tbs[0]; first.focus(); } } if (rs_s2.RS_OnTextBoxesInitialized) RS_OnTextBoxesInitialized();
																																						 for (var h = 0; h < rsw_ObjsToInit.length; h++) { rsw_ObjsToInit[h].Init(); } for (var h = 0; h < rsw_aux_oninit_handlers.length; h++) { eval(rsw_aux_oninit_handlers[h]);
																																						 } if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[247], null); } function rsw_spellCheckTextBox(textBox, optionalButton) { var found = false;
																																						 if (textBox != null) { if (typeof (textBox.isStatic) == rsS75[248]) { for (var i = 0; i < rsw_scs.length; i++) { if (rsw_scs[i].textBoxID == textBox.shadowTB.id && textBox.isDirty) { if (rsw_scs[i].rsw_tbs != null && rsw_scs[i].rsw_tbs.shadowTB != null && rsw_scs[i].rsw_tbs.shadowTB != textBox) rsw_scs[i].rsw_tbs.shadowTB = textBox;
																																						 if (typeof (optionalButton) != rsS75[9]) rsw_scs[i].buttonID = optionalButton.id; rsw_scs[i].OnSpellButtonClicked(); found = true; } else if (rsw_scs[i].textBoxID == textBox.shadowTB.id) found = true;
																																						 } } else { for (var i = 0; i < rsw_scs.length; i++) { if (rsw_scs[i].textBoxID == textBox.id) { if (rsw_scs[i].rsw_tbs != null && rsw_scs[i].rsw_tbs.shadowTB != null && rsw_scs[i].rsw_tbs.shadowTB != textBox) rsw_scs[i].rsw_tbs.shadowTB = textBox;
																																						 if (typeof (optionalButton) != rsS75[9]) rsw_scs[i].buttonID = optionalButton.id; rsw_scs[i].OnSpellButtonClicked(); found = true; } } } } if (!found && typeof(rsw_addTextBoxSpellChecker)==rsS75[150]) { rsw_addTextBoxSpellChecker(textBox, true, 0);
																																						 if (typeof (optionalButton) != rsS75[9]) rsw_scs[rsw_scs.length - 1].buttonID = optionalButton.id; rsw_scs[rsw_scs.length - 1].OnSpellButtonClicked(); } } function rsw_createLink(contentWindowDoc, CssSheetURL) { var linkElement = contentWindowDoc.createElement(rsS75[249]);
																																						 linkElement.type = rsS75[250]; var url = (typeof (CssSheetURL) == rsS75[9] || CssSheetURL == rsS75[0]) ? rsw_rs_styleURL : CssSheetURL; contentWindowDoc.getElementsByTagName(rsS75[251])[0].appendChild(linkElement);
																																						 linkElement.setAttribute(rsS75[252], url); linkElement.setAttribute(rsS75[253], rsS75[254]); } function rsw_updateActiveTextbox(activeElement) { var activeID = -1;
																																						 for (var i = 0; i < rsw_tbs.length; i++) { if (activeElement == rsw_tbs[i].ifDoc || activeElement == rsw_tbs[i].iframe || (rsw_tbs[i].iframe != null && typeof(rsw_tbs[i].iframe.contentWindow) != rsS75[9] && activeElement == rsw_tbs[i].iframe.contentWindow) || (rsw_tbs[i].iframe != null && typeof (rsw_tbs[i].iframe.contentWindow) != rsS75[9] && rsw_tbs[i].iframe.contentWindow != null && rsw_tbs[i].iframe.contentWindow.document != null && activeElement == rsw_tbs[i].iframe.contentWindow.document.documentElement)) { rsw_previouslyActiveTextbox = rsw_activeTextbox;
																																						 rsw_activeTextbox = rsw_tbs[i]; activeID = i; } } } function rsw_ignoreAll(error) { var errorText = error.innerHTML.replace(/<[^>]+>/g, rsS75[0]); var tError; for (var i = 0;
																																						 i<rsw_tbs.length; i++) { rsw_ignoreAllTB(errorText, rsw_tbs[i]); } } function rsw_ignoreAllTB(errorText, tb) { var errors = tb.getSpanElements(); var changeIndexes = new Array();
																																						 for (var i = 0; i < errors.length; i++) { tError = errors[i].innerHTML.replace(/<[^>]+>/g, rsS75[0]); if (errors[i].className == rsS75[255] && tError == errorText) { rsw_dehighlight(errors[i]);
																																						 rsw_addIgnoreAllWord(errorText); i--; } } } function rsw_dehighlight(errorNode) { try { errorNode.removeNode(false); } catch (e) { errorNode.removeAttribute(rsS75[33]);
																																						 errorNode.removeAttribute(rsS75[256]); errorNode.setAttribute(rsS75[257], rsS75[0]); } } function rsw_getTargetElement(e) { var relTarg; if (!e) var e = rs_s2.event;
																																						 if (e.relatedTarget) relTarg = e.relatedTarget; else if (e.fromElement) relTarg = e.fromElement; return relTarg; } function rsw_edit(error) { rsw_activeTextbox.createEditBox(error);
																																						 rsw_activeTextbox.OnCorrection(new RSW_CorrectionEvent(rsS75[258], error.innerHTML.replace(/<[^>]+>/g, rsS75[0]), rsS75[259])); } function rsw_inlineTB_onBlur() { rsw_refreshActiveTextbox();
																																						 rsw_activeTextbox.updateShadow(); } function rsw_inlineTB_onkeypress(e) { var ev; if (typeof (e) != rsS75[9]) ev = e; else ev = event; if (ev && ev.keyCode) { if (ev.keyCode == 13) { if (ev.preventDefault) ev.preventDefault();
																																						 return false; } } return true; } function rsw_add(error) { rsw_refreshActiveTextbox(); var errorText = rsw_innerHTMLToText(error.innerHTML); rsw_ignoreAll(error);
																																						 rsw_inProcessSC = rsw_activeTextbox.spellChecker; rsw_serverAdd(errorText); } function rsw_innerHTMLToText(html) { return html.replace(/<[^>]+>/g, rsS75[0]); } function rsw_innerText(node, lastElementInCollection, lastElementInCollectionIsBR) { var t = rsS75[0];
																																						 var innerT; if (node.nodeName.toLowerCase() == rsS75[260]) t = rsS75[134]; if (node.nodeName.toLowerCase() == rsS75[261]) t = String.fromCharCode(8226); var nV = null;
																																						 try { nV = node.nodeValue; } catch (er) { } if (node.childNodes.length == 0) { if (nV && node.nodeType == 3) { if (rsw_activeTextbox != null && (rsw_activeTextbox.pasting )) { innerT = nV.replace(/\n/g, rsS75[27]).replace(/\r/g, rsS75[0]);
																																						 } else innerT = nV.replace(/\n/g, rsS75[0]).replace(/\r/g, rsS75[0]); while (rsw_newlineexp.test(innerT)) innerT = innerT.replace(rsw_newlineexp, rsS75[0]); t += innerT;
																																						 } } else { for (var i = 0; i < node.childNodes.length; i++) t += rsw_innerText(node.childNodes[i]); if (node.nodeName.toLowerCase() == rsS75[261]) t += rsS75[134];
																																						 } if(!node.scopeName || (node.scopeName==rsS75[0] || node.scopeName==rsS75[262])){ if (node.nodeName.toLowerCase() == rsS75[263]) t += node.value; if (node.nodeName.toLowerCase() == rsS75[264] && !rsw_nodeHasTrailingBR(node) && !lastElementInCollection && (rsw_activeTextbox == null || rsw_activeTextbox.isStatic || node.parentElement == null || node.parentElement.nodeName.toLowerCase() != rsS75[39]) ) t += rsS75[134];
																																						 if (node.nodeName.toLowerCase() == rsS75[39] && !lastElementInCollection ) t += rsS75[134]; } return t; } function rsw_tagAddsNewline(node) { if (node && node.nodeName && (node.nodeName.toLowerCase() == rsS75[264] || node.nodeName.toLowerCase() == rsS75[39])) { return !rsw_nodeHasTrailingBR(node);
																																						 } return false; } function rsw_nodeHasTrailingBR(node) { var textContent = false; for (var i = node.childNodes.length - 1; i >= 0; i--) { if (node.childNodes[i].childNodes.length > 0) { return rsw_nodeHasTrailingBR(node.childNodes[i]);
																																						 } else { textContent |= rsw_nodeContainsText(node.childNodes[i]); if (node.childNodes[i].nodeName.toLowerCase() == rsS75[260]) return !textContent; } } return false;
																																						 } function rsw_nodeContainsBR(node) { var contains = false; for (var i = 0; i < node.childNodes.length; i++) { contains = contains || node.childNodes[i].nodeName.toLowerCase() == rsS75[260];
																																						 if (rsw_nodeContainsText(node.childNodes[i])) return false; } return contains; } function rsw_nodeContainsText(node) { if (node.nodeValue != null && node.nodeValue.length > 0) return true;
																																						 for (var i = 0; i < node.childNodes.length; i++) { if (rsw_nodeContainsText(node.childNodes[i])) return true; } return false; } function rsw_stringContainsWhitespaceOnly(t) { for(var i=0;
																																						 i<t.length; i++) if( !/\s/.test(t.charAt(i))) return false; return true; } var rsw_ignoreAllWords = new Array(); function rsw_addIgnoreAllWord(word) { var found = false;
																																						 for (var i = 0; i < rsw_ignoreAllWords.length; i++) if (rsw_ignoreAllWords[i] == word) found = true; if (!found) rsw_ignoreAllWords[rsw_ignoreAllWords.length] = word;
																																						 } function rsw_changeTo(error, replacement) { rsw_refreshActiveTextbox(); var orig = rsw_activeTextbox.getShadowText(); rsw_activeTextbox.changeTo(error, replacement);
																																						 rsw_activeTextbox.updateShadow(); rsw_activeTextbox.OnCorrection(new RSW_CorrectionEvent(rsS75[258], error.innerHTML.replace(/<[^>]+>/g, rsS75[0]), replacement, orig));
																																						 } function rsw_changeAllTo(error, replacement) { rsw_refreshActiveTextbox(); var errorText = error.innerHTML.replace(/<[^>]+>/g, rsS75[0]); var tError; var errors = rsw_activeTextbox.getSpanElements();
																																						 for (var i = 0; i < errors.length; i++) { tError = errors[i].innerHTML.replace(/<[^>]+>/g, rsS75[0]); if (errors[i].className == rsS75[255] && tError == errorText) { rsw_changeTo(errors[i], replacement);
																																						 i--; } } } function rsw_escapeHTML(t) { if (typeof (t) != rsS75[44]) return t; var pos = -1; while ((pos = t.indexOf(rsS75[265], pos + 1)) > -1) t = t.substring(0, pos) + rsS75[266] + t.substring(pos + 1);
																																						 var exp1 = new RegExp(rsS75[144]); while (exp1.test(t)) t = t.replace(exp1, rsS75[168]); var exp2 = new RegExp(rsS75[145]); while (exp2.test(t)) t = t.replace(exp2, rsS75[169]);
																																						 return t; } function rsw_unescapeHTML(t) { var pos = -1; while ((pos = t.indexOf(rsS75[266], pos + 1)) > -1) t = t.substring(0, pos) + rsS75[265] + t.substring(pos + 5);
																																						 var exp1 = new RegExp(rsS75[168]); while (exp1.test(t)) t = t.replace(exp1, rsS75[144]); var exp2 = new RegExp(rsS75[169]); while (exp2.test(t)) t = t.replace(exp2, rsS75[145]);
																																						 return t; } function RSWITextBox(controlClientID) { this.shadowTBID = controlClientID; this._getTBS = _getTBS; this._onKeyDown = _onKeyDown; this._onKeyUp = _onKeyUp;
																																						 this._onKeyPress = _onKeyPress; this._onCorrection = _onCorrection; this._onPaste = _onPaste; this._onContextMenu = _onContextMenu; this._onBlur = _onBlur; this._onFocus = _onFocus;
																																						 this._onMouseDown = _onMouseDown; this._onClick = _onClick; this._onMouseUp = _onMouseUp; this.tbs = null; rsw_ObjsToInit[rsw_ObjsToInit.length] = this; this.Init = Init;
																																						 function Init() { this._getTBS(); } function _getTBS() { if (this.tbs == null) { this.tbs = rsw_getTBSFromID(this.shadowTBID, false); this.tbs.repObj = this; } return this.tbs;
																																						 } function _onKeyDown(e) { if (typeof (this.OnKeyDown) == rsS75[150]) this.OnKeyDown(this, e); } function _onKeyUp(e) { if (typeof (this.OnKeyUp) == rsS75[150]) this.OnKeyUp(this, e);
																																						 } function _onKeyPress(e) { if (typeof (this.OnKeyPress) == rsS75[150]) this.OnKeyPress(this, e); } function _onCorrection(e) { if (typeof (this.OnCorrection) == rsS75[150]) this.OnCorrection(this, e);
																																						 } function _onPaste(e) { if (typeof (this.OnPaste) == rsS75[150]) this.OnPaste(this, e); } function _onContextMenu(e) { if (typeof (this.OnContextMenu) == rsS75[150]) this.OnContextMenu(this, e);
																																						 } function _onBlur(e) { if (typeof (this.OnBlur) == rsS75[150]) this.OnBlur(this, e); } function _onFocus(e) { if (typeof (this.OnFocus) == rsS75[150]) this.OnFocus(this, e);
																																						 } function _onMouseDown(e) { if (typeof (this.OnMouseDown) == rsS75[150]) this.OnMouseDown(this, e); } function _onMouseUp(e) { if (typeof (this.OnMouseUp) == rsS75[150]) this.OnMouseUp(this, e);
																																						 } function _onClick(e) { if (typeof (this.OnClick) == rsS75[150]) this.OnClick(this, e); } this.GetText = GetText; this.SetText = SetText; this.OnKeyDown; this.OnKeyUp;
																																						 this.OnKeyPress; this.OnCorrection; this.OnPaste; this.OnContextMenu; this.OnBlur; this.OnFocus; this.OnMouseDown; this.OnMouseUp; this.OnClick; this.GetNumberOfErrors = GetNumberOfErrors;
																																						 this.Focus = Focus; this.SetDisabled = SetDisabled; this.Select = Select; function SetDisabled(disabled) { var tbs = this._getTBS(); return tbs.setDisabled(disabled);
																																						 } function Select() { var tbs = this._getTBS(); tbs.select(); } function GetText() { var tbs = this._getTBS(); return tbs.shadowTB.value; } function SetText(text) { var tbs = this._getTBS();
																																						 rsw_setShadowTB(tbs.shadowTB, text); tbs.updateIframe(); } function GetNumberOfErrors() { var tbs = this._getTBS(); return tbs.getNumberOfErrors(); } function Focus() { var tbs = this._getTBS();
																																						 return tbs.focus(); } } function RSWITextBox_DownLevel(controlClientID) { this.shadowTBID = controlClientID; this._getTBS = _getTBS; this._onKeyDown = _onKeyDown;
																																						 this._onKeyUp = _onKeyUp; this._onKeyPress = _onKeyPress; this._onCorrection = _onCorrection; this._onPaste = _onPaste; this._onContextMenu = _onContextMenu; this._onBlur = _onBlur;
																																						 this._onFocus = _onFocus; this._onMouseDown = _onMouseDown; this._onMouseUp = _onMouseUp; this._onClick = _onClick; this.tbs = null; rsw_ObjsToInit[rsw_ObjsToInit.length] = this;
																																						 RSWITextBox_DownLevels[RSWITextBox_DownLevels.length] = this; this.Init = Init; this.shadowTB; this._getShadowTB = _getShadowTB; function Init() { this._getShadowTB().onkeydown = this._onKeyDown;
																																						 this._getShadowTB().onkeyup = this._onKeyUp; this._getShadowTB().onkeypress = this._onKeyPress; this._getShadowTB().onpaste = this._onPaste; this._getShadowTB().oncontextmenu = this._onContextMenu;
																																						 this._getShadowTB().onblur = this._onBlur; this._getShadowTB().onfocus = this._onFocus; this._getShadowTB().onmouseup = this._onMouseUp; this._getShadowTB().onmousedown = this._onMouseDown;
																																						 this._getShadowTB().onclick = this._onClick; } function _getShadowTB() { if (this.shadowTB == null) this.shadowTB = rs_s3.getElementById(this.shadowTBID); return this.shadowTB;
																																						 } function _getTBS() { if (this.tbs == null) { this.tbs = rsw_getTBSFromID(this.shadowTBID, false); this.tbs.repObj = this; } return this.tbs; } function _tb(e) { if (e) return _findRSWITextBox_DownLevel(e.target.id);
																																						 else return _findRSWITextBox_DownLevel(event.srcElement.id); } function _findRSWITextBox_DownLevel(id) { for (var i = 0; i < RSWITextBox_DownLevels.length; i++) { if (RSWITextBox_DownLevels[i].shadowTBID == id) return RSWITextBox_DownLevels[i];
																																						 } return null; } function _onKeyDown(e) { var tb = _tb(e); if (typeof (tb.OnKeyDown) == rsS75[150]) tb.OnKeyDown(tb, e != null ? e : event); } function _onKeyUp(e) { var tb = _tb(e);
																																						 if (typeof (tb.OnKeyUp) == rsS75[150]) tb.OnKeyUp(tb, e != null ? e : event); } function _onKeyPress(e) { var tb = _tb(e); if (typeof (tb.OnKeyPress) == rsS75[150]) tb.OnKeyPress(tb, e != null ? e : event);
																																						 } function _onCorrection(e) { var tb = _tb(e); if (typeof (tb.OnCorrection) == rsS75[150]) tb.OnCorrection(tb, e != null ? e : event); } function _onPaste(e) { var tb = _tb(e);
																																						 if (typeof (tb.OnPaste) == rsS75[150]) tb.OnPaste(tb, e != null ? e : event); } function _onContextMenu(e) { var tb = _tb(e); if (typeof (tb.OnContextMenu) == rsS75[150]) tb.OnContextMenu(tb, e != null ? e : event);
																																						 } function _onBlur(e) { var tb = _tb(e); if (typeof (tb.OnBlur) == rsS75[150]) tb.OnBlur(tb, e != null ? e : event); } function _onFocus(e) { var tb = _tb(e); if (typeof (tb.OnFocus) == rsS75[150]) tb.OnFocus(tb, e != null ? e : event);
																																						 } function _onMouseDown(e) { var tb = _tb(e); if (typeof (tb.OnMouseDown) == rsS75[150]) tb.OnMouseDown(tb, e != null ? e : event); } function _onMouseUp(e) { var tb = _tb(e);
																																						 if (typeof (tb.OnMouseUp) == rsS75[150]) tb.OnMouseUp(tb, e != null ? e : event); } function _onClick(e) { var tb = _tb(e); if (typeof (tb.OnClick) == rsS75[150]) tb.OnClick(tb, e != null ? e : event);
																																						 } this.GetText = GetText; this.SetText = SetText; this.OnKeyDown; this.OnKeyUp; this.OnKeyPress; this.OnCorrection; this.OnPaste; this.OnContextMenu; this.OnBlur;
																																						 this.OnFocus; this.OnMouseDown; this.OnMouseUp; this.GetNumberOfErrors = GetNumberOfErrors; this.Focus = Focus; this.SetDisabled = SetDisabled; this.Select = Select;
																																						 this.OnClick; function Select() { this._getShadowTB().select(); } function SetDisabled(disabled) { this._getShadowTB().disabled = disabled; } function GetText() { return this._getShadowTB().value;
																																						 } function SetText(text) { this._getShadowTB().value = text; } function GetNumberOfErrors() { var tbs = this._getTBS(); return tbs.getNumberOfErrors(); } function Focus() { return this._getShadowTB().focus();
																																						 } } function rsw_broadcastToListeners(eventType, evt) { rsw_refreshActiveTextbox(); if (eventType == rsS75[267]) { rsw_activeTextbox.rsw_key_downed_flag = true; rsw_activeTextbox.rsw_key_downed_within_lim = true;
																																						 if (rsw_key_down_timer != null) clearTimeout(rsw_key_down_timer); rsw_key_down_timer = setTimeout(rsS75[268], rsw_key_down_timeout); if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[134] + rsw_debug_getTime() + rsw_activeTextbox.shadowTBID+rsS75[269] + String.fromCharCode(rsw_activeTextbox.iframe.contentWindow.event.keyCode);
																																						 } if (rs_s2._INT_notifyTextBoxListeners) _INT_notifyTextBoxListeners(eventType, evt); if (rs_s2._notifyTextBoxListeners) _notifyTextBoxListeners(eventType, evt); } function RSW_IntEvent(eventType) { this.type = eventType;
																																						 } function RSW_CorrectionEvent(eventType, errorWord, replacement, oldText) { this.type = eventType; this.errorWord = errorWord, this.replacement = replacement; this.originalText = oldText;
																																						 } function IETB(iframeEl, editable) { this.skipAYTUpdates = false; this.iframe = iframeEl; this.editable = editable; this.ifDoc; this.initialize = initialize; this.ifDocElement;
																																						 this.setContent = setContent; this.getContent = getContent; this._onKeyPress = _onKeyPress; this._onKeyUp = _onKeyUp; this._onKeyDown = _onKeyDown; this._onPaste = _onPaste;
																																						 this._onMouseDown = _onMouseDown; this._onMouseUp = _onMouseUp; this._onClick = _onClick; this._onContextMenu = _onContextMenu; this._onFocus = _onFocus; this._onBlur = _onBlur;
																																						 this.focus = focus; this.getSpanElements = getSpanElements; this.changeTo = changeTo; this.getAbsY = getAbsY; this.getAbsX = getAbsX; this.isStatic = false; this.getContentText = getContentText;
																																						 this.selectedErrorNode = selectedErrorNode; this.containsElement = containsElement; this.multiline = true; this.enabled = true; this.maxlength = 0; this.shadowTB;
																																						 this.shadowTBID; this.updateIframe = updateIframe; this.updateShadow = updateShadow; this.getShadowText = getShadowText; this.spellChecker; this.OnCorrection = OnCorrection;
																																						 this.oldOnBlur; this.oldOnFocus; this.isDirty = false; this.recordCaretPos = recordCaretPos; this.resetCaretPos = resetCaretPos; this.setCaretPos = setCaretPos; this.caretBL;
																																						 this.caretBT; this.selectedText; this.CssSheetURL; this.getNumberOfErrors = getNumberOfErrors; this.textIsXHTML; this.unhook = unhook; this.repObj = null; this.setDisabled = setDisabled;
																																						 this.select = select; this.isFocused = false; this.insertErrorHighlights = insertErrorHighlights; this.attachEvents = attachEvents; this.isVisible = isVisible; this.container = container;
																																						 function container() { if (this.iframe != null) { if(this.iframe.parentNode) return this.iframe.parentNode; if (this.iframe.parentElement) return this.iframe.parentElement;
																																						 if (this.iframe.parent) return this.iframe.parent; } return null; } function isVisible() { var rect = this.iframe.getBoundingClientRect(); return rect.left != 0 || rect.top != 0 || rect.bottom != 0 || rect.right != 0;
																																						 } function select() { this.focus(); var r = this.ifDoc.selection.createRange(); r.expand(rsS75[270]); r.select(); } function getNumberOfErrors() { var errors = this.getSpanElements();
																																						 var numErrors = 0; for (var i = 0; i < errors.length; i++) { if (errors[i].className == rsS75[255]) { numErrors++; } } return numErrors; } function insertErrorHighlights(result, client) { try { var selection = this.ifDoc.selection;
																																						 var selRange = selection.createRange().duplicate(); var bookmark = selRange.getBookmark(); var range; var numRs; var content = this.getContentText(); var calibration = -1;
																																						 range = this.ifDoc.body.createTextRange().duplicate(); range.collapse(true); var contentChar; var rangeCharCode; var contentCharCode; var lookingForLeadingSpace = false;
																																						 var start = 0; var k = 0; var numberOfChars = 0; for (var i = 0; i < result.errorPositionArray.length; i++) { if (i > 0) start = result.errorPositionArray[i - 1].start;
																																						 lookingForLeadingSpace = result.errorPositionArray[i].word.charAt(0) == rsS75[27]; if (lookingForLeadingSpace) result.errorPositionArray[i].start++; for (var j = start;
																																						 j <= result.errorPositionArray[i].start; j++) { contentCharCode = content.charCodeAt(j); if ( (contentCharCode > 0x20 && contentCharCode < 0x7f) || contentCharCode > 0xA1 ) numberOfChars++;
																																						 } do { range.moveEnd(rsS75[271], 1); rangeCharCode = range.text.charCodeAt(0); if ((rangeCharCode > 0x20 && rangeCharCode < 0x7f) || rangeCharCode > 0xA1) k++; range.move(rsS75[271], 1);
																																						 } while (k < numberOfChars); if (lookingForLeadingSpace) range.move(rsS75[271], -2); else range.move(rsS75[271], -1); var startRangeParent = range.parentElement();
																																						 if (lookingForLeadingSpace) delta = -1; else delta = 0; range.moveEnd(rsS75[271], (result.errorPositionArray[i].end - result.errorPositionArray[i].start) - delta);
																																						 var rangeParent = range.parentElement(); if (rangeParent.nodeName == rsS75[272]) { rangeParent.removeNode(false); rangeParent = range.parentElement(); } if (rangeParent != null && (rangeParent.getAttribute(rsS75[33]) == null || rangeParent.getAttribute(rsS75[33]) != rsS75[255]) && range.text == result.errorPositionArray[i].word) { if (startRangeParent.getAttribute(rsS75[33]) == rsS75[255]) startRangeParent.removeNode(false);
																																						 range.execCommand(rsS75[273], false); var span = this.ifDoc.createElement(rsS75[274]); span.setAttribute(rsS75[256], rsS75[255]); span.setAttribute(rsS75[33], rsS75[255]);
																																						 var mouseup = client.createErrorMouseUp(result.errorPositionArray[i].suggestions); span.setAttribute(rsS75[275], mouseup); span.onmouseup = function () { rsw_clickedSpan = this;
																																						 }; span.attachEvent(rsS75[257], function () { if (rsw_clickedSpan != null) { var suggsClean = rsw_clickedSpan.getAttribute(rsS75[275]); rsw_showMenu(rsw_getSuggestionsArray(suggsClean), rsw_clickedSpan, arguments[0]);
																																						 } rsw_clickedSpan = null; }); span.oncontextmenu = function () { try { event.cancelBubble = true; event.preventDefault(); } catch (e) { } return false; }; var italic = range.parentElement();
																																						 italic.applyElement(span, rsS75[276]); italic.removeNode(false); } } } catch (e) { } } function recordCaretPos() { try { var selection = this.ifDoc.selection; var range = selection.createRange().duplicate();
																																						 if (rsw_ie9) { var start = 0, end = 0, normalizedValue, textInputRange, len, endRange; el = this.ifDoc; this.origTextInRange = this.getContentText(); len = this.origTextInRange.length;
																																						 normalizedValue = this.origTextInRange.replace(/\r\n/g, rsS75[19]); textInputRange = this.ifDoc.body.createTextRange(); textInputRange.moveToBookmark(range.getBookmark());
																																						 endRange = this.ifDoc.body.createTextRange(); endRange.collapse(false); if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[277] + textInputRange.text + rsS75[27] + textInputRange.compareEndPoints(rsS75[278], endRange);
																																						 if (textInputRange.compareEndPoints(rsS75[278], endRange) > -1) { start = end = len; } else { start = -textInputRange.moveStart(rsS75[271], -len); if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[279] + textInputRange.compareEndPoints(rsS75[280], endRange);
																																						 if (textInputRange.compareEndPoints(rsS75[280], endRange) > -1) { end = len; } else { end = -textInputRange.moveEnd(rsS75[271], -len); end += normalizedValue.slice(0, end).split(rsS75[19]).length - 1;
																																						 } } this.cursorPosition = start; } else { range.moveStart(rsS75[281], -9000000); this.origTextInRange = range.text.replace(/\n/g, rsS75[0]); this.cursorPosition = this.origTextInRange.length;
																																						 } if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[282] + this.cursorPosition; } catch (e) { } } function setCaretPos(characterIndex) { var selection = this.ifDoc.selection;
																																						 var newRange = selection.createRange(); newRange.move(rsS75[281], -9000000); newRange.moveStart(rsS75[271], characterIndex); newRange.collapse(false); newRange.select();
																																						 } function resetCaretPos() { try { var selection = this.ifDoc.selection; var newRange = selection.createRange(); newRange.move(rsS75[281], -9000000); var o = newRange.moveEnd(rsS75[271], this.cursorPosition);
																																						 var ignNewL = this.origTextInRange.replace(/\r/g, rsS75[0]); if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[283] + o + rsS75[284] + newRange.text.indexOf(rsS75[18]) + rsS75[285] + newRange.text.replace(/\r\n/g, rsS75[19]) + rsS75[286] + ignNewL + rsS75[287];
																																						 ; var moveAmount = 1; if (navigator.userAgent.indexOf(rsS75[288]) > -1) newRange.moveStart(rsS75[271], this.cursorPosition); else newRange.collapse(false); newRange.select();
																																						 } catch (e) { } } function OnCorrection(e) { rsw_refreshActiveTextbox(); if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onCorrection(e); rsw_broadcastToListeners(rsS75[258], e);
																																						 if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[289], rsw_activeTextbox, e); } function focus() { this.iframe.contentWindow.focus(); try { this.iframe.focus();
																																						 } catch (e) { } this.iframe.contentWindow.focus(); this.isFocused = true; try { var caret = this.ifDoc.selection.createRange(); correctCaret(caret); } catch (e) { } } function containsElement(element) { return element.ownerDocument == this.ifDoc;
																																						 } function selectedErrorNode() { rsw_refreshActiveTextbox(); try { var selection = rsw_activeTextbox.ifDoc.selection; var parentEl = selection.createRange().parentElement();
																																						 if (parentEl.className == rsS75[255]) return parentEl; else { if (parentEl.children.length > 0 && parentEl.children[parentEl.children.length - 1].className==rsS75[255]) { var r = this.ifDoc.body.createTextRange();
																																						 r.moveToElementText(parentEl.children[parentEl.children.length - 1]); var r2 = selection.createRange(); if (r.compareEndPoints(rsS75[280], r2) == 0) { return parentEl.children[parentEl.children.length - 1];
																																						 } else { r2.moveStart(rsS75[271], r.compareEndPoints(rsS75[280], r2)); if (r2.text == rsS75[151]) return parentEl.children[parentEl.children.length - 1]; } } return null;
																																						 } } catch (e) { return null; } } function getAbsX(element, event) { return element.getBoundingClientRect().left + rsw_activeTextbox.iframe.getBoundingClientRect().left + rsw_getScrollX(window);
																																						 } function getAbsY(element, event) { return element.getBoundingClientRect().top + rsw_activeTextbox.iframe.getBoundingClientRect().top + rsw_getScrollY(window); } function changeTo(error, replacement) { try { var repl = this.ifDoc.createTextNode(replacement);
																																						 error.parentNode.replaceChild(repl, error); } catch (e) { return null; } } function getSpanElements() { return this.ifDoc.getElementsByTagName(rsS75[274]); } function _onKeyPress() { rsw_refreshActiveTextbox();
																																						 rsw_hideCM(); var evt = rsw_activeTextbox.iframe.contentWindow.event; var errorNode = rsw_activeTextbox.selectedErrorNode(); if (errorNode) rsw_dehighlight(errorNode);
																																						 if (evt != null && evt.keyCode == 13 && !rsw_activeTextbox.multiline) { evt.returnValue = false; } else if (evt != null && evt.keyCode == 13) { } rsw_activeTextbox.isDirty = true;
																																						 if (rsw_activeTextbox.maxlength > 0) { if (rsw_activeTextbox.getContentText().replace(/\r/g, rsS75[0]).replace(/\n/g, rsS75[0]).length >= rsw_activeTextbox.maxlength) evt.returnValue = false;
																																						 } if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onKeyPress(rsw_activeTextbox.iframe.contentWindow.event); rsw_broadcastToListeners(rsS75[290], rsw_activeTextbox.iframe.contentWindow.event);
																																						 if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[290], rsw_activeTextbox, evt); } function _onKeyDown() { rsw_refreshActiveTextbox(); if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onKeyDown(rsw_activeTextbox.iframe.contentWindow.event);
																																						 rsw_broadcastToListeners(rsS75[267], rsw_activeTextbox.iframe.contentWindow.event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[267], rsw_activeTextbox, rsw_activeTextbox.iframe.contentWindow.event);
																																						 } function _onKeyUp() { rsw_refreshActiveTextbox(); rsw_hideCM(); if (!rsw_ie9) rsw_activeTextbox.ifDoc.body.createTextRange().execCommand(rsS75[291]); var evt = rsw_activeTextbox.iframe.contentWindow.event;
																																						 if (evt == null || !(evt.keyCode >= 33 && evt.keyCode <= 40)) { var errorNode = rsw_activeTextbox.selectedErrorNode(); if (errorNode) rsw_dehighlight(errorNode); } rsw_activeTextbox.updateShadow();
																																						 if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onKeyUp(evt); rsw_broadcastToListeners(rsS75[292], evt); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[292], rsw_activeTextbox, evt);
																																						 } function _onMouseDown() { rsw_refreshActiveTextbox(); if (rsw_activeTextbox == null) { if ((rsw_chrome || rsw_applewebkit) && typeof (this.document) != rsS75[9]) rsw_updateActiveTextbox(this.document);
																																						 else rsw_updateActiveTextbox(this); } if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onMouseDown(rsw_activeTextbox.iframe.contentWindow.event); rsw_hideCM();
																																						 rsw_broadcastToListeners(rsS75[293], rsw_activeTextbox.iframe.contentWindow.event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[293], rsw_activeTextbox, rsw_activeTextbox.iframe.contentWindow.event);
																																						 rsw_activeTextbox.updateShadow(); } function _onMouseUp() { rsw_refreshActiveTextbox(); if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onMouseUp(rsw_activeTextbox.iframe.contentWindow.event);
																																						 rsw_broadcastToListeners(rsS75[294], rsw_activeTextbox.iframe.contentWindow.event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[294], rsw_activeTextbox, rsw_activeTextbox.iframe.contentWindow.event);
																																						 } function _onClick() { } function _onFocus(event, circle) { rsw_refreshActiveTextbox(); if (typeof (rsw_activeTextbox.iframe.contentWindow) != rsS75[9]) { rsw_yScroll = rsw_getScrollY(rsw_activeTextbox.iframe.contentWindow);
																																						 rsw_activeTextbox.ifDoc.body.className += rsS75[295]; } if (typeof (rsw_useIFrameMenuBacker) == rsS75[9] || rsw_useIFrameMenuBacker) rsw_hideCM(); rsw_activeTextbox.updateShadow();
																																						 rsw_updateActiveTextbox(this); rsw_activeTextbox.isFocused = true; if (rsw_correctCaret) { var caret = rsw_activeTextbox.ifDoc.selection.createRange(); correctCaret(caret);
																																						 } rsw_broadcastToListeners(rsS75[296], event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[296], rsw_activeTextbox, event); if (navigator.userAgent.indexOf(rsS75[288]) > -1 || navigator.userAgent.indexOf(rsS75[297]) > -1) { rsw_activeTextbox.ifDoc.body.setAttribute(rsS75[298], rsS75[185]);
																																						 if (rsw_yScroll != null) rsw_setScrollY(rsw_activeTextbox.iframe.contentWindow, rsw_yScroll ); } if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onFocus(new RSW_IntEvent(rsS75[296]));
																																						 if (rsw_activeTextbox.oldOnFocus && rsw_activeTextbox.oldOnFocus != rsw_activeTextbox._onFocus && !circle) rsw_activeTextbox.oldOnFocus(event, true); } function _onBlur(event, circle) { rsw_refreshActiveTextbox();
																																						 if (rsw_activeTextbox != null) { rsw_activeTextbox.isFocused = false; rsw_activeTextbox.rsw_key_downed_within_lim = false; rsw_activeTextbox.updateShadow(); rsw_activeTextbox.ifDoc.body.className = rsw_activeTextbox.ifDoc.body.className.replace(/(?:^|\s)rsw_focused(?!\S)/g, rsS75[0]);
																																						 } function callOnchange(event, textbox) { return function () { try { if (typeof (rsw_fireEventInShadow) == rsS75[150]) rsw_fireEventInShadow(rsS75[299], textbox);
																																						 else if (typeof (textbox.shadowTB.onchange) == rsS75[150]) textbox.shadowTB.onchange(event); } catch (x) { } } } if (rsw_activeTextbox != null && rsw_activeTextbox.shadowTB.defaultValue != rsw_activeTextbox.shadowTB.value) setTimeout(callOnchange(event, rsw_activeTextbox), 100);
																																						 if (navigator.userAgent.indexOf(rsS75[288]) > -1 || navigator.userAgent.indexOf(rsS75[297]) > -1) rsw_activeTextbox.ifDoc.body.setAttribute(rsS75[298], rsS75[300]);
																																						 if (rsw_activeTextbox != null && rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onBlur(new RSW_IntEvent(rsS75[301])); if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[302];
																																						 if (rsw_activeTextbox != null && rsw_activeTextbox.isAYT && rsw_spellCheckOnBlur) rsw_spellCheckTextBox(rsw_activeTextbox); rsw_broadcastToListeners(rsS75[301], event);
																																						 if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[301], rsw_activeTextbox, event); if (rsw_activeTextbox != null && rsw_activeTextbox.oldOnBlur && rsw_activeTextbox.oldOnBlur != rsw_activeTextbox._onBlur && !circle) rsw_activeTextbox.oldOnBlur(event, true);
																																						 } function setContent(content, contentIsFromShadow) { if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[303] + content + rsS75[304] + contentIsFromShadow;
																																						 var pos = -1; var ppos = 0; var t = rsS75[0]; while ((pos = content.indexOf(rsS75[19], pos + 1)) > -1) { if (pos > ppos + 2) { if (content.substring(pos - 1, pos) == rsS75[18]) t += content.substring(ppos, pos - 1) + rsS75[305];
																																						 else t += content.substring(ppos, pos) + rsS75[305]; } else { if (content.charAt(ppos) != rsS75[18]) t += content.substring(ppos, pos) + rsS75[305]; else t += rsS75[305];
																																						 } ppos = pos + 1; } if ((ppos < content.length || ppos == 0) && !rsw_ie9Standards) { } else if (ppos == content.length) { } else { } t += content.substring(ppos, content.length);
																																						 if (!this.multiline) { var pos = -1; var ppos = 0; var opener = -1; var closer = -1; while ((pos = t.indexOf(rsS75[27], pos + 1)) > -1) { opener = t.lastIndexOf(rsS75[144], pos);
																																						 closer = t.lastIndexOf(rsS75[145], pos); if ((opener == -1 && closer == -1) || opener == -1 || opener < closer) t = t.substring(0, pos) + String.fromCharCode(160) + t.substring(pos + 1);
																																						 ppos = pos; } t = rsS75[306] + t + rsS75[307]; } else { var pos = -1; var ppos = 0; var opener = -1; var closer = -1; var flag = true; while ((pos = t.indexOf(rsS75[27], pos + 1)) > -1) { if (pos + 1 < t.length && (t.charAt(pos + 1) == rsS75[27] || (pos > 4 && t.charAt(pos - 1) == rsS75[145] && t.substring(pos - 5, pos - 1) != rsS75[274]))) { opener = t.lastIndexOf(rsS75[144], pos);
																																						 closer = t.lastIndexOf(rsS75[145], pos); if ((opener == -1 && closer == -1) || opener == -1 || opener < closer) { t = t.substring(0, pos) + String.fromCharCode(160) + t.substring(pos + 1);
																																						 } ppos = pos; } } } if (this.ifDoc!=null && this.ifDoc.body != null) this.ifDoc.body.innerHTML = t; if (!contentIsFromShadow && this.ifDoc != null && this.ifDoc.body != null) this.updateShadow();
																																						 } function correctCaret(caret) { if (caret.text.length == 0 && caret.moveStart(rsS75[271], -1) < 0) { caret.select(); caret.moveStart(rsS75[271], 1); caret.select();
																																						 caret.collapse(true); } caret.select(); } function getContent() { return this.ifDoc.body.innerHTML; } function getContentText() { if (this.ifDocElement == null) return rsS75[0];
																																						 var contentElements; for(var i=0; i<this.ifDocElement.childNodes.length;i++) if (this.ifDocElement.childNodes[i].tagName==rsS75[60]) contentElements = this.ifDocElement.childNodes[i].childNodes;
																																						 var contents = rsS75[0]; if (contentElements == null) return rsS75[0]; for (var i = 0; i < contentElements.length; i++) { var nV = null; try { nV = contentElements[i].nodeValue;
																																						 } catch (er) { } if (nV) contents += nV; else if (contentElements[i].nodeName.toLowerCase() == rsS75[260] && i < contentElements.length - 1) contents += rsS75[134];
																																						 else if (contentElements[i].nodeName.toLowerCase() == rsS75[263]) contents += contentElements[i].value; else contents += rsw_innerText(contentElements[i], i == contentElements.length - 1, contentElements[contentElements.length - 1].nodeName.toLowerCase() == rsS75[260]);
																																						 } return contents; } function _onContextMenu() { rsw_refreshActiveTextbox(); if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onContextMenu(rsw_activeTextbox.iframe.contentWindow.event);
																																						 rsw_broadcastToListeners(rsS75[308], rsw_activeTextbox.iframe.contentWindow.event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[308], rsw_activeTextbox, rsw_activeTextbox.iframe.contentWindow.event);
																																						 } function _onPaste() { rsw_refreshActiveTextbox(); if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onPaste(rsw_activeTextbox.iframe.contentWindow.event);
																																						 var errorNode = rsw_activeTextbox.selectedErrorNode(); if (errorNode) rsw_dehighlight(errorNode); setTimeout(rsS75[309] + rsS75[310], 300); rsw_broadcastToListeners(rsS75[311], rsw_activeTextbox.iframe.contentWindow.event);
																																						 if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[311], rsw_activeTextbox, rsw_activeTextbox.iframe.contentWindow.event); } function initialize(attempts) { var ifID;
																																						 if (!this.iframe) ifID = this.tbConfig.values[0]; else ifID = this.iframe.id; this.shadowTBID = ifID.substring(0, ifID.length - 3); this.shadowTB = rs_s3.getElementById(this.shadowTBID);
																																						 if (rsw_id_waitingToInitialize == null) rsw_id_waitingToInitialize = ifID; if (!attempts) attempts = 0; if ((!this.iframe.contentWindow.loaded && attempts < 100) || attempts == 0 || rsw_id_waitingToInitialize != ifID) { var time = 50;
																																						 attempts++; eval(rsS75[312] + this.shadowTBID + rsS75[313] + attempts + rsS75[314] + time + rsS75[315]); return; } rsw_id_waitingToInitialize = null; this.ifDoc = this.iframe.contentWindow.document;
																																						 this.iframe.contentWindow.document.documentElement.setAttribute(rsS75[316], false); this.ifDocElement = this.iframe.contentWindow.document.documentElement; function getDefaultFontSize(pa) { pa = pa || rs_s3.body;
																																						 var who = rs_s3.createElement(rsS75[39]); who.style.cssText = rsS75[317]; who.appendChild(rs_s3.createTextNode(rsS75[318])); pa.appendChild(who); var fs = [who.offsetWidth, who.offsetHeight];
																																						 pa.removeChild(who); return fs; } this.iframe.contentWindow.document.documentElement.style.fontSize = getDefaultFontSize(this.shadowTB.parentElement)[1]+rsS75[47];
																																						 rsw_createLink(this.ifDoc, this.CssSheetURL); this.ifDoc.styleSheets[0].addRule(rsS75[229], rsS75[319]); if (this.enabled) { if (this.editable) { this.ifDoc.body.setAttribute(rsS75[298], rsS75[185]);
																																						 } this.attachEvents(); } rsw_setSettings(this); } function attachEvents() { if (this.ifDocElement.onmousedown != this._onMouseDown) { this.ifDocElement.onmousedown = this._onMouseDown;
																																						 this.ifDocElement.onmouseup = this._onMouseUp; this.ifDocElement.onclick = this._onClick; this.ifDocElement.onkeypress = this._onKeyPress; this.ifDocElement.onkeydown = this._onKeyDown;
																																						 this.ifDocElement.onkeyup = this._onKeyUp; this.ifDocElement.onpaste = this._onPaste; } if (this._onFocus != this.iframe.onfocus) { this.oldOnFocus = this.iframe.onfocus;
																																						 this.iframe.onfocus = this._onFocus; this.oldOnBlur = this.iframe.onblur; this.iframe.onblur = this._onBlur; this.ifDoc.oncontextmenu = this._onContextMenu; } try { rs_s3.execCommand(rsS75[320], false, false);
																																						 } catch (exc) { } } function setDisabled(disabled) { this.ifDoc.body.setAttribute(rsS75[298], !disabled); this.enabled = !disabled; if (this.enabled) this.attachEvents();
																																						 if (typeof (rsw_ignoreDisabledBoxes) != rsS75[9] && rsw_ignoreDisabledBoxes && disabled) this.updateIframe(); if (this.enabled) this.attachEvents(); if (this.multiline) { if (disabled) this.ifDoc.body.className = rsS75[321];
																																						 else this.ifDoc.body.className = rsS75[322]; } else { if (disabled) this.ifDoc.body.className = rsS75[323]; else this.ifDoc.body.className = rsS75[324]; } } function unhook() { this.ifDoc.body.setAttribute(rsS75[298], rsS75[300]);
																																						 this.ifDocElement.onmousedown = null; this.ifDocElement.onmouseup = null; this.ifDocElement.onclick = null; this.ifDocElement.onkeypress = null; this.ifDocElement.onkeydown = null;
																																						 this.ifDocElement.onkeyup = null; this.ifDocElement.onpaste = null; this.oldOnFocus = null; this.iframe.onfocus = null; this.oldOnBlur = null; this.iframe.onblur = null;
																																						 this.ifDoc.oncontextmenu = null; } function updateIframe() { if (this.textIsXHTML) this.setContent((this.shadowTB.value), true); else this.setContent(rsw_escapeHTML(this.shadowTB.value), true);
																																						 } function updateShadow() { var reg = new RegExp(String.fromCharCode(160), rsS75[325]); rsw_setShadowTB(this.shadowTB, this.getContentText().replace(reg, rsS75[27]));
																																						 } function getShadowText() { return this.shadowTB.value; } } function MozlyTB(iframeEl, editable) { this.skipAYTUpdates = false; this.iframe = iframeEl; this.editable = editable;
																																						 this.ifDoc; this.designMode; this.initialize = initialize; this.ifDocElement; this.setContent = setContent; this.getContent = getContent; this._onKeyPress = _onKeyPress;
																																						 this._onKeyUp = _onKeyUp; this._onKeyDown = _onKeyDown; this._onMouseDown = _onMouseDown; this._onMouseUp = _onMouseUp; this._onFocus = _onFocus; this.isFocused = false;
																																						 this._onBlur = _onBlur; this._onPaste = _onPaste; this._onClick = _onClick; this._onContextMenu = _onContextMenu; this.getSpanElements = getSpanElements; this.changeTo = changeTo;
																																						 this.getAbsY = getAbsY; this.getAbsX = getAbsX; this.isStatic = false; this.getContentText = getContentText; this.selectedErrorNode = selectedErrorNode; this.containsElement = containsElement;
																																						 this.focus = focus; this.multiline = false; this.enabled = true; this.maxlength = 0; this.shadowTB; this.updateIframe = updateIframe; this.updateShadow = updateShadow;
																																						 this.getShadowText = getShadowText; this.spellChecker; this.OnCorrection = OnCorrection; this.isWrappedInNOBR = false; this.oldOnBlur; this.oldOnFocus; this.isDirty = false;
																																						 this.recordCaretPos = recordCaretPos; this.resetCaretPos = resetCaretPos; this.setCaretPos = setCaretPos; this.selOffset; this.selOffsetEnd; this.CssSheetURL; this.getNumberOfErrors = getNumberOfErrors;
																																						 this.textIsXHTML; this.unhook = unhook; this.repObj = null; this.setDisabled = setDisabled; this.select = select; this.attachEvents = attachEvents; this.isVisible = isVisible;
																																						 this.recordCaretPosAppleWebKit = recordCaretPosAppleWebKit; this.resetCaretPosAppleWebKit = resetCaretPosAppleWebKit; this.container = container; function container() { if (this.iframe != null) { if (this.iframe.parentNode) return this.iframe.parentNode;
																																						 if (this.iframe.parentElement) return this.iframe.parentElement; if (this.iframe.parent) return this.iframe.parent; } return null; } this.walk = function (container, range, isStart, index) { if (container.nodeType != Node.TEXT_NODE && container.childNodes.length > 0) { var gobbledLength = 0;
																																						 for (var c = 0; c < container.childNodes.length; c++) { var newGobbledLength = this.walk(container.childNodes[c], range, isStart, index - gobbledLength); if (newGobbledLength == index - gobbledLength) return index;
																																						 else gobbledLength = newGobbledLength; } return gobbledLength; } else { var rangeText = container.nodeValue; if (rangeText != null && (rangeText.length > index || (rangeText.length == index && !isStart))) { if (isStart) range.setStart(container, index);
																																						 else range.setEnd(container, index); return index; } else { if (rangeText == null) return 0; else { if (container.nextSibling != null && rangeText.length == index) { if (isStart) { if (container.nextSibling.childNodes.length > 0) { for (var z = 0;
																																						 z < container.nextSibling.childNodes.length; z++) { if (container.nextSibling.childNodes[z].nodeType == Node.TEXT_NODE) { range.setStart(container.nextSibling.childNodes[z], 0);
																																						 break; } } } else range.setStart(container.nextSibling, 0); } } return rangeText.length; } } } }; function isVisible() { var rect = this.iframe.getBoundingClientRect();
																																						 return rect.left != 0 || rect.top != 0 || rect.width != 0 || rect.height != 0; } function select() { this.focus(); if (this.getContentText().length > 0) { var sel = this.iframe.contentWindow.getSelection();
																																						 var range = sel.getRangeAt(0); var contentElements = this.ifDoc.body.childNodes; range.setStartBefore(contentElements[0]); range.setEndAfter(contentElements[contentElements.length - 1]);
																																						 } } function getNumberOfErrors() { var errors = this.getSpanElements(); var numErrors = 0; for (var i = 0; i < errors.length; i++) { if (errors[i].className == rsS75[255]) { numErrors++;
																																						 } } return numErrors; } function recordCaretPos() { try { var sel = this.iframe.contentWindow.getSelection(); var range = sel.getRangeAt(0); var len = 0; var contentElements = this.ifDoc.body.childNodes;
																																						 this.selOffset = rsw_getAbsSel(range, len, contentElements)[0]; this.selOffsetEnd = rsw_getAbsSel(range, len, contentElements, true)[0]; } catch (e) { } } function recordCaretPosAppleWebKit() { var oWin = this.iframe.contentWindow;
																																						 var sel = oWin.getSelection(); this._previous_range = new Object(); this._previous_range.baseNode = sel.baseNode; this._previous_range.baseOffset = sel.baseOffset;
																																						 this._previous_range.extentNode = sel.extentNode; this._previous_range.extentOffset = sel.extentOffset; } function resetCaretPosAppleWebKit() { var oWin = this.iframe.contentWindow;
																																						 var sel = oWin.getSelection(); sel.setBaseAndExtent(this._previous_range.baseNode, this._previous_range.baseOffset, this._previous_range.extentNode, this._previous_range.extentOffset);
																																						 } function setCaretPos(characterIndex) { this.selOffset = characterIndex; this.selOffsetEnd = characterIndex; this.resetCaretPos(); } function resetCaretPos() { try { var sel = this.iframe.contentWindow.getSelection();
																																						 var range; if(sel.rangeCount==0) range = this.iframe.contentWindow.document.createRange(); else range = sel.getRangeAt(0); var contentElements = this.ifDoc.body.childNodes;
																																						 var absRange = new Array(); var absRangeEnd = new Array(); rsw_findEl(this.selOffset, contentElements, absRange); rsw_findEl(this.selOffsetEnd, contentElements, absRangeEnd);
																																						 if (absRange.length == 0) range.setStartAfter(contentElements[contentElements.length - 1]); if (absRangeEnd.length == 0) range.setEndAfter(contentElements[contentElements.length - 1]);
																																						 if (absRange.length > 0) { if (absRange[4]) range.setStartAfter(absRange[2]); else range.setStart(absRange[2], absRange[3]); } if (absRangeEnd.length > 0) { if (absRangeEnd[4]) range.setEndAfter(absRangeEnd[2]);
																																						 else range.setEnd(absRangeEnd[2], absRangeEnd[3]); } if (this.isFocused && (rsw_chrome || rsw_applewebkit || rsw_msie11)) { sel.removeAllRanges(); sel.addRange(range);
																																						 } } catch (e) { try { var range; if (sel.rangeCount == 0) range = this.iframe.contentWindow.document.createRange(); else range = sel.getRangeAt(0); var contentElements = this.ifDoc.body.childNodes;
																																						 var lastElPtr = contentElements.length - 1; while (!contentElements[lastElPtr].nodeValue && lastElPtr > 0) { lastElPtr--; } range.setEnd(contentElements[lastElPtr], contentElements[lastElPtr].nodeValue.length);
																																						 range.setStart(contentElements[lastElPtr], contentElements[lastElPtr].nodeValue.length); if (rsw_chrome || rsw_applewebkit) { sel.removeAllRanges(); sel.addRange(range);
																																						 } } catch (ex) { } } } function OnCorrection(e) { rsw_refreshActiveTextbox(); if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onCorrection(e); rsw_broadcastToListeners(rsS75[258], e);
																																						 if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[289], rsw_activeTextbox, e); } function focus() { this.iframe.contentWindow.focus(); this.isFocused = true;
																																						 } function containsElement(element) { return element.ownerDocument == this.ifDoc; } function selectedErrorNode() { rsw_refreshActiveTextbox(); try { var selection = rsw_activeTextbox.iframe.contentWindow.getSelection();
																																						 if (selection.anchorNode!= null && selection.anchorNode.parentNode.className == rsS75[255]) return selection.anchorNode.parentNode; else if (selection.anchorNode != null && selection.anchorNode.parentNode != null) { var parentEl = selection.anchorNode.parentNode;
																																						 for (var i = 0; parentEl.children.length > i; i++) { if (parentEl.children[i].className == rsS75[255]) { var r = this.ifDoc.createRange(); r.selectNode(parentEl.children[i]);
																																						 r.setEndAfter(selection.anchorNode); if (r.toString().charAt(r.toString().length - 2) == rsS75[151]) return parentEl.children[i]; } } } return null; } catch (e) { return null;
																																						 } } function getAbsX(element, event) { return element.getBoundingClientRect().left + rsw_activeTextbox.iframe.getBoundingClientRect().left + rsw_getScrollX(window);
																																						 } function getAbsY(element, event) { return element.getBoundingClientRect().top + rsw_activeTextbox.iframe.getBoundingClientRect().top + rsw_getScrollY(window); } function changeTo(error, replacement) { var repl = this.ifDoc.createTextNode(replacement);
																																						 if(error.parentNode!=null) error.parentNode.replaceChild(repl, error); } function getSpanElements() { return this.ifDoc.getElementsByTagName(rsS75[274]); } function getContentText() { var text;
																																						 if (!rsw_msie11 && this.ifDoc.body.innerText) { text = this.ifDoc.body.innerText; if (text.charAt(text.length - 1) == rsS75[19]) text = text.substring(0, text.length - 1);
																																						 return text; } var contentElements = this.ifDoc.body.childNodes; var contents = rsS75[0]; var innerT = rsS75[0]; for (var i = 0; i < contentElements.length; i++) { innerT = rsw_innerText(contentElements[i]);
																																						 if (contentElements[i].nodeName.toLowerCase() == rsS75[263]) contents += contentElements[i].value; else if ((contentElements[i].nodeName.toLowerCase() != rsS75[260] || i < contentElements.length - 1) ) { if (i == contentElements.length - 1 && innerT.charAt(innerT.length - 1) == rsS75[19]) innerT = innerT.substring(0, innerT.length - 1);
																																						 if (i == contentElements.length - 1 && innerT.charAt(innerT.length - 1) == rsS75[18]) innerT = innerT.substring(0, innerT.length - 1); contents += innerT; } } contents = contents.replace(/\r\n/g, "\n");
																																						 return contents; } function _onClick(event) { if (navigator.userAgent.toLowerCase().indexOf(rsS75[326]) == -1 && navigator.userAgent.toLowerCase().indexOf(rsS75[204]) > -1) { if (typeof event != rsS75[9]) { try { var embedhandler = event.target.attributes[rsS75[257]].nodeValue;
																																						 var suggestions = rsw_getSuggestionsArray(embedhandler); rsw_showMenu(suggestions, event.target, event); } catch (ex) { } } } } function _onKeyPress(event) { rsw_refreshActiveTextbox();
																																						 rsw_hideCM(); var evt = event; if (evt != null && evt.keyCode == 13 && !rsw_activeTextbox.multiline) { event.preventDefault(); event.cancelBubble = true; } if (evt != null && evt.keyCode == 9) { } rsw_activeTextbox.isDirty = true;
																																						 if (rsw_activeTextbox.maxlength > 0) { if ( evt.keyCode != 8 && evt.keyCode != 46 && (evt.keyCode < 33 || evt.keyCode > 40) && rsw_activeTextbox.getContentText().replace(/\r/g, rsS75[0]).replace(/\n/g, rsS75[0]).length >= rsw_activeTextbox.maxlength) { event.preventDefault();
																																						 event.cancelBubble = true; } } if (rsw_debug) rs_s3.getElementById(rsS75[132]).value += rsS75[327]+rsw_activeTextbox.ifDoc.body.innerHTML+rsS75[26]; if (!rsw_activeTextbox.multiline && rsw_activeTextbox.getContentText() == rsS75[134]) { var els = rsw_activeTextbox.ifDoc.getElementsByTagName(rsS75[328]);
																																						 for (var xi = 0; xi < els.length; xi++) { els[xi].parentNode.removeChild(els[xi]); } } if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onKeyPress(event);
																																						 rsw_broadcastToListeners(rsS75[290], event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[290], rsw_activeTextbox, event); } function _onKeyDown(event) { rsw_refreshActiveTextbox();
																																						 if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onKeyDown(event); rsw_broadcastToListeners(rsS75[267],event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[267], rsw_activeTextbox, event);
																																						 } function _onFocus(event) { rsw_refreshActiveTextbox(); rsw_hideCM(); rsw_activeTextbox.updateShadow(); if (typeof (rsw_activeTextbox.iframe.contentWindow) != rsS75[9]) { rsw_activeTextbox.ifDoc.body.className += rsS75[295];
																																						 } if ((rsw_chrome || rsw_applewebkit) && typeof (this.document) != rsS75[9]) rsw_updateActiveTextbox(this.document); else rsw_updateActiveTextbox(this); rsw_activeTextbox.isFocused = true;
																																						 if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onFocus(event); rsw_broadcastToListeners(rsS75[296], event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[296], rsw_activeTextbox, event);
																																						 } function _onBlur(event) { rsw_refreshActiveTextbox(); rsw_activeTextbox.isFocused = false; rsw_activeTextbox.rsw_key_downed_within_lim = false; rsw_activeTextbox.updateShadow();
																																						 rsw_activeTextbox.ifDoc.body.className = rsw_activeTextbox.ifDoc.body.className.replace(/(?:^|\s)rsw_focused(?!\S)/g, rsS75[0]); if (rsw_activeTextbox.shadowTB.onchange) { if (rsw_activeTextbox.shadowTB.defaultValue != rsw_activeTextbox.shadowTB.value) { var evt = rs_s3.createEvent(rsS75[329]);
																																						 evt.initUIEvent(rsS75[299], event.canBubble, event.cancelable, event.view, event.detail); rsw_activeTextbox.shadowTB.dispatchEvent(evt); } } if (rsw_activeTextbox.isAYT && rsw_spellCheckOnBlur) rsw_spellCheckTextBox(rsw_activeTextbox);
																																						 if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onBlur(event); rsw_broadcastToListeners(rsS75[301], event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[301], rsw_activeTextbox, event);
																																						 } function _onKeyUp(event) { rsw_refreshActiveTextbox(); if (event == null || !(event.keyCode >= 33 && event.keyCode <= 40)) { var errorNode = rsw_activeTextbox.selectedErrorNode();
																																						 if (errorNode) rsw_dehighlight(errorNode); } rsw_activeTextbox.updateShadow(); if (!rsw_activeTextbox.multiline && rsw_activeTextbox.ifDoc.body.innerHTML == rsS75[19]) rsw_activeTextbox.ifDoc.body.innerHTML = rsS75[0];
																																						 if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onKeyUp(event); rsw_broadcastToListeners(rsS75[292], event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[292], rsw_activeTextbox, event);
																																						 } function _onPaste(event) { rsw_refreshActiveTextbox(); if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onPaste(event); var errorNode = rsw_activeTextbox.selectedErrorNode();
																																						 if (errorNode) rsw_dehighlight(errorNode); setTimeout(rsS75[309] + rsS75[310], 300); rsw_broadcastToListeners(rsS75[311], event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[311], rsw_activeTextbox, event);
																																						 } function _onMouseDown(event) { rsw_refreshActiveTextbox(); rsw_hideCM(); if (rsw_activeTextbox == null) { if ((rsw_chrome || rsw_applewebkit) && typeof (this.document) != rsS75[9]) rsw_updateActiveTextbox(this.document);
																																						 else rsw_updateActiveTextbox(this); } if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onMouseDown(event); rsw_broadcastToListeners(rsS75[293], event);
																																						 if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[293], rsw_activeTextbox, event); rsw_activeTextbox.updateShadow(); } function _onMouseUp(event) { rsw_refreshActiveTextbox();
																																						 if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onMouseUp(event); if (rsw_msie11) { if (typeof event != rsS75[9]) { try { var omu = event.target.attributes[rsS75[257]];
																																						 if (omu != null) { var embedhandler = omu.nodeValue; var suggestions = rsw_getSuggestionsArray(embedhandler); rsw_showMenu(suggestions, event.target, event); } } catch (ex) { } } } rsw_broadcastToListeners(rsS75[294], event);
																																						 if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[294], rsw_activeTextbox, event); } function setContent(content, contentIsFromShadow) { var t = content + rsS75[19];
																																						 if (this.multiline) { t = t.replace(/(.*?)\n/g, rsS75[330]); t = t.replace(/<p><\/p>/g, rsS75[331]); } var newlineexp = new RegExp(rsS75[18]); while (newlineexp.test(t)) t = t.replace(newlineexp, rsS75[0]);
																																						 if (!this.multiline) { var pos = -1; var ppos = 0; var opener = -1; var closer = -1; while ((pos = t.indexOf(rsS75[27], pos + 1)) > -1) { opener = t.lastIndexOf(rsS75[144], pos);
																																						 closer = t.lastIndexOf(rsS75[145], pos); if ((opener == -1 && closer == -1) || opener == -1 || opener < closer) t = t.substring(0, pos) + rsS75[332] + t.substring(pos + 1);
																																						 ppos = pos; } if (t.length == 0 || t==rsS75[19]) t = rsS75[0]; else { this.isWrappedInNOBR = true; } } else { var pos = -1; var ppos = 0; var opener = -1; var closer = -1;
																																						 var flag = true; while ((pos = t.indexOf(rsS75[27], pos + 1)) > -1) { if (pos + 1 < t.length && ( t.charAt(pos + 1) == rsS75[27] || (pos > 4 && t.charAt(pos - 1) == rsS75[145] && t.substring(pos - 5, pos - 1) != rsS75[274]) || (pos + 3 < t.length && t.charAt(pos + 1) == rsS75[144] && t.charAt(pos + 2) == rsS75[333] && t.charAt(pos + 3) == rsS75[264]) || (pos >= 3 && t.charAt(pos - 1) == rsS75[145] && t.charAt(pos - 2) == rsS75[264]) ) ) { opener = t.lastIndexOf(rsS75[144], pos);
																																						 closer = t.lastIndexOf(rsS75[145], pos); if ((opener == -1 && closer == -1) || opener == -1 || opener < closer) { t = t.substring(0, pos) + String.fromCharCode(160) + t.substring(pos + 1);
																																						 } ppos = pos; } } } if(this.ifDoc!=null && this.ifDoc.body!=null) this.ifDoc.body.innerHTML = t; if(!contentIsFromShadow && this.ifDoc != null && this.ifDoc.body != null)this.updateShadow();
																																						 } function getContent() { return this.ifDoc.body.innerHTML; } function setDisabled(disabled) { if (disabled) this.iframe.contentDocument.designMode = rsS75[334]; else this.iframe.contentDocument.designMode = rsS75[335];
																																						 this.enabled = !disabled; if (typeof (rsw_ignoreDisabledBoxes) != rsS75[9] && rsw_ignoreDisabledBoxes && disabled) this.updateIframe(); if (this.multiline) { if (disabled) this.ifDoc.body.className = rsS75[321];
																																						 else this.ifDoc.body.className = rsS75[322]; } else { if (disabled) this.ifDoc.body.className = rsS75[323]; else this.ifDoc.body.className = rsS75[324]; } } function _onContextMenu(e) { rsw_refreshActiveTextbox();
																																						 if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onContextMenu(e); if (rsw_MenuOnRightClick && rsw_contextMenu!=null && rsw_contextMenu.isVisible) { e.cancelBubble = true;
																																						 e.preventDefault(); } rsw_broadcastToListeners(rsS75[308], e); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[308], rsw_activeTextbox, e);
																																						 } function initialize(attempts) { rsw_refreshActiveTextbox(); var ifID = this.iframe.id; this.shadowTBID = ifID.substring(0, ifID.length - 3); this.shadowTB = rs_s3.getElementById(this.shadowTBID);
																																						 if (rsw_id_waitingToInitialize == null) rsw_id_waitingToInitialize = ifID; if (!attempts) attempts = 0; if (!this.iframe.contentWindow.loaded && attempts < 100) { attempts++;
																																						 eval(rsS75[336] + this.shadowTBID + rsS75[313] + attempts + rsS75[337]); return; } rsw_id_waitingToInitialize = null; this.ifDoc = this.iframe.contentWindow.document;
																																						 this.iframe.contentWindow.document.documentElement.setAttribute(rsS75[316], false); this.ifDocElement = this.iframe.contentWindow.document.documentElement; function getDefaultFontSize(pa) { pa = pa || rs_s3.body;
																																						 var who = rs_s3.createElement(rsS75[39]); who.style.cssText = rsS75[317]; who.appendChild(rs_s3.createTextNode(rsS75[318])); pa.appendChild(who); var fs = [who.offsetWidth, who.offsetHeight];
																																						 pa.removeChild(who); return fs; } this.iframe.contentWindow.document.documentElement.style.fontSize = rsw_getStyleProperty(this.iframe.parentElement, rsS75[338]);
																																						 rsw_createLink(this.ifDoc, this.CssSheetURL); if (this.enabled) { if (this.editable) { eval(rsS75[339] + this.iframe.id + rsS75[340]); this.attachEvents(); } } rsw_setSettings(this);
																																						 if (rsw_ffMaxLengthChecker == null && this.maxlength > 0) rsw_ffMaxLengthChecker = setInterval(rsS75[341], 300); } this._onFrameFocus = function (e) { }; function attachEvents() { this.ifDoc.addEventListener(rsS75[293], this._onMouseDown, false);
																																						 this.ifDoc.addEventListener(rsS75[294], this._onMouseUp, false); this.ifDoc.addEventListener(rsS75[290], this._onKeyPress, false); this.ifDoc.addEventListener(rsS75[267], this._onKeyDown, false);
																																						 this.ifDoc.addEventListener(rsS75[292], this._onKeyUp, false); this.ifDoc.addEventListener(rsS75[308], this._onContextMenu, false); if (!rsw_chrome && !rsw_applewebkit && !rsw_msie11) { this.iframe.addEventListener(rsS75[296], this._onFrameFocus, false);
																																						 this.ifDoc.addEventListener(rsS75[296], this._onFocus, false); this.ifDoc.addEventListener(rsS75[301], this._onBlur, false); } else { this.iframe.addEventListener(rsS75[296], this._onFrameFocus, false);
																																						 this.iframe.contentWindow.addEventListener(rsS75[296], this._onFocus, false); this.iframe.contentWindow.addEventListener(rsS75[301], this._onBlur, false); } this.iframe.contentWindow.document.addEventListener(rsS75[311], this._onPaste, false);
																																						 this.ifDoc.addEventListener(rsS75[342], this._onClick, false); if (typeof (this.tbConfig) != rsS75[9] && this.tbConfig != null && this.tbConfig.keys != null) { for (var v = 0;
																																						 v < this.tbConfig.keys.length; v++) if (this.tbConfig.keys[v] == rsS75[343]) this.multiline = this.tbConfig.values[v]; if (!this.multiline && rsw_showHorizScrollBarsInFF) { this.iframe.scrolling = rsS75[344];
																																						 for (var v = 0; v < this.tbConfig.keys.length; v++) { if (this.tbConfig.keys[v] == rsS75[345]) { var hstr = parseInt(this.tbConfig.values[v].substring(1, this.tbConfig.values[v].length - 3), 10) + 22;
																																						 if (hstr < 50) this.tbConfig.values[v] = rsS75[151] + hstr + rsS75[346]; } } } } } function unhook() { this.ifDoc.removeEventListener(rsS75[293], this._onMouseDown, false);
																																						 this.ifDoc.removeEventListener(rsS75[294], this._onMouseUp, false); this.ifDoc.removeEventListener(rsS75[290], this._onKeyPress, false); this.ifDoc.removeEventListener(rsS75[267], this._onKeyDown, false);
																																						 this.ifDoc.removeEventListener(rsS75[292], this._onKeyUp, false); this.ifDoc.removeEventListener(rsS75[308], this._onContextMenu, false); this.ifDoc.removeEventListener(rsS75[296], this._onFocus, false);
																																						 this.ifDoc.removeEventListener(rsS75[301], this._onBlur, false); this.ifDoc.removeEventListener(rsS75[342], this._onClick, false); } function updateIframe() { if (this.textIsXHTML) this.setContent((this.shadowTB.value), true);
																																						 else this.setContent(rsw_escapeHTML(this.shadowTB.value), true); } function updateShadow() { var reg = new RegExp(String.fromCharCode(160), rsS75[325]); rsw_setShadowTB(this.shadowTB, this.getContentText().replace(reg, rsS75[27]));
																																						 } function getShadowText() { return this.shadowTB.value; } } function LabelTB(iframeEl, editable) { this.isLabel = true; this.skipAYTUpdates = true; this.iframe = iframeEl;
																																						 this.editable = editable; this.ifDoc; this.designMode; this.initialize = initialize; this.ifDocElement; this.setContent = setContent; this.getContent = getContent;
																																						 this._onKeyPress = _onKeyPress; this._onKeyUp = _onKeyUp; this._onKeyDown = _onKeyDown; this._onMouseDown = _onMouseDown; this._onMouseUp = _onMouseUp; this.noReconcile = true;
																																						 this._onFocus = _onFocus; this.isFocused = false; this._onBlur = _onBlur; this._onPaste = _onPaste; this._onClick = _onClick; this._onContextMenu = _onContextMenu;
																																						 this.getSpanElements = getSpanElements; this.changeTo = changeTo; this.getAbsY = getAbsY; this.getAbsX = getAbsX; this.isStatic = false; this.getContentText = getContentText;
																																						 this.selectedErrorNode = selectedErrorNode; this.containsElement = containsElement; this.focus = focus; this.multiline = false; this.enabled = true; this.maxlength = 0;
																																						 this.shadowTB; this.updateIframe = updateIframe; this.updateShadow = updateShadow; this.getShadowText = getShadowText; this.spellChecker; this.OnCorrection = OnCorrection;
																																						 this.isWrappedInNOBR = false; this.oldOnBlur; this.oldOnFocus; this.isDirty = false; this.recordCaretPos = recordCaretPos; this.resetCaretPos = resetCaretPos; this.setCaretPos = setCaretPos;
																																						 this.selOffset; this.selOffsetEnd; this.CssSheetURL; this.targetIsPlain = false; this.getNumberOfErrors = getNumberOfErrors; this.textIsXHTML; this.unhook = unhook;
																																						 this.repObj = null; this.setDisabled = setDisabled; this.select = select; this.attachEvents = attachEvents; this.isVisible = isVisible; this.recordCaretPosAppleWebKit = recordCaretPosAppleWebKit;
																																						 this.resetCaretPosAppleWebKit = resetCaretPosAppleWebKit; this.container = container; function container() { if (this.iframe != null) { if (this.iframe.parentNode) return this.iframe.parentNode;
																																						 if (this.iframe.parentElement) return this.iframe.parentElement; if (this.iframe.parent) return this.iframe.parent; } return null; } this.walk = function (container, range, isStart, index) { if (container.nodeType != Node.TEXT_NODE && container.childNodes.length > 0) { var gobbledLength = 0;
																																						 for (var c = 0; c < container.childNodes.length; c++) { var newGobbledLength = this.walk(container.childNodes[c], range, isStart, index - gobbledLength); if (newGobbledLength == index - gobbledLength) return index;
																																						 else gobbledLength = newGobbledLength; } return gobbledLength; } else { var rangeText = container.nodeValue; if (rangeText != null && (rangeText.length > index || (rangeText.length == index && !isStart))) { if (isStart) range.setStart(container, index);
																																						 else range.setEnd(container, index); return index; } else { if (rangeText == null) return 0; else { if (container.nextSibling != null && rangeText.length == index) { if (isStart) { if (container.nextSibling.childNodes.length > 0) { for (var z = 0;
																																						 z < container.nextSibling.childNodes.length; z++) { if (container.nextSibling.childNodes[z].nodeType == Node.TEXT_NODE) { range.setStart(container.nextSibling.childNodes[z], 0);
																																						 break; } } } else range.setStart(container.nextSibling, 0); } } return rangeText.length; } } } }; function isVisible() { var rect = this.iframe.getBoundingClientRect();
																																						 return rect.left != 0 || rect.top != 0 || rect.width != 0 || rect.height != 0; } function select() { this.focus(); if (this.getContentText().length > 0) { var sel = this.iframe.contentWindow.getSelection();
																																						 var range = sel.getRangeAt(0); var contentElements = this.ifDoc.body.childNodes; range.setStartBefore(contentElements[0]); range.setEndAfter(contentElements[contentElements.length - 1]);
																																						 } } function getNumberOfErrors() { var errors = this.getSpanElements(); var numErrors = 0; for (var i = 0; i < errors.length; i++) { if (errors[i].className == rsS75[255]) { numErrors++;
																																						 } } return numErrors; } function recordCaretPos() { } function recordCaretPosAppleWebKit() { } function resetCaretPosAppleWebKit() { } function setCaretPos(characterIndex) { } function resetCaretPos() { } function OnCorrection(e) { rsw_refreshActiveTextbox();
																																						 if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onCorrection(e); rsw_broadcastToListeners(rsS75[258], e); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[289], rsw_activeTextbox, e);
																																						 } function focus() { } function containsElement(element) { var p = element; while ((p = p.parentNode) != null) { if (p.id == this.ifDoc.id) return true; } return false;
																																						 } function selectedErrorNode() { rsw_refreshActiveTextbox(); try { var selection = rsw_activeTextbox.iframe.contentWindow.getSelection(); if (selection.anchorNode!= null && selection.anchorNode.parentNode.className == rsS75[255]) return selection.anchorNode.parentNode;
																																						 else if (selection.anchorNode != null && selection.anchorNode.parentNode != null) { var parentEl = selection.anchorNode.parentNode; for (var i = 0; parentEl.children.length > i;
																																						 i++) { if (parentEl.children[i].className == rsS75[255]) { var r = this.ifDoc.createRange(); r.selectNode(parentEl.children[i]); r.setEndAfter(selection.anchorNode);
																																						 if (r.toString().charAt(r.toString().length - 2) == rsS75[151]) return parentEl.children[i]; } } } return null; } catch (e) { return null; } } function getAbsX(element, event) { return element.getBoundingClientRect().left + rsw_getScrollX(window);
																																						 } function getAbsY(element, event) { return element.getBoundingClientRect().top + rsw_getScrollY(window); } function changeTo(error, replacement) { var repl = rs_s3.createTextNode(replacement);
																																						 error.parentNode.replaceChild(repl, error); } function getSpanElements() { return rs_s3.getElementById(this.ifDoc.id).getElementsByTagName(rsS75[274]); } function getContentText() { var text;
																																						 if (!rsw_msie11 && this.ifDoc.innerText) { text = this.ifDoc.innerText; if (text.charAt(text.length - 1) == rsS75[19]) text = text.substring(0, text.length - 1); return text;
																																						 } var contentElements = this.ifDoc.childNodes; var contents = rsS75[0]; var innerT = rsS75[0]; for (var i = 0; i < contentElements.length; i++) { innerT = rsw_innerText(contentElements[i]);
																																						 if (contentElements[i].nodeName.toLowerCase() == rsS75[263]) contents += contentElements[i].value; else if ((contentElements[i].nodeName.toLowerCase() != rsS75[260] || i < contentElements.length - 1) ) { if (i == contentElements.length - 1 && innerT.charAt(innerT.length - 1) == rsS75[19]) innerT = innerT.substring(0, innerT.length - 1);
																																						 if (i == contentElements.length - 1 && innerT.charAt(innerT.length - 1) == rsS75[18]) innerT = innerT.substring(0, innerT.length - 1); contents += innerT; } } contents = contents.replace(/\r\n/g, "\n");
																																						 return contents; } function _onClick(event) { if (navigator.userAgent.toLowerCase().indexOf(rsS75[326]) == -1 && navigator.userAgent.toLowerCase().indexOf(rsS75[204]) > -1) { if (typeof event != rsS75[9]) { try { var embedhandler = event.target.attributes[rsS75[257]].nodeValue;
																																						 var suggestions = rsw_getSuggestionsArray(embedhandler); rsw_showMenu(suggestions, event.target, event); } catch (ex) { } } } } function _onKeyPress(event) { } function _onKeyDown(event) { } function _onFocus(event) { } function _onBlur(event) { } function _onKeyUp(event) { } function _onPaste(event) { } function _onMouseDown(event) { rsw_refreshActiveTextbox();
																																						 rsw_hideCM(); if ((rsw_chrome || rsw_applewebkit) && typeof (this.document) != rsS75[9]) rsw_updateActiveTextbox(this.document); else rsw_updateActiveTextbox(this);
																																						 if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onMouseDown(event); rsw_broadcastToListeners(rsS75[293], event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[293], rsw_activeTextbox, event);
																																						 rsw_activeTextbox.updateShadow(); } function _onMouseUp(event) { rsw_refreshActiveTextbox(); if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onMouseUp(event);
																																						 if (rsw_msie11) { if (typeof event != rsS75[9]) { try { var omu = event.target.attributes[rsS75[257]]; if (omu != null) { var embedhandler = omu.nodeValue; var suggestions = rsw_getSuggestionsArray(embedhandler);
																																						 rsw_showMenu(suggestions, event.target, event); } } catch (ex) { } } } rsw_broadcastToListeners(rsS75[294], event); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[294], rsw_activeTextbox, event);
																																						 } function setContent(content, contentIsFromShadow) { var ignoredRegex = /<!--rs_ignored-->/; var arr; var ptr = 0; if (this.spellChecker.tbInterface.ignoredRegions.length > 0) { while (ignoredRegex.test(content)) content = content.replace(ignoredRegex, this.spellChecker.tbInterface.ignoredRegions[ptr++]);
																																						 } var t = content; if (this.multiline) { t = t.replace(/(.*?)\n/g, rsS75[330]); t = t.replace(/<p><\/p>/g, rsS75[331]); } var newlineexp = new RegExp(rsS75[18]); while (newlineexp.test(t)) t = t.replace(newlineexp, rsS75[0]);
																																						 if (!this.multiline) { var pos = -1; var ppos = 0; var opener = -1; var closer = -1; while ((pos = t.indexOf(rsS75[27], pos + 1)) > -1) { opener = t.lastIndexOf(rsS75[144], pos);
																																						 closer = t.lastIndexOf(rsS75[145], pos); if ((opener == -1 && closer == -1) || opener == -1 || opener < closer) t = t.substring(0, pos) + rsS75[332] + t.substring(pos + 1);
																																						 ppos = pos; } if (t.length == 0 || t==rsS75[19]) t = rsS75[0]; else { this.isWrappedInNOBR = true; } } else { var pos = -1; var ppos = 0; var opener = -1; var closer = -1;
																																						 var flag = true; while ((pos = t.indexOf(rsS75[27], pos + 1)) > -1) { if (pos + 1 < t.length && ( t.charAt(pos + 1) == rsS75[27] || (pos > 4 && t.charAt(pos - 1) == rsS75[145] && t.substring(pos - 5, pos - 1) != rsS75[274]) || (pos + 3 < t.length && t.charAt(pos + 1) == rsS75[144] && t.charAt(pos + 2) == rsS75[333] && t.charAt(pos + 3) == rsS75[264]) || (pos >= 3 && t.charAt(pos - 1) == rsS75[145] && t.charAt(pos - 2) == rsS75[264]) ) ) { opener = t.lastIndexOf(rsS75[144], pos);
																																						 closer = t.lastIndexOf(rsS75[145], pos); if ((opener == -1 && closer == -1) || opener == -1 || opener < closer) { t = t.substring(0, pos) + String.fromCharCode(160) + t.substring(pos + 1);
																																						 } ppos = pos; } } } if(this.ifDoc!=null && this.ifDoc!=null) this.ifDoc.innerHTML = t; if(!contentIsFromShadow && this.ifDoc != null)this.updateShadow(); } function getContent() { return this.ifDoc.innerHTML;
																																						 } function setDisabled(disabled) { } function _onContextMenu(e) { rsw_refreshActiveTextbox(); if (rsw_activeTextbox.repObj != null) rsw_activeTextbox.repObj._onContextMenu(e);
																																						 if (rsw_MenuOnRightClick && rsw_contextMenu.isVisible) { e.cancelBubble = true; e.preventDefault(); } rsw_broadcastToListeners(rsS75[308], e); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[308], rsw_activeTextbox, e);
																																						 } function initialize(attempts) { rsw_refreshActiveTextbox(); this.id = this.iframe.id; this.shadowTBID = this.iframe.id+rsS75[30]; if ( (this.shadowTB=document.getElementById(this.shadowTBID)) == null) { var input = rs_s3.createElement(rsS75[263]);
																																						 input.type = rsS75[80]; input.style.display = rsS75[37]; input.setAttribute(rsS75[347], this.shadowTBID); input.setAttribute(rsS75[348], rsS75[185]); input.id = this.shadowTBID;
																																						 rs_s3.documentElement.appendChild(input); this.shadowTB = input; } this.ifDoc = this.iframe; this.ifDocElement = this.ifDoc; this.attachEvents(); } this.addEvent = function (evnt, elem, func) { if (elem.addEventListener) elem.addEventListener(evnt, func, false);
																																						 else if (elem.attachEvent) { elem.attachEvent(rsS75[335] + evnt, func); } else { elem[evnt] = func; } }; this.removeEvent = function (evnt, elem, func) { if (elem.removeEventListener) elem.removeEventListener(evnt, func, false);
																																						 else if (elem.detachEvent) { elem.detachEvent(rsS75[335] + evnt, func); } else { elem[evnt] = null; } }; function attachEvents() { this.addEvent(rsS75[293], this.ifDoc, this._onMouseDown);
																																						 this.addEvent(rsS75[294], this.ifDoc, this._onMouseUp); this.addEvent(rsS75[308], this.ifDoc, this._onContextMenu); } function unhook() { this.removeEvent(rsS75[293], this.ifDoc, this._onMouseDown);
																																						 this.removeEvent(rsS75[294], this.ifDoc, this._onMouseUp); this.removeEvent(rsS75[308], this.ifDoc, this._onContextMenu); } function updateIframe() { if (this.textIsXHTML) this.setContent((this.shadowTB.value), true);
																																						 else this.setContent(rsw_escapeHTML(this.shadowTB.value), true); } function updateShadow() { var reg = new RegExp(String.fromCharCode(160), rsS75[325]); rsw_setShadowTB(this.shadowTB, this.getContentText().replace(reg, rsS75[27]));
																																						 } function getShadowText() { return this.shadowTB.value; } } function OldIETB(iframe) { this.iframe = iframe; this.ifDoc; this.initialize = initialize; this.ifDocElement;
																																						 this.setContent = setContent; this.getContent = getContent; this._onKeyPress = _onKeyPress; this._onPaste = _onPaste; this._onMouseDown = _onMouseDown; this._onContextMenu = _onContextMenu;
																																						 this._onDoubleClick = _onDoubleClick; this.getSpanElements = getSpanElements; this.changeTo = changeTo; this.getAbsY = getAbsY; this.getAbsX = getAbsX; this.isStatic = true;
																																						 this.createEditBox = createEditBox; this.getContentText = getContentText; this.containsElement = containsElement; this.getShadowText = getShadowText; this.updateShadow = updateShadow;
																																						 this.multiline = true; this.spellChecker; this.OnCorrection = OnCorrection; this.getNumberOfErrors = getNumberOfErrors; this.getContentCleanHTML = getContentCleanHTML;
																																						 this.targetIsPlain = true; this.unhook = unhook; this.resetCaretPos = unimplementedFunction; this.recordCaretPos = unimplementedFunction; this.container = container;
																																						 function container() { if (this.iframe != null) { if (this.iframe.parentNode) return this.iframe.parentNode; if (this.iframe.parentElement) return this.iframe.parentElement;
																																						 if (this.iframe.parent) return this.iframe.parent; } return null; } function unimplementedFunction() { } function containsElement(element) { var p; if (element == this.iframe) return true;
																																						 while ((p = element.parentNode)) { if (p == this.iframe) return true; element = p; } return false; } function getAbsX(element, ev) { return element.getBoundingClientRect().left + rsw_getScrollX(window);
																																						 } function getAbsY(element, ev) { return element.getBoundingClientRect().top + rsw_getScrollY(window); } function changeTo(error, replacement) { var repl = rs_s3.createTextNode(replacement);
																																						 error.parentNode.replaceChild(repl, error); } function findElementsCell(element) { var p = element; while ((p = p.parentNode) != null && p.tagName.toLowerCase() != rsS75[349]) { } return p;
																																						 } function createEditBox(error) { var width = error.offsetWidth; var repl = rs_s3.createElement(rsS75[263]); repl.setAttribute(rsS75[350], rsw_innerHTMLToText(error.innerHTML));
																																						 repl.setAttribute(rsS75[33], rsS75[351]); repl.onkeypress = rsw_inlineTB_onkeypress; repl.onblur = rsw_inlineTB_onBlur; repl.style.width = width * 1.8; error.parentNode.replaceChild(repl, error);
																																						 var scrollTop = this.iframe.scrollTop; repl.focus(); this.iframe.scrollTop = scrollTop; } function getSpanElements() { return this.iframe.getElementsByTagName(rsS75[274]);
																																						 } function _onKeyPress() { rsw_hideCM(); rsw_broadcastToListeners(rsS75[290]); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[290], null, null);
																																						 } function _onMouseDown() { rsw_hideCM(); rsw_broadcastToListeners(rsS75[293]); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[293], null, null);
																																						 } function _onDoubleClick() { rsw_getTBSHoldingElement(this).spellChecker.OnTextBoxDoubleClicked(); rsw_broadcastToListeners(rsS75[352]); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[352], null, null);
																																						 } function setContent(content) { if (this.targetIsPlain) { var pos = -1; var ppos = 0; var t = rsS75[0]; while ((pos = content.indexOf(rsS75[19], pos + 1)) > -1) { if (pos > ppos + 2) { if (content.substring(pos - 1, pos) == rsS75[18]) t += rsS75[353] + content.substring(ppos, pos - 1) + rsS75[354];
																																						 else t += rsS75[353] + content.substring(ppos, pos) + rsS75[354]; } else t += content.substring(ppos, pos) + rsS75[355]; ppos = pos; } if (ppos < content.length - 1) t += rsS75[353] + content.substring(ppos, content.length) + rsS75[354];
																																						 var flag = false; if (!this.multiline) { var pos = -1; var ppos = 0; var opener = -1; var closer = -1; while ((pos = t.indexOf(rsS75[27], pos + 1)) > -1) { opener = t.lastIndexOf(rsS75[144], pos);
																																						 closer = t.lastIndexOf(rsS75[145], pos); if ((opener == -1 && closer == -1) || opener == -1 || opener < closer) t = t.substring(0, pos) + rsS75[332] + t.substring(pos + 1);
																																						 ppos = pos; } t = rsS75[306] + t + rsS75[307]; } else { var pos = -1; var ppos = 0; var opener = -1; var closer = -1; var flag = true; while ((pos = t.indexOf(rsS75[27], pos + 1)) > -1) { if (pos + 1 < t.length && t.charAt(pos + 1) == rsS75[27]) { opener = t.lastIndexOf(rsS75[144], pos);
																																						 closer = t.lastIndexOf(rsS75[145], pos); if ((opener == -1 && closer == -1) || opener == -1 || opener < closer) { if (flag) t = t.substring(0, pos) + rsS75[332] + t.substring(pos + 1);
																																						 else t = t.substring(0, pos) + rsS75[27] + t.substring(pos + 1); flag = !flag; } ppos = pos; } } } var tabexp = new RegExp(rsS75[356]); while (tabexp.test(t)) t = t.replace(tabexp, rsS75[357]);
																																						 this.iframe.innerHTML = t; } else this.iframe.innerHTML = content; } function getContent() { return this.iframe.innerHTML; } function getContentCleanHTML() { rsw_processedNodes = new Array();
																																						 var nodes = this.iframe.childNodes; var out = rsS75[0]; for (var i = 0; i < nodes.length; i++) { out += rsw_cleanHTML(nodes[i]); } return out; } function isNodeKnownToBeProcessed(node) { if (rsw_processedNodes == null || typeof (node.sourceIndex) == rsS75[9]) return false;
																																						 for (var i = 0; i < rsw_processedNodes.length; i++) { if (node.sourceIndex == rsw_processedNodes[i]) return true; } return false; } var rsw_processedNodes = null;
																																						 function rsw_cleanHTML(node) { var t = rsS75[0]; var styleInAttr = false; var quote = '"'; if (isNodeKnownToBeProcessed(node)) return t; if (rsw_processedNodes != null && typeof (node.sourceIndex) != rsS75[9]) rsw_processedNodes.push(node.sourceIndex);
																																						 if (node.nodeName.toLowerCase() != rsS75[358] && node.nodeName.toLowerCase() != rsS75[263] && !(node.nodeName.toLowerCase() == rsS75[274] && node.className == rsS75[255]) ) { t += rsS75[144] + node.nodeName + rsS75[27];
																																						 for (var att = 0; node.attributes!=null && att < node.attributes.length; att++) { if (node.attributes[att].nodeValue) { styleInAttr = styleInAttr || node.attributes[att].nodeName.toLowerCase() == rsS75[34];
																																						 if (node.attributes[att].nodeValue.indexOf(rsS75[151])>-1) quote = '"'; else quote = rsS75[151]; t += node.attributes[att].nodeName + rsS75[359] + quote + node.attributes[att].nodeValue + quote + rsS75[27];
																																						 } } if (typeof (node.style) != rsS75[9] && !styleInAttr) { t += "style=\""; t += node.style.cssText; t += "\" "; } if (node.childNodes.length == 0 && !node.nodeValue && node.nodeName!=rsS75[42] && node.nodeName!=rsS75[360]) t += rsS75[333];
																																						 t += rsS75[145]; } if (node.childNodes.length == 0) { if (node.nodeValue) { t += node.nodeValue.replace(rsS75[144], rsS75[168]).replace(rsS75[145], rsS75[169]); } if (node.value) { t += node.value.replace(rsS75[144], rsS75[168]).replace(rsS75[145], rsS75[169]);
																																						 } } else { for (var i = 0; i < node.childNodes.length; i++) t += rsw_cleanHTML(node.childNodes[i]); } if (node.nodeName.toLowerCase() != rsS75[358] && node.nodeName.toLowerCase() != rsS75[263] && !(node.nodeName.toLowerCase() == rsS75[274] && node.className == rsS75[255]) && !(node.childNodes.length == 0 && !node.nodeValue && node.nodeName != rsS75[42] && node.nodeName != rsS75[360]) ) t += rsS75[146] + node.nodeName + rsS75[145];
																																						 return t; } function getContentText() { var contentElements = this.iframe.childNodes; var contents = rsS75[0]; for (var i = 0; i < contentElements.length; i++) { var nV = null;
																																						 try { nV = contentElements[i].nodeValue; } catch (er) { } if (nV) contents += nV.replace(/\n/g, rsS75[0]).replace(/\r/g, rsS75[0]); else if (contentElements[i].nodeName.toLowerCase() == rsS75[260] && i < contentElements.length - 1) contents += rsS75[134];
																																						 else if (contentElements[i].nodeName.toLowerCase() == rsS75[263]) contents += contentElements[i].value; else contents += rsw_innerText(contentElements[i], i == contentElements.length - 1, contentElements[contentElements.length - 1].nodeName.toLowerCase() == rsS75[260]);
																																						 } var t = contents; while (rsw_newlineexp.test(t)) t = t.replace(rsw_newlineexp, rsS75[0]); contents = t; return contents; } function _onContextMenu() { rsw_broadcastToListeners(rsS75[308]);
																																						 if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[308], null, null); return false; } function _onPaste() { rsw_broadcastToListeners(rsS75[311]);
																																						 if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[311], null, null); } function getShadowText() { return this.shadowTB.value; } function updateShadow() { if (this.targetIsPlain) this.spellChecker.tbInterface.setText(this.getContentText());
																																						 else this.spellChecker.tbInterface.setText(this.getContentCleanHTML()); } function initialize() { this.iframe.onmousedown = this._onMouseDown; this.iframe.ondblclick = this._onDoubleClick;
																																						 var ifID = this.iframe.id; this.shadowTBID = ifID.substring(0, ifID.length - 2); this.shadowTB = rs_s3.getElementById(this.shadowTBID); } function unhook() { this.iframe.onmousedown = null;
																																						 this.iframe.ondblclick = null; } function OnCorrection(e) { if (this.getNumberOfErrors() == 0) { if (this.spellChecker.enterEditModeWhenNoErrors) { this.spellChecker.OnSpellButtonClicked(true);
																																						 } } rsw_broadcastToListeners(rsS75[258]); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[289], null, e); } function getNumberOfErrors() { var errors = this.getSpanElements();
																																						 var numErrors = 0; for (var i = 0; i < errors.length; i++) { if (errors[i].className == rsS75[255]) { numErrors++; } } return numErrors; } } function rsw_getElementHeight(Elem, isoverlay) { var op5 = (navigator.userAgent.indexOf(rsS75[361]) != -1) || (navigator.userAgent.indexOf(rsS75[362]) != -1);
																																						 if (rs_s3.layers) { var elem = rsw_getObjNN4(document, Elem); return elem.clip.height; } else { if (rs_s3.getElementById) { var elem = rs_s3.getElementById(Elem);
																																						 } else if (rs_s3.all) { var elem = rs_s3.all[Elem]; } if (op5) { xPos = elem.style.pixelHeight; } else { xPos = elem.offsetHeight; } if (!isoverlay && elem.style != null && elem.style.height != null && elem.style.height.indexOf(rsS75[45]) > -1) return elem.style.height;
																																						 return xPos; } } function rsw_getObjNN4(obj, name) { var x = obj.layers; var foundLayer; for (var i = 0; i < x.length; i++) { if (x[i].id == name) foundLayer = x[i];
																																						 else if (x[i].layers.length) var tmp = rsw_getObjNN4(x[i], name); if (tmp) foundLayer = tmp; } return foundLayer; } function rsw_getElementWidth(Elem, isoverlay) { var op5 = (navigator.userAgent.indexOf(rsS75[361]) != -1) || (navigator.userAgent.indexOf(rsS75[362]) != -1);
																																						 if (rs_s3.layers) { var elem = rsw_getObjNN4(document, Elem); return elem.clip.width; } else { if (rs_s3.getElementById) { var elem = rs_s3.getElementById(Elem); } else if (rs_s3.all) { var elem = rs_s3.all[Elem];
																																						 } if (op5) { xPos = elem.style.pixelWidth; } else { xPos = elem.offsetWidth; } if (!isoverlay && elem.style != null && elem.style.width != null && elem.style.width.indexOf(rsS75[45]) > -1) return elem.style.width;
																																						 return xPos; } } function rsw_findPosX(obj) { var curleft = 0; var isFirefox = navigator.userAgent.toLowerCase().indexOf(rsS75[204]) > -1; if (typeof (obj.offsetParent) != rsS75[9] && obj.offsetParent) { while (obj.offsetParent) { var tBW = rsw_getStyleProperty(obj, rsS75[54]);
																																						 curleft += obj.offsetLeft + (isFirefox ? parseInt(tBW.substring(0, tBW.length - 2)) : 0); ; if (obj.parentNode.scrollLeft) curleft -= obj.parentNode.scrollLeft; obj = obj.offsetParent;
																																						 } var tBW = rsw_getStyleProperty(obj, rsS75[54]); curleft += obj.offsetLeft + (isFirefox ? parseInt(tBW.substring(0, tBW.length - 2)) : 0); ; } else if (obj.x) curleft += obj.x;
																																						 return curleft; } function rsw_findPosY(obj) { var curtop = 0; var isFirefox = navigator.userAgent.toLowerCase().indexOf(rsS75[204]) > -1; if (typeof (obj.offsetParent) != rsS75[9] && obj.offsetParent) { while (obj.offsetParent) { var tBW = rsw_getStyleProperty(obj, rsS75[58]);
																																						 curtop += obj.offsetTop + (isFirefox ? parseInt(tBW.substring(0, tBW.length - 2)) : 0); if (obj.parentNode.scrollTop) curtop -= obj.parentNode.scrollTop; obj = obj.offsetParent;
																																						 } var tBW = rsw_getStyleProperty(obj, rsS75[58]); curtop += obj.offsetTop + (isFirefox ? parseInt(tBW.substring(0, tBW.length - 2)) : 0); } else if (obj.y) curtop += obj.y;
																																						 return curtop; } function rsw_decodeSuggestionItem(item) { return unescape(item).replace(rsS75[363], rsS75[151]).replace(rsS75[364], "\""); } function RS_ContextMenu(errorElement, suggestions, textBox) { this.suggestions = suggestions;
																																						 this.CMItems = new Array(); this.x = 0; this.y = 0; this.CMelement = null; this.textBox = textBox; this.show = show; this.setCMContent = setCMContent; this.hide = hide;
																																						 this.setVisible = setVisible; this.moveCMElement = moveCMElement; this.getContentHtml = getContentHtml; this.addItems = addItems; this.addItems(); function addItems() { var newSuggs = new Array();
																																						 var errorText = errorElement.textContent ? errorElement.textContent : errorElement.innerText; var errorLength = errorText.length; for (var i = 0; i < this.suggestions.length;
																																						 i++) { if (this.suggestions[i].indexOf(rsS75[365]) == 0 || this.textBox.maxlength == 0 || typeof(this.textBox.maxlength)==rsS75[9] || rsw_decodeSuggestionItem(this.suggestions[i]).length - errorLength + this.textBox.getContentText().length <= this.textBox.maxlength) newSuggs[newSuggs.length] = this.suggestions[i];
																																						 } this.suggestions = newSuggs; var isCapitalCorrection = false; if (this.suggestions.length == 1) { isCapitalCorrection = this.suggestions[0].toLowerCase() == errorText && this.suggestions[0].charAt(0).toUpperCase() == this.suggestions[0].charAt(0);
																																						 } var isDuplicateWordErr = false; for (i = 0; i < this.suggestions.length; i++) { if (this.suggestions[i].indexOf(rsS75[365]) < 0) { this.CMItems[this.CMItems.length] = new RS_ContextMenuItem(errorElement, rsw_decodeSuggestionItem(this.suggestions[i]), escape(this.suggestions[i]), rsS75[299] );
																																						 if (this.textBox.spellChecker.showChangeAllItem) { this.CMItems[this.CMItems.length] = new RS_ContextMenuItem(errorElement, unescape(this.textBox.spellChecker.changeAllText), escape(this.suggestions[i]), rsS75[366], rsS75[367] );
																																						 } } else { this.CMItems[this.CMItems.length] = new RS_ContextMenuItem(errorElement, this.textBox.spellChecker.removeDuplicateText, escape(this.suggestions[i].substring(1)), rsS75[368] );
																																						 isDuplicateWordErr = true; } } if (this.suggestions.length == 0) { this.CMItems[0] = new RS_ContextMenuItem(errorElement, this.textBox.spellChecker.noSuggestionsText, rsS75[170], rsS75[369] );
																																						 i = 1; } else { i = this.CMItems.length; } if (!isDuplicateWordErr || this.textBox.isStatic) { this.CMItems[i] = new RS_ContextMenuItem(errorElement, rsS75[370], rsS75[370], rsS75[370] );
																																						 } if (this.textBox.isStatic) { if (this.textBox.spellChecker.showEditMenuItem) { this.CMItems[i + 1] = new RS_ContextMenuItem(errorElement, this.textBox.spellChecker.editText, rsS75[174], rsS75[371] );
																																						 i++; } } if (!isDuplicateWordErr) { if (this.textBox.spellChecker.showIgnoreAllMenuItem) { this.CMItems[i + 1] = new RS_ContextMenuItem(errorElement, this.textBox.spellChecker.ignoreAllText, rsS75[171], rsS75[372] );
																																						 } else i--; var thisUD = null; var overrideUD = false; if(typeof (rsw_getParameterValue) == rsS75[150]){ thisUD = rsw_getParameterValue(this.textBox.shadowTB, rsS75[233]);
																																						 overrideUD = thisUD == null || thisUD == rsS75[0]; } var ShowAddItemAlways = false; if (typeof (rsw_getParameterValue) == rsS75[150]) ShowAddItemAlways = rsw_getParameterValue(this.textBox.shadowTB, rsS75[373]);
																																						 if (((this.textBox.spellChecker.showAddMenuItem && !overrideUD) || ShowAddItemAlways) && !isCapitalCorrection) { this.CMItems[i + 2] = new RS_ContextMenuItem(errorElement, this.textBox.spellChecker.addText, rsS75[173], rsS75[374] );
																																						 } } if (rs_s3.getElementById(rsS75[375]) == null) rsw_create_menu_div(); this.CMelement = rs_s3.getElementById(rsS75[375]); this.CMIFelement = rs_s3.getElementById(rsS75[376]);
																																						 this.setVisible(false); } function show() { if (typeof(this.textBox.enabled)!=rsS75[9] && !this.textBox.enabled) return; this.setVisible(true); this.moveCMElement();
																																						 this.setCMContent(this.getContentHtml()); if (typeof (rsw_useIFrameMenuBacker) == rsS75[9] || rsw_useIFrameMenuBacker) { if (navigator.userAgent.toLowerCase().indexOf(rsS75[377]) > -1 && !rsw_isMac) { this.CMIFelement.style.left = this.x + rsS75[47];
																																						 this.CMIFelement.style.top = this.y + rsS75[47]; this.CMIFelement.style.height = (rsw_getElementHeight(rsS75[375]) - 4) + rsS75[47]; this.CMIFelement.style.width = (rsw_getElementWidth(rsS75[375]) - 4) + rsS75[47];
																																						 } } } function hide() { this.setVisible(false); this.CMelement.innerHtml = rsS75[0]; } function setCMContent(s) { this.CMelement.innerHTML = s; } function setVisible(visible) { this.CMelement.style.visibility = visible ? rsS75[95] : rsS75[127];
																																						 if (typeof (rsw_useIFrameMenuBacker) == rsS75[9] || rsw_useIFrameMenuBacker) { if (navigator.userAgent.toLowerCase().indexOf(rsS75[377]) > -1 && !rsw_isMac) { this.CMIFelement.style.visibility = visible ? rsS75[95] : rsS75[127];
																																						 this.CMIFelement.style.display = visible ? rsS75[201] : rsS75[37]; } } this.isVisible = visible; } function moveCMElement() { this.CMelement.style.left = this.x + rsS75[47];
																																						 this.CMelement.style.top = this.y + rsS75[47]; if (typeof (rsw_useIFrameMenuBacker) == rsS75[9] || rsw_useIFrameMenuBacker) { if (navigator.userAgent.toLowerCase().indexOf(rsS75[377]) > -1 && !rsw_isMac) { this.CMIFelement.style.left = this.x + rsS75[47];
																																						 this.CMIFelement.style.top = this.y + rsS75[47]; } } } function getContentHtml() { var s = "<table class=\"" + RS_ContextMenuTable_Class + "\" width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
																																						 var hasSubMenu = false; for (var i = 0; i < this.CMItems.length; i++) { hasSubMenu = i < this.CMItems.length - 1 && this.CMItems[i + 1].type == rsS75[367]; s += rsS75[378] + (hasSubMenu ? rsS75[379] : rsS75[380]) + rsS75[145];
																																						 s += this.CMItems[i].getContentHtml(); s += rsS75[381]; if (hasSubMenu) { i++; s += rsS75[382] + this.CMItems[i].getContentHtml() + rsS75[381]; } s += rsS75[383];
																																						 } s += rsS75[384]; return s; } } function RS_ContextMenuItem(e, unescapedValue, escapedValue, action, type) { this.unescapedValue = unescapedValue; this.escapedValue = escapedValue;
																																						 this.action = action; this.getContentHtml = getContentHtml; this.type = type ? type : rsS75[385]; function getContentHtml() { var s; if (this.unescapedValue != rsS75[370] && this.action != rsS75[369]) { s = "<span class=\"" + (this.type == rsS75[385] ? RS_ContextMenuItem_Class : RS_ContextMenuItem_AllSubItem_Class ) + "\" " + " onclick=\"RS_CMItemClicked( '" + this.escapedValue + rsS75[386] + this.action + "') ;\"" + " onMouseOut=\" RS_CMItemHighlight(this, 'out');\" " + " onMouseOver=\"RS_CMItemHighlight(this, 'over'); \" " + rsS75[145] + this.unescapedValue + rsS75[387];
																																						 } else if (this.action == rsS75[369]) { s = "<span class=\"" + RS_ContextMenuItem_Disabled_Class + "\" " + rsS75[145] + this.unescapedValue + rsS75[387]; } else { s = "<hr class=\"" + RS_CMItemSeparator_Class + "\"/>";
																																						 } return s; } } function RS_CMItemHighlight(e, type) { var p = e.className.indexOf(rsS75[388]); if (type == rsS75[389]) { if (p > 0) e.className = e.className.substring(0, p);
																																						 } else { if (p == -1) e.className = e.className + rsS75[388]; } } function RS_CMItemClicked(replacement, action) { rsw_refreshActiveTextbox(); var yScroll = null;
																																						 if (typeof(rsw_activeTextbox.iframe.contentWindow) != rsS75[9]) yScroll = rsw_getScrollY(rsw_activeTextbox.iframe.contentWindow); replacement = unescape(replacement).replace(rsS75[363], rsS75[151]).replace(rsS75[364], "\"");
																																						 if (action == rsS75[372]) { if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[390], rsw_activeTextbox, rsw_lastRightClickedError); rsw_ignoreAll(rsw_lastRightClickedError);
																																						 if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[391], rsw_activeTextbox, rsw_lastRightClickedError); } else if (action == rsS75[371]) { if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[392], rsw_activeTextbox, rsw_lastRightClickedError);
																																						 rsw_edit(rsw_lastRightClickedError); } else if (action == rsS75[374]) { if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[393], rsw_activeTextbox, rsw_lastRightClickedError);
																																						 rsw_add(rsw_lastRightClickedError); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[394], rsw_activeTextbox, rsw_lastRightClickedError); } else if (action == rsS75[368]) { if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[395], rsw_activeTextbox, rsw_lastRightClickedError);
																																						 rsw_changeTo(rsw_lastRightClickedError, rsS75[0]); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[396], rsw_activeTextbox, rsw_lastRightClickedError);
																																						 } else if (action == rsS75[366]) { if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[397], rsw_activeTextbox, rsw_lastRightClickedError, replacement);
																																						 rsw_changeAllTo(rsw_lastRightClickedError, replacement); if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[398], rsw_activeTextbox, rsw_lastRightClickedError, replacement);
																																						 } else { if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[399], rsw_activeTextbox, rsw_lastRightClickedError, replacement); rsw_changeTo(rsw_lastRightClickedError, replacement);
																																						 if (typeof (rsw_broadcastEvent) == rsS75[150]) rsw_broadcastEvent(rsS75[400], rsw_activeTextbox, rsw_lastRightClickedError, replacement); } rsw_hideCM(); if (rsw_activeTextbox.focus && !rsw_activeTextbox.isFocused) { rsw_activeTextbox.focus();
																																						 if (rsw_activeTextbox.resetCaretPos) rsw_activeTextbox.resetCaretPos(); if(yScroll!=null) rsw_setScrollY(rsw_activeTextbox.iframe.contentWindow, yScroll); } } function rsw_hideCM() { if (rsw_contextMenu) { rsw_contextMenu.hide();
																																						 } } function rsw_create_menu_div() { var divElement = rs_s3.createElement(rsS75[31]); divElement.id = rsS75[375]; divElement.setAttribute(rsS75[401], rsS75[402]);
																																						 try { divElement.oncontextmenu = function () { try { event.cancelBubble = true; event.preventDefault(); } catch (e) { } return false; }; } catch (e) { } rs_s3.getElementsByTagName(rsS75[403])[0].appendChild(divElement);
																																						 if (navigator.userAgent.toLowerCase().indexOf(rsS75[377]) > -1) { var ifElement = rs_s3.createElement(rsS75[42]); ifElement.id = rsS75[376]; ifElement.setAttribute(rsS75[404], rsS75[405]);
																																						 ifElement.setAttribute(rsS75[406], rsS75[407]); ifElement.setAttribute(rsS75[408], rsS75[409]); ifElement.setAttribute(rsS75[34], rsS75[410]); rs_s3.getElementsByTagName(rsS75[403])[0].appendChild(ifElement);
																																						 } } var rsw_ayt_initializing = false; function RapidSpell_Web_AsYouType() { this.triggeredLast = false; this.checkerCurrentlyInitializing = 0; this.onTextBoxesInit = onTextBoxesInit;
																																						 this.checkNext = checkNext; this.onFinish = onFinish; this.onPause = onPause; this.checkAsYouTypeOnPageLoad = true; this.stop = stop; this.start = start; this.stopped = false;
																																						 function start() { this.stopped = false; } function stop() { this.stopped = true; } function onPause() { rsw_refreshActiveTextbox(); if (!this.stopped && typeof (rsw_activeTextbox) != rsS75[9] && rsw_activeTextbox != null && rsw_activeTextbox.spellChecker != null) { rsw_activeTextbox.updateShadow();
																																						 rsw_activeTextbox.spellChecker.OnSpellButtonClicked(); } } function onTextBoxesInit() { rsw_ayt_initializing = true; rsw_ayt_check = true; this.checkNext(); } function checkNext() { if (rsw_haltProcesses || !rsw_ayt_enabled || this.stopped) { rsw_ayt_initializing = false;
																																						 this.checkerCurrentlyInitializing++; this.onFinish(); } if (rsw_scs.length > this.checkerCurrentlyInitializing) { var tbs = rsw_scs[this.checkerCurrentlyInitializing].getTBS();
																																						 if (tbs != null && (tbs.isStatic || !tbs.isVisible() || tbs.skipAYTUpdates)) { this.checkerCurrentlyInitializing++; this.onFinish(); } else if (tbs!=null){ if (this.checkAsYouTypeOnPageLoad) rsw_scs[this.checkerCurrentlyInitializing].OnSpellButtonClicked();
																																						 this.checkerCurrentlyInitializing++; if (!this.checkAsYouTypeOnPageLoad) this.onFinish(); tbs.isAYT = true; } } return this.checkerCurrentlyInitializing < rsw_scs.length;
																																						 } function onFinish() { if (!rsw_ayt_initializing) { } if (rsw_ayt_initializing && this.triggeredLast) { rsw_ayt_initializing = false; if (typeof (_notifySpellCheckListeners) != rsS75[9]) _notifySpellCheckListeners(rsS75[411]);
																																						 } if (rsw_ayt_initializing) { this.triggeredLast = !this.checkNext(); } } } String.prototype.rsw_reverse = function () { return this.split(rsS75[0]).reverse().join(rsS75[0]);
																																						 }; function RSW_Diff(p, v, a) { this.position = p; this.vector = v; this.addedText = a; } function RSW_VisibleCharSeq(str) { this.str = str; this.length = str.length;
																																						 this.allVisible = false; this.reverse = reverse; this.isReversed = false; function reverse() { this.str = this.str.rsw_reverse(); this.isReversed = !this.isReversed;
																																						 } this.insertAtVisible = insertAtVisible; function insertAtVisible(addition, pos) { return this.visibleSubstring(0, pos) + addition + this.visibleSubstring(pos, this.visibleLength());
																																						 } this.toString = toString; function toString() { return this.str; } this.visibleSubstring = visibleSubstring; function visibleSubstring(start, end) { var visiChars = 0;
																																						 var inTag = false; var inEnt = false; var sub = rsS75[0]; var includeTags = true; var tagOpen = this.isReversed ? rsS75[145] : rsS75[144]; var tagClose = this.isReversed ? rsS75[144] : rsS75[145];
																																						 var entOpen = this.isReversed ? rsS75[412] : rsS75[265]; var entClose = this.isReversed ? rsS75[265] : rsS75[412]; for (var i = 0; i < this.str.length; i++) { if (this.str.charAt(i) == tagOpen && !inTag && !this.allVisible) inTag = true;
																																						 if (this.str.charAt(i) == entOpen && !inTag && !inEnt && !this.allVisible) { var closer = this.str.indexOf(entClose, i); if (closer > -1 && closer - i < 9) { inEnt = true;
																																						 if (visiChars >= start && visiChars < end) { var entity = this.str.substring(i, closer); if (entity == rsS75[413]) sub += rsS75[265]; if (entity == rsS75[414]) sub += rsS75[27];
																																						 if (entity == rsS75[415]) sub += rsS75[144]; if (entity == rsS75[416]) sub += rsS75[145]; } visiChars++; } } if (includeTags && inTag && visiChars >= start && visiChars <= end) sub += this.str.charAt(i);
																																						 if (!inTag && !inEnt) { if ( visiChars >= start && visiChars < end) sub += this.str.charAt(i); visiChars++; } if (this.str.charAt(i) == tagClose && inTag) inTag = false;
																																						 if (this.str.charAt(i) == entClose && inEnt) inEnt = false; } return sub; } this.lastDiffI = 0; this.lastDiffPos = 0; this.visibleCharAt = visibleCharAt; function visibleCharAt(pos) { var startI = 0;
																																						 var visiChars = 0; var tagOpen = this.isReversed ? rsS75[145] : rsS75[144]; var tagClose = this.isReversed ? rsS75[144] : rsS75[145]; var entOpen = this.isReversed ? rsS75[412] : rsS75[265];
																																						 var entClose = this.isReversed ? rsS75[265] : rsS75[412]; var entityChar; if (pos > this.lastDiffPos) { startI = this.lastDiffI; visiChars = this.lastDiffPos; } var inTag = false;
																																						 var inEnt = false; for (var i = startI; i < this.str.length; i++) { if (this.str.charAt(i) == tagOpen && !inTag && !this.allVisible) inTag = true; if (this.str.charAt(i) == entOpen && !inTag && !inEnt && !this.allVisible) { var closer = this.str.indexOf(entClose, i);
																																						 if (closer > -1 && closer - i < 9) { inEnt = true; var entity = this.str.substring(i, closer); if (entity == rsS75[413]) entityChar = rsS75[265]; if (entity == rsS75[414]) entityChar = rsS75[27];
																																						 if (entity == rsS75[415]) entityChar = rsS75[144]; if (entity == rsS75[416]) entityChar = rsS75[145]; if (visiChars == pos) return entityChar; visiChars++; } } if (!inTag && !inEnt) { if (visiChars == pos) { this.lastDiffI = i;
																																						 this.lastDiffPos = visiChars; if (this.str.charAt(i) == String.fromCharCode(160)) return rsS75[27]; if (this.str.charAt(i) == rsS75[18]) return rsS75[19]; return this.str.charAt(i);
																																						 } visiChars++; } if (this.str.charAt(i) == tagClose && inTag) inTag = false; if (this.str.charAt(i) == entClose && inEnt) inEnt = false; } } this.visibleLength = visibleLength;
																																						 function visibleLength() { var visiChars = 0; var inTag = false; var inEnt = false; for (var i = 0; i < this.str.length; i++) { if (this.str.charAt(i) == rsS75[144] && !inTag && !this.allVisible) inTag = true;
																																						 if (this.str.charAt(i) == rsS75[265] && !inTag && !inEnt && !this.allVisible) { var closer = this.str.indexOf(rsS75[412], i); if (closer > -1 && closer - i < 9) { inEnt = true;
																																						 visiChars++; } } if (!inTag && !inEnt) { visiChars++; } if (this.str.charAt(i) == rsS75[145] && inTag) inTag = false; if (this.str.charAt(i) == rsS75[412] && inEnt) inEnt = false;
																																						 } return visiChars; } } function RSW_diff(beforeS, afterS) { var cs = -1; var ce = -1; var scanLength = 0; var before = new RSW_VisibleCharSeq(beforeS); var after = new RSW_VisibleCharSeq(afterS);
																																						 after.allVisible = true; var beforeVisiLen = before.visibleLength(); var afterVisiLen = after.visibleLength(); for (var i = 0; i < beforeVisiLen && cs < 0; i++) if (!(i >= afterVisiLen || after.visibleCharAt(i) == before.visibleCharAt(i))) cs = i;
																																						 if (cs == -1 && afterVisiLen != beforeVisiLen) cs = beforeVisiLen; after.reverse(); before.reverse(); for (var i = 0; i < afterVisiLen && ce < 0; i++) if (i >= (beforeVisiLen - cs) || !(i >= beforeVisiLen || after.visibleCharAt(i) == before.visibleCharAt(i))) ce = (afterVisiLen - i);
																																						 if (ce == -1) ce = afterVisiLen; var vector = ce - cs; if (vector == 0) vector = afterVisiLen - beforeVisiLen; after.reverse(); return new RSW_Diff(cs, vector, after.visibleSubstring(cs, ce));
																																						 } function RSW_EditableElementFinder() { this.findPlainTargetElement = findPlainTargetElement; this.findRichTargetElements = findRichTargetElements; this.obtainElementWithInnerHTML = obtainElementWithInnerHTML;
																																						 this.findEditableElements = findEditableElements; this.elementIsEditable = elementIsEditable; this.getEditableContentDocument = getEditableContentDocument; function findPlainTargetElement(elementID) { var rsw_elected = rs_s3.getElementById(elementID);
																																						 if (rsw_elected != null && rsw_elected.tagName && (rsw_elected.tagName.toUpperCase() == rsS75[417] || rsw_elected.tagName.toUpperCase() == rsS75[418])) { return rsw_elected;
																																						 } else return null; } function findRichTargetElements(debugTextBox) { var editables = new Array(); this.findEditableElements(document, editables, window, rsS75[0], debugTextBox);
																																						 return editables; } function obtainElementWithInnerHTML(editable) { if (typeof (editable.innerHTML) != rsS75[9]) return editable; else if (typeof (editable.documentElement) != rsS75[9]) return editable.documentElement;
																																						 return null; } function findEditableElements(node, editables, parent, debugInset, debugTextBox) { var children = node.childNodes; var editableElement; if ((editableElement = this.elementIsEditable(node)) != null || (editableElement = this.getEditableContentDocument(node, debugTextBox)) != null ) { editables[editables.length] = editableElement;
																																						 } for (var i = 0; i < children.length; i++) { this.findEditableElements(children[i], editables, node, debugInset + rsS75[27], debugTextBox); } } function elementIsEditable(element) { if ( ( typeof (element.getAttribute) != rsS75[9] && ( element.getAttribute(rsS75[298]) == rsS75[185] || element.getAttribute(rsS75[419]) == rsS75[335] ) ) || ( (element.contentEditable && element.contentEditable == true) || (element.designMode && element.designMode.toLowerCase() == rsS75[335]) ) ) return [element, element];
																																						 else return null; } function getEditableContentDocument(element, debugTextBox) { if (element.tagName && element.tagName == rsS75[42]) { var kids = new Array(); try{ if (element.contentWindow && element.contentWindow.document) { this.findEditableElements(element.contentWindow.document, kids, element, rsS75[420], debugTextBox);
																																						 if (kids.length > 0) { var editable = kids[0][0]; if (typeof (editable.body) != rsS75[9]) editable = editable.body; return [editable, element]; } } } catch (ex) { } } return null;
																																						 } } var rsw_require_init = true; function rsw_ASPNETAJAX_OnInitializeRequest(sender, eventArgs) { rsw_cancelCall = true; rsw_haltProcesses = true; rsw_require_init = true;
																																						 for (var i = 0; i < rsw_scs.length; i++) { if (rsw_scs[i] != null) { if (rsw_scs[i].state == rsS75[202] || (rsw_scs[i].state == rsS75[184] && typeof (rapidSpell) == rsS75[421])) { if (rsw_scs[i].rsw_tbs != null) { rsw_scs[i].rsw_tbs.updateShadow();
																																						 rsw_scs[i].rsw_tbs.iframe.style.display = rsS75[37]; if (rsw_scs[i].rsw_tbs.iframe.parentNode != null) rsw_scs[i].rsw_tbs.iframe.parentNode.removeChild(rsw_scs[i].rsw_tbs.iframe);
																																						 rsw_scs[i].rsw_tbs.shadowTB.style.display = rsw_scs[i].rsw_tbs.shadowTBDisplay; } } rsw_scs[i].rsw_tbs = null; } } rsw_tbs = new Array(); rsw_scs = new Array(); rsw_config = new Array();
																																						 rsw_ObjsToInit = new Array(); } function rsw_ASPNETAJAX_OnEndRequest(sender, eventArgs) { rsw_haltProcesses = false; try { rsw_createLink(document, rsw_rs_menu_styleURL);
																																						 rsw_createLink(document, rsw_rs_styleURL); } catch (ex) { } if (typeof (attachInitHandler) != rsS75[9]) attachInitHandler(); if (rsw_require_init) { if (typeof (rsw_autoCallRSWInit) == rsS75[9] || rsw_autoCallRSWInit) setTimeout(rsS75[422], 200);
																																						 } rsw_require_init = false; } if (typeof (Sys) != rsS75[9] && typeof (Sys.Application) != rsS75[9]) Sys.Application.notifyScriptLoaded(); 
