/*--Keyoti js--*/
/*<![CDATA[*/
//Version 1.56 (RapidSpell Web assembly version 3.5 onwards)
//Copyright Keyoti Inc. 2005-2013
//This code is not to be modified, copied or used without a license in any form.

var rsS11=new Array("Microsoft Internet Explorer",
"MSIE ([0-9]{1,}[\.0-9]{0,})",
"rsTCInt",
"undefined",
"IgnoreXML",
"True",
"popUpCheckSpelling",
"('rsTCInt",
"')",
"g",
"&#34;",
"&",
"&amp;",
"Error: element ",
" does not exist, check TextComponentName.",
"",
"Sorry, a textbox with ID=",
" couldn't be found - please check the TextComponentID or TextComponentName property.",
"_IF",
"&lt;",
"&gt;",
"TEXTAREA",
"INPUT",
" EDITABLE",
"\r\n",
" ",
"contentEditable",
"true",
"designMode",
"on",
"IFRAME",
" --has contentWindow.document",
"*");																																				var rs_s2=window;var rs_s3=document; var rsw_launcher_script_loaded = true; var rsw_suppressWarnings=false; function rsw_getInternetExplorerVersion() { var rv = -1;
																																						 if (navigator.appName == rsS11[0]) { var ua = navigator.userAgent; var re = new RegExp(rsS11[1]); if (re.exec(ua) != null) rv = parseFloat(RegExp.$1); } return rv;
																																						 } function SpellCheckLauncher(clientID){ this.clientID = clientID; this.OnSpellButtonClicked = OnSpellButtonClicked; this.config; this.hasRunFieldID; this.getParameterValue = getParameterValue;
																																						 this.setParameterValue = setParameterValue; this.tbInterface = null; function getParameterValue(param){ for(var pp=0; pp<this.config.keys.length; pp++){ if(this.config.keys[pp]==param) return this.config.values[pp];
																																						 } } function setParameterValue(param, value){ for(var pp=0; pp<this.config.keys.length; pp++){ if(this.config.keys[pp]==param) this.config.values[pp] = value; } } function OnSpellButtonClicked(){ this.tbInterface = eval(rsS11[2]+this.clientID);
																																						 if(this.tbInterface!=null && typeof(this.tbInterface.findContainer)!=rsS11[3]){ this.textBoxID = this.tbInterface.findContainer().id; if (!this.tbInterface.targetIsPlain){ this.setParameterValue(rsS11[4], rsS11[5]);
																																						 } } eval(rsS11[6]+this.clientID+rsS11[7]+this.clientID+rsS11[8]); } } function RS_writeDoc(toWindow, isSafari){ toWindow.document.open(); toWindow.document.write(spellBoot);
																																						 toWindow.document.close(); if(isSafari) toWindow.document.forms[0].submit(); } function escQuotes(text){ var rx = new RegExp("\"", rsS11[9]); return text.replace(rx,rsS11[10]);
																																						 } function escEntities(text){ var rx = new RegExp(rsS11[11], rsS11[9]); return text.replace(rx,rsS11[12]); } function RSStandardInterface(tbElementName){ this.tbName = tbElementName;
																																						 this.getText = getText; this.setText = setText; function getText(){ if(!document.getElementById(this.tbName) && !rsw_suppressWarnings) { alert(rsS11[13]+this.tbName+rsS11[14]);
																																						 return rsS11[15]; } else return rs_s3.getElementById(this.tbName).value; } function setText(text) { if(rs_s3.getElementById(this.tbName)) rs_s3.getElementById(this.tbName).value = text;
																																						 if(typeof(rsw_tbs)!=rsS11[3]){ for(var i=0; i<rsw_tbs.length; i++){ if(rsw_tbs[i].shadowTB.id==this.tbName){ if(rsw_tbs[i].updateIframe){rsw_tbs[i].updateIframe();
																																						rsw_tbs[i].focus();} } } } } } function RSAutomaticInterface(tbElementName){ this.tbName = tbElementName;this.getText = getText;this.setText = setText; this.identifyTarget = identifyTarget;
																																						 this.target=null; this.targetContainer = null; this.searchedForTarget = false; this.targetIsPlain = true; this.showNoFindError = showNoFindError; this.finder = null;
																																						 this.findContainer = findContainer; function findContainer(){ this.identifyTarget(); return this.targetContainer; } function showNoFindError(){ alert(rsS11[16]+this.tbName+rsS11[17]);
																																						 } function identifyTarget(){ if(!this.searchedForTarget){ this.searchedForTarget = true; if(this.finder == null) this.finder = new RSW_EditableElementFinder(); var plain = this.finder.findPlainTargetElement(this.tbName);
																																						 var richs = this.finder.findRichTargetElements(); if(plain==null && (richs==null || richs.length==0) && !rsw_suppressWarnings) showNoFindError(); else{ if(richs==null || richs.length==0){ this.targetIsPlain = true;
																																						 this.target = plain; this.targetContainer = plain; } else { if(plain==null && richs.length==1){ this.targetIsPlain = false; this.target = this.finder.obtainElementWithInnerHTML(richs[0][0]);
																																						 this.targetContainer = richs[0][1]; } else { for (var rp = 0; rp < richs.length; rp ++){ if(typeof(richs[rp][1].id)!=rsS11[3] && richs[rp][1].id.indexOf(this.tbName)>-1){ if(plain!=null && richs[rp][1].id == plain.id+rsS11[18]){ this.targetIsPlain = true;
																																						 this.target = plain; this.targetContainer = plain; break; } else { this.targetIsPlain = false; this.target = this.finder.obtainElementWithInnerHTML(richs[rp][0]);
																																						 this.targetContainer = richs[rp][1]; break; } } } if(this.target==null){ this.target = plain; this.targetIsPlain = true; this.targetContainer = plain; } } } } } } function getText(){ this.identifyTarget();
																																						 if( this.targetIsPlain ) return this.target.value; else return this.target.innerHTML; } function setText(text){ this.identifyTarget(); if (this.targetIsPlain) { var ver = rsw_getInternetExplorerVersion();
																																						 if (ver > 0 && ver < 9) text = text.replace(/</g, rsS11[19]).replace(/>/g, rsS11[20]); this.target.value = text; } else this.target.innerHTML = text; if(typeof(rsw_tbs)!=rsS11[3]){ for(var i=0;
																																						 i<rsw_tbs.length; i++){ if(rsw_tbs[i].shadowTB.id==this.tbName){ if(rsw_tbs[i].updateIframe){rsw_tbs[i].updateIframe();rsw_tbs[i].focus();} } } } } } function RSW_EditableElementFinder(){ this.findPlainTargetElement = findPlainTargetElement;
																																						 this.findRichTargetElements = findRichTargetElements; this.obtainElementWithInnerHTML = obtainElementWithInnerHTML; this.findEditableElements = findEditableElements;
																																						 this.elementIsEditable = elementIsEditable; this.getEditableContentDocument = getEditableContentDocument; function findPlainTargetElement(elementID){ var rsw_elected = rs_s3.getElementById(elementID);
																																						 if(rsw_elected!=null && rsw_elected.tagName && (rsw_elected.tagName.toUpperCase()==rsS11[21] || rsw_elected.tagName.toUpperCase()==rsS11[22])){ return rsw_elected;
																																						 } else return null; } function findRichTargetElements(debugTextBox){ var editables = new Array(); this.findEditableElements(document, editables, window,rsS11[15], debugTextBox);
																																						 return editables; } function obtainElementWithInnerHTML(editable){ if(typeof(editable.innerHTML)!=rsS11[3]) return editable; else if(typeof(editable.documentElement)!=rsS11[3]) return editable.documentElement;
																																						 return null; } function findEditableElements(node, editables, parent, debugInset, debugTextBox){ var children = node.childNodes; var editableElement; if(debugTextBox) debugTextBox.value += debugInset + node.tagName;
																																						 if( (editableElement=this.elementIsEditable(node))!=null || (editableElement=this.getEditableContentDocument(node, debugTextBox))!=null ){ if(debugTextBox) debugTextBox.value += rsS11[23];
																																						 editables[editables.length] = editableElement; } if(debugTextBox) debugTextBox.value += rsS11[24]; for (var i = 0; i < children.length; i++) { this.findEditableElements(children[i], editables, node, debugInset+rsS11[25], debugTextBox);
																																						 } } function elementIsEditable(element){ if ( ( typeof(element.getAttribute)!=rsS11[3] && ( element.getAttribute(rsS11[26])==rsS11[27] || element.getAttribute(rsS11[28])==rsS11[29] ) ) || ( (element.contentEditable && element.contentEditable==true) || (element.designMode && element.designMode.toLowerCase()==rsS11[29]) ) ) return [element, element];
																																						 else return null; } function getEditableContentDocument(element, debugTextBox){ if(element.tagName && element.tagName==rsS11[30]){ var kids = new Array(); if(element.contentWindow && element.contentWindow.document){ if(debugTextBox) debugTextBox.value += rsS11[31];
																																						 this.findEditableElements(element.contentWindow.document, kids, element, rsS11[32], debugTextBox); if(kids.length>0){ var editable = kids[0][0]; if(typeof(editable.body)!=rsS11[3]) editable = editable.body;
																																						 return [editable, element]; } } } return null; } } if( typeof(Sys)!=rsS11[3] && typeof(Sys.Application)!=rsS11[3]) Sys.Application.notifyScriptLoaded(); 
