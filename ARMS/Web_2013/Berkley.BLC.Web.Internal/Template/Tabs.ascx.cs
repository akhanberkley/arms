using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Berkley.BLC.Entities;
using QCI.Web;

namespace Berkley.BLC.Web.Internal.Template
{
    public partial class Tabs : System.Web.UI.UserControl
    {
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.PreRender += new EventHandler(this.Page_PreRender);
        }
        #endregion

        protected string _appPath; // used during HTML rendering
        protected string _templatePath; // used during HTML rendering
        protected Tab[] _tabs; // used during HTML rendering

        private void Page_Load(object sender, System.EventArgs e)
        {
            _appPath = ARMSUtility.AppPath;
            _templatePath = _appPath + "/template";
        }


        private void Page_PreRender(object sender, System.EventArgs e)
        {
            // get the tabs based on the current user's role
            UserIdentity user = UserIdentity.Current;
            if (user == null)
            {
                _tabs = new Tab[0];
            }
            else
            {
                _tabs = GetTabsForUser(user);
            }

            // determine the active tab based on the page being viewed
            string url = GetAppRelativePageUrl().ToLower();
            for (int i = _tabs.Length - 1; i >= 0; i--)
            {
                Tab tab = _tabs[i];
                if (tab.ContainsUrl(url))
                {
                    tab.IsActive = true;
                    break;
                }
            }
        }


        private Tab[] GetTabsForUser(UserIdentity user)
        {
            Tab[] tabs = new Tab[10];
            int tabCount = 0;

            PermissionSet permissions = user.Permissions;
            if (permissions.CanViewCompanySummary)
            {
                tabs[tabCount++] = new Tab("Summary", "companysummary.aspx");
            }
            if (permissions.CanViewMySurveysPage)
            {
                tabs[tabCount++] = new Tab("My Surveys", "mysurveys.aspx");
            }
            if (permissions.CanSearchSurveys)
            {
                tabs[tabCount++] = new Tab("Search", "search.aspx");
            }
            if (permissions.CanCreateSurveys)
            {
                tabs[tabCount++] = new Tab("Survey Request", "createsurveysearch.aspx");
            }
            if (permissions.CanViewServicePlansPage)
            {
                tabs[tabCount++] = new Tab("Service Plans", "serviceplans.aspx");
            }
            if (permissions.CanWorkAgencyVisits)
            {
                tabs[tabCount++] = new Tab("Agency Visit", "AgencyVisits.aspx");
            }
            if (permissions.CanViewLinks)
            {
                tabs[tabCount++] = new Tab("Links", "links.aspx");
            }
            if (permissions.CanAdminClients)
            {
                tabs[tabCount++] = new Tab("Manage Clients", "AdminClients.aspx");
            }
            if (permissions.CanViewAdminPage)
            {
                tabs[tabCount++] = new Tab("Administration", "administration.aspx");
            }
       
   
            // trim array to length
            Tab[] trimmed = new Tab[tabCount];
            Array.Copy(tabs, 0, trimmed, 0, tabCount);

            return trimmed;
        }


        private string GetAppRelativePageUrl()
        {
            // get the length of the path to this web application (will be '/' if on wwwroot)
            string appPath = Request.ApplicationPath;
            int length = appPath.Length;
            if (appPath != "/") length += 1;

            // remove the app path from the site-relative page url
            string relPath = Request.Path.Remove(0, length);

            // return the relative path
            return relPath;
        }
    }

    public class Tab
    {
        private string _text;
        private string _url;
        private string[] _childUrls;
        private bool _isActive;

        public Tab(string text, string url) : this(text, url, null) { }

        public Tab(string text, string url, params string[] childUrls)
        {
            _text = text.ToUpper();
            _url = url;
            _childUrls = childUrls;
            _isActive = false;
        }


        public string Text
        {
            get { return _text; }
        }

        public string Url
        {
            get { return _url; }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }


        public bool ContainsUrl(string url)
        {
            url = url.ToLower();
            if (_url == url) return true;

            if (_childUrls != null)
            {
                for (int i = _childUrls.Length - 1; i >= 0; i--)
                {
                    string childUrl = _childUrls[i];
                    int len = childUrl.Length;
                    if (len > 1 && childUrl[len - 1] == '*') // wildcard match
                    {
                        if (url.StartsWith(childUrl.Substring(0, len - 1))) return true;
                    }
                    else // exact match
                    {
                        if (childUrl == url) return true;
                    }
                }
            }

            return false;
        }
    }
}
