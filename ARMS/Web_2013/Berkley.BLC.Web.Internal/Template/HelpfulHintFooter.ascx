<%@ Control Language="C#" AutoEventWireup="false" CodeFile="HelpfulHintFooter.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Template.HelpfulHintFooter" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<!-- BEGIN HELPFUL HINT FOOTER -->
</td>
            <td width="15">&nbsp;</td>
            <td valign="top" class="columnHint">
                <cc:Box id="boxHint" runat="server" title="Helpful Hint:" Visible="false" width="210">
                    <div><%=_hintText %></div>
                </cc:Box>
            </td>
        </tr>
        </table>
	</td>
</tr>
<!-- END PAGE FOOTER -->
