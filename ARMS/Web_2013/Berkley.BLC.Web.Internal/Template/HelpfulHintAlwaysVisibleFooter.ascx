<%@ Control Language="C#" AutoEventWireup="false" CodeFile="HelpfulHintAlwaysVisibleFooter.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Template.HelpfulHintAlwaysVisibleFooter" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!-- BEGIN HELPFUL HINT FOOTER -->

<%if (_pageHint.AlwaysVisible) { %>
<ajaxToolkit:AlwaysVisibleControlExtender ID="avcHint" runat="server" 
        TargetControlID="divHelpfulHint"
        VerticalSide="Top"
        VerticalOffset="100"
        HorizontalSide="Right"
        HorizontalOffset="25"
        ScrollEffectDuration=".1">
</ajaxToolkit:AlwaysVisibleControlExtender>

<div id="divHelpfulHint" runat="server">
    <cc:Box id="boxHint2" runat="server" title="Helpful Hint:" width="210">
        <div><%=_hintText %></div>
    </cc:Box>
</div>
<% } else { %>
</td>
            <td width="15">&nbsp;</td>
            <td valign="top" class="columnHint">
                <cc:Box id="boxHint" runat="server" title="Helpful Hint:" Visible="false" width="210">
                    <div><%=_hintText %></div>
                </cc:Box>
            </td>
        </tr>
        </table>
	</td>
</tr>
<% } %>