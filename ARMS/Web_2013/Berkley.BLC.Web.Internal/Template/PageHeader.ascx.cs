using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Berkley.BLC.Entities;
using QCI.Web;
using Berkley.BLC.Core;

namespace Berkley.BLC.Web.Internal.Template
{
    public partial class PageHeader : System.Web.UI.UserControl
    {
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        protected string _appPath; // used during HTML rendering
        protected string _env; // used during HTML rendering
        protected UserIdentity _user; // used during HTML rendering

        private void Page_Load(object sender, System.EventArgs e)
        {
            _appPath = ARMSUtility.AppPath;

            // get the current user (if any)
            _user = UserIdentity.Current;

            // set the environment to display on the page
            _env = (CoreSettings.ConfigurationType != ConfigurationType.Production) ? string.Format("* {0} Environment *", CoreSettings.ConfigurationType.ToString()) : string.Empty;

            //injecting the last known size in the code behind because we can't wait for the DOM to generate to do it on the client side
            divFontResizer.Visible = (Request.Browser.Browser == "IE" || Request.Browser.Browser == "Chrome");
            
            HttpCookie fontCookie = Request.Cookies["ARMSWebsiteFontSize"];
            string fontSize = (fontCookie != null) ? fontCookie.Value : "11";

            HtmlGenericControl style = new HtmlGenericControl();
            style.TagName = "style";
            style.Attributes.Add("type", "text/css");

            string html = ".pageBody TABLE{font-size:" + fontSize + "px;} " +
                "INPUT{font-size:" + fontSize + "px;} " +
                ".cbo{font-size:" + fontSize + "px;} " +
                ".txt{font-size:" + fontSize + "px;} " +
                ".lbl{font-size:" + fontSize + "px;} " +
                ".chk{font-size:" + fontSize + "px;} " +
                ".logoCompany{font-size:" + fontSize + "px;} " +
                ".instructions{font-size:" + fontSize + "px;} " +
                ".logout{font-size:" + fontSize + "px;} " +
                ".loggedUser{font-size:" + fontSize + "px;} " +
                ".tabControl TD{font-size:" + fontSize + "px;} " +
                ".pageBody .subheading{font-size:" + fontSize + "px;}";

            style.InnerHtml = html;
            divDefaultFontSize.Controls.Add(style);
        }

        public bool TabsVisible
        {
            get { return ucTabs.Visible; }
            set { ucTabs.Visible = value; }
        }
    }
}
