using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Berkley.BLC.Entities;
using QCI.Web;

namespace Berkley.BLC.Web.Internal.Template
{
    public partial class HelpfulHintFooter : System.Web.UI.UserControl
    {
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        protected string _hintText = string.Empty;

        private void Page_Load(object sender, System.EventArgs e)
        {
            //set the helpful hint
            string path = this.Page.AppRelativeVirtualPath.TrimStart('~');
            WebPageHelpfulHint pageHint = WebPageHelpfulHint.GetOne("WebPagePath = ? && CompanyID = ?", path, UserIdentity.Current.CompanyID);

            if (pageHint != null && pageHint.HelpfulHintText.Length > 0)
            {
                boxHint.Visible = true;
                _hintText = pageHint.HelpfulHintText;
            }
        }
    }
}
