<%@ Control Language="C#" AutoEventWireup="false" CodeFile="Tabs.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Template.Tabs" %>
<%@ Import namespace="Berkley.BLC.Web.Internal.Template" %>
<!-- BEGIN TAB CONTROL -->
<table class="tabControl" cellspacing="0" cellpadding="0" border="0">
	<tr>
<%
for( int i = 0; i < _tabs.Length; i++ )
{
	Tab tab = _tabs[i];
	if( tab.IsActive ) { %>
		<td><img src="<%= _templatePath %>/images/tableft_on.gif" width="11" height="18"></td>
		<td class="tabTextOn" nowrap><a href="<%= _appPath + "/" + tab.Url %>"><%= tab.Text %></a></td>
		<td><img src="<%= _templatePath %>/images/tabright_on.gif" width="11" height="18"></td>
<% } else { %>
		<td><img src="<%= _templatePath %>/images/tableft_off.gif" width="11" height="18"></td>
		<td class="tabTextOff" nowrap><a href="<%= _appPath + "/" + tab.Url %>"><%= tab.Text %></a></td>
		<td><img src="<%= _templatePath %>/images/tabright_off.gif" width="11" height="18"></td>
<% }
}
%>
		<td width="100%">&nbsp;</td>
	</tr>
</table>
<!-- END TAB CONTROL -->
