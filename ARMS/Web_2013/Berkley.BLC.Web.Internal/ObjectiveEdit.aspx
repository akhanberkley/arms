<%@ Page Language="C#" EnableEventValidation="false" ValidateRequest="false" AutoEventWireup="false" CodeFile="ObjectiveEdit.aspx.cs" Inherits="ObjectiveEditAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintHeader" Src="~/Template/HelpfulHintHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="HintFooter" Src="~/Template/HelpfulHintFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Date" Src="~/Fields/DateField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <style type="text/css">a img {border-style:none;}</style>

    <script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/Keyoti_RapidSpell_Web_Common/RapidSpell-DIALOG.js"></script>
    <script type="text/jscript" src="<%=ARMSUtility.TemplatePath %>/javascripts/csshttprequest.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="<%=ARMSUtility.TemplatePath %>/atd.css" />

    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><br><br>
    
    <uc:HintHeader ID="ucHintHeader" runat="server" />
    <cc:Box id="boxObjective" runat="server" Title="Objective Details" width="510">
        <table class="fields">
            <asp:Repeater ID="repObjectiveAttributes" Runat="server">
		        <ItemTemplate>
			        <tr>
				        <td class="label" valign="top" nowrap><span id="lblAttributeLabel" runat="server"></span></td>
                        <td nowrap><cc:DynamicControlsPlaceholder id="pHolder" Runat="Server" /></td>
			        </tr>			
		        </ItemTemplate>
	        </asp:Repeater>
            <uc:Combo id="cboObjectiveStatus" runat="server" field="Objective.StatusCode" Name="Objective Status" topItemText="" isRequired="true"
		        DataType="ObjectiveStatus" DataValueField="Code" DataTextField="Name" DataOrder="DisplayOrder ASC" />
        </table>
    </cc:Box>
    
    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Submit" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:HintFooter ID="ucHintFooter" runat="server" />
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
