<%@ Page Language="C#" AutoEventWireup="false" CodeFile="AdminFeeUserEdit.aspx.cs" Inherits="AdminFeeUserEditAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Text" Src="~/Fields/TextField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Combo" Src="~/Fields/ComboField.ascx" %>
<%@ Register TagPrefix="uc" TagName="Check" Src="~/Fields/CheckBoxField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.Internal.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />

    <span class="heading"><%= base.PageHeading %></span><br>
    <br>

    <cc:Box id="boxUser" runat="server" title="User Information" width="500">
    <table class="fields">
    <uc:Text id="txtName" runat="server" field="FeeUser.Name" />
    <uc:Text id="txtUsername" runat="server" field="FeeUser.Username" IsRequired="true" />
    <uc:Text id="txtPassword" runat="server" field="" Name="Password" IsRequired="true" AutoComplete="off" />
    <uc:Text id="txtPhoneNumber" runat="server" field="FeeUser.PhoneNumber" />
    <uc:Text id="txtAltPhoneNumber" runat="server" field="FeeUser.AlternatePhoneNumber" />
    <uc:Text id="txtEmailAddress" runat="server" field="FeeUser.EmailAddress" Columns="60" />
    <uc:Check id="chkAccountDisabled" runat="server" field="FeeUser.AccountDisabled" />
    </table>
    </cc:Box>

    <asp:Button ID="btnSubmit" Runat="server" CssClass="btn" Text="Add/Update" />
    <asp:Button ID="btnCancel" Runat="server" CssClass="btn" Text="Cancel" CausesValidation="False" />
    
    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
