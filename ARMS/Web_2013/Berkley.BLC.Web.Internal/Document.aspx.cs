using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web.Validation;
using Berkley.BLC.Business;

public partial class DocumentAspx : AppBasePage
{
    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(DocumentAspx_Load);
    }
    #endregion

    void DocumentAspx_Load(object sender, EventArgs e)
    {
        //determine if this is a template, a letter, service plan document, or a survey document
        Guid gDocID = ARMSUtility.GetGuidFromQueryString("docid", false);
        SurveyDocument doc = (gDocID != Guid.Empty) ? SurveyDocument.Get(gDocID) : null;

        Guid gActivityDocID = ARMSUtility.GetGuidFromQueryString("activitydocid", false);
        ActivityDocument activityDoc = (gActivityDocID != Guid.Empty) ? ActivityDocument.Get(gActivityDocID) : null;

        Guid gPlanDocID = ARMSUtility.GetGuidFromQueryString("plandocid", false);
        ServicePlanDocument planDoc = (gPlanDocID != Guid.Empty) ? ServicePlanDocument.Get(gPlanDocID) : null;

        Guid gTemplateID = ARMSUtility.GetGuidFromQueryString("templateid", false);
        MergeDocumentVersion templateDoc = (gTemplateID != Guid.Empty) ? MergeDocumentVersion.Get(gTemplateID) : null;

        Guid gLetterID = ARMSUtility.GetGuidFromQueryString("letterid", false);
        MergeDocumentVersion letterDoc = (gLetterID != Guid.Empty) ? MergeDocumentVersion.Get(gLetterID) : null;

        Guid gReportID = ARMSUtility.GetGuidFromQueryString("reportid", false);
        MergeDocumentVersion reportDoc = (gReportID != Guid.Empty) ? MergeDocumentVersion.Get(gReportID) : null;

        Guid gDocumentID = ARMSUtility.GetGuidFromQueryString("documentid", false);
        MergeDocumentVersion documentDoc = (gDocumentID != Guid.Empty) ? MergeDocumentVersion.Get(gDocumentID) : null;

        Guid gSurveyID = ARMSUtility.GetGuidFromQueryString("surveyid", false);
        Survey eSurvey = (gSurveyID != Guid.Empty) ? Survey.Get(gSurveyID) : null;

        Guid gServicePlanID = ARMSUtility.GetGuidFromQueryString("serviceplanid", false);
        ServicePlan eServicePlan = (gServicePlanID != Guid.Empty) ? ServicePlan.Get(gServicePlanID) : null;


        string fileNetReference;
        string mimeType;
        string docName;
        Company eCompany;
        if (doc != null) //Uploaded Document
        {
            fileNetReference = doc.FileNetReference;
            mimeType = doc.MimeType;
            docName = doc.DocumentName;
            eCompany = doc.Survey.Company;
        }
        else if (activityDoc != null) //Activity Uploaded Document
        {
            fileNetReference = activityDoc.FileNetReference;
            mimeType = activityDoc.MimeType;
            docName = activityDoc.DocumentName;
            eCompany = activityDoc.UploadUser.Company;
        }
        else if (planDoc != null) //Service Plan Uploaded Document
        {
            fileNetReference = planDoc.FileNetReference;
            mimeType = planDoc.MimeType;
            docName = planDoc.DocumentName;
            eCompany = planDoc.ServicePlan.Company;
        }
        else if (templateDoc != null) //Template Document
        {
            fileNetReference = templateDoc.FileNetReference;
            mimeType = templateDoc.MimeType;
            docName = templateDoc.DocumentName;
            eCompany = templateDoc.MergeDocument.Company;
        }
        else if (letterDoc != null) //Letter Document
        {
            fileNetReference = letterDoc.FileNetReference;
            mimeType = letterDoc.MimeType;
            docName = letterDoc.DocumentName;
            eCompany = letterDoc.MergeDocument.Company;
        }
        else if (reportDoc != null) //Report Document
        {
            fileNetReference = reportDoc.FileNetReference;
            mimeType = reportDoc.MimeType;
            docName = reportDoc.DocumentName;
            eCompany = reportDoc.MergeDocument.Company;
        }
        else if (documentDoc != null) //Document
        {
            fileNetReference = documentDoc.FileNetReference;
            mimeType = documentDoc.MimeType;
            docName = documentDoc.DocumentName;
            eCompany = documentDoc.MergeDocument.Company;
        }
        else
        {
            throw new Exception("Was expecting either a letter, template, or uploaded document id.");
        }

        //if (this.IsPostBack)
        //{

            byte[] content;
            ImagingHelper imaging = new ImagingHelper(eCompany);
            System.IO.Stream contentStream = imaging.RetrieveFileImage(fileNetReference);

            //Determine if this is a merge document
            if (letterDoc != null || reportDoc != null)
            {
                if (eSurvey == null)
                {
                    throw new Exception("Survey to be used to mail merge is null.");
                }

                MailMerge mergedDoc = new MailMerge(contentStream, docName);
                mergedDoc.Merge(eSurvey);

                //Added By Vilas Meshram: 05/16/2013 : Squish# 21329 : Add Additional Merge Fields for Service Plan and Survey Templates
                if(eServicePlan != null)
                    mergedDoc.Merge(eServicePlan);

                contentStream = mergedDoc.GetMergedDocument;
                contentStream.Position = 0;
            }
            else if (documentDoc != null)
            {
                if (eServicePlan == null)
                {
                    throw new Exception("ServicePlan to be used to mail merge is null.");
                }

                MailMerge mergedDoc = new MailMerge(contentStream, docName);
                mergedDoc.Merge(eServicePlan);
                contentStream = mergedDoc.GetMergedDocument;
                contentStream.Position = 0;

            }

            // fill a buffer with the data from the stream
            // note: it would be better to just stream the file right to the response but we can't easily do so
            using (contentStream)
            {
                int bufferSize = (int)contentStream.Length;
                content = new byte[bufferSize];

                int bytesRead = contentStream.Read(content, 0, bufferSize);
                if (bytesRead != bufferSize)
                {
                    throw new Exception(bytesRead + " bytes were read from the content stream.  Exactly " + bufferSize + " bytes were expected.");
                }
            }

            // remove any content in the response
            this.Response.Clear();
            this.Response.Buffer = true;

            // set the MIME type and provide a 'friendly' filename for the client
            this.Response.ContentType = mimeType;

            //string nameForHeader = (this.Request.Browser.Browser == "IE" && (this.Request.Browser.Version == "7.0" || this.Request.Browser.Version == "8.0")) ? docName.Replace(" ", "%20") : docName;
            //this.Response.AddHeader("Content-Disposition", "inline;filename=" + nameForHeader);
            string contentDisposition;

            if (this.Request.Browser.Browser == "IE" && (this.Request.Browser.Version == "7.0" || this.Request.Browser.Version == "8.0"))
                contentDisposition = "inline; filename=" + Uri.EscapeDataString(docName);
            else
                contentDisposition = "inline; filename=\"" + docName + "\"; filename*=UTF-8''" + Uri.EscapeDataString(docName);

            this.Response.AddHeader("Content-Disposition", contentDisposition);

            // stream the file content to the user
            this.Response.BinaryWrite(content);
            this.Response.Flush();
            //if (this.Request.Browser.Browser == "IE" || this.Request.Browser.Browser == "InternetExplorer")
                this.Response.End();
        //}
    }
}
