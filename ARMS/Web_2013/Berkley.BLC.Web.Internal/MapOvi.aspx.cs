﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using System.Text;
using Microsoft.Http;

using Berkley.BLC.Entities;
using Entities = Berkley.BLC.Entities;
using QCI.Web;
using QCI.Web.Validation;


public partial class MapOviAspx : AppBasePage
{

    #region Event Handlers
    override protected void OnInit(EventArgs e)
    {
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.Load += new EventHandler(MapOOAspx_Load);
    }
    #endregion

    private List<Pair> _colorList = new List<Pair>();
    
    protected const string _OviAppID = "EPkDdA57A5KGKDWYbha5";
    protected const string _OviToken = "avKY5Ulmv_Vp9EwyOn8a5Q";

    private const string MappingFunction = "MappingFunction";
    private List<Triplet> _geoLocations = new List<Triplet>();
    private List<MapLocation> _invalidLocations = new List<MapLocation>();
    private List<MapLocation> _notMappableLocations = new List<MapLocation>();

    protected int iTotalAddress = 0;


    protected void MapOOAspx_Load(object sender, EventArgs e)
    {
        LoadColors();
        List<MapLocation> mapLocations = new List<MapLocation>();

        //set the page heading
        base.PageHeading = HttpUtility.UrlDecode(ARMSUtility.GetStringFromQueryString("name", true));

        string surveyNumbers = ARMSUtility.GetStringFromQueryString("surveys", true);
        DataSet ds = DB.Engine.GetDataSet(BuildQuery(surveyNumbers));

        //get the list of locations by consultant
        List<System.Threading.Thread> threads = new List<System.Threading.Thread>();
        Dictionary<string, MapGroup> locationsByUser = new Dictionary<string, MapGroup>();

        //For getting coordinates from OVI maps 
        List<MapLocation> mapReturnLocations = new List<MapLocation>();
        List<MapLocation> cityCenterLocations = new List<MapLocation>();
        hnTotalAddress.Value = "0";
       
        //Get a return string of cordinates details from OVI map in format "latitude1;longitude1~latitude2;longitude2~latitude3;longitude3"
        string sResult = Request.Params.Get("__EVENTARGUMENT");

        //Retrive cordinates details and store it in List<MapLocation> class
        if (!string.IsNullOrEmpty(sResult))
        {
            string[] strArrayX = sResult.Split('~');
            string k = string.Empty;

            string[] strArrayY = k.Split(';');

            string latitude = string.Empty;
            string longitude = string.Empty;

            if (sResult != string.Empty)
            {

                for (int x = 0; x < strArrayX.Length; x++)
                {
                    MapLocation pLoc = new MapLocation();
                    bool isCityCenter = false;

                    strArrayY = strArrayX[x].ToString().Split(';');
                    if (strArrayY.Length == 5)
                        isCityCenter = true;

                    pLoc.Latitude = strArrayY[0].ToString().Trim() ;
                    pLoc.Longitude = strArrayY[1].ToString().Trim();

                    if (!isCityCenter)
                    {
                        pLoc.StreetLine1 = strArrayY[2].ToString().Trim();
                        pLoc.City = strArrayY[3].ToString().Trim();
                        pLoc.StateCode = strArrayY[4].ToString().Trim();
                        pLoc.ZipCode = strArrayY[5].ToString().Trim();

                        mapReturnLocations.Add(pLoc); 
                    } 
                    else 
                    {
                        pLoc.City = strArrayY[2].ToString().Trim();
                        pLoc.StateCode = strArrayY[3].ToString().Trim();
                        pLoc.ZipCode = strArrayY[4].ToString().Trim();

                        cityCenterLocations.Add(pLoc);
                    }
                }
            }
        }
        //For getting coordinates from OVI maps 

        //Read MapService setting from config file
        string sMapService = Berkley.BLC.Core.CoreSettings.MapService;

        int i = 0;
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            string key = Common.ReadString(dr[(int)Column.ConsultantName]);
            if (locationsByUser.ContainsKey(key))
            {
                MapLocation mapLocation = BuildMapLocation(dr);

                if (sMapService == "GoogleMap")
                {
                    //Get coordinates using Google Map
                    GetCoordinatesGoogleMap(ref mapLocation, locationsByUser[key].NamedColor);
                }
                else
                {
                    //Get coordinates using Ovi Maps
                    if (string.IsNullOrEmpty(sResult))
                    {
                        GetCoordinatesMapOviJavaScript(ref mapLocation, locationsByUser[key].NamedColor);
                    }
                    else
                    {
                        GetCoordinatesMapOvi(ref mapLocation, locationsByUser[key].NamedColor, mapReturnLocations, cityCenterLocations);
                    }
                }

                locationsByUser[key].MapLocations.Add(mapLocation);
            }
            else
            {
                MapGroup mapGroup = BuildMapGroup(dr, i);
                MapLocation mapLocation = BuildMapLocation(dr);
                mapGroup.MapLocations.Add(mapLocation);

                if (sMapService == "GoogleMap")
                {
                    //Get coordinates using Google Map
                    GetCoordinatesGoogleMap(ref mapLocation, mapGroup.NamedColor);
                }
                else
                {
                    //Get coordinates using Ovi Maps
                    if (string.IsNullOrEmpty(sResult))
                    {
                        GetCoordinatesMapOviJavaScript(ref mapLocation, mapGroup.NamedColor);
                    }
                    else
                    {
                        GetCoordinatesMapOvi(ref mapLocation, mapGroup.NamedColor, mapReturnLocations, cityCenterLocations);
                    }
                }

                locationsByUser.Add(key, mapGroup);
                i = (i < _colorList.Count - 1) ? i + 1 : 0;
            }
        }

        //display a message stating which survey has an invalid zip code
        if (_invalidLocations.Count > 0)
        {
            string count = _invalidLocations.Count.ToString();
            string message = string.Empty;

            JavaScript.ShowMessage(this, string.Format("{0} survey(s) could not be mapped accurately.\r\n\r\nLocation markers for those have been placed at the city center and are denoted with an Exclaimation Mark.", count));

            // Bind to the repeater for invalid locations
            repCityCenterLocations.DataSource = _invalidLocations;
            repCityCenterLocations.DataBind();
            repCityCenterLocations.Visible = true;
        }
        else
        {
            repCityCenterLocations.Visible = false;
        }

        if (_notMappableLocations.Count > 0)
        {
            string count = _notMappableLocations.Count.ToString();
            string message = string.Empty;

            foreach (MapLocation loc in _notMappableLocations)
            {
                message += string.Format("Survey #{0} - Location #{1}\r\n", loc.SurveyNumber, loc.LocationNumber);
            }

            JavaScript.ShowMessage(this, string.Format("The following survey(s) could not be mapped due to invalid address data:\r\n\r\n{0}", message));

            // Bind to the repeater for invalid locations
            repInvalidLocations.DataSource = _notMappableLocations;
            repInvalidLocations.DataBind();
            repInvalidLocations.Visible = true;

        }
        else
        {
            repInvalidLocations.Visible = false;
        }

        // Bind to the Map Key repeater
        repAssignedUsers.DataSource = locationsByUser.Values;
        repAssignedUsers.DataBind();

        AddMapOviScriptsToPage(locationsByUser);
  
    }

    private void GetCoordinatesMapOviJavaScript(ref MapLocation location, string color)
    {
        MapLocation loc = location as MapLocation;

        string fullAddress = (!string.IsNullOrWhiteSpace(loc.StreetLine1) ? loc.StreetLine1 + ", " : string.Empty)
                    + (!string.IsNullOrWhiteSpace(loc.City) ? loc.City + ", " : string.Empty)
                    + (!string.IsNullOrWhiteSpace(loc.StateCode) ? loc.StateCode + ", " : string.Empty)
                    + (!string.IsNullOrWhiteSpace(loc.ZipCode) ? loc.ZipCode.Substring(0, 5) : string.Empty);

        fullAddress = fullAddress.TrimEnd(',');
        fullAddress = fullAddress.TrimStart(',');
        fullAddress = fullAddress.Trim();

        string cityState = (!string.IsNullOrWhiteSpace(loc.City) ? loc.City + ", " : string.Empty)
                    + (!string.IsNullOrWhiteSpace(loc.StateCode) ? loc.StateCode + ", " : string.Empty)
                    + (!string.IsNullOrWhiteSpace(loc.ZipCode) ? loc.ZipCode.Substring(0, 5) : string.Empty);

        cityState = cityState.TrimEnd(',');
        cityState = cityState.TrimStart(',');
        cityState = cityState.Trim();


        StringBuilder sb = new StringBuilder();

        sb.AppendFormat("getCoordinatesMapOvi(\"{0}\"); \r\n", fullAddress);
        sb.AppendFormat("getCoordinatesMapOvi(\"{0}\"); \r\n", cityState);

        iTotalAddress += 2;

        this.ClientScript.RegisterStartupScript(this.GetType(), "GetCordinate-" + iTotalAddress, sb.ToString(), true);
        hnTotalAddress.Value = iTotalAddress.ToString();
    }

    private void GetCoordinatesMapOvi(ref MapLocation location, string color, List<MapLocation> retLocation, List<MapLocation> retCityCenter)
    {
        MapLocation mapLocation = location as MapLocation;
        mapLocation.Color = color;

        HttpQueryString vars = new HttpQueryString();

        //only by zip
        //vars.Add("q", mapLocation.ZipCode.Substring(0, 5));

        //by full address
        //vars.Add("street", mapLocation.StreetLine1);
        //vars.Add("city", mapLocation.City);
        //vars.Add("state", mapLocation.StateCode);
        //vars.Add("zip", mapLocation.ZipCode.Substring(0, 5));

        //vars.Add("flags", "c");

        // find a specific object
        MapLocation Inputlocation = location;
        MapLocation mapResult = retLocation.Find(delegate(MapLocation p) 
        {return p.StreetLine1 == Inputlocation.StreetLine1 && p.City==Inputlocation.City && p.StateCode==Inputlocation.StateCode && p.ZipCode==Inputlocation.ZipCode;});

        if (mapResult != null)
        {
            mapLocation.Latitude = (mapResult.Latitude == "null") ? string.Empty : mapResult.Latitude;
            mapLocation.Longitude = (mapResult.Longitude == "null") ? string.Empty : mapResult.Longitude;
        }

        // query for the locations city center lat long in the retCityCenters array
        // city center coordinates will be used to fall back to if the address is not able to be geocoded.
        MapLocation cityCenterResult = retCityCenter.Find(delegate(MapLocation p)
        { return p.City == Inputlocation.City && p.StateCode == Inputlocation.StateCode && p.ZipCode == Inputlocation.ZipCode; });

        if (cityCenterResult != null)
        {
            mapLocation.CityCenterLatitude = (cityCenterResult.Latitude == "nulll") ? string.Empty : cityCenterResult.Latitude;
            mapLocation.CityCenterLongitude = (cityCenterResult.Longitude == "null") ? string.Empty : cityCenterResult.Longitude;
        }

        if (string.IsNullOrEmpty(mapLocation.Latitude) && string.IsNullOrEmpty(mapLocation.Longitude))
        {
            _invalidLocations.Add(mapLocation);

            // If no city center was found as well
            if (string.IsNullOrEmpty(mapLocation.CityCenterLatitude) && string.IsNullOrEmpty(mapLocation.CityCenterLongitude)) 
            {
                _notMappableLocations.Add(mapLocation);
            }

        }

    }


    private void GetCoordinatesGoogleMap(ref MapLocation location, string color)
    {
        MapLocation mapLocation = location as MapLocation;

        HttpQueryString vars = new HttpQueryString();

        //only by zip
        //vars.Add("q", mapLocation.ZipCode.Substring(0, 5));

        //by full address
        vars.Add("street", mapLocation.StreetLine1);
        vars.Add("city", mapLocation.City);
        vars.Add("state", mapLocation.StateCode);
        vars.Add("zip", mapLocation.ZipCode.Substring(0, 5));

        vars.Add("flags", "c");

        string geocoderUri = string.Format(@"http://maps.googleapis.com/maps/api/geocode/xml?address={0},{1},{2},{3}&sensor=false", mapLocation.StreetLine1, mapLocation.City, mapLocation.ZipCode.Substring(0, 5), mapLocation.StateCode);

        XmlDocument geocoderXmlDoc = new XmlDocument();
        geocoderXmlDoc.Load(geocoderUri);

        XmlNamespaceManager nsMgr = new XmlNamespaceManager(geocoderXmlDoc.NameTable);
        nsMgr.AddNamespace("geo", @"http://www.w3.org/2003/01/geo/wgs84_pos#");

        string latitude = null;
        string longitude = null;


        //if (geocoderXmlDoc.DocumentElement.SelectSingleNode(@"//geometry/location/lat", nsMgr) == null)
        //{
        //    iTotalAddress += 1;
        //    JavaScript.ShowMessage(this, iTotalAddress.ToString() + "  I am Null");
        //}

        if (geocoderXmlDoc.DocumentElement.SelectSingleNode(@"//geometry/location/lat", nsMgr)!=null)
            latitude=geocoderXmlDoc.DocumentElement.SelectSingleNode(@"//geometry/location/lat", nsMgr).InnerText;

        if (geocoderXmlDoc.DocumentElement.SelectSingleNode(@"//geometry/location/lng", nsMgr)!=null)
            longitude=geocoderXmlDoc.DocumentElement.SelectSingleNode(@"//geometry/location/lng", nsMgr).InnerText;
        
    
        if (longitude != null && latitude != null)
        {
            mapLocation.Longitude = longitude;
            mapLocation.Latitude = latitude;
            mapLocation.Color = color;
        }
        else
        {
            _invalidLocations.Add(mapLocation);
        }


    }


    private void AddMapOviScriptsToPage(Dictionary<string, MapGroup> locationsByUser)
    {

        if (!ScriptHelper.IsClientScriptRegistered(this.Page, MappingFunction))
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("var bubbleContainer = new ovi.mapsapi.map.component.InfoBubbles();");
            sb.AppendLine("var map = new ovi.mapsapi.map.Display( ");
            sb.AppendLine(" document.getElementById('map'), { ");
            sb.AppendLine(" components:[");
            sb.AppendLine("             new ovi.mapsapi.map.component.Behavior(),");
            sb.AppendLine("             new ovi.mapsapi.map.component.ZoomBar(),");
            sb.AppendLine("             new ovi.mapsapi.map.component.Overview(),");
            sb.AppendLine("             new ovi.mapsapi.map.component.TypeSelector(),");
            sb.AppendLine("             new ovi.mapsapi.map.component.ViewControl(),");
            sb.AppendLine("             new ovi.mapsapi.map.component.ScaleBar(),");
            sb.AppendLine("             bubbleContainer");
            sb.AppendLine("            ],");
            sb.AppendLine(" zoomLevel: 3,");
            sb.AppendLine(" center: [41.88425, -87.63245]");
            sb.AppendLine(" });");

            int i = 0;
            List<String> listNames = new List<string>();
            foreach (var pair in locationsByUser)
            {
                MapGroup mapGroup = pair.Value as MapGroup;

                foreach (MapLocation mapLocation in mapGroup.MapLocations)
                {
                    // Locates and Places the Map Marker into the view only if the location has Lat/Long values.
                    // Also falls back to the cooridinate position of the city center
                    
                    bool isMappable = (!string.IsNullOrEmpty(mapLocation.Longitude) && !string.IsNullOrEmpty(mapLocation.Latitude)) ? true : false;
                    bool hasCityCenter = (!string.IsNullOrEmpty(mapLocation.CityCenterLongitude) && !string.IsNullOrEmpty(mapLocation.CityCenterLatitude)) ? true : false;

                    if (isMappable || hasCityCenter)
                    {
                        // building the custom map marker js
                        string geoName = string.Format("geoPoint{0}", i);
                        string markerName = string.Format("newMarker{0}", i);
                        string titleBubble = string.Format("titleBubble{0}", i);
                        string footnote = string.Empty;

                        string plotlatitude = (!string.IsNullOrEmpty(mapLocation.Latitude)) ? mapLocation.Latitude : mapLocation.CityCenterLatitude; ;
                        string plotlongitude = (!string.IsNullOrEmpty(mapLocation.Longitude)) ? mapLocation.Longitude : mapLocation.CityCenterLongitude;

                        string mapLocationIcon = mapLocation.Color + ((!isMappable) ? "-notice" : string.Empty);

                        sb.AppendFormat("var {0} = new ovi.mapsapi.geo.Coordinate({1}, {2});\r\n", geoName, plotlatitude, plotlongitude);
                        sb.AppendFormat("var {0} = new ovi.mapsapi.map.Marker({1},", markerName, geoName);
                        sb.AppendLine("  {");
                        sb.AppendFormat("     icon: '{0}',", ARMSUtility.AppPath + "/images/colors/" + mapLocationIcon + ".png");
                        sb.AppendLine(" \r\n     anchor: new ovi.mapsapi.util.Point(15, 15)");
                        sb.AppendLine("   });\r\n");
                        sb.AppendFormat("  map.objects.add({0});\r\n", markerName);
                        sb.AppendFormat("  map.zoomTo({0}.getBoundingBox(), true, \"default\");\r\n\r\n", markerName);
                        
                        // if not mappable place a small message into tooltip footnote
                        if (!isMappable)
                        {
                            footnote = "<br><b><span class=\"notice\">Address is not mappable.<br>Point placed at city center.</span></b>";
                        }

                        //Mouse Over
                        sb.AppendFormat("  {0}.addListener('mouseover', function ()", markerName);
                        sb.AppendLine("{");
                        sb.AppendFormat("    {0}= bubbleContainer.addBubble('<b>Survey #:&nbsp;</b>{2}<br><b>Location #:&nbsp;</b>{3}<br><b>Insured:&nbsp;</b>{4}{5}', {1});\r\n", titleBubble, geoName, mapLocation.SurveyNumber, mapLocation.LocationNumber, mapLocation.InsuredName, footnote);
                        sb.AppendLine("   });\r\n");

                        //Mouse Leave
                        sb.AppendFormat("  {0}.addListener('mouseleave', function ()", markerName);
                        sb.AppendLine("{");
                        sb.AppendFormat("       bubbleContainer.removeBubble({0})\r\n", titleBubble);
                        sb.AppendLine("   });\r\n");

                        listNames.Add(geoName);
                        i++;
                    }
                }

                //sb.AppendFormat("  map.zoomTo(map.getBoundingBox(), true, \"default\");\r\n\r\n");

            }

            this.ClientScript.RegisterStartupScript(this.GetType(), MappingFunction, sb.ToString(), true);

        }
    }

    private void LoadColors()
    {
        _colorList.Add(new Pair("red", "#FF0000"));
        _colorList.Add(new Pair("yellow", "#FFFF00"));
        _colorList.Add(new Pair("green", "#008000"));
        _colorList.Add(new Pair("blue", "#0000FF"));
        _colorList.Add(new Pair("purple", "#800080"));
        _colorList.Add(new Pair("orange", "#FFA500"));
        _colorList.Add(new Pair("tourquoise", "#40E0D0"));
        _colorList.Add(new Pair("olive", "#808000"));
        _colorList.Add(new Pair("teal", "#008080"));
        _colorList.Add(new Pair("lime", "#00FF00"));
        _colorList.Add(new Pair("gray", "#808080"));
        _colorList.Add(new Pair("salmon", "#FFA07A"));
        _colorList.Add(new Pair("yellowgreen", "#9ACD32"));
        _colorList.Add(new Pair("Violet", "#C71585"));
        _colorList.Add(new Pair("darkred", "#8B0000"));
        _colorList.Add(new Pair("cornsilk", "#FFF8DC"));
        _colorList.Add(new Pair("pink", "#FFC0CB"));
        _colorList.Add(new Pair("chocolate", "#D2691E"));
        _colorList.Add(new Pair("rosybrown", "#BC8F8F"));
        _colorList.Add(new Pair("steelblue", "#4682B4"));
    }

    private string BuildQuery(string surveyNumbers)
    {
        StringBuilder sql = new StringBuilder();

        sql.Append("SELECT DISTINCT ");
        sql.Append("s.SurveyNumber, ");
        sql.Append("i.Name, ");
        sql.Append("l.LocationNumber, ");
        sql.Append("l.StreetLine1, ");
        sql.Append("l.City, ");
        sql.Append("l.StateCode, ");
        sql.Append("l.ZipCode, ");
        sql.Append("CASE WHEN (s.SurveyStatusCode = 'SA' OR s.SurveyStatusCode = 'SW' OR s.SurveyStatusCode = 'SR') THEN u1.Name ");
        sql.Append("WHEN ((s.SurveyStatusCode = 'OH' OR s.SurveyStatusCode = 'SU') AND u3.Name IS NOT NULL) THEN u3.Name ");
        sql.Append("WHEN u2.Name IS NOT NULL THEN u2.Name ELSE 'Not Assigned' END AS ConsultantUserName ");
        sql.Append("FROM Survey s ");
        sql.Append("INNER JOIN Survey_PolicyCoverage spc ON spc.SurveyID = s.SurveyID ");
        sql.Append("INNER JOIN Location l ON l.LocationID = spc.LocationID ");
        sql.Append("INNER JOIN Insured i ON i.InsuredID = l.InsuredID ");
        sql.Append("LEFT JOIN dbo.[User] AS u1 ON u1.UserID = s.AssignedUserID ");
        sql.Append("LEFT JOIN dbo.[User] AS u2 ON u2.UserID = s.ConsultantUserID ");
        sql.Append("LEFT JOIN dbo.[User] AS u3 ON u3.UserID = s.RecommendedUserID ");
        sql.AppendFormat("WHERE spc.Active = 1 AND LEN(l.ZipCode) >= 5 AND s.SurveyNumber IN ({0})  ", surveyNumbers);

        return sql.ToString();
    }

    private MapGroup BuildMapGroup(DataRow dr, int orderIndex)
    {
        MapGroup mapGroup = new MapGroup();
        mapGroup.UserName = Common.ReadString(dr[(int)Column.ConsultantName]);
        mapGroup.CSSColor = _colorList[orderIndex].Second.ToString();
        mapGroup.NamedColor = _colorList[orderIndex].First.ToString();
        mapGroup.OrderIndex = orderIndex + 1;

        return mapGroup;
    }

    private MapLocation BuildMapLocation(DataRow dr)
    {
        MapLocation mapLocation = new MapLocation();
        mapLocation.LocationNumber = Common.ReadString(dr[(int)Column.LocationNumber]);
        mapLocation.StreetLine1 = Common.ReadString(dr[(int)Column.LocationStreetLine1]);
        mapLocation.City = Common.ReadString(dr[(int)Column.LocationCity]);
        mapLocation.StateCode = Common.ReadString(dr[(int)Column.LocationStateCode]);
        mapLocation.ZipCode = Common.ReadString(dr[(int)Column.LocationZipCode]);
        mapLocation.SurveyNumber = Common.ReadString(dr[(int)Column.SurveyNumber]);
        mapLocation.InsuredName = Common.ReadString(dr[(int)Column.InsuredName]).Replace("'", "");

        return mapLocation;
    }

    public enum Column
    {
        SurveyNumber = 0,
        InsuredName = 1,
        LocationNumber = 2,
        LocationStreetLine1 = 3,
        LocationCity = 4,
        LocationStateCode = 5,
        LocationZipCode = 6,
        ConsultantName = 7
    }

    protected class MapGroup
    {
        private string _userName = string.Empty;
        private string _cssColor = string.Empty;
        private string _namedColor = string.Empty;
        private int _orderIndex = Int32.MinValue;
        private List<MapLocation> _mapLocation = new List<MapLocation>();

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
            }
        }

        public string CSSColor
        {
            get { return _cssColor; }
            set
            {
                _cssColor = value;
            }
        }

        public string NamedColor
        {
            get { return _namedColor; }
            set
            {
                _namedColor = value;
            }
        }

        public int OrderIndex
        {
            get { return _orderIndex; }
            set
            {
                _orderIndex = value;
            }
        }

        public List<MapLocation> MapLocations
        {
            get { return _mapLocation; }
            set
            {
                _mapLocation = value;
            }
        }
    }

    protected class MapLocation
    {
        private string _surveyNumber = string.Empty;
        private string _insuredName = string.Empty;
        private string _locationNumber = string.Empty;
        private string _streetLine1 = string.Empty;
        private string _city = string.Empty;
        private string _stateCode = string.Empty;
        private string _zipCode = string.Empty;
        private string _color = string.Empty;
        private string _longitude = string.Empty;
        private string _latitude = string.Empty;
        private string _cityCenterLongitude = string.Empty;
        private string _cityCenterLatitude = string.Empty;

        public string SurveyNumber
        {
            get { return _surveyNumber; }
            set
            {
                _surveyNumber = value;
            }
        }

        public string InsuredName
        {
            get { return _insuredName; }
            set
            {
                _insuredName = value;
            }
        }

        public string LocationNumber
        {
            get { return _locationNumber; }
            set
            {
                _locationNumber = value;
            }
        }

        public string StreetLine1
        {
            get { return _streetLine1; }
            set
            {
                _streetLine1 = value;
            }
        }

        public string City
        {
            get { return _city; }
            set
            {
                _city = value;
            }
        }

        public string StateCode
        {
            get { return _stateCode; }
            set
            {
                _stateCode = value;
            }
        }

        public string ZipCode
        {
            get { return _zipCode; }
            set
            {
                _zipCode = value;
            }
        }

        public string Color
        {
            get { return _color; }
            set
            {
                _color = value;
            }
        }

        public string Longitude
        {
            get { return _longitude; }
            set
            {
                _longitude = value;
            }
        }

        public string Latitude
        {
            get { return _latitude; }
            set
            {
                _latitude = value;
            }
        }

        public string CityCenterLongitude
        {
            get { return _cityCenterLongitude; }
            set
            {
                _cityCenterLongitude = value;
            }
        }

        public string CityCenterLatitude
        {
            get { return _cityCenterLatitude; }
            set
            {
                _cityCenterLatitude = value;
            }
        }
    }
 
}
