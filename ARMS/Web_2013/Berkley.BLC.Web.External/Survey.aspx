﻿<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false" ValidateRequest="false"  CodeFile="Survey.aspx.cs" Inherits="Berkley.BLC.Web.External.SurveyAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.External.Controls" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="dp" Namespace="QCI.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="Counter" Src="~/Controls/Counter.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" media="screen" type="text/css" href="<%= base.TemplatePath %>/styles.css" />
    <link rel="stylesheet" media="print" type="text/css" href="<%= base.TemplatePath %>/Print-Survey.css" />
    
    <form id="frm" method="post" runat="server">
    <input id="hdnActionLinkClickedFlag" type="hidden" />
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span>
    
    <a class="nav" href="Surveys.aspx<%= Utility.GetQueryStringForNav("surveyid") %>" style="MARGIN-LEFT: 20px"><< Back to Queue</a>
    
    <br><br>
    
    <table cellspacing="0" cellpadding="0" border="0">
    <tr>
	    <td valign="top" width="500">
        <cc:Box id="boxSurvey" runat="server" title="Survey Information" width="100%">
	        <table class="fields">
            <asp:Image ID="imgPriority" runat="server" ImageUrl="./Images/arrow_forward.png"  Visible="False"/>
	        <uc:Label id="lblNumber" runat="server" field="Survey.Number" />
	        <uc:Label id="lblDueDate" runat="server" field="Survey.DueDate" />
	        <uc:Label id="lblStatus" runat="server" field="SurveyStatus.Name" />
	        <uc:Label id="lblDateSurveyed" runat="server" field="Survey.SurveyedDate" Name="Date Surveyed" />
	        <uc:Label id="lblSurveyHours" runat="server" field="Survey.Hours" Name="Vendor Hours"/>
	        <uc:Label id="lblFeeCost" runat="server" field="Survey.FeeConsultantCost" Name="Vendor Cost" ValueFormat="{0:c}"/>
	        <% if (CurrentUser.Company.SupportsOverallGrading) { %>
	        <uc:Label id="lblGrading" runat="server" field="SurveyGrading.Name" Name="Overall Grading" />
	        <% } %>
	        <% if (_eSurvey.AssignDate != DateTime.MinValue) { %>
	        <uc:label id="lblAssignDate" runat="server" field="Survey.AssignDate" Name="Date Assigned" />
	        <% } %>
	        <% if (_eSurvey.AcceptDate != DateTime.MinValue) { %>
	        <uc:label id="lblAcceptDate" runat="server" field="Survey.AcceptDate" Name="Date Accepted" />
	        <% } %>
            <% if (CurrentUser.Company.SupportsNonDisclosure) { %>
	            <% if (_eSurvey.NonDisclosureRequired) { %>
                <tr>
		            <td class="label" valign="top" style="color:Red" nowrap>Non Disclosure Required</td>
		            <td style="color:Red">Yes</td>
	            </tr>
                <% } else { %>
                <uc:Label id="lblRequireNonDisclosure" runat="server" field="Survey.NonDisclosureRequired" />
                <% } %>
            <% } %>
	        <uc:Label id="lblLastModifiedOn" runat="server" field="Survey.LastModifiedOn" ValueFormat="{0:g}" />
	        </table>
        </cc:Box>

        <cc:Box id="boxNotes" runat="server" title="Comments" width="100%">
        <a id="lnkAddNote" runat="server" href="javascript:Toggle('divNotes')">Show/Hide</a>
        <div id="divNotes" style="MARGIN-TOP: 10px">
        <% if( repNotes.Items.Count > 0 ) { %>
        <asp:Repeater ID="repNotes" Runat="server">
	        <HeaderTemplate>
		        <table width="100%" cellspacing="0" cellpadding="0" border="0">
	        </HeaderTemplate>
	        <ItemTemplate>
		        <tr>
			        <td style="BORDER-BOTTOM: solid 1px black">
				        <%# GetUserName(DataBinder.Eval(Container.DataItem, "User")) %> - 
				        <%# HtmlEncode(Container.DataItem, "EntryDate", "{0:g}") %>
			        </td>
		        </tr>
		        <tr>
			        <td style="PADDING-BOTTOM: 10px">
				        <%# GetEncodedComment(Container.DataItem) %>
			        </td>
		        </tr>
	        </ItemTemplate>
	        <FooterTemplate>
		        </table>
	        </FooterTemplate>
        </asp:Repeater>
        <% } else { %>
        <div style="PADDING-BOTTOM: 10px">
	        (none)
        </div>
        <% } %>
        </div>
        <div id="divAddNote" runat="server" style="MARGIN-TOP: 10px">
	        Comment<br>
        </div>
	        <asp:TextBox id="txtNote" runat="server" CssClass="txt" TextMode="MultiLine" Rows="5" Columns="80" />
        <div style="padding-top:5px">
	        <asp:Button ID="btnAddNote" Runat="server" Text="Add Comment" CssClass="btn" CausesValidation="False" />&nbsp;
        </div>
        </cc:Box>

        <cc:Box id="boxDocs" runat="server" title="Documents" width="100%">
        <a id="lnkAddDoc" href="javascript:Toggle('divAddDoc')">Show/Hide</a>
        <div id="divAddDoc" runat="server" style="MARGIN-TOP: 10px">
        <% if( repDocs.Items.Count > 0 ) { %>
        <asp:Repeater ID="repDocs" Runat="server">
	        <HeaderTemplate>
		        <table cellspacing="0" cellpadding="0" border="0">
	        </HeaderTemplate>
	        <ItemTemplate>
		        <tr>
			        <td style="PADDING-BOTTOM: 3px">
			            <a id="lnkDownloadDocInNewWindow" href='<%# DataBinder.Eval(Container.DataItem, "ID", "Document.aspx?docid={0}") %>' target="_blank" runat="server"><%# GetFileNameWithSize(Container.DataItem) %></a>
				        <asp:LinkButton ID="lnkDownloadDoc" runat="server" OnClick="lnkDownloadDoc_OnClick"><%# GetFileNameWithSize(Container.DataItem) %></asp:LinkButton>
			        </td>
			        <td style="PADDING-LEFT: 10px">
				        From <%# GetUserName(DataBinder.Eval(Container.DataItem, "UploadUser")) %> on <%# HtmlEncode(Container.DataItem, "UploadedOn", "{0:g}") %>
			        </td>
		        </tr>
	        </ItemTemplate>
	        <FooterTemplate>
		        </table>
	        </FooterTemplate>
        </asp:Repeater>
        <% } else { %>
        <div style="PADDING-BOTTOM: 10px">
	        (none)
        </div>
        <% } %>
	        <div id="divUploadDoc" style="padding-top:5px" runat="server">
	            Attach Document<br>
	            <input type="file" id="fileUploader" runat="server" name="fileDocument" class="txt" size="70"><br>
	            <%if (cboDocType.Items.Count > 0) { %>
	            Doc Type<br>
	            <asp:DropDownList ID="cboDocType" runat="server" CssClass="cbo"></asp:DropDownList><br>
	            <%if (CurrentUser.Company.SupportsDocRemarks) { %>
                    Doc Remarks<br>
	                <asp:TextBox ID="txtDocRemarks" runat="server" Columns="35" CssClass="txt"></asp:TextBox>
                <% } %>
	            <% } %>
	            <div style="padding-top:5px">
	                <asp:Button ID="btnAttachDocument" Runat="server" Text="Upload" CssClass="btn" CausesValidation="False" />
	            </div>
	        </div>
        </div>
        </cc:Box>

        </td>
        <td width="15">&nbsp;</td>
        <td valign="top">
        
        <cc:Box id="boxLinks" runat="server" title="Insured Details" width="175">
        <ul>
	        <li><a href="insured.aspx<%= Utility.GetQueryStringForNav() %>">Insured Details</a></li>
        </ul>
        </cc:Box>
        	
        <cc:Box id="boxActions" runat="server" title="Available Actions" width="180">
	        <asp:Repeater ID="repActions" Runat="server">
		        <HeaderTemplate>
			        <ul>
		        </HeaderTemplate>
		        <ItemTemplate>
			        <li>
				        <asp:LinkButton id="btnAction" OnClientClick="DisableLinkAfterClick(this);" runat="server" CommandName='<%# HtmlEncode(Container.DataItem, "Code") %>'><%# HtmlEncode(Container.DataItem, "ActionName") %></asp:LinkButton>
			        </li>
		        </ItemTemplate>
		        <FooterTemplate>
			        <% if( repActions.Items.Count == 0 ) { %>
			        <li>None</li>
			        <% } %>
			        </ul>
		        </FooterTemplate>
	        </asp:Repeater>

	        <div id="divSetDetails" runat="server">
		        <span>Date Surveyed:</span>
		        <div style="margin: 1px 0px 10px 0px">
			        <dp:DatePicker id="dpDateSurveyed" runat="server" CssClass="txt" Columns="10" AutoPostBack="False" onfocus="StartAutoFormat(this, 'Date')" /><br>
		        </div>
		        
		        <span>Total Hours:</span>
		        <div style="margin: 1px 0px 10px 0px">
			        <asp:TextBox ID="txtTotalHours" CssClass="txt" Columns="10" runat="server"></asp:TextBox>
		        </div>
		        
		        <% if (CurrentUser.Company.SupportsOverallGrading) { %>
		        <span>Overall Grading:</span>
		        <div style="margin: 1px 0px 10px 0px">
			        <asp:DropDownList ID="cboOverallGrading" runat="server" CssClass="cbo" />
		        </div>
		        <% } %>
		        
		        <span>Cost:</span>
		        <div style="margin: 1px 0px 10px 0px">
			        <asp:TextBox ID="txtCost" CssClass="txt" Columns="10" runat="server"></asp:TextBox>
		        </div>
		        
		        <% if (CurrentUser.Company.SupportsVendorRecReminder) { %>
		        <span>Recommendations:</span>
		        <div style="margin: 1px 0px 10px 0px">
			        <asp:RadioButton id="rdoRecYes" GroupName="Recs" Text="Yes" CssClass="rdo" runat="server" />
			        <asp:RadioButton id="rdoRecNo" GroupName="Recs" Text="No" CssClass="rdo" runat="server" />
		        </div>
		        <% } %>
              <% if (Berkley.BLC.Business.Utility.GetCompanyParameter("ShowUWNotified", CurrentUser.Company) == "true"){%>
                 <span>Was Underwriter Notified:</span>
                <div style="margin: 1px 0px 10px 0px">
                    <asp:RadioButton id="rdoUWNotifiedYes" GroupName="UWNotified" Text="Yes" CssClass="rdo" runat="server" />
                    <asp:RadioButton id="rdoUWNotifiedNo" GroupName="UWNotified" Text="No" CssClass="rdo" runat="server" />
                </div>
               <% } %>
		        <asp:Button ID="btnSetDetailsSubmit" Runat="server" Text="Submit" CssClass="btn" />
		        <asp:Button ID="btnSetDetailsCancel" Runat="server" Text="Cancel" CssClass="btn" />
	        </div>
	        <div id="divReports" runat="server">
		        <span>Generate:</span>
		        <div style="margin: 5px 0px 5px 0px">
			        <asp:DropDownList ID="cboReports" runat="server" CssClass="cbo" />
		        </div>
		        <input type="button" id="btnReportSelect" value="Generate" class="btn" OnClick="<%= string.Format("GenerateReport('{0}');", _eSurvey.ID) %>" />
		        <asp:Button ID="btnReportCancel" Runat="server" Text="Cancel" CssClass="btn" />
	        </div>
        </cc:Box>
	    </td>
    </tr>
    </table>


    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
    function Toggle(id)
    {
	    ToggleControlDisplay(id);
    }
    function GenerateReport(surveyID)
    {
	    var cboReports = document.getElementById('cboReports');
	    
	    if (cboReports.value != null && cboReports.value != "") {
	        window.open('document.aspx?reportid=' + cboReports.value + '&surveyid=' + surveyID);
	        document.location = document.URL;
	    }
	    else {
	        alert("Select a report to generate.");
	    }
	}
	function DisableLinkAfterClick(link) {
	    if (link.innerText == 'Complete Request') {
	        if (document.getElementById('hdnActionLinkClickedFlag').value == 1) {
	            link.disabled = true;
	            return false;
	        }
	        else {
	            link.disabled = false
	        }

	        document.getElementById('hdnActionLinkClickedFlag').value = 1;
	    }
	}
    </script>
    </form>
</body>
</html>
