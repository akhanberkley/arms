<%@ Control Language="C#" AutoEventWireup="false" CodeFile="PageFooter.ascx.cs" Inherits="Berkley.BLC.Web.External.Template.PageFooter" %>
<!-- BEGIN PAGE FOOTER-->
	</td>
</tr>
<tr>
	<td width="100%" height="3"><div class="footerBorder" style="BORDER-TOP: #808080 1px solid;"></div></td>
</tr>
<tr>
	<td class="footerLine" height="15"><div></div></td>
</tr>
<tr class="pageFooter">
	<td width="100%">
		<a class="footerLink" target="_blank" href="http://www.wrbc.com/"><%= _copyright %></a>
	</td>
</tr>
</table>

<asp:ValidationSummary id="valSummary" Runat="server" DisplayMode="BulletList" ShowMessageBox="True" ShowSummary="False" HeaderText="Please correct the following errors:" />
<asp:Literal ID="litJavaScriptHolder" Runat="server" /><%-- this literal is required for the QCI.Web.JavaScript engine to work; it's attaching to the pre-render event of the summary, which is causing bugs --%>
<!-- END PAGE FOOTER -->
