<%@ Control Language="C#" AutoEventWireup="false" CodeFile="PageHeader.ascx.cs" Inherits="Berkley.BLC.Web.External.Template.PageHeader" %>
<%@ Register TagPrefix="uc" TagName="Tabs" Src="Tabs.ascx" %>
<!-- BEGIN PAGE HEADER -->
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
	<td class="logoLine">
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td valign="top" nowrap><img src="<%= _appPath %>/template/images/title.gif" border="0" alt="Account Reporting Management System"><span class="surveyHeader">Loss Control Survey Request</span></td>
			<td width="100%" style="font-size:large"><%= _env %></td>
			<td style="PADDING-RIGHT: 10px" align="right" nowrap>
				<span class="logoCompany"><%= (_user != null) ? _user.CompanyName : "&nbsp;" %></span><br>
				<span><%= (_user != null) ? _user.Name : "&nbsp;" %></span><br>
				<% if( _user != null && _user.CanLogout ) { %>
				<div class="logout"><a href="<%= _appPath %>/logout.aspx">Logout</a></div>
				<% } %>
 			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="tabsLine" valign="bottom">

<uc:Tabs id="ucTabs" runat="server" />

	</td>
</tr>
<tr>
	<td class="pageBody" width="100%" height="400" valign="top">
<!-- END PAGE HEADER -->
