using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Reflection;
using QCI.Web;

namespace Berkley.BLC.Web.External.Template
{
    public partial class PageFooter : System.Web.UI.UserControl
    {
        //protected System.Web.UI.WebControls.ValidationSummary valSummary;

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.PreRender += new System.EventHandler(this.Page_PreRender);

        }
        #endregion

        protected string _appPath; // used during HTML rendering
        protected static string _copyright = null; // used during HTML rendering

        private void Page_Load(object sender, System.EventArgs e)
        {
            _appPath = Utility.AppPath;

            // get the copyright information from this assembly
            if (_copyright == null)
            {
                Assembly assembly = Assembly.Load("App_Code");
                AssemblyCopyrightAttribute copyrightAttrib = (AssemblyCopyrightAttribute)Attribute.GetCustomAttribute(assembly, typeof(AssemblyCopyrightAttribute));
                _copyright = Server.HtmlEncode(copyrightAttrib.Copyright);
            }
        }

        private void Page_PreRender(object sender, EventArgs e)
        {
            // show the summary control when there are validators on the page
            valSummary.Visible = (this.Page.Validators.Count > 0);
        }
    }
}
