using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Berkley.BLC.Entities;
using QCI.Web;
using Berkley.BLC.Core;

namespace Berkley.BLC.Web.External.Template
{
    public partial class PageHeader : System.Web.UI.UserControl
    {
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        protected string _appPath; // used during HTML rendering
        protected string _env; // used during HTML rendering
        protected UserIdentity _user; // used during HTML rendering

        private void Page_Load(object sender, System.EventArgs e)
        {
            _appPath = Utility.AppPath;

            // get the current user (if any)
            _user = UserIdentity.Current;

            // set the environment to display on the page
            _env = (CoreSettings.ConfigurationType != ConfigurationType.Production) ? string.Format("* {0} Environment *", CoreSettings.ConfigurationType.ToString()) : string.Empty;
        }
    }
}
