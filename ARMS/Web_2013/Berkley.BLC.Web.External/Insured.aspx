﻿<%@ Page Language="C#" AutoEventWireup="false" EnableEventValidation="false"  CodeFile="Insured.aspx.cs" Inherits="Berkley.BLC.Web.External.InsuredAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="uc" TagName="Label" Src="~/Fields/LabelField.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.External.Controls" %>
<%@ Import Namespace="Berkley.BLC.Entities" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <div class="insuredscreen">
    <link rel="stylesheet" media="screen" type="text/css" href="<%= base.TemplatePath %>/styles.css">
    <link rel="stylesheet" media="print" type="text/css" href="<%= base.TemplatePath %>/Print-Insured.css" />

    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="headingForPrint" id="lblHeadingForPrint" runat="server">Survey Location:&nbsp;<%= cboLocations.SelectedItem %></span>
    <span class="heading">Survey Location:&nbsp;<asp:dropdownlist id="cboLocations" runat="server" AutoPostBack="True" CssClass="cbo"></asp:dropdownlist></span>&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="button" id="btnPrintPage" value="Print" class="btn" onClick="window.print()" />
    <a class="nav" id="BackToSurvey" href="Survey.aspx<%= Utility.GetQueryStringForNav("locationid") %>" style="MARGIN-LEFT: 20px"><< Back to Survey</a><br><br>
    
    <div class="surveyDetails">
    <cc:Box id="boxSurvey" runat="server" title="Survey Information" width="700">        
        <table>
            <tr>
                <td valign="top" width="35%">
                    <table class="fields">
                        <uc:Label id="lblNumber" runat="server" field="Survey.Number" />
                        <uc:Label id="lblStatus" runat="server" field="SurveyStatus.Name" />
                        <uc:Label id="lblCreateDate" runat="server" field="Survey.CreateDate" Name="Created On" />
                        <uc:Label id="lblCreatedBy" runat="server" field="Survey.HtmlCreatedBy" Name="Created By" />
                        <uc:label id="lblAssignDate" runat="server" field="Survey.AssignDate" Name="Assigned Date" />
                        <uc:label id="lblAcceptDate" runat="server" field="Survey.AcceptDate" Name="Accepted Date" />
                    </table>
                </td>
                <td width="30%" align="center"><img class="sep" src="<%= AppPath %>/images/black_pixel.gif" height="75px" width="1px"></td>
                <td valign="top" width="35%">
                    <table class="fields">
                        <uc:Label id="lblDueDate" runat="server" field="Survey.DueDate" />
                        <uc:Label id="lblType" runat="server" field="SurveyTypeType.Name" Name="Survey Type" />
                        <uc:Label id="lblAssignedUser" runat="server" field="User.Name" Name="Assigned Rep" />
                    </table>
                </td>
            </tr>
        </table>         
    </cc:Box>
    </div>
    
    <hr class="boxBreak" />
    
    <cc:Box id="boxInsured" runat="server" title="Insured" width="700">
        <table>
            <tr>
                <td valign="top" width="350px">
                    <table class="fields">
                        <uc:Label id="lblClientID" runat="server" field="Insured.ClientID" />
                        <uc:Label id="lblName" runat="server" field="Insured.Name" />
                        <uc:Label id="lblName2" runat="server" field="Insured.Name2" />
                        <uc:Label id="lblAddress" runat="server" field="Insured.HtmlAddress" Name="Address" />
                        <uc:Label id="lblBusinessOperations" runat="server" field="Insured.BusinessOperations" Name="Business Ops." />
                        <uc:Label id="lblSIC" runat="server" field="Insured.HtmlSIC" Name="SIC" />
                    </table>
                </td>
                <td width="10px" align="center"><img class="sep" src="<%= AppPath %>/images/black_pixel.gif" height="110px" width="1px"></td>
                <td valign="top">
                    <table class="fields">
                        <uc:Label id="lblUnderwriter" runat="server" field="Survey.HtmlUnderwriterName" Name="Underwriter" />
                        <uc:Label id="lblUnderwriterPhone" runat="server" field="Survey.HtmlUnderwriterPhone" Name="Underwriter Phone" />
                        <tr>
                            <td class="label" valign="top" nowrap>Account Status</td>
                            <td class="lbl"><%= GetAccountStatus(_eSurvey)%></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </cc:Box>
    
    <hr class="boxBreak" />
    
    <cc:Box id="boxContacts" runat="server" Title="Contacts" width="700">
        <% if (repContacts.Items.Count > 0) { %>
        <asp:Repeater ID="repContacts" Runat="server">
            <HeaderTemplate>
                <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0">
                    <tr>
                        <td nowrap><u>Name</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Title</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Phone</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Alt Phone</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Fax</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Email</u></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Name", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Title", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px" nowrap><%# HtmlEncode(Container.DataItem, "HtmlPhoneNumber")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncode(Container.DataItem, "HtmlAltPhoneNumber")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncode(Container.DataItem, "HtmlFaxNumber")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "EmailAddress", string.Empty, "(none)")%></td>
                </tr>			
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <%} else {%>
        <div style="PADDING-LEFT: 10px">
            (none)
        </div>
        <% } %>
    </cc:Box>
    
    <hr class="boxBreak" />
    
    <cc:Box id="boxAgency" runat="server" Title="Agency" width="700">
        <table>
            <tr>
                <td valign="top" width="350px">
                    <table class="fields">
                        <uc:Label id="lblAgencyNumber" runat="server" field="Agency.Number" />
                        <uc:Label id="lblAgencyName" runat="server" field="Agency.Name" />
                        <uc:Label id="lblAgencyAddress" runat="server" field="Agency.HtmlAddress" Name="Address" />
                    
                    </table>
                </td>
                <td width="10px" align="center"><img class="sep" src="<%= AppPath %>/images/black_pixel.gif" height="90px" width="1px"></td>
                <td valign="top">
                    <table class="fields">
                        <uc:Label id="lblAgencyPhone" runat="server" field="Agency.HtmlPhoneNumber" Name="Phone Number" />
                        <uc:Label id="lblAgencyFax" runat="server" field="Agency.HtmlFaxNumber" Name="Fax Number" />
                        <uc:Label id="lblAgentContactName" runat="server" field="Insured.AgentContactName" Name="Agent Name" />
                        <uc:Label id="lblAgentContactPhoneNumber" runat="server" field="Insured.AgentContactPhoneNumber" Name="Agent Phone" />
                        <uc:Label id="lblAgentContactEmail" runat="server" field="Insured.HtmlAgentContactEmail" Name="Agent Email" />
                    </table>
                </td>
            </tr>
        </table>
    </cc:Box>
    
    <hr class="boxBreak" />
    
    <cc:Box id="boxPolicySummary" runat="server" Title="Policies" width="700">
        <% if (repPolicySummary.Items.Count > 0) { %>
            <asp:Repeater ID="repPolicySummary" Runat="server">
                <HeaderTemplate>
                    <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0">
                        <tr>
                            <td nowrap><u>Policy #</u></td>
                            <td style="PADDING-LEFT: 15px"><u>Mod</u></td>
                            <td style="PADDING-LEFT: 15px"><u>Symbol</u></td>
                            <td style="PADDING-LEFT: 15px"><u>LOB</u></td>
                            <td style="PADDING-LEFT: 15px"><u>Hazard Grade</u></td>
                            <td style="PADDING-LEFT: 15px"><u>Effective</u></td>
                            <td style="PADDING-LEFT: 15px"><u>Expiration</u></td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Mod", Int32.MinValue, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Symbol", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "LineOfBusiness.Name", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "HazardGrade", string.Empty, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "EffectiveDate", "{0:d}", DateTime.MinValue, "(none)")%></td>
                        <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "ExpireDate", "{0:d}", DateTime.MinValue, "(none)")%></td>
                    </tr>			
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        <%} else {%>
        <div style="PADDING-LEFT: 10px">
            (none)
        </div>
        <% } %>   
    </cc:Box>
    
    <hr class="boxBreak" />
    
    <cc:Box id="boxPolicies" runat="server" Title="Policies/Coverages" width="700">
        <asp:Repeater ID="repPolicies" Runat="server">
        <ItemTemplate>
            <a href="javascript:Toggle('<%# DataBinder.Eval(Container.DataItem, "ID") %>')"><%# GetSinglePolicyLine(Container.DataItem) %></a>
            <div id="<%# HtmlEncode(Container.DataItem, "ID") %>" style="MARGIN-TOP: 10px;">
                <table>
                    <%if (!SurveyTypeType.IsNotWrittenBusiness(_eSurvey.Type.Type)) {%>
                    <tr><td><u><b>Policy Details</b></u></td></tr>
                    <tr><td>
                        <table class="fields">
                            <tr>
                                <td class="label" valign="top" nowrap>Policy Number</td>
                                <td><%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(not specified)")%></td>
                            </tr>
                            <tr>
                                <td class="label" valign="top" nowrap>Policy Mod</td>
                                <td><%# HtmlEncodeNullable(Container.DataItem, "Mod", Int32.MinValue, "(not specified)")%></td>
                            </tr>
                            <tr>
                                <td class="label" valign="top" nowrap>Line of Business</td>
                                <td><%# HtmlEncodeNullable(Container.DataItem, "LineOfBusiness.Name", string.Empty, "(not specified)")%></td>
                            </tr>
                            <tr>
                                <td class="label" valign="top" nowrap>Effective Date</td>
                                <td><%# HtmlEncodeNullable(Container.DataItem, "EffectiveDate", "{0:d}", DateTime.MinValue, "(not specified)")%></td>
                            </tr>
                            <tr>
                                <td class="label" valign="top" nowrap>Expiration Date</td>
                                <td><%# HtmlEncodeNullable(Container.DataItem, "ExpireDate", "{0:d}", DateTime.MinValue, "(not specified)")%></td>
                            </tr>
                            <tr>
                                <td class="label" valign="top" nowrap>Hazard Grade</td>
                                <td><%# HtmlEncodeNullable(Container.DataItem, "HazardGrade", string.Empty, "(not specified)")%></td>
                            </tr>
                            <tr>
                                <td class="label" valign="top" nowrap>Branch Code</td>
                                <td><%# HtmlEncodeNullable(Container.DataItem, "BranchCode", string.Empty, "(not specified)") %></td>
                            </tr>
                            <tr>
                                <td class="label" valign="top" nowrap>Carrier</td>
                                <td><%# HtmlEncodeNullable(Container.DataItem, "Carrier", string.Empty, "(not specified)") %></td>
                            </tr>
                            <tr>
                                <td class="label" valign="top" nowrap>ProfitCenter</td>
                                <td><%# GetProfitCenterName(Container.DataItem) %></td>
                            </tr>
                        </table>
                        </td></tr>
                        <%} %>
                        <tr><td><u><b>Coverages</b></u></td></tr>
                        <tr><td>
                        
                        <asp:Repeater ID="repCoverages" DataSource="<%# GetCoveragesForPolicy(Container.DataItem) %>" Runat="server" OnItemCreated="repCoverages_ItemCreated">
                            <ItemTemplate>
                                    <tr id="rowCoverageName" runat="server">
                                        <td class="label" valign="top" nowrap><b><%# HtmlEncode(Container.DataItem, "DisplayCoverageName")%></b></td>
                                        <td><%# HtmlEncode(Container.DataItem, "DisplayReport")%></td>
                                    </tr>
                                    <tr>
                                        <asp:Repeater ID="repCoverageValues" Runat="server" DataSource="<%# GetValuesForCoverage(Container.DataItem) %>">
                                            <ItemTemplate>
                                                <tr id="rowCoverageValue" runat="server">
                                                    <td class="label" valign="top" nowrap><%# GetEncodedName(Container.DataItem) %></td>
                                                    <td><%# GetEncodedValue(Container.DataItem) %></td>
                                                    <td><%# GetCoverageDetails(Container.DataItem)%></td>  
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                            </ItemTemplate>
                            <SeparatorTemplate>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </SeparatorTemplate>
                            <HeaderTemplate>
                                <table class="fields">
                            </HeaderTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                            </asp:Repeater>
                            </td></tr>
                        </table>
                    </div>
                </ItemTemplate>
                <SeparatorTemplate>
                    <br>
                    <hr>
                </SeparatorTemplate>
                </asp:Repeater>
    </cc:Box>
    
    <hr class="boxBreak" />
    
    <cc:Box id="boxBuildings" runat="server" Title="Locations/Buildings" width="700">
    <asp:Repeater ID="repLocations" Runat="server">
        <HeaderTemplate>
            <table>
        </HeaderTemplate>
        <FooterTemplate>
           </table> 
        </FooterTemplate>
        <ItemTemplate>
            <tr>
                <td valign="top" nowrap><b><%# GetLocationDetails(Container.DataItem) %></b></td>
            </tr>
            <tr>
                <td>
                    <asp:Repeater ID="repBuildings" DataSource="<%# GetBuildingsForLocation(Container.DataItem) %>" Runat="server">
                        <HeaderTemplate>
                            <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0">
                                <tr>
                                    <td>Building<br /><u>Number</u></td>
                                    <% if (CurrentUser.Company.ID != Company.BerkleyLifeSciences.ID) { %>
                                    <td style="PADDING-LEFT: 15px">Year<br /><u>Built</u></td>
                                    <td style="PADDING-LEFT: 15px">Sprinkler<br /><u>System</u></td>
                                    <td style="PADDING-LEFT: 15px">Public<br /><u>Protection</u></td>
                                    <% } %>

                                    <td style="PADDING-LEFT: 15px">Building<br /><u>Value</u></td>
                                    <td style="PADDING-LEFT: 15px">Building<br /><u>Contents</u></td>
                                    <td style="PADDING-LEFT: 15px">Stock<br /><u>Values</u></td>
                                    <td style="PADDING-LEFT: 15px">Business<br /><u>Interruption</u></td>
                                    <td style="PADDING-LEFT: 15px">Location<br /><u>Occupancy</u></td>

                                    <% if (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID) { %>
                                    <td style="PADDING-LEFT: 15px"><u>Change in Env. Control</u></td>
                                    <td style="PADDING-LEFT: 15px"><u>Scientific Animals</u></td>
                                    <td style="PADDING-LEFT: 15px"><u>Contamination</u></td>
                                    <td style="PADDING-LEFT: 15px"><u>Radio Active Contamination</u></td>
                                    <td style="PADDING-LEFT: 15px"><u>Flood</u></td>
                                    <td style="PADDING-LEFT: 15px"><u>Earthquake</u></td>
                                    <% } %>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Number", string.Empty, "(none)")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "YearBuilt", Int32.MinValue, "(none)")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncode(Container.DataItem, "HtmlHasSprinklerSystemNone")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "PublicProtection", Int32.MinValue, "(none)")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Value", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Contents", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "StockValues", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "BusinessInterruption", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "LocationOccupancy", string.Empty, "(none)")%></td>

                                <% if (CurrentUser.Company.ID == Company.BerkleyLifeSciences.ID) { %>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "ChangeInEnvironmentalControl", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "ScientificAnimals", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Contamination", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "RadioActiveContamination", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Flood", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Earthquake", "{0:C0}", decimal.MinValue, "(none)")%></td>
                                <% } %>
                            </tr>			
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>			
        </ItemTemplate>
    </asp:Repeater>
    </cc:Box>
    
    <% if (CurrentUser.Company.ImportRateModFactors) { %>
    <hr class="boxBreak" />
    <cc:Box id="boxRateModFactors" runat="server" title="Rate Modification Factors" width="700">
        <% if (repRateModFactors.Items.Count > 0) { %>
        <asp:Repeater ID="repRateModFactors" Runat="server">
            <HeaderTemplate>
                <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0">
                    <tr>
                        <td nowrap><u>Policy #</u></td>
                        <td style="PADDING-LEFT: 20px" nowrap><u>Mod</u></td>
                        <td style="PADDING-LEFT: 20px" nowrap><u>State</u></td>
                        <td style="PADDING-LEFT: 20px" nowrap><u>Description</u></td>
                        <td style="PADDING-LEFT: 20px" nowrap><u>Rate</u></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Policy.Number", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 20px" nowrap><%# HtmlEncodeNullable(Container.DataItem, "Policy.Mod", Int32.MinValue, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 20px" nowrap><%# HtmlEncodeNullable(Container.DataItem, "State.Name", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "Type.Description", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 20px"><%# HtmlEncodeNullable(Container.DataItem, "Rate", "{0:N3}", decimal.MinValue, "(none)")%></td>
                </tr>			
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <%} else {%>
        <div style="PADDING-LEFT: 10px">
            (none)
        </div>
        <% } %>
    </cc:Box>
    <% } %>
    
    <hr class="boxBreak" />
    
    <cc:Box id="boxClaim" runat="server" title="Claims" width="700" >
        <% if (repClaims.Items.Count > 0) { %>

        <% if (_disclaimerMessage.Length > 0){ %>
            <b> Disclaimer: <%= _disclaimerMessage %></b>
        <% } %>

        <asp:Repeater ID="repClaims" Runat="server">
            <HeaderTemplate>
                <table style="MARGIN: 5px 0px 0px 20px" cellspacing="0">
                    <tr>
                        <td nowrap><u>Policy #</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Sym</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Policy Exp</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Loss Date</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Claimant</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Status</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Loss Type</u></td>
                        <td style="PADDING-LEFT: 15px" nowrap><u>Claim Comment</u></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td valign="top" nowrap><%# HtmlEncodeNullable(Container.DataItem, "PolicyNumber", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "PolicySymbol", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px" nowrap><%# HtmlEncodeNullable(Container.DataItem, "PolicyExpireDate", "{0:d}", DateTime.MinValue, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px" nowrap><%# HtmlEncodeNullable(Container.DataItem, "LossDate", "{0:d}", DateTime.MinValue, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Claimant", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Status", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "LossType", string.Empty, "(none)")%></td>
                    <td valign="top" style="PADDING-LEFT: 15px"><%# HtmlEncodeNullable(Container.DataItem, "Comment", string.Empty, "(none)")%></td>
                </tr>			
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <%} else {%>
        <div style="PADDING-LEFT: 10px">
            (none)
        </div>
        <% } %>
    </cc:Box>
    
    <hr class="boxBreak" />
    
    <div class="surveyDetails">
    <cc:Box id="boxNotes" runat="server" title="Comments" width="100%">
        <% if( repNotes.Items.Count > 0 ) { %>
        <asp:Repeater ID="repNotes" Runat="server">
            <HeaderTemplate>
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="BORDER-BOTTOM: solid 1px black">
                        <%# GetUserName(DataBinder.Eval(Container.DataItem, "User")) %> - 
                        <%# HtmlEncode(Container.DataItem, "EntryDate", "{0:g}") %>
                    </td>
                </tr>
                <tr>
                    <td style="PADDING-BOTTOM: 10px">
                        <%# GetEncodedComment(Container.DataItem) %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <% } else { %>
        <div style="PADDING-BOTTOM: 10px">
            (none)
        </div>
        <% } %>
    </cc:Box>
    </div>

    <uc:Footer ID="ucFooter" runat="server" />
    
    <script language="javascript" type="text/javascript" src="<%= AppPath %>/common.js"></script>
    <script language="javascript">
    function Toggle(id)
    {
        ToggleControlDisplay(id);
    }
    </script>
    </form>
    </div>
</body>
</html>
