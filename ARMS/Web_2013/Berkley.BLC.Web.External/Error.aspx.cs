﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web;
using Wilson.ORMapper;

namespace Berkley.BLC.Web.External
{
    public partial class ErrorAspx : AppBasePage
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(ErrorLinksAspx_Load);
        }
        #endregion

        void ErrorLinksAspx_Load(object sender, EventArgs e)
        {
            boxError.Title = string.Format("Occured on {0}", DateTime.Now);

            if (Request.QueryString["msg"] != null && !string.IsNullOrEmpty(Request.QueryString["msg"]))
            {
                lblErrorDetails.Text = Request.QueryString["msg"].ToString();
            }
        }
    }
}
