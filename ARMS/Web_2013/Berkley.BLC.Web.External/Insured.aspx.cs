using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using Berkley.BLC.Entities;
using QCI.Web;
using QCI.Web.Validation;

namespace Berkley.BLC.Web.External
{
    public partial class InsuredAspx : AppBasePage
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(InsuredAspx_Load);
            this.PreRender += new EventHandler(InsuredAspx_PreRender);
            this.repLocations.ItemCreated += new RepeaterItemEventHandler(repLocations_ItemCreated);
            this.cboLocations.SelectedIndexChanged += new EventHandler(cboLocations_SelectedIndexChanged);
        }
        #endregion

        protected Survey _eSurvey;
        protected Location _eLocation;
        protected string _disclaimerMessage;

        void InsuredAspx_Load(object sender, EventArgs e)
        {
            Guid gSurveyID = Utility.GetGuidFromQueryString("surveyid", true);
            _eSurvey = Survey.Get(gSurveyID);

            Guid gLocationID = Utility.GetGuidFromQueryString("locationid", false);
            _eLocation = (gLocationID != Guid.Empty) ? Location.Get(gLocationID) : _eSurvey.Location;

            // populate all our static fields
            this.PopulateFields(_eSurvey);
            this.PopulateFields(_eSurvey.Status);
            this.PopulateFields(_eSurvey.Insured);
            this.PopulateFields(_eSurvey.Insured.Agency);

            // populate statics that could be null
            if (_eSurvey.AssignedUser != null)
                this.PopulateFields(_eSurvey.AssignedUser);
            else
                lblAssignedUser.Text = "(not specified)";

            if (_eSurvey.Type != null)
                this.PopulateFields(_eSurvey.Type.Type);
            else
                this.lblType.Text = "(not specified)";

            if (!this.IsPostBack)
            {
                // populate the locations
                Location[] locations = Location.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && Active = true]", "Number ASC", _eSurvey.ID);
                ComboHelper.BindCombo(cboLocations, locations, "ID", "SingleLineAddressWithNumber");
                cboLocations.SelectedValue = _eLocation.ID.ToString();

                lblHeadingForPrint.Visible = !CurrentUser.Company.DefaultLocationsToOneSurvey;
            }
        }

        void InsuredAspx_PreRender(object sender, EventArgs e)
        {
            Policy[] activePolicies = Policy.GetSortedArray("SurveyPolicyCoverages[SurveyID = ? && LocationID = ? && Active = true]", "Number ASC", _eSurvey.ID, _eLocation.ID);
            repPolicies.DataSource = activePolicies;
            repPolicies.DataBind();

            repPolicySummary.DataSource = activePolicies;
            repPolicySummary.DataBind();

            LocationContact[] eContacts = LocationContact.GetSortedArray("LocationID = ?", "PriorityIndex ASC", _eLocation.ID);
            repContacts.DataSource = eContacts;
            repContacts.DataBind();

            Claim[] eClaims = Claim.GetSortedArray("Policy[InsuredID = ?]", "LossDate DESC", _eSurvey.InsuredID);
            repClaims.DataSource = eClaims;
            repClaims.DataBind();

            SurveyNote[] notes = SurveyNote.GetSortedArray("SurveyID = ? && InternalOnly = false", "EntryDate DESC", _eSurvey.ID);
            repNotes.DataSource = notes;
            repNotes.DataBind();

            Location[] activeLocations = Location.GetSortedArray("SurveyPolicyCoverages[SurveyID = ?]", "Number ASC", _eSurvey.ID);
            repLocations.DataSource = activeLocations;
            repLocations.DataBind();

            RateModificationFactor[] rateModFactors = RateModificationFactor.GetSortedArray("Policy[InsuredID = ?]", "Policy.Number ASC, Policy.Mod ASC, StateCode ASC, TypeCode ASC", _eSurvey.InsuredID);
            repRateModFactors.DataSource = rateModFactors;
            repRateModFactors.DataBind();

            repClaims.Visible = (CurrentUser.Company.DisplayClaimsByDefault) ? true : _eSurvey.DisplayClaims;

            // Retrive Disclaimer message
            _disclaimerMessage = Berkley.BLC.Business.Utility.GetCompanyParameter("DisclaimerMessage", CurrentUser.Company).ToString();

        }

        void cboLocations_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("Insured.aspx{0}&locationid={1}", Utility.GetQueryStringForNav("locationid"), cboLocations.SelectedValue), true);
        }

        protected string GetSinglePolicyLine(object dataItem)
        {
            Policy ePolicy = (Policy)dataItem;
            return string.Format("Policy #{0}-{1}: {2}", ePolicy.Number, ePolicy.Mod, ePolicy.LineOfBusiness.Name);
        }

        protected Policy _currentPolicy;
        protected CustomCoverageName[] GetCoveragesForPolicy(object dataItem)
        {
            Policy ePolicy = (Policy)dataItem;
            _currentPolicy = ePolicy;

            VWExistingCoverage[] coverages = VWExistingCoverage.GetSortedArray("SurveyID = ? && LocationID = ? && PolicyID = ? && ReportTypeCode = ?", "CoverageNameDisplayOrder DESC, CoverageValueDisplayOrder ASC", _eSurvey.ID, _eLocation.ID, ePolicy.ID, ReportType.Report.Code);

            SortedList list = new SortedList();
            foreach (VWExistingCoverage coverage in coverages)
            {
                CustomCoverageName customCoverageName;
                if (!list.ContainsKey(coverage.CoverageNameDisplayOrder))
                {
                    customCoverageName = new CustomCoverageName(coverage.CoverageNameTypeName, coverage.ReportTypeName);
                }
                else
                {
                    customCoverageName = list[coverage.CoverageNameDisplayOrder] as CustomCoverageName;
                }

                if (!coverage.CoverageNameIsRequired)
                {
                    CustomCoverage customCoverage = new CustomCoverage(coverage.CoverageID, coverage.CoverageTypeName, coverage.CoverageValue, coverage.DotNetDataType);
                    customCoverageName.CustomCoverages.Add(customCoverage);
                }

                list.Remove(coverage.CoverageNameDisplayOrder);
                list.Add(coverage.CoverageNameDisplayOrder, customCoverageName);
            }

            CustomCoverageName[] results = new CustomCoverageName[list.Count];
            list.Values.CopyTo(results, 0);

            return results;
        }

        protected CustomCoverage[] GetValuesForCoverage(object dataItem)
        {
            return (dataItem as CustomCoverageName).CustomCoverages.ToArray();
        }

        protected int _buildingCount = 1;
        protected string SetBuildingCount(object dataItem)
        {
            _buildingCount = GetBuildingsForLocation(dataItem).Length;
            return string.Empty;
        }

        protected string GetCoverageDetails(object dataItem)
        {
            CustomCoverage myCustomeCoverage = (CustomCoverage)dataItem;
            CoverageDetail[] mydetails = GetCoverageDetailsForPolicy(dataItem);

            if (mydetails.Length > 0)
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<tr id='rowCoverageDetails' runat='server'>");
                sb.AppendLine("        <td></td>");
                sb.AppendLine("        <td>");
                sb.AppendLine("           <table");
                sb.AppendLine("             <asp:Repeater ID=\"repCoverageDetails\" runat=\"server\"  DataSource='" + mydetails + "'>");
                sb.AppendLine("                 <ItemTemplate>");
                sb.AppendLine("                     <tr id=\"rowCoverageDetailsValue\"> ");
                sb.AppendLine("                         <td> </td>");
                sb.AppendLine("                         <td class=\"lbl\" valign=\"top\"  nowrap style=\"PADDING-RIGHT: 1px\">");

                for (int i = 0; i < mydetails.Length; i++)
                {
                    sb.Append(GetEncodedCoverageDetailValue(mydetails[i]) + "<br/>");
                }

                sb.AppendLine("                         </td>");
                sb.AppendLine("                     </tr>");
                sb.AppendLine("                 </ItemTemplate>");
                sb.AppendLine("             </asp:Repeater>");
                sb.AppendLine("           </table>");
                sb.AppendLine("        </td>");
                sb.AppendLine("     </tr>");

                return sb.ToString();
            }
            else
            {
                return string.Empty;
            }


        }
        
        protected CoverageDetail[] GetCoverageDetailsForPolicy(object dataItem)
        {
            CustomCoverage eCoverage = (CustomCoverage)dataItem;

            CoverageDetail[] coverageDetails = CoverageDetail.GetSortedArray("CoverageID = ?", "Value ASC", eCoverage.CoverageID);
            return coverageDetails;
        }
        
        protected string GetEncodedCoverageDetailValue(object dataItem)
        {

            string result = (dataItem as CoverageDetail).Value;

            // html encode the string (must do this first)
            result = Server.HtmlEncode(result);
            result = result.Replace("|", "<br>");

            return result;
        }


        protected Building[] GetBuildingsForLocation(object dataItem)
        {
            Location location = (Location)dataItem;
            return Building.GetSortedArray("LocationID = ?", "Number ASC", location.ID);
        }

        protected string GetAccountStatus(Survey survey)
        {
            return (survey.Insured.NonrenewedDate == DateTime.MinValue) ? "Active" : "Non-Renewed";
        }

        private HtmlTableRow rowCoverageName;

        protected void repCoverages_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            // we only care about datagrid items
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) && e.Item.DataItem != null)
            {
                //Event Handler
                rowCoverageName = (HtmlTableRow)e.Item.FindControl("rowCoverageName");

                //Determine if we should gray out the non reported coverage
                CustomCoverageName customCoverageName = (CustomCoverageName)e.Item.DataItem;
                if (customCoverageName.DisplayReport.ToUpper() == "NO REPORT")
                {
                    rowCoverageName.Style.Add("color", "gray");
                }

                Repeater repCoverageValues = (Repeater)e.Item.FindControl("repCoverageValues");
                repCoverageValues.ItemCreated += new RepeaterItemEventHandler(repCoverageValues_ItemCreated);
            }
        }

        void repCoverageValues_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            // we only care about datagrid items
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Event Handler
                HtmlTableRow rowCoverageValue = (HtmlTableRow)e.Item.FindControl("rowCoverageValue");
                rowCoverageValue.Style.Value = rowCoverageName.Style.Value;
            }
        }

        protected string GetEncodedName(object dataItem)
        {
            string result = (dataItem as CustomCoverage).DisplayCoverageType;

            // html encode the string (must do this first)
            result = Server.HtmlEncode(result);

            // convert '|' characters
            result = result.Replace("|", "<br>");

            return result;
        }

        protected string GetEncodedValue(object dataItem)
        {
            string result = "(not specified)";
            CustomCoverage eCoverage = (dataItem as CustomCoverage);
            string sValue = eCoverage.DisplayCoverageValue;

            if (sValue != null && sValue.Length > 0)
            {
                // covert to type

                try
                {
                    switch (eCoverage.DotNetDataType)
                    {
                        case "DateTime":
                            sValue = Convert.ToDateTime(sValue).ToString("d");
                            break;
                        case "Decimal":
                            sValue = Convert.ToDecimal(sValue).ToString("C0");
                            break;
                        case "Int32":
                            sValue = Convert.ToInt32(sValue).ToString("N0");
                            break;
                        default:
                            //do nothing
                            break;
                    }
                }
                catch (FormatException)
                {
                    sValue = sValue.Trim();
                }

                // html encode the string (must do this first)
                result = Server.HtmlEncode(sValue);

                // convert '|' characters
                if (result.Contains("|"))
                {
                    string[] strArray = result.Split('|');
                    string strBuilder = string.Empty;
                    foreach (string s in strArray)
                    {
                        strBuilder += (s != null && s.Length > 0) ? s + "<br>" : "(not specified)<br>";
                    }
                    result = strBuilder + "<br>";
                }
            }

            return result;
        }

        protected string GetBoolLabel(string value)
        {
            Int16 i = Convert.ToInt16(value);
            string result;

            if (i == 0)
                result = "No";
            else if (i == 1)
                result = "Yes";
            else
                result = "(none)";

            return result;
        }

        protected string GetUserName(object obj)
        {
            User user = (obj as User);
            return Server.HtmlEncode((user != null) ? user.Name : _eSurvey.CreateByExternalUserName);
        }

        protected string GetProfitCenterName(object obj)
        {
            Policy policy = (obj as Policy);
            ProfitCenter pr = ProfitCenter.GetOne("Code = ? && CompanyID = ?", policy.ProfitCenter, CurrentUser.CompanyID);

            if (pr != null)
            {
                return pr.Name;
            }
            else
            {
                return "(not specified)";
            }
        }

        protected string GetLocationDetails(object dataItem)
        {
            Location location = (Location)dataItem;

            string result = HtmlEncodeNullable(dataItem, "SingleLineAddressWithNumber", string.Empty, "(none)");
            if (location.Description.Length > 0)
            {
                result += string.Format(" - ({0})", location.Description);
            }

            return result;
        }

        protected string GetEncodedComment(object dataItem)
        {
            string comment = (dataItem as SurveyNote).Comment;
            bool internalOnly = (dataItem as SurveyNote).InternalOnly;

            // html encode the string (must do this first)
            comment = Server.HtmlEncode(comment);

            // convert CR, LF, and TAB characters
            comment = comment.Replace("\r\n", "<br>");
            comment = comment.Replace("\r", "<br>");
            comment = comment.Replace("\n", "<br>");
            comment = comment.Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");

            return comment;
        }

        void repLocations_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            // we only care about datagrid items
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Event Handler nested repeater
                Repeater repBuildings = (Repeater)e.Item.FindControl("repBuildings");
                repBuildings.ItemDataBound += new RepeaterItemEventHandler(repBuildings_ItemDataBound);
            }
        }

        void repBuildings_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater repBuildings = (Repeater)sender;
            Building[] buildings = repBuildings.DataSource as Building[];
            repBuildings.FooterTemplate = new MyTemplate(buildings.Length > 0);

            if (buildings.Length <= 0 && e.Item.ItemType != ListItemType.Footer)
            {
                 e.Item.Visible = false;
            }
        }

        private class MyTemplate : ITemplate
        {
            private bool _showDefaultFooter;
            public MyTemplate(bool showDefaultFooter) 
            {
                _showDefaultFooter = showDefaultFooter;
            }

            public void InstantiateIn(System.Web.UI.Control container)
            {
                Literal l = new Literal();
                if (!_showDefaultFooter)
                {
                    l.Text = "<div style='PADDING-LEFT: 15px'>(no buildings)</div>";
                }
                else
                {
                    l.Text = "</table>";
                }
                container.Controls.Add(l);
            }
        }

        protected class CustomCoverage
        {
            private string _displayCoverageType = string.Empty;
            private string _displayCoverageValue = string.Empty;
            private string _dotNetDataType = string.Empty;
            private Guid _coverageID = Guid.Empty;

            public CustomCoverage(Guid coverageID, string displayCoverageType, string displayCoverageValue, string dotNetDataType)
            {
                _coverageID = coverageID;
                _displayCoverageType = displayCoverageType;
                _displayCoverageValue = displayCoverageValue;
                _dotNetDataType = dotNetDataType;
            }
            public Guid CoverageID
            {
                get { return _coverageID; }
            }

            public string DisplayCoverageType
            {
                get { return _displayCoverageType; }
            }

            public string DisplayCoverageValue
            {
                get { return _displayCoverageValue; }
            }

            public string DotNetDataType
            {
                get { return _dotNetDataType; }
            }
        }

        protected class CustomCoverageName
        {
            private string _displayCoverageName = string.Empty;
            private string _displayReport = string.Empty;
            private List<CustomCoverage> _customCoverages = new List<CustomCoverage>();

            public CustomCoverageName(string displayCoverageName, string displayReport)
            {
                _displayCoverageName = displayCoverageName;
                _displayReport = displayReport;
            }

            public string DisplayCoverageName
            {
                get { return _displayCoverageName; }
            }

            public string DisplayReport
            {
                get { return _displayReport; }
            }

            public List<CustomCoverage> CustomCoverages
            {
                get { return _customCoverages; }
            }
        }
    }
}
