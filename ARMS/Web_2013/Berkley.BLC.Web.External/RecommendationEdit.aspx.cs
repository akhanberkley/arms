using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web.Validation;

namespace Berkley.BLC.Web.External
{

    public partial class RecommendationEditAspx : AppBasePage
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(RecommendationEditAspx_Load);
            this.btnSubmit.Click += new EventHandler(btnSubmit_Click);
            this.btnCancel.Click += new EventHandler(btnCancel_Click);
        }
        #endregion

        protected SurveyRecommendation _eRec;
        protected Survey _eSurvey;

        void RecommendationEditAspx_Load(object sender, EventArgs e)
        {
            // register our AJAX class for use in client script
            Ajax.Utility.RegisterTypeForAjax(typeof(AjaxMethods));
            cboRecCode.Attributes.Add("OnChange", "GetDescription(this.value);");

            Guid gSurveyRecID = Utility.GetGuidFromQueryString("surveyrecid", false);
            _eRec = (gSurveyRecID != Guid.Empty) ? SurveyRecommendation.Get(gSurveyRecID) : null;

            Guid gSurveyID = Utility.GetGuidFromQueryString("surveyid", true);
            _eSurvey = Survey.Get(gSurveyID);

            if (!this.IsPostBack)
            {
                // populate the rec list with all rec codes and descriptions
                Recommendation[] eRecs = Recommendation.GetSortedArray("CompanyID = ? && Disabled = false", "Code ASC", this.CurrentUser.CompanyID);
                cboRecCode.DataBind(eRecs, "ID", "{0} - {1}", "Code|Title".Split('|'));

                // populate the company specific types
                RecClassification[] recClassifications = RecClassification.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
                cboRecClass.DataBind(recClassifications, "ID", "Type.Name");
                cboRecClass.Visible = recClassifications.Length > 0;

                RecStatus[] recStatuses = RecStatus.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
                cboRecStatus.DataBind(recStatuses, "ID", "Type.Name");

                if (_eRec != null) // edit
                {
                    this.PopulateFields(_eRec);
                    this.PopulateFields(_eRec.Recommendation);
                }
                else
                {
                    lblDateCreated.Text = DateTime.Today.ToShortDateString();
                    lblDateCompleted.Text = "(not specified)";
                    cboRecStatus.SelectedValue = RecStatusType.Open.Code;
                }

                btnSubmit.Text = (_eRec == null) ? "Add" : "Update";
                base.PageHeading = (_eRec == null) ? "Add Recommendation" : "Update Recommendation";
            }
        }

        void btnSubmit_Click(object sender, EventArgs e)
        {
            // double check state of validators (in case browser is not uplevel)
            if (!this.IsValid)
            {
                ShowValidatorErrors();
                return;
            }

            try
            {
                SurveyRecommendation eRec;
                if (_eRec == null) // add
                {
                    eRec = new SurveyRecommendation(Guid.NewGuid());
                    eRec.SurveyID = _eSurvey.ID;

                    //get the highest number and incriment by one
                    SurveyRecommendation[] eRecs = SurveyRecommendation.GetSortedArray(_eSurvey.GetAllSurveysForAccount, "PriorityIndex DESC");
                    eRec.PriorityIndex = (eRecs.Length > 0) ? eRecs[0].PriorityIndex + 1 : 1;
                }
                else
                {
                    eRec = _eRec.GetWritableInstance();
                }

                eRec.RecommendationID = new Guid(cboRecCode.SelectedValue);
                eRec.RecommendationNumber = txtRecNumber.Text;
                eRec.EntryText = txtRecDescription.Text;
                eRec.Comments = txtComments.Text;
                eRec.RecClassificationID = (cboRecClass.SelectedValue != string.Empty) ? new Guid(cboRecClass.SelectedValue) : Guid.Empty;
                eRec.DateCreated = (eRec.DateCreated == DateTime.MinValue) ? DateTime.Today : eRec.DateCreated;
                eRec.RecStatusID = (cboRecStatus.SelectedValue != string.Empty) ? new Guid(cboRecStatus.SelectedValue) : Guid.Empty;

                //Set the complete date
                if (eRec.DateCompleted == DateTime.MinValue && eRec.RecStatus.TypeCode == RecStatusType.Completed.Code)
                {
                    eRec.DateCompleted = DateTime.Today;
                }
                else if (eRec.DateCompleted != DateTime.MinValue && eRec.RecStatus.TypeCode != RecStatusType.Completed.Code)
                {
                    eRec.DateCompleted = DateTime.MinValue;
                }
                else
                {
                    eRec.DateCompleted = eRec.DateCompleted;
                }

                // commit changes
                eRec.Save();

                //Update the survey history
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser, true);
                manager.RecManagement(eRec, (_eRec == null) ? SurveyManager.UIAction.Add : SurveyManager.UIAction.Update);

                // update our member var so the redirect will work correctly
                _eRec = eRec;
            }
            catch (ValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }
            catch (FieldValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }

            Response.Redirect(string.Format("Recommendations.aspx{0}", Utility.GetQueryStringForNav("surveyrecid")), true);
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("Recommendations.aspx{0}", Utility.GetQueryStringForNav("surveyrecid")), true);
        }

        public class AjaxMethods
        {
            [Ajax.AjaxMethod()]
            public static string GetDescription(string id)
            {
                Recommendation eRec = Recommendation.Get(new Guid(id));
                return eRec.Description;
            }
        }
    }
}
