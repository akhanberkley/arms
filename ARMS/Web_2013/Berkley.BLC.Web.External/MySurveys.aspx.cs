using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using Berkley.BLC.Entities;
using QCI.Web;
using QCI.Web.Validation;

namespace Berkley.BLC.Web.External
{
    public partial class MySurveysAspx : AppBasePage
    {
		protected string _itemClass; // used during HTML rendering
		private User _activeUser;
		protected Guid _feeCompanyID;
		protected int _assignedCount;
        protected int _completed;

		protected int miNewWork;
		protected int miCorrections;
		protected int miUpdated;
		protected int miReturnedToCompany;
        protected bool _vendorHideReturntoCompany = false;
        protected bool _vendorHideCompletedPast6Months = false;

		#region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(MySurveysAspx_Load);
        }
        #endregion

        void MySurveysAspx_Load(object sender, EventArgs e)
        {
			string strSelect;

            _vendorHideCompletedPast6Months = (Berkley.BLC.Business.Utility.GetCompanyParameter("VendorHideCompletedPast6Months", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
            _vendorHideReturntoCompany = (Berkley.BLC.Business.Utility.GetCompanyParameter("VendorHideReturntoCompany", CurrentUser.Company).ToUpper() == "TRUE") ? true : false;
                        
            // Put user code to initialize the page here
			this.PageHeading = "Vendor Assignments";
			_activeUser = this.CurrentUser;

			// All Assigned
			strSelect = String.Format("SELECT Count(*) from Survey where CompanyID='{0}' AND AssignedUserID='{1}' AND SurveyStatusCode='{2}'", _activeUser.CompanyID, _activeUser.ID, SurveyStatus.AssignedSurvey.Code);
			_assignedCount = (int) DB.Engine.ExecuteScalar(strSelect);
			_feeCompanyID = _activeUser.ID;

			// New Work
			strSelect = String.Format("SELECT Count(*) from Survey where CompanyID='{0}' AND AssignedUserID='{1}' AND (Correction=0 OR Correction is NULL) AND SurveyStatusCode='{2}'", _activeUser.CompanyID, _activeUser.ID, SurveyStatus.AwaitingAcceptance.Code);
			miNewWork = (int)DB.Engine.ExecuteScalar(strSelect);

			// Corrections
			strSelect = String.Format("SELECT Count(*) from Survey where CompanyID='{0}' AND AssignedUserID='{1}' AND Correction=1 AND (SurveyStatusCode='{2}' OR SurveyStatusCode='{3}')", _activeUser.CompanyID, _activeUser.ID, SurveyStatus.AssignedSurvey.Code, SurveyStatus.AwaitingAcceptance.Code);
			miCorrections = (int)DB.Engine.ExecuteScalar(strSelect);

			// Returned to Company
			strSelect = String.Format("SELECT Count(*) from Survey where CompanyID='{0}' AND AssignedUserID='{1}' AND SurveyStatusCode = '{2}'", _activeUser.CompanyID, _activeUser.ID, SurveyStatus.ReturningToCompany.Code);
			miReturnedToCompany = (int)DB.Engine.ExecuteScalar(strSelect);

            // All Assigned
            strSelect = String.Format("SELECT Count(*) from Survey s INNER JOIN SurveyStatus ss ON ss.SurveyStatusCode = s.SurveyStatusCode where CompanyID='{0}' AND ConsultantUserID='{1}' AND ss.SurveyStatusTypeCode!='{2}' AND ReportCompleteDate >= '{3}'", _activeUser.CompanyID, _activeUser.ID, SurveyStatusType.Survey.Code, DateTime.Today.AddMonths(-6));
            _completed = (int)DB.Engine.ExecuteScalar(strSelect);
        }
    }
}
