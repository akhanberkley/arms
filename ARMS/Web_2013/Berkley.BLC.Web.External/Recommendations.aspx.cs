using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using Wilson.ORMapper;

namespace Berkley.BLC.Web.External
{
    public partial class RecommendationsAspx : AppBasePage
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(RecommendationsAspx_Load);
            this.PreRender += new EventHandler(RecommendationsAspx_PreRender);
            this.grdRecs.ItemCommand += new DataGridCommandEventHandler(grdRecs_ItemCommand);
            this.grdRecs.ItemCreated += new DataGridItemEventHandler(grdRecs_ItemCreated);
        }
        #endregion

        protected Survey _eSurvey;

        void RecommendationsAspx_Load(object sender, EventArgs e)
        {
            Guid id = Utility.GetGuidFromQueryString("surveyid", true);
            _eSurvey = Survey.Get(id);

            //determine if this company utilizes rec classifications
            foreach (DataGridColumn column in grdRecs.Columns)
            {
                if (column.HeaderText == "Classification")
                {
                    column.Visible = (RecClassification.GetOne("CompanyID = ?", CurrentUser.CompanyID) != null);
                }
            }
        }

        void RecommendationsAspx_PreRender(object sender, EventArgs e)
        {
            SurveyRecommendation[] eRecs = SurveyRecommendation.GetSortedArray(_eSurvey.GetAllSurveysForAccount, "PriorityIndex ASC");
            DataGridHelper.BindGrid(grdRecs, eRecs, "ID", "There are no recs for this survey.");
        }

        void grdRecs_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            Guid gRecID = (Guid)grdRecs.DataKeys[e.Item.ItemIndex];

            // get the current recs from the DB and find the index of the primary rec
            SurveyRecommendation[] eRecs = SurveyRecommendation.GetSortedArray(_eSurvey.GetAllSurveysForAccount, "PriorityIndex ASC");
            int iRecIndex = int.MinValue;
            for (int i = 0; i < eRecs.Length; i++)
            {
                if (eRecs[i].ID == gRecID)
                {
                    iRecIndex = i;
                    break;
                }
            }
            if (iRecIndex == int.MinValue) // not found
            {
                return;
            }

            SurveyRecommendation rec = eRecs[iRecIndex];

            // execute the requested transformation on the recs
            using (Transaction trans = DB.Engine.BeginTransaction())
            {
                ArrayList recList = new ArrayList(eRecs);
                switch (e.CommandName.ToUpper())
                {
                    case "MOVEUP":
                        {
                            recList.RemoveAt(iRecIndex);
                            if (iRecIndex > 0)
                            {
                                recList.Insert(iRecIndex - 1, rec);
                            }
                            else // top item (move to bottom)
                            {
                                recList.Add(rec);
                            }
                            break;
                        }
                    case "MOVEDOWN":
                        {
                            recList.RemoveAt(iRecIndex);
                            if (iRecIndex < eRecs.Length - 1)
                            {
                                recList.Insert(iRecIndex + 1, rec);
                            }
                            else // bottom item (move to top)
                            {
                                recList.Insert(0, rec);
                            }
                            break;
                        }
                    case "REMOVE":
                        {
                            recList.RemoveAt(iRecIndex);
                            eRecs[iRecIndex].Delete(trans);

                            //Update the survey history
                            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, CurrentUser, true);
                            manager.RecManagement(eRecs[iRecIndex], SurveyManager.UIAction.Remove);

                            break;
                        }
                    default:
                        {
                            throw new NotSupportedException("Grid command '" + e.CommandName + "' was not expected.");
                        }
                }

                // save all the recs still in our list
                // note: we are updated the priority number as we save to make sure they are always sequential from 1
                for (int i = 0; i < recList.Count; i++)
                {
                    SurveyRecommendation eRecUpdate = (recList[i] as SurveyRecommendation).GetWritableInstance();
                    eRecUpdate.PriorityIndex = (i + 1);
                    eRecUpdate.Save(trans);
                }

                trans.Commit();
            }
        }

        void grdRecs_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            // we only care about datagrid items
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (!this.IsPostBack)
                {
                    //determine if the user has permission to remove recs
                    LinkButton lnk = (LinkButton)e.Item.FindControl("btnRemoveRec");
                    //lnk.Enabled = CurrentUser.Permissions.CanTakeAction(SurveyActionType.RemoveRec, _eSurvey, CurrentUser);
                    //if (lnk.Enabled)
                    //{
                        JavaScript.AddConfirmationNoticeToWebControl(lnk, "Are you sure you want to remove this recommendation?");
                    //}
                }
            }
        }
    }
}
