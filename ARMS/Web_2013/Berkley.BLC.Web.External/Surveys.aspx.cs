using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;

namespace Berkley.BLC.Web.External
{
    public partial class SurveysAspx : AppBasePage
    {
        protected string mstrFunction;
        protected int _recordCount = 0;

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
            BuildDataGrid();
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(SurveysAspx_Load);
            this.PreRender += new System.EventHandler(this.Page_PreRender);
            this.grdItemz.PageIndexChanged += new DataGridPageChangedEventHandler(grdItems_PageIndexChanged);
            this.grdItemz.SortCommand += new DataGridSortCommandEventHandler(grdItems_SortCommand);
            this.grdItemz.ItemDataBound += new DataGridItemEventHandler(grdItems_ItemDataBound);

            this.ddlItemsPerPage.SelectedIndexChanged += new EventHandler(ddlItemsPerPage_SelectedIndexChanged);
        }

        #endregion

        private void SurveysAspx_Load(object sender, EventArgs e)
        {
            /* 
            * This page is called based on a fee company user,
            */
            mstrFunction = Utility.GetStringFromQueryString("function", true);

            if (!this.IsPostBack)
            {
                this.ViewState["SortExpression"] = "DueDate DESC";
                this.ViewState["SortReversed"] = false;
                this.ViewState["PageIndex"] = 0;
            }
        }

        private void Page_PreRender(object sender, System.EventArgs e)
        {
            string sortExp = (string)this.ViewState["SortExpression"];
            if ((bool)this.ViewState["SortReversed"])
            {
                sortExp = sortExp.Replace(" DESC", " TEMP");
                sortExp = sortExp.Replace(" ASC", " DESC");
                sortExp = sortExp.Replace(" TEMP", " ASC");
            }

            string noRecordsMessage = "No assigned surveys currently exist.";
            VWSurvey[] roSurvey = GetSurveys(sortExp);

            grdItemz.ItemCreated += new DataGridItemEventHandler(Utility.PagingGrid_ItemCreated);
            DataGridHelper.BindPagingGrid(grdItemz, roSurvey, null, noRecordsMessage);
        }

        private VWSurvey[] GetSurveys(string strSort)
        {
            ArrayList args = new ArrayList();
            string strQuery = "";

            if (cboDueDateDays.SelectedValue.Contains("90"))
            {
                strQuery += "DueDate <= ? && ";
                args.Add(DateTime.Today.AddDays(90));
            }

            switch (mstrFunction)
            {
                case "allassignments":
                    base.PageHeading = "All Assigned";
                    args.Add(CurrentUser.CompanyID);
                    args.Add(CurrentUser.ID);
                    args.Add(SurveyStatus.AssignedSurvey.Code);
                    strQuery += "CompanyID = ? && AssignedUserID = ? && SurveyStatusCode = ?";
                    tdDueDateCol.Visible = true;
                    break;

                case "newwork":
                    base.PageHeading = "New Work";
                    args.Add(CurrentUser.CompanyID);
                    args.Add(CurrentUser.ID);
                    args.Add(SurveyStatus.AwaitingAcceptance.Code);
                    args.Add(0);
                    strQuery +=
                        "CompanyID = ? && AssignedUserID = ? && SurveyStatusCode = ? && (Correction = ? || ISNULL(Correction))";
                    tdButtonCol.Visible = true;
                    break;

                case "corrections":
                    base.PageHeading = "Corrections";
                    args.Add(CurrentUser.CompanyID);
                    args.Add(CurrentUser.ID);
                    args.Add(SurveyStatus.AwaitingAcceptance.Code);
                    args.Add(SurveyStatus.AssignedSurvey.Code);
                    args.Add(1);
                    strQuery +=
                        "CompanyID = ? && AssignedUserID = ? && (SurveyStatusCode = ? || SurveyStatusCode = ?) && Correction = ?";
                    tdDueDateCol.Visible = true;
                    break;

                case "returntocompany":
                    base.PageHeading = "Return To Company";
                    args.Add(CurrentUser.CompanyID);
                    args.Add(CurrentUser.ID);
                    args.Add(SurveyStatus.ReturningToCompany.Code);
                    strQuery += "CompanyID = ? && AssignedUserID = ? && SurveyStatusCode = ?";
                    tdButtonCol.Visible = true;
                    btnAcceptWork.Text = "Return To Company";
                    break;

                case "completed":
                    base.PageHeading = "Completed";
                    args.Add(CurrentUser.CompanyID);
                    args.Add(CurrentUser.ID);
                    args.Add(SurveyStatusType.Survey.Code);
                    args.Add(DateTime.Today.AddMonths(-6));
                    strQuery +=
                        "CompanyID = ? && ConsultantUserID = ? && SurveyStatusTypeCode != ? && ReportCompleteDate >= ?";
                    tdButtonCol.Visible = false;
                    break;

                default:
                    return null;
            }

            VWSurvey[] surveys = VWSurvey.GetSortedArray(strQuery, strSort, args.ToArray());
            _recordCount = surveys.Length;

            return surveys;
        }

        protected void grdItems_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            // get current sort expression and reverse the sort if same as new one
            string sortExp = (string)this.ViewState["SortExpression"];
            bool sortRev = (bool)this.ViewState["SortReversed"];
            sortRev = !sortRev && (sortExp == e.SortExpression);

            this.ViewState["SortExpression"] = e.SortExpression;
            this.ViewState["SortReversed"] = sortRev;
        }

        private void ddlItemsPerPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            grdItemz.PageSize = Convert.ToInt32(this.ddlItemsPerPage.SelectedValue);
        }

        private void grdItems_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            grdItemz.CurrentPageIndex = e.NewPageIndex;
        }

        private void grdItems_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            //20121128 Dustin: Don't think this is needed for the external site.
        }

        protected void btnAcceptWork_Click(object sender, EventArgs e)
        {
            switch (mstrFunction)
            {
                case "newwork":
                    Survey[] newSurveys =
                        Survey.GetArray(
                            "CompanyID = ? && AssignedUserID = ? && StatusCode = ? && (Correction = 0 || ISNULL(Correction))",
                            CurrentUser.CompanyID, CurrentUser.ID, SurveyStatus.AwaitingAcceptance.Code);

                    foreach (Survey survey in newSurveys)
                    {
                        SurveyManager manager = SurveyManager.GetInstance(survey, CurrentUser, true);
                        manager.Accept();
                    }

                    Response.Redirect("MySurveys.aspx", true);
                    break;

                case "returntocompany":
                    Survey[] returnSurveys = Survey.GetArray("CompanyID = ? && AssignedUserID = ? && StatusCode = ?",
                                                             CurrentUser.CompanyID, CurrentUser.ID,
                                                             SurveyStatus.ReturningToCompany.Code);

                    foreach (Survey survey in returnSurveys)
                    {
                        SurveyManager manager = SurveyManager.GetInstance(survey, CurrentUser, true);
                        manager.ReturnToCompany();
                    }

                    Response.Redirect("MySurveys.aspx", true);
                    break;

            }
        }

        private void BuildDataGrid()
        {
            //Get the columns by order from the DB
            GridColumnFilterCompany[] filterCols =
                GridColumnFilterCompany.GetSortedArray("CompanyID = ? && IsExternal = 1 && Display=1",
                                                       "Display DESC, DisplayOrder ASC", CurrentUser.CompanyID);

            foreach (GridColumnFilterCompany column in filterCols)
            {
                string columnName = (column.GridColumnFilter.Name == "Call<br>Count")
                                        ? CurrentUser.Company.CallCountLabel
                                        : column.GridColumnFilter.Name;

                var template = new TemplateColumn
                    {
                        HeaderText = columnName,
                        SortExpression = column.GridColumnFilter.SortExpression,
                        ItemTemplate = new MyTemplate(column.GridColumnFilter)
                    };
                template.ItemStyle.HorizontalAlign = (column.GridColumnFilter.ID == GridColumnFilter.Reminder.ID)
                                                         ? HorizontalAlign.Center
                                                         : HorizontalAlign.Left;
                grdItemz.Columns.Add(template);
            }
        }

        private class MyTemplate : ITemplate
        {
            private string expression;
            private GridColumnFilter filter;

            public MyTemplate(GridColumnFilter filter)
            {
                this.filter = filter;
                this.expression = filter.FilterExpression;
            }

            public void InstantiateIn(System.Web.UI.Control container)
            {
                //figure out the type of control to bind
                if (filter.ID == GridColumnFilter.Reminder.ID)
                {
                    ImageButton btnReminder = new ImageButton();
                    btnReminder.ID = "btnReminder";
                    btnReminder.ImageUrl = Utility.AppPath + "/images/dash.gif";
                    btnReminder.DataBinding += new EventHandler(this.BindImageButton);
                    //btnReminder.Command += new CommandEventHandler(btnReminder_Command);
                    container.Controls.Add(btnReminder);
                }
                else if (filter.ID == GridColumnFilter.SurveyLink.ID)
                {
                    HyperLink link = new HyperLink();
                    link.ID = "lnkSurvey";
                    link.DataBinding += new EventHandler(this.BindHyperLink);
                    container.Controls.Add(link);
                }
                else if (filter.ID == GridColumnFilter.InsuredLink.ID)
                {
                    HyperLink link = new HyperLink();
                    link.ID = "lnkInsured";
                    link.DataBinding += new EventHandler(this.BindHyperLink);
                    container.Controls.Add(link);
                }
                else
                {
                    Literal l = new Literal();
                    l.DataBinding += new EventHandler(this.BindData);
                    container.Controls.Add(l);
                }
            }

            public void BindData(object sender, EventArgs e)
            {
                Literal l = (Literal)sender;
                DataGridItem container = (DataGridItem)l.NamingContainer;

                switch (filter.SystemTypeCode)
                {
                    case TypeCode.DateTime:
                        l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:d}", DateTime.MinValue, "(none)");
                        break;
                    case TypeCode.Decimal:
                        l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:C}", Decimal.MinValue, "(none)");
                        break;
                    case TypeCode.Int64:
                        l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:N2}", Decimal.MinValue, "(none)");
                        break;
                    case TypeCode.Int32:
                        l.Text = HtmlEncodeNullable(container.DataItem, expression, "{0:N0}", Int32.MinValue, "(none)");
                        break;
                    default:
                        if (expression.ToLower().Equals("priority"))
                        {
                            var d = (VWSurvey)container.DataItem;
                            string imgName = !string.IsNullOrEmpty(d.Priority)
                                                 ? d.Priority
                                                 : "./Images/priorities/none.png";
                            var imgHtml =
                                string.Format(
                                    " <img id='imgPriority' src='{0}'  height='20' width='20' />",
                                    imgName);
                            l.Text = imgHtml;
                        }
                        else
                            l.Text = HtmlEncodeNullable(container.DataItem, expression, string.Empty, "(none)");
                        break;
                }
            }

            public void BindImageButton(object sender, EventArgs e)
            {
                ImageButton btnReminder = (ImageButton)sender;
                DataGridItem container = (DataGridItem)btnReminder.NamingContainer;

                DateTime reminderDT = Convert.ToDateTime(HtmlEncode(container.DataItem, "ReminderDate"));
                
                // Set reminder icon
                if (reminderDT > DateTime.MinValue)
                {
                    btnReminder.ImageUrl = (reminderDT > DateTime.Today) ? Utility.AppPath + "/images/clock.gif" : Utility.AppPath + "/images/xmark.gif";
                    btnReminder.AlternateText = string.Format("{0}: {1}", reminderDT.ToShortDateString(), ((HtmlEncode(container.DataItem, "ReminderComment")).Length > 0) ? HtmlEncode(container.DataItem, "ReminderComment") : "(no comments)");
                }
                else
                {
                    btnReminder.ImageUrl = Utility.AppPath + "/images/dash.gif";
                }

                btnReminder.CommandName = HtmlEncode(container.DataItem, "SurveyID");
            }

            public void BindHyperLink(object sender, EventArgs e)
            {
                HyperLink link = (HyperLink)sender;
                DataGridItem container = (DataGridItem)link.NamingContainer;
                link.Text = HtmlEncodeNullable(container.DataItem, expression, string.Empty, "(none)");

                if (link.ID == "lnkSurvey")
                {
                    link.NavigateUrl = string.Format("Survey.aspx?surveyid={0}&{1}",
                                                     HtmlEncode(container.DataItem, "SurveyID"),
                                                     HttpContext.Current.Request.QueryString.ToString());
                }
                else
                {
                    link.NavigateUrl = string.Format("Insured.aspx?surveyid={0}&locationid={1}&{2}",
                                                     HtmlEncode(container.DataItem, "SurveyID"),
                                                     HtmlEncode(container.DataItem, "LocationID"),
                                                     HttpContext.Current.Request.QueryString.ToString());
                }
            }
        }
    }
}