using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using Berkley.BLC.Entities;
using Entities = Berkley.BLC.Entities;
using QCI.Web;
using QCI.Web.Validation;

namespace Berkley.BLC.Web.External
{
    public partial class LoginAspx : AppBasePage
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(LoginAspx_Load);
            this.btnLogin.Click += new EventHandler(btnLogin_Click);
        }
        #endregion

        void LoginAspx_Load(object sender, EventArgs e)
        {
            this.PageHeading = "Login";

            // give the username field the focus on first page hit
            if (!this.IsPostBack)
            {
                JavaScript.SetInputFocus(txtUsername);
            }
        }

        void btnLogin_Click(object sender, EventArgs e)
        {
            // handle non-IE browsers and disabled javascript
            if (!this.IsValid)
            {
                ShowValidatorErrors();
                return;
            }

            string username = string.Empty;
            string password = string.Empty;
            try
            {
                username = FieldValidation.ValidateStringField(txtUsername, "Username", 20, true);
                password = FieldValidation.ValidateStringField(txtPassword, "Password", 20, true);
            }
            catch (FieldValidationException val)
            {
                JavaScript.ShowMessage(this, val.Message);
                return;
            }

            // authenticate the username/password entered
            string failureMessage;
            FeeUser authedUser = Security.Authenticate(username, password, Request.UserHostAddress, out failureMessage);
            if (authedUser == null)
            {
                JavaScript.ShowMessage(this, failureMessage);
                JavaScript.SetInputFocus(txtPassword);
                return;
            }

            // log the user in and take them to the default/return page
            Security.LoginUser(authedUser, false, "mysurveys.aspx");
        }
    }
}
