<%@ Application Language="C#" %>
<%@ Import namespace="Berkley.BLC.Entities" %>
<%@ Import namespace="QCI.DataAccess" %>
<%@ Import namespace="QCI.ExceptionManagement" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // initalize the DAL with the default connection string - which is used for all database access
        string connStr = Berkley.BLC.Core.CoreSettings.ARMSConnectionString;
		if( connStr == null ) throw new Exception("Configuration setting 'ConnectionString' must be specified in the application config file.");
		DB.Initialize(connStr);
		SqlHelper.ConnectionString = connStr;

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }

    void Application_AuthenticateRequest(object sender, EventArgs e)
    {
        // replace the principal object with a custom one containing the user's roles, which
        // will will flow through the rest of this request.
        Berkley.BLC.Web.External.Security.ReplaceUserPrincipal(Context);
    }
        
    void Application_Error(object sender, EventArgs e) 
    {
        // get the exception that caused this event
        Exception ex = Server.GetLastError();
        if (ex != null)
        {
            // skip 404 errors (file not found) and ajax invoked errors
            if (ex is HttpException &&
                ((ex as HttpException).GetHttpCode() == 404 || ex.Message.Contains("A potentially dangerous Request.Path") || (ex.InnerException != null && ex.InnerException.Message.Contains("A potentially dangerous Request.Path"))))
            {
                return;
            }

            // publish the exception (using the configured publishers)
            // include the full URL if available; the query string is almost always valuable.
            HttpContext context = HttpContext.Current;
            if (context != null && context.Request != null)
            {
                NameValueCollection info = new NameValueCollection();
                info.Add("Raw URL", context.Request.RawUrl);
                //ExceptionManager.Publish(ex, info);

                string msg = ExceptionManager.PublishEmail(ex);
                HttpContext.Current.Server.ClearError();
                Response.Redirect("~/Error.aspx?page=" +
                        HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.Url.ToString()) + "&msg=" + msg);
            }
            else // no http context and/or request
            {
                //ExceptionManager.Publish(ex);

                string msg = ExceptionManager.PublishEmail(ex);
                HttpContext.Current.Server.ClearError();
                Response.Redirect("~/Error.aspx?page=" +
                        HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.Url.ToString()) + "&msg=" + msg);
            }
        }

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Application_BeginRequest(Object sender, EventArgs e)
    {
        // verify user is on a secure connection (if required)
        if (!this.Request.IsSecureConnection)
        {
            string requireSSL = System.Configuration.ConfigurationManager.AppSettings["RequireSSL"];
            
            if (requireSSL == null) throw new Exception("Configuration setting 'RequireSSL' must be specified in the application config file.");

            if (requireSSL != "0") // required
            {
                string url = Regex.Replace(this.Request.Url.AbsoluteUri, @"^http://", "https://", RegexOptions.IgnoreCase);
                Response.Redirect(url, true);
                return;
            }
        }
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
