using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using Berkley.BLC.Entities;
using QCI.Web.Validation;
using Berkley.BLC.Business;

namespace Berkley.BLC.Web.External
{
    public partial class DocumentAspx : AppBasePage
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(DocumentAspx_Load);
        }
        #endregion

        void DocumentAspx_Load(object sender, EventArgs e)
        {
            //determine if this is a template, a letter, service plan document, or an survey document
            Guid gDocID = Utility.GetGuidFromQueryString("docid", false);
            SurveyDocument doc = (gDocID != Guid.Empty) ? SurveyDocument.Get(gDocID) : null;

            Guid gReportID = Utility.GetGuidFromQueryString("reportid", false);
            MergeDocumentVersion reportDoc = (gReportID != Guid.Empty) ? MergeDocumentVersion.Get(gReportID) : null;

            Guid gSurveyID = Utility.GetGuidFromQueryString("surveyid", false);
            Survey eSurvey = (gSurveyID != Guid.Empty) ? Survey.Get(gSurveyID) : null;

            string fileNetReference;
            string mimeType;
            string docName;
            Company eCompany;
            if (doc != null) //Uploaded Document
            {
                fileNetReference = doc.FileNetReference;
                mimeType = doc.MimeType;
                docName = doc.DocumentName;
                eCompany = doc.Survey.Company;
            }
            else if (reportDoc != null) //Report Document
            {
                fileNetReference = reportDoc.FileNetReference;
                mimeType = reportDoc.MimeType;
                docName = reportDoc.DocumentName;
                eCompany = reportDoc.MergeDocument.Company;
            }
            else
            {
                throw new Exception("Was expecting either a letter, template, or uploaded document id.");
            }

            if (this.IsPostBack)
            {

                byte[] content;
                ImagingHelper imaging = new ImagingHelper(eCompany);
                System.IO.Stream contentStream = imaging.RetrieveFileImage(fileNetReference);

                //Determine if this is a merge document
                if (reportDoc != null)
                {
                    if (eSurvey == null)
                    {
                        throw new Exception("Survey to be used to mail merge is null.");
                    }

                    MailMerge mergedDoc = new MailMerge(contentStream, eSurvey);
                    contentStream = mergedDoc.GetMergedDocument;
                    contentStream.Position = 0;
                }

                // fill a buffer with the data from the stream
                // note: it would be better to just stream the file right to the response but we can't easily do so
                using (contentStream)
                {
                    int bufferSize = (int)contentStream.Length;
                    content = new byte[bufferSize];

                    int bytesRead = contentStream.Read(content, 0, bufferSize);
                    if (bytesRead != bufferSize)
                    {
                        throw new Exception(bytesRead + " bytes were read from the content stream.  Exactly " + bufferSize + " bytes were expected.");
                    }
                }

                // remove any content in the response
                this.Response.Clear();
                this.Response.Buffer = true;

                // set the MIME type and provide a 'friendly' filename for the client
                this.Response.ContentType = mimeType;
                //this.Response.AddHeader("Content-Disposition", "inline;filename=" + docName);
                string contentDisposition;

                if (this.Request.Browser.Browser == "IE" && (this.Request.Browser.Version == "7.0" || this.Request.Browser.Version == "8.0"))
                    contentDisposition = "inline; filename=" + Uri.EscapeDataString(docName);
                else
                    contentDisposition = "inline; filename=\"" + docName + "\"; filename*=UTF-8''" + Uri.EscapeDataString(docName);

                this.Response.AddHeader("Content-Disposition", contentDisposition);
        


                // stream the file content to the user
                this.Response.BinaryWrite(content);
                this.Response.Flush();
                //if (this.Request.Browser.Browser == "IE" || this.Request.Browser.Browser == "InternetExplorer")
                    this.Response.End();
            }
        }
    }
}
