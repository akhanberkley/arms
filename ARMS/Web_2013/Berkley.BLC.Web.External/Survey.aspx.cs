using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections;

using Berkley.BLC.Entities;
using Berkley.BLC.Business;
using QCI.Web;
using QCI.Web.Validation;
using QCI.ExceptionManagement;

namespace Berkley.BLC.Web.External
{
    public partial class SurveyAspx : AppBasePage
    {
        protected Survey _eSurvey;
        protected string _vendorDocType;
        protected string _handleCompletedCallLabel;
        
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(SurveyAspx_Load);
            this.PreRender += new EventHandler(SurveyAspx_PreRender);
            this.repActions.ItemCommand += new RepeaterCommandEventHandler(repActions_ItemCommand);
            this.repActions.ItemDataBound += new RepeaterItemEventHandler(repActions_ItemDataBound);
            this.btnAddNote.Click += new EventHandler(btnAddNote_Click);
            this.btnAttachDocument.Click += new EventHandler(btnAttachDocument_Click);
            this.btnSetDetailsSubmit.Click +=new EventHandler(btnSetDetailsSubmit_Click);
            this.btnSetDetailsCancel.Click +=new EventHandler(btnSetDetailsCancel_Click);
            this.repDocs.ItemDataBound += new RepeaterItemEventHandler(repDocs_ItemDataBound);
            this.btnReportCancel.Click += new EventHandler(btnReportCancel_Click);
        }
        #endregion

        void SurveyAspx_Load(object sender, EventArgs e)
        {
            Guid id = Utility.GetGuidFromQueryString("SurveyID", true);
            _eSurvey = Survey.Get(id);

            if (!this.IsPostBack)
            {
                ShowActionControl(this.repActions);
            }
        }

        void SurveyAspx_PreRender(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                // re-load the survey to avoid dirty reads after postbacks
                _eSurvey = Survey.Get(_eSurvey.ID);
            }

            // our date picker needs this script
            Utility.AddJavaScriptToPage(this, Utility.AppPath + "/AutoFormat.js");

            // put the insured and survey number in the header
            base.PageHeading = string.Format("Survey #{0} ({1})", _eSurvey.Number, _eSurvey.Insured.Name);

            // populate all our static fields
            this.PopulateFields(_eSurvey);
            this.PopulateFields(_eSurvey.Status);

            // populate statics that could be null
            if (_eSurvey.Grading != null)
            {
                this.PopulateFields(_eSurvey.Grading);
            }
            else
            {
                lblGrading.Text = "(not specified)";
            }

            // get all documents associated to this survey
            SurveyDocument[] docs = SurveyDocument.GetSortedArray("SurveyID = ? && IsRemoved = false", "UploadedOn DESC", _eSurvey.ID);
            repDocs.DataSource = docs;
            repDocs.DataBind();
            boxDocs.Title = string.Format("Documents ({0})", docs.Length);

            // get all notes associated to this survey
            SurveyNote[] notes = SurveyNote.GetSortedArray("SurveyID = ? && InternalOnly = false && AdminOnly = false", "EntryDate DESC", _eSurvey.ID);
            repNotes.DataSource = notes;
            repNotes.DataBind();
            boxNotes.Title = string.Format("Comments ({0})", notes.Length);

            // get all the doc types for the company
            DocType[] docTypes = DocType.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", this.CurrentUser.CompanyID);
            ComboHelper.BindCombo(cboDocType, docTypes, "Code", "{0} - {1}", "Code|Description".Split('|'));
            if (docTypes.Length > 0)
            {
                cboDocType.Items.Insert(0, new ListItem("-- select doc type --", ""));

                // check if the survey date was entered yet
                if (_eSurvey.SurveyedDate == DateTime.MinValue)
                {
                    JavaScript.AddConfirmationNoticeToWebControl(btnAttachDocument, "The survey date has not been entered yet in the Survey Details.  Are you sure you would like to continue uploading this document?");
                }
            }

            //Enforce document type for Vendors
            _vendorDocType = Berkley.BLC.Business.Utility.GetCompanyParameter("VendorDocumentType", CurrentUser.Company);
            if (!string.IsNullOrWhiteSpace(_vendorDocType))
            {
                cboDocType.Enabled = false;
                cboDocType.SelectedValue = _vendorDocType;
            }

            // determine available actions the user can take on this survey
            SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser);
            SurveyActionType[] actions = manager.GetExternalAvailableActions();

            // turn on (or show) the actions available
            EnableActions(actions);
        }

        private void EnableActions(SurveyActionType[] eActions)
        {
            //// reset integrated actions
            divAddNote.Visible = false;
            divUploadDoc.Visible = false;

            //// configure page for each integrated action in list
            ArrayList nonIntegrated = new ArrayList(eActions.Length);
            foreach (SurveyActionType eAction in eActions)
            {
                if (eAction == SurveyActionType.AddNote)
                {
                    //if just added note, keep the focus there
                    if (Utility.GetIntFromQueryString("note", false).Equals(1))
                    {
                        JavaScript.SetInputFocus(lnkAddNote);
                    }

                    divAddNote.Visible = true;
                }
                else if (eAction == SurveyActionType.AddDocument)
                {
                    divUploadDoc.Visible = true;
                }
                else if (eAction == SurveyActionType.RemoveRec)
                {
                    //do nothing, action is not used on this screen
                }
                else // not integrated
                {
                    nonIntegrated.Add(eAction);
                }
            }

            // bind the actions repeater with what is left
            repActions.DataSource = nonIntegrated;
            repActions.DataBind();
        }

        private void ShowActionControl(Control ctrl)
        {
            this.repActions.Visible = (ctrl == this.repActions);
            this.divSetDetails.Visible = (ctrl == this.divSetDetails);
            this.divReports.Visible = (ctrl == this.divReports);
        }

        void repActions_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string actionTypeCode = e.CommandName;
            SurveyActionType action = SurveyActionType.Get(actionTypeCode);
            TakeAction(action);
        }

        void repActions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //// we only care about repeater items
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // see if this action has a confirm prompt specified
                SurveyActionType actionType = (SurveyActionType)e.Item.DataItem;

                if (_eSurvey.Status == SurveyStatus.AssignedSurvey && actionType == SurveyActionType.ReturnToCompany)
                {
                    // attach the message to the button control
                    LinkButton btn = (LinkButton)e.Item.FindControl("btnAction");
                    if (btn != null)
                    {
                        JavaScript.AddConfirmationNoticeToWebControl(btn, string.Format("Are you sure you want to return this uncompleted survey back to {0}?", CurrentUser.Company.Name));
                    }
                }
            }
        }

        private void TakeAction(SurveyActionType actionType)
        {
            try
            {
                // determine what action to take based on the type
                if (actionType == SurveyActionType.SetSurveyDetails)
                {
                    PrepForSettingDetails();
                    ShowActionControl(this.divSetDetails);
                }
                else if (actionType == SurveyActionType.AcceptSurvey)
                {
                    SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser, true);
                    manager.Accept();

                    if (Response.IsClientConnected)
                    {
                        Response.Redirect(GetReturnUrl(), true);
                    }
                }
                else if (actionType == SurveyActionType.SendForReview)
                {
                    string strBuilder = string.Empty;
                    if (_eSurvey.SurveyedDate == DateTime.MinValue)
                    {
                        strBuilder += "- Survey Date is required.\n";
                    }

                    if (_eSurvey.Hours == Decimal.MinValue)
                    {
                        strBuilder += "- Hours are required.\n";
                    }

                    if (CurrentUser.Company.SupportsOverallGrading && _eSurvey.GradingCode == string.Empty)
                    {
                        strBuilder += "- Overall Grading is required.\n";
                    }

                    if (CurrentUser.Company.RequireVendorCost && _eSurvey.FeeConsultantCost == Decimal.MinValue)
                    {
                        strBuilder += "- Cost is required.\n";
                    }

                    if (!String.IsNullOrEmpty(strBuilder))
                    {
                        throw new FieldValidationException("Please address the following:\n\n" + strBuilder);
                    }
                    
                    SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser, true);
                    manager.SendForReview(true);

                    if (Response.IsClientConnected)
                    {
                        Response.Redirect("MySurveys.aspx", true);
                    }
                }
                else if (actionType == SurveyActionType.ReturnToCompany)
                {
                    SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser, true);
                    manager.ReturnToCompany();

                    if (Response.IsClientConnected)
                    {
                        Response.Redirect("MySurveys.aspx", true);
                    }
                }
                else if (actionType == SurveyActionType.DownloadReport)
                {
                    PrepForGenerateReports();
                    ShowActionControl(this.divReports);
                }
                else if (actionType == SurveyActionType.ManageRecs)
                {
                    if (Response.IsClientConnected)
                    {
                        Response.Redirect(string.Format("recommendations.aspx{0}", Utility.GetQueryStringForNav()), true);
                    }
                }
                else
                {
                    throw new NotSupportedException("Action Type '" + actionType.Code + "' was not expected.");
                }
            }
            catch (FieldValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }
            catch (ValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }
        }

        protected void lnkDownloadDoc_OnClick(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnk = (LinkButton)sender;
                SurveyDocument doc = SurveyDocument.Get(new Guid(lnk.Attributes["DocumentID"]));

                StreamDocument docRetriever = new StreamDocument(doc, CurrentUser.Company);
                docRetriever.AttachDocToResponse(this);
            }
            catch (System.Web.HttpException ex)
            {
                //ignore http exceptions for when the user quits a doc download and the stream doesn't get flushed
                if (ex.Message.Contains("The remote host closed the connection"))
                {
                    //swallow exception
                }
                else
                {
                    throw new Exception("See inner exception for details.", ex);
                }
            }
        }

        void repDocs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                SurveyDocument eDoc = (SurveyDocument)e.Item.DataItem;
                LinkButton lnk = (LinkButton)e.Item.FindControl("lnkDownloadDoc");
                HtmlAnchor lnkDownloadDocInNewWindow = (HtmlAnchor)e.Item.FindControl("lnkDownloadDocInNewWindow");
                lnk.Attributes.Add("DocumentID", eDoc.ID.ToString());

                //determine which link to display
                lnk.Visible = !eDoc.MimeType.Contains("htm");
                lnkDownloadDocInNewWindow.Visible = eDoc.MimeType.Contains("htm");
            }
        }

        #region ACTION: Set Survey Details

        private void PrepForSettingDetails()
        {
            // populate the combo
            SurveyGradingCompany[] eGradings = SurveyGradingCompany.GetSortedArray("CompanyID = ?", "DisplayOrder ASC", CurrentUser.CompanyID);
            ComboHelper.BindCombo(cboOverallGrading, eGradings, "SurveyGrading.Code", "SurveyGrading.Name");
            cboOverallGrading.Items.Insert(0, new ListItem("-- select grading --", ""));

            //load the values, if any
            dpDateSurveyed.Date = _eSurvey.SurveyedDate;
            txtTotalHours.Text = (_eSurvey.Hours != decimal.MinValue) ? _eSurvey.Hours.ToString("N2") : string.Empty;
            txtCost.Text = (_eSurvey.FeeConsultantCost != decimal.MinValue) ? _eSurvey.FeeConsultantCost.ToString("N2") : string.Empty;
            cboOverallGrading.SelectedValue = _eSurvey.GradingCode;

            rdoRecYes.Checked = _eSurvey.RecsRequired;
            rdoRecNo.Checked = !_eSurvey.RecsRequired;
            rdoUWNotifiedYes.Checked = _eSurvey.UWNotified;
            rdoUWNotifiedNo.Checked = !_eSurvey.UWNotified;
        }

        void btnSetDetailsSubmit_Click(object sender, EventArgs e)
        {
            // perform some validation
            try
            {
                decimal hours = decimal.MinValue;
                if (txtTotalHours.Text.Trim().Length > 0)
                {
                    hours = (decimal)FieldValidation.ValidateFloatField(txtTotalHours, "Total Hours");
                }

                decimal cost = decimal.MinValue;
                if (txtCost.Text.Trim().Length > 0)
                {
                    cost = (decimal)FieldValidation.ValidateFloatField(txtCost, "Cost");
                }

                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser, true);
                manager.SetSurveyDetails(dpDateSurveyed.Date, hours, cboOverallGrading.SelectedValue, string.Empty, Decimal.MinValue, cost, rdoRecYes.Checked, rdoUWNotifiedYes.Checked, string.Empty, DateTime.MinValue);

                //Check if Recs are required and if any comments were entered by the vendor
                if (rdoRecYes.Checked)
                {
                    QCI.Web.JavaScript.ShowMessageAndRedirect(this, "Since recommendations were selected as being required, please list the recommendations in the comments section.\n", GetReturnUrl());
                    return;
                }
            }
            catch (FieldValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }
            catch (ValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }

            // show the actions list again
            if (Response.IsClientConnected)
            {
                Response.Redirect(GetReturnUrl(), true);
            }
        }

        void btnSetDetailsCancel_Click(object sender, EventArgs e)
        {
            // show the actions list again
            if (Response.IsClientConnected)
            {
                Response.Redirect(GetReturnUrl(), true);
            }
        }

        #endregion

        #region ACTION: Add Note

        void btnAddNote_Click(object sender, EventArgs e)
        {
            try
            {
                string comment = FieldValidation.ValidateStringField(txtNote, "Comment", 8000, true);

                // add the note to this survey
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser, true);
                manager.AddNote(comment, false, false, DateTime.Now, string.Empty);
            }
            catch (FieldValidationException ex)
            {
                this.ShowErrorMessage(ex);
                return;
            }

            if (Response.IsClientConnected)
            {
                Response.Redirect(GetReturnUrl(), true);
            }
        }

        #endregion

        #region ACTION: Attach Document

        void btnAttachDocument_Click(object sender, EventArgs e)
        {
            HttpPostedFile postedFile = fileUploader.PostedFile;

            try
            {
                // make sure a filename was uploaded
                if (postedFile == null || postedFile.FileName.Length == 0) // no filename was specified
                {
                    throw new FieldValidationException("Please select a file to submit.");
                }

                // make sure the file name is not larger the max allowed
                string fileName = System.IO.Path.GetFileName(postedFile.FileName);
                if (fileName.Length > 200)
                {
                    throw new FieldValidationException("The file name cannot exceed 200 characters.");
                }

                // check for an extesion
                if (System.IO.Path.GetExtension(fileName).Length <= 0)
                {
                    throw new FieldValidationException("The file name must contain a file extension.");
                }

                // make sure the file has content
                int max = int.Parse(ConfigurationManager.AppSettings["MaxRequestLength"]);
                if (postedFile.ContentLength <= 0) // the file is empty
                {
                    throw new FieldValidationException("The file submitted is empty.");
                }
                else if (postedFile.ContentLength > max)
                {
                    // the maximum file size is 10 Megabytes
                    throw new FieldValidationException("The file size cannot be larger than 10 Megabytes.");
                }

                // check if the company has doc types and if one was selected
                if (cboDocType.Items.Count > 0 && cboDocType.SelectedValue == "")
                {
                    throw new FieldValidationException("Please select a doc type.");
                }
            }
            catch (FieldValidationException ex)
            {
                JavaScript.SetInputFocus(boxDocs);
                this.ShowErrorMessage(ex);
                return;
            }

            // attach this file to the audit
            try
            {
                DocType docType = DocType.GetOne("Code = ?", cboDocType.SelectedValue);
                
                SurveyManager manager = SurveyManager.GetInstance(_eSurvey, this.CurrentUser, true);
                manager.AttachDocument(postedFile.FileName, postedFile.ContentType, postedFile.InputStream, docType, txtDocRemarks.Text, true);

                if (Response.IsClientConnected)
                {
                    Response.Redirect(GetReturnUrl(), false);
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                //Do nothing, let the request redirect
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
                JavaScript.ShowMessage(this, "An error occured while trying to save your document to the imaging server.  An administrator has been notified.");
                return;
            }
        }

        #endregion

        #region ACTION: Generate Report

        private void PrepForGenerateReports()
        {
            MergeDocumentVersion[] eReports = MergeDocumentVersion.GetSortedArray("MergeDocument[CompanyID = ? && Disabled = false && IsLetter = false] && IsActive = true",
                "MergeDocument.PriorityIndex ASC", this.CurrentUser.CompanyID);

            // populate the combo
            ComboHelper.BindCombo(cboReports, eReports, "ID", "MergeDocument.Name");
            cboReports.Items.Insert(0, new ListItem("-- select report --", ""));
        }

        void btnReportCancel_Click(object sender, EventArgs e)
        {
            // show the actions list again
            if (Response.IsClientConnected)
            {
                Response.Redirect(GetReturnUrl(), true);
            }
        }

        #endregion

        protected string GetUserName(object obj)
        {
            User user = (obj as User);
            return Server.HtmlEncode((user != null) ? user.Name : "System");
        }

        protected string GetEncodedComment(object dataItem)
        {
            string comment = (dataItem as SurveyNote).Comment;
            bool internalOnly = (dataItem as SurveyNote).InternalOnly;

            // html encode the string (must do this first)
            comment = Server.HtmlEncode(comment);

            // convert CR, LF, and TAB characters
            comment = comment.Replace("\r\n", "<br>");
            comment = comment.Replace("\r", "<br>");
            comment = comment.Replace("\n", "<br>");
            comment = comment.Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");

            return comment;
        }

        protected string GetFileNameWithSize(object dataItem)
        {
            SurveyDocument doc = (SurveyDocument)dataItem;

            string result = string.Format("{0} ({1:#,##0} KB)", doc.DocumentName, doc.SizeInBytes / 1024);

            return Server.HtmlEncode(result);
        }

        protected string GetEncodedHistory(object dataItem)
        {
            string history = (dataItem as SurveyHistory).EntryText;

            // html encode the string (must do this first)
            return Server.HtmlEncode(history);
        }

        private string GetReturnUrl()
        {
            return string.Format("survey.aspx{0}", Utility.GetQueryStringForNav());
        }
    }
}
