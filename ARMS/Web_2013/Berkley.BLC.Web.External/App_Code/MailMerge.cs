using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections.Specialized;

using Aspose.Words;
using Berkley.BLC.Entities;

/// <summary>
/// Summary description for MailMerge
/// </summary>
public class MailMerge
{
    private Document doc;
    private DataSet ds;

    #region Enumerations

    public enum MergeTables
    {
        InsuredData = 0,
        Recs = 1,
        PrimaryContact = 2,
        Policies = 3,
        NonPrimaryContacts = 4,
        Locations = 5
    }

    #endregion

    /// <summary>
    /// Uses Apose.Word to merge the predefined mail merge fields with the passed survey.
    /// </summary>
    /// <param name="stream">The document stream.</param>
    /// <param name="survey">Survey to be used to merge the fields.</param>
    public MailMerge(Stream stream, Survey survey)
    {
        Aspose.Words.License license = new Aspose.Words.License();
        license.SetLicense("Aspose.Words.lic");

        doc = new Document(stream);

        //execute the stored procedure
        string command = string.Format("EXEC Document_GetMergeFields @SurveyID = '{0}'", survey.ID);
        ds = DB.Engine.GetDataSet(command);

        Merge(survey);
    }

    public Stream GetMergedDocument
    {
        get
        {
            MemoryStream result = new MemoryStream();
            doc.Save(result, SaveFormat.Doc);

            return result;
        }
    }

    private void Merge(Survey survey)
    {
        //Insured Data and the primary contact
        doc.MailMerge.Execute(ds.Tables[(int)MergeTables.InsuredData]);
        doc.MailMerge.Execute(ds.Tables[(int)MergeTables.PrimaryContact]);

        //NOTE: Repeatable Items must have a table name
        DataTable dtRecs = ds.Tables[(int)MergeTables.Recs];
        dtRecs.TableName = "Recs";
        doc.MailMerge.ExecuteWithRegions(dtRecs);

        //NOTE: Repeatable Items must have a table name (3 sets of differently names policy tables)
        DataTable dtPolicies = ds.Tables[(int)MergeTables.Policies];
        dtPolicies.TableName = "Policies";
        doc.MailMerge.ExecuteWithRegions(dtPolicies);

        dtPolicies.TableName = "Policies2";
        doc.MailMerge.ExecuteWithRegions(dtPolicies);

        dtPolicies.TableName = "Policies3";
        doc.MailMerge.ExecuteWithRegions(dtPolicies);

        //NOTE: Repeatable Items must have a table name
        DataTable dtContacts = ds.Tables[(int)MergeTables.NonPrimaryContacts];
        dtContacts.TableName = "NonPrimaryContacts";
        doc.MailMerge.ExecuteWithRegions(dtContacts);

        //NOTE: Repeatable Items must have a table name
        DataTable dtLocations = ds.Tables[(int)MergeTables.Locations];
        dtLocations.TableName = "Locations";
        doc.MailMerge.ExecuteWithRegions(dtLocations);
    }
}
