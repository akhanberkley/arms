using System;
using System.Collections;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Berkley.BLC.Entities;
using QCI.Web;
using QCI.Web.Validation;

namespace Berkley.BLC.Web.External
{
    /// <summary>
    /// Provides a base class for all web pages in this application.
    /// </summary>
    public class AppBasePage : UIMapperBasePage
    {
        private static string _appPath = null;
        private static string _copyright = null;

        private string _pagePath = null;
        private string _pageHeader = null;
        private User _currentUser = null;

        /// <summary>
        /// HTML-encodes a string for display to a user on a web page.
        /// </summary>
        /// <param name="value">Value to be HTML-encoded.</param>
        /// <returns>An HTML-encoded string.</returns>
        public static string HtmlEncode(string value)
        {
            return HttpUtility.HtmlEncode(value);
        }

        /// <summary>
        /// Evaluates data-binding expressions at runtime (using Databinder.Eval method) and returns the result as HTML-encoded text to be displayed in the requesting browser.
        /// </summary>
        /// <param name="container">The object reference against which the expression is evaluated.</param>
        /// <param name="expression">The navigation path from the container to the property value to be placed in the bound control property. This must be a string type of property or field names separated by dots, such as Tables[0].DefaultView.[0].Price.</param>
        /// <returns>An HTML-encoded string that results from the evaluation of the data-binding expression.</returns>
        public static string HtmlEncode(object container, string expression)
        {
            return HtmlEncode(container, expression, "{0}");
        }

        /// <summary>
        /// Evaluates data-binding expressions at runtime (using Databinder.Eval method) and formats the result as HTML-encoded text to be displayed in the requesting browser.
        /// </summary>
        /// <param name="container">The object reference against which the expression is evaluated.</param>
        /// <param name="expression">The navigation path from the container to the property value to be placed in the bound control property. This must be a string type of property or field names separated by dots, such as Tables[0].DefaultView.[0].Price.</param>
        /// <param name="format">A .NET Framework format string, similar to those used by String.Format, that converts the Object (which results from the evaluation of the data-binding expression) to a String that can be displayed by the requesting browser.</param>
        /// <returns>An HTML-encoded string that results from the evaluation of the data-binding expression.</returns>
        public static string HtmlEncode(object container, string expression, string format)
        {
            string value = DataBinder.Eval(container, expression, format);
            return HtmlEncode(value);
        }


        /// <summary>
        /// HTML-encodes a string for display to a user on a web page.
        /// </summary>
        /// <param name="value">Value to be HTML-encoded.</param>
        /// <param name="nullValue">The value that represents a null value.</param>
        /// <param name="nullText">The text value to return if the expression value matches the nullValue provided.</param>
        /// <returns>An HTML-encoded string.</returns>
        public static string HtmlEncodeNullable(object value, object nullValue, string nullText)
        {
            string result;
            if (value == null && nullValue == null || value != null && value.Equals(nullValue))
            {
                result = nullText;
            }
            else
            {
                result = (value != null) ? value.ToString() : string.Empty;
            }
            return HtmlEncode(result);
        }

        /// <summary>
        /// Evaluates data-binding expressions at runtime (using Databinder.Eval method) and returns the result as HTML-encoded text to be displayed in the requesting browser.
        /// </summary>
        /// <param name="container">The object reference against which the expression is evaluated.</param>
        /// <param name="expression">The navigation path from the container to the property value to be placed in the bound control property. This must be a string type of property or field names separated by dots, such as Tables[0].DefaultView.[0].Price.</param>
        /// <param name="nullValue">The value that represents a null value.</param>
        /// <param name="nullText">The text value to return if the expression value matches the nullValue provided.</param>
        /// <returns>An HTML-encoded string that results from the evaluation of the data-binding expression.</returns>
        public static string HtmlEncodeNullable(object container, string expression, object nullValue, string nullText)
        {
            object value = DataBinder.Eval(container, expression);
            return HtmlEncodeNullable(value, nullValue, nullText);
        }

        /// <summary>
        /// Evaluates data-binding expressions at runtime (using Databinder.Eval method) and returns the result as HTML-encoded text to be displayed in the requesting browser.
        /// </summary>
        /// <param name="container">The object reference against which the expression is evaluated.</param>
        /// <param name="expression">The navigation path from the container to the property value to be placed in the bound control property. This must be a string type of property or field names separated by dots, such as Tables[0].DefaultView.[0].Price.</param>
        /// <param name="format">A .NET Framework format string, similar to those used by String.Format, that converts the Object (which results from the evaluation of the data-binding expression) to a String that can be displayed by the requesting browser.</param>
        /// <param name="nullValue">The value that represents a null value.</param>
        /// <param name="nullText">The text value to return if the expression value matches the nullValue provided.</param>
        /// <returns>An HTML-encoded string that results from the evaluation of the data-binding expression.</returns>
        public static string HtmlEncodeNullable(object container, string expression, string format, object nullValue, string nullText)
        {
            object value = DataBinder.Eval(container, expression);
            string result;
            if (value == null && nullValue == null || value != null && value.Equals(nullValue))
            {
                result = nullText;
            }
            else // non-null
            {
                result = (value != null) ? string.Format(format, value) : string.Empty;
            }
            return HtmlEncode(result);
        }


        /// <summary>
        /// Gets the full URL path to the application root, relative to the server root.
        /// </summary>
        public string AppPath
        {
            get
            {
                if (_appPath == null)
                {
                    // get the root virtual path to this web application
                    _appPath = Request.ApplicationPath;
                    if (_appPath == "/") _appPath = string.Empty;
                }
                return _appPath;
            }
        }

        /// <summary>
        /// Gets the copyright information from the assembly.
        /// The value returned is HTML-encoded and is intented to be added to the metadata for the page.
        /// </summary>
        public string AppCopyright
        {
            get
            {
                if (_copyright == null)
                {
                    // get the copyright information from this assembly
                    AssemblyCopyrightAttribute copyrightAttrib = (AssemblyCopyrightAttribute)Attribute.GetCustomAttribute(Assembly.GetExecutingAssembly(), typeof(AssemblyCopyrightAttribute));
                    _copyright = Server.HtmlEncode(copyrightAttrib.Copyright);
                }
                return _copyright;
            }
        }


        /// <summary>
        /// Gets the full URL path to the template folder.
        /// </summary>
        public string TemplatePath
        {
            get { return this.AppPath + "/template"; }
        }

        /// <summary>
        /// Gets the path to this page, relative to the application root.
        /// </summary>
        public string PagePath
        {
            get
            {
                if (_pagePath == null)
                {
                    _pagePath = Request.Path.Substring(this.AppPath.Length);
                }
                return _pagePath;
            }
        }

        /// <summary>
        /// Gets the HTML-encoded title of this page.
        /// </summary>
        public string PageTitle
        {
            get
            {
                return Server.HtmlEncode("Berkley ARMS - ") + this.PageHeading;
            }
        }

        /// <summary>
        /// Gets or sets the HTML-encoded heading of this page.
        /// NOTE: When setting this property do not HTML-encode the value.
        /// </summary>
        public string PageHeading
        {
            get { return Server.HtmlEncode(_pageHeader); }
            set { _pageHeader = value; }
        }


        /// <summary>
        /// Gets the current user identity making the request.
        /// </summary>
        public UserIdentity UserIdentity
        {
            get { return (this.User.Identity as UserIdentity); }
        }

        /// <summary>
        /// Gets the current user making the request.
        /// </summary>
        public User CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    _currentUser = Berkley.BLC.Entities.User.GetOne("ID = ?", this.UserIdentity.UserID);
                }
                return _currentUser;
            }
        }


        /// <summary>
        /// Builds the same validation summary message presented to clients on IE
        /// and adds a JavaScript to the page so it is displayed when the page is loaded.
        /// </summary>
        public void ShowValidatorErrors()
        {
            ValidationSummary summary = FindValidationSummary(this.Controls);
            if (summary == null)
            {
                throw new Exception("Page does not contain a ValidationSummary control.");
            }
            JavaScript.ShowValidationSummaryMessage(summary);
        }


        /// <summary>
        /// Adds a script to the page that will cause an a human-readable version of a FieldValidationException to be displayed when the page loads.
        /// The page focus will also be set to the field indicated as the source of the exception.
        /// </summary>
        /// <param name="ex">Exception to show the user.</param>
        public void ShowErrorMessage(FieldValidationException ex)
        {
            JavaScript.ShowMessage(this, ex.Message);
            if (ex.Control != null)
            {
                JavaScript.SetInputFocus(ex.Control);
            }
        }

        /// <summary>
        /// Adds a script to the page that will cause a human-readable version of a ValidationException to be displayed when the page loads.
        /// The page focus will also be set to the field indicated as the source of the exception.
        /// </summary>
        /// <param name="ex">Exception to show the user.</param>
        public void ShowErrorMessage(ValidationException ex)
        {
            JavaScript.ShowMessage(this, ex.Message);
        }


        private ValidationSummary FindValidationSummary(ControlCollection controls)
        {
            // do a post-tree traversal of the control hierarchy
            ValidationSummary result = null;

            for (int i = controls.Count - 1; i >= 0; i--)
            {
                Control control = controls[i];
                if (control is ValidationSummary)
                {
                    result = (ValidationSummary)control;
                    break;
                }
                else
                {
                    // traverse into the child controls
                    result = FindValidationSummary(control.Controls);
                    if (result != null)
                    {
                        break;
                    }
                }
            }

            return result;
        }

    }
}
