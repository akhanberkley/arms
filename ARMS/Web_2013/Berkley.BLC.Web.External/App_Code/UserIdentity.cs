using System;
using System.Data;
using System.Globalization;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

using Berkley.BLC.Entities;

namespace Berkley.BLC.Web.External
{
    /// <summary>
    /// Represents the identity of a user logged into the application.
    /// </summary>
    public class UserIdentity : IIdentity
    {
        private FormsIdentity _formsIdentity;
        private string _cacheKey;
        private Guid _feeUserID;
        private Guid _userID;
        private string _username;
        private string _name;
        private string _emailAddress;
        private Guid _companyID;
        private string _companyName;

        /// <summary>
        /// Initializes a new instance of the UserIdentity class.
        /// </summary>
        /// <param name="authTicket">The authentication ticket upon which this identity is based.</param>
        /// <param name="cacheKey">Key value that can be used to remove the identity in the HTTP cache when needed.</param>
        public UserIdentity(FormsAuthenticationTicket authTicket, string cacheKey)
        {
            // get the Forms identity from the passed ticket
            _formsIdentity = new FormsIdentity(authTicket);
            _cacheKey = cacheKey;

            try
            {
                // note: the Name property contains the FeeUserID, NOT the username!!
                _feeUserID = new Guid(_formsIdentity.Name);

                // fetch the user identity info for this user from the database
                FeeUser feeUser = FeeUser.Get(_feeUserID);
                _userID = feeUser.FeeCompanyUserID;
                _username = feeUser.Username;
                _name = feeUser.Name;
                _emailAddress = feeUser.EmailAddress;
                _companyID = feeUser.FeeCompanyUser.CompanyID;
                _companyName = feeUser.FeeCompanyUser.Company.Name;
            }
            catch (Exception ex)
            {
                // rethrow a more detailed message
                throw new Exception("Failed to get identity information for fee user '" + _userID + "'.", ex);
            }
        }


        /// <summary>
        /// Gets the user of the current HTTP request, if any.
        /// </summary>
        /// <returns>Identity of the user making the request; or null if the user has no identity.</returns>
        public static UserIdentity Current
        {
            get
            {
                HttpContext context = HttpContext.Current;
                if (context != null)
                {
                    return (context.User.Identity as UserIdentity);
                }
                else
                {
                    return null;
                }
            }
        }


        #region --- IIdentity Members ---

        /// <summary>
        /// Gets a value that indicates whether the user has been authenticated.
        /// </summary>
        public bool IsAuthenticated
        {
            get { return _formsIdentity.IsAuthenticated; }
        }


        /// <summary>
        /// Gets the name of the current user.
        /// </summary>
        public string Name
        {
            get { return _name; }
        }


        /// <summary>
        /// Gets the type of authentication used.
        /// </summary>
        public string AuthenticationType
        {
            get { return _formsIdentity.AuthenticationType; }
        }


        #endregion

        /// <summary>
        /// Gets the base FormsAuthenticationTicket used to build this identity.
        /// </summary>
        internal FormsAuthenticationTicket Ticket
        {
            get { return _formsIdentity.Ticket; }
        }


        /// <summary>
        /// Gets the ID of this fee user.
        /// </summary>
        public Guid FeeUserID
        {
            get { return _feeUserID; }
        }

        /// <summary>
        /// Gets the ID of this user.
        /// </summary>
        public Guid UserID
        {
            get { return _userID; }
        }

        /// <summary>
        /// Gets the email address of this user.
        /// </summary>
        public string EmailAddress
        {
            get { return _emailAddress; }
        }

        /// <summary>
        /// Gets the ID of the company to which this user is associated.
        /// </summary>
        public Guid CompanyID
        {
            get { return _companyID; }
        }

        /// <summary>
        /// Gets the name of the company to which this user is associated.
        /// </summary>
        public string CompanyName
        {
            get { return _companyName; }
        }

        /// <summary>
        /// Gets a flag value indicating if this user has an account that can be logged out.
        /// </summary>
        public bool CanLogout
        {
            get { return true; }
        }


        /// <summary>
        /// Removes this user's principal and identity from the system cache to 
        /// fource the user's identity to be reloaded on the next request.
        /// </summary>
        public void RemoveFromCache()
        {
            // remove this user's principal object from the cache
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Cache.Remove(_cacheKey);
            }
        }
    }
}
