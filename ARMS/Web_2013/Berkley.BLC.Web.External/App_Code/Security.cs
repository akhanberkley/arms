using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

using Berkley.BLC.Entities;
using QCI.ExceptionManagement;
using QCI.Encryption;

namespace Berkley.BLC.Web.External
{
    /// <summary>
    /// Contains security related utility methods.
    /// </summary>
    public sealed class Security
    {
        private const int MAX_FAILED_LOGINS_ALLOWED = 10;
        private const int MINUTES_LOCKED_OUT = 3;

        /// <summary>
        /// Private constructor to prevent object instantiation of this class.
        /// </summary>
        private Security() { }


        /// <summary>
        /// Authenticates a user's credentials aginst information found in the database.
        /// Failed login attempts are records and the account will be locked out after a
        /// pre-defined number of failures.
        /// </summary>
        /// <param name="username">The username of the user to authenticate.</param>
        /// <param name="password">The user-entered password to be verified.</param>
        /// <param name="ipAddress">The IP Address of the client attempting to login to the system.</param>
        /// <param name="failureMessage">Message that can be displayed to the end-user to tell them why authentication failed.</param>
        /// <returns>Reference to a FeeUser object if the authenticated was successful; otherwise null.</returns>
        public static FeeUser Authenticate(string username, string password, string ipAddress, out string failureMessage)
        {
            // NOTE: Password expiration check has to happen on the login page.

            // try to authenticate the user
            try
            {
                // try to get the user record by username
                FeeUser user = FeeUser.GetOne("Username = ?", username);

                // see if the username was valid (e.g., a user record was found)
                if (user == null)
                {
                    LoginHistory login = new LoginHistory(username, ipAddress, false);
                    login.Save();
                    failureMessage = "The username and/or password entered was not valid.";
                    return null;
                }

                // see if the account is disabled
                if (user.AccountDisabled)
                {
                    RecordLoginFailure(user, ipAddress);
                    failureMessage = "Your account has been disabled. Contact your administrator for more information.";
                    return null;
                }

                // see if the account is currently locked out (too many failed attempts)
                if (user.LockedOutUntil != DateTime.MinValue && user.LockedOutUntil > DateTime.Now)
                {
                    RecordLoginFailure(user, ipAddress);
                    failureMessage = "Your account has been temporarily locked due to too many failed login attempts.\\nPlease wait a few minutes and try again.";
                    return null;
                }

                // check the password by comparing hashed values
                string passwordHash = user.CreatePasswordHash(password);
                if (passwordHash != user.PasswordHash)
                {
                    RecordLoginFailure(user, ipAddress);
                    failureMessage = "The username and/or password entered was not valid.";
                    return null;
                }

                // if we get here, user has been authenticated
                failureMessage = null;
                RecordLoginSuccess(user, ipAddress);
                return user;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to authenticate user '" + username + "'.", ex);
            }
        }


        /// <summary>
        /// Creates an authentication ticket and attaches it to a cookie in the outgoing response.
        /// Redirects the user to the page provided by the ReturnUrl query string variable, or the default specified.
        /// </summary>
        /// <param name="authedUser">User that has been authenticated.</param>
        /// <param name="persistentCookie">Flag that indicates if the auth cookie should be persistent.</param>
        /// <param name="defaultPageUrl">URL to redirect the user to if a ReturnUrl was not provided on the query string of the request.</param>
        public static void LoginUser(FeeUser authedUser, bool persistentCookie, string defaultPageUrl)
        {
            HttpContext current = HttpContext.Current;

            // create the forms auth cookie and add it to the outgoing response;
            // note: the name in the auth ticket must be the ID of the user NOT the username
            FormsAuthentication.SetAuthCookie(authedUser.ID.ToString(), persistentCookie);

            // redirect the user back to the originally page requested; or to the default if none
            string url = current.Request["ReturnUrl"];
            if (url == null || url.Length == 0)
            {
                url = defaultPageUrl;
            }

            current.Response.Redirect(url, true);
        }


        /// <summary>
        /// Generates a GenericPrincipal object, containing the IadaIdentity and role of the user,
        /// from the Forms Authentication cookie found in the request from the passed HttpContext.
        /// NOTE: The application cache is used to store GenericPrincipal objects, reducing the
        /// number of times they need to be re-created.
        /// </summary>
        /// <param name="currentContext">Current HttpContext from this instance of the application.</param>
        /// <returns></returns>
        public static void ReplaceUserPrincipal(HttpContext currentContext)
        {
            // get the Forms Auth cookie; bail if there is none
            HttpCookie authCookie = currentContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null) return;

            // build the cache key where there should be a cached version of the principal
            string cacheKey = "GenericPrincipal." + authCookie.Value;

            // try to get the GenericPrincipal from cache; create one if cache miss
            GenericPrincipal principal = (GenericPrincipal)currentContext.Cache[cacheKey];
            if (principal == null) // not in cache
            {
                // decrypt the auth ticket contained in the cookie and create a new identity
                // bail if ticket is invalid, decryption fails, or identity cannot be created
                UserIdentity identity;
                try
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    if (authTicket == null) return;

                    identity = new UserIdentity(authTicket, cacheKey);
                }
                catch (Exception ex) // ticket is not valid or failed to be decrypted, or identity could not be loaded
                {
                    // throw away the user's auth ticket - there could might something wrong with it
                    FormsAuthentication.SignOut();

                    // record this exception (possible security issue) and bail out
                    ExceptionManager.Publish(ex);
                    return;
                }

                // create a new prinpical object based on the identity and role
                //string role = ConvertFromSecurityRole(identity.Role);
                principal = new GenericPrincipal(identity, new string[0]);

                // cache the new prinpical object, set in to expire at the same time as the ticket
                currentContext.Cache.Insert(cacheKey, principal, null, identity.Ticket.Expiration, TimeSpan.Zero);
            }

            // replace the current principal with a new one for this user
            currentContext.User = principal;
        }


        private static void RecordLoginSuccess(FeeUser user, string ipAddress)
        {
            LoginHistory login = new LoginHistory(user.Username, ipAddress, true);
            login.Save();

            user = user.GetWritableInstance();
            user.FailedLoginAttempts = 0;
            user.LockedOutUntil = DateTime.MinValue;
            user.Save();
        }

        private static void RecordLoginFailure(FeeUser user, string ipAddress)
        {
            LoginHistory login = new LoginHistory(user.Username, ipAddress, false);
            login.Save();

            // increment the failed attempts count
            user = user.GetWritableInstance();

            // lock them out for a while if they have too many failed attempts
            user.FailedLoginAttempts += 1;
            if (user.FailedLoginAttempts >= MAX_FAILED_LOGINS_ALLOWED)
            {
                user.LockedOutUntil = DateTime.Now.AddMinutes(MINUTES_LOCKED_OUT);
                user.FailedLoginAttempts = 0;
            }

            user.Save();
        }
    }
}
