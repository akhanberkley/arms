using System;
using System.ComponentModel;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace QCI.Web.Controls
{
    /// <summary>
    /// Displays a read-only textbox with a interactive popup calendar on client browsers
    /// which allows users to easily select dates.
    /// </summary>
    [DefaultProperty("Date"), ParseChildren(false), ValidationProperty("Text"), DefaultEvent("DateChanged")]
    public class DatePicker : WebControl, IPostBackDataHandler
    {
        private static string _resourcePath = null;

        private EventHandler _dateChanged;

        /// <summary>
        /// Creates a new instance of this control.
        /// </summary>
        public DatePicker()
        {
            // make sure the path to the resource files is defined
            if (_resourcePath == null)
            {
                // get the appliction path
                string appPath = HttpContext.Current.Request.ApplicationPath;
                if (appPath == "/") appPath = null;

                // get the relative path and combine with app path
                string path = ConfigurationManager.AppSettings["DatePicker.ResourcePath"];
                if (path != null && path.Length > 0)
                {
                    if (!path.StartsWith("/")) path = "/" + path;
                    if (!path.EndsWith("/")) path += "/";
                    _resourcePath = appPath + path;
                }
                else // no path defined
                {
                    _resourcePath = appPath + "/datepicker/";
                }

                _resourcePath = _resourcePath.ToLower();
            }
        }


        #region --- Properties ---

        /// <summary>
        /// Gets the current date as a string.
        /// </summary>
        public string Text
        {
            get
            {
                //NOTE: This property was added as a hack to get the CompareValidator to work with the control.
                //		Assiging the ValidationProperty attrib to "Date" causes the validator to think the date is always invalid.
                //		This probably has to do with the way it parses the date after calling ToString() on the date value retreived from this control.
                DateTime date = this.Date;
                return (date != DateTime.MinValue) ? this.Date.ToShortDateString() : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the selected date.  DateTime.MinValue indicates no date.
        /// </summary>
        public DateTime Date
        {
            get
            {
                object value = ViewState["Date"];
                return (value != null) ? (DateTime)value : DateTime.MinValue;
            }
            set
            {
                ViewState["Date"] = (value != DateTime.MinValue) ? (object)value.Date : null;
            }
        }


        /// <summary>
        /// Gets or sets the format of the date.
        /// </summary>
        public string DateFormat
        {
            get
            {
                object value = ViewState["DateFormat"];
                return (value != null) ? (string)value : "m/d/yyyy";
            }
            set
            {
                ViewState["DateFormat"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the day of the week to display in the first day column of the calendar view.
        /// </summary>
        public DayOfWeek FirstDayOfWeek
        {
            get
            {
                object value = ViewState["FirstDayOfWeek"];
                return (value != null) ? (DayOfWeek)value : CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
            }
            set
            {
                ViewState["FirstDayOfWeek"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the minimum date that can be selected by the user.
        /// </summary>
        public DateTime MinDate
        {
            get
            {
                object value = ViewState["MinDate"];
                return (value != null) ? (DateTime)value : DateTime.MinValue;
            }
            set
            {
                ViewState["MinDate"] = value;
                //ViewState["MaxDate"] = (value != DateTime.MaxValue) ? (object)value : null;
            }
        }


        /// <summary>
        /// Gets or sets the maximum date that can be selected by the user.
        /// </summary>
        public DateTime MaxDate
        {
            get
            {
                object value = ViewState["MaxDate"];
                return (value != null) ? (DateTime)value : DateTime.MaxValue;
            }
            set
            {
                ViewState["MaxDate"] = (value != DateTime.MaxValue) ? (object)value : null;
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether a postback to the server automatically occurs when the user changes the selected date.
        /// </summary>
        public bool AutoPostBack
        {
            get
            {
                object value = ViewState["AutoPostBack"];
                return (value != null) ? (bool)value : false;
            }
            set
            {
                ViewState["AutoPostBack"] = value;
            }
        }


        /// <summary>
        /// Gets or sets the display width of the text box in characters.
        /// </summary>
        public virtual int Columns
        {
            get
            {
                object value = this.ViewState["Columns"];
                return (value != null) ? (int)value : 0;
            }
            set
            {
                if (value < 0) throw new ArgumentOutOfRangeException("value");
                this.ViewState["Columns"] = value;
            }
        }


        #endregion

        /// <summary>
        /// Occurs when the date selected in the picker changed and the page is posted back.
        /// </summary>
        public event EventHandler DateChanged
        {
            add { _dateChanged += value; }
            remove { _dateChanged -= value; }
        }


        /// <summary>
        /// Processes post back data for an ASP.NET server control.
        /// </summary>
        /// <param name="postDataKey">The key identifier for the control.</param>
        /// <param name="values">The collection of all incoming name values.</param>
        /// <returns>True if the server control's state changes as a result of the post back; otherwise false.</returns>
        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection values)
        {
            DateTime presentValue = this.Date;
            string postedString = values[postDataKey];

            DateTime postedValue;
            if (postedString != null && postedString.Length > 0)
            {
                try
                {
                    postedValue = DateTime.Parse(postedString);
                }
                catch // invalid date string
                {
                    postedValue = DateTime.MinValue;
                }
            }
            else // blank
            {
                postedValue = DateTime.MinValue;
            }

            if (presentValue != postedValue)
            {
                this.Date = postedValue;
                return true;
            }

            return false;
        }


        /// <summary>
        /// Signals the server control object to notify the ASP.NET application that the state of the control has changed.
        /// </summary>
        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            OnDateChanged(EventArgs.Empty);
        }


        /// <summary>
        /// Raises the DateChanged event.
        /// </summary>
        /// <param name="e">An EventArgs object that contains the event data.</param>
        protected virtual void OnDateChanged(EventArgs e)
        {
            if (_dateChanged != null)
            {
                _dateChanged(this, e);
            }
        }


        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An EventArgs object that contains the event data.</param>
        /// <remarks>This method notifies the server control to perform any necessary prerendering steps prior to saving view state and rendering content.</remarks>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // --- From TextBox Control ---
            //if (!this.SaveTextViewState)
            //{
            //	this.ViewState.SetItemDirty("Text", false);
            //}

            // verify date is between min and max (when specified)
            if (this.MaxDate != DateTime.MaxValue && this.MinDate != DateTime.MinValue) // both specified
            {
                if (this.MinDate >= this.MaxDate)
                {
                    throw new Exception("MaxDate value cannot be on or before MinDate value.");
                }
            }

            if (!this.Page.ClientScript.IsClientScriptBlockRegistered("DatePickerStyles"))
            {
                string link = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + _resourcePath + "DatePicker.css\">";
                this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), "DatePickerStyles", link);
            }

            if (!this.Page.ClientScript.IsStartupScriptRegistered("DatePickerBlock"))
            {
                string script = GetClientHtmlBlock();
                this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), "DatePickerBlock", script);
            }

            if (this.Page != null && this.AutoPostBack)
            {
                this.Page.RegisterRequiresPostBack(this);
            }
        }


        /// <summary>
        /// Sends server control content to a provided HtmlTextWriter object, which writes the 
        /// content to be rendered on the client.
        /// </summary>
        /// <param name="writer">The HtmlTextWriter object that receives the server control content.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            if (this.Page != null)
            {
                this.Page.VerifyRenderingInServerForm(this);
            }

            // ----------------------------------------------------------------
            // add a readonly input box to the output
            // ----------------------------------------------------------------
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
            //writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly, "readonly");
            writer.AddAttribute(HtmlTextWriterAttribute.Name, this.UniqueID);

            string text = (this.Date != DateTime.MinValue ? this.Date.ToShortDateString() : "");
            if (text.Length > 0)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Value, text);
            }

            int cols = this.Columns;
            if (cols > 0)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Size, cols.ToString(NumberFormatInfo.InvariantInfo));
            }

            string onClickFunction = null;
            if (this.Page != null && this.Enabled)
            {
                onClickFunction = string.Format("ShowDatePicker('{0}', '{1}', {2}, {3}, {4});",
                    this.ClientID, this.DateFormat, (int)this.FirstDayOfWeek, GetNewDateScript(this.MinDate), GetNewDateScript(this.MaxDate));
                //writer.AddAttribute(HtmlTextWriterAttribute.Onclick, onClickFunction);
            }

            if (this.Page != null && this.AutoPostBack)
            {
                string onChangeFunction = this.Page.ClientScript.GetPostBackEventReference(this, "");
                if (base.Attributes.Count > 0)
                {
                    string baseFunction = base.Attributes["onchange"];
                    if (baseFunction != null)
                    {
                        onChangeFunction = baseFunction + onChangeFunction;
                        base.Attributes.Remove("onchange");
                    }
                }
                writer.AddAttribute(HtmlTextWriterAttribute.Onchange, onChangeFunction);
            }

            base.AddAttributesToRender(writer);

            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            // ----------------------------------------------------------------
            // add an image button to the output
            // ----------------------------------------------------------------
            writer.AddAttribute(HtmlTextWriterAttribute.Src, _resourcePath + "calendar.gif");
            writer.AddAttribute(HtmlTextWriterAttribute.Alt, "Show Calendar");
            writer.AddAttribute(HtmlTextWriterAttribute.Align, "absmiddle");
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "MARGIN-LEFT: 2px");
            if (onClickFunction != null)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Onclick, onClickFunction);
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Img);
            writer.RenderEndTag();
        }


        private string GetNewDateScript(DateTime date)
        {
            if (date == DateTime.MinValue || date == DateTime.MaxValue)
            {
                return "null";
            }
            else
            {
                return string.Format("new Date({0},{1},{2})", date.Year, date.Month - 1, date.Day);
            }
        }


        private string GetClientHtmlBlock()
        {
            StringBuilder sb = new StringBuilder(1000);

            //TODO: consider removing the appends and making it one multi-line string
            sb.Append("<div id=\"DP_MainDiv\" style=\"POSITION: absolute; VISIBILITY: hidden; Z-INDEX: 999\">\n");
            sb.Append("<table>\n");
            sb.Append("<tr>\n");
            sb.Append("	<td>\n");
            sb.Append("		<table class=\"DP_Header\">\n");
            sb.Append("		<tr>\n");
            sb.Append("			<td>\n");
            sb.Append("				<span id=DP_LeftSpan class=\"DP_controlBox\" onclick='DP_DecMonth()' ondblclick='DP_DecMonth()' onmousedown='DP_OnMonthArrowDown(-1)' onmouseup='DP_OnMonthArrowUp(-1)' onmouseover='DP_PickListOver(this)' onmouseout='DP_PickListOut(this)'>\n");
            sb.AppendFormat("					&nbsp;<img id=DP_LeftImg src=\"{0}left.gif\" width=10 height=11 border=0>&nbsp;</span>&nbsp;\n", _resourcePath);
            sb.Append("				<span id=DP_RightSpan class=\"DP_controlBox\" onclick='DP_IncMonth()' ondblclick='DP_IncMonth()' onmousedown='DP_OnMonthArrowDown(1)' onmouseup='DP_OnMonthArrowUp(1)' onmouseover='DP_PickListOver(this)' onmouseout='DP_PickListOut(this)'>\n");
            sb.AppendFormat("					&nbsp;<img id=DP_RightImg src=\"{0}right.gif\" width=10 height=11 border=0>&nbsp;</span>&nbsp;\n", _resourcePath);
            sb.Append("				<span id=DP_MonthSpan class=\"DP_controlBox\" onclick='DP_ShowMonthList(this)' onmouseover='DP_PickListOver(this)' onmouseout='DP_PickListOut(this)'></span>&nbsp;\n");
            sb.Append("				<span id=DP_YearSpan class=\"DP_controlBox\" onclick='DP_ShowYearList(this)' onmouseover='DP_PickListOver(this)' onmouseout='DP_PickListOut(this)'></span>\n");
            sb.Append("			</td>\n");
            sb.Append("			<td align=right>\n");
            sb.AppendFormat("				<a href=\"javascript:HideDatePicker()\"><img src=\"{0}close.gif\" width=15 height=13 border=0 alt=\"Close\"></a>\n", _resourcePath);
            sb.Append("			</td>\n");
            sb.Append("		</tr>\n");
            sb.Append("		</table>\n");
            sb.Append("	</td>\n");
            sb.Append("</tr>\n");
            sb.Append("<tr>\n");
            sb.Append("	<td>\n");
            sb.Append("		<div id=\"DP_CalendarDiv\"></div>\n");
            sb.Append("	</td>\n");
            sb.Append("</tr>\n");
            sb.Append("<tr>\n");
            sb.Append("	<td>\n");
            sb.Append("		<table class=\"DP_Footer\">\n");
            sb.Append("		<tr>\n");
            sb.AppendFormat("			<td><a href=\"javascript:DP_PickDay('today')\">{0}</a></td>\n", "Today");
            sb.AppendFormat("			<td><a href=\"javascript:DP_PickDay('none')\">{0}</a></td>\n", "None");
            sb.Append("		</tr>\n");
            sb.Append("		</table>\n");
            sb.Append("	</td>\n");
            sb.Append("</tr>\n");
            sb.Append("</table>\n");
            sb.Append("</div>\n");
            sb.Append("<div id=\"DP_PickListDiv\" style=\"POSITION: absolute; VISIBILITY: hidden; Z-INDEX: 999\"></div>\n");
            sb.Append("<script language=\"javascript\">\n");
            sb.AppendFormat("var _dpResPath = '{0}';\n", _resourcePath);
            sb.Append("var _dpMonthNames = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');\n");
            sb.Append("var _dpDayNames = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');\n");
            sb.Append("</script>\n");
            sb.AppendFormat("<script language=\"javascript\" type=\"text/javascript\" src=\"{0}DatePicker.js\"></script>\n", _resourcePath);

            return sb.ToString();
        }
    }
}
