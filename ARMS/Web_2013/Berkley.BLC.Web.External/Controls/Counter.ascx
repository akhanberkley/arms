<%@ Control Language="C#" AutoEventWireup="false" CodeFile="Counter.ascx.cs" Inherits="Berkley.BLC.Web.External.Controls.Counter" %>

<asp:TextBox ID="txt" Runat="server" CssClass="txt" Columns="5" MaxLength="5" />
<input type="button" value="<" id="btnDown" runat="server" class="counter" />
<input type="button" value=">" id="btnUp" runat="server" class="counter" />

 <script language="javascript">
function Incriment(id, incrimentBy)
{
    var txt = document.getElementById(id);
    var intValue = parseInt(txt.value);
    
    if( txt.value.length > 0 && !isNaN(intValue) ) {
		txt.value = intValue + incrimentBy;
	}
	else {
	    txt.value = 0;
	}  
}
</script>
