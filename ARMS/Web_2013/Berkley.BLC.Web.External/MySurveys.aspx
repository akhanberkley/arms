﻿<%@ Page Language="C#" AutoEventWireup="false"  CodeFile="MySurveys.aspx.cs" Inherits="Berkley.BLC.Web.External.MySurveysAspx" %>
<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.External.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><%= base.PageTitle %></title>
</head>
<body style="margin:0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css" />
    <form id="frm" method="post" runat="server">
    <uc:Header ID="ucHeader" runat="server" />
    
    <span class="heading"><%= base.PageHeading %></span><br><br>
    
	<cc:Box id="boxAssignments" runat="server" Title="Assignments" width="210">
	    <table class="fields">
		    <tr class="header">
			    <td><a href="surveys.aspx?function=newwork" style="MARGIN-RIGHT: 20px">New Work</a></td>
			    <td><%=miNewWork%></td>
		    </tr>
		    <tr class="header">
			    <td><a href="surveys.aspx?function=corrections" style="MARGIN-RIGHT: 20px">Corrections</a></td>
			    <td><%=miCorrections%></td>
		    </tr>
            <% if(_vendorHideReturntoCompany == false) { %>
		    <tr class="header">
			    <td><a href="surveys.aspx?function=returntocompany" style="MARGIN-RIGHT: 20px">Return To Company</a></td>
			    <td><%=miReturnedToCompany%></td>
		    </tr>
            <% } %>
		    <tr class="header">
			    <td><a href="surveys.aspx?function=allassignments" style="MARGIN-RIGHT: 20px">All Assigned</a></td>
			    <td><%=_assignedCount%></td>
		    </tr>
            <% if (_vendorHideCompletedPast6Months == false)
               { %>
		    <tr class="header">
			    <td><a href="surveys.aspx?function=completed" style="MARGIN-RIGHT: 20px">Completed - Past 6 Months</a></td>
			    <td><%=_completed%></td>
		    </tr>
               <% } %>
	    </table>
    </cc:Box>

    <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
