<%@ Control Language="C#" AutoEventWireup="false" CodeFile="TextField.ascx.cs" Inherits="Berkley.BLC.Web.Internal.Fields.TextField" %>
<% if (base.TableRow) {%><tr><% } %>
<td class="label" valign="<%= (txt.Rows > 1) ? "top" : "middle" %>" nowrap>
	<asp:Label ID="lbl" Runat="server" /><%= (this.IsRequired) ? " *" : "" %>
</td>
<td>
	<asp:TextBox ID="txt" Runat="server" CssClass="txt" />
	<asp:RequiredFieldValidator ID="val" Runat="server" CssClass="val" ControlToValidate="txt" Display="Dynamic" />
	<asp:RegularExpressionValidator ID="valRegex" Runat="server" CssClass="val" ControlToValidate="txt" Display="Dynamic" />
    <% if (Directions.Length > 0) {%>
    &nbsp;<asp:Label ID="lblDirections" Runat="server" />
    <% } %>
</td>
<% if (base.TableRow) {%></tr><% } %>