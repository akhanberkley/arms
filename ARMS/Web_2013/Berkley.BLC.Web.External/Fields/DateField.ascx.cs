using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Berkley.BLC.Entities;
using QCI.Web.Validation;

namespace Berkley.BLC.Web.Internal.Fields
{
    public partial class DateField : MappedFieldControl
    {
        protected bool noEndRow;

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Control_Load);
            this.PreRender += new EventHandler(this.Control_PreRender);
        }
        #endregion

        private void Control_Load(object sender, System.EventArgs e)
        {
        }

        private void Control_PreRender(object sender, EventArgs e)
        {
            if (lbl.Text.Length == 0)
            {
                lbl.Text = this.GetDefaultLabelText();
            }
            
            Utility.AddJavaScriptToPage(this.Page, Utility.AppPath + "/AutoFormat.js");

            val.Visible = this.IsRequired;
            if (val.Visible)
            {
                val.Text = "*";
                val.ErrorMessage = string.Format("{0} is required.", base.Name);
            }

            valType.Text = "*";
            valType.ErrorMessage = string.Format("{0} is not a valid date.", base.Name);

            valMin.Visible = true;
            if (this.MinDate != DateTime.MinValue)
            {
                valMin.ValueToCompare = this.MinDate.ToShortDateString();
                valMin.Text = "*";
                valMin.ErrorMessage = string.Format("{0} must be on or after {1:d}.", base.Name, this.MinDate);
            }
            else // no defined lower bound
            {
                valMin.ValueToCompare = new DateTime(1800, 1, 1).ToShortDateString();
                valMin.Text = "*";
                valMin.ErrorMessage = string.Format("{0} is too far in the past.", base.Name);
            }

            valMax.Visible = (this.MaxDate != DateTime.MaxValue);
            if (valMax.Visible)
            {
                valMin.ValueToCompare = this.MaxDate.ToShortDateString();
                valMax.Text = "*";
                valMax.ErrorMessage = string.Format("{0} must be on or before {1:d}.", base.Name, this.MaxDate);
            }
        }


        /// <summary>
        /// Gets the internal control responsible for capturing user input.
        /// </summary>
        public override Control InputControl
        {
            get { return dp; } //TODO: consider adding support for TabIndex to DatePicker
        }


        /// <summary>
        /// Gets or sets the selected date.
        /// </summary>
        public DateTime Date
        {
            get { return dp.Date; }
            set { dp.Date = value.Date; }
        }

        /// <summary>
        /// Gets or sets the minimum date that can be selected by the user.
        /// </summary>
        public DateTime MinDate
        {
            get { return dp.MinDate; }
            set { dp.MinDate = value; }
        }

        /// <summary>
        /// Gets or sets the maximum date that can be selected by the user.
        /// </summary>
        public DateTime MaxDate
        {
            get { return dp.MaxDate; }
            set { dp.MaxDate = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a postback to the server automatically occurs when the user changes the selected date.
        /// </summary>
        public bool AutoPostBack
        {
            get { return dp.AutoPostBack; }
            set { dp.AutoPostBack = value; }
        }

        /// <summary>
        /// Gets or sets the display width of the input box in characters.
        /// </summary>
        public int Columns
        {
            get { return dp.Columns; }
            set { dp.Columns = value; }
        }

        /// <summary>
        /// Gets or sets the maximum number of characters allowed in the input box.
        /// </summary>
        public int MaxLength
        {
            get { return txt.MaxLength; }
            set { txt.MaxLength = value; }
        }

        /// <summary>
        /// Gets or sets the label value for this control.  This value will appear to the left of the input field.
        /// </summary>
        public string Label
        {
            get { return this.lbl.Text; }
            set { this.lbl.Text = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the control is enabled.
        /// </summary>
        public bool Enabled
        {
            get { return dp.Enabled; }
            set { dp.Enabled = value; }
        }

        /// <summary>
        /// Gets or sets a flag indicating to suspend injecting a end row tag.
        /// </summary>
        public bool NoEndTR
        {
            get { return noEndRow; }
            set { noEndRow = (bool)value; }
        }


        /// <summary>
        /// Gets the user input value from the control.
        /// </summary>
        /// <returns>Value from the control.</returns>
        public override object GetValue()
        {
            return this.Date;
        }

        /// <summary>
        /// Sets the user input value of the control.
        /// </summary>
        /// <param name="value">Value to be set.</param>
        public override void SetValue(object value)
        {
            this.Date = Convert.ToDateTime(value);
        }

    }
}
