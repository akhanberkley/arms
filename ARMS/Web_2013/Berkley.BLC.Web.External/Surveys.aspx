﻿<%@ Page Language="C#" AutoEventWireup="false" CodeFile="Surveys.aspx.cs" Inherits="Berkley.BLC.Web.External.SurveysAspx" %>

<%@ Register TagPrefix="uc" TagName="Header" Src="~/Template/PageHeader.ascx" %>
<%@ Register TagPrefix="uc" TagName="Footer" Src="~/Template/PageFooter.ascx" %>
<%@ Register TagPrefix="cc" Namespace="Berkley.BLC.Web.External.Controls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><%= base.PageTitle%></title>
</head>
<body style="margin: 0px">
    <link rel="stylesheet" type="text/css" href="<%= base.TemplatePath %>/styles.css" />
    <form id="frm" method="post" runat="server">
        <uc:Header ID="ucHeader" runat="server" />

        <span class="heading"><%= base.PageHeading %></span><br>
        <br>

        <% if (_recordCount == 1)
           { %>
    (1 survey)<br>
        <% }
           else if (_recordCount > 0)
           { %>
    (<%= _recordCount %> surveys)<br>
        <% } %>

        <table>
            <tr>
                <td colspan="2">
                    <asp:DataGrid ID="grdItemz" runat="server" CssClass="grid" AutoGenerateColumns="False" AllowPaging="True" PageSize="100" AllowSorting="True">
                        <HeaderStyle CssClass="header" VerticalAlign="Top" />
                        <ItemStyle CssClass="item" />
                        <AlternatingItemStyle CssClass="altItem" />
                        <FooterStyle CssClass="footer" />
                        <PagerStyle CssClass="pager" Mode="NumericPages" />
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="label" valign="top" nowrap>Items Per Page:</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlItemsPerPage" runat="server" AutoPostBack="True" CssClass="cbo">
                                    <asp:ListItem Value="20">20</asp:ListItem>
                                    <asp:ListItem Value="50">50</asp:ListItem>
                                    <asp:ListItem Value="100" Selected="True">100</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
                <td id="tdButtonCol" runat="server" align="right" visible="false">
                    <asp:Button ID="btnAcceptWork" runat="server" CssClass="btn" Width="140px" Text="Accept all New Work" OnClick="btnAcceptWork_Click"></asp:Button>
                </td>
                <td id="tdDueDateCol" runat="server" align="right" visible="false">
                    <table>
                        <tr>
                            <td class="label" valign="top" nowrap>Due Date within:</td>
                            <td align="left">
                                <asp:DropDownList ID="cboDueDateDays" runat="server" AutoPostBack="True" CssClass="cbo">
                                    <asp:ListItem Value="90">90 days</asp:ListItem>
                                    <asp:ListItem Value="" Selected="True">(All)</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <uc:Footer ID="ucFooter" runat="server" />
    </form>
</body>
</html>
