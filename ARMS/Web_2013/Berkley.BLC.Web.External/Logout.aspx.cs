using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Berkley.BLC.Web.External
{
    public partial class LogoutAspx : AppBasePage
    {
        #region Event Handlers
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(LogoutAspx_Load);
        }
        #endregion

        void LogoutAspx_Load(object sender, EventArgs e)
        {
            // remove the user's auth ticket/cookie
            FormsAuthentication.SignOut();

            // redirect user to the login page
            string loginUrl = System.Configuration.ConfigurationManager.AppSettings["LoginUrl"];
            if (loginUrl == null) throw new Exception("Setting 'LoginUrl' must be specified in the application config file.");

            this.Response.Redirect(loginUrl, true);
        }

    }
}
