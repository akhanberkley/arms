﻿/// <reference path="app.ts" />
module wfa {
    'use strict';
    
    export class DatabaseOption {
        name: string;
        databases: string[];
    }

    export class Exception {
        Expanded: boolean;
        Database: string;
        LoggedDate: any;
        ApplicationName: string;
        ApplicationEnvironment: string;
        ExceptionInformation: any;
        StackTrace: string;
        ExceptionContext: any;
        WebRequestData: any;
    }
} 