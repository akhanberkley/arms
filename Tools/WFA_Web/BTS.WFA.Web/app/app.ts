﻿/// <reference path="../scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../scripts/typings/angular-ui/angular-ui-router.d.ts" />
/// <reference path="../scripts/typings/moment/moment.d.ts" />
/// <reference path="../scripts/typings/jquery.ui.datetimepicker/jquery.ui.datetimepicker.d.ts" />
module wfa {
    'use strict';
    //Setup angular
    export var app = angular.module('wfa', ['ui.router', 'ngAnimate', 'ngSanitize']);

    app.config(function ($locationProvider: ng.ILocationProvider) {
        $locationProvider.html5Mode(true).hashPrefix('!');
    });

    app.config(function ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) {
        $urlRouterProvider.otherwise("/");
        //Always remove trailing slash
        $urlRouterProvider.rule(function ($injector: ng.auto.IInjectorService, $location: ng.ILocationService) {
            var path = $location.path();
            if (path != '/' && path.slice(-1) === '/') {
                $location.replace().path(path.slice(0, -1));
            }
        });

        $stateProvider
            .state('index', {
            url: "/",
            controller: function ($state: ng.ui.IStateService) {
                $state.go('exceptions', {}, { location: 'replace' });
            }
        }).state('exceptions', {
            url: "/exceptions",
            template: '<exceptions></exceptions>'
        }).state('exceptions.exception', {
            url: "/{db}/{id}",
            templateProvider: function ($stateParams: ng.ui.IStateParamsService) {
                return `<exception id="'${$stateParams['id']}'" db="'${$stateParams['db']}'"></exception>`;
            }
        });
    });
}