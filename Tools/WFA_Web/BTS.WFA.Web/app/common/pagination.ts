﻿/// <reference path="../app.ts" />
module wfa {
    'use strict';
    app.directive('pagination', function ($rootScope: ng.IRootScopeService, $timeout: ng.ITimeoutService): ng.IDirective {
        return {
            replace: true,
            restrict: 'E',
            templateUrl: '/app/common/pagination.html',
            scope: {
                Source: '=source'
            },
            bindToController: true,
            controller: PaginationController,
            controllerAs: 'vm'
        };
    });

    export class PagedDataSource {
        page: number;
        items: any[];

        constructor(public pageSize: number, public array: any[]) {
            this.pageSize = pageSize || 10;
            this.changePage(1);
        }

        changePage(page: number) {
            this.page = page;

            var startIndex = (this.page - 1) * this.pageSize;
            this.items = this.array.slice(startIndex, startIndex + this.pageSize);
        }
    }

    class PaginationController {
        Source: PagedDataSource;
        Pages: number[];
        TotalPages: number;

        constructor($scope: ng.IScope) {
            this.TotalPages = 1;
            this.Pages = [];

            $scope.$watch(() => this.Source,(source) => {
                if (this.Source != null) {
                    var totalPages = (this.Source.pageSize < 1) ? 1 : Math.ceil(this.Source.array.length / this.Source.pageSize);
                    this.TotalPages = Math.max(totalPages || 0, 1);

                    this.CalculatePages();
                }
            });
        }

        CalculatePages() {
            var maxPages = 6;
            var pages = <number[]>[];
            var startPage = Math.max(this.Source.page - Math.floor(maxPages / 2), 1);
            var endPage = startPage + maxPages - 1;
            if (endPage > this.TotalPages) {
                endPage = this.TotalPages;
                startPage = Math.max(endPage - maxPages + 1, 1);
            }

            for (var i = startPage; i <= endPage; i++) {
                pages.push(i);
            }

            this.Pages = pages;
        }

        SelectPage(page: number) {
            if (this.Source.page !== page && page <= this.TotalPages) {
                this.Source.changePage(page);
                this.CalculatePages();
            }
        }

    }
} 