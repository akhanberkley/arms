﻿/// <reference path="../app.ts" />
module wfa {
    'use strict';
    app.directive('datePicker', function ($window: ng.IWindowService, $filter: ng.IFilterService): ng.IDirective {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                'options': '=datePicker'
            },
            link: function (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes, ngModelCtrl: ng.INgModelController) {
                var shortDateRegex = /^\d{1,2}\/\d{1,2}\/\d{2}$/;
                var longDateRegex = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                var formatter = <ng.IModelFormatter>function (value) {
                    return $filter('date')(value, 'MM/dd/yyyy');
                };

                ngModelCtrl.$formatters.push(formatter);
                ngModelCtrl.$parsers.push(function (value) {
                    if (value == null || !longDateRegex.test(value)) {
                        return null;
                    } else {
                        var m = moment(value, 'MM/DD/YYYY');
                        return (m.isValid()) ? m.toDate() : null;
                    }
                });

                element.on('blur', function () {
                    if (shortDateRegex.test(ngModelCtrl.$viewValue)) {
                        var m = moment(ngModelCtrl.$viewValue, 'MM/DD/YY');
                        if (m.isValid()) {
                            ngModelCtrl.$setViewValue(formatter(m.toDate()));
                        }
                    }

                    element.val(formatter(ngModelCtrl.$modelValue));
                });

                var manuallyFocused = false;
                var options = <JQueryUI.DatepickerOptions>(scope['options'] || {});
                options.dateFormat = 'mm/dd/yy';
                options.onSelect = function (date) {
                    ngModelCtrl.$setViewValue(date);
                    manuallyFocused = true;
                    scope.$digest();
                    element.focus();
                };
                //Prevent IE from re-opening datepicker if we manually focused it after calendar click
                if ($window.navigator.userAgent.indexOf('MSIE ') >= 0 || $window.navigator.userAgent.indexOf('Trident/') >= 0) {
                    options.beforeShow = function (input, inst) {
                        return (manuallyFocused) ? manuallyFocused = false : true;
                    };
                }

                element.datepicker(options);
            }
        };
    });
}  