﻿/// <reference path="../app.ts" />
module wfa {
    'use strict';
    app.directive('exceptions',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/exceptions.html',
            replace: true,
            controller: ExceptionsController,
            controllerAs: 'vm'
        }
    });

    class ExceptionsController {
        Databases: DatabaseOption[];
        PageLoaded: boolean;
        Searching: boolean;
        ItemsPerPage: number;

        ExceptionFrom: any;
        ExceptionTo: any;
        SelectedDatabase: DatabaseOption;

        Exceptions: Exception[];
        PagedExceptions: PagedDataSource;

        constructor($window: ng.IWindowService, private ExceptionService: ExceptionService, private $state: ng.ui.IStateService, private $http: ng.IHttpService, private $q: ng.IQService) {
            var today = moment().toDate();
            today.setHours(0, 0, 0, 0);
            this.ExceptionFrom = today;
            
            var maxPixelHeight = $window.innerHeight - 225;//approx. starting point
            this.ItemsPerPage = Math.max(Math.floor(maxPixelHeight / 92), 3);

            $http.get('/api/Database').then((result: ng.IHttpPromiseCallbackArg<string[]>) => {
                this.Databases = [];
                this.Databases.push({ name: 'All', databases: result.data });
                result.data.forEach((db) => {
                    this.Databases.push({ name: db, databases: [db] });
                });

                this.SelectedDatabase = this.Databases[0];
                this.PageLoaded = true;

                if (!$state.is('exceptions.exception')) {
                    this.Search();
                }
            });
        }

        Search() {
            this.$state.go('exceptions');
            this.Searching = true;
            this.Exceptions = [];
            var requests = <ng.IPromise<any>[]>[];
            this.SelectedDatabase.databases.forEach((db) => {
                requests.push(this.ExceptionService.GetExceptions(db, { From: this.ExceptionFrom, To: this.ExceptionTo }).then((results) => {
                    this.Exceptions = this.Exceptions.concat(results);
                }));
            });

            this.$q.all(requests).then(() => {
                this.Exceptions.sort((a: Exception, b: Exception) => b.LoggedDate - a.LoggedDate);
                this.PagedExceptions = new PagedDataSource(this.ItemsPerPage, this.Exceptions);

                this.Searching = false;
            });
        }
    }
}