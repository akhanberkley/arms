﻿/// <reference path="../app.ts" />
module wfa {
    'use strict';
    app.directive('exception',() => {
        return <ng.IDirective>{
            restrict: 'E',
            templateUrl: '/app/components/exception.html',
            scope: {
                Id: '=id',
                Database: '=db',
                Item: '=item'
            },
            replace: true,
            bindToController: true,
            controller: ExceptionController,
            controllerAs: 'vm'
        }
    });

    class ExceptionController {
        Id: number;
        Database: string;
        Item: Exception;

        Exception: Exception;

        constructor(ExceptionService: ExceptionService, private $state: ng.ui.IStateService) {
            if (this.Id != null && this.Database != null) {
                ExceptionService.GetExceptions(this.Database, { Id: this.Id }).then((results) => {
                    this.Exception = results[0];
                });
            } else {
                this.Exception = this.Item;
            }
        }

        ToggleVisibility() {
            this.Exception.Expanded = !this.Exception.Expanded;
        }

        ShowDirectLink() {
            return !this.$state.is('exceptions.exception');
        }
    }
}