﻿/// <reference path="../app.ts" />
module wfa {
    'use strict';
    app.filter('decodeNewline', function () {
        return function (val: string) {
            if (val)
                return val.replace(/\\r\\n/g, '\n').replace(/\\n/g, '\n');
            return val;
        };
    });
} 