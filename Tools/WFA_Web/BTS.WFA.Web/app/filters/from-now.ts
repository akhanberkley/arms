﻿/// <reference path="../app.ts" />
module wfa {
    'use strict';
    app.filter('fromNow', function () {
        return function (date: any) {
            return moment(date).fromNow();
        };
    });
} 