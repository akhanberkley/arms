﻿/// <reference path="../app.ts" />
module wfa {
    'use strict';
    export interface ExceptionSearchCriteria {
        Id?: number;
        From?: any;
        To?: any;
    }

    export class ExceptionService {
        TimezoneOffset = -60;

        constructor(private $http: ng.IHttpService, private $filter: ng.IFilterService) { }

        GetExceptions(db: string, criteria: ExceptionSearchCriteria) {
            var query = <string[]>[];

            if (criteria.Id) {
                query.push(`Id eq ${criteria.Id}`);
            }
            if (criteria.From) {
                query.push("LoggedDate ge " + this.$filter('date')(criteria.From, 'yyyy-MM-ddTHH:mm:ss') + 'Z');
            }
            if (criteria.To) {
                query.push("LoggedDate le " + this.$filter('date')(moment(criteria.To).add(1, 'day').toDate(), 'yyyy-MM-ddTHH:mm:ss') + 'Z');
            }

            var filter = '?$orderby=LoggedDate desc';
            if (query.length > 0) {
                filter += '&$filter=' + query.join(' and ');
            }

            return this.$http.get(`/api/Database/${db}/Exception${filter}`).then((result: ng.IHttpPromiseCallbackArg<Exception[]>) => {
                result.data.forEach((e) => {
                    e.Database = db;
                    e.LoggedDate = moment(e.LoggedDate, 'YYYY-MM-DDTHH:mm:ss.SSS').add(this.TimezoneOffset, 'minutes').toDate();
                    e.ExceptionInformation = this.ParseJSON(e.ExceptionInformation);
                    e.ExceptionContext = this.ParseJSON(e.ExceptionContext);
                    e.WebRequestData = this.ParseJSON(e.WebRequestData);
                });

                return result.data;
            });
        }

        ParseJSON(val: string) {
            var result = <any>null;
            try {
                result = JSON.parse(val);
            } catch (ex) {
                result = "Failed to parse data:\n" + val;
            }

            return result;
        }
    }

    app.service('ExceptionService', ExceptionService);
}