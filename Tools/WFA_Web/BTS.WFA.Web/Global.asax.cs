﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BTS.WFA.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteTable.Routes.MapRoute("Index", "", new { controller = "Home", action = "Index" });
            GlobalConfiguration.Configure(config =>
            {
                config.MapHttpAttributeRoutes();
            });

            BundleTable.Bundles.Add(new Bundle("~/bundles/app-scripts").IncludeDirectory("~/app", "*.js", true));
            
            BundleTable.Bundles.Add(new StyleBundle("~/bundles/app-styles").Include("~/styles/site.css")
                                                                           .IncludeDirectory("~/app", "*.css", true));
        }
    }
}