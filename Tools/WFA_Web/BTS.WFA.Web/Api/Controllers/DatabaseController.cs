﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.OData.Query;

namespace BTS.WFA.Web.Api.Controllers
{
    [RoutePrefix("api/Database")]
    public class DatabaseController : ApiController
    {
        [HttpGet, Route("")]
        public IEnumerable<string> Get()
        {
            foreach (ConnectionStringSettings c in ConfigurationManager.ConnectionStrings)
                yield return c.Name;
        }

        [HttpGet, Route("{connectionString}/Exception")]
        public IEnumerable<BTS.WFA.Data.WfaException> Get(string connectionString, ODataQueryOptions<BTS.WFA.Data.WfaException> query)
        {
            using (var db = new BTS.WFA.Data.WfaSqlDb(connectionString))
            {
                var results = db.Exceptions.AsQueryable();
                results = query.ApplyTo(results) as IQueryable<BTS.WFA.Data.WfaException>;

                return results.ToArray();
            }
        }
    }
}
