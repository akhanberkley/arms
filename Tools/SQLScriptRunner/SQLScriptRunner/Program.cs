﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.IO;

namespace SQLScriptRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length < 2)
            {
                Console.WriteLine("Usage: SQLScriptRunner [directory] [connectionstring]");
            }
            else
            {
                var directoryPath = args[0];
                var connectionString = args[1];

                try
                {
                    var directory = new DirectoryInfo(directoryPath);
                    using (var db = new SqlConnection(connectionString))
                    {
                        db.Open();
                        
                        foreach(var file in directory.GetFiles())
                        {
                            var sql = (new StreamReader(file.OpenRead())).ReadToEnd();
                            var sqlFragments = sql.Split(new[] { Environment.NewLine + "GO" }, StringSplitOptions.RemoveEmptyEntries);

                            Console.WriteLine("{0}: Executing..", file.Name);
                            using(var cmd = db.CreateCommand())
                            {
                                using (var transaction = db.BeginTransaction())
                                {
                                    try
                                    {
                                        cmd.Transaction = transaction;
                                        cmd.CommandTimeout = 60;

                                        foreach(var fragment in sqlFragments)
                                        {
                                            cmd.CommandText = fragment;
                                            cmd.ExecuteNonQuery();
                                        }

                                        transaction.Commit();
                                    }
                                    catch (Exception ex)
                                    {
                                        transaction.Rollback();
                                        throw ex;
                                    }

                                }
                            }
                            
                            Console.WriteLine("{0}: Complete", file.Name);
                        }

                        db.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
