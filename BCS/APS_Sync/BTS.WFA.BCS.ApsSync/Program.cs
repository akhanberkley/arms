﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCS = BTS.WFA.BCS;

namespace BTS.WFA.BCS.ApsSync
{
    class Program
    {
        static void Main(string[] args)
        {
            CompanyDetail cd = null;
            try
            {
                var appHandlers = BCS.Application.CreateHandlers(ConfigurationManager.AppSettings["ApplicationHandlers"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
                BCS.Application.Initialize<BCS.Data.SqlDb>(appHandlers.ToArray());

                var dbEnvironment = BCS.Application.GetDatabaseEnvironment();

                Console.WriteLine(String.Format("This application will sync all agencies in {0}.dbo.APSSync", dbEnvironment));
                Console.WriteLine("To sync a specific company, enter it below");
                Console.WriteLine("Leave blank to sync all companies");
                Console.WriteLine();
                Console.Write("Company Number or Abbreviation: ");

                while (cd == null)
                {
                    var entry = Console.ReadLine().Trim().ToUpper();
                    if (!String.IsNullOrEmpty(entry))
                    {
                        if (BCS.Application.CompanyMap.ContainsKey(entry))
                            cd = BCS.Application.CompanyMap[entry];
                        else
                            cd = BCS.Application.CompanyMap.Values.FirstOrDefault(cn => cn.Summary.CompanyNumber == entry);
                    }

                    if (String.IsNullOrEmpty(entry) || cd != null)
                        break;
                    else
                        Console.Write("Company not found.  Please re-enter the number or abbreviation, or type in nothing for all companies: ");
                }
                Console.Write("Sync all APS agencies for company? ");
                bool allAgencies = (new[] { "YES", "Y" }).Contains(Console.ReadLine().ToUpper());
                Console.WriteLine();

                var sw = Stopwatch.StartNew();
                BCS.Services.AgencyService.SyncAgencies(cd, allAgencies, (s => Console.Out.WriteLine(s)));
                sw.Stop();

                Console.WriteLine();
                Console.WriteLine("Sync completed in " + sw.Elapsed.ToString("h' hours, 'm' minutes, and 's' seconds'"));
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                BCS.Application.HandleException(ex, cd);
            }
        }
    }
}
