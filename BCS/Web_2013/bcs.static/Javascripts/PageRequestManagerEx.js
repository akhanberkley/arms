﻿// JScript File
// http://geekswithblogs.net/rashid/archive/2007/08/08/Asp.net-Ajax-UpdatePanel-Simultaneous-Update---A-Remedy.aspx

var PageRequestManagerEx =
{
    _initialized : false,

    init : function()
    {
        if (!PageRequestManagerEx._initialized)
        {
            var _callQueue = new Array();
            var _executingElement = null;
            var _prm = Sys.WebForms.PageRequestManager.getInstance();

            _prm.add_initializeRequest(initializeRequest);
            _prm.add_endRequest(endRequest);

            PageRequestManagerEx._initialized = true;
        }

        function initializeRequest(sender, args)
        {
            try
            {
                var date = new Date();
                document.getElementById('startTime').value = date.getMinutes() + ':' + date.getSeconds() + ' OR ' + date.getMilliseconds();
            }
            catch(err) {};
                       
            if (_prm.get_isInAsyncPostBack())
            {
                //if we are here that means there already a call pending.

                //Get the element which cause the postback
                var postBackElement = args.get_postBackElement();

                //We need to check this otherwise it will abort the request which we made from the
                //end request
                if (_executingElement != postBackElement)
                {
                    //Does not match which means it is another control
                    //which request the update, so cancel it temporary and 
                    //add it in the call queue
                    args.set_cancel(true);
                    Array.enqueue(_callQueue, postBackElement);
                }

                //Reset it as we are done with our matching
                _executingElement = null;
            }
        }

        function endRequest(sender, args)
        {
            //Check if we have a pending call
            if (_callQueue.length > 0)
            {
                //Get the first item from the call queue and setting it
                //as current executing item
                _executingElement = Array.dequeue(_callQueue);

                //Now Post the from which will also fire the initializeRequest
                _prm._doPostBack(_executingElement.name, '');
            }
             try
            { 
                var date = new Date();
                document.getElementById('endTime').value = date.getMinutes() + ':' + date.getSeconds() + ' OR ' + date.getMilliseconds();
            }
            catch(err) {};
        }
    }
}

if (typeof(Sys) != 'undefined')
{
    Sys.Application.notifyScriptLoaded();
}


