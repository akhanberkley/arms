/* 
Author: Adam Tibi
Date: 04 April 2006
Email: aztibi@gmail.com
http://www.codeproject.com/KB/validation/MultipleFieldsValidator.aspx
*/

function MultipleFieldsValidatorEvaluateIsValid(val) {
 
    controltovalidateIDs = val.controlstovalidate.split(',');
    var initialvalues = val.initialvalues.split(',');
    switch (val.condition) {
        case 'OR':
            //for(var controltovalidateIDIndex in controltovalidateIDs) {
            for(var i =0 ; i < controltovalidateIDs.length ; i++) {
                //var controlID = controltovalidateIDs[controltovalidateIDIndex];
                var controlID = controltovalidateIDs[i];
                if (ValidatorTrim(ValidatorGetValue(controlID)) != initialvalues[i]) {
                    return true;
                } 
            }
            return false;
        break;
        case 'XOR':
            // TODO: initial values and XOR
            //for(var controltovalidateIDIndex in controltovalidateIDs) {
            for(var i =0 ; i < controltovalidateIDs.length ; i++) {
                //var controlID = controltovalidateIDs[controltovalidateIDIndex];
                var controlID = controltovalidateIDs[i];
                if (controltovalidateIDIndex == '0') {
                    var previousResult = !(ValidatorTrim(ValidatorGetValue(controlID)) == '');
                    continue;
                }
                var currentResult = !(ValidatorTrim(ValidatorGetValue(controlID)) == '');
                if (currentResult != previousResult) {
                    return true;
                }
                previousResult != currentResult;
            }
            return false;
        break;
        case 'AND':
            //for(var controltovalidateIDIndex in controltovalidateIDs) {
            for(var i =0 ; i < controltovalidateIDs.length ; i++) {
                //var controlID = controltovalidateIDs[controltovalidateIDIndex];
                var controlID = controltovalidateIDs[i];
                if (ValidatorTrim(ValidatorGetValue(controlID)) == initialvalues[i]) {
                    return false;
                } 
            }
            return true;
        break;
    }
    return false;
}
