<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClientName.ascx.cs" Inherits="UserControls_ClientName" %>
<table cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table cellspacing="0" cellpadding="2">
                <tr>
                    <td bgcolor="#dcdcdc">Name Type</td>
                </tr>
                <tr>
                    <td colspan="2"><UserControls:ClientNameTypeRadioButtonList ID="ClientNameTypeRadioButtonList1" runat="server" OnSelectedIndexChanged="ClientNameTypeRadioButtonList1_SelectedIndexChanged" AutoPostBack="True"></UserControls:ClientNameTypeRadioButtonList></td>
                </tr>
                <tr>                
                    <td bgcolor="#dcdcdc"><asp:Label ID="lbNameLast" text="Name" runat="server"></asp:Label></td>
                    <td bgcolor="#dcdcdc"><asp:Label ID="lbNameFirst" text="First Name" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td><asp:TextBox ID="txNameLast" runat="server" Width="280px"></asp:TextBox></td>
                    <td><asp:TextBox ID="txNameFirst" runat="server" Width="280px"></asp:TextBox></td>
                </tr>
            </table>
        </td>
    </tr>
</table>