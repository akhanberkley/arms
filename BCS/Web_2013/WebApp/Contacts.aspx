<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true"
    CodeFile="Contacts.aspx.cs" Inherits="Contacts" Title="Berkley Clearance System - Contacts" Theme="defaultTheme" %>

<%@ MasterType VirtualPath="~/Site.PATS.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <aje:ScriptManager id="ScriptManager1" runat="server"></aje:ScriptManager>
    <table cellpadding="0" cellspacing="0" class="box">
        <tbody>
            <tr>
                <td class="TL"><div /></td>
                <td class="TC">Agency Contacts</td>
                <td class="TR"><div /></td>
            </tr>
            <tr>
                <td class="ML"><div /></td>
                <td class="MC">
                    <asp:TextBox ID="txFilter" runat="server"></asp:TextBox>&nbsp;
                    <asp:Button ID="Button1" runat="server" Text="Filter By Agency Number" OnClick="Button1_Click" />
                    <br />
                    <br />
                    <UserControls:Grid ID="gridContacts" runat="server" AllowPaging="True" EmptyDataText="No Contacts were found."
                        AutoGenerateColumns="False" CaptionAlign="Left" DataSourceID="odsContacts" OnSelectedIndexChanging="gridContacts_SelectedIndexChanging"
                        DataKeyNames="Id" OnPageIndexChanged="gridContacts_PageIndexChanged" OnRowDeleted="gridContacts_RowDeleted">
                        <Columns>
                            <asp:CommandField ButtonType="Button" ShowSelectButton="True" SelectText='<%$ Code: BCS.WebApp.Components.ConfigValues.GetEditButtonDixplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId),false) %>' ControlStyle-Width="50px" />
                            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
                            <asp:BoundField DataField="AgencyId" HeaderText="Agency Id" Visible="False" />
                            <asp:TemplateField HeaderText="Agency Name">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "Agency.AgencyName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Agency Number">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval (Container.DataItem,"Agency.AgencyNumber") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Name" HeaderText="Name" />
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                        Text="Delete" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.Agency) && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                                        Message='<%# DataBinder.Eval (Container.DataItem, "Name", "Are you sure you want to delete this Contact {0}?") %>' />
                                    <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="btnDelete"
                                        ConfirmText='<%# DataBinder.Eval (Container.DataItem, "Name", "Are you sure you want to delete this Contact {0}?") %>'>
                                    </ajx:ConfirmButtonExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </UserControls:Grid>
                    <asp:ObjectDataSource ID="odsContacts" runat="server" DeleteMethod="DeleteContact"
                        SelectMethod="GetContactsSort" TypeName="BCS.Core.Clearance.Admin.DataAccess" OnDeleted="odsContacts_Deleted">
                        <DeleteParameters>
                            <asp:Parameter Name="id" Type="Int32" />
                        </DeleteParameters>
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txFilter" Name="filterAgencyNumber" PropertyName="Text"
                                Type="String" />
                            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    &nbsp;
                    <br />
                    <asp:Button ID="btnAdd" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.Agency) && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                        Text="Add" OnClick="btnAdd_Click" /><br />
                    <br />
                    
                    <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
                        <br />
                    </asp:PlaceHolder>
                    
                    <asp:DetailsView ID="DetailsView1" runat="server"
                        AutoGenerateRows="False" DataSourceID="odsContact" Height="50px" OnItemDeleted="DetailsView1_ItemDeleted"
                        OnItemInserted="DetailsView1_ItemInserted" OnItemUpdated="DetailsView1_ItemUpdated"
                        OnItemUpdating="DetailsView1_ItemUpdating" DataKeyNames="Id" OnModeChanging="DetailsView1_ModeChanging">
                        <Fields>
                            <asp:TemplateField HeaderText="Id" Visible="False">
                                <EditItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Agency">
                                <EditItemTemplate>
                                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="odsAgencies" DataTextField="AgencyName"
                                        DataValueField="Id" SelectedValue='<%# Bind("AgencyId") %>'>
                                    </asp:DropDownList><asp:ObjectDataSource ID="odsAgencies" runat="server" SelectMethod="GetAgencies"
                                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="odsAgencies" DataTextField="AgencyName"
                                        DataValueField="Id" SelectedValue='<%# Bind("AgencyId") %>'>
                                    </asp:DropDownList><asp:ObjectDataSource ID="odsAgencies" runat="server" SelectMethod="GetAgencies"
                                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "Agency.AgencyName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox4"
                                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox4"
                                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*"></asp:RequiredFieldValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date of Birth">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("DateOfBirth", "{0:MM/dd/yyyy}") %>'
                                        OnInit="DatePickerTextBox_Init"></asp:TextBox>
                                    <asp:CompareValidator
                                        ID="CompareValidator10" runat="server" ControlToValidate="TextBox10" Display="Dynamic"
                                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                                    <asp:RangeValidator ID="rvDOB" runat="server" SetFocusOnError="True" Display="Dynamic" ControlToValidate="TextBox10"
                                        Type="Date" OnInit="rvDOB_Init" ErrorMessage="&nbsp;" Text="* Date of Birth cannot be greater than 12 years ago."></asp:RangeValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("DateOfBirth", "{0:MM/dd/yyyy}") %>'
                                        OnInit="DatePickerTextBox_Init"></asp:TextBox>
                                    <asp:CompareValidator
                                        ID="CompareValidator10" runat="server" ControlToValidate="TextBox10" Display="Dynamic"
                                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                                    <asp:RangeValidator ID="rvDOB" runat="server" SetFocusOnError="True" Display="Dynamic" ControlToValidate="TextBox10"
                                        Type="Date" OnInit="rvDOB_Init" ErrorMessage="&nbsp;" Text="* Date of Birth cannot be greater than 12 years ago."></asp:RangeValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Text='<%# Bind("DateOfBirth", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Primary Phone">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PrimaryPhone") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox1"
                                        ErrorMessage="&nbsp;" Text="* Wrong format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"
                                        Display="Dynamic"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PrimaryPhone") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox1"
                                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label651" runat="server" Text='<%# Bind("PrimaryPhone") %> ' OnDataBinding="PhoneFormatControl_DataBinding"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Primary Phone Extension">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox111" runat="server" Text='<%# Bind("PrimaryPhoneExtension") %>'
                                        ></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator111" runat="server"
                                        ControlToValidate="TextBox111" ErrorMessage="&nbsp;" Text="* Wrong format." ValidationExpression="\d{2,4}"
                                        Display="Dynamic"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox111" runat="server" Text='<%# Bind("PrimaryPhoneExtension") %>'
                                        ></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator111" runat="server"
                                        ControlToValidate="TextBox111" Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Format."
                                        ValidationExpression="\d{2,4}"></asp:RegularExpressionValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label651" runat="server" Text='<%# Bind("PrimaryPhoneExtension") %> '
                                        ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Secondary Phone">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox51" runat="server" Text='<%# Bind("SecondaryPhone") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator211" runat="server"
                                        ControlToValidate="TextBox51" ErrorMessage="&nbsp;" Text="* Wrong format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"
                                        Display="Dynamic"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox51" runat="server" Text='<%# Bind("SecondaryPhone") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator211" runat="server"
                                        ControlToValidate="TextBox51" Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Format."
                                        ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label61" runat="server" Text='<%# Bind("SecondaryPhone") %> ' OnDataBinding="PhoneFormatControl_DataBinding"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Secondary Phone Extension">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("SecondaryPhoneExtension") %>'
                                        ></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1111" runat="server"
                                        ControlToValidate="TextBox11" ErrorMessage="&nbsp;" Text="* Wrong format." ValidationExpression="\d{2,4}"
                                        Display="Dynamic"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("SecondaryPhoneExtension") %>'
                                        ></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1111" runat="server"
                                        ControlToValidate="TextBox11" Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Format."
                                        ValidationExpression="\d{2,4}"></asp:RegularExpressionValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label61" runat="server" Text='<%# Bind("SecondaryPhoneExtension") %> '
                                        ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fax">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox22" runat="server" Text='<%# Bind("Fax") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator33" runat="server"
                                        ControlToValidate="TextBox22" Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Format."
                                        ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox22" runat="server" Text='<%# Bind("Fax") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator44" runat="server"
                                        ControlToValidate="TextBox22" Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Format" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label25" runat="server" Text='<%# Bind("Fax") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email" SortExpression="Email">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="TextBox8"
                                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox><asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator6" runat="server" ControlToValidate="TextBox8"
                                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label11" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>           
                            <asp:TemplateField HeaderText="Retirement Date">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBoxRetirementDate" runat="server" Text='<%# Bind("RetirementDate", "{0:MM/dd/yyyy}") %>' OnInit="DatePickerTextBox_Init"></asp:TextBox>
                                    <asp:CompareValidator
                                        ID="CompareValidatorRetirementDate" runat="server" ControlToValidate="TextBoxRetirementDate" Display="Dynamic"
                                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBoxRetirementDate" runat="server" Text='<%# Bind("RetirementDate", "{0:MM/dd/yyyy}") %>' OnInit="DatePickerTextBox_Init"></asp:TextBox>
                                    <asp:CompareValidator
                                        ID="CompareValidatorRetirementDate" runat="server" ControlToValidate="TextBoxRetirementDate" Display="Dynamic"
                                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Text='<%# Bind("RetirementDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ButtonType="Button" ShowEditButton='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' ShowInsertButton='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' />
                        </Fields>
                    </asp:DetailsView>
                    <asp:ObjectDataSource ID="odsContact" runat="server" SelectMethod="GetContact" TypeName="BCS.Core.Clearance.Admin.DataAccess"
                        InsertMethod="AddContact" OnInserting="odsContact_Inserting" OnUpdating="odsContact_Updating"
                        UpdateMethod="UpdateContact" DataObjectTypeName="BCS.Core.Clearance.BizObjects.Contact"
                        OnDeleted="odsContact_Deleted" OnInserted="odsContact_Inserted" OnUpdated="odsContact_Updated">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="gridContacts" Name="id" PropertyName="SelectedValue"
                                Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td class="MR"><div /></td>
            </tr>
            <tr>
                <td class="BL"><div /></td>
                <td class="BC"><div /></td>
                <td class="BR"><div /></td>
            </tr>
        </tbody>
    </table>
</asp:Content>
