#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.UserControls;
using BCS.WebApp.Components;
using System.Linq;
#endregion

public partial class Agencies : System.Web.UI.Page
{
    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
        lblSyncResult.Text = "";
        btnSyncAgency.Visible = User.IsInRole(BTS.WFA.BCS.Domains.Roles.SystemAdministrator);

        if (DetailsView1.CurrentMode != DetailsViewMode.ReadOnly)
        {
            AgencyNumberDropdownList agencyNumberDropdownList = DetailsView1.FindControl("AgencyNumberDropdownList1") as AgencyNumberDropdownList;
            WebControl rfv = DetailsView1.FindControl("rfvReferralDate") as WebControl;
            //if (rfv != null && agencyNumberDropdownList != null)
            //    rfv.Enabled = agencyNumberDropdownList.SelectedIndex > 0;

            TextBox txReferralDate = DetailsView1.FindControl("TextBox88") as TextBox;
            WebControl rfv1 = DetailsView1.FindControl("rfvReferralAgency") as WebControl;

            //if (rfv1 != null && txReferralDate != null)
            //    rfv1.Enabled = txReferralDate.Text.Length > 0;

            if (null != agencyNumberDropdownList && null != rfv && null != txReferralDate && null != rfv1)
            {
                agencyNumberDropdownList.Attributes.Add("onchange", string.Format("requireAnother('{0}','{1}','{2}','{3}');",
                       agencyNumberDropdownList.ClientID, txReferralDate.ClientID, rfv.ClientID, rfv1.ClientID));
                txReferralDate.Attributes.Add("onchange", string.Format("requireAnother('{0}','{1}','{2}','{3}');",
                    agencyNumberDropdownList.ClientID, txReferralDate.ClientID, rfv.ClientID, rfv1.ClientID));

                //ClientScript.RegisterStartupScript(typeof(string), "requireAnother", string.Format(" alert ('abc') ;requireAnother('{0}','{1}','{2}','{3}');",
                //    agencyNumberDropdownList.ClientID, txReferralDate.ClientID, rfv.ClientID, rfv1.ClientID), true);

                rfv.Enabled = agencyNumberDropdownList.SelectedIndex > 0 && txReferralDate.Text.Length > 0;
                rfv1.Enabled = agencyNumberDropdownList.SelectedIndex > 0 && txReferralDate.Text.Length > 0;
            }

        }
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        if (Session["CompanyChanged"] != null)
        {
            Session.Contents.Remove("CompanyChanged");
            gridAgencies.SelectedIndex = -1;
            DetailsView1.DataBind();
        }
        base.OnLoadComplete(e);
    }
    #endregion

    #region Control Events
    protected void Button1_Click(object sender, EventArgs e)
    {
        gridAgencies.SelectedIndex = -1;
        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly);
    }
    protected void btnSyncAgency_Click(object sender, EventArgs e)
    {
        var agencyNumber = txFilterAgencyNumber.Text.Trim();
        if (String.IsNullOrEmpty(agencyNumber))
            lblSyncResult.Text = "Agency Number to sync is required";
        else
        {
            var companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
            var cn = BTS.WFA.BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == companyNumber);
            var results = BTS.WFA.BCS.Services.AgencyService.SyncAgency(cn, agencyNumber, false);
            lblSyncResult.Text = String.Format("Sync {0}: {1}", (results.Succeeded) ? "Succeeded" : "Failed", results.Message);
        }
    }
    protected void odsAgency_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        (e.InputParameters[0] as BCS.Core.Clearance.BizObjects.Agency).ModifiedBy = Common.GetUser();
    }
    protected void odsAgency_Updating(object sender, ObjectDataSourceMethodEventArgs e)
    {
        BCS.Core.Clearance.BizObjects.Agency agency = e.InputParameters[0] as BCS.Core.Clearance.BizObjects.Agency;
        agency.ModifiedBy = Common.GetUser();
    }
    protected void odsAgency_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            ShowMessage(e.ReturnValue.ToString());
            // remove the cached agencies
            Cache.Remove(string.Format(CacheKeys.CompanyNumberAgencies, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId)));
        }
    }
    protected void odsAgency_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            ShowMessage(e.ReturnValue.ToString());
            // remove the cached agencies
            Cache.Remove(string.Format(CacheKeys.CompanyNumberAgencies, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId)));
        }
    }
    protected void odsAgencies_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            ShowMessage(e.ReturnValue.ToString());
            // remove the cached agencies
            Cache.Remove(string.Format(CacheKeys.CompanyNumberAgencies, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId)));
        }
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        gridAgencies.DataBind();
        DetailsView1.DataBind();
    }
    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        gridAgencies.DataBind();
        DetailsView1.DataBind();
    }
    protected void gridAgencies_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        gridAgencies.DataBind();
        DetailsView1.DataBind();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Common.GetSafeIntFromSession(SessionKeys.CompanyId) == 0 || Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) == 0)
        {
            ShowMessage("Please select a company/number before adding an agency.");
            return;
        }
        gridAgencies.SelectedIndex = -1;
        DetailsView1.ChangeMode(DetailsViewMode.Insert);
    }

    protected void gridAgencies_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        if (!BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.Agency))
            DetailsView1.ChangeMode(DetailsViewMode.Edit);
    }
    protected void gridAgencies_PageIndexChanged(object sender, EventArgs e)
    {
        gridAgencies.SelectedIndex = -1;
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        e.OldValues.Add("LicensedStatesList", (BCS.Core.Clearance.BizObjects.StateList)ViewState["OldLicensedStatesList"]);
        e.NewValues.Add("LicensedStatesList", ToStateList(Common.GetSelectedValues(DetailsView1.FindControl("StateListBox1") as StateListBox)));

        bool b = Common.ShouldUpdate(e);
        e.Cancel = b;

        if (b)
        {
            // there was nothing to update. Just show the user the success message.
            ShowMessage("Agency Sucessfully updated.");
            DetailsView1.DataBind();
        }
        else
        {
            e.NewValues["Phone"] = Common.DeFormatPhone(e.NewValues["Phone"] != null ? e.NewValues["Phone"].ToString() : string.Empty);
            e.NewValues["Fax"] = Common.DeFormatPhone(e.NewValues["Fax"] != null ? e.NewValues["Fax"].ToString() : string.Empty);
            e.NewValues["ContactPersonPhone"] = Common.DeFormatPhone(e.NewValues["ContactPersonPhone"] != null ? e.NewValues["ContactPersonPhone"].ToString() : string.Empty);
            e.NewValues["ContactPersonFax"] = Common.DeFormatPhone(e.NewValues["ContactPersonFax"] != null ? e.NewValues["ContactPersonFax"].ToString() : string.Empty);

            //e.NewValues["Zip"] = Common.DeFormatZip(e.NewValues["Zip"] != null ? e.NewValues["Zip"].ToString() : string.Empty);
        }

    }
    protected void DatePicker_Init(object sender, EventArgs e)
    {
        TextBox tb = sender as TextBox;
        tb.Attributes.Add("onclick", "scwShow(this,this);");
        tb.Attributes.Add("onkeydown", "hideCalOnTab(event);");
        tb.Attributes.Add("onblur", "this.value = purgeDate(this);");
    }
    protected void DatePicker_Init1(object sender, EventArgs e)
    {
        AgencyNumberDropdownList agencyNumberDropdownList = DetailsView1.FindControl("AgencyNumberDropdownList1") as AgencyNumberDropdownList;
        WebControl rfv = DetailsView1.FindControl("rfvReferralDate") as WebControl;

        TextBox txReferralDate = DetailsView1.FindControl("TextBox88") as TextBox;
        WebControl rfv1 = DetailsView1.FindControl("rfvReferralAgency") as WebControl;

        TextBox tb = sender as TextBox;
        tb.Attributes.Add("onclick", string.Format("scwShow(this,this);processAfter=new Function(\"requireAnother('{0}','{1}','{2}','{3}');\")",
        agencyNumberDropdownList.ClientID, txReferralDate.ClientID, rfv.ClientID, rfv1.ClientID));
        tb.Attributes.Add("onkeydown", "hideCalOnTab(event);");
        tb.Attributes.Add("onblur", string.Format("this.value = purgeDate(this); requireAnother('{0}','{1}','{2}','{3}');",
            agencyNumberDropdownList.ClientID, txReferralDate.ClientID, rfv.ClientID, rfv1.ClientID));
    }
    protected void DatePicker_InitToday(object sender, EventArgs e)
    {
        DatePicker_Init(sender, e);
        (sender as TextBox).Text = DateTime.Today.ToString("MM/dd/yyyy");
    }
    protected void PhoneFormatControl_DataBinding(object sender, EventArgs e)
    {
        ITextControl tx = sender as ITextControl;
        tx.Text = Common.FormatPhone(tx.Text);
    }
    //protected void ZipFormatControl_DataBinding(object sender, EventArgs e)
    //{
    //    ITextControl tx = sender as ITextControl;
    //    tx.Text = Common.FormatZip(tx.Text);
    //}
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        e.Cancel = BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.Agency) || ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
    }
    protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        e.Values.Add("LicensedStatesList", ToStateList(Common.GetSelectedValues(DetailsView1.FindControl("StateListBox1") as StateListBox)));
        e.Values["Phone"] = Common.DeFormatPhone(e.Values["Phone"] != null ? e.Values["Phone"].ToString() : string.Empty);
        e.Values["Fax"] = Common.DeFormatPhone(e.Values["Fax"] != null ? e.Values["Fax"].ToString() : string.Empty);
        e.Values["ContactPersonPhone"] = Common.DeFormatPhone(e.Values["ContactPersonPhone"] != null ? e.Values["ContactPersonPhone"].ToString() : string.Empty);
        e.Values["ContactPersonFax"] = Common.DeFormatPhone(e.Values["ContactPersonFax"] != null ? e.Values["ContactPersonFax"].ToString() : string.Empty);

        //e.Values["Zip"] = Common.DeFormatZip(e.Values["Zip"] != null ? e.Values["Zip"].ToString() : string.Empty);        
    }
    protected void Sort_PreRender(object sender, EventArgs e)
    {
        ListControl senderControl = (sender as ListControl);
        ListItem[] items = Common.SortListItems(senderControl.Items);
        senderControl.Items.Clear();
        senderControl.Items.AddRange(items);
    }
    protected void AgencyNumberDropdownList1_Init(object sender, EventArgs e)
    {
        try
        {
            AgencyNumberDropdownList anddl = sender as AgencyNumberDropdownList;
            BCS.Biz.Agency agency = (((sender as DropDownList).NamingContainer) as DetailsView).DataItem as BCS.Biz.Agency;

            anddl.ExcludedAgencyNumber = agency.AgencyNumber;
        }
        catch (Exception)
        { }
    }
    protected void DetailsView1_DataBound(object sender, EventArgs e)
    {
        if (DetailsView1.DataItem != null)
        {
            BCS.WebApp.UserControls.StateListBox slb = DetailsView1.FindControl("StateListBox1") as BCS.WebApp.UserControls.StateListBox;
            foreach (BCS.Biz.AgencyLicensedState aLicState in (DetailsView1.DataItem as BCS.Biz.Agency).AgencyLicensedStates)
            {
                slb.Items.FindByValue(aLicState.StateId.ToString()).Selected = true;
            }
            //ViewState["OldLicensedStates"] = Array.ConvertAll<string, Int32>(Common.GetSelectedValues(slb), new Converter<string, Int32>(Convert.ToInt32));

            ViewState["OldLicensedStatesList"] = ToStateList(Common.GetSelectedValues(slb));
        }
        if (DetailsView1.CurrentMode != DetailsViewMode.ReadOnly)
        {
            AgencyNumberDropdownList agencyNumberDropdownList = DetailsView1.FindControl("AgencyNumberDropdownList1") as AgencyNumberDropdownList;
            WebControl rfv = DetailsView1.FindControl("rfvReferralDate") as WebControl;
            //if (rfv != null && agencyNumberDropdownList != null)
            //    rfv.Enabled = agencyNumberDropdownList.SelectedIndex > 0;

            TextBox txReferralDate = DetailsView1.FindControl("TextBox88") as TextBox;
            WebControl rfv1 = DetailsView1.FindControl("rfvReferralAgency") as WebControl;

            if (null != agencyNumberDropdownList && null != rfv && null != txReferralDate && null != rfv1)
            {
                //if (rfv1 != null && txReferralDate != null)
                //    rfv1.Enabled = txReferralDate.Text.Length > 0;

                agencyNumberDropdownList.Attributes.Add("onchange", string.Format("requireAnother('{0}','{1}','{2}','{3}');",
                    agencyNumberDropdownList.ClientID, txReferralDate.ClientID, rfv.ClientID, rfv1.ClientID));
                txReferralDate.Attributes.Add("onchange", string.Format("requireAnother('{0}','{1}','{2}','{3}');",
                    agencyNumberDropdownList.ClientID, txReferralDate.ClientID, rfv.ClientID, rfv1.ClientID));

                //ClientScript.RegisterStartupScript(typeof(string), "requireAnother", string.Format(" alert ('abc') ;requireAnother('{0}','{1}','{2}','{3}');",
                //    agencyNumberDropdownList.ClientID, txReferralDate.ClientID, rfv.ClientID, rfv1.ClientID), true);

                rfv.Enabled = agencyNumberDropdownList.SelectedIndex > 0;
                rfv1.Enabled = txReferralDate.Text.Length > 0;
            }

        }
    }
    protected void AgencyNumberDropdownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        AgencyNumberDropdownList agencyNumberDropdownList = sender as AgencyNumberDropdownList;
        WebControl rfv = DetailsView1.FindControl("rfvReferralDate") as WebControl;
        //agencyNumberDropdownList.Attributes.Add("onchange", string.Format("enabledrequiredrefdate(this,'{0}');", rfv.ClientID));
        if (rfv != null && agencyNumberDropdownList != null)
            rfv.Enabled = agencyNumberDropdownList.SelectedIndex > 0;
    }
    protected void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        throw new ApplicationException("Ajax Error", e.Exception);
    }

    protected void ValidationButton_Init(object sender, EventArgs e)
    {
        Button btn = sender as Button;
        btn.Attributes.Add("onclick", string.Format("if(!Page_ClientValidate('{0}')) {{  $find('alwaysVisible').get_element().style.display = ''; $find('alwaysVisible').initialize(); }}", btn.ValidationGroup));
    }
    #endregion

    #region Other Methods

    private BCS.Core.Clearance.BizObjects.StateList ToStateList(string[] stateids)
    {
        BCS.Core.Clearance.BizObjects.StateList stateList = new BCS.Core.Clearance.BizObjects.StateList();
        stateList.List = new BCS.Core.Clearance.BizObjects.State[stateids.Length];
        for (int i = 0; i < stateids.Length; i++)
        {
            BCS.Core.Clearance.BizObjects.State state = new BCS.Core.Clearance.BizObjects.State();
            state.Id = Convert.ToInt32(stateids[i]);
            stateList.List[i] = state;
        }
        return stateList;
    }
    private void ShowMessage(string message)
    {
        ValidationTable.Style.Add("display", "");
        Common.SetStatus(messagePlaceHolder, message);
    }
    #endregion
}
