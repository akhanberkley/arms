using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace BCS.WebApp
{
	/// <summary>
	/// Summary description for KeepSessionAlive.
	/// </summary>
	public partial class KeepSessionAlive : System.Web.UI.Page
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
			Response.AddHeader("Refresh", "600");
		}

		
	}
}
