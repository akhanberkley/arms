﻿<%@ Page Language="C#" AutoEventWireup="true"  Theme="DefaultTheme" CodeFile="SubmissionHistory.aspx.cs" Title="Submission History" Inherits="SubmissionHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Berkley Clearance System - Submission Status History</title>
    <link href="Styles/pats_style.css" rel="stylesheet" type="text/css" />
</head>
<script language="javascript" type="text/javascript">
function func()
{                
    var hdn = this.document.getElementById('<%=hdn.ClientID %>');
    hdn.value = hdn.value + "1";
}
</script>
<body class="pageBody">
    <form id="form1" runat="server" onsubmit="func();">
    <aje:ScriptManager id="ScriptManager1" runat="server" OnAsyncPostBackError="ScriptManager1_AsyncPostBackError"></aje:ScriptManager>
    
    <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
    </asp:PlaceHolder>
    <table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Submission History</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        
        <div style="padding:10px;">
            <asp:Label ID="lblCurrentStatus" runat="server"></asp:Label><br />
            <br />
            <UserControls:Grid ID="GridView1" CssClass="grid" runat="server" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnRowDataBound="GridView1_RowDataBound" 
             DataKeyNames="Id" DataSourceID="ObjectDataSource1" AutoGenerateColumns="False" AllowPaging="True" EmptyDataText="There is no history for this submission.">
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" />
                    <asp:BoundField DataField="SubmissionId" HeaderText="SubmissionId" Visible="False" />
                    <asp:BoundField DataField="PreviousSubmissionStatusId" HeaderText="PreviousSubmissionStatusId" Visible="False" />                    
                    <asp:TemplateField HeaderText="Status Date">                    
                        <ItemTemplate>
                            <asp:Label ID="lblStatusDate" runat="server" Text='<%# Bind("SubmissionStatusDate", "{0:MM/dd/yyyy}") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Previous Submission Status">                   
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "SubmissionStatus.StatusCode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status Changed Date">                    
                        <ItemTemplate>
                            <asp:Label ID="lblDate" runat="server" Text='<%# Bind("StatusChangeDt", "{0:MM/dd/yyyy HH:mm}") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="StatusChangeBy" HeaderText="Status Changed By" />
                    <asp:TemplateField HeaderText="Active" ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" Text="Active" CommandName="Select" CausesValidation="false" id="LinkButton1"></asp:LinkButton>
                            <ajx:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="LinkButton1"
                             ConfirmText="Are you sure you want to make this status history inactive, this cannot be undone?"></ajx:ConfirmButtonExtender>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </UserControls:Grid>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetSubmissionStatusHistory"
                TypeName="BCS.Core.Clearance.Submissions">           
            </asp:ObjectDataSource>
           <asp:HiddenField ID="hdn" runat="server" />
        </div>
    
    </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
    </form>
</body>
</html>
