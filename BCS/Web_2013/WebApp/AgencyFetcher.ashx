<%@ WebHandler Language="C#" Class="AgencyFetcher"  %>

using System;
using System.Web;
using System.Text;
using BCS.WebApp.UserControls;
using System.Data;

public class AgencyFetcher : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
//        context.Response.ContentType = "text/plain";

        string lastOrderId = Convert.ToString(context.Request.QueryString["lastOrderId"]);

        AgencyDropDownList addl = new AgencyDropDownList();
        addl.CompanyNumberId = 3;
        DataSet ds = addl.GetCachedSource();
        DataTable dataTable = ds.Tables["Agency"];

        DataRow[] dataRows = dataTable.Select("AgencyName > '" + lastOrderId + "'");

        int limit = Math.Min(dataRows.Length, 1000);
        
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 1000; i++)
        {
            sb.Append("<tr ");
            if (i % 2 == 0)
                sb.Append("style=\"color: Black; background-color: #EEEEEE;\"");
            else
                sb.Append("style=\"color: Black; background-color: Gainsboro;\"");
            sb.Append(">");
            sb.Append("<td><div onmouseover=\"this.style.backgroundColor='yellow';\" onmouseout=\"this.style.backgroundColor='';\">");
        
            sb.Append("<span id=\"hAgencyId\">" + dataRows[i]["Id"] + "</span>&nbsp;");
            sb.Append("<span id=\"ipAgencyName\">" + dataRows[i]["AgencyName"] + "</span>|&nbsp;");
            sb.Append("<span id=\"ipAgencyNumber\">" + dataRows[i]["AgencyNumber"] + "</span>|&nbsp;");
            sb.Append("<span id=\"ipCity\">" + dataRows[i]["City"] + "</span>|&nbsp;");
            sb.Append("<span id=\"ipState\">" + dataRows[i]["State"] + "</span>|&nbsp;");
            sb.Append("<span id=\"ipZip\">" + dataRows[i]["Zip"] + "</span>");

            sb.Append("</div></td></tr>");
        }
        context.Response.Write(sb.ToString());
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}