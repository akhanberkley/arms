<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="AgencySubmissionSearch.aspx.cs" Inherits="BCS.WebApp.AgencySubmissionSearch" Title="Berkley Clearance System : Agency Submission Search" Theme="DefaultTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<aje:ScriptManager id="ScriptManager1" runat="server" OnAsyncPostBackError="ScriptManager1_AsyncPostBackError"></aje:ScriptManager>
<asp:PlaceHolder runat="server" ID="messagePlaceHolder">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="Filter"></asp:ValidationSummary>
</asp:PlaceHolder>
<p><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx" OnInit="HyperLink1_Init">Search By Client</asp:HyperLink>&nbsp;|&nbsp;
<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/underwritersubmissionsearch.aspx" OnInit="HyperLink1_Init">Search By Underwriter</asp:HyperLink>
</p>
    <table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Agency Submission Search</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        
        <table>
            <tr>
                <td>Agency Number</td>
                <td>
                    <asp:TextBox ID="txFilterAgencyNumber" runat="server"></asp:TextBox>
                </td>
                <td>Agency Name</td>
                <td>
                    <asp:TextBox ID="txFilterAgencyName" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnFilter" runat="server" Text="Filter"  OnClick="Filter_Click" CausesValidation="false" />
                </td>
            </tr>
        </table>
    <br />
    <br />
    <UserControls:Grid ID="gridAgencies" runat="server" AllowPaging="True" EmptyDataText="No Agencies were found." AutoGenerateColumns="False" DataKeyNames="Id" CaptionAlign="Left" DataSourceID="odsAgencies"
        OnPageIndexChanged="gridAgencies_PageIndexChanged"
        OnSelectedIndexChanged="gridAgencies_SelectedIndexChanged" >
        <Columns>
            <asp:CommandField ButtonType="Button" ShowSelectButton="True" SelectText="Load" ControlStyle-Width="40px" />
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
            <asp:BoundField DataField="CompanyNumberId" HeaderText="Company Number" Visible="False"/>
             <asp:TemplateField HeaderText="Company Number">                
                <ItemTemplate>
                    <asp:Label ID="lblCompanyNumber" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CompanyNumber.PropertyCompanyNumber") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="AgencyName" HeaderText="Agency Name" />
            <asp:BoundField DataField="AgencyNumber" HeaderText="Agency Number" />
            <asp:BoundField DataField="ContactPerson" HeaderText="Contact Person" />
            <asp:BoundField DataField="FEIN" HeaderText="FEIN" />
        </Columns>
    </UserControls:Grid>
    <asp:ObjectDataSource ID="odsAgencies" runat="server" SelectMethod="GetAgencies"
        TypeName="BCS.Core.Clearance.Admin.DataAccess" DeleteMethod="DeleteAgency" >
        <SelectParameters>
            <asp:ControlParameter ControlID="txFilterAgencyNumber" DefaultValue="" Name="filterAgencyNumber" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txFilterAgencyName" DefaultValue="" Name="filterAgencyName" PropertyName="Text"
            Type="String" />
            <asp:SessionParameter DefaultValue="0" Name="companyId" SessionField="CompanySelected"
                Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
    
    <br />
    <br />
    <table id="tblFilter" runat="server" visible="false">
        <tr>
            <td>
                Start Date</td>
            <td>
                <asp:TextBox ID="txStartDate" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                <asp:RequiredFieldValidator ID="xx" runat="server" ControlToValidate="txStartDate"
                    ErrorMessage="&nbsp" Text="*" ValidationGroup="Filter">
                </asp:RequiredFieldValidator>
                <asp:CompareValidator
                    ID="CompareValidator1" runat="server" ControlToValidate="txStartDate" Display="Dynamic"
                    ErrorMessage="E&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date" ValidationGroup="Filter"></asp:CompareValidator>
            </td>
            <td>
                End Date</td>
            <td>
                <asp:TextBox ID="txEndDate" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txEndDate"
                    ErrorMessage="&nbsp" Text="*" ValidationGroup="Filter">
                </asp:RequiredFieldValidator>
                <asp:CompareValidator
                    ID="CompareValidator2" runat="server" ControlToValidate="txEndDate" Display="Dynamic"
                    ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date" ValidationGroup="Filter"></asp:CompareValidator>
            </td>
            <td>
                <asp:Button ID="btnDateFilter" runat="server" Text="Filter By Submission Date"  OnClick="btnDateFilter_Click" ValidationGroup="Filter" />
            </td>
            <td>
                <asp:Button ID="btnClearFilter" runat="server" Text="Clear Date Filter"  OnClick="btnClearFilter_Click" CausesValidation="false" />
            </td>
        </tr>
    </table>
            <br />
    <%-- Start Customizable Grid--%>
    <UserControls:Grid ID="gvSubmissions" runat="server" AllowPaging="True" AutoGenerateColumns="False" AllowSorting="True"
     DataKeyNames="id,ClientCore_Client_Id,submission_number,submission_number_sequence" EmptyDataText="No Submissions were found for the selected agency."
     OnInit="gvSubmissions_Init"
     OnSelectedIndexChanged="gvSubmissions_SelectedIndexChanged"
     OnPageIndexChanging="gvSubmissions_PageIndexChanging"
     OnDataBound="gvSubmissions_DataBound"
     OnRowDeleting="gvSubmissions_Deleting"
     OnSorting="gvSubmissions_Sorting" >
     <Columns>
            <%--TODO: proposed remove any edits grids--%>
            <asp:TemplateField>
            <ItemTemplate>
                <asp:Image ID="Image1" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("folder_find.gif") %>' AlternateText="View Status History" />
                <ajx:ModalPopupExtender runat="server" ID="programmaticModalPopup"
                    TargetControlID="Image1"
                    PopupControlID="programmaticPopup" 
                    BackgroundCssClass="modalBackground"
                    DropShadow="True"
                    RepositionMode="RepositionOnWindowScroll" >
                </ajx:ModalPopupExtender>
                <asp:Panel ID="programmaticPopup" runat="server" Style="display: none" CssClass="modalPopup" OnLoad="Panel1_Load" Width="520px">
                <div>
                    <aje:UpdatePanel ID="UpdatePanel81" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                    <asp:Label id="lblSubmissionDetails" runat="server"></asp:Label>
                    <asp:Label id="lblCurrentStatus" runat="server"></asp:Label>
                    <asp:PlaceHolder ID="HistoryStatusPH" runat="server"></asp:PlaceHolder>
                    <br /><br />
                    <UserControls:Grid id="GridView1" runat="server" CssClass="grid" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" EmptyDataText="There is no history for this submission." AllowPaging="True" AutoGenerateColumns="False"
                    DataSourceID="ObjectDataSource1" DataKeyNames="Id" OnRowDataBound="GridView1_RowDataBound" Width="520px">
                        <EmptyDataTemplate>
                            <table width="100%">
                              <tr>
                                <td style="border: none;"> There is no history for this submission. </td>
                              </tr>
                            </table>
                        </EmptyDataTemplate>
                        <Columns>
                        <asp:BoundField DataField="Id" Visible="False" HeaderText="Id"></asp:BoundField>
                        <asp:BoundField DataField="SubmissionId" Visible="False" HeaderText="SubmissionId"></asp:BoundField>
                        <asp:BoundField DataField="PreviousSubmissionStatusId" Visible="False" HeaderText="PreviousSubmissionStatusId"></asp:BoundField>
                        <asp:TemplateField HeaderText="Status Date"><ItemTemplate>
                                        <asp:Label ID="lblStatusDate" runat="server" Text='<%# Bind("SubmissionStatusDate", "{0:MM/dd/yyyy}") %>' ></asp:Label>
                                    
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Previous Submission Status"><ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "SubmissionStatus.StatusCode") %>'></asp:Label>
                                    
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status Changed Date"><ItemTemplate>
                                        <asp:Label ID="lblDate" runat="server" Text='<%# Bind("StatusChangeDt", "{0:MM/dd/yyyy HH:mm}") %>' ></asp:Label>
                                    
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="StatusChangeBy" HeaderText="Status Changed By"></asp:BoundField>
                        <asp:TemplateField HeaderText="Active" ShowHeader="False"><ItemTemplate>
                            <asp:LinkButton runat="server" Text="Active" CommandName="Select" CausesValidation="false" ID="LinkButton1"></asp:LinkButton>
                            <ajx:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="LinkButton1"
                             ConfirmText="Are you sure you want to make this status history inactive, this cannot be undone?"></ajx:ConfirmButtonExtender>
                        </ItemTemplate>
                        </asp:TemplateField>
                        </Columns>
                    <PagerTemplate>
                    <asp:PlaceHolder runat="server"></asp:PlaceHolder>
                    </PagerTemplate>
                    </UserControls:Grid> <asp:ObjectDataSource id="ObjectDataSource1" runat="server" TypeName="BCS.Core.Clearance.Submissions" SelectMethod="GetSubmissionStatusHistory" OldValuesParameterFormatString="original_{0}"><SelectParameters>
                    <asp:Parameter Type="Int32" DefaultValue="0" Name="submissionId" />
                    </SelectParameters>
                    </asp:ObjectDataSource> 
                    </ContentTemplate>
                        </aje:UpdatePanel>
                        
                        <p style="text-align: center;">
                            <asp:Button ID="OkButton" runat="server" Text="OK" Visible="False" CausesValidation="False"/>
                            <asp:Button ID="CancelButton" runat="server" Text="Done" CausesValidation="False"/>
                        </p>
                    </div>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField ShowHeader="False" Visible='<%$ Code: BCS.WebApp.Components.Security.CanDelete() %>'>
            <ItemTemplate>
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                     Text="Delete" />
                <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="btnDelete"
                    ConfirmText='<%# DataBinder.Eval(Container.DataItem, "submission_number", "Are you sure you want to delete this Submission \"{0}\"?") %>' />
            </ItemTemplate>
        </asp:TemplateField>
             </Columns>
    </UserControls:Grid>
    <%-- End Customizable Grid--%>
    
    </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
</asp:Content>

