<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="SubSearch.aspx.cs" Inherits="SubSearch" Title="Berkley Clearance System - Submission" Theme="DefaultTheme" %>
<%@ MasterType VirtualPath="~/Site.PATS.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<ajx:ToolkitScriptManager ID="ScriptManager1" runat="server" CombineScripts="True" >
</ajx:ToolkitScriptManager>
<aje:UpdatePanel ID="MainUpdatePanel" runat="server">
<ContentTemplate>
    <table cellpadding="0" cellspacing="0" class="box" width="500" style="margin-top: 5px">
        <tbody>
        <tr>
            <td class="TL"><div /></td>
            <td class="TC">Load Submission</td>
            <td class="TR"><div /></td>
        </tr>
        <tr>
            <td class="ML"><div /></td>
            <td class="MC" style="width: 100%">
                <asp:Panel ID="Panel1" runat="server" DefaultButton="Button1">
                    <p>Enter submission number or policy number below and click <b>"Load Submission"</b> to load the submission detail.</p>
                    <table cellpadding="0" cells pacing="0" >
                        <tr>
                            <td>Submission Number <br /><asp:TextBox id="txSubBo" runat="server"></asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txSubBo"
                                Display="None" Text="*" ErrorMessage="Submission Number is required."
                                SetFocusOnError="true" ValidationGroup="S">
                            </asp:RequiredFieldValidator></td>
                            <td><br />:&nbsp;<asp:TextBox ID="txSeq" runat="server" ValidationGroup="S" Columns="2"></asp:TextBox>&nbsp;<em>(Optional sequence number)</em></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txSubBo"
                                    ErrorMessage="Submission Number is invalid." Text=""
                                    MinimumValue="0" MaximumValue='<%$ Code: Int64.MaxValue %>'
                                    Display="None" Type="Double" ValidationGroup="S">
                                </asp:RangeValidator>
                            </td>
                            <td>
                                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txSeq"
                                    ErrorMessage="Sequence is invalid." Text=""
                                    MinimumValue="0" MaximumValue='<%$ Code: Int32.MaxValue %>'
                                    Display="None" Type="Integer" ValidationGroup="S" >
                                </asp:RangeValidator>
                            </td>
                        </tr>
                    </table>
                    <p><asp:button id="Button1" runat="server" Text="Load Submission" OnClick="Button1_Click" AccessKey="1" ValidationGroup="S"
                        OnInit="ValidationButton_Init"></asp:button></p>
                </asp:Panel>
                <asp:Panel ID="Panel2" runat="server" DefaultButton="Button2">
                    <p></p>
                    <table cellpadding="0" cellspacing="0" >
                        <tr>
                            <td>
                                Policy Number <br />
                                <asp:TextBox id="txPol" runat="server" ValidationGroup="P"></asp:TextBox>&nbsp;:
                            </td>
                            <td>
                                <br />
                                <asp:TextBox id="txPolSeq" runat="server" ValidationGroup="P" Columns="2"></asp:TextBox>&nbsp;<em>(Optional sequence number)
                            </td>
                        </tr>
                    </table>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txPol" Display="None" Text=""
                        ErrorMessage="Policy Number is required." SetFocusOnError="true" ValidationGroup="P">
                    </asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txPolSeq"
                        ErrorMessage="Sequence is invalid." Text=""
                        MinimumValue="0" MaximumValue='<%$ Code: Int32.MaxValue %>'
                        Display="None" Type="Integer" ValidationGroup="P" >
                    </asp:RangeValidator>
                    <p><asp:button id="Button2" runat="server" Text="Load Submission" OnClick="Button2_Click" AccessKey="2" ValidationGroup="P"
                        OnInit="ValidationButton_Init"></asp:button></p>
                </asp:Panel>
            </td>
            <td class="MR"><div /></td>
        </tr>
        <tr>
            <td class="BL"><div/></td>
            <td class="BC"><div/></td>
            <td class="BR"><div/></td>
        </tr>
        </tbody>
    </table>
    <table id="ValidationTable" runat="server" cellpadding="0" cellspacing="0" class="box" width="30%" style="display: none;">
       
        <tr>
            <td class="TL"><div /></td>
            <td class="TC">Validation</td>
            <td class="TR"><div /></td>
        </tr>
        <tr>
            <td class="ML"><div /></td>
            <td class="MC" style="width: 100%">
                <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="S" SkinID="NewOneToBeDefaulted"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="P" SkinID="NewOneToBeDefaulted"></asp:ValidationSummary>
                </asp:PlaceHolder>
            </td>
            <td class="MR"><div /></td>
        </tr>
        <tr>
            <td class="BL"><div/></td>
            <td class="BC"><div/></td>
            <td class="BR"><div/></td>
        </tr>
        
    </table>
    <ajx:AlwaysVisibleControlExtender ID="avceVerify" runat="server"
        BehaviorID="alwaysVisible"
        TargetControlID="ValidationTable"
        VerticalOffset="100"
        HorizontalSide="Right">
    </ajx:AlwaysVisibleControlExtender>
</ContentTemplate>
</aje:UpdatePanel>
</asp:Content>