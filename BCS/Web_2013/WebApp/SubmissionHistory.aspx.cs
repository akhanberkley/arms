using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.Biz;
using AjaxControlToolkit;

public partial class SubmissionHistory : System.Web.UI.Page
{
    static string done = "x";
    static object objLock = new object();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            done = "x";
    }
    protected override void OnInit(EventArgs e)
    {
        if (Request.QueryString.Count > 0 && Request.QueryString["op"] != null)
        {
            TSHAK.Components.SecureQueryString qs = new TSHAK.Components.SecureQueryString(BCS.Core.Core.Key, Request.QueryString["op"]);
            ObjectDataSource1.SelectParameters.Add("submissionId", qs["id"]);
            
            BCS.Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Submission.Columns.Id, qs["id"]);
            BCS.Biz.Submission s = dm.GetSubmission(BCS.Biz.FetchPath.Submission.SubmissionStatus);
            if (s.SubmissionStatus != null)
                lblCurrentStatus.Text = "Current Submission Status : " + s.SubmissionStatus.StatusCode;
            else
                lblCurrentStatus.Text = "Submission Status is not assigned to this submission.";
        }            
        base.OnInit(e);
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        lock (objLock)
        {
            if (hdn.Value.Length > done.Length || done == "x")
            { 
                // setup done so that it helps in deciding refresh or not
                done = hdn.Value;
                
                //Do work required in case of genuine postback
                string result = BCS.Core.Clearance.Submissions.ToggleSubmissionStatusHistoryActive(Convert.ToInt32(GridView1.SelectedDataKey["Id"]), Common.GetUser());
                Common.SetStatus(messagePlaceHolder, result, true);
            }
        }
        GridView1.SelectedIndex = -1;
        GridView1.DataBind();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SubmissionStatusHistory aHistory = e.Row.DataItem as SubmissionStatusHistory;
            if (!aHistory.Active)
            {
                foreach (TableCell var in e.Row.Cells)
                {
                    var.Style.Add(HtmlTextWriterStyle.TextDecoration, "line-through");
                }
            }

            LinkButton lb = e.Row.FindControl("LinkButton1") as LinkButton;
            ConfirmButtonExtender confirmButtonExtender = e.Row.FindControl("ConfirmButtonExtender1") as ConfirmButtonExtender;
            if (lb != null)
            {
                lb.Text = aHistory.Active ? "active" : "inactive";
                lb.Enabled = aHistory.Active.Value || User.IsInRole("System Administrator");
            }
            if (confirmButtonExtender != null)
            {
                confirmButtonExtender.Enabled = aHistory.Active.Value || User.IsInRole("System Administrator");
                if (User.IsInRole("System Administrator"))
                {
                    confirmButtonExtender.ConfirmText = "Are you sure you want to change this status history?";
                }
            }
        }
    }

    protected void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        throw new ApplicationException("Ajax Error", e.Exception);
    }
}
