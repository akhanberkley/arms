<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="PersonAssignments.aspx.cs" Inherits="PersonAssignments" Title="Berkley Clearance System - Person Assignments" Theme="DefaultTheme" %>
<%@ Register TagPrefix="UserControls" Namespace="BCS.WebApp.UserControls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<ajx:ToolkitScriptManager ID="ScriptManager1" runat="server" CombineScripts="True" >
</ajx:ToolkitScriptManager>
<asp:PlaceHolder runat="server" ID="messagePlaceHolder">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
    <br />
</asp:PlaceHolder>
<script type="text/javascript">
   function Confirm(source, clientside_arguments)
   {
      if (document.getElementById('<%=DropDownList2.ClientID%>').value == 0 || document.getElementById('<%=DropDownList1.ClientID%>').value == 0 )
      {
         clientside_arguments.IsValid=false;
         source.innerText = "Please select From and To Persons.";
         if (document.getElementById('_ctl0_ContentPlaceHolder1_errMessage') != null)
            document.getElementById('_ctl0_ContentPlaceHolder1_errMessage').innerText = '';
         return;
      }
      clientside_arguments.IsValid = confirm('Existing roles of the destination person will be overwritten?');
      source.innerText = '';
      return;
   }
</script>
<table cellpadding="0" cellspacing="0" class="box" >
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Person Assignments</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        
    <asp:ObjectDataSource ID="odsPersons" runat="server" SelectMethod="GetPersons" TypeName="BCS.Core.Clearance.Admin.DataAccess" OnSelected="odsPersons_Selected">
        <SelectParameters>
            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="companyNumberId" SessionField="CompanyNumberSelected"
                Type="Int32" />
            <asp:Parameter DefaultValue=""  Name="username" />
            <asp:Parameter DefaultValue="" Name="fullname" />
        </SelectParameters>
    </asp:ObjectDataSource>    
    <br />
    <table width="100%">
        <tr>
            <td style="vertical-align: bottom; white-space:nowrap;"><span> Select Person : </span><br />
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="odsPersons" DataTextField="FullName"
                    DataValueField="Id" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" OnPreRender="ddl_PreRender" ValidationGroup="Save Copy Move">
                </asp:DropDownList>
                <ajx:ListSearchExtender ID="ListSearchExtender1" runat="server" TargetControlID="DropDownList1" PromptPosition="Right"></ajx:ListSearchExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="DropDownList1"
                    Display="Dynamic" ErrorMessage="&nbsp;" Text="*" InitialValue="0" ValidationGroup="Save"></asp:RequiredFieldValidator></td>
            <td style="vertical-align: bottom; white-space:nowrap;">
                <asp:Button ID="btnCopy" runat="server"  Text="Copy To" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' OnClick="btnCopy_Click" ValidationGroup="CopyMove" />                
            </td>
            <td style="vertical-align: bottom; white-space:nowrap;">
                &nbsp;<asp:Button ID="btnMove" runat="server"  Text="Move To" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' OnClick="btnMove_Click" ValidationGroup="CopyMove" />&nbsp;
                <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="odsPersons" DataTextField="FullName" DataValueField="Id" OnPreRender="ddl_PreRender">
                </asp:DropDownList>
                <ajx:ListSearchExtender ID="ListSearchExtender2" runat="server" TargetControlID="DropDownList2"></ajx:ListSearchExtender>
            </td>            
        </tr>
        <tr><td> &nbsp; </td><td style="width: 75px"></td><td></td></tr>
        <tr><td> &nbsp; </td><td style="width: 75px"></td><td></td></tr>
        <tr>
            <td style="width:45%; vertical-align: top;">
                <span>Assigned Agencies</span><br /><asp:ListBox ID="ListBox1" runat="server" DataSourceID="odsPA" DataTextField="AgencyName"
                    DataValueField="Id" OnDataBound="ListBox1_DataBound" Width="100%" Rows="10" ValidationGroup="Save" OnPreRender="SortOnPreRender" AutoPostBack="True" OnSelectedIndexChanged="ListBox1_SelectedIndexChanged">
                </asp:ListBox>
                <ajx:ListSearchExtender ID="ListSearchExtender3" runat="server" TargetControlID="ListBox1" PromptPosition="Right"></ajx:ListSearchExtender>
                <asp:ObjectDataSource ID="odsPA" runat="server" OnSelected="odsPA_Selected"
                    SelectMethod="GetAgenciesOfPerson" TypeName="BCS.Core.Clearance.Admin.DataAccess">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DropDownList1" Name="personid" PropertyName="SelectedValue"
                        Type="Int32" />
                </SelectParameters>
                </asp:ObjectDataSource>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ListBox1"
                    ErrorMessage="&nbsp;" Text="* Please select the agency to be saved." ValidationGroup="Save"></asp:RequiredFieldValidator>
            </td>
            <td align="center" style="width: 75px">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:Button ID="btnAddAgency" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' Text=">" OnClick="btnAddAgency_Click" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Button ID="btnRemoveAgency" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text="<" OnClick="btnRemoveAgency_Click" />
                        </td>
                    </tr>
                </table>
            </td>       
            <td style="width:45%; vertical-align: top;">
                <span>Available Agencies</span><br />
                <asp:ListBox ID="ListBox2" runat="server" DataSourceID="odsPNA" DataTextField="AgencyName"
                    DataValueField="Id" SelectionMode="Multiple" Width="100%" Rows="10" OnPreRender="SortOnPreRender"></asp:ListBox>
                <ajx:ListSearchExtender ID="ListSearchExtender4" runat="server" TargetControlID="ListBox2" PromptPosition="Right"></ajx:ListSearchExtender>
                <asp:ObjectDataSource ID="odsPNA" runat="server"
                        SelectMethod="GetAgenciesNotOfPerson" TypeName="BCS.Core.Clearance.Admin.DataAccess" >
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList1" Name="personid" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:Parameter Name="personAgencyIds" Type="Object" />
                        
                    </SelectParameters>
               </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td style="height: 18px">
                Assigned Underwriting Units
            </td>
            <td style="width: 75px; height: 18px;">
                
            </td>
            <td style="height: 18px">
                Available Underwriting Units
            </td>
        </tr>
        <tr>
            <td width="45%">
                <asp:ListBox ID="ListBox3" runat="server" DataSourceID="ObjectDataSource1" DataTextField="Code" DataValueField="Id" SelectionMode="Multiple" Width="100%" Rows="7" ValidationGroup="Save" AutoPostBack="True" OnSelectedIndexChanged="ListBox3_SelectedIndexChanged"></asp:ListBox><asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetLineOfBusinessesByAgencyIds"
                    TypeName="BCS.Core.Clearance.Admin.DataAccess" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                        <asp:ControlParameter ControlID="DropDownList1" Name="personId" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:Parameter Name="agencyIds" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                </td>
            <td align="center" style="width: 75px">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:Button ID="btnAddLOB" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text=">" OnClick="btnAddLOB_Click" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Button ID="btnRemoveLOB" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' Text="<" OnClick="btnRemoveLOB_Click" />
                        </td>
                    </tr>
                </table>
            </td>       
            <td width="45%">
                <asp:ListBox ID="ListBox4" runat="server" DataSourceID="ObjectDataSource2" DataTextField="Code" DataValueField="Id" SelectionMode="Multiple" Width="100%" Rows="7"></asp:ListBox><asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetLineOfBusinessesNotByAgencyIds"
                    TypeName="BCS.Core.Clearance.Admin.DataAccess" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                        <asp:ControlParameter ControlID="DropDownList1" Name="personId" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:Parameter Name="agencyIds" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                Assigned
                Roles</td>
            <td style="width: 75px">
                
            </td>
            <td>
                Available Roles</td>
        </tr>
        <tr>        
            <td width="45%">
                <asp:ListBox ID="ListBox5" runat="server" DataSourceID="odsPR" DataTextField="RoleName" DataValueField="Id" SelectionMode="Multiple" Width="100%" ValidationGroup="Save" AutoPostBack="True" OnSelectedIndexChanged="ListBox5_SelectedIndexChanged" ></asp:ListBox><asp:ObjectDataSource ID="odsPR" runat="server" SelectMethod="GetRolesOfPerson"
                    TypeName="BCS.Core.Clearance.Admin.DataAccess">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList1" Name="personid" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="ListBox1" Name="agencyId" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                </td>
            <td align="center" style="width: 75px">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:Button ID="btnAddRole" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' Text=">" OnClick="btnAddRole_Click" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Button ID="btnRemoveRole" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text="<" OnClick="btnRemoveRole_Click" />
                        </td>
                    </tr>
                </table>
            </td>       
            <td width="45%">
                <asp:ListBox ID="ListBox6" runat="server" DataSourceID="odsPNR" DataTextField="RoleName" DataValueField="Id" SelectionMode="Multiple" Width="100%" ></asp:ListBox><asp:ObjectDataSource ID="odsPNR" runat="server" SelectMethod="GetRolesNotOfPerson"
                    TypeName="BCS.Core.Clearance.Admin.DataAccess">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList1" Name="personid" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="ListBox1" Name="agencyId" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                   
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:CheckBox ID="cbPrimary" runat="server" Text="Set as primary" />&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">                
                <asp:Button ID="btnSave" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text="Save" OnClick="btnSave_Click" ValidationGroup="Save"/>&nbsp;
                <asp:Button ID="Button1" runat="server"  Text="Cancel" OnClick="Button1_Click" CausesValidation="False"/></td>
        </tr>
    </table>
    <div>
    <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="Confirm" CssClass="Error" Display="Dynamic" ValidationGroup="CopyMove" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
    </div>
    
    </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
</asp:Content>

