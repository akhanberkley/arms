<%@ Page language="c#" MasterPageFile="~/Site.PATS.master" Title="Berkley Clearance System : Client" Inherits="BCS.WebApp.Client" CodeFile="Client.aspx.cs" Theme="DefaultTheme" ValidateRequest="False" %>
<%@ Register Src="ClientNameSelector.ascx" TagName="ClientNameSelector" TagPrefix="uc1" %>
<%@ Register Src="~/ClientAddress.ascx" TagName="ClientAddress" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%--

TODO:

Temporary (hidden field) workaround for linkbutton clicks not restoring parent as active tab, instead restores to last active tab other than parent.
http://forums.asp.net/p/1068120/1550723.aspx#1550723

Caution :: using VS designer with ajax tab container and panel adds unnecessary attributes to tags, and sets unintended properties.

--%>
<%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="http://cloud.github.com/downloads/digitalBush/jquery.maskedinput/jquery.maskedinput-1.3.min.js"></script>--%>

<script type="text/javascript" src="Javascripts/jquery-1.6.2.js"></script>
<script type="text/javascript" src="Javascripts/jquery-1.8.1.js"></script>
<script type="text/javascript" src="Javascripts/maskedInput.js"></script>
<%--<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>--%>
<script type="text/javascript">
    $(document).ready(function () {
        $("input[id$=txtBusinessPhone]").mask("(999) 999-9999");
    });
    function ActiveTabChanged(sender, e) {
        var activeTab = $get('<%=activeTab.ClientID%>');
        activeTab.value = sender.get_activeTabIndex();
    }
    var restoreSICCode = "";
    function itemSelected( source, eventArgs )
    {
       var ss = eventArgs.get_value().split("|");
       $get('<%=hdnSICCode.ClientID%>').value = ss[0];
       $get('<%=litSICCodeDesc.ClientID%>').value = ss[1];
       restoreSICCode = eventArgs.get_text();
    }
       function sicCodeSelectionIgnored(elem)
       {
        if( $get('<%=hdnSICCode.ClientID%>').value == '0' || $get('<%=txSICCode.ClientID%>').value.length == 0)
        {
            $get('<%=txSICCode.ClientID%>').value = '';
            $get('<%=litSICCodeDesc.ClientID%>').value = '';
            $get('<%=hdnSICCode.ClientID%>').value = '0';
        }
        else
        {
            elem.value = restoreSICCode;
        }
       }
       function sicCodeSelectionInit(elem)
       {
        restoreSICCode = $get('<%=txSICCode.ClientID%>').value;
    }

    var restoreNAICSCode = "";
    function naicsItemSelected(source, eventArgs) {
        var ss = eventArgs.get_value().split("|");
        $get('<%=hdnNAICSCode.ClientID%>').value = ss[0];
        $get('<%=litNAICSCodeDesc.ClientID%>').value = ss[1];
        restoreNAICSCode = eventArgs.get_text();
       // fillSicCodes();
    }

    function naicsCodeSelectionIgnored(elem) {
       if ($get('<%=hdnNAICSCode.ClientID%>').value == '0' || $get('<%=txtNAICSCode.ClientID%>').value.length == 0) {
            $get('<%=txtNAICSCode.ClientID%>').value = '';
            $get('<%=litNAICSCodeDesc.ClientID%>').value = '';
            $get('<%=hdnNAICSCode.ClientID%>').value = '0';
        }
        else {
            elem.value = restoreNAICSCode;
        }
    }
    function naicsCodeSelectionInit(elem) {
        restoreNAICSCode = $get('<%=txtNAICSCode.ClientID%>').value;
      }

    function fillSicCodes() {
        var naicsCode = $get('<%=hdnNAICSCode.ClientID%>').value;
        PageMethods.GetSicCodeAndDescription(naicsCode, fillSicCode_Success,fillSicCode_fail);
    }

    function fillSicCode_Success(result) {
        var siccode = result.split("|");
        $get('<%=txSICCode.ClientID%>').value = siccode[1];
        $get('<%=litSICCodeDesc.ClientID%>').value = siccode[2];
        $get('<%=hdnSICCode.ClientID%>').value = siccode[0];
        restoreSICCode = siccode[1];
    }

    function fillSicCode_fail() {

    }
 </script>
<asp:HiddenField ID="activeTab" runat="server" />
<ajx:ToolkitScriptManager id="ScriptManager1" runat="server" CombineScripts="True" OnAsyncPostBackError="ScriptManager1_AsyncPostBackError" EnablePageMethods="true">
    <Services>
        <aje:ServiceReference  Path="WebServices/SubmissionService.asmx" />
    </Services>
</ajx:ToolkitScriptManager>

<script language="javascript" type="text/javascript">
    <!-- 
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    
    prm.add_initializeRequest(InitializeRequest);
    prm.add_endRequest(EndRequest);
    
    function InitializeRequest(sender, args) {
            CancelIfInAsyncPostBack();
        var pop = $find("mpeUpdateProgressBehavior");
        pop.show();
    }
   
    function EndRequest(sender, args) {
        var pop = $find("mpeUpdateProgressBehavior");
        pop.hide();
    }
    function CancelIfInAsyncPostBack() {
        if (prm.get_isInAsyncPostBack()) {
          prm.abortPostBack();
        }
    }
        
    if (typeof(Sys) !== "undefined") Sys.Application.notifyScriptLoaded();
    // -->
    </script>
<script type="text/javascript" id="verification">
var dontKnowImageSrc = '';
var lastVerifiedId = '';
Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);
function pageLoaded(sender, args)
{
    if(document.getElementById('<%= validate.ClientID %>') != null)
    dontKnowImageSrc = document.getElementById('<%= validate.ClientID %>').src;
    if($find("beh") != null)
        $find("beh").add_shown(PopShown);

    $("input[id$=txtBusinessPhone]").mask("(999) 999-9999");
}


function showPopup()
{
    $find("beh").add_shown(PopShown);
    $find("beh").show();
}
function PopShown(sender, args)
{
    if($get('<%=programmaticPopup.ClientID %>').style.maxHeight == null); // try maxHeight behavior for IE6
    {
        if($get('<%=programmaticPopup.ClientID %>').offsetHeight > window.screen.availHeight - 100) // - 100, some arbitrary value
        {
            $get('<%=programmaticPopup.ClientID %>').style.height = '600px';
            $find('beh')._layout();
        }
    }
}
function portolioChanged()
{
    var RsltElem =  document.getElementById('<%= validate.ClientID %>');
    RsltElem.src = dontKnowImageSrc;
}
// This function calls the Web service method 
// passing the method name.  
function verifyPortfolio()
{
    var PortElem =  document.getElementById('<%= txtPort.ClientID %>');
    if(lastVerifiedId != PortElem.value)
    {
        var nameElem =  document.getElementById('portfolioName');
        nameElem.innerHTML = "Please wait...";
        lastVerifiedId = PortElem.value;
        BCS.WebApp.ScriptServices.SubmissionService.Verify(PortElem.value, SucceededCallbackWithContext, FailedCallback);
    }
}

// This is the callback function called if the
// Web service succeeded. It accepts the result
// object, the user context, and the method name as
// parameters.
function SucceededCallbackWithContext(result, userContext, methodName)
{
	// Page element to display the result.    
    var RsltElem =  document.getElementById('<%= validate.ClientID %>');
    var nameElem =  document.getElementById('portfolioName');
    
    var parts = result.split('|');
    
  	RsltElem.src =  parts[0];
    nameElem.innerHTML = parts[1];
 	
}

// This is the callback function called if the
// Web service failed. It accepts the error object
// as a parameter.
function FailedCallback(error)
{
    // Display the error.    
    var RsltElem = document.getElementById('<%= validate.ClientID %>');
    var nameElem =  document.getElementById('portfolioName');
    nameElem.innerHTML = "";
    RsltElem.src = dontKnowImageSrc;
}

if (typeof(Sys) !== "undefined") Sys.Application.notifyScriptLoaded();

</script>
<script type="text/javascript" id="classCodeValidation">

var restoreClassCode = "0";
function storeClassClode(ddl)
{
    restoreClassCode = ddl.value;
}
function classCodeValidate(ddl, scwEvt)
{
   var aE = document.getElementById('<%= ExistingClassCodes.ClientID %>').value;
    
   var splits = aE.split(',');
   for (var i=0; i < splits.length; i++)
   {
    if (ddl.value == splits[i])
    {
        alert('The class code is already assigned. Please select another one.');
        ddl.value = restoreClassCode;
        
        // stop the postback
         if (scwEvt.stopPropagation)
                scwEvt.stopPropagation();    // Capture phase
         else   scwEvt.cancelBubble = true;  // Bubbling phase

         if (scwEvt.preventDefault)
                scwEvt.preventDefault();     // Capture phase
         else   scwEvt.returnValue=false;    // Bubbling phase
         
        // end stop the postback
         
         // revert to original selection         
         return false;
    }
   }
   return true;
}

</script> 
<aje:UpdatePanel ID="UpdatePanel81" runat="server">
    <ContentTemplate>
            <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ></asp:ValidationSummary>
                <br />
            </asp:PlaceHolder>
        </ContentTemplate>
</aje:UpdatePanel>

<aje:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" >
        <ProgressTemplate>
            <asp:Panel ID="PNLProgress" runat="server" style="width:400px; background-color:White; border-width:2px; border-color:Black; border-style:solid; padding:20px;">
                <img id="imgProcessimg" runat="server" alt="processing" src='<%$ Code : Common.GetImageUrl("indicator.gif") %>' /> &nbsp; Processing... Please Wait....
            </asp:Panel>
            <ajx:ModalPopupExtender BackgroundCssClass="modalBackground" ID="mpeUpdateProgress" BehaviorID="mpeUpdateProgressBehavior" TargetControlID="PNLProgress" runat="server" PopupControlID="UpdateProgress1">
    </ajx:ModalPopupExtender>
        </ProgressTemplate>
    </aje:UpdateProgress>

<aje:UpdatePanel ID="MainUpdatePanel" runat="server">
    <ContentTemplate>
	
	<table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">
            <span style="font-size: 14px;">Client 
	        <asp:PlaceHolder ID="phid" Visible="false" runat="server">
	            (#<asp:Label ID="lblClientId" runat="server"></asp:Label>)
	            <asp:Image ID="MergeImage" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("h3Arrow.gif") %>' onclick="showPopup();" AlternateText="Merge" OnInit="MergeImage_Init" />
	            <asp:Image ID="dummyImage" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("h3Arrow.gif") %>' style="display: none;" />
                <ajx:ModalPopupExtender runat="server" ID="programmaticModalPopup"
                        BehaviorID="beh"
                        TargetControlID="dummyImage"
                        PopupControlID="programmaticPopup" 
                        BackgroundCssClass="modalBackground"
                        DropShadow="True"
                        RepositionMode="RepositionOnWindowScroll" >
                </ajx:ModalPopupExtender>
	        </asp:PlaceHolder>: <asp:Literal ID="lblOperation" runat="server" EnableViewState="false"></asp:Literal></span>
	    </td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
            
            <asp:Panel ID="pnlNew" runat="server" DefaultButton="btnSaveNew">
            
                <table>
                    <tr>
                        <td valign="top">
                            <table cellpadding="5" cellspacing="0">
                                <tr id="client_row_business_phone">
                                 <td> Business Phone<br />
                                     <asp:TextBox ID="txtBusinessPhone" runat="server" Width="171px"></asp:TextBox>
                                     <%--<ajx:MaskedEditExtender ID="mskExtender" TargetControlID="txtBusinessPhone" Mask="(999)999-9999" MessageValidatorTip="true" MaskType="Number" runat="server"
                                InputDirection="LeftToRight" AcceptNegative="None" ErrorTooltipEnabled="true"  />
        <ajx:MaskedEditValidator ID ="mskValidator" ControlExtender="mskExtender" ControlToValidate="txtBusinessPhone" runat="server" ToolTip="Enter a Valid Phone Number"  />--%>
                                 </td>
                                </tr>
                                <tr id="client_row_business_description">
                                    <td>Business Description<br />
                                        <asp:TextBox ID="txtBusinessDescription" Width="190px" Height="90px" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="client_row_sic_code">
                                    <td>SIC Code<br />
                                        <asp:TextBox ID="txSICCode" runat="server" Width="171px" OnInit="txSICCode_Init"></asp:TextBox>
                                       <ajx:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" ServiceMethod="GetCompletionList"
                                            TargetControlID="txSICCode" UseContextKey="True" MinimumPrefixLength="1" CompletionSetCount="13" FirstRowSelected="true"
                                            CompletionInterval="100"
                                            CompletionListCssClass="autocomplete_completionListElement" 
                                            CompletionListItemCssClass="autocomplete_listItem" 
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                            OnClientItemSelected="itemSelected"
                                            Enabled="True" >
                                        </ajx:AutoCompleteExtender>
                                        &nbsp;<asp:ImageButton ID="txtSICLookup" AlternateText="Lookup" ImageUrl='<%$ Code : Common.GetImageUrl("lookup.gif") %>' Enabled='<%$ Code: !Common.IsClientReadOnly() %>' runat="server" OnInit="txtSICLookup_Init" />
                                        <input id="hdnSICCode" type="hidden" runat="server" value="0" style="width:210px;" />
                                        <br />
                                        <%--<input ID="litSICCodeDesc" style="border:none;font-style:italic; overflow: visible; width:190px;" contenteditable="false" runat="server"  />--%>
                                        <textarea  id="litSICCodeDesc" runat="server" readonly="readonly" rows="2" cols="20"
                                         style="border: none;font-style: italic;overflow: auto;width: 210px; max-height:210px; "></textarea>
                                    </td>
                                </tr>
                                <tr id="client_row_naics_code">
                                  <td>NAICS Code <br />
                                   <asp:TextBox ID="txtNAICSCode" runat="server" Width="171px" OnInit="txtNAICSCode_Init"></asp:TextBox>
                                   <ajx:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" ServiceMethod="GetNAICSCompletionList"
                                            TargetControlID="txtNAICSCode" UseContextKey="True" MinimumPrefixLength="1" CompletionSetCount="13" FirstRowSelected="true"
                                            CompletionInterval="100"
                                            CompletionListCssClass="autocomplete_completionListElement" 
                                            CompletionListItemCssClass="autocomplete_listItem" 
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                            OnClientItemSelected="naicsItemSelected"
                                            Enabled="True" >
                                        </ajx:AutoCompleteExtender>
                                         <input id="hdnNAICSCode" type="hidden" runat="server" value="0" style="width:210px;" />
                                         <br />
                                         <textarea  id="litNAICSCodeDesc" runat="server" readonly="readonly" rows="2" cols="20"
                                         style="border: none;font-style: italic;overflow: auto;width: 210px; max-height:210px; "></textarea>
                                   </td>
                                </tr>
                                <tr id="client_row_fein">
                                    <td valign="middle">FEIN/Tax Identification<br />
                                        <div id="client_row_tax_type">
                                        <asp:RadioButtonList ID="rblTaxType" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="FEIN" Value="FEIN" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="SSN" Value="SSN"></asp:ListItem>
                                        </asp:RadioButtonList>
                                        </div>
                                        <asp:TextBox ID="txtFEIN" runat="server" Width="190px"></asp:TextBox><br />                                        
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                        ErrorMessage="&nbsp;" Text="* ######### or FEIN Format ##-####### or Social Security Number format ###-##-####"
                                        ValidationExpression="\d{9}|[a-zA-Z\d]{2}-[a-zA-Z\d]{7}|\d{3}-\d{2}-\d{4}"
                                        ControlToValidate="txtFEIN" Display="Dynamic" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr id="rowPortfolio" runat="server" class="rowPortfolio">
                                    <td>Portfolio Id&nbsp;
                                        <img id="idq" runat="server" alt="tip" src='<%$ Code : Common.GetImageUrl("question.gif") %>' title="Be sure to use an existing client id, otherwise the edit will error." />
                                        <img alt="copy" runat="server" id="copy" src='<%$ Code : Common.GetImageUrl("copy.gif") %>' title="Copy Portfolio Id to clipboard" />
                                        <img onclick="verifyPortfolio();" alt="copy" runat="server" id="validate" src='<%$ Code : Common.GetImageUrl("validate.gif") %>' title="Validate Portfolio Id" class="default_portfoliosearch" />
                                        <label id="portfolioName" onclick="copyErrorToClipboard(this.innerHTML);" ></label>
                                        <br />
                                        <asp:TextBox ID="txtPort" runat="server" Width="190px" onchange="portolioChanged();"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Placeholder ID="phCodeTypes" runat="server"></asp:Placeholder>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Placeholder ID="phAttributes" runat="server"></asp:Placeholder>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">
                            <table>
                                <tr>
                                    <td>
                                        <ajx:TabContainer ID="TabContainer1" runat="server" OnClientActiveTabChanged="ActiveTabChanged" ActiveTabIndex="0">        
                                            <ajx:TabPanel ID="TabPanel1" runat="server" HeaderText="Names" >
                                            <ContentTemplate>
                                            <aje:UpdatePanel id="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                               <ContentTemplate>
                                                    <asp:Panel ID="pnlNames" runat="server" ScrollBars="Auto" Height="240px" Width="415px" CssClass="tabcontent">
                                                        <% if (AllowPrimaryNameChange)
                                                           { %>
                                                            <table style="font-weight: bold">
                                                                <tr>
                                                                    <td style="width: 57px" title="Is Primary Name">Primary</td>
                                                                    <td>Details</td>
                                                                </tr>
                                                            </table>
                                                        <% } %>
                                                        <UserControls:DynamicControlsPlaceholder ID="phNames" runat="server" ControlsWithoutIDs="Persist">
                                                        </UserControls:DynamicControlsPlaceholder>
                                                    </asp:Panel>
                                                    <div style="padding:10px; border-top: black 1px solid;">
                                                        <asp:Button ID="btnAddName" runat="server" Text="Additional Name" OnClick="btnAddName_Click"
                                                         Enabled='<%$ Code: !Common.IsClientReadOnly() %>' CausesValidation="false" />
                                                    </div>
                                                    </ContentTemplate>
                                                </aje:UpdatePanel>            
                                            </ContentTemplate>
                                            </ajx:TabPanel>
                                            <ajx:TabPanel ID="TabPanel2" runat="server" HeaderText="Addresses">
                                            <ContentTemplate>
                                                 <aje:UpdatePanel id="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                     <ContentTemplate>
                                                        <asp:Panel ID="pnlAddresses" runat="server" ScrollBars="Auto" Height="240px" CssClass="tabcontent">
                                                            <% if (AllowPrimaryNameChange)
                                                           { %>
                                                            <table style="font-weight: bold">
                                                                <tr>
                                                                    <td style="width: 57px" title="Is Primary Name">Primary</td>
                                                                    <td>Details</td>
                                                                </tr>
                                                            </table>
                                                        <% } %>
                                                            <UserControls:DynamicControlsPlaceholder ID="phAddresses" runat="server" ControlsWithoutIDs="Persist">                    
                                                            </UserControls:DynamicControlsPlaceholder>
                                                        </asp:Panel>
                                                        <div style="padding:10px; border-top: black 1px solid;">
                                                            <asp:Button ID="btnAddAddress" runat="server" Text="Additional Address" OnClick="btnAddAddress_Click"
                                                             Enabled='<%$ Code: !Common.IsClientReadOnly() %>' CausesValidation="false" />
                                                        </div>
                                                    </ContentTemplate>
                                                 </aje:UpdatePanel>
                                            </ContentTemplate>
                                            </ajx:TabPanel>
                                            <ajx:TabPanel ID="TabPanel4" runat="server" HeaderText="Class Codes" OnInit="ClassCodeTab_Init" >
                                                <ContentTemplate>
                                                    <aje:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="pnlClassCodesGrid" runat="server" ScrollBars="Auto" Height="360px" CssClass="tabcontent">
                                                                <asp:GridView id="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" AllowPaging="False"
                                                                 OnRowEditing="GridView1_RowEditing" OnRowDataBound="GridView1_RowDataBound" 
                                                                 OnRowUpdated="GridView1_RowUpdated" OnRowCommand="GridView1_RowCommand" OnInit="GridView1_Init" ShowFooter="false"
                                                                 DataSourceId="ObjectDataSource1" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnDataBound="GridView1_DataBound">
                                                                 <%--<Columns>
                                                                    <asp:TemplateField >
                                                  
                                                                        <ItemTemplate>
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        
                                                                                    </td>
                                                                                    <td>
                                                                                    </td>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3">
                                                                                        <asp:GridView runat="server" ID="gvHazardValues" AutoGenerateColumns="true"></asp:GridView>
                                                                                    </td>
                   
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3">
                                                                                        <asp:GridView runat="server" ID="gvClassCodeCompanyNumberCodes" AutoGenerateColumns="false">
                                                                                            <Columns>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemTemplate>
                                                                                                        <UserControls:ClassCodeDropDownList runat="server" AutoPostBack="false"></UserControls:ClassCodeDropDownList>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                 </Columns>--%>
                                                                </asp:GridView>
                                                                 <asp:ObjectDataSource id="ObjectDataSource1" runat="server" DeleteMethod="DeleteClassCodeHVOfAClient" TypeName="BCS.Core.Clearance.Admin.DataAccess"
                                                                  SelectMethod="GetClassCodesHVOfAClient" OnDeleted="ObjectDataSource1_Deleted" >
                                                                    <SelectParameters>
                                                                        <asp:SessionParameter Name="clientCoreClientId" SessionField="ClientId" Type="Int64" />
                                                                        <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected" Type="Int32" />
                                                                    </SelectParameters>
                                                                    <DeleteParameters>
                                                                        <asp:Parameter Name="id" Type="Int32" />
                                                                    </DeleteParameters>
                                                                                                                                       
                                                                </asp:ObjectDataSource>
                                                                <asp:HiddenField ID="ExistingClassCodes" runat="server" />
                                                            </asp:Panel>
                                                            <div style="padding:10px; border-top: black 1px solid;">
                                                                <asp:Button ID="btnAddClassCodeGrid" runat="server" Text="Additional Class Code" Enabled='<%$ Code: !Common.IsClientReadOnly() %>' OnClick="btnAddClassCodeOnGrid_Click" CausesValidation="False" />
                                                            </div>
                                                        </ContentTemplate>
                                                    </aje:UpdatePanel>
                                                </ContentTemplate>
                                            </ajx:TabPanel>
                                        </ajx:TabContainer>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="margin-top:20px">
                                            <asp:Button ID="btnSaveNew" runat="server" OnClick="btnSaveNew_Click" Enabled='<%$ Code: !Common.IsClientReadOnly() %>' Text="Create Client" AccessKey="S" />&nbsp;
                                            <asp:Button ID="Button2" runat="server" OnClick="btnCancel_Click" Text="Cancel" CausesValidation="False" />
                                            <asp:Button ID="btnRequestDelete" runat="server" Text="Send Request to Delete Client" OnClientClick="return confirm('Are you sure you want to delete this client?');"
                                                onclick="RequestClientDelete" Visible='<%$ Code: BCS.WebApp.Components.Security.CanDeleteClient() %>' Enabled='<%$ Code: BCS.WebApp.Components.Security.CanDeleteClient() %>' />
                                            <asp:HiddenField runat="server" ID="hdnDeleted" />
                                            <asp:Label runat="server" ID="lblDeleteMsg" Visible="false"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            
    <asp:Panel ID="programmaticPopup" runat="server" Style="display: none; max-height: 600px;" CssClass="modalPopup" Width="700px" ScrollBars="Auto">
        <div>
        <aje:UpdatePanel ID="UpdatePanel181" runat="server">
        <ContentTemplate>
        <asp:PlaceHolder ID="HistoryStatusPH" runat="server"></asp:PlaceHolder>
        <br /><br />
        <span class="heading">Merge</span>
        <table runat="server" id="tblClientInfo" cellpadding="8" cellspacing="0" width="100%">
            <tr>
               <th style="border: gray 1px dotted; vertical-align:top; color: white; background-color:#00563f;">From</th>
               <th style="border: gray 1px dotted; vertical-align:top; color: white; background-color:#00563f;">To</th>
            </tr>
            <tr>
                <td style="border: gray 1px dotted; vertical-align:top;">
                    <%--source client--%>
                    <h3 class="sectionGroupHeader">Client Information: #<asp:Label ID="sourceClientIdLabel" runat="server"></asp:Label></h3>
                    <h4 class="sectionGroupHeader">Client Names</h4>
                    <asp:CheckBoxList ID="cblNames" runat="server"></asp:CheckBoxList>
                    <h4 class="sectionGroupHeader">Client Addresses</h4>
                    <asp:CheckBoxList ID="cblAddresses" runat="server"></asp:CheckBoxList>
                </td>
                <td style="border: gray 1px dotted; vertical-align:top;">
                    <%--target client--%>
                    <h3 class="sectionGroupHeader">Client Information: #
                    <asp:TextBox ID="targetClientIdTextBox" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvRequiredId" runat="server" Text="*" ErrorMessage="*"
                    ValidationGroup="merge" ControlToValidate="targetClientIdTextBox" Display="Dynamic" SetFocusOnError="True">
                    </asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvSameId" runat="server" ControlToValidate="targetClientIdTextBox"
                    ValidationGroup="merge" ErrorMessage="!!" Text="same client" Display="Dynamic" Operator="NotEqual"
                    SetFocusOnError="True">
                    </asp:CompareValidator>
                    &nbsp;
                    <%-- onclient click is needed for IE6, since modal popup hides list controls and we have modal popup (update progress) over modal popup--%>
                    <asp:Button ID="loadTargetClientButton" runat="server" Text="Load Client" OnClick="loadTargetClientButton_Click"
                    ValidationGroup="merge" OnClientClick="if(Page_ClientValidate('merge')) { $find('beh').hide(); }" />
                    </h3>
                    <asp:Panel ID="targetPanel" runat="server" Visible="False">
                    <h4 class="sectionGroupHeader">Client Names</h4>
                    <%--<asp:CheckBoxList ID="cblNames1" runat="server"></asp:CheckBoxList>--%>
                    <asp:BulletedList ID="cblNames1" runat="server"></asp:BulletedList>
                    <h4 class="sectionGroupHeader">Client Addresses</h4>
                    <%--<asp:CheckBoxList ID="cblAddresses1" runat="server"></asp:CheckBoxList>--%>
                    <asp:BulletedList ID="cblAddresses1" runat="server"></asp:BulletedList>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        </ContentTemplate>
        </aje:UpdatePanel>                            
        <p style="text-align: center;">
            <asp:Button ID="OkButton" runat="server" Text="OK" CausesValidation="False" OnClick="Merge"/>
            <asp:Button ID="CancelButton" runat="server" Text="Cancel" CausesValidation="False" OnClientClick="$find('beh').hide();" OnClick="MergeCancel" />
        </p>
        </div>
    </asp:Panel>
        </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
    
    </ContentTemplate>
</aje:UpdatePanel>
</asp:Content>
