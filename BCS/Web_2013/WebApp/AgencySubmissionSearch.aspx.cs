using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Templates = BCS.WebApp.Templates;
using BCS.WebApp.UserControls;
using BCS.WebApp.Components;
using BCS.Biz;
using AjaxControlToolkit;
using System.Drawing;
//using BCS.DAL;
using System.Collections.Generic;
//using BOA.Util;

namespace BCS.WebApp
{
    public partial class AgencySubmissionSearch : System.Web.UI.Page
    {
        private const string key4Searched = "Searched";
        private const string key4PreviousSortExpression = "PreviousSortExpression";
        private const string key4PreviousSortDirection = "PreviousSortDirection";
        private const string key4DefaultSort = "DefaultSort";
        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            if (Session["CompanyChanged"] != null)
            {
                Session.Contents.Remove("CompanyChanged");
                Response.Redirect("AgencySubmissionSearch.aspx");
            }
        }
        #endregion

        #region Control Events
        protected void Filter_Click(object sender, EventArgs e)
        {
            gridAgencies.SelectedIndex = -1;
            gridAgencies.DataBind();
            gvSubmissions.DataBind();
            btnClearFilter_Click(btnClearFilter, EventArgs.Empty);
        }
        protected void gridAgencies_PageIndexChanged(object sender, EventArgs e)
        {
            gridAgencies.SelectedIndex = -1;
            gvSubmissions.DataBind();
        }

        protected void gvSubmissions_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gv = sender as GridView;
            Session["SubmissionId"] = gv.SelectedDataKey.Value;
            Session["SubmissionId"] = gv.SelectedDataKey.Values["id"];
            Session["ClientId"] = gv.SelectedDataKey.Values["ClientCore_Client_Id"];
            Session["subref"] = "AgencySubmissionSearch.aspx";
            if (DefaultValues.UseWizard)
                Response.Redirect("SubmissionWizard.aspx");
            Response.Redirect("Submission.aspx?op=edit");
        }
        protected void gvSubmissions_Init(object sender, EventArgs e)
        {
            #region required fields independent of company

            // col id, required
            BoundField bf = new BoundField();
            bf.HeaderText = "Id";
            bf.DataField = "Id";
            bf.Visible = false;
            gvSubmissions.Columns.Insert(0, bf);

            TemplateField tf = new TemplateField();

            // col submission no
            // col selection, required to be in first column (index 0), for "Remote Agency" Role handling
            ButtonField snf = new ButtonField();
            snf.ButtonType = ButtonType.Link;
            snf.DataTextField = "submission_number";
            snf.HeaderText = "Submission Number";
            snf.CommandName = "Select";
            // changed to ButtonType.Link, adding style attrs to make it prominent (grid links are set as black somewhere, need to track them down)
            // the followinig color is from pats_style stylesheet
            snf.ItemStyle.ForeColor = Color.FromArgb(00, 40, Convert.ToInt32("BF", 16));
            if (gvSubmissions.AllowSorting)
                snf.SortExpression = snf.DataTextField;
            gvSubmissions.Columns.Insert(0, snf);

            // col submission no sequence
            BoundField snsf = new BoundField();
            snsf.HeaderText = "Sequence";
            snsf.DataField = "submission_number_sequence";
            snsf.HtmlEncode = false;
            if (gvSubmissions.AllowSorting)
                snsf.SortExpression = snsf.DataField;
            gvSubmissions.Columns.Insert(1, snsf);

            // col client address, required for making associated submissions visually distinct
            tf = new TemplateField();
            tf.ItemTemplate = new Templates.LabelTemplate(DataControlRowType.DataRow, "address", "clientcore_address_id", "lblClientAddress");
            tf.Visible = false;
            gvSubmissions.Columns.Add(tf);


            #endregion

            // col client core client id, needed on this page
            bf = new BoundField();
            bf.HeaderText = "Client Id";
            bf.DataField = "clientcore_client_id";
            bf.SortExpression = bf.DataField;
            bf.Visible = true;
            gvSubmissions.Columns.Insert(2, bf);

            // col insured name, needed on this page
            bf = new BoundField();
            bf.HeaderText = "InsuredName";
            bf.DataField = "insured_name";
            bf.SortExpression = bf.DataField;
            bf.Visible = true;
            gvSubmissions.Columns.Insert(3, bf);

            // col dba, needed on this page
            bf = new BoundField();
            bf.HeaderText = "DBA";
            bf.DataField = "dba";
            bf.SortExpression = bf.DataField;
            bf.Visible = true;
            gvSubmissions.Columns.Insert(4, bf);

            Biz.SubmissionDisplayGridCollection dfields = ConfigValues.GetSubmissionGridDisplay(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));


            #region Some Customization on required fields #3709
            // see if any customization is there on the submission_number column
            Biz.SubmissionDisplayGrid customField = dfields.FindByFieldName("submission_number");
            if (customField != null)
            {
                snf.HeaderText = string.IsNullOrEmpty(customField.HeaderText) ? customField.FieldName : customField.HeaderText;
                dfields.Remove(customField);
            }
            // see if any customization is there on the submission_number_sequence column
            customField = dfields.FindByFieldName("submission_number_sequence");
            if (customField != null)
            {
                snsf.HeaderText = string.IsNullOrEmpty(customField.HeaderText) ? customField.FieldName : customField.HeaderText;
                dfields.Remove(customField);
            }
            #endregion

            foreach (Biz.SubmissionDisplayGrid cfield in dfields)
            {
                TemplateField atf = new TemplateField();
                atf.ItemTemplate = new Templates.GenericTemplate(DataControlRowType.DataRow, string.Empty, cfield.FieldName, cfield.AttributeDataType.DataTypeName, string.Empty);
                //atf.HeaderTemplate = new Templates.GenericTemplate(DataControlRowType.Header, string.IsNullOrEmpty(cfield.HeaderText) ? cfield.FieldName : cfield.HeaderText, string.Empty, string.Empty);
                atf.HeaderText = string.IsNullOrEmpty(cfield.HeaderText) ? cfield.FieldName : cfield.HeaderText;

                if (gvSubmissions.AllowSorting)
                    atf.SortExpression = cfield.FieldName;
                gvSubmissions.Columns.Insert(gvSubmissions.Columns.Count - 2, atf);
            }
        }
        protected void gvSubmissions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string sort = string.Empty;
            if (ViewState[key4PreviousSortExpression] != null && ViewState[key4PreviousSortDirection] != null)
            {
                sort = string.Format("{0} {1}", ViewState[key4PreviousSortExpression], ViewState[key4PreviousSortDirection]);
            }
            if (string.IsNullOrEmpty(sort))
            {
                sort = (string)ViewState[key4DefaultSort];
            }
            LoadSubmissions(e.NewPageIndex, sort);
        }

        protected void gridAgencies_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["ApplyDateFilter"] = false;

            ViewState[key4Searched] = null;
            string sort = string.Empty;
            if (ViewState[key4PreviousSortExpression] != null && ViewState[key4PreviousSortDirection] != null)
            {
                sort = string.Format("{0} {1}", ViewState[key4PreviousSortExpression], ViewState[key4PreviousSortDirection]);
            }
            if (string.IsNullOrEmpty(sort))
            {
                sort = (string)ViewState[key4DefaultSort];
            }
            LoadSubmissions(0, sort);
        }
        protected void gvSubmissions_Deleting(object sender, GridViewDeleteEventArgs e)
        {
            BCS.Biz.DataManager dm = new DataManager(DefaultValues.DSN);
            DataSet ds = BCS.Biz.StoredProcedures.DeleteSubmissionById(Convert.ToInt32(gvSubmissions.DataKeys[e.RowIndex]["id"]));
            // TODO: interpret result from stored proc
            Common.SetStatus(messagePlaceHolder, "Submission successfully deleted.");
            Cache.Remove("SearchedCacheDependency");
            ViewState[key4Searched] = null;
            string sort = string.Empty;
            if (ViewState[key4PreviousSortExpression] != null && ViewState[key4PreviousSortDirection] != null)
            {
                sort = string.Format("{0} {1}", ViewState[key4PreviousSortExpression], ViewState[key4PreviousSortDirection]);
            }
            if (string.IsNullOrEmpty(sort))
            {
                sort = (string)ViewState[key4DefaultSort];
            }
            LoadSubmissions(gvSubmissions.PageIndex, sort);
        }
        protected void gvSubmissions_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortDirection = " ASC";
            if ((string)ViewState[key4PreviousSortExpression] == e.SortExpression)
            {
                if ((string)ViewState[key4PreviousSortDirection] == sortDirection)
                {
                    sortDirection = " DESC";
                }
            }
            ViewState[key4PreviousSortDirection] = sortDirection;
            ViewState[key4PreviousSortExpression] = e.SortExpression;

            string sort = e.SortExpression.ToString() + sortDirection;

            LoadSubmissions(gvSubmissions.PageIndex, sort);

        }
        protected void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
        {
            throw new ApplicationException("Ajax Error", e.Exception);
        }
        protected void btnDateFilter_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
                return;
            ViewState["ApplyDateFilter"] = true;
            ViewState[key4Searched] = null;
            string sort = string.Empty;
            if (ViewState[key4PreviousSortExpression] != null && ViewState[key4PreviousSortDirection] != null)
            {
                sort = string.Format("{0} {1}", ViewState[key4PreviousSortExpression], ViewState[key4PreviousSortDirection]);
            }
            if (string.IsNullOrEmpty(sort))
            {
                sort = (string)ViewState[key4DefaultSort];
            }
            LoadSubmissions(0, sort);
        }



        protected void DateBox_Init(object sender, EventArgs e)
        {
            (sender as TextBox).Attributes.Add("onclick", "scwShow(this,this);");
            (sender as TextBox).Attributes.Add("onkeydown", "hideCalOnTab(event);");
            (sender as TextBox).Attributes.Add("onblur", "this.value = purgeDate(this);");
        }
        protected void btnClearFilter_Click(object sender, EventArgs e)
        {
            txStartDate.Text = string.Empty;
            txEndDate.Text = string.Empty;
            ViewState["ApplyDateFilter"] = false;
            ViewState[key4Searched] = null;
            string sort = key4DefaultSort;
            if (ViewState[key4PreviousSortExpression] != null && ViewState[key4PreviousSortDirection] != null)
            {
                sort = string.Format("{0} {1}", ViewState[key4PreviousSortExpression], ViewState[key4PreviousSortDirection]);
            }
            LoadSubmissions(0, sort);
        }
        protected void gvSubmissions_DataBound(object sender, EventArgs e)
        {
            tblFilter.Visible = gridAgencies.SelectedIndex != -1;
            gvSubmissions.Visible = gridAgencies.SelectedIndex != -1;
        }
        #endregion

        #region Other Methods

        private DateTime GetSafeDateTime(string p)
        {
            return Common.GetSafeDateTime(p, System.Data.SqlTypes.SqlDateTime.MinValue.Value);
        }

        private void LoadSubmissions(int pageIndex, string sort)
        {
            gvSubmissions.PageIndex = pageIndex;
            if (ViewState[key4Searched] != null)
            {
                DataTable savedDt = ViewState[key4Searched] as DataTable;

                savedDt.DefaultView.Sort = sort;

                gvSubmissions.DataSource = savedDt;
                gvSubmissions.DataBind();
                return;
            }

            string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);

            object selAgency = gridAgencies.SelectedIndex < 0 ? 0 : gridAgencies.SelectedDataKey.Values["Id"];

            Triplet Criteria = new Triplet(JoinPath.Submission.Columns.AgencyId, selAgency, OrmLib.MatchType.Exact);
            Triplet CriteriaStDate = new Triplet(JoinPath.Submission.Columns.SubmissionDt, GetSafeDateTime(txStartDate.Text), OrmLib.MatchType.Greater);
            Triplet CriteriaEndDate = new Triplet(JoinPath.Submission.Columns.SubmissionDt, GetSafeDateTime(txEndDate.Text), OrmLib.MatchType.Lesser);

            //List<BCS.DAL.EFilter> filters = new List<BCS.DAL.EFilter>();
            //filters.Add(new BCS.DAL.EFilter("AgencyId", (int)selAgency));
            //if (ApplyDateFilter())
            //{
            //    filters.Add(new BCS.DAL.EFilter("SubmissionDt", GetSafeDateTime(txStartDate.Text), BCS.DAL.MatchType.GreaterThan));
            //    filters.Add(new BCS.DAL.EFilter("SubmissionDt", GetSafeDateTime(txEndDate.Text), BCS.DAL.MatchType.LessThan));
            //}

            Triplet[] criterion = new Triplet[] { Criteria };

            if (ApplyDateFilter())
            {
                criterion = new Triplet[] { Criteria, CriteriaStDate, CriteriaEndDate };
            }

            //DateTime start = DateTime.Now;
            string xml = BCS.Core.Clearance.Submissions.GetSubmissions(companyNumber, false, criterion);
            //string res = (DateTime.Now - start).TotalMilliseconds.ToString();
            //start = DateTime.Now;

            //string xml = BCS.Core.Clearance.Submissions.GetSubmissions(companyNumber,false, filters.ToArray());
            //string res2 = (DateTime.Now - start).TotalMilliseconds.ToString();
            DataTable dt = Common.GenerateDataTable(companyNumber, "submission", xml);
            if (dt != null)
            {
                DataTable ndt = dt.Clone();
                ndt.Columns["submission_number"].DataType = typeof(long);
                ndt.Columns["submission_number_sequence"].DataType = typeof(int);
                foreach (DataRow dr in dt.Rows)
                    ndt.ImportRow(dr);
                ndt.AcceptChanges();

                if (!string.IsNullOrEmpty(sort))
                {
                    ndt.DefaultView.Sort = sort;
                }
                else
                {
                    ndt.DefaultView.Sort = dt.DefaultView.Sort;
                    ViewState[key4DefaultSort] = dt.DefaultView.Sort;
                }

                gvSubmissions.DataSource = ndt.DefaultView;
                gvSubmissions.DataBind();

                // has size in 10s of MB for agencies having large number of submissions, just uncomment the line below if we do want to go that route
                //ViewState[Searched] = ndt;
            }
            else
            {
                gvSubmissions.DataSource = dt;
                gvSubmissions.DataBind();
            }
        }

        private bool ApplyDateFilter()
        {
            return (bool?)ViewState["ApplyDateFilter"] ?? false;
        }
        #endregion


        protected void Panel1_Load(object sender, EventArgs e)
        {
            string sid = gvSubmissions.DataKeys[((sender as Control).NamingContainer as GridViewRow).RowIndex].Values["id"].ToString();
            ObjectDataSource1.SelectParameters["submissionId"].DefaultValue = sid;

            lblSubmissionDetails.Text = string.Format("Submission # : {0}-{1}", gvSubmissions.DataKeys[((sender as Control).NamingContainer as GridViewRow).RowIndex].Values["submission_number"],
                gvSubmissions.DataKeys[((sender as Control).NamingContainer as GridViewRow).RowIndex].Values["submission_number_sequence"]);
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView historyGrid = sender as GridView;
            if (historyGrid.SelectedIndex == -1)
                return;
            string result = BCS.Core.Clearance.Submissions.ToggleSubmissionStatusHistoryActive(Convert.ToInt32(historyGrid.SelectedDataKey["Id"]), Common.GetUser());
            Common.SetStatus(HistoryStatusPH, result, true);
            historyGrid.SelectedIndex = -1;
            historyGrid.DataBind();

            string sort = string.Empty;
            if (ViewState[key4PreviousSortExpression] != null && ViewState[key4PreviousSortDirection] != null)
            {
                sort = string.Format("{0} {1}", ViewState[key4PreviousSortExpression], ViewState[key4PreviousSortDirection]);
            }
            if (string.IsNullOrEmpty(sort))
            {
                sort = (string)ViewState[key4DefaultSort];
            }
            LoadSubmissions(gvSubmissions.PageIndex, sort);
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                SubmissionStatusHistory aHistory = e.Row.DataItem as SubmissionStatusHistory;
                if (!aHistory.Active)
                {
                    foreach (TableCell var in e.Row.Cells)
                    {
                        var.Style.Add(HtmlTextWriterStyle.TextDecoration, "line-through");
                    }
                }

                LinkButton lb = e.Row.FindControl("LinkButton1") as LinkButton;
                ConfirmButtonExtender confirmButtonExtender = e.Row.FindControl("ConfirmButtonExtender1") as ConfirmButtonExtender;
                if (lb != null)
                {
                    lb.Text = aHistory.Active ? "active" : "inactive";
                    lb.Enabled = BCS.WebApp.Components.Security.CanActivateStatus() && (aHistory.Active.Value || User.IsInRole("System Administrator"));
                }
                if (confirmButtonExtender != null)
                {
                    confirmButtonExtender.Enabled = BCS.WebApp.Components.Security.CanActivateStatus() && (aHistory.Active.Value || User.IsInRole("System Administrator"));
                    if (User.IsInRole("System Administrator"))
                    {
                        confirmButtonExtender.ConfirmText = "Are you sure you want to change this status history?";
                    }
                }
            }
        }

        protected void HyperLink1_Init(object sender, EventArgs e)
        {
            HyperLink1.Visible = (User as BCS.Core.Security.CustomPrincipal).IsInAnyRoles("User", "Partial User", "Auditor", "System Administrator");
        }
    }
}