<%@ Page language="c#" MasterPageFile="~/Site.PATS.master" Title="Berkley Clearance System : Client" Inherits="BCS.WebApp.FAQ" CodeFile="FAQ.aspx.cs" Theme="DefaultTheme" ValidateRequest="False" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1"  Runat="Server">
    <script src="Javascripts/jquery-1.4.4.js" type="text/javascript"></script>
    <script type="text/javascript">
        function ToggleDisplay(Id) {
            //alert(Id);
            var ele = "#" + Id;

            if ($(ele).css("display") && $(ele).css("display") != "block") {
                $(".answer").slideUp();
                $(ele).slideDown();
            }
            else {
                $(ele).slideUp();
            }
            return false;
        }

    </script>

    <table cellpadding="0" cellspacing="0" class="box" style="margin-top:20px;">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">FAQ</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC" style="width: 100%">
        <%--content start--%>
                <asp:ListView runat="server" ID="lvFAQ" OnItemDataBound="lvFAQ_OnDataBound">
        <LayoutTemplate>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr runat="server" id="itemPlaceHolder"></tr>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr runat="server">
                <td style="padding-bottom:10px;">
                    <div style="margin-bottom:10px;">
                        <span style="font-weight:bold;color:#444444;padding-right:5px;">Question:</span>&nbsp;<asp:LinkButton runat="server" ID="lnkShowAnswer">                        
                        
                            <span style="font-size:12px;"><asp:Literal runat="server" ID="ltQuestion" Text='<%#Bind("Question") %>'></asp:Literal></span>
                        
                        </asp:LinkButton>

                    </div>
                    <asp:Panel runat="server" ID='pnlAnswer' ClientIDMode="Static" CssClass='hide answer'>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top">
                                    <span style="color:#0066cc;font-size:16px;font-weight:normal;padding-bottom:5px;padding-right:5px;">Answer:</span>
                                </td>
                                <td valign="top" style="padding-top:4px;">
                                    <span style="font-size:12px;color:#000000;"><asp:Literal ID="ltAnswer" runat="server" Text='<%#Bind("Answer") %>'></asp:Literal></span>
                                    <asp:Panel runat="server" ID="pnlUsersWithAccess" Visible="false">
                                        <div style="color:#0066cc;margin-top:5px;">Users with access to perform this update:</div>
                                        <table runat="server" width="100%" cellpadding="3" cellspacing="0" border="0" ID="tblUsers"></table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>

                        </asp:Panel>
                    </div>
                </td>
            </tr>
        </ItemTemplate>
    </asp:ListView>
        <%--content end--%>
            </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
    

     
</asp:Content>
