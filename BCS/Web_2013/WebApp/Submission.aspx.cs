#region Using
using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Policy;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;
using Microsoft.Web.Services2.Security.X509;

using OrmLib;
using BCS.Biz;
using BCS.WebApp.Components;
using BCS.WebApp.UserControls;

using structures = BTS.ClientCore.Wrapper.Structures;
using System.Collections.Generic;
using AjaxControlToolkit;
using System.Reflection;
using BTS.LogFramework;
#endregion


public partial class Submission : System.Web.UI.Page
{
    private string _operation = "";
    private string disableScript = "if(Page_ClientValidate()){1}document.getElementById('{0}').style.display='none'; document.getElementById('{3}').disabled=true; var elem = document.createElement('input'); elem.type = 'button'; elem.disabled = true; elem.value= $get('{0}').value; $get('{0}').parentNode.insertBefore(elem,$get('{0}'));{2}";

    public bool WarningAdded
    {
        get { return (bool?)ViewState["WarningAdded"] ?? false; }
        set { ViewState["WarningAdded"] = value; }
    }

    #region Messages
    string obsoleteUnderwriter = "The Underwriter {0} previously associated with this submission, is no longer valid. Please select a new one prior to saving the submission, or contact your system administrator for further information.";
    string obsoleteAnalyst = "The Analyst {0} previously associated with this submission, is no longer valid. Please select a new one prior to saving the submission, or contact your system administrator for further information.";
    #endregion

    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
        // Hide all the panels to start:        
        this.pnlAddSubmission.Visible = false;

        copy.Attributes.Add("onclick", string.Format("javascript:copyErrorToClipboard(document.getElementById('{0}').innerHTML);", txSubmissionNumber.ClientID));
        copyComments.Attributes.Add("onclick", string.Format("javascript:copyErrorToClipboard(document.getElementById('{0}').innerHTML);", txComments.ClientID));

        txEffectiveDate.Attributes.Add("onclick", string.Format("scwShow(this,this); processAfter=new Function(\"__doPostBack(\'ctl00$ContentPlaceHolder1$txEffectiveDate\',\'\'); \")", txEffectiveDate.ClientID, txExpirationDate.ClientID));
        txEffectiveDate.Attributes.Add("onkeydown", "hideCalOnTab(event);");
        txEffectiveDate.Attributes.Add("onblur", "this.value = purgeDate(this);");
                
        txExpirationDate.Attributes.Add("onclick", "scwShow(this,this);");
        txExpirationDate.Attributes.Add("onkeydown", "hideCalOnTab(event);");
        txExpirationDate.Attributes.Add("onblur", "this.value = purgeDate(this);");

        txSubmissionStatusDate.Attributes.Add("onclick", "scwShow(this,this);");
        txSubmissionStatusDate.Attributes.Add("onkeydown", "hideCalOnTab(event);");
        txSubmissionStatusDate.Attributes.Add("onblur", "this.value = purgeDate(this);");

        txEstimatedPremium.Attributes.Add("onblur", "this.value=formatCurrency(this.value);");
        txOtherCarrierPremium.Attributes.Add("onblur", "this.value=formatCurrency(this.value);");

        if (!IsPostBack)
        {
            btnSubmitSubmission.Attributes.Add("onclick",
                     string.Format(disableScript, btnSubmitSubmission.ClientID, "{", "}", Button1.ClientID));            
        }
        ScriptManager1.RegisterPostBackControl(btnSubmitSubmission);
        int cnId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        if (!IsPostBack)
        {
            Common.SetStatus(messagePlaceHolder);

            OtherCarrierDropDownList1.CompanyNumberId = cnId;
            OtherCarrierDropDownList1.DataBind();
            cblAgentUnspecifiedReasons.CompanyNumberId = cnId;
            cblAgentUnspecifiedReasons.DataBind();

            SubmissionStatusDropDownList1.CompanyNumberId = cnId;
            SubmissionStatusDropDownList1.DataBind();
            SubmissionStatusReasonDropDownList1.StatusId = Common.GetSafeSelectedValue(SubmissionStatusDropDownList1);
            SubmissionStatusReasonDropDownList1.DataBind();
            SubmissionTypeDropDownList1.CompanyNumberId = cnId;
            SubmissionTypeDropDownList1.DataBind();
            //SubmissionTypeDropDownList1.DataBind();
            AgencyDropDownList1.CompanyNumberId = cnId;
            AgencyDropDownList1.DataBind();
            AgentDropDownList1.DataBind();
            ContactDropDownList1.DataBind();

            PolicySystemDropDownList1.CompanyNumberId = cnId;
            PolicySystemDropDownList1.DataBind();

            txPolicyNumber.CompanyNumberId = cnId;

            UnderwritersDropDownList1.CompanyNumberId = cnId;
            AnalystDropDownList1.CompanyNumberId = cnId;

            LineOfBusinessDropDownList1.DataBindByCompanyNumber(cnId);
        }

        LoadSubmission(false);

        ibSearchAgencies.Attributes.Add("onclick",
            string.Format("openAgencySearch('{0}',{1},'{2}');return false",
           AgencyDropDownList1.ClientID, cnId, GetEffectiveDate(DateTime.Now)));
        
        if (!IsPostBack)
        {
            SetInitialFocus();
        }
    }
    protected override void OnInit(EventArgs e)
    {
        Form.SubmitDisabledControls = true;
        hdnUser.Value = User.Identity.Name;
        if (Request.QueryString.Count > 0)
        {
            if (Request.QueryString["op"] != null)
            {
                this._operation = Request.QueryString["op"].ToString();
                lblOperation.Text =
                        System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this._operation);
            }
            else
            {
                throw new ApplicationException("Missing operation.");
            }
        }
        else
        {
            throw new ApplicationException("The page cannot be directly accessed.");
        }


        SetOperation(this._operation);

        if (Session["PossibleDuplicates"] != null)
        {
            if (Session["PossibleDuplicates"].ToString().Length > 0)
            {
                string s = "The following submissions were flagged as potential duplicates: {0}";
                StringBuilder sb = new StringBuilder(Session["PossibleDuplicates"].ToString());
                sb.Remove(sb.Length - 1, 1);
                if (sb.ToString().Contains(","))
                {
                    sb.Replace(";", " and ", sb.ToString().LastIndexOf(",") - 1, 2);
                    sb.Replace(";", "; ");
                }
                txComments.Text = string.Format(s, sb);
            }
            Session.Contents.Remove("PossibleDuplicates");
        }

        #region Code to dynamically generate and populate the Code Types DDL and Attributes


        int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        LoadCodeTypes(companyNumberId);
        LoadAttributes(companyNumberId);
        #endregion

        Common.SetTabOrder(TabIndex_Section_10, 10);
        Common.SetTabOrder(TabIndex_Section_20, 20);
        Common.SetTabOrder(TabIndex_Section_30, 30);
        Common.SetTabOrder(TabIndex_Section_40, 40);
        Common.SetTabOrder(TabIndex_Section_50, 50);
        Common.SetTabOrder(TabIndex_Section_60, 60);
        Common.SetTabOrder(TabIndex_Section_70, 70);
        Common.SetTabOrder(TabIndex_Section_80, 80);
        Common.SetTabOrder(TabIndex_Section_90, 90);

        base.OnInit(e);
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        if (Session["CompanyChanged"] != null)
        {
            Session.Contents.Remove("CompanyChanged");
            Server.Transfer("Default.aspx");
        }
        base.OnLoadComplete(e);
    }
    #endregion

    #region Control Events

    protected void AgencyDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (AgencyDropDownList1.SelectedIndex > -1)
        {
            BCS.Biz.Agency ag = GetSelectedAgency(AgencyDropDownList1);
            lblAgencyComment.Text = ag != null ? ag.Comments : string.Empty;
            lblAgencyComment.Visible = !string.IsNullOrEmpty(lblAgencyComment.Text);

            int selLob = Common.GetSafeSelectedValue(LineOfBusinessDropDownList1);
            LineOfBusinessDropDownList1.DataBindByAgency(Convert.ToInt32(AgencyDropDownList1.SelectedValue));
            if (selLob != 0)
            {
                Common.SetSafeSelectedValue(LineOfBusinessDropDownList1, selLob);
                LineOfBusinessDropDownList1_SelectedIndexChanged(LineOfBusinessDropDownList1, EventArgs.Empty);
            }

            int selAgent = Common.GetSafeSelectedValue(AgentDropDownList1);
            AgentDropDownList1.AgencyId = Convert.ToInt32(AgencyDropDownList1.SelectedValue);
            AgentDropDownList1.CancelDate = GetEffectiveDate(DateTime.MaxValue.Subtract(TimeSpan.FromSeconds(1)));
            AgentDropDownList1.DataBind();
            if (selAgent != 0)
                Common.SetSafeSelectedValue(AgentDropDownList1, selAgent);

            int selContact = Common.GetSafeSelectedValue(ContactDropDownList1);
            ContactDropDownList1.AgencyId = Convert.ToInt32(AgencyDropDownList1.SelectedValue);
            ContactDropDownList1.DataBind();
            if (selContact != 0)
                Common.SetSafeSelectedValue(ContactDropDownList1, selContact);

            int selUnd = Common.GetSafeSelectedValue(UnderwritersDropDownList1);
            UnderwritersDropDownList1.DataBind(Convert.ToInt32(AgencyDropDownList1.SelectedValue),
                Array.ConvertAll(Common.GetSelectedValues(LineOfBusinessDropDownList1), new Converter<string, int>(Convert.ToInt32)));
            if (selUnd != 0)
                Common.SetSafeSelectedValue(UnderwritersDropDownList1, selUnd);

            int selAna = Common.GetSafeSelectedValue(AnalystDropDownList1);
            AnalystDropDownList1.DataBind(Convert.ToInt32(AgencyDropDownList1.SelectedValue),
                Array.ConvertAll(Common.GetSelectedValues(LineOfBusinessDropDownList1), new Converter<string, int>(Convert.ToInt32)));
            if (selAna != 0)
                Common.SetSafeSelectedValue(AnalystDropDownList1, selAna);

            DateTime effDate = GetEffectiveDate(DateTime.MaxValue.Subtract(TimeSpan.FromSeconds(1)));
            DateTime refDate = ag != null && !ag.ReferralDate.IsNull && ag.ReferralDate.Value != DateTime.MinValue ? ag.ReferralDate.Value : DateTime.MinValue.AddDays(1);
            if (ag != null && !string.IsNullOrEmpty(ag.ReferralAgencyNumber) && refDate <= effDate)
            {
                AgencyDropDownList1.ClearSelection();
                int referredAgencyId = GetReferredAgencyId(ag);
                if (null != AgencyDropDownList1.Items.FindByValue(referredAgencyId.ToString()))
                {
                    AgencyDropDownList1.Items.FindByValue(referredAgencyId.ToString()).Selected = true;
                    AgencyDropDownList1_SelectedIndexChanged(sender, e);
                    if (IsPostBack)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(string), "referredAgency",
                                   "alert('Displaying referral agency');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(string), "missingReferredAgency",
                              string.Format(
                              "alert('This agency is referred to agency ({0}), which is no longer active.  Referral agency can not be displayed.');",
                              ag.ReferralAgencyNumber), true);
                }
            }
            if (IsPostBack)
                ScriptManager1.SetFocus(AgencyDropDownList1);
        }
        WarnIfUnlicensedAgency();
        //ListSearchExtender2.ClientState = "Focused";
    }

    private BCS.Biz.Agency GetSelectedAgency(AgencyDropDownList agddl)
    {
        int selectedAgency = Common.GetSafeSelectedValue(agddl);
        DataSet dataSet = agddl.GetCachedSource();
        DataManager dm = new DataManager(DefaultValues.DSN);
        dm.DataSet = dataSet;
        Agency retAgency = dm.GetAgencyFromDataSet(selectedAgency);
        return retAgency;
    }
    private int GetReferredAgencyId(Agency agency)
    {
        DataSet dataSet = AgencyDropDownList1.GetCachedSource();
        // there should be only one row after the select, since there is a unique index on company number, agency number
        DataRow[] rows = dataSet.Tables["Agency"].Select("AgencyNumber = '" + agency.ReferralAgencyNumber + "'");
        if (rows.Length != 1)
            return Int32.MinValue;
        return Convert.ToInt32(rows[0]["Id"]);
    }

    protected void LineOfBusinessDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        int selUnd = Common.GetSafeSelectedValue(UnderwritersDropDownList1);
        int selAna = Common.GetSafeSelectedValue(AnalystDropDownList1);
        if (AgencyDropDownList1.SelectedIndex > -1 && LineOfBusinessDropDownList1.SelectedIndex > -1)
        {
            UnderwritersDropDownList1.DataBind(Convert.ToInt32(AgencyDropDownList1.SelectedValue),
                Array.ConvertAll(Common.GetSelectedValues(LineOfBusinessDropDownList1), new Converter<string, int>(Convert.ToInt32)));

            AnalystDropDownList1.DataBind(Convert.ToInt32(AgencyDropDownList1.SelectedValue),
                Array.ConvertAll(Common.GetSelectedValues(LineOfBusinessDropDownList1), new Converter<string, int>(Convert.ToInt32)));
        }
        else
        {
            UnderwritersDropDownList1.DataBind();
            AnalystDropDownList1.DataBind();
        }
        if (selUnd != 0)
        {
            if (!DefaultValues.FillAllUnderwriters(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                Common.SetSafeSelectedValue(UnderwritersDropDownList1, selUnd);
        }
        if (selAna != 0)
            Common.SetSafeSelectedValue(AnalystDropDownList1, selAna);

        if (IsPostBack)
            ScriptManager1.SetFocus(LineOfBusinessDropDownList1);
    }
    protected void SubmissionStatusDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SubmissionStatusReasonDropDownList1.StatusId = Convert.ToInt32(SubmissionStatusDropDownList1.SelectedValue);
        SubmissionStatusReasonDropDownList1.DataBind();

        txSubmissionStatusDate.Text = DateTime.Today.ToString("MM/dd/yyyy");

        ScriptManager1.SetFocus(SubmissionStatusDropDownList1);
    }
    protected void btnSubmitSubmission_Command(object sender, CommandEventArgs e)
    {
        Button b = sender as Button;
        if (b.Text == "Cancel")
        {
            Session["LoadSavedSearch"] = true;
            if (Session["subref"] != null)
            {
                string ss = Session["subref"].ToString();
                Session.Contents.Remove("subref");
                Response.Redirect(ss);
            }
            Response.Redirect("Default.aspx");
        }
        if (!Page.IsValid)
        {
            ScriptManager.RegisterStartupScript(this, typeof(string), "validationFailed",
                "alert('Unable to save submission. Please Enter/Correct the fields with * next to them.');", true);
            return;
        }

        bool supportsMultipleLOB = DefaultValues.SupportsMultipleLineOfBusinesses(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        if (supportsMultipleLOB)
            if (!LOBList1.IsValid())
                return;

        //CompanyDropDownList ct = Master.FindControl("CompanySelector1").FindControl("CompanyDropDownList1") as CompanyDropDownList;
        //CompanyNumberDropDownList cnt = Master.FindControl("CompanySelector1").FindControl("CompanyNumberDropDownList1") as CompanyNumberDropDownList;
        int companyid = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
        //if (cnt.SelectedItem == null || cnt.SelectedItem.Text.Length == 0)
        //{
        //    Common.SetStatus(messagePlaceHolder, "Please select a company number.");
        //    return;
        //}
        string companynumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
        int companynumberid = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);

        BCS.Core.Clearance.BizObjects.Submission coresub;

        SubmissionProxy.Submission sp = Common.GetSubmissionProxy();

        string clientid = Session["ClientId"] as string;
        string clientaddressids = GetSafeSelectedClientAddressIds();
        string clientnameids = GetSafeSelectedClientNameIds();
        string clientportfolioid = lblPortfolioId.Text.Length == 0 ? "0" : lblPortfolioId.Text;
        string user = Common.GetUser();
        int subtypeid = Convert.ToInt32(SubmissionTypeDropDownList1.SelectedValue);
        string statusdate = txSubmissionStatusDate.Text;
        int substatusid = Convert.ToInt32(SubmissionStatusDropDownList1.SelectedValue);
        int substatusreasonid = Convert.ToInt32(SubmissionStatusReasonDropDownList1.SelectedValue);
        if (supportsMultipleLOB)
        {
            BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild[] children = LOBList1.GetItems(true);
            List<int> statuses = new List<int>();
            for (int i = 0; i < children.Length; i++)
            {
                if (children[i].SubmissionStatusId != 0)
                    statuses.Add(children[i].SubmissionStatusId);
            }
            // process further if any lob item was assigned a status, else submission status will be what was selected in the status dropdown
            if (statuses.Count != 0)
            {
                BCS.Biz.DataManager dm = new DataManager(DefaultValues.DSN);
                dm.QueryCriteria.And(JoinPath.CompanyNumberSubmissionStatus.Columns.CompanyNumberId, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                CompanyNumberSubmissionStatusCollection dbStatuses = dm.GetCompanyNumberSubmissionStatusCollection();
                dbStatuses = dbStatuses.FilterByHierarchicalOrder(0, CompareType.Not);
                dbStatuses = dbStatuses.SortByHierarchicalOrder(OrmLib.SortDirection.Ascending);

                if (dbStatuses.Count > 0)
                {
                    List<int> hStatuses = new List<int>();

                    for (int i = 0; i < dbStatuses.Count; i++)
                    {
                        hStatuses.Add(dbStatuses[i].SubmissionStatusId);
                    }
                    int idToAssign = statuses[0];

                    foreach (int i in statuses)
                    {
                        int lowerIndex = Math.Min(hStatuses.IndexOf(idToAssign), hStatuses.IndexOf(i));
                        idToAssign = hStatuses[lowerIndex];
                    }
                    substatusid = idToAssign;
                    substatusreasonid = children[statuses.IndexOf(idToAssign)].SubmissionStatusReasonId;
                }
            }
        }
        int agencyid = Convert.ToInt32(AgencyDropDownList1.SelectedValue);
        int agentid = Convert.ToInt32(AgentDropDownList1.SelectedValue);
        int contactid = Convert.ToInt32(ContactDropDownList1.SelectedValue);
        decimal dpremium = txEstimatedPremium.Text.Length > 0 ? Decimal.Parse(
            txEstimatedPremium.Text, System.Globalization.NumberStyles.Currency,
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat) : decimal.Zero;
        string dteff = string.Empty;
        string dtexp = string.Empty;
        if (!cbTBD.Checked)
        {
            dteff = txEffectiveDate.Text;
            dtexp = txExpirationDate.Text;
        }
        int underwriterid = Convert.ToInt32(UnderwritersDropDownList1.SelectedValue);
        int analystid = Convert.ToInt32(AnalystDropDownList1.SelectedValue);
        int technicianid = 0; // passing 0 not to get errors
        int policysystemid = Convert.ToInt32(PolicySystemDropDownList1.SelectedValue);
        string comments = txComments.Text;
        string policynumber = txPolicyNumber.Text;
        int lineofbusinessid = Convert.ToInt32(LineOfBusinessDropDownList1.SelectedValue);

        string lobchilren = supportsMultipleLOB ? LOBList1.GetItemsAsString(true) : string.Empty;
        string dba = txDba.Text;
        string otherreason = txSubmissionStatusReasonOther.Text;
        int othercarrierid = Common.GetSafeSelectedValue(OtherCarrierDropDownList1);

        decimal ocpremium = txOtherCarrierPremium.Text.Length > 0 ? Decimal.Parse(
            txOtherCarrierPremium.Text, System.Globalization.NumberStyles.Currency,
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat) : decimal.Zero;

        string[] agentunspecifiedreasons = Common.GetSelectedValues(cblAgentUnspecifiedReasons);

        ArrayList alids = new ArrayList();
        if (phCodeTypes.Controls.Count > 0)
        {
            if (phCodeTypes.Controls[0] is HtmlTable)
            {
                HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                foreach (HtmlTableRow row in table.Rows)
                {
                    foreach (HtmlTableCell cell in row.Cells)
                    {
                        foreach (Control c in cell.Controls)
                        {
                            if (c is DropDownList)
                            {
                                DropDownList ddl = c as DropDownList;
                                if (ddl.SelectedIndex > 0)
                                {
                                    alids.Add(ddl.SelectedValue);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (supportsMultipleLOB)
        {
            alids.AddRange(LOBList1.GetCodeTypeValues());
        }
        string[] codeids = new string[alids.Count];
        alids.CopyTo(codeids);

        alids = new ArrayList();
        if (phAttributes.Controls.Count > 0)
        {
            if (phAttributes.Controls[0] is HtmlTable)
            {
                HtmlTable table = phAttributes.Controls[0] as HtmlTable;
                foreach (HtmlTableRow row in table.Rows)
                {
                    foreach (HtmlTableCell cell in row.Cells)
                    {
                        foreach (Control c in cell.Controls)
                        {
                            if (c is WebControl)
                            {
                                if (c is IValidator)
                                    continue;
                                Type t = c.GetType();
                                // Gets the attributes for the collection.
                                System.ComponentModel.AttributeCollection attrs = System.ComponentModel.TypeDescriptor.GetAttributes(c);

                                /* Prints the name of the default property by retrieving the 
                                 * DefaultPropertyAttribute from the AttributeCollection. */
                                ControlValuePropertyAttribute myAttribute =
                                   (ControlValuePropertyAttribute)attrs[typeof(ControlValuePropertyAttribute)];

                                System.Reflection.PropertyInfo pi = t.GetProperty(myAttribute.Name);
                                object value = pi.GetValue(c, null);
                                alids.Add(string.Format("{0}={1}", c.ID, value));
                            }
                        }
                    }
                }
            }
        }
        string[] attributes = new string[alids.Count];
        alids.CopyTo(attributes);

        Button btn = sender as Button;
        if (btn.CommandName == "Edit")
        {
            string submissionid = Session["SubmissionId"].ToString();
            string insuredname = txInsuredName.Text;

            string edtXML = sp.ModifySubmissionByIds(submissionid, Convert.ToInt64(clientid),
                string.Empty, // stop gap parameter for other apps
                clientaddressids, clientnameids, Convert.ToInt64(clientportfolioid), companyid, subtypeid, agencyid, agentid,
                string.Join("|", agentunspecifiedreasons),
                contactid,
                companynumberid, substatusid, statusdate, substatusreasonid, otherreason, policynumber, string.Join("|", codeids), string.Join("|", attributes), dpremium, dteff, dtexp,
                underwriterid, analystid, technicianid, policysystemid, comments, user, lineofbusinessid, lobchilren, insuredname, dba,
                othercarrierid, ocpremium);

            try
            {
                coresub = (BCS.Core.Clearance.BizObjects.Submission)BCS.Core.XML.Deserializer.Deserialize(
                    edtXML, typeof(BCS.Core.Clearance.BizObjects.Submission));
                if (coresub != null)
                    if (coresub.Message != null)
                    {
                        lblObsoleteAnalyst.Text = string.Empty;
                        lblObsoleteUnderwriter.Text = string.Empty;
                        Session["SubmissionId"] = coresub.Id.ToString();
                        LoadSubmission(true);
                        Common.SetStatus(messagePlaceHolder, string.Format("{0}, Submission Number : {1}", coresub.Message, coresub.SubmissionNumber));

                        SetInitialFocus();
                    }
            }
            catch (Exception)
            {
                Common.SetStatus(messagePlaceHolder, "Submission Edit not successful. ");
            }
            Cache.Remove("SearchedCacheDependency");

            return;
        }

        string submissionNo = string.Empty;

        string addinsuredname = txInsuredName.Text;
        if (string.IsNullOrEmpty(addinsuredname))
        {
            if (Common.GetSelectedItems(cblNames).Length == 1)
            {

                string selText = cblNames.SelectedItem.Text;
                int startIndexToStrip = selText.LastIndexOf("(");
                selText = selText.Remove(startIndexToStrip);
                addinsuredname = selText;
            }
        }
        addinsuredname = Server.HtmlDecode(addinsuredname);
        
        // else of the above if, implying new submission.
        string addXML = sp.AddSubmissionByIds(clientid,
            string.Empty, // stop gap for other apps
            clientaddressids, clientnameids, clientportfolioid,
            user, // stop gap for other apps
            companyid, // stop gap for other apps
            user, string.Empty, subtypeid, agencyid, agentid,
            string.Join("|", agentunspecifiedreasons),
            contactid, companynumber, substatusid, statusdate, substatusreasonid, otherreason,
            policynumber + "-Y", // append '-<something>' to indicate send to cobra, CORE checks if the system is bcsasclientcore. TODO remove this handling from Core.Clearance.Submissions and from here since we are not using web services anymore for CWG conversion process
            string.Join("|", codeids), string.Join("|", attributes),
            dpremium, dteff, dtexp, underwriterid, analystid, technicianid, policysystemid, comments, lineofbusinessid, lobchilren, addinsuredname, dba, submissionNo,
            othercarrierid, ocpremium);

        try
        {
            coresub = (BCS.Core.Clearance.BizObjects.Submission)BCS.Core.XML.Deserializer.Deserialize(
                addXML, typeof(BCS.Core.Clearance.BizObjects.Submission));
            if (coresub != null)
            {
                if (coresub.Status == "Failure")
                {
                    Common.SetStatus(messagePlaceHolder, string.Format("Unable to add submission because {0}", coresub.Message));
                }
                else
                {
                    if (coresub.Message != null)
                    {
                        Session["SubmissionId"] = coresub.Id.ToString();
                        Cache.Remove("SearchedCacheDependency");

                        LoadSubmission(true);
                        Common.SetStatus(messagePlaceHolder, string.Format("{0}, Submission Number : {1}", coresub.Message, coresub.SubmissionNumber));

                        btn.CommandName = "Edit";
                        btn.Text = "Update Submission";
                        lblOperation.Text = "Edit";
                        _operation = "edit";
                        lbInsuredName.Visible = true;
                        txInsuredName.Visible = true;
                        btnSelectClient.Visible = true;
                        btnSwitchClient.Visible = true;
                        SubmissionStatusDropDownList1.Enabled = true;
                        SetOperation(_operation);

                        SetInitialFocus();
                    }
                }
            }
        }
        catch (Exception)
        {
            Common.SetStatus(messagePlaceHolder,
                "Submission Add not successful. ");
        }
    }

    private string StripNameType(string p)
    {
        int index = p.LastIndexOf(" (");
        /* index cannot be negative since there is always a name type displayed, NA when null */
        p = p.Remove(index);

        return p;
    }

    protected void txEffectiveDate_TextChanged(object sender, EventArgs e)
    {
        int selAgency = Common.GetSafeSelectedValue(AgencyDropDownList1);

        DateTime eff = DateTime.MinValue;
        try
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(System.Globalization.CultureInfo.CurrentCulture.Name);
            ci.Calendar.TwoDigitYearMax = 2079;
            eff = Convert.ToDateTime(txEffectiveDate.Text, ci);

            txExpirationDate.Text = eff.AddYears(1).ToString("MM/dd/yyyy");
            UnderwritersDropDownList1.RetirementDate = eff;
            AnalystDropDownList1.RetirementDate = eff;
            LoadCodeTypes(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
            Common.SetTabOrder(TabIndex_Section_20, 20);
        }
        catch (FormatException)
        {
            txEffectiveDate.Text = string.Empty;
            txExpirationDate.Text = string.Empty;
        }


        AgencyDropDownList1.CancelDate = GetEffectiveDate(DateTime.MaxValue.Subtract(TimeSpan.FromSeconds(1)));
        AgencyDropDownList1.DataBind();

        if (AgencyDropDownList1.Items.FindByValue(selAgency.ToString()) != null)
        {
            AgencyDropDownList1.ClearSelection();
            AgencyDropDownList1.Items.FindByValue(selAgency.ToString()).Selected = true;
            AgencyDropDownList1_SelectedIndexChanged(AgencyDropDownList1, EventArgs.Empty);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, typeof(string), "obsoleteAgency1",
                "alert('Agency previously selected, is no longer available based on the effective date entered. Please select another.');", true);

            BCS.WebApp.UserControls.Helper.ClearDBItems(AgentDropDownList1);
            BCS.WebApp.UserControls.Helper.ClearDBItems(ContactDropDownList1);
            BCS.WebApp.UserControls.Helper.ClearDBItems(LineOfBusinessDropDownList1);
            BCS.WebApp.UserControls.Helper.ClearDBItems(UnderwritersDropDownList1);
            BCS.WebApp.UserControls.Helper.ClearDBItems(AnalystDropDownList1);
        }
        if (IsPostBack)
            txExpirationDate.Focus();
    }
    protected void btnCopy_Click(object sender, EventArgs e)
    {
        SavePageState(true);
        Session.Contents.Remove("SubmissionId");
        Response.Redirect("Submission.aspx?op=add");
    }
    protected void DefaultSelect(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
            return;
        ListControl rbl = (sender as ListControl);
        if (rbl.Items.Count == 1)
        {
            rbl.SelectedIndex = 0;
        }
    }

    protected void btnSelectClient_Click(object sender, EventArgs e)
    {
        Session["cliref"] = string.Format("Submission.aspx?op=edit");
        SavePageState(false);
        Response.Redirect("Client.aspx?op=edit");
    }
    protected void btnSwitchClient_Click(object sender, EventArgs e)
    {
        SavePageState(false);
        Response.Redirect("SwitchClient.aspx");
    }

    protected void PolicySystemDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PolicySystemDropDownList1.SelectedIndex > 0)
        {
            txPolicyNumber.CompanyNumberId = Convert.ToInt32(PolicySystemDropDownList1.SelectedValue);
        }
        else
        {
            txPolicyNumber.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        }
        ScriptManager1.SetFocus(PolicySystemDropDownList1);
    }
    protected void txSubmissionStatusDate_Init(object sender, EventArgs e)
    {
        txSubmissionStatusDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
    }
    protected void rblAddresses_SelectedIndexChanged(object sender, EventArgs e)
    {
        WarnIfUnlicensedAgency();
        ScriptManager1.SetFocus(rblAddresses);
    }
    protected void lbDbaNames_Init(object sender, EventArgs e)
    {
        lbDbaNames.Attributes.Add("onchange", string.Format("setValueToTextBox({0},{1});", lbDbaNames.ClientID, txDba.ClientID));
    }
    protected void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        throw new ApplicationException("Ajax Error", e.Exception);
    }
    protected void AgentDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        rfvAgentReasons.Enabled = DefaultValues.RequiresAgentUnspecifiedReasons(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) &&
            AgentDropDownList1.SelectedIndex < 1;
        if(IsPostBack)
            ScriptManager1.SetFocus(AgentDropDownList1);
    }
    protected void rfvAgentReasons_Init(object sender, EventArgs e)
    {
        rfvAgentReasons.Enabled = DefaultValues.RequiresAgentUnspecifiedReasons(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
    }
    protected void cbSelectAllAddresses_Init(object sender, EventArgs e)
    {
        cbSelectAllAddresses.Attributes.Add("onclick", string.Format("checkAllCBL(this, '{0}')", rblAddresses.ClientID));
    }
    protected void cbSelectAllNames_Init(object sender, EventArgs e)
    {
        cbSelectAllNames.Attributes.Add("onclick", string.Format("checkAllCBL(this, '{0}')", cblNames.ClientID));
    }
    protected void cbSelectAllAddresses_CheckedChanged(object sender, EventArgs e)
    {
        WarnIfUnlicensedAgency();
        ScriptManager1.SetFocus(cbSelectAllAddresses);
    }
    protected void cbTBD_Init(object sender, EventArgs e)
    {
        cbTBD.Attributes.Add("onclick", string.Format("TBDChanged('{0}', '{1}')", cbTBD.ClientID, rfvEffectiveDate.ClientID));
    }
    protected void cbTBD_CheckedChanged(object sender, EventArgs e)
    {
        rfvEffectiveDate.Enabled = !cbTBD.Checked;
        CompareValidator1.Enabled = !cbTBD.Checked;
        CompareValidator2.Enabled = !cbTBD.Checked;
        cvEffExp.Enabled = !cbTBD.Checked;
        txEffectiveDate.Enabled = !cbTBD.Checked;
        txExpirationDate.Enabled = !cbTBD.Checked;
        txEffectiveDate_TextChanged(txEffectiveDate, EventArgs.Empty);
        txEffectiveDate.Text = cbTBD.Checked ? "TBD" : string.Empty;
        txExpirationDate.Text = cbTBD.Checked ? "TBD" : string.Empty;
    }
    protected void DisableOnInit(object sender, EventArgs e)
    {
        (sender as WebControl).Attributes.Add("disabled", "true");
    }
    protected void customPolicyNumberValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;
        
        // at the moment only cwg has additional validation, and on adds
        if (Common.IsCWG() && _operation == "add")
        {
            if (txPolicyNumber.Generated)
            {
                // only validate if not generated
                return;
            }
            int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
            long entered = 0;
            try
            {
                entered = Convert.ToInt64(txPolicyNumber.Text);
            }
            catch (Exception)
            {
                // should be a number
                args.IsValid = false;
                customPolicyNumberValidator.Text = "Should be numeric.";
                return;
            }

            // should be less than less than or equal to the the next policy number in the control number table
            DataManager dm = new DataManager(DefaultValues.DSN);
            dm.QueryCriteria.And(JoinPath.NumberControl.Columns.CompanyNumberId, companyNumberId);
            dm.QueryCriteria.And(JoinPath.NumberControl.NumberControlType.Columns.TypeName, "Policy Number");
            NumberControl nc = dm.GetNumberControl();
            if (nc.ControlNumber.Value <= entered)
            {
                args.IsValid = false;
                customPolicyNumberValidator.ErrorMessage = "&nbsp;";
                customPolicyNumberValidator.Text = string.Format("Should be less than less than or equal to {0}.", nc.ControlNumber.Value);
                return;
            }
            // no existing policy numbers entered in Clearance for the new number
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(JoinPath.Submission.Columns.CompanyNumberId, companyNumberId);
            dm.QueryCriteria.And(JoinPath.Submission.Columns.PolicyNumber, entered.ToString());
            BCS.Biz.SubmissionCollection existingPolicySubmissions = dm.GetSubmissionCollection();
            if (existingPolicySubmissions != null && existingPolicySubmissions.Count > 0)
            {
                args.IsValid = false;
                customPolicyNumberValidator.Text = "Policy already exists in clearance";
                return;
            }
            
        }
    }
    #region modal popup for submission history

    protected void Panel1_Load(object sender, EventArgs e)
    {
        //string sid = Common.GetSafeStringFromSession("submissionId", "0");
        //ObjectDataSource1.SelectParameters["submissionId"].DefaultValue = sid;
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView historyGrid = sender as GridView;
        if (historyGrid.SelectedIndex == -1)
            return;
        int newStatusId = Common.GetSafeSelectedValue(SubmissionStatusDropDownList1);
        string result = BCS.Core.Clearance.Submissions.ToggleSubmissionStatusHistoryActive(Convert.ToInt32(historyGrid.SelectedDataKey["Id"]),
            Common.GetUser(), ref newStatusId);
        if (SubmissionStatusDropDownList1.SelectedValue != newStatusId.ToString())
        {
            Common.SetSafeSelectedValue(SubmissionStatusDropDownList1, newStatusId);
            SubmissionStatusDropDownList1_SelectedIndexChanged(SubmissionStatusDropDownList1, EventArgs.Empty);
        }
        Common.SetStatus(HistoryStatusPH, result, true);
        historyGrid.SelectedIndex = -1;
        ObjectDataSource1.DataBind();
        historyGrid.DataBind();

        programmaticModalPopup.Show();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SubmissionStatusHistory aHistory = e.Row.DataItem as SubmissionStatusHistory;
            if (!aHistory.Active)
            {
                foreach (TableCell var in e.Row.Cells)
                {
                    var.Style.Add(HtmlTextWriterStyle.TextDecoration, "line-through");
                }
            }

            LinkButton lb = e.Row.FindControl("LinkButton1") as LinkButton;
            ConfirmButtonExtender confirmButtonExtender = e.Row.FindControl("ConfirmButtonExtender1") as ConfirmButtonExtender;
            if (lb != null)
            {
                lb.Text = aHistory.Active ? "active" : "inactive";
                lb.Enabled = BCS.WebApp.Components.Security.CanActivateStatus() && (aHistory.Active.Value || User.IsInRole("System Administrator"));
            }
            if (confirmButtonExtender != null)
            {
                confirmButtonExtender.Enabled = BCS.WebApp.Components.Security.CanActivateStatus() && (aHistory.Active.Value || User.IsInRole("System Administrator"));
                if (User.IsInRole("System Administrator"))
                {
                    confirmButtonExtender.ConfirmText = "Are you sure you want to change this status history?";
                }
            }
        }
    } 
    #endregion
    #endregion

    #region Other Methods

    private void WarnIfUnlicensedAgency()
    {
        if (!DefaultValues.EnforceAgencyLicensedState(Common.GetSafeIntFromSession("CompanySelected")))
        {
            return;
        }

        System.Collections.Specialized.StringCollection licStates = new System.Collections.Specialized.StringCollection();
        
        string[] clientStates = GetSafeSelectedClientAddressStates();
        if (clientStates == null || clientStates.Length == 0)
        {

            btnSubmitSubmission.Attributes.Add("onclick",
                     string.Format(disableScript, btnSubmitSubmission.ClientID, "{", "}", Button1.ClientID));
            WarningAdded = false;
            return;
        }
        bool IsInAgencyLicensedStates = false;

        BCS.Core.Clearance.BizObjects.Agency ag = BCS.Core.Clearance.Lookups.GetAgency(Convert.ToInt32(AgencyDropDownList1.SelectedValue));

        if (ag != null && ag.LicensedStatesList != null && ag.LicensedStatesList.List != null && ag.LicensedStatesList.List.Length > 0)
        {
            foreach (BCS.Core.Clearance.BizObjects.State state in ag.LicensedStatesList.List)
            {
                licStates.Add(state.Abbreviation);        
            }
        }
        else
        {
            WarningAdded = false;
            return;
        }
        foreach (string clientState in clientStates)
        {
            if (!licStates.Contains(clientState))
            {
                IsInAgencyLicensedStates = false;
                break;
            }
            IsInAgencyLicensedStates = true;
        }


        if (!IsInAgencyLicensedStates)
        {
            string[] licStatesArr = new string[licStates.Count];
            licStates.CopyTo(licStatesArr, 0);
            string warningText = string.Format("Review state entered.  Only these states: ({0}) are authorized for this agency.",
                string.Join(", ", licStatesArr));

            string previousAttributeValue = btnSubmitSubmission.Attributes["onclick"];
            if (!WarningAdded)
            {
                warningText = warningText.Replace("'", "\\'");
                string newAttributeValue = previousAttributeValue + "; if(Page_ClientValidate()) { alert('" + warningText + "');}";
                btnSubmitSubmission.Attributes.Add("onclick", newAttributeValue);
            }
            WarningAdded = true;
        }
        else
        {
            btnSubmitSubmission.Attributes.Add("onclick",
                     string.Format(disableScript, btnSubmitSubmission.ClientID, "{", "}", Button1.ClientID));
            WarningAdded = false;
        }
    }
    private void LoadSubmission(bool ignorePostback)
    {
        pnlAddSubmission.Visible = true;

		if (!IsPostBack || ignorePostback)
        {
            if (Session["SubmissionId"] != null)
            {
                int id = Convert.ToInt32(Session["SubmissionId"]);
                SubmissionProxy.Submission s = Common.GetSubmissionProxy();
                BCS.Core.Clearance.BizObjects.SubmissionList sl = (BCS.Core.Clearance.BizObjects.SubmissionList)
                    BCS.Core.XML.Deserializer.Deserialize(
                    s.GetSubmissionById(id), typeof(BCS.Core.Clearance.BizObjects.SubmissionList));

                if (sl.List.Length == 0)
                {
                    BTS.LogFramework.LogCentral.Current.LogError(
                        string.Format("Probable cause of IndexOutOfRangeException. Expected Submission entry for Id {0}.", id));
                }
                PreviousUnderwriter.Text = sl.List[0].PreviousUnderwriterFullName;

                UnderwritersDropDownList1.RetirementDate = sl.List[0].EffectiveDate;
                AnalystDropDownList1.RetirementDate = sl.List[0].EffectiveDate;
                lblSubmissionDate.Text = sl.List[0].SubmissionDt == DateTime.MinValue ? string.Empty : sl.List[0].SubmissionDt.ToString("MM/dd/yyyy");
                if (txEffectiveDate.Enabled)
                {
                    txEffectiveDate.Text = sl.List[0].EffectiveDate == DateTime.MinValue ? string.Empty : sl.List[0].EffectiveDate.ToString("MM/dd/yyyy");
                }
		        txEffectiveDate_TextChanged(txEffectiveDate, EventArgs.Empty);
				
                if (txExpirationDate.Enabled)
                {
                    txExpirationDate.Text = sl.List[0].ExpirationDate == DateTime.MinValue ? string.Empty : sl.List[0].ExpirationDate.ToString("MM/dd/yyyy");
                }

                //CompanyDropDownList ct = Master.FindControl("CompanySelector1").FindControl("CompanyDropDownList1") as CompanyDropDownList;
                //ct.DataBind(Common.GetUser());
                //ct.Items.FindByValue(sl.List[0].CompanyId.ToString()).Selected = true;
                //Common.AddCookie("CompanySelected", sl.List[0].CompanyId.ToString());

                //CompanyNumberDropDownList cnt = Master.FindControl("CompanySelector1").FindControl("CompanyNumberDropDownList1") as CompanyNumberDropDownList;
                //cnt.CompanyId = Convert.ToInt32(ct.SelectedValue);
                //cnt.DataBind();
                //cnt.Items.FindByValue(sl.List[0].CompanyNumberId.ToString()).Selected = true;

                //Session["CompanyNumberSelected"] = sl.List[0].CompanyNumberId;
                //Common.AddCookie("CompanyNumberSelected", sl.List[0].CompanyNumberId.ToString());
                //LoadCodeTypes(sl.List[0].CompanyNumberId);
                //LoadAttributes(sl.List[0].CompanyNumberId);
                //Common.SetTabOrder(TabIndex_Section_20, 20);
                //Common.SetTabOrder(TabIndex_Section_40, 40);

                string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
                #region client load for edit submission
                structures.Info.ClientInfo client = new BTS.ClientCore.Wrapper.Structures.Info.ClientInfo();
                if (!IsPostBack)
                {
                    string clientid = sl.List[0].ClientCoreClientId.ToString();
                    Session["ClientId"] = clientid;

                    client = LoadClient(companyNumber, clientid);
                    if (client.Client.PortfolioId != sl.List[0].ClientCorePortfolioId.ToString())
                    {
                        s.UpdatePortfolioId(Common.GetSafeStringFromSession(SessionKeys.CompanyNumber),
                            Convert.ToInt64(clientid), Convert.ToInt64(client.Client.PortfolioId), Common.GetUser());
                    }
                }
                #endregion


                phSubmissionNumber.Visible = true;
                phSubmissionDate.Visible = true;
                //btnHistory.Visible = true;
                btnCopy.Visible = true;
                txSubmissionNumber.Text = sl.List[0].SubmissionNumber.ToString();
                Session["SubmissionNumber"] = txSubmissionNumber.Text;

                txSubmissionNumberSequence.Text = sl.List[0].SubmissionNumberSequence.ToString();

                SetSafeSelectedClientAddressIds(sl.List[0].ClientCoreAddressIds);
                if (sl.List[0].ClientCoreAddressIds == null || sl.List[0].ClientCoreAddressIds.Length == 0)
                {
                    foreach (ListItem item in rblAddresses.Items)
                    {
                        item.Selected = true;
                    }
                    #region Code that would ease the issue when there are no addresses associated with submission, associates the submission with all of the client addresses
                    List<string> lstAddressIds = new List<string>();
                    foreach (structures.Info.Address var in client.Addresses)
                    {
                        lstAddressIds.Add(var.AddressId);
                    }
                    string[] arrAddressIds = new string[lstAddressIds.Count];
                    lstAddressIds.CopyTo(arrAddressIds);
                    BCS.Core.Clearance.Submissions.UpdateSubmissionClientAddresses(sl.List[0].Id, string.Join("|", arrAddressIds)); 
                    #endregion
                }
                SetSafeSelectedClientNameIds(sl.List[0].ClientCoreNameIds);
                if (sl.List[0].ClientCoreNameIds == null || sl.List[0].ClientCoreNameIds.Length == 0)
                {
                    foreach (ListItem item in cblNames.Items)
                    {
                        item.Selected = true;
                    }
                }

                if (OtherCarrierDropDownList1.Items.FindByValue(sl.List[0].OtherCarrierId.ToString()) != null)
                {
                    OtherCarrierDropDownList1.ClearSelection();
                    OtherCarrierDropDownList1.Items.FindByValue(sl.List[0].OtherCarrierId.ToString()).Selected = true;
                }
                else
                {
                    if (sl.List[0].OtherCarrierId != 0)
                    {
                        OtherCarrierDropDownList1.ClearSelection();
                        ClientScript.RegisterStartupScript(typeof(string), "obsoleteOtherCarrier",
                                  "alert('The Other Carrier previously associated with this submission, is no longer valid. Please select a new one prior to saving the submission, or contact your system administrator for further information.');", true);
                    }
                }

                if (SubmissionStatusDropDownList1.CompanyNumberId != sl.List[0].CompanyNumberId)
                {
                    SubmissionStatusDropDownList1.CompanyNumberId = sl.List[0].CompanyNumberId;
                    SubmissionStatusDropDownList1.DataBind();
                }

                if (SubmissionStatusDropDownList1.Items.FindByValue(sl.List[0].SubmissionStatusId.ToString()) != null)
                {
                    SubmissionStatusDropDownList1.ClearSelection();
                    SubmissionStatusDropDownList1.Items.FindByValue(sl.List[0].SubmissionStatusId.ToString()).Selected = true;
                }
                else
                {
                    if (sl.List[0].SubmissionStatusId != 0)
                    {
                        SubmissionStatusDropDownList1.ClearSelection();
                        ClientScript.RegisterStartupScript(typeof(string), "obsoleteSubmissionStatus",
                                  "alert('The Submission Status previously associated with this submission, is no longer valid. Please select a new one prior to saving the submission, or contact your system administrator for further information.');", true);
                    }
                }
                SubmissionStatusDropDownList1_SelectedIndexChanged(null, EventArgs.Empty);

                // try to set with Id in db
                if (SubmissionStatusReasonDropDownList1.Items.FindByValue(sl.List[0].SubmissionStatusReasonId.ToString()) != null)
                {
                    SubmissionStatusReasonDropDownList1.ClearSelection();
                    SubmissionStatusReasonDropDownList1.Items.FindByValue(sl.List[0].SubmissionStatusReasonId.ToString()).Selected = true;
                }
                else
                {
                    SubmissionStatusReasonDropDownList1.Extra = sl.List[0].SubmissionStatusReasonId;
                    SubmissionStatusReasonDropDownList1.DataBind();
                    SetSafeSelectedValue(SubmissionStatusReasonDropDownList1, sl.List[0].SubmissionStatusReasonId, "obsoleteStatusReason",
                        "The Submission Status Reason previously associated with this submission, is no longer valid. Please select a new one prior to saving the submission, or contact your system administrator for further information.");
                }

                if (SubmissionTypeDropDownList1.CompanyNumberId != sl.List[0].CompanyNumberId)
                {
                    SubmissionTypeDropDownList1.CompanyNumberId = sl.List[0].CompanyNumberId;
                    SubmissionTypeDropDownList1.DataBind();
                }
                if (SubmissionTypeDropDownList1.Items.FindByValue(sl.List[0].SubmissionTypeId.ToString()) != null)
                {
                    Common.SetSafeSelectedValue(SubmissionTypeDropDownList1, sl.List[0].SubmissionTypeId);
                }
                else
                {
                    ClientScript.RegisterStartupScript(typeof(string), "obsoleteSubmissionType",
                            "alert('The Submission Type previously associated with this submission, is no longer valid. Please select a new one prior to saving the submission, or contact your system administrator for further information.');", true);
                }
                txSubmissionStatusDate.Text = sl.List[0].SubmissionStatusDate == DateTime.MinValue ? DateTime.Today.ToString("MM/dd/yyyy") : sl.List[0].SubmissionStatusDate.ToString("MM/dd/yyyy");

                AgencyDropDownList1.CompanyNumberId = sl.List[0].CompanyNumberId;
                AgencyDropDownList1.Extra = sl.List[0].AgencyId;
                AgencyDropDownList1.CancelDate = GetEffectiveDate(DateTime.MaxValue.Subtract(TimeSpan.FromSeconds(1)));
                AgencyDropDownList1.DataBind();
                if (AgencyDropDownList1.Items.FindByValue(sl.List[0].AgencyId.ToString()) != null)
                {
                    AgencyDropDownList1.ClearSelection();
                    AgencyDropDownList1.Items.FindByValue(sl.List[0].AgencyId.ToString()).Selected = true;
                    BCS.Core.Clearance.BizObjects.Agency ag = BCS.Core.Clearance.Lookups.GetAgency(sl.List[0].Agency.Id);
                    lblAgencyComment.Text = ag != null ? ag.Comments : string.Empty;
                    lblAgencyComment.Visible = !string.IsNullOrEmpty(lblAgencyComment.Text);
                }
                else
                {
                    ClientScript.RegisterStartupScript(typeof(string), "obsoleteAgency",
                            "alert('The agency previously associated with this submission, is inactive or canceled. Please select a new one prior to saving the submission, or contact your system administrator for further information.');", true);
                }

		        AgencyDropDownList1_SelectedIndexChanged(null, EventArgs.Empty);
				

                if (AgencyDropDownList1.Items.FindByValue(sl.List[0].AgencyId.ToString()) != null)
                {
                    LineOfBusinessDropDownList1.ClearSelection();
                    bool hasObsoleteLineOfBusiness = false;
                    if (sl.List[0].LineOfBusinessList != null && sl.List[0].LineOfBusinessList.Length > 0)
                    {
                        foreach (BCS.Core.Clearance.BizObjects.LineOfBusiness l in sl.List[0].LineOfBusinessList)
                        {
                            if (LineOfBusinessDropDownList1.Items.FindByValue(l.Id.ToString()) != null)
                                LineOfBusinessDropDownList1.Items.FindByValue(l.Id.ToString()).Selected = true;
                            else
                                hasObsoleteLineOfBusiness = true;
                        }
                    }
                    else
                    {
                        if (hasObsoleteLineOfBusiness)
                        {
                            ClientScript.RegisterStartupScript(typeof(string), "obsoleteLineOfBusiness",
                              "alert('The Underwriting Unit previously associated with this submission, is no longer valid. Please select a new one prior to saving the submission, or contact your system administrator for further information.');", true);
                        }
                    }
                }

		        LineOfBusinessDropDownList1_SelectedIndexChanged(null, EventArgs.Empty);
				
                if (AgentDropDownList1.Items.FindByValue(sl.List[0].AgentId.ToString()) != null)
                {
                    AgentDropDownList1.ClearSelection();
                    AgentDropDownList1.Items.FindByValue(sl.List[0].AgentId.ToString()).Selected = true;
                }
                else
                {
                    if (sl.List[0].AgentId != 0)
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "obsoleteAgent",
                          "alert('The Agent previously associated with this submission, is no longer valid. Please select a new one prior to saving the submission, or contact your system administrator for further information.');", true);
                    }
                }
                AgentDropDownList1_SelectedIndexChanged(AgentDropDownList1, EventArgs.Empty);

                if (ContactDropDownList1.Items.FindByValue(sl.List[0].ContactId.ToString()) != null)
                {
                    ContactDropDownList1.ClearSelection();
                    ContactDropDownList1.Items.FindByValue(sl.List[0].ContactId.ToString()).Selected = true;
                }
                else
                {
                    if (sl.List[0].ContactId != 0)
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "obsoleteContact",
                          "alert('The Contact previously associated with this submission, is no longer valid. Please select a new one prior to saving the submission, or contact your system administrator for further information.');", true);
                    }
                }

                if (UnderwritersDropDownList1.Items.FindByValue(sl.List[0].UnderwriterId.ToString()) != null)
                {
                    UnderwritersDropDownList1.ClearSelection();
                    UnderwritersDropDownList1.Items.FindByValue(sl.List[0].UnderwriterId.ToString()).Selected = true;
                }
                else
                {
                    if (sl.List[0].UnderwriterId != 0)
                    {
                        if (!DefaultValues.FillAllUnderwriters(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                        {
                            lblObsoleteUnderwriter.Text = string.Format(obsoleteUnderwriter, string.Concat("'", sl.List[0].UnderwriterName, "'"));
                        }
                        else
                        {
                            UnderwritersDropDownList1.Extra = sl.List[0].UnderwriterId;

                            LineOfBusinessDropDownList1_SelectedIndexChanged(null, EventArgs.Empty);

                            if (UnderwritersDropDownList1.Items.FindByValue(sl.List[0].UnderwriterId.ToString()) != null)
                            {
                                UnderwritersDropDownList1.ClearSelection();
                                UnderwritersDropDownList1.Items.FindByValue(sl.List[0].UnderwriterId.ToString()).Selected = true;
                            }
                            else
                            {
                                lblObsoleteUnderwriter.Text = string.Format(obsoleteUnderwriter, string.Concat("'", sl.List[0].UnderwriterName, "'"));
                            }
                        }
                    }
                }
                if (AnalystDropDownList1.Items.FindByValue(sl.List[0].AnalystId.ToString()) != null)
                {
                    AnalystDropDownList1.ClearSelection();
                    AnalystDropDownList1.Items.FindByValue(sl.List[0].AnalystId.ToString()).Selected = true;
                }
                else
                {
                    if (sl.List[0].AnalystId != 0)
                    {
                        lblObsoleteAnalyst.Text = string.Format(obsoleteAnalyst, string.Concat("'", sl.List[0].AnalystName, "'"));
                    }
                }

                if (PolicySystemDropDownList1.Items.FindByValue(sl.List[0].PolicySystemId.ToString()) != null)
                {
                    PolicySystemDropDownList1.ClearSelection();
                    PolicySystemDropDownList1.Items.FindByValue(sl.List[0].PolicySystemId.ToString()).Selected = true;
                }
                else
                {
                    if (sl.List[0].PolicySystemId != 0)
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "obsoletePolicySystem",
                          "alert('The Policy System previously associated with this submission, is no longer valid. Please select a new one prior to saving the submission, or contact your system administrator for further information.');", true);
                    }
                }
                PolicySystemDropDownList1_SelectedIndexChanged(null, EventArgs.Empty);
                
                txComments.Text = sl.List[0].Comments;
                txPolicyNumber.Text = sl.List[0].PolicyNumber;
                txEstimatedPremium.Text = string.Format("{0:c}", sl.List[0].EstimatedWrittenPremium);
                txOtherCarrierPremium.Text = string.Format("{0:c}", sl.List[0].OtherCarrierPremium);
                txInsuredName.Text = sl.List[0].InsuredName;
                txDba.Text = sl.List[0].Dba;
                txSubmissionStatusReasonOther.Text = sl.List[0].SubmissionStatusReasonOther;

                #region Bind Submissions Company Number Codes
                if (sl.List[0].SubmissionCodeTypess != null)
                {
                    foreach (BCS.Core.Clearance.BizObjects.SubmissionCodeType codeType in sl.List[0].SubmissionCodeTypess)
                    {
                        int i = codeType.CodeId;
                        //if (phCodeTypes.Controls.Count > 0)
                        //{
                        //    if (phCodeTypes.Controls[0] is HtmlTable)
                        //    {
                        //        HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                        //        foreach (HtmlTableRow row in table.Rows)
                        //        {
                        //            foreach (HtmlTableCell cell in row.Cells)
                        //            {
                        //                foreach (Control c in cell.Controls)
                        //                {
                        //                    if (c is DropDownList)
                        //                    {
                        //                        DropDownList ddl = c as DropDownList;
                        //                        ListItem li = ddl.Items.FindByValue(i.ToString());
                        //                        if (li != null)
                        //                        {
                        //                            ddl.ClearSelection();
                        //                            li.Selected = true;
                        //                        }
                        //                    }
                        //                }
                        //            }
                        //        }
                        //    }
                        //}
                        // TODO : logic below is replaced by above. could be implemented in other sections of this page.
                        // careful of else of if(li != null), its there so that legacy submissions with expired codes can be still viewed.
                        Control pc = Common.FindControlRecursive(Page, codeType.CodeName + "ddl");
                        if (pc != null)
                        {
                            if (pc is DropDownList)
                            {
                                DropDownList ddl = pc as DropDownList;
                                ListItem li = ddl.Items.FindByValue(i.ToString());
                                if (li != null)
                                {
                                    ddl.ClearSelection();
                                    li.Selected = true;
                                }
                                else
                                {
                                    ListItem nli = new ListItem(string.Concat(codeType.CodeValue, " - ", codeType.CodeDescription), codeType.CodeId.ToString());
                                    ddl.ClearSelection();
                                    nli.Selected = true;
                                    ddl.Items.Add(nli);
                                }
                            }
                        }
                    }
                }
                else // there might be some default values for these code types, clear out these values if we are loading a submission with no codes assigned.
                {
                    if (phCodeTypes.Controls.Count > 0)
                    {
                        if (phCodeTypes.Controls[0] is HtmlTable)
                        {
                            HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                            foreach (HtmlTableRow row in table.Rows)
                            {
                                foreach (HtmlTableCell cell in row.Cells)
                                {
                                    foreach (Control c in cell.Controls)
                                    {
                                        if (c is DropDownList)
                                        {
                                            DropDownList ddl = c as DropDownList;
                                            ddl.ClearSelection();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                LOBList1.SetCodeTypeValues(sl.List[0].SubmissionCodeTypess);
                #endregion


                #region Bind Submission Company Number Attributes
                if (sl.List[0].AttributeList != null && phAttributes.Controls.Count > 0)
                {
                    foreach (BCS.Core.Clearance.BizObjects.SubmissionAttribute sa in sl.List[0].AttributeList)
                    {
                        if (phAttributes.Controls[0] is HtmlTable)
                        {
                            HtmlTable table = phAttributes.Controls[0] as HtmlTable;
                            foreach (HtmlTableRow row in table.Rows)
                            {
                                foreach (HtmlTableCell cell in row.Cells)
                                {
                                    foreach (Control c in cell.Controls)
                                    {
                                        if (c is WebControl)
                                        {
                                            System.ComponentModel.AttributeCollection attributes = System.ComponentModel.TypeDescriptor.GetAttributes(c);
                                            ControlValuePropertyAttribute attribute = (ControlValuePropertyAttribute)attributes[typeof(ControlValuePropertyAttribute)];

                                            if (c.ID == sa.CompanyNumberAttributeId.ToString())
                                                c.GetType().GetProperty(attribute.Name).SetValue(c, Convert.ChangeType(sa.AttributeValue, c.GetType().GetProperty(attribute.Name).PropertyType), null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region submission lob children
                LOBList1.SetItems(sl.List[0].LineOfBusinessChildList, true);

                #endregion

                SetSafeSelectedValues(cblAgentUnspecifiedReasons, sl.List[0].AgentUnspecifiedReasonList);
            }
            else
            {
                #region client load for add
                string clientid = Session["ClientId"].ToString();
                //CompanyNumberDropDownList cnddl = Master.FindControl("companyselector1").FindControl("CompanyNumberDropDownList1") as CompanyNumberDropDownList;
                LoadClient(Common.GetSafeStringFromSession(SessionKeys.CompanyNumber), clientid);
                                
                #endregion
            }
            LoadPageState();
        }
    }

    private void SetSafeSelectedValues(ListControl listControl,
        BCS.Core.Clearance.BizObjects.SubmissionAgentUnspecifiedReason[] submissionAgentUnspecifiedReasons)
    {
        if (submissionAgentUnspecifiedReasons == null)
            return;
        listControl.ClearSelection();
        foreach (BCS.Core.Clearance.BizObjects.SubmissionAgentUnspecifiedReason saur in submissionAgentUnspecifiedReasons)
            listControl.Items.FindByValue(saur.CompanyNumberAgentUnspecifiedReasonId.ToString()).Selected = true;
    }

    private structures.Info.ClientInfo LoadClient(string companyNumber, string clientid)
    {
        ClientProxy.Client clientproxy = Common.GetClientProxy();

        string clientxml = clientproxy.GetClientById(companyNumber, clientid);

        try
        {
            //structures.Info.ClientInfo client = (structures.Info.ClientInfo)BCS.Core.XML.Deserializer.Deserialize(
            //clientxml, typeof(structures.Info.ClientInfo), "seq_nbr");

            BCS.Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                (BCS.Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                clientxml, typeof(BCS.Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

            structures.Info.ClientInfo client = clearanceclient.ClientCoreClient;

            pnlClientInfo.Visible = true;


            if (client.Client.ClientId == clientid)
            {
                Session["Client"] = client;
                foreach (structures.Info.Name name in client.Client.Names)
                {
                    StringBuilder sb = new StringBuilder();
                    if (name.NameBusiness != null && name.NameBusiness.Length > 0)
                        sb.Append(name.NameBusiness);
                    else
                        sb.AppendFormat("{0} {1} {2}", name.NameFirst, name.NameMiddle, name.NameLast);

                    if (name.ClearanceNameType == "DBA")
                    {
                        lbDbaNames.Items.Add(new ListItem(sb.ToString(), sb.ToString()));
                    }

                    if (!string.IsNullOrEmpty(name.ClearanceNameType))
                    {
                        sb.AppendFormat(" ({0})", name.ClearanceNameType);
                    }
                    else
                    {
                        sb.AppendFormat(" ({0})", "NA");
                    }
                    
                    cblNames.Items.Add(new ListItem(Server.HtmlEncode(sb.ToString()), name.SequenceNumber));

                    
                    if (name.ClearanceNameType == "Insured")
                    {
                        // only populate first insured, if multile insureds are present.
                        if (txInsuredName.Text == string.Empty)
                            txInsuredName.Text = name.NameBusiness;
                    }
                }
                lbDbaNames.Enabled = lbDbaNames.Items.Count != 0;
                if (DefaultValues.DesiresDBAAutoSelectWhenOnlyOne(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                {
                    if (lbDbaNames.Items.Count == 1)
                        txDba.Text = lbDbaNames.Items[0].Text;
                }
                if (lbDbaNames.Items.Count == 0)
                    lbDbaNames.Items.Add(new ListItem("No DBAs exist."));


                if (client.Addresses != null)
                {
                    foreach (structures.Info.Address addr in client.Addresses)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0} {1} {2} {3} {4} {5}", addr.HouseNumber, addr.Address1, addr.Address2,
                            addr.City, addr.StateProvinceId, addr.PostalCode);
                        if (!string.IsNullOrEmpty(addr.ClearanceAddressType))
                        {
                            sb.AppendFormat(" ({0})", addr.ClearanceAddressType);
                        }
                        else
                        {
                            sb.AppendFormat(" ({0})", "NA");
                        }

                        rblAddresses.Items.Add(new ListItem(Server.HtmlEncode(sb.ToString()), string.Format("{0}-{1}", addr.AddressId, addr.StateProvinceId)));
                    }
                }
                lblClientId.Text = client.Client.ClientId;
                lblPortfolioId.Text = client.Client.PortfolioId;
            }

            #region code used when editing client from submission page is enabled
            hfClientId.Value = clientid;
            #endregion

            return client;

        }
        catch (Exception ex)
        {
            BTS.LogFramework.LogCentral.Current.LogWarn(string.Format("The client associated with the submission either does not exist or is inaccessible. Id = {0} CompanyNumberId = {1}", clientid, Session["CompanyNumberSelected"]), ex);
            pnlClientNames.Visible = false;
            pnlClientAddress.Visible = false;
            hfClientId.Visible = false;
            lblPortfolioId.Visible = false;
            rblAddresses.Items.Add(new ListItem("", "0-"));
            cblNames.Items.Add(new ListItem("", "0"));
            rblAddresses.SelectedIndex = 0;
            cblNames.SelectedIndex = 0;

            pnlClientInfo.Visible = true;
            Common.SetStatus(pnlClientInfo,
                "The client associated with the submission either does not exist or is inaccessible.");
        }
        return new BTS.ClientCore.Wrapper.Structures.Info.ClientInfo();
    }

    private int[] GetCompanyNumerCodes(ArrayList al, string p)
    {
        Pair[] codesdesc = new Pair[al.Count];
        al.CopyTo(codesdesc);

        string[] codes = new string[codesdesc.Length];
        string[] descs = new string[codesdesc.Length];
        string[] types = new string[codesdesc.Length];
        for (int i = 0; i < codesdesc.Length; i++)
        {
            codes[i] = codesdesc[i].First.ToString().Trim();
            descs[i] = codesdesc[i].Second.ToString().Trim().Substring(0, codesdesc[i].Second.ToString().Length - 2);
            types[i] = codesdesc[i].Second.ToString().Trim().Substring(codesdesc[i].Second.ToString().Length - 2);

        }


        DataManager dmm = new DataManager(DefaultValues.DSN);
        dmm.QueryCriteria.And(JoinPath.CompanyNumberCode.CompanyNumber.Columns.PropertyCompanyNumber, p).And(
            JoinPath.CompanyNumberCode.Columns.Code, codes, MatchType.In);
        CompanyNumberCodeCollection codecoll = dmm.GetCompanyNumberCodeCollection();

        CompanyNumberCodeCollection ncodecoll = new CompanyNumberCodeCollection();

        foreach (string type in types)
        {
            CompanyNumberCode du = codecoll.FindByCompanyCodeTypeId(type);
            if (codecoll.FindByCompanyCodeTypeId(type) != null)
                ncodecoll.Add(codecoll.FindByCompanyCodeTypeId(type));
        }

        int[] codeids = new int[ncodecoll.Count];
        for (int i = 0; i < ncodecoll.Count; i++)
        {
            codeids[i] = ncodecoll[i].Id;
        }

        return codeids;
    }
    private void LoadCodeTypes(int companyNumberId)
    {
        #region save selections to restore
        ArrayList alids = new ArrayList();
        if (phCodeTypes.Controls.Count > 0)
        {
            if (phCodeTypes.Controls[0] is HtmlTable)
            {
                HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                foreach (HtmlTableRow row in table.Rows)
                {
                    foreach (HtmlTableCell cell in row.Cells)
                    {
                        foreach (Control c in cell.Controls)
                        {
                            if (c is DropDownList)
                            {
                                DropDownList ddl = c as DropDownList;
                                if (ddl.SelectedIndex > 0)
                                {
                                    alids.Add(Common.GetSafeSelectedValue(ddl));
                                }
                            }
                        }
                    }
                }
            }
        }
        string[] arrStr = LOBList1.GetCodeTypeValues();
        int[] arrInt = new int[arrStr.Length];
        arrInt = Array.ConvertAll<string, int>(arrStr, new Converter<string, int>(Convert.ToInt32));
        alids.AddRange(arrInt);
        #endregion

        DateTime effDt = GetEffectiveDate(DateTime.Today);
        DataManager dm = new DataManager(DefaultValues.DSN);
        dm.QueryCriteria.And(JoinPath.CompanyNumberCodeType.Columns.CompanyNumberId, companyNumberId);
        dm.QueryCriteria.And(JoinPath.CompanyNumberCodeType.Columns.ClientSpecific, false);
        DataManager.CriteriaGroup cg = new DataManagerBase.CriteriaGroup();        

        CompanyNumberCodeTypeCollection allTypes = dm.GetCompanyNumberCodeTypeCollection(FetchPath.CompanyNumberCodeType.CompanyNumberCode);

        // fitler what should be visible in this section
        CompanyNumberCodeTypeCollection types = allTypes.FilterByVisibleInLOBGridSection(false);
        types = types.SortByDisplayOrder(OrmLib.SortDirection.Ascending);

        // filter what should be in lob grid
        CompanyNumberCodeTypeCollection otherTypes = allTypes.FilterByVisibleInLOBGridSection(true);
        otherTypes = otherTypes.SortByDisplayOrder(OrmLib.SortDirection.Ascending);
        LOBList1.LoadCodeTypes(otherTypes, effDt);

        phCodeTypes.Controls.Clear();

        if (types != null && types.Count > 0)
        {
            HtmlTable table = new HtmlTable();
            table.ID = "tblCodeTypes";
            phCodeTypes.Controls.Add(table);
            foreach (CompanyNumberCodeType type in types)
            {
                CompanyNumberCodeCollection codes = type.CompanyNumberCodes;
                if (codes != null && codes.Count > 0)
                {
                    HtmlTableRow row = new HtmlTableRow();
                    table.Rows.Add(row);
                    HtmlTableCell cell = new HtmlTableCell();
                    row.Cells.Add(cell);
                    cell.InnerText = type.CodeName;

                    cell = new HtmlTableCell();
                    row.Cells.Add(cell);
                    DropDownList ddl = new DropDownList();
                    ddl.ID = type.CodeName + "ddl";
                    Common.AddControl(cell, ddl, false, true);
                    ListItemCollection lic = new ListItemCollection();
                    foreach (CompanyNumberCode code in codes)
                    {
                        if (code.ExpirationDate.IsNull || code.ExpirationDate > effDt)
                            lic.Add(new ListItem(string.Concat(code.Code, " - ", code.Description), code.Id.ToString()));
                    }
                    lic.Insert(0, new ListItem("", "0"));

                    ddl.Items.AddRange(Common.SortListItems(lic));
                    foreach (int var in alids)
                    {
                        if (var != 0)
                        {
                            ListItem li = ddl.Items.FindByValue(var.ToString());
                            if (null != li)
                            {
                                li.Selected = true;
                                alids.Remove(var);
                                break;
                            }
                        }
                    }

                    if (!type.DefaultCompanyNumberCodeId.IsNull)
                        Common.SetSafeSelectedValue(ddl, type.DefaultCompanyNumberCodeId);

                    // if only one real option, set it as default. #850                    
                    if (ddl.Items.Count == 2)
                        ddl.Items[1].Selected = true;

                    if (type.Required)
                    {
                        RFV rfv = new RFV();
                        Common.AddControl(cell, rfv, false, false);
                        rfv.Display = ValidatorDisplay.None;
                        rfv.ControlToValidate = ddl.ID;
                        rfv.InitialValue = "0";
                        rfv.SetFocusOnError = true;
                        rfv.ErrorMessage = "&nbsp;";
                        rfv.Text = " *";
                    }
                }
            }
            phCodeTypes.Visible = true;
            rowCodeTypes.Visible = table.Rows.Count > 0;
        }
        else
        { rowCodeTypes.Visible = false; }        
    }
    private void LoadAttributes(int companyNumberId)
    {
        BCS.Biz.CompanyNumberAttributeCollection attributes =
                BCS.Core.Clearance.Lookups.GetCompanyNumberAttributesByCompanyNumberId(companyNumberId.ToString(), false);
        attributes = attributes.SortByDisplayOrder(OrmLib.SortDirection.Ascending);
        phAttributes.Controls.Clear();
        if (attributes.Count > 0)
        {
            HtmlTable table = new HtmlTable();
            table.ID = "tblAttributes";
            phAttributes.Controls.Add(table);
            foreach (BCS.Biz.CompanyNumberAttribute attr in attributes)
            {
                HtmlTableRow row = new HtmlTableRow();
                table.Rows.Add(row);
                HtmlTableCell cell = new HtmlTableCell();
                row.Cells.Add(cell);
                cell.InnerText = attr.Label;

                cell = new HtmlTableCell();
                row.Cells.Add(cell);

                Type t = Type.GetType(string.Concat(attr.AttributeDataType.UserControl, ", System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), false, true);
                System.Reflection.ConstructorInfo ci = t.GetConstructor(new Type[] { });
                WebControl c = (WebControl)ci.Invoke(null);
                c.ID = attr.Id.ToString();
                if (attr.AttributeDataType.DataTypeName.Equals("Date", StringComparison.CurrentCultureIgnoreCase))
                {
                    c.Attributes.Add("onclick", "scwShow(this,this);");
                    c.Attributes.Add("onkeydown", "hideCalOnTab(event);");
                    c.Attributes.Add("onblur", "this.value = purgeDate(this);");
                }
                if (attr.AttributeDataType.DataTypeName.Equals("Money", StringComparison.CurrentCultureIgnoreCase))
                    c.Attributes.Add("onblur", "this.value=formatCurrency(this.value);");
                
                Common.AddControl(cell, c, false, true);
                if (attr.Required)
                {
                    RFV rfv = new RFV();
                    Common.AddControl(cell, rfv, false, false);
                    rfv.Display = ValidatorDisplay.None;
                    rfv.ControlToValidate = c.ID;
                    rfv.SetFocusOnError = true;
                    rfv.ErrorMessage = "&nbsp;";
                    rfv.Text = " *";
                }
                if (attr.AttributeDataType.DataTypeName.Equals("Date", StringComparison.CurrentCultureIgnoreCase))
                {
                    CompareValidator cv = new CompareValidator();
                    Common.AddControl(cell, cv, false, false);
                    cv.ControlToValidate = c.ID;
                    cv.SetFocusOnError = true;
                    cv.Type = ValidationDataType.Date;
                    cv.Operator = ValidationCompareOperator.DataTypeCheck;
                    cv.ErrorMessage = "&nbsp;";
                    cv.Text = "* MM/dd/yyyy";
                }
                if (attr.ReadOnlyProperty)
                    c.Enabled = false;
            }
            phAttributes.Visible = true;
            rowAttributes.Visible = table.Rows.Count > 0;
        }
        else
        { rowAttributes.Visible = false; phAttributes.Visible = false; }
    }

    private void LoadPageState()
    {
        if (Request.UrlReferrer == null)
            return;

        if (Request.UrlReferrer.AbsolutePath != Page.ResolveUrl("Client.aspx") &&
            Request.UrlReferrer.AbsolutePath != Page.ResolveUrl("SwitchClient.aspx") &&
            Request.Url.AbsolutePath != Request.UrlReferrer.AbsolutePath)
        {
            Session.Contents.Remove("temporarySubmission");
            return;
        }
        try
        {
            if (Session["temporarySubmission"] != null)
            {
                BCS.Core.Clearance.BizObjects.Submission s = (BCS.Core.Clearance.BizObjects.Submission)Session["temporarySubmission"];

                Common.SetSafeSelectedValue(OtherCarrierDropDownList1, s.OtherCarrierId);

                bool isCopy = Request.Url.AbsolutePath == Request.UrlReferrer.AbsolutePath;
                bool desiresDefaultStatus = ConfigValues.DesiresDefaultStatusOnSubmissionCopy(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

                if (isCopy && desiresDefaultStatus)
                {
                    // we need not do anything here, since its anyhow defaulted
                }
                else
                {
                    Common.SetSafeSelectedValue(SubmissionStatusDropDownList1, s.SubmissionStatusId);
                    SubmissionStatusDropDownList1_SelectedIndexChanged(SubmissionStatusDropDownList1, EventArgs.Empty);

                    Common.SetSafeSelectedValue(SubmissionStatusReasonDropDownList1, s.SubmissionStatusReasonId);
                }
            

                Common.SetSafeSelectedValue(SubmissionTypeDropDownList1, s.SubmissionTypeId);

                txEffectiveDate.Text = s.EffectiveDate == DateTime.MinValue ? string.Empty : s.EffectiveDate.ToString("MM/dd/yyyy");
                txEffectiveDate_TextChanged(txEffectiveDate, EventArgs.Empty);

                txExpirationDate.Text = s.ExpirationDate == DateTime.MinValue ? string.Empty : s.ExpirationDate.ToString("MM/dd/yyyy");
                txSubmissionStatusDate.Text = s.SubmissionStatusDate == DateTime.MinValue ? string.Empty : s.SubmissionStatusDate.ToString("MM/dd/yyyy");
                if (isCopy)
                {
                    txSubmissionStatusDate.Text = DateTime.Today.ToString(Common.DateFormat);
                }
                #region client load
                SetSafeSelectedClientAddressIds(s.ClientCoreAddressIds);
                SetSafeSelectedClientNameIds(s.ClientCoreNameIds);                
                #endregion

                Common.SetSafeSelectedValue(AgencyDropDownList1, s.AgencyId);
                AgencyDropDownList1_SelectedIndexChanged(AgencyDropDownList1, EventArgs.Empty);

                Common.SetSafeSelectedValue(AgentDropDownList1, s.AgentId);
                AgentDropDownList1_SelectedIndexChanged(AgentDropDownList1, EventArgs.Empty);
                Common.SetSafeSelectedValue(ContactDropDownList1, s.ContactId);

                Common.SetSafeSelectedValue(LineOfBusinessDropDownList1, s.LineOfBusinessList[0].Id);
                LineOfBusinessDropDownList1_SelectedIndexChanged(LineOfBusinessDropDownList1, EventArgs.Empty);

                #region Load Company Number Code
                LoadCodeTypes(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                if (s.SubmissionCodeTypeIds != null)
                {
                    foreach (int i in s.SubmissionCodeTypeIds)
                    {
                        if (phCodeTypes.Controls[0] is HtmlTable)
                        {
                            HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                            foreach (HtmlTableRow row in table.Rows)
                            {
                                foreach (HtmlTableCell cell in row.Cells)
                                {
                                    foreach (Control c in cell.Controls)
                                    {
                                        if (c is DropDownList)
                                        {
                                            DropDownList ddl = c as DropDownList;
                                            ListItem li = ddl.Items.FindByValue(i.ToString());
                                            if (li != null)
                                            {
                                                ddl.ClearSelection();
                                                li.Selected = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Load Attributes
                if (s.AttributeList != null)
                {
                    foreach (BCS.Core.Clearance.BizObjects.SubmissionAttribute sa in s.AttributeList)
                    {
                        if (phAttributes.Controls[0] is HtmlTable)
                        {
                            HtmlTable table = phAttributes.Controls[0] as HtmlTable;
                            foreach (HtmlTableRow row in table.Rows)
                            {
                                foreach (HtmlTableCell cell in row.Cells)
                                {
                                    foreach (Control c in cell.Controls)
                                    {
                                        if (c is WebControl)
                                        {
                                            System.ComponentModel.AttributeCollection attributes = System.ComponentModel.TypeDescriptor.GetAttributes(c);
                                            ControlValuePropertyAttribute attribute = (ControlValuePropertyAttribute)attributes[typeof(ControlValuePropertyAttribute)];

                                            if (c.ID == sa.CompanyNumberAttributeId.ToString())
                                                c.GetType().GetProperty(attribute.Name).SetValue(c, Convert.ChangeType(sa.AttributeValue, c.GetType().GetProperty(attribute.Name).PropertyType), null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
                
                Common.SetTabOrder(TabIndex_Section_20, 20);
                Common.SetTabOrder(TabIndex_Section_40, 40);

                Common.SetSafeSelectedValue(UnderwritersDropDownList1, s.UnderwriterId);

                Common.SetSafeSelectedValue(AnalystDropDownList1, s.AnalystId);

                Common.SetSafeSelectedValue(PolicySystemDropDownList1, s.PolicySystemId);

                txPolicyNumber.Text = s.PolicyNumber;

                txDba.Text = s.Dba;
                txInsuredName.Text = s.InsuredName;
                txSubmissionStatusReasonOther.Text = s.SubmissionStatusReasonOther;
                txEstimatedPremium.Text = string.Format("{0:c}", s.EstimatedWrittenPremium);
                txOtherCarrierPremium.Text = string.Format("{0:c}", s.OtherCarrierPremium);

                if (Request.Url.AbsolutePath != Request.UrlReferrer.AbsolutePath)
                {
                    LOBList1.SetItems(s.LineOfBusinessChildList, true);
                    LOBList1.SetCodeTypeValues(s.SubmissionCodeTypeIds);
                }
                txComments.Text = s.Comments;
                SetSafeSelectedValues(cblAgentUnspecifiedReasons, s.AgentUnspecifiedReasonList);

                if (isCopy)
                {
                    List<string> listCopyExcludedFields = ConfigValues.GetCopyExcludedFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                    foreach (string var in listCopyExcludedFields)
                    {
                        Control c = Common.FindControlRecursive(pnlAddSubmission, var);
                        System.ComponentModel.AttributeCollection attributes = System.ComponentModel.TypeDescriptor.GetAttributes(c);
                        ControlValuePropertyAttribute attribute = (ControlValuePropertyAttribute)attributes[typeof(ControlValuePropertyAttribute)];
                        
                        //c.GetType().GetProperty(attribute.Name).SetValue(c, Convert.ChangeType(sa.AttributeValue, c.GetType().GetProperty(attribute.Name).PropertyType), null);
                        Type type = c.GetType();
                        PropertyInfo pinfo = type.GetProperty(attribute.Name);
                        pinfo.SetValue(c, attribute.DefaultValue, null);

                    }
                }
            }
        }
        catch (Exception ex)
        { }

        Session.Contents.Remove("temporarySubmission");

    }
    private void SavePageState(bool excludeLOBChildren)
    {
        BCS.Core.Clearance.BizObjects.Submission s = new BCS.Core.Clearance.BizObjects.Submission();

        s.OtherCarrierId = Common.GetSafeSelectedValue(OtherCarrierDropDownList1);
        s.SubmissionStatusId = Common.GetSafeSelectedValue(SubmissionStatusDropDownList1);
        s.SubmissionStatusReasonId = Common.GetSafeSelectedValue(SubmissionStatusReasonDropDownList1);
        s.SubmissionStatusReasonOther = txSubmissionStatusReasonOther.Text;
        s.SubmissionTypeId = Common.GetSafeSelectedValue(SubmissionTypeDropDownList1);
        try
        { s.EffectiveDate = Convert.ToDateTime(txEffectiveDate.Text); }
        catch (Exception)
        { s.EffectiveDate = DateTime.MinValue; }
        try
        { s.ExpirationDate = Convert.ToDateTime(txExpirationDate.Text); }
        catch (Exception)
        { s.ExpirationDate = DateTime.MinValue; }
        try
        { s.SubmissionStatusDate = Convert.ToDateTime(txSubmissionStatusDate.Text); }
        catch (Exception)
        { s.SubmissionStatusDate = DateTime.MinValue; }
        s.AgencyId = Common.GetSafeSelectedValue(AgencyDropDownList1);
        s.AgentId = Common.GetSafeSelectedValue(AgentDropDownList1);
        s.ContactId = Common.GetSafeSelectedValue(ContactDropDownList1);

        #region Line of Businesses
        ArrayList lobids = new ArrayList();
        string[] slobids = Common.GetSelectedValues(LineOfBusinessDropDownList1);
        foreach (string sl in slobids)
        {
            BCS.Core.Clearance.BizObjects.LineOfBusiness lob = new BCS.Core.Clearance.BizObjects.LineOfBusiness();
            lob.Id = Convert.ToInt32(sl);
            lobids.Add(lob);
        }
        s.LineOfBusinessList = new BCS.Core.Clearance.BizObjects.LineOfBusiness[lobids.Count];
        lobids.CopyTo(s.LineOfBusinessList);
        #endregion

        #region Company Number Codes
        ArrayList alids = new ArrayList();
        if (phCodeTypes.Controls.Count > 0)
        {
            if (phCodeTypes.Controls[0] is HtmlTable)
            {
                HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                foreach (HtmlTableRow row in table.Rows)
                {
                    foreach (HtmlTableCell cell in row.Cells)
                    {
                        foreach (Control c in cell.Controls)
                        {
                            if (c is DropDownList)
                            {
                                DropDownList ddl = c as DropDownList;
                                if (ddl.SelectedIndex > 0)
                                {
                                    alids.Add(Common.GetSafeSelectedValue(ddl));
                                }
                            }
                        }
                    }
                }
            }
        }
        string[] arrStr = LOBList1.GetCodeTypeValues();
        int[] arrInt = new int[arrStr.Length];
        arrInt = Array.ConvertAll<string, int>(arrStr, new Converter<string, int>(Convert.ToInt32));
        alids.AddRange(arrInt);
        s.SubmissionCodeTypeIds = new int[alids.Count];
        alids.CopyTo(s.SubmissionCodeTypeIds);
        #endregion

        #region Attributes
        alids = new ArrayList();
        if (phAttributes.Controls.Count > 0)
        {
            if (phAttributes.Controls[0] is HtmlTable)
            {
                HtmlTable table = phAttributes.Controls[0] as HtmlTable;
                foreach (HtmlTableRow row in table.Rows)
                {
                    foreach (HtmlTableCell cell in row.Cells)
                    {
                        foreach (Control c in cell.Controls)
                        {
                            if (c is WebControl)
                            {
                                Type t = c.GetType();
                                // Gets the attributes for the collection.
                                System.ComponentModel.AttributeCollection attrs = System.ComponentModel.TypeDescriptor.GetAttributes(c);

                                /* Prints the name of the default property by retrieving the 
                                 * DefaultPropertyAttribute from the AttributeCollection. */
                                ControlValuePropertyAttribute myAttribute =
                                   (ControlValuePropertyAttribute)attrs[typeof(ControlValuePropertyAttribute)];

                                System.Reflection.PropertyInfo pi = t.GetProperty(myAttribute.Name);
                                object value = pi.GetValue(c, null);

                                BCS.Core.Clearance.BizObjects.SubmissionAttribute sattr = new BCS.Core.Clearance.BizObjects.SubmissionAttribute();
                                sattr.AttributeValue = value.ToString();
                                sattr.CompanyNumberAttributeId = Convert.ToInt32(c.ID);
                                alids.Add(sattr);
                            }
                        }
                    }
                }
            }
        }
        s.AttributeList = new BCS.Core.Clearance.BizObjects.SubmissionAttribute[alids.Count];
        alids.CopyTo(s.AttributeList);
        #endregion

        if (!excludeLOBChildren)
            s.LineOfBusinessChildList = LOBList1.GetItems(false);


        s.ClientCoreAddressIds = GetSafeSelectedClientAddressIdsAsIntArray();
        s.ClientCoreNameIds = GetSafeSelectedClientNameIdsAsIntArray();

        s.UnderwriterId = Common.GetSafeSelectedValue(UnderwritersDropDownList1);
        s.AnalystId = Common.GetSafeSelectedValue(AnalystDropDownList1);
        s.PolicySystemId = Common.GetSafeSelectedValue(PolicySystemDropDownList1);
        s.PolicyNumber = txPolicyNumber.Text;
        s.Dba = txDba.Text;
        s.SubmissionStatusReasonOther = txSubmissionStatusReasonOther.Text;
        if (txInsuredName.Visible)
            s.InsuredName = txInsuredName.Text;
        try
        {
            s.EstimatedWrittenPremium = Decimal.Parse(
              txEstimatedPremium.Text, System.Globalization.NumberStyles.Currency,
              System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat);
        }
        catch (Exception)
        { s.EstimatedWrittenPremium = decimal.Zero; }
        try
        {
            s.OtherCarrierPremium = Decimal.Parse(
              txOtherCarrierPremium.Text, System.Globalization.NumberStyles.Currency,
              System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat);
        }
        catch (Exception)
        { s.OtherCarrierPremium = decimal.Zero; }
        s.Comments = txComments.Text;

        string[] reasons = Common.GetSelectedValues(cblAgentUnspecifiedReasons);
        s.AgentUnspecifiedReasonList = new BCS.Core.Clearance.BizObjects.SubmissionAgentUnspecifiedReason[reasons.Length];
        for (int i = 0; i < reasons.Length; i++)
        {
            s.AgentUnspecifiedReasonList[i] = new BCS.Core.Clearance.BizObjects.SubmissionAgentUnspecifiedReason();
            s.AgentUnspecifiedReasonList[i].CompanyNumberAgentUnspecifiedReasonId = Convert.ToInt32(reasons[i]);
        }

        Session["temporarySubmission"] = s;
    }

    private int[] GetSafeSelectedClientNameIdsAsIntArray()
    {
        try
        {
            string[] selectedValues = Common.GetSelectedValues(cblNames);
            int[] intSelectedValues = Array.ConvertAll<string, int>(selectedValues, new Converter<string, int>(Convert.ToInt32));
            return intSelectedValues;
        }
        catch (Exception)
        {
            return new int[0];
        }
    }

    private string GetSafeSelectedClientNameIds()
    {
        try
        {
            string[] selectedValues = Common.GetSelectedValues(cblNames);
            return string.Join("|", selectedValues);
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    private void SetSafeSelectedClientNameIds(int[] nameIds)
    {
        try
        {
            cblNames.ClearSelection();
            foreach (ListItem item in cblNames.Items)
            {
                foreach (int nameId in nameIds)
                {
                    if (item.Value.Split("-".ToCharArray())[0] == nameId.ToString())
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        {
            rblAddresses.Items[0].Selected = true;
        }
    }

    private string GetSafeSelectedClientAddressIds()
    {
        try
        {
            string[] selectedValues = Common.GetSelectedValues(rblAddresses);
            List<string> listSelectedValues = new List<string>();
            foreach (string var in selectedValues)
            {
                listSelectedValues.Add(var.Split("-".ToCharArray())[0]);
            }
            selectedValues = new string[listSelectedValues.Count];
            listSelectedValues.CopyTo(selectedValues);
            return string.Join("|", selectedValues);
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    private int[] GetSafeSelectedClientAddressIdsAsIntArray()
    {
        try
        {
            string[] selectedValues = Common.GetSelectedValues(rblAddresses);
            List<int> listSelectedValues = new List<int>();
            foreach (string var in selectedValues)
            {
                listSelectedValues.Add(Convert.ToInt32(var.Split("-".ToCharArray())[0]));
            }
            int[] selectedAddressIds = new int[listSelectedValues.Count];
            listSelectedValues.CopyTo(selectedAddressIds);
            return selectedAddressIds;
        }
        catch (Exception)
        {
            return new int[0];
        }
    }

    private string[] GetSafeSelectedClientAddressStates()
    {
        try
        {
            string[] selectedValues = Common.GetSelectedValues(rblAddresses);
            List<string> listSelectedValues = new List<string>();
            foreach (string var in selectedValues)
            {
                listSelectedValues.Add(var.Split("-".ToCharArray())[1]);
            }
            string[] selectedAddressStates = new string[listSelectedValues.Count];
            listSelectedValues.CopyTo(selectedAddressStates);
            return selectedAddressStates;
        }
        catch (Exception)
        {
            return new string[0];
        }
    }

    private void SetSafeSelectedClientAddressIds(int[] addressIds)
    {
        try
        {
            rblAddresses.ClearSelection();
            foreach (ListItem item in rblAddresses.Items)
            {
                foreach (int addressId in addressIds)
                {
                    if (item.Value.Split("-".ToCharArray())[0] == addressId.ToString())
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
        }
        catch (Exception)
        {
            rblAddresses.Items[0].Selected = true;
        }
    }

    private void SetSafeSelectedValue(ListControl ddl, ValueType selectedValue, string key, string message)
    {
        if (ddl.Items.FindByValue(selectedValue.ToString()) != null)
        {
            ddl.ClearSelection();
            ddl.Items.FindByValue(selectedValue.ToString()).Selected = true;
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), key, string.Format("alert('{0}');", message), true);
        }
    }

    private StateCollection getAllStates()
    {
        if (Page.Cache["States"] == null)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);
            StateCollection sc = dataManager.GetStateCollection();
            sc = sc.SortByAbbreviation(OrmLib.SortDirection.Ascending);
            Page.Cache["States"] = sc;
            return sc;
        }
        return Page.Cache["States"] as StateCollection;
    }
    private DateTime GetEffectiveDate(DateTime defaultValue)
    {
        if (cbTBD.Checked)
        {
            return Common.GetSafeDateTime(lblSubmissionDate.Text, defaultValue);
        }
        else
        {
            return Common.GetSafeDateTime(txEffectiveDate.Text, defaultValue);
        }
    }

    public void SetInitialFocus()
    {
        string controlId = string.Empty;
        if (this._operation == "add")
            controlId = ConfigValues.GetAddInitialFocusField(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        else
            controlId = ConfigValues.GetEditInitialFocusField(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        if (!string.IsNullOrEmpty(controlId))
        {
            Control contentControl = Master.FindControl("ContentPlaceHolder1");
            Control focusControl = contentControl.FindControl(controlId);
            if (focusControl != null)
                ScriptManager1.SetFocus(focusControl);
        }
    }

    // TODO
    /// <summary>
    /// script sets focus in order of controls provided, if any control cannot be focused next in order gets the focus (seems lacking
    /// with scriptmanager)
    /// </summary>
    /// <param name="ctrls"></param>
    [Obsolete("to be removed now that focus is driven by config, null indicates dont need focus")]    
    public void SetInitialFocuses(List<Control> ctrls)
    {
        StringBuilder sb = new StringBuilder("<script type='text/javascript'>");

        sb.Append("var lastFocusedControl;");
        sb.Append("function setInitialFocus()");
        sb.Append("{");
        foreach(Control ctrl in ctrls)
        {
            string openBrace = "{";
            string closeBrace = "}";
            sb.AppendFormat(
                "try {0} document.{2}['{3}'].focus(); lastFocusedControl = '{3}'; {1} catch(err) {0} if (lastFocusedControl != null) {0}document.{2}[lastFocusedControl].focus();{1} {1};",
                openBrace, closeBrace, Form.ClientID, ctrl.UniqueID);
        }
        sb.Append("}");
        sb.Append("window.onload = SetInitialFocus;");
        sb.Append("</script>");

        Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "InitialFocus", sb.ToString());        
    }
    private void SetOperation(string _operation)
    {
        bool SupportsMultipleLineOfBusinesses = DefaultValues.SupportsMultipleLineOfBusinesses(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
       
        // Based on the operation
        switch (this._operation.ToLower())
        {
            case "add":
                {
                    this.pnlAddSubmission.Visible = true;
                    lbInsuredName.Visible = false;
                    txInsuredName.Visible = false;

                    btnSelectClient.Visible = false;
                    btnSwitchClient.Visible = false;

                    submission_row_underwriter_previous.Style.Add(HtmlTextWriterStyle.Display, "none");

                    UnderwritersDropDownList1.RetirementDate = DateTime.Today;
                    AnalystDropDownList1.RetirementDate = DateTime.Today;
                                        
                    if (!Page.IsPostBack && SupportsMultipleLineOfBusinesses)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            LOBList1.AddItem();
                        }
                    }
                    txPolicyNumber.ReadOnly = false;
                    #region Set Disabled based on company config
                    string[] addDisabledFields = ConfigValues.GetAddDisabledFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                    foreach (string var in addDisabledFields)
                    {
                        // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
                        // as parameter to FindControlRecursive method
                        Control c = Common.FindControlRecursive(pnlAddSubmission, var.Trim());
                        if (c != null)
                        {
                            if (c is WebControl)
                            {
                                WebControl wc = c as WebControl;
                                wc.Enabled = false;
                            }
                            if (c is UserControl)
                            {
                                PropertyInfo pi = c.GetType().GetProperty("Enabled");
                                if (pi != null)
                                    pi.SetValue(c, false, null);
                            }
                        }
                    }
                    #endregion
                    #region Set Invisible based on company config
                    string[] addInvisibleFields = ConfigValues.GetAddInvisibleFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                    foreach (string var in addInvisibleFields)
                    {
                        // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
                        // as parameter to FindControlRecursive method
                        Control c = Common.FindControlRecursive(pnlAddSubmission, var.Trim());
                        if (c != null)
                        {
                            if (c is WebControl)
                            {
                                WebControl wc = c as WebControl;
                                wc.Enabled = false;
                            }
                            if (c is UserControl)
                            {
                                PropertyInfo pi = c.GetType().GetProperty("Visible");
                                if (pi != null)
                                    pi.SetValue(c, false, null);
                            }
                        }
                    }
                    #endregion
                    break;
                }
            case "edit":
                {
                    this.btnSubmitSubmission.CommandName = "Edit";
                    this.btnSubmitSubmission.Text = "Update Submission";

                    btnSelectClient.Visible = true;
                    btnSwitchClient.Visible = true;

                    SubmissionStatusDropDownList1.Enabled = !SupportsMultipleLineOfBusinesses;
                    txPolicyNumber.ReadOnly = ConfigValues.GenerateOnlyPolicyNumber(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                    #region Set Disabled based on company config
                    string[] editDisabledFields = ConfigValues.GetEditDisabledFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                    foreach (string var in editDisabledFields)
                    {
                        // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
                        // as parameter to FindControlRecursive method
                        Control c = Common.FindControlRecursive(pnlAddSubmission, var.Trim());
                        if (c != null)
                        {
                            if (c is WebControl)
                            {
                                WebControl wc = c as WebControl;
                                wc.Enabled = false;
                            }
                            if (c is UserControl)
                            {
                                PropertyInfo pi = c.GetType().GetProperty("Enabled");
                                if (pi != null)
                                    pi.SetValue(c, false, null);
                            }
                        }
                    } 
                    #endregion
                    #region Set Invisible based on company config
                    string[] editInvisibleFields = ConfigValues.GetEditInvisibleFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                    foreach (string var in editInvisibleFields)
                    {
                        // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
                        // as parameter to FindControlRecursive method
                        Control c = Common.FindControlRecursive(pnlAddSubmission, var.Trim());
                        if (c != null)
                        {
                            if (c is WebControl)
                            {
                                WebControl wc = c as WebControl;
                                wc.Enabled = false;
                            }
                            if (c is UserControl)
                            {
                                PropertyInfo pi = c.GetType().GetProperty("Visible");
                                if (pi != null)
                                    pi.SetValue(c, false, null);
                            }
                        }
                    }
                    #endregion
                }
                break;
            default:
                {
                    throw new System.NotSupportedException("Invalid operation requested.");
                    //break;
                }
        }
    }

    #endregion
}
