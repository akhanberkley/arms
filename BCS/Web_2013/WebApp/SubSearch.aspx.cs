using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using BCS.WebApp.UserControls;
using BCS.WebApp.Components;

public partial class SubSearch : System.Web.UI.Page
{    
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            txSubBo.Focus();
        }

        Master.AuthorizationCheck();

        #region When hit directly via a url
        string snu = string.Empty;
        string seq = string.Empty;
        string cnu = string.Empty;
        if (CheckSubmissionParameters(ref snu, ref seq, ref cnu))
        {
            SubmissionProxy.Submission sproxy = Common.GetSubmissionProxy();
            string subXml = sproxy.GetSubmissionBySubmissionNumberAndCompanyNumber(
                snu.ToString(), cnu.ToString(), seq.ToString());
            if (subXml == null)
            {
                ShowMessage("No submission exists with the parameters supplied.");
                return;
            }
            BCS.Core.Clearance.BizObjects.Submission sl = (BCS.Core.Clearance.BizObjects.Submission)
                        BCS.Core.XML.Deserializer.Deserialize(
                        subXml, typeof(BCS.Core.Clearance.BizObjects.Submission));
            
            //check if user is remote agency and has access to this submission
            BCS.Core.Security.CustomPrincipal user = User as BCS.Core.Security.CustomPrincipal;
            // non-aps agency ids are int
            int agencyId = user.HasAgency() ? Convert.ToInt32(BCS.WebApp.Components.Security.GetMasterAgency(user.Agency)) : -1;
            if (user.HasAgency() && (sl.Agency.MasterAgencyId != agencyId && sl.AgencyId != agencyId))
            {
                ShowMessage("No submission exists with the parameters supplied.");
                return;
            }

            Session["SubmissionId"] = sl.Id;
            Session["subref"] = "SubSearch.aspx";
            if (DefaultValues.UseWizard)
                Response.Redirect("SubmissionWizard.aspx");
            Response.Redirect("Submission.aspx?op=edit");
        } 
        #endregion
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        if (null != Session["LoadSavedSearch"])
        {
            Session.Contents.Remove("LoadSavedSearch");
            if (null != Session[SessionKeys.SearchedCriteria])
            {
                SearchCriteria backToSearch = (SearchCriteria)Session[SessionKeys.SearchedCriteria];
                if (backToSearch.SubmissionNumber != null)
                {
                    txSubBo.Text = backToSearch.SubmissionNumber.SubmissionNumber;
                    txSeq.Text = backToSearch.SubmissionNumber.Sequence;

                    //AccordionCtrl.SelectedIndex = 0;
                }
                if (!string.IsNullOrEmpty(backToSearch.PolicyNumber))
                {
                    txPol.Text = backToSearch.PolicyNumber;

                    //AccordionCtrl.SelectedIndex = 1;
                }
                Session.Contents.Remove(SessionKeys.SearchedCriteria);
            }
        }
        base.OnLoadComplete(e);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (!IsValid)
            return;
        int companyId = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
        int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumber);
        string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);

        if (companyId == 0 || companyNumberId == 0)
        {
            ShowMessage("Please select a company number.");
            return;
        }

        SubmissionProxy.Submission sproxy = Common.GetSubmissionProxy();
        string subXml = sproxy.GetSubmissionBySubmissionNumberAndCompanyNumber(
            txSubBo.Text, companyNumber, txSeq.Text);
        if (subXml == null)
        {
            ShowMessage("No submission exists with the submission number entered.");
            return;
        }
        BCS.Core.Clearance.BizObjects.Submission sl = (BCS.Core.Clearance.BizObjects.Submission)
                    BCS.Core.XML.Deserializer.Deserialize(
                    subXml, typeof(BCS.Core.Clearance.BizObjects.Submission));
        
        //check if user is remote agency and has access to this submission
        BCS.Core.Security.CustomPrincipal user = User as BCS.Core.Security.CustomPrincipal;
        int agencyId = user.HasAgency() ? Convert.ToInt32(BCS.WebApp.Components.Security.GetMasterAgency(user.Agency)) : -1;
        if (user.HasAgency() && (sl.Agency.MasterAgencyId != agencyId && sl.AgencyId != agencyId))
        {
            ShowMessage("No submission exists with the submission number entered.");
            return;
        }

        SearchCriteria sc = new SearchCriteria();
        sc.SubmissionNumber = new SearchSubmissionNumber();
        sc.SubmissionNumber.SubmissionNumber = txSubBo.Text;
        sc.SubmissionNumber.Sequence = txSeq.Text;
        Session[SessionKeys.SearchedCriteria] = sc;

        Session["SubmissionId"] = sl.Id;
        Session["subref"] = "SubSearch.aspx";
        if (DefaultValues.UseWizard)
            Response.Redirect("SubmissionWizard.aspx");
        Response.Redirect("Submission.aspx?op=edit");

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        if (!IsValid)
            return;
        int companyId = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
        int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumber);
        string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);

        if (companyId == 0 || companyNumberId == 0)
        {
            ShowMessage("Please select a company number.");
            return;
        }

        SubmissionProxy.Submission sproxy = Common.GetSubmissionProxy();
        string subXml = sproxy.GetSubmissionsByPolicyNumberAndCompanyNumber(txPol.Text, companyNumber);
       
        BCS.Core.Clearance.BizObjects.SubmissionList slist = (BCS.Core.Clearance.BizObjects.SubmissionList)
                    BCS.Core.XML.Deserializer.Deserialize(
                    subXml, typeof(BCS.Core.Clearance.BizObjects.SubmissionList));


        if (slist.List.Length == 0)
        {
            ShowMessage("No submission exists with the policy number entered.");
            return;
        }
        BCS.Core.Clearance.BizObjects.Submission sl = slist.List[0];

        //check if user is remote agency and has access to this submission
        BCS.Core.Security.CustomPrincipal user = User as BCS.Core.Security.CustomPrincipal;
        int agencyId = user.HasAgency() ? Convert.ToInt32(BCS.WebApp.Components.Security.GetMasterAgency(user.Agency)) : -1;
        if (user.HasAgency() && (sl.Agency.MasterAgencyId != agencyId && sl.AgencyId != agencyId))
        {
            ShowMessage("No submission exists with the policy number entered.");
            return;
        }

        if (slist.List.Length > 1)
        {
            if (string.IsNullOrEmpty(txPolSeq.Text))
            {
                int[] allSequences = new int[slist.List.Length];
                for (int i = 0; i < slist.List.Length; i++)
                {
                    allSequences[i] = slist.List[i].SubmissionNumberSequence;
                }
                Array.Sort(allSequences);
                Array.Reverse(allSequences);

                if (allSequences[0] == allSequences[1])
                {
                    ShowMessage("Multiple submissions were found with the policy number entered.");
                    return;
                }
                else
                {
                    foreach (BCS.Core.Clearance.BizObjects.Submission var in slist.List)
                    {
                        if (var.SubmissionNumberSequence == allSequences[0])
                        {
                            sl = var;
                            break;
                        }
                    }
                }
            }
            else
            {
                bool found = false;
                foreach (BCS.Core.Clearance.BizObjects.Submission var in slist.List)
                {
                    if (var.SubmissionNumberSequence.ToString() == txPolSeq.Text)
                    {
                        sl = var;
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    ShowMessage("No submission exists with the policy number entered.");
                    return;
                }
            }
        }
        SearchCriteria sc = new SearchCriteria();
        sc.PolicyNumber = txPol.Text;
        Session[SessionKeys.SearchedCriteria] = sc;

        Session["SubmissionId"] = sl.Id;
        Session["subref"] = "SubSearch.aspx";
        if (DefaultValues.UseWizard)
            Response.Redirect("SubmissionWizard.aspx");
        Response.Redirect("Submission.aspx?op=edit");

    }

    private void ShowMessage(string message)
    {
        ValidationTable.Style.Add("display", "");
        Common.SetStatus(messagePlaceHolder, message);
    }
    private bool CheckSubmissionParameters(ref string snu, ref string seq, ref string cnu)
    {
        if (Request.QueryString.Count == 0)
            return false;
        snu = GetNullSafeQSParamAsInt64("s").ToString();
        seq = GetNullSafeQSParamAsInt64("q").ToString();
        cnu = GetNullSafeQSParamAsInt64("c").ToString();
        if (seq == "0")
            seq = string.Empty;
        return true;          
    }

    private long GetNullSafeQSParamAsInt64(string name)
    {
        long o = 0;
        Int64.TryParse(Request.QueryString[name],out o);
        return o;
    }
    protected void ValidationButton_Init(object sender, EventArgs e)
    {
        Button btn = sender as Button;
        btn.Attributes.Add("onclick", string.Format("if(!Page_ClientValidate('{0}')) {{  $find('alwaysVisible').get_element().style.display = ''; $find('alwaysVisible').initialize(); }}", btn.ValidationGroup));
    }
}
