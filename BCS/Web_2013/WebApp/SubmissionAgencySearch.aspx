<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SubmissionAgencySearch.aspx.cs" Inherits="SubmissionAgencySearch" Theme="DefaultTheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Berkley Clearance System - Submission Agency Search</title>
    <link href="Styles/pats_style.css" rel="stylesheet" type="text/css" />
    <script src="Javascripts/prototype.js" type="text/javascript"></script>
    <script type="text/javascript">
        function selectAgency(){
            if ($("ParentAgencyControlId").value != ""){
                if ($("SelectedAgencyId").value != ""){                    
                    var parentAgency = opener.document.getElementById($("ParentAgencyControlId").value)
                    
                    if (parentAgency){
                        for (var i=0; i < parentAgency.length; i++){
                            if (parentAgency[i].value == $('SelectedAgencyId').value) {
                                parentAgency[i].selected = true;
                            }
                        }
                        opener.document.forms[0].submit();
                        self.close();
                    }
                }else{
                    alert("Please select an agency.");
                }
            }
            return false;
        }        
    </script>
</head>
<body class="pageBody">
    <form id="form1" runat="server">
    <asp:HiddenField ID="ParentAgencyControlId" runat="server" />
    <asp:HiddenField ID="SelectedAgencyId" runat="server" />
    <aje:ScriptManager id="ScriptManager1" runat="server" OnAsyncPostBackError="ScriptManager1_AsyncPostBackError"></aje:ScriptManager>
    <span class="heading">Search for Agency</span>
    <table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">&nbsp;</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">            
            <table>
            <tr>
                <td>Agency Number</td>
                <td>
                    <asp:TextBox ID="txFilterAgencyNumber" runat="server"></asp:TextBox>
                </td>
                <td>City</td>
                <td>
                    <asp:TextBox ID="txFilterCity" runat="server"></asp:TextBox>
                </td> 
                <td>State</td>
                <td>
                    <asp:TextBox ID="txFilterState" runat="server"></asp:TextBox>
                </td>                
                <td>
                    <asp:Button ID="btnFilter" runat="server" Text="Filter"  OnClick="btnFilter_Click" CausesValidation="false" />
                </td>
            </tr>
        </table>

    &nbsp;    
    <br />
    <br />
    <UserControls:Grid ID="gridAgencies" runat="server" AllowPaging="True" EmptyDataText="No Agencies were found." AutoGenerateColumns="False" DataKeyNames="Id" CaptionAlign="Left" DataSourceID="odsAgencies" OnRowDataBound="gridAgencies_RowDataBound">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <itemtemplate>
                    <asp:ImageButton runat="server" Text="Select" CommandName="Select" ImageUrl='<%$ Code : Common.GetImageUrl("load_button.gif") %>' CausesValidation="False" id="btnSelectAgency"></asp:ImageButton>
                
</itemtemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />            
            <asp:BoundField DataField="AgencyName" HeaderText="Agency Name" />
            <asp:BoundField DataField="AgencyNumber" HeaderText="Agency Number" />
            <asp:BoundField DataField="ContactPerson" HeaderText="Contact Person" />
            <asp:BoundField DataField="City" HeaderText="City" />
            <asp:BoundField DataField="State" HeaderText="State" />
            <asp:BoundField DataField="FEIN" HeaderText="FEIN" />            
        </Columns>
        <PagerTemplate>
            <asp:PlaceHolder runat="server"></asp:PlaceHolder>
        </PagerTemplate>
    </UserControls:Grid>
            <asp:ObjectDataSource ID="odsAgencies" runat="server" SelectMethod="GetAgenciesByNameNumberCityStateAnd"
                TypeName="BCS.Core.Clearance.Admin.DataAccess" OnInit="odsAgencies_Init">
                <SelectParameters>
                    <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected"
                        Type="Int32" />
                    <asp:QueryStringParameter DefaultValue="DateTime.Min" Name="date" QueryStringField="dt"
                        Type="DateTime" />
                    <asp:Parameter Name="agencyName" Type="String" DefaultValue="" />
                    <asp:ControlParameter ControlID="txFilterAgencyNumber" Name="agencyNumber" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="txFilterCity" Name="city" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txFilterState" Name="state" PropertyName="Text"
                        Type="String" />
                    <asp:Parameter Name="masterAgencyId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
    
            </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
    </form>
</body>
</html>
