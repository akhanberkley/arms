<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="ReSeq.aspx.cs" Inherits="ReSeq" Title="Berkley Clearance System : Re-Sequence Submission" Theme="DefaultTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:PlaceHolder runat="server" ID="messagePlaceHolder">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
    <br />
</asp:PlaceHolder>
<table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Re-Sequence Submission</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        <%--content--%>
        <table cellpadding="0" cellspacing="0" >            
            <tr>
                <td>
                    <asp:TextBox id="txNewSubBo" runat="server" ToolTip="Empty will just reassign sequence."></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" Text="* Invalid Submission Number" ErrorMessage="&nbsp;" 
                        SetFocusOnError="True" Display="Dynamic" ControlToValidate="txNewSubBo" MinimumValue="0" MaximumValue='<%$ Code: Int64.MaxValue %>' Type="Double"></asp:RangeValidator>                     
                    <img id="img2dd" runat="server" src='<%$ Code : Common.GetImageUrl("question.gif") %>' alt="Hover for info." title="Empty will generate a new submission number and sequence." />                    
                </td>
                <td>:</td>
                <td>
                    <asp:TextBox ID="txNewSeq" runat="server" Width="40px"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" Text="* Invalid Sequence" ErrorMessage="&nbsp;" 
                        SetFocusOnError="True" Display="Dynamic" ControlToValidate="txNewSeq" MinimumValue="0" MaximumValue='<%$ Code: Int32.MaxValue %>' Type="Integer"></asp:RangeValidator>
                    <img id="img2ddd" runat="server" src='<%$ Code : Common.GetImageUrl("question.gif") %>' alt="Hover for info." title="Optional, Highest sequence will be assigned." />
                </td>
            </tr>
        </table>
        <p><asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
        &nbsp;
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" /></p>
        <%--end content--%>
        </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
</asp:Content>

