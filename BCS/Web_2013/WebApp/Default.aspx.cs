#region Using
using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.IO;

using OrmLib;
using BCS.Biz;
using BCS.WebApp.Components;
using BCS.WebApp.UserControls;
using BCS.WebApp;

using Structures = BTS.ClientCore.Wrapper.Structures;
using Templates = BCS.WebApp.Templates;
using BCS.Core.Security;
using AjaxControlToolkit;
using System.Xml;
using System.Collections.Generic;
using System.Web.Services.Protocols;
using BTS.LogFramework;
using BCS.Core;
#endregion


namespace BCS.WebApp
{
    /// <summary>
    /// Summary description for _Default.
    /// </summary>
    public partial class _Default : System.Web.UI.Page
    {
        private const string key4Searched = "Searched";
        private const string key4PreviousSortExpression = "PreviousSortExpression";
        private const string key4PreviousSortDirection = "PreviousSortDirection";
        private const string key4DefaultSort = "DefaultSort";
        #region Page Events

        protected override void OnInit(EventArgs e)
        {
            if (!IsPostBack)
            {
                Common.Forward();
            }
            base.OnInit(e);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (Session["IsExternalUser"] != null)
                Response.Redirect("ExternalSearch.aspx");
            Common.SetStatus(messagePlaceHolder);

            if (!IsPostBack)
            {
                if (Request.QueryString["clearsearch"] != null)
                    Session.Remove("LoadSavedSearch");
            }
            TextBox txt = (TextBox)ClientSearch1.FindControl("txtPhoneNum");
            //txt.Attributes.Add("onKeyUp", "callScript()");
            //btnSearch.Attributes.Add("onClick", "enableElements()");

            if (!IsPostBack)
            {
             
                if (Request.QueryString["clientid"] != null)
                {
                    Session["ClientId"] = Request.QueryString["clientid"];
                    string clientid = Request.QueryString["clientid"];
                    ClientProxy.Client clientproxy = Common.GetClientProxy();

                    Structures.Info.ClientInfo client;
                    Dictionary<string, Structures.Search.Client> clientDictionary = new Dictionary<string, Structures.Search.Client>();
                    Core.Clearance.BizObjects.SubmissionList list = new BCS.Core.Clearance.BizObjects.SubmissionList();
                    string currentCompanyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);

                    if (!string.IsNullOrEmpty(clientid))
                    {
                        try
                        {
                            string clientxml = clientproxy.GetClientByIdForSearchScreen(currentCompanyNumber, clientid, true);

                            Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                                (Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                                clientxml, typeof(Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

                            client = clearanceclient.ClientCoreClient;

                            Structures.Search.Client c = new BTS.ClientCore.Wrapper.Structures.Search.Client();
                            c.Info.ClientCategory = client.Client.ClientCategory;
                            c.Info.ClientId = client.Client.ClientId;
                            c.Info.ClientStatus = client.Client.ClientStatus;
                            c.Info.ClientType = client.Client.ClientType;
                            c.Info.CorpId = client.Client.CorpId;
                            c.Info.Segmented = client.Client.SegmentedClient;
                            c.Info.TaxId = client.Client.TaxId;
                            c.Info.TaxIdType = client.Client.TaxIdType;

                            if (client.Addresses != null)
                            {
                                c.Addresses = new Structures.Search.AddressVersion[client.Addresses.Length];

                                int i = 0;
                                foreach (Structures.Info.Address address in client.Addresses)
                                {
                                    c.Addresses[i].Address1 = address.Address1;
                                    c.Addresses[i].Address2 = address.Address2;
                                    c.Addresses[i].Address3 = address.Address3;
                                    c.Addresses[i].Address4 = address.Address4;
                                    c.Addresses[i].AddressId = address.AddressId;
                                    c.Addresses[i].City = address.City;
                                    c.Addresses[i].CountryId = address.CountryId;
                                    c.Addresses[i].County = address.County;
                                    c.Addresses[i].CountyId = address.CountyId;
                                    c.Addresses[i].HouseNumber = address.HouseNumber;
                                    c.Addresses[i].PostalCode = address.PostalCode;
                                    c.Addresses[i].StateProvCode = address.StateProvinceId;
                                    c.Addresses[i].StateProvId = address.StateProvinceId;
                                    i++;
                                }
                            }

                            if (client.Client.Names != null)
                            {
                                c.Names = new BTS.ClientCore.Wrapper.Structures.Search.ClientName[client.Client.Names.Length];

                                int i = 0;
                                foreach (Structures.Info.Name name in client.Client.Names)
                                {
                                    c.Names[i].ClientId = name.ClientId;
                                    c.Names[i].NameBusiness = name.NameBusiness;
                                    c.Names[i].NameFirst = name.NameFirst;
                                    c.Names[i].NameLast = name.NameLast;
                                    c.Names[i].NameMiddle = name.NameMiddle;
                                    c.Names[i].NamePrefix = name.NamePrefix;
                                    c.Names[i].NameSuffix = name.NameSuffix;
                                    c.Names[i].NameType = name.NameType;
                                    c.Names[i].PrimaryNameFlag = name.PrimaryNameFlag;
                                    c.Names[i].SequenceNumber = name.SequenceNumber;
                                    i++;
                                }
                            }
                            c.Portfolio = client.Client.Portfolio;

                            clientDictionary.Add(c.Info.ClientId, c);
                            LoadSearchResults(clientDictionary);

                        }
                        catch (System.Net.WebException wex)
                        {
                            btnEditClient.Visible = false;
                            btnCreateShellSubmission.Visible = false;
                            phSubmissions.Visible = false;
                            phPortfolio.Visible = false;

                            switch (wex.Status)
                            {
                                case System.Net.WebExceptionStatus.ConnectFailure:
                                    {
                                        Common.SetStatus(messagePlaceHolder, "Unable to connect to the ClientCORE. Please try again. Contact the system admin if the problem persists.");
                                        LogCentral.Current.LogFatal("Unable to connect to the ClientCORE supplied by the URL.", wex);
                                    }
                                    break;
                                case System.Net.WebExceptionStatus.Timeout:
                                    {
                                        Common.SetStatus(messagePlaceHolder, "The search has timed out. Please try again.");
                                        LogCentral.Current.LogWarn("The search is timed out.", wex);
                                    }
                                    break;
                                default:
                                    throw;
                            }
                        }
                        catch (Exception ex)
                        {
                            LogCentral.Current.LogWarn(string.Format("The Client associated with the submission either does not exist or is inaccessible. Id = {0} CompanyNumberId = {1}", clientid, Session["CompanyNumberSelected"]), ex);
                            Common.SetStatus(messagePlaceHolder, "Requested client either does not exist or is inaccessible.");
                            btnEditClient.Visible = false;
                            btnCreateShellSubmission.Visible = false;
                            phSubmissions.Visible = false;
                            phPortfolio.Visible = false;
                            pnlSearchCriteria.Visible = true;
                        }

                        btnAddClient.Visible = true;
                        pnlSearchCriteria.Visible = false;
                        pnlSearchResults.Visible = true;
                        return;
                    }
                }
               
            }
        }
      


        protected void Page_PreRender(object sender, System.EventArgs e)
        {

            //if (hdnHideModalPopup.Value == "hide")
            //{
            //    mdlAdvClientSrchResults.Show();
            //    dvOverlay.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "block");
            //    pnlSearchResults.Visible = false;
            //    pnlAdvClientSrchResults.Style["display"] = "block";
            //}
            //else
            //{
            //    mdlAdvClientSrchResults.Hide();
            //    dvOverlay.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "none");
            //    pnlAdvClientSrchResults.Style["display"] = "none";
            //}

                         
          
        }
        protected override void OnLoadComplete(EventArgs e)
        {
            //if (ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
            //    Response.Redirect("APSSite/Default.aspx");

            if (Session[SessionKeys.CompanyInitialized] != null)
            {
                Session.Contents.Remove(SessionKeys.CompanyInitialized);
                Response.Redirect("Default.aspx");
            }
            if (Session["CompanyChanged"] != null)
            {
                Session.Contents.Remove("CompanyChanged");
                Session.Contents.Remove("LoadSavedSearch");
                pnlSearchCriteria.Visible = true;
                pnlSearchResults.Visible = false;
                lbAddress.Items.Clear();
                lbNames.Items.Clear();
                Session.Contents.Remove("Clients");
                Response.Redirect("Default.aspx");
            }

            if (null != Session["LoadSavedSearch"])
            {
                Session.Contents.Remove("LoadSavedSearch");
                if (null != Session["SavedSearch"])
                {
                    SavedSearch backToSearch = (SavedSearch)Session["SavedSearch"];
                    ClientSearch1.SearchCriteria = backToSearch;

                    btnSearch_Click(null, EventArgs.Empty);
                    if (null != lbNames.Items.FindByValue(backToSearch.SelectedClientId))
                    {
                        lbNames.ClearSelection();
                        lbNames.Items.FindByValue(backToSearch.SelectedClientId).Selected = true;
                    }
                    lbNames_SelectedIndexChanged(this.lbNames, EventArgs.Empty);
                    if (null != lbAddress.Items.FindByValue(backToSearch.SelectedClientAddressId))
                    {
                        lbAddress.ClearSelection();
                        lbAddress.Items.FindByValue(backToSearch.SelectedClientAddressId).Selected = true;
                    }
                    lbAddress_SelectedIndexChanged(this.lbAddress, EventArgs.Empty);
                }
            }

            Session.Contents.Remove("SavedSearch");
            
            base.OnLoadComplete(e);
        }
        #endregion

        #region Control Events
        protected void btnAddNewClient_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Client.aspx?op=add");
        }
        protected void btnClosePopup_Click(object sender, EventArgs e)
        {
            gvAdvClientSrchResults.DataSource = null;
            gvAdvClientSrchResults.DataBind();
            pnlAdvClientSrchResults.Style["display"] = "none";
            mdlAdvClientSrchResults.Hide();
            hdnHideModalPopup.Value = "";
            Session["advClientSearch"] = null;
        }

        protected void imgbtnRefresh_Click(object sender, EventArgs e)
        {
            gvAdvClientSrchResults.DataSource = null;
            gvAdvClientSrchResults.DataBind();
            ClientProxy.Client clientproxy = Common.GetClientProxy();
            string name = txtAdvSrchName.Text;
            string city = txtAdvSrchCity.Text;
            string state = txtAdvSrchState.Text;
            string currentCompanyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
            Structures.Search.Vector vector = (Structures.Search.Vector)BCS.Core.XML.Deserializer.Deserialize(
                                   clientproxy.GetClient(currentCompanyNumber,
                                   string.Empty, string.Empty, string.Empty, city, state, string.Empty, string.Empty, string.Empty,
                                    name), typeof(Structures.Search.Vector));

            List<BCS.Core.Clearance.BizObjects.ClientSearchResult> cList = new List<BCS.Core.Clearance.BizObjects.ClientSearchResult>();

            if (vector.Clients != null && vector.Clients.Length > 0)
            {
                foreach (Structures.Search.Client c in vector.Clients)
                {
                    if (!string.IsNullOrEmpty(c.Portfolio.Id))
                    {
                        BCS.Core.Clearance.BizObjects.ClientSearchResult rslt = new Core.Clearance.BizObjects.ClientSearchResult();
                        rslt.ClientId = Convert.ToInt32(c.Portfolio.Id);
                        rslt.Address = c.Addresses[0].Address1 + " " + c.Addresses[0].Address2 + "," + c.Addresses[0].City + "," + c.Addresses[0].StateProvCode;
                        rslt.Name = c.Names[0].NameBusiness;
                        cList.Add(rslt);
                    }
                }

                DataTable dt = new DataTable();
                dt.Columns.Add("ClientId", System.Type.GetType("System.String"));
                dt.Columns.Add("Name", System.Type.GetType("System.String"));
                dt.Columns.Add("Address", System.Type.GetType("System.String"));

                for (int i = 0; i < cList.Count; i++)
                {

                    DataRow dr = dt.NewRow();
                    dr["ClientId"] = cList[i].ClientId.ToString();
                    dr["Name"] = cList[i].Name;
                    dr["Address"] = cList[i].Address;

                    dt.Rows.Add(dr);
                }
                gvAdvClientSrchResults.DataSource = dt;
                gvAdvClientSrchResults.DataBind();


            }
            else
            {
                gvAdvClientSrchResults.DataSource = null;
                gvAdvClientSrchResults.DataBind();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int currentCompanyId = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
            int currentCompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
            string currentCompanyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
            bool usedPhoneLookup = false;
            if (currentCompanyId == 0)
            {
                Common.SetStatus(messagePlaceHolder, "Please select a company.");
                return;
            }
            if (currentCompanyNumberId == 0)
            {
                Common.SetStatus(messagePlaceHolder, "Please select a company number.");
                return;
            }

            int length = ClientSearch1.TaxIds.Length + ClientSearch1.PortfolioIds.Length + ClientSearch1.ClientId.Length +
                ClientSearch1.ClientNamesToSearch.Count + ClientSearch1.AddressVersionsToSearch.Count + ClientSearch1.PolicyNumber.Length + ClientSearch1.PhoneNumber.Length;

            if (length == 0)
            {
                string message = "Please enter a criteria to search.";

                BCS.Core.Security.CustomPrincipal user = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
                bool isRemoteAgency = user.HasAgency();
                if (isRemoteAgency)
                {
                    message += " All fields are required when searching by name.";
                }

                Common.SetStatus(messagePlaceHolder, message);
                return;
            }
            pnlSearchCriteria.Visible = false;
            pnlSearchResults.Visible = true;

            Dictionary<string, Structures.Search.Client> clientDictionary = new Dictionary<string, Structures.Search.Client>();
            Core.Clearance.BizObjects.SubmissionList list = new BCS.Core.Clearance.BizObjects.SubmissionList();

            try
            {
                ClientProxy.Client clientproxy = Common.GetClientProxy();

                Structures.Info.ClientInfo client;

                string clientid = ClientSearch1.ClientId;
                StringCollection searchMessages = new StringCollection();
                hdnAdvSrchState.Value = "";
              #region Reverse Phone Lookup

                string phoneNum = ClientSearch1.PhoneNumber.Replace("(","").Replace(")","").Replace("-","").Replace(" ","");

                if (!string.IsNullOrEmpty(phoneNum))
                {
                   
                    usedPhoneLookup = true;
                    AdvClientSearch.AdvClientSearchWebServicePublicService advProxy = Common.GetAdvClientSearch();
                    int companyId = Common.GetSafeIntFromSession(BCS.WebApp.Components.SessionKeys.CompanyId);
                    BCS.DAL.CompanyClientAdvancedSearch advSrchAttributes = BCS.Core.Clearance.SubmissionsEF.GetAdvSearchAttributesByCompany(companyId);

                    if (advSrchAttributes != null)
                    {
                        advProxy.Url = advSrchAttributes.ClientAdvSearchEndPointURL;
                        AdvClientSearch.Security cssec = new AdvClientSearch.Security();
                        cssec.dsik = advSrchAttributes.DSIK;
                    
                        AdvClientSearch.UsernameTokenType utok = new AdvClientSearch.UsernameTokenType();
                        AdvClientSearch.AttributedString attUsrName = new AdvClientSearch.AttributedString();
                        attUsrName.Value = advSrchAttributes.Username;
                        utok.Username = attUsrName;
                        cssec.UsernameToken = utok;
                        advProxy.RequestSecurity = cssec;
                                                
                     }

                    AdvClientSearch.AdvClientSearchRslt_Type result = advProxy.performClientSearchBasedOnPhoneNumber(phoneNum, "test");

                    if (result != null)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(result.ClienInfo.ClientID))
                            {
                                clientid = result.ClienInfo.ClientID;
                                goto ClientIDSearch;
                            }
                            else
                            {

                                // service did not return any Client ID , so let us go ahead and do the business name search to check if this client exists in CC
                                if (result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.NameInfo != null)
                                {
                                    string name = Common.formatString(result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.NameInfo[0].CommlName == null ? result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.NameInfo[0].PersonName.Surname.Value.ToString() : result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.NameInfo[0].CommlName.CommercialName.Value.ToString());
                                    name = CheckForNameFormating(name);
                                    string cityValue = Common.formatString(advSrchAttributes.PopulateCity == false ? string.Empty : result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].City.Value.ToString());
                                    string stateValue = advSrchAttributes.PopulateState == false ? string.Empty : result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].StateProv.Value.ToString();
                                    string addressValue = Common.formatString(result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].Addr1 == null ? string.Empty : result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].Addr1.Value.ToString());
                                    Structures.Search.Vector vector =
                                       (Structures.Search.Vector)BCS.Core.XML.Deserializer.Deserialize(
                                       clientproxy.GetClient(currentCompanyNumber,
                                       string.Empty, string.Empty, string.Empty, cityValue, stateValue, string.Empty, string.Empty, string.Empty,
                                        name), typeof(Structures.Search.Vector));

                                    if (vector.Clients != null && vector.Clients.Length > 0)
                                    {
                                        foreach (Structures.Search.Client c in vector.Clients)
                                            if (!clientDictionary.ContainsKey(c.Info.ClientId))
                                                clientDictionary.Add(c.Info.ClientId, c);

                                        Panel pnl = (Panel)ClientSearch1.FindControl("pnlShowSrchResults");
                                        pnl.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "block");
                                        Label lbl = (Label)ClientSearch1.FindControl("lblPhNumresults");
                                        lbl.Visible = true;
                                        TextBox txt = (TextBox)ClientSearch1.FindControl("txtPhoneNum");
                                        StringBuilder sb = new StringBuilder();
                                        sb.Append(txt.Text + "<br/>");
                                        sb.Append(CheckForNameFormating(name) + "<br/>");
                                        //sb.Append(Common.formatString((String.IsNullOrEmpty(result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].Addr1.Value.ToString()) ? string.Empty : result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].Addr1.Value.ToString())) + "<br/>");
                                        if (!string.IsNullOrEmpty(addressValue))
                                            sb.Append(Common.formatString(addressValue) + "<br/>");
                                        sb.Append(Common.formatString(result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].City.Value.ToString()) + ",");
                                        sb.Append(result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].StateProv.Value.ToString() + "<br/>");
                                        Label lbl2 = (Label)ClientSearch1.FindControl("lblSrchResults");
                                        lbl2.Text = sb.ToString();
                                        Label lbl1 = (Label)ClientSearch1.FindControl("lblNoresults");
                                        lbl1.Visible = false;
                                        txt.Text = "";
                                    }

                                    Control dynamicPlaceHolder = ClientSearch1.FindControl("SearchByNamePlaceHolder");
                                    foreach (UserControls_ClientNameSelector ucn in dynamicPlaceHolder.Controls)
                                    {

                                        ucn.BusinessNameValue = name;
                                        ucn.CityValue = cityValue;
                                        hdnAdvSrchState.Value = result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].StateProv.Value.ToString();
                                        DropDownList ddlStateDropDown = (System.Web.UI.WebControls.DropDownList)(ucn.FindControl("StateDropDownList1"));
                                        ddlStateDropDown.ClearSelection();
                                        ddlStateDropDown.Items.FindByText(stateValue).Selected = true;

                                    }
                                    Session["advClientSearch"] = result;
                                    if (vector.Clients == null)
                                    {
                                        Panel pnl = (Panel)ClientSearch1.FindControl("pnlShowSrchResults");
                                        pnl.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "block");
                                        Label lbl = (Label)ClientSearch1.FindControl("lblPhNumresults");
                                        lbl.Visible = true;
                                        TextBox txt = (TextBox)ClientSearch1.FindControl("txtPhoneNum");
                                        StringBuilder sb = new StringBuilder();
                                        sb.Append(txt.Text + "<br/>");
                                        sb.Append(CheckForNameFormating(name) + "<br/>");
                                        //sb.Append(Common.formatString(String.IsNullOrEmpty(result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].Addr1.Value.ToString()) ? string.Empty : result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].Addr1.Value.ToString()) + "<br/>") ;
                                        if (!string.IsNullOrEmpty(addressValue))
                                            sb.Append(Common.formatString(addressValue) + "<br/>");
                                        sb.Append(Common.formatString(result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].City.Value.ToString()) + ",");
                                        sb.Append(result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].StateProv.Value.ToString() + "<br/>");
                                        Label lbl2 = (Label)ClientSearch1.FindControl("lblSrchResults");
                                        lbl2.Text = sb.ToString();
                                        Label lbl1 = (Label)ClientSearch1.FindControl("lblNoresults");
                                        lbl1.Visible = true;
                                        txt.Text = "";
                                        goto End;
                                    }

                                    if (vector.Error != null)
                                    {
                                        searchMessages.Add(string.Format(
                                            "Too many results returned for search on name - {0}. Please narrow your search criteria.",
                                            name));

                                        pnlSearchCriteria.Visible = true;
                                    }


                                    #region mess
                                    // fill the name , city and state so user knows how we did the search

                                    //txtAdvSrchName.Text = name;
                                    //txtAdvSrchCity.Text = result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].City.Value.ToString();
                                    //txtAdvSrchState.Text = result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo.Addr[0].StateProv.Value.ToString();


                                    //List<BCS.Core.Clearance.BizObjects.ClientSearchResult> cList = new List<BCS.Core.Clearance.BizObjects.ClientSearchResult>();

                                    //if (vector.Clients != null && vector.Clients.Length > 0)
                                    //{
                                    //    foreach (Structures.Search.Client c in vector.Clients)
                                    //    {
                                    //        if (!string.IsNullOrEmpty(c.Portfolio.Id))
                                    //        {
                                    //            BCS.Core.Clearance.BizObjects.ClientSearchResult rslt = new Core.Clearance.BizObjects.ClientSearchResult();
                                    //            rslt.ClientId = Convert.ToInt32(c.Portfolio.Id);
                                    //            rslt.Address = c.Addresses[0].Address1 + " " + c.Addresses[0].Address2 + "," + c.Addresses[0].City + "," + c.Addresses[0].StateProvCode;
                                    //            rslt.Name = c.Names[0].NameBusiness;
                                    //            cList.Add(rslt);
                                    //        }
                                    //    }

                                    //    DataTable dt = new DataTable();
                                    //    dt.Columns.Add("ClientId", System.Type.GetType("System.String"));
                                    //    dt.Columns.Add("Name", System.Type.GetType("System.String"));
                                    //    dt.Columns.Add("Address", System.Type.GetType("System.String"));

                                    //    for (int i = 0; i < cList.Count; i++)
                                    //    {

                                    //        DataRow dr = dt.NewRow();
                                    //        dr["ClientId"] = cList[i].ClientId.ToString();
                                    //        dr["Name"] = cList[i].Name;
                                    //        dr["Address"] = cList[i].Address;

                                    //        dt.Rows.Add(dr);
                                    //    }
                                    //    pnlSearchCriteria.Visible = true;
                                    //   // pnlAdvClientSrchResults.Visible = true;
                                    //    pnlAdvClientSrchResults.Style["display"] = "block";
                                    //    gvAdvClientSrchResults.DataSource = dt;
                                    //    gvAdvClientSrchResults.DataBind();
                                    //    mdlAdvClientSrchResults.Show();
                                    //    dvOverlay.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "block");
                                    //    mdlAdvClientSrchResults.BackgroundCssClass = "modalBackground";
                                    //    hdnHideModalPopup.Value = "hide";
                                    //    Session["advClientSearch"] = result;
                                    //}

                                    //if (vector.Error == null && vector.Clients == null)
                                    //{
                                    //    //searchMessages.Add(string.Format(
                                    //    //    "Too many results returned for search on name - {0}, city - {1} / state {2}. Please narrow your search criteria.",
                                    //    //    cn.ClientName.NameBusiness, cn.City, cn.State));
                                    //    gvAdvClientSrchResults.DataSource = null;
                                    //    gvAdvClientSrchResults.DataBind();
                                    //    pnlAdvClientSrchResults.Style["display"] = "block";
                                    //    pnlSearchCriteria.Visible = true;
                                    //    mdlAdvClientSrchResults.TargetControlID = "btnHiddenforModal";
                                    //    mdlAdvClientSrchResults.BackgroundCssClass = "modalBackground";
                                    //    mdlAdvClientSrchResults.Show();
                                    //    dvOverlay.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "block");
                                    //    hdnHideModalPopup.Value = "hide";
                                    //    Session["advClientSearch"] = result;

                                    //}
                                    //}
                                    //else
                                    //{
                                    //    gvAdvClientSrchResults.DataSource = null;
                                    //    gvAdvClientSrchResults.EmptyDataText = "No results found for the given phone number.";
                                    //    gvAdvClientSrchResults.DataBind();
                                    //    pnlAdvClientSrchResults.Style["display"] = "block";
                                    //    pnlSearchCriteria.Visible = true;
                                    //    mdlAdvClientSrchResults.Show();
                                    //    dvOverlay.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "block");
                                    //    hdnHideModalPopup.Value = "hide";
                                    //    Session["advClientSearch"] = result;

                                    //}
                                    # endregion
                                    goto LoadResults;
                                }



                                else
                                {
                                    Panel pnl = (Panel)ClientSearch1.FindControl("pnlShowSrchResults");
                                    pnl.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "none");
                                    Label lbl = (Label)ClientSearch1.FindControl("lblPhNumresults");
                                    lbl.Visible = false;
                                    Label lbl2 = (Label)ClientSearch1.FindControl("lblSrchResults");
                                    lbl2.Text = "";
                                    Label lbl1 = (Label)ClientSearch1.FindControl("lblNoresults");
                                    lbl1.Visible = false;


                                    Control dynamicPlaceHolder = ClientSearch1.FindControl("SearchByNamePlaceHolder");
                                    foreach (UserControls_ClientNameSelector ucn in dynamicPlaceHolder.Controls)
                                    {
                                        ucn.BusinessNameValue = "";
                                        ucn.CityValue = "";
                                        hdnAdvSrchState.Value = "";
                                        DropDownList ddlStateDropDown = (System.Web.UI.WebControls.DropDownList)(ucn.FindControl("StateDropDownList1"));
                                        ddlStateDropDown.ClearSelection();

                                    }
                                    goto End;
                                }
                                //break;
                            }
                        }

                        catch (Exception ex)
                        {
                            goto End;
                        }
                        
                    }
                    else
                        goto End;
                }
                #endregion


                #region Search By Id
                ClientIDSearch:
                if (Request.QueryString["clientid"] != null)
                {
                    Session["ClientId"] = Request.QueryString["clientid"];
                    clientid = Request.QueryString["clientid"];
                }
                if (!string.IsNullOrEmpty(clientid))
                {
                    try
                    {
                        string clientxml = clientproxy.GetClientByIdForSearchScreen(currentCompanyNumber, clientid,true);

                        Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                            (Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                            clientxml, typeof(Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

                        client = clearanceclient.ClientCoreClient;

                        Structures.Search.Client c = new BTS.ClientCore.Wrapper.Structures.Search.Client();
                        c.Info.ClientCategory = client.Client.ClientCategory;
                        c.Info.ClientId = client.Client.ClientId;
                        c.Info.ClientStatus = client.Client.ClientStatus;
                        c.Info.ClientType = client.Client.ClientType;
                        c.Info.CorpId = client.Client.CorpId;
                        c.Info.Segmented = client.Client.SegmentedClient;
                        c.Info.TaxId = client.Client.TaxId;
                        c.Info.TaxIdType = client.Client.TaxIdType;

                        if (client.Addresses != null)
                        {
                            c.Addresses = new Structures.Search.AddressVersion[client.Addresses.Length];

                            int i = 0;
                            foreach (Structures.Info.Address address in client.Addresses)
                            {
                                c.Addresses[i].Address1 = address.Address1;
                                c.Addresses[i].Address2 = address.Address2;
                                c.Addresses[i].Address3 = address.Address3;
                                c.Addresses[i].Address4 = address.Address4;
                                c.Addresses[i].AddressId = address.AddressId;
                                c.Addresses[i].City = address.City;
                                c.Addresses[i].CountryId = address.CountryId;
                                c.Addresses[i].County = address.County;
                                c.Addresses[i].CountyId = address.CountyId;
                                c.Addresses[i].HouseNumber = address.HouseNumber;
                                c.Addresses[i].PostalCode = address.PostalCode;
                                //c.Addresses[i].Primary = address.Primary;
                                c.Addresses[i].StateProvCode = address.StateProvinceId;
                                c.Addresses[i].StateProvId = address.StateProvinceId;
                                i++;
                            }
                        }

                        if (client.Client.Names != null)
                        {
                            c.Names = new BTS.ClientCore.Wrapper.Structures.Search.ClientName[client.Client.Names.Length];

                            int i = 0;
                            foreach (Structures.Info.Name name in client.Client.Names)
                            {
                                c.Names[i].ClientId = name.ClientId;
                                c.Names[i].NameBusiness = name.NameBusiness;
                                c.Names[i].NameFirst = name.NameFirst;
                                c.Names[i].NameLast = name.NameLast;
                                c.Names[i].NameMiddle = name.NameMiddle;
                                c.Names[i].NamePrefix = name.NamePrefix;
                                c.Names[i].NameSuffix = name.NameSuffix;
                                c.Names[i].NameType = name.NameType;
                                c.Names[i].PrimaryNameFlag = name.PrimaryNameFlag;
                                c.Names[i].SequenceNumber = name.SequenceNumber;
                                i++;
                            }
                        }
                        c.Portfolio = client.Client.Portfolio;

                        clientDictionary.Add(c.Info.ClientId, c);
                        LoadSearchResults(clientDictionary);
                    }
                    catch (System.Net.WebException wex)
                    {
                        btnEditClient.Visible = false;
                        btnCreateShellSubmission.Visible = false;
                        phSubmissions.Visible = false;
                        phPortfolio.Visible = false;

                        switch (wex.Status)
                        {
                            case System.Net.WebExceptionStatus.ConnectFailure:
                                {
                                    Common.SetStatus(messagePlaceHolder, "Unable to connect to the ClientCORE. Please try again. Contact the system admin if the problem persists.");
                                    LogCentral.Current.LogFatal("Unable to connect to the ClientCORE supplied by the URL.", wex);
                                }
                                break;
                            case System.Net.WebExceptionStatus.Timeout:
                                {
                                    Common.SetStatus(messagePlaceHolder, "The search has timed out. Please try again.");
                                    LogCentral.Current.LogWarn("The search is timed out.", wex);
                                }
                                break;
                            default:
                                throw;
                        }
                    }
                    catch (Exception ex)
                    {
                        LogCentral.Current.LogWarn(string.Format("The Client associated with the submission either does not exist or is inaccessible. Id = {0} CompanyNumberId = {1}", clientid, Session["CompanyNumberSelected"]), ex);
                        Common.SetStatus(messagePlaceHolder, "Requested client either does not exist or is inaccessible.");
                        btnEditClient.Visible = false;
                        btnCreateShellSubmission.Visible = false;
                        phSubmissions.Visible = false;
                        phPortfolio.Visible = false;
                        pnlSearchCriteria.Visible = true;
                    }

                    btnAddClient.Visible = true;

                    return;
                }
                #endregion

                string somethingentered = ClientSearch1.PolicyNumber;

                if (!string.IsNullOrEmpty(somethingentered))
                {
                    string searchType = DefaultValues.GetSpecialSearchText(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                    if (searchType == "Policy Number")
                    {
                        string policynumber = ClientSearch1.PolicyNumber;
                        #region Search By Policy Number
                        if (!string.IsNullOrEmpty(policynumber))
                        {
                            System.Text.RegularExpressions.Regex nonNumericCharacters = new System.Text.RegularExpressions.Regex(@"[^0-9]");
                            String plcynum = nonNumericCharacters.Replace(policynumber, String.Empty);
                            
                            SortedList distinctClientIds = new SortedList();

                            StringBuilder statusMessageSB = new StringBuilder();
                            SubmissionProxy.Submission subproxy = Common.GetSubmissionProxy();
                            //OLD call - 
                            DateTime start = DateTime.Now;
                           // string xml = subproxy.SearchSubmissionsByPolicyNumber(currentCompanyNumber, policynumber);

                            //string test1 = (DateTime.Now - start).TotalMilliseconds.ToString();
                            //string test2 = (DateTime.Now - start).TotalMilliseconds.ToString();
                            //string test3 = test2 + ";" + test1;
                            //start = DateTime.Now;
                            ////NEW call - bypass service
                           // string xml = BCS.Core.Clearance.Submissions.SearchSubmissionsByPolicyNumber(currentCompanyNumber, policynumber, 0, 0, false);
                            string xml = BCS.Core.Clearance.Submissions.SearchSubmissionsByPolicyNumber(currentCompanyNumber, plcynum, 0, 0, false);

                            BCS.Core.Clearance.BizObjects.SubmissionList sl = (BCS.Core.Clearance.BizObjects.SubmissionList)
                            BCS.Core.XML.Deserializer.Deserialize(
                            xml, typeof(BCS.Core.Clearance.BizObjects.SubmissionList));


                            if (sl.PageSize > 200)
                            {
                                //Common.SetStatus(messagePlaceHolder, "Too many results returned for search on Policy Number . Please narrow your search criteria.");
                                StringCollection sc = new StringCollection();
                                sc.Add("1");
                                LoadSearchResults(clientDictionary,sc);
                            }
                            else
                            {

                                foreach (Core.Clearance.BizObjects.Submission s in sl.List)
                                {
                                    if (!distinctClientIds.Contains(s.ClientCoreClientId))
                                        distinctClientIds.Add(s.ClientCoreClientId, s.ClientCoreClientId);
                                }

                                foreach (long key in distinctClientIds.Keys)
                                {
                                    string clientxml =
                                        clientproxy.GetClientByIdForSearchScreen(currentCompanyNumber, key.ToString(), true);

                                    try
                                    {
                                        Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                                        (Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                                        clientxml, typeof(Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

                                        client = clearanceclient.ClientCoreClient;

                                        Structures.Search.Client c = new BTS.ClientCore.Wrapper.Structures.Search.Client();
                                        c.Info.ClientCategory = client.Client.ClientCategory;
                                        c.Info.ClientId = client.Client.ClientId;
                                        c.Info.ClientStatus = client.Client.ClientStatus;
                                        c.Info.ClientType = client.Client.ClientType;
                                        c.Info.CorpId = client.Client.CorpId;
                                        c.Info.Segmented = client.Client.SegmentedClient;
                                        c.Info.TaxId = client.Client.TaxId;
                                        c.Info.TaxIdType = client.Client.TaxIdType;

                                        c.Addresses = new Structures.Search.AddressVersion[client.Addresses.Length];

                                        int i = 0;
                                        foreach (Structures.Info.Address address in client.Addresses)
                                        {
                                            c.Addresses[i].Address1 = address.Address1;
                                            c.Addresses[i].Address2 = address.Address2;
                                            c.Addresses[i].Address3 = address.Address3;
                                            c.Addresses[i].Address4 = address.Address4;
                                            c.Addresses[i].AddressId = address.AddressId;
                                            c.Addresses[i].City = address.City;
                                            c.Addresses[i].CountryId = address.CountryId;
                                            c.Addresses[i].County = address.County;
                                            c.Addresses[i].CountyId = address.CountyId;
                                            c.Addresses[i].HouseNumber = address.HouseNumber;
                                            c.Addresses[i].PostalCode = address.PostalCode;
                                            //c.Addresses[i].Primary = address.Primary;
                                            c.Addresses[i].StateProvCode = address.StateProvinceId;
                                            c.Addresses[i].StateProvId = address.StateProvinceId;
                                            i++;
                                        }

                                        //for (int j = 0; j < client.Addresses.Length; j++)
                                        //{
                                        //    c.Addresses[j].Address1 = client.Addresses[j].Address1;
                                        //    c.Addresses[j].Address2 = client.Addresses[j].Address2;
                                        //    c.Addresses[j].Address3 = client.Addresses[j].Address3;
                                        //    c.Addresses[j].Address4 = client.Addresses[j].Address4;
                                        //    c.Addresses[j].AddressId = client.Addresses[j].AddressId;
                                        //    c.Addresses[j].City = client.Addresses[j].City;
                                        //    c.Addresses[j].CountryId = client.Addresses[j].CountryId;
                                        //    c.Addresses[j].County = client.Addresses[j].County;
                                        //    c.Addresses[j].CountyId = client.Addresses[j].CountyId;
                                        //    c.Addresses[j].HouseNumber = client.Addresses[j].HouseNumber;
                                        //    c.Addresses[j].PostalCode = client.Addresses[j].PostalCode;
                                        //    //c.Addresses[i].Primary = address.Primary;
                                        //    c.Addresses[j].StateProvCode = client.Addresses[j].StateProvinceId;
                                        //    c.Addresses[j].StateProvId = client.Addresses[j].StateProvinceId;

                                        //}

                                        c.Names = new BTS.ClientCore.Wrapper.Structures.Search.ClientName[client.Client.Names.Length];

                                        i = 0;
                                        foreach (Structures.Info.Name name in client.Client.Names)
                                        {
                                            c.Names[i].ClientId = name.ClientId;
                                            c.Names[i].NameBusiness = name.NameBusiness;
                                            c.Names[i].NameFirst = name.NameFirst;
                                            c.Names[i].NameLast = name.NameLast;
                                            c.Names[i].NameMiddle = name.NameMiddle;
                                            c.Names[i].NamePrefix = name.NamePrefix;
                                            c.Names[i].NameSuffix = name.NameSuffix;
                                            c.Names[i].NameType = name.NameType;
                                            c.Names[i].PrimaryNameFlag = name.PrimaryNameFlag;
                                            c.Names[i].SequenceNumber = name.SequenceNumber;
                                            i++;
                                        }

                                        //for (int j = 0; j < client.Client.Names.Length; j++)
                                        //{
                                        //    c.Names[j].ClientId = client.Client.Names[j].ClientId;
                                        //    c.Names[j].NameBusiness = client.Client.Names[j].NameBusiness;
                                        //    c.Names[j].NameFirst = client.Client.Names[j].NameFirst;
                                        //    c.Names[j].NameLast = client.Client.Names[j].NameLast;
                                        //    c.Names[j].NameMiddle = client.Client.Names[j].NameMiddle;
                                        //    c.Names[j].NamePrefix = client.Client.Names[j].NamePrefix;
                                        //    c.Names[j].NameSuffix = client.Client.Names[j].NameSuffix;
                                        //    c.Names[j].NameType = client.Client.Names[j].NameType;
                                        //    c.Names[j].PrimaryNameFlag = client.Client.Names[j].PrimaryNameFlag;
                                        //    c.Names[j].SequenceNumber = client.Client.Names[j].SequenceNumber;
                                        //}

                                        c.Portfolio = client.Client.Portfolio;

                                        if (!clientDictionary.ContainsKey(c.Info.ClientId))
                                            clientDictionary.Add(c.Info.ClientId, c);

                                    }

                                    catch (Exception)
                                    {
                                        statusMessageSB.AppendFormat("{0} , ", key);
                                        continue;
                                    }
                                }
                           
                            if (statusMessageSB.Length > 0)
                            {
                                LogCentral.Current.LogWarn(null,
                                    new Exception(string.Format("Policy Number Search ( criteria - {0}) : Unable to access the following client(s). {1}",
                                    policynumber, statusMessageSB)));
                            }
                            LoadSearchResults(clientDictionary);
                            
                        }
                        }
                        #endregion

                    }
                    else if (searchType == "Submission Number")
                    {
                        string submissionNumber = ClientSearch1.PolicyNumber;
                        #region Search By submission Number
                        if (!string.IsNullOrEmpty(submissionNumber))
                        {
                            SortedList distinctClientIds = new SortedList();

                            StringBuilder statusMessageSB = new StringBuilder();
                            SubmissionProxy.Submission subproxy = Common.GetSubmissionProxy();
                            string xml = subproxy.SearchSubmissionsBySubmissionNumber(currentCompanyNumber, Convert.ToInt64(submissionNumber));

                            BCS.Core.Clearance.BizObjects.SubmissionList sl = (BCS.Core.Clearance.BizObjects.SubmissionList)
                            BCS.Core.XML.Deserializer.Deserialize(
                            xml, typeof(BCS.Core.Clearance.BizObjects.SubmissionList));


                            foreach (Core.Clearance.BizObjects.Submission s in sl.List)
                            {
                                if (!distinctClientIds.Contains(s.ClientCoreClientId))
                                    distinctClientIds.Add(s.ClientCoreClientId, s.ClientCoreClientId);
                            }

                            foreach (long key in distinctClientIds.Keys)
                            {
                                string clientxml =
                                    clientproxy.GetClientByIdForSearchScreen(currentCompanyNumber, key.ToString(),true);

                                try
                                {
                                    Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                                    (Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                                    clientxml, typeof(Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

                                    client = clearanceclient.ClientCoreClient;

                                    Structures.Search.Client c = new BTS.ClientCore.Wrapper.Structures.Search.Client();
                                    c.Info.ClientCategory = client.Client.ClientCategory;
                                    c.Info.ClientId = client.Client.ClientId;
                                    c.Info.ClientStatus = client.Client.ClientStatus;
                                    c.Info.ClientType = client.Client.ClientType;
                                    c.Info.CorpId = client.Client.CorpId;
                                    c.Info.Segmented = client.Client.SegmentedClient;
                                    c.Info.TaxId = client.Client.TaxId;
                                    c.Info.TaxIdType = client.Client.TaxIdType;

                                    c.Addresses = new Structures.Search.AddressVersion[client.Addresses.Length];

                                    int i = 0;
                                    foreach (Structures.Info.Address address in client.Addresses)
                                    {
                                        c.Addresses[i].Address1 = address.Address1;
                                        c.Addresses[i].Address2 = address.Address2;
                                        c.Addresses[i].Address3 = address.Address3;
                                        c.Addresses[i].Address4 = address.Address4;
                                        c.Addresses[i].AddressId = address.AddressId;
                                        c.Addresses[i].City = address.City;
                                        c.Addresses[i].CountryId = address.CountryId;
                                        c.Addresses[i].County = address.County;
                                        c.Addresses[i].CountyId = address.CountyId;
                                        c.Addresses[i].HouseNumber = address.HouseNumber;
                                        c.Addresses[i].PostalCode = address.PostalCode;
                                        //c.Addresses[i].Primary = address.Primary;
                                        c.Addresses[i].StateProvCode = address.StateProvince;
                                        c.Addresses[i].StateProvId = address.StateProvinceId;
                                        i++;
                                    }

                                    c.Names = new BTS.ClientCore.Wrapper.Structures.Search.ClientName[client.Client.Names.Length];

                                    i = 0;
                                    foreach (Structures.Info.Name name in client.Client.Names)
                                    {
                                        c.Names[i].ClientId = name.ClientId;
                                        c.Names[i].NameBusiness = name.NameBusiness;
                                        c.Names[i].NameFirst = name.NameFirst;
                                        c.Names[i].NameLast = name.NameLast;
                                        c.Names[i].NameMiddle = name.NameMiddle;
                                        c.Names[i].NamePrefix = name.NamePrefix;
                                        c.Names[i].NameSuffix = name.NameSuffix;
                                        c.Names[i].NameType = name.NameType;
                                        c.Names[i].PrimaryNameFlag = name.PrimaryNameFlag;
                                        c.Names[i].SequenceNumber = name.SequenceNumber;
                                        i++;
                                    }
                                    if (!clientDictionary.ContainsKey(c.Info.ClientId))
                                        clientDictionary.Add(c.Info.ClientId, c);

                                }
                                catch (Exception)
                                {
                                    statusMessageSB.AppendFormat("{0} , ", key);
                                    continue;
                                }
                            }
                            if (statusMessageSB.Length > 0)
                            {
                                LogCentral.Current.LogWarn(null,
                                    new Exception(string.Format("Policy Number Search ( criteria - {0}) : Unable to access the following client(s). {1}",
                                    submissionNumber, statusMessageSB)));
                            }
                            LoadSearchResults(clientDictionary);
                        }
                        #endregion
                    }
                    return;
                }

                //StringCollection searchMessages = new StringCollection();

                // Collect our taxIds
                string[] taxIds;
                if (ClientSearch1.TaxIds.Length > 0)
                {
                    taxIds = ClientSearch1.TaxIds.Split(',');

                    for (int i = 0; i < taxIds.Length; i++)
                    {
                        Structures.Search.Vector vector = (Structures.Search.Vector)BCS.Core.XML.Deserializer.Deserialize(clientproxy.GetClientByTaxId(currentCompanyNumber, taxIds[i]), typeof(Structures.Search.Vector));
                        if (vector.Clients != null && vector.Clients.Length > 0)
                            foreach (Structures.Search.Client c in vector.Clients)
                                if (!clientDictionary.ContainsKey(c.Info.ClientId))
                                    clientDictionary.Add(c.Info.ClientId, c);

                        if (vector.Error != null)
                        {
                            searchMessages.Add(string.Format(
                                "Too many results returned for search on address : tax id - {0}. Please narrow your search criteria.",
                                taxIds[i]));
                            pnlSearchCriteria.Visible = true;
                        }
                    }
                }

                // Collect our portfolioIds
                string[] porIds;
                if (ClientSearch1.PortfolioIds.Length > 0)
                {
                    porIds = ClientSearch1.PortfolioIds.Split(',');

                    for (int i = 0; i < porIds.Length; i++)
                    {
                        if (ClientSearch1.PortfolioSearchType == "Id")
                        {
                            string portSearchXML = clientproxy.GetClientsByPortfolioId(currentCompanyNumber, porIds[i]);
                            Structures.Search.Vector vector = (Structures.Search.Vector)BCS.Core.XML.Deserializer.Deserialize(portSearchXML, typeof(Structures.Search.Vector));

                            if (vector.Clients != null && vector.Clients.Length > 0)
                                foreach (Structures.Search.Client c in vector.Clients)
                                    if (!clientDictionary.ContainsKey(c.Info.ClientId))
                                        clientDictionary.Add(c.Info.ClientId, c);

                            if (vector.Error != null)
                            {
                                searchMessages.Add(string.Format(
                                    "Too many results returned for search on address : portfolio id - {0}. Please narrow your search criteria.",
                                    porIds[i]));
                                pnlSearchCriteria.Visible = true;
                            }
                        }
                        else
                        {
                            //string portfolioXml = clientproxy.GetClientsByPortfolioName(currentCompanyNumber, porIds[i]);
                            //Structures.Search.SearchedPortfolios searchedPortfolios = (Structures.Search.SearchedPortfolios)BCS.Core.XML.Deserializer.Deserialize(portfolioXml,
                            //    typeof(Structures.Search.SearchedPortfolios));

                            //if (string.IsNullOrEmpty(searchedPortfolios.Message))
                            //{
                            //    LoadPortfolios(portfolioXml);
                            //}
                            //else
                            //{
                            //    phPortfolioInfo.Visible = true;
                            //    Common.SetStatus(messagePlaceHolder, searchedPortfolios.Message);
                            //}

                            //return;
                            string portSearchXML = clientproxy.GetClientsByPortfolioName(currentCompanyNumber, porIds[i]);
                            Structures.Search.Vector vector = (Structures.Search.Vector)BCS.Core.XML.Deserializer.Deserialize(portSearchXML, typeof(Structures.Search.Vector));

                            if (vector.Clients != null && vector.Clients.Length > 0)
                                foreach (Structures.Search.Client c in vector.Clients)
                                    if (!clientDictionary.ContainsKey(c.Info.ClientId))
                                        clientDictionary.Add(c.Info.ClientId, c);

                            if (vector.Error != null)
                            {
                                searchMessages.Add(string.Format(
                                    "Too many results returned for search on address : portfolio id - {0}. Please narrow your search criteria.",
                                    porIds[i]));
                                pnlSearchCriteria.Visible = true;
                            }
                        }
                    }
                }

                foreach (Structures.Search.AddressVersion av in ClientSearch1.AddressVersionsToSearch)
                {
                    Structures.Search.Vector vector =
                        (Structures.Search.Vector)BCS.Core.XML.Deserializer.Deserialize(
                        clientproxy.GetClientsByAddress(currentCompanyNumber, av.HouseNumber, av.Address1, av.Address2, string.Empty,
                        string.Empty, av.City, av.StateProvCode, av.County, av.CountryCode, av.PostalCode), typeof(Structures.Search.Vector));
                    if (vector.Clients != null && vector.Clients.Length > 0)
                        foreach (Structures.Search.Client c in vector.Clients)
                            if (!clientDictionary.ContainsKey(c.Info.ClientId))
                                clientDictionary.Add(c.Info.ClientId, c);

                    if (vector.Error != null)
                    {
                        searchMessages.Add(string.Format(
                            "Too many results returned for search on address : house no - {0}, street - {0}, city - {0}, state - {0}, zip - {0}. Please narrow your search criteria.",
                            string.IsNullOrEmpty(av.HouseNumber) ? "NULL" : av.HouseNumber,
                            string.IsNullOrEmpty(av.Address1) ? "NULL" : av.Address1,
                            string.IsNullOrEmpty(av.City) ? "NULL" : av.City,
                            string.IsNullOrEmpty(av.StateProvCode) ? "NULL" : av.StateProvCode,
                            string.IsNullOrEmpty(av.PostalCode) ? "NULL" : av.PostalCode));

                        pnlSearchCriteria.Visible = true;
                    }
                }

                foreach (ClientNameSearch cn in ClientSearch1.ClientNamesToSearch)
                {
                    if (cn.ClientName.NameBusiness != null)
                    {
                        if (string.IsNullOrEmpty(cn.City) && string.IsNullOrEmpty(cn.State))
                        {
                            Structures.Search.Vector vector =
                                (Structures.Search.Vector)BCS.Core.XML.Deserializer.Deserialize(
                                clientproxy.GetClientByBusinessName(currentCompanyNumber, cn.ClientName.NameBusiness), typeof(Structures.Search.Vector));
                            if (vector.Clients != null && vector.Clients.Length > 0)
                                foreach (Structures.Search.Client c in vector.Clients)
                                    if (!clientDictionary.ContainsKey(c.Info.ClientId))
                                        clientDictionary.Add(c.Info.ClientId, c);

                            if (vector.Error != null)
                            {
                                searchMessages.Add(string.Format(
                                    "Too many results returned for search on name - {0}. Please narrow your search criteria.",
                                    cn.ClientName.NameBusiness));

                                pnlSearchCriteria.Visible = true;
                            }
                        }
                        else
                        {
                            Structures.Search.Vector vector =
                                (Structures.Search.Vector)BCS.Core.XML.Deserializer.Deserialize(
                                clientproxy.GetClient(currentCompanyNumber,
                                string.Empty, string.Empty, string.Empty, cn.City, cn.State, string.Empty, string.Empty, string.Empty,
                                cn.ClientName.NameBusiness), typeof(Structures.Search.Vector));
                            if (vector.Clients != null && vector.Clients.Length > 0)
                                foreach (Structures.Search.Client c in vector.Clients)
                                    if (!clientDictionary.ContainsKey(c.Info.ClientId))
                                        clientDictionary.Add(c.Info.ClientId, c);

                            if (vector.Error != null)
                            {
                                searchMessages.Add(string.Format(
                                    "Too many results returned for search on name - {0}, city - {1} / state {2}. Please narrow your search criteria.",
                                    cn.ClientName.NameBusiness, cn.City, cn.State));

                                pnlSearchCriteria.Visible = true;
                            }
                        }
                    }
                    else
                    {
                        Structures.Search.Vector vector =
                                (Structures.Search.Vector)BCS.Core.XML.Deserializer.Deserialize(
                                clientproxy.GetClientsByName(currentCompanyNumber, cn.ClientName.NameFirst, cn.ClientName.NameLast), typeof(Structures.Search.Vector));
                        if (vector.Clients != null && vector.Clients.Length > 0)
                            foreach (Structures.Search.Client c in vector.Clients)
                                if (!clientDictionary.ContainsKey(c.Info.ClientId))
                                    clientDictionary.Add(c.Info.ClientId, c);

                        if (vector.Error != null)
                        {
                            searchMessages.Add(string.Format(
                                "Too many results returned for search on last name -{0}, first name - {1}. Please narrow your search criteria.",
                                string.IsNullOrEmpty(cn.ClientName.NameLast) ? "NULL" : cn.ClientName.NameLast,
                                string.IsNullOrEmpty(cn.ClientName.NameFirst) ? "NULL" : cn.ClientName.NameFirst));

                            pnlSearchCriteria.Visible = true;
                        }
                    }

                }

                LoadResults:
                LoadSearchResults(clientDictionary, searchMessages);

                
                
                // We make the user search at least once (i.e. visible is set to false in markup)
                btnAddClient.Visible = true;

            End:
                if (usedPhoneLookup)
                {
                    searchMessages.Clear();
                    LoadSearchResults(clientDictionary, searchMessages);
                    btnAddClient.Visible = false;
                }
            
               

            }
           
            catch (System.Net.WebException wex)
            {
                switch (wex.Status)
                {
                    case System.Net.WebExceptionStatus.ConnectFailure:
                        {
                            Common.SetStatus(messagePlaceHolder, "Unable to connect to the ClientCORE. Please try again. Contact the system admin if the problem persists.");
                            LogCentral.Current.LogFatal("Unable to connect to the ClientCORE supplied by the URL.", wex);
                        }
                        break;
                    case System.Net.WebExceptionStatus.Timeout:
                        {
                            Common.SetStatus(messagePlaceHolder, "The search has timed out. Please try again.");
                            LogCentral.Current.LogWarn("The search is timed out.", wex);
                        }
                        break;
                    default:
                        throw;
                }
            }
            catch (SoapException sex)
            {
                pnlSearchCriteria.Visible = true;
                throw;
            }
            catch (Exception ex)
            {
                pnlSearchCriteria.Visible = true;
                throw ex;
            }

           
        }

        protected void lbSearchAgain_Click(object sender, System.EventArgs e)
        {
            pnlSearchCriteria.Visible = true;
            pnlSearchResults.Visible = false;
            phPortfolio.Visible = false;
            lbAddress.Items.Clear();
            lbNames.Items.Clear();
            //PortfolioTree.Nodes.Clear();
            Session.Contents.Remove("Clients");
            Session.Contents.Remove("ClientId");
            Session.Contents.Remove("SavedSearch");
        }

        protected void chkIncludeDeleted_OnCheckChanged(object sender, EventArgs e)
        {
            lbNames_SelectedIndexChanged(lbNames, null);
        }

        protected void lbNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbAddress.ClearSelection();
            ListBox lb = sender as ListBox;
            if (lb == null)
                return;
            if (lb.SelectedIndex < 0)
                return;

            string selectedValue = lb.SelectedValue.Split('^')[0];
         
            //for (int i = 0; i < lb.Items.Count; i++)
            //{
            //    lb.Items[i].Attributes.CssStyle.Add("background-color", "#ffffff");
            //    if (lb.Items[i].Value.Contains(selectedValue))
            //    {
            //        lb.Items[i].Selected = true;
            //        lb.Items[i].Attributes.CssStyle.Add("background-color", "yellow");
            //    }
            //}


            //CompanyNumberDropDownList cnt = Master.FindControl("companyselector1").FindControl("CompanyNumberDropDownList1") as CompanyNumberDropDownList;

            BCS.Core.Security.CustomPrincipal user = User as BCS.Core.Security.CustomPrincipal;

            //int agencyId = user.HasAgency() ? Components.Security.GetMasterAgency(user.Agency) : -1;
            // will not filter the results, but disable the load button
            string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
            int agencyId = -1;

            //DateTime start = DateTime.Now;
            //string xml = BCS.Core.Clearance.Submissions.SearchByClientCoreClientIdAndAgencyId(companyNumber,
            //     lb.SelectedIndex < 0 ? 0 : Convert.ToInt64(selectedValue), agencyId, 0, 0,chkIncludeDeleted.Checked);

            //string oldTest = (DateTime.Now - start).TotalMilliseconds.ToString();

            //start = DateTime.Now;
            string xml = BCS.Core.Clearance.SubmissionsEF.SearchByClientCoreClientIdAndAgencyId(companyNumber, lb.SelectedIndex < 0 ? 0 : Convert.ToInt64(selectedValue), agencyId, 0, 0, chkIncludeDeleted.Checked);
            //string newTest = (DateTime.Now - start).TotalMilliseconds.ToString();

            System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(@"\^\d*");
            Session["ClientId"] = rgx.Replace(lb.SelectedValue, "");
            DataTable dt = Common.GenerateDataTable(companyNumber, "submission", xml, new Pair("master_agency_id", "master_agency_id"));

            if (dt != null)
            {
                DataTable ndt = dt.Clone();
                ndt.Columns["submission_number"].DataType = typeof(long);
                ndt.Columns["submission_number_sequence"].DataType = typeof(int);
                foreach (DataRow dr in dt.Rows)
                    ndt.ImportRow(dr);
                ndt.AcceptChanges();
                ndt.DefaultView.Sort = dt.DefaultView.Sort;
                ViewState[key4DefaultSort] = dt.DefaultView.Sort;

                gvSubmissions.PageIndex = 0;
                gvSubmissions.DataSource = ndt.DefaultView;
                gvSubmissions.DataBind();

                ViewState[key4Searched] = ndt;
            }
            else
            {
                gvSubmissions.DataSource = dt;
                gvSubmissions.DataBind();
            }
            ViewState[key4PreviousSortExpression] = null;
            phSubmissions.Visible = true;

            LoadAddresses(selectedValue);
            LoadPortfolio(selectedValue);

            btnSubmission.Visible = lbAddress.Items.Count > 0;

            bool clientMarkedDeleted = Core.Clearance.Clients.ClientMarkedAsDeleted(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId), Convert.ToInt64(selectedValue));
            btnSubmission.Enabled = !clientMarkedDeleted;

            if (clientMarkedDeleted)
                Common.SetStatus(messagePlaceHolder, "Client is pending deletion.");
            
            

        }
        protected void lbAddress_SelectedIndexChanged(object sender, EventArgs e)
        {
            string addressid = lbAddress.SelectedValue;

            foreach (GridViewRow row in gvSubmissions.Rows)
            {
                if (row.FindControl("lblClientAddress") != null)
                {
                    string[] addressids = (row.FindControl("lblClientAddress") as Label).Text.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (Array.BinarySearch(addressids, addressid) > -1)
                    //if ((row.FindControl("lblClientAddress") as Label).Text == addressid)
                    {
                        //row.BackColor = System.Drawing.Color.Red;
                        row.Attributes.CssStyle.Add("background-color", "yellow");
                        //row.BorderColor = Color.Black;
                        //row.BorderWidth = Unit.Pixel(1);
                    }
                    else
                    {
                        row.Attributes.CssStyle.Clear();
                    }
                }
            }
        }

        protected void btnSubmission_Command(object sender, CommandEventArgs e)
        {
            SaveSearch();
            Button btn = sender as Button;
            if (btn.CommandName == "Add")
            {
                Session["PossibleDuplicates"] = BuildPossibleDuplicates();
                Session.Contents.Remove("SubmissionId");
                Session.Contents.Remove(SessionKeys.VerifyingSubmission);
                Session["subref"] = "Default.aspx";
                if (DefaultValues.UseWizard)
                    Response.Redirect("SubmissionWizard.aspx");
                Response.Redirect("Submission.aspx?op=add");

            }
        }
        protected void btnAddClient_Click(object sender, EventArgs e)
        {
            SaveSearch();
            Session.Contents.Remove("SubmissionId");
            Session.Contents.Remove("ClientId");
            Session.Contents.Remove("advClientSearch");
            Response.Redirect("Client.aspx?op=add");
        }

        protected void btnEditClient_Click(object sender, EventArgs e)
        {
            SaveSearch();
            Session.Contents.Remove("SubmissionId");
            if (lbNames.SelectedIndex < 0 && lbAddress.SelectedIndex < 0)
            {
                Common.SetStatus(messagePlaceHolder, "Please select a client.");
                return;
            }
            if (lbNames.SelectedIndex > -1)
            {
                System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(@"\^\d*");
                Session["ClientId"] = rgx.Replace(lbNames.SelectedValue, "");
            }
            else
            {
                System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(@"\^\d*");
               Session["ClientId"] = rgx.Replace(lbAddress.SelectedValue,"");
            }
            Session["cliref"] = "Default.aspx";
            Response.Redirect("Client.aspx?op=edit");
        }
        protected void gvSubmissions_Init(object sender, EventArgs e)
        {
            #region required fields independent of company

            // col duplicate selection, required for duplicate selection along with the submission number and sequence
            TemplateField tf = new TemplateField();

            if (ConfigValues.SupportsDuplicateOnSubmissionGrid(Common.GetSafeIntFromSession("CompanySelected")))
            {
                tf.ItemTemplate = new Templates.CheckBoxTemplate(DataControlRowType.EmptyDataRow, BCS.WebApp.Templates.HeaderType.Text, "", "");
                tf.HeaderTemplate = new Templates.CheckBoxTemplate(DataControlRowType.Header, BCS.WebApp.Templates.HeaderType.Text, "Dup", "");
                gvSubmissions.Columns.Insert(0, tf);
            }

            // col id, required
            BoundField bf = new BoundField();
            bf.HeaderText = "Id";
            bf.DataField = "Id";
            bf.Visible = false;
            gvSubmissions.Columns.Insert(0, bf);

            // col selection, required to be in first column (index 0), for "Remote Agency" Role handling
            CommandField cf = new CommandField();

            ButtonField snf = new ButtonField();
            snf.ButtonType = ButtonType.Link;
            snf.DataTextField = "submission_number";
            snf.HeaderText = "Submission Number";
            snf.CommandName = "Select";
            // changed to ButtonType.Link, adding style attrs to make it prominent (grid links are set as black somewhere, need to track them down)
            // the followinig color is from pats_style stylesheet
            snf.ItemStyle.ForeColor = Color.FromArgb(00, 40, Convert.ToInt32("BF", 16));
            if (gvSubmissions.AllowSorting)
                snf.SortExpression = snf.DataTextField;
            gvSubmissions.Columns.Insert(0, snf);

            // col history, required for history icon
            tf = new TemplateField();
            tf.ItemTemplate = new Templates.ImageTemplate(DataControlRowType.EmptyDataRow, "", "");
            tf.Visible = false;
            gvSubmissions.Columns.Insert(gvSubmissions.Columns.Count - 2, tf);

            // col client core client id, TODO: probably not needed
            bf = new BoundField();
            bf.HeaderText = "clientcore_client_id";
            bf.DataField = "clientcore_client_id";
            bf.Visible = false;
            gvSubmissions.Columns.Insert(gvSubmissions.Columns.Count - 2, bf);

            //// col submission no
            //BoundField snf = new BoundField();
            //snf.HeaderText = "Submission Number";
            //snf.DataField = "submission_number";
            //gvSubmissions.Columns.Add(snf);

            // col submission no sequence
            BoundField snsf = new BoundField();
            snsf.HeaderText = "Sequence";
            snsf.DataField = "submission_number_sequence";
            // want to be able to have html in header #3709
            snsf.HtmlEncode = false;
            if (gvSubmissions.AllowSorting)
                snsf.SortExpression = snsf.DataField;
            gvSubmissions.Columns.Insert(gvSubmissions.Columns.Count - 2, snsf);

            // col client address, required for making associated submissions visually distinct
            tf = new TemplateField();
            tf.ItemTemplate = new Templates.LabelTemplate(DataControlRowType.DataRow, "address", "clientcore_address_ids_string", "lblClientAddress");
            tf.Visible = false;
            gvSubmissions.Columns.Add(tf);


            #endregion

            BCS.Biz.SubmissionDisplayGridCollection dfields = ConfigValues.GetSubmissionGridDisplay(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));

            #region Some Customization on required fields #3709
            // see if any customization is there on the submission_number column
            Biz.SubmissionDisplayGrid customField = dfields.FindByFieldName("submission_number");
            if (customField != null)
            {
                snf.HeaderText = string.IsNullOrEmpty(customField.HeaderText) ? customField.FieldName : customField.HeaderText;
                dfields.Remove(customField);
            }
            // see if any customization is there on the submission_number_sequence column
            customField = dfields.FindByFieldName("submission_number_sequence");
            if (customField != null)
            {
                snsf.HeaderText = string.IsNullOrEmpty(customField.HeaderText) ? customField.FieldName : customField.HeaderText;
                dfields.Remove(customField);
            }
            #endregion

            foreach (Biz.SubmissionDisplayGrid cfield in dfields)
            {
                TemplateField atf = new TemplateField();
                atf.ItemTemplate = new Templates.GenericTemplate(DataControlRowType.DataRow, string.Empty, cfield.FieldName, cfield.AttributeDataType.DataTypeName, string.Empty);
                //atf.HeaderTemplate = new Templates.GenericTemplate(DataControlRowType.Header, string.IsNullOrEmpty(cfield.HeaderText) ? cfield.FieldName : cfield.HeaderText, string.Empty, string.Empty);
                atf.HeaderText = string.IsNullOrEmpty(cfield.HeaderText) ? cfield.FieldName : cfield.HeaderText;
                if (gvSubmissions.AllowSorting)
                    atf.SortExpression = cfield.FieldName;
            
               
                gvSubmissions.Columns.Insert(gvSubmissions.Columns.Count - 2, atf);

                //Add Copy Policy Number button to clip board
                if (cfield.FieldName == "policy_number")
                {
                    atf = new TemplateField();
                    atf.ItemTemplate = new Templates.GenericTemplate(DataControlRowType.DataRow, string.Empty, "policy_number", "ClipboardCopy", string.Empty);
                    gvSubmissions.Columns.Insert(gvSubmissions.Columns.Count - 2, atf);
                }
            }

            if (ConfigValues.SupportsResequencing(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) && BCS.WebApp.Components.Security.CanResequence())
            {
                cf = new CommandField();
                cf.ButtonType = ButtonType.Image;
                cf.EditImageUrl = Common.GetImageUrl("setnext.gif");
                cf.ShowEditButton = true;
                cf.EditText = "Re-Seq";
                cf.HeaderText = "Re-Seq";
                cf.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                gvSubmissions.Columns.Insert(2, cf);
            }

            //Add submission Copy to Clipboard button
            TemplateField cctf = new TemplateField();
            cctf.ItemTemplate = new Templates.GenericTemplate(DataControlRowType.DataRow,string.Empty,"submission_number","ClipboardCopy",string.Empty);
            gvSubmissions.Columns.Insert(2, cctf);

        }
        protected void gvSubmissions_Deleting(object sender, GridViewDeleteEventArgs e)
        {
            BCS.Biz.DataManager dm = new DataManager(DefaultValues.DSN);
            //DataSet ds = BCS.Biz.StoredProcedures.DeleteSubmissionById(Convert.ToInt32(gvSubmissions.DataKeys[e.RowIndex]["id"]));
            // TODO: interpret result from stored proc

            int id = 0;
            Int32.TryParse(gvSubmissions.DataKeys[e.RowIndex]["id"].ToString(),out id);

            if (id > 0)
                Core.Clearance.Submissions.MarkSubmissionAsDeleted(id);

            Common.SetStatus(messagePlaceHolder, "Submission successfully deleted.");
            Cache.Remove("SearchedCacheDependency");
            lbNames_SelectedIndexChanged(lbNames, EventArgs.Empty);
        }
        protected void gvSubmissions_Deleted(object sender, GridViewDeletedEventArgs e)
        {
            //gvSubmissions.DataBind();
        }
        protected void gvSubmissions_Sorted(object sender, EventArgs e)
        { }
        protected void gvSubmissions_Sorting(object sender, GridViewSortEventArgs e)
        {
            Session["PossibleDuplicates"] = BuildPossibleDuplicates();
            DataTable dt = ViewState[key4Searched] as DataTable;
           
            string sortDirection = " ASC";
            if ((string)ViewState[key4PreviousSortExpression] == e.SortExpression)
            {
                if ((string)ViewState[key4PreviousSortDirection] == sortDirection)
                {
                    sortDirection = " DESC";
                }
            }
            dt.DefaultView.Sort = e.SortExpression + sortDirection;
            
            gvSubmissions.DataSource = dt;
            gvSubmissions.DataBind();

            ViewState[key4PreviousSortExpression] = e.SortExpression;
            ViewState[key4PreviousSortDirection] = sortDirection;
            ViewState[key4Searched] = dt;

        }
        protected void gvSubmissions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string smasterAgencyId = "-1";
            BCS.Core.Security.CustomPrincipal user = User as BCS.Core.Security.CustomPrincipal;

            if (user.HasAgency())
            {
                // non-aps agency ids are int
                int masterAgencyId = Convert.ToInt32(BCS.WebApp.Components.Security.GetMasterAgency(user.Agency));
                if (masterAgencyId != 0)
                    smasterAgencyId = masterAgencyId.ToString();
            }
            DataRowView dr = e.Row.DataItem as DataRowView;
            if (dr != null)
            {
                //string key = string.Format("#{0}-{1}", DataBinder.Eval(e.Row.DataItem, "submission_number", string.Empty), DataBinder.Eval(e.Row.DataItem, "submission_number_sequence", string.Empty));
                string key = string.Format(
                        "Submission Number : {0}-{1}, Policy Number : {2}, Status : {3}, Status Date : {4:MM/dd/yyyy}"
                        , DataBinder.Eval(e.Row.DataItem, "submission_number", string.Empty), DataBinder.Eval(e.Row.DataItem, "submission_number_sequence", string.Empty),
                        DataBinder.Eval(e.Row.DataItem, "policy_number", string.Empty),
                        DataBinder.Eval(e.Row.DataItem, "submission_status_code", string.Empty),
                        GetSafeDateTimeString(DataBinder.Eval(e.Row.DataItem, "submission_status_date")));
                if (Session["PossibleDuplicates"] != null)
                {
                    string dups = Session["PossibleDuplicates"].ToString();
                    if (dups.Contains(key))
                    {
                        (e.Row.FindControl("cbDup") as CheckBox).Checked = true;

                        Session["PossibleDuplicates"] = dups.Replace(string.Format("{0};", key), string.Empty);
                    }
                }
                TSHAK.Components.SecureQueryString qs = new TSHAK.Components.SecureQueryString(Core.Core.Key);
                qs["Id"] = DataBinder.Eval(e.Row.DataItem, "id").ToString();
                System.Web.UI.WebControls.Image i = e.Row.FindControl("btnHistory") as System.Web.UI.WebControls.Image;
                i.Attributes.Add("onclick", string.Format("openWindow('SubmissionHistory.aspx?op={0}','submissionHistory', 700, 400); return false;", HttpUtility.UrlEncode(qs.ToString())));

                WebControl loadImage = e.Row.Controls[0].Controls[0] as WebControl;
                Control c = e.Row.FindControl("btnHistory");
                string rowsMasterAgency = DataBinder.Eval(e.Row.DataItem, "master_agency_id", string.Empty);
                string rowsAgency = DataBinder.Eval(e.Row.DataItem, "agency_id", string.Empty);

                //loadImage.Visible = (!user.HasAgency()) || (smasterAgencyId == rowsMasterAgency || smasterAgencyId == rowsAgency);
                // for some reason visible property is not carried over to the databound event, so we will use enabled
                loadImage.Enabled = (!user.HasAgency()) || (smasterAgencyId == rowsMasterAgency || smasterAgencyId == rowsAgency);
                
                c.Visible = loadImage.Enabled;
            }
        }
        protected void gvSubmissions_DataBound(object sender, EventArgs e)
        {
            string smasterAgencyId = "-1";
            BCS.Core.Security.CustomPrincipal user = User as BCS.Core.Security.CustomPrincipal;

            if (user.HasAgency())
            {
                int masterAgencyId = Convert.ToInt32(BCS.WebApp.Components.Security.GetMasterAgency(user.Agency));
                if (masterAgencyId != 0)
                    smasterAgencyId = masterAgencyId.ToString();
            }


            if (user.HasAgency())
            {
                BCS.Biz.SubmissionDisplayGridCollection dfields = ConfigValues.GetSubmissionGridDisplay(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                dfields = dfields.FilterByRemoteAgencyVisible(false);

                foreach (Biz.SubmissionDisplayGrid dfield in dfields)
                {
                    foreach (GridViewRow grow in gvSubmissions.Rows)
                    {
                        WebControl loadImage = grow.Controls[0].Controls[0] as WebControl;
                        if (!loadImage.Enabled)
                        {
                            foreach (TableCell var in grow.Cells)
                            {
                                DataControlField dcf = (var as DataControlFieldCell).ContainingField;
                                if (dfield.HeaderText == dcf.HeaderText)
                                {
                                    if (var.Controls.Count == 1)
                                    {
                                        var.Controls[0].Visible = false;
                                    }
                                    else
                                        (var as WebControl).Style.Add(HtmlTextWriterStyle.Visibility, "hidden");
                                }
                            }
                        }
                    }
                }
            }
        }
        protected void gvSubmissions_PageIndexChanged(object sender, EventArgs e)
        {
            lbAddress_SelectedIndexChanged(null, System.EventArgs.Empty);
        }
        protected void gvSubmissions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Session["PossibleDuplicates"] = BuildPossibleDuplicates();
            gvSubmissions.PageIndex = e.NewPageIndex;

            DataTable dt = ViewState[key4Searched] as DataTable;
            if (!string.IsNullOrEmpty((string)ViewState[key4PreviousSortExpression]))
                dt.DefaultView.Sort = string.Format("{0} {1}", ViewState[key4PreviousSortExpression], ViewState[key4PreviousSortDirection]);
            else
                dt.DefaultView.Sort = (string)ViewState[key4DefaultSort];
            gvSubmissions.DataSource = dt.DefaultView;
            gvSubmissions.DataBind();

        }
        protected void gvSubmissions_SelectedIndexChanged(object sender, EventArgs e)
        {
            SaveSearch();
            GridView gv = sender as GridView;
            Session["SubmissionId"] = gv.SelectedDataKey.Value;
            Session["SubmissionId"] = gv.SelectedDataKey.Values["id"];
            Session["ClientId"] = gv.SelectedDataKey.Values["ClientCore_Client_Id"];
            Session["subref"] = "Default.aspx";
            if (DefaultValues.UseWizard)
                Response.Redirect("SubmissionWizard.aspx");
            Response.Redirect("Submission.aspx?op=edit");
        }

        protected void gvSubmissions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                SaveSearch();
                gvSubmissions.SelectedIndex = Convert.ToInt32(e.CommandArgument);
                Session["SubmissionId"] = gvSubmissions.SelectedDataKey.Values["id"];
                Session["SubmissionNumber"] = gvSubmissions.SelectedDataKey.Values["submission_number"];
                Session["SubmissionNumberSequence"] = gvSubmissions.SelectedDataKey.Values["submission_number_sequence"];
                Session["subref"] = "Default.aspx";
                Response.Redirect("Reseq.aspx");
            }
        }

        protected void gvAdvClientSrchResults_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            ClientProxy.Client clientproxy = Common.GetClientProxy();
            Structures.Info.ClientInfo client;
            string clientid = "";
            string currentCompanyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);

            Dictionary<string, Structures.Search.Client> clientDictionary = new Dictionary<string, Structures.Search.Client>();
            Core.Clearance.BizObjects.SubmissionList list = new BCS.Core.Clearance.BizObjects.SubmissionList();
            if (e.CommandName == "View")
            {
                Button btn = (Button)e.CommandSource;
                GridViewRow myRow = (GridViewRow)btn.Parent.Parent;
                GridView myGrid = (GridView)sender;

                clientid = myGrid.DataKeys[myRow.RowIndex].Value.ToString();
                if (!string.IsNullOrEmpty(clientid))
                {
                    try
                    {
                        string clientxml = clientproxy.GetClientByIdForSearchScreen(currentCompanyNumber, clientid, true);

                        Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                            (Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                            clientxml, typeof(Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

                        client = clearanceclient.ClientCoreClient;

                        Structures.Search.Client c = new BTS.ClientCore.Wrapper.Structures.Search.Client();
                        c.Info.ClientCategory = client.Client.ClientCategory;
                        c.Info.ClientId = client.Client.ClientId;
                        c.Info.ClientStatus = client.Client.ClientStatus;
                        c.Info.ClientType = client.Client.ClientType;
                        c.Info.CorpId = client.Client.CorpId;
                        c.Info.Segmented = client.Client.SegmentedClient;
                        c.Info.TaxId = client.Client.TaxId;
                        c.Info.TaxIdType = client.Client.TaxIdType;

                        if (client.Addresses != null)
                        {
                            c.Addresses = new Structures.Search.AddressVersion[client.Addresses.Length];

                            int i = 0;
                            foreach (Structures.Info.Address address in client.Addresses)
                            {
                                c.Addresses[i].Address1 = address.Address1;
                                c.Addresses[i].Address2 = address.Address2;
                                c.Addresses[i].Address3 = address.Address3;
                                c.Addresses[i].Address4 = address.Address4;
                                c.Addresses[i].AddressId = address.AddressId;
                                c.Addresses[i].City = address.City;
                                c.Addresses[i].CountryId = address.CountryId;
                                c.Addresses[i].County = address.County;
                                c.Addresses[i].CountyId = address.CountyId;
                                c.Addresses[i].HouseNumber = address.HouseNumber;
                                c.Addresses[i].PostalCode = address.PostalCode;
                                //c.Addresses[i].Primary = address.Primary;
                                c.Addresses[i].StateProvCode = address.StateProvinceId;
                                c.Addresses[i].StateProvId = address.StateProvinceId;
                                i++;
                            }
                        }

                        if (client.Client.Names != null)
                        {
                            c.Names = new BTS.ClientCore.Wrapper.Structures.Search.ClientName[client.Client.Names.Length];

                            int i = 0;
                            foreach (Structures.Info.Name name in client.Client.Names)
                            {
                                c.Names[i].ClientId = name.ClientId;
                                c.Names[i].NameBusiness = name.NameBusiness;
                                c.Names[i].NameFirst = name.NameFirst;
                                c.Names[i].NameLast = name.NameLast;
                                c.Names[i].NameMiddle = name.NameMiddle;
                                c.Names[i].NamePrefix = name.NamePrefix;
                                c.Names[i].NameSuffix = name.NameSuffix;
                                c.Names[i].NameType = name.NameType;
                                c.Names[i].PrimaryNameFlag = name.PrimaryNameFlag;
                                c.Names[i].SequenceNumber = name.SequenceNumber;
                                i++;
                            }
                        }
                        c.Portfolio = client.Client.Portfolio;

                        clientDictionary.Add(c.Info.ClientId, c);
                        LoadSearchResults(clientDictionary);
                        pnlAdvClientSrchResults.Visible = false;
                        phPortfolio.Visible = true;
                        phSubmissions.Visible = true;
                        pnlSearchResults.Visible = true;
                        pnlSearchCriteria.Visible = false;
                        //mdlAdvClientSrchResults.Hide();
                        hdnHideModalPopup.Value = "";
                    }
                    catch (Exception ex) { }
                }
            }
        }

        protected void lbtnExportToExcel_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("content-disposition",
                string.Format("attachment;filename={0}.xls", "Submissions"));
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            DataTable dt = ViewState[key4Searched] as DataTable;
            BCS.Biz.SubmissionDisplayGridCollection dfields = ConfigValues.GetSubmissionGridDisplay(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
            //dfields = dfields.FilterByRemoteAgencyVisible(false);
            GridView gvToExport = new GridView();

            gvToExport.AutoGenerateColumns = false;
            gvToExport.AllowPaging = false;

            BoundField snf = new BoundField();
            snf.DataField = "submission_number";
            snf.HeaderText = "Submission Number";
            gvToExport.Columns.Insert(0, snf);        

            // col submission no sequence
            BoundField snsf = new BoundField();
            snsf.HeaderText = "Sequence";
            snsf.DataField = "submission_number_sequence";
            snsf.HtmlEncode = false;
            gvToExport.Columns.Insert(1, snsf);
            int j = 2;
            foreach (Biz.SubmissionDisplayGrid df in dfields)
            {
                TemplateField atf = new TemplateField();
                atf.ItemTemplate = new Templates.GenericTemplate(DataControlRowType.DataRow, string.Empty, df.FieldName, df.AttributeDataType.DataTypeName, string.Empty);
                atf.HeaderText = string.IsNullOrEmpty(df.HeaderText) ? df.FieldName : df.HeaderText;

                gvToExport.Columns.Insert(j, atf);
                j++;
            }

            gvToExport.DataSource = dt;
            gvToExport.DataBind();

            gvToExport.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
            //string tab = "";
            //foreach (DataColumn dc in dt.Columns)
            //{
            //    Response.Write(tab + dc.ColumnName);
            //    tab = "\t";
            //}
            //Response.Write("\n");
            //int i;
            //foreach (DataRow dr in dt.Rows)
            //{
            //    tab = "";
            //    for (i = 0; i < dt.Columns.Count; i++)
            //    {
            //        Response.Write(tab + dr[i].ToString());
            //        tab = "\t";
            //    }
            //    Response.Write("\n");
            //}
            //Response.End();
            //using (StringWriter sw = new StringWriter())
            //{
            //    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
            //    {
            //        //  Create a table to contain the grid
            //        Table table = new Table();
                   
            //        foreach (GridViewRow row in gvSubmissions.Rows)
            //        {
            //            table.Rows.Add(row);
            //        }

                   
            //        //  render the table into the htmlwriter
            //        table.RenderControl(htw);

            //        //  render the htmlwriter into the response
            //        HttpContext.Current.Response.Write(sw.ToString());
            //        HttpContext.Current.Response.End();
            //    }
            //}
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            //base.VerifyRenderingInServerForm(control);
        }
        protected void btnCreateShellSubmission_Click(object sender, EventArgs e)
        {
            Session.Contents.Remove("SubmissionId");
            if (lbNames.SelectedIndex < 0 && lbAddress.SelectedIndex < 0)
            {
                Common.SetStatus(messagePlaceHolder, "Please select a client to add a submission (shell).");
                return;
            }

            SaveSearch();
            if (lbNames.SelectedIndex > -1)
            {
                System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(@"\^\d*");
                Session["ClientId"] = rgx.Replace(lbNames.SelectedValue, "");
            }
            else
            {
                System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(@"\^\d*");
                Session["ClientId"] = rgx.Replace(lbAddress.SelectedValue, "");
            }
            Session["cliref"] = "Default.aspx";
            Response.Redirect("SubmissionShell.aspx");
        }
        #endregion

        #region Other Methods

        private void LoadSearchResults(Dictionary<string, Structures.Search.Client> clientDictionary)
        {
            LoadSearchResults(clientDictionary, new StringCollection());
        }
        private void LoadSearchResults(Dictionary<string, Structures.Search.Client> clientDictionary, StringCollection messages)
        {
            Session["Clients"] = clientDictionary;
            btnAddClientNew.Visible = false;
            if (clientDictionary.Count == 0)
            {
                if (messages.Count > 0)
                {   
                    Common.SetStatus(messagePlaceHolder, "Too many results returned, please narrow your search criteria.");
                }
                else
                {
                    if (hdnAdvSrchState.Value.Length <= 0)
                    {
                        btnAddClientNew.Visible = true;
                        int currentCompanyId = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
                        if(currentCompanyId == 3)
                         Common.SetStatus(messagePlaceHolder, "Unable to return results based on the specified search criteria. Please try again.");
                        else
                         Common.SetStatus(messagePlaceHolder, "No results returned for the specified search criteria. Please try again.");
                    }
                }
                btnEditClient.Visible = false;
                btnCreateShellSubmission.Visible = false;
                pnlSearchResults.Visible = false;
                phSubmissions.Visible = false;
                phPortfolio.Visible = false;
                pnlSearchCriteria.Visible = true;

                Control ctr = Common.FindControlRecursive(this, "txBusinessName");
                if (ctr != null)
                    ctr.Focus();

                return;
            }
            btnEditClient.Visible = true;
            btnCreateShellSubmission.Visible = ConfigValues.SupportsShellSubmissions(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

            ListItemCollection names = new ListItemCollection();

            foreach (Structures.Search.Client clientproxy in clientDictionary.Values)
            {
                int i = 0;
                foreach (Structures.Search.ClientName name in clientproxy.Names)
                {
                    if (name.NameBusiness != null && name.NameBusiness.Length > 0)
                    {
                        names.Add(new ListItem(string.Format("{0} ({1}{2})", name.NameBusiness, clientproxy.Info.ClientId, (name.PrimaryNameFlag == "Y") ? " - Primary" : ""),
                            string.Format("{0}^{1}", clientproxy.Info.ClientId, i)));
                    }
                    else
                    {
                        names.Add(new ListItem(string.Format("{0} {1} {2} ({3}{4})", name.NameLast, name.NameMiddle, name.NameFirst, clientproxy.Info.ClientId, (name.PrimaryNameFlag == "Y") ? " - Primary" : ""),
                            string.Format("{0}^{1}", clientproxy.Info.ClientId, i)));
                    }
                    i++;
                }
            }
            ListItem[] nameItems = new ListItem[names.Count];
            names.CopyTo(nameItems, 0);
            nameItems = Common.SortListItems(nameItems);
            lbNames.Items.Clear();
            lbNames.Items.AddRange(nameItems);

            phSubmissions.Visible = lbNames.SelectedIndex > -1;
            phPortfolioInfo.Visible = lbNames.SelectedIndex > -1;

            if (clientDictionary.Count == 1)
            {
                lbNames.SelectedIndex = 0;
                lbNames_SelectedIndexChanged(lbNames, EventArgs.Empty);
            }
        }
        private void LoadAddresses(string p)
        {
            ListItemCollection addresses = new ListItemCollection();

            Dictionary<string, Structures.Search.Client> ht = Session["Clients"] as Dictionary<string, Structures.Search.Client>;
            Structures.Search.Client clientproxy = (Structures.Search.Client)ht[p];

            BCS.Biz.DataManager dm = new DataManager(DefaultValues.DSN);
            dm.QueryCriteria.And(Biz.JoinPath.CompanyParameter.Columns.CompanyID, Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            bool displayReferralAttribute = false;
            CompanyParameterCollection companyParameters = dm.GetCompanyParameterCollection();

            if (companyParameters != null && companyParameters.Count > 0)
                displayReferralAttribute = true;

            if (clientproxy.Addresses != null)
            {
                if (displayReferralAttribute)
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(Biz.JoinPath.Client.Columns.ClientCoreClientId,clientproxy.Info.ClientId);
                    dm.QueryCriteria.And(Biz.JoinPath.Client.Columns.CompanyNumberId, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));

                    Biz.Client clearanceClient = dm.GetClient(Biz.FetchPath.Client.ClientCompanyNumberAttributeValue,Biz.FetchPath.Client.ClientCompanyNumberAttributeValue.CompanyNumberAttribute);
                    StringBuilder sb = new StringBuilder();

                    if (clearanceClient != null)
                    {    
                        foreach(Biz.ClientCompanyNumberAttributeValue sa in clearanceClient.ClientCompanyNumberAttributeValues)
                        {
                            if (sa.CompanyNumberAttribute.ViewWithSubmissionAddress && sa.AttributeValue.ToUpper() != "FALSE")
                            {
                                sb.Append(string.Format("{0}-{1} ", sa.CompanyNumberAttribute.Label, sa.AttributeValue));
                            }
                        }
                    }

                    foreach (Structures.Search.AddressVersion address in clientproxy.Addresses)
                    {
                        addresses.Add(new ListItem(string.Format("{0} {1} {2} {3} {4} {5} ({6}) {7}",
                           address.HouseNumber, address.Address1, address.Address2, address.City, address.StateProvCode,
                           address.PostalCode, clientproxy.Info.ClientId, sb.ToString().Trim()), address.AddressId));
                    }

                }
                else
                {
                    foreach (Structures.Search.AddressVersion address in clientproxy.Addresses)
                    {
                        addresses.Add(new ListItem(string.Format("{0} {1} {2} {3} {4} {5} ({6})",
                           address.HouseNumber, address.Address1, address.Address2, address.City, address.StateProvCode,
                           address.PostalCode, clientproxy.Info.ClientId), address.AddressId));
                    }
                }
            }

            ListItem[] addrItems = new ListItem[addresses.Count];
            addresses.CopyTo(addrItems, 0);
            lbAddress.Items.Clear();
            lbAddress.Items.AddRange(addrItems);
        }

        private string BuildPossibleDuplicates()
        {
            if (!ConfigValues.SupportsDuplicateOnSubmissionGrid(Common.GetSafeIntFromSession("CompanySelected")))
            { return string.Empty; }
            System.Text.StringBuilder dups = new System.Text.StringBuilder();
            if (Session["PossibleDuplicates"] != null)
            {
                dups.Append(Session["PossibleDuplicates"].ToString());
            }
            foreach (GridViewRow row in gvSubmissions.Rows)
            {
                CheckBox cb = row.FindControl("cbDup") as CheckBox;
                if (cb.Checked)
                {
                    DataKey dk = gvSubmissions.DataKeys[row.RowIndex];
                    //string entry = string.Format(
                    //    "Submission Number : {0}-{1}, Policy Number : {2}, Status : {3}, Status Date : {4:MM/dd/yyyy}"
                    //    , dk.Values["submission_number"], dk.Values["submission_number_sequence"],
                    //    dk.Values["policy_number"],
                    //    dk.Values["submission_status_code"],
                    //    GetSafeDateTimeString(dk.Values["submission_status_date"]));

                    string entry = string.Format(
                        "Policy #: {0}", dk.Values["policy_number"]);

                    if (!dups.ToString().Contains(entry))
                        dups.AppendFormat("{0}; ", entry);
                }
            }
            Session["PossibleDuplicates"] = dups;
            return dups.ToString();
        }


        private void SaveSearch()
        {
            SavedSearch saveSearch = new SavedSearch();
            saveSearch.PolicyNumber = ClientSearch1.PolicyNumber;
            saveSearch.TaxIds = ClientSearch1.TaxIds;
            saveSearch.PortfolioIds = ClientSearch1.PortfolioIds;
            saveSearch.SelectedClientId = lbNames.SelectedValue;
            saveSearch.SelectedClientAddressId = lbAddress.SelectedValue;

            saveSearch.SearchedClient = new ClientSearch();
            saveSearch.SearchedClient.Info = new BTS.ClientCore.Wrapper.Structures.Search.ClientInfo();
            saveSearch.SearchedClient.Info.ClientId = ClientSearch1.ClientId;
            saveSearch.SearchedClient.Names = new ClientNameSearch[ClientSearch1.ClientNamesToSearch.Count];
            saveSearch.SearchedClient.Addresses = new BTS.ClientCore.Wrapper.Structures.Search.AddressVersion[ClientSearch1.AddressVersionsToSearch.Count];
            ClientSearch1.ClientNamesToSearch.CopyTo(saveSearch.SearchedClient.Names);
            ClientSearch1.AddressVersionsToSearch.CopyTo(saveSearch.SearchedClient.Addresses);

            Session["SavedSearch"] = saveSearch;
        }


        private string GetSafeDateTimeString(object p)
        {
            try
            {
                DateTime safeDateTime = Convert.ToDateTime(p);
                if (safeDateTime == DateTime.MinValue)
                    return string.Empty;

                return safeDateTime.ToString("MM/dd/yyyy");
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion
        protected void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
        {
            throw new ApplicationException("Ajax Error", e.Exception);
        }

        #region modal popup for submission history
        protected void Panel1_Load(object sender, EventArgs e)
        {
            string sid = gvSubmissions.DataKeys[((sender as Control).NamingContainer as GridViewRow).RowIndex].Values["id"].ToString();
            ObjectDataSource1.SelectParameters["submissionId"].DefaultValue = sid;

            lblSubmissionNumber.Text = string.Format("Submission # : {0}-{1}", gvSubmissions.DataKeys[((sender as Control).NamingContainer as GridViewRow).RowIndex].Values["submission_number"],
                gvSubmissions.DataKeys[((sender as Control).NamingContainer as GridViewRow).RowIndex].Values["submission_number_sequence"]);
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView historyGrid = sender as GridView;
            if (historyGrid.SelectedIndex == -1)
                return;
            string result = BCS.Core.Clearance.Submissions.ToggleSubmissionStatusHistoryActive(Convert.ToInt32(historyGrid.SelectedDataKey["Id"]), Common.GetUser());
            Common.SetStatus(HistoryStatusPH, result, true);
            historyGrid.SelectedIndex = -1;
            historyGrid.DataBind();

            gvSubmissions_PageIndexChanging(gvSubmissions, new GridViewPageEventArgs(gvSubmissions.PageIndex));
            gvSubmissions_PageIndexChanged(gvSubmissions, EventArgs.Empty);
           
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                SubmissionStatusHistory aHistory = e.Row.DataItem as SubmissionStatusHistory;
                if (!aHistory.Active)
                {
                    foreach (TableCell var in e.Row.Cells)
                    {
                        var.Style.Add(HtmlTextWriterStyle.TextDecoration, "line-through");
                    }
                }

                LinkButton lb = e.Row.FindControl("LinkButton1") as LinkButton;
                ConfirmButtonExtender confirmButtonExtender = e.Row.FindControl("ConfirmButtonExtender1") as ConfirmButtonExtender;
                if (lb != null)
                {
                    lb.Text = aHistory.Active ? "active" : "inactive";
                    lb.Enabled = BCS.WebApp.Components.Security.CanActivateStatus() && (aHistory.Active.Value || User.IsInRole("System Administrator"));
                }
                if (confirmButtonExtender != null)
                {
                    confirmButtonExtender.Enabled = BCS.WebApp.Components.Security.CanActivateStatus() && (aHistory.Active.Value || User.IsInRole("System Administrator"));
                    if (User.IsInRole("System Administrator"))
                    {
                        confirmButtonExtender.ConfirmText = "Are you sure you want to change this status history?";
                    }
                }
            }
        }
        #endregion

        protected void PortfolioTree_SelectedNodeChanged(object sender, EventArgs e)
        {
            pnlSearchCriteria.Visible = false;
            pnlSearchResults.Visible = true;
            phPortfolio.Visible = true;

            StringCollection searchMessages = new StringCollection();
            Dictionary<string, Structures.Search.Client> clientDictionary = new Dictionary<string, Structures.Search.Client>();

            TreeView treeView = sender as TreeView;
            string portfolioId = treeView.SelectedValue;

            ClientProxy.Client clientproxy = Common.GetClientProxy();

            string portSearchXML = clientproxy.GetClientsByPortfolioId(Common.GetSafeStringFromSession(SessionKeys.CompanyNumber), portfolioId);
            Structures.Search.Vector vector = (Structures.Search.Vector)BCS.Core.XML.Deserializer.Deserialize(portSearchXML, typeof(Structures.Search.Vector));

            if (vector.Clients != null && vector.Clients.Length > 0)
                foreach (Structures.Search.Client c in vector.Clients)
                    if (!clientDictionary.ContainsKey(c.Info.ClientId))
                        clientDictionary.Add(c.Info.ClientId, c);

            if (vector.Error != null)
            {
                searchMessages.Add(string.Format(
                    "Too many results returned for search on address : portfolio id - {0}. Please narrow your search criteria.",
                    portfolioId));
            }
            LoadSearchResults(clientDictionary, searchMessages);
        }


        private void LoadPortfolio(string p)
        {
            BCS.Biz.DataManager dm = new DataManager(DefaultValues.DSN);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyParameter.Columns.CompanyID, Common.GetSafeIntFromSession(SessionKeys.CompanyId));

            BCS.Biz.CompanyParameter companyParameter = dm.GetCompanyParameter();
            
            Dictionary<string, Structures.Search.Client> ht = Session["Clients"] as Dictionary<string, Structures.Search.Client>;
            Structures.Search.Client client = (Structures.Search.Client)ht[p];
            phPortfolio.Visible = (!string.IsNullOrEmpty(client.Portfolio.Id) || (bool)companyParameter.AlwaysDisplayPortfolio);
            phPortfolioInfo.Visible = (!string.IsNullOrEmpty(client.Portfolio.Id) || (bool)companyParameter.AlwaysDisplayPortfolio);
            lblPortfolioId.Text = client.Portfolio.Id;
            lblFEIN.Text = (!String.IsNullOrEmpty(client.Info.TaxId)) ? client.Info.TaxId : "(none)";
            lnkPortfolioName.Text = (string.IsNullOrEmpty(client.Portfolio.Name)) ? "(not specified)" : client.Portfolio.Name;
            lnkPortfolioName.Enabled =  (User as BCS.Core.Security.CustomPrincipal).IsInAnyRoles("Portfolio Administrator", "System Administrator");
            popupPortfolioEdit.Enabled = lnkPortfolioName.Enabled;
            TextBox1.Text =  client.Portfolio.Name;
            CompareValidator1.ValueToCompare = client.Portfolio.Name;
            lblClientCountinPortfolio.Text =  client.Portfolio.ClientCount;

        }
        protected void UpdatePortFolio(object sender, EventArgs e)
        {
            SaveSearch();
            Session["LoadSavedSearch"] = true;
            ClientProxy.Client clientProxy = new ClientProxy.Client();
            string result = clientProxy.UpdatePortfolio(Common.GetSafeStringFromSession(SessionKeys.CompanyNumber), lblPortfolioId.Text, TextBox1.Text);
            Response.Redirect("Default.aspx");
        }
        protected void HyperLink1_Init(object sender, EventArgs e)
        {
            HyperLink2.Visible = (User as BCS.Core.Security.CustomPrincipal).IsInAnyRoles("User", "Partial User", "Auditor", "System Administrator");
        }

        protected void pnlNotification_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    BCS.Biz.DataManager dm = new DataManager(DefaultValues.DSN);
                    dm.QueryCriteria.And(Biz.JoinPath.Notification.Columns.StartDate, DateTime.Now, MatchType.LesserOrEqual);
                    dm.QueryCriteria.And(Biz.JoinPath.Notification.Columns.EndDate, DateTime.Now, MatchType.GreaterOrEqual);

                    NotificationCollection notes = dm.GetNotificationCollection();

                    if (notes != null && notes.Count > 0)
                    {
                        pnlNotification.Visible = true;
                        lbNotification.Text = notes[0].Description;
                        if (!string.IsNullOrWhiteSpace(notes[0].Link))
                        {
                            hlNotification.NavigateUrl = notes[0].Link;
                            hlNotification.Text = "Enhancement List";
                        }
                    }
                    else
                    {
                        pnlNotification.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    //do nothing
                }
            }
        }

        public string CheckForNameFormating(string nameToFormat)
        {
            string nameToModify = nameToFormat;
            string[] nameArray = nameToModify.Split(' ');
            
            for (int i = 0; i < nameArray.Length-1; i++)
            {
                string currName = nameArray[i];
                string nextName = nameArray[i + 1];
                string newName = "";
                if (currName.Length == 1)
                {
                    if ((currName.ToUpper() == "O") && (nextName.Length > 1))
                    {
                        newName = currName + "'" + nextName;
                        nameToModify =  nameToModify.Replace(currName + ' ' + nextName, newName);
                    }
                }
            }

            nameArray = nameToModify.Split(' ');
            for (int i = 0; i < nameArray.Length-1; i++)
            {
                string currName = nameArray[i];
                string nextName = nameArray[i + 1];
                string newName = "";
                if (nextName.Length == 1)
                {
                    if ((nextName.ToUpper() == "S") && (currName.Length > 1))
                    {
                        newName = currName + "'" + nextName.ToLower();
                        nameToModify =  nameToModify.Replace(currName + ' ' + nextName, newName);
                    }
                }
            }
            return nameToModify;
        }

   }
}
