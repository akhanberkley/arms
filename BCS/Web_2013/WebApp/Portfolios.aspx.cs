using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Portfolios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        gridPortfolios.SelectedIndex = -1;
        detPortfolio.ChangeMode(DetailsViewMode.ReadOnly);
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        gridPortfolios.SelectedIndex = -1;
        detPortfolio.ChangeMode(DetailsViewMode.Insert);
    }
}
