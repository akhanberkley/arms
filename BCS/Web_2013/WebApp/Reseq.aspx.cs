using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.Core.Clearance.Admin;

public partial class ReSeq : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txNewSubBo.Text = Common.GetSafeStringFromSession("SubmissionNumber");
            txNewSeq.Text = Common.GetSafeStringFromSession("SubmissionNumberSequence");
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Int64 numNewSubNo = 0;
        if (!string.IsNullOrEmpty(txNewSubBo.Text))
            numNewSubNo = Convert.ToInt64(txNewSubBo.Text);
        int numNewSeq = 0;
        if (!string.IsNullOrEmpty(txNewSeq.Text))
            numNewSeq = Convert.ToInt32(txNewSeq.Text);
        
        int submissionId = Common.GetSafeIntFromSession("SubmissionId");

        string result = DataAccess.ReSequenceSubmission(submissionId, numNewSubNo, numNewSeq, Common.GetUser());
        if (result == BCS.Core.Clearance.Messages.SuccessfulReSequenceSubmission)
        {
            ClearSessionVarsUsed();

            Session["StatusMessage"] = result;
            Session["LoadSavedSearch"] = true;
            Cache.Remove("SearchedCacheDependency");
            Response.Redirect("Default.aspx");
        }
        else
            Common.SetStatus(this.messagePlaceHolder, result, true);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearSessionVarsUsed();
        Session["LoadSavedSearch"] = true;
        Response.Redirect("Default.aspx");
    }

    private void ClearSessionVarsUsed()
    {

        Session.Contents.Remove("SubmissionId");
        Session.Contents.Remove("SubmissionNumber");
        Session.Contents.Remove("SubmissionNumberSequence");

    }
}
