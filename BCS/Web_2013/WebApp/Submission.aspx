<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" Async="true" CodeFile="Submission.aspx.cs" Inherits="Submission" Title="Berkley Clearance System : Submission" Theme="DefaultTheme" MaintainScrollPositionOnPostback="False" ValidateRequest="False" %>

<%@ Register Src="WebUserControls/AutoNumberGenerator.ascx" TagName="AutoNumberGenerator"
    TagPrefix="uc1" %>
    <%@ Register Src="WebUserControls/LOBList.ascx" TagName="LOBList" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/Site.PATS.master" %>
<%@ Import Namespace="BCS.WebApp.Components" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript">

        function defaultExpirationDate(strDate) {            
            var d = new Date(strDate);
            if (isNaN(d))
            {
                return "";
            }
            var nd = new Date(d.getYear()+1, d.getMonth(), d.getDate());
             var s = (d.getMonth() + 1).toString().length == 1 ? "0" + (d.getMonth() + 1) + "/" : (d.getMonth() + 1) + "/" ;
                s += d.getDate().toString().length == 1 ? "0" + d.getDate() + "/" : d.getDate() + "/" ;
                s += d.getFullYear() + 1;
                alert(s);
            return s;            
        }
        
        function openAgencySearch(ctl, c, dt)
        {
            openWindow('SubmissionAgencySearch.aspx?ctl='+ctl+'&c='+c+'&dt='+dt,'SubmissionAgencySearch',700,630);
        }
        function setValueToTextBox(elemSelect,elemTextBox)
        {
            try { elemTextBox.value = elemSelect.value; } catch(err) {};
        }
        function stamp()
        {
            var txElement = document.getElementById('<%=txComments.ClientID%>');
            var txAddElement = document.getElementById('<%=txAddComments.ClientID%>');
            
            var oldValue = txElement.value;
            var d = new Date();
            var formattedDate = (d.getMonth() + 1).toString().length == 1 ? "0" + (d.getMonth() + 1) + "/" : (d.getMonth() + 1) + "/" ;
                formattedDate += d.getDate().toString().length == 1 ? "0" + d.getDate() + "/" : d.getDate() + "/" ;
                formattedDate += d.getFullYear();
            var newStamp = "[" + d.toLocaleTimeString() + " " + formattedDate + " " + document.getElementById('<%=hdnUser.ClientID%>').value + "]  \r\r";
            var newValue = newStamp + txAddElement.value + "\r\r" + oldValue;
            txElement.value = newValue;
            txAddElement.value = '';
            document.getElementById('btnAddComment').disabled = true;
            
            if (!txElement.disabled)
                txElement.focus();                
            if (txElement.createTextRange)
            {
                var FieldRange = txElement.createTextRange();
                FieldRange.moveStart('character', newStamp.length-2);
                FieldRange.collapse();
                FieldRange.select();
            }
            else
            {
                txElement.setSelectionRange(newStamp.length-2,newStamp.length-2);
            }
            
            txElement.removeAttribute('disabled');
            }
            function checkAllCBL(me, cblid)
            {
               var cblist = document.getElementById (cblid);
               var cboxes = cblist.getElementsByTagName("input");
               for(var i=0;i<cboxes.length;i++)
                {
                    cboxes[i].checked = me.checked;
                }                
            }  
            function TBDChanged(cb, assocValidator)
            {
                ValidatorEnable(document.getElementById(assocValidator), !document.getElementById(cb).checked);
            }
    </script>
    <ajx:ToolkitScriptManager id="ScriptManager1" runat="server" CombineScripts="True" OnAsyncPostBackError="ScriptManager1_AsyncPostBackError"></ajx:ToolkitScriptManager>
    <script language="javascript" type="text/javascript">
        <!-- 
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        var oldClassName = theForm.className;
        function CancelIfInAsyncPostBack() {
            if (prm.get_isInAsyncPostBack()) {
              prm.abortPostBack();
            }
        }
        prm.add_initializeRequest(InitializeRequest);
        prm.add_endRequest(EndRequest);
        var postBackElement;
        function InitializeRequest(sender, args) {
//            if (prm.get_isInAsyncPostBack()) {
//                args.set_cancel(true);
//            }
            CancelIfInAsyncPostBack();
            
            var pop = $find("ctl00_ContentPlaceHolder1_mpeUpdateProgress");
            //pop.disableTab();
            pop.show();
        }
       
        function EndRequest(sender, args) {
            var pop = $find("ctl00_ContentPlaceHolder1_mpeUpdateProgress");
            pop.hide();
            
        }
        
        // -->
        </script>
        
    <aje:UpdatePanel ID="UpdatePanel81" runat="server">
        <ContentTemplate>
            <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ></asp:ValidationSummary>
                <br />
            </asp:PlaceHolder>            
        </ContentTemplate>
    </aje:UpdatePanel>
    
    <span class="heading">Submission <asp:PlaceHolder ID="phSubmissionNumber" runat="server" Visible="false">
        (#<asp:Label ID="txSubmissionNumber" runat="server"></asp:Label>
        <img alt="copy" runat="server" id="copy" src='<%$ Code : Common.GetImageUrl("copy.gif") %>' title="Copy Submission Number to clipboard" />-<asp:Label ID="txSubmissionNumberSequence" runat="server"></asp:Label>)
        &nbsp;
        <asp:Button ID="btnCopy" runat="server" Visible="false" Text="Copy" Enabled='<%$ Code: BCS.WebApp.Components.Security.CanUserAdd() %>' OnClick="btnCopy_Click" />
        </asp:PlaceHolder> : <asp:Literal ID="lblOperation" runat="server"></asp:Literal>
        <br />
        <asp:PlaceHolder ID="phSubmissionDate" runat="server" Visible="false">
            <span style="font-size:11px;">Submission Created Date: <asp:Literal ID="lblSubmissionDate" runat="server"></asp:Literal></span>
        </asp:PlaceHolder>
    </span>
    <asp:Panel ID="pnlAddSubmission" runat="server" DefaultButton="btnSubmitSubmission">
     <aje:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" >
        <ProgressTemplate>
            <asp:Panel ID="PNLProgress" runat="server" style="width:400px; background-color:White; border-width:2px; border-color:Black; border-style:solid; padding:20px;">
                <img id="processing" runat="server" alt="processing" src='<%$ Code : Common.GetImageUrl("indicator.gif") %>' /> &nbsp; Processing... Please Wait....
            </asp:Panel>
            <ajx:ModalPopupExtender BackgroundCssClass="modalBackground" ID="mpeUpdateProgress" TargetControlID="PNLProgress" runat="server" PopupControlID="UpdateProgress1">
    </ajx:ModalPopupExtender>
        </ProgressTemplate>
    </aje:UpdateProgress>
    
        <table cellpadding="0" cellspacing="0" class="box">
            <tbody>
            <tr>
                <td class="TL"><div /></td>
                <td class="TC">Details</td>
                <td class="TR"><div /></td>
            </tr>
            <tr>
                <td class="ML"><div /></td>
                <td class="MC">
                    <table border="0" cellpadding="5" cellspacing="0">
                        <tr>
                            <td id="TabIndex_Section_10" runat="server" valign="top" >
                                <table cellpadding="5" cellspacing="0" style="border-right: black 1px solid; border-top: black 1px solid; border-left: black 1px solid; border-bottom: black 1px solid;">
                                   <tr id="submission_row_submission_type">
                                        <td>Submission Type:<br /><UserControls:SubmissionTypeDropDownList id="SubmissionTypeDropDownList1" runat="server" AutoPostBack="False"></UserControls:SubmissionTypeDropDownList>
                                            <UserControls:RFV ID="rfvSubmissionType" runat="server" ControlToValidate="SubmissionTypeDropDownList1"
                                                ErrorMessage="&nbsp;" Text="*" SetFocusOnError="True" InitialValue="0" Display="None"></UserControls:RFV></td>
                                    </tr>
                                    <tr id="submission_row_effective_date">
                                        <td>Effective Date:<br />
                                            <aje:UpdatePanel ID="upEffectiveDate" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:TextBox id="txEffectiveDate" runat="server" AutoPostBack="True" AutoCompleteType="Disabled" OnTextChanged="txEffectiveDate_TextChanged"></asp:TextBox>
                                                <asp:CheckBox OnInit="cbTBD_Init" id="cbTBD" runat="server" AutoPostBack="True" Text="TBD" CssClass="submission_row_effective_date_TBD" OnCheckedChanged="cbTBD_CheckedChanged"></asp:CheckBox>
                                                <UserControls:RFV id="rfvEffectiveDate" runat="server" Display="None" ErrorMessage="&nbsp;" ControlToValidate="txEffectiveDate" SetFocusOnError="True" Text="*"></UserControls:RFV>
                                                <asp:CompareValidator id="CompareValidator1" runat="server" Display="Dynamic" ErrorMessage="&nbsp;" ControlToValidate="txEffectiveDate" SetFocusOnError="True" Text="* mm/dd/yyyy" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                                            </ContentTemplate>
                                            </aje:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr id="submission_row_expiration_date">
                                        <td>Expiration Date:<br />
                                            <aje:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                <asp:TextBox ID="txExpirationDate" runat="server" AutoCompleteType="Disabled"></asp:TextBox>                                                
                                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txExpirationDate"
                                                    Display="Dynamic" ErrorMessage="&nbsp;" Text="* mm/dd/yyyy" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ></asp:CompareValidator>
                                                <asp:CompareValidator ID="cvEffExp" runat="server" ControlToValidate="txExpirationDate" ControlToCompare="txEffectiveDate"
                                                    Display="Dynamic" ErrorMessage="&nbsp;" Text="* cannot be greater than effective date." SetFocusOnError="True" Operator="GreaterThan" Type="Date"></asp:CompareValidator>                                                    
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aje:AsyncPostBackTrigger ControlID="txEffectiveDate" />
                                                    <aje:AsyncPostBackTrigger ControlID="cbTBD" />
                                                </Triggers>
                                            </aje:UpdatePanel>
                                         </td>
                                    </tr>
                                    <tr id="submission_row_agency">
                                        <td>Agency:<br />
                                            <aje:UpdatePanel ID="upAgency" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <UserControls:AgencyCityStateDropDownList id="AgencyDropDownList1" runat="server" SortBy="AgencyName" AutoPostBack="True" OnSelectedIndexChanged="AgencyDropDownList1_SelectedIndexChanged" ActiveOnly="True"></UserControls:AgencyCityStateDropDownList>
                                                    <asp:ImageButton ID="ibSearchAgencies" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("lookup.gif") %>' AlternateText="Click this icon to search for an agency" />
                                                    <UserControls:RFV ID="rfvAgency" runat="server" ControlToValidate="AgencyDropDownList1" ErrorMessage="&nbsp;" Text="*" InitialValue="0" SetFocusOnError="True" Display="None"></UserControls:RFV>
                                                    <ajx:ListSearchExtender ID="ListSearchExtender2" runat="server"
                                                        TargetControlID="AgencyDropDownList1" SkinID="WithMargin">
                                                    </ajx:ListSearchExtender>
                                                    <br />
                                                    <asp:Label runat="server" ID="lblAgencyComment" CssClass="ImpInfo" Visible="false"></asp:Label>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aje:AsyncPostBackTrigger ControlID="txEffectiveDate" EventName="TextChanged" />
                                                    <aje:AsyncPostBackTrigger ControlID="cbTBD" />
                                                </Triggers>
                                            </aje:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr id="submission_row_agent">
                                        <td>Agent:<br />
                                            <aje:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <UserControls:AgentDropDownList id="AgentDropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="AgentDropDownList1_SelectedIndexChanged"></UserControls:AgentDropDownList>
                                                    <br />
                                                    <div id="submission_row_agent_reasons">
                                                        <UserControls:AgentUnspecifiedReasonsCheckBoxList id="cblAgentUnspecifiedReasons" runat="server">
                                                        </UserControls:AgentUnspecifiedReasonsCheckBoxList>
                                                        <UserControls:RequiredFieldValidatorForCheckBoxLists id="rfvAgentReasons" runat="server" SetFocusOnError="true" 
                                                            ErrorMessage="&nbsp;" Text="* Agent unspeficied Reason is required." ControlToValidate="cblAgentUnspecifiedReasons" OnInit="rfvAgentReasons_Init" 
                                                            Display="Dynamic">
                                                        </UserControls:RequiredFieldValidatorForCheckBoxLists>
                                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aje:AsyncPostBackTrigger ControlID="txEffectiveDate" EventName="TextChanged" />
                                                    <aje:AsyncPostBackTrigger ControlID="cbTBD" />
                                                    <aje:AsyncPostBackTrigger ControlID="AgencyDropDownList1"></aje:AsyncPostBackTrigger>
                                                    <aje:AsyncPostBackTrigger ControlID="LineOfBusinessDropDownList1"></aje:AsyncPostBackTrigger>
                                                </Triggers>
                                            </aje:UpdatePanel>
                                            </td>
                                    </tr>
                                    <tr id="submission_row_contact">
                                        <td>Contact:<br />
                                            <aje:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <UserControls:ContactDropDownList id="ContactDropDownList1" runat="server"></UserControls:ContactDropDownList>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aje:AsyncPostBackTrigger ControlID="txEffectiveDate" EventName="TextChanged" />
                                                    <aje:AsyncPostBackTrigger ControlID="cbTBD" />
                                                    <aje:AsyncPostBackTrigger ControlID="AgencyDropDownList1" />
                                                    <aje:AsyncPostBackTrigger ControlID="LineOfBusinessDropDownList1" />
                                                </Triggers>
                                            </aje:UpdatePanel>
                                            </td>
                                    </tr>
                                </table>
                            </td>
                            <td id="TabIndex_Section_30" runat="server" valign="top">
                                <table cellpadding="5" cellspacing="0" style="border: #666 1px solid;">
                                    <tr id="submission_row_underwriting_unit">
                                        <td>Underwriting Unit:<br />
                                            <aje:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <UserControls:LineOfBusinessDropDownList ID="LineOfBusinessDropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="LineOfBusinessDropDownList1_SelectedIndexChanged"></UserControls:LineOfBusinessDropDownList>
                                                    <UserControls:RFV ID="rfvLineOfBusiness" runat="server" ErrorMessage="&nbsp;" Text="*" Display="None" ControlToValidate="LineOfBusinessDropDownList1" SetFocusOnError="True" InitialValue="0"></UserControls:RFV>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aje:AsyncPostBackTrigger ControlID="txEffectiveDate" EventName="TextChanged" />
                                                    <aje:AsyncPostBackTrigger ControlID="cbTBD" />
                                                    <aje:AsyncPostBackTrigger ControlID="AgencyDropDownList1" />
                                                </Triggers>
                                            </aje:UpdatePanel>
                                            </td>
                                    </tr>       
                                    <tr id="submission_row_underwriter">
                                        <td>Underwriter:<br />
                                            <aje:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <UserControls:UnderwritersDropDownList ID="UnderwritersDropDownList1" Role="Underwriter" runat="server"
                                                     AutoPostBack="False" ActiveOnly="True" FillAll='<%$ Code: DefaultValues.FillAllUnderwriters(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'>
                                                    </UserControls:UnderwritersDropDownList>
                                                    <UserControls:RFV ID="rfvUnderwriter" runat="server" ControlToValidate="UnderwritersDropDownList1" Display="None" InitialValue="0" ErrorMessage="&nbsp;" Text="*" SetFocusOnError="True"></UserControls:RFV>
                                                    <br /><asp:Label ID="lblObsoleteUnderwriter" runat="server" CssClass="Warning"></asp:Label>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aje:AsyncPostBackTrigger ControlID="txEffectiveDate" />
                                                    <aje:AsyncPostBackTrigger ControlID="cbTBD" />
                                                    <aje:AsyncPostBackTrigger ControlID="AgencyDropDownList1" />
                                                    <aje:AsyncPostBackTrigger ControlID="LineOfBusinessDropDownList1" />
                                                </Triggers>
                                            </aje:UpdatePanel>    
                                            </td>
                                    </tr>
                                    <tr id="submission_row_underwriter_previous" runat="server" class="submission_row_underwriter_previous">
                                        <td>Previous Underwriter:<br />
                                            <asp:TextBox ID="PreviousUnderwriter" runat="server" Enabled="False" EnableViewState="false" ReadOnly="True"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="submission_row_analyst">
                                        <td>Assistant/Analyst:<br />
                                            <aje:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <UserControls:UnderwritersDropDownList ID="AnalystDropDownList1" Role="Analyst" runat="server" AutoPostBack="False" ActiveOnly="True"></UserControls:UnderwritersDropDownList>
                                                    <br /><asp:Label ID="lblObsoleteAnalyst" runat="server" CssClass="Warning"></asp:Label>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aje:AsyncPostBackTrigger ControlID="txEffectiveDate" EventName="TextChanged" />
                                                    <aje:AsyncPostBackTrigger ControlID="cbTBD" />
                                                    <aje:AsyncPostBackTrigger ControlID="AgencyDropDownList1" />
                                                    <aje:AsyncPostBackTrigger ControlID="LineOfBusinessDropDownList1" />
                                                </Triggers>
                                            </aje:UpdatePanel>
                                            </td>
                                    </tr>
                                    <tr id="submission_row_policy_system">
                                        <td>Policy System:<br />
                                            <ajx:TabContainer ID="TabContainer3" runat="server" ActiveTabIndex="0" SkinID="StandAloneTab">        
                                            <ajx:TabPanel ID="TabPanel3" runat="server" >
                                                <ContentTemplate>
                                                    <aje:UpdatePanel id="upPSys" runat="server" UpdateMode="Conditional">                                                    
                                                        <ContentTemplate>
                                                            <UserControls:PolicySystemDropDownList ID="PolicySystemDropDownList1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PolicySystemDropDownList1_SelectedIndexChanged">
                                                        </UserControls:PolicySystemDropDownList>
                                                    </ContentTemplate>
                                                    </aje:UpdatePanel>
                                                </ContentTemplate>
                                            </ajx:TabPanel>
                                            </ajx:TabContainer>
                                            </td>
                                    </tr>
                                    <tr id="submission_row_policy_number">
                                        <td>Policy Number:<br />
                                            <ajx:TabContainer ID="TabContainer2" runat="server" ActiveTabIndex="0" SkinID="StandAloneTab">        
                                            <ajx:TabPanel ID="TabPanel2" runat="server" >
                                                <ContentTemplate>
                                                    <aje:UpdatePanel id="upPNGen" runat="server">
                                                        <ContentTemplate>
                                                            <uc1:AutoNumberGenerator ID="txPolicyNumber" runat="server" NumberControlTypeName="Policy Number" ReadableText="Policy Number"
                                                             Required='<%$Code: ConfigValues.RequiresPolicyNumber(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                                                             FreehandOnly='<%$Code: ConfigValues.FreehandOnlyPolicyNumber(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                                                             ShowStaticAsterisk="True" GeneratedType="PolicyNumber" />
                                                             <asp:CustomValidator id="customPolicyNumberValidator" runat="server"
                                                              OnServerValidate="customPolicyNumberValidator_ServerValidate"></asp:CustomValidator> 
                                                        </ContentTemplate>
                                                    </aje:UpdatePanel>
                                                </ContentTemplate>
                                            </ajx:TabPanel>
                                            </ajx:TabContainer>
                                        </td>
                                    </tr>
                                    <tr id="submission_row_insured_name">
                                        <td><asp:Label ID="lbInsuredName" Text="Insured Name:" runat="server"></asp:Label><br />
                                            <asp:TextBox ID="txInsuredName" runat="server"></asp:TextBox>
                                            </td>
                                    </tr>
                                    <tr id="submission_row_dba">
                                        <td>Dba:<br />
                                            <asp:TextBox ID="txDba" runat="server" AutoCompleteType="Disabled" style="width: 80%;"></asp:TextBox>
                                            <span id="submission_row_dba_dbas">
                                            <asp:ListBox ID="lbDbaNames" runat="server" style="width: 80%;" OnInit="lbDbaNames_Init"></asp:ListBox>
                                            </span>
                                            </td>
                                    </tr>
                                    <tr id="submission_row_premium">
                                        <td>Premium:<br />
                                            <asp:TextBox ID="txEstimatedPremium" runat="server"></asp:TextBox>
                                            </td>
                                    </tr>
                                 </table>
                            </td>
                            <td id="TabIndex_Section_50" runat="server" valign="top">
                                <aje:UpdatePanel ID="upnlStatus" runat="server">
                                <ContentTemplate>
                                <table cellpadding="5" cellspacing="0" style="border: #999 1px solid;">
                                    <tr id="submission_row_status">
                                        <td>Status:<br />
                                            <UserControls:SubmissionStatusDropDownList id="SubmissionStatusDropDownList1" runat="server" AutoPostBack="true"                                                 
                                                OnSelectedIndexChanged="SubmissionStatusDropDownList1_SelectedIndexChanged"></UserControls:SubmissionStatusDropDownList>                                            
                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("folder_find.gif") %>' AlternateText="View Status History" />
	                                        <ajx:ModalPopupExtender runat="server" ID="programmaticModalPopup"
                                                    TargetControlID="Image1"
                                                    PopupControlID="programmaticPopup" 
                                                    BackgroundCssClass="modalBackground"
                                                    DropShadow="True"
                                                    RepositionMode="RepositionOnWindowScroll" >
                                            </ajx:ModalPopupExtender>
                                            
                                        </td>
                                    </tr>
                                    <tr id="submission_row_status_date">
                                        <td>Status Date:<br />
                                            <asp:TextBox ID="txSubmissionStatusDate" runat="server" OnInit="txSubmissionStatusDate_Init"></asp:TextBox>
                                            <%--<UserControls:RFV ID="rfvSubmissionStatusDate" runat="server" ErrorMessage="&nbsp;" Text="*" ControlToValidate="txSubmissionStatusDate" Display="None" SetFocusOnError="True"></UserControls:RFV>--%>
                                            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txSubmissionStatusDate"
                                                    Display="Dynamic" ErrorMessage="&nbsp;" Text="* mm/dd/yyyy" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date"></asp:CompareValidator>
                                        </td>
                                    </tr>                                  
                                    <tr id="submission_row_status_reason">
                                        <td>Submission Status Reason:<br />
                                            <UserControls:SubmissionStatusReasonDropDownList id="SubmissionStatusReasonDropDownList1" runat="server" AutoPostBack="False"
                                            Enabled='<%$ Code: !DefaultValues.SupportsMultipleLineOfBusinesses(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'>
                                            </UserControls:SubmissionStatusReasonDropDownList></td>
                                    </tr>
                                    <tr id="submission_row_status_reason_comment">
                                        <td>Status Reason Comment:<br />
                                            <asp:TextBox ID="txSubmissionStatusReasonOther" runat="server" Height="138px" 
                                                TextMode="MultiLine" Width="130px"></asp:TextBox> </td>
                                    </tr>
                                    <tr id="submission_row_other_carrier">
                                        <td>Other Carrier:<br />
                                            <UserControls:OtherCarrierDropDownList id="OtherCarrierDropDownList1" runat="server" SortBy="Code" ></UserControls:OtherCarrierDropDownList>
                                        </td>
                                    </tr>
                                    <tr id="submission_row_other_carrier_premium">
                                        <td>Other Carrier Premium:<br />                                            
                                            <asp:TextBox ID="txOtherCarrierPremium" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                </ContentTemplate>
                                </aje:UpdatePanel>
                            </td>
                            <td id="TabIndex_Section_60" runat="server" rowspan="2" valign="top">
                                <table id="submission_row_comments" cellpadding="3" cellspacing="0" style="border: #ccc 1px solid;">
                                    <tr>                                    
                                        <td>New Comment:<br /><asp:TextBox ID="txAddComments" runat="server" TextMode="MultiLine" Height="75px" Width="200px" onkeyup="document.getElementById('btnAddComment').disabled=this.value.length <= 0;"></asp:TextBox></td>
                                    </tr>
                                    <tr id="">
                                        <td>
                                            <input id="btnAddComment" type="button" value="Add Comment" onclick="stamp(); cbCommentEnablity.checked =true;" disabled="disabled" />
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>                                    
                                        <td>Comments:&nbsp;&nbsp;&nbsp;<input id="cbCommentEnablity" type="checkbox"
                                         onclick="document.getElementById('<%=txComments.ClientID%>').disabled=!document.getElementById('<%=txComments.ClientID%>').disabled;" /><span style="color:Blue;">&nbsp;Enable Edit</span>
                                         <img alt="copy" runat="Server" id="copyComments" src='<%$ Code : Common.GetImageUrl("copy.gif") %>' title="Copy Comments to clipboard" />
                                         <br />
                                         <asp:TextBox ID="txComments" runat="server" TextMode="MultiLine" Height="385px" Width="200px" OnInit="DisableOnInit"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" valign="top">
                                <aje:UpdatePanel id="upCodeTypes" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                <table border="0" width="100%" cellpadding="3" cellspacing="0">
                                    <tr>
                                        <td id="TabIndex_Section_20" runat="server" valign="top" width="50%">
                                            <fieldset id="rowCodeTypes" runat="server"><legend>Code Types</legend><asp:Placeholder ID="phCodeTypes" runat="server" ></asp:Placeholder>                               
                                            </fieldset>
                                        </td>
                                        <td id="TabIndex_Section_40" runat="server" valign="top" width="50%">
                                            <fieldset id="rowAttributes" runat="server"><legend>Attributes</legend><asp:Placeholder ID="phAttributes" runat="server" ></asp:Placeholder>                               
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                                </ContentTemplate>
                                <Triggers>
                                    <aje:AsyncPostBackTrigger ControlID="txEffectiveDate" />
                                </Triggers>
                                </aje:UpdatePanel>
                            </td>
                        </tr>
                        <tr id="submission_row_lobchildren">
                            <td id="TabIndex_Section_70" runat="server">
                                <ajx:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" SkinID="StandAloneTab">        
                                    <ajx:TabPanel ID="TabPanel1" runat="server" >
                                        <ContentTemplate>
                                            <aje:UpdatePanel id="upLOBList" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <uc1:LOBList ID="LOBList1" runat="server" TabIndex="70" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <aje:AsyncPostBackTrigger ControlID="txEffectiveDate" />
                                                </Triggers>
                                            </aje:UpdatePanel>
                                        </ContentTemplate>
                                    </ajx:TabPanel>
                                </ajx:TabContainer>
                            </td>
                        </tr>
                        <tr>
                            <td id="TabIndex_Section_90" runat="server" colspan="4">
                                
                                <aje:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnSubmitSubmission" runat="server" Enabled='<%$ Code: !Security.IsUserReadOnly() %>' Text="Create Submission"  OnCommand="btnSubmitSubmission_Command" AccessKey="S" />
                                    &nbsp;<asp:Button ID="Button1" runat="server" Text="Cancel"  OnCommand="btnSubmitSubmission_Command" ValidationGroup="A" CausesValidation="False" EnableViewState="False" AccessKey="C" />                                
                                
                                    <%--region using ConfirmButtonExtender for licensed state warning--%>
                                     <%--<ajx:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" 
                                        TargetControlID="btnSubmitSubmission"
                                        Enabled="false" ConfirmOnFormSubmit="True"  />
                                    <ajx:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button1" PopupControlID="PNL" OkControlID="ButtonOk" BackgroundCssClass="modalBackground" />
                                    <asp:Panel ID="PNL" runat="server" style="display:none; width:400px; background-color:White; border-width:2px; border-color:Black; border-style:solid; padding:20px;">
                                            <asp:Label ID="lblLicensedStateWarning" runat="server"></asp:Label>
                                        <br /><br />
                                        <div style="text-align:right;">
                                            <asp:Button ID="ButtonOk" runat="server" Text="OK" />
                                            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" Visible="False" />
                                        </div>
                                    </asp:Panel>--%>
                                    <%--end region using ConfirmButtonExtender for licensed state warning--%>
                                </ContentTemplate>
                                <Triggers>
                                    <aje:AsyncPostBackTrigger ControlID="AgencyDropDownList1" />
                                </Triggers>
                                </aje:UpdatePanel>                                 
                            </td>
                        </tr>
                        <tr>
                            <td id="TabIndex_Section_80" runat="server" colspan="4">
                            <%--<asp:TextBox ID="txDummy" runat="server"></asp:TextBox>
                            <UserControls:RFV ID="rfvDummy" runat="server" ControlToValidate="txDummy" Display="Dynamic"
                             ErrorMessage="&nbsp;" Text="Dummy" SetFocusOnError="True"></UserControls:RFV>--%>
                                <asp:Panel ID="pnlClientInfo" Visible="false" runat="server">
                                    <table cellpadding="0" cellspacing="0" class="box" width="100%" style="margin-top:10px;">
                                        <tbody>
                                            <tr>
                                                <td class="TL"><div /></td>
                                                <td class="TC">Client Information</td>
                                                <td class="TR"><div /></td>
                                            </tr>
                                            <tr>
                                                <td class="ML"><div /></td>
                                                <td class="MC"> 
                                                    <table cellpadding="4" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:HiddenField ID="hfClientId" runat="server" />
                                                                <table cellpadding="5" cellspacing="0" width="100%" style="background-color: #F5E8C5;">
                                                                    <tr>
                                                                        <td align="right" width="70"><b>Client Id:</b></td>                                    
                                                                        <td width="70"><asp:Label ID="lblClientId" runat="server"></asp:Label></td>
                                                                        <td align="right" width="70"><b>Portfolio Id:</b></td>
                                                                        <td><asp:Label ID="lblPortfolioId" runat="server"></asp:Label></td>
                                                                    </tr>
                                                                </table>
                                                                
                                                                <asp:Panel ID="pnlClientNames" runat="server">
                                                                    <br />
                                                                    <span style="font-family:Arial Sans-Serif;font-size:14px;">Client Names</span>
                                                                    <asp:CheckBox id="cbSelectAllNames" ForeColor="Blue" runat="server" Text="Select All Names" OnInit="cbSelectAllNames_Init"></asp:CheckBox>
                                                                    <hr />
                                                                    <asp:CheckBoxList ID="cblNames" runat="server" OnPreRender="DefaultSelect"></asp:CheckBoxList>
                                                                    <UserControls:RequiredFieldValidatorForCheckBoxLists ID="rfvNames" runat="server" ControlToValidate="cblNames" ErrorMessage="&nbsp;" Text="* Please select a Name." Display="Dynamic"  SetFocusOnError="true"></UserControls:RequiredFieldValidatorForCheckBoxLists>
                                                                </asp:Panel>
                                                                
                                                                <asp:Panel ID="pnlClientAddress" runat="server">
                                                                <aje:UpdatePanel ID="UpdatePanel8" runat="server">                                                                    
                                                                    <ContentTemplate>
                                                                        <br />
                                                                        <span style="font-family:Arial Sans-Serif;font-size:14px;">Client Addresses</span>
                                                                        <asp:CheckBox ForeColor="blue" ID="cbSelectAllAddresses" runat="server" Text="Select All Addresses" OnInit="cbSelectAllAddresses_Init" AutoPostBack='<%$ Code: DefaultValues.EnforceAgencyLicensedState(Common.GetSafeIntFromSession("CompanySelected")) %>' OnCheckedChanged="cbSelectAllAddresses_CheckedChanged"></asp:CheckBox>
                                                                        <hr />
                                                                        <asp:CheckBoxList id="rblAddresses" runat="server" AutoPostBack='<%$ Code: DefaultValues.EnforceAgencyLicensedState(Common.GetSafeIntFromSession("CompanySelected")) %>' OnSelectedIndexChanged="rblAddresses_SelectedIndexChanged" OnPreRender="DefaultSelect"></asp:CheckBoxList> <UserControls:RequiredFieldValidatorForCheckBoxLists id="rfvAddresses" runat="server" Display="Dynamic" ErrorMessage="&nbsp;" ControlToValidate="rblAddresses" SetFocusOnError="true" Text="* Please select an Address."></UserControls:RequiredFieldValidatorForCheckBoxLists> 
                                                                    </ContentTemplate>
                                                                </aje:UpdatePanel>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <hr noshade="noshade" style="margin-bottom:10px;" />
                                                                <asp:Button ID="btnSelectClient" Enabled='<%$ Code: !Security.IsUserReadOnly() %>' runat="server" Text="Edit Client" OnClick="btnSelectClient_Click" CausesValidation="False" />
                                                                &nbsp;<asp:Button ID="btnSwitchClient" Enabled='<%$ Code: !Security.IsUserReadOnly() %>' runat="server" Text="Switch Client" OnClick="btnSwitchClient_Click" CausesValidation="False" />
                                                            </td>                    
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="MR"><div /></td>
                                            </tr>
                                            <tr>
                                                <td class="BL"><div /></td>
                                                <td class="BC"><div /></td>
                                                <td class="BR"><div /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
    
                </td>
                <td class="MR"><div /></td>
            </tr>
            <tr>
                <td class="BL"><div/></td>
                <td class="BC"><div/></td>
                <td class="BR"><div/></td>
            </tr>
            </tbody>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdnUser" runat="server" />
    <asp:Panel ID="programmaticPopup" runat="server" Style="display: none" CssClass="modalPopup" OnLoad="Panel1_Load" Width="520px">
        <div>
        <aje:UpdatePanel ID="UpdatePanel181" runat="server">
        <ContentTemplate>
        <asp:Label id="lblCurrentStatus" runat="server"></asp:Label>
        <asp:PlaceHolder ID="HistoryStatusPH" runat="server"></asp:PlaceHolder>
        <br /><br />
        <UserControls:Grid id="GridView1" runat="server" CssClass="grid" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" EmptyDataText="There is no history for this submission." AllowPaging="True" AutoGenerateColumns="False"
         DataSourceID="ObjectDataSource1" DataKeyNames="Id,PreviousSubmissionStatusId" OnRowDataBound="GridView1_RowDataBound" Width="520px">
         <EmptyDataTemplate>
            <table width="100%">
              <tr>
                <td style="border: none;"> There is no history for this submission. </td>
              </tr>
            </table>
        </EmptyDataTemplate>
         <Columns>
            <asp:BoundField DataField="Id" Visible="False" HeaderText="Id"></asp:BoundField>
            <asp:BoundField DataField="SubmissionId" Visible="False" HeaderText="SubmissionId"></asp:BoundField>
            <asp:BoundField DataField="PreviousSubmissionStatusId" Visible="False" HeaderText="PreviousSubmissionStatusId"></asp:BoundField>
            <asp:TemplateField HeaderText="Status Date">
                <ItemTemplate>
                    <asp:Label ID="lblStatusDate" runat="server" Text='<%# Bind("SubmissionStatusDate", "{0:MM/dd/yyyy}") %>' ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Previous Submission Status">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "SubmissionStatus.StatusCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status Changed Date">
                <ItemTemplate>
                    <asp:Label ID="lblDate" runat="server" Text='<%# Bind("StatusChangeDt", "{0:MM/dd/yyyy HH:mm}") %>' ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="StatusChangeBy" HeaderText="Status Changed By"></asp:BoundField>
            <asp:TemplateField HeaderText="Active" ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton runat="server" Text="Active" CommandName="Select" CausesValidation="false" ID="LinkButton1"></asp:LinkButton>
                    <ajx:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="LinkButton1"
                    ConfirmText="Are you sure you want to make this status history inactive, this cannot be undone?"></ajx:ConfirmButtonExtender>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerTemplate>
        <asp:PlaceHolder runat="server"></asp:PlaceHolder>
        </PagerTemplate>
        </UserControls:Grid>
        <asp:ObjectDataSource id="ObjectDataSource1" runat="server" TypeName="BCS.Core.Clearance.Submissions" SelectMethod="GetSubmissionStatusHistory" OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="0" Name="submissionId" SessionField="SubmissionId" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource> 
        </ContentTemplate>
        </aje:UpdatePanel>                            
        <p style="text-align: center;">
            <asp:Button ID="OkButton" runat="server" Text="OK" Visible="False" CausesValidation="False" />
            <asp:Button ID="CancelButton" runat="server" Text="Done" CausesValidation="False" />
        </p>
        </div>
    </asp:Panel>
</asp:Content>