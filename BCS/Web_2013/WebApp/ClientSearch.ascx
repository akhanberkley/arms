<%@ Control Language="C#" AutoEventWireup="true" CompilationMode="auto" CodeFile="ClientSearch.ascx.cs" Inherits="BCS.WebApp.UserControls.UserControls_ClientSearch" %>
<%@ Register Src="~/ClientAddress.ascx" TagName="ClientAddress" TagPrefix="uc1" %>
<%@ Register Src="~/ClientNameSelector.ascx" TagName="ClientNameSelector" TagPrefix="uc1" %>


<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="http://cloud.github.com/downloads/digitalBush/jquery.maskedinput/jquery.maskedinput-1.3.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("input[id$=txtPhoneNum]").mask("(999) 999-9999");


        //        function callScript() {
        //            var pqr = $("input[id$=txtPhoneNum]").val();
        //            if (pqr.length > 0) {
        //                //$("#aspnetForm :input").attr("disabled", true);
        //              $('input ,textarea,select').attr("disabled", true);
        //              $("input[id$=txtPhoneNum]").removeAttr("disabled");
        //               
        //            }
        //            else {
        //                $("#aspnetForm:input").removeAttr("disabled", true);
        //            }
        //           
        //        }
    });
</script>
<style type="text/css">
    .onFocus
    {
        text-align: left;
        background-color: Yellow;
    }
</style>


<div>
    <h3 class="sectionGroupHeader">Insured/DBA Name&nbsp;<asp:ImageButton ID="ibAddName" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("plus.gif") %>' OnClick="ibAddName_Click" />&nbsp;<asp:ImageButton ID="ibRemoveName" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("minus.gif") %>' OnClick="ibRemoveName_Click" /></h3>
    <asp:Label ID="lblNoresults" runat="server" Text="* No existing clients were found in Clearance using the criteria below" ForeColor="Red" Font-Italic="true" Visible="false"></asp:Label>
    <UserControls:DynamicControlsPlaceholder ID="SearchByNamePlaceHolder" runat="server"></UserControls:DynamicControlsPlaceholder>
    <em>* Please do not include any name prefixes, or suffixes.</em>
</div>

<div>
    <asp:Panel ID="pnlAdvClientSearch" runat="server">

        <h3 class="sectionGroupHeader">Phone Number &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <asp:Label ID="lblPhNumresults" runat="server" Text="Phone Number Results" Visible="false"></asp:Label>
        </h3>
        <table cellspacing="1">
            <tr>
                <td>
                    <asp:Label ID="lblPhValidation" runat="server" Text="" ForeColor="Red"></asp:Label>
                    <table width="20%" cellspacing="0" cellpadding="1" class="grid">
                        <tr class="header">
                            <td></td>
                            <td>Business Phone</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image ID="imgPhone" runat="server" ImageUrl="~/Images/ph3.gif" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtPhoneNum" runat="server" CssClass="exclude"></asp:TextBox>
                                <%--  <ajx:MaskedEditExtender ID="mskExtender" TargetControlID="txtPhoneNum" OnFocusCssClass="onFocus" 
                                Mask="(999) 999-9999" MessageValidatorTip="true" 
                                MaskType="Number" runat="server"  ClearMaskOnLostFocus="true"   
                                InputDirection="LeftToRight" AcceptNegative="None" AutoComplete="true" AutoCompleteValue="0" ErrorTooltipEnabled="true"   />

        <ajx:MaskedEditValidator ID ="mskValidator" ControlExtender="mskExtender"
                                 ControlToValidate="txtPhoneNum" runat="server"
                                 IsValidEmpty="false" 
                                 Display="Dynamic"
                                 ToolTip="Enter a Valid Phone Number"  />--%>
                            </td>
                        </tr>
                    </table>
                    <em style="font-size: 10px; display: block;">* Must be 10 digits in length. </em>
                </td>
                <td colspan="2">
                    <asp:Label ID="lblhack" runat="server" Width="80px" Text=""></asp:Label>
                </td>
                <td>
                    <asp:Panel ID="pnlShowSrchResults" runat="server" Style="display: none">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblSrchResults" runat="server" Text="" ForeColor="#0A664C" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnAddThisClient" runat="server" Text="Add This Client" OnClick="btnAddThisClient_Click" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>

<div>
    <h3 class="sectionGroupHeader">Address&nbsp;<asp:ImageButton ID="ibAddAddress" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("plus.gif") %>' OnClick="ibAddAddress_Click" />&nbsp;<asp:ImageButton ID="ibRemoveAddress" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("minus.gif") %>' OnClick="ibRemoveAddress_Click" />&nbsp;<img id="imgh" runat="server" alt="help" src='<%$ Code : Common.GetImageUrl("question.gif") %>' title="When searching by State it must be used in conjuction with another address field or no results will be returned.  When searching for both Name and Address results for either will be returned." /></h3>
    <UserControls:DynamicControlsPlaceholder ID="SearchByAddressPlaceHolder" runat="server"></UserControls:DynamicControlsPlaceholder>
</div>
<br />
<div>
    <table cellpadding="1" style="border: solid 1px; border-collapse: collapse;">
        <tr>
            <td style="border: solid 1px; vertical-align: top;">
                <div class="searchCriteriaSection">
                    <h3 class="sectionGroupHeader">FEIN/Tax Identifier&nbsp;<img id="idqq" runat="server" alt="help" src='<%$ Code : Common.GetImageUrl("question.gif") %>' title="Enter the Federal Employer Identification Number (FEIN) you wish to search for. Note that partial matching happens automatically, but by entering the exact number, your result set will be smaller and return faster." /></h3>
                    <asp:TextBox ID="txFein" runat="server"></asp:TextBox>
                    <em style="font-size: 10px; display: block;">* Separate multiples values with a comma.</em>
                </div>
            </td>
            <td style="border: solid 1px; vertical-align: top;">
                <div class="searchCriteriaSection">
                    <h3 class="sectionGroupHeader">Client Id&nbsp;<img id="imgqqq" runat="server" alt="help" src='<%$ Code : Common.GetImageUrl("question.gif") %>' title="Enter the Client Id you wish to search for. If this is entered other search criteria will be ignored." /></h3>
                    <asp:TextBox ID="txClientId" runat="server"></asp:TextBox>
                </div>
            </td>
            <td style="border: solid 1px; vertical-align: top;">
                <div class="searchCriteriaSection">
                    <h3 class="sectionGroupHeader">
                        <asp:Literal ID="somelit" runat="server" Text='<%$ Code: DefaultValues.GetSpecialSearchText(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'>
                        </asp:Literal>
                        &nbsp;
                <asp:Image runat="server" ID="imgNumberSearch" ToolTip='<%$ Code: DefaultValues.GetSpecialSearchText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) + " Search." %>'
                    ImageUrl='<%$ Code : Common.GetImageUrl("question.gif") %>' EnableViewState="false" />
                    </h3>
                    <asp:TextBox ID="txPolicyNumber" runat="server"></asp:TextBox>
                </div>
            </td>
            <td style="border: solid 1px; vertical-align: top;" class="default_portfoliosearch">
                <div class="searchCriteriaSection">
                    <h3 class="sectionGroupHeader">Portfolio &nbsp;</h3>
                    <asp:TextBox ID="txPortfolioId" runat="server"></asp:TextBox>
                    <asp:RadioButtonList ID="rblPortfolioSearchType" runat="server" RepeatLayout="Flow" RepeatDirection="horizontal" Style="display: block;">
                        <asp:ListItem Text="Id" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Name"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </td>
        </tr>
    </table>

</div>
