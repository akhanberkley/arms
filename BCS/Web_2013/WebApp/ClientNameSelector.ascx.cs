#region Using
using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    public partial class UserControls_ClientNameSelector : System.Web.UI.UserControl
    {
        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (IsBusiness)
                    this.ClientData1.Rows[2].Visible = false;

                this.rbPersonal.Enabled = string.IsNullOrEmpty(NameSequenceOrID);
                this.rbBusiness.Enabled = string.IsNullOrEmpty(NameSequenceOrID);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            StateDropDownList1.DataBind();
            rblClientNameType.DataBind();
            this.btnSave.Visible = Mode == Common.Mode.Modify && Components.ConfigValues.NameEditable(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            this.btnRemove.Visible = Mode == Common.Mode.Add;
            this.rblClientNameType.Visible = !(Mode == Common.Mode.Search);
            rfvAddressType.Visible = !(Mode == Common.Mode.Search);

            this.txBusinessName.MaxLength = Components.ConfigValues.GetBusinessNameLength(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

            base.OnInit(e);
        }        
        #endregion

        #region Control Events
        protected void rbBusiness_CheckedChanged(object sender, EventArgs e)
        {
            this.ClientData1.Rows[0].Visible = true;
            this.ClientData1.Rows[1].Visible = true;
            this.ClientData1.Rows[2].Visible = false;
        }

        protected void rbPersonal_CheckedChanged(object sender, EventArgs e)
        {
            this.ClientData1.Rows[0].Visible = false;
            this.ClientData1.Rows[1].Visible = false;
            this.ClientData1.Rows[2].Visible = true;

            this.ClientData2.Rows[0].Cells[0].Visible = ShowFullName;
            this.ClientData2.Rows[0].Cells[2].Visible = ShowFullName;
            this.ClientData2.Rows[0].Cells[4].Visible = ShowFullName;

            this.ClientData2.Rows[1].Cells[0].Visible = ShowFullName;
            this.ClientData2.Rows[1].Cells[2].Visible = ShowFullName;
            this.ClientData2.Rows[1].Cells[4].Visible = ShowFullName;
            this.btnSave.Visible = ShowFullName;
        }
        protected void btnRemove_Click(object sender, ImageClickEventArgs e)
        {
            if (this.NameSequenceOrID == null || this.NameSequenceOrID.Length == 0)
                btnRemove.Parent.Parent.Controls.Remove(btnRemove.Parent.Parent.FindControl(this.ID));
        }
        protected void rfvAddressType_Init(object sender, EventArgs e)
        {
            rfvAddressType.Enabled = DefaultValues.RequiresNameType(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) 
                && Mode != Common.Mode.Search;
        }
        #endregion

        #region Properties

        public Common.Mode Mode
        {
            set
            {
                ViewState["Mode"] = value;
                this.btnSave.Visible = Mode == Common.Mode.Modify &&
                    Components.ConfigValues.NameEditable(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                this.btnRemove.Visible = Mode == Common.Mode.Add || (Mode == Common.Mode.Modify && this.NameSequenceOrID.Length == 0);
                this.rblClientNameType.Visible = !(Mode == Common.Mode.Search);
                rfvAddressType.Visible = !(Mode == Common.Mode.Search);
                rfvAddressType_Init(rfvAddressType, EventArgs.Empty);
                
                //this.StateDropDownList1.Visible = (Mode == Common.Mode.Search);
                //this.txCity.Visible = (Mode == Common.Mode.Search);
                this.ClientData1.Rows[0].Cells[1].Visible = (Mode == Common.Mode.Search);
                this.ClientData1.Rows[0].Cells[2].Visible = (Mode == Common.Mode.Search);
                this.ClientData1.Rows[1].Cells[1].Visible = (Mode == Common.Mode.Search);
                this.ClientData1.Rows[1].Cells[2].Visible = (Mode == Common.Mode.Search);

                BCS.Core.Security.CustomPrincipal cPrincpal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
                bool shouldEnable =
                    (Mode == Common.Mode.Add // mode is add or
                    || (Mode == Common.Mode.Modify && this.NameSequenceOrID.Length == 0)) // mode is edit, but the name is new or
                    || ( Components.ConfigValues.NameEditable(Common.GetSafeIntFromSession(SessionKeys.CompanyId))
                        ? true : cPrincpal.IsInAnyRoles("Company Administrator", "System Administrator") // either company allows edit OR if Company/System administrator
                        ); 
                this.txBusinessName.Enabled = shouldEnable;
            }
            get
            { return (Common.Mode?)ViewState["Mode"] ?? Common.Mode.Search; }
        }
        public bool ShowFullName
        {
            set
            { ViewState["ShowFullName"] = value; }
            get
            { return (bool?)ViewState["ShowFullName"] ?? true; }
        }

        public bool PrimaryName
        {
            set
            { rdoPrimaryName.Checked = value; }
            get
            { return rdoPrimaryName.Checked; }
        }

        public bool PrimaryFlagEditable
        {
            set
            { hfPrimaryFlagEditable.Value = value.ToString(); }
            get
            {
                string value = hfPrimaryFlagEditable.Value;
                return (String.IsNullOrEmpty(value)) ? false : bool.Parse(value);
            }
        }

        public bool IsBusiness
        {
            get { return this.rbBusiness.Checked; }
            set
            {
                this.rbBusiness.Checked = value;
                rbBusiness_CheckedChanged(null, System.EventArgs.Empty);
            }
        }

        public bool IsPersonal
        {
            get { return this.rbPersonal.Checked; }
            set
            {
                this.rbPersonal.Checked = value;
                rbPersonal_CheckedChanged(null, System.EventArgs.Empty);
            }
        }

        public string NameTypeValue
        {
            get
            {
                if (this.hiddenNameType.Value == null)
                    return string.Empty;
                return this.hiddenNameType.Value;
            }
            set
            { this.hiddenNameType.Value = value; }

        }
        public string CityValue
        {
            get
            {
                if (this.txCity.Text == null)
                    return string.Empty;
                return this.txCity.Text.Trim();
            }
            set
            { this.txCity.Text = value; }

        }
        public string StateValue
        {
            get
            {
                if (this.StateDropDownList1.SelectedItem == null)
                    return string.Empty;
                return this.StateDropDownList1.SelectedItem.Text;
            }
            set
            {
                if (this.StateDropDownList1.Items.FindByText(value) != null)
                {
                    this.StateDropDownList1.Items.FindByText(value).Selected = true;
                }
            }

        }
        public string ClearanceNameTypeValue
        {
            get
            {
                return rblClientNameType.SelectedItem == null ? null : rblClientNameType.SelectedItem.Text;
            }
            set
            {
                if (rblClientNameType.Items.FindByText(value) != null)
                {
                    rblClientNameType.Items.FindByText(value).Selected = true;
                }
            }
        }


        public string BusinessNameValue
        {
            get
            {
                if (txBusinessName.Text.Length == 0)
                    return null;
                return this.txBusinessName.Text.Trim();
            }
            set { this.txBusinessName.Text = value; }
        }

        public string NamePrefixValue
        {
            get { return this.txPrefix.Text; }
            set { this.txPrefix.Text = value; }
        }

        public string FirstNameValue
        {
            get
            {
                if (txFirst.Text.Length == 0)
                    return null;
                return this.txFirst.Text;
            }
            set { this.txFirst.Text = value; }
        }

        public string MiddleNameValue
        {
            get { return this.txMiddle.Text; }
            set { this.txMiddle.Text = value; }
        }

        public string LastNameValue
        {
            get
            {
                if (txLast.Text.Length == 0)
                    return null;
                return this.txLast.Text;
            }
            set { this.txLast.Text = value; }
        }

        public string NameSuffixValue
        {
            get { return this.txSuffix.Text; }
            set { this.txSuffix.Text = value; }
        }

        public string NameSequenceOrID
        {
            get { return this.hiddenNameSequenceOrId.Value; }
            set { this.hiddenNameSequenceOrId.Value = value; }
        }

        public bool Enabled
        {
            set
            {
                foreach (Control c in this.Controls)
                {
                    if (c is WebControl)
                        (c as WebControl).Enabled = value;
                }
            }
        }
        #endregion

        #region Other Methods
        public void SetFocus()
        {
            WebControl wc;
            if (IsBusiness)
                wc = txBusinessName;
            else
                wc = txFirst;
            if (null != ScriptManager.GetCurrent(this.Page))
            {
                ScriptManager.GetCurrent(this.Page).SetFocus(wc);
            }
            else
            {
                wc.Focus();
            }
        } 
        #endregion
    }
}