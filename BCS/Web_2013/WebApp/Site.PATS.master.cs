#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Policy;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;
using Microsoft.Web.Services2.Security.X509;

using OrmLib;
using BCS.Biz;
using BCS.Core;
using BCS.WebApp.Components;

using TSHAK.Components;
#endregion

public partial class Site_PATS : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region Session alert handling
        bool implementSessionAlert = false;
        try
        {
            implementSessionAlert = Convert.ToBoolean(ConfigurationManager.AppSettings["ImplementSessionAlert"]);
        }
        catch
        { /*do nothing */ }
        if (implementSessionAlert)
        {
            Session[SessionKeys.UserSession] = DateTime.Now;
            object objSection = ConfigurationManager.GetSection("system.web/sessionState");

            // Get the section related object.
            System.Web.Configuration.SessionStateSection sessionStateSection =
                (System.Web.Configuration.SessionStateSection)objSection;


            bool addAlert = true;
            int minutesBeforePrefernce = 4;
            try
            {
                minutesBeforePrefernce = Convert.ToInt32(ConfigurationManager.AppSettings["MinutesBeforePrefernce"]);
                if (minutesBeforePrefernce >= sessionStateSection.Timeout.Minutes)
                {
                    // TODO: log
                    addAlert = false;
                }
            }
            catch
            { /*do nothing */ }

            if (addAlert)
            {
                DateTime sessionStartTime = Common.GetSafeDateTimeFromSession(SessionKeys.UserSession);
                DateTime alertAfterTime = sessionStartTime.AddMinutes(sessionStateSection.Timeout.Minutes - minutesBeforePrefernce);
                TimeSpan alertAfter = alertAfterTime.Subtract(sessionStartTime);

                string openBrace = "{";
                string closBrace = "}";
                string newLine = "\\\\r\\\\n\\\\r\\\\n";
                string tab = "\\\\r";

                string jsAlertScript =
                    string.Format("var timeOutId; timeOutId = setTimeout(\" var confirmResult = confirm('Session will end at {2} in {0} minutes. {5}{6}OK - to go to the landing page and start a new Session. {5}{6}Cancel - to stay on the current page. Canceling and interaction after session ends might cause unexpected results.'); if(confirmResult) {3} self.location = '.'; {4} /*else {3} alert(new Date().toLocaleTimeString()); {4}*/\", {1});",
                    minutesBeforePrefernce, alertAfter.TotalMilliseconds, sessionStartTime.AddMinutes(sessionStateSection.Timeout.Minutes).ToLongTimeString(), openBrace, closBrace, newLine, tab);
                
                string jsDisplayScript =
                                    string.Format("var timeOutId4display; timeOutId4display = setTimeout(\" var confirmResult = confirm('Session will end at {2} in {0} minutes. {5}{6}OK - to go to the landing page and start a new Session. {5}{6}Cancel - to stay on the current page. Canceling and interaction after session ends might cause unexpected results.'); if(confirmResult) {3} self.location = '.'; {4} /*else {3} alert(new Date().toLocaleTimeString()); {4}*/\", {1});",
                                    minutesBeforePrefernce, alertAfter.TotalMilliseconds, sessionStartTime.AddMinutes(sessionStateSection.Timeout.Minutes).ToLongTimeString(), openBrace, closBrace, newLine, tab);

                Page.ClientScript.RegisterStartupScript(
                    typeof(string),
                    "SessionTimeOutAlert",
                    jsAlertScript,
                    true);
            }
        }
        #endregion
        this.lblEnvironment.Text = DefaultValues.EnvironmentTitle;

        if (!IsPostBack)
        {
            AuthorizationCheck();
        }

        #region Apply Theme
        // get theme.
        int companyId = 0;
        Int32.TryParse(Common.GetCookie(SessionKeys.CompanyId), out companyId);
        string themeStyleSheeet =
            BCS.WebApp.Components.DefaultValues.ThemeStyleSheet(companyId);
        this.page_head.Controls.Add(new LiteralControl("<link href=\"" + string.Concat(DefaultValues.CssPath, "pats_style_sprites.css") + "\" rel=\"stylesheet\" type=\"text/css\" />"));
        this.page_head.Controls.Add(new LiteralControl("<link href=\"" + themeStyleSheeet + "\" rel=\"stylesheet\" type=\"text/css\" />"));
        #endregion

        #region Apply javascripts

        HtmlGenericControl Include = new HtmlGenericControl("script");
        Include.Attributes.Add("type", "text/javascript");
        Include.Attributes.Add("src", string.Concat(DefaultValues.ScriptPath, "Calendar.js"));

        this.page_head.Controls.Add(Include);
        #endregion

        if (Session["SessionTimeOut"] != null)
        {
            Session.Contents.Remove("SessionTimeOut");
            Page.ClientScript.RegisterStartupScript(typeof(string), "sessionTimedOut",
                                   "alert('Session was timed out. Please try again.');", true);
        }
    }

    public int GetCompanySelected()
    {
        try
        {
            return Convert.ToInt32(
               (CompanySelector1.FindControl("CompanyDropDownList1") as DropDownList).SelectedValue);
        }
        catch (Exception)
        { return 0; }
    }

    public int GetCompanyNumberSelected()
    {
        try
        {
            return Convert.ToInt32(
                (CompanySelector1.FindControl("CompanyNumberDropDownList1") as DropDownList).SelectedValue);
        }
        catch (Exception)
        {return 0;}
    }

    /// <summary>
    /// Since the master page loads after the content page has been loaded, when the application is directly hit (for example bug #520)
    /// a method is needed to check authorization before any of the content pages are loaded (to avoid database calls to populate controls, etc)
    /// pending investigation of a better way to handle it.
    /// </summary>
    public void AuthorizationCheck()
    {
        #region Determine user has access
        BCS.Core.Security.CustomPrincipal customPrincipal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
        if (customPrincipal ==null)
        {
            // cant figure out user
            SecureQueryString qs = new SecureQueryString(Core.Key);
            qs["SecurityEvent"] = "Invalid user.";
            BTS.LogFramework.LogCentral.Current.LogWarn(string.Format("Unauthorized access."));

            Response.Redirect("~/NoAccess.aspx?data=" + HttpUtility.UrlEncode(qs.ToString()));
        }
        string[] roles = customPrincipal.Roles;

        if (roles == null || roles.Length == 0)
        {
            // User has no access.
            SecureQueryString qs = new SecureQueryString(Core.Key);
            qs["SecurityEvent"] = "Your account has no security role(s) associated with it.";
            BTS.LogFramework.LogCentral.Current.LogWarn(string.Format("Unauthorized access : Username {0}", customPrincipal.Identity.Name));

            Response.Redirect("~/NoAccess.aspx?data=" + HttpUtility.UrlEncode(qs.ToString()));
        }
        #endregion

        #region Determine user has associated companies
        UserCompanyCollection ucompanies = BCS.WebApp.Components.Security.GetUserCompanies(Page.User.Identity.Name);

        if (ucompanies == null || ucompanies.Count == 0)
        {
            // User has no companies associated.
            SecureQueryString qs = new SecureQueryString(Core.Key);
            qs["SecurityEvent"] = "Your account has no companies associated with it.";
            BTS.LogFramework.LogCentral.Current.LogWarn(string.Format("Unauthorized access : Username {0}, no companies associated.", customPrincipal.Identity.Name));

            Response.Redirect("~/NoAccess.aspx?data=" + HttpUtility.UrlEncode(qs.ToString()));
        }
        #endregion
    }
}
