namespace BCS.WebApp
{
    using System;
    using System.Data;
    using System.Configuration;
    using System.Collections;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Web.UI.HtmlControls;
    using BCS.WebApp.Components;

    public partial class Menu2 : System.Web.UI.UserControl
    {
        protected bool showSearch = false;
        protected bool showLoad = false;
        protected bool showAttrLink = false;
        protected bool showAdminLink = false;
        protected bool showAgencyLink = false;
        protected bool showAgentLink = false;
        protected bool showAgencyLOBLink = false;
        protected bool showCompanyNumberCodeLink = false;
        protected bool showPersonLink = false;
        protected bool showPersonAssignLink = false;
        protected bool showAgencyAssignLink = false;
        protected bool showReAssignLink = false;
        protected bool showSwitchLink = false;
        protected bool showClassCodesLink = false;
        protected bool showContactLink = false;
        protected bool showGeneratorLink = false;
        protected bool showAgencySearchLink = false;
        protected bool showUWULink = false;
        protected bool showUserLink = false;
        protected bool showLinkLink = true;

        protected bool showSearchTab = true;
        protected bool showSubmissionsTab = true;
        protected bool showAgenciesTab = true;
        protected bool showUWsTab = true;
        protected bool showAssignmentsTab = true;
        protected bool showAdministrationTab = true;
        protected bool showSystemAdministrationTab = true;
        protected bool usesFAQ = false;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(Common.GetCookie(SessionKeys.CompanyId)) || Common.GetCookie(SessionKeys.CompanyId) == "0")
            {
                showSearchTab = false;
                showSubmissionsTab = false;
                showAgenciesTab = false;
                showUWsTab = false;
                showAssignmentsTab = false;
                showAdministrationTab = false;
                showSystemAdministrationTab = false;
                usesFAQ = false;
                return;
            }

            string[] roles = (HttpContext.Current.User as Core.Security.CustomPrincipal).Roles;
            System.Collections.Specialized.StringCollection SRoles = new System.Collections.Specialized.StringCollection();
            SRoles.AddRange(roles);

            bool isAPS = ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            //showAgenciesTab = !isAPS;
            //showUWsTab = !isAPS;
            showAssignmentsTab = !isAPS;

            if (SRoles.Contains("System Administrator"))
            {
                showSearch = true;
                showLoad = true;
                showAttrLink = true;
                showAdminLink = true;
                showAgencyLink = true;
                showAgencyLOBLink = true;
                showAgentLink = true;
                showCompanyNumberCodeLink = true;
                showPersonLink = true;//!isAPS;
                showPersonAssignLink = true;
                showAgencyAssignLink = true;
                showReAssignLink = true;
                showSwitchLink = true;
                showClassCodesLink = true;
                showContactLink = true;
                showGeneratorLink = true;
                showAgencySearchLink = true;
                showUWULink = true;
                showUserLink = true;
                usesFAQ = true;
                return;
            }
            if (SRoles.Contains("User"))
            {
                showLoad = true;
                showSearch = true;
                showAgencySearchLink = true;
            }
            if (SRoles.Contains("Partial User"))
            {
                showLoad = true;
                showSearch = true;
                showAgencySearchLink = true;
            }
            if (SRoles.Contains("Auditor"))
            {
                showLoad = true;
                showSearch = true;
                showAgencySearchLink = true;
            }
            if (SRoles.Contains("Remote Agency"))
            {
                showLoad = true;
                showSearch = true;
            }

            if (SRoles.Contains("Agency Administrator"))
            {
                showAgencyLink = true;
            }
            if (SRoles.Contains("Agency Read-Only"))
            {
                showAgencyLink = true;
                showAgentLink = true;
                showContactLink = true;
            }
            if (SRoles.Contains("Assignment Administrator"))
            {
                showAgencyLOBLink = true;
                showPersonLink = true;// !isAPS;
                showPersonAssignLink = true;
                showAgencyAssignLink = true;
            }
            if (SRoles.Contains("Company Administrator"))
            {
                showCompanyNumberCodeLink = true;
                showClassCodesLink = true;
                showContactLink = true;
                showUserLink = true;
                showSwitchLink = true;
            }
            if (SRoles.Contains("ReAssignment Administrator"))
            {
                showReAssignLink = true;
            }
            if (SRoles.Contains("Switch Client Administrator"))
            {
                showSwitchLink = true;
            }
            if (SRoles.Contains("Agent Administrator"))
            {
                showAgentLink = true;
            }
            if (SRoles.Contains("Policy Number Generator"))
            {
                showGeneratorLink = true;
            }
            if (SRoles.Contains("Agency Contact Administrator"))
            {
                showContactLink = true;
            }

            // if sub menu items under a tab are invisible, make the tab itself invisible
            if (!showSearch && !showAgencySearchLink)
            {
                showSearchTab = false;
            }
            if (!showLoad && !showReAssignLink && !showSwitchLink && !showGeneratorLink)
            {
                showSubmissionsTab = false;
            }
            if (!showAgencyLOBLink && !showUWULink)// && !isAPS)
            {
                showUWsTab = false;
            }
            if ((!showPersonAssignLink && !showAgencyAssignLink) || isAPS)
            {
                showAssignmentsTab = false;
            }
            if (!showAgencyLink && !showAgentLink && !showContactLink)// && !isAPS)
            {
                showAgenciesTab = false;
            }

            if (showSearch)
            {
                usesFAQ = UsesFAQ();
            }
        }

        protected bool UsesFAQ()
        {
            try
            {
                usesFAQ = true;
                return ConfigValues.GetCompanyParameter(Common.GetSafeIntFromSession(SessionKeys.CompanyId)).UsesFAQ.IsTrue;
            }
            catch
            {
                return false;
            }
        }

        protected void lbPolicyNumberGenerator_Click(object sender, EventArgs e)
        {
            int companyNumberId = Common.GetSafeIntFromSession("CompanyNumberSelected");
            string message = "";
            if (companyNumberId > 0)
            {
                BCS.Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumber.Columns.Id, companyNumberId);
                BCS.Biz.CompanyNumber cn = dm.GetCompanyNumber();

                if (cn != null)
                {

                    LookupProxy.Lookup lookup = new LookupProxy.Lookup();
                    string nextNumber =
                        lookup.GetNextNumberControlValueByCompanyNumberAndTypeName(cn.PropertyCompanyNumber,
                        "Policy Number", 0);
                    if (nextNumber.ToString().Length > 0)
                    {
                        message = "The following Policy Number was generated for you: " + nextNumber.ToString();
                        message += "\\n(It has already been copied to your clipboard.)";

                        Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "__doPolicyNumberGeneratorClipboard", "<script language=\"javascript\">\n" +
                            "window.clipboardData.setData('Text','" + nextNumber.ToString() + "');\n" +
                            "</script>");
                    }
                    else
                    {
                        message = "Unable to get the next Policy Number. Please try again.";
                    }
                }
            }
            else
                message = "Please select a company and company number.";


            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "__doPolicyNumberGeneratorAlert", "<script language=\"javascript\">\n" +
                "alert('" + message + "');" +
                "</script>");

        }
}
}