using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BTS.CacheEngine.V2;
using BCS.WebApp.Components;

namespace BCS.WebApp.UserControls
{
    public partial class UserControls_ClientClassCode : System.Web.UI.UserControl
    {

        #region Properties
        public int CompanyNumberId
        {
            set
            { ViewState["CompanyNumberId"] = value; }
            get
            {
                if (ViewState["CompanyNumberId"] == null)
                    return 0;
                return (int)ViewState["CompanyNumberId"];
            }
        }

        public int ClassCodeId
        {
            set
            { ViewState["ClassCodeId"] = value; }
            get
            {
                if (ViewState["ClassCodeId"] == null)
                    return 0;
                return (int)ViewState["ClassCodeId"];
            }
        }

        public int ClientClassCodeId
        {
            set
            { ViewState["ClientClassCodeId"] = value; }
            get
            {
                if (ViewState["ClientClassCodeId"] == null)
                    return 0;
                return (int)ViewState["ClientClassCodeId"];
            }
        }

        private BCS.Core.Clearance.BizObjects.ClientClassCode _clientClassCode = new BCS.Core.Clearance.BizObjects.ClientClassCode();
        public BCS.Core.Clearance.BizObjects.ClientClassCode ClientClassCode
        {
            set
            {
                _clientClassCode = value;
            }

            get
            {
                return _clientClassCode;
            }
        }

        public Common.Mode Mode
        {
            set
            {
                ViewState["Mode"] = value;
            }
            get
            {
                if (ViewState["Mode"] == null)
                    return Common.Mode.Add;
                return (Common.Mode)ViewState["Mode"];
            }
        }
        #endregion

        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            string cachekey = string.Concat("HazardGrades4", CompanyNumberId);
            CacheEngine ce = new CacheEngine(DefaultValues.DefaultObjectCacheTimeInMinutes);

            Biz.HazardGradeTypeCollection hgtcc = new BCS.Biz.HazardGradeTypeCollection();
            if (!ce.CachedItemExist(cachekey))
            {
                Biz.DataManager dm = new BCS.Biz.DataManager(Components.DefaultValues.DSN);
                dm.QueryCriteria.And(Biz.JoinPath.HazardGradeType.CompanyNumberHazardGradeType.Columns.CompanyNumberId, CompanyNumberId);
                hgtcc = dm.GetHazardGradeTypeCollection();
                ce.AddItemToCache(cachekey, hgtcc);
            }
            else
            {
                hgtcc = (BCS.Biz.HazardGradeTypeCollection)ce.GetCachedItem(cachekey);
            }
            if (hgtcc.Count > 0)
            {
                HtmlTable table = new HtmlTable();
                table.ID = "baseTable";
                table.CellPadding = 0;
                table.CellSpacing = 0;
                table.BorderColor = "gainsboro";
                table.Style.Add(HtmlTextWriterStyle.BorderCollapse, "collapse");
                table.Border = 1;
                table.Style.Add(HtmlTextWriterStyle.MarginBottom, "10px;");

                // Add first row - two cells, 1. spans three columns 2. spans rest of the columns
                HtmlTableRow firstRow = AddFirstRow(table, hgtcc.Count);
                HtmlTableRow secondRow = AddSecondRow(table, hgtcc);
                HtmlTableRow thirdRow = AddThirdRow(table, hgtcc);
                HtmlTableRow fourthRow = AddFourthRow(table, hgtcc);
                Common.AddControl(pnlCC, table, false, false);

                if (!IsPostBack)
                    LoadClassCode(this.ClientClassCode);
            }
        }
        #endregion

        #region Control Events
        void ddlDesc_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            DropDownList sddl = this.FindControl("ClassCodeDropDownList1") as DropDownList;
            sddl.ClearSelection();
            sddl.Items.FindByValue(ddl.SelectedValue).Selected = true;
            ccddl_SelectedIndexChanged(sddl, EventArgs.Empty);
        }

        void ccddl_SelectedIndexChanged(object sender, EventArgs e)
        {
            Biz.DataManager dm = new BCS.Biz.DataManager(Components.DefaultValues.DSN);
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, (sender as DropDownList).SelectedValue);
            Biz.ClassCode ccode = dm.GetClassCode(Biz.FetchPath.ClassCode.SicCodeList);
            if (ccode != null && ccode.SicCodeList != null)
            {
                (this.FindControl("txtSICCode") as TextBox).Text = ccode.SicCodeList.Code.ToString();
            }
            else
            {
                (this.FindControl("txtSICCode") as TextBox).Text = string.Empty;
            }
            this.ClassCodeId = Convert.ToInt32((sender as DropDownList).SelectedValue);

            dm = new BCS.Biz.DataManager(Components.DefaultValues.DSN);
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberClassCodeHazardGradeValue.Columns.ClassCodeId, this.ClassCodeId);
            dm.QueryCriteria.And(
                Biz.JoinPath.CompanyNumberClassCodeHazardGradeValue.Columns.CompanyNumberId, this.CompanyNumberId);

            Biz.CompanyNumberClassCodeHazardGradeValueCollection coll = dm.GetCompanyNumberClassCodeHazardGradeValueCollection();
            coll = coll.FilterByClassCodeId(this.ClassCodeId);
            foreach (Biz.CompanyNumberClassCodeHazardGradeValue hgv in coll)
            {
                (this.FindControl("hazardGradeId" + hgv.HazardGradeTypeId) as ITextControl).Text = hgv.HazardGradeValue;
            }
            if (coll.Count == 0)
            {
                HtmlTableRow baseControl = (this.FindControl("baseTable") as HtmlTable).Rows[2];
                foreach (HtmlTableCell cell in baseControl.Cells)
                {
                    foreach (Control fcontrol in cell.Controls)
                    {
                        if (fcontrol is TextBox)
                        {
                            if (fcontrol.ID.IndexOf("hazardGradeId") > -1)
                                (fcontrol as ITextControl).Text = string.Empty;
                        }
                    }
                }
            }
            DropDownList sddl = (this.FindControl("ClassCodeDescDropDownList1") as DropDownList);
            sddl.ClearSelection();
            sddl.Items.FindByValue(this.ClassCodeId.ToString()).Selected = true;
            (sender as Control).Focus();
        }

        void btnRemove_Click(object sender, ImageClickEventArgs e)
        {
            if (this.ClientClassCodeId != 0)
            {
                Biz.DataManager dm = new BCS.Biz.DataManager(Components.DefaultValues.DSN);
                dm.QueryCriteria.And(Biz.JoinPath.ClientClassCode.Columns.Id, this.ClientClassCodeId);
                Biz.ClientClassCode cctbd = dm.GetClientClassCode();

                cctbd.Delete();
                dm.CommitAll();
            }

            (sender as Control).NamingContainer.Parent.Controls.Remove(this);
            return;
        }
        #endregion

        #region Other Methods
        private HtmlTableRow AddFirstRow(HtmlTable htmlTable, int colCount)
        {
            HtmlTableRow row = new HtmlTableRow();
            row.BgColor = "gainsboro";
            Common.AddControl(htmlTable, row, false, false);

            HtmlTableCell cell = new HtmlTableCell(); cell.ColSpan = 3; cell.BgColor = "gainsboro";
            Common.AddControl(row, cell, false, false);

            cell = new HtmlTableCell(); cell.ColSpan = colCount + 1; cell.Align = "center";
            cell.Style.Add(HtmlTextWriterStyle.BackgroundImage, "url(" + Common.GetImageUrl("tdBg.gif"));
            cell.Style.Add("background-repeat", "repeat-x");
            Common.AddControl(row, cell, false, false);

            Label l = new Label();
            l.Text = "Hazard Grades";
            l.BackColor = System.Drawing.Color.Gainsboro;
            l.Style.Add(HtmlTextWriterStyle.Padding, "1px 1px 1px 3px;");
            Common.AddControl(cell, l, false, false);

            return row;
        }
        private HtmlTableRow AddSecondRow(HtmlTable htmlTable, BCS.Biz.HazardGradeTypeCollection hazardGradeTypeCollection)
        {
            HtmlTableRow row = new HtmlTableRow();
            row.BgColor = "gainsboro";
            row.Style.Add(HtmlTextWriterStyle.PaddingTop, "5px;");
            row.Style.Add(HtmlTextWriterStyle.PaddingBottom, "5px;");
            Common.AddControl(htmlTable, row, false, false);

            HtmlTableCell cell = new HtmlTableCell();
            cell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "5px;");
            cell.Style.Add(HtmlTextWriterStyle.PaddingRight, "5px;");
            cell.Style.Add("border-bottom", "solid 1px black;");
            Common.AddControl(cell, new Label(), false, false);
            Common.AddControl(row, cell, false, false);

            cell = new HtmlTableCell();
            cell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "5px;");
            cell.Style.Add(HtmlTextWriterStyle.PaddingRight, "5px;");
            cell.Style.Add("border-bottom", "solid 1px black;");
            Common.AddControl(row, cell, false, false);
            Label l = new Label();
            l.Text = "Class Code";
            Common.AddControl(cell, l, false, false);

            cell = new HtmlTableCell();
            cell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "5px;");
            cell.Style.Add(HtmlTextWriterStyle.PaddingRight, "5px;");
            cell.Style.Add("border-bottom", "solid 1px black;");
            Common.AddControl(row, cell, false, false);
            l = new Label();
            l.Text = "SIC Code";
            Common.AddControl(cell, l, false, false);

            foreach (Biz.HazardGradeType hgt in hazardGradeTypeCollection)
            {
                cell = new HtmlTableCell();
                cell.Align = "center";
                cell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "5px;");
                cell.Style.Add(HtmlTextWriterStyle.PaddingRight, "5px;");
                cell.Style.Add("border-bottom", "solid 1px black;");
                Common.AddControl(row, cell, false, false);

                l = new Label();
                l.Text = hgt.TypeName;
                Common.AddControl(cell, l, false, false);
            }

            cell = new HtmlTableCell();
            cell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "5px;");
            cell.Style.Add(HtmlTextWriterStyle.PaddingRight, "5px;");
            cell.Style.Add("border-bottom", "solid 1px black;");
            Common.AddControl(row, cell, false, false);

            return row;
        }
        private HtmlTableRow AddThirdRow(HtmlTable htmlTable, BCS.Biz.HazardGradeTypeCollection hazardGradeTypeCollection)
        {
            HtmlTableRow row = new HtmlTableRow();
            row.Style.Add(HtmlTextWriterStyle.PaddingTop, "5px;");
            row.Style.Add(HtmlTextWriterStyle.PaddingBottom, "5px;");
            Common.AddControl(htmlTable, row, false, false);

            HtmlTableCell cell = new HtmlTableCell();
            cell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "5px;");
            cell.Style.Add(HtmlTextWriterStyle.PaddingRight, "5px;");
            Common.AddControl(row, cell, false, false);
            ImageButton btnSave = new ImageButton();
            btnSave.ID = "btnSave";
            btnSave.Visible = Mode == Common.Mode.Modify;
            btnSave.Enabled = !Components.Security.IsUserReadOnly();
            btnSave.CommandArgument = "ClassCode";
            btnSave.ImageUrl = Common.GetImageUrl("save.gif");
            Common.AddControl(cell, btnSave, false, false);


            cell = new HtmlTableCell();
            cell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "5px;");
            cell.Style.Add(HtmlTextWriterStyle.PaddingRight, "5px;");
            Common.AddControl(row, cell, false, false);
            ClassCodeDropDownList ccddl = new ClassCodeDropDownList();
            ccddl.ID = "ClassCodeDropDownList1";
            ccddl.AutoPostBack = true;
            ccddl.SelectedIndexChanged += new EventHandler(ccddl_SelectedIndexChanged);
            ccddl.CompanyNumberId = this.CompanyNumberId;
            ccddl.DataBind();
            Common.AddControl(cell, ccddl, false, false);

            RequiredFieldValidator rfv = new RequiredFieldValidator();
            rfv.ControlToValidate = ccddl.ID;
            rfv.InitialValue = "0";
            rfv.Display = ValidatorDisplay.Dynamic;
            rfv.ErrorMessage = "*";
            rfv.SetFocusOnError = true;
            Common.AddControl(cell, rfv, false, false);

            cell = new HtmlTableCell();
            cell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "5px;");
            cell.Style.Add(HtmlTextWriterStyle.PaddingRight, "5px;");
            Common.AddControl(row, cell, false, false);
            TextBox tt = new TextBox();
            tt.ID = "txtSICCode";
            tt.ReadOnly = true;
            tt.Width = Unit.Pixel(50);
            Common.AddControl(cell, tt, false, false);

            foreach (Biz.HazardGradeType hgt in hazardGradeTypeCollection)
            {
                cell = new HtmlTableCell();
                cell.BgColor = "white";
                cell.Align = "center";
                cell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "5px;");
                cell.Style.Add(HtmlTextWriterStyle.PaddingRight, "5px;");
                Common.AddControl(row, cell, false, false);

                Label lb = new Label();
                lb.ID = "hazardGradeId" + hgt.Id;
                lb.Width = Unit.Pixel(20);
                lb.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
                lb.Style.Add(HtmlTextWriterStyle.BorderWidth, "0");
                Common.AddControl(cell, lb, false, false);
            }

            cell = new HtmlTableCell();
            cell.Style.Add(HtmlTextWriterStyle.PaddingLeft, "5px;");
            cell.Style.Add(HtmlTextWriterStyle.PaddingRight, "5px;");
            Common.AddControl(row, cell, false, false);
            ImageButton btnRemove = new ImageButton();
            btnRemove.ID = "btnRemove";
            btnRemove.CausesValidation = false;
            btnRemove.Enabled = !Components.Security.IsUserReadOnly();
            btnRemove.Click += new ImageClickEventHandler(btnRemove_Click);
            btnRemove.ImageUrl = Common.GetImageUrl("cancelbig.gif");
            Common.AddControl(cell, btnRemove, false, false);

            if (null != ScriptManager.GetCurrent(this.Page))
            {
                ScriptManager.GetCurrent(Page).RegisterPostBackControl(btnRemove);
            }

            return row;
        }
        private HtmlTableRow AddFourthRow(HtmlTable htmlTable, BCS.Biz.HazardGradeTypeCollection hgtcc)
        {
            HtmlTableRow row = new HtmlTableRow();
            row.Style.Add(HtmlTextWriterStyle.PaddingTop, "5px;");
            row.Style.Add(HtmlTextWriterStyle.PaddingBottom, "5px;");
            Common.AddControl(htmlTable, row, false, false);

            HtmlTableCell cell = new HtmlTableCell();
            Common.AddControl(row, cell, false, false);

            cell = new HtmlTableCell(); cell.ColSpan = 2; cell.BgColor = "gainsboro"; cell.Align = "center";
            cell.InnerText = "Description";
            Common.AddControl(row, cell, false, false);

            cell = new HtmlTableCell(); cell.ColSpan = hgtcc.Count; cell.Align = "center";
            Common.AddControl(row, cell, false, false);

            ClassCodeDropDownList ddlDesc = new ClassCodeDropDownList();
            ddlDesc.ID = "ClassCodeDescDropDownList1";
            ddlDesc.AutoPostBack = true;
            ddlDesc.SelectedIndexChanged += new EventHandler(ddlDesc_SelectedIndexChanged);
            ddlDesc.DataTextField = "Description";
            ddlDesc.CompanyNumberId = this.CompanyNumberId;
            ddlDesc.DataBind();
            Common.AddControl(cell, ddlDesc, false, false);

            return row;
        }
        private void LoadClassCode(BCS.Core.Clearance.BizObjects.ClientClassCode classCode)
        {
            this.ClientClassCodeId = classCode.Id;
            (this.FindControl("ClassCodeDropDownList1") as DropDownList).ClearSelection();
            (this.FindControl("ClassCodeDropDownList1") as DropDownList).Items.FindByValue(classCode.ClassCodeId.ToString()).Selected = true;
            ccddl_SelectedIndexChanged(this.FindControl("ClassCodeDropDownList1"), System.EventArgs.Empty);

            if (classCode.Id != 0)
                (this.FindControl("btnRemove") as WebControl).Attributes.Add("onclick", "return window.confirm(\"Are you sure you want to delete this Class Code?\");");
        }
        public void SetFocus()
        {
            Control c = this.FindControl("ClassCodeDropDownList1");           

            if (null != ScriptManager.GetCurrent(this.Page))
            {
                ScriptManager.GetCurrent(this.Page).SetFocus(c);
                ScriptManager.GetCurrent(this.Page).SetFocus(c.ClientID);
            }
            else
            {
                c.Focus();
            }
        } 
        #endregion

    }
}