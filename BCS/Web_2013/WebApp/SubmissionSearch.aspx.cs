#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;

using OrmLib;
using BCS.Biz;
using BCS.Core; 
using BCS.WebApp.Components;
using BCS.WebApp.UserControls;
using BCS.WebApp;
using Templates = BCS.WebApp.Templates;
using System.Text;
using System.Drawing;
#endregion

public partial class SubmissionSearch : System.Web.UI.Page
{
    #region Page Events
    private const string key4Searched = "Searched";
    private const string key4PreviousSortExpression = "PreviousSortExpression";
    private const string key4PreviousSortDirection = "PreviousSortDirection";
    private const string key4DefaultSort = "DefaultSort";
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void HyperLink1_Init(object sender, EventArgs e)
    {
        HyperLink1.Visible = (User as BCS.Core.Security.CustomPrincipal).IsInAnyRoles("User", "Partial User", "Auditor", "System Administrator");
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        if (Session["CompanyChanged"] != null)
        {
            Session.Contents.Remove("CompanyChanged");
            Response.Redirect("UnderwriterSubmissionSearch.aspx");
        }
        LoadSearch();
        btable.Visible = Session["FilterCriteria"] != null && gvSubmissions.Rows.Count != 0;
        base.OnLoadComplete(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        if (!IsPostBack)
        {
            AgencyDropDownList1.DataBind();
            LineOfBusinessDropDownList1.DataBindByCompanyNumber(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));

            SubmissionTypeDropDownList1.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
            SubmissionTypeDropDownList1.DataBind();

            SubmissionStatusDropDownList1.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
            SubmissionStatusDropDownList1.DataBind();
        }
        
        base.OnLoad(e);
    }
    #endregion

    #region Control Events
    protected void DateBox_Init(object sender, EventArgs e)
    {
        (sender as TextBox).Attributes.Add("onclick", "scwShow(this,this);");
        (sender as TextBox).Attributes.Add("onkeydown", "hideCalOnTab(event);");
        (sender as TextBox).Attributes.Add("onblur", "this.value = purgeDate(this);");
    }

    protected void gvSubmissions_Init(object sender, EventArgs e)
    {
        #region required fields independent of company

        // col id, required
        BoundField bf = new BoundField();
        bf.HeaderText = "Id";
        bf.DataField = "Id";
        bf.Visible = false;
        gvSubmissions.Columns.Add(bf);

        //// col duplicate selection, required for duplicate selection along with the submission number and sequence
        TemplateField tf = new TemplateField();
        //tf.ItemTemplate = new Templates.CheckBoxTemplate(DataControlRowType.EmptyDataRow, BCS.WebApp.Templates.HeaderType.Text, "", "");
        //tf.HeaderTemplate = new Templates.CheckBoxTemplate(DataControlRowType.Header, BCS.WebApp.Templates.HeaderType.SelectAll, "", "");
        //gvSubmissions.Columns.Add(tf);

        // col client core client id, TODO: probably not needed
        bf = new BoundField();
        bf.HeaderText = "clientcore_client_id";
        bf.DataField = "clientcore_client_id";
        bf.Visible = false;
        gvSubmissions.Columns.Add(bf);

        // col submission no
        ButtonField snf = new ButtonField();
        snf.CommandName = "Select";
        snf.HeaderText = "Submission Number";
        snf.DataTextField = "submission_number";
        snf.ItemStyle.ForeColor = Color.FromArgb(00, 40, Convert.ToInt32("BF", 16));
        if (gvSubmissions.AllowSorting)
            snf.SortExpression = snf.DataTextField;
        gvSubmissions.Columns.Add(snf);

        // col submission no sequence
        BoundField snsf = new BoundField();
        snsf.HeaderText = "Sequence";
        snsf.DataField = "submission_number_sequence";
        if (gvSubmissions.AllowSorting)
            snsf.SortExpression = snsf.DataField;
        gvSubmissions.Columns.Add(snsf);

        // col client address, required for making associated submissions visually distinct
        tf = new TemplateField();
        tf.ItemTemplate = new Templates.LabelTemplate(DataControlRowType.DataRow, "address", "clientcore_address_id", "lblClientAddress");
        tf.Visible = false;
        gvSubmissions.Columns.Add(tf);


        #endregion
        BCS.Biz.SubmissionDisplayGridCollection dfields = ConfigValues.GetSubmissionGridDisplay(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));

        #region Some Customization on required fields #3709
        // see if any customization is there on the submission_number column
        BCS.Biz.SubmissionDisplayGrid customField = dfields.FindByFieldName("submission_number");
        if (customField != null)
        {
            snf.HeaderText = string.IsNullOrEmpty(customField.HeaderText) ? customField.FieldName : customField.HeaderText;
            dfields.Remove(customField);
        }
        // see if any customization is there on the submission_number_sequence column
        customField = dfields.FindByFieldName("submission_number_sequence");
        if (customField != null)
        {
            snsf.HeaderText = string.IsNullOrEmpty(customField.HeaderText) ? customField.FieldName : customField.HeaderText;
            dfields.Remove(customField);
        }
        #endregion
        foreach (SubmissionDisplayGrid cfield in dfields)
        {
            TemplateField atf = new TemplateField();
            if (gvSubmissions.AllowSorting)
                atf.SortExpression = cfield.FieldName;
            atf.ItemTemplate = new Templates.GenericTemplate(DataControlRowType.DataRow, string.Empty, cfield.FieldName, cfield.AttributeDataType.DataTypeName, string.Empty);
            //atf.HeaderTemplate = new Templates.GenericTemplate(DataControlRowType.Header, string.IsNullOrEmpty(cfield.HeaderText) ? cfield.FieldName : cfield.HeaderText, string.Empty, string.Empty);
            atf.HeaderText = string.IsNullOrEmpty(cfield.HeaderText) ? cfield.FieldName : cfield.HeaderText;

            gvSubmissions.Columns.Add(atf);
        }
    }
    protected void gvSubmissions_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ViewState["PossibleUpdates"] = BuildPossibleUpdates();
        string sort = string.Empty;
        if (ViewState[key4PreviousSortExpression] != null && ViewState[key4PreviousSortDirection] != null)
        {
            sort = string.Format("{0} {1}", ViewState[key4PreviousSortExpression], ViewState[key4PreviousSortDirection]);
        }
        if (string.IsNullOrEmpty(sort))
        {
            sort = (string)ViewState[key4DefaultSort];
        }
        LoadSubmissions(e.NewPageIndex, sort);
    }
    protected void gvSubmissions_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
    }
    protected void gvSubmissions_DataBound(object sender, EventArgs e)
    {
        dataRowCount.Value = gvSubmissions.Rows.Count.ToString();
    }
        
    protected void AgencyDropDownList1_Init(object sender, EventArgs e)
    {
        Type t = sender.GetType();
        System.Reflection.PropertyInfo pi = t.GetProperty("CompanyNumberId");
        if (pi != null)
        {
            pi.SetValue(sender, Convert.ToInt32(Session["CompanyNumberSelected"]), new object[] { });
        }
        //AgencyDropDownList1.DataBind();
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        if (Common.GetSafeIntFromSession(SessionKeys.CompanyId) == 0)
        {
            Common.SetStatus(messagePlaceHolder, "Please select a company.");
            return;
        }
        if (Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) == 0)
        {
            Common.SetStatus(messagePlaceHolder, "Please select a company number.");
            return;
        }

        Session.Contents.Remove("FilterCriteria");
        Hidden1.Value = "0";
        if(Page.IsPostBack) // if only user clicked the button, this same method is also used when restore results back from other screens
            selectAllState.Value = "0";
        

        LoadSubmissions(0, string.Empty);
    }
    private void LoadSubmissions(int pageIndex, string sort)
    {
        string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);

        ArrayList filters = new ArrayList();

        if (txSubmissionStartDate.Text.Length > 0)
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.SubmissionDt, txSubmissionStartDate.Text, OrmLib.MatchType.GreaterOrEqual));
        }

        if (txSubmissionEndDate.Text.Length > 0)
        {
            DateTime date;
            DateTime.TryParse(txSubmissionEndDate.Text, out date);

            date = date.AddDays(1);

            filters.Add(new Triplet(JoinPath.Submission.Columns.SubmissionDt,date.ToString(), OrmLib.MatchType.LesserOrEqual));
        }

        if (AgencyDropDownList1.SelectedIndex > 0)
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.AgencyId, AgencyDropDownList1.SelectedValue, OrmLib.MatchType.Exact));
        }

        if (LineOfBusinessDropDownList1.SelectedIndex > 0)
        {
            filters.Add(new Triplet(JoinPath.Submission.SubmissionLineOfBusiness.Columns.LineOfBusinessId,
                LineOfBusinessDropDownList1.SelectedValue, OrmLib.MatchType.Exact));
        }
        if (SubmissionTypeDropDownList1.SelectedIndex > 0)
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.SubmissionTypeId,
                SubmissionTypeDropDownList1.SelectedValue, OrmLib.MatchType.Exact));
        }
        if (SubmissionStatusDropDownList1.SelectedIndex > 0)
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.SubmissionStatusId,
                SubmissionStatusDropDownList1.SelectedValue, OrmLib.MatchType.Exact));
        }
        if (txEffFrom.Text.Length > 0)// TODO: VALIDATE
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.EffectiveDate, txEffFrom.Text, OrmLib.MatchType.GreaterOrEqual));
            filters.Add(new Triplet(JoinPath.Submission.Columns.EffectiveDate, txEffTo.Text, OrmLib.MatchType.LesserOrEqual));
        }
        if (txExpFrom.Text.Length > 0)// TODO: VALIDATE
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.ExpirationDate, txExpFrom.Text, OrmLib.MatchType.GreaterOrEqual));
            filters.Add(new Triplet(JoinPath.Submission.Columns.ExpirationDate, txExpTo.Text, OrmLib.MatchType.LesserOrEqual));
        }
        if (txPremium.Text.Length > 0)
        {
            decimal d = decimal.Zero;
            decimal.TryParse(txPremium.Text, out d);
            OrmLib.MatchType m = (OrmLib.MatchType)Enum.Parse(typeof(OrmLib.MatchType), ddlOperators.SelectedValue);
            filters.Add(new Triplet(JoinPath.Submission.Columns.EstimatedWrittenPremium, d, m));
        }
       


        if (filters.Count == 0)
        {
            gvSubmissions.DataSource = null;
            gvSubmissions.DataBind();
            return;
        }

        Triplet[] criterion = new Triplet[filters.Count];
        filters.CopyTo(criterion);
        Session["FilterCriteria"] = criterion;
        string xml = BCS.Core.Clearance.Submissions.GetSubmissions(companyNumber,false, criterion);
        DataTable dt = Common.GenerateDataTable(companyNumber, "submission", xml);

        ExtractIds(dt);

        if (dt != null)
        {
            DataTable ndt = dt.Clone();
            ndt.Columns["submission_number"].DataType = typeof(int);
            ndt.Columns["submission_number_sequence"].DataType = typeof(int);
            foreach (DataRow dr in dt.Rows)
                ndt.ImportRow(dr);
            ndt.AcceptChanges();
            if (!string.IsNullOrEmpty(sort.Trim()))
            {
                ndt.DefaultView.Sort = sort;
            }
            else
            {
                ndt.DefaultView.Sort = dt.DefaultView.Sort;
                ViewState[key4DefaultSort] = dt.DefaultView.Sort;
            }

            gvSubmissions.PageIndex = pageIndex;
            gvSubmissions.DataSource = ndt;
            gvSubmissions.DataBind();
        }
        else
        {
            gvSubmissions.DataSource = dt;
            gvSubmissions.DataBind();
        }
    }
    protected void gvSubmissions_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["PossibleUpdates"] = BuildPossibleUpdates();
        
        string sortDirection = " ASC";
        if ((string)ViewState[key4PreviousSortExpression] == e.SortExpression)
        {
            if ((string)ViewState[key4PreviousSortDirection] == sortDirection)
            {
                sortDirection = " DESC";
            }
        }
        ViewState[key4PreviousSortDirection] = sortDirection;
        ViewState[key4PreviousSortExpression] = e.SortExpression;

        string sort = e.SortExpression.ToString() + sortDirection;

        LoadSubmissions(gvSubmissions.PageIndex, sort);

    }

    private void ExtractIds(DataTable dataTable)
    {
        if (dataTable != null)
        {
            StringBuilder dups = new StringBuilder();

            foreach (DataRow var in dataTable.Rows)
            {
                dups.AppendFormat("{0},", var["Id"].ToString());
            }
            ViewState["AllIds"] = dups;
        }
    }
    protected void btnReassign_Click(object sender, EventArgs e)
    {
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        AgencyDropDownList1.SelectedIndex = 0;
        txSubmissionEndDate.Text = string.Empty;
        txSubmissionStartDate.Text = string.Empty;
        LineOfBusinessDropDownList1.SelectedIndex = 0;
        SubmissionStatusDropDownList1.SelectedIndex = 0;
        SubmissionTypeDropDownList1.SelectedIndex = 0;

        txEffFrom.Text = string.Empty;
        txEffTo.Text = string.Empty;
        txExpFrom.Text = string.Empty;
        txExpTo.Text = string.Empty;
        txPremium.Text = string.Empty;  
        
        btnFilter_Click(null, EventArgs.Empty);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("SubmissionSearch.aspx");
    }
    protected void btnReassign_Init(object sender, EventArgs e)
    {
    }

    protected void uddl_Init(object sender, EventArgs e)
    {
        (sender as UnderwritersDropDownList).CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        (sender as UnderwritersDropDownList).DataBindByCompanyNumber();
    }

    protected void gvSubmissions_SelectedIndexChanged(object sender, EventArgs e)
    {
        SaveSearch();
        GridView gv = sender as GridView;
        Session["SubmissionId"] = gv.SelectedDataKey.Value;
        Session["SubmissionId"] = gv.SelectedDataKey.Values["id"];
        Session["ClientId"] = gv.SelectedDataKey.Values["ClientCore_Client_Id"];
        Session["subref"] = "SubmissionSearch.aspx";
        if (DefaultValues.UseWizard)
            Response.Redirect("SubmissionWizard.aspx");
        Response.Redirect("Submission.aspx?op=edit");
    }

    protected void ValidationButton_Init(object sender, EventArgs e)
    {
        //Button btn = sender as Button;
        //btn.Attributes.Add("onclick", string.Format("if(!Page_ClientValidate('{0}')) {{  $find('alwaysVisible').get_element().style.display = ''; $find('alwaysVisible').initialize(); }}", btn.ValidationGroup));
    }


   
    #endregion

    #region Other Methods

    private void SaveSearch()
    {
        ReassignScreen thisScreen = new ReassignScreen();
        thisScreen.AgencyId = Common.GetSafeSelectedValue(AgencyDropDownList1);
        thisScreen.FromEff = txEffFrom.Text;
        thisScreen.ToEff = txEffTo.Text;
        thisScreen.FromExp = txExpFrom.Text;
        thisScreen.ToExp = txExpTo.Text;
        thisScreen.LobId = Common.GetSafeSelectedValue(LineOfBusinessDropDownList1);
        thisScreen.TypeId = Common.GetSafeSelectedValue(SubmissionTypeDropDownList1);
        thisScreen.StatusId = Common.GetSafeSelectedValue(SubmissionStatusDropDownList1);

        thisScreen.SubmissionDateFrom = txSubmissionStartDate.Text;
        thisScreen.SubmissionDateTo = txSubmissionEndDate.Text;
        
        thisScreen.FromExp = txEffFrom.Text;
        thisScreen.ToExp = txEffTo.Text;
        thisScreen.FromExp = txExpFrom.Text;
        thisScreen.ToExp = txExpTo.Text;
        thisScreen.PremiumComparisonType = ddlOperators.Text;
        decimal d = decimal.Zero;
        decimal.TryParse(txPremium.Text, out d);
        thisScreen.Premium = d;
        thisScreen.PossibleUpdates = BuildPossibleUpdates();
        thisScreen.SelectedAll = selectAllState.Value;
        thisScreen.Sort = string.Format("{0} {1}", ViewState[key4PreviousSortExpression], ViewState[key4PreviousSortDirection]);
        thisScreen.PageIndex = gvSubmissions.PageIndex;
        SearchCriteria sc = new SearchCriteria();
        sc.ReassignScreen = thisScreen;
        Session[SessionKeys.SearchedCriteria] = sc;
    }
    private void LoadSearch()
    {
        if (null != Session["LoadSavedSearch"])
        {
            Session.Contents.Remove("LoadSavedSearch");
            if (null != Session[SessionKeys.SearchedCriteria])
            {
                // TODO: 
                SearchCriteria sc = (SearchCriteria)Session[SessionKeys.SearchedCriteria];
                AgencyDropDownList1.Text = sc.ReassignScreen.AgencyId.ToString();
                LineOfBusinessDropDownList1.Text = sc.ReassignScreen.LobId.ToString();
                SubmissionTypeDropDownList1.Text = sc.ReassignScreen.TypeId.ToString();
                SubmissionStatusDropDownList1.Text = sc.ReassignScreen.StatusId.ToString();
                txEffFrom.Text = sc.ReassignScreen.FromEff;
                txEffTo.Text = sc.ReassignScreen.ToEff;
                txExpFrom.Text = sc.ReassignScreen.FromExp;
                txExpTo.Text = sc.ReassignScreen.ToExp;
                txPremium.Text = sc.ReassignScreen.Premium == decimal.Zero ? string.Empty : string.Format("{0:c}", sc.ReassignScreen.Premium);
                ViewState["PossibleUpdates"] = sc.ReassignScreen.PossibleUpdates;
                selectAllState.Value = sc.ReassignScreen.SelectedAll;
                txSubmissionStartDate.Text = sc.ReassignScreen.SubmissionDateFrom;
                txSubmissionEndDate.Text = sc.ReassignScreen.SubmissionDateTo;

                LoadSubmissions(sc.ReassignScreen.PageIndex, sc.ReassignScreen.Sort);
            }
        }
    }
    private bool ReadyState()
    {
        return Hidden1.Value != "0" || selectAllState.Value != "0" || 
            (ViewState["PossibleUpdates"] != null && ViewState["PossibleUpdates"].ToString().Length > 0);
    }
    private string BuildPossibleUpdates()
    {
        return string.Empty;
    } 
    #endregion
}
