using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class LFVDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void DetailsView1_DataBound(object sender, EventArgs e)
    {
        foreach (DetailsViewRow dvRow in DetailsView1.Rows)
        {
            if (dvRow.Cells[0].Text == "Level")
            {
                if (dvRow.Cells[1].Text == "FATAL")
                {
                    DetailsView1.FieldHeaderStyle.BackColor = System.Drawing.Color.Red;
                }
                if (dvRow.Cells[1].Text == "ERROR")
                {
                    DetailsView1.FieldHeaderStyle.BackColor = System.Drawing.Color.Tan;
                }
                if (dvRow.Cells[1].Text == "WARN")
                {
                    DetailsView1.FieldHeaderStyle.BackColor = System.Drawing.Color.DarkKhaki;
                }
                if (dvRow.Cells[1].Text == "INFO")
                {
                    DetailsView1.FieldHeaderStyle.BackColor = System.Drawing.Color.LightGreen;
                }
            }
        }
    }
}
