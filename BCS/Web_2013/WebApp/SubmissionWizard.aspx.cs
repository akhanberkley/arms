#region Usings
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.Components;
using BCS.Core.Clearance.BizObjects;
using BCS.WebApp.UserControls;
using System.Collections.Generic;
using Structures = BTS.ClientCore.Wrapper.Structures;
using BTS.LogFramework;
using System.Text;
using AjaxControlToolkit;
using System.Reflection;
using BCS.WebApp.Comparers;
using BCS.WebApp;
using System.Data.Linq;
using System.Linq.Expressions;
using System.Linq;
#endregion

public partial class SubmissionWizard : System.Web.UI.Page
{
    #region Properties
    protected int DefaultStatus
    {
        get
        {
            if (ViewState["DefaultStatus"] == null)
            {
                int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
                BCS.Biz.CompanyNumberSubmissionStatusCollection statuses = SubmissionStatusDropDownList.GetStatuses(companyNumberId);
                if (statuses == null)
                    return -1;
                BCS.Biz.CompanyNumberSubmissionStatus status = statuses.FindByDefaultSelection(true);
                if (status == null)
                    return -1;

                return status.SubmissionStatusId;
            }
            return (int)ViewState["DefaultStatus"];
        }
        set
        {
            ViewState["DefaultStatus"] = value;
        }
    }
    protected Dictionary<int, bool> ValidSteps
    {
        get
        {
            if (ViewState["ValidSteps"] == null)
                return new Dictionary<int, bool>();
            return (Dictionary<int, bool>)ViewState["ValidSteps"];
        }
        set
        {
            ViewState["ValidSteps"] = value;
        }
    }
    protected SubmissionLineOfBusinessChild[] LOBS
    {
        get
        {
            if (ViewState["LOBS"] != null)
            {
                return (SubmissionLineOfBusinessChild[])ViewState["LOBS"];
            }
            else
            {
                return new SubmissionLineOfBusinessChild[0];
            }
        }
        set
        {
            ViewState["LOBS"] = value;
        }
    }
    protected SubmissionComment[] Comments
    {
        get
        {
            if (ViewState["Comments"] != null)
            {
                return (SubmissionComment[])ViewState["Comments"];
            }
            else
            {
                return new SubmissionComment[0];
            }
        }
        set
        {
            ViewState["Comments"] = value;
        }
    }
    protected List<UnderwritersDropDownList.PersonAps> UwApsPersonCollection
    {
        get
        {
            if (Session["uwApsPersonCollection"] != null)
            {
                return (List<UnderwritersDropDownList.PersonAps>)Session["uwApsPersonCollection"];
            }
            else
            {
                return null;
            }
        }
        set
        {
            Session["uwApsPersonCollection"] = value;
        }
    }
    protected List<UnderwritersDropDownList.PersonAps> AnApsPersonCollection
    {
        get
        {
            if (Session["anApsPersonCollection"] != null)
            {
                return (List<UnderwritersDropDownList.PersonAps>)Session["anApsPersonCollection"];
            }
            else
            {
                return null;
            }
        }
        set
        {
            Session["anApsPersonCollection"] = value;
        }
    }
    protected bool ShowGrid
    {
        get
        {
            if (ViewState["ShowGrid"] == null)
                return ConfigValues.AgenciesInGrid(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            else
                return (bool)ViewState["ShowGrid"];
        }
        set
        {
            ViewState["ShowGrid"] = value;
        }
    }

    protected bool IsReadOnly
    {
        get
        {
            if (hdnSubmissionIsDeleted.Value == "True")
                return true;
            return false;
        }
    }

    protected bool IsOfacHit { get; set; }

    #endregion

    #region Page Events
    protected override void OnPreInit(EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["id"] != null)
            {
                int submissionID = 0;
                int currentSubmissionID = 0;
                bool redirect = false;

                if (Session["SubmissionId"] != null)
                    Int32.TryParse(Session["SubmissionId"].ToString(), out currentSubmissionID);

                Int32.TryParse(Request.QueryString["id"].ToString(), out submissionID);

                if (submissionID > 0)
                {
                    //Validate User
                    Site_PATS masterPage = (Site_PATS)Page.Master;
                    masterPage.AuthorizationCheck();

                    //Load Company Information
                    if (Common.LoadUserInformationBySubmissionID(submissionID))
                    {
                        if (currentSubmissionID != submissionID)
                            Response.Write("<script>javascript:window.location.reload();</script>");
                    }
                    else
                    {
                        redirect = true;
                    }
                }

                if (submissionID == 0 || redirect)
                {
                    Response.Write("<script>javascript:alert('The Submission ID was not found in Clearance');window.location='default.aspx';</script>");
                    Response.End();
                }

                ViewState["CompanyNumberId"] = Session[SessionKeys.CompanyNumberId];
            }
        }
        base.OnPreInit(e);
    }

    protected override void OnInit(EventArgs e)
    {
        // invalid session
        if (Common.GetSafeIntFromSession(SessionKeys.CompanyId) == 0)
            Response.Redirect("Default.aspx");

        LoadControlsForCodeTypesAndAttributes();
        Load4Copy();

        if (!IsPostBack)
        {
            AgencyDropDownList1.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
            Session.Remove("CopiedSubmissionId");
            Session.Remove("copystatushistories");
            if (Session["SubmissionId"] != null)
                hdnSubmission.Value = (string)Session["SubmissionId"].ToString();
        }
        else if (!(Session["copystatushistories"] != null && (bool)Session["copystatushistories"]) && Session["CopiedSubmissionId"] != null)
        {
            Session.Remove("CopiedSubmissionId");
        }

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DateTime startDTPageLoadTime = DateTime.Now;

            // just databind so that it displays empty text
            LOBList4Display.DataBind();
            gridSelectedAddresses.DataBind();
            gridSelectedNames.DataBind();
            gridComments4Display.DataBind();

            if (DefaultValues.SupportsAgentUnspecifiedReasons(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                cblAgentUnspecifiedReasons.DataBind();

            #region set tabbed fields
            WizardConfigSteps steps = ConfigValues.GetWizardSteps(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

            string nextControlId = string.Empty;
            string prevControlId = string.Empty;
            string nextButtonId = string.Empty;
            string prevButtonId = string.Empty;

            foreach (WizardConfigStep var in steps.Steps)
            {
                nextControlId = var.TabForward;
                prevControlId = var.TabBackward;

                if (var.Index == 0) // start template, no previous only next
                {
                    Control navControl = Wizard1.FindControl("StartNavigationTemplateContainerID");
                    Control nextControl = navControl.FindControl("StepNextButton");
                    nextButtonId = nextControl.UniqueID;
                }
                else if (var.Index == steps.Steps.Count - 1) // finish step, finish OR previous no next
                {
                    Control navControl = Wizard1.FindControl("FinishNavigationTemplateContainerID");
                    Control nextControl = navControl.FindControl("StepFinishButton");
                    nextButtonId = nextControl.UniqueID;
                    Control prevControl = navControl.FindControl("StepPreviousButton");
                    prevButtonId = prevControl.UniqueID;
                }
                else // intermediate steps
                {
                    Control navControl = Wizard1.FindControl("StepNavigationTemplateContainerID");
                    Control nextControl = navControl.FindControl("StepNextButton");
                    nextButtonId = nextControl.UniqueID;
                    Control prevControl = navControl.FindControl("StepPreviousButton");
                    prevButtonId = prevControl.UniqueID;
                }

                // if both forward tab and backward tab controls are one
                if (nextControlId == prevControlId)
                {
                    Control FC = Common.FindControlRecursive(Wizard1, nextControlId);

                    if (!string.IsNullOrEmpty(nextButtonId) || !string.IsNullOrEmpty(prevButtonId))
                    {
                        if (FC is WebControl)
                        {
                            WebControl wc = FC as WebControl;
                            wc.Attributes.Add("onkeydown", wc.Attributes["onkeydown"] + string.Format(
                                "javascript:if (event.keyCode==09) {{ {0} {1} }}",
                                string.IsNullOrEmpty(nextButtonId) ? string.Empty : string.Format("if(!event.shiftKey){{ __doPostBack('{0}','');if ((Sys.Browser.agent === Sys.Browser.InternetExplorer) && (Sys.Browser.version >= 7)) {{ cancelEvent(event); }} }} ", nextButtonId),
                                string.IsNullOrEmpty(prevButtonId) ? string.Empty : string.Format("if(event.shiftKey) {{ __doPostBack('{0}','');if ((Sys.Browser.agent === Sys.Browser.InternetExplorer) && (Sys.Browser.version >= 7)) {{ cancelEvent(event); }} }} ", prevButtonId)));
                        }
                    }
                }
                else
                {
                    Control FC = Common.FindControlRecursive(Wizard1, nextControlId);
                    Control BC = Common.FindControlRecursive(Wizard1, prevControlId);

                    if (!string.IsNullOrEmpty(nextButtonId))
                    {
                        if (FC is WebControl)
                        {
                            WebControl wc = FC as WebControl;
                            wc.Attributes.Add("onkeydown", wc.Attributes["onkeydown"] + string.Format("javascript:if (event.keyCode==09) {{ if(!event.shiftKey) {{ __doPostBack('{0}','');if ((Sys.Browser.agent === Sys.Browser.InternetExplorer) && (Sys.Browser.version >= 7)) {{ cancelEvent(event); }} }} }}", nextButtonId));
                        }
                    }
                    if (!string.IsNullOrEmpty(prevButtonId))
                    {
                        if (BC is WebControl)
                        {
                            WebControl wc = BC as WebControl;
                            wc.Attributes.Add("onkeydown", wc.Attributes["onkeydown"] + string.Format("javascript:if (event.keyCode==09) {{ if(event.shiftKey) {{ __doPostBack('{0}','');if ((Sys.Browser.agent === Sys.Browser.InternetExplorer) && (Sys.Browser.version >= 7)) {{ cancelEvent(event); }} }}  }}", prevButtonId));
                        }
                    }
                }

            }

            #endregion
            if (DefaultValues.SupportsMultipleLineOfBusinesses(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
            {
                SubmissionLineOfBusinessChild[] forFooter = new SubmissionLineOfBusinessChild[1];
                forFooter[0] = new SubmissionLineOfBusinessChild();
                LOBFooter.DataSource = forFooter;
                LOBFooter.DataBind();
            }

            //remove the CopiedSubmissionId if still in session.
            Session.Remove("CopiedSubmissionId");

            //Set policy number validation
            //
            if (Session["SubmissionId"] != null)
            {
                //SubmissionStatusDropDownList2.DataBind();
                #region ui visibility
                Wizard1.Visible = false;
                divMenu.Visible = true;

                divAddHeader.Visible = false;
                divEditHeader.Visible = true;
                #endregion

                #region set invisible/disabled fields

                #region Set Disabled based on company config
                string[] editDisabledFields = ConfigValues.GetEditDisabledFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                foreach (string var in editDisabledFields)
                {
                    // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
                    // as parameter to FindControlRecursive method
                    Control c = Common.FindControlRecursive(Wizard1, var.Trim());
                    if (c != null)
                    {
                        if (c is WebControl)
                        {
                            WebControl wc = c as WebControl;
                            wc.Enabled = false;
                        }
                        if (c is UserControl)
                        {
                            PropertyInfo pi = c.GetType().GetProperty("Enabled");
                            if (pi != null)
                                pi.SetValue(c, false, null);
                        }
                    }
                }
                #endregion
                #region Set Invisible based on company config
                string[] editInvisibleFields = ConfigValues.GetEditInvisibleFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                foreach (string var in editInvisibleFields)
                {
                    // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
                    // as parameter to FindControlRecursive method
                    Control c = Common.FindControlRecursive(Wizard1, var.Trim());
                    if (c != null)
                    {
                        if (c is WebControl)
                        {
                            c.Visible = false;
                        }
                        if (c is UserControl)
                        {
                            PropertyInfo pi = c.GetType().GetProperty("Visible");
                            if (pi != null)
                                pi.SetValue(c, false, null);
                        }
                    }
                }
                #endregion
                #endregion
                int id = Convert.ToInt32(Session["SubmissionId"]);
                SubmissionProxy.Submission s = Common.GetSubmissionProxy();
                BCS.Core.Clearance.BizObjects.SubmissionList sl = (BCS.Core.Clearance.BizObjects.SubmissionList)
                    BCS.Core.XML.Deserializer.Deserialize(
                    s.GetSubmissionById(id), typeof(BCS.Core.Clearance.BizObjects.SubmissionList));

                xLoadSubmission(sl.List[0]);

                if (IsReadOnly)
                {
                    SetPageToReadOnly();
                }

                // manually setting type to not loaded in case of edits, so that wizard step can pick up and display correct setting in the drop down
                TypeLoaded.Text = Boolean.FalseString; // TODO: evaluate the need

                submissionStatusReason.Visible = ConfigValues.SupportsSnapShotEditing(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) && !IsReadOnly;
                spSubmissionStatusLabel.Visible = !submissionStatusReason.Visible;

                //spSubmissionStatusIdBox.Visible = ConfigValues.SupportsSnapShotEditing(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                spSubmissionStatusDateLabel.Visible = !submissionStatusReason.Visible;

                if (submissionStatusReason.Visible)
                {
                    submission_row_status.Attributes.Add("class", "displayedNone");
                    submission_row_status_reason.Attributes.Add("class", "displayedNone");
                    submission_row_status_date.Attributes.Add("class", "displayedNone");
                    submissionStatusReason.SetFocus();
                }


                //btnUpdateFromSnapShot.Visible = ConfigValues.SupportsSnapShotEditing(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                //ScriptManager1.SetFocus(SubmissionStatusDropDownList2); // to make sure the script reference for the script method WebForm_AutoFocus gets added.
            }
            else
            {
                if (ConfigValues.DesiresLoadClientBeforeStep(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                {
                    #region Client
                    DateTime startDTClientLoadTime = DateTime.Now;
                    GetAndLoadClient(Session["ClientId"].ToString());
                    DateTime endDTClientLoadTime = DateTime.Now;
                    TimeSpan tsClientLoadTime = endDTClientLoadTime.Subtract(startDTClientLoadTime);
                    if (LogInfoMessages.Text == Boolean.TrueString)
                    {
                        LogCentral.Current.LogInfo(string.Format("tsClientLoadTime {0}", tsClientLoadTime.TotalSeconds));
                    }

                    DefaultSelect(gvClientAddresses, "addressCheckBox");
                    //DefaultSelect(NameList);
                    DefaultSelect(gvClientNames, "nameCheckBox");
                    #region set addresses
                    // set client states for licensed state warning
                    if (DefaultValues.EnforceAgencyLicensedState(Common.GetSafeIntFromSession("CompanySelected")))
                        SetClientStates();
                    #endregion
                    #region set names
                    // default select if only one

                    // for edits we are already loading the client on the submission load itself, so this place should be fine for the enhancement request
                    // to auto-select all names on submission adds
                    if (ConfigValues.AutoSelectNamesOnSubmissionAdd(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                    {
                        /*foreach (ListItem var in NameList.Items)
                        {
                            // for input
                            var.Selected = true;
                        }
                         * */

                        for (int i = 0; i < gv.Rows.Count; i++)
                        {
                            CheckBox cb = (CheckBox)gvClientNames.Rows[i].FindControl("ClientNameType");
                            cb.Checked = true;
                        }
                    }
                    PopulateData(Wizard1.WizardSteps.Count - 1); // this is needed since there are auto-selects
                    #endregion
                    ClientLoaded.Text = Boolean.TrueString;
                    #endregion
                }
                SavedState.Text = "0";
                #region set invisible/disabled fields

                #region Set Disabled based on company config
                string[] addDisabledFields = ConfigValues.GetAddDisabledFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                foreach (string var in addDisabledFields)
                {
                    // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
                    // as parameter to FindControlRecursive method
                    Control c = Common.FindControlRecursive(Wizard1, var.Trim());
                    if (c != null)
                    {
                        if (c is WebControl)
                        {
                            WebControl wc = c as WebControl;
                            wc.Enabled = false;
                        }
                        if (c is UserControl)
                        {
                            PropertyInfo pi = c.GetType().GetProperty("Enabled");
                            if (pi != null)
                                pi.SetValue(c, false, null);
                        }
                    }
                }
                #endregion
                #region Set Invisible based on company config
                string[] addInvisibleFields = ConfigValues.GetAddInvisibleFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                foreach (string var in addInvisibleFields)
                {
                    // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
                    // as parameter to FindControlRecursive method
                    Control c = Common.FindControlRecursive(Wizard1, var.Trim());
                    if (c != null)
                    {
                        if (c is WebControl)
                        {
                            c.Visible = false;
                        }
                        if (c is UserControl)
                        {
                            PropertyInfo pi = c.GetType().GetProperty("Visible");
                            if (pi != null)
                                pi.SetValue(c, false, null);
                        }
                    }
                }

                submissionStatusReason.Visible = false;
                spSubmissionStatusLabel.Visible = !submissionStatusReason.Visible;

                //spSubmissionStatusIdBox.Visible = ConfigValues.SupportsSnapShotEditing(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                spSubmissionStatusDateLabel.Visible = !submissionStatusReason.Visible;

                if (submissionStatusReason.Visible)
                {
                    submission_row_status.Attributes.Add("class", "displayedNone");
                    submission_row_status_reason.Attributes.Add("class", "displayedNone");
                    submission_row_status_date.Attributes.Add("class", "displayedNone");
                }

                #endregion
                #endregion
                #region populate the first step things
                int firstStep = GetAddDefaultStepIndex();
                PopulateControls(firstStep);
                Wizard1.ActiveStepIndex = firstStep;
                // set the command argument finish button on intermediate navigation steps
                Common.FindControlRecursive(Wizard1, "StepFinishButton");
                #endregion

                submission_row_underwriter_previous.Style.Add(HtmlTextWriterStyle.Display, "none");

                #region identified duplicates handling
                if (Session["PossibleDuplicates"] != null)
                {
                    if (Session["PossibleDuplicates"].ToString().Length > 0)
                    {
                        string s = "Possible Duplicate: {0}";
                        StringBuilder sb = new StringBuilder(Session["PossibleDuplicates"].ToString());
                        sb.Remove(sb.Length - 1, 1);
                        if (sb.ToString().Contains(","))
                        {
                            sb.Replace(";", " and ", sb.ToString().LastIndexOf(",") - 1, 2);
                            sb.Replace(";", "; ");
                        }
                        txComments.Text = string.Format(s, sb).TrimEnd(';');
                        AddComment();
                    }
                    Session.Contents.Remove("PossibleDuplicates");
                }
                #endregion
            }

            DateTime endDTPageLoadTime = DateTime.Now;
            TimeSpan tsPageLoadTime = endDTPageLoadTime.Subtract(startDTPageLoadTime);
            if (LogInfoMessages.Text == Boolean.TrueString)
            {
                LogCentral.Current.LogInfo(string.Format("tsPageLoadTime {0}", tsPageLoadTime.TotalSeconds));
            }

            // TODO: had to do the initializations here any place before load having the same peculiar affect as with initiliazing check boxes oninit
            #region Initializations
            List<SubmissionField> fields1 = ConfigValues.GetMappingFields(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
            foreach (SubmissionField field in fields1)
            {
                WebControl baseControl = Common.FindControlRecursive(Page, field.ControlId) as WebControl;
                if (null != baseControl)
                {
                    foreach (FieldPair var in field.AssociatedControlIds)
                    {
                        if (var.Type.Contains("Id"))
                        {
                            Control targetControl = Common.FindControlRecursive(Page, var.Id);
                            if (null != targetControl)
                            {
                                if (baseControl is CheckBoxList)
                                {
                                    baseControl.Attributes.Add("onmouseout", string.Format(" setCBLValue('{0}','{1}');", baseControl.ClientID, targetControl.ClientID));
                                    baseControl.Attributes.Add("onkeydown", string.Format(" setCBLValue('{0}','{1}');", baseControl.ClientID, targetControl.ClientID));
                                }
                                else if (baseControl is CheckBox)
                                {
                                    (baseControl as CheckBox).InputAttributes.Add("onclick", string.Format("setValue(this,'{0}');", targetControl.ClientID));
                                }
                                else
                                {
                                    //baseControl.Attributes.Add("onchange", string.Format("setValue({0},{1});", baseControl.ClientID, targetControl.ClientID));
                                    baseControl.Attributes.Add("onchange", string.Format("setValue(this,'{0}');{1} ", targetControl.ClientID, baseControl.Attributes["onchange"]));
                                }
                            }
                        }
                    }
                }
            }

            #endregion
            #region Initializations 4 copy controls
            List<string> CopyIds = new List<string>(); // copy all the ids used to a control
            List<SubmissionField> fields4Copy = ConfigValues.GetMappingFields(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
            foreach (SubmissionField field in fields4Copy)
            {
                WebControl baseControl = Common.FindControlRecursive(Page, field.ControlId + "4Copy") as WebControl;
                if (null != baseControl)
                {
                    foreach (FieldPair var in field.AssociatedControlIds)
                    {
                        if (var.Type.Contains("Id"))
                        {
                            Control targetControl = Common.FindControlRecursive(Page, var.Id);
                            if (null != targetControl)
                            {
                                if (baseControl is CheckBoxList)
                                {
                                    baseControl.Attributes.Add("onmouseout", string.Format(" setCBLValue('{0}','{1}');", baseControl.ClientID, targetControl.ClientID));
                                    baseControl.Attributes.Add("onkeydown", string.Format(" setCBLValue('{0}','{1}');", baseControl.ClientID, targetControl.ClientID));
                                }
                                else if (baseControl is CheckBox)
                                {
                                    (baseControl as CheckBox).InputAttributes.Add("onclick", string.Format("setValue(this,'{0}');", targetControl.ClientID));
                                }
                                else
                                {
                                    //baseControl.Attributes.Add("onchange", string.Format("setValue({0},{1});", baseControl.ClientID, targetControl.ClientID));
                                    baseControl.Attributes.Add("onchange", string.Format("setValue(this,'{0}');", targetControl.ClientID));
                                }
                            }
                            if (baseControl is DateBox)
                            {
                                CopyIds.Add(string.Format("{0}|{1}", targetControl.ClientID, (baseControl as DateBox).TextBox.ClientID));
                            }
                            else
                            {
                                CopyIds.Add(string.Format("{0}|{1}", targetControl.ClientID, baseControl.ClientID));
                            }
                        }
                    }
                }
            }
            txCopyIds.Text = string.Join(",", CopyIds.ToArray());

            #endregion

            if (Request.QueryString["Ofac"] != null)
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "OFACMatch", "alert('" + BTS.WFA.BCS.Services.LegacyService.OFACMessage + "');", true);

            LoadOtherSubmissions2(true);
        }
        else
        {
            LoadOtherSubmissions2(false);
        }

        AddValidationToControls();

        if (Request.Params["__EVENTTARGET"] != null && Request.Params["__EVENTTARGET"].EndsWith("4Copy"))
        {
            string controlId = Request.Params["__EVENTTARGET"];
            Control control = Common.FindControlRecursive(Page, controlId.Substring(controlId.LastIndexOf('$') + 1));
            qSomethingChangedOnCopyPanel(control);
        }

    }
    protected override void OnLoadComplete(EventArgs e)
    {
        if (Session["CompanyChanged"] != null)
        {
            Session.Contents.Remove("CompanyChanged");
            Session.Contents.Remove("LoadSavedSearch");
            Session.Contents.Remove("Clients");
            Response.Redirect("Default.aspx");
        }

        AddressListSelectAll.Checked = Common.GetSelectedItems(gvClientAddresses, "addressCheckBox").Count == gvClientAddresses.Rows.Count;
        AddressListSelectAll.Text = Common.GetSelectedItems(gvClientAddresses, "addressCheckBox").Count == gvClientAddresses.Rows.Count ? "de-select all addresses" : "select all addresses";
        //NameListSelectAll.Checked = Common.GetSelectedItems(NameList).Length == NameList.Items.Count;
        NameListSelectAll.Checked = Common.GetSelectedItems(gvClientNames, "nameCheckBox").Count == gvClientNames.Rows.Count;
        //NameListSelectAll.Text = Common.GetSelectedItems(NameList).Length == NameList.Items.Count ? "de-select all names" : "select all names";
        NameListSelectAll.Text = Common.GetSelectedItems(gvClientNames, "nameCheckBox").Count == gvClientNames.Rows.Count ? "de-select all names" : "select all names";

        SubmissionTypeDropDownList1.AutoPostBack = Common.IsCWG();
        var policyNumRequired = PolicyNumberIsRequired();
        spanPolicyNumRequired.Visible = policyNumRequired;
        rfvPolicyNumber.Enabled = policyNumRequired;

        base.OnLoadComplete(e);
    }
    #endregion

    #region Control Events

    #region validation

    protected void ApplyValidation(object sender, EventArgs e)
    {
        ClearValidationRules((Control)sender);
        ProcessValidation((Control)sender);
    }

    private void AddValidationToControls()
    {
        BCS.Biz.ValidationConditionCollection conditions = ValidationEngine.GetValidationConditions();
        if (conditions == null) return;

        foreach (BCS.Biz.ValidationCondition condition in conditions.FilterByDisplayOrder(1))
        {
            Control control = Common.FindControlRecursive(Wizard1, condition.ValidationField.ControlID);
            if (control == null) continue;

            if (control is DropDownList)
            {
                DropDownList ddl = (DropDownList)control;
                ddl.SelectedIndexChanged -= ApplyValidation;
                ddl.SelectedIndexChanged += ApplyValidation;
                ddl.AutoPostBack = true;
            }
            else if (control is TextBox)
            {
                TextBox txt = (TextBox)control;
                txt.TextChanged -= ApplyValidation;
                txt.TextChanged += ApplyValidation;
                txt.AutoPostBack = true;
            }
        }
    }

    //Move this
    private void ProcessValidation(Control control)
    {
        if (!ValidationEngine.HasValidationConditions(control.ID)) return;

        BCS.Biz.ValidationConditionCollection conditions = ValidationEngine.GetValidationConditions(control.ID);

        foreach (BCS.Biz.ValidationCondition condition in conditions)
        {
            BCS.Biz.ValidationRuleCollection rules = EvaluateValidationConditions(condition);
            if (rules != null)
            {
                ApplyValidationRules(rules, control.ID);
                //Session.Add(string.Format("{0}Rules", control.ID), rules);
            }
        }
    }

    private void ClearValidationRules(Control control)
    {
        if (control == null || Session[string.Format("{0}Rules", control.ID)] == null) return;

        List<string> appliedRules = (List<string>)Session[string.Format("{0}Rules", control.ID)];
        if (appliedRules != null && appliedRules.Count > 0)
        {
            foreach (string changes in appliedRules)
            {
                string[] items = changes.Split('|');

                Control ruleControl = null;
                if (items.Length > 0)
                    ruleControl = Common.FindControlRecursive(Wizard1, items[0]);

                for (int i = 1; i < items.Length; i++)
                {
                    string[] attribute = items[i].Split('=');
                    if (attribute.Length != 2) continue;

                    if (ruleControl is DropDownList)
                    {
                        DropDownList ddl = (DropDownList)ruleControl;
                        switch (attribute[0])
                        {
                            case "Visible":
                                ddl.Visible = bool.Parse(attribute[1]);
                                break;
                            case "Enabled":
                                ddl.Enabled = bool.Parse(attribute[i]);
                                break;
                            case "SelectedIndex":
                                ddl.SelectedIndex = int.Parse(attribute[1]);
                                break;
                        }
                    }
                    else if (ruleControl is TextBox)
                    {
                        TextBox txt = (TextBox)ruleControl;
                        switch (attribute[0])
                        {
                            case "Visible":
                                txt.Visible = bool.Parse(attribute[1]);
                                break;
                            case "Enabled":
                                txt.Enabled = bool.Parse(attribute[i]);
                                break;
                        }
                    }

                }
                ClearValidationRules(ruleControl);
            }
            Session.Remove(string.Format("{0}Rules", control.ID));
        }
    }

    private BCS.Biz.ValidationRuleCollection EvaluateValidationConditions(BCS.Biz.ValidationCondition condition)
    {
        BCS.Biz.ValidationConditionCollection conditions = ValidationEngine.GetValidationConditions((int)condition.ValidationID);

        foreach (BCS.Biz.ValidationCondition item in conditions)
        {
            Control ctrl = Common.FindControlRecursive(Wizard1, item.ValidationField.ControlID);
            if (ctrl == null) return null;

            if (!ValidationEngine.EvaluateValdationCondition(item, GetControlValue(ctrl))) return null;
        }

        //made it this far - get the rules
        return ValidationEngine.GetValidationRules((int)condition.ValidationID);
    }

    private void ApplyValidationRules(BCS.Biz.ValidationRuleCollection rules, string controlName)
    {
        if (rules == null || rules.Count == 0) return;

        List<string> appliedChanges = (List<string>)Session[string.Format("{0}Rules", controlName)];
        if (appliedChanges == null) appliedChanges = new List<string>();

        foreach (BCS.Biz.ValidationRule rule in rules)
        {
            Control control = Common.FindControlRecursive(Wizard1, rule.ValidationField.ControlID);
            StringBuilder changes = new StringBuilder();

            WebControl webControl = null;
            if (control is WebControl) webControl = (WebControl)control;

            if (webControl.Enabled != (bool)rule.EnableControl)
            {
                webControl.Enabled = (bool)rule.EnableControl;
                changes.Append(string.Format("Enabled={0}|", !webControl.Enabled));
            }
            if (webControl.Visible != (bool)rule.DisplayControl)
            {
                webControl.Visible = (bool)rule.DisplayControl;
                changes.Append(string.Format("Visible={0}|", !webControl.Visible));
            }

            if (control is DropDownList)
            {
                DropDownList ddl = (DropDownList)control;
                if (rule.SetNewValue)
                {
                    if (ddl.Items.FindByText(rule.NewValue) != null)
                    {
                        ddl.SelectedValue = ddl.Items.FindByText(rule.NewValue).Value;
                        ddl.AutoPostBack = false;
                        changes.Append("SelectedIndex=-1|");
                        ApplyValidation(ddl, new EventArgs());
                    }
                }
            }
            else if (control is TextBox)
            {
                TextBox txt = (TextBox)control;
                if (rule.SetNewValue) txt.Text = rule.NewValue;
            }

            if (!string.IsNullOrEmpty(changes.ToString()))
                appliedChanges.Add(string.Format("{0}|{1}", rule.ValidationField.ControlID, changes.ToString().TrimEnd('|')));
        }

        Session[string.Format("{0}Rules", controlName)] = appliedChanges;
        return;
    }

    private string GetControlValue(Control control)
    {
        if (control == null) return null;

        if (control is DropDownList)
        {
            DropDownList ddl = (DropDownList)control;
            return ddl.SelectedItem.Text;
        }
        else if (control is TextBox)
        {
            TextBox txt = (TextBox)control;
            return txt.Text;
        }

        return string.Empty;
    }

    #endregion


    protected void ToggleAgencyDisplay(object sender, ImageClickEventArgs e)
    {
        ShowGrid = !ShowGrid;
        #region grid
        if (ShowGrid)
        {
            rowAgencyGridFilter.Style[HtmlTextWriterStyle.Display] = "";
            rowAgencyGrid.Style[HtmlTextWriterStyle.Display] = "";
        }
        else
        {
            rowAgencyGridFilter.Style[HtmlTextWriterStyle.Display] = "none";
            rowAgencyGrid.Style[HtmlTextWriterStyle.Display] = "none";
        }
        gridAgencies.Visible = ShowGrid;
        gridAgencies.Enabled = ShowGrid;
        #endregion
        #region dropdown
        if (!ShowGrid)
        {
            rowAgencyDropDown.Style[HtmlTextWriterStyle.Display] = "";
            ScriptManager1.SetFocus(AgencyDropDownList1);
        }
        else
        {
            rowAgencyDropDown.Style[HtmlTextWriterStyle.Display] = "none";
        }
        #endregion

        PopulateControls(Wizard1.ActiveStepIndex);

    }
    protected void SetAgencyGridDisplay(object sender, EventArgs e)
    {
        if (!ShowGrid)
        {
            (sender as HtmlTableRow).Style.Add(HtmlTextWriterStyle.Display, "none");
        }
    }
    protected void SetAgencyDropDownDisplay(object sender, EventArgs e)
    {
        if (ShowGrid)
        {
            (sender as HtmlTableRow).Style.Add(HtmlTextWriterStyle.Display, "none");
        }
    }
    #region Wizard
    protected void Wizard1_ActiveStepChanged(object sender, EventArgs e)
    {
    }
    protected void Wizard1_PreviousButtonClick(object sender, WizardNavigationEventArgs e)
    {
        Wizard wizard = sender as Wizard;

        WizardConfigSteps dictionary = ConfigValues.GetWizardSteps(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

        int prevStep = e.CurrentStepIndex;

        List<WizardConfigStep> reversedSteps = new List<WizardConfigStep>(dictionary.Steps);
        reversedSteps.Reverse();

        bool isAdd = Session["SubmissionId"] == null;
        if (isAdd)
        {
            if ((Common.IsCWG() && IsProspect() && prevStep == 8))
                prevStep = 7;
            else
                foreach (WizardConfigStep var in reversedSteps)
                {
                    if (var.Index >= prevStep)
                        continue;
                    if (!var.AddVisible)
                        continue;
                    else
                    {
                        prevStep = var.Index;
                        break;
                    }
                }
        }
        else
        {
            foreach (WizardConfigStep var in reversedSteps)
            {
                if (var.Index >= prevStep)
                    continue;
                if (!var.EditVisible)
                    continue;
                else
                {
                    prevStep = var.Index;
                    break;
                }
            }
        }
        PopulateData(e.CurrentStepIndex);
        PopulateControls(prevStep);

        ValidateAndStoreStep(e.CurrentStepIndex);
        Wizard1.ActiveStepIndex = prevStep;
    }
    protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
    {
        Wizard wizard = sender as Wizard;

        WizardConfigSteps dictionary = ConfigValues.GetWizardSteps(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

        int nextStep = e.NextStepIndex;
        bool isAdd = Session["SubmissionId"] == null;
        if (isAdd)
        {
            if (!(Common.IsCWG() && IsProspect() && nextStep == 7))
                foreach (WizardConfigStep var in dictionary.Steps)
                {
                    if (var.Index < nextStep)
                        continue;
                    if (!var.AddVisible)
                        continue;
                    else
                    {
                        nextStep = var.Index;
                        break;
                    }
                }
        }
        else
        {
            foreach (WizardConfigStep var in dictionary.Steps)
            {
                if (var.Index < nextStep)
                    continue;
                if (!var.EditVisible)
                    continue;
                else
                {
                    nextStep = var.Index;
                    break;
                }
            }
        }


        PopulateData(e.CurrentStepIndex);
        PopulateControls(nextStep);

        ValidateAndStoreStep(e.CurrentStepIndex);
        Wizard1.ActiveStepIndex = nextStep;
    }
    protected void Wizard1_SideBarButtonClick(object sender, WizardNavigationEventArgs e)
    {
        Wizard wizard = sender as Wizard;
        PopulateData(e.CurrentStepIndex);
        PopulateControls(e.NextStepIndex);
        ValidateAndStoreStep(e.CurrentStepIndex);
    }
    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        if (Wizard1.Visible)
        {
            // populate data, since we are getting values from the snapshot panel
            PopulateData(Wizard1.ActiveStepIndex);

            IsValidStep(Wizard1.ActiveStepIndex);
        }

        if (!Page.IsValid)
        {
            Wizard1.FinishDestinationPageUrl = "";
            return;
        }
        Wizard1.FinishDestinationPageUrl = Request.RawUrl;
        SaveSubmission();

        if (IsOfacHit)
            Wizard1.FinishDestinationPageUrl += "?Ofac=1";
        else
            Wizard1.FinishDestinationPageUrl = Wizard1.FinishDestinationPageUrl.Replace("?Ofac=1", "");

        Wizard1.Visible = false;
        divMenu.Visible = true;

        divAddHeader.Visible = false;
        divEditHeader.Visible = true;
        SavedState.Text = "1";
    }
    protected void Wizard1_CancelButtonClick(object sender, EventArgs e)
    {
        if (Session["SubmissionId"] == null)
        {
            lnkBackToSearch_OnClick(sender, e);
        }
    }
    protected void FinishFromSnapShot_Click(object sender, EventArgs e)
    {
        //#region reset or restore valid reason
        //BCS.Biz.SubmissionStatusSubmissionStatusReasonCollection companyReasons =
        //    SubmissionStatusReasonDropDownList.GetCompanyStatusReasons(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
        //companyReasons = companyReasons.FilterBySubmissionStatusId(Common.GetSafeSelectedValue(SubmissionStatusDropDownList2));
        //companyReasons = companyReasons.FilterByActive(true);

        //BCS.Biz.SubmissionStatusReasonCollection reasons = new BCS.Biz.SubmissionStatusReasonCollection();
        //foreach (BCS.Biz.SubmissionStatusSubmissionStatusReason var in companyReasons)
        //{
        //    reasons.Add(var.SubmissionStatusReason);
        //}

        //BCS.Biz.SubmissionStatusReason aReason = reasons.FindById(spSubmissionStatusReasonIdLabel.Text);
        //if (null == aReason)
        //{
        //    spSubmissionStatusReasonIdLabel.Text = "0";
        //}
        //#endregion
        //Finish_Click(sender, e);
    }
    protected void Finish_Click(object sender, EventArgs e)
    {
        Wizard1_FinishButtonClick(sender, new WizardNavigationEventArgs(Wizard1.ActiveStepIndex, Wizard1.ActiveStepIndex));
        if (!string.IsNullOrEmpty(Wizard1.FinishDestinationPageUrl))
            Response.Redirect(Wizard1.FinishDestinationPageUrl);
    }
    protected void SideBarList_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        #region set validity
        if (ValidSteps.ContainsKey(e.Item.ItemIndex))
        {
            e.Item.FindControl("ImageStatusSuccess").Visible = ValidSteps[e.Item.ItemIndex];
            e.Item.FindControl("ImageStatusFailure").Visible = !ValidSteps[e.Item.ItemIndex];
        }
        // even though set in the designer, it sometimes is causing validation
        LinkButton linkButton = e.Item.FindControl("SideBarButton") as LinkButton;
        linkButton.CausesValidation = false;

        WizardConfigStep step = ConfigValues.GetWizardSteps(Common.GetSafeIntFromSession(SessionKeys.CompanyId)).Steps[e.Item.ItemIndex];
        bool isAdd = Session["SubmissionId"] == null;
        if (Common.IsCWG() && IsProspect() && e.Item.ItemIndex == 7)// "Other carrier" should be visible for CWG prospect submissions
            linkButton.Visible = true;
        else
            linkButton.Visible = isAdd ? step.AddVisible : step.EditVisible;

        #endregion
    }

    #endregion

    #region Copy
    protected void lnkCopySubmission_OnClick(object sender, EventArgs e)
    {
        SavedState.Text = "0";
        divEditHeader.Visible = false;
        divAddHeader.Visible = true;
        divMenu.Visible = false;
        Wizard1.Visible = true;

        Session.Add("CopiedSubmissionId", Session["SubmissionId"]);
        Session.Contents.Remove("SubmissionId");
        hdnSubmission.Value = "";
        LOBListLoaded.Text = Boolean.FalseString;
        EffectiveLOBCount.Text = "0";

        #region populate the first step things
        int firstStep = GetCopyDefaultStepIndex();
        PopulateControls(firstStep);
        Wizard1.ActiveStepIndex = firstStep;
        #endregion

        #region Exclusions
        List<string> listCopyExcludedFields = ConfigValues.GetCopyExcludedFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (string varP in listCopyExcludedFields)
        {
            Control inputControlP = Common.FindControlRecursive(Wizard1, varP);
            System.ComponentModel.AttributeCollection attributesP = System.ComponentModel.TypeDescriptor.GetAttributes(inputControlP);
            ControlValuePropertyAttribute attributeP = (ControlValuePropertyAttribute)attributesP[typeof(ControlValuePropertyAttribute)];

            Type typeP = inputControlP.GetType();
            PropertyInfo pinfoP = typeP.GetProperty(attributeP.Name);

            object defaultValue = attributeP.DefaultValue;
            object defaultText = attributeP.DefaultValue;

            if (inputControlP is ListControl)
            {
                MethodInfo mi = typeP.GetMethod("DataBind");
                mi.Invoke(inputControlP, null);

                // if list control then get the display text to set, for any defaults within the control
                defaultValue = pinfoP.GetValue(inputControlP, null);

                PropertyInfo ListItemP = typeP.GetProperty("SelectedItem");
                if (ListItemP != null)
                {
                    object propValue = ListItemP.GetValue(inputControlP, null);
                    if (null != propValue)
                        defaultText = propValue.GetType().GetProperty("Text").GetValue(propValue, null);
                }
            }

            pinfoP.SetValue(inputControlP, defaultValue, null);

            // set appropriately in the snapshot panels
            Dictionary<string, string> mappingFields = ConfigValues.GetMappingFields(inputControlP.ID);
            foreach (KeyValuePair<string, string> var in mappingFields)
            {
                Control assocControl = Common.FindControlRecursive(Page, var.Key);
                if (assocControl != null)
                {
                    System.ComponentModel.AttributeCollection attributes = System.ComponentModel.TypeDescriptor.GetAttributes(assocControl);
                    ControlValuePropertyAttribute attribute = (ControlValuePropertyAttribute)attributes[typeof(ControlValuePropertyAttribute)];

                    Type type = assocControl.GetType();
                    PropertyInfo pinfo = type.GetProperty(attribute.Name);
                    if (type.Name == "CheckBox")
                    {
                        pinfo.SetValue(assocControl, false, null);
                    }
                    else
                    {
                        if (var.Value == "DisplayText")
                        {
                            MethodInfo methodToExecute = this.GetType().BaseType.GetMethod("SetSafe" + var.Value, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                            methodToExecute.Invoke(this, new object[] { assocControl as ITextControl, defaultText });
                        }
                        if (var.Value.Contains("Id"))
                        {
                            MethodInfo methodToExecute = this.GetType().BaseType.GetMethod("SetSafe" + var.Value, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                            methodToExecute.Invoke(this, new object[] { assocControl as ITextControl, defaultValue });
                        }
                    }
                }
            }
        }
        #endregion

        #region Toggle Enable/Visible from Edit to Add

        #region reset Edit invisible/disabled fields

        #region Set Enabled, what was disabled on edit based on company config
        string[] editDisabledFields = ConfigValues.GetEditDisabledFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (string var in editDisabledFields)
        {
            // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
            // as parameter to FindControlRecursive method
            Control c = Common.FindControlRecursive(Wizard1, var.Trim());
            if (c != null)
            {
                if (c is WebControl)
                {
                    WebControl wc = c as WebControl;
                    wc.Enabled = false;
                }
                if (c is UserControl)
                {
                    PropertyInfo pi = c.GetType().GetProperty("Enabled");
                    if (pi != null)
                        pi.SetValue(c, true, null);
                }
            }
        }
        #endregion
        #region Set Visible what was invisible based on company config
        string[] editInvisibleFields = ConfigValues.GetEditInvisibleFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (string var in editInvisibleFields)
        {
            // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
            // as parameter to FindControlRecursive method
            Control c = Common.FindControlRecursive(Wizard1, var.Trim());
            if (c != null)
            {
                if (c is WebControl)
                {
                    c.Visible = false;
                }
                if (c is UserControl)
                {
                    PropertyInfo pi = c.GetType().GetProperty("Visible");
                    if (pi != null)
                        pi.SetValue(c, true, null);
                }
            }
        }
        #endregion
        #endregion

        #region set invisible/disabled fields

        #region Set Disabled based on company config
        string[] addDisabledFields = ConfigValues.GetAddDisabledFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (string var in addDisabledFields)
        {
            // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
            // as parameter to FindControlRecursive method
            Control c = Common.FindControlRecursive(Wizard1, var.Trim());
            if (c != null)
            {
                if (c is WebControl)
                {
                    WebControl wc = c as WebControl;
                    wc.Enabled = false;
                }
                if (c is UserControl)
                {
                    PropertyInfo pi = c.GetType().GetProperty("Enabled");
                    if (pi != null)
                        pi.SetValue(c, false, null);
                }
            }
        }
        #endregion
        #region Set Invisible based on company config
        string[] addInvisibleFields = ConfigValues.GetAddInvisibleFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (string var in addInvisibleFields)
        {
            // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
            // as parameter to FindControlRecursive method
            Control c = Common.FindControlRecursive(Wizard1, var.Trim());
            if (c != null)
            {
                if (c is WebControl)
                {
                    c.Visible = false;
                }
                if (c is UserControl)
                {
                    PropertyInfo pi = c.GetType().GetProperty("Visible");
                    if (pi != null)
                        pi.SetValue(c, false, null);
                }
            }
        }
        #endregion
        #endregion
        #endregion

        WizardConfigSteps allSteps = ConfigValues.GetWizardSteps(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (WizardConfigStep var in allSteps.Steps)
        {
            if (var.AddVisible)
                ValidateAndStoreStep(var.Index);
        }
    }
    protected void qSomethingChangedOnCopyPanel(Control sender)
    {
        if (sender.ID == "SubmissionStatusDropDownList14Copy")
        {
            SubmissionStatusReasonDropDownList reasonDDL = Common.FindControlRecursive(Page, "SubmissionStatusReasonDropDownList14Copy") as SubmissionStatusReasonDropDownList;
            if (reasonDDL != null)
            {
                reasonDDL.StatusId = Common.GetSafeSelectedValue(sender as ListControl);
                reasonDDL.DataBind();
            }
            PopulateControls(5);
            SubmissionStatusDropDownList1_SelectedIndexChanged(SubmissionStatusDropDownList1, EventArgs.Empty);
        }
        if (sender.ID == "txEffectiveDate4Copy")
        {
            txEffectiveDate.Text = (sender as DateBox).Text;
            txEffectiveDate_Changed(txEffectiveDate, EventArgs.Empty);
        }
    }
    protected void CopyDone(object sender, EventArgs e)
    {
        if (chkCopyStatusHistory.Visible && chkCopyStatusHistory.Checked)
        {
            Session.Add("CopiedSubmissionId", Session["SubmissionId"]);
        }
        popupCopy.Cancel();

        SavedState.Text = "0";
        divEditHeader.Visible = false;
        divAddHeader.Visible = true;
        divMenu.Visible = false;
        Wizard1.Visible = true;

        Session.Contents.Remove("SubmissionId");

        LOBListLoaded.Text = Boolean.FalseString;
        EffectiveLOBCount.Text = "0";
        LOBList4Display.DataBind();

        #region populate the first step things
        int firstStep = GetCopyDefaultStepIndex();
        PopulateControls(firstStep);
        Wizard1.ActiveStepIndex = firstStep;
        #endregion

        #region Toggle Enable/Visible from Edit to Add

        #region reset Edit invisible/disabled fields

        #region Set Enabled, what was disabled on edit based on company config
        string[] editDisabledFields = ConfigValues.GetEditDisabledFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (string var in editDisabledFields)
        {
            // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
            // as parameter to FindControlRecursive method
            Control c = Common.FindControlRecursive(Wizard1, var.Trim());
            if (c != null)
            {
                if (c is WebControl)
                {
                    WebControl wc = c as WebControl;
                    wc.Enabled = false;
                }
                if (c is UserControl)
                {
                    PropertyInfo pi = c.GetType().GetProperty("Enabled");
                    if (pi != null)
                        pi.SetValue(c, true, null);
                }
            }
        }
        #endregion
        #region Set Visible what was invisible based on company config
        string[] editInvisibleFields = ConfigValues.GetEditInvisibleFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (string var in editInvisibleFields)
        {
            // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
            // as parameter to FindControlRecursive method
            Control c = Common.FindControlRecursive(Wizard1, var.Trim());
            if (c != null)
            {
                if (c is WebControl)
                {
                    c.Visible = false;
                }
                if (c is UserControl)
                {
                    PropertyInfo pi = c.GetType().GetProperty("Visible");
                    if (pi != null)
                        pi.SetValue(c, true, null);
                }
            }
        }
        #endregion
        #endregion

        #region set invisible/disabled fields

        #region Set Disabled based on company config
        string[] addDisabledFields = ConfigValues.GetAddDisabledFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (string var in addDisabledFields)
        {
            // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
            // as parameter to FindControlRecursive method
            Control c = Common.FindControlRecursive(Wizard1, var.Trim());
            if (c != null)
            {
                if (c is WebControl)
                {
                    WebControl wc = c as WebControl;
                    wc.Enabled = false;
                }
                if (c is UserControl)
                {
                    PropertyInfo pi = c.GetType().GetProperty("Enabled");
                    if (pi != null)
                        pi.SetValue(c, false, null);
                }
            }
        }
        #endregion
        #region Set Invisible based on company config
        string[] addInvisibleFields = ConfigValues.GetAddInvisibleFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (string var in addInvisibleFields)
        {
            Control c = Common.FindControlRecursive(Page, var.Trim());
            if (c != null)
            {
                if (c is WebControl)
                {
                    c.Visible = false;
                }
                if (c is UserControl)
                {
                    PropertyInfo pi = c.GetType().GetProperty("Visible");
                    if (pi != null)
                        pi.SetValue(c, false, null);
                }
            }
        }
        #endregion
        #endregion
        #endregion

        WizardConfigSteps allSteps = ConfigValues.GetWizardSteps(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (WizardConfigStep var in allSteps.Steps)
        {
            if (var.AddVisible)
                ValidateAndStoreStep(var.Index);
        }
    }
    protected void CopyLoaded(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            List<SubmissionCopyExclusion> exclusions = ConfigValues.GetCopyExcludedFieldsDetailed(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

            foreach (SubmissionCopyExclusion var in exclusions)
            {
                Control foundExcludedField = Common.FindControlRecursive(Page, var.ControlId + "4Copy");
                if (foundExcludedField != null)
                {
                    if (!string.IsNullOrEmpty(var.Method))
                    {
                        Type t = foundExcludedField.GetType();
                        MethodInfo mi = t.GetMethod(var.Method);
                        if (null != mi)
                        {
                            mi.Invoke(foundExcludedField, var.MethodParams);
                        }
                        else // try on page
                        {
                            mi = this.GetType().BaseType.GetMethod(var.Method, BindingFlags.NonPublic | BindingFlags.Instance);
                            ParameterInfo[] parameterInfoArray = mi.GetParameters();

                            object[] paramsToPass = new object[var.MethodParams.Length];
                            for (int i = 0; i < parameterInfoArray.Length; i++)
                            {
                                paramsToPass[i] = Convert.ChangeType(var.MethodParams[i], parameterInfoArray[i].ParameterType);
                            }
                            mi.Invoke(this, paramsToPass);
                        }
                    }
                }
            }
        }
    }
    #endregion
    protected void lnkEditSubmission_OnClick(object sender, EventArgs e)
    {
        SavedState.Text = "0";
        Wizard1.Visible = true;
        divMenu.Visible = false;

        int firstIndex = GetEditDefaultStepIndex();
        Wizard1.ActiveStepIndex = firstIndex;

        PopulateControls(firstIndex);

        WizardConfigSteps allSteps = ConfigValues.GetWizardSteps(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (WizardConfigStep var in allSteps.Steps)
        {
            if (var.EditVisible)
                ValidateAndStoreStep(var.Index);
        }
    }

    protected void lnkEditClient_OnClick(object sender, EventArgs e)
    {
        Session["cliref"] = string.Format("SubmissionWizard.aspx");
        //SavePageState(false);
        Response.Redirect("Client.aspx?op=edit");
    }

    protected void lnkBackToSearch_OnClick(object sender, EventArgs e)
    {
        Session["LoadSavedSearch"] = true;
        if (Session["subref"] != null)
        {
            string ss = Session["subref"].ToString();
            Session.Contents.Remove("subref");
            Response.Redirect(ss);
        }
        Response.Redirect("Default.aspx");
    }

    protected void btnClearFilter_OnClick(object sender, EventArgs e)
    {
        txFilterAgencyName.Text = string.Empty;
        txFilterAgencyNumber.Text = string.Empty;
        txFilterCity.Text = string.Empty;
        txFilterState.Text = string.Empty;
    }
    protected void btnFilter_OnClick(object sender, EventArgs e)
    {
        #region dummy grid to find selected by datakey
        string selectedAgencyId = spAgencyIdLabel.Text;
        if (selectedAgencyId != "0")
        {
            gv.DataSourceID = odsAgencies.ID;
            gv.DataBind();
            DataKeyArray dkArray = gv.DataKeys;

            int rawRowIndex = -1;
            for (int i = 0; i < dkArray.Count; i++)
            {
                if (dkArray[i].Value.ToString() == selectedAgencyId)
                {
                    rawRowIndex = i;
                    break;
                }
            }
            int pageIndex = (int)(rawRowIndex / gridAgencies.PageSize);
            int rowIndex = rawRowIndex - (gridAgencies.PageSize * pageIndex);

            gridAgencies.PageIndex = pageIndex;
            gridAgencies.SelectedIndex = rowIndex;

            gv.SelectedIndex = rawRowIndex;
        }
        #endregion
    }

    protected void btnLoadSwitched_Click(object sender, EventArgs e)
    {
        txInsuredName.Text = string.Empty;
        txDBA.Text = string.Empty;
        GetAndLoadClient(SwitchClientTextBox.Text);

        // default select if only one
        DefaultSelect(gvClientAddresses, "addressCheckBox");
        //DefaultSelect(NameList);
        DefaultSelect(gvClientNames, "nameCheckBox");
        // client is the last step
        PopulateData(Wizard1.WizardSteps.Count - 1);
        Wizard1.ActiveStepIndex = Wizard1.WizardSteps.Count - 1;
        Wizard1.Visible = true;
        divMenu.Visible = false;
        SavedState.Text = "0";
    }

    protected void chkCopyStatusHistory_CheckChanged(object sender, EventArgs e)
    {
        if (chkCopyStatusHistory.Checked)
            Session["copystatushistories"] = true;
        else
            Session.Remove("copystatushistories");
    }

    protected void txEffectiveDate_Changed(object sender, EventArgs e)
    {
        DateTime eff = DateTime.MinValue;
        try
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(System.Globalization.CultureInfo.CurrentCulture.Name);
            ci.Calendar.TwoDigitYearMax = 2079;
            eff = Convert.ToDateTime(txEffectiveDate.Text, ci);
            txExpirationDate.Text = eff.AddYears(1).ToString(Common.DateFormat);


            spEffectiveDateLabel.Text = txEffectiveDate.Text.Length == 0 ? hfDisplayText.Value : txEffectiveDate.Text;
            spEffectiveDateIdLabel.Text = txEffectiveDate.Text;
            spExpirationDateLabel.Text = txExpirationDate.Text.Length == 0 ? hfDisplayText.Value : txExpirationDate.Text;
            spExpirationDateIdLabel.Text = txExpirationDate.Text;
            // TODO: underwriter analyst technician and code types, i.e. date dependent things
            // TODO: tab orders
        }
        catch (FormatException)
        {
            txEffectiveDate.Text = string.Empty;
            txExpirationDate.Text = string.Empty;
            spEffectiveDateLabel.Text = hfDisplayText.Value;
            spEffectiveDateIdLabel.Text = string.Empty;
            spExpirationDateLabel.Text = hfDisplayText.Value;
            spExpirationDateIdLabel.Text = string.Empty;
        }
        DateTime effDate = GetEffectiveDate(DateTime.MaxValue.Subtract(TimeSpan.FromSeconds(1)));
        odsAgencies.SelectParameters["date"].DefaultValue = effDate.ToString();
        AgencyDropDownList1.CancelDate = effDate;

        AgentLoaded.Text = Boolean.FalseString;
        AgentDropDownList1.CancelDate = effDate;

        ContactLoaded.Text = Boolean.FalseString;
        ContactDropDownList1.RetirementDate = effDate;

        underwriterLoaded.Text = Boolean.FalseString;
        UnderwriterDropDownList1.RetirementDate = effDate;
        analystLoaded.Text = Boolean.FalseString;
        AnalystDropDownList1.RetirementDate = effDate;
        technicianLoaded.Text = Boolean.FalseString;
        TechnicianDropDownList1.RetirementDate = effDate;

        CodeTypesLoaded.Text = Boolean.FalseString;

        bool usesAPSServiceForAssignments =
            DefaultValues.SupportsAPSServiceforAssignments(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

        #region Validations
        #region Underwriter
        if (spUnderwriterIdLabel.Text != "0")
        {
            int underwriterId = int.MinValue;
            if (usesAPSServiceForAssignments)
            {
                long apsId = long.MinValue;
                long.TryParse(spUnderwriterIdLabel.Text, out apsId);
                BCS.Biz.Person uwPerson = BCS.Core.Clearance.Admin.DataAccess.GetPersonByAPSId(apsId
                            , Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                if (uwPerson != null)
                    underwriterId = uwPerson.Id;
            }
            else
                underwriterId = Convert.ToInt32(spUnderwriterIdLabel.Text);

            BCS.Biz.Person underwriter = BCS.Core.Clearance.Admin.DataAccess.GetPerson(underwriterId);
            if (underwriter.RetirementDate < effDate)
            {
                Common.SetStatus(messagePlaceHolder, string.Format("Underwriter {0} is retired as of effective date. Hence was cleared.", spUnderwriterLabel.Text));
                SetSafeDisplayText(spUnderwriterLabel, string.Empty);
                SetSafeIdNumber(spUnderwriterIdLabel, string.Empty);
            }
        }
        #endregion
        #region Analyst
        if (spAnalystIdLabel.Text != "0")
        {
            int analystId = int.MinValue;
            if (usesAPSServiceForAssignments)
            {
                long apsId = long.MinValue;
                long.TryParse(spAnalystIdLabel.Text, out apsId);
                BCS.Biz.Person anPerson = BCS.Core.Clearance.Admin.DataAccess.GetPersonByAPSId(apsId
                            , Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                if (anPerson != null)
                    analystId = anPerson.Id;
            }
            else
                analystId = Convert.ToInt32(spAnalystIdLabel.Text);

            BCS.Biz.Person analyst = BCS.Core.Clearance.Admin.DataAccess.GetPerson(analystId);
            if (analyst.RetirementDate < effDate)
            {
                Common.SetStatus(messagePlaceHolder, string.Format("Analyst {0} is retired as of effective date. Hence was cleared.", spAnalystLabel.Text));
                SetSafeDisplayText(spAnalystLabel, string.Empty);
                SetSafeIdNumber(spAnalystIdLabel, string.Empty);
            }
        }
        #endregion
        #region Technician
        if (spTechnicianIdLabel.Text != "0")
        {
            int TechnicianId = int.MinValue;
            if (usesAPSServiceForAssignments)
            {
                long apsId = long.MinValue;
                long.TryParse(spTechnicianIdLabel.Text, out apsId);
                BCS.Biz.Person techPerson = BCS.Core.Clearance.Admin.DataAccess.GetPersonByAPSId(apsId
                            , Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                if (techPerson != null)
                    TechnicianId = techPerson.Id;
            }
            else
                TechnicianId = Convert.ToInt32(spTechnicianIdLabel.Text);

            BCS.Biz.Person Technician = BCS.Core.Clearance.Admin.DataAccess.GetPerson(TechnicianId);
            if (Technician.RetirementDate < effDate)
            {
                Common.SetStatus(messagePlaceHolder, string.Format("Technician {0} is retired as of effective date. Hence was cleared.", spTechnicianLabel.Text));
                SetSafeDisplayText(spTechnicianLabel, string.Empty);
                SetSafeIdNumber(spTechnicianIdLabel, string.Empty);
            }
        }
        #endregion

        #region Codes
        List<DropDownList> ddls = FindDropDownListControls(CodeTypesContainerPanel);
        List<string> ddlIds = new List<string>();
        List<string> idIds = new List<string>();
        List<string> valueIds = new List<string>();
        foreach (DropDownList ddl in ddls)
        {
            ddlIds.Add(ddl.ID);
            ITextControl idControl = Common.FindControlRecursive(spCodeTypesContainerPanel, ddl.ID.Replace("ddl", "Id")) as ITextControl;
            ITextControl valueControl = Common.FindControlRecursive(spCodeTypesContainerPanel, ddl.ID.Replace("ddl", "Value")) as ITextControl;
            idIds.Add(idControl.Text);
            valueIds.Add(valueControl.Text);
        }
        BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
        dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCode.Columns.Id, idIds, OrmLib.MatchType.In);
        BCS.Biz.CompanyNumberCodeCollection codes = dm.GetCompanyNumberCodeCollection(BCS.Biz.FetchPath.CompanyNumberCode.CompanyNumberCodeType);
        List<string> invalidIds = new List<string>();
        foreach (BCS.Biz.CompanyNumberCode code in codes)
        {
            if (code.ExpirationDate < effDate)
            {
                invalidIds.Add(code.CompanyNumberCodeType.CodeName.Replace(" ", ""));
            }
        }
        foreach (string invalidId in invalidIds)
        {
            ITextControl idControl = Common.FindControlRecursive(spCodeTypesContainerPanel, ddlIds[invalidIds.IndexOf(invalidId)].Replace("ddl", "Id")) as ITextControl;
            ITextControl valueControl = Common.FindControlRecursive(spCodeTypesContainerPanel, ddlIds[invalidIds.IndexOf(invalidId)].Replace("ddl", "Value")) as ITextControl;

            Common.SetStatus(messagePlaceHolder, string.Format("Code {0} is retired as of effective date. Hence was cleared.", valueControl.Text));
            SetSafeDisplayText(valueControl, string.Empty);
            SetSafeIdNumber(idControl, string.Empty);
        }
        #endregion

        #endregion

        ScriptManager1.SetFocus(txEffectiveDate);
    }
    protected void AgencyDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListItem selectedItem = Common.GetSafeSelectedItem(AgencyDropDownList1);
        SetIfReferred(selectedItem.Value);
        // get again it gets reset by referral 
        selectedItem = Common.GetSafeSelectedItem(AgencyDropDownList1);
        string[] agencyParts = selectedItem.Text.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        if (agencyParts.Length == 0)
        {
            SetSafeIdText(spAgencyCityStateZipLabel, string.Empty);
            SetSafeDisplayText(spAgencyNameNumberLabel, string.Empty);
        }
        else
        {
            spAgencyNameNumberLabel.Text = string.Format("{0}", agencyParts[0]);
            string[] agencyCityStateArr = agencyParts[1].Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            List<string> neat = new List<string>();
            for (int i = 0; i < agencyCityStateArr.Length; i++)
                if (!string.IsNullOrEmpty(agencyCityStateArr[i].Trim()))
                    neat.Add(agencyCityStateArr[i]);

            SetSafeIdText(spAgencyCityStateZipLabel, string.Join(",", neat.ToArray()));
        }
        spAgencyIdLabel.Text = AgencyDropDownList1.SelectedValue;

        int intAgencyId = Convert.ToInt32(AgencyDropDownList1.SelectedValue);
        LineOfBusinessDropDownList1.DataBindByAgency(intAgencyId);
        lobLoaded.Text = Boolean.TrueString;
        underwriterLoaded.Text = Boolean.FalseString;
        analystLoaded.Text = Boolean.FalseString;
        technicianLoaded.Text = Boolean.FalseString;

        // TODO: restore previous lob selection

        // in case of default selection
        ListItem safeItemLOB = Common.GetSafeSelectedItem(LineOfBusinessDropDownList1);
        SetSafeDisplayText(spLineOfBusinessLabel, safeItemLOB.Text);
        SetSafeIdNumber(spLineOfBusinessIdLabel, safeItemLOB.Value);

        AgentDropDownList1.AgencyId = intAgencyId;
        AgentLoaded.Text = Boolean.FalseString;
        ContactDropDownList1.AgencyId = Convert.ToInt32(AgencyDropDownList1.SelectedValue);
        ContactLoaded.Text = Boolean.FalseString;

        //Wizard1_NextButtonClick(Wizard1, new WizardNavigationEventArgs(1, 2));

        // set agency licensed states
        if (DefaultValues.EnforceAgencyLicensedState(Common.GetSafeIntFromSession("CompanySelected")))
        {
            AgencyLicensedStates.Text = BCS.Core.Clearance.Admin.DataAccess.GetLicensedStates(intAgencyId).Replace(",", "|");
        }
        ScriptManager1.SetFocus(sender as Control);
    }
    protected void LineOfBusinessDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        underwriterLoaded.Text = Boolean.FalseString;
        analystLoaded.Text = Boolean.FalseString;
        technicianLoaded.Text = Boolean.FalseString;

        ListItem safeItemLOB = Common.GetSafeSelectedItem(LineOfBusinessDropDownList1);
        SetSafeDisplayText(spLineOfBusinessLabel, safeItemLOB.Text);
        SetSafeIdNumber(spLineOfBusinessIdLabel, safeItemLOB.Value);

        //if (safeItemLOB.Text.Equals(String.Empty))
        //{
        //    underwriterLoaded.Text = Boolean.TrueString;
        //    analystLoaded.Text = Boolean.TrueString;
        //    technicianLoaded.Text = Boolean.TrueString;
        //}
        PopulateControls(Wizard1.ActiveStepIndex);
        PopulateData(Wizard1.ActiveStepIndex);
        ScriptManager1.SetFocus(sender as Control);
    }
    protected void PolicySystemDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (PolicySystemDropDownList1.SelectedIndex > 0)
        {
            txPolicyNumber.CompanyNumberId = Convert.ToInt32(PolicySystemDropDownList1.SelectedValue);
        }
        else
        {
            txPolicyNumber.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        }
        ScriptManager1.SetFocus(PolicySystemDropDownList1);
    }
    protected void SubmissionStatusDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        int intStatusId = Convert.ToInt32(SubmissionStatusDropDownList1.SelectedValue);
        SubmissionStatusReasonDropDownList1.StatusId = intStatusId;
        SubmissionStatusReasonDropDownList1.DataBind();

        SetSafeDate(txSubmissionStatusDate, DateTime.Today, DateTime.Today.ToString());
        SetSafeDate(spSubmissionStatusIdBox, DateTime.Today, DateTime.Today.ToString());
        SetSafeDate(spSubmissionStatusDateLabel, DateTime.Today, DateTime.Today.ToString());
        SetSafeIdDate(spSubmissionStatusDateIdLabel, DateTime.Today.ToString());

        ScriptManager1.SetFocus(SubmissionStatusDropDownList1);
    }

    protected void gridAgencies_DataBound(object sender, EventArgs e)
    {
        var isCwg = Common.IsCWG();
        gridAgencies.Columns[6].Visible = isCwg;

        if (gridAgencies.Rows.Count == 1)
        {
            gridAgencies.SelectedIndex = 0;
            SetAgencySelectionFromGrid();
        }
    }
    protected void gridAgencies_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetAgencySelectionFromGrid();
        gridAgencies.DataBind(); // this should not be needed but somehow the agencies seem to lose state
    }

    private void SetAgencySelectionFromGrid()
    {
        SetIfReferred(gridAgencies.SelectedDataKey["Id"].ToString());
        spAgencyNameNumberLabel.Text = string.Format("{0} ({1})", gridAgencies.SelectedDataKey.Values["AgencyName"], gridAgencies.SelectedDataKey.Values["AgencyNumber"]);

        string agencyCityState = string.Format("{0},{1}", gridAgencies.SelectedDataKey.Values["City"], gridAgencies.SelectedDataKey.Values["State"]);
        string[] agencyCityStateArr = agencyCityState.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        SetSafeIdText(spAgencyCityStateZipLabel, string.Join(",", agencyCityStateArr));

        spAgencyIdLabel.Text = string.Format("{0}", gridAgencies.SelectedDataKey.Values["Id"]);

        int intAgencyId = Convert.ToInt32(spAgencyIdLabel.Text);
        LineOfBusinessDropDownList1.DataBindByAgency(intAgencyId);
        // in case of default selection
        if (LineOfBusinessDropDownList1.SelectedItem != null)
        {
            spLineOfBusinessIdLabel.Text = LineOfBusinessDropDownList1.SelectedValue;
            spLineOfBusinessLabel.Text = LineOfBusinessDropDownList1.SelectedItem.Text;
        }
        underwriterLoaded.Text = Boolean.FalseString;
        analystLoaded.Text = Boolean.FalseString;
        technicianLoaded.Text = Boolean.FalseString;

        AgentDropDownList1.AgencyId = Convert.ToInt32(spAgencyIdLabel.Text);
        AgentDropDownList1.Extra = 0;
        spAgentIdLabel.Text = "0";
        AgentLoaded.Text = Boolean.FalseString;
        ContactDropDownList1.AgencyId = Convert.ToInt32(spAgencyIdLabel.Text);
        ContactLoaded.Text = Boolean.FalseString;
        ValidateAndStoreStep(Wizard1.ActiveStepIndex + 1);
        // set agency licensed states
        if (DefaultValues.EnforceAgencyLicensedState(Common.GetSafeIntFromSession("CompanySelected")))
        {
            AgencyLicensedStates.Text = BCS.Core.Clearance.Admin.DataAccess.GetLicensedStates(intAgencyId).Replace(",", "|");
        }
        Control navControl = Wizard1.FindControl("StepNavigationTemplateContainerID");
        Control nextControl = navControl.FindControl("StepNextButton");
        ScriptManager1.SetFocus(nextControl);
    }

    private void SetIfReferred(string selectedId)
    {
        if (AgencyDropDownList1.CompanyNumberId == 0)
            AgencyDropDownList1.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        DataSet dataSet = AgencyDropDownList1.GetCachedSource();
        if (dataSet == null)
            return;
        DataRow dr = dataSet.Tables["Agency"].Select("Id = '" + selectedId + "'")[0];
        string referredAgencyNumber = Convert.ToString(dr["ReferralAgencyNumber"]);

        DateTime referredAgencyDate;
        DateTime.TryParse(Convert.ToString(dr["ReferralDate"]), out referredAgencyDate);

        if (string.IsNullOrEmpty(referredAgencyNumber) || referredAgencyDate > GetEffectiveDate(DateTime.MaxValue))
            return;
        DataRow[] rows = dataSet.Tables["Agency"].Select("AgencyNumber = '" + referredAgencyNumber + "'");

        if (rows.Length < 1)
        {
            return;
        }

        DateTime agencyCancelDate;
        DateTime.TryParse(Convert.ToString(rows[0]["CancelDate"]), out agencyCancelDate);
        if (agencyCancelDate != DateTime.MinValue && agencyCancelDate < GetEffectiveDate(DateTime.MaxValue))
        {
            Common.SetStatus(messagePlaceHolder, string.Format("Referred Agency Number {0} has been cancelled for the Agency Selected", referredAgencyNumber));
            return;
        }

        spAgencyIdLabel.Text = rows[0]["Id"].ToString();
        if (ShowGrid)
        {
            txFilterAgencyName.Text = rows[0]["AgencyName"].ToString();
            txFilterAgencyNumber.Text = rows[0]["AgencyNumber"].ToString();
            txFilterCity.Text = rows[0]["City"].ToString();
            txFilterState.Text = rows[0]["State"].ToString();

            btnFilter_OnClick(btnFilter, EventArgs.Empty);
        }
        else
        {
            //AgencyDropDownList1.Items.FindByValue(spAgencyIdLabel.Text).Selected = true; ;
        }
        AgencyInfo.Visible = true;
        AgencyInfo.Text = "Auto-selected referred agency.";

    }
    protected void gridAgencies_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Control c = e.Row.FindControl("lblAgencyComments");
        if (c != null)
        {
            if (c is Label)
            {
                Label wc = c as Label;
                if (null != e.Row.DataItem)
                {
                    if (string.IsNullOrEmpty((e.Row.DataItem as BCS.Biz.Agency).Comments))
                    {
                        wc.Text = "(none)";
                    }
                }
            }
        }
    }
    protected void gridAgencies_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        #region dummy grid to find selected by datakey
        gridAgencies.DataBind();
        gv.DataSourceID = odsAgencies.ID;
        gv.DataBind();
        string selectedAgencyId = spAgencyIdLabel.Text;
        DataKeyArray dkArray = gv.DataKeys;

        int rawRowIndex = -1;
        for (int i = 0; i < dkArray.Count; i++)
        {
            if (dkArray[i].Value.ToString() == selectedAgencyId)
            {
                rawRowIndex = i;
                break;
            }
        }
        int pageIndex = (int)(rawRowIndex / gridAgencies.PageSize);
        int rowIndex = rawRowIndex - (gridAgencies.PageSize * pageIndex);

        if (pageIndex == e.NewPageIndex)
        {
            gridAgencies.SelectedIndex = rowIndex;
        }
        else
        {
            gridAgencies.SelectedIndex = -1;
        }

        gv.SelectedIndex = rawRowIndex;
        #endregion
    }
    protected void odsAgencies_Init(object sender, EventArgs e)
    {
        BCS.Core.Security.CustomPrincipal user = (Page.User as BCS.Core.Security.CustomPrincipal);
        odsAgencies.SelectParameters["masterAgencyId"].DefaultValue =
            user.HasAgency() ? BCS.WebApp.Components.Security.GetMasterAgency(user.Agency).ToString() : "-1";

        if (Cache[odsAgencies.CacheKeyDependency] == null)
        {
            // TODO: see below
            // doing this just to create a cache dependency, pending enhancement see reason below
            // object data source's cache data is difficult to access. any updates to agency we will have to somehow clear this cache
            // if this page is accessed without this dependency ever being created, it will always get data from the data store instead of cache
            // may use a different dependency altogether
            AgencyDropDownList aDdl = new AgencyDropDownList();
            aDdl.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
            aDdl.GetCachedSource();
        }
    }
    protected void lbDbaNames_Init(object sender, EventArgs e)
    {
        lbDbaNames.Attributes.Add("onchange", string.Format("mimicComboBox({0},{1}); this.selectedIndex = -1;", lbDbaNames.ClientID, txDBA.ClientID));
    }
    protected void lbInsuredNames_Init(object sender, EventArgs e)
    {
        lbInsuredNames.Attributes.Add("onchange", string.Format("mimicComboBox({0},{1}); this.selectedIndex = -1;", lbInsuredNames.ClientID, txInsuredName.ClientID));
    }

    protected void cbSelectAllAddresses_CheckedChanged(object sender, EventArgs e)
    {
        if (DefaultValues.EnforceAgencyLicensedState(Common.GetSafeIntFromSession("CompanySelected")))
            SetClientStates();

        PopulateData(Wizard1.ActiveStepIndex);
    }

    protected void List_Load(object sender, EventArgs e)
    {
        CheckBoxList cbl = sender as CheckBoxList;
        foreach (ListItem li in cbl.Items)
        {
            Control c1 = Common.FindControlRecursive(Page, cbl.ID + "4Validation");
            Control c2 = Common.FindControlRecursive(Page, cbl.ID + "4StateWarning");
            Control cAll = Common.FindControlRecursive(Page, cbl.ID + "SelectAll");

            li.Attributes.Add("onclick", string.Format("NameOrAddressListSelectionChanged('{1}', ['{2}', '{3}'], '{4}');", cbl.ID, cbl.ClientID,
                c1 == null ? string.Empty : c1.ClientID, c2 == null ? string.Empty : c2.ClientID,
                cAll.ClientID));
        }
    }

    protected void GridViewCL_Load(object sender, EventArgs e)
    {
        GridView gv = sender as GridView;

        for (int i = 0; i < gv.Rows.Count; i++)
        {
            string name;
            string controlName;

            if (gv.ID.Contains("Address"))
            {
                controlName = "addressCheckBox";
                name = "AddressList";
            }
            else
            {
                controlName = "nameCheckBox";
                name = "NameList";
            }

            CheckBox cb = (CheckBox)gv.Rows[i].FindControl(controlName);

            Control c1 = Common.FindControlRecursive(Page, name + "4Validation");
            Control c2 = Common.FindControlRecursive(Page, name + "4StateWarning");
            Control cAll = Common.FindControlRecursive(Page, name + "SelectAll");

            cb.Attributes.Add("onclick", string.Format("NameOrAddressListSelectionChangedGV('{1}', ['{2}', '{3}'], '{4}');", cb.ID, GetClientCBClientIDs(gv.ID, controlName),
                c1 == null ? string.Empty : c1.ClientID, c2 == null ? string.Empty : c2.ClientID,
                cAll.ClientID));
        }
    }

    protected void AllCheck_Init(object sender, EventArgs e)
    {
        // oninit has a peculiar affect, controls loosing their state on postbacks especially loblist1 and commentsgrid. So, using onload.
        CheckBox cAll = sender as CheckBox;

        Control cbl;
        string gridViewName;
        string controlName;
        if (cAll.ID.Contains("Address"))
        {
            gridViewName = "gvClientAddresses";
            controlName = "addressCheckBox";
            cbl = Common.FindControlRecursive(Page, gridViewName);
        }
        else
        {
            gridViewName = "gvClientNames";
            controlName = "nameCheckBox";
            cbl = Common.FindControlRecursive(Page, gridViewName);
        }

        Control c1 = Common.FindControlRecursive(Page, cAll.ID.Replace("SelectAll", string.Empty) + "4Validation");
        Control c2 = Common.FindControlRecursive(Page, cAll.ID.Replace("SelectAll", string.Empty) + "4StateWarning");

        cAll.Attributes.Add("onclick", string.Format("checkAllCBLGV({4}, '{1}' ); NameOrAddressListSelectionChangedGV('{1}', ['{2}', '{3}'], '{4}');", cbl.ID, GetClientCBClientIDs(gridViewName, controlName),
            c1 == null ? string.Empty : c1.ClientID, c2 == null ? string.Empty : c2.ClientID,
            cAll.ClientID));
    }

    private string GetClientCBClientIDs(string gridViewName, string controlName)
    {
        StringBuilder sb = new StringBuilder();

        GridView gv = (GridView)Common.FindControlRecursive(Page, gridViewName);

        for (int i = 0; i < gv.Rows.Count; i++)
        {
            sb.Append(string.Format("{0}|", gv.Rows[i].FindControl(controlName).ClientID));
        }

        return sb.ToString().TrimEnd('|');
    }

    protected void cbTBD_CheckedChanged(object sender, EventArgs e)
    {

        CheckBox cbTBD = sender as CheckBox;
        rfvEff.Enabled = !cbTBD.Checked;
        txEffectiveDate.Enabled = !cbTBD.Checked;
        txExpirationDate.Enabled = !cbTBD.Checked;

        BCS.Biz.CompanyParameter parameter = BCS.WebApp.Components.ConfigValues.GetCompanyParameter(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        if (cbTBD.Checked && !parameter.SubmissionStatusDateTBD.IsNull)
        {
            txEffectiveDate.Text = DateTime.Now.AddDays((int)parameter.SubmissionStatusDateTBD).ToString("MM/dd/yyyy");
            txExpirationDate.Text = DateTime.Parse(txEffectiveDate.Text).AddYears(1).ToString("MM/dd/yyyy");

            spEffectiveDateIdLabel.Text = txEffectiveDate.Text;
            spExpirationDateIdLabel.Text = txExpirationDate.Text;

            txEffectiveDate_Changed(txEffectiveDate, EventArgs.Empty);
            return;
        }

        txEffectiveDate_Changed(txEffectiveDate, EventArgs.Empty);
        txEffectiveDate.Text = cbTBD.Checked ? "TBD" : string.Empty;
        txExpirationDate.Text = cbTBD.Checked ? "TBD" : string.Empty;

        spEffectiveDateIdLabel.Text = cbTBD.Checked ? "TBD" : string.Empty;
        spExpirationDateIdLabel.Text = cbTBD.Checked ? "TBD" : string.Empty;


    }

    protected void CheckBox4Toggle_Init(object sender, EventArgs e)
    {
        CheckBox cb = (sender as CheckBox);
        cb.InputAttributes.CssStyle.Add(HtmlTextWriterStyle.Display, "none");
        cb.InputAttributes.Add("disabled", "true");
        cb.LabelAttributes.CssStyle.Add(HtmlTextWriterStyle.MarginLeft, "11px");
    }

    #region LOB Children Grid

    private void UpdateLOBSToViewState()
    {
        List<SubmissionLineOfBusinessChild> list = GetLOBSAsList();
        foreach (GridViewRow gridRow in LobList1.Rows)
        {
            DropDownList lobDdl = gridRow.FindControl("LineOfBusinessChildrenDropDownList1") as DropDownList;
            DropDownList statusDdl = gridRow.FindControl("SubmissionStatusDropDownListLOB1") as DropDownList;
            DropDownList reasonDdl = gridRow.FindControl("SubmissionStatusReasonDropDownListLOB1") as DropDownList;
            object oid = LobList1.DataKeys[gridRow.RowIndex].Value;
            int id = 0;
            try
            {
                id = Convert.ToInt32(oid);
            }
            catch { }

            foreach (SubmissionLineOfBusinessChild var in list)
            {
                if (id == var.Id)
                {
                    var.CompanyNumberLOBGridId = Convert.ToInt32(lobDdl.SelectedValue);
                    var.SubmissionStatusId = Convert.ToInt32(statusDdl.SelectedValue);
                    var.SubmissionStatusReasonId = Convert.ToInt32(reasonDdl.SelectedValue);
                }
            }
            LOBS = list.ToArray();
        }
    }
    protected void SubmissionStatusDropDownListLOB1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SubmissionStatusDropDownList statusDDL = sender as SubmissionStatusDropDownList;
        Control statusReasonCtrl = Common.FindControlRecursive((sender as Control).NamingContainer, "SubmissionStatusReasonDropDownListLOB1");
        SubmissionStatusReasonDropDownList statusReasonDDL = statusReasonCtrl as SubmissionStatusReasonDropDownList;
        int reasonId = Common.GetSafeSelectedValue(statusReasonDDL);

        statusReasonDDL.StatusId = Convert.ToInt32(statusDDL.SelectedValue);
        statusReasonDDL.DataBind();

        Common.SetSafeSelectedValue(statusReasonDDL, reasonId);

        UpdateLOBSToViewState();
        BindLobs();
        // since we are rebinding, search by row index
        Control cToFocus = LobList1.Rows[((sender as Control).NamingContainer as GridViewRow).RowIndex].FindControl("SubmissionStatusDropDownListLOB1");
        ScriptManager1.SetFocus(cToFocus);
    }

    protected void LOBStatusesDDL_SelectedIndexChanged(object sender, EventArgs e)
    {
        Control ctrl = Common.FindControlRecursive((sender as Control).NamingContainer, "LOBStatusReasonsDDL");

        SubmissionStatusReasonDropDownList statusReasonDDL = ctrl as SubmissionStatusReasonDropDownList;
        statusReasonDDL.StatusId = Convert.ToInt32((sender as DropDownList).SelectedValue);
        statusReasonDDL.DataBind();

        WebControl updateCtrl = Common.FindControlRecursive((sender as Control).NamingContainer, "ibUpdate") as WebControl;
        updateCtrl.Enabled = statusReasonDDL.StatusId != 0;
        ScriptManager1.SetFocus(sender as Control);
    }

    protected void UpdateStatusesFromLOB(object sender, EventArgs e)
    {
        Control parent = (sender as Control).NamingContainer;
        DropDownList statusDDL = Common.FindControlRecursive(parent, "LOBStatusesDDL") as DropDownList;
        DropDownList statusReasonDDL = Common.FindControlRecursive(parent, "LOBStatusReasonsDDL") as DropDownList;
        List<SubmissionLineOfBusinessChild> list = GetLOBSAsList();


        System.Collections.Specialized.StringCollection excludedStatues = DefaultValues.GetLOBGridExcludedUpdateStatuses(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (SubmissionLineOfBusinessChild var in list)
        {
            if (!excludedStatues.Contains(var.SubmissionStatusId.ToString()))
            {
                var.SubmissionStatusId = Convert.ToInt32(statusDDL.SelectedValue);
                var.SubmissionStatusReasonId = Convert.ToInt32(statusReasonDDL.SelectedValue);
            }
        }
        LOBS = list.ToArray();
        BindLobs();

        ScriptManager1.SetFocus(sender as Control);
    }

    protected void RemoveLOB(Object sender, GridViewDeleteEventArgs e)
    {
        if (LobList1.Rows.Count <= 1)
        {
            e.Cancel = true;
            return;
        }

        List<SubmissionLineOfBusinessChild> list = GetLOBSAsList();
        object oid = LobList1.DataKeys[e.RowIndex].Value;
        int id = 0;
        try
        {
            id = Convert.ToInt32(oid);
        }
        catch { }
        for (int i = 0; i < list.Count; i++)
        {
            SubmissionLineOfBusinessChild item = list[i];
            if (id == item.Id)
            {
                if (item.Id < 0)
                {
                    list.RemoveAt(i);
                    break;
                }
                else
                {
                    item.Deleted = true;
                    break;
                }
            }
        }
        LOBS = list.ToArray();
        BindLobs();
    }
    protected void AddLOB(Object sender, EventArgs e)
    {
        AddLOB();
    }

    protected void LOBFooter_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Style.Add(HtmlTextWriterStyle.Display, "none");
        }
    }
    protected void LobList1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
            return;

        int reasonId = (e.Row.DataItem as BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild).SubmissionStatusReasonId;

        Control lobCtrl = Common.FindControlRecursive(e.Row, "LineOfBusinessChildrenDropDownList1");
        Control statusReasonCtrl = Common.FindControlRecursive(e.Row, "SubmissionStatusReasonDropDownListLOB1");

        LineOfBusinessChildrenDropDownList lobDDL = lobCtrl as LineOfBusinessChildrenDropDownList;
        SubmissionStatusReasonDropDownList statusReasonDDL = statusReasonCtrl as SubmissionStatusReasonDropDownList;

        lobDDL.Extra = (e.Row.DataItem as BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild).CompanyNumberLOBGridId;
        lobDDL.DataBind();
        Common.SetSafeSelectedValue(lobDDL, (e.Row.DataItem as BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild).CompanyNumberLOBGridId);


        statusReasonDDL.StatusId = (e.Row.DataItem as BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild).SubmissionStatusId;
        statusReasonDDL.Extra = (e.Row.DataItem as BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild).SubmissionStatusReasonId;
        statusReasonDDL.DataBind();

        Common.SetSafeSelectedValue(statusReasonDDL, reasonId);
    }
    protected void LobList1_DataBound(object sender, EventArgs e)
    {
        foreach (GridViewRow srcRow in LobList1.Rows)
        {
            if (srcRow.RowIndex <= LOBList4Display.Rows.Count - 1)
            {
                GridViewRow targetRow = LOBList4Display.Rows[srcRow.RowIndex];

                WebControl baseControl = Common.FindControlRecursive(srcRow, "SubmissionStatusReasonDropDownListLOB1") as WebControl;
                WebControl targetControl = Common.FindControlRecursive(targetRow, "SubmissionStatusReasonDropDownListLOB1") as WebControl;

                baseControl.Attributes.Add("onchange", string.Format("setValue(this,'{0}');", targetControl.ClientID));
            }
        }
    }
    protected void LobList1_RowDataBound4Display(object sender, GridViewRowEventArgs e)
    {
        LobList1_RowDataBound(sender, e);
        if (e.Row.RowType != DataControlRowType.DataRow)
            return;

        DropDownList lobDDLCtrl = Common.FindControlRecursive(e.Row, "LineOfBusinessChildrenDropDownList1") as DropDownList;
        ITextControl lobTbCtrl = Common.FindControlRecursive(e.Row, "LineOfBusinessChildrenTextBox1") as ITextControl;
        if (lobDDLCtrl != null && lobTbCtrl != null)
        {
            if (lobDDLCtrl.SelectedItem != null)
            {
                lobTbCtrl.Text = lobDDLCtrl.SelectedItem.Text;
            }
        }

        DropDownList ssDDLCtrl = Common.FindControlRecursive(e.Row, "SubmissionStatusDropDownListLOB1") as DropDownList;
        ITextControl ssTbCtrl = Common.FindControlRecursive(e.Row, "SubmissionStatusTextBox1") as ITextControl;
        if (ssDDLCtrl != null && ssTbCtrl != null)
        {
            if (ssDDLCtrl.SelectedItem != null)
            {
                ssTbCtrl.Text = ssDDLCtrl.SelectedItem.Text;
            }
        }

        DropDownList ssrDDLCtrl = Common.FindControlRecursive(e.Row, "SubmissionStatusReasonDropDownListLOB1") as DropDownList;
        ITextControl ssrTbCtrl = Common.FindControlRecursive(e.Row, "SubmissionStatusReasonTextBox1") as ITextControl;
        if (ssrDDLCtrl != null && ssrTbCtrl != null)
        {
            if (ssrDDLCtrl.SelectedItem != null)
            {
                ssrTbCtrl.Text = ssrDDLCtrl.SelectedItem.Text;
            }
        }


        RequiredFieldValidator reqReasonCtrl = Common.FindControlRecursive(e.Row, "rfvReason") as RequiredFieldValidator;
        if (null != reqReasonCtrl)
        {
            //reqReasonCtrl.ControlToValidate = statusReasonDDL.ID;
            reqReasonCtrl.ErrorMessage = string.Format("Reason for Status {0} is required.", Common.GetSafeSelectedItem(ssDDLCtrl).Text);
            reqReasonCtrl.Enabled = ssrDDLCtrl.Items.Count > 1;
        }
    }

    #endregion

    #region Status History Grid
    protected void gridStatusHistory_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView historyGrid = sender as GridView;
        if (historyGrid.SelectedIndex == -1)
            return;
        int newStatusId = Common.GetSafeSelectedValue(SubmissionStatusDropDownList1);
        string result = BCS.Core.Clearance.Submissions.ToggleSubmissionStatusHistoryActive(Convert.ToInt32(historyGrid.SelectedDataKey["Id"]),
            Common.GetUser(), ref newStatusId);
        if (SubmissionStatusDropDownList1.SelectedValue != newStatusId.ToString())
        {
            spSubmissionStatusIdLabel.Text = newStatusId.ToString();
            statusLoaded.Text = Boolean.FalseString;
        }
        Common.SetStatus(HistoryStatusPH, result, true);
        historyGrid.SelectedIndex = -1;
        odsStatusHistory.DataBind();
        historyGrid.DataBind();

        gridAgencies.DataBind(); // this should not be needed but somehow the agencies seem to lose state
        Response.Redirect(Request.Url.AbsoluteUri);
    }
    protected void gridStatusHistory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            BCS.Biz.SubmissionStatusHistory aHistory = e.Row.DataItem as BCS.Biz.SubmissionStatusHistory;
            if (!aHistory.Active)
            {
                foreach (TableCell var in e.Row.Cells)
                {
                    var.Style.Add(HtmlTextWriterStyle.TextDecoration, "line-through");
                }
            }

            LinkButton lb = e.Row.FindControl("LinkButton1") as LinkButton;
            ConfirmButtonExtender confirmButtonExtender = e.Row.FindControl("ConfirmButtonExtender1") as ConfirmButtonExtender;
            if (lb != null)
            {
                lb.Text = aHistory.Active ? "active" : "inactive";
                lb.Enabled = BCS.WebApp.Components.Security.CanActivateStatus() && (aHistory.Active.Value || User.IsInRole("System Administrator"));
                lb.Enabled = lb.Enabled && !IsReadOnly;
            }
            if (confirmButtonExtender != null)
            {
                confirmButtonExtender.Enabled = BCS.WebApp.Components.Security.CanActivateStatus() && (aHistory.Active.Value || User.IsInRole("System Administrator"));
                confirmButtonExtender.Enabled = confirmButtonExtender.Enabled && !IsReadOnly;
                if (User.IsInRole("System Administrator"))
                {
                    confirmButtonExtender.ConfirmText = "Are you sure you want to change this status history?";
                }
            }
        }
    }
    protected void odsStatusHistory_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        StatusHistoryCountLabel.Text = (e.ReturnValue as BCS.Biz.SubmissionStatusHistoryCollection).Count.ToString();
    }
    #endregion

    #region Comments Grid

    protected void gridComments_DataBound(object sender, EventArgs e)
    {
        CommentCountLabel.Text = (sender as GridView).Rows.Count.ToString();
    }
    #endregion

    protected void aceFilterAgencyName_Load(object sender, EventArgs e)
    {
        aceFilterAgencyName.ContextKey = "AgencyName|" + GetEffectiveDate(DateTime.MaxValue.Subtract(new TimeSpan(0, 0, 1)));
    }

    protected void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        throw new ApplicationException(string.Empty, e.Exception);
    }
    #endregion

    #region Other Methods

    protected string GetImageUrl(string image)
    {
        return Common.GetImageUrl(image);
    }

    protected void DefaultSelect(ListControl listCtrl)
    {
        if (listCtrl.Items.Count == 1)
        {
            listCtrl.SelectedIndex = 0;
        }
    }

    protected void DefaultSelect(GridView gv, string controlName)
    {
        if (gv.Rows.Count != 1) return;

        CheckBox cb = (CheckBox)gv.Rows[0].FindControl(controlName);
        cb.Checked = true;
    }
    private void Load4Copy()
    {
        List<SubmissionCopyExclusion> exclusions = ConfigValues.GetCopyExcludedFieldsDetailed(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        lnkCopySubmission.Visible = exclusions.Count == 0;
        lnkCopy.Visible = exclusions.Count != 0;
        List<IValidator> validators = FindValidationControls(Page);
        List<IValidator> reqValidators = new List<IValidator>();
        foreach (IValidator var in validators)
        {
            if (var.GetType().Name == "RequiredFieldValidator" || var.GetType().BaseType.Name == "RequiredFieldValidator")
            {
                reqValidators.Add(var);
            }
        }

        Table table = new Table(); table.Width = Unit.Percentage(100);
        foreach (SubmissionCopyExclusion var in exclusions)
        {
            Control foundExcludedField = Common.FindControlRecursive(Page, var.ControlId);
            if (null != foundExcludedField)
            {
                TableRow tr = new TableRow();
                TableCell tc = new TableCell();
                tc.Attributes.Add("align", "right");
                tc.Style.Add(HtmlTextWriterStyle.WhiteSpace, "nowrap");
                tc.Text = var.Header; tr.Cells.Add(tc);

                tc = new TableCell();
                tc.Attributes.Add("align", "left");
                Control clonedControl = ControlCloner.Copy(foundExcludedField, "4Copy");
                clonedControl.Visible = true;
                tc.Controls.Add(clonedControl);

                if (clonedControl is DropDownList)
                {
                    (clonedControl as DropDownList).Style.Add("margin-top", "12px");

                    ListSearchExtender lse = new ListSearchExtender();
                    lse.ID = Guid.NewGuid().ToString();
                    lse.TargetControlID = clonedControl.ID;
                    lse.PromptCssClass = "ListSearchExtenderPrompt4CopyPanel";
                    lse.SkinID = "none";
                    tc.Controls.Add(lse);
                }
                tr.Cells.Add(tc);

                tr.Enabled = var.Enabled;
                if (!var.Visible)
                {
                    //tr.CssClass = "displayedNone";
                    tr.Style.Add(HtmlTextWriterStyle.Display, "none");
                }

                table.Rows.Add(tr);
            }
        }
        CopyContainerPanel.Controls.AddAt(0, table);

        chkCopyStatusHistory.Visible = BCS.WebApp.Components.ConfigValues.DisplayCopyStatusesOption(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
    }
    private int GetAddDefaultStepIndex()
    {
        WizardConfigSteps dictionary = ConfigValues.GetWizardSteps(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        return dictionary.AddDefaultStepIndex;
    }
    private int GetCopyDefaultStepIndex()
    {
        WizardConfigSteps dictionary = ConfigValues.GetWizardSteps(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        return dictionary.CopyDefaultStepIndex;
    }
    private int GetEditDefaultStepIndex()
    {
        WizardConfigSteps dictionary = ConfigValues.GetWizardSteps(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        return dictionary.EditDefaultStepIndex;
    }

    protected bool IsProspect()
    {
        return (spSubmissionTypeLabel.Text == "Prospect");
    }

    protected bool PolicyNumberIsRequired()
    {
        if (Common.IsCWG())
            return !IsProspect();
        else
            return ConfigValues.RequiresPolicyNumber(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
    }

    private void xLoadSubmission(BCS.Core.Clearance.BizObjects.Submission submissionObj)
    {
        Session[SessionKeys.VerifyingSubmission] = submissionObj;
        // type
        SetId(spSubmissionTypeIdLabel, submissionObj.SubmissionType.Id);
        SetSafeDisplayText(spSubmissionTypeLabel, submissionObj.SubmissionType.TypeName);

        // submission date
        SetSafeDate(SubmissionDateLabel, submissionObj.SubmissionDt, string.Empty);

        // effective date
        SetSafeDate(spEffectiveDateLabel, submissionObj.EffectiveDate, string.Empty);
        SetSafeDate(spEffectiveDateIdLabel, submissionObj.EffectiveDate, string.Empty);
        SetSafeDate(txEffectiveDate, submissionObj.EffectiveDate, string.Empty);

        // expiration date
        SetSafeDate(spExpirationDateLabel, submissionObj.ExpirationDate, string.Empty);
        SetSafeDate(spExpirationDateIdLabel, submissionObj.ExpirationDate, string.Empty);
        SetSafeDate(txExpirationDate, submissionObj.ExpirationDate, string.Empty);

        // submission number
        SubmissionNumberLabel.Text = submissionObj.SubmissionNumber.ToString();
        Session["SubmissionNumber"] = SubmissionNumberLabel.Text;
        // submission number sequence
        SubmissionNumberSequenceLabel.Text = submissionObj.SubmissionNumberSequence.ToString();

        // other carrier
        SetId(spOtherCarrierIdLabel, submissionObj.OtherCarrierId);
        if (submissionObj.OtherCarrier != null)
        {
            SetSafeDisplayText(spOtherCarrierLabel, string.Format("{0} - {1}", submissionObj.OtherCarrier.Code, submissionObj.OtherCarrier.Description));
        }

        // status
        SetId(spSubmissionStatusIdLabel, submissionObj.SubmissionStatusId);
        if (submissionObj.SubmissionStatus != null)
        {
            SetSafeDisplayText(spSubmissionStatusLabel, submissionObj.SubmissionStatus.StatusCode);
        }
        //Common.SetSafeSelectedValue(SubmissionStatusDropDownList2, submissionObj.SubmissionStatusId);

        // status reason
        SetId(spSubmissionStatusReasonIdLabel, submissionObj.SubmissionStatusReasonId);
        SetSafeDisplayText(spSubmissionStatusReasonLabel, submissionObj.SubmissionStatusReason);

        // status date
        SetSafeDate(spSubmissionStatusDateLabel, submissionObj.SubmissionStatusDate, hfDisplayText.Value);
        SetSafeDate(spSubmissionStatusDateIdLabel, submissionObj.SubmissionStatusDate, DateTime.Today.ToString(Common.DateFormat));
        SetSafeDate(txSubmissionStatusDate, submissionObj.SubmissionStatusDate, DateTime.Today.ToString(Common.DateFormat));
        SetSafeDate(spSubmissionStatusIdBox, submissionObj.SubmissionStatusDate, DateTime.Today.ToString(Common.DateFormat));

        // cancellation date
        SetSafeDate(spCancellationDateLabel, submissionObj.CancellationDate, hfDisplayText.Value);

        // deleted information
        if (submissionObj.Deleted)
        {
            hdnSubmissionIsDeleted.Value = submissionObj.Deleted.ToString();
            lblDeletedInformation.Text = string.Format("Submission has been deleted by {0} on {1}", submissionObj.DeletedBy, submissionObj.DeletedDate.ToString("MM/dd/yyyy"));
        }
        // agency
        SetId(spAgencyIdLabel, submissionObj.AgencyId);
        SetSafeDisplayText(spAgencyNameNumberLabel, string.Format("{0} ({1})", submissionObj.Agency.AgencyName, submissionObj.Agency.AgencyNumber));
        string agencyCityState = string.Format("{0}|{1}", submissionObj.Agency.City, submissionObj.Agency.State);
        string[] agencyCityStateArr = agencyCityState.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        SetSafeIdText(spAgencyCityStateZipLabel, string.Join(",", agencyCityStateArr));
        SetIfReferred(submissionObj.AgencyId.ToString());

        // line of business
        SetId(spLineOfBusinessIdLabel, submissionObj.LineOfBusinessId);
        SetSafeDisplayText(spLineOfBusinessLabel, submissionObj.LineOfBusiness);

        // agent
        SetId(spAgentIdLabel, submissionObj.AgentId);
        if (submissionObj.Agent != null)
        {
            SetSafeDisplayText(spAgentLabel, string.Format("{0} - {1} - {2}", submissionObj.Agent.Surname, submissionObj.Agent.FirstName, submissionObj.Agent.BrokerNo));
        }

        // contact
        SetId(spContactIdLabel, submissionObj.ContactId);
        if (submissionObj.Contact != null)
        {
            SetSafeDisplayText(spContactLabel, submissionObj.Contact.Name);
        }

        bool usesAPSServiceForAssignments =
            DefaultValues.SupportsAPSServiceforAssignments(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

        // underwriter
        SetSafeDisplayText(spUnderwriterLabel, submissionObj.UnderwriterFullName);
        if (!usesAPSServiceForAssignments)
        {
            SetId(spUnderwriterIdLabel, submissionObj.UnderwriterId);
        }
        else
        {
            BCS.Biz.Person uwPerson = BCS.Core.Clearance.Admin.DataAccess.GetPerson(submissionObj.UnderwriterId);
            if (uwPerson != null)
                SetSafeIdNumber(spUnderwriterIdLabel, uwPerson.APSId.ToString());
        }

        // previous underwriter
        SetSafeDisplayText(spPreviousUnderwriterLabel, submissionObj.PreviousUnderwriterFullName);

        // analyst
        SetSafeDisplayText(spAnalystLabel, submissionObj.AnalystFullName);
        if (!usesAPSServiceForAssignments)
        {
            SetId(spAnalystIdLabel, submissionObj.AnalystId);
        }
        else
        {
            BCS.Biz.Person anPerson = BCS.Core.Clearance.Admin.DataAccess.GetPerson(submissionObj.AnalystId);
            if (anPerson != null)
                SetSafeIdNumber(spAnalystIdLabel, anPerson.APSId.ToString());
        }

        // technician
        SetSafeDisplayText(spTechnicianLabel, submissionObj.TechnicianFullName);
        if (!usesAPSServiceForAssignments)
        {
            SetId(spTechnicianIdLabel, submissionObj.TechnicianId);
        }
        else
        {
            BCS.Biz.Person techPerson = BCS.Core.Clearance.Admin.DataAccess.GetPerson(submissionObj.TechnicianId);
            if (techPerson != null)
                SetSafeIdNumber(spTechnicianIdLabel, techPerson.APSId.ToString());
        }

        // policy system
        SetId(spPolicySystemIdLabel, submissionObj.PolicySystemId);
        if (submissionObj.PolicySystem != null)
        {
            SetSafeDisplayText(spPolicySystemLabel, submissionObj.PolicySystem.Abbreviation);
        }
        else
        {
            SetSafeDisplayText(spPolicySystemLabel, ConfigValues.GetCurrentCompany(Common.GetSafeIntFromSession(SessionKeys.CompanyId)).CompanyInitials);
        }

        // policy number
        SetSafeDisplayText(spPolicyNumberLabel, submissionObj.PolicyNumber);
        SetSafeIdText(spPolicyNumberIdLabel, submissionObj.PolicyNumber);

        // estimated written premium
        SetSafeDisplayText(spEstimatedPremiumLabel, string.Format("{0:c}", submissionObj.EstimatedWrittenPremium));
        SetSafeDecimal(spEstimatedPremiumIdLabel, submissionObj.EstimatedWrittenPremium.ToString());

        // other carrier premium
        SetSafeDisplayText(spOtherCarrierPremiumLabel, string.Format("{0:c}", submissionObj.OtherCarrierPremium));
        SetSafeDecimal(spOtherCarrierPremiumIdLabel, submissionObj.OtherCarrierPremium.ToString());

        // insured name
        SetSafeDisplayText(spInsuredNameLabel, Server.HtmlEncode(submissionObj.InsuredName));
        SetSafeIdText(spInsuredNameIdLabel, Server.HtmlEncode(submissionObj.InsuredName));

        // dba
        SetSafeDisplayText(spDBALabel, Server.HtmlEncode(submissionObj.Dba));
        SetSafeIdText(spDBAIdLabel, Server.HtmlEncode(submissionObj.Dba));

        // submission status other reason
        SetSafeDisplayText(spSubmissionStatusReasonOtherLabel, submissionObj.SubmissionStatusReasonOther);
        SetSafeIdText(txSubmissionStatusReasonOther, submissionObj.SubmissionStatusReasonOther);
        SetSafeIdText(spSubmissionStatusReasonOtherIdLabel, submissionObj.SubmissionStatusReasonOther);

        if (submissionObj.AgentUnspecifiedReasonList != null)
        {
            List<string> textItems = new List<string>();
            List<string> valueItems = new List<string>();
            foreach (SubmissionAgentUnspecifiedReason var in submissionObj.AgentUnspecifiedReasonList)
            {
                textItems.Add(var.Reason);
                valueItems.Add(var.CompanyNumberAgentUnspecifiedReasonId.ToString());
            }

            spAgentReasons.DataSource = textItems;
            spAgentReasons.DataBind();

            spAgentUnspecifiedIdLabel.Text = string.Join("|", valueItems.ToArray());
        }

        #region Bind Submissions Company Number Codes and Attributes
        BindCodes(spCodeTypesContainerPanel, submissionObj.SubmissionCodeTypess);
        BindCodes(spCodeTypesContainerInLOBGridPanel, submissionObj.SubmissionCodeTypess);
        BindAttributes(spCodeTypesContainerPanel, submissionObj.AttributeList);
        BindAttributes(spCodeTypesContainerPanelOtherCarrier, submissionObj.AttributeList);
        BindCodes(spCodeTypesContainerPanelOtherCarrier, submissionObj.SubmissionCodeTypess);
        #endregion

        #region submission lob children
        LOBS = submissionObj.LineOfBusinessChildList;
        EffectiveLOBCount.Text = (2 * submissionObj.LineOfBusinessChildList.Length).ToString(); // onchange script seems to fire twice, hence 2* pending ......
        BindLobs();
        LOBListLoaded.Text = Boolean.TrueString;
        #endregion

        #region submission comments
        Comments = submissionObj.SubmissionComments;
        BindComments();
        #endregion

        #region set control attrs
        AgencyDropDownList1.CancelDate = submissionObj.EffectiveDate == DateTime.MinValue ? DateTime.Now : submissionObj.EffectiveDate;

        AgentDropDownList1.AgencyId = submissionObj.AgencyId;
        AgentDropDownList1.CancelDate = submissionObj.EffectiveDate == DateTime.MinValue ? DateTime.Now : submissionObj.EffectiveDate;

        ContactDropDownList1.AgencyId = submissionObj.AgencyId;

        UnderwriterDropDownList1.RetirementDate = submissionObj.EffectiveDate == DateTime.MinValue ? DateTime.Now : submissionObj.EffectiveDate;
        AnalystDropDownList1.RetirementDate = submissionObj.EffectiveDate == DateTime.MinValue ? DateTime.Now : submissionObj.EffectiveDate;
        TechnicianDropDownList1.RetirementDate = submissionObj.EffectiveDate == DateTime.MinValue ? DateTime.Now : submissionObj.EffectiveDate;

        SubmissionStatusReasonDropDownList1.StatusId = submissionObj.SubmissionStatusId;

        // set agency licensed states
        if (DefaultValues.EnforceAgencyLicensedState(Common.GetSafeIntFromSession("CompanySelected")))
        {
            AgencyLicensedStates.Text = BCS.Core.Clearance.Admin.DataAccess.GetLicensedStates(submissionObj.AgencyId).Replace(",", "|");
        }

        #endregion

        #region Client
        DateTime startDTClientLoadTime = DateTime.Now;
        GetAndLoadClient(submissionObj.ClientCoreClientId.ToString());
        DateTime endDTClientLoadTime = DateTime.Now;
        TimeSpan tsClientLoadTime = endDTClientLoadTime.Subtract(startDTClientLoadTime);
        if (LogInfoMessages.Text == Boolean.TrueString)
        {
            LogCentral.Current.LogInfo(string.Format("tsClientLoadTime {0}", tsClientLoadTime.TotalSeconds));
        }

        Session["ClientId"] = submissionObj.ClientCoreClientId.ToString();
        List<string> arrAddressIds = new List<string>();
        #region set addresses
        if (submissionObj.ClientCoreAddressIds == null || submissionObj.ClientCoreAddressIds.Length == 0)
        {
            // auto-update submission, if by any chance its not associated with any address, the client is already loaded
            arrAddressIds = Common.GetValues(gvClientAddresses);
            BCS.Core.Clearance.Submissions.UpdateSubmissionClientAddresses(submissionObj.Id, string.Join("|", arrAddressIds.ToArray()));
        }
        else
        {
            arrAddressIds.AddRange(Array.ConvertAll<int, string>(submissionObj.ClientCoreAddressIds, new Converter<int, string>(Convert.ToString)));
        }
        //AddressList.ClearSelection();

        gridSelectedAddresses.DataSource = gvClientAddresses.DataSource;
        gridSelectedAddresses.DataBind();

        for (int i = 0; i < gvClientAddresses.Rows.Count; i++)
        {
            string keyValue = gvClientAddresses.DataKeys[i].Value.ToString();
            if (arrAddressIds.Contains(keyValue))
            {
                CheckBox cb = (CheckBox)gvClientAddresses.Rows[i].FindControl("addressCheckBox");
                CheckBox cb2 = (CheckBox)gridSelectedAddresses.Rows[i].FindControl("CheckBoxAddr1");
                if (cb != null) cb.Checked = true;
                if (cb2 != null) cb2.Checked = true;

                ListItem li = AddressList4Validation.Items.FindByValue(keyValue);
                if (null != li)
                    li.Selected = true;

                li = AddressList4StateWarning.Items.FindByValue(keyValue);
                if (null != li)
                    li.Selected = true;
            }
        }


        // set client states for licensed state warning
        if (DefaultValues.EnforceAgencyLicensedState(Common.GetSafeIntFromSession("CompanySelected")))
            SetClientStates();
        #endregion
        #region set names
        List<string> arrNameIds = new List<string>();
        if (submissionObj.ClientCoreNameIds == null || submissionObj.ClientCoreNameIds.Length == 0)
        {
            // select all, if by any chance its not associated with any name, the client is already loaded
            //arrNameIds = Common.GetValues(NameList);
            arrNameIds = Common.GetValues(gvClientNames);
        }
        else
        {
            arrNameIds.AddRange(Array.ConvertAll<int, string>(submissionObj.ClientCoreNameIds, new Converter<int, string>(Convert.ToString)));
        }
        //NameList.ClearSelection();
        gridSelectedNames.DataSource = gvClientNames.DataSource;
        gridSelectedNames.DataBind();

        for (int i = 0; i < gvClientNames.Rows.Count; i++)
        {
            string keyValue = gvClientNames.DataKeys[i].Value.ToString();
            if (arrNameIds.Contains(keyValue))
            {
                CheckBox cb = (CheckBox)gvClientNames.Rows[i].FindControl("nameCheckBox");
                CheckBox cb2 = (CheckBox)gridSelectedNames.Rows[i].FindControl("CheckBoxName1");
                cb.Checked = true;
                cb2.Checked = true;

                ListItem li = NameList4Validation.Items.FindByValue(keyValue);
                if (null != li)
                    li.Selected = true;
            }
        }
        #endregion
        ClientLoaded.Text = Boolean.TrueString;
        #endregion
    }

    private void SetPageToReadOnly()
    {
        pnlWizard.Visible = false;
    }

    protected void SaveSubmission()
    {
        int companyid = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
        string companynumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
        int companynumberid = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);

        BCS.Core.Clearance.BizObjects.Submission coresub;

        SubmissionProxy.Submission sp = Common.GetSubmissionProxy();

        string clientid = ClientIdLabel.Text;
        string clientaddressids = string.Join("|", Common.GetSelectedValues(gvClientAddresses, "addressCheckBox"));
        string clientnameids = string.Join("|", Common.GetSelectedValues(gvClientNames, "nameCheckBox"));
        string clientNameIdsAndTypes = string.Join("|", Common.GetAllIdsAndTypesFromGV(gvClientNames, "clientNameType"));
        string cilentAddressIdsAndTypes = string.Join("|", Common.GetAllIdsAndTypesFromGV(gvClientAddresses, "clientAddressType"));
        string clientportfolioid = PortfolioIdLabel.Text.Length == 0 ? "0" : PortfolioIdLabel.Text;
        string user = User.Identity.Name;
        int subtypeid = Convert.ToInt32(spSubmissionTypeIdLabel.Text);
        string statusdate = Common.GetSafeDateTime(spSubmissionStatusDateIdLabel.Text, DateTime.Today).ToString(Common.DateFormat);
        int substatusid = Convert.ToInt32(spSubmissionStatusIdLabel.Text);
        int substatusreasonid = Convert.ToInt32(spSubmissionStatusReasonIdLabel.Text);

        bool supportsMultipleLOB = DefaultValues.SupportsMultipleLineOfBusinesses(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        if (supportsMultipleLOB)
        {
            BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild[] children = GetUpdateableLOBS();
            List<int> statuses = new List<int>();
            for (int i = 0; i < children.Length; i++)
            {
                if (children[i].SubmissionStatusId != 0)
                    statuses.Add(children[i].SubmissionStatusId);
            }
            // process further if any lob item was assigned a status, else submission status will be what was selected in the status dropdown
            if (statuses.Count != 0)
            {
                BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberSubmissionStatus.Columns.CompanyNumberId, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                BCS.Biz.CompanyNumberSubmissionStatusCollection dbStatuses = dm.GetCompanyNumberSubmissionStatusCollection();
                dbStatuses = dbStatuses.FilterByHierarchicalOrder(0, OrmLib.CompareType.Not);
                dbStatuses = dbStatuses.SortByHierarchicalOrder(OrmLib.SortDirection.Ascending);

                if (dbStatuses.Count > 0)
                {
                    List<int> hStatuses = new List<int>();

                    for (int i = 0; i < dbStatuses.Count; i++)
                    {
                        hStatuses.Add(dbStatuses[i].SubmissionStatusId);
                    }
                    int idToAssign = statuses[0];

                    foreach (int i in statuses)
                    {
                        int lowerIndex = Math.Min(hStatuses.IndexOf(idToAssign), hStatuses.IndexOf(i));
                        idToAssign = hStatuses[lowerIndex];
                    }
                    substatusid = idToAssign;
                    substatusreasonid = children[statuses.IndexOf(idToAssign)].SubmissionStatusReasonId;
                }
            }
        }
        int agencyid = Convert.ToInt32(spAgencyIdLabel.Text);
        int agentid = Convert.ToInt32(spAgentIdLabel.Text);
        int contactid = Convert.ToInt32(spContactIdLabel.Text);

        decimal dpremium = decimal.Zero;
        decimal.TryParse(spEstimatedPremiumIdLabel.Text, System.Globalization.NumberStyles.Currency,
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat, out dpremium);

        string dteff = string.Empty;
        string dtexp = string.Empty;
        if (!TBDCheckBox.Checked || !ConfigValues.GetCompanyParameter(companyid).SubmissionStatusDateTBD.IsNull)
        {
            dteff = spEffectiveDateIdLabel.Text;
            dtexp = spExpirationDateIdLabel.Text;
        }

        bool usesAPSServiceForAssignments =
            DefaultValues.SupportsAPSServiceforAssignments(Common.GetSafeIntFromSession(SessionKeys.CompanyId));


        int underwriterid, analystid, technicianid;
        if (usesAPSServiceForAssignments)
        {
            underwriterid = LookUpPersonIdUsingAPSId(spUnderwriterIdLabel.Text);
            analystid = LookUpPersonIdUsingAPSId(spAnalystIdLabel.Text);
            technicianid = LookUpPersonIdUsingAPSId(spTechnicianIdLabel.Text);
        }
        else
        {
            underwriterid = Convert.ToInt32(spUnderwriterIdLabel.Text);
            analystid = Convert.ToInt32(spAnalystIdLabel.Text);
            technicianid = Convert.ToInt32(spTechnicianIdLabel.Text);
        }

        int policysystemid = Convert.ToInt32(spPolicySystemIdLabel.Text);
        string comments = GetUpdateableCommentsAsString();
        string policynumber = spPolicyNumberIdLabel.Text;
        int lineofbusinessid = Convert.ToInt32(spLineOfBusinessIdLabel.Text);

        string lobchilren = GetUpdateableLOBSAsString();
        string insuredname = Server.HtmlDecode(spInsuredNameIdLabel.Text);
        string dba = Server.HtmlDecode(spDBAIdLabel.Text);
        string otherreason = spSubmissionStatusReasonOtherIdLabel.Text;
        int othercarrierid = Convert.ToInt32(spOtherCarrierIdLabel.Text);

        decimal ocpremium = decimal.Zero;
        decimal.TryParse(spOtherCarrierPremiumIdLabel.Text, System.Globalization.NumberStyles.Currency,
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat, out ocpremium);

        // TODO: 
        string agentunspecifiedreasons = spAgentUnspecifiedIdLabel.Text;

        #region codes
        List<string> codeIds = new List<string>();
        List<DropDownList> ddlsNormal = FindDropDownListControls(CodeTypesContainerPanel); // get drop down lists from regular
        foreach (DropDownList ddl in ddlsNormal)
        {
            Control c = Common.FindControlRecursive(spCodeTypesContainerPanel, ddl.ID.Replace("ddl", string.Empty) + "Id");
            codeIds.Add((c as ITextControl).Text);
        }
        List<DropDownList> ddlsLOBGrid = FindDropDownListControls(CodeTypesContainerInLOBGridPanel); // get drop down lists from lob grid
        foreach (DropDownList ddl in ddlsLOBGrid)
        {
            Control c = Common.FindControlRecursive(spCodeTypesContainerInLOBGridPanel, ddl.ID.Replace("ddl", string.Empty) + "Id");
            codeIds.Add((c as ITextControl).Text);
        }

        List<DropDownList> ddlsOtherCarrierGrid = FindDropDownListControls(codeTypesContainerPanelOtherCarrier);
        foreach (DropDownList ddl in ddlsOtherCarrierGrid)
        {
            Control c = Common.FindControlRecursive(spCodeTypesContainerPanelOtherCarrier, ddl.ID.Replace("ddl", string.Empty) + "Id");
            codeIds.Add((c as ITextControl).Text);
        }
        string[] codeids = codeIds.ToArray();
        #endregion
        #region attributes
        string[] attrIds = AttrIdsHidden.Value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        List<string> attrVals = new List<string>();
        foreach (string attrId in attrIds)
        {
            Control c = Common.FindControlRecursive(spCodeTypesContainerPanel, attrId + "IdLabel");

            Type t = c.GetType();
            // Gets the attributes for the collection.
            System.ComponentModel.AttributeCollection attrs = System.ComponentModel.TypeDescriptor.GetAttributes(c);

            /* Prints the name of the default property by retrieving the 
             * DefaultPropertyAttribute from the AttributeCollection. */
            ControlValuePropertyAttribute myAttribute =
               (ControlValuePropertyAttribute)attrs[typeof(ControlValuePropertyAttribute)];

            System.Reflection.PropertyInfo pi = t.GetProperty(myAttribute.Name);
            object value = pi.GetValue(c, null);
            attrVals.Add(string.Format("{0}={1}", attrId, value));
        }

        #region OtherCarrierAttributes
        string[] attrIdsOtherCarrier = AttrIdsHiddenOtherCarrier.Value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        foreach (string attrId in attrIdsOtherCarrier)
        {
            Control c = Common.FindControlRecursive(spCodeTypesContainerPanelOtherCarrier, attrId + "IdLabel");

            Type t = c.GetType();
            // Gets the attributes for the collection.
            System.ComponentModel.AttributeCollection attrs = System.ComponentModel.TypeDescriptor.GetAttributes(c);

            /* Prints the name of the default property by retrieving the 
             * DefaultPropertyAttribute from the AttributeCollection. */
            ControlValuePropertyAttribute myAttribute =
               (ControlValuePropertyAttribute)attrs[typeof(ControlValuePropertyAttribute)];

            System.Reflection.PropertyInfo pi = t.GetProperty(myAttribute.Name);
            object value = pi.GetValue(c, null);
            attrVals.Add(string.Format("{0}={1}", attrId, value));
        }
        #endregion
        string[] attributes = attrVals.ToArray();
        #endregion

        string submissionid = hdnSubmission.Value;
        if (Session["SubmissionId"] != null) // indicating edit submission
        {
            //string submissionid = Session["SubmissionId"].ToString();


            string edtXML = sp.ModifySubmissionByIdsWithTypes(submissionid, Convert.ToInt64(clientid),
                string.Empty, // stop gap parameter for other apps
                clientaddressids, clientnameids, Convert.ToInt64(clientportfolioid), companyid, subtypeid, agencyid, agentid,
                agentunspecifiedreasons,
                contactid,
                companynumberid, substatusid, statusdate, substatusreasonid, otherreason, policynumber, string.Join("|", codeids), string.Join("|", attributes), dpremium, dteff, dtexp,
                underwriterid, analystid, technicianid, policysystemid, comments, user, lineofbusinessid, lobchilren, insuredname, dba,
                othercarrierid, ocpremium, cilentAddressIdsAndTypes, clientNameIdsAndTypes);

            try
            {
                coresub = (BCS.Core.Clearance.BizObjects.Submission)BCS.Core.XML.Deserializer.Deserialize(
                    edtXML, typeof(BCS.Core.Clearance.BizObjects.Submission));
                if (coresub != null)
                    if (coresub.Message != null)
                    {
                        Session["SubmissionId"] = coresub.Id.ToString();
                        Common.SetStatus(messagePlaceHolder, string.Format("{0}, Submission Number : {1}", coresub.Message, coresub.SubmissionNumber));
                    }
            }
            catch (Exception)
            {
                Common.SetStatus(messagePlaceHolder, "Submission Edit not successful. ");
            }
            Cache.Remove("SearchedCacheDependency");

            return;
        }

        string submissionNo = string.Empty;

        if (DefaultValues.EnforceAgencyLicensedState(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
        {
            SubmissionComment[] existingComments = GetUpdateableComments();
            bool foundExistingWarningComment = false;
            foreach (SubmissionComment var in existingComments)
            {
                if (var.Comment.Contains("Review state entered"))
                {
                    foundExistingWarningComment = true;
                }
            }
            if (!foundExistingWarningComment)
            {
                string result = ValidateAgencyLicensedStates();

                txComments.Text = result;
                AddComment();
            }
            comments = GetUpdateableCommentsAsString();
        }

        // else of the above if, implying new submission.

        //CWG duplicate policynumber check
        if (Common.IsCWG())
        {
            customPolicyNumberValidator.Validate();
            if (!Page.IsValid)
                return;
        }

        cvAgentRequired.Validate();
        if (!Page.IsValid)
            return;

        string addXML = sp.AddSubmissionByIdsWithTypes(clientid,
            string.Empty, // stop gap for other apps
            clientaddressids, clientnameids, clientportfolioid,
            user, // stop gap for other apps
            companyid, // stop gap for other apps
            user, string.Empty, subtypeid, agencyid, agentid,
            agentunspecifiedreasons,
            contactid, companynumber, substatusid, statusdate, substatusreasonid, otherreason,
            policynumber, // append '-<something>' to indicate send to cobra, CORE checks if the system is bcsasclientcore. TODO remove this handling from Core.Clearance.Submissions and from here since we are not using web services anymore for CWG conversion process
            string.Join("|", codeids), string.Join("|", attributes),
            dpremium, dteff, dtexp, underwriterid, analystid, technicianid, policysystemid, comments, lineofbusinessid, lobchilren, insuredname, dba, submissionNo,
            othercarrierid, ocpremium, cilentAddressIdsAndTypes, clientNameIdsAndTypes);

        try
        {
            coresub = (BCS.Core.Clearance.BizObjects.Submission)BCS.Core.XML.Deserializer.Deserialize(
                addXML, typeof(BCS.Core.Clearance.BizObjects.Submission));
            if (coresub != null)
            {
                if (coresub.Status == "Failure")
                {
                    Common.SetStatus(messagePlaceHolder, string.Format("Unable to add submission because {0}", coresub.Message));
                }
                else
                {
                    if (coresub.Message != null)
                    {
                        Session["SubmissionId"] = coresub.Id.ToString();
                        Cache.Remove("SearchedCacheDependency");

                        Common.SetStatus(messagePlaceHolder, string.Format("{0}, Submission Number : {1}", coresub.Message, coresub.SubmissionNumber));

                        SetSafeDate(SubmissionDateLabel, coresub.SubmissionDt, string.Empty);
                        SubmissionNumberLabel.Text = coresub.SubmissionNumber.ToString();
                        SubmissionNumberSequenceLabel.Text = coresub.SubmissionNumberSequence.ToString();

                        performOFACcheck(coresub.Id, coresub.SubmissionNumber.ToString());

                        if (Session["CopiedSubmissionId"] != null)
                        {
                            BCS.Core.Clearance.Submissions.CopySubmissionStatus(Common.GetSafeIntFromSession("CopiedSubmissionId"), coresub.Id);
                            Session.Remove("CopiedSubmissionId");
                        }
                    }
                }
            }




        }
        catch (Exception)
        {
            Common.SetStatus(messagePlaceHolder,
                "Submission Add not successful. ");
        }
    }

    private int LookUpPersonIdUsingAPSId(string personIndex)
    {
        long apsId = long.MinValue;
        string personName = string.Empty;
        int personId = int.MinValue;

        long.TryParse(personIndex, out apsId);

        BCS.Biz.Person uwPerson = BCS.Core.Clearance.Admin.DataAccess.GetPersonByAPSId(apsId
                    , Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));

        if (uwPerson != null)
            personId = uwPerson.Id;
        else
            personId = Convert.ToInt32(personIndex);
        return personId;

    }

    private int GetBizPersonId(long apsPersonId, string companyNumber, string personName)
    {
        BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
        dm.QueryCriteria.Clear();
        dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.Columns.APSId, apsPersonId);
        BCS.Biz.Person person = dm.GetPerson();

        if (person == null)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.Columns.FullName, personName);
            person = dm.GetPerson();
            if (person != null)
                person.APSId = apsPersonId; // set the missing aps person id
        }
        return person.Id;
    }


    private string ValidateAgencyLicensedStates()
    {
        string[] clientStates = SelectedStates.Text.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        string[] agencyStates = AgencyLicensedStates.Text.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

        List<string> listClientStates = new List<string>(); listClientStates.AddRange(clientStates);
        List<string> listAgencyStates = new List<string>(); listAgencyStates.AddRange(agencyStates);

        foreach (string var in listClientStates)
        {
            if (!listAgencyStates.Contains(var))
            {
                return " Review state entered. Only these states : (" + string.Join(",", listAgencyStates.ToArray()) + ") are authorized for this agency";
            }
        }

        return string.Empty;
    }


    private void ValidateAndStoreStep(int stepIndex)
    {
        if (string.IsNullOrEmpty(Wizard1.WizardSteps[stepIndex].ID))
            return;
        Dictionary<int, bool> dict = ValidSteps;
        bool isValid = IsValidStep(stepIndex);
        if (ValidSteps.ContainsKey(stepIndex))
        {
            dict[stepIndex] = isValid;
        }
        else
        {

            dict.Add(stepIndex, isValid);
        }
        ValidSteps = dict;
    }

    private bool IsValidStep(int stepIndex)
    {
        string stepName = Wizard1.WizardSteps[stepIndex].ID.Replace("WizardStep", string.Empty);
        switch (stepName)
        {
            #region Dates
            case "Dates":
                {
                    // submission type is required
                    if ("0" == spSubmissionTypeIdLabel.Text || string.IsNullOrEmpty(spSubmissionTypeIdLabel.Text))
                        return false;

                    // if tbd checked nothing else is required on this step
                    if (TBDCheckBox.Checked)
                    {
                        return true;
                    }

                    // effective date is required
                    if (string.IsNullOrEmpty(spEffectiveDateIdLabel.Text))
                        return false;

                    DateTime eff = DateTime.MinValue;
                    DateTime exp = DateTime.MinValue;
                    #region invalid date for effectiveDate
                    try
                    {
                        eff = Convert.ToDateTime(spEffectiveDateIdLabel.Text);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                    #endregion

                    #region invalid date for expirationDate
                    try
                    {
                        exp = Convert.ToDateTime(spExpirationDateIdLabel.Text);
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                    #endregion


                    // effective date cannot be earlier than effective date
                    if (exp < eff)
                        return false;

                    // reached here in this step, its valid
                    return true;
                }
            #endregion
            #region Agency
            case "Agency":
                {
                    if ("0" == spAgencyIdLabel.Text || string.IsNullOrEmpty(spAgencyIdLabel.Text))
                        return false;

                    return true;
                    //break;
                }
            #endregion
            #region Agent
            case "Agent":
                {
                    if (Common.IsCWG() && IsProspect())
                        return true;
                    else if (DefaultValues.SupportsAgentUnspecifiedReasons(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                    {
                        if ("0" == spAgentIdLabel.Text)
                        {
                            if ("0" == spAgentUnspecifiedIdLabel.Text || "" == spAgentUnspecifiedIdLabel.Text)
                                return false;
                        }
                    }
                    return true;
                }
            #endregion
            #region LOB
            case "LOB":
                {
                    // lob is required
                    if ("0" == spLineOfBusinessIdLabel.Text || string.IsNullOrEmpty(spLineOfBusinessIdLabel.Text))
                        return false;
                    // underwriter is required
                    if (ConfigValues.RequiresUnderWriter(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                    {
                        if ("0" == spUnderwriterIdLabel.Text || string.IsNullOrEmpty(spUnderwriterIdLabel.Text))
                            return false;
                    }

                    return true;
                    //break;
                }
            #endregion
            #region Policy
            case "Policy":
                {
                    if (PolicyNumberIsRequired())
                    {
                        return !string.IsNullOrEmpty(spPolicyNumberIdLabel.Text);
                    }
                    return true;
                }
            #endregion
            #region Status
            case "Status":
                {
                    return true;
                }
            #endregion
            #region Codes
            case "Codes":
                {
                    List<IValidator> validatorControls = FindValidationControls(spCodeTypesContainerPanel);
                    foreach (IValidator var in validatorControls)
                    {
                        // we only have required field validators now, since we have javascript auto correct on dates
                        if (var is RequiredFieldValidator)
                        {
                            RequiredFieldValidator rfvVar = var as RequiredFieldValidator;
                            Control c = spCodeTypesContainerPanel.FindControl(rfvVar.ControlToValidate);

                            PropertyInfo pi = GetDefaultPropertyInfo(c);
                            object value = pi.GetValue(c, null);

                            if (value.ToString() == rfvVar.InitialValue)
                                return false;
                        }
                    }
                    return true;
                }
            #endregion
            #region Comments
            case "Comments":
                {
                    return true;
                }
            #endregion
            #region Client
            case "Client":
                {
                    bool namesAndAddressPresent = Common.GetSelectedItems(AddressList4Validation).Length > 0 && Common.GetSelectedItems(NameList4Validation).Length > 0;
                    bool insuredNamePresent = true;
                    if (ConfigValues.RequiresInsuredName(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                    {
                        insuredNamePresent = txInsuredName.Text != string.Empty;
                    }

                    return namesAndAddressPresent && insuredNamePresent;
                }
            #endregion
            #region LOBList
            case "LOBList":
                {
                    bool lOBValid = false;
                    bool codesValid = true;
                    if (DefaultValues.SupportsMultipleLineOfBusinesses(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                    {
                        int lobCount = Convert.ToInt32(EffectiveLOBCount.Text);
                        lOBValid = lobCount > 0;
                        if (spSubmissionTypeIdLabel.Text == "2")
                        {
                            lOBValid = true;
                            LOBSRequired.Enabled = false;
                        }
                    }

                    List<IValidator> validatorControls = FindValidationControls(spCodeTypesContainerInLOBGridPanel);
                    foreach (IValidator var in validatorControls)
                    {
                        // we only have required field validators now, since we have javascript auto correct on dates
                        if (var is RequiredFieldValidator)
                        {
                            RequiredFieldValidator rfvVar = var as RequiredFieldValidator;
                            Control c = spCodeTypesContainerInLOBGridPanel.FindControl(rfvVar.ControlToValidate);

                            PropertyInfo pi = GetDefaultPropertyInfo(c);
                            object value = pi.GetValue(c, null);

                            if (value.ToString() == rfvVar.InitialValue)
                                codesValid = false;
                        }
                    }
                    return lOBValid && codesValid;
                }
            #endregion
            #region OtherCarrier
            case "OtherCarrier":
                {
                    return true;
                }
            #endregion
            default:
                break;
        }

        return false;
    }

    private PropertyInfo GetDefaultPropertyInfo(Control c)
    {
        // TODO: can be reused, written the same thing couple of more times
        Type t = c.GetType();
        // Gets the attributes for the collection.
        System.ComponentModel.AttributeCollection attrs = System.ComponentModel.TypeDescriptor.GetAttributes(c);

        /* Prints the name of the default property by retrieving the 
         * DefaultPropertyAttribute from the AttributeCollection. */
        ControlValuePropertyAttribute myAttribute =
           (ControlValuePropertyAttribute)attrs[typeof(ControlValuePropertyAttribute)];

        System.Reflection.PropertyInfo pi = t.GetProperty(myAttribute.Name);
        return pi;
    }


    private void PopulateControls(int stepIndex)
    {
        #region Set Focus
        WizardConfigSteps steps = ConfigValues.GetWizardSteps(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        string controlIdToFocus = string.Empty;
        foreach (WizardConfigStep var in steps.Steps)
        {
            if (var.Index == stepIndex)
            {
                controlIdToFocus = var.FocusOn;
                break;
            }
        }
        if (!string.IsNullOrEmpty(controlIdToFocus))
        {
            Control controlToFocus = Common.FindControlRecursive(Wizard1.WizardSteps[stepIndex], controlIdToFocus);
            if (null != controlToFocus)
                ScriptManager1.SetFocus(controlToFocus);
        }
        #endregion

        if (string.IsNullOrEmpty(Wizard1.WizardSteps[stepIndex].ID))
            return;
        string stepName = Wizard1.WizardSteps[stepIndex].ID.Replace("WizardStep", string.Empty);
        switch (stepName)
        {
            #region Dates
            case "Dates":
                {
                    if (Reload(TypeLoaded))
                    {
                        SubmissionTypeDropDownList1.DataBind();
                        TypeLoaded.Text = Boolean.TrueString;
                        // by default a selection might be specified and only if not already set
                        if (spSubmissionTypeIdLabel.Text == "0")
                            SetSubmissionTypeValues();
                    }
                    Common.SetSafeSelectedValue(SubmissionTypeDropDownList1, spSubmissionTypeIdLabel.Text);
                    SetSafeIdDate(txEffectiveDate, spEffectiveDateIdLabel.Text);
                    SetSafeIdDate(txExpirationDate, spExpirationDateIdLabel.Text);
                    break;
                }
            #endregion
            #region Agency
            case "Agency":
                {
                    // start with settiing empty
                    AgencyInfo.Text = string.Empty;

                    if (!ShowGrid)
                    {
                        if (spAgencyIdLabel.Text != "0") // set an extra
                            AgencyDropDownList1.Extra = Convert.ToInt32(spAgencyIdLabel.Text);
                        AgencyDropDownList1.DataBind();

                        if (spAgencyIdLabel.Text != Common.GetSafeSelectedValue(AgencyDropDownList1).ToString())
                        {
                            DataSet ds = AgencyDropDownList1.GetCachedSource();
                            BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
                            dm.DataSet = ds;
                            BCS.Biz.AgencyCollection agencies = dm.GetAgencyCollectionFromDataSet();
                            BCS.Biz.Agency agency = agencies.FindById(spAgencyIdLabel.Text);
                            if (!string.IsNullOrEmpty(agency.ReferralAgencyNumber))
                            {
                                if (!agency.ReferralDate.IsNull)
                                {
                                    BCS.Biz.Agency refAgency = agencies.FindByAgencyNumber(agency.ReferralAgencyNumber);
                                    AgencyInfo.Text = "Auto-selected referred agency.";

                                    Common.SetSafeSelectedValue(AgencyDropDownList1, refAgency.Id);
                                }
                            }
                            AgencyComments.Text = agency.Comments;
                            AgencyComments.Visible = !string.IsNullOrEmpty(AgencyComments.Text);
                        }
                    }
                    if (ShowGrid)
                    {
                        //bool useRenewalCancelDate = false;
                        if (spAgencyIdLabel.Text != "0") // set an extra
                            odsAgencies.SelectParameters["extra"].DefaultValue = spAgencyIdLabel.Text;
                        odsAgencies.SelectParameters["date"].DefaultValue = GetEffectiveDate(DateTime.MaxValue).ToString();

                        int cmpnumber = Convert.ToInt32(Session["CompanyNumberSelected"]);
                        BCS.Biz.CompanyParameter companyParameter = ConfigValues.GetCompanyParameter(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                        if (companyParameter.UseAPSRenewalCancelDate && spSubmissionTypeLabel.Text == "Renewal")
                            odsAgencies.SelectParameters["useRenewalCancelDate"].DefaultValue = "true";
                        gridAgencies.DataSourceID = odsAgencies.ID;
                        gridAgencies.DataBind();

                        gv.DataSourceID = odsAgencies.ID;

                        var cwgProspectAgencyName = Globals.GetApplicationSetting("CWGdummyAgencyName");
                        if (IsProspect())
                            txFilterAgencyName.Text = cwgProspectAgencyName;
                        else if (txFilterAgencyName.Text == cwgProspectAgencyName)
                            txFilterAgencyName.Text = "";

                        gv.DataBind();
                    }

                    // try to restore
                    if (!ShowGrid)
                    {
                        Common.SetSafeSelectedValue(AgencyDropDownList1, spAgencyIdLabel.Text);
                        ScriptManager1.SetFocus(AgencyDropDownList1);
                    }
                    else
                    {
                        DateTime startDTFindSelectionFromDummyGrid = DateTime.Now;
                        #region dummy grid to find selected by datakey
                        string selectedAgencyId = spAgencyIdLabel.Text;
                        DataKeyArray dkArray = gv.DataKeys;

                        int rawRowIndex = -1;
                        for (int i = 0; i < dkArray.Count; i++)
                        {
                            if (dkArray[i].Value.ToString() == selectedAgencyId)
                            {
                                rawRowIndex = i;
                                break;
                            }
                        }
                        int pageIndex = (int)(rawRowIndex / gridAgencies.PageSize);
                        int rowIndex = rawRowIndex - (gridAgencies.PageSize * pageIndex);

                        gridAgencies.PageIndex = pageIndex;
                        gridAgencies.SelectedIndex = rowIndex;

                        gv.SelectedIndex = rawRowIndex;
                        #endregion
                        DateTime endDTFindSelectionFromDummyGrid = DateTime.Now;
                        TimeSpan tsFindSelectionFromDummyGrid = endDTFindSelectionFromDummyGrid.Subtract(startDTFindSelectionFromDummyGrid);
                        if (LogInfoMessages.Text == Boolean.TrueString)
                        {
                            LogCentral.Current.LogInfo(string.Format("tsFindSelectionFromDummyGrid {0}", tsFindSelectionFromDummyGrid.TotalSeconds));
                        }

                    }

                    break;
                }
            #endregion
            #region Agent
            case "Agent":
                {
                    #region Agent

                    // start with settiing empty
                    AgentInfo.Text = string.Empty;


                    if (Reload(AgentLoaded))
                    {
                        if (spAgentIdLabel.Text != "0")
                            AgentDropDownList1.Extra = Convert.ToInt32(spAgentIdLabel.Text);
                        AgentDropDownList1.DataBind();
                        AgentLoaded.Text = Boolean.TrueString;

                        // by default a selection might be specified and only if not already set
                        if (spAgentIdLabel.Text == "0")
                        {
                            int safeValue = Common.GetSafeSelectedValue(AgentDropDownList1);
                            spAgentIdLabel.Text = safeValue.ToString();
                            spAgentLabel.Text = safeValue == 0 ? hfDisplayText.Value : AgentDropDownList1.SelectedItem.Text;
                        }
                    }
                    // try to restore
                    Common.SetSafeSelectedValue(AgentDropDownList1, spAgentIdLabel.Text);

                    if (spAgentIdLabel.Text != Common.GetSafeSelectedValue(AgentDropDownList1).ToString())
                    {
                        AgentInfo.Text = "The agent previously selected is not valid as of the effective date.";
                    }
                    #endregion

                    if (Reload(AgentReasonLoaded))
                    {
                        cblAgentUnspecifiedReasons.DataBind();
                        AgentReasonLoaded.Text = Boolean.TrueString;
                    }
                    // restore reasons
                    string[] reasons = spAgentUnspecifiedIdLabel.Text.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    cblAgentUnspecifiedReasons.ClearSelection();
                    foreach (string reason in reasons)
                    {
                        ListItem li = cblAgentUnspecifiedReasons.Items.FindByValue(reason);
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }

                    if (Reload(ContactLoaded))
                    {
                        ContactDropDownList1.DataBind();
                        ContactLoaded.Text = Boolean.TrueString;
                    }
                    Common.SetSafeSelectedValue(ContactDropDownList1, spContactIdLabel.Text);

                    break;
                }
            #endregion
            #region LOB
            case "LOB":
                {
                    if (Reload(lobLoaded))
                    {
                        int agencyId = 0;
                        Int32.TryParse(spAgencyIdLabel.Text, out agencyId);
                        LineOfBusinessDropDownList1.DataBindByAgency(agencyId);
                        lobLoaded.Text = Boolean.TrueString;
                    }
                    Common.SetSafeSelectedValue(LineOfBusinessDropDownList1, spLineOfBusinessIdLabel.Text);
                    bool usesAPSServiceForAssignments =
                        DefaultValues.SupportsAPSServiceforAssignments(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

                    List<UnderwritersDropDownList.PersonAps> uwApsPersonCollection = new List<UnderwritersDropDownList.PersonAps>();
                    List<UnderwritersDropDownList.PersonAps> anApsPersonCollection = new List<UnderwritersDropDownList.PersonAps>();

                    DateTime submissionEffectiveDate = GetEffectiveDate(DateTime.MaxValue.Subtract(TimeSpan.FromSeconds(1)));
                    BCS.Biz.CompanyParameter companyParameter = ConfigValues.GetCompanyParameter(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                    if (Reload(underwriterLoaded))
                    {
                        int intAgencyId = Convert.ToInt32(spAgencyIdLabel.Text);
                        int intLOBId = Common.GetSafeSelectedValue(LineOfBusinessDropDownList1);
                        string uwNameNotInCollection = string.Empty;

                        if (spUnderwriterIdLabel.Text != "0")
                        {   // set extra
                            UnderwriterDropDownList1.Extra = Convert.ToInt64(spUnderwriterIdLabel.Text);
                            uwNameNotInCollection = spUnderwriterLabel.Text;
                        }

                        if (!usesAPSServiceForAssignments)
                        {
                            UnderwriterDropDownList1.DataBind(intAgencyId, intLOBId);
                        }
                        else
                        {
                            UnderwriterDropDownList1.DataBind(uwNameNotInCollection,
                                                              usesAPSServiceForAssignments,
                                                              intAgencyId,
                                                              submissionEffectiveDate,
                                                              out uwApsPersonCollection,
                                                              out anApsPersonCollection,
                                                              intLOBId);
                            if (uwApsPersonCollection != null && uwApsPersonCollection.Count > 0)
                                UwApsPersonCollection = uwApsPersonCollection;
                        }

                        // in case of default
                        if (spUnderwriterIdLabel.Text == "0" || ((companyParameter != null && companyParameter.ClearUWOnLOBChange == true) && Session["SubmissionId"] == null))
                        {
                            string tempAIC = Globals.GetApplicationSetting("AICNextGen").ToString();
                            if (Common.GetSafeIntFromSession(SessionKeys.CompanyId) == 3 && tempAIC == "True")
                            {
                                //Do nothing - need to parameterize this
                            }
                            else
                            {
                                long safeValue = Common.GetSafeSelectedValueLong(UnderwriterDropDownList1);
                                spUnderwriterIdLabel.Text = safeValue.ToString();
                                spUnderwriterLabel.Text = safeValue == 0 ? hfDisplayText.Value : UnderwriterDropDownList1.SelectedItem.Text;
                            }
                        }
                        // it means find the id in the existing list
                        underwriterLoaded.Text = Boolean.TrueString;
                    }
                    Common.SetSafeSelectedValue(UnderwriterDropDownList1, spUnderwriterIdLabel.Text);
                    //if (BCS.WebApp.Components.DefaultValues.SupportsFillPersonsInAssignments(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                    //{
                    //    //string uw = spUnderwriterLabel.Text.Replace("(unassigned)","").Trim();
                    //    //Common.SetSafeSelectedText(UnderwriterDropDownList1, uw);
                    //    //if (UnderwriterDropDownList1.SelectedIndex < 1)
                    //    //    Common.SetSafeSelectedText(UnderwriterDropDownList1, string.Format("{0} (unassigned)", uw));
                    //}
                    //else
                    //{
                    //    Common.SetSafeSelectedText(UnderwriterDropDownList1, spUnderwriterLabel.Text);
                    //}

                    if (Reload(analystLoaded))
                    {
                        int intAgencyId = Convert.ToInt32(spAgencyIdLabel.Text);
                        int intLOBId = Common.GetSafeSelectedValue(LineOfBusinessDropDownList1);
                        string anNameNotInCollection = string.Empty;

                        //bool loadFromSession = false;

                        if (spAnalystIdLabel.Text != "0")
                        {   // set extra
                            anNameNotInCollection = spAnalystLabel.Text;
                        }

                        if (!usesAPSServiceForAssignments)
                            AnalystDropDownList1.DataBind(intAgencyId, intLOBId);
                        else
                        {
                            //loadFromSession = true;                       

                            AnalystDropDownList1.DataBind(anNameNotInCollection,
                                                          usesAPSServiceForAssignments,
                                                          intAgencyId,
                                                          submissionEffectiveDate,
                                                          out uwApsPersonCollection,
                                                          out anApsPersonCollection,
                                                          intLOBId);

                            if (anApsPersonCollection != null && anApsPersonCollection.Count > 0)
                                AnApsPersonCollection = anApsPersonCollection;
                        }
                        // in case of default
                        if (spAnalystIdLabel.Text == "0" || ((companyParameter != null && companyParameter.ClearUWOnLOBChange == true) && Session["SubmissionId"] == null))
                        {
                            string tempAIC = Globals.GetApplicationSetting("AICNextGen").ToString();
                            if (Common.GetSafeIntFromSession(SessionKeys.CompanyId) == 3 && tempAIC == "True")
                            {
                                //Do nothing - need to parameterize this
                            }
                            else
                            {
                                long safeValue = Common.GetSafeSelectedValueLong(AnalystDropDownList1);
                                spAnalystIdLabel.Text = safeValue.ToString();
                                spAnalystLabel.Text = safeValue == 0 ? hfDisplayText.Value : AnalystDropDownList1.SelectedItem.Text;
                            }
                        }

                        analystLoaded.Text = Boolean.TrueString;
                    }
                    Common.SetSafeSelectedValue(AnalystDropDownList1, spAnalystIdLabel.Text);
                    //Common.SetSafeSelectedText(AnalystDropDownList1, spAnalystLabel.Text);

                    if (Reload(technicianLoaded))
                    {
                        int intAgencyId = Convert.ToInt32(spAgencyIdLabel.Text);
                        int intLOBId = Common.GetSafeSelectedValue(LineOfBusinessDropDownList1);
                        TechnicianDropDownList1.DataBind(intAgencyId, intLOBId);
                        // in case of default
                        if (spTechnicianIdLabel.Text == "0")
                        {
                            long safeValue = Common.GetSafeSelectedValueLong(TechnicianDropDownList1);
                            spTechnicianIdLabel.Text = safeValue.ToString();
                            spTechnicianLabel.Text = safeValue == 0 ? hfDisplayText.Value : TechnicianDropDownList1.SelectedItem.Text;
                        }

                        technicianLoaded.Text = Boolean.TrueString;
                    }
                    Common.SetSafeSelectedValue(TechnicianDropDownList1, spTechnicianIdLabel.Text);
                    break;
                }
            #endregion
            #region Policy
            case "Policy":
                {
                    if (Reload(policySystemLoaded))
                    {
                        PolicySystemDropDownList1.DataBind();
                        policySystemLoaded.Text = Boolean.TrueString;
                    }
                    Common.SetSafeSelectedValue(PolicySystemDropDownList1, spPolicySystemIdLabel.Text);

                    txPolicyNumber.Text = spPolicyNumberIdLabel.Text;
                    txEstimatedPremium.Text = spEstimatedPremiumIdLabel.Text;
                    break;
                }
            #endregion
            #region Status
            case "Status":
                {
                    if (Reload(statusLoaded))
                    {
                        SubmissionStatusDropDownList1.DataBind();
                        statusLoaded.Text = Boolean.TrueString;
                    }
                    Common.SetSafeSelectedValue(SubmissionStatusDropDownList1, spSubmissionStatusIdLabel.Text);
                    if (Reload(statusReasonLoaded))
                    {
                        if (spSubmissionStatusReasonIdLabel.Text != "0") // set extra
                            SubmissionStatusReasonDropDownList1.Extra = Convert.ToInt32(spSubmissionStatusReasonIdLabel.Text);

                        SubmissionStatusReasonDropDownList1.DataBind();
                        statusReasonLoaded.Text = Boolean.TrueString;
                    }
                    Common.SetSafeSelectedValue(SubmissionStatusReasonDropDownList1, spSubmissionStatusReasonIdLabel.Text);

                    txSubmissionStatusDate.Text = spSubmissionStatusDateIdLabel.Text;
                    if (txSubmissionStatusDate.Text.Length <= 0)
                    {
                        //txtStatusDate.Date = DateTime.Today;
                        txSubmissionStatusDate.Text = DateTime.Today.ToString(Common.DateFormat);
                    }

                    break;
                }
            #endregion
            #region Codes & Attributes
            case "Codes":
                {
                    if (Reload(CodeTypesLoaded))
                    {
                        LoadCodeTypesItems(CodeTypesContainerPanel);
                        CodeTypesLoaded.Text = Boolean.TrueString;
                    }
                    #region restore code/attribute entries
                    #region codes
                    List<DropDownList> ddlsNormal = FindDropDownListControls(CodeTypesContainerPanel); // get normal code types
                    foreach (DropDownList ddl in ddlsNormal) // set normal code types
                    {
                        string codeTypeName = ddl.ID.Replace("ddl", string.Empty);
                        string ddlID = codeTypeName + "Id";
                        Control idControl = Common.FindControlRecursive(spCodeTypesContainerPanel, ddlID);
                        string prevValue = (idControl as ITextControl).Text;

                        if (prevValue != "0")
                        {
                            Common.SetSafeSelectedValue(ddl, prevValue);

                            if (prevValue != Common.GetSafeSelectedValue(ddl).ToString())
                            {
                                Control valueControl = Common.FindControlRecursive(spCodeTypesContainerPanel, codeTypeName + "Value");
                                CodeTypesInfo.Text += string.Format("<br /><br />The '{0}' Code '{1}' previously selected is not valid as of effective date entered.",
                                    codeTypeName, (valueControl as ITextControl).Text);
                            }
                        }
                    }
                    #endregion

                    #region attributes
                    string[] attrIds = AttrIdsHidden.Value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    List<string> attrVals = new List<string>();

                    foreach (string attrId in attrIds)
                    {
                        Control sourceCtrl = Common.FindControlRecursive(spCodeTypesContainerPanel, attrId + "IdLabel");
                        Control targetCtrl = Common.FindControlRecursive(CodeTypesContainerPanel, attrId);

                        Type t = sourceCtrl.GetType();
                        // Gets the attributes for the collection.
                        System.ComponentModel.AttributeCollection attrs = System.ComponentModel.TypeDescriptor.GetAttributes(sourceCtrl);

                        /* Prints the name of the default property by retrieving the 
                            * DefaultPropertyAttribute from the AttributeCollection. */
                        ControlValuePropertyAttribute myAttribute =
                            (ControlValuePropertyAttribute)attrs[typeof(ControlValuePropertyAttribute)];

                        System.Reflection.PropertyInfo sourcePi = t.GetProperty(myAttribute.Name);
                        System.Reflection.PropertyInfo targetPi = targetCtrl.GetType().GetProperty(myAttribute.Name);
                        object value = sourcePi.GetValue(sourceCtrl, null);

                        targetPi.SetValue(targetCtrl, value, null);
                    }
                    string[] attributes = attrVals.ToArray();
                    #endregion
                    #endregion
                    break;
                }
            #endregion
            #region OtherCarrier
            case "OtherCarrier":
                {
                    if (Reload(OtherCarrierLoaded))
                    {
                        OtherCarrierDropDownList1.DataBind();
                        LoadCodeTypesItems(codeTypesContainerPanelOtherCarrier);
                        codeTypesLoadedOtherCarrier.Text = Boolean.TrueString;
                        //PriorOtherCarrierDropDownList.DataBind();
                    }
                    Common.SetSafeSelectedValue(OtherCarrierDropDownList1, spOtherCarrierIdLabel.Text);
                    txOtherCarrierPremium.Text = spOtherCarrierPremiumIdLabel.Text;

                    #region restore code/attribute entries
                    #region codes
                    List<DropDownList> ddlsNormal = FindDropDownListControls(codeTypesContainerPanelOtherCarrier); // get normal code types
                    foreach (DropDownList ddl in ddlsNormal) // set normal code types
                    {
                        string codeTypeName = ddl.ID.Replace("ddl", string.Empty);
                        string ddlID = codeTypeName + "Id";
                        Control idControl = Common.FindControlRecursive(spCodeTypesContainerPanelOtherCarrier, ddlID);
                        string prevValue = (idControl as ITextControl).Text;

                        if (prevValue != "0")
                        {
                            Common.SetSafeSelectedValue(ddl, prevValue);

                            if (prevValue != Common.GetSafeSelectedValue(ddl).ToString())
                            {
                                Control valueControl = Common.FindControlRecursive(spCodeTypesContainerPanelOtherCarrier, codeTypeName + "Value");
                                CodeTypesInfo.Text += string.Format("<br /><br />The '{0}' Code '{1}' previously selected is not valid as of effective date entered.",
                                    codeTypeName, (valueControl as ITextControl).Text);
                            }
                        }
                    }
                    #endregion

                    #region attributes
                    string[] attrIds = AttrIdsHiddenOtherCarrier.Value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    List<string> attrVals = new List<string>();

                    foreach (string attrId in attrIds)
                    {
                        Control sourceCtrl = Common.FindControlRecursive(spCodeTypesContainerPanelOtherCarrier, attrId + "IdLabel");
                        Control targetCtrl = Common.FindControlRecursive(codeTypesContainerPanelOtherCarrier, attrId);

                        Type t = sourceCtrl.GetType();
                        // Gets the attributes for the collection.
                        System.ComponentModel.AttributeCollection attrs = System.ComponentModel.TypeDescriptor.GetAttributes(sourceCtrl);

                        /* Prints the name of the default property by retrieving the 
                            * DefaultPropertyAttribute from the AttributeCollection. */
                        ControlValuePropertyAttribute myAttribute =
                            (ControlValuePropertyAttribute)attrs[typeof(ControlValuePropertyAttribute)];

                        System.Reflection.PropertyInfo sourcePi = t.GetProperty(myAttribute.Name);
                        System.Reflection.PropertyInfo targetPi = targetCtrl.GetType().GetProperty(myAttribute.Name);
                        object value = sourcePi.GetValue(sourceCtrl, null);

                        targetPi.SetValue(targetCtrl, value, null);

                    }
                    string[] attributes = attrVals.ToArray();
                    #endregion
                    #endregion
                    break;
                }
            #endregion
            #region Comments
            case "Comments":
                {
                    SubmissionComment[] comments = GetUpdateableComments();
                    // populate the newest one here
                    foreach (SubmissionComment var in comments)
                    {
                        if (var.Id < 0)
                        {
                            txComments.Text = Server.HtmlDecode(var.Comment);
                            CurrentCommentId.Text = var.Id.ToString();
                            break;
                        }
                    }
                    break;
                }
            #endregion
            #region LOBList
            case "LOBList":
                {
                    if (Reload(CodeTypesInLOBGridLoaded))
                    {
                        LoadCodeTypesItems(CodeTypesContainerInLOBGridPanel);
                        CodeTypesInLOBGridLoaded.Text = Boolean.TrueString;
                    }
                    #region restore code/attribute entries
                    List<DropDownList> ddls = FindDropDownListControls(CodeTypesContainerInLOBGridPanel); // get lob grid code types
                    foreach (DropDownList ddl in ddls) // set lob grid types
                    {
                        Control c = Common.FindControlRecursive(spCodeTypesContainerInLOBGridPanel, ddl.ID.Replace("ddl", string.Empty) + "Id");
                        Common.SetSafeSelectedValue(ddl, (c as ITextControl).Text);
                    }
                    #endregion
                    if (Reload(LOBListLoaded))
                    {
                        #region add mode lobs
                        if (DefaultValues.SupportsMultipleLineOfBusinesses(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                        {
                            SubmissionLineOfBusinessChild[] forAdd = new SubmissionLineOfBusinessChild[5];
                            for (int i = 1; i < forAdd.Length + 1; i++)
                            {
                                int index = i - 1;
                                forAdd[index] = new SubmissionLineOfBusinessChild();
                                if (DefaultStatus != -1)
                                    forAdd[index].SubmissionStatusId = DefaultStatus;
                                forAdd[index].Id = -1 * i;
                            }
                            LOBS = forAdd;
                        }
                        else
                        {
                            LOBS = new SubmissionLineOfBusinessChild[0];
                        }
                        #endregion
                    }
                    BindLobs();
                    LOBListLoaded.Text = Boolean.TrueString;
                    break;
                }
            #endregion
            #region Client
            case "Client":
                {
                    if (Reload(ClientLoaded))
                    {
                        string currentClientId = Session["ClientId"] as string;

                        DateTime startDTFromStep = DateTime.Now;
                        GetAndLoadClient(currentClientId);
                        DateTime endDTFromStep = DateTime.Now;
                        TimeSpan tsFromStep = endDTFromStep.Subtract(startDTFromStep);
                        if (LogInfoMessages.Text == Boolean.TrueString)
                        {
                            LogCentral.Current.LogInfo(string.Format("tsFromStep {0}", tsFromStep.TotalSeconds));
                        }

                        // default select if only one
                        DefaultSelect(gvClientAddresses, "addressCheckBox");
                        //DefaultSelect(NameList);
                        DefaultSelect(gvClientNames, "nameCheckBox");

                        // for edits we are already loading the client on the submission load itself, so this place should be fine for the enhancement request
                        // to auto-select all names on submission adds
                        if (ConfigValues.AutoSelectNamesOnSubmissionAdd(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
                        {
                            for (int i = 0; i < gvClientNames.Rows.Count; i++)
                            {
                                CheckBox cb = (CheckBox)gvClientNames.Rows[i].FindControl("nameCheckBox");
                                if (cb != null) cb.Checked = true;
                            }
                        }
                        PopulateData(stepIndex); // this is needed since there are auto-selects

                    }
                    else
                    {
                        txDBA.Text = Server.HtmlDecode(spDBAIdLabel.Text);
                        txInsuredName.Text = Server.HtmlDecode(spInsuredNameIdLabel.Text);
                    }
                    break;
                }
            #endregion
            default:
                break;
        }
    }
    private void PopulateData(int stepIndex)
    {
        if (string.IsNullOrEmpty(Wizard1.WizardSteps[stepIndex].ID))
            return;
        string stepName = Wizard1.WizardSteps[stepIndex].ID.Replace("WizardStep", string.Empty);
        switch (stepName)
        {
            #region Dates
            case "Dates":
                {
                    ListItem safeItem = Common.GetSafeSelectedItem(SubmissionTypeDropDownList1);
                    SetSafeDisplayText(spSubmissionTypeLabel, safeItem.Text);
                    SetSafeIdNumber(spSubmissionTypeIdLabel, safeItem.Value);

                    SetSafeDisplayText(spEffectiveDateLabel, txEffectiveDate.Text);
                    spEffectiveDateIdLabel.Text = spEffectiveDateLabel.Text == hfDisplayText.Value ? string.Empty : spEffectiveDateLabel.Text;
                    SetSafeDisplayText(spExpirationDateLabel, txExpirationDate.Text);
                    spExpirationDateIdLabel.Text = spExpirationDateLabel.Text == hfDisplayText.Value ? string.Empty : spExpirationDateLabel.Text;
                    break;
                }
            #endregion
            #region Agency
            case "Agency":
                {
                    if (!ShowGrid)
                    {
                        AgencyDropDownList1_SelectedIndexChanged(AgencyDropDownList1, EventArgs.Empty);
                    }
                    break;
                }
            #endregion
            #region Agent
            case "Agent":
                {
                    ListItem safeItemAgent = Common.GetSafeSelectedItem(AgentDropDownList1);
                    SetSafeDisplayText(spAgentLabel, safeItemAgent.Text);
                    SetSafeIdNumber(spAgentIdLabel, safeItemAgent.Value);

                    if (safeItemAgent.Value == "0") // no agent, set reasons
                    {
                        ListItem[] items = Common.GetSelectedItems(cblAgentUnspecifiedReasons);
                        spAgentReasons.DataSource = items;
                        spAgentReasons.DataBind();

                        string newIds = string.Empty;
                        for (int i = 0; i < items.Length; i++)
                        {
                            #region ids
                            newIds += items[i].Value;
                            if (i != items.Length - 1)
                                newIds += "|";
                            #endregion
                        }
                        spAgentUnspecifiedIdLabel.Text = newIds;
                    }
                    else // ignore reasons
                    {
                        spAgentReasons.DataSource = string.Empty;
                        spAgentReasons.DataBind();
                        cblAgentUnspecifiedReasons.ClearSelection();
                        spAgentUnspecifiedIdLabel.Text = string.Empty;
                    }


                    ListItem safeItemContact = Common.GetSafeSelectedItem(ContactDropDownList1);
                    SetSafeDisplayText(spContactLabel, safeItemContact.Text);
                    SetSafeIdNumber(spContactIdLabel, safeItemContact.Value);
                    break;
                }
            #endregion
            #region LOB
            case "LOB":
                {


                    ListItem safeItemLOB = Common.GetSafeSelectedItem(LineOfBusinessDropDownList1);
                    SetSafeDisplayText(spLineOfBusinessLabel, safeItemLOB.Text);
                    SetSafeIdNumber(spLineOfBusinessIdLabel, safeItemLOB.Value);

                    ListItem safeItemUnderwriter = Common.GetSafeSelectedItem(UnderwriterDropDownList1);
                    SetSafeDisplayText(spUnderwriterLabel, safeItemUnderwriter.Text);
                    SetSafeIdNumber(spUnderwriterIdLabel, safeItemUnderwriter.Value);


                    ListItem safeItemAnalyst = Common.GetSafeSelectedItem(AnalystDropDownList1);
                    SetSafeDisplayText(spAnalystLabel, safeItemAnalyst.Text);
                    SetSafeIdNumber(spAnalystIdLabel, safeItemAnalyst.Value);


                    ListItem safeItemtechnician = Common.GetSafeSelectedItem(TechnicianDropDownList1);
                    SetSafeDisplayText(spTechnicianLabel, safeItemtechnician.Text);
                    SetSafeIdNumber(spTechnicianIdLabel, safeItemtechnician.Value);
                    break;
                }
            #endregion
            #region Policy
            case "Policy":
                {
                    SetSafeDisplayText(spPolicyNumberLabel, txPolicyNumber.Text);
                    SetSafeIdText(spPolicyNumberIdLabel, spPolicyNumberLabel.Text);

                    ListItem safeItemPolicySystem = Common.GetSafeSelectedItem(PolicySystemDropDownList1);
                    SetSafeDisplayText(spPolicySystemLabel, string.IsNullOrEmpty(safeItemPolicySystem.Text) ?
                        ConfigValues.GetCurrentCompany(Common.GetSafeIntFromSession(SessionKeys.CompanyId)).CompanyInitials :
                        safeItemPolicySystem.Text);
                    SetSafeIdNumber(spPolicySystemIdLabel, safeItemPolicySystem.Value);

                    SetSafeDecimal(spEstimatedPremiumLabel, txEstimatedPremium.Text);
                    SetSafeDecimal(spEstimatedPremiumIdLabel, txEstimatedPremium.Text);

                    break;
                }
            #endregion
            #region Status
            case "Status":
                {
                    // status
                    ListItem safeItemStatus = Common.GetSafeSelectedItem(SubmissionStatusDropDownList1);
                    SetSafeDisplayText(spSubmissionStatusLabel, safeItemStatus.Text);
                    SetSafeIdNumber(spSubmissionStatusIdLabel, safeItemStatus.Value);

                    // status reasons
                    ListItem safeItemReason = Common.GetSafeSelectedItem(SubmissionStatusReasonDropDownList1);
                    SetSafeDisplayText(spSubmissionStatusReasonLabel, safeItemReason.Text);
                    SetSafeIdNumber(spSubmissionStatusReasonIdLabel, safeItemReason.Value);

                    // status date
                    SetSafeDate(spSubmissionStatusDateLabel, txSubmissionStatusDate.Text, hfDisplayText.Value);
                    SetSafeDate(spSubmissionStatusDateIdLabel, txSubmissionStatusDate.Text, DateTime.Today.ToString(Common.DateFormat));
                    SetSafeDate(spSubmissionStatusIdBox, txSubmissionStatusDate.Text, DateTime.Today.ToString(Common.DateFormat));

                    // status other reason
                    SetSafeDisplayText(spSubmissionStatusReasonOtherLabel, txSubmissionStatusReasonOther.Text);
                    SetSafeIdText(spSubmissionStatusReasonOtherIdLabel, txSubmissionStatusReasonOther.Text);

                    break;
                }
            #endregion
            #region Codes & Attributes
            case "Codes":
                {
                    PopulateCodes(CodeTypesContainerPanel, spCodeTypesContainerPanel);

                    #region Attributes
                    // under the assumption that, only focusable controls are input-able controls
                    List<Control> attrControls = new List<Control>();
                    string[] attrIds = AttrIdsHidden.Value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (string attrId in attrIds)
                    {
                        Control c = Common.FindControlRecursive(CodeTypesContainerPanel, attrId);
                        attrControls.Add(c);
                    }
                    foreach (Control var in attrControls)
                    {
                        #region 4 hidden id control
                        {
                            Control c = Common.FindControlRecursive(spCodeTypesContainerPanel, string.Concat(var.ID, "IdLabel"));
                            if (c != null)
                            {
                                if (var is CheckBox)
                                {
                                    CheckBox displayCheckBox = c as CheckBox;
                                    displayCheckBox.Checked = (var as CheckBox).Checked;
                                }
                                if (var is ITextControl)
                                {
                                    ITextControl itBox = c as ITextControl;
                                    itBox.Text = (var as ITextControl).Text;
                                }
                                // other attributes not handled, since they will fall under above or have their unique handling, can only know when added
                            }
                        }
                        #endregion
                        #region 4 display control
                        {
                            Control c = Common.FindControlRecursive(spCodeTypesContainerPanel, string.Concat(var.ID, "Label"));
                            if (c != null)
                            {
                                if (var is CheckBox)
                                {
                                    CheckBox displayCheckBox = c as CheckBox;
                                    displayCheckBox.Checked = (var as CheckBox).Checked;
                                }
                                if (var is ITextControl)
                                {
                                    ITextControl itBox = c as ITextControl;
                                    itBox.Text = (var as ITextControl).Text;
                                    if (string.IsNullOrEmpty(itBox.Text))
                                        itBox.Text = hfDisplayText.Value;
                                }
                                // other attributes not handled, since they will fall under above or have their unique handling, can only know when added
                            }
                        }
                        #endregion
                    }
                    #endregion
                    break;
                }
            #endregion
            #region Client
            case "Client":
                {
                    //gridSelectedNames.DataSource = gvClientNames.DataSource;
                    //gridSelectedNames.DataBind();

                    /*foreach (ListItem var in NameList.Items)
                    {
                        NameList4Validation.Items.FindByValue(var.Value).Selected = var.Selected;
                    }*/

                    for (int i = 0; i < gvClientNames.Rows.Count; i++)
                    {
                        CheckBox cb = (CheckBox)gvClientNames.Rows[i].FindControl("nameCheckBox");
                        //CheckBox cb2 = (CheckBox)gridSelectedNames.Rows[i].FindControl("CheckBoxName1");

                        //if (cb != null && cb2 != null)
                        //    cb2.Checked = cb.Checked;

                        NameList4Validation.Items.FindByValue(gvClientNames.DataKeys[i].Value.ToString()).Selected = cb.Checked;
                    }

                    //gridSelectedAddresses.DataSource = AddressList.Items;
                    //gridSelectedAddresses.DataBind();

                    //foreach (ListItem var in AddressList.Items)
                    //{
                    //    AddressList4Validation.Items.FindByValue(var.Value).Selected = var.Selected;
                    //    if (null != AddressList4StateWarning.Items.FindByValue(var.Value))
                    //    {
                    //        AddressList4StateWarning.Items.FindByValue(var.Value).Selected = var.Selected;
                    //    }
                    //}

                    for (int i = 0; i < gvClientAddresses.Rows.Count; i++)
                    {
                        string keyValue = gvClientAddresses.DataKeys[i].Value.ToString();
                        CheckBox cb = (CheckBox)gvClientAddresses.Rows[i].FindControl("addressCheckBox");

                        AddressList4Validation.Items.FindByValue(keyValue).Selected = cb.Checked;
                        if (null != AddressList4StateWarning.Items.FindByValue(keyValue))
                        {
                            AddressList4StateWarning.Items.FindByValue(keyValue).Selected = cb.Checked;
                        }
                    }


                    SetSafeDisplayText(spDBALabel, txDBA.Text);
                    SetSafeIdText(spDBAIdLabel, spDBALabel.Text);

                    SetSafeDisplayText(spInsuredNameLabel, txInsuredName.Text);
                    SetSafeIdText(spInsuredNameIdLabel, spInsuredNameLabel.Text);
                    break;
                }
            #endregion
            #region LOBList
            case "LOBList":
                {
                    #region lobs
                    UpdateLOBSToViewState();
                    BindLobs();
                    #endregion
                    PopulateCodes(CodeTypesContainerInLOBGridPanel, spCodeTypesContainerInLOBGridPanel);
                    break;
                }
            #endregion
            #region Comments
            case "Comments":
                {
                    if (string.IsNullOrEmpty(CurrentCommentId.Text)) // add comment only if raw new
                        AddComment();
                    else
                    {
                        SubmissionComment[] comments = GetUpdateableComments();
                        // update the one edited
                        foreach (SubmissionComment var in comments)
                        {
                            if (var.Id.ToString() == CurrentCommentId.Text)
                            {
                                var.Comment = txComments.Text;
                                if (string.IsNullOrEmpty(var.Comment))
                                    var.Deleted = true;
                                Comments = comments;
                                BindComments();
                                break;
                            }
                        }
                    }

                    break;
                }
            #endregion
            #region OtherCarrier
            case "OtherCarrier":
                {
                    ListItem safeItem = Common.GetSafeSelectedItem(OtherCarrierDropDownList1);
                    SetSafeDisplayText(spOtherCarrierLabel, safeItem.Text);
                    SetSafeIdNumber(spOtherCarrierIdLabel, safeItem.Value);

                    SetSafeDecimal(spOtherCarrierPremiumLabel, txOtherCarrierPremium.Text);
                    SetSafeDecimal(spOtherCarrierPremiumIdLabel, txOtherCarrierPremium.Text);

                    PopulateCodes(codeTypesContainerPanelOtherCarrier, spCodeTypesContainerPanelOtherCarrier);
                    #region Attributes
                    // under the assumption that, only focusable controls are input-able controls
                    List<Control> attrControls = new List<Control>();
                    string[] attrIds = AttrIdsHiddenOtherCarrier.Value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (string attrId in attrIds)
                    {
                        Control c = Common.FindControlRecursive(codeTypesContainerPanelOtherCarrier, attrId);
                        attrControls.Add(c);
                    }
                    foreach (Control var in attrControls)
                    {
                        #region 4 hidden id control
                        {
                            Control c = Common.FindControlRecursive(spCodeTypesContainerPanelOtherCarrier, string.Concat(var.ID, "IdLabel"));
                            if (c != null)
                            {
                                if (var is CheckBox)
                                {
                                    CheckBox displayCheckBox = c as CheckBox;
                                    displayCheckBox.Checked = (var as CheckBox).Checked;
                                }
                                if (var is ITextControl)
                                {
                                    ITextControl itBox = c as ITextControl;
                                    itBox.Text = (var as ITextControl).Text;
                                }
                                // other attributes not handled, since they will fall under above or have their unique handling, can only know when added
                            }
                        }
                        #endregion
                        #region 4 display control
                        {
                            Control c = Common.FindControlRecursive(spCodeTypesContainerPanelOtherCarrier, string.Concat(var.ID, "Label"));
                            if (c != null)
                            {
                                if (var is CheckBox)
                                {
                                    CheckBox displayCheckBox = c as CheckBox;
                                    displayCheckBox.Checked = (var as CheckBox).Checked;
                                }
                                if (var is ITextControl)
                                {
                                    ITextControl itBox = c as ITextControl;
                                    itBox.Text = (var as ITextControl).Text;
                                    if (string.IsNullOrEmpty(itBox.Text))
                                        itBox.Text = hfDisplayText.Value;
                                }
                                // other attributes not handled, since they will fall under above or have their unique handling, can only know when added
                            }
                        }
                        #endregion
                    }
                    #endregion
                    break;
                }
            #endregion
            default:
                break;
        }
    }


    private void BindCodes(Panel panel, SubmissionCodeType[] sCodes)
    {
        if (sCodes == null)
            return;
        foreach (SubmissionCodeType type in sCodes)
        {
            ITextControl txCtrl = Common.FindControlRecursive(panel, type.CodeTypeId.ToString() + "CodeValue") as ITextControl;
            ITextControl idCtrl = Common.FindControlRecursive(panel, type.CodeTypeId.ToString() + "CodeId") as ITextControl;

            if (null != txCtrl)
            {
                #region process display text, sometimes code or description may be absent
                string itemText = string.Format("{0}-{1}", type.CodeValue, type.CodeDescription);
                itemText = itemText.Trim();
                string[] itemTextArray = itemText.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                itemText = string.Join(" - ", itemTextArray);
                #endregion

                txCtrl.Text = itemText;
            }
            if (null != idCtrl)
                idCtrl.Text = type.CodeId.ToString();
        }
    }
    private void BindAttributes(Panel panel, SubmissionAttribute[] sAttributes)
    {
        if (sAttributes == null)
            return;
        foreach (SubmissionAttribute var in sAttributes)
        {
            Control idCtrl = Common.FindControlRecursive(panel, string.Concat(var.CompanyNumberAttributeId, "IdLabel"));
            Control lblCtrl = Common.FindControlRecursive(panel, string.Concat(var.CompanyNumberAttributeId, "Label"));

            string controlType = var.DataType.ToLower();
            switch (controlType)
            {
                case "date":
                    {
                        if (lblCtrl is ITextControl)
                        {
                            ITextControl iTC = lblCtrl as ITextControl;
                            iTC.Text = string.Format("{0:MM/dd/yyyy}", var.AttributeValue);
                            if (string.IsNullOrEmpty(iTC.Text))
                                iTC.Text = hfDisplayText.Value;
                        }
                        if (idCtrl is ITextControl)
                        {
                            ITextControl iTC = idCtrl as ITextControl;
                            iTC.Text = string.Format("{0:MM/dd/yyyy}", var.AttributeValue);
                        }
                        break;
                    }
                case "money":
                    {
                        if (lblCtrl is ITextControl)
                        {
                            ITextControl iTC = lblCtrl as ITextControl;
                            iTC.Text = string.Format("{0:c}", var.AttributeValue);
                            if (string.IsNullOrEmpty(iTC.Text))
                                iTC.Text = hfDisplayText.Value;
                        }
                        if (idCtrl is ITextControl)
                        {
                            ITextControl iTC = idCtrl as ITextControl;
                            iTC.Text = string.Format("{0:c}", var.AttributeValue);
                        }
                        break;
                    }
                case "bit":
                    {
                        if (idCtrl is CheckBox)
                        {
                            CheckBox displayCheckBox = idCtrl as CheckBox;
                            bool b = false;
                            bool result = Boolean.TryParse(var.AttributeValue, out b);
                            displayCheckBox.Checked = b;
                        }
                        break;
                    }
                case "text":
                    {
                        if (lblCtrl is ITextControl)
                        {
                            ITextControl iTC = lblCtrl as ITextControl;
                            iTC.Text = var.AttributeValue;
                            if (string.IsNullOrEmpty(iTC.Text))
                                iTC.Text = hfDisplayText.Value;
                        }
                        if (idCtrl is ITextControl)
                        {
                            ITextControl iTC = idCtrl as ITextControl;
                            iTC.Text = var.AttributeValue;
                        }
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            // may be we can add warnings if invalid data types
        }
    }
    private void PopulateCodes(Panel fromPanel, Panel toPanel)
    {
        List<DropDownList> ddls = FindDropDownListControls(fromPanel);
        foreach (DropDownList ddl in ddls)
        {
            int safeValue = Common.GetSafeSelectedValue(ddl);

            string idPrefix = ddl.ID.Replace("ddl", string.Empty);
            Label label = Common.FindControlRecursive(toPanel, string.Concat(idPrefix, "Value")) as Label;
            label.Text = safeValue == 0 ? hfDisplayText.Value : ddl.SelectedItem.Text;

            TextBox textBox = Common.FindControlRecursive(toPanel, string.Concat(idPrefix, "Id")) as TextBox;
            textBox.Text = safeValue == 0 ? "0" : ddl.SelectedItem.Value;
        }
    }

    private void LoadClient(BTS.ClientCore.Wrapper.Structures.Info.ClientInfo client)
    {
        //NameList.Items.Clear();
        NameList4Validation.Items.Clear();
        //AddressList.Items.Clear();
        AddressList4Validation.Items.Clear();
        AddressList4StateWarning.Items.Clear();
        lbDbaNames.Items.Clear();
        lbInsuredNames.Items.Clear();

        DataTable dt = new DataTable();
        dt.Columns.Add("Sequence", typeof(string));
        dt.Columns.Add("Selected", typeof(bool));
        dt.Columns.Add("Text", typeof(string));
        dt.Columns.Add("Type", typeof(string));

        DataTable dta = new DataTable();
        dta.Columns.Add("AddressId", typeof(int));
        dta.Columns.Add("Selected", typeof(bool));
        dta.Columns.Add("Text", typeof(string));
        dta.Columns.Add("Type", typeof(string));

        string submissionInsured = string.Empty;
        BCS.Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
        dm.QueryCriteria.And(BCS.Biz.JoinPath.Submission.Columns.Id, Common.GetSafeIntFromSession("SubmissionId"));

        BCS.Biz.Submission sub = dm.GetSubmission();

        if (sub != null)
            submissionInsured = sub.InsuredName;

        //if (client.Client.ClientId == clientid) // TODO: why is this written
        {
            #region Wizard Step
            Session["Client"] = client; // TODO: why is this written?
            foreach (Structures.Info.Name name in client.Client.Names.OrderByDescending(n => n.PrimaryNameFlag).ThenBy(n => n.SequenceNumber))
            {
                StringBuilder sb = new StringBuilder();
                if (name.NameBusiness != null && name.NameBusiness.Length > 0)
                    sb.Append(name.NameBusiness);
                else
                    sb.AppendFormat("{0} {1} {2}", name.NameFirst, name.NameMiddle, name.NameLast);

                //if (name.ClearanceNameType == "DBA")
                //{
                //    lbDbaNames.Items.Add(new ListItem(sb.ToString(), sb.ToString()));
                //}
                //else
                //{
                //    lbInsuredNames.Items.Add(new ListItem(sb.ToString(), sb.ToString()));
                //}


                if (name.ClearanceNameType == "DBA" || string.IsNullOrEmpty(name.ClearanceNameType))
                {
                    lbDbaNames.Items.Add(new ListItem(sb.ToString(), sb.ToString()));
                }

                if (name.ClearanceNameType == "Insured" || string.IsNullOrEmpty(name.ClearanceNameType))
                {
                    if (!string.IsNullOrEmpty(submissionInsured) && sb.ToString() == submissionInsured)
                        txInsuredName.Text = sb.ToString();
                    lbInsuredNames.Items.Add(new ListItem(sb.ToString(), sb.ToString()));
                }

                string primary = (name.PrimaryNameFlag == "Y") ? " (Primary)" : "";
                dt.Rows.Add(name.SequenceNumber, false, sb.ToString() + primary, name.ClearanceNameType);

                if (!string.IsNullOrEmpty(name.ClearanceNameType))
                {
                    sb.AppendFormat(" ({0})", name.ClearanceNameType);
                }
                else
                {
                    sb.AppendFormat(" ({0})", "NA");
                }



                //NameList.Items.Add(new ListItem(Server.HtmlEncode(sb.ToString()), name.SequenceNumber));
                NameList4Validation.Items.Add(new ListItem(Server.HtmlEncode(sb.ToString()), name.SequenceNumber));
            }
            lbDbaNames.Enabled = lbDbaNames.Items.Count != 0;
            lbInsuredNames.Enabled = lbInsuredNames.Items.Count != 0;
            if (DefaultValues.DesiresDBAAutoSelectWhenOnlyOne(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
            {
                if (lbDbaNames.Items.Count == 1)
                    txDBA.Text = lbDbaNames.Items[0].Text;
            }
            if (DefaultValues.DesiresInsuredAutoSelectWhenOnlyOne(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
            {
                if (lbInsuredNames.Items.Count == 1)
                    txInsuredName.Text = lbInsuredNames.Items[0].Text;
            }
            if (lbDbaNames.Items.Count == 0)
                lbDbaNames.Items.Add(new ListItem("No DBAs exist."));
            if (lbInsuredNames.Items.Count == 0)
                lbInsuredNames.Items.Add(new ListItem("No Insureds exist."));


            if (client.Addresses != null)
            {


                foreach (Structures.Info.Address addr in client.Addresses)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat("{0} {1} {2} {3} {4} {5}", addr.HouseNumber, addr.Address1, addr.Address2,
                        addr.City, addr.StateProvinceId, addr.PostalCode);
                    /*if (!string.IsNullOrEmpty(addr.ClearanceAddressType))
                    {
                        sb.AppendFormat(" ({0})", addr.ClearanceAddressType);
                    }
                    else
                    {
                        sb.AppendFormat(" ({0})", "NA");
                    }*/

                    dta.Rows.Add(addr.AddressId, false, sb.ToString(), addr.ClearanceAddressType);

                    //AddressList.Items.Add(new ListItem(Server.HtmlEncode(sb.ToString()), addr.AddressId));
                    AddressList4Validation.Items.Add(new ListItem(Server.HtmlEncode(sb.ToString()), addr.AddressId));

                    if (DefaultValues.EnforceAgencyLicensedState(Common.GetSafeIntFromSession("CompanySelected")))
                        AddressList4StateWarning.Items.Add(new ListItem(addr.StateProvinceId, addr.AddressId));
                }
            }

            ClientIdLabel.Text = client.Client.ClientId;
            PortfolioIdLabel.Text = client.Client.PortfolioId;
            #endregion

            #region Snapshot

            gvClientAddresses.DataSource = dta;
            gvClientAddresses.DataBind();
            ViewState["ClientAddresses"] = dta;

            gridSelectedAddresses.DataSource = gvClientAddresses.DataSource;
            gridSelectedAddresses.DataBind();

            gvClientNames.DataSource = dt;
            gvClientNames.DataBind();
            ViewState["ClientNames"] = dt;

            gridSelectedNames.DataSource = gvClientNames.DataSource;
            gridSelectedNames.DataBind();

            spClientIdLabel.Text = client.Client.ClientId;
            hdrClientIdLabelAdd.Text = client.Client.ClientId;
            hdrClientIdLabelEdt.Text = client.Client.ClientId;
            spPortfolioIdLabel.Text = client.Client.PortfolioId;
            #endregion
        }
        ClientLoaded.Text = Boolean.TrueString;

    }

    private BTS.ClientCore.Wrapper.Structures.Info.ClientInfo GetAndLoadClient(string clientid)
    {
        string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
        ClientProxy.Client clientproxy = Common.GetClientProxy();

        string clientxml = clientproxy.GetClientById(companyNumber, clientid);

        try
        {
            BCS.Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                (BCS.Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                clientxml, typeof(BCS.Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

            if (string.IsNullOrEmpty(clearanceclient.ClientCoreClient.Client.ClientId))
            {
                Common.SetStatus(messagePlaceHolder, "The client associated with the submission either does not exist or is inaccessible.");
                return new BTS.ClientCore.Wrapper.Structures.Info.ClientInfo();
            }

            LoadClient(clearanceclient.ClientCoreClient);
            return clearanceclient.ClientCoreClient;
        }
        catch (Exception ex)
        {
            LogCentral.Current.LogWarn(string.Format("The client associated with the submission either does not exist or is inaccessible. Id = {0} CompanyNumberId = {1}", clientid, Session["CompanyNumberSelected"]), ex);

            Common.SetStatus(pnlClientInfo,
                "The client associated with the submission either does not exist or is inaccessible.");
            return new BTS.ClientCore.Wrapper.Structures.Info.ClientInfo();
        }
    }

    private bool Reload(ITextControl itControl)
    {
        return !Convert.ToBoolean(itControl.Text);
    }

    private DateTime GetEffectiveDate(DateTime defaultValue)
    {
        if (TBDCheckBox.Checked && ConfigValues.GetCompanyParameter(Common.GetSafeIntFromSession(SessionKeys.CompanyId)).SubmissionStatusDateTBD.IsNull)
        {
            return Common.GetSafeDateTime(SubmissionDateLabel.Text, defaultValue);
        }
        else
        {
            return Common.GetSafeDateTime(spEffectiveDateIdLabel.Text, defaultValue);
        }
    }


    private void SetClientStates()
    {
        string[] selectedValues = Common.GetSelectedValues(gvClientAddresses, "addressCheckBox");

        List<string> listSelectedStates = new List<string>();
        foreach (string selectedValue in selectedValues)
        {
            string selectedState = AddressList4StateWarning.Items.FindByValue(selectedValue).Text;
            if (!listSelectedStates.Contains(selectedState))
            {
                listSelectedStates.Add(selectedState);
            }
        }
        SelectedStates.Text = string.Join("|", listSelectedStates.ToArray());
    }

    private void LoadControlsForCodeTypesAndAttributes()
    {
        // this would indicates they are already loaded, so no need to load again
        if (CodeTypesContainerPanel.Controls.Count > 1)
            return;
        int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        bool clientSpecific = false;



        BCS.Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
        BCS.Biz.CompanyNumberCodeTypeCollection types = ConfigValues.GetCompanyCodeTypes(companyNumberId, clientSpecific, false, false);

        BCS.Biz.CompanyNumberCodeTypeCollection typesInLOBGrid = types.FilterByVisibleInLOBGridSection(true);
        BCS.Biz.CompanyNumberCodeTypeCollection typesInNormal = types.FilterByVisibleInLOBGridSection(false).FilterByOtherCarrierSpecific(false);

        BCS.Biz.CompanyNumberCodeTypeCollection typesInOtherCarrierGrid = types.FilterByOtherCarrierSpecific(true);

        BCS.Biz.CompanyNumberAttributeCollection attrs = ConfigValues.GetCompanyAttributes(companyNumberId, clientSpecific);
        BCS.Biz.CompanyNumberAttributeCollection attrsOtherCarrier = attrs.FilterByOtherCarrierSpecific(true);
        attrs = attrs.FilterByOtherCarrierSpecific(false);

        yLoadControlsForCodeTypesAndAttributes(CodeTypesContainerPanel, spCodeTypesContainerPanel, typesInNormal, attrs, CodeTypes.Normal);
        yLoadControlsForCodeTypesAndAttributes(CodeTypesContainerInLOBGridPanel, spCodeTypesContainerInLOBGridPanel, typesInLOBGrid, null, CodeTypes.Normal);
        yLoadControlsForCodeTypesAndAttributes(codeTypesContainerPanelOtherCarrier, spCodeTypesContainerPanelOtherCarrier, typesInOtherCarrierGrid, attrsOtherCarrier, CodeTypes.OtherCarrierSpecific);
    }

    private enum CodeTypes : int
    {
        Normal,
        OtherCarrierSpecific,
        LineOfBusinessSpecific,
        ClassCodeSpecific
    }

    private void yLoadControlsForCodeTypesAndAttributes(Panel baseInputPanel, Panel baseDisplayPanel,
       BCS.Biz.CompanyNumberCodeTypeCollection ipTypes, BCS.Biz.CompanyNumberAttributeCollection ipAttrs, CodeTypes codeType)
    {
        baseInputPanel.Controls.Clear();
        baseDisplayPanel.Controls.Clear();

        int totalCount = 0;
        if (null != ipTypes)
            totalCount += ipTypes.Count;
        if (null != ipAttrs)
            totalCount += ipAttrs.Count;

        List<int> listDisplayOrder = new List<int>(totalCount);
        if (null != ipTypes)
        {
            foreach (BCS.Biz.CompanyNumberCodeType var in ipTypes)
            {
                if (!listDisplayOrder.Contains(var.DisplayOrder.Value))
                {
                    listDisplayOrder.Add(var.DisplayOrder.Value);
                }
            }
        }
        if (null != ipAttrs)
        {
            foreach (BCS.Biz.CompanyNumberAttribute var in ipAttrs)
            {
                if (!listDisplayOrder.Contains(var.DisplayOrder.Value))
                {
                    listDisplayOrder.Add(var.DisplayOrder.Value);
                }
            }
        }
        listDisplayOrder.Sort();

        int halfCount = totalCount / 2;
        halfCount = (totalCount % 2 == 0) ? halfCount : halfCount + 1;

        HtmlTable mainInputTable = new HtmlTable();
        mainInputTable.Style.Add(HtmlTextWriterStyle.Width, "100%");
        HtmlTableRow mainInputRow = new HtmlTableRow();
        HtmlTableCell mainInputCell = new HtmlTableCell();
        mainInputCell.Style.Add(HtmlTextWriterStyle.VerticalAlign, "top");

        HtmlTable mainSnapsTable = new HtmlTable();
        mainSnapsTable.Style.Add(HtmlTextWriterStyle.Width, "100%");
        HtmlTableRow mainSnapsRow = new HtmlTableRow();
        HtmlTableCell mainSnapsCell = new HtmlTableCell();
        mainSnapsCell.Style.Add(HtmlTextWriterStyle.VerticalAlign, "top");
        mainSnapsCell.Style.Add(HtmlTextWriterStyle.Width, "50%");

        HtmlTable inputTable = new HtmlTable();
        inputTable.Style.Add("margin", "2px;"); inputTable.Style.Add("padding", "2px;");
        HtmlTable snapsTable = new HtmlTable();
        snapsTable.Style.Add("margin", "2px;"); snapsTable.Style.Add("padding", "2px;");

        int countingRows = 0;
        foreach (int index in listDisplayOrder)
        {
            #region code types
            if (null != ipTypes)
            {
                BCS.Biz.CompanyNumberCodeTypeCollection types = ipTypes.FilterByDisplayOrder(index.ToString());
                if (types != null)
                {
                    if (types.Count > 1) // can do without this condition too, but lets just keep it since anyhow.......
                    {
                        types = types.SortByCodeName(OrmLib.SortDirection.Ascending); // this is needed sometimes the filter might not retain the previous sort order
                        // causing issues with viewstate being changed
                    }
                    for (int i = 0; i < types.Count; i++)
                    {
                        BCS.Biz.CompanyNumberCodeType type = types[i];

                        HtmlTableRow inputRow = new HtmlTableRow();
                        HtmlTableRow snapsRow = new HtmlTableRow();

                        HtmlTableCell inputCell1 = new HtmlTableCell(); inputCell1.Align = "right";
                        HtmlTableCell snapsCell1 = new HtmlTableCell(); snapsCell1.Align = "right";

                        HtmlTableCell inputCell2 = new HtmlTableCell();
                        HtmlTableCell snapsCell2 = new HtmlTableCell();

                        #region Display Panel
                        Panel displayPanel = new Panel();
                        #region Display Label
                        Label displayNameLabel = new Label();
                        displayNameLabel.ID = type.CodeName.Replace(" ", "");
                        displayNameLabel.Text = type.CodeName + ":";

                        Common.AddControl(snapsCell1, displayNameLabel);

                        Label displayValueLabel = new Label();
                        displayValueLabel.ID = type.Id.ToString() + "CodeValue";
                        displayValueLabel.Text = hfDisplayText.Value;
                        Common.AddControl(snapsCell2, displayValueLabel);

                        #endregion
                        #region Id Label
                        TextBox idControl = new TextBox();
                        idControl.ID = type.Id.ToString() + "CodeId";
                        idControl.CssClass = "displayedNone";
                        idControl.Text = "0";
                        Common.AddControl(snapsCell2, idControl);
                        #endregion
                        #endregion

                        #region Input Panel
                        Panel inputPanel = new Panel();

                        Label label1 = new Label();
                        label1.ID = string.Concat("CT", type.Id.ToString());
                        label1.Text = type.CodeName;
                        // add controls to cells
                        Common.AddControl(inputCell1, label1);

                        inputPanel.ID = type.Id.ToString() + "CodeInputPanel";
                        DropDownList inputControl = new DropDownList();
                        inputControl.ID = type.Id.ToString() + "Codeddl";
                        inputControl.CssClass = "cbo";
                        inputControl.Style.Add(HtmlTextWriterStyle.MarginTop, "6px");

                        ListSearchExtender lse = new ListSearchExtender();
                        lse.ID = Guid.NewGuid().ToString().Replace("-", "");
                        lse.TargetControlID = inputControl.ID;
                        #endregion

                        #region add validation/indication controls
                        if (type.Required)
                        {
                            RequiredFieldValidator rfv = new RequiredFieldValidator();
                            Common.AddControl(snapsCell2, rfv);
                            rfv.InitialValue = "0";
                            rfv.Display = ValidatorDisplay.None;
                            rfv.ControlToValidate = idControl.ID;
                            rfv.SetFocusOnError = false;
                            rfv.ErrorMessage = type.CodeName + " is required";

                            Label reqLabel = new Label();
                            reqLabel.CssClass = "errorText";
                            reqLabel.Text = "*";
                            Common.AddControl(inputCell1, reqLabel, false, false);

                        }
                        //if (ValidationEngine.HasValidationConditions(inputControl.ID))
                        //{
                        //    inputControl.AutoPostBack = true;
                        //    inputControl.SelectedIndexChanged += ApplyValidation;
                        // }
                        #endregion

                        // add required indicator before the the input control
                        Common.AddControl(inputPanel, inputControl);
                        Common.AddControl(inputPanel, lse);

                        // add panels to the cell
                        Common.AddControl(inputCell2, inputPanel);

                        // add cells to row
                        Common.AddControl(inputRow, inputCell1);
                        Common.AddControl(inputRow, inputCell2);
                        Common.AddControl(snapsRow, snapsCell1);
                        Common.AddControl(snapsRow, snapsCell2);

                        // add rows to table
                        Common.AddControl(inputTable, inputRow);
                        Common.AddControl(snapsTable, snapsRow);

                        countingRows++;
                        if (countingRows == halfCount)
                        {
                            // add table to container panel
                            Common.AddControl(mainInputCell, inputTable, false, false);
                            Common.AddControl(mainSnapsCell, snapsTable, false, false);
                            Common.AddControl(mainInputRow, mainInputCell);
                            Common.AddControl(mainSnapsRow, mainSnapsCell);

                            //mainInputCell = new HtmlTableCell();
                            //mainInputCell.Style.Add(HtmlTextWriterStyle.VerticalAlign, "top");
                            mainSnapsCell = new HtmlTableCell();
                            mainSnapsCell.Style.Add(HtmlTextWriterStyle.VerticalAlign, "top");
                            mainSnapsCell.Style.Add(HtmlTextWriterStyle.Width, "50%");

                            //inputTable = new HtmlTable();
                            //inputTable.Style.Add("margin", "2px;"); inputTable.Style.Add("padding", "2px;");
                            snapsTable = new HtmlTable();
                            snapsTable.Style.Add("margin", "2px;"); snapsTable.Style.Add("padding", "2px;");

                            countingRows = 0;
                        }
                    }
                }
            }
            #endregion
            #region Attributes
            if (null != ipAttrs)
            {
                BCS.Biz.CompanyNumberAttributeCollection attrs = ipAttrs.FilterByDisplayOrder(index.ToString());
                if (attrs != null)
                {
                    if (attrs.Count > 1) // can do without this condition too, but lets just keep it since anyhow.......
                    {
                        attrs = attrs.SortByLabel(OrmLib.SortDirection.Ascending); // this is needed sometimes the filter might not retain the previous sort order
                        // causing issues with viewstate being changed
                    }
                    for (int i = 0; i < attrs.Count; i++)
                    {
                        HtmlTableRow inputRow = new HtmlTableRow();
                        HtmlTableRow snapsRow = new HtmlTableRow();

                        HtmlTableCell inputCell1 = new HtmlTableCell(); inputCell1.Align = "right";
                        HtmlTableCell snapsCell1 = new HtmlTableCell(); snapsCell1.Align = "right";

                        HtmlTableCell inputCell2 = new HtmlTableCell();
                        HtmlTableCell snapsCell2 = new HtmlTableCell(); snapsCell2.VAlign = "bottom";

                        Label label1 = new Label();
                        label1.Text = attrs[i].Label;
                        Label splabel1 = new Label();
                        splabel1.Text = attrs[i].Label + ":";

                        // add controls to cells
                        Common.AddControl(inputCell1, label1, false, false);
                        Common.AddControl(snapsCell1, splabel1, false, false);

                        string inputControlId = attrs[i].Id.ToString() + "IdLabel";
                        if (attrs[i].AttributeDataType.DataTypeName.Equals("bit", StringComparison.CurrentCultureIgnoreCase))
                        {
                            CheckBox checkBox = new CheckBox();
                            checkBox.ID = inputControlId;
                            //checkBox.Enabled = false;
                            checkBox.InputAttributes.Add("disabled", "disabled");

                            ToggleButtonExtender toggle = new ToggleButtonExtender();
                            toggle.ID = checkBox.ID + "Toggle";
                            toggle.TargetControlID = checkBox.ID;
                            toggle.DontUseImages = true;
                            toggle.CheckedImageAlternateText = "Yes";
                            toggle.UncheckedImageAlternateText = "No";
                            toggle.ImageHeight = 20;
                            toggle.ImageWidth = 20;

                            Common.AddControl(snapsCell2, checkBox);
                            Common.AddControl(snapsCell2, toggle);
                        }
                        else
                        {
                            Label vaControl = new Label();
                            vaControl.ID = attrs[i].Id.ToString() + "Label";
                            vaControl.Text = hfDisplayText.Value;
                            Common.AddControl(snapsCell2, vaControl);

                            TextBox idControl = new TextBox();
                            idControl.ID = inputControlId;
                            idControl.CssClass = "displayedNone";
                            Common.AddControl(snapsCell2, idControl);
                        }

                        #region add validation/indication controls
                        if (attrs[i].Required)
                        {
                            RequiredFieldValidator rfv = new RequiredFieldValidator();
                            Common.AddControl(snapsCell2, rfv, false, false);
                            rfv.Display = ValidatorDisplay.None;
                            rfv.ControlToValidate = inputControlId;
                            rfv.SetFocusOnError = false;
                            rfv.ErrorMessage = attrs[i].Label + " is required";
                            rfv.Text = " *";

                            Label reqLabel = new Label();
                            reqLabel.CssClass = "errorText";
                            reqLabel.Text = "*";
                            Common.AddControl(inputCell1, reqLabel, false, false);
                        }


                        // add required indicator before the the input control
                        WebControl c = BuildAttributeControl(inputCell2, attrs[i]);
                        if (codeType == CodeTypes.OtherCarrierSpecific)
                        {
                            AttrIdsHiddenOtherCarrier.Value += c.ID + ",";
                        }
                        else
                        {
                            AttrIdsHidden.Value += c.ID + ",";
                        }
                        // right now just for text box, we will see enhancements as they come in.
                        if (c is TextBox)
                        {
                            TextBox cT = c as TextBox;
                            if (!string.IsNullOrEmpty(attrs[i].UIAttributes))
                            {
                                string[] splitAttributeValues = attrs[i].UIAttributes.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                foreach (string splitAttributeValue in splitAttributeValues)
                                {
                                    string[] keyValue = splitAttributeValue.Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                    if (keyValue.Length == 2)
                                    {
                                        PropertyInfo pi = cT.GetType().GetProperty(keyValue[0]);
                                        if (pi != null)
                                        {
                                            object propValue = pi.PropertyType.IsEnum ? Enum.Parse(pi.PropertyType, keyValue[1], true) :
                                                Convert.ChangeType(keyValue[1], pi.PropertyType);
                                            pi.SetValue(cT, propValue, null);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        // add cells to row
                        Common.AddControl(inputRow, inputCell1, false, false);
                        Common.AddControl(inputRow, inputCell2, false, false);
                        Common.AddControl(snapsRow, snapsCell1, false, false);
                        Common.AddControl(snapsRow, snapsCell2, false, false);

                        // add rows to table
                        Common.AddControl(inputTable, inputRow, false, false);
                        Common.AddControl(snapsTable, snapsRow, false, false);

                        countingRows++;
                        if (countingRows == halfCount)
                        {
                            // add table to container panel
                            Common.AddControl(mainInputCell, inputTable, false, false);
                            Common.AddControl(mainSnapsCell, snapsTable, false, false);
                            Common.AddControl(mainInputRow, mainInputCell);
                            Common.AddControl(mainSnapsRow, mainSnapsCell);

                            //mainInputCell = new HtmlTableCell();
                            //mainInputCell.Style.Add(HtmlTextWriterStyle.VerticalAlign, "top");
                            mainSnapsCell = new HtmlTableCell();
                            mainSnapsCell.Style.Add(HtmlTextWriterStyle.VerticalAlign, "top");
                            mainSnapsCell.Style.Add(HtmlTextWriterStyle.Width, "50%");

                            //inputTable = new HtmlTable();
                            //inputTable.Style.Add("margin", "2px;"); inputTable.Style.Add("padding", "2px;");
                            snapsTable = new HtmlTable();
                            snapsTable.Style.Add("margin", "2px;"); snapsTable.Style.Add("padding", "2px;");

                            countingRows = 0;
                        }
                    }
                }
            }
            #endregion
        }
        if (countingRows < halfCount)
        {
            // add table to container panel
            Common.AddControl(mainInputCell, inputTable, false, false);
            Common.AddControl(mainSnapsCell, snapsTable, false, false);
            Common.AddControl(mainInputRow, mainInputCell);
            Common.AddControl(mainSnapsRow, mainSnapsCell);
        }

        Common.AddControl(mainInputTable, mainInputRow);
        Common.AddControl(baseInputPanel, mainInputTable);

        Common.AddControl(mainSnapsTable, mainSnapsRow);
        Common.AddControl(baseDisplayPanel, mainSnapsTable);
    }

    private WebControl BuildAttributeControl(Control cell, BCS.Biz.CompanyNumberAttribute attr)
    {
        WebControl c = null;
        string controlType = attr.AttributeDataType.DataTypeName.ToLower();
        switch (controlType)
        {
            case "date":
                {
                    c = new DateBox();
                    c.Attributes.Add("class", "txt");
                    break;
                }
            case "money":
                {
                    c = new MoneyBox();
                    c.Attributes.Add("class", "txt");
                    break;
                }
            case "bit":
                {
                    CheckBox chk = new CheckBox();
                    chk.InputAttributes.Add("class", "chk");
                    c = chk;
                    break;
                }
            case "text":
                {
                    c = new TextBox();
                    c.Attributes.Add("class", "txt");
                    break;
                }
            default:
                {
                    Type t = System.Type.GetType(string.Concat(attr.AttributeDataType.UserControl, ", System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), false, true);
                    System.Reflection.ConstructorInfo ci = t.GetConstructor(new Type[] { });
                    c = (WebControl)ci.Invoke(null);
                    break;
                }
        }
        c.ID = attr.Id.ToString();
        Common.AddControl(cell, c, false, true);

        if (attr.ReadOnlyProperty)
            c.Enabled = false;
        return c;
    }

    private void LoadCodeTypesItems(Panel ContainerPanel)
    {
        DateTime effDate = GetEffectiveDate(DateTime.MaxValue.Subtract(TimeSpan.FromSeconds(1)));

        BCS.Biz.CompanyNumberCodeTypeCollection types = ConfigValues.GetCompanyCodeTypes(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId), false);
        foreach (BCS.Biz.CompanyNumberCodeType type in types)
        {
            // get and pass extra
            ITextControl forExtra = Common.FindControlRecursive(Page, type.Id.ToString() + "CodeId") as ITextControl;
            ListItem[] items = GetItems(type.Id, effDate, Convert.ToInt32(forExtra.Text));
            DropDownList ddl = Common.FindControlRecursive(ContainerPanel, type.Id.ToString() + "Codeddl") as DropDownList;
            if (null != ddl)
            {
                ddl.Items.Clear();
                ddl.Items.AddRange(items);
            }
        }
    }

    private List<DropDownList> FindDropDownListControls(Control Container)
    {
        List<DropDownList> ddls = new List<DropDownList>();
        foreach (Control var in Container.Controls)
        {
            if (var is DropDownList)
                ddls.Add(var as DropDownList);
            if (var.HasControls())
            {
                ddls.AddRange(FindDropDownListControls(var));
            }
        }
        return ddls;
    }

    private List<IValidator> FindValidationControls(Control Container)
    {
        List<IValidator> ivalidators = new List<IValidator>();
        foreach (Control var in Container.Controls)
        {
            if (var is IValidator)
                ivalidators.Add(var as IValidator);
            if (var.HasControls())
            {
                ivalidators.AddRange(FindValidationControls(var));
            }
        }
        return ivalidators;
    }

    private void BindItems(string listControlId, int id)
    {
        ListControl listControl = Common.FindControlRecursive(Page, listControlId) as ListControl;
        if (null != listControl)
        {
            // this is used by copy functionality, so extra is not needed. just pass in 0.
            ListItem[] items = GetItems(id, GetEffectiveDate(System.Data.SqlTypes.SqlDateTime.MinValue.Value.AddDays(1)), 0);
            listControl.Items.AddRange(items);
        }
    }
    private ListItem[] GetItems(int id, DateTime effDate, int extra)
    {
        //int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
        dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCodeType.Columns.Id, id);
        //dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCodeType.Columns.CompanyNumberId, companyNumberId);

        BCS.Biz.CompanyNumberCodeType companyNumberCodeType = dm.GetCompanyNumberCodeType(
            BCS.Biz.FetchPath.CompanyNumberCodeType.CompanyNumberCode);

        List<ListItem> lic = new List<ListItem>();
        lic.Add(new ListItem("", "0"));

        int defaultId = 0;
        if (!companyNumberCodeType.DefaultCompanyNumberCodeId.IsNull)
        {
            defaultId = companyNumberCodeType.DefaultCompanyNumberCodeId.Value;
        }
        BCS.Biz.CompanyNumberCodeCollection sorted = companyNumberCodeType.CompanyNumberCodes.SortByCode(OrmLib.SortDirection.Ascending);
        foreach (BCS.Biz.CompanyNumberCode code in sorted)
        {
            if (code.ExpirationDate.IsNull || (code.ExpirationDate.Value > effDate) || code.Id == extra)
            {
                #region process display text, sometimes code or description may be absent
                string itemText = string.Concat(code.Code, "-", code.Description);
                itemText = itemText.Trim();
                string[] itemTextArray = itemText.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                itemText = string.Join(" - ", itemTextArray);
                #endregion
                ListItem li = new ListItem(itemText, code.Id.ToString());
                if (code.Id == defaultId)
                {
                    li.Selected = true;
                }
                lic.Add(li);
            }
        }

        return lic.ToArray();
    }

    // targets for both SetSafeText and SetSafeId are [.*]Label controls

    // sets default text based on string being empty or not
    private void SetSafeDisplayText(ITextControl label, string text)
    {
        label.Text = string.IsNullOrEmpty(text) ? hfDisplayText.Value : text;
    }
    private void SetSafeIdText(ITextControl label, string idText)
    {
        label.Text = (string.IsNullOrEmpty(idText) || idText == hfDisplayText.Value) ? string.Empty : idText;
    }
    private void SetSafeIdNumber(ITextControl label, string idText)
    {
        label.Text = (string.IsNullOrEmpty(idText) || idText == hfDisplayText.Value) ? "0" : idText;
    }

    private void SetId(ITextControl label, int id)
    {
        label.Text = id.ToString();
    }
    private void SetId(ITextControl label, Int64 id)
    {
        label.Text = id.ToString();
    }
    private void SetSafeDate(ITextControl label, DateTime dateTime, string defaultValue)
    {
        label.Text = dateTime == DateTime.MinValue ? defaultValue : dateTime.ToString(Common.DateFormat);
    }
    private void SetSafeDate(ITextControl label, string strDateTime, string defaultValue)
    {
        SetSafeDate(label, OrmLib.Validator.IsDate(strDateTime) ? Convert.ToDateTime(strDateTime) : DateTime.MinValue, defaultValue);
    }
    private void SetSafeIdDate(ITextControl label, string strDateTime)
    {
        SetSafeIdText(label, OrmLib.Validator.IsDate(strDateTime) ? Convert.ToDateTime(strDateTime).ToString(Common.DateFormat) : string.Empty);
    }

    private void SetSafeDecimal(ITextControl label, string strDecimal)
    {
        decimal d = decimal.Zero;
        decimal.TryParse(strDecimal, System.Globalization.NumberStyles.Currency,
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat, out d);

        label.Text = string.Format("{0:c}", d);
    }

    #region LOB Children
    protected void AddLOB()
    {
        UpdateLOBSToViewState();
        int minId = 0;
        List<SubmissionLineOfBusinessChild> list = GetLOBSAsList();
        List<int> ids = new List<int>();
        foreach (SubmissionLineOfBusinessChild var in list)
        {
            minId = Math.Min(var.Id, minId);
        }
        minId--;

        SubmissionLineOfBusinessChild newLOB = new SubmissionLineOfBusinessChild();
        if (DefaultStatus != -1)
            newLOB.SubmissionStatusId = DefaultStatus;
        newLOB.Id = minId;
        list.Add(newLOB);

        LOBS = list.ToArray();
        BindLobs();
    }

    private List<SubmissionLineOfBusinessChild> GetLOBSAsList()
    {
        SubmissionLineOfBusinessChild[] lobs = LOBS;
        List<SubmissionLineOfBusinessChild> list = new List<SubmissionLineOfBusinessChild>();
        list.AddRange(lobs);
        return list;
    }

    private SubmissionLineOfBusinessChild[] GetUndeletedLOBS()
    {
        List<SubmissionLineOfBusinessChild> list = GetLOBSAsList();
        List<SubmissionLineOfBusinessChild> undeleted = new List<SubmissionLineOfBusinessChild>();
        foreach (SubmissionLineOfBusinessChild var in list)
        {
            if (!var.Deleted)
            {
                undeleted.Add(var);
            }
        }

        return undeleted.ToArray();
    }
    private SubmissionLineOfBusinessChild[] GetUpdateableLOBS()
    {
        List<SubmissionLineOfBusinessChild> list = GetLOBSAsList();
        List<SubmissionLineOfBusinessChild> updateable = new List<SubmissionLineOfBusinessChild>();
        foreach (SubmissionLineOfBusinessChild var in list)
        {
            if (var.CompanyNumberLOBGridId != 0)
            {
                updateable.Add(var);
            }
        }

        return updateable.ToArray();
    }
    private string GetUpdateableLOBSAsString()
    {
        SubmissionLineOfBusinessChild[] list = GetUpdateableLOBS();
        List<SubmissionLineOfBusinessChild> updateable = new List<SubmissionLineOfBusinessChild>();
        foreach (SubmissionLineOfBusinessChild var in list)
        {
            if (var.CompanyNumberLOBGridId != 0)
            {
                updateable.Add(var);
            }

            // core expects id as 0 for new, so set all negative ids to 0
            if (var.Id <= 0)
                var.Id = 0;
        }

        List<string> stringItems = new List<string>();
        foreach (SubmissionLineOfBusinessChild var in updateable)
        {
            stringItems.Add(string.Format("{0}={1}={2}={3}={4}|",
                    var.Id,
                    var.CompanyNumberLOBGridId,
                    var.SubmissionStatusId,
                    var.SubmissionStatusReasonId,
                    var.Deleted));
        }
        return string.Join("|", stringItems.ToArray());
    }
    private void UpdateEffectiveLOB(int p)
    {
        int count = Convert.ToInt32(EffectiveLOBCount.Text);
        count = count + p;
        EffectiveLOBCount.Text = count.ToString();
    }
    private void BindLobs()
    {

        LOBList4Display.DataSource = GetUpdateableLOBS();
        LOBList4Display.DataBind();

        LobList1.DataSource = GetUndeletedLOBS();
        LobList1.DataBind();
    }

    #endregion

    #region comments
    protected void RemoveComment(Object sender, GridViewDeleteEventArgs e)
    {
        if (gridComments4Display.Rows.Count < 1)
        {
            e.Cancel = true;
            return;
        }

        List<SubmissionComment> list = GetCommentsAsList();
        object oid = gridComments4Display.DataKeys[e.RowIndex].Value;
        int id = 0;
        try
        {
            id = Convert.ToInt32(oid);
        }
        catch { }
        for (int i = 0; i < list.Count; i++)
        {
            SubmissionComment item = list[i];
            if (id == item.Id)
            {
                if (item.Id < 0)
                {
                    list.RemoveAt(i);
                    break;
                }
                else
                {
                    item.Deleted = true;
                    BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
                    dm.QueryCriteria.Clear();

                    dm.QueryCriteria.And(BCS.Biz.JoinPath.SubmissionComment.Columns.Id, item.Id);
                    BCS.Biz.SubmissionComment itemToDelete = dm.GetSubmissionComment();

                    if (itemToDelete != null)
                    {
                        itemToDelete.Delete();
                        dm.CommitAll();
                        list.RemoveAt(i);
                    }
                    break;
                }
            }
        }
        Comments = list.ToArray();
        BindComments();
        gridAgencies.DataBind(); // this should not be needed but somehow the agencies seem to lose state
    }
    protected void AddComment()
    {
        if (string.IsNullOrEmpty(txComments.Text.Trim()))
        {
            return;
        }
        int minId = 0;
        List<SubmissionComment> list = GetCommentsAsList();
        List<int> ids = new List<int>();
        foreach (SubmissionComment var in list)
        {
            minId = Math.Min(var.Id, minId);
        }
        minId--;

        SubmissionComment newComment = new SubmissionComment();
        newComment.Id = minId;
        newComment.Comment = txComments.Text;
        newComment.EntryBy = User.Identity.Name;
        newComment.EntryDt = DateTime.Now;
        list.Add(newComment);

        Comments = list.ToArray();
        BindComments();

        txComments.Text = string.Empty;
    }
    private List<SubmissionComment> GetCommentsAsList()
    {
        SubmissionComment[] comments = Comments;
        List<SubmissionComment> list = new List<SubmissionComment>();
        list.AddRange(comments);
        return list;
    }
    private SubmissionComment[] GetUndeletedComments()
    {
        List<SubmissionComment> list = GetCommentsAsList();
        List<SubmissionComment> comments = new List<SubmissionComment>();
        foreach (SubmissionComment var in list)
        {
            if (!var.Deleted)
            {
                comments.Add(var);
            }
        }

        comments.Sort(new SubmissionCommentComparer());
        return comments.ToArray();
    }
    private SubmissionComment[] GetUpdateableComments()
    {
        List<SubmissionComment> list = GetCommentsAsList();
        List<SubmissionComment> comments = new List<SubmissionComment>();
        foreach (SubmissionComment var in list)
        {
            if (!string.IsNullOrEmpty(var.Comment))
            {
                comments.Add(var);
            }
        }

        comments.Sort(new SubmissionCommentComparer());
        return comments.ToArray();
    }

    private string GetUpdateableCommentsAsString()
    {
        SubmissionComment[] list = GetUpdateableComments();
        List<SubmissionComment> updateable = new List<SubmissionComment>();
        foreach (SubmissionComment var in list)
        {
            // core expects id as 0 for new, so set all negative ids to 0
            if (var.Id <= 0)
                var.Id = 0;

            updateable.Add(var);
        }

        List<string> stringItems = new List<string>();
        string separator = "=====";
        foreach (SubmissionComment var in updateable)
        {
            stringItems.Add(string.Format("{0}{5}{1}{5}{2}{5}{3}{5}{4}|",
                    var.Id,
                    var.Comment,
                    var.EntryBy,
                    var.EntryDt,
                    var.Deleted,
                    separator));
        }
        return string.Join("|", stringItems.ToArray());
    }
    private void BindComments()
    {
        SubmissionComment[] comments = GetUndeletedComments();

        gridComments4Display.DataSource = comments;
        gridComments4Display.DataBind();
    }
    #endregion

    #endregion

    protected void gridComments4Display_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //if (e.NewPageIndex < 0)
        //    gridComments4Display.PageIndex = 0;
        //else
        gridComments4Display.PageIndex = e.NewPageIndex;
        BindComments();

        gridAgencies.DataBind(); // this should not be needed but somehow the agencies seem to lose state
    }
    protected void gridStatusHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gridAgencies.DataBind(); // this should not be needed but somehow the agencies seem to lose state
    }

    protected void cvInsuredOrDBA_ServerValidate(object sender, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (string.IsNullOrEmpty(spInsuredNameIdLabel.Text) && string.IsNullOrEmpty(spDBAIdLabel.Text))
            args.IsValid = false;
    }

    protected void cvAgentRequired_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        if (Common.IsCWG() && IsProspect())
            args.IsValid = true;
        else if (DefaultValues.SupportsAgentUnspecifiedReasons(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
        {
            if ("0" == spAgentIdLabel.Text && ("0" == spAgentUnspecifiedIdLabel.Text || "" == spAgentUnspecifiedIdLabel.Text))
            {
                args.IsValid = false;
                cvAgentRequired.ErrorMessage = "Agent OR Reason is required.";
                cvAgentRequired.IsValid = false;
                return;
            }
        }
    }

    protected void customPolicyNumberValidator_ServerValidate(object source, ServerValidateEventArgs args)
    {
        args.IsValid = true;

        // at the moment only cwg has additional validation, and on adds
        if (Common.IsCWG() && !IsProspect() && Session["SubmissionId"] == null)
        {
            // if generated either on wizard or copy panel
            if (txPolicyNumber.Generated ||
                (Common.FindControlRecursive(CopyContainerPanel, txPolicyNumber.ID + "4Copy") as WebUserControls_AutoNumberGenerator).Generated)
            {
                // only validate if not generated
                return;
            }
            int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
            long entered = 0;

            try
            {
                entered = Convert.ToInt64(spPolicyNumberIdLabel.Text);
            }
            catch (Exception)
            {
                // should be a number                
                args.IsValid = false;
                customPolicyNumberValidator.ErrorMessage = "Policy Number is not numeric";
                customPolicyNumberValidator.IsValid = false;
                return;
            }

            // should be less than less than or equal to the the next policy number in the control number table
            BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.NumberControl.Columns.CompanyNumberId, companyNumberId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.NumberControl.NumberControlType.Columns.TypeName, "Policy Number");
            BCS.Biz.NumberControl nc = dm.GetNumberControl();
            if (nc.ControlNumber.Value < entered)
            {
                args.IsValid = false;
                customPolicyNumberValidator.ErrorMessage = string.Format("Policy Number should be less than or equal to {0}.", nc.ControlNumber.Value);
                customPolicyNumberValidator.IsValid = false;
                return;
            }
            else if (nc.ControlNumber.Value == entered)
            {
                nc.ControlNumber = nc.ControlNumber + 1;
                dm.CommitAll();
            }
            // no existing policy numbers entered in Clearance for the new number
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Submission.Columns.CompanyNumberId, companyNumberId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Submission.Columns.PolicyNumber, entered.ToString());
            BCS.Biz.SubmissionCollection existingPolicySubmissions = dm.GetSubmissionCollection();
            if (existingPolicySubmissions != null && existingPolicySubmissions.Count > 0)
            {
                args.IsValid = false;
                customPolicyNumberValidator.ErrorMessage = "Policy Number already exists in clearance.";
                customPolicyNumberValidator.IsValid = false;
                return;
            }

        }
    }

    protected void chkOtherSubmissions_onCheckChanged(object sender, EventArgs e)
    {
        LoadOtherSubmissions2(false);
    }

    protected void LoadOtherSubmissions2(bool refresh)
    {
        try
        {
            DataTable dt = null;
            DateTime begDate = DateTime.Now.AddYears(-10);
            if (refresh)
            {
                string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
                int agencyId = -1;
                string xml = BCS.Core.Clearance.Submissions.SearchByClientCoreClientIdAndAgencyId(companyNumber, long.Parse(Session["ClientID"].ToString()), agencyId, 0, 0, false);

                dt = Common.GenerateDataTable(companyNumber, "submission", xml, new Pair("master_agency_id", "master_agency_id"));


                if (dt != null)
                {
                    DataView dv = dt.AsDataView();
                    dv.Sort = dt.DefaultView.Sort;
                    dt = dv.ToTable();
                }
                ViewState["OtherSubmissionsDS"] = dt;

            }
            else
            {
                dt = (DataTable)ViewState["OtherSubmissionsDS"];
            }

            if (ViewState["CompanySubmissionYears"] == null)
            {
                int years = BCS.WebApp.Components.ConfigValues.SubmissionDisplayStartDate(int.Parse(Common.GetSafeStringFromSession(SessionKeys.CompanyId)));
                begDate = DateTime.Now.AddYears(years * -1);
                ViewState["CompanySubmissionYears"] = years;
            }
            else
            {
                begDate = DateTime.Now.AddYears((int)ViewState["CompanySubmissionYears"] * -1);
            }

            if (dt == null || dt.Rows.Count == 0)
            {
                lbOtherSubmissions.Visible = true;
                return;
            }
            else
                lbOtherSubmissions.Visible = false;

            DataTable ndt = null;

            string submissionId = (Session["SubmissionId"] != null ? Session["SubmissionId"].ToString() : string.Empty);

            if ((from d in dt.AsEnumerable()
                 where d.Field<string>("id") != submissionId
                     && d.Field<DateTime>("effective_date") > begDate
                 select d).Count() > 0)
            {
                ndt = (from d in dt.AsEnumerable()
                       where d.Field<string>("id") != submissionId
                           && d.Field<DateTime>("effective_date") > begDate
                       select d).DefaultIfEmpty().CopyToDataTable();
            }
            else
                return;


            if (ndt != null && !chkShowAllOtherSubmissions.Checked && dt.Rows.Count > 15)
            {
                ndt = ndt.AsEnumerable().Take(15).CopyToDataTable();
                chkShowAllOtherSubmissions.Visible = true;
            }
            else
                chkShowAllOtherSubmissions.Visible = dt.Rows.Count > 15;

            gvSubmissions.DataSource = ndt.AsDataView();
            gvSubmissions.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    protected void lnkOtherSubmission_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        Session["SubmissionId"] = btn.CommandArgument;
        if (DefaultValues.UseWizard)
            Response.Redirect("SubmissionWizard.aspx");
        Response.Redirect("Submission.aspx?op=edit");
    }


    protected void gvSubmissions_Init(object sender, EventArgs e)
    {
        BCS.Biz.SubmissionDisplayGridCollection dfields = ConfigValues.GetSubmissionGridDisplaySS(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));

        foreach (BCS.Biz.SubmissionDisplayGrid cfield in dfields)
        {
            TemplateField atf = new TemplateField();
            atf.ItemTemplate = new BCS.WebApp.Templates.GenericTemplate(DataControlRowType.DataRow, string.Empty, cfield.FieldName, cfield.AttributeDataType.DataTypeName, string.Empty);
            //atf.HeaderTemplate = new Templates.GenericTemplate(DataControlRowType.Header, string.IsNullOrEmpty(cfield.HeaderText) ? cfield.FieldName : cfield.HeaderText, string.Empty, string.Empty);
            atf.HeaderText = string.IsNullOrEmpty(cfield.HeaderText) ? cfield.FieldName : cfield.HeaderText;
            atf.HeaderText = FormatHeaderText(atf.HeaderText);
            if (gvSubmissions.AllowSorting)
                atf.SortExpression = cfield.FieldName;

            gvSubmissions.Columns.Insert(gvSubmissions.Columns.Count, atf);
        }
    }

    protected void gvSubmissions_PageIndexChanged(object sender, GridViewPageEventArgs e)
    {
        gvSubmissions.PageIndex = e.NewPageIndex;
        gvSubmissions.DataBind();
    }

    private string FormatHeaderText(string headerText)
    {
        headerText = headerText.Replace("Number", "#");
        headerText = headerText.Replace("Symbol", "Sym");
        headerText = headerText.Replace("Sequence", "Seq");

        return headerText;
    }

    public string GetLOBVisible()
    {
        return DefaultValues.SupportsMultipleLineOfBusinesses(Common.GetSafeIntFromSession(SessionKeys.CompanyId)).ToString();
    }

    private void performOFACcheck(int submissionId, string submissionNumber)
    {
        Structures.Info.ClientInfo _client = (Structures.Info.ClientInfo)Session["Client"];
        if (_client.Addresses != null && _client.Client.Names != null)
        {
            //pass the very first name and address for the client to do an OFAC check.
            //we might need to revise this to pass every name and address ? something that needs to be looked into.
            Structures.Info.Name name = (_client.Client.Names.Any(n => n.PrimaryNameFlag == "Y")) ? _client.Client.Names.First(n => n.PrimaryNameFlag == "Y") : _client.Client.Names[0];
            Structures.Info.Address addr = _client.Addresses[0];

            int companyId = Common.GetSafeIntFromSession(BCS.WebApp.Components.SessionKeys.CompanyId);
            string clientNameValue = name.NameBusiness;
            string address = (addr.HouseNumber + ' ' + addr.Address1 + ' ' + addr.Address2).Trim();
            string city = addr.City;
            string state = addr.StateProvinceId;
            string country = addr.Country;
            string zipCode = addr.PostalCode;

            IsOfacHit = BTS.WFA.BCS.Services.LegacyService.IsOFACMatch(companyId, submissionId, String.Format("Submission # :{0}", submissionNumber), clientNameValue, address, city, state, zipCode, country);
        }
    }

    protected void SubmissionTypeDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetSubmissionTypeValues();
    }
    private void SetSubmissionTypeValues()
    {
        int safeValue = Common.GetSafeSelectedValue(SubmissionTypeDropDownList1);
        spSubmissionTypeIdLabel.Text = safeValue.ToString();
        spSubmissionTypeLabel.Text = safeValue == 0 ? hfDisplayText.Value : SubmissionTypeDropDownList1.SelectedItem.Text;
    }
}
