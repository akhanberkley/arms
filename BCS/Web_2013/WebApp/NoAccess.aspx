<%@ Page language="c#" Inherits="BCS.WebApp.NoAccess" CodeFile="NoAccess.aspx.cs" Theme="" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Berkley Clearance System : No Access</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1" />
		<meta name="CODE_LANGUAGE" content="C#" />
		<meta name="vs_defaultClientScript" content="JavaScript" />
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
		<style type="text/css">
			BODY {
				margin-top: 0px;
				font-size: 10pt;
				margin-left: 0px;
				margin-right: 0px;
				font-family: Verdana, Arial;
			}
			.errorbanner {
				border-top: lightgrey 2px solid; 
				font-weight: bold; 
				border-left-width: thin; 
				font-size: 20pt; 
				border-left-color: black; 
				padding-bottom: 6px; 
				width: 100%; 
				color: white; 
				padding-top: 2px; 
				border-bottom: darkgray 4px solid; 
				position: relative;  
				background-color: red; 
				text-align: center; 
				border-right-width: thin; 
				border-right-color: black;
				margin-top: 100px;
				display: block;
			}
			.errorinfo {
				WIDTH: 100%;  
				TEXT-ALIGN: center;
				margin-bottom: 30px;
			}
			.errormessages {
				WIDTH: 100%;  
				TEXT-ALIGN: center;
				margin-bottom: 100px;
			}
		</style>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<div style="clear:both;"></div>
			<span class="errorbanner">Unauthorized Access Detected</span>
			<p class="errorinfo">You do not currently have access to this system. Please see messages below for further information.</p>
			<p class="errormessages">
				<b>Messages:</b><br /><asp:Label id="lblSecurityEvent" runat="server"></asp:Label></p>
				<p class="errormessages">Please <a href="mailto:BTSHelpdesk@wrberkley.com">contact the BTS HelpDesk</a> 
for further assistance.</p>
Username : <asp:TextBox ID="tbUser" runat="server"></asp:TextBox><br />
Password : <asp:TextBox ID="tbPassword" runat="server" TextMode="Password"></asp:TextBox>
<asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" />
		</form>
	</body>
</html>
