<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="SubmissionSearchNew.aspx.cs"
    Inherits="SubmissionSearchNew" Title="Berkley Clearance System : Search By Submission"
    Theme="defaultTheme" %>

<%@ Register TagPrefix="UserControls" Namespace="BCS.WebApp.UserControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajx:ToolkitScriptManager ID="ScriptManager1" runat="server" CombineScripts="True" EnablePageMethods="true">
    </ajx:ToolkitScriptManager>
    <script type="text/javascript">
        function changeState(chboxid) {
            var Hidden1 = document.getElementById('<%=Hidden1.ClientID%>');
            if (document.getElementById(chboxid.id).checked) {
                var value = Hidden1.value;
                value++;
                Hidden1.value = value;
                //alert(value);
            }
            else {
                var value = Hidden1.value;
                value--;
                if (value < 0)
                    value = 0;
                Hidden1.value = value;

                var selectAllState = document.getElementById('<%=selectAllState.ClientID%>');
                selectAllState.value = 0;

                //alert(value);
            }
            // var xxx = document.getElementById('xxxxx').checked;        
        }

        function selectedAll(chboxid) {
            var control = document.getElementById('<%=selectAllState.ClientID%>');
            if (document.getElementById(chboxid.id).checked) {
                control.value = 1;
                //alert(value);
            }
            else {
                control.value = 0;
                //alert(value);
            }
        }

        function ValidateAndConfirm(controlToValidate, confirmationMessage) {
            if (document.getElementById(controlToValidate.id).value == "0")
                return false;
            window.event.returnValue = confirm(confirmationMessage);
        }

        var filteredDropdown = "false";
        var effDate = "";
        var effDateFrom = "";
        var oldid = "";

        function FilterPolicySymbolDropDownByEffectiveDate(sender) {
            var txtEffFrom = document.getElementById('<%=txEffFrom.ClientID%>');
            var txtEffTo = document.getElementById('<%=txEffTo.ClientID%>');
            if (txtEffFrom.value != "" && txtEffTo.value != "") {
                if (filteredDropdown == "false" || txtEffTo.value != effDate || txtEffFrom.value != effDateFrom) {
                    var effDateRange = txtEffFrom.value + "|" + txtEffTo.value;
                    //var ddlId = document.getElementById('<%=hdnPolicySymbolddlID.ClientID%>').value;
                    var ddlId = sender.id.replace("ctl00_", "").replace("ContentPlaceHolder1_", "");
                    effDate = txtEffTo.value;
                    effDateFrom = txtEffFrom.value;
                    oldid = sender.id;
                    filteredDropdown = "true";
                    PageMethods.PopulatePolicySymbolByEffectiveDates(effDateRange, ddlId, call_success);
                }
            }
            else if (filteredDropdown == "true") {
                var effDateRange = " ";
                //var ddlId = document.getElementById('<%=hdnPolicySymbolddlID.ClientID%>').value;
            var ddlId = sender.id.replace("ctl00_", "").replace("ContentPlaceHolder1_", "");
            PageMethods.PopulatePolicySymbolByEffectiveDates(effDateRange, ddlId, call_success);
            effDate = "";
            effDateFrom = "";
            oldid = sender.id;
            filteredDropdown = "false";

        }
}

function call_success(res) {
    if (res != "" && res.length > 0) {
        var ddlId = document.getElementById('<%=hdnPolicySymbolddlID.ClientID%>').value;
        var id = 'ContentPlaceHolder1_' + ddlId;
        var controlDDL = document.getElementById(id);
        //var controlDDL = document.getElementById(oldid);
        var listItems = res.split('~');
        controlDDL.options.length = 0;
        var opt = document.createElement('option');
        opt.text = "  ";
        opt.value = "0";
        controlDDL.options.add(opt);
        for (i = 0; i < listItems.length - 1; i++) {
            var thisItem = listItems[i];
            if (thisItem != "" & thisItem.length > 0) {
                addItem = thisItem.split('|');
                var opt = document.createElement('option');
                opt.text = addItem[0];
                opt.value = addItem[1];
                controlDDL.options.add(opt);
            }
        }

    }
    else {
        var ddlId = document.getElementById('<%=hdnPolicySymbolddlID.ClientID%>').value;
        var id = 'ContentPlaceHolder1_' + ddlId;
        var controlDDL = document.getElementById(id);
        controlDDL.options.length = 0;
        var opt = document.createElement('option');
        opt.text = " ";
        opt.value = "0";
        controlDDL.options.add(opt);
    }
}
    </script>

    <p>
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx" OnInit="HyperLink1_Init">Search By Client</asp:HyperLink>
        <%--<asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/agencysubmissionsearch.aspx" OnInit="HyperLink1_Init">Search By Agency</asp:HyperLink>--%>
    </p>


    <asp:PlaceHolder runat="server" ID="messagePlaceHolder"></asp:PlaceHolder>


    <table cellpadding="0" cellspacing="0" class="box">
        <tbody>
            <tr>
                <td class="TL">
                    <div />
                </td>
                <td class="TC">Search by Submission</td>
                <td class="TR">
                    <div />
                </td>
            </tr>
            <tr>
                <td class="ML">
                    <div />
                </td>
                <td class="MC">

                    <table id="atable" cellpadding="2">
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlPrimaryFilters" runat="server">
                                    <table style="width: 100%" cellpadding="5">
                                        <tr id="rowUnderwriterPrimary" runat="server" visible="true">
                                            <td align="right">Date Created</td>
                                            <td style="white-space: nowrap">
                                                <asp:TextBox ID="txSubmissionStartDate" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                                                &nbsp; 
                               to &nbsp;
                               <asp:TextBox ID="txSubmissionEndDate" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="CreatedBy" runat="server">
                                            <td nowrap="nowrap" align="right">Created By
                                            </td>
                                            <td>
                                                <UserControls:CompanyUsersDropDown runat="server" ID="ddlUserCreatedBy"></UserControls:CompanyUsersDropDown>
                                                <ajx:ListSearchExtender ID="ListSearchExtender12" runat="server" TargetControlID="ddlUserCreatedBy"></ajx:ListSearchExtender>
                                            </td>
                                        </tr>
                                        <tr id="LastUpdated" runat="server">
                                            <td align="right">Last Updated</td>
                                            <td style="white-space: nowrap">
                                                <asp:TextBox ID="txLastModifiedStart" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                                                &nbsp; 
                               to &nbsp;
                               <asp:TextBox ID="txLastModifiedEnd" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="ModifiedBy" runat="server">
                                            <td nowrap="nowrap" align="right">Modified By
                                            </td>
                                            <td>
                                                <UserControls:CompanyUsersDropDown runat="server" ID="ddlUserModifiedBy"></UserControls:CompanyUsersDropDown>
                                                <ajx:ListSearchExtender ID="ListSearchExtender13" runat="server" TargetControlID="ddlUserModifiedBy"></ajx:ListSearchExtender>
                                            </td>
                                        </tr>

                                        <tr id="SubmissionType" runat="server">
                                            <td align="right">Type</td>
                                            <td>
                                                <UserControls:SubmissionTypeDropDownList ID="SubmissionTypeDropDownList1" runat="server" Default="False">
                                                </UserControls:SubmissionTypeDropDownList>
                                                <ajx:ListSearchExtender ID="ListSearchExtender4" runat="server" TargetControlID="SubmissionTypeDropDownList1"></ajx:ListSearchExtender>
                                            </td>
                                        </tr>
                                        <tr id="SubmissionStatus" runat="server">
                                            <td align="right">Status</td>
                                            <td>
                                                <UserControls:SubmissionStatusDropDownList ID="SubmissionStatusDropDownList1"
                                                    runat="server" Default="False">
                                                </UserControls:SubmissionStatusDropDownList>
                                                <ajx:ListSearchExtender ID="ListSearchExtender5" runat="server" TargetControlID="SubmissionStatusDropDownList1"></ajx:ListSearchExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                            <td valign="top">
                                <asp:Panel ID="pnlAdditionalFilters" runat="server" GroupingText="Additional Filters">
                                    <table style="width: 100%" cellpadding="5">
                                        <tr id="Agency" runat="server">
                                            <td align="right">Agency
                                            </td>
                                            <td>
                                                <UserControls:AgencyDropDownList ID="AgencyDropDownList1" runat="server" IdentifyInactive="True" OnInit="AgencyDropDownList1_Init" DisplayFormat="{0} ({1})">
                                                </UserControls:AgencyDropDownList>
                                                <ajx:ListSearchExtender ID="ListSearchExtender2" runat="server" TargetControlID="AgencyDropDownList1"></ajx:ListSearchExtender>
                                            </td>
                                        </tr>
                                        <tr id="Underwriter" runat="server">
                                            <td align="right">Underwriter
                                            </td>
                                            <td>
                                                <UserControls:UnderwritersDropDownList ID="uddl" runat="server" Role="Underwriter" AutoPostBack="False"
                                                    OnInit="uddl_Init">
                                                </UserControls:UnderwritersDropDownList>
                                                <ajx:ListSearchExtender ID="ListSearchExtender223" runat="server" TargetControlID="uddl"></ajx:ListSearchExtender>
                                            </td>
                                        </tr>


                                        <tr id="UnderwritingUnit" runat="server">
                                            <td align="right">Underwriting Unit</td>
                                            <td>
                                                <UserControls:LineOfBusinessDropDownList ID="LineOfBusinessDropDownList1" runat="server">
                                                </UserControls:LineOfBusinessDropDownList>
                                                <ajx:ListSearchExtender ID="ListSearchExtender3" runat="server" TargetControlID="LineOfBusinessDropDownList1"></ajx:ListSearchExtender>
                                            </td>
                                        </tr>
                                        <tr id="EffectiveFrom" runat="server">
                                            <td nowrap="nowrap" align="right">Effective From</td>
                                            <td>
                                                <asp:TextBox ID="txEffFrom" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                                                &nbsp;
                                To
                                &nbsp;
                                <asp:TextBox ID="txEffTo" runat="server" OnInit="DateBox_Init" o></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="ExpirationFrom" runat="server">
                                            <td nowrap="nowrap" align="right">Expiration From</td>
                                            <td>
                                                <asp:TextBox ID="txExpFrom" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                                                &nbsp;
                                To
                                &nbsp;
                                <asp:TextBox ID="txExpTo" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr id="Premium" runat="server">
                                            <td nowrap="nowrap" align="right">Premium</td>
                                            <td>
                                                <asp:DropDownList ID="ddlOperators" runat="server">
                                                    <asp:ListItem Value="GreaterOrEqual">&gt;=</asp:ListItem>
                                                    <asp:ListItem Value="LesserOrEqual">&lt;=</asp:ListItem>
                                                </asp:DropDownList>
                                                &nbsp;
                                <asp:TextBox ID="txPremium" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>

                            </td>
                        </tr>
                        <tr valign="top">
                            <td colspan="2" valign="top">
                                <fieldset id="rowAttributes" runat="server">
                                    <legend>Attributes</legend>
                                    <table style="width: 100%">
                                        <tr valign="top">
                                            <td style="width: 50%">
                                                <asp:PlaceHolder ID="plAttributesLeft" runat="server" />
                                            </td>
                                            <td style="width: 50%">
                                                <asp:PlaceHolder ID="plAttributesRight" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="btnFilter" runat="server" Text="Filter Submissions" OnClick="btnFilter_Click" AccessKey="S" OnInit="ValidationButton_Init" />
                                <asp:Button ID="btnClear" runat="server" Text="Clear Filters" CausesValidation="False" OnClick="btnClear_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr noshade />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <br />
                            </td>
                        </tr>
                    </table>
                    <input id="Hidden1" runat="server" type="hidden" value="0" />
                    <input id="selectAllState" runat="server" type="hidden" value="0" />
                    <input id="dataRowCount" runat="server" type="hidden" value="0" />
                    <asp:HiddenField ID="hdnPolicySymbolddlID" runat="server" Value="0" />
                </td>
                <td class="MR">
                    <div />
                </td>
            </tr>
            <tr>
                <td class="BL">
                    <div />
                </td>
                <td class="BC">
                    <div />
                </td>
                <td class="BR">
                    <div />
                </td>
            </tr>
        </tbody>
    </table>

    <table id="btable" runat="server" cellpadding="0" cellspacing="0" class="box">

        <tr>
            <td class="TL">
                <div />
            </td>
            <td class="TC">Submissions
            </td>
            <td class="TR">
                <div />
            </td>
        </tr>
        <tr>
            <td class="ML">
                <div />
            </td>
            <td class="MC">


                <asp:Label runat="server" Visible="false" ID="lblResultCount"></asp:Label>
                <asp:ImageButton runat="server" ID="ibtnExportToExcel" OnClick="lbtnExportToExcel_Click" ImageUrl='~/Images/Excel.gif' ToolTip="Import To Excel" Visible="false"></asp:ImageButton>
                <UserControls:Grid ID="gvSubmissions" EmptyDataText="No Submissions were found for the criteria entered"
                    runat="server" AllowPaging="True" AutoGenerateColumns="False" OnInit="gvSubmissions_Init"
                    OnPageIndexChanging="gvSubmissions_PageIndexChanging"
                    OnRowDataBound="gvSubmissions_RowDataBound"
                    OnRowCommand="gvSubmissions_RowCommand"
                    OnSelectedIndexChanged="gvSubmissions_SelectedIndexChanged"
                    DataKeyNames="id,ClientCore_Client_Id,submission_number,submission_number_sequence"
                    OnDataBound="gvSubmissions_DataBound"
                    OnSorting="gvSubmissions_Sorting" AllowSorting="True">
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("folder_find.gif") %>' AlternateText="View Status History" />
                                <ajx:ModalPopupExtender runat="server" ID="programmaticModalPopup"
                                    TargetControlID="Image1"
                                    PopupControlID="programmaticPopup"
                                    BackgroundCssClass="modalBackground"
                                    DropShadow="True"
                                    RepositionMode="RepositionOnWindowScroll">
                                </ajx:ModalPopupExtender>
                                <asp:Panel ID="programmaticPopup" runat="server" Style="display: none" CssClass="modalPopup" OnLoad="Panel1_Load" HorizontalAlign="center" Width="520px">
                                    <div>
                                        <aje:UpdatePanel ID="UpdatePanel81" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="lblSubmissionNumber" runat="server"></asp:Label>
                                                <asp:Label ID="lblCurrentStatus" runat="server"></asp:Label>
                                                <asp:PlaceHolder ID="HistoryStatusPH" runat="server"></asp:PlaceHolder>
                                                <br />
                                                <br />
                                                <UserControls:Grid ID="GridView1" runat="server" CssClass="grid" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" EmptyDataText="There is no history for this submission." AllowPaging="True" AutoGenerateColumns="False"
                                                    DataSourceID="ObjectDataSource1" DataKeyNames="Id" OnRowDataBound="GridView1_RowDataBound" Width="520px">
                                                    <EmptyDataTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td style="border: none;">There is no history for this submission. </td>
                                                            </tr>
                                                        </table>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:BoundField DataField="Id" Visible="False" HeaderText="Id"></asp:BoundField>
                                                        <asp:BoundField DataField="SubmissionId" Visible="False" HeaderText="SubmissionId"></asp:BoundField>
                                                        <asp:BoundField DataField="PreviousSubmissionStatusId" Visible="False" HeaderText="PreviousSubmissionStatusId"></asp:BoundField>
                                                        <asp:TemplateField HeaderText="Status Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStatusDate" runat="server" Text='<%# Bind("SubmissionStatusDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Previous Submission Status">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "SubmissionStatus.StatusCode") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Status Changed Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDate" runat="server" Text='<%# Bind("StatusChangeDt", "{0:MM/dd/yyyy HH:mm}") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="StatusChangeBy" HeaderText="Status Changed By"></asp:BoundField>
                                                        <asp:TemplateField HeaderText="Active" ShowHeader="False">
                                                            <ItemTemplate>
                                                                <asp:LinkButton runat="server" Text="Active" CommandName="Select" CausesValidation="false" ID="LinkButton1"></asp:LinkButton>
                                                                <ajx:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="LinkButton1"
                                                                    ConfirmText="Are you sure you want to make this status history inactive, this cannot be undone?">
                                                                </ajx:ConfirmButtonExtender>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerTemplate>
                                                        <asp:PlaceHolder ID="PlaceHolder2" runat="server"></asp:PlaceHolder>
                                                    </PagerTemplate>
                                                </UserControls:Grid>
                                                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="BCS.Core.Clearance.Submissions" SelectMethod="GetSubmissionStatusHistory" OldValuesParameterFormatString="original_{0}">
                                                    <SelectParameters>
                                                        <asp:Parameter Type="Int32" DefaultValue="0" Name="submissionId" />
                                                    </SelectParameters>
                                                </asp:ObjectDataSource>
                                            </ContentTemplate>
                                        </aje:UpdatePanel>
                                        <p style="text-align: center;">
                                            <asp:Button ID="OkButton" runat="server" Text="OK" Visible="False" CausesValidation="False" />
                                            <asp:Button ID="CancelButton" runat="server" Text="Done" CausesValidation="False" />
                                        </p>
                                    </div>
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerTemplate>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                    </PagerTemplate>
                </UserControls:Grid>
            </td>
            <td class="MR">
                <div />
            </td>
        </tr>
        <tr>
            <td class="BL">
                <div />
            </td>
            <td class="BC">
                <div />
            </td>
            <td class="BR">
                <div />
            </td>
        </tr>

    </table>
</asp:Content>
