using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SubmissionAgencySearch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ParentAgencyControlId.Value = Request.QueryString["ctl"].ToString();
    }
    protected void gridAgencies_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Control c = e.Row.FindControl("btnSelectAgency");
        if (c != null)
        {
            if (c is WebControl)
            {
                WebControl wc = c as WebControl;
                DataKey dk = gridAgencies.DataKeys[e.Row.RowIndex];
                wc.Attributes.Add("onclick",
                    string.Format("$get('{1}').value = '{0}'; selectAgency('{0}');return false", dk[0], SelectedAgencyId.ClientID));
            }
        }
    }
    protected void btnFilter_Click(object sender, EventArgs e)
    {
        // just cause postback, object datasource will pick inputs
        gridAgencies.PageIndex = 0;
    }
    protected void odsAgencies_Init(object sender, EventArgs e)
    {
        BCS.Core.Security.CustomPrincipal user = (Page.User as BCS.Core.Security.CustomPrincipal);
        odsAgencies.SelectParameters["masterAgencyId"].DefaultValue = 
            user.HasAgency() ? BCS.WebApp.Components.Security.GetMasterAgency(user.Agency).ToString() : "-1";
    }
    protected void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        throw new ApplicationException("Ajax Error", e.Exception);
    }
}
