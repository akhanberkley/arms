<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LFV.aspx.cs" Inherits="LFV" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Berkley Log Framework Viewer</title>
    <style type="text/css">
        .pageTitle
        {
            border-top-width: thin;
            font-weight: bold;
            border-left-width-value: thin;
            border-left-width-ltr-source: physical;
            border-left-width-rtl-source: physical;
            font-size: 20pt;
            border-left-color-value: black;
            border-left-color-ltr-source: physical;
            border-left-color-rtl-source: physical;
            border-bottom-width: thin;
            border-bottom-color: black;
            width: 100%;
            color: white;
            border-top-color: black;
            background-color: #000033;
            text-align: center;
            border-right-width-value: thin;
            border-right-width-ltr-source: physical;
            border-right-width-rtl-source: physical;
            border-right-color-value: black;
            border-right-color-ltr-source: physical;
            border-right-color-rtl-source: physical;
        }
        .ERROR
        {
            color : Red;
        }
        input
        {
	        border: #ab975c 1px solid;
	        font-size: 10pt;
	        filter: progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr= '#FFFFFFFF' , EndColorStr= '#FFEEDDAA' );
        }
    </style>
    <script type="text/javascript">
			function openDetailWindow(data) {
				window.open("LFVDetail.aspx?data=" + data, "Detail", "resizable=yes,scrollbars=yes,location=no,menubar=no");
				return false;
			}
	</script>
</head>
<body>
    <form id="form1" runat="server">    
    <div>
        <span class="pageTitle">Berkley Log Framework Viewer</span>
        <br /><br /><br />
        Select an application:<asp:DropDownList ID="ApplicationDDL" runat="server" AppendDataBoundItems="True"
            AutoPostBack="True" DataSourceID="sdsApps" DataTextField="Application" DataValueField="Application">
            <asp:ListItem Value=""></asp:ListItem>
        </asp:DropDownList><asp:SqlDataSource ID="sdsApps" runat="server" ConnectionString="data source=BTSDEDEVSQL20;initial catalog=bcs_log;password=report;persist security info=True;user id=bcs_logger;pooling=true"
            ProviderName="System.Data.SqlClient" SelectCommand="BLF_GetUniqueApplications"
            SelectCommandType="StoredProcedure" DataSourceMode="DataReader"></asp:SqlDataSource>
        � Environment:
        <asp:DropDownList ID="EnvironmentDDL" runat="server" AutoPostBack="True">
            <asp:ListItem Value="" Text=""></asp:ListItem>
			<asp:ListItem Value="Production">Production</asp:ListItem>
			<asp:ListItem Value="TEST">Test</asp:ListItem>
			<asp:ListItem Value="INT">Integration</asp:ListItem>
			<asp:ListItem Value="DEV">Development</asp:ListItem>
			<asp:ListItem Value="Developer">Developer</asp:ListItem>
        </asp:DropDownList>
        � Level:
        <asp:DropDownList ID="LevelDDL" runat="server" AutoPostBack="True">
            <asp:ListItem Value="" Text=""></asp:ListItem>
			<asp:ListItem Value="FATAL">FATAL</asp:ListItem>
			<asp:ListItem Value="ERROR">ERROR</asp:ListItem>
			<asp:ListItem Value="WARN">WARN</asp:ListItem>
			<asp:ListItem Value="INFO">INFO</asp:ListItem>
        </asp:DropDownList>
        � Page:
        <asp:DropDownList ID="PageDDL" runat="server" AutoPostBack="True">
            <asp:ListItem>1</asp:ListItem><asp:ListItem>2</asp:ListItem><asp:ListItem>3</asp:ListItem><asp:ListItem>4</asp:ListItem><asp:ListItem>5</asp:ListItem>
            <asp:ListItem>6</asp:ListItem><asp:ListItem>7</asp:ListItem><asp:ListItem>8</asp:ListItem><asp:ListItem>9</asp:ListItem><asp:ListItem>10</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <asp:GridView ID="EntriesGrid" runat="server" AutoGenerateColumns="False" DataSourceID="EntriesSource"
         DataKeyNames="Id" AllowPaging="True" OnRowDataBound="EntriesGrid_RowDataBound">
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:Button ID="btnFullDetails" runat="server" CausesValidation="False" CommandName="Select"
                            Text="Full Details" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True"
                    SortExpression="Id" Visible="False"/>
                <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" />
                <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
                <asp:BoundField DataField="Logger" HeaderText="Logger" SortExpression="Logger" Visible="False" />
                <asp:BoundField DataField="Application" HeaderText="Application" SortExpression="Application" />
                <asp:BoundField DataField="Environment" HeaderText="Environment" SortExpression="Environment" />
                <asp:TemplateField HeaderText="Message" SortExpression="Message">
                    <ItemTemplate>
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Exception" HeaderText="Exception" SortExpression="Exception" Visible="False" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="EntriesSource" runat="server" ConnectionString="data source=BTSDEDEVSQL20;initial catalog=bcs_log;password=report;persist security info=True;user id=bcs_logger;pooling=true"
            ProviderName="System.Data.SqlClient" SelectCommand="BLF_GetLogEntriesByApplicationPaged" 
            SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="False" OnSelected="EntriesSource_Selected" >
            <SelectParameters>
                <asp:ControlParameter ControlID="ApplicationDDL" Name="Application" PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="EnvironmentDDL" Name="Environment" PropertyName="SelectedValue" Type="String" DefaultValue="" />
                <asp:ControlParameter ControlID="LevelDDL" Name="Level" PropertyName="SelectedValue" Type="String" DefaultValue="" />
                <asp:ControlParameter ControlID="PageDDL" DefaultValue="1" Name="PageNumber" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="EntriesGrid" DefaultValue="10" Name="PageSize" PropertyName="PageSize"
                    Type="Int32" />
                <asp:Parameter Direction="InputOutput" Name="totalCount" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
