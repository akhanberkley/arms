if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_APSSubmissionClientCoreClientAddress_APSSubmission]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[APSSubmissionClientCoreClientAddress] DROP CONSTRAINT FK_APSSubmissionClientCoreClientAddress_APSSubmission
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_APSSubmissionClientCoreClientName_APSSubmission]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[APSSubmissionClientCoreClientName] DROP CONSTRAINT FK_APSSubmissionClientCoreClientName_APSSubmission
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_APSSubmissionCompanyNumberAgentUnspecifiedReason_APSSubmission]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[APSSubmissionCompanyNumberAgentUnspecifiedReason] DROP CONSTRAINT FK_APSSubmissionCompanyNumberAgentUnspecifiedReason_APSSubmission
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_APSSubmissionCompanyNumberAttributeValue_APSSubmission]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[APSSubmissionCompanyNumberAttributeValue] DROP CONSTRAINT FK_APSSubmissionCompanyNumberAttributeValue_APSSubmission
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_APSSubmissionCompanyNumberAttributeValueHistory_APSSubmission]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[APSSubmissionCompanyNumberAttributeValueHistory] DROP CONSTRAINT FK_APSSubmissionCompanyNumberAttributeValueHistory_APSSubmission
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_APSSubmissionCompanyNumberCode_APSSubmission]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[APSSubmissionCompanyNumberCode] DROP CONSTRAINT FK_APSSubmissionCompanyNumberCode_APSSubmission
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_APSSubmissionCompanyNumberCodeHistory_APSSubmission]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[APSSubmissionCompanyNumberCodeHistory] DROP CONSTRAINT FK_APSSubmissionCompanyNumberCodeHistory_APSSubmission
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_APSSubmissionLineOfBusinessChildren_APSSubmission]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[APSSubmissionLineOfBusinessChildren] DROP CONSTRAINT FK_APSSubmissionLineOfBusinessChildren_APSSubmission
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_APSSubmissionStatusHistory_APSSubmission]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[APSSubmissionStatusHistory] DROP CONSTRAINT FK_APSSubmissionStatusHistory_APSSubmission
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Update_APSSubmissionHistory]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[Update_APSSubmissionHistory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Update_APSSubmissionCompanyNumberAttributeValueHistory]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[Update_APSSubmissionCompanyNumberAttributeValueHistory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Insert_APSSubmissionCompanyNumberCodeHistory]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[Insert_APSSubmissionCompanyNumberCodeHistory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Update_APSSubmissionCompanyNumberCodeHistory]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[Update_APSSubmissionCompanyNumberCodeHistory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Delete_APSSubmissionCompanyNumberCodeHistory]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[Delete_APSSubmissionCompanyNumberCodeHistory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[APSSubmission]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[APSSubmission]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[APSSubmissionClientCoreClientAddress]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[APSSubmissionClientCoreClientAddress]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[APSSubmissionClientCoreClientName]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[APSSubmissionClientCoreClientName]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[APSSubmissionCompanyNumberAgentUnspecifiedReason]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[APSSubmissionCompanyNumberAgentUnspecifiedReason]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[APSSubmissionCompanyNumberAttributeValue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[APSSubmissionCompanyNumberAttributeValue]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[APSSubmissionCompanyNumberAttributeValueHistory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[APSSubmissionCompanyNumberAttributeValueHistory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[APSSubmissionCompanyNumberCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[APSSubmissionCompanyNumberCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[APSSubmissionCompanyNumberCodeHistory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[APSSubmissionCompanyNumberCodeHistory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[APSSubmissionHistory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[APSSubmissionHistory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[APSSubmissionLineOfBusinessChildren]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[APSSubmissionLineOfBusinessChildren]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[APSSubmissionStatusHistory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[APSSubmissionStatusHistory]
GO

CREATE TABLE [dbo].[APSSubmission] (
	[Id] [bigint] IDENTITY (2147483647, 1) NOT NULL ,
	[CompanyNumberId] [int] NOT NULL ,
	[SubmissionTypeId] [int] NOT NULL ,
	[ClientCoreClientId] [bigint] NOT NULL ,
	[ClientCorePortfolioId] [bigint] NULL ,
	[AgencyId] [bigint] NOT NULL ,
	[UnderwritingUnitId] [bigint] NOT NULL ,
	[AgentId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ContactId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SubmissionStatusId] [int] NULL ,
	[SubmissionStatusDate] [datetime] NULL ,
	[CancellationDate] [datetime] NULL ,
	[SubmissionStatusReasonId] [int] NULL ,
	[SubmissionStatusReasonOther] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UnderwriterPersonId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AnalystPersonId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PolicySystemId] [int] NULL ,
	[SubmissionNumber] [bigint] NOT NULL ,
	[Sequence] [int] NOT NULL ,
	[PolicyNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EstimatedWrittenPremium] [money] NULL ,
	[EffectiveDate] [datetime] NULL ,
	[ExpirationDate] [datetime] NULL ,
	[InsuredName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Dba] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Comments] [varchar] (6000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OtherCarrierId] [int] NULL ,
	[OtherCarrierPremium] [money] NULL ,
	[SubmissionBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SubmissionDt] [datetime] NOT NULL ,
	[UpdatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UpdatedDt] [datetime] NULL ,
	[SystemDt] [datetime] NOT NULL ,
	[SystemId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[APSSubmissionClientCoreClientAddress] (
	[SubmissionId] [bigint] NOT NULL ,
	[ClientCoreClientAddressId] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[APSSubmissionClientCoreClientName] (
	[SubmissionId] [bigint] NOT NULL ,
	[ClientCoreNameId] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[APSSubmissionCompanyNumberAgentUnspecifiedReason] (
	[SubmissionId] [bigint] NOT NULL ,
	[CompanyNumberAgentUnspecifiedReasonId] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[APSSubmissionCompanyNumberAttributeValue] (
	[SubmissionId] [bigint] NOT NULL ,
	[CompanyNumberAttributeId] [int] NOT NULL ,
	[AttributeValue] [varchar] (255) COLLATE SQL_Latin1_General_CP437_BIN NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[APSSubmissionCompanyNumberAttributeValueHistory] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[SubmissionId] [bigint] NOT NULL ,
	[CompanyNumberAttributeId] [int] NOT NULL ,
	[AttributeValue] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[By] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[HistoryDt] [datetime] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[APSSubmissionCompanyNumberCode] (
	[SubmissionId] [bigint] NOT NULL ,
	[CompanyNumberCodeId] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[APSSubmissionCompanyNumberCodeHistory] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[SubmissionId] [bigint] NOT NULL ,
	[Action] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CompanyNumberCodeId] [int] NOT NULL ,
	[By] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[HistoryDt] [datetime] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[APSSubmissionHistory] (
	[Id] [bigint] IDENTITY (2147483647, 1) NOT NULL ,
	[SubmissionId] [bigint] NOT NULL ,
	[CompanyNumberId] [int] NOT NULL ,
	[SubmissionTypeId] [int] NOT NULL ,
	[ClientCoreClientId] [bigint] NOT NULL ,
	[ClientCorePortfolioId] [bigint] NULL ,
	[AgencyId] [bigint] NOT NULL ,
	[UnderwritingUnitId] [bigint] NOT NULL ,
	[AgentId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ContactId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SubmissionStatusId] [int] NULL ,
	[SubmissionStatusDate] [datetime] NULL ,
	[CancellationDate] [datetime] NULL ,
	[SubmissionStatusReasonId] [int] NULL ,
	[SubmissionStatusReasonOther] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UnderwriterPersonId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AnalystPersonId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PolicySystemId] [int] NULL ,
	[SubmissionNumber] [bigint] NOT NULL ,
	[Sequence] [int] NOT NULL ,
	[PolicyNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EstimatedWrittenPremium] [money] NULL ,
	[EffectiveDate] [datetime] NULL ,
	[ExpirationDate] [datetime] NULL ,
	[InsuredName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Dba] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Comments] [varchar] (6000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OtherCarrierId] [int] NULL ,
	[OtherCarrierPremium] [money] NULL ,
	[SubmissionBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SubmissionDt] [datetime] NOT NULL ,
	[UpdatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UpdatedDt] [datetime] NULL ,
	[SystemDt] [datetime] NOT NULL ,
	[SystemId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HistoryDt] [datetime] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[APSSubmissionLineOfBusinessChildren] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[SubmissionId] [bigint] NOT NULL ,
	[CompanyNumberLOBGridId] [int] NOT NULL ,
	[SubmissionStatusId] [int] NULL ,
	[SubmissionStatusReasonId] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[APSSubmissionStatusHistory] (
	[Id] [int] IDENTITY (1, 1) NOT NULL ,
	[SubmissionId] [bigint] NOT NULL ,
	[PreviousSubmissionStatusId] [int] NULL ,
	[SubmissionStatusDate] [datetime] NULL ,
	[StatusChangeDt] [datetime] NOT NULL ,
	[StatusChangeBy] [varchar] (50) COLLATE SQL_Latin1_General_CP437_BIN NOT NULL ,
	[Active] [bit] NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[APSSubmission] WITH NOCHECK ADD 
	CONSTRAINT [PK_APSSubmission] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[APSSubmissionClientCoreClientAddress] WITH NOCHECK ADD 
	CONSTRAINT [PK_APSSubmissionClientCoreClientAddress] PRIMARY KEY  CLUSTERED 
	(
		[SubmissionId],
		[ClientCoreClientAddressId]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[APSSubmissionClientCoreClientName] WITH NOCHECK ADD 
	CONSTRAINT [PK_APSSubmissionClientCoreName] PRIMARY KEY  CLUSTERED 
	(
		[SubmissionId],
		[ClientCoreNameId]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[APSSubmissionCompanyNumberAgentUnspecifiedReason] WITH NOCHECK ADD 
	CONSTRAINT [PK_APSSubmissionAgentUnspecifiedReason] PRIMARY KEY  CLUSTERED 
	(
		[SubmissionId],
		[CompanyNumberAgentUnspecifiedReasonId]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[APSSubmissionCompanyNumberAttributeValue] WITH NOCHECK ADD 
	CONSTRAINT [PK_APSSubmissionCompanyNumberAttributeValue] PRIMARY KEY  CLUSTERED 
	(
		[SubmissionId],
		[CompanyNumberAttributeId]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[APSSubmissionCompanyNumberAttributeValueHistory] WITH NOCHECK ADD 
	CONSTRAINT [PK_APSSubmissionCompanyNumberAttributeValueHistory] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[APSSubmissionCompanyNumberCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_APSSubmissionCompanyCode] PRIMARY KEY  CLUSTERED 
	(
		[SubmissionId],
		[CompanyNumberCodeId]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[APSSubmissionCompanyNumberCodeHistory] WITH NOCHECK ADD 
	CONSTRAINT [PK_APSSubmissionCompanyNumberCodeHistory] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[APSSubmissionHistory] WITH NOCHECK ADD 
	CONSTRAINT [PK_APSSubmissionHistory] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[APSSubmissionLineOfBusinessChildren] WITH NOCHECK ADD 
	CONSTRAINT [PK_APSSubmissionLineOfBusinessChildren] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[APSSubmissionStatusHistory] WITH NOCHECK ADD 
	CONSTRAINT [PK_APSSubmissionStatusHistory] PRIMARY KEY  CLUSTERED 
	(
		[Id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[APSSubmission] ADD 
	CONSTRAINT [DF_APSSubmission_SubmissionDt] DEFAULT (getdate()) FOR [SubmissionDt],
	CONSTRAINT [DF_APSSubmission_SystemDt] DEFAULT (getdate()) FOR [SystemDt]
GO

 CREATE  UNIQUE  INDEX [IX_UniqueAPSSubmissionNumberANDSequence] ON [dbo].[APSSubmission]([CompanyNumberId], [SubmissionNumber], [Sequence]) ON [PRIMARY]
GO

ALTER TABLE [dbo].[APSSubmissionCompanyNumberAttributeValueHistory] ADD 
	CONSTRAINT [DF_APSSubmissionCompanyNumberAttributeValueHistory_HistoryDt] DEFAULT (getdate()) FOR [HistoryDt]
GO

ALTER TABLE [dbo].[APSSubmissionCompanyNumberCodeHistory] ADD 
	CONSTRAINT [DF_APSSubmissionCompanyNumberCodeHistory_HistoryDt] DEFAULT (getdate()) FOR [HistoryDt]
GO

ALTER TABLE [dbo].[APSSubmissionStatusHistory] ADD 
	CONSTRAINT [DF_APSSubmissionStatusHistory_StatusChangeDt] DEFAULT (getdate()) FOR [StatusChangeDt],
	CONSTRAINT [DF_APSSubmissionStatusHistory_Active] DEFAULT (1) FOR [Active]
GO

ALTER TABLE [dbo].[APSSubmission] ADD 
	CONSTRAINT [FK_APSSubmission_CompanyNumber] FOREIGN KEY 
	(
		[CompanyNumberId]
	) REFERENCES [dbo].[CompanyNumber] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmission_OtherCarrier] FOREIGN KEY 
	(
		[OtherCarrierId]
	) REFERENCES [dbo].[OtherCarrier] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmission_SubmissionStatus] FOREIGN KEY 
	(
		[SubmissionStatusId]
	) REFERENCES [dbo].[SubmissionStatus] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmission_SubmissionStatusReason] FOREIGN KEY 
	(
		[SubmissionStatusReasonId]
	) REFERENCES [dbo].[SubmissionStatusReason] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmission_SubmissionType] FOREIGN KEY 
	(
		[SubmissionTypeId]
	) REFERENCES [dbo].[SubmissionType] (
		[Id]
	)
GO

ALTER TABLE [dbo].[APSSubmissionClientCoreClientAddress] ADD 
	CONSTRAINT [FK_APSSubmissionClientCoreClientAddress_APSSubmission] FOREIGN KEY 
	(
		[SubmissionId]
	) REFERENCES [dbo].[APSSubmission] (
		[Id]
	)
GO

ALTER TABLE [dbo].[APSSubmissionClientCoreClientName] ADD 
	CONSTRAINT [FK_APSSubmissionClientCoreClientName_APSSubmission] FOREIGN KEY 
	(
		[SubmissionId]
	) REFERENCES [dbo].[APSSubmission] (
		[Id]
	)
GO

ALTER TABLE [dbo].[APSSubmissionCompanyNumberAgentUnspecifiedReason] ADD 
	CONSTRAINT [FK_APSSubmissionCompanyNumberAgentUnspecifiedReason_APSSubmission] FOREIGN KEY 
	(
		[SubmissionId]
	) REFERENCES [dbo].[APSSubmission] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmissionCompanyNumberAgentUnspecifiedReason_CompanyNumberAgentUnspecifiedReason] FOREIGN KEY 
	(
		[CompanyNumberAgentUnspecifiedReasonId]
	) REFERENCES [dbo].[CompanyNumberAgentUnspecifiedReason] (
		[Id]
	)
GO

ALTER TABLE [dbo].[APSSubmissionCompanyNumberAttributeValue] ADD 
	CONSTRAINT [FK_APSSubmissionCompanyNumberAttributeValue_APSSubmission] FOREIGN KEY 
	(
		[SubmissionId]
	) REFERENCES [dbo].[APSSubmission] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmissionCompanyNumberAttributeValue_CompanyNumberAttribute] FOREIGN KEY 
	(
		[CompanyNumberAttributeId]
	) REFERENCES [dbo].[CompanyNumberAttribute] (
		[Id]
	)
GO

ALTER TABLE [dbo].[APSSubmissionCompanyNumberAttributeValueHistory] ADD 
	CONSTRAINT [FK_APSSubmissionCompanyNumberAttributeValueHistory_APSSubmission] FOREIGN KEY 
	(
		[SubmissionId]
	) REFERENCES [dbo].[APSSubmission] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmissionCompanyNumberAttributeValueHistory_CompanyNumberAttribute] FOREIGN KEY 
	(
		[CompanyNumberAttributeId]
	) REFERENCES [dbo].[CompanyNumberAttribute] (
		[Id]
	)
GO

ALTER TABLE [dbo].[APSSubmissionCompanyNumberCode] ADD 
	CONSTRAINT [FK_APSSubmissionCompanyCode_CompanyCode] FOREIGN KEY 
	(
		[CompanyNumberCodeId]
	) REFERENCES [dbo].[CompanyNumberCode] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmissionCompanyNumberCode_APSSubmission] FOREIGN KEY 
	(
		[SubmissionId]
	) REFERENCES [dbo].[APSSubmission] (
		[Id]
	)
GO

ALTER TABLE [dbo].[APSSubmissionCompanyNumberCodeHistory] ADD 
	CONSTRAINT [FK_APSSubmissionCompanyNumberCodeHistory_APSSubmission] FOREIGN KEY 
	(
		[SubmissionId]
	) REFERENCES [dbo].[APSSubmission] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmissionCompanyNumberCodeHistory_CompanyNumberCode] FOREIGN KEY 
	(
		[CompanyNumberCodeId]
	) REFERENCES [dbo].[CompanyNumberCode] (
		[Id]
	)
GO

ALTER TABLE [dbo].[APSSubmissionLineOfBusinessChildren] ADD 
	CONSTRAINT [FK_APSSubmissionLineOfBusinessChildren_APSSubmission] FOREIGN KEY 
	(
		[SubmissionId]
	) REFERENCES [dbo].[APSSubmission] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmissionLineOfBusinessChildren_CompanyNumberLOBGrid] FOREIGN KEY 
	(
		[CompanyNumberLOBGridId]
	) REFERENCES [dbo].[CompanyNumberLOBGrid] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmissionLineOfBusinessChildren_SubmissionStatus] FOREIGN KEY 
	(
		[SubmissionStatusId]
	) REFERENCES [dbo].[SubmissionStatus] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmissionLineOfBusinessChildren_SubmissionStatusReason] FOREIGN KEY 
	(
		[SubmissionStatusReasonId]
	) REFERENCES [dbo].[SubmissionStatusReason] (
		[Id]
	)
GO

ALTER TABLE [dbo].[APSSubmissionStatusHistory] ADD 
	CONSTRAINT [FK_APSSubmissionStatusHistory_APSSubmission] FOREIGN KEY 
	(
		[SubmissionId]
	) REFERENCES [dbo].[APSSubmission] (
		[Id]
	),
	CONSTRAINT [FK_APSSubmissionStatusHistory_SubmissionStatus] FOREIGN KEY 
	(
		[PreviousSubmissionStatusId]
	) REFERENCES [dbo].[SubmissionStatus] (
		[Id]
	)
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE   TRIGGER dbo.Update_APSSubmissionHistory
ON dbo.APSSubmission 
FOR UPDATE
AS
INSERT dbo.APSSubmissionHistory  
	(
	SubmissionId, CompanyNumberId,	SubmissionTypeId, ClientCoreClientId, ClientCorePortfolioId, AgencyId, UnderwritingUnitId,
	AgentId, ContactId,  SubmissionStatusId, SubmissionStatusDate, CancellationDate, SubmissionStatusReasonId, SubmissionStatusReasonOther, UnderwriterPersonId, AnalystPersonId, PolicySystemId, SubmissionNumber,
	Sequence, PolicyNumber, EstimatedWrittenPremium, EffectiveDate, ExpirationDate, InsuredName, Dba, Comments, OtherCarrierId, OtherCarrierPremium, SubmissionBy, SubmissionDt,
	UpdatedBy, UpdatedDt, SystemDt, SystemId, HistoryDt
	)
SELECT 	
	Id, CompanyNumberId, SubmissionTypeId, ClientCoreClientId, ClientCorePortfolioId, AgencyId, UnderwritingUnitId, AgentId, ContactId, SubmissionStatusId,SubmissionStatusDate,
    CancellationDate, SubmissionStatusReasonId, SubmissionStatusReasonOther, UnderwriterPersonId, AnalystPersonId, PolicySystemId, SubmissionNumber, [Sequence], PolicyNumber,
EstimatedWrittenPremium, EffectiveDate, ExpirationDate, InsuredName, Dba, Comments, OtherCarrierId, OtherCarrierPremium, SubmissionBy, SubmissionDt, UpdatedBy, UpdatedDt, SystemDt, SystemId, GETDATE()
FROM deleted


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



CREATE TRIGGER [dbo].[Update_APSSubmissionCompanyNumberAttributeValueHistory] 
ON [dbo].[APSSubmissionCompanyNumberAttributeValue] 
FOR UPDATE
AS 

 DECLARE @id bigint;
 DECLARE @value varchar(255);
 DECLARE @cnaid int;
 DECLARE @by varchar(50);
 
 SET @id = (SELECT SubmissionId FROM deleted);
 SET @value = (SELECT AttributeValue FROM deleted);
 SET @cnaid = (SELECT CompanyNumberAttributeId FROM deleted);
 SET @by = (SELECT UpdatedBy FROM dbo.APSSubmission WHERE Id = @id);
 
 INSERT dbo.APSSubmissionCompanyNumberAttributeValueHistory  
 ( SubmissionId, CompanyNumberAttributeId, AttributeValue, HistoryDt, [By] )
 
 VALUES ( @id, @cnaid, @value, GETDATE(), @by);






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE TRIGGER [dbo].[Insert_APSSubmissionCompanyNumberCodeHistory] 
ON [dbo].[APSSubmissionCompanyNumberCode] 
FOR INSERT
AS
 -- variable to hold just inserted submissionid
 DECLARE @id bigint;
 -- variable to hold Submission date
 DECLARE @sdt datetime;
 -- variable to hold Update date
 DECLARE @udt datetime;
 DECLARE @by varchar(50);
  
 SET @id = (SELECT SubmissionId FROM inserted);
 SET @sdt = (SELECT SubmissionDt FROM APSSubmission  where Id=@id);
 SET @udt = (SELECT UpdatedDt FROM APSSubmission  where Id=@id); 
 SET @by = (SELECT UpdatedBy FROM dbo.APSSubmission WHERE Id = @id);
 
 IF (CAST(@sdt AS smalldatetime) <> CAST(@udt AS smalldatetime))
  BEGIN
   DECLARE @action varchar(10);
   DECLARE @cncid int;
   SET @action = 'Added';
   SET @cncid = (SELECT CompanyNumberCodeId FROM inserted);
   
   INSERT dbo.APSSubmissionCompanyNumberCodeHistory  
   ( SubmissionId, Action , CompanyNumberCodeId , [By] )
 
   VALUES ( @id, @action, @cncid, @by);
  END

SET ANSI_NULLS ON




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE TRIGGER [dbo].[Update_APSSubmissionCompanyNumberCodeHistory] 
ON [dbo].[APSSubmissionCompanyNumberCode] 
FOR UPDATE
AS

 DECLARE @id bigint;
 DECLARE @action varchar(10);
 DECLARE @cncid int;
 DECLARE @by varchar(50);
 
 SET @id = (SELECT SubmissionId FROM deleted);
 SET @action = 'Updated';
 SET @cncid = (SELECT CompanyNumberCodeId FROM deleted);
 SET @by = (SELECT UpdatedBy FROM dbo.APSSubmission WHERE Id = @id);
 
 INSERT dbo.APSSubmissionCompanyNumberCodeHistory  
 ( SubmissionId, Action , CompanyNumberCodeId , [By] )
 
 VALUES ( @id, @action, @cncid, @by);




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



CREATE TRIGGER [dbo].[Delete_APSSubmissionCompanyNumberCodeHistory] 
ON [dbo].[APSSubmissionCompanyNumberCode] 
FOR DELETE
AS

 DECLARE @id bigint;
 DECLARE @action varchar(10);
 DECLARE @cncid int;
 DECLARE @by varchar(50);
 
 SET @id = (SELECT SubmissionId FROM deleted);
 SET @action = 'Deleted';
 SET @cncid = (SELECT CompanyNumberCodeId FROM deleted);
 SET @by = (SELECT UpdatedBy FROM dbo.APSSubmission WHERE Id = @id);
 
 INSERT dbo.APSSubmissionCompanyNumberCodeHistory  
 ( SubmissionId, Action , CompanyNumberCodeId , [By] )
 
 VALUES ( @id, @action, @cncid, @by);




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

