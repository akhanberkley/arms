CREATE 
  INDEX [IX_Submission_CCAddress] ON [dbo].[SubmissionClientCoreClientAddress] ([SubmissionId])

CREATE 
  INDEX [IX_Submission_CCName] ON [dbo].[SubmissionClientCoreClientName] ([SubmissionId])

CREATE 
  INDEX [IX_Submission_AgentReasons] ON [dbo].[SubmissionCompanyNumberAgentUnspecifiedReason] ([SubmissionId])

CREATE 
  INDEX [IX_Submission_Attribute] ON [dbo].[SubmissionCompanyNumberAttributeValue] ([SubmissionId])

CREATE 
  INDEX [IX_Submission_CompanyCode] ON [dbo].[SubmissionCompanyNumberCode] ([SubmissionId])

CREATE 
  INDEX [IX_Submission_History] ON [dbo].[SubmissionHistory] ([SubmissionId])

CREATE 
  INDEX [IX_Submission_LineOfBusiness] ON [dbo].[SubmissionLineOfBusiness] ([SubmissionId])

CREATE 
  INDEX [IX_Submission_LineOfBusinessChildren] ON [dbo].[SubmissionLineOfBusinessChildren] ([SubmissionId])
