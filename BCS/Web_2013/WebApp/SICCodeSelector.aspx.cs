using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SICCodeSelector : System.Web.UI.Page
{
    BCS.Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!Page.IsPostBack)
        {
            dm.QueryCriteria.Clear();
            BCS.Biz.SicDivisionCollection collection = dm.GetSicDivisionCollection();
            collection = collection.SortByDivisionDescription(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.SicDivision div in collection)
            {
                ddlDivisions.Items.Add(new ListItem(div.DivisionDescription,div.Id.ToString()));
            }
            ddlDivisions.Items.Insert(0, new ListItem("","0"));

            ddlCodes.Items.Clear();
            dm.QueryCriteria.Clear();
            BCS.Biz.SicCodeListCollection codecollection = dm.GetSicCodeListCollection();
            codecollection = codecollection.SortByCodeDescription(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.SicCodeList var in codecollection)
            {
                ddlCodes.Items.Add(new ListItem(string.Format("{0} (# {1})", var.CodeDescription, var.Code), string.Format("{0}|{1}", var.Id, var.Code)));
            }
            ddlCodes.Items.Insert(0, new ListItem("", "0| "));
        }
    }
    protected void ddlDivisions_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlGroups.Items.Clear();
        dm.QueryCriteria.Clear();
        dm.QueryCriteria.And(BCS.Biz.JoinPath.SicGroup.Columns.SicDivisionId, ddlDivisions.SelectedValue);
        BCS.Biz.SicGroupCollection collection = dm.GetSicGroupCollection();
        collection = collection.SortByGroupDescription(OrmLib.SortDirection.Ascending);
        foreach (BCS.Biz.SicGroup var in collection)
        {
            ddlGroups.Items.Add(new ListItem(var.GroupDescription,var.Id.ToString()));
        }
        ddlGroups.Items.Insert(0, new ListItem("", "0"));


        ddlCodes.Items.Clear();
        dm.QueryCriteria.Clear();
        if (ddlGroups.SelectedValue != "0")
            dm.QueryCriteria.And(BCS.Biz.JoinPath.SicCodeList.Columns.SicGroupId, ddlGroups.SelectedValue);
        else
        {
            if (ddlDivisions.SelectedValue != "0")
                dm.QueryCriteria.And(BCS.Biz.JoinPath.SicCodeList.SicGroup.Columns.SicDivisionId, ddlDivisions.SelectedValue);
        }
        BCS.Biz.SicCodeListCollection collection_to_loop = dm.GetSicCodeListCollection();
        collection_to_loop = collection_to_loop.SortByCodeDescription(OrmLib.SortDirection.Ascending);
        foreach (BCS.Biz.SicCodeList var in collection_to_loop)
        {
            ddlCodes.Items.Add(new ListItem(string.Format("{0} (# {1})", var.CodeDescription, var.Code), string.Format("{0}|{1}", var.Id, var.Code)));
        }
        ddlCodes.Items.Insert(0, new ListItem("", "0| "));
    }
    protected void ddlGroups_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlCodes.Items.Clear();
        dm.QueryCriteria.Clear();
        if ( ddlGroups.SelectedValue != "0")
            dm.QueryCriteria.And(BCS.Biz.JoinPath.SicCodeList.Columns.SicGroupId, ddlGroups.SelectedValue);
        BCS.Biz.SicCodeListCollection collection = dm.GetSicCodeListCollection();
        collection.SortByCodeDescription(OrmLib.SortDirection.Ascending);
        foreach (BCS.Biz.SicCodeList var in collection)
        {
            ddlCodes.Items.Add(new ListItem(string.Format("{0} (# {1})", var.CodeDescription, var.Code), string.Format("{0}|{1}", var.Id, var.Code)));
        }
        ddlCodes.Items.Insert(0, new ListItem("", "0| "));
    }
    protected void ddlCodes_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlNaicsCodes.Items.Clear();
        dm.QueryCriteria.Clear();
        if (ddlCodes.SelectedValue.Split('|')[0] != "0")
            dm.QueryCriteria.And(BCS.Biz.JoinPath.NaicsCodeList.Columns.SicCodeListId, ddlCodes.SelectedValue.Split('|')[0]);
        BCS.Biz.NaicsCodeListCollection collection = dm.GetNaicsCodeListCollection();
        collection.SortByCodeDescription(OrmLib.SortDirection.Ascending);
        foreach (BCS.Biz.NaicsCodeList var in collection)
        {
            ddlNaicsCodes.Items.Add(new ListItem(String.Format("{0} (#{1})", var.CodeDescription, var.Code), String.Format("{0}|{1}", var.Id, var.Code)));
        }
        ddlNaicsCodes.Items.Insert(0, new ListItem("", "0| "));
    }
}
