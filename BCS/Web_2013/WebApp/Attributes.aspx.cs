using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Attributes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        gridAttributes.SelectedIndex = -1;
        DetailsView1.ChangeMode(DetailsViewMode.Insert);
    }
    protected void gridAttributes_PageIndexChanged(object sender, EventArgs e)
    {
        gridAttributes.SelectedIndex = -1;
    }
    protected void gridAttributes_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        gridAttributes.DataBind();
        DetailsView1.DataBind();
    }
    protected void gridAttributes_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        if (!BCS.WebApp.Components.Security.IsUserReadOnly())
            DetailsView1.ChangeMode(DetailsViewMode.Edit);
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        gridAttributes.DataBind();
        DetailsView1.DataBind();
    }
    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        gridAttributes.DataBind();
        DetailsView1.DataBind();
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        bool b = Common.ShouldUpdate(e);        

        e.Cancel = b;

        if (b)
        {
            // there was nothing to update. Just show the user the success message.
            Common.SetStatus(messagePlaceHolder, "Company Number Attribute Sucessfully updated.");
        }
    }
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        e.Cancel = BCS.WebApp.Components.Security.IsUserReadOnly();
    }
    protected void ObjectDataSource2_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }

    }
    protected void ObjectDataSource2_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void ObjectDataSource1_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void Sort_PreRender(object sender, EventArgs e)
    {
        ListControl senderControl = (sender as ListControl);
        ListItem[] items = Common.SortListItems(senderControl.Items);
        senderControl.Items.Clear();
        senderControl.Items.AddRange(items);
    }
    protected void SetRange(object sender, EventArgs e)
    {
        (sender as RangeValidator).MinimumValue = "0";
        (sender as RangeValidator).MaximumValue = "100";
    }
}
