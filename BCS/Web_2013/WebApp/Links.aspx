<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="Links.aspx.cs" Inherits="Links" Title="Berkley Clearance System - Resource Links" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<ajx:ToolkitScriptManager id="ScriptManager1" runat="server"></ajx:ToolkitScriptManager>
<table cellpadding="0" cellspacing="0" class="box" width="70%">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Resource Links</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC" style="width: 100%;">
        <%--content--%>
            <UserControls:Grid ID="Grid1" runat="server" DataSourceID="ObjectDataSource1" AutoGenerateColumns="False" DataKeyNames="Id"
                ShowHeader="False" Width="100%" AllowPaging="True"
                OnSelectedIndexChanging="Grid1_SelectedIndexChanging" EmptyDataText="No Links found.">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <table width="100%">
                            <tr>
                                <td align="left" style="width: 50px;">
                                    <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CommandName="Select" Text="Edit"
                                        Visible='<%$ Code: BCS.WebApp.Components.Security.CanDelete() %>' style="width: 40px;"/>
                                </td>
                                <td align="left" style="">
                                    <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                    <br />
                                    <asp:HyperLink Id="lnkUrl" runat="server" Text='<%# Bind("Url") %>' NavigateUrl='<%# Bind("Url") %>' Target="_blank">
                                    </asp:HyperLink>
                                </td>
                                <td align="right" style="width: 50px;">
                                    <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                         Text="Delete" Visible='<%$ Code: BCS.WebApp.Components.Security.CanDelete() %>'/>
                                    <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="btnDelete"
                                        ConfirmText='<%# DataBinder.Eval(Container.DataItem, "Url", "Are you sure you want to delete this link \"{0}\"?")%> '>
                                    </ajx:ConfirmButtonExtender>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <br />
                    </ItemTemplate>                    
                 </asp:TemplateField>
            </Columns>
                <PagerTemplate>
                    <asp:PlaceHolder runat="server"></asp:PlaceHolder>
                </PagerTemplate>
            </UserControls:Grid>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
                DeleteMethod="DeleteLink" SelectMethod="GetLinks" TypeName="BCS.Core.Clearance.Admin.DataAccess">
                <DeleteParameters>
                    <asp:Parameter Name="id" Type="Int32" />
                </DeleteParameters>
                <SelectParameters>
                    <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br id="Br3" runat="server" visible='<%$ Code: BCS.WebApp.Components.Security.CanDelete() %>' />
            <asp:Button ID="btnAdd" Visible='<%$ Code: BCS.WebApp.Components.Security.CanDelete() %>' runat="server" Text="Add"  OnClick="btnAdd_Click" AccessKey="A" />
            <br id="Br4" runat="server" visible='<%$ Code: BCS.WebApp.Components.Security.CanDelete() %>' />
            <br id="Br5" runat="server" visible='<%$ Code: BCS.WebApp.Components.Security.CanDelete() %>' />            
            <asp:DetailsView ID="DetailsView1" runat="server" DataSourceID="ObjectDataSource2" DataKeyNames="Id" AutoGenerateRows="False"
                Width="100%"                
                OnItemInserting="DetailsView1_ItemInserting"
                OnItemUpdated="DetailsView1_ItemUpdated"
                OnItemInserted="DetailsView1_ItemInserted">
                <Fields>
                    <asp:TemplateField HeaderText="Url">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUrl" runat="server" Text='<%# Bind("Url") %>' Columns="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvUrl" runat="server" ControlToValidate="txtUrl"
                                Display="None" ErrorMessage="Url is Required." Text="" SetFocusOnError="True" ></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtUrl"
                                Display="None" ErrorMessage="Invalid Url" SetFocusOnError="True"
                                ValidationExpression="http(s)?://([[\w\.-]+]{0,1})+[\w-]+\.[\w-]+([:[0-9]{1,6}]{0,1})?(/[\w- ./?%&=]*)?|http(s)?://[\w-]+([:[0-9]{1,6}]{0,1})?(/[\w- ./?%&=]*)?">
                            </asp:RegularExpressionValidator>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="txtUrl" runat="server" Text='<%# Bind("Url") %>' Columns="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvUrl" runat="server" ControlToValidate="txtUrl"
                                Display="None" ErrorMessage="Url is Required." Text="" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtUrl"
                                Display="None" ErrorMessage="Invalid Url" SetFocusOnError="True"
                                ValidationExpression="http(s)?://([[\w\.-]+]{0,1})+[\w-]+\.[\w-]+([:[0-9]{1,6}]{0,1})?(/[\w- ./?%&=]*)?|http(s)?://[\w-]+([:[0-9]{1,6}]{0,1})?(/[\w- ./?%&=]*)?">
                            </asp:RegularExpressionValidator>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCode" runat="server" Text='<%# Bind("Url") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDesc" runat="server" Text='<%# Bind("Description") %>' Columns="100"></asp:TextBox>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="txtDesc" runat="server" Text='<%# Bind("Description") %>' Columns="100"></asp:TextBox>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Display Order">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtOrder" runat="server" Text='<%# Bind("DisplayOrder") %>' Columns="3"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvOrder" runat="server" ControlToValidate="txtOrder"
                                Display="None" ErrorMessage="Display order is Required." Text="" SetFocusOnError="True" ></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvOrder" runat="server" ErrorMessage="Display order should be numberic only." ControlToValidate="txtOrder"
                                Operator="DataTypeCheck" Type="Integer" Display="None" SetFocusOnError="True" ></asp:CompareValidator>
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:TextBox ID="txtOrder" runat="server" Text='<%# Bind("DisplayOrder") %>' Columns="3"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvOrder" runat="server" ControlToValidate="txtOrder"
                                Display="None" ErrorMessage="Display order is Required." Text="" SetFocusOnError="True" ></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvOrder" runat="server" ErrorMessage="Display order should be numberic only." ControlToValidate="txtOrder"
                                Operator="DataTypeCheck" Type="Integer" Display="None" SetFocusOnError="True" ></asp:CompareValidator>
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblOrder" runat="server" Text='<%# Bind("DisplayOrder") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:Button ID="Button1" runat="server" CausesValidation="True" CommandName="Update"
                                OnInit="ValidationButton_Init" Text="Update" AccessKey="S" />&nbsp;<asp:Button ID="Button2" runat="server"
                                    CausesValidation="False" CommandName="Cancel" Text="Cancel" AccessKey="C" />
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:Button ID="Button1" runat="server" CausesValidation="True" CommandName="Insert"
                                OnInit="ValidationButton_Init" Text="Insert" AccessKey="S" />&nbsp;<asp:Button ID="Button2" runat="server"
                                    CausesValidation="False" CommandName="Cancel" Text="Cancel" AccessKey="C" />
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Button ID="Button1" runat="server" CausesValidation="False" CommandName="Edit"
                                OnInit="ValidationButton_Init" Text="Edit" AccessKey="S" />&nbsp;<asp:Button ID="Button2" runat="server"
                                    CausesValidation="False" CommandName="New" Text="New" AccessKey="N" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Fields>
            </asp:DetailsView>        
            <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" DataObjectTypeName="BCS.Core.Clearance.BizObjects.Link"
                InsertMethod="AddLink" SelectMethod="GetLink" TypeName="BCS.Core.Clearance.Admin.DataAccess"
                UpdateMethod="UpdateLink"
                OnInserted="ObjectDataSource2_Inserted"
                OnUpdated="ObjectDataSource2_Updated">
                <SelectParameters>
                    <asp:ControlParameter ControlID="Grid1" Name="id" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        <%--end content--%>
        </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
<table id="ValidationTable" runat="server" cellpadding="0" cellspacing="0" class="box" width="30%" style="display: none;">
       
        <tr>
            <td class="TL"><div /></td>
            <td class="TC">Validation</td>
            <td class="TR"><div /></td>
        </tr>
        <tr>
            <td class="ML"><div /></td>
            <td class="MC" style="width: 100%">
                <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" SkinID="NewOneToBeDefaulted"></asp:ValidationSummary>
                </asp:PlaceHolder>
            </td>
            <td class="MR"><div /></td>
        </tr>
        <tr>
            <td class="BL"><div/></td>
            <td class="BC"><div/></td>
            <td class="BR"><div/></td>
        </tr>
       
    </table>
    <ajx:AlwaysVisibleControlExtender ID="avceVerify" runat="server"
        BehaviorID="alwaysVisible"
        TargetControlID="ValidationTable"
        VerticalOffset="100"
        HorizontalSide="Right">
    </ajx:AlwaysVisibleControlExtender>
</asp:Content>

