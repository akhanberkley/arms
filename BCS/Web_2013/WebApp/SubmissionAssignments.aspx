<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="SubmissionAssignments.aspx.cs"
    Inherits="SubmissionAssignments" Title="Berkley Clearance System : Reassign Submissions"
    Theme="defaultTheme" %>
<%@ Register TagPrefix="UserControls" Namespace="BCS.WebApp.UserControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<ajx:ToolkitScriptManager ID="ScriptManager1" runat="server" CombineScripts="True" >
</ajx:ToolkitScriptManager>
<script>
    function changeState(chboxid)
    {
      var Hidden1= document.getElementById('<%=Hidden1.ClientID%>');
      if ( document.getElementById(chboxid.id).checked )
      {
        var value = Hidden1.value;
        value++;
        Hidden1.value = value;
        //alert(value);
      }
      else
      {
        var value = Hidden1.value;
        value--;
        if (value<0)
            value=0;
        Hidden1.value = value;
        
        var selectAllState = document.getElementById('<%=selectAllState.ClientID%>');
        selectAllState.value = 0;
        
        //alert(value);
      }        
       // var xxx = document.getElementById('xxxxx').checked;        
    }
    
    function selectedAll(chboxid)
    {
        var control = document.getElementById('<%=selectAllState.ClientID%>');
        if ( document.getElementById(chboxid.id).checked )
        {
            control.value = 1;
            //alert(value);
        }
        else
        {
            control.value = 0;            
            //alert(value);
        }
    }
    
    function ValidateAndConfirm(controlToValidate, confirmationMessage)
    {
        if(document.getElementById(controlToValidate.id).value == "0")
            return false;
      window.event.returnValue = confirm(confirmationMessage);
    }
</script>


    <table id="ValidationTable" runat="server" cellpadding="0" cellspacing="0" class="box" width="30%" style="display: none;">
       
        <tr>
            <td class="TL"><div /></td>
            <td class="TC">Validation</td>
            <td class="TR"><div /></td>
        </tr>
        <tr>
            <td class="ML"><div /></td>
            <td class="MC" style="width: 100%">
                <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="Reassign"></asp:ValidationSummary>
                </asp:PlaceHolder>
            </td>
            <td class="MR"><div /></td>
        </tr>
        <tr>
            <td class="BL"><div/></td>
            <td class="BC"><div/></td>
            <td class="BR"><div/></td>
        </tr>
        
    </table>
<ajx:AlwaysVisibleControlExtender ID="avceVerify" runat="server"
    BehaviorID="alwaysVisible"
    TargetControlID="ValidationTable"
    VerticalOffset="100"
    HorizontalSide="Right">
</ajx:AlwaysVisibleControlExtender>
<table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Reassign Submissions</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        
    <table id="atable" cellpadding="2">
        <tr>
            <td colspan="3" style="border:solid 1px green; margin: 5px; padding: 5px;">
                Driven By:
                <b> <asp:RadioButtonList ID="DrivenByRBL" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="True"
                    OnSelectedIndexChanged="DrivenByRBL_SelectedIndexChanged">
                    <asp:ListItem Text="Agency" Value="Agency" Selected="True"></asp:ListItem>
                    <asp:ListItem Text='<%$ Code : UnderwritingRoleLabel.GetText("UW") %>' Value="Underwriter"></asp:ListItem>
                </asp:RadioButtonList></b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                Select role to reassign:
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                    <asp:ListItem Text='<%$ Code : UnderwritingRoleLabel.GetText("UW") %>' Value="UnderwriterPersonId"></asp:ListItem>
                    <asp:ListItem Text='<%$ Code : UnderwritingRoleLabel.GetText("AN") %>' Value="AnalystPersonId"></asp:ListItem>
                    <asp:ListItem Text='<%$ Code : UnderwritingRoleLabel.GetText("TN") %>' Value="TechnicianPersonId"></asp:ListItem>
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RadioButtonList1" SetFocusOnError="True"
                    Display="Dynamic" ErrorMessage="&nbsp;" Text="*" ValidationGroup="Reassign"></asp:RequiredFieldValidator>               
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Panel ID="pnlPrimaryFilters" runat="server" GroupingText="Primary Filters">
                    <table style="width:100%" cellpadding="5">
                        <tr id="rowUnderwriterPrimary" runat="server" visible="false">
                            <td align="right">
                                <UserControls:UnderwritingRoleLabel runat="server" Type="UW"></UserControls:UnderwritingRoleLabel> </td>
                            <td>
                                <UserControls:UnderwritersDropDownList ID="uddl" runat="server" Role="Underwriter" AutoPostBack="True"
                                 OnInit="uddl_Init" OnSelectedIndexChanged="uddl_SelectedIndexChanged" >
                                </UserControls:UnderwritersDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender1" runat="server" TargetControlID="uddl"></ajx:ListSearchExtender>
                                <asp:RequiredFieldValidator ID="rfvUnderwriter" runat="server" ControlToValidate="uddl"
                                    ErrorMessage="&nbsp;" Text="*" InitialValue="0" SetFocusOnError="True" Enabled="False"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr id="rowAgencyPrimary" runat="server">
                            <td align="right">
                                Agency</td>
                            <td>
                                <UserControls:AgencyDropDownList ID="AgencyDropDownList1" runat="server" IdentifyInactive="True" AutoPostBack="True" OnInit="AgencyDropDownList1_Init" OnSelectedIndexChanged="AgencyDropDownList1_SelectedIndexChanged" DisplayFormat="{0} ({1})">
                                </UserControls:AgencyDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender2" runat="server" TargetControlID="AgencyDropDownList1"></ajx:ListSearchExtender>
                                <asp:RequiredFieldValidator ID="rfvAgency" runat="server" ControlToValidate="AgencyDropDownList1"
                                    ErrorMessage="&nbsp;" Text="*" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Underwriting Unit</td>
                            <td>
                                <UserControls:LineOfBusinessDropDownList ID="LineOfBusinessDropDownList1" runat="server" Enabled="False" AutoPostBack="True" OnSelectedIndexChanged="LineOfBusinessDropDownList1_SelectedIndexChanged">
                                </UserControls:LineOfBusinessDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender3" runat="server" TargetControlID="LineOfBusinessDropDownList1"></ajx:ListSearchExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Type</td>
                            <td>
                                <UserControls:SubmissionTypeDropDownList ID="SubmissionTypeDropDownList1" runat="server" Enabled="False" Default="False">
                                </UserControls:SubmissionTypeDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender4" runat="server" TargetControlID="SubmissionTypeDropDownList1"></ajx:ListSearchExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Status</td>
                            <td>
                                <UserControls:SubmissionStatusDropDownList ID="SubmissionStatusDropDownList1"
                                    runat="server" Enabled="False" Default="False">
                                </UserControls:SubmissionStatusDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender5" runat="server" TargetControlID="SubmissionStatusDropDownList1"></ajx:ListSearchExtender>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td valign="top">
                <asp:Panel ID="pnlAdditionalFilters" runat="server" GroupingText="Additional Filters" Enabled="False">
                    <table style="width:100%" cellpadding="5">
                        <tr>
                            <td nowrap="nowrap" align="right">
                                Effective From</td>
                            <td>
                                <asp:TextBox ID="txEffFrom" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                            </td>
                            <td>
                                To</td>
                            <td>
                                <asp:TextBox ID="txEffTo" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" align="right">
                                Expiration From</td>
                            <td>
                                <asp:TextBox ID="txExpFrom" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                            </td>
                            <td>
                                To</td>
                            <td>
                                <asp:TextBox ID="txExpTo" runat="server" OnInit="DateBox_Init"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap" align="right">
                                Premium</td>
                            <td>
                                <asp:DropDownList ID="ddlOperators" runat="server">
                                    <asp:ListItem Value="GreaterOrEqual">&gt;=</asp:ListItem>
                                    <asp:ListItem Value="LesserOrEqual">&lt;=</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:TextBox ID="txPremium" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td id="cellUnderwriterAdditionalText" runat="server" nowrap="nowrap" align="right">
                                <UserControls:UnderwritingRoleLabel runat="server" Type="UW"></UserControls:UnderwritingRoleLabel></td>
                            <td id="cellUnderwriterAdditionalControl" runat="server" align="left" colspan="3" >
                                <UserControls:UnderwritersDropDownList Role="Underwriter" ID="UnderwritersDropDownList1" runat="Server" Default="False">
                                </UserControls:UnderwritersDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender6" runat="server" TargetControlID="UnderwritersDropDownList1"></ajx:ListSearchExtender>
                            </td>                        
                        </tr>
                        <tr>
                            <td nowrap="nowrap" align="right">
                                <UserControls:UnderwritingRoleLabel ID="UnderwritingRoleLabel1" runat="server" Type="AN"></UserControls:UnderwritingRoleLabel>
                            </td>
                            <td>                                
                                <UserControls:UnderwritersDropDownList Role="Analyst" ID="AnalystDropDownList1" runat="Server" Default="False">
                                </UserControls:UnderwritersDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender7" runat="server" TargetControlID="AnalystDropDownList1"></ajx:ListSearchExtender>
                            </td>
                            <td nowrap="nowrap" align="right">
                                <UserControls:UnderwritingRoleLabel ID="UnderwritingRoleLabel2" runat="server" Type="TN"></UserControls:UnderwritingRoleLabel>
                            </td>
                            <td>                                
                                <UserControls:UnderwritersDropDownList Role="Technician" ID="TechnicianDropDownList1" runat="Server" Default="False">
                                </UserControls:UnderwritersDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender8" runat="server" TargetControlID="TechnicianDropDownList1"></ajx:ListSearchExtender>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Button ID="btnFilter" runat="server" Text="Filter Submissions" OnClick="btnFilter_Click" AccessKey="S" OnInit="ValidationButton_Init" />
                <asp:Button ID="btnClear" runat="server" Text="Clear Filters" CausesValidation="False" OnClick="btnClear_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="3"><hr noshade /></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel ID="pnlReassignment" runat="server" GroupingText="Reassignment" Enabled="False">
                    <table cellpadding="5">
                        <tr>
                            <td>
                                Reassign checked Submissions to
                            </td>
                            <td>
                                <UserControls:UnderwritersDropDownList Role="Underwriter" runat="server" ValidationGroup="Reassign" ID="UnderwritersDropDownList2" ActiveOnly="True" RetirementDate='<%$Code: DateTime.Today %>'>
                                </UserControls:UnderwritersDropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="&nbsp;" Text="*" ControlToValidate="UnderwritersDropDownList2" Display="Dynamic" InitialValue="0" ValidationGroup="Reassign"></asp:RequiredFieldValidator>&nbsp;
                                <ajx:ListSearchExtender ID="ListSearchExtender9" runat="server" TargetControlID="UnderwritersDropDownList2"></ajx:ListSearchExtender>
                            </td>
                            <td colspan="2">
                                <asp:Button ID="btnReassign" runat="server" Text="Reassign" OnClick="btnReassign_Click" ValidationGroup="Reassign" OnInit="btnReassign_Init" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CausesValidation="False" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <br />
            </td>
        </tr>
    </table>
    <input id="Hidden1" runat="server" type="hidden" value="0" />
    <input id="selectAllState" runat="server" type="hidden" value="0" />
    <input id="dataRowCount" runat="server" type="hidden" value="0" />
    
    </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>

<table id="btable" runat="server" cellpadding="0" cellspacing="0" class="box">
    
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Reassign Submissions</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        
                <UserControls:Grid ID="gvSubmissions" EmptyDataText="No Submissions were found for the criteria entered"
                    runat="server" AllowPaging="True" AutoGenerateColumns="False" OnInit="gvSubmissions_Init"
                    OnPageIndexChanging="gvSubmissions_PageIndexChanging"
                    OnRowDataBound="gvSubmissions_RowDataBound"
                    OnSelectedIndexChanged="gvSubmissions_SelectedIndexChanged"
                    DataKeyNames="id,ClientCore_Client_Id,submission_number,submission_number_sequence"
                    OnDataBound="gvSubmissions_DataBound"
                    OnSorting="gvSubmissions_Sorting" AllowSorting="True">
                    <PagerTemplate>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                    </PagerTemplate>
                </UserControls:Grid>
        </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
   
</table>
</asp:Content>
