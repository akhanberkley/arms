<%@ Page language="c#" MasterPageFile="~/Site.PATS.master" Title="Berkley Clearance System" Inherits="BCS.WebApp._Default" CodeFile="Default.aspx.cs" Theme="DefaultTheme" ValidateRequest="False"%>
<%@ Register Src="ClientSearch.ascx" TagName="ClientSearch" TagPrefix="uc1" %>
<%@ Register Src="~/ClientAddress.ascx" TagName="ClientAddress" TagPrefix="uc1" %>
<%@ Register Src="~/ClientNameSelector.ascx" TagName="ClientNameSelector" TagPrefix="uc1" %>
<%@ Register TagPrefix="UserControls" namespace="BCS.WebApp.UserControls" %>
<%@ MasterType VirtualPath="~/Site.PATS.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="Javascripts/jquery-1.4.4.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function () {
        HighLightSelectedNames();
        //checkIfPhoneNumberIsFilled();
    });

    function callScript() {
        if ($("input[id$=txtPhoneNum]").val() != '') {
            $("input[id$=lblPhValidation]").val("");
            //$("#aspnetForm :input").attr("disabled", true);
            $('input:not(.exclude),textarea,select').attr('disabled', 'disabled');
            $("input[id$=txtPhoneNum]").removeAttr("disabled");
            $("input[id$=btnSearch]").removeAttr("disabled");
            $("input[id$=txtPhoneNum]").focus();
        }
        else {
           // $("#aspnetForm :input").removeAttr("disabled", true);
            $('input,textarea,select').removeAttr('disabled', 'disabled');
        }
        var phNum = $("input[id$=txtPhoneNum]").val().replace("___", "").replace("____", "").replace("__", "").replace("_", "").replace("-", "").replace("(", "").replace(")", "").replace(".", "").replace("/", "").replace(".", "").replace("-", "").replace("/").replace(" ", "");
        if (phNum.length == 10) {
            $("input[id$=txtPhoneNum]").val(phNum);
            $("input[id$=btnSearch]").focus();
        }

        if (phNum.length == 0) {
            $('input,textarea,select').removeAttr('disabled', 'disabled');
        }
        
    }

    function checkIfPhoneNumberIsFilled() {
        var phNum = $("input[id$=txtPhoneNum]").val().replace("___", "").replace("____", "").replace("__", "").replace("_", "").replace("-", "").replace("(", "").replace(")", "").replace(".", "").replace("/", "").replace(".", "").replace("-", "").replace("/").replace(" ", "");
        if (phNum.length > 0) {
            $("input[id$=txtPhoneNum]").focus();
        }
    }
    function checkPhoneNumber() {
        var phNum = $("input[id$=txtPhoneNum]").val().replace("___", "").replace("____", "").replace("__", "").replace("_", "").replace("-", "").replace("(", "").replace(")", "").replace(".", "").replace("/", "").replace(".", "").replace("-", "").replace("/").replace(" ", "");
        if (phNum.length < 10) {
            $("input[id$=txtPhoneNum]").val(phNum);
            $("input[id$=txtPhoneNum]").focus();
        }
    }
    function enableElements() {
        var phNum = $("input[id$=txtPhoneNum]").val().replace("___", "").replace("____", "").replace("__", "").replace("_", "").replace("-", "").replace("(", "").replace(")", "").replace(".", "").replace("/", "").replace(".", "").replace("-", "").replace("/").replace(" ", "");
        if (phNum.length < 10) {
            $("input[id$=txtPhoneNum]").val(phNum);
            return false;
        }
        $("#aspnetForm :input").removeAttr("disabled", true);
        $('input,textarea,select').removeAttr('disabled', 'disabled');
    }
    function HighLightSelectedNames() {
        
        var clientid = $("#hdSelectedName").val();
        if (clientid == null || clientid.length == 0)
            return;

        var items = $get("lbNames");

        for (var i = 0; i < items.options.length; i++) {
            if (items.options[i].value.indexOf(clientid)>=0) {
                $("#divSelectedNames").append(items.options[i].text + "<br />");
                items.options[i].style.backgroundColor = "yellow";
            }
            else
                items.options[i].style.backgroundColor = "white";
        }
    }

    function NameChanged(listBox) {

        var value = listBox.options[listBox.selectedIndex].value.split('^')[0];

        var lbNameValue = $get('hdSelectedName').value;

        if (lbNameValue == null || lbNameValue != value) {  
            $get('hdSelectedName').value = value;
            
            __doPostBack();
        }
    }


    function showProcessing() {
        $("#dvClientSrchResults").css("display", "none");
        //$('[id$=dvClientSrchResults]').css("display", "none");
        //$('[id$=dvClientSrchProcessing]').css("display", "");
       // document.getElementById('dvClientSrchResults').style.display = 'none';
        $("#dvClientSrchProcessing").css("display", "block");
        $("#dvOverlay").css("display", "block");
        //$('[id$=dvOverlay]').css("display", "block");
         return true;
    }

</script>

<ajx:ToolkitScriptManager EnablePartialRendering="true" CombineScripts="True" id="ScriptManager1" runat="server"></ajx:ToolkitScriptManager>
    <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
    </asp:PlaceHolder>
    <p>
    <%--<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/AgencySubmissionSearch.aspx" OnInit="HyperLink1_Init">Search By Agency</asp:HyperLink>&nbsp;|&nbsp;--%>
    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/submissionsearchnew.aspx" OnInit="HyperLink1_Init">Search By Submission</asp:HyperLink>
    </p>
    <asp:panel id="pnlSearchCriteria" Runat="server" Width="100%" DefaultButton="btnSearch">
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td valign="top">
                <table id="criteriaTable" cellpadding="0" cellspacing="0" class="box" style="margin-top: 10px;">
                    <tbody>
                        <tr>
                            <td class="TL"><div /></td>
                            <td class="TC"> Search Criteria                                
                            </td>
                            <td class="TR"><div /></td>
                        </tr>
                        <tr>
                            <td class="ML"><div /></td>
                            <td class="MC">
	                        <uc1:ClientSearch id="ClientSearch1" runat="server"></uc1:ClientSearch>
                            <p style="margin-top:15px;">
                                <asp:Button ID="btnSearch" runat="server"  Text="Search" AccessKey="S" OnClick="btnSearch_Click" />
                                &nbsp;&nbsp;
                                <asp:Button ID="btnAddClientNew" runat="server" Enabled='<%$ Code: BCS.WebApp.Components.Security.CanUserAdd() %>' Visible="false" Text="Add Client" OnClick="btnAddClient_Click" />
                                <asp:HiddenField ID="hdnAdvSrchState" runat="server" Value="" />
                            </p>
                            </td>
                            <td class="MR"><div /></td>
                        </tr>
                        <tr>
                            <td class="BL"><div/></td>
                            <td class="BC"><div/></td>
                            <td class="BR"><div/></td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td valign="top">
                <asp:Panel runat="server"  ID="pnlNotification" OnLoad="pnlNotification_Load">
                    <table  cellpadding="0" cellspacing="0" class="box" style="margin-top:10px;margin-left:20px;">
                        <tbody>
                            <tr>
                                <td class="TL"><div /></td>
                                <td class="TC">Site Notification</td>
                                <td class="TR"><div /></td>
                            </tr>
                            <tr>
                                <td class="ML"><div /></td>
                                <td class="MC" nowrap>
	                                <asp:Label ID="lbNotification" runat="server"></asp:Label>
                                    <br />
                                    <asp:HyperLink ID="hlNotification" runat="server"></asp:HyperLink>
                                </td>
                                <td class="MR"><div /></td>
                            </tr>
                            <tr>
                                <td class="BL"><div/></td>
                                <td class="BC"><div/></td>
                                <td class="BR"><div/></td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    </asp:panel>


    <asp:panel id="pnlSearchResults" Visible="False" Runat="server">
	<table id="resultsTable" cellpadding="0" cellspacing="0" class="box" style="margin-top: 10px;">
        <tbody>
        <tr>
            <td class="TL"><div /></td>
            <td class="TC">Search Results</td>
            <td class="TR"><div /></td>
        </tr>
        <tr>
            <td class="ML"><div /></td>
            <td class="MC" style="width: 100%">           
		    <p>Below are the results found, based on the criteria you entered on the previous screen. Select an entry to load submissions below.</p>
		    <table width="100%">
		        <tr>
		            <td><strong>Names:</strong></td>
		            <td><strong>All Names for Selected Client:</strong></td>		        
		        </tr>
		        <tr>
		            <td width="60%" valign="top" rowspan="3">
                        <asp:ListBox Font-Size="11px"  ID="lbNames" runat="server" Width="100%" Rows="15" ClientIDMode="Static" onchange="NameChanged(this);" AccessKey="N"
                            OnSelectedIndexChanged="lbNames_SelectedIndexChanged"></asp:ListBox></td>
                            <asp:HiddenField ID="hdSelectedName" ClientIDMode="Static" runat="server" />
                    
		            <td valign="top"  style="border:1px solid #888888;">
                        <div id="divSelectedNames" style="padding:1px;overflow-x:auto;overflow-y:auto;height:100px;">
                        </div>
                    </td>
		        </tr>
                <tr>
                    <td><strong>Addresses:</strong></td>
                </tr>
                <tr>
                    <td width="60%" valign="top">
                        <asp:ListBox ID="lbAddress" runat="server" AutoPostBack="True" Font-Size="11px" OnSelectedIndexChanged="lbAddress_SelectedIndexChanged"
                            Rows="6" Width="100%"></asp:ListBox></td>
                </tr>
		    </table>
    		
    <div class="default_portfoliosearch">
        <asp:PlaceHolder ID="phPortfolio" runat="server" Visible="False">
            <%--<table cellpadding="0" cellspacing="0" class="box">
            <tbody>
            <tr>
            <td class="TL"><div /></td>
            <td class="TC">Portfolio Info</td>
            <td class="TR"><div /></td>
            </tr>
            <tr>
            <td class="ML"><div /></td>
            <td class="MC" style="width: 600px">--%>
                <fieldset style="border: solid 1px maroon; width: 50%; background-color: ButtonFace">
                <legend style="font-weight: bold;">Portfolio Info for the selected client.</legend>
                <asp:Panel ID="phPortfolioInfo" runat="server">
                <div style="display: block;">
                <b style="padding-left: 20px;">Name - </b><asp:HyperLink ID="lnkPortfolioName" runat="server" NavigateUrl="#" />
                <label>,</label>
                <b>ID - </b><asp:Label ID="lblPortfolioId" runat="server" />
                <label>, </label>
                <b>Number of Clients - </b><asp:Label ID="lblClientCountinPortfolio" runat="server" />
                <label>, </label>
                <b>Tax ID - </b><asp:Label ID="lblFEIN" runat="server" />
                <br /><br />
                </div>
                <asp:Panel ID="PortfolioEditPanel" runat="server" DefaultButton="Button1" BorderColor="Maroon" BorderStyle="Solid" BorderWidth="1" CssClass="modalPopup" style="display:none; width: auto;">
                    <asp:TextBox ID="TextBox1" runat="server" Columns="32"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                        ControlToValidate="TextBox1" ErrorMessage="*" Display="Dynamic" SetFocusOnError="True" ValidationGroup="PortfolioEdit" >
                    </asp:RequiredFieldValidator>
                    <asp:Button ID="Button1" runat="server" Text="Submit" ValidationGroup="PortfolioEdit" OnClick="UpdatePortFolio" UseSubmitBehavior="False" />
                    <br />
                    <asp:CompareValidator ID="CompareValidator1" runat="server"
                        ControlToValidate="TextBox1" ErrorMessage="Same Name" Display="Dynamic" SetFocusOnError="True" ValidationGroup="PortfolioEdit"
                        Type="String" Operator="NotEqual">
                    </asp:CompareValidator>
                </asp:Panel>
                <ajx:PopupControlExtender ID="popupPortfolioEdit" runat="server" 
                    TargetControlID="lnkPortfolioName"
                    PopupControlID="PortfolioEditPanel"
                    Position="Bottom" BehaviorID="PortfolioEdit">
                </ajx:PopupControlExtender>
                </asp:Panel>
                </fieldset>
                
            <%--</td>
            <td class="MR"><div /></td>
            </tr>
            <tr>
            <td class="BL"><div/></td>
            <td class="BC"><div/></td>
            <td class="BR"><div/></td>
            </tr>
            </tbody>
            </table>--%>
        </asp:PlaceHolder>
   </div>
   <br />
		    <div style="padding:10px; border-left: black 1px solid;border-bottom: black 1px solid;">
		        <asp:Button ID="btnAddClient" runat="server" Enabled='<%$ Code: BCS.WebApp.Components.Security.CanUserAdd() %>' Text="Add Client" OnClick="btnAddClient_Click" />&nbsp;
                <asp:Button ID="btnEditClient" runat="server" Text="Edit Client" OnClick="btnEditClient_Click" AccessKey="E"/>&nbsp;
                <asp:Button ID="btnCreateShellSubmission" runat="server"
                    Enabled='<%$ Code: BCS.WebApp.Components.Security.CanUserAdd() %>'
                    Visible='<%$ Code: ConfigValues.SupportsShellSubmissions(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                    Text="Add Submission (Shell)"
                    OnClick="btnCreateShellSubmission_Click" />
		    </div>
    		
		    <p>
		        <img id="img1" runat="server" src='<%$ Code : Common.GetImageUrl("setnext.gif") %>' alt="" />&nbsp;<asp:LinkButton id="lbSearchAgain" runat="server" onclick="lbSearchAgain_Click">Search Again</asp:LinkButton>
		    </p>
		    
        <asp:Panel ID="phSubmissions" runat="server">
		    <h3 class="sectionGroupHeader">Submissions
            <asp:ImageButton runat="server" ID="ibtnExportToExcel" OnClick="lbtnExportToExcel_Click" ImageUrl='~/Images/Excel.gif' ToolTip="Import To Excel" ></asp:ImageButton>
            <span style="margin-left:20px;font-size:12px;font-weight:normal;color:#0066cc;">
            <table>
             <tr>
               <td width="500px">
                <asp:CheckBox runat="server" ID="chkIncludeDeleted" OnCheckedChanged="chkIncludeDeleted_OnCheckChanged" AutoPostBack="true" Text="Include Deleted Submissions" Visible="<%$Code : ConfigValues.SupportsOptionToSearchDeletedSubmissions() %>" />
               </td>
               <td width = "500px" align="right">
                <asp:LinkButton runat="server" ID="lbtnExportToExcel" Text="ExportToExcel" align="right" OnClick="lbtnExportToExcel_Click" Visible="false"></asp:LinkButton>
               </td>
             </tr>
            </table>                             
            </span>
            </h3>
            <%-- Start Customizable Grid--%>
            <UserControls:Grid ID="gvSubmissions" runat="server" AllowPaging="True" AutoGenerateColumns="False" AllowSorting="True"
                EmptyDataText="No Submissions were found for the selected client."            
                DataKeyNames="id,ClientCore_Client_Id,submission_number,submission_number_sequence,policy_number,submission_status_code,submission_status_date"
                OnInit="gvSubmissions_Init"
                OnPageIndexChanged="gvSubmissions_PageIndexChanged"
                OnPageIndexChanging="gvSubmissions_PageIndexChanging"
                OnRowDataBound="gvSubmissions_RowDataBound"
                OnSelectedIndexChanged="gvSubmissions_SelectedIndexChanged"
                OnDataBound="gvSubmissions_DataBound"
                OnRowCommand="gvSubmissions_RowCommand"
                OnRowDeleting="gvSubmissions_Deleting"
                OnRowDeleted="gvSubmissions_Deleted"
                OnSorting="gvSubmissions_Sorting"
                OnSorted="gvSubmissions_Sorted"
                >
             <Columns>
             <%--TODO: proposed remove any edits grids--%>
                <asp:TemplateField>
                    <ItemTemplate>
                    <asp:Image ID="Image1" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("folder_find.gif") %>' AlternateText="View Status History"  />
	                <ajx:ModalPopupExtender runat="server" ID="programmaticModalPopup"
                            TargetControlID="Image1"
                            PopupControlID="programmaticPopup" 
                            BackgroundCssClass="modalBackground"
                            DropShadow="True"
                            RepositionMode="RepositionOnWindowScroll" >
                    </ajx:ModalPopupExtender>
                    <asp:Panel ID="programmaticPopup" runat="server" Style="display: none" CssClass="modalPopup" OnLoad="Panel1_Load" HorizontalAlign="center" Width="520px">
                        <div>
                        <aje:UpdatePanel ID="UpdatePanel81" runat="server">
                        <ContentTemplate>
                        <asp:Label id="lblSubmissionNumber" runat="server"></asp:Label>                        
                        <asp:Label id="lblCurrentStatus" runat="server"></asp:Label>
                        <asp:PlaceHolder ID="HistoryStatusPH" runat="server"></asp:PlaceHolder>
                        <br /><br />
                        <UserControls:Grid id="GridView1" runat="server" CssClass="grid" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" EmptyDataText="There is no history for this submission." AllowPaging="True" AutoGenerateColumns="False"
                         DataSourceID="ObjectDataSource1" DataKeyNames="Id" OnRowDataBound="GridView1_RowDataBound" Width="520px">
                         <EmptyDataTemplate>
                            <table width="100%">
                              <tr>
                                <td style="border: none;"> There is no history for this submission. </td>
                              </tr>
                            </table>
                        </EmptyDataTemplate>
                         <Columns>
                            <asp:BoundField DataField="Id" Visible="False" HeaderText="Id"></asp:BoundField>
                            <asp:BoundField DataField="SubmissionId" Visible="False" HeaderText="SubmissionId"></asp:BoundField>
                            <asp:BoundField DataField="PreviousSubmissionStatusId" Visible="False" HeaderText="PreviousSubmissionStatusId"></asp:BoundField>
                            <asp:TemplateField HeaderText="Status Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatusDate" runat="server" Text='<%# Bind("SubmissionStatusDate", "{0:MM/dd/yyyy}") %>' ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Previous Submission Status">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "SubmissionStatus.StatusCode") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status Changed Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblDate" runat="server" Text='<%# Bind("StatusChangeDt", "{0:MM/dd/yyyy HH:mm}") %>' ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="StatusChangeBy" HeaderText="Status Changed By"></asp:BoundField>
                            <asp:TemplateField HeaderText="Active" ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Active" CommandName="Select" CausesValidation="false" ID="LinkButton1"></asp:LinkButton>
                                    <ajx:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="LinkButton1"
                                    ConfirmText="Are you sure you want to make this status history inactive, this cannot be undone?"></ajx:ConfirmButtonExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerTemplate>
                        <asp:PlaceHolder runat="server"></asp:PlaceHolder>
                        </PagerTemplate>
                        </UserControls:Grid>
                        <asp:ObjectDataSource id="ObjectDataSource1" runat="server" TypeName="BCS.Core.Clearance.Submissions" SelectMethod="GetSubmissionStatusHistory" OldValuesParameterFormatString="original_{0}">
                            <SelectParameters>
                                <asp:Parameter Type="Int32" DefaultValue="0" Name="submissionId" />
                            </SelectParameters>
                        </asp:ObjectDataSource> 
                        </ContentTemplate>
                        </aje:UpdatePanel>                            
                        <p style="text-align: center;">
                            <asp:Button ID="OkButton" runat="server" Text="OK" Visible="False" CausesValidation="False"/>
                            <asp:Button ID="CancelButton" runat="server" Text="Done" CausesValidation="False"/>
                        </p>
                        </div>
                    </asp:Panel>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False" Visible='<%$ Code: BCS.WebApp.Components.Security.CanDelete() %>'>
                    <ItemTemplate>
                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                             Text="Delete" />
                        <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="btnDelete"
                            ConfirmText='<%# DataBinder.Eval(Container.DataItem, "submission_number", "Are you sure you want to delete this Submission \"{0}\"?") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
             </Columns>
            </UserControls:Grid>
            <%-- End Customizable Grid--%>
            <p>            
                <asp:Button Visible="false" AccessKey="A" ID="btnSubmission" Enabled='<%$ Code: BCS.WebApp.Components.Security.CanUserAdd() %>' CommandName="Add" runat="server"  Text="Add Submission" OnCommand="btnSubmission_Command" />
            </p>
        </asp:Panel>
		    </td>
            <td class="MR"><div /></td>
            </tr>
            <tr>
                <td class="BL"><div/></td>
                <td class="BC"><div/></td>
                <td class="BR"><div/></td>
            </tr>
            </tbody>
        </table>                                    
                
	</asp:panel>

    <div id="dvOverlay" class="OverlayEffect" style="display:none;" runat="server"></div>
    
    
    <ajx:ModalPopupExtender ID="mdlAdvClientSrchResults" runat="server" 
                              PopupControlID="pnlAdvClientSrchResults"
                              TargetControlID="btnHiddenforModal"
                              DropShadow="true"
                              Drag ="true"
                              BackgroundCssClass="modalBackground" 
                              BehaviorID="mdlAdvClientSrchResults" 
                              RepositionMode="RepositionOnWindowScroll">
    </ajx:ModalPopupExtender>
     <asp:Panel ID="pnlAdvClientSrchResults" runat="server"  CssClass="Mask" style="width:540px;">
     <style type="text/css">
         .Mask { display: none; width: 80%; height: 250px; z-index: 100000; padding: 3px; margin: 3px; position: absolute;left: 330px; 
                 top: 250px; overflow: hidden;border-width:3px; border-color :Gray;background-color:#ffffdd;border-style:solid; }
         .modalShow 
           { 
           background-color:#CCCCFF; 
           filter:alpha(opacity=70); 
           opacity:0.7;
          }
         .OverlayEffect
  {    
  background-color: gray; 
  filter: alpha(opacity=70);
  opacity: 0.7;     
  width:100%;    
  height:100%;  
  z-index:400;  
  position:absolute; 
  top:0;  
  left:0; 
}
     </style>
      <table>
        <tr>
        <td>
         <asp:Button ID="btnHiddenforModal" runat="server" style="display:none;" />
         </td>
         </tr>
         <tr>
         <td>
                 
           <table>
             <tr>
              <td align="left">Name:</td>
              <td align="left">City:</td>
              <td align="left">State:</td>
             </tr>
             <tr>
              <td>
                <asp:TextBox ID="txtAdvSrchName" runat="server" Width="180px"></asp:TextBox>
              </td>
              <td>
                <asp:TextBox ID="txtAdvSrchCity" runat="server" Width="140px"></asp:TextBox>
                </td>
                <td>
                <asp:TextBox ID="txtAdvSrchState" runat="server" Width="140px"></asp:TextBox>
                </td>
                <td>
                  <asp:ImageButton ID="imgbtnRefresh" runat="server" ImageUrl="~/Images/refresh.jpg" OnClientClick="showProcessing();"  OnClick="imgbtnRefresh_Click" /> 
                </td>
             </tr>
           </table>
           
         </td>
        </tr>
        <tr>
       <td align="center">
       
       <div id="dvClientSrchResults"  style="width:520px;">
          <asp:GridView ID="gvAdvClientSrchResults" runat="server" AllowPaging="false" AllowSorting="false"
                     AutoGenerateColumns="true" OnRowCommand="gvAdvClientSrchResults_RowCommand" DataKeyNames="ClientId" 
                     EmptyDataText="No records Found in Client Core . Click 'Add Client' to create a new Client"
                     CssClass="grid" Width="520px"  >
                     <Columns>
                       <%--<asp:BoundField HeaderText="ClientId" DataField="ClientId" />
                       <asp:BoundField HeaderText = "Name" DataField="Name" />
                       <asp:BoundField HeaderText = "Address" DataField = "Address" />--%>
                       <asp:TemplateField>
                         <ItemTemplate>
                           <asp:Button ClientIDMode="Predictable" runat="server" Text="View" CommandName="View" />
                         </ItemTemplate>
                       </asp:TemplateField>
                     </Columns>
                    
          </asp:GridView>
         </div>
       
          <div id="dvClientSrchProcessing"  style="width:520px;display:none">
             <img id="processing" runat="server" alt="processing" src='<%$ Code : Common.GetImageUrl("indicator.gif") %>' /> &nbsp; Processing... Please Wait....
          </div>
         </td>
        </tr>

        <tr>
        <td align="center">
          <asp:Button ID="btnAddNewClient" runat="server" Text="Add Client" ClientIDMode="Static" OnClick="btnAddNewClient_Click" />
          <asp:Button ID="btnClose" runat="server" Text="Cancel" ClientIDMode="Static" OnClick="btnClosePopup_Click" />
          <asp:HiddenField ID="hdnHideModalPopup" runat="server" Value="" />
          </td>
        </tr>
       
    </table>

   </asp:Panel>
   
</asp:Content>
