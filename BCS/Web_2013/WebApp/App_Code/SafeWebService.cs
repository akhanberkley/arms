using System.Net;

namespace BCS.WebApp.Services.WebServicesHelpers
{
    /// <summary>
    /// Summary description for SafeWebService
    /// </summary>
    public class SafeWebService
    {
    }

    public class ClientHelper : ClientProxy.Client
    {
        protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
        {
            //return base.GetWebRequest(uri);
            WebRequest request = base.GetWebRequest(uri);
            (request as HttpWebRequest).KeepAlive = false;
            return request;
        }
    }

    public class SubmissionHelper : SubmissionProxy.Submission
    {
        protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
        {
            //return base.GetWebRequest(uri);
            WebRequest request = base.GetWebRequest(uri);
            (request as HttpWebRequest).KeepAlive = false;
            return request;
        }
    }

    public class LookupHelper : LookupProxy.Lookup
    {
        protected override System.Net.WebRequest GetWebRequest(System.Uri uri)
        {
            //return base.GetWebRequest(uri);
            WebRequest request = base.GetWebRequest(uri);
            (request as HttpWebRequest).KeepAlive = false;
            return request;
        }
    }

    public class AdvClientSearchHelper : AdvClientSearch.AdvClientSearchWebServicePublicService
    {
        public AdvClientSearchHelper()
        {
            //int companyId = (int)System.Web.HttpContext.Current.Session["CompanyId"];
            //this.Url = "http://eip-int/eip/http-post/AdvCliSearchWS";

            //AdvClientSearch.Security cssec = new AdvClientSearch.Security();
            //cssec.dsik = "91f46d1c-81f9-41bf-abbd-046f1598af3f";

            //AdvClientSearch.UsernameTokenType utok = new AdvClientSearch.UsernameTokenType();
            //AdvClientSearch.AttributedString attUsrName = new AdvClientSearch.AttributedString();
            //attUsrName.Value = "jangolan";

            //utok.Username = attUsrName;
            //cssec.UsernameToken = utok;

            //this.RequestSecurity = cssec;
        }
      }
}
