using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Web;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Policy;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;
using Microsoft.Web.Services2.Security.X509;

using BCS.Biz;
using BCS.Core;

using BTS.CacheEngine.V2;

using OrmLib;

namespace BCS.WebApp.Components
{
    public enum ClearanceReadOnlyContext
    {
        Agency,
        NonAgency
    }

    public enum Generatables
    {
        PolicyNumber,
        AgentNumber
    }
	/// <summary>
	/// Summary description for Security.
	/// </summary>
	public sealed class Security
	{
		/// <summary>
		/// Private ctor
		/// </summary>
		private Security() {}

		/// <summary>
		/// Get a string array of roles based on user identity name
		/// </summary>
		/// <param name="identityName"></param>
		/// <returns></returns>
		/// <remarks>Cached Value</remarks>
		public static string[] GetUserRoles(string identityName)
		{
            // TODO : items seem to cached per page basis
            CacheEngine cache = new CacheEngine(DefaultValues.DefaultObjectCacheTimeInMinutes);
            string cacheKey = string.Format(CacheKeys.UserRoles, identityName);
			string[] returnRoles;

			if (!cache.CachedItemExist(cacheKey))            
			{
				DataManager dataManager = new DataManager(DefaultValues.DSN);

				dataManager.QueryCriteria.Clear();
				dataManager.QueryCriteria.And(JoinPath.User.Columns.Username, identityName, MatchType.Exact);
				dataManager.QueryCriteria.And(JoinPath.User.Columns.Active, true, MatchType.Exact);
			
				User user = dataManager.GetUser(FetchPath.User.UserRole.Role);

				if (user != null)
				{
					returnRoles = new string[user.UserRoles.Count];
					for (int i = 0; i< user.UserRoles.Count; i++)
					{
                        returnRoles[i] = user.UserRoles[i].Role.RoleName;
					}
					
					cache.AddItemToCache(cacheKey,returnRoles);
				}
				else
					return null;
			}
			else
			{
				returnRoles = (string[])cache.GetCachedItem(cacheKey);
			}

			return returnRoles;
		}
        public static string[] GetUserRoles(string identityName, out long agency)
        {
            CacheEngine cache = new CacheEngine(DefaultValues.DefaultObjectCacheTimeInMinutes);
            string cacheKey = string.Format(CacheKeys.UserRoles, identityName);
            string agencyCacheKey = string.Format(CacheKeys.UserAgency, identityName);
            string[] returnRoles;
            agency = 0;
            if (!cache.CachedItemExist(cacheKey) || !cache.CachedItemExist(agencyCacheKey))
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);

                dataManager.QueryCriteria.Clear();
                dataManager.QueryCriteria.And(JoinPath.User.Columns.Username, identityName, MatchType.Exact);
                dataManager.QueryCriteria.And(JoinPath.User.Columns.Active, true, MatchType.Exact);

                User user = dataManager.GetUser(FetchPath.User.UserRole.Role);

                if (user != null)
                {
                    returnRoles = new string[user.UserRoles.Count];
                    for (int i = 0; i < user.UserRoles.Count; i++)
                    {
                        returnRoles[i] = user.UserRoles[i].Role.RoleName;
                    }
                    cache.AddItemToCache(cacheKey, returnRoles);
                    
                    agency = user.AgencyId.IsNull ? 0 : user.AgencyId.Value;
                    cache.AddItemToCache(agencyCacheKey, agency, new System.Web.Caching.CacheDependency(null, new string[] { cacheKey }));
                }
                else
                    return null;
            }
            else
            {
                returnRoles = (string[])cache.GetCachedItem(cacheKey);
                //if (!cache.CachedItemExist(agencyCacheKey))
                //    GetUserRoles(identityName, out agency);
                agency = (long)cache.GetCachedItem(agencyCacheKey);
            }

            return returnRoles;
        }

		/// <summary>
		/// Get a CompanyCollection of assigned companies based on user identity name
		/// </summary>
		/// <param name="identityName"></param>
		/// <returns></returns>
		/// <remarks>Cached Value</remarks>
        public static UserCompanyCollection GetUserCompanies(string identityName)
		{
            CacheEngine cache = new CacheEngine(DefaultValues.DefaultObjectCacheTimeInMinutes);
            string cacheKey = string.Format(CacheKeys.UserCompanies, identityName);
            UserCompanyCollection returnCompanies;

			if (!cache.CachedItemExist(cacheKey))
			{
				DataManager dataManager = new DataManager(DefaultValues.DSN);

				dataManager.QueryCriteria.Clear();
				dataManager.QueryCriteria.And(JoinPath.User.Columns.Username, identityName, MatchType.Exact);
				dataManager.QueryCriteria.And(JoinPath.User.Columns.Active, true, MatchType.Exact);
			
				User user = dataManager.GetUser(FetchPath.User.UserCompany.Company,
                    FetchPath.User.UserCompany.Company.ClientSystemType, FetchPath.User.UserCompany.Company.SubmissionPage,
                    FetchPath.User.UserCompany.Company.SubmissionCopyExclusion);

				if (user != null)
				{
					returnCompanies = user.UserCompanys;
                    cache.AddItemToCache(cacheKey, returnCompanies);
				}
				else
					return null;
			}   
			else
			{
                returnCompanies = (UserCompanyCollection)cache.GetCachedItem(cacheKey);
			}

			return returnCompanies;
		}

        public static bool IsUserReadOnly()
        {
            //if (HttpContext.Current.Session["ReadOnlyUser"] != null)
            //    return (bool)HttpContext.Current.Session["ReadOnlyUser"];

            //DataManager dataManager = new DataManager(DefaultValues.DSN);
            //BCS.Core.Security.CustomPrincipal customPrincipal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;

            //string[] sroles = customPrincipal.Roles;
            //dataManager.QueryCriteria.Clear();
            //dataManager.QueryCriteria.And(JoinPath.Role.Columns.RoleName, sroles, MatchType.In);

            //RoleCollection roles = dataManager.GetRoleCollection();
            //if (roles != null)
            //{
            //    foreach(Role role in roles)
            //    {
            //        if (role.PropertyReadOnly)
            //        {
            //            HttpContext.Current.Session["ReadOnlyUser"] = true;
            //            return true;
            //        }
            //    }
            //}
            //return false;

            return IsUserReadOnly(ClearanceReadOnlyContext.NonAgency);
        }

        public static bool IsUserReadOnly(ClearanceReadOnlyContext context)
        {
            RoleCollection allRoles = GetAllRoles();
            BCS.Core.Security.CustomPrincipal customPrincipal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;

            string[] sroles = customPrincipal.Roles;
            RoleCollection roles = new RoleCollection();
            foreach (string var in sroles)
            {
                Role role = allRoles.FindByRoleName(var);

                if (role != null)
                    roles.Add(role);
            }
                        
            if (null != roles.FindByUniversalAccess(true))
            {
                return false;
            }
            bool retVal = false;
            if (null != roles.FindByPropertyReadOnly(true))
            {
                retVal = true;
                if (null != roles.FindByRoleName("Auditor"))
                {
                    return true;
                }
                else
                    retVal = false;

                switch (context)
                {
                    case ClearanceReadOnlyContext.Agency:
                        {
                            if (null != roles.FindByRoleName("Agency Read-Only"))
                            {
                                retVal = true;
                            }
                            if (null != roles.FindByRoleName("Agency Administrator"))
                            {
                                return false;
                            }
                            break;
                        }
                    case ClearanceReadOnlyContext.NonAgency:
                        break;
                    default:
                        break;
                }
            }
            return retVal;
        }

        public static bool CanUserAdd()
        {
            BCS.Core.Security.CustomPrincipal customPrincipal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
            if (customPrincipal == null)
                return false;

            string[] sroles = customPrincipal.Roles;

            if (sroles == null)
                return false;
                
            bool b = (!customPrincipal.IsInRole("Partial User") && !IsUserReadOnly());
            return b;
        }

        public static bool CanUserGenerate(Generatables g)
        {
            BCS.Core.Security.CustomPrincipal customPrincipal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;

            switch (g)
            {
                case Generatables.PolicyNumber:
                    {
                        string[] sroles = customPrincipal.Roles;

                        bool b = (customPrincipal.IsInAnyRoles("Remote Agency", "User", "System Administrator"));

                        bool r = !IsUserReadOnly();

                        return b && r;
                    }
                case Generatables.AgentNumber:
                    {
                        string[] sroles = customPrincipal.Roles;

                        bool b = (customPrincipal.IsInAnyRoles("Agency Administrator", "Agent Administrator", "System Administrator"));

                        bool r = !IsUserReadOnly();

                        return b && r;
                    }
                default:
                    return false;
            }
        }


        public static bool CanResequence()
        {
            BCS.Core.Security.CustomPrincipal customPrincipal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
            if (customPrincipal == null)
                return false;
            return customPrincipal.IsInAnyRoles(new string[] { "System Administrator", "Re-sequence Administrator" });
        }

        public static bool CanActivateStatus()
        {
            BCS.Core.Security.CustomPrincipal user = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
            if (user == null)
                return false;
            if (user.IsInRole("System Administrator"))
                return true;
            return !user.IsInAnyRoles(new string[] { "Partial User", "Auditor" });
        }

        public static long GetMasterAgency(long agencyId)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);
            dataManager.QueryCriteria.And(JoinPath.Agency.Columns.Id, agencyId);
            Agency agency = dataManager.GetAgency(FetchPath.Agency.ParentRelationAgency);

            if (null == agency)
                return 0;
            if (agency.ParentRelationAgency == null)
                return agencyId;
            return agency.ParentRelationAgency.Id;
        }

        public static RoleCollection GetAllRoles()
        {
            if (HttpContext.Current.Cache["Roles"] == null)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                RoleCollection rc = dataManager.GetRoleCollection();
                rc = rc.SortByRoleName(OrmLib.SortDirection.Ascending);
                HttpContext.Current.Cache["Roles"] = rc;
                return rc;
            }
            return HttpContext.Current.Cache["Roles"] as RoleCollection;
        }

		/// <summary>
		/// Returns a SecurityToken based on the current username passed.
		/// </summary>
		/// <param name="currentUsername"></param>
		/// <returns></returns>
		public static SecurityToken GetCurrentUserSecurityToken(string currentUsername)
		{
			byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes( currentUsername );
			
			Array.Reverse( passwordBytes );
			string passwordEquivalent = Convert.ToBase64String( passwordBytes );
			
			SecurityToken token = new UsernameToken( currentUsername, passwordEquivalent, PasswordOption.SendHashed );

			return token;
		}

        public static bool Logon(string userName, string password)
        {
            return false;
        }


        public static bool CanMerge()
        {
            BCS.Core.Security.CustomPrincipal customPrincipal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
            return customPrincipal.IsInAnyRoles(new string[] { "System Administrator", "Company Administrator" });
        }

        public static bool CanDelete()
        {
            BCS.Core.Security.CustomPrincipal customPrincipal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
            if (customPrincipal == null)
                return false;
            return customPrincipal.IsInAnyRoles(new string[] { "System Administrator", "Company Administrator","Delete Submission" });
        }

        public static bool CanDeleteClient()
        {
            BCS.Core.Security.CustomPrincipal customPrincipal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
            if (customPrincipal == null)
                return false;
            return customPrincipal.IsInAnyRoles(new string[] { "System Administrator", "Company Administrator", "Delete Client" });
        }
    }
}
