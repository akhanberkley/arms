using System;
using System.Collections.Specialized;

namespace BCS.WebApp.Components
{
	/// <summary>
	/// Summary description for DefaultValues.
	/// </summary>
	public sealed class DefaultValues
	{
		/// <summary>
		/// Private ctor
		/// </summary>
		private DefaultValues() {}

		/// <summary>
		/// Check the web.config file for the Event Log Name.
		/// </summary>
		public static string EventLogName
		{
			get{return Globals.GetApplicationSetting("EventLogName");}
		}

        public static bool UseWizard
        {
            get { return Convert.ToBoolean(Globals.GetApplicationSetting("UseWizard")); }
        }

		/// <summary>
        /// Check the web.config file for the Environment for title.
		/// </summary>
        public static string EnvironmentTitle
		{
            get
            {
                string env = Globals.GetApplicationSetting("Environment");
                if (string.IsNullOrEmpty(env))
                    return string.Empty;
                return string.Format("* {0} Environment *", env);
            }
        }

        /// <summary>
        /// Check the web.config file for the Environment.
        /// </summary>
        public static string Environment
        {
            get
            {
                string env = Globals.GetApplicationSetting("Environment");
                if (string.IsNullOrEmpty(env))
                    return string.Empty;
                else
                    return env;
            }
        }
        public static string DefaultStyleSheet
        {
            get
            {
                string path = Globals.GetApplicationSetting("StaticPath");
                string dir = Globals.GetApplicationSetting("StaticCssDirectory");
                string file = Globals.GetApplicationSetting("DefaultStyleSheet");
                string slash = "/";

                return string.Concat(path.EndsWith(slash) ? path : (path + slash), dir.EndsWith(slash) ? dir : (dir + slash), file);
            }
        }
        public static string CssPath
        {
            get
            {
                string path = Globals.GetApplicationSetting("StaticPath");
                string dir = Globals.GetApplicationSetting("StaticCssDirectory");
                string slash = "/";

                return string.Concat(path.EndsWith(slash) ? path : (path + slash), dir.EndsWith(slash) ? dir : (dir + slash));
            }
        }
        public static string ImagePath
        {
            get
            {
                string path = Globals.GetApplicationSetting("StaticPath");
                string dir = Globals.GetApplicationSetting("StaticImageDirectory");
                string slash = "/";

                return string.Concat(path.EndsWith(slash) ? path : (path + slash), dir.EndsWith(slash) ? dir : (dir + slash));
            }
        }
        public static string ScriptPath
        {
            get
            {
                string path = Globals.GetApplicationSetting("StaticPath");
                string dir = Globals.GetApplicationSetting("StaticScriptDirectory");
                string slash = "/";

                return string.Concat(path.EndsWith(slash) ? path : (path + slash), dir.EndsWith(slash) ? dir : (dir + slash));
            }
        }

        /// <summary>
        /// Check the web.config file for the DSN.
        /// </summary>
        public static string DSN
        {
            get { return Globals.GetConnectionString("DSN"); }
        }

        /// <summary>
        /// default date format
        /// </summary>
        public static string DateFormat
        {
            get
            {
                return "MM/dd/yyyy";
            }
        }

		/// <summary>
		/// Check the web.config file for the DSN.
		/// </summary>
		public static int DefaultObjectCacheTimeInMinutes
		{
            get { return Convert.ToInt32(Globals.GetApplicationSetting("DefaultObjectCacheTimeInMinutes")); }
        }

        public static bool SupportsAPSServiceforAssignments(int companyId)
        {
            bool useAPSServiceforAssignments = false;

            BCS.Biz.CompanyParameter parameter = ConfigValues.GetCompanyParameter(companyId);

            if (parameter != null && parameter.UseAPSForAssignments.IsTrue)
                useAPSServiceforAssignments = (bool)parameter.UseAPSForAssignments;

            return useAPSServiceforAssignments;
        }

        public static bool SupportsFillPersonsInAssignments(int companyId)
        {
            bool bSupportsFillPersonsinAssignments = false;

            BCS.Biz.CompanyParameter parameter = ConfigValues.GetCompanyParameter(companyId);

            if (parameter != null && parameter.FillAllAssignments.IsTrue)
                bSupportsFillPersonsinAssignments = (bool)parameter.FillAllAssignments;

            return bSupportsFillPersonsinAssignments;
        }

        public static bool SupportsInternationalAddresses(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null ? userCompany.Company.SupportsInternationalAddresses.Value : false;
        }

        public static bool SupportsIndividualClients(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null ? userCompany.Company.SupportsIndividualClients.Value : false;
        }

        public static bool SupportsClassCodes(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null ? userCompany.Company.SupportsClassCodes.Value : false;
        }

        public static bool SupportsMultipleLineOfBusinesses(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null ? userCompany.Company.SupportsMultipleLineOfBusinesses.Value : false;
        }

        public static bool EnforceAgencyLicensedState(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null ? userCompany.Company.EnforceAgencyLicensedState.Value : false;
        }

        public static string[] GetStatusIds(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            return userCompany != null && !string.IsNullOrEmpty(userCompany.Company.LOBGridUpdateStatuses) ?
                userCompany.Company.LOBGridUpdateStatuses.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries) :
                new string[0];
        }

        /// <summary>
        /// gets a company specific theme. defaults to DEF.css (file name chosen arbitrarily to be consistent with company initials.
        /// will require rename if some company has initials to DEF.css and also change to the default returned here)
        /// </summary>
        /// <param name="Id">company Id</param>
        /// <returns></returns>
        public static string ThemeStyleSheet(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null && userCompany.Company.ThemeStyleSheet != null ?
                userCompany.Company.ThemeStyleSheet : DefaultStyleSheet;
        }

        public static StringCollection GetLOBGridExcludedUpdateStatuses(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            if (string.IsNullOrEmpty(userCompany.Company.LOBGridUpdateExcludedStatuses))
                return new StringCollection();

            string[] strs = userCompany.Company.LOBGridUpdateExcludedStatuses.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);            
            StringCollection sc = new StringCollection();
            foreach (string str in strs)
                sc.Add(str.Trim());
            return sc;
        }

        public static bool SupportsAgentUnspecifiedReasons(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null ? userCompany.Company.SupportsAgentUnspecifiedReasons.Value : false;
        }

        public static bool IsCobra(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null ? userCompany.Company.ClientSystemType.Code == "Cobra" : false;
        }

        public static bool IsBCSAsClientCore(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null ? userCompany.Company.ClientSystemType.Code == "BCSasClientCore" : false;
        }

        public static bool RequiresAgentUnspecifiedReasons(int companyId)
        {
            if (!SupportsAgentUnspecifiedReasons(companyId))
                return false;
            // assuming if company supports reasons, it requires them
            return true;
        }
        public static bool RequiresAddressType(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null ? userCompany.Company.RequiresAddressType.Value : false;
        }

        public static bool RequiresNameType(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null ? userCompany.Company.RequiresNameType.Value : false;
        }
        public static bool DesiresDBAAutoSelectWhenOnlyOne(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);


            return userCompany != null ? userCompany.Company.DesiresDBAAutoSelectWhenOnlyOne.Value : false;
        }
        public static bool DesiresInsuredAutoSelectWhenOnlyOne(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);


            return userCompany != null ? userCompany.Company.DesiresInsuredAutoSelectWhenOnlyOne.Value : false;
        }
        public static bool FillAllUnderwriters(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);


            return userCompany != null ? userCompany.Company.FillAllUnderwriters.Value : false;
        }

        public static string GetSpecialSearchText(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return string.Empty;
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);


            if (userCompany == null || userCompany.Company.SearchOnPolicyNumberOrSubmissonNumber.IsNull)
                return string.Empty;

            // 0 - Policy Number, 1 - Submission Number
            return userCompany.Company.SearchOnPolicyNumberOrSubmissonNumber.Value ? "Submission Number" : "Policy Number";
        }

        public static string GetAPSServiceUrl(int companyId)
        {
            string apsServiceUrl = string.Empty;
            string partial = "apsws/services/APSService";
            string companyInitials = string.Empty;

            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = null;
            if (userCompanies != null)
            {
                userCompany = userCompanies.FindByCompanyId(companyId);
            }
            
            if (userCompany != null && userCompany.Company != null)
                companyInitials = userCompany.Company.CompanyInitials;

            string apsEnv = Globals.GetApplicationSetting("APSServiceEnv");
            return string.Format("http://{0}-{1}/{2}", companyInitials, apsEnv, partial);
        }
    }
}
