using System;
using System.Web;

using BCS.Biz;

namespace BCS.WebApp.Components
{
	/// <summary>
	/// Summary description for SessionValues.
	/// </summary>
	public sealed class SessionValues
	{
	}

    public sealed class SessionKeys
    {
        public static readonly string CompanyId = "CompanySelected";
        public static readonly string CompanyNumberId = "CompanyNumberSelected";
        public static readonly string CompanyNumber = "CompanyNumber";

        public static readonly string CompanyInitialized = "CompanyInitialized";
        public static readonly string UserSession = string.Format("UserSession4{0}", HttpContext.Current.User.Identity.Name);
        public static readonly string VerifyingSubmission = "VerifyingSubmission";
        public static readonly string SearchedClients = "Clients";
        public static readonly string SearchedCriteria = "SearchedCriteria";
    }
    public sealed class CacheKeys
    {
        public static string UserAgency
        {
            get
            {
                return "Agency4{0}";
            }
        }
        public static string UserRoles
        {
            get
            {
                return "ROLES4{0}";
            }
        }
        public static string UserCompanies
        {
            get
            {
                return "Companies4{0}";
            }
        }

        public static string CompanyNumberAgencies
        {
            get
            {
                return "Agencies4CompanyNumber{0}";
            }
        }

        public static string CompanyNumberSubmissionDisplayGrid
        {
            get
            {
                return "SubmissionDisplayGrid4CompanyNumber{0}";
            }
        }
        public static string CompanyNumberSubmissionDisplayGridSS
        {
            get
            {
                return "SubmissionDisplayGrid4CompanyNumber{0}OnSS";
            }
        }

        public static string CompanyNumberSubmissionDisplayGridSC
        {
            get
            {
                return "SubmissionDisplayGrid4CompanyNumber{0}OnSC";
            }
        }
        public static string CompanyNumberLOBGrid
        {
            get
            {
                return "LOBGrid4CompanyNumber{0}";
            }
        }

        public static string CompanyNumberStatuses
        {
            get
            {
                return "Statuses4CompanyNumber{0}";
            }
        }

        public static string CompanyNumberTypes
        {
            get
            {
                return "Types4CompanyNumber{0}";
            }
        }

        public static string CompanyNumberStatusReasons
        {
            get
            {
                return "StatusReasons4CompanyNumber{0}";
            }
        }

        public static string WizardSteps
        {
            get
            {
                return "WizardSteps4{0}";
            }
        }

        public static string SubmissionFieldMaps
        {
            get
            {
                return "SubmissionFieldMaps";
            }
        }

        public static string SubmissionSpecificCompanyNumberCodeTypes
        {
            get
            {
                return "SubmissionSpecificCodeTypes4CompanyNumber{0}";
            }
        }

        public static string ClientSpecificCompanyNumberCodeTypes
        {
            get
            {
                return "ClientSpecificCodeTypes4CompanyNumber{0}";
            }
        }

        public static string SubmissionSpecificCompanyNumberAttributes
        {
            get
            {
                return "SubmissionSpecificAttributes4CompanyNumber{0}";
            }
        }

        public static string ClientSpecificCompanyNumberAttributes
        {
            get
            {
                return "ClientSpecificAttributes4CompanyNumber{0}";
            }
        }
        public static string CompanyParameterForCompanyId
        {
            get
            {
                return "CompanyParameterCompanyId{0}";
            }
        }
    }
}
