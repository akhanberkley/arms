using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.Collections;

namespace BCS.WebApp.Components
{
    /// <summary>
    /// helper class to retrieve configuration settings
    /// </summary>
    public class ConfigValues
    {
        public static bool NameEditable(int companyId)
        {
            return true;
        }
        public static Biz.Company GetCurrentCompany(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if(null == userCompany || null==userCompany.Company)
                return null;
            return userCompany.Company;
        }
        private static bool IsCobraOrCobraRelated(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            try
            {
                return userCompany.Company.ClientSystemType.Code.Equals("Cobra", StringComparison.CurrentCultureIgnoreCase) ||
                        userCompany.Company.ClientSystemType.Code.Equals("bcsasclientcore", StringComparison.CurrentCultureIgnoreCase);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static int GetBusinessNameLength(int companyId)
        {
            return 0;
        }

        public static bool AddressEditable(int companyId)
        {
            return true;
        }

        public static string GetEditButtonDixplayText(int companyId,bool apsCanEdit)
        {
            if (Security.IsUserReadOnly() || (ConfigValues.UsesAPS(companyId) && !apsCanEdit))
                return "View";
            else
                return "Edit";
        }

        public static int GetStreetnameLength(int companyId)
        {
            return 40;
        }

        public static int GetAddress2Length(int companyId)
        {
            return 40;
        }

        public static int GetCityLength(int companyId)
        {
            return 40;
        }

        public static int GetStreetnumberLength(int companyId)
        {
            return 10;
        }

        public static bool DesiresDefaultStatusOnSubmissionCopy(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);


            return userCompany != null ? userCompany.Company.DesiresDefaultStatusOnSubmissionCopy.Value : false;
        }
        public static bool DesiresLoadClientBeforeStep(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);


            return userCompany != null ? userCompany.Company.LoadClientBeforeStep.Value : false;
        }

        public static bool RequiresPolicyNumber(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null ? userCompany.Company.RequiresPolicyNumber.Value : false;
        }

        public static bool RequiresAnalyst(int companyId)
        {
            Biz.DataManager dm = new Biz.DataManager(Components.DefaultValues.DSN);
            dm.QueryCriteria.And(Biz.JoinPath.CompanyParameter.Columns.CompanyID, companyId);

            Biz.CompanyParameterCollection cp = dm.GetCompanyParameterCollection();

            if (cp.Count > 0 && cp[0].RequiresUWAssistant)
                return true;

            return false;
        }

        public static bool RequiresUnderWriter(int companyId)
        {
            Biz.DataManager dm = new Biz.DataManager(Components.DefaultValues.DSN);
            dm.QueryCriteria.And(Biz.JoinPath.CompanyParameter.Columns.CompanyID, companyId);

            Biz.CompanyParameterCollection cp = dm.GetCompanyParameterCollection();

            if (cp.Count > 0 && cp[0].RequiresUW)
                return true;

            return false;
        }

        public static Biz.CompanyParameter GetCompanyParameter(int companyId)
        {
            BTS.CacheEngine.V2.CacheEngine cache = new BTS.CacheEngine.V2.CacheEngine(DefaultValues.DefaultObjectCacheTimeInMinutes);
            string cacheKey = string.Format(CacheKeys.CompanyParameterForCompanyId, companyId);
            Biz.CompanyParameter returnParameter = null;

            if (!cache.CachedItemExist(cacheKey))
            {
                Biz.DataManager dm = new Biz.DataManager(DefaultValues.DSN);
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.CompanyParameter.Columns.CompanyID, companyId);

                Biz.CompanyParameterCollection parameters = dm.GetCompanyParameterCollection();

                if (parameters != null && parameters.Count > 0)
                {
                    cache.AddItemToCache(cacheKey, parameters[0]);
                    returnParameter = parameters[0];
                }
            }
            else
            {
                returnParameter = (Biz.CompanyParameter)cache.GetCachedItem(cacheKey);
            }

            return returnParameter;
        }

        public static bool DisplayCopyStatusesOption(int companyId)
        {
            Biz.CompanyParameter p = GetCompanyParameter(companyId);
            return p.CopySubmissionStatusOption.IsTrue;
        }

        public static bool RequiresInsuredName(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);


            return userCompany != null ? userCompany.Company.SubmissionPage.RequiresInsuredName.Value : false;
        }

        public static bool RequiresInsuredOrDBAName(int companyId)
        {
            Biz.CompanyParameter cp = GetCompanyParameter(companyId);

            if (cp != null && !cp.RequiresInsuredOrDBA.IsNull)
            {
                return (bool)cp.RequiresInsuredOrDBA;
            }

            return false;
        }

        public static bool UsesTechnician(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return false;
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany == null)
                return false;
            if (userCompany.Company == null)
                return false;

            return !userCompany.Company.TechnicianRoleId.IsNull;
        }

        public static bool UsesAnalyst(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return false;
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany == null)
                return false;
            if (userCompany.Company == null)
                return false;

            if (userCompany.Company.Id == 1 || userCompany.Company.AnalystRoleId.IsNull)
                return false;

            return true;
        }

        public static bool AutoSelectNamesOnSubmissionAdd(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return false;
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany == null)
                return false;
            if (userCompany.Company == null)
                return false;

            return userCompany.Company.AutoSelectNamesOnSubmissionAdd.Value;
        }

        public static bool GenerateOnlyPolicyNumber(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);


            return userCompany != null ? userCompany.Company.GenerateOnlyPolicyNumber.Value : false;
        }

        public static bool SupportsResequencing(int companyId)
        {
            // base on current company, it not all companies support
            return true;
        }

        public static bool SupportsSnapShotEditing(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return false;
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany == null)
                return false;
            if (userCompany.Company == null)
                return false;
            if (userCompany.Company.SubmissionPage == null)
                return false;

            return userCompany.Company.SubmissionPage.SnapShotEditable.Value;
        }
        public static bool SupportsShellSubmissions(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return false;
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany == null)
                return false;
            if (userCompany.Company == null)
                return false;

            return userCompany.Company.SupportsShellSubmission.Value;
        }
        public static bool SupportsDuplicateOnSubmissionGrid(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (null == userCompanies)
                return false;

            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return userCompany != null ? userCompany.Company.SupportsDuplicates.Value : false;
        }

        public static string GetAddInitialFocusField(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return
                userCompany != null && userCompany.Company.SubmissionPage != null && !string.IsNullOrEmpty(userCompany.Company.SubmissionPage.AddInitialFocusField)
                ? userCompany.Company.SubmissionPage.AddInitialFocusField : string.Empty;
        }

        public static string GetPolicyNumberRegex(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return ".*";
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany == null)
                return ".*";
            if (userCompany.Company == null)
                return ".*";

            return userCompany.Company.PolicyNumberRegex;
        }

        public static string GetDisplayText(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return "(not specified)";
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany == null)
                return "(not specified)";
            if (userCompany.Company == null)
                return "(not specified)";
            if (userCompany.Company.SubmissionPage == null)
                return "(not specified)";

            return userCompany.Company.SubmissionPage.DefaultText;
        }

        public static string GetEditInitialFocusField(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);

            return
                userCompany != null && userCompany.Company.SubmissionPage != null && !string.IsNullOrEmpty(userCompany.Company.SubmissionPage.EditInitialFocusField)
                ? userCompany.Company.SubmissionPage.EditInitialFocusField : string.Empty;
        }

        public static string[] GetEditDisabledFields(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany != null && userCompany.Company != null && userCompany.Company.SubmissionPage != null && !string.IsNullOrEmpty(userCompany.Company.SubmissionPage.EditDisabledFields))
            {
                return userCompany.Company.SubmissionPage.EditDisabledFields.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            }
            else
                return new string[0];
        }

        public static string[] GetEditInvisibleFields(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany != null && userCompany.Company != null && userCompany.Company.SubmissionPage != null && !string.IsNullOrEmpty(userCompany.Company.SubmissionPage.EditInvisibleFields))
            {
                return userCompany.Company.SubmissionPage.EditInvisibleFields.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            }
            else
                return new string[0];
        }

        public static string[] GetAddInvisibleFields(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany != null && userCompany.Company != null && userCompany.Company.SubmissionPage != null && !string.IsNullOrEmpty(userCompany.Company.SubmissionPage.AddInvisibleFields))
            {
                return userCompany.Company.SubmissionPage.AddInvisibleFields.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            }
            else
                return new string[0];
        }

        public static string[] GetAddDisabledFields(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany != null && userCompany.Company != null && userCompany.Company.SubmissionPage != null && !string.IsNullOrEmpty(userCompany.Company.SubmissionPage.AddDisabledFields))
            {
                return userCompany.Company.SubmissionPage.AddDisabledFields.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            }
            else
                return new string[0];
        }

        public static List<string> GetCopyExcludedFields(int companyId)
        {
            List<string> list = new List<string>();
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany != null && userCompany.Company != null && userCompany.Company.SubmissionPage != null && !string.IsNullOrEmpty(userCompany.Company.SubmissionPage.CopyExcludedFields))
            {
                list.AddRange(userCompany.Company.SubmissionPage.CopyExcludedFields.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
            }
            return list;
        }

        public static bool FreehandOnlyPolicyNumber(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);


            return userCompany != null ? userCompany.Company.FreehandOnlyPolicyNumber.Value : false;
        }

        public static bool UsesAPS(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return false;
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany == null)
                return false;
            if (userCompany.Company == null)
                return false;

            return userCompany.Company.UsesAPS.Value;
        }

        public static bool UsesCobra(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return false;
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany == null)
                return false;
            if (userCompany.Company == null)
                return false;

            return userCompany.Company.ClientSystemType.Code == "BCSasClientCORE";
        }

        public static bool AgenciesInGrid(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return false;
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany == null)
                return false;
            if (userCompany.Company == null)
                return false;

            return userCompany.Company.AgenciesInGrid.Value;
        }

        public static int SubmissionDisplayStartDate(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return 10;
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany == null)
                return 10;
            if (userCompany.Company == null)
                return 10;

            Biz.DataManager dm = new Biz.DataManager(Components.DefaultValues.DSN);
            dm.QueryCriteria.And(Biz.JoinPath.CompanyParameter.Columns.CompanyID, userCompany.CompanyId);

            Biz.CompanyParameterCollection parameters = dm.GetCompanyParameterCollection();
            if (parameters == null || parameters.Count < 1) return 10;

            return (int)parameters[0].SubmissionDisplayYears;
        }
        
        public static WizardConfigSteps GetWizardSteps(int companyId)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            if (userCompanies == null)
                return null;
            Biz.UserCompany userCompany = userCompanies.FindByCompanyId(companyId);
            if (userCompany == null)
                return null;
            if (userCompany.Company == null)
                return null;

            string companyInitials = userCompany.Company.CompanyInitials;

            string cacheKey = string.Format(CacheKeys.WizardSteps, companyId);
            if (HttpContext.Current.Cache[cacheKey] == null)
            {
                string concatString = string.IsNullOrEmpty(DefaultValues.Environment) ? string.Empty : DefaultValues.Environment + ".";
                string vPath = "~/App_Data/" + concatString + "WizardConfiguration.xml";
                WizardConfigSteps steps = new WizardConfigSteps();
                List<WizardConfigStep> dictionary = new List<WizardConfigStep>();
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(HttpContext.Current.Server.MapPath(vPath));
                XPathNavigator nav = xdoc.CreateNavigator();
                #region Get Individual Steps
                XPathNodeIterator iterator = nav.Select(string.Format("WizardSteps/{0}/WizardStep", companyInitials));
                while (iterator.MoveNext())
                {
                    XPathNavigator var = iterator.Current;

                    WizardConfigStep step = new WizardConfigStep();
                    step.Index = Convert.ToInt32(var.GetAttribute("Index", string.Empty));
                    step.AddVisible = Convert.ToBoolean(var.GetAttribute("AddVisible", string.Empty));
                    step.EditVisible = Convert.ToBoolean(var.GetAttribute("EditVisible", string.Empty));
                    step.FocusOn = var.GetAttribute("Focus", string.Empty);
                    step.TabForward = var.GetAttribute("OnTabForward", string.Empty);
                    step.TabBackward = var.GetAttribute("OnTabBackward", string.Empty);

                    dictionary.Add(step);
                }
                steps.Steps = dictionary;
                #endregion
                
                {
                    iterator = nav.Select(string.Format("WizardSteps/{0}", companyInitials));
                    iterator.MoveNext();
                    XPathNavigator var = iterator.Current;

                    steps.AddDefaultStepIndex = Convert.ToInt32(var.GetAttribute("AddDefaultStep", string.Empty));
                    steps.EditDefaultStepIndex = Convert.ToInt32(var.GetAttribute("EditDefaultStep", string.Empty));
                    steps.CopyDefaultStepIndex = Convert.ToInt32(var.GetAttribute("CopyDefaultStep", string.Empty));
                }
                HttpContext.Current.Cache.Add(cacheKey, steps,
                    new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath(vPath)),
                    System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration,
                    System.Web.Caching.CacheItemPriority.Normal, null);
                return steps;
            }
            else
            {
                WizardConfigSteps steps = (WizardConfigSteps)HttpContext.Current.Cache[cacheKey];
                return steps;
            }
        }

        public static Biz.SubmissionDisplayGridCollection GetSubmissionGridDisplay(int companyNumberId)
        {
            if (companyNumberId == 0)
                return new BCS.Biz.SubmissionDisplayGridCollection();
            string cacheKey = string.Format(CacheKeys.CompanyNumberSubmissionDisplayGrid, companyNumberId);
            if (HttpContext.Current.Cache[cacheKey] == null)
            {
                Biz.DataManager dm = new Biz.DataManager(Components.DefaultValues.DSN);
                dm.QueryCriteria.And(Biz.JoinPath.SubmissionDisplayGrid.Columns.CompanyNumberId, companyNumberId);
                dm.QueryCriteria.And(Biz.JoinPath.SubmissionDisplayGrid.Columns.DisplayOnClientSearchScreen, 1);
                Biz.SubmissionDisplayGridCollection dfields = dm.GetSubmissionDisplayGridCollection(Biz.FetchPath.SubmissionDisplayGrid.AttributeDataType).SortByDisplayOrder(OrmLib.SortDirection.Ascending);

                HttpContext.Current.Cache[cacheKey] = dfields;

                Biz.SubmissionDisplayGridCollection disconnectedDfields = new BCS.Biz.SubmissionDisplayGridCollection();
                foreach (Biz.SubmissionDisplayGrid var in dfields)
                {
                    disconnectedDfields.Add(var);
                }

                return disconnectedDfields;
            }
            else
            {
                Biz.SubmissionDisplayGridCollection dfields = (Biz.SubmissionDisplayGridCollection)HttpContext.Current.Cache[cacheKey];

                Biz.SubmissionDisplayGridCollection disconnectedDfields = new BCS.Biz.SubmissionDisplayGridCollection();
                foreach (Biz.SubmissionDisplayGrid var in dfields)
                {
                    disconnectedDfields.Add(var);
                }

                return disconnectedDfields;
            }
        }


        public static Biz.SubmissionDisplayGridCollection GetSubmissionGridDisplaySC(int companyNumberId)
        {
            if (companyNumberId == 0)
                return new BCS.Biz.SubmissionDisplayGridCollection();
            string cacheKey = string.Format(CacheKeys.CompanyNumberSubmissionDisplayGridSC, companyNumberId);
            if (HttpContext.Current.Cache[cacheKey] == null)
            {
                Biz.DataManager dm = new Biz.DataManager(Components.DefaultValues.DSN);
                dm.QueryCriteria.And(Biz.JoinPath.SubmissionDisplayGrid.Columns.CompanyNumberId, companyNumberId);
                dm.QueryCriteria.And(Biz.JoinPath.SubmissionDisplayGrid.Columns.DisplayOnSubmissionSearchScreen, 1);
                Biz.SubmissionDisplayGridCollection dfields = dm.GetSubmissionDisplayGridCollection(Biz.FetchPath.SubmissionDisplayGrid.AttributeDataType).SortByDisplayOrder(OrmLib.SortDirection.Ascending);

                HttpContext.Current.Cache[cacheKey] = dfields;

                Biz.SubmissionDisplayGridCollection disconnectedDfields = new BCS.Biz.SubmissionDisplayGridCollection();
                foreach (Biz.SubmissionDisplayGrid var in dfields)
                {
                    disconnectedDfields.Add(var);
                }

                return disconnectedDfields;
            }
            else
            {
                Biz.SubmissionDisplayGridCollection dfields = (Biz.SubmissionDisplayGridCollection)HttpContext.Current.Cache[cacheKey];

                Biz.SubmissionDisplayGridCollection disconnectedDfields = new BCS.Biz.SubmissionDisplayGridCollection();
                foreach (Biz.SubmissionDisplayGrid var in dfields)
                {
                    disconnectedDfields.Add(var);
                }

                return disconnectedDfields;
            }
        }
        public static Biz.SubmissionDisplayGridCollection GetSubmissionGridDisplaySS(int companyNumberId)
        {
            if (companyNumberId == 0)
                return new BCS.Biz.SubmissionDisplayGridCollection();
            string cacheKey = string.Format(CacheKeys.CompanyNumberSubmissionDisplayGridSS, companyNumberId);
            if (HttpContext.Current.Cache[cacheKey] == null)
            {
                Biz.DataManager dm = new Biz.DataManager(Components.DefaultValues.DSN);
                dm.QueryCriteria.And(Biz.JoinPath.SubmissionDisplayGrid.Columns.CompanyNumberId, companyNumberId);
                dm.QueryCriteria.And(Biz.JoinPath.SubmissionDisplayGrid.Columns.DisplayOnSubmissionScreen, 1);
                Biz.SubmissionDisplayGridCollection dfields = dm.GetSubmissionDisplayGridCollection(Biz.FetchPath.SubmissionDisplayGrid.AttributeDataType).SortByDisplayOrder(OrmLib.SortDirection.Ascending);

                HttpContext.Current.Cache[cacheKey] = dfields;

                Biz.SubmissionDisplayGridCollection disconnectedDfields = new BCS.Biz.SubmissionDisplayGridCollection();
                foreach (Biz.SubmissionDisplayGrid var in dfields)
                {
                    disconnectedDfields.Add(var);
                }

                return disconnectedDfields;
            }
            else
            {
                Biz.SubmissionDisplayGridCollection dfields = (Biz.SubmissionDisplayGridCollection)HttpContext.Current.Cache[cacheKey];

                Biz.SubmissionDisplayGridCollection disconnectedDfields = new BCS.Biz.SubmissionDisplayGridCollection();
                foreach (Biz.SubmissionDisplayGrid var in dfields)
                {
                    disconnectedDfields.Add(var);
                }

                return disconnectedDfields;
            }
        }

        public static Biz.CompanyNumberAttributeCollection GetCompanyAttributes(int companyNumberId, bool clientSpecific)
        {
            if (companyNumberId == 0)
                return new BCS.Biz.CompanyNumberAttributeCollection();
            BCS.Biz.CompanyNumberAttributeCollection attrs = null;
            string AttrCacheKey = string.Format(!clientSpecific ? CacheKeys.SubmissionSpecificCompanyNumberAttributes :
                CacheKeys.ClientSpecificCompanyNumberAttributes, companyNumberId);
            if (HttpContext.Current.Cache[AttrCacheKey] == null)
            {
                BCS.Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberAttribute.Columns.CompanyNumberId, companyNumberId);
                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberAttribute.Columns.ClientSpecific, clientSpecific);
                attrs = dm.GetCompanyNumberAttributeCollection(BCS.Biz.FetchPath.CompanyNumberAttribute.AttributeDataType);
                attrs = attrs.SortByDisplayOrder(OrmLib.SortDirection.Ascending);
                HttpContext.Current.Cache[AttrCacheKey] = attrs;
            }
            else
            {
                attrs = (BCS.Biz.CompanyNumberAttributeCollection)HttpContext.Current.Cache[AttrCacheKey];
            }

            return attrs;
        }

        public static Biz.CompanyNumberCodeTypeCollection GetCompanyCodeTypes(int companyNumberId, bool clientSpecific)
        {
            return GetCompanyCodeTypes(companyNumberId, clientSpecific, false,false);
        }

        public static Biz.CompanyNumberCodeTypeCollection GetCompanyCodeTypes(int companyNumberId, bool clientSpecific, bool classCodeSpecific, bool otherCarrierSpecific)
        {
            if (companyNumberId == 0)
                return new BCS.Biz.CompanyNumberCodeTypeCollection();
            BCS.Biz.CompanyNumberCodeTypeCollection types = null;
            string CTCacheKey = string.Format(!clientSpecific ? CacheKeys.SubmissionSpecificCompanyNumberCodeTypes :
                CacheKeys.ClientSpecificCompanyNumberCodeTypes, companyNumberId);
            if (HttpContext.Current.Cache[CTCacheKey] == null)
            {
                BCS.Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCodeType.Columns.CompanyNumberId, companyNumberId);
                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCodeType.Columns.ClientSpecific, clientSpecific);
                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCodeType.Columns.ClassCodeSpecific, classCodeSpecific);
                //dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCodeType.Columns.OtherCarrierSpecific, otherCarrierSpecific);

                types = dm.GetCompanyNumberCodeTypeCollection();
                types = types.SortByDisplayOrder(OrmLib.SortDirection.Ascending);
                HttpContext.Current.Cache[CTCacheKey] = types;
            }
            else
            {
                types = (BCS.Biz.CompanyNumberCodeTypeCollection)HttpContext.Current.Cache[CTCacheKey];
            }
            return types;
        }
        
        public static List<SubmissionField> GetMappingFields(int companyNumberId)
        {
            string cacheKey = CacheKeys.SubmissionFieldMaps;
            XmlElement root = null;
            if (HttpContext.Current.Cache[cacheKey] == null)
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(HttpContext.Current.Server.MapPath("~/App_Data/SubmissionFieldMap.xml"));
                root = xdoc.DocumentElement;
                HttpContext.Current.Cache.Add(cacheKey, root,
                    new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/App_Data/SubmissionFieldMap.xml")),
                    System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration,
                    System.Web.Caching.CacheItemPriority.Normal, null);
            }
            else
            {
                root = (XmlElement)HttpContext.Current.Cache[cacheKey];
            }

            List<SubmissionField> fields = new List<SubmissionField>();
            IEnumerator enumerator = root.GetEnumerator();
            while (enumerator.MoveNext())
            {
                XmlNode controlNode = enumerator.Current as XmlNode;
                if (null != controlNode && controlNode.Name == "control")
                {
                    SubmissionField sf = new SubmissionField();
                    sf.ControlId = controlNode.Attributes["id"].Value;
                    List<FieldPair> fieldPairs = new List<FieldPair>();

                    foreach (XmlNode var in controlNode.ChildNodes)
                    {
                        FieldPair fp = new FieldPair();
                        fp.Id = var.Attributes["id"].InnerText.Replace("XXX", sf.ControlId).Replace("Codeddl", string.Empty);
                        fp.Type = var.Attributes["type"].InnerText.Replace("XXX", sf.ControlId).Replace("Codeddl", string.Empty);

                        fieldPairs.Add(fp);
                    }
                    sf.AssociatedControlIds = fieldPairs;
                    fields.Add(sf);
                }
            }

            Biz.CompanyNumberAttributeCollection attrs = GetCompanyAttributes(companyNumberId, false);
            Biz.CompanyNumberCodeTypeCollection types = GetCompanyCodeTypes(companyNumberId, false);
            List<SubmissionField> calculatedFields = new List<SubmissionField>();
            foreach (SubmissionField field in fields)
            {
                if (field.ControlId.Contains("XXXCodeddl")) // for code types
                {
                    foreach (Biz.CompanyNumberCodeType type in types)
                    {
                        SubmissionField sf = new SubmissionField();
                        sf.AssociatedControlIds = new List<FieldPair>();
                        sf.ControlId = field.ControlId.Replace("XXX", type.Id.ToString());
                        foreach (FieldPair pair in field.AssociatedControlIds)
                        {
                            FieldPair fp = new FieldPair();
                            fp.Id = pair.Id.Replace("XXX", type.Id.ToString());
                            fp.Type = pair.Type.Replace("XXX", type.Id.ToString());
                            sf.AssociatedControlIds.Add(fp);
                        }
                        calculatedFields.Add(sf);
                    }
                }
                if (field.ControlId.Contains("XXXattr")) // for code types
                {
                    foreach (Biz.CompanyNumberAttribute attr in attrs)
                    {
                        SubmissionField sf = new SubmissionField();
                        sf.AssociatedControlIds = new List<FieldPair>();
                        sf.ControlId = field.ControlId.Replace("XXXattr", attr.Id.ToString());
                        foreach (FieldPair pair in field.AssociatedControlIds)
                        {
                            FieldPair fp = new FieldPair();
                            fp.Id = pair.Id.Replace("XXXattr", attr.Id.ToString());
                            fp.Type = pair.Type.Replace("XXXattr", attr.Id.ToString());
                            sf.AssociatedControlIds.Add(fp);
                        }
                        calculatedFields.Add(sf);
                    }
                }
            }
            fields.AddRange(calculatedFields);
            
            return fields;
        }

        public static Dictionary<string, string> GetMappingFields(string fieldName)
        {
            string cacheKey = CacheKeys.SubmissionFieldMaps;
            XmlElement root = null;
            if (HttpContext.Current.Cache[cacheKey] == null)
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(HttpContext.Current.Server.MapPath("~/App_Data/SubmissionFieldMap.xml"));
                root = xdoc.DocumentElement;
                HttpContext.Current.Cache.Add(cacheKey, root,
                    new System.Web.Caching.CacheDependency(HttpContext.Current.Server.MapPath("~/App_Data/SubmissionFieldMap.xml")),
                    System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration,
                    System.Web.Caching.CacheItemPriority.Normal, null);
            }
            else
            {
                root = (XmlElement)HttpContext.Current.Cache[cacheKey];
            }

            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            string searchString = string.Format("/controls/control[@id='{0}']", fieldName);
            if (fieldName.EndsWith("ddl")) // for code types
            {
                searchString = string.Format("/controls/control[@id='{0}']", "XXXCodeddl");
            }
            if (OrmLib.Validator.IsNumeric(fieldName)) // for attributes
            {
                searchString = string.Format("/controls/control[@id='{0}']", "XXXattr");
            }
            XmlNode controlNode = root.SelectSingleNode(searchString);
            if (controlNode == null)
                return dictionary;

            foreach (XmlNode var in controlNode.ChildNodes)
            {
                dictionary.Add(var.Attributes["id"].InnerText.Replace("XXX", fieldName).Replace("Codeddl", string.Empty),
                    var.Attributes["type"].InnerText.Replace("XXX", fieldName).Replace("Codeddl", string.Empty));
            }
            return dictionary;
        }

        public static List<SubmissionCopyExclusion> GetCopyExcludedFieldsDetailed(int companyId)
        {
            List<SubmissionCopyExclusion> exclusions = new List<SubmissionCopyExclusion>();

            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            BCS.Biz.UserCompany uCompany = userCompanies.FindByCompanyId(companyId);
            BCS.Biz.Company company = uCompany.Company;
            Biz.SubmissionCopyExclusionCollection ordered = company.SubmissionCopyExclusions.SortById(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.SubmissionCopyExclusion var in ordered)
            {

                SubmissionCopyExclusion exclusion = new SubmissionCopyExclusion();
                exclusion.ControlId = var.ControlId;
                exclusion.Header = var.Header;
                exclusion.Method = var.Method;
                exclusion.Enabled = var.Enabled.Value;
                exclusion.Visible = var.Visible.Value;
                if (!string.IsNullOrEmpty(var.MethodParams))
                {
                    exclusion.MethodParams = var.MethodParams.Split(",".ToCharArray());
                }
                exclusions.Add(exclusion);
            }
            return exclusions;
        }


        public static bool SupportsOptionToSearchDeletedSubmissions()
        {
            Biz.CompanyParameter param = GetCompanyParameter(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

            if (param != null)
                return param.OptionToViewDeletedSubmissions.IsTrue;

            return false;
        }
       
    }
}
