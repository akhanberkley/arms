﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;


namespace BCS.WebApp
{
    /// <summary>
    /// Summary description for Email
    /// </summary>
    public class Email
    {
        public static void SendErrorEmail(Exception ex)
        {
            if (ex == null)
                return;
           
            string to = ConfigurationManager.AppSettings["ErrorEmailToAddresses"].ToString();
            string from = ConfigurationManager.AppSettings["ErrorEmailAddressFrom"].ToString();
            string subject = "BCS Fatal Exception " + ex.Message;

            
            string body = DateTime.Now.ToString() + "\n\n";
            body += ex.Message + "\n\n";
            body += ex.InnerException + "\n\n";
            body += ex.StackTrace;

            
            MailMessage msg = new MailMessage(from, to, subject, body);

            SendEmail(msg);
        }

        public static void SendEmail(MailMessage msg)
        {
            string smtp = ConfigurationManager.AppSettings["SmtpServer"].ToString();
            if (string.IsNullOrWhiteSpace(smtp))
                smtp = "smtp.wrberkley.com";

            SmtpClient client = new SmtpClient(smtp);
            client.Send(msg);
        }

        public static void SendRequestToDeleteClientId(long clientID, string companyName)
        {
            MailMessage msg = new MailMessage();

            msg.To.Add(ConfigurationManager.AppSettings["DeleteClientRequestTo"]);
            
            MailAddress from = new MailAddress("BTSClearance@wrberkley.com","Clearance Client Delete Request");
            msg.From = from;

            msg.Subject = "Delete Client";

            string bodyText = ConfigurationManager.AppSettings["DeleteClientRequestMSG"];

            msg.Body = string.Format(bodyText, HttpContext.Current.User.Identity.Name, clientID, companyName, ConfigurationManager.AppSettings["Environment"]);

            SendEmail(msg);
        }
    }
}