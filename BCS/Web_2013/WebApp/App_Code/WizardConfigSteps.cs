using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

namespace BCS.WebApp
{
    public class WizardConfigSteps
    {
        public WizardConfigSteps()
        {

        }
        private List<WizardConfigStep> steps;
        private int editDefaultStepIndex, addDefaultStepIndex, copyDefaultStepIndex;

        public int AddDefaultStepIndex
        {
            get { return addDefaultStepIndex; }
            set { addDefaultStepIndex = value; }
        }

        public int CopyDefaultStepIndex
        {
            get { return copyDefaultStepIndex; }
            set { copyDefaultStepIndex = value; }
        }

        public int EditDefaultStepIndex
        {
            get { return editDefaultStepIndex; }
            set { editDefaultStepIndex = value; }
        }

        public List<WizardConfigStep> Steps
        {
            get { return steps; }
            set { steps = value; }
        }
    }

    public class WizardConfigStep
    {
        private int index;
        private bool addVisible, editVisible;
        private string focusOn, tabForward, tabBackward;

        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        public bool AddVisible
        {
            get { return addVisible; }
            set { addVisible = value; }
        }

        public bool EditVisible
        {
            get { return editVisible; }
            set { editVisible = value; }
        }

        public string FocusOn
        {
            get { return focusOn; }
            set { focusOn = value; }
        }

        public string TabForward
        {
            get { return tabForward; }
            set { tabForward = value; }
        }

        public string TabBackward
        {
            get { return tabBackward; }
            set { tabBackward = value; }
        }
    } 
}
