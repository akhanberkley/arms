using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BCS.WebApp
{
    public class SubmissionCopyExclusion
    {
        string controlId;

        public string ControlId
        {
            get { return controlId; }
            set { controlId = value; }
        }

        string header;

        public string Header
        {
            get { return header; }
            set { header = value; }
        }
        string method;

        public string Method
        {
            get { return method; }
            set { method = value; }
        }
        object[] methodParams;

        public object[] MethodParams
        {
            get { return methodParams; }
            set { methodParams = value; }
        }

        bool visible;

        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }

        bool enabled;

        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }
    } 
}
