#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for LineOfBusinessDropDownList
    /// </summary>
    public class LineOfBusinessDropDownList : System.Web.UI.WebControls.DropDownList
    {
        #region DataBind
        public override void DataBind()
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            base.Items.Clear();

            LineOfBusinessCollection lobc = dataManager.GetLineOfBusinessCollection();
            lobc = lobc.SortByCode(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.LineOfBusiness lob in lobc)
            {
                base.Items.Add(new ListItem(lob.Code, lob.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        } 
        #endregion

        #region Special DataBindings...
        public void DataBindByAgency(int agencyId)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(Biz.JoinPath.Agency.Columns.Id, agencyId);
            BCS.Biz.Agency agency = dataManager.GetAgency();

            dataManager.QueryCriteria.Clear();
            if (agency != null && agency.UnfilteredUnderwriting)
                dataManager.QueryCriteria.And(JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.CompanyNumber.Columns.Id, agency.CompanyNumberId);
            else
                dataManager.QueryCriteria.And(JoinPath.LineOfBusiness.AgencyLineOfBusiness.Agency.Columns.Id, agencyId);

            base.Items.Clear();

            LineOfBusinessCollection lobc = dataManager.GetLineOfBusinessCollection();
            lobc = lobc.SortByCode(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.LineOfBusiness lob in lobc)
            {
                base.Items.Add(new ListItem(lob.Code, lob.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("","0"));

            if (Items.Count == 2)
            {
                Items[1].Selected = true;
            }
        }

        public void DataBindByCompanyNumber(int companyNumberId)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);
            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.CompanyNumber.Columns.Id, companyNumberId);

            base.Items.Clear();

            LineOfBusinessCollection lobc = dataManager.GetLineOfBusinessCollection();
            lobc = lobc.SortByCode(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.LineOfBusiness lob in lobc)
            {
                base.Items.Add(new ListItem(lob.Code, lob.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("","0"));

            if (Items.Count == 2)
            {
                Items[1].Selected = true;
            }
        }

        public void DataBindBySubmission(int submissionId)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);
            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(JoinPath.LineOfBusiness.SubmissionLineOfBusiness.Submission.Columns.Id, submissionId);

            base.Items.Clear();

            LineOfBusinessCollection lobc = dataManager.GetLineOfBusinessCollection();
            lobc = lobc.SortByCode(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.LineOfBusiness lob in lobc)
            {
                base.Items.Add(new ListItem(lob.Code, lob.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("","0"));

            if (Items.Count == 2)
            {
                Items[1].Selected = true;
            }
        }
        #endregion
    }
}