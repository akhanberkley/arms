#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for UserDropDownList
    /// </summary>
    public class UserDropDownList : System.Web.UI.WebControls.DropDownList
    {

        protected override void OnInit(EventArgs e)
        {
            if (DataTextField == null || DataTextField.Length == 0)
                DataTextField = "Username";
            if (DataValueField == null || DataValueField.Length == 0)
                DataValueField = "Id";

            base.DataSource = getDataSource();
            base.OnInit(e);
        }
       
        protected override void OnDataBinding(EventArgs e)
        {
            base.Items.Insert(0, new ListItem("", "0"));
            this.AppendDataBoundItems = true;
            base.OnDataBinding(e);
            this.AppendDataBoundItems = false;
        }

        private UserCollection getDataSource()
        {
            if (Page.Cache["Users"] == null)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                UserCollection sc = dataManager.GetUserCollection();
                sc = sc.SortByUsername(OrmLib.SortDirection.Ascending);
                Page.Cache["Users"] = sc;
                return sc;
            }
            return Page.Cache["Users"] as UserCollection;
        }
    }
}
