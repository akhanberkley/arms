#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for PolicySystemDropDownList
    /// </summary>
    public class PolicySystemDropDownList : System.Web.UI.WebControls.DropDownList
    {
        #region Properties

        public int CompanyNumberId
        {
            get
            {
                return ViewState["CompanyNumberId"] == null ? 0 : (int)ViewState["CompanyNumberId"];
            }
            set
            {
                ViewState["CompanyNumberId"] = value;
            }
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.Items.Clear();
            if (this.CompanyNumberId != 0)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                dataManager.QueryCriteria.Clear();
                dataManager.QueryCriteria.And(JoinPath.PolicySystem.CompanyNumberPolicySystem.Columns.CompanyNumberId, CompanyNumberId);
                PolicySystemCollection ac = dataManager.GetPolicySystemCollection();
                ListItemCollection items = new ListItemCollection();
                foreach (BCS.Biz.PolicySystem a in ac)
                {
                    items.Add(new ListItem(a.Abbreviation, a.Id.ToString()));
                }
                base.Items.AddRange(Common.SortListItems(items));
            }
            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        }
        #endregion

    }
}