#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for CountryDropDownList
    /// </summary>
    public class CountryDropDownList : System.Web.UI.WebControls.DropDownList
    {
        protected override void OnInit(EventArgs e)
        {
            if (DataTextField == null || DataTextField.Length == 0)
                DataTextField = "CountryDescription";
            if (DataValueField == null || DataValueField.Length == 0)
                DataValueField = "CountryCode";
            base.DataSource = getDataSource();

           
            base.OnInit(e);
        }

        private Biz.CountryCollection getDataSource()
        {
            if (Page.Cache["Countries"] == null)
            {
                Biz.DataManager dataManager = new BCS.Biz.DataManager(Components.DefaultValues.DSN);
                Biz.CountryCollection sc = dataManager.GetCountryCollection().SortByOrder(OrmLib.SortDirection.Ascending);
                Page.Cache["Countries"] = sc;
                return sc;
            }
            return Page.Cache["Countries"] as Biz.CountryCollection;
        }

        
    }
}