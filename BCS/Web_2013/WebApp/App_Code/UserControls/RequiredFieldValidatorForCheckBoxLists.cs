using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Web;
using System.Text;

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// http://www.4guysfromrolla.com/webtech/tips/t040302-1.shtml
    /// </summary>
    public class RequiredFieldValidatorForCheckBoxLists : CustomValidator
    {
        private ListControl _listctrl;
        protected override void OnInit(EventArgs e)
        {
            base.ValidateEmptyText = true;
            base.ClientValidationFunction = "EvaluateCheckBoxListIsValid";
            base.OnInit(e);
        }

        /// <summary>
        /// Gets false. This property is ignored
        /// </summary>
        [Browsable(false)]
        public new bool ValidateEmptyText
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the name of the Client validation function used, Control internally sets this property
        /// </summary>
        [Browsable(false)]
        public new string ClientValidationFunction
        {
            get
            { return base.ClientValidationFunction; }
        }

        protected override bool ControlPropertiesValid()
        {
            if (ControlToValidate.Length == 0)
            {
                throw new HttpException(
                    string.Format("The ControlToValidate property of '{0}' cannot be blank.", ID));
            }
            Control ctrl = NamingContainer.FindControl(ControlToValidate);

            if (ctrl != null)
            {
                _listctrl = (ListControl)ctrl;
                return (_listctrl != null);
            }
            else
            {
                throw new HttpException(
                    string.Format("Unable to find control id '{0}' referenced by the 'ControlToValidate' property of '{1}'.", ControlToValidate, ID));
            }
        }
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            base.AddAttributesToRender(writer);

            if (!Page.ClientScript.IsStartupScriptRegistered(typeof(string), "RFV4CBL"))
            {
                StringBuilder scriptBuilder = new StringBuilder();
                scriptBuilder.Append("function EvaluateCheckBoxListIsValid(source, args)");
                scriptBuilder.Append("{");
                scriptBuilder.Append("var chkListaTipoModificaciones= document.getElementById (source.controltovalidate);");
                scriptBuilder.Append("var chkLista= chkListaTipoModificaciones.getElementsByTagName(\"input\");");
                scriptBuilder.Append("for(var i=0;i<chkLista.length;i++)");
                scriptBuilder.Append("{");
                scriptBuilder.Append("if(chkLista[i].checked)");
                scriptBuilder.Append("{");
                scriptBuilder.Append("args.IsValid = true;");
                scriptBuilder.Append("return;");
                scriptBuilder.Append("}");
                scriptBuilder.Append("}");
                scriptBuilder.Append("args.IsValid = false;");
                scriptBuilder.Append("}");
                Page.ClientScript.RegisterStartupScript(typeof(string), "RFV4CBL", scriptBuilder.ToString(), true);
            }
        }
        protected override bool EvaluateIsValid()
        {
            bool valid = _listctrl.SelectedIndex != -1;
            if (!valid)
                if (SetFocusOnError)
                {
                    if (null != ScriptManager.GetCurrent(this.Page))
                    {
                        ScriptManager.GetCurrent(this.Page).SetFocus(_listctrl);
                    }
                    else
                    {
                        _listctrl.Focus();
                    }
                }
            return _listctrl.SelectedIndex != -1;
        }
    }
    /*
     
     // this can be used to enable client validation http://forums.asp.net/p/884658/2382698.aspx
     function ChequarSelecciondeTipoModificacion(source, args)
            {
               var chkListaTipoModificaciones= document.getElementById ('<%= boxid.ClientID %>');
               var chkLista= chkListaTipoModificaciones.getElementsByTagName("input");
               for(var i=0;i<chkLista.length;i++)
                {  
                    if(chkLista[i].checked)
                    {
                        args.IsValid = true;
                        return;
                    }
                }
              args.IsValid = false;
            } 
      */
}