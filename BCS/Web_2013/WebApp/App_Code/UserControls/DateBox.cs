using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for DateBox
    /// </summary>
    public class oldDateBox : TextBox
    {
        protected override void OnInit(EventArgs e)
        {
            // TODO: enhance to check if script/scriptmethods are registered
            // TODO: autocomplete type can be hidden 
            if (this.AutoPostBack)
            {
                this.Attributes.Add("onclick", string.Format("scwShow(this,this); processAfter=new Function(\"__doPostBack(\'{0}\',\'\'); \")", this.UniqueID));
            }
            else
            {
                this.Attributes.Add("onclick",
                    string.Format("scwShow(this,this); processAfter=new Function(\" if(document.createEvent) {{ var e = document.createEvent('HTMLEvents'); e.initEvent('change', false, false); document.getElementById('{0}').dispatchEvent(e); }} else {{ document.getElementById('{0}').fireEvent('onChange'); }} document.getElementById('{0}').focus(); \");", this.ClientID));
            }
            this.Attributes.Add("onkeydown", "hideCalOnTab(event);");
            this.Attributes.Add("onblur", "this.value = purgeDate(this);");
            this.Attributes.Add("autocomplete", "off");

            this.Columns = 10;
            base.OnInit(e);
        }
    }


    [DefaultProperty("Text"), ControlValueProperty("Text"),
       ToolboxData("<{0}:DateBox runat=server></{0}:DateBox>")]
    public class DateBox : WebControl, INamingContainer, IPostBackEventHandler, ITextControl
    {
        public delegate void TextChangedEventHandler(object sender, EventArgs e);
        public event TextChangedEventHandler TextChanged;

        protected virtual void OnTextChanged(EventArgs e)
        {
            if (TextChanged != null)
                TextChanged(this, e);
        }
        
        private TextBox TBOX;
        private Image IMG;

        /// <summary>
        /// 
        /// </summary>
        public string Text
        {
            get { EnsureChildControls(); return (null == TBOX) ? string.Empty : TBOX.Text; }
            set { EnsureChildControls(); if (null != TBOX) { TBOX.Text = value; } }
        }

        public TextBox TextBox
        {
            get
            {
                return TBOX;
            }
        }

        /// <summary>
        /// Gets or sets whether to display a separate calendar icon
        /// </summary>
        public bool Separate
        {
            get { return (bool?)ViewState["Separate"] ?? true; }
            set { ViewState["Separate"] = value; }
        }

        /// <summary>
        /// Gets or sets whether to display a separate calendar icon
        /// </summary>
        public bool AutoPostBack
        {
            get { return (bool?)ViewState["AutoPostBack"] ?? false; }
            set { ViewState["AutoPostBack"] = value; if (TextBox != null) TextBox.AutoPostBack = value; }
        }
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            // copying all attributes to the text box attributes to text box on prerender, so comment out the add attributes here
            //base.AddAttributesToRender(writer);
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID); // to aid in focus add id attribute to the parent span tag rendered
        }
        protected override void CreateChildControls()
        {
            TextBox textBox = new TextBox();
            textBox.ID = this.ID;
            textBox.Style.Add(HtmlTextWriterStyle.VerticalAlign, "middle");
            textBox.CssClass = "txt";
            textBox.Attributes.Add("autocomplete", "off");
            textBox.Columns = 10;
            textBox.AutoPostBack = AutoPostBack;
            if (AutoPostBack)
            {
                textBox.TextChanged += new EventHandler(textBox_TextChanged);
            }
            TBOX = textBox;
            Controls.Add(textBox);
            if (!Separate)
            {

            }
            else
            {

                Image image = new Image();
                image.ID = "img4" + textBox.ID;
                image.Style.Add(HtmlTextWriterStyle.VerticalAlign, "middle");
                image.Style.Add(HtmlTextWriterStyle.MarginLeft, "5px");
                image.ImageUrl = Common.GetImageUrl("cal_pic_sm.gif");
                IMG = image;

                Controls.Add(image);
            }

            base.CreateChildControls();
        }

        void textBox_TextChanged(object sender, EventArgs e)
        {
            OnTextChanged(e);
        }
        protected override void OnPreRender(EventArgs e)
        {
            TBOX.Attributes.Add("onkeydown", "hideCalOnTab(event);");
            TBOX.Attributes.Add("onchange", "this.value = purgeDate(this);");
            if (AutoPostBack)
            {
                WebControl wc = Separate ? (WebControl)IMG : (WebControl)TBOX;
                wc.Attributes.Add("onclick",
                                 string.Format("scwShow(document.getElementById('{0}'), this); processAfter=new Function(\" if(document.createEvent) {{ var e = document.createEvent('HTMLEvents'); e.initEvent('change', false, false); document.getElementById('{0}').dispatchEvent(e); }} else {{ document.getElementById('{0}').fireEvent('onChange'); }} document.getElementById('{0}').focus(); \");",
                                     TBOX.ClientID));
            }
            else
            {
                WebControl wc = Separate ? (WebControl)IMG : (WebControl)TBOX;
                wc.Attributes.Add("onclick",
                            string.Format("scwShow(document.getElementById('{0}'), this); processAfter=new Function(\" if(document.createEvent) {{ var e = document.createEvent('HTMLEvents'); e.initEvent('change', false, false); document.getElementById('{0}').dispatchEvent(e); }} else {{ document.getElementById('{0}').fireEvent('onChange'); }} document.getElementById('{0}').focus();  \");",
                                TBOX.ClientID));
            }
            base.CssClass = "";
            System.Web.UI.AttributeCollection attrs = Attributes;
            System.Collections.IEnumerator itor = attrs.Keys.GetEnumerator();
            while (itor.MoveNext())
            {
                object o = itor.Current;
                TBOX.Attributes[o.ToString()] = TBOX.Attributes[o.ToString()] + ";" + attrs[o.ToString()];
            }
            base.OnPreRender(e);
        }

        #region IPostBackEventHandler Members

        public void RaisePostBackEvent(string eventArgument)
        {
            OnTextChanged(new EventArgs());
        }

        #endregion

        #region ITextControl Members

        string ITextControl.Text
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }

        #endregion
    }
}
