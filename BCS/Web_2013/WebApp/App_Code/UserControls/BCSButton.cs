using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// purpose: to make enable/disable or visible/invisible based on user roles., instead of having to write same
    /// markup or code again and again. TODO: add meat
    /// </summary>
    public class BCSButton : Button
    {
        /// <summary>
        /// button's boolean property that gets affected
        /// </summary>
        public string Property
        {
            set
            { ViewState["Property"] = value; }
            get
            { return (string)ViewState["Property"] ?? "Enabled"; }
        }

        /// <summary>
        /// whether a user is not permitted to add or edit or both
        /// </summary>
        public BCSButtonContext BCSContext
        {
            set
            { ViewState["Property"] = value; }
            get
            { return (BCSButtonContext?)ViewState["Property"] ?? BCSButtonContext.AddEdit; }
        }        
    }
}
