#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
	/// <summary>
	/// Summary description for GeographicDirectionalDropDownList.
	/// </summary>
	public class GeographicDirectionalDropDownList : System.Web.UI.WebControls.DropDownList
	{
        #region DataBind
        public override void DataBind()
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            base.Items.Clear();

            GeographicDirectionalCollection gdc = dataManager.GetGeographicDirectionalCollection();
            foreach (GeographicDirectional gd in gdc)
            {
                base.Items.Add(new ListItem(gd.PropertyGeographicDirectional + " (" + gd.Abbreviation + ")", gd.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        } 
        #endregion
	}
}
