#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
	/// <summary>
	/// Summary description for CompanyNumberDropDownList.
	/// </summary>
	public class CompanyNumberDropDownList : System.Web.UI.WebControls.DropDownList
	{
        #region Properties
        public int CompanyId
        {
            get
            {
                return (int)ViewState["CompanyId"];
            }
            set
            {
                ViewState["CompanyId"] = value;
            }
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            if (this.CompanyId != 0)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                dataManager.QueryCriteria.Clear();
                dataManager.QueryCriteria.And(JoinPath.CompanyNumber.Columns.CompanyId, this.CompanyId, OrmLib.MatchType.Exact);

                base.Items.Clear();

                CompanyNumberCollection cnc = dataManager.GetCompanyNumberCollection();
                cnc = cnc.SortByPropertyCompanyNumber(OrmLib.SortDirection.Ascending);
                foreach (BCS.Biz.CompanyNumber cn in cnc)
                {
                    base.Items.Add(new ListItem(cn.PropertyCompanyNumber, cn.Id.ToString()));
                }

                // add blank at the beginning
                base.Items.Insert(0, new ListItem("", "0"));
            }
            if (base.Items.Count == 2)
            {
                base.Items[1].Selected = true;
            }
        } 
        #endregion
	}
}
