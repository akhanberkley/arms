﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCS.WebApp.Components;
using System.Collections;
using System.Web.UI.WebControls;
using BCS.Biz;
using System.ComponentModel;

namespace BCS.WebApp.UserControls
{

    public enum Specific
    {
        None = 0,
        Client,
        ClassCode,
        OtherCarrier
    }
    /// <summary>
    /// Summary description for ClientClassCodeCompanyNumberCodeDropDown
    /// </summary>
    public class CompanyNumberCodeDropDownList : System.Web.UI.WebControls.DropDownList
    {
        public CompanyNumberCodeDropDownList(Specific spec, int? companyCodeTypeId, int? classCodeId)
        {
            this.Specific = spec;
            this.ClassCodeId = classCodeId;
            this.CompanyNumberCodeTypeId = companyCodeTypeId;

            if (companyCodeTypeId != null)
                this.ID = string.Format("{0}ddl", companyCodeTypeId);
        }

        public int CompanyNumberId
        {
            get
            {
                return Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
            }
        }

        public Specific Specific
        {
            get
            {
                return ViewState["Specific"] == null ? Specific.None : (Specific)ViewState["Specific"];
            }
            set
            {
                ViewState["Specific"] = value;
            }
        }

        public int? ClassCodeId
        {
            get { return (int?)ViewState["ClassCodeId"]; }
            set { ViewState["ClassCodeId"] = value; }
        }

        public int? CompanyNumberCodeTypeId
        {
            get { return (int?)ViewState["CompanyNumberCodeTypeId"]; }
            set { ViewState["CompanyNumberCodeTypeId"] = value;}
        }

        public override void DataBind()
        {
            base.Items.Clear();

            if (CompanyNumberCodeTypeId != null)
            {
                CompanyNumberCodeCollection codes = GetCodes();
                if (codes != null && codes.Count > 0)
                {
                    foreach (CompanyNumberCode code in codes)
                    {
                        base.Items.Add(new ListItem(code.Code,code.Id.ToString()));
                    }
                }
            }

            base.Items.Insert(0, new ListItem(string.Empty));

            base.DataBind();
        }

        private CompanyNumberCodeCollection GetCodes()
        {
            DataManager dm = new DataManager(DefaultValues.DSN);

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(JoinPath.CompanyNumberCode.CompanyNumberCodeType.Columns.CompanyNumberId, CompanyNumberId);
            dm.QueryCriteria.And(JoinPath.CompanyNumberCode.CompanyNumberCodeType.Columns.Id, CompanyNumberCodeTypeId);

            if (Specific == Specific.Client)
                dm.QueryCriteria.And(JoinPath.CompanyNumberCode.CompanyNumberCodeType.Columns.ClientSpecific, true);

            else if (Specific == Specific.ClassCode)
            {
                dm.QueryCriteria.And(JoinPath.CompanyNumberCode.CompanyNumberCodeType.Columns.ClassCodeSpecific, true);                
            }
            else if (Specific == Specific.OtherCarrier)
            {
                dm.QueryCriteria.And(JoinPath.CompanyNumberCode.CompanyNumberCodeType.Columns.OtherCarrierSpecific, true);
            }

            return dm.GetCompanyNumberCodeCollection();
        }
    }
}