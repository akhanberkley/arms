#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    public class UnderwritingRoleLabel : Label
    {
        public UnderwritingRoleLabel()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        protected override void OnLoad(EventArgs e)
        {
            base.Text = GetText(_type);
            base.OnLoad(e);
        }

        private string _type;
        
        public string Type
        {
            get {return _type;}
            set {_type = value;}
        }

        public static string GetText(string roleType)
        {
            string type = "underwritingLabel|{0}";

            if (!string.IsNullOrEmpty(type))
            {
                switch (roleType)
                {
                    case "UW" :
                        type = "underwritinglabel|{0}";
                        break;
                    case "AN" :
                        type = "analystlabel|{0}";
                        break;  
                    case "TN" :
                        type = "technicianlabel|{0}";
                        break;
                }
            }

            string cacheKey = string.Format(type, Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            string text = (string)HttpContext.Current.Cache[cacheKey];

            if (string.IsNullOrEmpty(text))
            {
                BCS.Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(BCS.Biz.JoinPath.Company.Columns.Id, Common.GetSafeIntFromSession(SessionKeys.CompanyId));

                Company company = null;

                switch (roleType)
                {
                    case "UW":
                        company = dm.GetCompany(FetchPath.Company.UnderwritingRole_by_UnderwriterRoleId);
                        if (company.UnderwritingRole_by_UnderwriterRoleId != null && !string.IsNullOrEmpty(company.UnderwritingRole_by_UnderwriterRoleId.RoleName))
                        {
                            text = string.Format("{0}", company.UnderwritingRole_by_UnderwriterRoleId.RoleName);
                            HttpContext.Current.Cache[cacheKey] = text;
                        }
                        else 
                            text="Underwriter";
                        break;
                    case "AN":
                        company = dm.GetCompany(FetchPath.Company.UnderwritingRole_by_AnalystRoleId);
                        if (company.UnderwritingRole_by_AnalystRoleId != null && !string.IsNullOrEmpty(company.UnderwritingRole_by_AnalystRoleId.RoleName))
                        {
                            text = string.Format("{0}", company.UnderwritingRole_by_AnalystRoleId.RoleName);
                            HttpContext.Current.Cache[cacheKey] = text;
                        }
                        else 
                            text = "Analyst";
                        break;
                    case "TN":
                        {
                            company = dm.GetCompany(FetchPath.Company.UnderwritingRole_by_TechnicianRoleId);
                            if (company.UnderwritingRole_by_TechnicianRoleId != null && !string.IsNullOrEmpty(company.UnderwritingRole_by_TechnicianRoleId.RoleName))
                            {
                                text = string.Format("{0}", company.UnderwritingRole_by_TechnicianRoleId.RoleName);
                                HttpContext.Current.Cache[cacheKey] = text;
                            }
                            else
                                text = "Technician";
                            break;
                        }
                    default :
                        {
                            company = dm.GetCompany(FetchPath.Company.UnderwritingRole_by_AnalystRoleId);
                            if (company.UnderwritingRole_by_AnalystRoleId != null && !string.IsNullOrEmpty(company.UnderwritingRole_by_AnalystRoleId.RoleName))
                            {
                                text = string.Format("{0}", company.UnderwritingRole_by_AnalystRoleId.RoleName);
                                HttpContext.Current.Cache[cacheKey] = text;
                            }
                             break;
                        } 
                }

                
            }

            return text;
        }
    }
}