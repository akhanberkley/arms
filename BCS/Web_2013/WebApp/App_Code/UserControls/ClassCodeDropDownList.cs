#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
using BTS.CacheEngine.V2;
using System.Collections.Generic;
using System.Collections;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for ClassCodeDropDownList
    /// </summary>
    public class ClassCodeDropDownList : System.Web.UI.WebControls.DropDownList
    {
        public int CompanyNumberId
        {
            get
            {
                return ViewState["CompanyNumberId"] == null ? 0 : (int)ViewState["CompanyNumberId"];
            }
            set
            {
                ViewState["CompanyNumberId"] = value;
            }
        }
        public int[] ExcludedIds
        {
            get
            {
                if (ViewState["ExcludedIds"] == null)
                    return new int[0];
                return (int[])ViewState["ExcludedIds"];
            }
            set
            {
                ViewState["ExcludedIds"] = value;
            }
        }

        public void ExcludeId(int id)
        {
            List<int> exList = new List<int>();
            exList.AddRange(ExcludedIds);
            exList.Add(id);
            ExcludedIds = exList.ToArray();
        }

        public void IncludeId(int id)
        {
            List<int> exList = new List<int>();
            exList.AddRange(ExcludedIds);
            exList.Remove(id);
            ExcludedIds = exList.ToArray();
        }

        public override void DataBind()
        {
            base.Items.Clear();

            string dataTextField = string.IsNullOrEmpty(DataTextField) ? "CodeValue" : DataTextField;
            string dataSortField = dataTextField;
            if (CompanyNumberId != 0)
            {
                ClassCodeCollection ccc = GetClassCodes(CompanyNumberId);
                ccc = ccc.SortBy(dataSortField);

                ccc = FilterExcluded(ccc, ExcludedIds);
                foreach (ClassCode cc in ccc)
                {
                    base.Items.Add(new ListItem(Convert.ToString(cc[dataTextField]), cc.Id.ToString()));
                }
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
            base.DataBind();
        }

        private ClassCodeCollection FilterExcluded(ClassCodeCollection ccc, int[] excludedIds)
        {
            foreach (int excludedId in excludedIds)
            {
                ccc = ccc.FilterById(excludedId, OrmLib.CompareType.Not);
            }
            return ccc;
        }

        public static ClassCodeCollection GetClassCodes(int companyNumberId)
        {
            string cachekey = string.Concat("ClassCodes4", companyNumberId);
            CacheEngine ce = new CacheEngine(DefaultValues.DefaultObjectCacheTimeInMinutes);
            ClassCodeCollection ccc = new ClassCodeCollection();

            if (!ce.CachedItemExist(cachekey))
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                dataManager.QueryCriteria.And(JoinPath.ClassCode.Columns.CompanyNumberId, companyNumberId);
                ccc = dataManager.GetClassCodeCollection();
                ce.AddItemToCache(cachekey, ccc);
            }
            else
            {
                ccc = (ClassCodeCollection)ce.GetCachedItem(cachekey);
            }
            return ccc;
        }
    }
}