#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
	/// <summary>
	/// Summary description for AddressTypeDropDownList.
	/// </summary>
	public class AddressTypeDropDownList : System.Web.UI.WebControls.DropDownList
	{
        #region DataBind
        public override void DataBind()
        {
            Lookups dataManager = new Lookups(DefaultValues.DSN);

            base.Items.Clear();

            AddressTypeCollection atc = dataManager.AddressTypes;
            foreach (AddressType at in atc)
            {
                base.Items.Add(new ListItem(at.PropertyAddressType, at.Id.ToString()));
            }

            // add blank at the beginning
            //base.Items.Insert(0, new ListItem("","0"));
            base.Items.Insert(0, new ListItem("", "0"));
        } 
        #endregion
	}
}
