#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for ScaleDropDownList
    /// </summary>
    public class ScaleDropDownList : DropDownList
    {
        /// <summary>
        /// 
        /// </summary>        
        [System.ComponentModel.Description("Scale starts from")]
        public int Min
        {
            get
            {
                return ViewState["Min"] == null ? 0 : (int)ViewState["Min"];
            }
            set
            {
                ViewState["Min"] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [System.ComponentModel.Description("Scale ends at")]
        public int Max
        {
            get
            {
                return ViewState["Max"] == null ? 10 : (int)ViewState["Max"];
            }
            set
            {
                ViewState["Max"] = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            for (int i = Min; i < Max; i++)
            {
                base.Items.Add(i.ToString());
            }
            base.OnInit(e);
        }
    }
}