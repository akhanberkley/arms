#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for ClientNameTypeRadioButtonList
    /// </summary>
    public class ClientNameTypeRadioButtonList : RadioButtonList
    {
        #region DataBind
        public override void DataBind()
        {
            Lookups lookups = new Lookups(DefaultValues.DSN);

            base.Items.Clear();

            ClientNameTypeCollection cntc = lookups.ClientNameTypes;
            foreach (ClientNameType cnt in cntc)
            {
                base.Items.Add(new ListItem(cnt.NameType, cnt.Id.ToString()));
            }
        } 
        #endregion
    }
}