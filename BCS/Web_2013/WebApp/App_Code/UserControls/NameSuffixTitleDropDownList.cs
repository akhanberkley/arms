#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
	/// <summary>
	/// Summary description for NameSuffixTitleDropDownList.
	/// </summary>
	public class NameSuffixTitleDropDownList : System.Web.UI.WebControls.DropDownList
	{
        #region DataBind
        public override void DataBind()
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            base.Items.Clear();

            NameSuffixTitleCollection nstc = dataManager.GetNameSuffixTitleCollection();
            foreach (NameSuffixTitle nst in nstc)
            {
                base.Items.Add(new ListItem(nst.SuffixTitle, nst.SuffixTitle));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        } 
        #endregion
	}
}
