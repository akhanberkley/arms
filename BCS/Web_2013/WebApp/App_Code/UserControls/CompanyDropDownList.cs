#region Using
using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
	/// <summary>
	/// Summary description for CompanyDropDownList.
	/// </summary>
	public class CompanyDropDownList : System.Web.UI.WebControls.DropDownList
	{
        #region DataBind
        public override void DataBind()
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            base.Items.Clear();

            CompanyCollection cc = dataManager.GetCompanyCollection();
            cc = cc.SortByCompanyName(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.Company c in cc)
            {
                base.Items.Add(new ListItem(c.CompanyName, c.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        }

        public void DataBind(string userName)
        {
            DataBind();

            UserCompanyCollection assignedCompanies = Security.GetUserCompanies(userName);

            if (assignedCompanies != null)
            {
                StringDictionary itemsToRemove = new StringDictionary();

                foreach (ListItem item in base.Items)
                {
                    if (item.Value != string.Empty)
                    {
                        // If we don't find in the assignedCompanies
                        if (assignedCompanies.FindByCompanyId(Convert.ToInt32(item.Value), OrmLib.CompareType.Exact) == null)
                        {
                            itemsToRemove.Add(item.Value, item.Text);
                        }
                    }
                }

                foreach (string s in itemsToRemove.Keys)
                {
                    if (itemsToRemove[s].Length == 0)
                        continue;
                    base.Items.Remove(new ListItem(itemsToRemove[s], s));
                }
            }
        } 
        #endregion	
	}
}
