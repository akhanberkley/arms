﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCS.DAL;


namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for CompanyNumberCodeTypeDropDown
    /// </summary>
    public class CompanyUsersDropDown : System.Web.UI.WebControls.DropDownList
    {
        protected override void OnLoad(EventArgs e)
        {
            if (!Page.IsPostBack)
                DataBind();
            base.OnLoad(e);
        }

        public override void DataBind()
        {
            base.DataSource = BCS.Core.Clearance.SubmissionsEF.GetCompanyUsersForDropDown(Common.GetSafeIntFromSession(BCS.WebApp.Components.SessionKeys.CompanyId));
            base.DataValueField = "Value";
            base.DataTextField = "Text";
            base.DataBind();

            base.Items.Insert(0, "");
        }

    }
}