using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCS.WebApp.UserControls
{
	/// <summary>
	/// Summary description for DateWebControl.
	/// </summary>
	[DefaultProperty("Text"),
		ToolboxData("<{0}:DateWebControl runat=server></{0}:DateWebControl>")]
	public class DateWebControl : WebControl, INamingContainer
	{
		private bool required;

		#region Controls
		ImageButton imageButton = new ImageButton();
		TextBox textDate = new TextBox();
		LiteralControl spacer = new LiteralControl("&nbsp;");
		#endregion

		#region Available Properties
		[Bindable(true),Category("Behavior"),DefaultValue("false")]
		public bool Required
		{
			get
			{
				return this.required;
			}
			set
			{
				this.required = value;
				base.EnsureChildControls();
			}
		}

		[Bindable(true),Category("Appearance"),DefaultValue("")]
		public string ImageButtonUrl
		{
			get
			{
				return this.imageButton.ImageUrl;
			}
			set
			{
				this.imageButton.ImageUrl = value;
				base.EnsureChildControls();
			}
		}

		[Bindable(true),Category("Appearance"),DefaultValue("")]
		public override string CssClass
		{
			get
			{
				return this.textDate.CssClass;
			}
			set
			{
				this.textDate.CssClass = value;
				base.EnsureChildControls();
			}
		}

		[Bindable(true),Category("Layout"),DefaultValue("")]
		public Unit TextBoxWidth
		{
			get
			{
				return this.textDate.Width;
			}
			set
			{
				this.textDate.Width = value;
				base.EnsureChildControls();
			}
		}

		[Bindable(true),Category("Appearance"),DefaultValue("")]
		public string Text
		{
			get
			{
				return this.textDate.Text;
			}
			set
			{
				this.textDate.Text = value;
				base.EnsureChildControls();
			}
		}
		#endregion

		#region Protected Methods Used To Output
		/// <summary>
		/// Render this control to the output parameter specified.
		/// </summary>
		/// <param name="output">The HTML writer to write out to </param>
		protected override void Render(HtmlTextWriter output)
		{
			//output.Write(Text);
			base.EnsureChildControls();
			base.Render(output);
		}

		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			this.imageButton.CausesValidation = false;
			
			this.textDate.ID = "textDate";
			this.Controls.Add(this.textDate);
			this.Controls.Add(this.spacer);
			this.Controls.Add(this.imageButton);			
		}

		protected override void OnPreRender(EventArgs e)
		{
			this.imageButton.Attributes.Add("onclick", 
				"calendar_window=window.open('" +
				Page.ResolveUrl("~/Calendar.aspx") +
				"?control=" +
				this.textDate.UniqueID.Replace(":", "_") +
				"','calendar_window','width=200,height=180,resizable=no,scrollbars=no');calendar_window.focus();");

			if (this.Required)
			{
				RequiredFieldValidator requiredFieldValidator1 = new RequiredFieldValidator();
				requiredFieldValidator1.Text = "* Required";
				requiredFieldValidator1.ControlToValidate = this.textDate.ID;
				requiredFieldValidator1.CssClass = "smalltext";
				this.Controls.Add(this.spacer);
				this.Controls.Add(requiredFieldValidator1);
			}

			base.OnPreRender (e);
		}

		// This is a common pattern for implementing a composite control.
		// These two methods, combined with the INamingContainer interface
		// create a composite control
		/// <summary>
		/// This override makes sure that if we or somebody else tries to access our child controls,
		/// that they will have been created in time.
		/// </summary>
		public override ControlCollection Controls 
		{
			get 
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		#endregion
	}
}
