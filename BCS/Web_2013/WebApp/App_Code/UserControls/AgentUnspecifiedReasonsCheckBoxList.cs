using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.Biz;
using BCS.WebApp.Components;

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for AgentUnspecifiedReasonsCheckBoxList
    /// </summary>
    public class AgentUnspecifiedReasonsCheckBoxList : CheckBoxList
    {
        [System.ComponentModel.Category("BCS")]
        public int CompanyNumberId
        {
            get
            {
                return ViewState["CompanyNumberId"] == null ? 0 : (int)ViewState["CompanyNumberId"];
            }
            set
            {
                ViewState["CompanyNumberId"] = value;
            }
        }
        public override void DataBind()
        {
            base.Items.Clear();
            DataManager dm = new DataManager(DefaultValues.DSN);
            dm.QueryCriteria.And(JoinPath.CompanyNumberAgentUnspecifiedReason.Columns.CompanyNumberId, CompanyNumberId);
            CompanyNumberAgentUnspecifiedReasonCollection reasons =
                dm.GetCompanyNumberAgentUnspecifiedReasonCollection(FetchPath.CompanyNumberAgentUnspecifiedReason.AgentUnspecifiedReason);
            foreach (CompanyNumberAgentUnspecifiedReason reason in reasons)
            {
                base.Items.Add(new ListItem(reason.AgentUnspecifiedReason.Reason, reason.AgentUnspecifiedReasonId.ToString()));
            }
        }
    } 
}
