#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
	/// <summary>
	/// Summary description for SecondaryUnitDesignatorDropDownList.
	/// </summary>
	public class SecondaryUnitDesignatorDropDownList : System.Web.UI.WebControls.DropDownList
	{
        #region DataBind
        public override void DataBind()
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            base.Items.Clear();

            SecondaryUnitDesignatorCollection sudc = dataManager.GetSecondaryUnitDesignatorCollection();
            foreach (SecondaryUnitDesignator sud in sudc)
            {
                base.Items.Add(new ListItem(sud.PropertySecondaryUnitDesignator, sud.PropertySecondaryUnitDesignator));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        } 
        #endregion
	}
}
