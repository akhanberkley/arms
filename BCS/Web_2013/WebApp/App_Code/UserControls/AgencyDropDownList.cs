#region Using
using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
using System.Collections.Generic;
using System.ComponentModel;
#endregion

namespace BCS.WebApp.UserControls
{
    public enum AgencyTypes
    {
        MastersOnly,
        NonMastersOnly,
        Both
    }

    /// <summary>
    /// Summary description for AgencyDropDownList
    /// </summary>
    public class AgencyDropDownList : BaseDropDown
    {
        #region Properties

        /// <summary>
        /// {0} - agency name, {1} - agency number
        /// </summary>        
        [Description("ex: number - name")]
        [Category("BCS")]
        public string DisplayFormat
        {
            get
            {
                return ViewState["DisplayFormat"] == null ? "{0} ({1})" : (string)ViewState["DisplayFormat"];
            }
            set
            {
                ViewState["DisplayFormat"] = value;
            }
        }

        [Category("BCS")]
        public int CompanyNumberId
        {
            get
            {
                return ViewState["CompanyNumberId"] == null ? 0 : (int)ViewState["CompanyNumberId"];
            }
            set
            {
                ViewState["CompanyNumberId"] = value;
            }
        }

        [Category("BCS")]
        public DateTime CancelDate
        {
            get
            {
                return ViewState["CancelDate"] == null ? DateTime.Now : (DateTime)ViewState["CancelDate"];
            }
            set
            {
                ViewState["CancelDate"] = value;
            }
        }
        [Category("BCS")]
        public string SortBy
        {
            set
            { ViewState["SortBy"] = value; }
            get
            { return (string)ViewState["SortBy"] ?? string.Empty; }
        }

        [Description(
            "whether to add empty item at the beginning with empty value or '0' value. True adds empty value." +
            " Needed when used within data controls.")]
        [Category("BCS")]
        public bool ItemAsEmpty
        {
            set
            { ViewState["ItemAsEmpty"] = value; }
            get
            { return (bool?)ViewState["ItemAsEmpty"] ?? false; }
        }

        [Description("whether to load inactive agencies")]
        [Category("BCS")]
        public bool ActiveOnly
        {
            set
            { ViewState["ActiveOnly"] = value; }
            get
            { return (bool?)ViewState["ActiveOnly"] ?? false; }
        }

        [Description("whether to identify inactive agencies")]
        [Category("BCS")]
        public bool IdentifyInactive
        {
            set
            { ViewState["IdentifyInactive"] = value; }
            get
            { return (bool?)ViewState["IdentifyInactive"] ?? false; }
        }
        [Description("whether to exclude master agencies")]
        [Category("BCS")]
        public AgencyTypes AgencyType
        {
            set
            { ViewState["AgencyType"] = value; }
            get
            { return (AgencyTypes?)ViewState["AgencyType"] ?? AgencyTypes.NonMastersOnly; }
        }
        #endregion

        protected override void OnInit(EventArgs e)
        {
            DataTextField = "Text";
            DataValueField = "Value";
            base.OnInit(e);
        }
        [Description("Extra to be added to the list in case not present by bindings provided")]
        [Category("BCS")]
        public int Extra
        {
            set { ViewState["Extra"] = value; }
            get { return (int?)ViewState["Extra"] ?? 0; }
        }
        #region DataBind
        public override void DataBind()
        {
            //base.DataSource = this.GetSource();
            //base.DataBind();

            base.Items.Clear();
            ListItemCollection ItemsC = this.GetSource();
            ListItem[] items = new ListItem[ItemsC.Count];
            ItemsC.CopyTo(items, 0);
            base.Items.AddRange(items);

            base.DataBind();
        }

        public void DataBindByUnderwriter(int underwriterid)
        {
            base.Items.Clear();
            
            DataManager dm = new DataManager(DefaultValues.DSN);
            dm.QueryCriteria.And(JoinPath.Agency.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.PersonId, underwriterid);
            dm.QueryCriteria.And(JoinPath.Agency.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.UnderwritingRoleId, GetRoleId(Role.Underwriter));
            AgencyCollection agencies = dm.GetAgencyCollection();
            agencies = agencies.SortByAgencyName(OrmLib.SortDirection.Ascending);

            List<ListItem> lic = new  List<ListItem>();
            lic.Add(ItemAsEmpty ? new ListItem(string.Empty, string.Empty) : new ListItem("", "0"));

            foreach (Agency a in agencies)
            {
                if (CountParams(DisplayFormat) == 2)
                    lic.Add(new ListItem(string.Format(DisplayFormat, a.AgencyName, a.AgencyNumber), a.Id.ToString()));

                if (CountParams(DisplayFormat) == 4)
                    lic.Add(new ListItem(string.Format(DisplayFormat, a.AgencyName, a.AgencyNumber, a.City, a.State),
                    a.Id.ToString()));
                if (CountParams(DisplayFormat) == 5)
                    lic.Add(new ListItem(string.Format(DisplayFormat, a.AgencyName, a.AgencyNumber, a.City, a.State, a.Zip),
                    a.Id.ToString()));
            }
            base.Items.AddRange(lic.ToArray());
            base.DataBind();
        }
        #endregion


        private int GetRoleId(Role role)
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.Company company = userCompanies.FindByCompanyId(Common.GetSafeIntFromSession(SessionKeys.CompanyId)).Company;

            switch (role)
            {
                case Role.Analyst:
                    {
                        return company.AnalystRoleId.IsNull ? 0 : company.AnalystRoleId.Value;
                    }
                case Role.Underwriter:
                    {
                        return company.UnderwriterRoleId.IsNull ? 0 : company.UnderwriterRoleId.Value;
                    }
                case Role.None:
                    {
                        return 0;
                    }

                default:
                    return 0;
            }
        }

        public DataSet GetCachedSource()
        {
            if(CompanyNumberId == 0)
                return null;
            BTS.CacheEngine.V2.CacheEngine cacheEngine = new BTS.CacheEngine.V2.CacheEngine(DefaultValues.DefaultObjectCacheTimeInMinutes);
            string CNCacheKey = string.Format(CacheKeys.CompanyNumberAgencies, CompanyNumberId);
            if (!cacheEngine.CachedItemExist(CNCacheKey))
            {
                DataManager dm = new DataManager(DefaultValues.DSN);
                dm.QueryCriteria.And(JoinPath.Agency.Columns.CompanyNumberId, CompanyNumberId);
                AgencyCollection agencies = dm.GetAgencyCollection(BCS.Biz.FetchPath.Agency.AgencyLicensedState);
                cacheEngine.AddItemToCache(CNCacheKey, dm.DataSet);
                return dm.DataSet;
            }
            return (DataSet)cacheEngine.GetCachedItem(CNCacheKey);
        }

        private ListItemCollection GetSource()
        {

            DataManager dm = new DataManager(DefaultValues.DSN);
            DataSet cachedDataSet = GetCachedSource();
            if (ActiveOnly)
            {
                cachedDataSet = FilterActiveAgencies(cachedDataSet);
            }

            BCS.Core.Security.CustomPrincipal user = (Page.User as BCS.Core.Security.CustomPrincipal);

            #region user role is "Remote Agency"
            if (user.HasAgency())
            {
                // since this is non-aps the agency ids would be int and not long
                int masterAgencyId = Convert.ToInt32(Security.GetMasterAgency(user.Agency));

                cachedDataSet = FilterMasterUmbrella(cachedDataSet, masterAgencyId);

                dm.DataSet = cachedDataSet;
                AgencyCollection ac = dm.GetAgencyCollectionFromDataSet();

                if (!string.IsNullOrEmpty(SortBy))
                {
                    ac = ac.SortBy(SortBy);
                }
                ListItemCollection lic = new ListItemCollection();
                lic.Add(ItemAsEmpty ? new ListItem(string.Empty, string.Empty) : new ListItem("", "0"));


                // add agencies under the master umbrella
                foreach (Agency a in ac)
                {
                    if (CountParams(DisplayFormat) == 2)
                        lic.Add(new ListItem(string.Format(DisplayFormat, a.AgencyName, a.AgencyNumber), a.Id.ToString()));

                    if (CountParams(DisplayFormat) == 4)
                        lic.Add(new ListItem(string.Format(DisplayFormat, a.AgencyName, a.AgencyNumber, a.City, a.State),
                        a.Id.ToString()));
                    if (CountParams(DisplayFormat) == 5)
                        lic.Add(new ListItem(string.Format(DisplayFormat, a.AgencyName, a.AgencyNumber, a.City, a.State, a.Zip),
                        a.Id.ToString()));
                }
                if (string.IsNullOrEmpty(SortBy))
                {
                    ListItemCollection slic = new ListItemCollection();
                    slic.AddRange(Common.SortListItems(lic));

                    lic = slic;
                }
                return lic;
            }
            #endregion

            #region user role is not "Remote Agency"
            else
            {
                string inactiveIdentificationText = " (inactive)";
                //AgencyCollection aac = dataManager.GetAgencyCollection();

                cachedDataSet = FilterMaster(cachedDataSet);                

                dm.DataSet = cachedDataSet;
                AgencyCollection aac = dm.GetAgencyCollectionFromDataSet();
                if (!string.IsNullOrEmpty(SortBy))
                {
                    aac.SortBy(SortBy);
                }
                ListItemCollection llic = new ListItemCollection();
                llic.Add(ItemAsEmpty ? new ListItem(string.Empty, string.Empty) : new ListItem("", "0"));
                foreach (Agency a in aac)
                {
                    if (CountParams(DisplayFormat) == 2)
                    {
                        if (IdentifyInactive)
                        {
                            if (!IsActive(a))
                            {
                                //llic.Add(new ListItem(string.Format(DisplayFormat + "{2}", a.AgencyName, a.AgencyNumber, inactiveIdentificationText), a.Id.ToString()));
                                ListItem inactiveLI = new ListItem(string.Format(DisplayFormat + "{2}", a.AgencyName, a.AgencyNumber, inactiveIdentificationText), a.Id.ToString());
                                inactiveLI.Attributes.Add("style", "background-color:yellow");
                                //inactiveLI.Attributes.Add("class", "disabled");
                                llic.Add(inactiveLI);
                                continue;
                            }
                        }
                        llic.Add(new ListItem(string.Format(DisplayFormat, a.AgencyName, a.AgencyNumber), a.Id.ToString()));
                    }

                    if (CountParams(DisplayFormat) == 4)
                    {
                        if (IdentifyInactive)
                        {
                            if (!IsActive(a))
                            {
                                //llic.Add(new ListItem(string.Format(DisplayFormat + "{4}", a.AgencyName, a.AgencyNumber, a.City, a.State, inactiveIdentificationText), a.Id.ToString()));                                
                                ListItem inactiveLI = new ListItem(string.Format(DisplayFormat + "{4}", a.AgencyName, a.AgencyNumber, a.City, a.State, inactiveIdentificationText), a.Id.ToString());
                                inactiveLI.Attributes.Add("style", "background-color:yellow");
                                //inactiveLI.Attributes.Add("class", "disabled");
                                llic.Add(inactiveLI);
                                continue;
                            }
                        }
                        llic.Add(new ListItem(string.Format(DisplayFormat, a.AgencyName, a.AgencyNumber, a.City, a.State),
                       a.Id.ToString()));
                    }
                    if (CountParams(DisplayFormat) == 5)
                    {
                        if (IdentifyInactive)
                        {
                            if (!IsActive(a))
                            {
                                //llic.Add(new ListItem(string.Format(DisplayFormat + "{4}", a.AgencyName, a.AgencyNumber, a.City, a.State, inactiveIdentificationText), a.Id.ToString()));                                
                                ListItem inactiveLI = new ListItem(string.Format(DisplayFormat + "{5}", a.AgencyName, a.AgencyNumber, a.City, a.State, a.Zip, inactiveIdentificationText), a.Id.ToString());
                                inactiveLI.Attributes.Add("style", "background-color:yellow");
                                //inactiveLI.Attributes.Add("class", "disabled");
                                llic.Add(inactiveLI);
                                continue;
                            }
                        }
                        llic.Add(new ListItem(string.Format(DisplayFormat, a.AgencyName, a.AgencyNumber, a.City, a.State, a.Zip),
                       a.Id.ToString()));
                    }
                }
                if (string.IsNullOrEmpty(SortBy))
                {
                    ListItemCollection slic = new ListItemCollection();
                    slic.AddRange(Common.SortListItems(llic));

                    llic = slic;
                }
                return llic;
            }
            #endregion
        }

        private DataSet FilterMasterUmbrella(DataSet cachedDataSet, int masterAgencyId)
        {
            string tablename = "Agency";
            string expr = string.Format(" (MasterAgencyId = {0} OR Id = {0}) AND (IsMaster = 0)", masterAgencyId);
            DataRow[] selectedRows = cachedDataSet.Tables[tablename].Select(expr);
            DataSet filterDataSet = cachedDataSet.Clone();
            foreach (DataRow drow in selectedRows)
                filterDataSet.Tables[tablename].Rows.Add(drow.ItemArray);
            filterDataSet.AcceptChanges();
            return filterDataSet;
        }

        private DataSet FilterMaster(DataSet cachedDataSet)
        {
            string tablename = "Agency";
            string expr = string.Empty;
            switch (AgencyType)
            {
                case AgencyTypes.MastersOnly:
                    expr = " (IsMaster = 1) ";
                    break;
                case AgencyTypes.NonMastersOnly:
                    expr = " (IsMaster = 0) ";
                    break;
                case AgencyTypes.Both:
                    break;
                default:
                    break;
            }
            //string expr = string.Format(" (IsMaster = 0) ");
            DataRow[] selectedRows = cachedDataSet.Tables[tablename].Select(expr);
            DataSet filterDataSet = cachedDataSet.Clone();
            foreach (DataRow drow in selectedRows)
                filterDataSet.Tables[tablename].Rows.Add(drow.ItemArray);
            filterDataSet.AcceptChanges();
            return filterDataSet;
        }

        private DataSet FilterActiveAgencies(DataSet cachedDataSet)
        {
            string tablename = "Agency";
            // select active only
            string expr = string.Format("(CancelDate IS NULL OR CancelDate >= '{0}') AND (EffectiveDate IS NULL OR EffectiveDate <= '{0}')",
                CancelDate.ToShortDateString());
            if (Extra != 0)
            {
                // select an extra too
                expr = string.Format("({0}) OR Id = {1}", expr, Extra);
            }
            DataRow[] selectedRows = cachedDataSet.Tables[tablename].Select(expr);
            DataSet filterDataSet = cachedDataSet.Clone();
            foreach (DataRow drow in selectedRows)
                filterDataSet.Tables[tablename].Rows.Add(drow.ItemArray);
            filterDataSet.AcceptChanges();
            return filterDataSet;
        }

        private bool IsActive(Agency a)
        {
            bool b1 = false, b2 = false;

            if (!a.EffectiveDate.IsNull)
            {
                if (a.EffectiveDate.Value <= CancelDate)
                    b1 = true;
            }
            else
                b1 = true;

            if (!a.CancelDate.IsNull)
            {
                if (a.CancelDate.Value >= CancelDate)
                    b2 = true;
            }
            else
                b2 = true;

            return b1 && b2;
        }
        private int CountParams(string p)
        {
            // TODO: 
            System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(@"{\d*}", System.Text.RegularExpressions.RegexOptions.None);
            System.Text.RegularExpressions.MatchCollection matches = r.Matches(p);
            return matches.Count;
        }
    }
}