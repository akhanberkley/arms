﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCS.DAL;


namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for CompanyNumberCodeTypeDropDown
    /// </summary>
    public class CompanyNumberCodeTypeDropDown : System.Web.UI.WebControls.DropDownList
    {
        public CompanyNumberCodeTypeDropDown()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public CompanyNumberCodeTypeDropDown(CompanyNumberCodeType type)
        {
            CodeType = type;
            this.ID = string.Format("ddl-{0}", type.Id);
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!Page.IsPostBack)
                DataBind();
            base.OnLoad(e);
        }

        public override void DataBind()
        {
            if (CodeType == null)
                return;

            base.DataSource = CodeType.CompanyNumberCode.OrderBy(c=>c.Code).ToList();
            base.DataTextField = "Code";
            base.DataValueField = "Id";
            base.DataBind();

            base.Items.Insert(0, "");
        }

        public void DataBind(string expirationDate)
        {
            if (CodeType == null)
                return;

            string[] dateArray = expirationDate.Split('|');
            base.Items.Clear();

            base.DataSource = CodeType.CompanyNumberCode.Where(c => c.ExpirationDate >= (DateTime)Convert.ToDateTime(dateArray[0]) && c.ExpirationDate <= (DateTime)Convert.ToDateTime(dateArray[1])).OrderBy(c => c.Code).ToList();
            base.DataTextField = "Code";
            base.DataValueField = "Id";
            base.DataBind();

            base.Items.Insert(0, "");
        }

        public CompanyNumberCodeType CodeType
        {
            get;
            set;
        }
    }
}