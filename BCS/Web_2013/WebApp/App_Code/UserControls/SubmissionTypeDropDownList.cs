#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
using System.ComponentModel;
#endregion

namespace BCS.WebApp.UserControls
{
	/// <summary>
	/// Summary description for SubmissionTypeDropDownList.
	/// </summary>
	public class SubmissionTypeDropDownList : System.Web.UI.WebControls.DropDownList
	{
        public int CompanyNumberId
        {
            get
            {
                if (ViewState["CompanyNumberId"] == null) return 0;
                return (int)ViewState["CompanyNumberId"];
            }
            set { ViewState["CompanyNumberId"] = value; }
        }

        [Description("Gets or sets a value indicating the selection be defaulted or not.")]
        [Category("Default")]
        [DefaultValue(true)]
        public bool Default
        {
            set { ViewState["Default"] = value; }
            get { return (bool?)ViewState["Default"] ?? true; }
        }
        #region DataBind
        public override void DataBind()
        {
            base.Items.Clear();
            CompanyNumberSubmissionTypeCollection cntypes = null;
            string cacheKey = string.Format(CacheKeys.CompanyNumberTypes, CompanyNumberId);

            if (this.Page.Cache[cacheKey] == null)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                dataManager.QueryCriteria.And(Biz.JoinPath.CompanyNumberSubmissionType.Columns.CompanyNumberId, CompanyNumberId);
                cntypes = dataManager.GetCompanyNumberSubmissionTypeCollection(Biz.FetchPath.CompanyNumberSubmissionType.SubmissionType).SortByOrder(OrmLib.SortDirection.Ascending);

                this.Page.Cache[cacheKey] = cntypes;
            }
            else
            {
                cntypes = (CompanyNumberSubmissionTypeCollection)this.Page.Cache[cacheKey];
            }
            foreach (CompanyNumberSubmissionType st in cntypes)
            {
                ListItem li = new ListItem(st.SubmissionType.TypeName, st.SubmissionType.Id.ToString());
                base.Items.Add(li);
                if (Default)
                {
                    if (st.DefaultSelection)
                    {
                        base.ClearSelection();
                        base.Items.FindByValue(li.Value).Selected = true;
                    }
                }
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        } 
        #endregion	
	}
}
