#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion
 
namespace BCS.WebApp.UserControls
{
	/// <summary>
	/// Summary description for StreetSuffixDropDownList.
	/// </summary>
	public class StreetSuffixDropDownList : System.Web.UI.WebControls.DropDownList
	{
        #region DataBind
        public override void DataBind()
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            base.Items.Clear();

            StreetSuffixCollection ssc = dataManager.GetStreetSuffixCollection();
            foreach (StreetSuffix ss in ssc)
            {
                base.Items.Add(new ListItem(ss.CommonAbbreviation, ss.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        } 
        #endregion
	}
}
