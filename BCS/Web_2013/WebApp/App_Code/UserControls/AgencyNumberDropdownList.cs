#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for AgencyNumberDropdownList
    /// </summary>
    public class AgencyNumberDropdownList : DropDownList
    {
        public int CompanyNumberId
        {
            get
            { return (int?)ViewState["CompanyNumberId"] ?? 0; }
            set
            { ViewState["CompanyNumberId"] = value; }
        }
        
        public bool AddEmptyElement        
        {

            get
            { return (bool?)ViewState["AddEmptyElement"] ?? true; }
            set
            {
                ViewState["AddEmptyElement"] = value;
            }
        }

        public DateTime CancelDate
        {
            get
            {
                // making default as 1/1/2005 as all agencies eff date were set as that
                return ViewState["CancelDate"] == null ? new DateTime(2005, 1, 2) : (DateTime)ViewState["CancelDate"];
            }
            set
            {
                ViewState["CancelDate"] = value;
            }
        }

        // purpose: when this control is used for referral agency number purpose, the agency being referred can be excluded from the dropdown
        public string ExcludedAgencyNumber
        {
            get
            { return (string)ViewState["ExcludedAgencyNumber"]; }
            set
            { ViewState["ExcludedAgencyNumber"] = value; }
        }

        protected override void OnDataBinding(EventArgs e)
        {
            base.DataSource = getDataSource();
            if ( AddEmptyElement)
                base.Items.Insert(0, new ListItem("", ""));
            this.AppendDataBoundItems = true;
            base.OnDataBinding(e);
            this.AppendDataBoundItems = false;
        }
        protected override void OnDataBound(EventArgs e)
        {
            base.OnDataBound(e);
        }

        private ListItem[] getDataSource()
        {
            if (CompanyNumberId != 0)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                dataManager.QueryCriteria.And(JoinPath.Agency.Columns.CompanyNumberId, CompanyNumberId);
                dataManager = Helper.AddAgencyCancelCriteria(dataManager,CancelDate);
                AgencyCollection ac = dataManager.GetAgencyCollection();
                ListItemCollection lic = new ListItemCollection();

                foreach (Agency a in ac)
                {

                    if (!string.IsNullOrEmpty(ExcludedAgencyNumber))
                    {
                        if (a.AgencyNumber == ExcludedAgencyNumber)
                            continue;
                    }                    
                    lic.Add(new ListItem(a.AgencyNumber, string.IsNullOrEmpty(DataValueField) ? a.AgencyNumber : DataValueField));
                }

                return Common.SortListItems(lic);
            }
            return new ListItem[] { };
        }        
    }
}
