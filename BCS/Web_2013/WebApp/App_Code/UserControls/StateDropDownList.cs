#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for StateDropDownList.
    /// </summary>
    public class StateDropDownList : System.Web.UI.WebControls.DropDownList
    {

        protected override void OnInit(EventArgs e)
        {
            if (DataTextField == null || DataTextField.Length == 0)
                DataTextField = "Abbreviation";
            if (DataValueField == null || DataValueField.Length == 0)
                DataValueField = "Id";

            base.DataSource = getDataSource();
            base.OnInit(e);
        }

        protected override void OnDataBound(EventArgs e)
        {
            base.OnDataBound(e);
        }
        protected override void OnDataBinding(EventArgs e)
        {
            base.Items.Insert(0, new ListItem("", ""));
            this.AppendDataBoundItems = true;
            base.OnDataBinding(e);
            this.AppendDataBoundItems = false;
        }

        private StateCollection getDataSource()
        {
            if (Page.Cache["States"] == null)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                StateCollection sc = dataManager.GetStateCollection();
                sc = sc.SortByAbbreviation(OrmLib.SortDirection.Ascending);
                Page.Cache["States"] = sc;
                return sc;
            }
            return Page.Cache["States"] as StateCollection;
        }
    }
}
