#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
/// <summary>
/// Summary description for ClientNameTypeDropDownList
/// </summary>
    public class ClientNameTypeDropDownList : DropDownList
    {

        protected override void OnInit(EventArgs e)
        {
            //this.DataBind();
            //base.OnInit(e);
        }


        #region DataBind
        public override void DataBind()
        {
            if (base.Items.Count > 0) return;
            Lookups lookups = new Lookups(DefaultValues.DSN);

            base.Items.Clear();

            ClientNameTypeCollection cntc = lookups.ClientNameTypes;
            foreach (ClientNameType cnt in cntc)
            {
                base.Items.Add(new ListItem(cnt.NameType, cnt.NameType));
            }
            base.Items.Insert(0, new ListItem("NA", ""));
        }
        #endregion
    }
}
