#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for AgencyCityStateDropDownList
    /// </summary>
    [Obsolete("use AgencyDropDownList, setting the format appropriately")]
    public class AgencyCityStateDropDownList : AgencyDropDownList
    {
        protected override void OnInit(EventArgs e)
        {
            DisplayFormat = "{0} - {1} | {2}, {3}";
            base.OnInit(e);
        }
    }
}