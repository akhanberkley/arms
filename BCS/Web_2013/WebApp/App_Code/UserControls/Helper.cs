#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.WebApp.Components;
using System.Collections.Generic; 
#endregion

namespace BCS.WebApp.UserControls
{
    public enum BCSButtonContext
    {
        Add,
        Edit,
        AddEdit
    }

    /// <summary>
    /// Summary description for Helper
    /// </summary>
    public class Helper
    {
        public static ListControl ClearDBItems(ListControl lc)
        {
            lc.Items.Clear();
            lc.Items.Insert(0, new ListItem("", "0"));
            return lc;
        }
        public static Biz.DataManager AddAgencyCancelCriteria(Biz.DataManager dm, DateTime date)
        {
            if (date == DateTime.MinValue)
                date = System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            Biz.DataManager.CriteriaGroup c1 = new OrmLib.DataManagerBase.CriteriaGroup();
            c1.And(Biz.JoinPath.Agency.Columns.CancelDate, null).Or(Biz.JoinPath.Agency.Columns.CancelDate, date, OrmLib.MatchType.GreaterOrEqual);
            dm.QueryCriteria.And(c1);
            Biz.DataManager.CriteriaGroup c2 = new OrmLib.DataManagerBase.CriteriaGroup();
            c2.And(Biz.JoinPath.Agency.Columns.EffectiveDate, null).Or(Biz.JoinPath.Agency.Columns.EffectiveDate, date, OrmLib.MatchType.LesserOrEqual);
            dm.QueryCriteria.And(c2);
            return dm;
        }

        public static Biz.DataManager AddAgentCancelCriteria(Biz.DataManager dm, DateTime CancelDate)
        {
            if (CancelDate == DateTime.MinValue)
                CancelDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value;
            Biz.DataManager.CriteriaGroup c1 = new OrmLib.DataManagerBase.CriteriaGroup();
            c1.And(Biz.JoinPath.Agent.Columns.CancelDate, null).Or(Biz.JoinPath.Agent.Columns.CancelDate, CancelDate, OrmLib.MatchType.Greater);
            dm.QueryCriteria.And(c1);
            Biz.DataManager.CriteriaGroup c2 = new OrmLib.DataManagerBase.CriteriaGroup();
            c2.And(Biz.JoinPath.Agent.Columns.EffectiveDate, null).Or(Biz.JoinPath.Agent.Columns.EffectiveDate, CancelDate, OrmLib.MatchType.Lesser);
            dm.QueryCriteria.And(c2);
            return dm;
        }


        public static ListItem[] GetLOBGridUpdateStatuses(int CompanyId)
        {
            string[] statusIds = DefaultValues.GetStatusIds(CompanyId);
            
            List<ListItem> ddlStatuses = new List<ListItem>();

            ddlStatuses.Add(new ListItem("", "0"));
            if (statusIds.Length != 0)
            {
                BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
                dm.QueryCriteria.And(BCS.Biz.JoinPath.SubmissionStatus.Columns.Id, statusIds, OrmLib.MatchType.In);
                BCS.Biz.SubmissionStatusCollection statuses = dm.GetSubmissionStatusCollection();
                statuses = statuses.SortByStatusCode(OrmLib.SortDirection.Ascending);
                foreach (BCS.Biz.SubmissionStatus var in statuses)
                {
                    ddlStatuses.Add(new ListItem(var.StatusCode, var.Id.ToString()));
                }
            }
            return ddlStatuses.ToArray();
        }
    }    
}
