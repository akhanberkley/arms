#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlTypes;
using System.Data;
using System.Collections;
using System.Linq;

#endregion

namespace BCS.WebApp.UserControls
{
    public enum Role
    {
        None,
        Analyst,
        Technician,
        Underwriter,
        UnderwriterNewBusiness
    }
    /// <summary>
    /// Summary description for UnderwritersDropDownList
    /// </summary>
    public class UnderwritersDropDownList : System.Web.UI.WebControls.DropDownList
    {
        #region Properties

        private List<PersonAps> m_UwApsPersonCollection;
        private List<PersonAps> m_AnApsPersonCollection;
        /// <summary>
        /// 
        /// </summary>
        [Category("BCS")]
        public int CompanyNumberId
        {
            get
            {
                return ViewState["CompanyNumberId"] == null ? 0 : (int)ViewState["CompanyNumberId"];
            }
            set
            {
                ViewState["CompanyNumberId"] = value;
            }
        }
        [Category("BCS")]
        public Role Role
        {
            get
            {
                return ViewState["Role"] == null ? Role.None : (Role)ViewState["Role"];
            }
            set
            {
                ViewState["Role"] = value;
            }
        }

        [Description("whether to load inactive persons")]
        [Category("BCS")]
        public bool ActiveOnly
        {
            set
            { ViewState["ActiveOnly"] = value; }
            get
            { return (bool?)ViewState["ActiveOnly"] ?? false; }
        }

        [Description("Extra to be added to the list in case not present by bindings provided")]
        [Category("BCS")]
        public Int64 Extra
        {
            set { ViewState["Extra"] = value; }
            get { return (Int64?)ViewState["Extra"] ?? 0; }
        }

        [Description("indicator to fill all underwriters regardless of assigned or not")]
        [Category("BCS")]
        public bool FillAll
        {
            set { ViewState["FillAll"] = value; }
            get { return (bool?)ViewState["FillAll"] ?? false; }
        }

        [Description("Gets or sets a value indicating the primary be defaulted or not.")]
        [Category("BCS")]
        [DefaultValue(true)]
        public bool Default
        {
            set { ViewState["Default"] = value; }
            get { return (bool?)ViewState["Default"] ?? true; }
        }

        [Description("Gets or sets a value indicating the persons not assigned to the supplied agency and UW to be identified by " +
            "'(unassign)' at the end.")]
        [Category("BCS")]
        [DefaultValue(true)]
        public bool IdentifyUnassigned
        {
            set { ViewState["IdentifyUnassigned"] = value; }
            get { return (bool?)ViewState["IdentifyUnassigned"] ?? true; }
        }

        [Description("Gets or sets a date considered for retirement.")]
        [Category("BCS")]
        [DefaultValue(true)]
        public DateTime RetirementDate
        {
            set
            { ViewState["RetirementDate"] = value; }
            get
            { return (DateTime?)ViewState["RetirementDate"] ?? SqlDateTime.MinValue.Value; }
        }

        public bool UsesAPS
        {
            set
            { ViewState["UsesAPS"] = value; }
            get
            {
                if (ViewState["UsesAPS"] == null)
                    ViewState["UsesAPS"] = DefaultValues.SupportsAPSServiceforAssignments(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

                return (bool)ViewState["UsesAPS"];
            }
        }

        #endregion

        #region DataBinds
        public override void DataBind()
        {
            base.Items.Clear();
            base.Items.Insert(0, new ListItem("", "0"));
            base.DataBind();
        }

        public void BindUnderwriters(int companyNumberId)
        {
            base.Items.Clear();

            DataManager dm = new DataManager(DefaultValues.DSN);

            dm.QueryCriteria.And(Biz.JoinPath.Person.Columns.CompanyNumberId, companyNumberId);
            dm.QueryCriteria.And(Biz.JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.UnderwritingRoleId, GetRoleId());

            Biz.PersonCollection people = dm.GetPersonCollection();

            base.DataSource = people.SortByFullName(OrmLib.SortDirection.Ascending);
            base.DataTextField = "FullName";
            base.DataValueField = "Id";

            base.DataBind();

            base.Items.Insert(0, new ListItem("(Select Underwriter)", "0"));
        }

        public void DataBind(int agencyId, params int[] lobIds)
        {
            if (UsesAPS)
            {
                List<PersonAps> personList1 = new List<PersonAps>();
                List<PersonAps> personList2 = new List<PersonAps>();
                DataBind(string.Empty
                        , true
                        , agencyId
                        , DateTime.MinValue
                        , out personList1
                        , out personList2
                        , lobIds);
                return;
            }
            if (CompanyNumberId == 0 || Role == Role.None || agencyId == 0 || lobIds.Length == 0)
            { DataBind(); return; }

            base.Items.Clear();
            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));



            int roleId = GetRoleId();
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            dataManager.QueryCriteria.Clear();
            // get all rows for the agency and line of business and role
            dataManager.QueryCriteria.And(
                JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agencyId).And(
                JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.LineOfBusinessId, lobIds, OrmLib.MatchType.In).And(
                JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.UnderwritingRoleId, roleId);

            AgencyLineOfBusinessPersonUnderwritingRolePersonCollection all =
                dataManager.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection();

            // We will split the query to get just the person ids

            List<int> listPersonIds = new List<int>();
            foreach (AgencyLineOfBusinessPersonUnderwritingRolePerson var in all)
            {
                listPersonIds.Add(var.PersonId);
            }
            int[] arrPersonIds = new int[listPersonIds.Count];
            listPersonIds.CopyTo(arrPersonIds);

            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(JoinPath.Person.Columns.CompanyNumberId, CompanyNumberId);
            dataManager.QueryCriteria.And(JoinPath.Person.Columns.Id, arrPersonIds, OrmLib.MatchType.In);
            if (ActiveOnly)
                AddInactiveCriteria(dataManager, RetirementDate);

            PersonCollection persons = dataManager.GetPersonCollection();
            ListItemCollection items = new ListItemCollection();

            bool aPrimaryAlreadySelected = false;

            // we still are using single lob in this context, multiple refers to a different entity companynumberlobgrid table.
            int templob = lobIds.Length == 0 ? 0 : lobIds[0];
            foreach (Person person in persons)
            {
                string displayFormat = person.FullName;

                foreach (AgencyLineOfBusinessPersonUnderwritingRolePerson allVar in all)
                {
                    if (allVar.PersonId == person.Id && allVar.AgencyId == agencyId && allVar.LineOfBusinessId == templob
                        && allVar.UnderwritingRoleId == roleId)
                    {
                        if (!ActiveOnly)
                        {
                            if (!Common.IsActive(person, RetirementDate))
                                displayFormat = string.Format("{0} ({1})", person.FullName, "inactive");

                        }
                        if (!aPrimaryAlreadySelected)
                        {
                            if (allVar.Primary.Value)
                            {
                                base.ClearSelection();
                                ListItem li = new ListItem(displayFormat, person.Id.ToString());
                                if (Default)
                                {
                                    li.Selected = true;
                                    aPrimaryAlreadySelected = true;
                                }
                                base.Items.Add(li);

                                continue;
                            }
                        }
                        items.Add(new ListItem(displayFormat, person.Id.ToString()));
                    }
                }
            }

            if (FillAll)
            {
                dataManager.QueryCriteria.Clear();
                dataManager.QueryCriteria.And(JoinPath.Person.Columns.CompanyNumberId, CompanyNumberId);
                if (ActiveOnly)
                    AddInactiveCriteria(dataManager, RetirementDate);

                PersonCollection allPersons = dataManager.GetPersonCollection();
                foreach (Person person in allPersons)
                {
                    string displayFormat = person.FullName;
                    if (!ActiveOnly)
                    {
                        if (!Common.IsActive(person, RetirementDate))
                            displayFormat = string.Format("{0} ({1})", person.FullName, "inactive");

                    }
                    if (null == items.FindByValue(person.Id.ToString()))
                    {
                        if (IdentifyUnassigned)
                            displayFormat += " (unassigned)";
                        items.Add(new ListItem(displayFormat, person.Id.ToString()));
                    }
                }
                if (Extra != 0)
                {
                    dataManager.QueryCriteria.Clear();
                    dataManager.QueryCriteria.And(Biz.JoinPath.Person.Columns.CompanyNumberId, CompanyNumberId);
                    dataManager.QueryCriteria.And(Biz.JoinPath.Person.Columns.Id, Extra);
                    Person extraPerson = dataManager.GetPerson();
                    if (extraPerson != null)
                    {
                        string displayFormat = extraPerson.FullName;
                        if (!Common.IsActive(extraPerson, RetirementDate))
                            displayFormat = string.Format("{0} ({1})", extraPerson.FullName, "inactive");

                        items.Add(new ListItem(displayFormat, extraPerson.Id.ToString()));
                    }
                }
            }

            base.Items.AddRange(Common.SortListItems(items));
        }

        public void DataBind(string uwNameNotinCollection,
                             bool useApsService,
                             int agencyId,
                             DateTime submissionEffectiveDate,
                             out List<PersonAps> uwApsCollection,
                             out List<PersonAps> anApsCollection,
                             params int[] lobIds)
        {
            if (!useApsService || CompanyNumberId == 0 || Role == Role.None || agencyId == 0 || lobIds.Length == 0)
            {
                anApsCollection = null;
                uwApsCollection = null;
                DataBind();
                return;
            }

            base.Items.Clear();
            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));

            DataManager dataManager = new DataManager(DefaultValues.DSN);

            dataManager.QueryCriteria.Clear();
            // get all rows for the agency and line of business and role
            dataManager.QueryCriteria.And(
                JoinPath.Agency.Columns.Id, agencyId).And(
                JoinPath.Agency.CompanyNumber.Columns.Id, CompanyNumberId);

            BCS.Biz.AgencyCollection agencies = dataManager.GetAgencyCollection();
            BCS.Biz.Agency agency = agencies.FindById(agencyId);

            dataManager.QueryCriteria.Clear();
            // get all rows for the agency and line of business and role
            dataManager.QueryCriteria.And(JoinPath.LineOfBusiness.Columns.Id, lobIds[0]);
            BCS.Biz.LineOfBusiness lob = dataManager.GetLineOfBusiness();

            dataManager.QueryCriteria.Clear();
            // get all rows for the agency and line of business and role
            dataManager.QueryCriteria.And(JoinPath.CompanyNumber.Columns.Id, CompanyNumberId);
            BCS.Biz.CompanyNumber compNo = dataManager.GetCompanyNumber();

            // HACK HACK...find a better way.
            //int roleIds = { 1, 2 }; //UW, AN
            // Call the service
            // Bind the ids to the LIST
            CompanyParameter parameter = ConfigValues.GetCompanyParameter((int)compNo.CompanyId);
            APSService.APSService apsService = new APSService.APSService();
            apsService.Url = parameter.APSService; //Components.DefaultValues.GetAPSServiceUrl((int)(compNo.CompanyId));
            List<string> roleIds = new List<string>(new string[] { "UW", "AN" });


            string primaryUWRole = parameter.PrimaryUWRole;
            string primaryANRole = parameter.PrimaryANRole;
            string primaryTNRole = null;
            if (Role == Role.Technician)
            {
                dataManager.QueryCriteria.Clear();
                dataManager.QueryCriteria.And(BCS.Biz.JoinPath.Company.Columns.Id, compNo.CompanyId);
                BCS.Biz.Company company = dataManager.GetCompany(FetchPath.Company.UnderwritingRole_by_TechnicianRoleId);

                var technicationRole = company.UnderwritingRole_by_TechnicianRoleId;
                if (technicationRole != null)
                    primaryTNRole = technicationRole.RoleCode;
            }

            if (!string.IsNullOrEmpty(primaryUWRole))
                roleIds.Add(primaryUWRole);
            if (!string.IsNullOrEmpty(primaryANRole))
                roleIds.Add(primaryANRole);
            if (!string.IsNullOrEmpty(primaryTNRole))
                roleIds.Add(primaryTNRole);

            APSService.personnelAssigned[] personnel = null;
            bool bAPSReqParamsAvailable = agency != null && (bool)(!agency.APSId.IsNull) && (bool)(agency.APSId > SqlInt64.MinValue) &&
                                 agency.APSId.ToString().Length > 0 && lob != null &&
                                (!lob.APSId.IsNull) && (bool)(lob.APSId > SqlInt64.MinValue) &&
                                agency.APSId.ToString().Length > 0;


            string cacheKey = string.Empty;
            BTS.CacheEngine.V2.CacheEngine cacheEngine = new BTS.CacheEngine.V2.CacheEngine(DefaultValues.DefaultObjectCacheTimeInMinutes);
            if (bAPSReqParamsAvailable)
            {
                try
                {

                    personnel = apsService.getPersonnelAssignmentsByAgencyIdUWIdRoleTypes
                        (long.Parse(agency.APSId.ToString()),
                         long.Parse(lob.APSId.ToString()),
                         roleIds.ToArray());
                }
                catch (Exception ex)
                {
                    //to do
                }
            }

            ListItemCollection items = new ListItemCollection();

            uwApsCollection = new List<PersonAps>();
            anApsCollection = new List<PersonAps>();

            bool aPrimaryAlreadySelected = false;
            bool primaryUWFound = false;
            bool primaryANFound = false;
            bool primaryTNFound = false;
            bool bNotInCollection = true;
            int uwIter = 1;
            int anIter = 1;

            //Hack for BRSIC regarding dummy agency
            if (lob != null && agency.UnfilteredUnderwriting)
            {
                APSService.personnelAssigned[] personnelUW = apsService.getPersonnelByRoleCode("UW");
                APSService.personnelAssigned[] personnelC = apsService.getPersonnelByRoleCode("UW-C");
                if (personnelC != null && personnelC.Length > 0)
                    personnel = personnelUW.Concat(personnelC).ToArray();
                else
                    personnel = personnelUW;

                foreach (APSService.personnelAssigned person in personnel)
                {
                    PersonAps apsPerson = null;

                    //first, check if the person is retired = do not add
                    DateTime personRetirementDate = (!string.IsNullOrEmpty(person.retirementDate)) ? Convert.ToDateTime(person.retirementDate) : DateTime.MinValue;
                    if (personRetirementDate > submissionEffectiveDate || personRetirementDate == DateTime.MinValue)
                    {
                        #region Underwriter
                        if ((person.roleCode.Equals("UW") || person.roleCode.Equals(primaryUWRole)) && Role == Role.Underwriter)
                        {
                            ListItem item = new ListItem(person.fullName, person.id.ToString());
                            if (person.fullName.Equals(uwNameNotinCollection, StringComparison.CurrentCultureIgnoreCase))
                                bNotInCollection = false;
                            items.Add(item);
                            apsPerson = new PersonAps(person.id, person.fullName, "UW", primaryUWFound, uwIter);
                            uwApsCollection.Add(apsPerson);
                            uwIter++;
                        }
                        #endregion
                        #region  Analyst
                        if ((person.roleCode.Equals("AN") || person.roleCode.Equals(primaryANRole)) && Role == Role.Analyst)
                        {
                            ListItem item = new ListItem(person.fullName, person.id.ToString());//anIter.ToString());
                            if (person.fullName.Equals(uwNameNotinCollection, StringComparison.CurrentCultureIgnoreCase))
                                bNotInCollection = false;
                            items.Add(item);
                            apsPerson = new PersonAps(person.id, person.fullName, "AN", primaryANFound, anIter);
                            anApsCollection.Add(apsPerson);
                            anIter++;
                        }
                        #endregion
                    }
                }

            }
            else if (personnel != null)
            {
                APSService.personnelAssigned primaryPerson = null;
                List<APSService.personnelAssigned> personnelList = null;

                if (Role == Role.Underwriter)
                    primaryPerson = GetPrimaryPersonnelAssigned(personnel, Role, primaryUWRole, submissionEffectiveDate);
                if (Role == Role.Analyst)
                    primaryPerson = GetPrimaryPersonnelAssigned(personnel, Role, primaryANRole, submissionEffectiveDate);
                if (Role == Role.Technician)
                    primaryPerson = GetPrimaryPersonnelAssigned(personnel, Role, primaryTNRole, submissionEffectiveDate);

                if (primaryPerson != null)
                {
                    personnelList = new List<APSService.personnelAssigned>(personnel);
                    personnelList.Remove(primaryPerson);
                    personnelList.Insert(0, primaryPerson);
                    personnel = personnelList.ToArray();
                }

                foreach (APSService.personnelAssigned person in personnel)
                {
                    PersonAps apsPerson = null;

                    //first, check if the person is retired = do not add
                    DateTime personRetirementDate = (!string.IsNullOrEmpty(person.retirementDate)) ? Convert.ToDateTime(person.retirementDate) : DateTime.MinValue;
                    if (personRetirementDate > submissionEffectiveDate || personRetirementDate == DateTime.MinValue)
                    {
                        #region Underwriter
                        if ((person.roleCode.Equals("UW") || person.roleCode.Equals(primaryUWRole)) && Role == Role.Underwriter)
                        {
                            ListItem item = new ListItem(person.fullName, person.id.ToString());
                            if (person.fullName.Equals(uwNameNotinCollection, StringComparison.CurrentCultureIgnoreCase))
                                bNotInCollection = false;
                            if (!aPrimaryAlreadySelected)
                            {
                                if (person.isPrimary.ToLower().Equals("true"))
                                {
                                    base.ClearSelection();
                                    primaryUWFound = true;
                                    if (Default)
                                    {
                                        item.Selected = true;
                                        aPrimaryAlreadySelected = true;
                                    }
                                }
                                else
                                    primaryUWFound = false;
                            }
                            items.Add(item);
                            apsPerson = new PersonAps(person.id, person.fullName, "UW", primaryUWFound, uwIter);
                            uwApsCollection.Add(apsPerson);
                            uwIter++;
                        }
                        #endregion
                        #region  Analyst
                        if ((person.roleCode.Equals("AN") || person.roleCode.Equals(primaryANRole)) && Role == Role.Analyst)
                        {
                            ListItem item = new ListItem(person.fullName, person.id.ToString());//anIter.ToString());
                            if (person.fullName.Equals(uwNameNotinCollection, StringComparison.CurrentCultureIgnoreCase))
                                bNotInCollection = false;
                            if (!aPrimaryAlreadySelected)
                            {
                                if (person.isPrimary.ToLower().Equals("true"))
                                {
                                    base.ClearSelection();
                                    primaryANFound = true;
                                    if (Default)
                                    {
                                        item.Selected = true;
                                        aPrimaryAlreadySelected = true;
                                    }
                                }
                                else
                                {
                                    primaryANFound = false;
                                }
                            }
                            items.Add(item);
                            apsPerson = new PersonAps(person.id, person.fullName, "AN", primaryANFound, anIter);
                            anApsCollection.Add(apsPerson);
                            anIter++;
                        }
                        #endregion
                        #region  Technician
                        if (person.roleCode.Equals(primaryTNRole) && Role == Role.Technician)
                        {
                            ListItem item = new ListItem(person.fullName, person.id.ToString());//anIter.ToString());
                            if (person.fullName.Equals(uwNameNotinCollection, StringComparison.CurrentCultureIgnoreCase))
                                bNotInCollection = false;
                            if (!aPrimaryAlreadySelected)
                            {
                                if (person.isPrimary.ToLower().Equals("true"))
                                {
                                    base.ClearSelection();
                                    primaryTNFound = true;
                                    if (Default)
                                    {
                                        item.Selected = true;
                                        aPrimaryAlreadySelected = true;
                                    }
                                }
                                else
                                {
                                    primaryTNFound = false;
                                }
                            }
                            items.Add(item);
                            apsPerson = new PersonAps(person.id, person.fullName, primaryTNRole, primaryTNFound, anIter);
                            anApsCollection.Add(apsPerson);
                            anIter++;
                        }
                        #endregion
                    }
                }

                #region Fill Persons From BCS database

                // Filling the collection w/ Persons from Database, CWG Request
                // The assumption is that CWG APS Service returns only Primary UW, only 1 record.
                string strPrimaryUWNameFromAPS = string.Empty;
                bool bFoundPrimaryUWFromAPS = false;
                foreach (PersonAps person in uwApsCollection)
                {
                    if ((person.APSRoleCode.Equals("UW") || person.APSRoleCode.Equals(primaryUWRole)) && person.IsPrimary)
                    {
                        strPrimaryUWNameFromAPS = person.APSName;
                        bFoundPrimaryUWFromAPS = true;
                        break;
                    }
                }
                if (Components.DefaultValues.SupportsFillPersonsInAssignments((int)compNo.CompanyId))
                {
                    cacheKey = string.Format("{0}|Underwriters", CompanyNumberId);
                    APSService.personnelAssigned[] persons = null;
                    if (!cacheEngine.CachedItemExist(cacheKey))
                    {
                        //BCS.Biz.PersonCollection persons = GetBizPersonCollection((int)compNo.CompanyId);
                        persons = GetAPSAssignedPersonCollection(apsService);
                        cacheEngine.AddItemToCache(cacheKey, persons);
                    }
                    else
                    {
                        persons = (APSService.personnelAssigned[])cacheEngine.GetCachedItem(cacheKey);
                    }
                    foreach (APSService.personnelAssigned person in persons)
                    {
                        DateTime personRetirementDate = (!string.IsNullOrEmpty(person.retirementDate)) ? Convert.ToDateTime(person.retirementDate) : DateTime.MinValue;
                        if (person.fullName != strPrimaryUWNameFromAPS && (personRetirementDate > submissionEffectiveDate || personRetirementDate == DateTime.MinValue) && uwApsCollection.Where(c => c.ApsId == person.id).Select(a => a.ApsId).Count() == 0)
                        {
                            int cntr = uwApsCollection.Count + 1;
                            PersonAps personApsFromDB = new PersonAps(person.id.ToString(), person.fullName, "UW", false, cntr);
                            ListItem itemsFromDB = new ListItem(string.Format("{0} (unassigned)", person.fullName), person.id);
                            itemsFromDB.Selected = false;
                            items.Add(itemsFromDB);
                            uwApsCollection.Add(personApsFromDB);
                        }
                    }
                }
                #endregion
            }

            if (bNotInCollection && bAPSReqParamsAvailable && uwNameNotinCollection != string.Empty)
            {
                //Add item to uwApsCollection
                BCS.Biz.Person person = GetBizPerson(SessionKeys.CompanyNumber, uwNameNotinCollection);
                if (person != null)
                {
                    // Add to collection.
                    int cntr = uwApsCollection.Count + 1;
                    PersonAps notInCollectionApsPerson = new PersonAps(person.Id.ToString(), person.FullName, "UW", true, cntr);
                    ListItem itemNotInCollection = new ListItem(person.FullName, person.Id.ToString());
                    itemNotInCollection.Selected = true;
                    items.Add(itemNotInCollection);
                    uwApsCollection.Add(notInCollectionApsPerson);
                }
            }


            base.Items.AddRange(Common.SortListItems(items));
        }

        private APSService.personnelAssigned GetPrimaryPersonnelAssigned(APSService.personnelAssigned[] personnel, Role role, string primaryRole, DateTime submissionEffectiveDate)
        {
            List<APSService.personnelAssigned> validPersonnel = new List<APSService.personnelAssigned>(personnel);

            for (int i = personnel.Length - 1; i >= 0; i--)
            {

                DateTime personRetirementDate = (!string.IsNullOrEmpty(personnel[i].retirementDate)) ? Convert.ToDateTime(personnel[i].retirementDate) : DateTime.MinValue;
                if (personRetirementDate < submissionEffectiveDate && personRetirementDate != DateTime.MinValue)
                {
                    validPersonnel.RemoveAt(i);
                    continue;
                }

                if (personnel[i].isPrimary.ToLower() == "true" && !string.IsNullOrEmpty(primaryRole) && personnel[i].roleCode.ToUpper() == primaryRole.ToUpper())
                    return personnel[i];
            }
            switch (role.ToString())
            {
                case "Underwriter":
                    foreach (APSService.personnelAssigned item in personnel)
                    {
                        if (item.roleCode == "UW" && item.isPrimary.ToLower() == "true")
                            return item;
                    }
                    break;
                case "Analyst":
                    foreach (APSService.personnelAssigned item in personnel)
                    {
                        if (item.roleCode == "AN" && item.isPrimary.ToLower() == "true")
                            return item;
                    }
                    break;
            }

            return null;
        }

        private BCS.Biz.PersonCollection GetBizPersonCollection(int companyId)
        {
            BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.CompanyNumber.Columns.CompanyId, companyId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.Columns.Active, true);
            BCS.Biz.PersonCollection personCol = dm.GetPersonCollection();

            return personCol;
        }

        private APSService.personnelAssigned[] GetAPSAssignedPersonCollection(APSService.APSService apsService)
        {
            string uwRoleCode = "UW";

            APSService.personnelAssigned[] personnelAssigned = apsService.getPersonnelByRoleCode(uwRoleCode);
            return personnelAssigned;
        }

        private BCS.Biz.Person GetBizPerson(string companyNumber, string personName)
        {
            BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.Person.Columns.FullName, personName);
            BCS.Biz.Person person = dm.GetPerson();

            return person;
        }

        public List<PersonAps> DataBind(string anNameNotInCollection,
                                         bool useApsService,
                                         bool loadFromSession,
                                         List<PersonAps> anApsPersonCollection,
                                         int agencyId,
                                         params int[] lobIds)
        {
            if (!useApsService || CompanyNumberId == 0 || Role == Role.None || agencyId == 0 || lobIds.Length == 0)
            { DataBind(); return null; }

            base.Items.Clear();
            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));

            ListItemCollection items = new ListItemCollection();
            bool notInCollection = true;
            bool dataAvailable = agencyId > 0 && lobIds[0] > 0;

            #region DO NOT LOAD From Session
            if (!loadFromSession)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                dataManager.QueryCriteria.Clear();
                // get all rows for the agency and line of business and role
                dataManager.QueryCriteria.And(
                    JoinPath.Agency.Columns.Id, agencyId).And(
                    JoinPath.Agency.CompanyNumber.Columns.Id, CompanyNumberId);

                BCS.Biz.AgencyCollection agencies = dataManager.GetAgencyCollection();
                BCS.Biz.Agency agency = agencies.FindById(agencyId);

                dataManager.QueryCriteria.Clear();
                // get all rows for the agency and line of business and role
                dataManager.QueryCriteria.And(JoinPath.LineOfBusiness.Columns.Id, lobIds[0]);
                BCS.Biz.LineOfBusinessCollection lobs = dataManager.GetLineOfBusinessCollection();
                BCS.Biz.LineOfBusiness lob = lobs.FindById(lobIds[0]);

                long agencyAPSId;
                long lobAPSId;

                string[] roleIds = { "AN" };
                //int[] roleIds = { 2 };
                // Call the service
                // Bind the ids to the LIST
                APSService.APSService apsService = new APSService.APSService();
                APSService.personnelAssigned[] personnel = null;
                bool validAPS = (agency != null && (bool)(!agency.APSId.IsNull) && (bool)(agency.APSId > SqlInt64.MinValue) && lob != null
                    && (bool)(lob.APSId > SqlInt64.MinValue));
                if (validAPS)
                {
                    personnel =
                        apsService.getPersonnelAssignmentsByAgencyIdUWIdRoleTypes
                                (long.Parse(agency.APSId.ToString()),
                                 long.Parse(lob.APSId.ToString()),
                                 roleIds);

                }
                List<PersonAps> anApsCollection = new List<PersonAps>();
                bool aPrimaryAlreadySelected = false;
                bool primaryANFound = false;
                int anIter = 1;
                if (personnel != null)
                {
                    foreach (APSService.personnelAssigned person in personnel)
                    {
                        PersonAps apsPerson = null;
                        if (person.roleCode.Equals("AN"))
                        {
                            if (person.isPrimary.ToLower().Equals("true"))
                            {
                                primaryANFound = true;
                            }
                            else
                            {
                                primaryANFound = false;
                            }
                            apsPerson = new PersonAps(person.id, person.fullName, "AN", primaryANFound, anIter);
                            anApsCollection.Add(apsPerson);
                            anIter++;
                        }
                    }
                }
            }
            #endregion
            #region LOAD FROM SESSION
            else
            {
                if (anApsPersonCollection != null && anApsPersonCollection.Count > 0)
                {
                    bool aPrimaryAlreadySelected = false;
                    foreach (PersonAps person in anApsPersonCollection)
                    {
                        if (person.APSRoleCode.Equals("AN"))
                        {
                            ListItem item = new ListItem(person.APSName, person.ControlIndex.ToString());
                            if (person.APSName.Equals(anNameNotInCollection))
                                notInCollection = false;

                            if (person.IsPrimary)
                            {
                                base.ClearSelection();
                                if (Default)
                                {
                                    item.Selected = true;
                                    aPrimaryAlreadySelected = true;
                                }
                            }
                            items.Add(item);
                        }
                    }
                }
            }
            #endregion

            if (notInCollection && dataAvailable && anNameNotInCollection != string.Empty)
            {
                //Add item to uwApsCollection
                BCS.Biz.Person person = GetBizPerson(SessionKeys.CompanyNumber, anNameNotInCollection);
                if (person != null)
                {
                    // Add to collection.
                    int cntr = anApsPersonCollection.Count + 1;
                    PersonAps notInCollectionApsPerson = new PersonAps(person.Id.ToString(), person.FullName, "AN", true, cntr);
                    ListItem itemNotInCollection = new ListItem(person.FullName, cntr.ToString());
                    itemNotInCollection.Selected = true;
                    items.Add(itemNotInCollection);
                    anApsPersonCollection.Add(notInCollectionApsPerson);
                }
            }

            base.Items.AddRange(Common.SortListItems(items));
            return anApsPersonCollection;
        }

        public void DataBind(int agencyId)
        {
            if (UsesAPS)
            {
                DataBindAPS(agencyId);
                return;
            }

            if (CompanyNumberId == 0 || Role == Role.None)
            { DataBind(); return; }

            base.Items.Clear();
            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));

            int roleId = GetRoleId();
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(
                JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agencyId).And(
                JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.UnderwritingRoleId, roleId);

            AgencyLineOfBusinessPersonUnderwritingRolePersonCollection all =
                dataManager.GetAgencyLineOfBusinessPersonUnderwritingRolePersonCollection();

            // We will split the query to get just the person ids

            List<int> listPersonIds = new List<int>();
            foreach (AgencyLineOfBusinessPersonUnderwritingRolePerson var in all)
            {
                listPersonIds.Add(var.PersonId);
            }
            int[] arrPersonIds = new int[listPersonIds.Count];
            listPersonIds.CopyTo(arrPersonIds);


            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(JoinPath.Person.Columns.CompanyNumberId, CompanyNumberId);
            dataManager.QueryCriteria.And(JoinPath.Person.Columns.Id, arrPersonIds, OrmLib.MatchType.In);
            if (ActiveOnly)
                AddInactiveCriteria(dataManager, RetirementDate);

            PersonCollection persons = dataManager.GetPersonCollection();
            ListItemCollection items = new ListItemCollection();

            bool aPrimaryAlreadySelected = false;

            foreach (Person person in persons)
            {
                string displayFormat = person.FullName;

                foreach (AgencyLineOfBusinessPersonUnderwritingRolePerson allVar in all)
                {
                    if (allVar.PersonId == person.Id && allVar.AgencyId == agencyId
                        && allVar.UnderwritingRoleId == roleId)
                    {
                        if (!ActiveOnly)
                        {
                            if (!Common.IsActive(person, RetirementDate))
                                displayFormat = string.Format("{0} ({1})", person.FullName, "inactive");

                        }
                        if (allVar.Primary.Value)
                        {
                            base.ClearSelection();
                            ListItem li = new ListItem(displayFormat, person.Id.ToString());
                            if (!aPrimaryAlreadySelected)
                            {
                                if (Default)
                                {
                                    li.Selected = true;
                                    aPrimaryAlreadySelected = true;
                                }
                            }
                            if (null == items.FindByValue(li.Value))
                                items.Add(li);

                            continue;
                        }
                        if (null == items.FindByValue(person.Id.ToString()))
                            items.Add(new ListItem(displayFormat, person.Id.ToString()));
                    }
                }
            }
            base.Items.AddRange(Common.SortListItems(items));
        }

        public void DataBindAPS(int agencyId)
        {
            if (CompanyNumberId == 0 || Role == Role.None)
            { DataBind(); return; }

            base.Items.Clear();
            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));

            int roleId = GetRoleId();
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(Biz.JoinPath.Person.Columns.CompanyNumberId, CompanyNumberId);

            Biz.PersonCollection people = dataManager.GetPersonCollection();
            ListItemCollection items = new ListItemCollection();
            foreach (Person person in people)
            {
                items.Add(new ListItem(person.FullName, person.APSId.ToString()));
            }

            /*
            dataManager.QueryCriteria.Clear();
            // get all rows for the agency and line of business and role
            dataManager.QueryCriteria.And(
                JoinPath.Agency.Columns.Id, agencyId).And(
                JoinPath.Agency.CompanyNumber.Columns.Id, CompanyNumberId);

            BCS.Biz.Agency agency = dataManager.GetAgency();

            dataManager.QueryCriteria.Clear();

            dataManager.QueryCriteria.And(JoinPath.CompanyNumber.Columns.Id, CompanyNumberId);
            BCS.Biz.CompanyNumber compNo = dataManager.GetCompanyNumber();

            APSService.APSService apsService = new APSService.APSService();
            apsService.Url = Components.DefaultValues.GetAPSServiceUrl((int)(compNo.CompanyId));
            List<string> roleIds = new List<string>();//(new string[] { "UW", "AN" });

            CompanyParameter parameter = ConfigValues.GetCompanyParameter((int)compNo.CompanyId);

            string primaryUWRole = parameter.PrimaryUWRole;
            string primaryANRole = parameter.PrimaryANRole;

            if (Role == Role.Underwriter)
            {
                roleIds.Add("UW");
                if (!string.IsNullOrEmpty(primaryUWRole))
                    roleIds.Add(primaryUWRole);
            }
            else if (Role == Role.Analyst)
            {
                roleIds.Add("AN");
                if (!string.IsNullOrEmpty(primaryANRole))
                    roleIds.Add(primaryANRole);
            }

            APSService.companyPersonnel[] persons = apsService.getAgencyCompanyPersonnelAssignmentsByAgencyId(long.Parse(agency.APSId.ToString()));

            //APSService.personnel[] persons6 = apsService.getPersonnelByRole(long.Parse(agency.APSId.ToString()),"1",false);

            ListItemCollection items = new ListItemCollection();

            if (persons == null)
                return;

            bool aPrimaryAlreadySelected = false;

            Hashtable personsAdded = new Hashtable();

            foreach (APSService.companyPersonnel person in persons)
            {
                foreach (APSService.companyPersonnelRole role in person.companyPersonnelRoles)
                {
                    if (roleIds.Contains(role.companyPersonnelRoleType.code) && !personsAdded.Contains(person.id))
                    {
                        items.Add(new ListItem(string.Format("{0} {1}",person.firstName,person.lastName), person.id.ToString()));
                        personsAdded.Add(person.id,person.firstName);
                        break;
                    }
                }
            }

            //foreach (Person person in persons)
            //{
            //    string displayFormat = person.FullName;


            //        if (allVar.PersonId == person.Id && allVar.AgencyId == agencyId
            //            && allVar.UnderwritingRoleId == roleId)
            //        {
            //            if (!ActiveOnly)
            //            {
            //                if (!Common.IsActive(person, RetirementDate))
            //                    displayFormat = string.Format("{0} ({1})", person.FullName, "inactive");

            //            }
            //            if (allVar.Primary.Value)
            //            {
            //                base.ClearSelection();
            //                ListItem li = new ListItem(displayFormat, person.Id.ToString());
            //                if (!aPrimaryAlreadySelected)
            //                {
            //                    if (Default)
            //                    {
            //                        li.Selected = true;
            //                        aPrimaryAlreadySelected = true;
            //                    }
            //                }
            //                if (null == items.FindByValue(li.Value))
            //                    items.Add(li);

            //                continue;
            //            }
            //            if (null == items.FindByValue(person.Id.ToString()))
            //                items.Add(new ListItem(displayFormat, person.Id.ToString()));
            //        }
                
            //}
             */
            base.Items.AddRange(Common.SortListItems(items));
        }

        public void DataBindByCompanyNumber()
        {
            base.Items.Clear();
            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));

            int roleId = GetRoleId();
            DataManager dataManager = new DataManager(DefaultValues.DSN);
            dataManager.QueryCriteria.And(JoinPath.Person.Columns.CompanyNumberId, CompanyNumberId);
            if (!UsesAPS)
            {
                dataManager.QueryCriteria.And(JoinPath.Person.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.UnderwritingRoleId, GetRoleId());
            }

            if (ActiveOnly)
                AddInactiveCriteria(dataManager, RetirementDate);

            PersonCollection allPersons = dataManager.GetPersonCollection();
            ListItemCollection items = new ListItemCollection();
            foreach (Person person in allPersons)
            {
                string displayFormat = person.FullName;
                if (!ActiveOnly)
                {
                    if (!Common.IsActive(person, RetirementDate))
                        displayFormat = string.Format("{0} ({1})", person.FullName, "inactive");

                }
                if (null == items.FindByValue(person.Id.ToString()))
                {
                    if (!UsesAPS)
                        items.Add(new ListItem(displayFormat, person.Id.ToString()));
                    else
                        items.Add(new ListItem(displayFormat, person.APSId.ToString()));
                }
            }

            base.Items.AddRange(Common.SortListItems(items));
        }

        #endregion

        #region Other Methods
        private int GetRoleId()
        {
            Biz.UserCompanyCollection userCompanies = Security.GetUserCompanies(System.Web.HttpContext.Current.User.Identity.Name);
            Biz.UserCompany uc = userCompanies.FindByCompanyId(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            if (uc == null)
                return 0;
            Biz.Company company = uc.Company;

            switch (Role)
            {
                case Role.Analyst:
                    {
                        return company.AnalystRoleId.IsNull ? 0 : company.AnalystRoleId.Value;
                    }
                case Role.Technician:
                    {
                        return company.TechnicianRoleId.IsNull ? 0 : company.TechnicianRoleId.Value;
                    }
                case Role.Underwriter:
                    {
                        return company.UnderwriterRoleId.IsNull ? 0 : company.UnderwriterRoleId.Value;
                    }
                case Role.None:
                    {
                        return 0;
                    }

                default:
                    return 0;
            }
        }

        private static BCS.Biz.DataManager AddInactiveCriteria(DataManager dm, DateTime date)
        {
            if (date == DateTime.MinValue)
                date = SqlDateTime.MinValue.Value;
            if (date == DateTime.MaxValue)
                date = SqlDateTime.MaxValue.Value;
            Biz.DataManager.CriteriaGroup c1 = new OrmLib.DataManagerBase.CriteriaGroup();
            c1.And(Biz.JoinPath.Person.Columns.RetirementDate, null).Or(Biz.JoinPath.Person.Columns.RetirementDate, date, OrmLib.MatchType.Greater);
            dm.QueryCriteria.And(c1);
            return dm;
        }
        #endregion

        public class PersonAps
        {
            private string m_apsID;
            private string m_apsName;
            private string m_apsRoleCode;
            private int m_controlIndex;
            private bool m_primary;

            public string ApsId
            {
                get { return m_apsID; }
                set { m_apsID = value; }
            }
            public string APSName
            {
                get { return m_apsName; }
                set { m_apsName = value; }
            }
            public string APSRoleCode
            {
                get { return m_apsRoleCode; }
                set { m_apsRoleCode = value; }
            }
            public int ControlIndex
            {
                get { return m_controlIndex; }
                set { m_controlIndex = value; }
            }
            public bool IsPrimary
            {
                get { return m_primary; }
                set { m_primary = value; }
            }


            public PersonAps(string apsID, string apsName, string apsRoleCode, bool primary, int controlIndex)
            {
                m_apsID = apsID;
                m_apsName = apsName;
                m_apsRoleCode = apsRoleCode;
                m_primary = primary;
                m_controlIndex = controlIndex;
            }


        }
    }

}