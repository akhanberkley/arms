#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for StateListBox
    /// </summary>
    public class StateListBox : ListBox
    {
        public new ListSelectionMode SelectionMode
        {
            get
            {
                return base.SelectionMode;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            if (DataTextField == null || DataTextField.Length == 0)
                DataTextField = "Abbreviation";
            if (DataValueField == null || DataValueField.Length == 0)
                DataValueField = "Id";

            base.SelectionMode = ListSelectionMode.Multiple;

            base.DataSource = getDataSource();
            base.OnInit(e);
        }

        private StateCollection getDataSource()
        {
            if (Page.Cache["States"] == null)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                StateCollection sc = dataManager.GetStateCollection();
                sc = sc.SortByAbbreviation(OrmLib.SortDirection.Ascending);
                Page.Cache["States"] = sc;
                return sc;
            }
            return Page.Cache["States"] as StateCollection;
        }
    }
}