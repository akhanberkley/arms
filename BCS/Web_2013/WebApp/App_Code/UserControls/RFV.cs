using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// TODO: does not work if a standard validation control is not present on the page. seems because of the client functions are not getting registered
    /// </summary>
    public class RFV : RequiredFieldValidator
    {
        public bool ShowStaticAsterisk
        {
            get
            { return (bool?)ViewState["ShowStaticAsterisk"] ?? true; }
            set
            { ViewState["ShowStaticAsterisk"] = value; }
        }
	
        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
            if (ShowStaticAsterisk && Enabled)
                writer.WriteLine("<label class=\"errorText\">*</label>");
        }
    } 
}
