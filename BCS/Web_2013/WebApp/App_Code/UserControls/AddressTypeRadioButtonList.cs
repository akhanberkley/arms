#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for AddressTypeRadioButtonList
    /// </summary>
    public class AddressTypeRadioButtonList : System.Web.UI.WebControls.RadioButtonList
    {
        #region DataBind
        public override void DataBind()
        {
            Lookups lookups = new Lookups(DefaultValues.DSN);

            base.Items.Clear();

            AddressTypeCollection atc = lookups.AddressTypes;
            foreach (AddressType at in atc)
            {
                base.Items.Add(new ListItem(at.PropertyAddressType, at.Id.ToString()));
            }           
        } 
        #endregion
    }
}