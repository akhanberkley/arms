#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
using System.ComponentModel;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for AgentDropDownList
    /// </summary>
    public class AgentDropDownList : System.Web.UI.WebControls.DropDownList
    {
        #region Properties
        public int AgencyId
        {
            get
            {
                if (ViewState["AgencyId"] == null)
                    return 0;
                return (int)ViewState["AgencyId"];
            }
            set
            {
                ViewState["AgencyId"] = value;
            }
        }
        public DateTime CancelDate
        {
            get
            {
                return ViewState["CancelDate"] == null ? DateTime.Now : (DateTime)ViewState["CancelDate"];
            }
            set
            {
                ViewState["CancelDate"] = value;
            }
        }
        [Description("Extra to the list in case not present by bindings provided.")]
        [Category("BCS")]
        public int Extra
        {
            set { ViewState["Extra"] = value; }
            get { return (int?)ViewState["Extra"] ?? 0; }
        }
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.Items.Clear();
            if (this.AgencyId != 0)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                dataManager.QueryCriteria.Clear();
                dataManager.QueryCriteria.And(JoinPath.Agent.Columns.AgencyId, this.AgencyId, OrmLib.MatchType.Exact);
                dataManager.QueryCriteria.Or(JoinPath.Agent.Columns.Id, Extra);

                dataManager = Helper.AddAgentCancelCriteria(dataManager, CancelDate);
                AgentCollection ac = dataManager.GetAgentCollection();
                ListItemCollection items = new ListItemCollection();
                foreach (BCS.Biz.Agent a in ac)
                {
                    items.Add(new ListItem(string.Format("{0} {1} - {2}", a.FirstName, a.Surname, a.BrokerNo), a.Id.ToString()));
                }
                base.Items.AddRange(Common.SortListItems(items));
            }
            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));

            if (base.Items.Count == 2)
            {
                base.Items[1].Selected = true;
            }
        }
        #endregion
    }
}
