#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
	/// <summary>
	/// Summary description for NamePrefixDropDownList.
	/// </summary>
	public class NamePrefixDropDownList : System.Web.UI.WebControls.DropDownList
	{
        #region DataBind
        public override void DataBind()
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            base.Items.Clear();

            NamePrefixCollection npc = dataManager.GetNamePrefixCollection();
            foreach (NamePrefix np in npc)
            {
                base.Items.Add(new ListItem(np.Prefix, np.Prefix));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        } 
        #endregion
	}
}
