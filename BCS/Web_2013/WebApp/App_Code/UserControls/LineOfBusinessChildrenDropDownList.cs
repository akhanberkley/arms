#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
using System.ComponentModel;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for LineOfBusinessChildrenDropDownList
    /// </summary>
    public class LineOfBusinessChildrenDropDownList : System.Web.UI.WebControls.DropDownList
    {
        [Description("Extra to the list in case not present by bindings provided.")]
        [Category("BCS")]
        public int Extra
        {
            set { ViewState["Extra"] = value; }
            get { return (int?)ViewState["Extra"] ?? 0; }
        }
        [Description("whether to load inactive agencies")]
        [Category("BCS")]
        public bool ActiveOnly
        {
            set
            { ViewState["ActiveOnly"] = value; }
            get
            { return (bool?)ViewState["ActiveOnly"] ?? false; }
        }
        [Category("BCS")]
        public int CompanyNumberId
        {
            get
            {
                return ViewState["CompanyNumberId"] == null ? 0 : (int)ViewState["CompanyNumberId"];
            }
            set
            {
                ViewState["CompanyNumberId"] = value;
            }
        }
        [Obsolete("set the company number property and use databind method", false)]
        public void DataBindByCompanyNumber(int companyNumberId)
        {
            if (companyNumberId == 0)
            {
                base.Items.Clear();
                base.Items.Insert(0, new ListItem("", "0"));
                return;
            }
            DataManager dataManager = new DataManager(DefaultValues.DSN);
            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(JoinPath.CompanyNumberLOBGrid.Columns.CompanyNumberId, companyNumberId);
            dataManager.QueryCriteria.And(JoinPath.CompanyNumberLOBGrid.Columns.Active, true);
            dataManager.QueryCriteria.Or(JoinPath.CompanyNumberLOBGrid.Columns.Id, Extra);

            base.Items.Clear();

            CompanyNumberLOBGridCollection lobc = dataManager.GetCompanyNumberLOBGridCollection();
            lobc = lobc.SortByOrder(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.CompanyNumberLOBGrid lob in lobc)
            {
                base.Items.Add(new ListItem(lob.MultiLOBDesc, lob.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        }

        public override void DataBind()
        {
            if (CompanyNumberId == 0)
            {
                base.Items.Clear();
                base.Items.Insert(0, new ListItem("", "0"));
                return;
            }
            CompanyNumberLOBGridCollection lobc;
            string cacheKey = string.Format(CacheKeys.CompanyNumberLOBGrid, CompanyNumberId);
            if (this.Page.Cache[cacheKey] == null)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                dataManager.QueryCriteria.Clear();
                dataManager.QueryCriteria.And(JoinPath.CompanyNumberLOBGrid.Columns.CompanyNumberId, CompanyNumberId);

                lobc = dataManager.GetCompanyNumberLOBGridCollection();
                this.Page.Cache[cacheKey] = lobc;
            }
            else
            {
                lobc = (CompanyNumberLOBGridCollection)this.Page.Cache[cacheKey];
            }            
            if (ActiveOnly)
            {
                BCS.Biz.CompanyNumberLOBGrid extraLob = lobc.FindById(Extra); // store extra before filtering
                lobc = lobc.FilterByActive(true);

                if (null != extraLob) // add extra
                {
                    if(lobc.FindById(Extra) == null)
                        lobc.Add(extraLob);
                }
            }

            lobc = lobc.SortByOrder(OrmLib.SortDirection.Ascending);


            base.Items.Clear();
            foreach (BCS.Biz.CompanyNumberLOBGrid lob in lobc)
            {
                base.Items.Add(new ListItem(lob.MultiLOBDesc, lob.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        }
    }
}