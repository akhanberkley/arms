using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for DateBox
    /// </summary>
    public class MoneyBox : TextBox
    {
        protected override void OnInit(EventArgs e)
        {
            // TODO: enhance to check if formatCurrency script is registered
            this.Attributes.Add("onblur", "this.value=formatCurrency(this.value);");
            base.OnInit(e);
        }
    }
}
