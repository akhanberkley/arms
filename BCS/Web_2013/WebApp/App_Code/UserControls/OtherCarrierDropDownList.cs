#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for OtherCarrierDropDownList
    /// </summary>
    public class OtherCarrierDropDownList : System.Web.UI.WebControls.DropDownList
    {
        [System.ComponentModel.Category("BCS")]
        public int CompanyNumberId
        {
            get
            {
                if (ViewState["CompanyNumberId"] == null) return 0;
                return (int)ViewState["CompanyNumberId"];
            }
            set { ViewState["CompanyNumberId"] = value; }
        }

        [System.ComponentModel.Category("BCS")]
        public string SortBy
        {
            set
            { ViewState["SortBy"] = value; }
            get
            { return (string)ViewState["SortBy"] ?? "Order"; }
        }


        #region DataBind
        public override void DataBind()
        {
            base.Items.Clear();

            DataManager dataManager = new DataManager(DefaultValues.DSN);

            dataManager.QueryCriteria.And(Biz.JoinPath.CompanyNumberOtherCarrier.Columns.CompanyNumberId, CompanyNumberId);
            
            CompanyNumberOtherCarrierCollection ocs =
                dataManager.GetCompanyNumberOtherCarrierCollection(Biz.FetchPath.CompanyNumberOtherCarrier.OtherCarrier);

            //if (string.IsNullOrEmpty(SortBy))
            //    ocs = ocs.SortBy(SortBy, OrmLib.SortDirection.Ascending);

            ocs = ocs.SortBy("Order", OrmLib.SortDirection.Ascending);

            foreach (CompanyNumberOtherCarrier oc in ocs)
            {
                ListItem li = new ListItem(oc.OtherCarrier.Code + " - " + oc.OtherCarrier.Description, oc.OtherCarrier.Id.ToString());
                base.Items.Add(li);
                if (oc.DefaultSelection)
                {
                    base.ClearSelection();
                    base.Items.FindByValue(li.Value).Selected = true;
                }

            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        }
        #endregion
    }
}