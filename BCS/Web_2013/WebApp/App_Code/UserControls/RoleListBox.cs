namespace BCS.WebApp.UserControls
{
    using System;
    using System.Data;
    using System.Configuration;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Web.UI.HtmlControls;
    using BCS.Biz;
    using BCS.WebApp.Components;

    /// <summary>
    /// List box control for roles
    /// </summary>
    public class RoleListBox : ListBox
    {
        public new ListSelectionMode SelectionMode
        {
            get
            {
                return base.SelectionMode;
            }
        }

        public new string DataTextField
        {
            get
            {
                return base.DataTextField;
            }
        }

        public new string DataValueField
        {
            get
            {
                return base.DataValueField;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            base.DataTextField = "RoleName";
            base.DataValueField = "Id";

            base.SelectionMode = ListSelectionMode.Multiple;

            base.DataSource = Security.GetAllRoles();
            base.OnInit(e);
        }

        //private RoleCollection getDataSource()
        //{
        //    if (Page.Cache["Roles"] == null)
        //    {
        //        DataManager dataManager = new DataManager(DefaultValues.DSN);
        //        RoleCollection rc = dataManager.GetRoleCollection();
        //        rc = rc.SortByRoleName(OrmLib.SortDirection.Ascending);
        //        Page.Cache["Roles"] = rc;
        //        return rc;
        //    }
        //    return Page.Cache["Roles"] as RoleCollection;
        //}
    }
}