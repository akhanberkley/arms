#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
using System.ComponentModel;
using System.Data.SqlTypes;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for AgentDropDownList
    /// </summary>
    public class ContactDropDownList : System.Web.UI.WebControls.DropDownList
    {
        #region Properties
        public int AgencyId
        {
            get
            {
                if (ViewState["AgencyId"] == null)
                    return 0;
                return (int)ViewState["AgencyId"];
            }
            set
            {
                ViewState["AgencyId"] = value;
            }
        }
        [Description("Gets or sets a date considered for retirement.")]
        [Category("BCS")]
        [DefaultValue(true)]
        public DateTime RetirementDate
        {
            set
            { ViewState["RetirementDate"] = value; }
            get
            { return (DateTime?)ViewState["RetirementDate"] ?? SqlDateTime.MinValue.Value; }
        }  
        #endregion

        #region DataBind
        public override void DataBind()
        {
            base.Items.Clear();
            if (this.AgencyId != 0)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                dataManager.QueryCriteria.Clear();
                dataManager.QueryCriteria.And(JoinPath.Contact.Columns.AgencyId, this.AgencyId, OrmLib.MatchType.Exact);

                ContactCollection cc = dataManager.GetContactCollection();
                ListItemCollection items = new ListItemCollection();
                foreach (BCS.Biz.Contact c in cc)
                {
                    items.Add(new ListItem(string.Concat(c.Name),c.Id.ToString()));
                }
                base.Items.AddRange(Common.SortListItems(items));
            }
            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        }
        #endregion
    }
}
