#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
using System.ComponentModel;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for SubmissionStatusDropDownList.
    /// </summary>
    public class SubmissionStatusDropDownList : System.Web.UI.WebControls.DropDownList
    {
        [Description("Gets or sets the company number id for which to load the statuses.")]
        [Category("BCS")]
        public int CompanyNumberId
        {
            set
            {
                ViewState["CompanyNumberId"] = value;
            }
            get
            {
                return (int?)ViewState["CompanyNumberId"] ?? 0;
            }
        }

        [Description("Gets or sets a value indicating the selection be defaulted or not.")]
        [Category("Default")]
        [DefaultValue(true)]
        public bool Default
        {
            set { ViewState["Default"] = value; }
            get { return (bool?)ViewState["Default"] ?? true; }
        }

        #region DataBind
        public override void DataBind()
        {
            base.Items.Clear();

            CompanyNumberSubmissionStatusCollection cnStatusses = GetStatuses(CompanyNumberId);
            foreach (CompanyNumberSubmissionStatus status in cnStatusses)
            {
                ListItem li = new ListItem(status.SubmissionStatus.StatusCode, status.SubmissionStatus.Id.ToString());
                base.Items.Add(li);
                if (Default)
                {
                    if (status.DefaultSelection)
                    {
                        base.ClearSelection();
                        base.Items.FindByValue(li.Value).Selected = true;
                    }
                }
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
            if(NamingContainer is IDataItemContainer)
                base.DataBind();
        }

        public static CompanyNumberSubmissionStatusCollection GetStatuses(int CompanyNumberId)
        {
            if(CompanyNumberId == 0)
                return null;
            CompanyNumberSubmissionStatusCollection cnStatusses;
            string cacheKey = string.Format(CacheKeys.CompanyNumberStatuses, CompanyNumberId);
            if (HttpContext.Current.Cache[cacheKey] == null)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);

                dataManager.QueryCriteria.And(Biz.JoinPath.CompanyNumberSubmissionStatus.Columns.CompanyNumberId, CompanyNumberId);
                cnStatusses = dataManager.GetCompanyNumberSubmissionStatusCollection(
                    Biz.FetchPath.CompanyNumberSubmissionStatus.SubmissionStatus).SortByOrder(OrmLib.SortDirection.Ascending);

                HttpContext.Current.Cache[cacheKey] = cnStatusses;
            }
            else
            {
                cnStatusses = (CompanyNumberSubmissionStatusCollection)HttpContext.Current.Cache[cacheKey];
            }

            return cnStatusses;
        }
        #endregion
    }    
}
