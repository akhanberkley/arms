#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
using System.ComponentModel;
using System.Collections.Generic;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for SubmissionStatusDropDownList.
    /// </summary>
    public class SubmissionStatusReasonDropDownList : System.Web.UI.WebControls.DropDownList
    {
        [Description("Gets or sets the company number id for which to load the status reasons.")]
        [Category("BCS")]
        public int CompanyNumberId
        {
            set
            {
                ViewState["CompanyNumberId"] = value;
            }
            get
            {
                return (int?)ViewState["CompanyNumberId"] ?? 0;
            }
        }
        [Description("Gets or sets the status for which reasons are to be loaded.")]
        [Category("BCS")]
        public int StatusId
        {
            get
            {
                if (ViewState["StatusId"] == null) return 0;
                return (int)ViewState["StatusId"];
            }
            set { ViewState["StatusId"] = value; }
        }
        [Description("Extra to the list in case not present by bindings provided.")]
        [Category("BCS")]
        public int Extra
        {
            set { ViewState["Extra"] = value; }
            get { return (int?)ViewState["Extra"] ?? 0; }
        }

        [Description("whether to load inactive agencies")]
        [Category("BCS")]
        public bool ActiveOnly
        {
            set
            { ViewState["ActiveOnly"] = value; }
            get
            { return (bool?)ViewState["ActiveOnly"] ?? false; }
        }
        #region DataBind
        public override void DataBind()
        {
            base.Items.Clear();
            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));

            if (StatusId == 0 || CompanyNumberId == 0)
            {
                if (NamingContainer is IDataItemContainer)
                    base.DataBind();
                return;
            }

            SubmissionStatusSubmissionStatusReasonCollection companyReasons = GetCompanyStatusReasons(CompanyNumberId);            
            companyReasons = companyReasons.FilterBySubmissionStatusId(StatusId);
            if (ActiveOnly)
            {
                // extract extra before filtering
                SubmissionStatusSubmissionStatusReason extraReason = companyReasons.FindBySubmissionStatusReasonId(Extra);

                companyReasons = companyReasons.FilterByActive(true);

                // add extra back to filtered
                if (extraReason != null)
                {
                    companyReasons.Add(extraReason);
                }
            }
            SubmissionStatusReasonCollection stc = new SubmissionStatusReasonCollection();
            foreach (SubmissionStatusSubmissionStatusReason var in companyReasons)
            {
                stc.Add(var.SubmissionStatusReason);
            }

            stc.SortByReasonCode(OrmLib.SortDirection.Ascending);
            foreach (SubmissionStatusReason st in stc)
            {
                base.Items.Add(new ListItem(st.ReasonCode, st.Id.ToString()));
            }
            if (NamingContainer is IDataItemContainer)
                base.DataBind();
        }
        public static SubmissionStatusSubmissionStatusReasonCollection GetCompanyStatusReasons(int companyNumberId)
        {
            string cacheKey = string.Format(CacheKeys.CompanyNumberStatusReasons, companyNumberId);
            CompanyNumberSubmissionStatusCollection companyStatusesAndReasons = null;
            if (HttpContext.Current.Cache[cacheKey] == null)
            {
                DataManager dataManager = new DataManager(DefaultValues.DSN);
                dataManager.QueryCriteria.And(JoinPath.CompanyNumberSubmissionStatus.Columns.CompanyNumberId, companyNumberId);
                companyStatusesAndReasons = dataManager.GetCompanyNumberSubmissionStatusCollection(
                    FetchPath.CompanyNumberSubmissionStatus.SubmissionStatus,
                    FetchPath.CompanyNumberSubmissionStatus.SubmissionStatus.SubmissionStatusSubmissionStatusReason,
                    FetchPath.CompanyNumberSubmissionStatus.SubmissionStatus.SubmissionStatusSubmissionStatusReason.SubmissionStatusReason);

                HttpContext.Current.Cache[cacheKey] = companyStatusesAndReasons;
            }
            else
            {
                companyStatusesAndReasons = (CompanyNumberSubmissionStatusCollection)HttpContext.Current.Cache[cacheKey];
            }

            SubmissionStatusSubmissionStatusReasonCollection reasons = new SubmissionStatusSubmissionStatusReasonCollection();
            foreach (CompanyNumberSubmissionStatus var in companyStatusesAndReasons)
            {
                SubmissionStatusSubmissionStatusReasonCollection varReasons = var.SubmissionStatus.SubmissionStatusSubmissionStatusReasons;
                foreach (SubmissionStatusSubmissionStatusReason varReason in varReasons)
                {
                    reasons.Add(varReason);
                }
            }
            return reasons;
        }
        #endregion
    }
}
