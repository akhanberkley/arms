#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for AgencyEnhancedDropDownList.
    /// </summary>
    [Obsolete("Functionality included in base agency drop down list itself.", true)]
    public class AgencyEnhancedDropDownList : AgencyDropDownList
    {
        protected override void OnInit(EventArgs e)
        {
            DisplayFormat = "{1} - {0}";
            base.OnInit(e);
        }
    }
}