using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BCS.WebApp.UserControls
{
    public class CheckBoxList4BCS : CheckBoxList
    {
        protected override void Render(HtmlTextWriter writer)
        {
            if (Items.Count == 0)
            {
                switch (RepeatLayout)
                {
                    case RepeatLayout.Flow:
                        {
                            writer.WriteBeginTag("span");
                            writer.WriteAttribute("id", this.ClientID);
                            writer.Write(">");
                            writer.WriteEndTag("span");
                            base.Render(writer);
                        } 
                        break;
                    case RepeatLayout.Table:
                        {
                            writer.WriteBeginTag("table");
                            writer.WriteAttribute("id", this.ClientID);
                            writer.Write(">");
                            writer.WriteEndTag("table");
                            base.Render(writer);
                        } 
                        break;
                    default:
                        break;
                }
               
            }
            else
            {
                base.Render(writer);
            }
        }
    } 
}
