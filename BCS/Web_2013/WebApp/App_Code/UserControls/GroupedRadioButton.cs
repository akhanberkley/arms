using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

namespace BCS.WebApp.UserControls
{
    public class GroupedRadioButton : RadioButton
    {

        protected override void Render(HtmlTextWriter writer)
        {
            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);
            base.Render(htmlWriter);

            string html = htmlWriter.InnerWriter.ToString();

            Regex regex = new Regex("name=\".*?\"", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
            html = html.Replace(regex.Match(html).Value, "name=\"" + GroupName + "\"");

            regex = new Regex("value=\".*?\"", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
            html = html.Replace(regex.Match(html).Value, "value=\"" + UniqueID + "\"");

            writer.Write(html);
        }


        protected override bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            object eventArgument = postCollection[this.GroupName];
            bool flag = false;
            if (eventArgument != null && eventArgument.Equals(this.UniqueID))
            {
                if (!Checked)
                {
                    Checked = true;
                    flag = true;
                }

                return flag;
            }

            if (Checked)
            {
                Checked = false;
            }

            return flag;
        }
    }
}
