#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for SicCodeDropDownList
    /// </summary>
    public class SicCodeDropDownList : DropDownList
    {
        /// <summary>
        /// {0} - sic code, {1} - sic description
        /// </summary>        
        [System.ComponentModel.Description("ex: code - description")]
        public string DisplayFormat
        {
            get
            {
                return ViewState["DisplayFormat"] == null ? "{0} - {1}" : (string)ViewState["DisplayFormat"];
            }
            set
            {
                ViewState["DisplayFormat"] = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            if (DataTextField == null || DataTextField.Length == 0)
                DataTextField = "CodeDescription";
            if (DataValueField == null || DataValueField.Length == 0)
                DataValueField = "Id";

            base.DataSource = getDataSource();
            base.OnInit(e);
        }

        protected override void OnDataBound(EventArgs e)
        {
            base.OnDataBound(e);
        }
        protected override void OnDataBinding(EventArgs e)
        {
            base.Items.Insert(0, new ListItem("", ""));
            this.AppendDataBoundItems = true;
            base.OnDataBinding(e);
            this.AppendDataBoundItems = false;
        }

        private SicCodeListCollection getDataSource()
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);
            dataManager.QueryCriteria.Clear();
            
            SicCodeListCollection cc = dataManager.GetSicCodeListCollection();
            foreach (Biz.SicCodeList scl in cc)
            {
                scl.CodeDescription = string.Format(DisplayFormat, scl.Code, scl.CodeDescription);
            }
            return cc.SortByCodeDescription(OrmLib.SortDirection.Ascending);
        }
    }
}