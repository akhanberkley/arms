#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for LineOfBusinessListBox
    /// </summary>
    public class LineOfBusinessListBox : System.Web.UI.WebControls.ListBox
    {
        //protected override void OnInit(EventArgs e)
        //{
        //    this.SelectionMode = ListSelectionMode.Multiple;
        //}
        //public override ListSelectionMode SelectionMode
        //{
        //    get
        //    {
        //        return base.SelectionMode;
        //    }
        //}
        #region DataBind
        public override void DataBind()
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            base.Items.Clear();

            LineOfBusinessCollection lobc = dataManager.GetLineOfBusinessCollection();
            lobc = lobc.SortByCode(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.LineOfBusiness lob in lobc)
            {
                base.Items.Add(new ListItem(lob.Code, lob.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        }
        #endregion

        #region Special DataBindings...
        public void DataBindByAgency(params int[] agencyids)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);
            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(JoinPath.LineOfBusiness.AgencyLineOfBusiness.Agency.Columns.Id, agencyids, OrmLib.MatchType.In);

            base.Items.Clear();

            LineOfBusinessCollection lobc = dataManager.GetLineOfBusinessCollection();
            lobc = lobc.SortByCode(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.LineOfBusiness lob in lobc)
            {
                base.Items.Add(new ListItem(lob.Code, lob.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        }

        public void DataBindByCompanyNumber(int companyNumberId)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);
            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.CompanyNumber.Columns.Id, companyNumberId);

            base.Items.Clear();

            LineOfBusinessCollection lobc = dataManager.GetLineOfBusinessCollection();
            lobc = lobc.SortByCode(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.LineOfBusiness lob in lobc)
            {
                base.Items.Add(new ListItem(lob.Code, lob.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        }

        public void DataBindBySubmission(int submissionId)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);
            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(JoinPath.LineOfBusiness.SubmissionLineOfBusiness.Submission.Columns.Id, submissionId);

            base.Items.Clear();

            LineOfBusinessCollection lobc = dataManager.GetLineOfBusinessCollection();
            lobc = lobc.SortByCode(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.LineOfBusiness lob in lobc)
            {
                base.Items.Add(new ListItem(lob.Code, lob.Id.ToString()));
            }

            // add blank at the beginning
            base.Items.Insert(0, new ListItem("", "0"));
        }
        #endregion
    }
}