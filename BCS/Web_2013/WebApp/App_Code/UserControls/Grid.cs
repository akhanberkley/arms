#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion


namespace BCS.WebApp.UserControls
{
    public class Grid : System.Web.UI.WebControls.GridView
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
        protected override void InitializePager(GridViewRow row, int columnSpan, PagedDataSource pagedDataSource)
        {            
            base.PagerTemplate = new BCS.WebApp.Templates.PagerTemplate();
            base.InitializePager(row, columnSpan, pagedDataSource);
        }
    }
}
