﻿#region Using
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BCS.Biz;
using BCS.WebApp.Components;
#endregion


namespace BCS.WebApp.UserControls
{
    /// <summary>
    /// Summary description for NaicsCodeDropDownList
    /// </summary>
    public class NaicsCodeDropDownList : DropDownList
    {

        /// <summary>
        /// {0} - Naics code, {1} - Naics description
        /// </summary> 
        [System.ComponentModel.Description("ex: code - description")]
        public string DisplayFormat
        {
            get
            {
                return ViewState["DisplayFormat"] == null ? "{0} - {1}" : (string)ViewState["DisplayFormat"];
            }
            set
            {
                ViewState["DisplayFormat"] = value;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            if (DataTextField == null || DataTextField.Length == 0)
                DataTextField = "CodeDescription";
            if (DataValueField == null || DataValueField.Length == 0)
                DataValueField = "Id";

            base.DataSource = getDataSource();
            base.OnInit(e);
        }

        protected override void OnDataBound(EventArgs e)
        {
            base.OnDataBound(e);
        }
        protected override void OnDataBinding(EventArgs e)
        {
            base.Items.Insert(0, new ListItem("", ""));
            this.AppendDataBoundItems = true;
            base.OnDataBinding(e);
            this.AppendDataBoundItems = false;
        }
        private NaicsCodeListCollection getDataSource()
        {
            DataManager dm = new DataManager(DefaultValues.DSN);
            dm.QueryCriteria.Clear();

            NaicsCodeListCollection ncl = dm.GetNaicsCodeListCollection();
            foreach (Biz.NaicsCodeList nc in ncl)
            {
                nc.CodeDescription = string.Format(DisplayFormat, nc.Code, nc.CodeDescription);
            }

            return ncl.SortByCodeDescription(OrmLib.SortDirection.Ascending);

        }
    }
}