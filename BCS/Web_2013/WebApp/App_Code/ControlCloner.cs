using System;
using System.Web.UI;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Collections;

namespace BCS.WebApp
{
    public class ControlCloner
    {
        public static Control Copy(Control ctrlSource, string idSuffix)
        {
            Type t = ctrlSource.GetType();

            Control ctrlDest = null;
            if (ctrlSource is UserControl)
            {
                MethodInfo mi = typeof(Page).GetMethod("LoadControl", new Type[2] { typeof(Type), typeof(object[]) });
                ctrlDest = (Control)mi.Invoke(ctrlSource.Page, new object[2] { t, null });
            }
            else
            {
                ctrlDest = (Control)t.InvokeMember("", BindingFlags.CreateInstance, null, null, null);
            }

            foreach (PropertyInfo prop in t.GetProperties())
            {
                if (prop.CanWrite)
                {
                    if (prop.Name == "ID")
                    {
                        if (string.IsNullOrEmpty(idSuffix))
                        {
                            ctrlDest.ID = ctrlSource.ID + "_Copy";
                        }
                        else
                        {
                            ctrlDest.ID = ctrlSource.ID + idSuffix;
                        }
                        if (ctrlSource is UserControl)
                        {
                            foreach (Control var in ctrlDest.Controls)
                            {
                                if (string.IsNullOrEmpty(idSuffix))
                                {
                                    // leave alone?
                                    if (!string.IsNullOrEmpty(var.ID))
                                        var.ID = ctrlSource.FindControl(var.ID).ID + "_Copy";
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(var.ID))
                                        var.ID = ctrlSource.FindControl(var.ID).ID + idSuffix;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (prop.CanRead)
                        {
                            prop.SetValue(ctrlDest, prop.GetValue(ctrlSource, null), null);
                        }
                    }
                }
                else
                {
                    // dealing with only attributes right now
                    if (prop.Name == "Attributes" || prop.Name == "InputAttributes" || prop.Name == "LabelAttributes")
                    {
                        if (ctrlDest is WebControl)
                        {
                            AttributeCollection attrs = prop.GetValue(ctrlSource, null) as AttributeCollection;
                            WebControl wc = ctrlDest as WebControl;

                            IEnumerator itor = attrs.Keys.GetEnumerator();
                            while (itor.MoveNext())
                            {
                                object o = itor.Current;
                                switch (prop.Name)
                                {
                                    case "Attributes":
                                        wc.Attributes.Add(o.ToString(), attrs[o.ToString()]);
                                        break;
                                    case "InputAttributes":
                                        (wc as CheckBox).InputAttributes.Add(o.ToString(), attrs[o.ToString()]);
                                        break;
                                    case "LabelAttributes":
                                        (wc as CheckBox).LabelAttributes.Add(o.ToString(), attrs[o.ToString()]);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        if (ctrlDest is UserControl)
                        {
                            foreach (Control var in ctrlSource.Controls)
                            {
                                if (var is WebControl)
                                {
                                    WebControl wcSrc = var as WebControl;
                                    WebControl wcDst = ctrlDest.FindControl(wcSrc.ID) as WebControl;
                                    if (null != wcDst)
                                    {
                                        AttributeCollection attrs = wcSrc.Attributes;
                                        WebControl wc = wcDst as WebControl;

                                        IEnumerator itor = attrs.Keys.GetEnumerator();
                                        while (itor.MoveNext())
                                        {
                                            object o = itor.Current;
                                            wc.Attributes.Add(o.ToString(), attrs[o.ToString()]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return ctrlDest;
        }
    }
}
