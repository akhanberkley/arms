using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using BCS.WebApp.Components;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections.Generic;
using BCS.Core.Clearance.Admin;
using aps = BCS.Core.Clearance.BizObjects.APS;
using BTS.LogFramework;

namespace BCS.WebApp.ScriptServices
{

    [ScriptService]
    [WebService(Namespace = "http://intranet.wrbts.ads.wrberkley.com/BCS/WebApp/APSHelper")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class APSHelper : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public string VerifySubmission()
        {
            string tabString = "&nbsp;&nbsp;&nbsp;&nbsp;";
            string staTag = "<{0}>";
            string staTagWAttr = "<{0} {1}>";
            string endTag = "</{0}>";
            string fulTag = "<{0}/>";

            List<string> listOfMessages = new List<string>();

            try
            {
                StringBuilder htmlStringBuilder = new StringBuilder();

                htmlStringBuilder.AppendFormat(staTagWAttr, "div", "id=\"divVerify\"");

                if (Session[SessionKeys.VerifyingSubmission] != null)
                {
                    #region Read submission and dispose
                    aps.Submission subToVerify = (aps.Submission)Session[SessionKeys.VerifyingSubmission];
                    Session.Contents.Remove(SessionKeys.VerifyingSubmission);
                    #endregion

                    BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Company.Columns.Id, subToVerify.CompanyId);
                    BCS.Biz.Company company = dm.GetCompany();

                    DateTime effDate = subToVerify.EffectiveDate;
                    if (effDate == DateTime.MinValue)
                        effDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value;

                    #region Agency
                    //{
                    //    if (subToVerify.AgencyId != 0)
                    //    {
                    //        BCS.Biz.Agency agency = DataAccess.GetAgency(subToVerify.AgencyId);
                    //        if (agency != null)
                    //        {
                    //            // verify cancelled
                    //            if (agency.EffectiveDate > effDate || agency.CancelDate < effDate)
                    //            {
                    //                listOfMessages.Add("Agency is cancelled as of submission's effective date.");
                    //            }

                    //            // verify referred
                    //            if (agency.ReferralDate < effDate)
                    //            {
                    //                listOfMessages.Add(string.Format("Agency is referred to {0} as of submission's effective date.", agency.ReferralAgencyNumber));
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        listOfMessages.Add("Missing Agency.");
                    //    }
                    //}
                    #endregion

                    #region Line Of Business, Underwriter, Analyst, REMOVED CAUSING CONFUSION THEY SAY.
                    //{
                    //    if (subToVerify.LineOfBusinessId != 0)
                    //    {
                    //        bool lobValid = IsLOBValid(dm, subToVerify.AgencyId, subToVerify.LineOfBusinessId);

                    //        if (lobValid) // if valid check for underwriter and analyst
                    //        {
                    //            #region get role ids used
                    //            int uRoleId = company.UnderwriterRoleId.IsNull ? 0 : company.UnderwriterRoleId.Value;
                    //            int aRoleId = company.AnalystRoleId.IsNull ? 0 : company.AnalystRoleId.Value;
                    //            #endregion
                    //            if (subToVerify.UnderwriterId != 0)
                    //            {
                    //                bool validUnderwriter = IsPersonValid(dm, subToVerify.AgencyId, subToVerify.LineOfBusinessId, subToVerify.UnderwriterId, uRoleId);
                    //                if (!validUnderwriter)
                    //                {
                    //                    listOfMessages.Add("Underwriter is not valid.");
                    //                }
                    //                else // check for retired
                    //                {
                    //                    BCS.Biz.Person person = DataAccess.GetPerson(subToVerify.UnderwriterId);
                    //                    if (person.RetirementDate < effDate)
                    //                    {
                    //                        listOfMessages.Add("Underwriter is retired as of submission's effective date.");
                    //                    }
                    //                }
                    //            }
                    //            else
                    //            {
                    //                listOfMessages.Add("Missing Underwriter.");
                    //            }
                    //            if (subToVerify.AnalystId != 0)
                    //            {
                    //                bool validAnalyst = IsPersonValid(dm, subToVerify.AgencyId, subToVerify.LineOfBusinessId, subToVerify.AnalystId, aRoleId);
                    //                if (!validAnalyst)
                    //                {
                    //                    listOfMessages.Add("Analyst/Assistant is not valid.");
                    //                }
                    //                else // check for retired
                    //                {
                    //                    BCS.Biz.Person person = DataAccess.GetPerson(subToVerify.AnalystId);
                    //                    if (person.RetirementDate < effDate)
                    //                    {
                    //                        listOfMessages.Add("Analyst/Assistant is retired as of submission's effective date.");
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        else // else underwriter and analyst cannot be valid
                    //        {
                    //            listOfMessages.Add("Underwriting unit id is not valid.");
                    //            listOfMessages.Add("Underwriter is not valid.");
                    //            listOfMessages.Add("Analyst/Assistant is not valid.");
                    //        }
                    //    }
                    //    else
                    //    {
                    //        listOfMessages.Add("Missing Underwriting unit.");
                    //    }
                    //}
                    #endregion

                    #region Agent
                    //{
                    //    if (subToVerify.AgentId != 0)
                    //    {
                    //        BCS.Biz.Agent agent = DataAccess.GetAgent(subToVerify.AgentId);
                    //        if (agent.AgencyId != subToVerify.AgencyId)
                    //        {
                    //            listOfMessages.Add("Invalid Agent. Does not belong to the agency.");
                    //        }
                    //        else
                    //        {
                    //            // verify cancelled
                    //            if (agent.EffectiveDate > effDate || agent.CancelDate < effDate)
                    //            {
                    //                listOfMessages.Add("Agent is cancelled as of submission's effective date.");
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (company.SupportsAgentUnspecifiedReasons.IsTrue)
                    //        {
                    //            if (subToVerify.AgentUnspecifiedReasonList == null || subToVerify.AgentUnspecifiedReasonList.Length == 0)
                    //            {
                    //                listOfMessages.Add("Agent unspecified reason(s) not enetered. ");
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion

                    #region Contact
                    //{
                    //    if (subToVerify.ContactId != 0)
                    //    {
                    //        BCS.Biz.Contact contact = DataAccess.GetContact(subToVerify.ContactId);
                    //        if (contact.AgencyId != subToVerify.AgencyId)
                    //        {
                    //            listOfMessages.Add("Invalid Contact. Does not belong to the agency.");
                    //        }
                    //    }
                    //}
                    #endregion

                    #region Status Reason
                    if (subToVerify.SubmissionStatusReasonId != 0)
                    {
                        dm.QueryCriteria.Clear();
                        dm.QueryCriteria.And(BCS.Biz.JoinPath.SubmissionStatusSubmissionStatusReason.Columns.SubmissionStatusReasonId, subToVerify.SubmissionStatusReasonId);
                        dm.QueryCriteria.And(BCS.Biz.JoinPath.SubmissionStatusSubmissionStatusReason.Columns.SubmissionStatusId, subToVerify.SubmissionStatusId);

                        // somehow reason id is not having affect on the Biz query, so get collection and filter
                        BCS.Biz.SubmissionStatusSubmissionStatusReasonCollection reasons = dm.GetSubmissionStatusSubmissionStatusReasonCollection();
                        reasons = reasons.FilterBySubmissionStatusId(
                            subToVerify.SubmissionStatusId).FilterBySubmissionStatusReasonId(
                            subToVerify.SubmissionStatusReasonId);

                        BCS.Biz.SubmissionStatusSubmissionStatusReason aReason = null;
                        if (reasons.Count == 1)
                            aReason = reasons[0];
                        if (null == aReason)
                        {
                            listOfMessages.Add(string.Format("Invalid status reason {0}.", subToVerify.SubmissionStatusReason));
                        }
                        else
                        {
                            if (!aReason.Active.Value)
                            {
                                listOfMessages.Add(string.Format("Inactive status reason {0}.", subToVerify.SubmissionStatusReason));
                            }
                        }
                    }
                    #endregion

                    #region Policy Number
                    if (company.RequiresPolicyNumber.IsTrue)
                    {
                        if (string.IsNullOrEmpty(subToVerify.PolicyNumber))
                        {
                            listOfMessages.Add("Missing policy number.");
                        }
                    }
                    #endregion

                    #region Company Codes
                    {
                        if (null != subToVerify.SubmissionCodeTypess)
                        {
                            List<int> submissionCodeTypeIds = new List<int>();
                            foreach (aps.SubmissionCodeType var in subToVerify.SubmissionCodeTypess)
                            {
                                submissionCodeTypeIds.Add(var.CodeId);
                            }

                            dm.QueryCriteria.Clear();
                            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCode.Columns.CompanyNumberId, subToVerify.CompanyNumberId);
                            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCode.Columns.Id, submissionCodeTypeIds, OrmLib.MatchType.In);
                            BCS.Biz.CompanyNumberCodeCollection codes = dm.GetCompanyNumberCodeCollection();

                            if (subToVerify.SubmissionCodeTypess != null)
                            {
                                foreach (aps.SubmissionCodeType var in subToVerify.SubmissionCodeTypess)
                                {
                                    BCS.Biz.CompanyNumberCode aCode = codes.FindById(var.CodeId);
                                    if (aCode == null)
                                    {
                                        listOfMessages.Add(string.Format("The Code [{0}-{1}] is invalid.", var.CodeValue, var.CodeDescription));
                                    }
                                    else
                                    {
                                        // verify referred
                                        if (aCode.ExpirationDate < effDate)
                                        {
                                            listOfMessages.Add(string.Format("The Code [{0}-{1}] is expired as of submission's effective date.",
                                                var.CodeValue, var.CodeDescription));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region LOB Children
                    {
                        if (company.SupportsMultipleLineOfBusinesses.IsTrue)
                        {
                            List<int> submissionLOBGridIds = new List<int>();
                            foreach (aps.SubmissionLineOfBusinessChild var in subToVerify.LineOfBusinessChildList)
                            {
                                submissionLOBGridIds.Add(var.CompanyNumberLOBGridId);
                            }

                            if (subToVerify.LineOfBusinessChildList != null && subToVerify.LineOfBusinessChildList.Length > 0)
                            {
                                #region fetch lobs
                                dm.QueryCriteria.Clear();
                                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberLOBGrid.Columns.CompanyNumberId, subToVerify.CompanyNumberId);
                                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberLOBGrid.Columns.Id, submissionLOBGridIds, OrmLib.MatchType.In);
                                BCS.Biz.CompanyNumberLOBGridCollection lobs = dm.GetCompanyNumberLOBGridCollection();
                                #endregion

                                #region fetch reasons
                                dm.QueryCriteria.Clear();
                                BCS.Biz.SubmissionStatusSubmissionStatusReasonCollection sssrs = dm.GetSubmissionStatusSubmissionStatusReasonCollection(
                                    BCS.Biz.FetchPath.SubmissionStatusSubmissionStatusReason.SubmissionStatus,
                                    BCS.Biz.FetchPath.SubmissionStatusSubmissionStatusReason.SubmissionStatusReason);
                                #endregion

                                foreach (aps.SubmissionLineOfBusinessChild lobChild in subToVerify.LineOfBusinessChildList)
                                {
                                    #region LOB Grid


                                    BCS.Biz.CompanyNumberLOBGrid aLOB = lobs.FindById(lobChild.CompanyNumberLOBGridId);
                                    if (aLOB == null)
                                    {
                                        listOfMessages.Add(string.Format("The Line Of Business [{0}-{1}] is invalid.",
                                            lobChild.CompanyNumberLOBGrid == null ? "??" : lobChild.CompanyNumberLOBGrid.Code,
                                            lobChild.CompanyNumberLOBGrid == null ? "??" : lobChild.CompanyNumberLOBGrid.Description));
                                    }
                                    else
                                    {
                                        // verify referred
                                        if (aLOB.Active.IsFalse)
                                        {
                                            listOfMessages.Add(string.Format("The Line Of Business [{0}-{1}] is in-active.",
                                                lobChild.CompanyNumberLOBGrid.Code, lobChild.CompanyNumberLOBGrid.Description));
                                        }
                                    }
                                    #endregion

                                    #region Status Reason
                                    //foreach (KeyValuePair<int, int> varStatusStatusReason in submissionLOBGridStatusReasonIds)
                                    {
                                        BCS.Biz.SubmissionStatusSubmissionStatusReasonCollection subSssrs = sssrs.FilterBySubmissionStatusId(lobChild.SubmissionStatusId);

                                        if (subSssrs.Count > 0) // check only if a status has reasons
                                        {
                                            subSssrs = subSssrs.FilterBySubmissionStatusReasonId(lobChild.SubmissionStatusReasonId);
                                            if (subSssrs.Count > 0)
                                            {
                                                BCS.Biz.SubmissionStatusSubmissionStatusReason aSssrs = subSssrs[0]; // there should be only one
                                                if (aSssrs.Active.IsFalse)
                                                {
                                                    listOfMessages.Add(string.Format("The Status Reason [{0}-{1}] is in-active.",
                                                        aSssrs.SubmissionStatus == null ? "??" : aSssrs.SubmissionStatus.StatusCode,
                                                        aSssrs.SubmissionStatusReason == null ? "??" : aSssrs.SubmissionStatusReason.ReasonCode));
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                listOfMessages.Add("Missing LOB.");
                            }



                        }
                    }
                    #endregion

                    #region Client
                    {
                        if (subToVerify.ClientCoreAddressIds.Length == 0)
                        {
                            listOfMessages.Add("Missing client addresses.");
                        }
                        // TODO: uncomment after BPM, etc adapts
                        //if (subToVerify.ClientCoreNameIds.Length == 0)
                        //{
                        //    listOfMessages.Add("Missing client names.");
                        //}
                    }
                    #endregion

                    #region Build return markup
                    if (subToVerify.Id == 0)
                        return string.Empty;


                    if (listOfMessages.Count > 0)
                    {
                        htmlStringBuilder.AppendFormat(staTagWAttr, "ul",
                            "style=\"list-style-type: circle;\"");

                        foreach (string var in listOfMessages)
                        {
                            htmlStringBuilder.AppendFormat(staTag, "li");
                            htmlStringBuilder.Append(var);
                            htmlStringBuilder.AppendFormat(endTag, "li");
                        }

                        htmlStringBuilder.AppendFormat(endTag, "ul");
                    }
                    #endregion
                }
                htmlStringBuilder.AppendFormat(endTag, "div");
                return htmlStringBuilder.ToString();
            }
            catch (Exception ex)
            {
                string message = "Unable to Verify Submission.";

                StringBuilder htmlStringBuilder = new StringBuilder();
                htmlStringBuilder.AppendFormat(staTagWAttr, "div", "id=\"divVerify\"");
                htmlStringBuilder.Append(message);
                htmlStringBuilder.AppendFormat(endTag, "div");

                LogCentral.Current.LogFatal(message, ex);
                //throw ex;
                return htmlStringBuilder.ToString();
            }

        }

        [ScriptMethod]
        [WebMethod(EnableSession = true)]
        public string GetAgencyMiniDetail(string contextKey)
        {
            BCS.APS.agency agency = new BCS.APS.DataSources().GetAgency(Common.GetSafeIntFromSession(SessionKeys.CompanyId), Convert.ToInt64(contextKey));
            string tabString = "&nbsp;&nbsp;&nbsp;&nbsp;";
            string staTag = "<{0}>";
            string endTag = "</{0}>";
            string fulTag = "<{0}/>";
            StringBuilder htmlStringBuilder = new StringBuilder();
            if (null != agency)
            {
                htmlStringBuilder.AppendFormat(staTag, "div");
                htmlStringBuilder.AppendFormat(staTag, "span");
                htmlStringBuilder.AppendFormat("{0}, {1}", agency.ShortName, agency.AgencyCode);
                htmlStringBuilder.AppendFormat(endTag, "span");
                htmlStringBuilder.AppendFormat(fulTag, "br");
                htmlStringBuilder.AppendFormat(staTag, "span");
                htmlStringBuilder.AppendFormat("{0}, {1}", agency.HomeCity, agency.HomeState.Description);
                htmlStringBuilder.AppendFormat(endTag, "span");
                htmlStringBuilder.AppendFormat(endTag, "div");
            }
            else
            {
                htmlStringBuilder.AppendFormat(staTag, "div");
                htmlStringBuilder.AppendFormat(staTag, "span");
                htmlStringBuilder.AppendFormat("unable to retrieve agency information.");
                htmlStringBuilder.AppendFormat(endTag, "span");
                htmlStringBuilder.AppendFormat(endTag, "div");
            }

            return htmlStringBuilder.ToString();
        }
        [ScriptMethod]
        [WebMethod(EnableSession = true)]
        public string GetUWMiniDetail(string contextKey)
        {
            BCS.APS.underwritingUnit uw = new BCS.APS.DataSources().GetUnderwritingUnit(Common.GetSafeIntFromSession(SessionKeys.CompanyId), Convert.ToInt64(contextKey));
            string tabString = "&nbsp;&nbsp;&nbsp;&nbsp;";
            string staTag = "<{0}>";
            string endTag = "</{0}>";
            string fulTag = "<{0}/>";
            StringBuilder htmlStringBuilder = new StringBuilder();
            if (null != uw)
            {
                htmlStringBuilder.AppendFormat(staTag, "div");
                htmlStringBuilder.AppendFormat(staTag, "span");
                htmlStringBuilder.AppendFormat("{0}", uw.Description);
                htmlStringBuilder.AppendFormat(endTag, "span");
                htmlStringBuilder.AppendFormat(endTag, "div");
            }
            else
            {
                htmlStringBuilder.AppendFormat(staTag, "div");
                htmlStringBuilder.AppendFormat(staTag, "span");
                htmlStringBuilder.AppendFormat("unable to retrieve underwriting unit information.");
                htmlStringBuilder.AppendFormat(endTag, "span");
                htmlStringBuilder.AppendFormat(endTag, "div");
            }

            return htmlStringBuilder.ToString();
        }

        /// <summary>
        /// UW person refers to underwriter, analyst or technician
        /// </summary>
        /// <param name="contextKey"></param>
        /// <returns></returns>
        [ScriptMethod]
        [WebMethod(EnableSession = true)]
        public string GetUWPersonMiniDetail(string contextKey)
        {
            BCS.APS.PersonTypes pType = (BCS.APS.PersonTypes)Enum.Parse(typeof(BCS.APS.PersonTypes), contextKey.Split("|".ToCharArray())[0]);
            BCS.APS.Objects.Person person = new BCS.APS.DataSources().GetPerson(Common.GetSafeIntFromSession(SessionKeys.CompanyId), contextKey, pType);
            string tabString = "&nbsp;&nbsp;&nbsp;&nbsp;";
            string staTag = "<{0}>";
            string endTag = "</{0}>";
            string fulTag = "<{0}/>";
            StringBuilder htmlStringBuilder = new StringBuilder();
            if (null != person)
            {
                htmlStringBuilder.AppendFormat(staTag, "div");
                htmlStringBuilder.AppendFormat(staTag, "span");
                htmlStringBuilder.AppendFormat("{0}", person.FirstName, person.LastName);
                htmlStringBuilder.AppendFormat(endTag, "span");
                htmlStringBuilder.AppendFormat(endTag, "div");
            }
            else
            {
                htmlStringBuilder.AppendFormat(staTag, "div");
                htmlStringBuilder.AppendFormat(staTag, "span");
                htmlStringBuilder.AppendFormat("unable to retrieve underwriting unit information.");
                htmlStringBuilder.AppendFormat(endTag, "span");
                htmlStringBuilder.AppendFormat(endTag, "div");
            }

            return htmlStringBuilder.ToString();
        }


        #region Agency auto completion
        [WebMethod(EnableSession = true), ScriptMethodAttribute()]
        public string[] GetCompletionList(string prefixText, int count, string contextKey)
        {
            Regex reg = new Regex(string.Format("^{0}.*", prefixText), RegexOptions.IgnoreCase);

            string suggestionKey = contextKey.Split("|".ToCharArray())[0];
            string dateTimeKey = contextKey.Split("|".ToCharArray())[1];

            BCS.APS.DataSources dataSource = new BCS.APS.DataSources();
            DataView dView = dataSource.GetAgenciesViewPaged(Common.GetSafeIntFromSession(SessionKeys.CompanyId), string.Empty, prefixText, string.Empty, string.Empty,
                Convert.ToDateTime(dateTimeKey), 0, 10);

            return AsStringArray("{0} ({1}) | {2}, {3}", dView);
        }
        private string[] AsStringArray(string displayFormat, DataView collection)
        {
            List<string> items = new List<string>(collection.Count);

            for (int i = 0; i < collection.Count; i++)
            {
                items.Add(
                    AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(
                    string.Format(displayFormat, collection[i]["shortName"], collection[i]["agencyCode"], collection[i]["homeCity"], collection[i]["homeState"]),
                    collection[i]["id"].ToString()));
            }
            return items.ToArray();
        }
        #endregion
    }


}