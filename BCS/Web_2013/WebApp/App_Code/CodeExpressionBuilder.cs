using System.CodeDom;
using System.Web.Compilation;
using System.Web.UI;
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace BCS.WebApp.Expressions
{
    /// <summary>
    /// Summary description for CodeExpressionBuilder
    /// </summary>
    [ExpressionPrefix("Code")]
    public class CodeExpressionBuilder : ExpressionBuilder
    {
        public override CodeExpression GetCodeExpression(BoundPropertyEntry entry, object parsedData, ExpressionBuilderContext context)
        { return new CodeSnippetExpression(entry.Expression); }
    }
}