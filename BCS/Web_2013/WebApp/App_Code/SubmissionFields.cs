using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;


namespace BCS.WebApp
{
    public class SubmissionField
    {
        string controlId;
        List<FieldPair> associatedControlIds;

        public string ControlId
        {
            get { return controlId; }
            set { controlId = value; }
        }

        public List<FieldPair> AssociatedControlIds
        {
            get { return associatedControlIds; }
            set { associatedControlIds = value; }
        }
    }

    public class FieldPair
    {
        string id;
        string type;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Type
        {
            get { return type; }
            set { type = value; }
        }
    } 
}
