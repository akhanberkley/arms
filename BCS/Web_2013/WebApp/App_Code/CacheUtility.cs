using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using BCS.WebApp.Components;

using BCS.Biz;
using BCS.Core;
using BCS.Core.Clearance;
using BTS.LogFramework;
using CacheEngine = BTS.CacheEngine.V2.CacheEngine;

/// <summary>
/// Summary description for CacheUtility
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class CacheUtility : System.Web.Services.WebService
{

    public CacheUtility()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


        //private static XmlDocument mappingDoc;
       

        [WebMethod(Description = "Processes Cache additions & deletions")]
        public void ProcessAgencyCache(string companyNumber, CacheChangeType changeType)
        {
            try
            {
                //Getting Company Number
                BCS.Biz.DataManager dm = CommonGetDataManager();
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                BCS.Biz.CompanyNumber companyNo = dm.GetCompanyNumber();

                HttpContext curr = HttpContext.Current;
                string AgencyCacheKey = string.Format("Agencies4CompanyNumber{0}", companyNo.Id);

                //Getting agencies
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(JoinPath.Agency.Columns.CompanyNumberId, companyNo.Id);
                AgencyCollection agencies = dm.GetAgencyCollection(BCS.Biz.FetchPath.Agency.AgencyLicensedState);

                //Creating Cache Engine and clearing Cache
                CacheEngine cacheEngine = new CacheEngine(AgencyCacheKey, DefaultValues.DefaultObjectCacheTimeInMinutes);
                if (cacheEngine.CachedItemExist(AgencyCacheKey))
                {
                    cacheEngine.RemoveItemFromCache(AgencyCacheKey);
                }
                //Adding to Cache
                cacheEngine.AddItemToCache(AgencyCacheKey, dm.DataSet);
            }
            catch (Exception ex)
            {
                string message = "Exception adding to Cache.";
                //ExceptionManager.Notify(message, ExceptionManager.GetFullDetailsString(ex), MailPriority.High);
            }
        }

        [WebMethod(Description = "Processes Cache additions & deletions")]
        public void ProcessUWCache(string companyNumber)
        {
            try
            {
                //Getting Company Number
                BCS.Biz.DataManager dm = CommonGetDataManager();
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                BCS.Biz.CompanyNumber companyNo = dm.GetCompanyNumber();
                CacheEngine cacheEngine = null;
                string cacheKey = string.Empty;
                
                //dm.QueryCriteria.Clear();
                //dm.QueryCriteria.And(BCS.Biz.JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.Columns.CompanyNumberId, companyNo.Id);
                //BCS.Biz.LineOfBusinessCollection lobs = dm.GetLineOfBusinessCollection();

                //if (lobs != null)
                //{
                //    foreach (BCS.Biz.LineOfBusiness lob in lobs)
                //    {
                //        cacheKey = string.Format("{0}|{1}|{2}", companyNo.Id,agencyNumber,lob.Code);
                //        cacheEngine = new CacheEngine(cacheKey, DefaultValues.DefaultObjectCacheTimeInMinutes);

                //        if (cacheEngine.CachedItemExist(cacheKey))
                //        {
                //            cacheEngine.RemoveItemFromCache(cacheKey);
                //        }
                //    }
                //}

                //Creating Cache Engine and clearing Cache
                string UnderwritersCacheKey = string.Format("{0}|Underwriters", companyNo.Id);
                cacheEngine = new CacheEngine(UnderwritersCacheKey, DefaultValues.DefaultObjectCacheTimeInMinutes);
               
                if (cacheEngine.CachedItemExist(UnderwritersCacheKey))
                {
                    cacheEngine.RemoveItemFromCache(UnderwritersCacheKey);
                }
            }
            catch (Exception ex)
            {
                string message = "Exception adding to Cache.";
                //ExceptionManager.Notify(message, ExceptionManager.GetFullDetailsString(ex), MailPriority.High);
            }
        }

        private static BCS.Biz.DataManager CommonGetDataManager()
        {
            return new BCS.Biz.DataManager(DefaultValues.DSN);
        }

        public enum CacheChangeType
        {
            Insert,
            Remove
        }

}

