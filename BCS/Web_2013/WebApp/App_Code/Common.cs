using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Collections.Generic;
using BCS.WebApp.Components;
using System.Collections;


/// <summary>
/// Summary description for Common
/// </summary>
public class Common
{
    public enum Mode
    {
        Search = 1,
        Add,
        Modify
    }

    public const string DateFormat = "MM/dd/yyyy";

    private static ClientProxy.Client _clientProxy = new BCS.WebApp.Services.WebServicesHelpers.ClientHelper();
    private static SubmissionProxy.Submission _submissionProxy = new BCS.WebApp.Services.WebServicesHelpers.SubmissionHelper();
    private static LookupProxy.Lookup _lookupProxy = new BCS.WebApp.Services.WebServicesHelpers.LookupHelper();
    private static AdvClientSearch.AdvClientSearchWebServicePublicService _advClientSearch = new BCS.WebApp.Services.WebServicesHelpers.AdvClientSearchHelper();

    public static ClientProxy.Client GetClientProxy()
    {
        //return new BCS.WebApp.Services.WebServicesHelpers.ClientHelper();
        return _clientProxy;
    }

    public static SubmissionProxy.Submission GetSubmissionProxy()
    {
        //return new BCS.WebApp.Services.WebServicesHelpers.SubmissionHelper();
        return _submissionProxy;
    }

    public static LookupProxy.Lookup GetLookupProxy()
    {
        //return new BCS.WebApp.Services.WebServicesHelpers.LookupHelper();
        return _lookupProxy;
    }

    public static AdvClientSearch.AdvClientSearchWebServicePublicService GetAdvClientSearch()
    {
        return _advClientSearch;
    }

    public static DataSet GenerateDataSet(string xml)
    {
        DataSet ds = new DataSet();
        System.IO.StringReader sr = new System.IO.StringReader(xml);
        ds.ReadXml(sr);
        return ds;
    }

    public static string GetImageUrl(string fileName)
    {
        return string.Concat(DefaultValues.ImagePath, fileName);
    }

    public static DataTable GenerateDataTable(string companyNumber, string tableName, string xml, params Pair[] optionalFields)
    {
        DataSet ds = new DataSet();
        System.IO.StringReader sr = new System.IO.StringReader(xml);
        ds.ReadXml(sr);

        BCS.Biz.SubmissionDisplayGridCollection all = ConfigValues.GetSubmissionGridDisplay(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
        BCS.Biz.SubmissionDisplayGridCollection sdgs = all.FilterBySupportField(null, OrmLib.CompareType.Not);


        //string[] cfs = customField1.Split("|".ToCharArray());
        foreach (BCS.Biz.SubmissionDisplayGrid acf in sdgs)
        {
            string[] cf = acf.SupportField.Split(",".ToCharArray());

            if (ds.Tables[tableName] != null)
            {
                if (!ds.Tables[tableName].Columns.Contains(cf[0]))
                {
                    foreach (System.Data.DataTable dt in ds.Tables)
                    {
                        if (dt.Columns.Contains(cf[0]))
                        {
                            System.Type type;
                            if (acf.AttributeDataType.DataTypeName.Equals("Date", StringComparison.CurrentCultureIgnoreCase))
                            { type = typeof(DateTime); }
                            else if (acf.AttributeDataType.DataTypeName.Equals("BIT", StringComparison.CurrentCultureIgnoreCase))
                            {
                                type = typeof(Boolean);
                            }
                            else if (acf.AttributeDataType.DataTypeName.Equals("MONEY", StringComparison.CurrentCultureIgnoreCase))
                            {
                                type = typeof(Decimal);
                            }
                            else
                                type = typeof(string);

                            ds.Tables[tableName].Columns.Add(new DataColumn(acf.FieldName, type));

                            StringCollection sc = FetchRelations(ds, tableName, dt.TableName);

                            for (int rowi = 0; rowi < ds.Tables[tableName].Rows.Count; rowi++)
                            {
                                DataRow[] crow = ds.Tables[tableName].Rows[rowi].GetChildRows(ds.Relations[sc[0]]);
                                if (crow.Length == 0)
                                { continue; }
                                if (sc.Count == 2)
                                {
                                    DataRow[] ccrows = crow[0].GetChildRows(ds.Relations[sc[1]]);
                                    foreach (DataRow var in ccrows)
                                    {
                                        if (var[cf[0]].ToString() == acf.FieldName)
                                        {
                                            if (null == var[cf[1]] || 0 == var[cf[1]].ToString().Length)
                                            {
                                                object nValue;
                                                if (!type.IsValueType)
                                                {
                                                    nValue = null;
                                                }
                                                else
                                                {
                                                    nValue = Activator.CreateInstance(type);
                                                }

                                                ds.Tables[tableName].Rows[rowi][acf.FieldName] = nValue;
                                            }
                                            else
                                            {
                                                ds.Tables[tableName].Rows[rowi][acf.FieldName] = var[cf[1]];
                                            }
                                            break;
                                        }
                                    }
                                }
                                if (sc.Count == 1)
                                {
                                    ds.Tables[tableName].Rows[rowi][acf.FieldName] = crow[0][cf[0]];
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        foreach (BCS.Biz.SubmissionDisplayGrid acf in sdgs)
        {
            if (ds.Tables[tableName] != null)
                if (!ds.Tables[tableName].Columns.Contains(acf.FieldName))
                    ds.Tables[tableName].Columns.Add(new DataColumn(acf.FieldName, typeof(string)));
        }
        
        // generate columns for any optional elements.
        foreach (Pair acf in optionalFields)
        {
            string[] cf = acf.Second.ToString().Split(",".ToCharArray());

            if (ds.Tables[tableName] != null)
            {
                if (!ds.Tables[tableName].Columns.Contains(cf[0]))
                {
                    foreach (System.Data.DataTable dt in ds.Tables)
                    {
                        if (dt.Columns.Contains(cf[0]))
                        {
                            ds.Tables[tableName].Columns.Add(new DataColumn(acf.First.ToString(), dt.Columns[cf[0]].DataType));

                            StringCollection sc = FetchRelations(ds, tableName, dt.TableName);

                            for (int rowi = 0; rowi < ds.Tables[tableName].Rows.Count; rowi++)
                            {
                                DataRow[] crow = ds.Tables[tableName].Rows[rowi].GetChildRows(ds.Relations[sc[0]]);
                                if (crow.Length == 0)
                                { continue; }
                                if (sc.Count == 2)
                                {
                                    DataRow[] ccrows = crow[0].GetChildRows(ds.Relations[sc[1]]);
                                    foreach (DataRow var in ccrows)
                                    {
                                        if (var[cf[0]].ToString() == acf.First.ToString())
                                        {
                                            ds.Tables[tableName].Rows[rowi][acf.First.ToString()] = var[cf[1]];
                                            break;
                                        }
                                    }
                                }
                                if (sc.Count == 1)
                                {
                                    ds.Tables[tableName].Rows[rowi][acf.First.ToString()] = crow[0][cf[0]];
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        BCS.Biz.SubmissionDisplayGridCollection sortedSdgs = all.FilterBySortOrder(0, OrmLib.CompareType.Not);
        sortedSdgs = sortedSdgs.SortBySortOrder(OrmLib.SortDirection.Ascending);
        List<string> expressions = new List<string>();
        foreach (BCS.Biz.SubmissionDisplayGrid var in sortedSdgs)
        {
            expressions.Add(string.Format("{0} {1}", var.FieldName, var.SortDirection ? "DESC" : "ASC"));
        }
        string[] arrStr = new string[expressions.Count];
        expressions.CopyTo(arrStr);
        string finalExpression = string.Join(",", arrStr);

        if (string.IsNullOrEmpty(finalExpression))
        {
            finalExpression = "submission_number DESC, submission_number_sequence DESC";
        }

        if (ds.Tables[tableName] == null)
            return null;
        DataTable ndt = ds.Tables[tableName].Clone();
        // set the datatypes who don't have support field
        foreach (BCS.Biz.SubmissionDisplayGrid acf in all)
        {
            if (sdgs.FindById(acf.Id) == null)
            {
                // there should a col 
                System.Type type;
                if (acf.AttributeDataType.DataTypeName.Equals("Date", StringComparison.CurrentCultureIgnoreCase))
                { type = typeof(DateTime); }
                else if (acf.AttributeDataType.DataTypeName.Equals("BIT", StringComparison.CurrentCultureIgnoreCase))
                {
                    type = typeof(Boolean);
                }
                else if (acf.AttributeDataType.DataTypeName.Equals("MONEY", StringComparison.CurrentCultureIgnoreCase))
                {
                    type = typeof(Decimal);
                }
                else
                    type = typeof(string);

                if(ndt.Columns[acf.FieldName] != null)
                    ndt.Columns[acf.FieldName].DataType = type;
            }
        }

        ndt.Columns["submission_number"].DataType = typeof(long);
        ndt.Columns["submission_number_sequence"].DataType = typeof(int);

        foreach (DataRow dr in ds.Tables[tableName].Rows)
            ndt.ImportRow(dr);
        ndt.AcceptChanges();

        ndt.DefaultView.Sort = finalExpression;

        return ndt;
    }

    private static StringCollection FetchRelations(DataSet ds, string baseTableName, string endTableName)
    {
        StringCollection sc = new System.Collections.Specialized.StringCollection();

        foreach (DataRelation var in ds.Tables[baseTableName].ChildRelations)
        {
            if (var.ChildTable.TableName == endTableName)
            {
                sc.Add(string.Format("{0}_{1}", baseTableName, endTableName));
            }
            else
            {
                if (var.ChildTable.ChildRelations.Count > 0)
                {

                    StringCollection temp = FetchRelations(ds, var.ChildTable.TableName, endTableName);
                    if (temp.Count > 0)
                        sc.Add(string.Format("{0}_{1}", baseTableName, var.ChildTable));
                    foreach (string s in temp)
                    {
                        sc.Add(s);
                    }

                }
                else
                    continue;
            }
        }

        return sc;
    }
    public static DataTable GenerateDataTableAPS(string companyNumber, string tableName, string xml, params Pair[] optionalFields)
    {
        // TODO: 
        DataSet ds = new DataSet();
        System.IO.StringReader sr = new System.IO.StringReader(xml);
        ds.ReadXml(sr);

        BCS.Biz.SubmissionDisplayGridCollection all = ConfigValues.GetSubmissionGridDisplay(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));

        BCS.Biz.SubmissionDisplayGridCollection sdgs = all.FilterBySupportField(null, OrmLib.CompareType.Not);

        foreach (BCS.Biz.SubmissionDisplayGrid acf in sdgs)
        {
            string[] cf = acf.SupportField.Split(",".ToCharArray());

            if (ds.Tables[tableName] != null)
            {
                if (!ds.Tables[tableName].Columns.Contains(cf[0]))
                {
                    foreach (System.Data.DataTable dt in ds.Tables)
                    {
                        if (dt.Columns.Contains(cf[0]))
                        {
                            System.Type type;
                            if (acf.AttributeDataType.DataTypeName.Equals("Date", StringComparison.CurrentCultureIgnoreCase))
                            { type = typeof(DateTime); }
                            else if (acf.AttributeDataType.DataTypeName.Equals("BIT", StringComparison.CurrentCultureIgnoreCase))
                            {
                                type = typeof(Boolean);
                            }
                            else if (acf.AttributeDataType.DataTypeName.Equals("MONEY", StringComparison.CurrentCultureIgnoreCase))
                            {
                                type = typeof(Decimal);
                            }
                            else
                                type = typeof(string);

                            ds.Tables[tableName].Columns.Add(new DataColumn(acf.FieldName, type));

                            StringCollection sc = FetchRelations(ds, tableName, dt.TableName);

                            for (int rowi = 0; rowi < ds.Tables[tableName].Rows.Count; rowi++)
                            {
                                DataRow[] crow = ds.Tables[tableName].Rows[rowi].GetChildRows(ds.Relations[sc[0]]);
                                if (crow.Length == 0)
                                { continue; }
                                if (sc.Count == 2)
                                {
                                    DataRow[] ccrows = crow[0].GetChildRows(ds.Relations[sc[1]]);
                                    foreach (DataRow var in ccrows)
                                    {
                                        if (var[cf[0]].ToString() == acf.FieldName)
                                        {
                                            if (null == var[cf[1]] || 0 == var[cf[1]].ToString().Length)
                                            {
                                                object nValue;
                                                if (!type.IsValueType)
                                                {
                                                    nValue = null;
                                                }
                                                else
                                                {
                                                    nValue = Activator.CreateInstance(type);
                                                }

                                                ds.Tables[tableName].Rows[rowi][acf.FieldName] = nValue;
                                            }
                                            else
                                            {
                                                ds.Tables[tableName].Rows[rowi][acf.FieldName] = var[cf[1]];
                                            }
                                            break;
                                        }
                                    }
                                }
                                if (sc.Count == 1)
                                {
                                    ds.Tables[tableName].Rows[rowi][acf.FieldName] = crow[0][cf[0]];
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        foreach (BCS.Biz.SubmissionDisplayGrid acf in sdgs)
        {
            if (ds.Tables[tableName] != null)
                if (!ds.Tables[tableName].Columns.Contains(acf.FieldName))
                    ds.Tables[tableName].Columns.Add(new DataColumn(acf.FieldName, typeof(string)));
        }


        // generate columns for any optional elements.
        foreach (Pair acf in optionalFields)
        {
            string[] cf = acf.Second.ToString().Split(",".ToCharArray());

            if (ds.Tables[tableName] != null)
            {
                if (!ds.Tables[tableName].Columns.Contains(cf[0]))
                {
                    foreach (System.Data.DataTable dt in ds.Tables)
                    {
                        if (dt.Columns.Contains(cf[0]))
                        {
                            ds.Tables[tableName].Columns.Add(new DataColumn(acf.First.ToString(), dt.Columns[cf[0]].DataType));

                            StringCollection sc = FetchRelations(ds, tableName, dt.TableName);

                            for (int rowi = 0; rowi < ds.Tables[tableName].Rows.Count; rowi++)
                            {
                                DataRow[] crow = ds.Tables[tableName].Rows[rowi].GetChildRows(ds.Relations[sc[0]]);
                                if (crow.Length == 0)
                                { continue; }
                                if (sc.Count == 2)
                                {
                                    DataRow[] ccrows = crow[0].GetChildRows(ds.Relations[sc[1]]);
                                    foreach (DataRow var in ccrows)
                                    {
                                        if (var[cf[0]].ToString() == acf.First.ToString())
                                        {
                                            ds.Tables[tableName].Rows[rowi][acf.First.ToString()] = var[cf[1]];
                                            break;
                                        }
                                    }
                                }
                                if (sc.Count == 1)
                                {
                                    ds.Tables[tableName].Rows[rowi][acf.First.ToString()] = crow[0][cf[0]];
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        BCS.Biz.SubmissionDisplayGridCollection sortedSdgs = all.FilterBySortOrder(0, OrmLib.CompareType.Not);
        sortedSdgs = sortedSdgs.SortBySortOrder(OrmLib.SortDirection.Ascending);
        List<string> expressions = new List<string>();
        foreach (BCS.Biz.SubmissionDisplayGrid var in sortedSdgs)
        {
            expressions.Add(string.Format("{0} {1}", var.FieldName, var.SortDirection ? "DESC" : "ASC"));
        }
        string[] arrStr = new string[expressions.Count];
        expressions.CopyTo(arrStr);
        string finalExpression = string.Join(",", arrStr);

        if (string.IsNullOrEmpty(finalExpression))
        {
            finalExpression = "submission_number DESC, submission_number_sequence DESC";
        }



        if (ds.Tables[tableName] == null)
            return null;
        DataTable ndt = ds.Tables[tableName].Clone();
        // set the datatypes who don't have support field
        foreach (BCS.Biz.SubmissionDisplayGrid acf in all)
        {
            if (sdgs.FindById(acf.Id) == null)
            {
                // there should a col 
                System.Type type;
                if (acf.AttributeDataType.DataTypeName.Equals("Date", StringComparison.CurrentCultureIgnoreCase))
                { type = typeof(DateTime); }
                else if (acf.AttributeDataType.DataTypeName.Equals("BIT", StringComparison.CurrentCultureIgnoreCase))
                {
                    type = typeof(Boolean);
                }
                else if (acf.AttributeDataType.DataTypeName.Equals("MONEY", StringComparison.CurrentCultureIgnoreCase))
                {
                    type = typeof(Decimal);
                }
                else
                    type = typeof(string);

                if (ndt.Columns[acf.FieldName] != null)
                    ndt.Columns[acf.FieldName].DataType = type;
            }
        }

        ndt.Columns["submission_number"].DataType = typeof(long);
        ndt.Columns["submission_number_sequence"].DataType = typeof(int);

        foreach (DataRow dr in ds.Tables[tableName].Rows)
            ndt.ImportRow(dr);
        ndt.AcceptChanges();

        ndt.DefaultView.Sort = finalExpression;

        return ndt;

        return ndt;
    }

    private static StringCollection FetchRelationsAPS(DataSet ds, string baseTableName, string endTableName)
    {
        StringCollection sc = new System.Collections.Specialized.StringCollection();

        foreach (DataRelation var in ds.Tables[baseTableName].ChildRelations)
        {
            if (var.ChildTable.TableName == endTableName)
            {
                sc.Add(string.Format("{0}_{1}", baseTableName, endTableName));
            }
            else
            {
                if (var.ChildTable.ChildRelations.Count > 0)
                {

                    StringCollection temp = FetchRelations(ds, var.ChildTable.TableName, endTableName);
                    if (temp.Count > 0)
                        sc.Add(string.Format("{0}_{1}", baseTableName, var.ChildTable));
                    foreach (string s in temp)
                    {
                        sc.Add(s);
                    }

                }
                else
                    continue;
            }
        }

        return sc;
    }

    public static void SetStatus(System.Web.UI.Control c, string p)
    {
        System.Web.UI.WebControls.TextBox l = new System.Web.UI.WebControls.TextBox();
        l.TextMode = TextBoxMode.MultiLine;
        l.ReadOnly = true;
        l.CssClass = "Error";
        l.Style.Add(HtmlTextWriterStyle.Width, "100%");
        l.Text = p;
        if (c != null)
            AddControl(c, l, false, false);
        l.Focus();
    }
    public static void SetStatus(System.Web.UI.Control c, string p, bool addid)
    {
        System.Web.UI.WebControls.TextBox l = new System.Web.UI.WebControls.TextBox();
        l.ReadOnly = true;
        l.CssClass = "Error";
        l.Style.Add(HtmlTextWriterStyle.Width, "100%");
        if (addid)
            l.ID = Guid.NewGuid().ToString().Replace("-", string.Empty);
        l.Text = p;
        //Control c = Master.FindControl("contentplaceholder1");
        if (c != null)
            AddControl(c, l, false, false);
        l.Focus();
    }

    public static void SetStatus(Control control)
    {
        if (System.Web.HttpContext.Current.Session["StatusMessage"] != null)
        {
            SetStatus(control, System.Web.HttpContext.Current.Session["StatusMessage"].ToString());
            System.Web.HttpContext.Current.Session.Contents.Remove("StatusMessage");
        }
    }

    public static Button AddButton(System.Web.UI.Control control, string id, string p, bool breakAfter, bool spaceAfter)
    {
        Button b = new Button();
        b.CssClass = "button";
        b.Text = p;
        b.ID = id != null ? id : control.UniqueID;
        AddControl(control, b, false, false);
        if (spaceAfter)
            AddControl(control, new LiteralControl("&nbsp;"), false, false);
        if (breakAfter)
            AddControl(control, new LiteralControl("<br/>"), false, false);
        return b;
    }

    public static void AddLabel(Control control, string text, string id, bool breakAfter, bool spaceAfter)
    {
        Label l = new Label();
        l.Text = text;
        l.ID = id;
        AddControl(control, l, breakAfter, spaceAfter);
    }

    public static void AddLabel(Control control, string text, bool breakAfter, bool spaceAfter)
    {
        Label l = new Label();
        l.Text = text;
        AddControl(control, l, breakAfter, spaceAfter);
    }

    public static void AddControl(Control control, Control child, bool breakAfter, bool spaceAfter)
    {
        control.Controls.Add(child);
        if (spaceAfter)
            control.Controls.Add(new LiteralControl("&nbsp;"));
        if (breakAfter)
            control.Controls.Add(new LiteralControl("<br/>"));
    }

    public static string GetUser()
    {
        BCS.Core.Security.CustomPrincipal m_principal = System.Web.HttpContext.Current.User
            as BCS.Core.Security.CustomPrincipal;
        return m_principal.Identity.Name;
    }

    public static ListItem[] SortListItems(ListItem[] listItems)
    {
        Array.Sort(listItems, new ListItemComparer());
        return listItems;
    }

    public static ListItem[] SortListItems(ListItemCollection lic)
    {
        ListItem[] listItems = new ListItem[lic.Count];
        lic.CopyTo(listItems, 0);
        Array.Sort(listItems, new ListItemComparer());
        return listItems;
    }

    public static void AddCookie(string name, string value)
    {
        TSHAK.Components.SecureQueryString sqs = new TSHAK.Components.SecureQueryString(BCS.Core.Core.Key);
        sqs["CookieValue"] = value;

        System.Web.HttpContext.Current.Response.Cookies.Add(new System.Web.HttpCookie(name, sqs.ToString()));
        System.Web.HttpContext.Current.Response.Cookies[name].Expires = DateTime.Now.AddMonths(1);
    }

    public static string GetCookie(string name)
    {
        if (System.Web.HttpContext.Current.Request.Cookies[name] == null ||
            System.Web.HttpContext.Current.Request.Cookies[name].Value == null)
            return null;
        try
        {

            TSHAK.Components.SecureQueryString qs = new TSHAK.Components.SecureQueryString(
                BCS.Core.Core.Key, System.Web.HttpContext.Current.Request.Cookies[name].Value);
            return qs["CookieValue"];
        }
        catch (TSHAK.Components.InvalidQueryStringException ex)
        {
            BTS.LogFramework.LogCentral.Current.LogInfo(string.Format("cookie retrieval failed. {0}", name), ex);
            DeleteCookie(name);
            return null;
        }
    }

    public static void DeleteCookie(string name)
    {
        if (System.Web.HttpContext.Current.Response.Cookies[name] != null)
            System.Web.HttpContext.Current.Response.Cookies[name].Expires = DateTime.Now.AddDays(-1);
    }

    public static bool ShouldUpdate(System.Web.UI.WebControls.DetailsViewUpdateEventArgs e)
    {
        bool dontUpdate = true;
        foreach (System.Collections.DictionaryEntry entry in e.NewValues)
        {
            if (e.NewValues[entry.Key] == null && e.OldValues[entry.Key] == null)
                continue;
            if (e.NewValues[entry.Key] == null || e.OldValues[entry.Key] == null)
            {
                dontUpdate = false;
                break;
            }

            if (!e.NewValues[entry.Key].Equals(e.OldValues[entry.Key]))
            {
                dontUpdate = false;
                break;
            }
        }
        return dontUpdate;
        //return false;
    }

    public static string[] GetValues(ListControl lc)
    {
        string[] DataValues = new string[lc.Items.Count];
        System.Collections.ArrayList al = new System.Collections.ArrayList();

        foreach (ListItem li in lc.Items)
            al.Add(li.Value);
        al.CopyTo(DataValues);
        return DataValues;
    }

    public static List<string> GetValues(GridView gv)
    {
        List<string> list = new List<string>();

        for (int i=0; i <gv.Rows.Count;i++)
        {
            list.Add(gv.DataKeys[i].Value.ToString());
        }

        return list;
    }

    public static ListItem[] GetSelectedItems(ListControl lc)
    {
        System.Collections.ArrayList al = new System.Collections.ArrayList();

        foreach (ListItem li in lc.Items)
            if (li.Selected)
                al.Add(li);
        ListItem[] items = new ListItem[al.Count];
        al.CopyTo(items);
        return items;
    }

    public static List<ListItem> GetSelectedItems(GridView gv, string controlName)
    {
        List<ListItem> selectedItems = new List<ListItem>();
        for (int i = 0; i < gv.Rows.Count;i++ )
        {
            CheckBox cb = (CheckBox)gv.Rows[i].FindControl(controlName);
            if (cb == null) continue;

            if (cb.Checked)
                selectedItems.Add(new ListItem(cb.Text));
        }

        return selectedItems;

    }

    public static int GetSafeSelectedValue(ListControl ddl)
    {
        return ddl.SelectedIndex < 0 ? 0 : Convert.ToInt32(ddl.SelectedValue);
    }

    public static long GetSafeSelectedValueLong(ListControl ddl)
    {
        return ddl.SelectedIndex < 0 ? 0 : Convert.ToInt64(ddl.SelectedValue);
    }

    public static ListItem GetSafeSelectedItem(ListControl ddl)
    {
        return ddl.SelectedIndex < 0 ? ddl.Items[0] : ddl.SelectedItem;
    }
    public static long GetSafeSelectedValue64(ListControl ddl)
    {
        long res = 0;
        Int64.TryParse(ddl.SelectedValue, out res);
        return res;
    }
    

    public static void SetSafeSelectedValue(ListControl ddl, ValueType selectedValue)
    {
        if (ddl.Items.FindByValue(selectedValue.ToString()) != null)
        {
            ddl.ClearSelection();
            ddl.Items.FindByValue(selectedValue.ToString()).Selected = true;
        }
    }
    public static void SetSafeSelectedValue(ListControl ddl, object selectedValue)
    {
        if (ddl.Items.FindByValue(selectedValue.ToString()) != null)
        {
            ddl.ClearSelection();
            ddl.Items.FindByValue(selectedValue.ToString()).Selected = true;
        }
    }
    public static void SetSafeSelectedText(ListControl ddl, string selectedText)
    {
        ddl.ClearSelection();
        if (ddl.Items.FindByText(selectedText) != null)
        {
            ddl.Items.FindByText(selectedText).Selected = true;
        }
    }

    public static string[] GetSelectedValues(ListControl lc)
    {
        System.Collections.ArrayList al = new System.Collections.ArrayList();

        foreach (ListItem li in lc.Items)
            if (li.Selected)
                al.Add(li.Value);
        string[] values = new string[al.Count];
        al.CopyTo(values);
        return values;
    }

    public static string[]  GetSelectedValues(GridView gv, string controlName)
    {
        List<string> selectedValues = new List<string>();
        for (int i = 0; i < gv.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)gv.Rows[i].FindControl(controlName);
            if (cb == null) continue;

            if (cb.Checked)
                selectedValues.Add(gv.DataKeys[i].Value.ToString());
        }

        return selectedValues.ToArray();
    }

    public static string[] GetAllIdsAndTypesFromGV(GridView gv, string valueControl)
    {
        List<string> selectedValues = new List<string>();
        string id, type;
        for (int i = 0; i < gv.Rows.Count; i++)
        {
            id = gv.DataKeys[i].Value.ToString();

            DropDownList dl = (DropDownList)gv.Rows[i].FindControl(valueControl);
            type = dl.SelectedValue;

            if (!string.IsNullOrEmpty(id))
                selectedValues.Add(string.Format("{0}*{1}", id, type));
        }

        return selectedValues.ToArray();
    }

    public static string FormatPhone(string ip)
    {
        if (string.IsNullOrEmpty(ip))
            return null;
        if (ip.Length != 10)
            return null;
        return string.Format("({0}){1}-{2}", ip.Substring(0, 3), ip.Substring(3, 3), ip.Substring(6));
    }
    public static string FormatZip(string ip)
    {
        if (string.IsNullOrEmpty(ip))
            return null;
        if (ip.Length != 5 && ip.Length != 9)
            return null;
        if (ip.Length == 5)
            return ip;
        return string.Format("{0}-{1}", ip.Substring(0, 5), ip.Substring(5));
    }
    public static string DeFormatPhone(string ip)
    {
        if (ip.Length == 0)
            return null;
        ip = ip.Replace("(", string.Empty).Replace(")", string.Empty).Replace(" ", string.Empty).Replace("-", string.Empty);
        return ip;
    }

    public static object DeFormatZip(string ip)
    {
        if (ip.Length == 0)
            return null;
        ip = ip.Replace("-", string.Empty);
        return ip;
    }

    public static void Forward()
    {
        BCS.Core.Security.CustomPrincipal user = System.Web.HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
        if (user == null)
            return;
        if (user.Roles == null)
            return;
        if (user.IsInAnyRoles("Auditor", "System Administrator", "User", "Remote Agency", "Partial User"))
            return;
        if (user.IsInAnyRoles("Agency Administrator", "Agency Read-Only"))
        {
            System.Web.HttpContext.Current.Response.Redirect("Agencies.aspx");
        }
        if (user.IsInRole("Company Administrator"))
        {
            System.Web.HttpContext.Current.Response.Redirect("CompanyNoCodes.aspx");
        }
        if (user.IsInRole("Assignment Administrator"))
        {
            System.Web.HttpContext.Current.Response.Redirect("Persons.aspx");
        }
        if (user.IsInRole("ReAssignment Administrator"))
        {
            System.Web.HttpContext.Current.Response.Redirect("SubmissionAssignments.aspx");
        }
        if (user.IsInRole("Agent Administrator"))
        {
            System.Web.HttpContext.Current.Response.Redirect("Agents.aspx");
        }
        if (user.IsInRole("Policy Number Generator"))
        {
            System.Web.HttpContext.Current.Response.Redirect("PNG.aspx");
        }
        if (user.IsInRole("Agency Contact Administrator"))
        {
            System.Web.HttpContext.Current.Response.Redirect("Contacts.aspx");
        }
        if (user.IsInRole("Switch Client Administrator"))
        {
            System.Web.HttpContext.Current.Response.Redirect("ClientSwitches.aspx");
        }
        // unconfigured roles
        TSHAK.Components.SecureQueryString qs = new TSHAK.Components.SecureQueryString(BCS.Core.Core.Key);
        qs["SecurityEvent"] = "This role is not configured for access to BCS.";
        BTS.LogFramework.LogCentral.Current.LogWarn(string.Format("Unconfigured role : Username {0}", user.Identity.Name));

        System.Web.HttpContext.Current.Response.Redirect("~/NoAccess.aspx?data=" + HttpUtility.UrlEncode(qs.ToString()));
    }

    public static bool IsFocusable(Control c)
    {
        // ms-help://MS.VSCC.v80/MS.MSDN.v80/MS.VisualStudio.v80.en/dv_aspnetcon/html/785722c6-004a-484a-bec2-e1304ee39df7.htm
        // controls that can recieve focus Button, CheckBox, DropDownList, HyperLink, ImageButton, LinkButton, ListBox, RadioButton, TextBox 

        if (c is Button || c is CheckBox || c is DropDownList || c is HyperLink || c is ImageButton || c is LinkButton || c is ListBox
             || c is RadioButton || c is TextBox || c is RadioButtonList || c is CheckBoxList)
            return true;
        return false;
    }

    public static bool SetTabOrder(Control control, short tabIndex)
    {
        foreach (Control c in control.Controls)
        {
            if (Common.IsFocusable(c))
            {
                WebControl wc = c as WebControl;
                if (wc.TabIndex == 0)
                    wc.TabIndex = tabIndex;
            }
            else
            {
                SetTabOrder(c, tabIndex);
            }
        }
        return false;
    }

    public static DateTime GetSafeDateTimeFromQS(string key)
    {
        return GetSafeDateTimeFromQS(key, DateTime.MinValue);
    }

    private static DateTime GetSafeDateTimeFromQS(string key, DateTime defaultValue)
    {
        try
        {
            return Convert.ToDateTime(HttpContext.Current.Request.QueryString[key]);
        }
        catch (Exception)
        {
            return defaultValue;
        }
    }
    private static DateTime GetSafeDateTimeFromQS(string key, DateTime defaultValue, IFormatProvider provider)
    {
        try
        {
            return Convert.ToDateTime(HttpContext.Current.Request.QueryString[key], provider);
        }
        catch (Exception)
        {
            return defaultValue;
        }
    }

    public static int GetSafeIntFromSession(string key)
    {
        return GetSafeIntFromSession(key, 0);
    }

    public static bool GetSafeBoolFromSession(string key)
    {
        return (HttpContext.Current.Session[key] == null) ? false : (bool)HttpContext.Current.Session[key];
    }

    public static int GetSafeIntFromSession(string key, int defaultValue)
    {
        try
        {
            return Convert.ToInt32(HttpContext.Current.Session[key]);
        }
        catch (Exception)
        {
            return defaultValue;
        }
    }
    public static long GetSafeLongFromSession(string key)
    {
        return GetSafeLongFromSession(key, 0);
    }

    public static long GetSafeLongFromSession(string key, long defaultValue)
    {
        try
        {
            return Convert.ToInt64(HttpContext.Current.Session[key]);
        }
        catch (Exception)
        {
            return defaultValue;
        }
    }

    public static string GetSafeStringFromSession(string key)
    {
        return GetSafeStringFromSession(key, string.Empty);
    }

    public static string GetSafeStringFromSession(string key, string defaultValue)
    {
        try
        {
            return HttpContext.Current.Session[key].ToString();
        }
        catch (Exception)
        {
            return defaultValue;
        }
    }

    public static DateTime GetSafeDateTime(string p)
    {
        return GetSafeDateTime(p, DateTime.MinValue);
    }

    public static DateTime GetSafeDateTime(string p, DateTime defaultValue)
    {
        try
        {
            return Convert.ToDateTime(p);
        }
        catch (Exception)
        {
            return defaultValue;
        }
    }

    /// <summary>
    /// Sets the range on a range validator control.
    /// </summary>
    /// <param name="rangeValidator">a range validator object</param>
    public static void SetDateOfBirthRange(RangeValidator rangeValidator)
    {
        rangeValidator.MaximumValue = DateTime.Now.AddYears(-12).ToShortDateString();
        rangeValidator.MinimumValue = DateTime.Now.AddYears(-112).ToShortDateString();
        rangeValidator.Text = string.Format("* Date of Birth should be in the range {0} - {1}.", rangeValidator.MinimumValue, rangeValidator.MaximumValue);
    }

    public static bool IsActive(BCS.Biz.Person person, DateTime asOfDate)
    {
        if (person.RetirementDate.IsNull)
            return true;
        if (person.RetirementDate.Value >= asOfDate)
            return true;

        return false;
    }
    public static DateTime GetSafeDateTimeFromSession(string key)
    {
        return GetSafeDateTimeFromSession(key, DateTime.MinValue);
    }
    private static DateTime GetSafeDateTimeFromSession(string key, DateTime defaultValue)
    {
        try
        {
            return Convert.ToDateTime(HttpContext.Current.Session[key]);
        }
        catch (Exception)
        {
            return defaultValue;
        }
    }
    private static DateTime GetSafeDateTimeFromSession(string key, DateTime defaultValue, IFormatProvider provider)
    {
        try
        {
            return Convert.ToDateTime(HttpContext.Current.Session[key], provider);
        }
        catch (Exception)
        {
            return defaultValue;
        }
    }

    /// <summary>
    /// Finds a Control recursively.
    /// </summary>
    /// <param name="baseControl"></param>
    /// <param name="Id"></param>
    /// <returns></returns>
    public static Control FindControlRecursive(Control baseControl, string Id)
    {
        if (baseControl.ID == Id)
            return baseControl;

        foreach (Control ctrl in baseControl.Controls)
        {
            Control foundCtrl = FindControlRecursive(ctrl, Id);

            if (foundCtrl != null)
                return foundCtrl;
        }
        return null;
    }

    public static bool IsCWG()
    {
        try
        {
            return BCS.WebApp.Components.Security.GetUserCompanies(HttpContext.Current.User.Identity.Name).FindByCompanyId(GetSafeIntFromSession(SessionKeys.CompanyId)).Company.CompanyInitials == "CWG";
        }
        catch (Exception)
        {
            return false;
        }
    }

    public static void AddControl(Control container, Control child)
    {
        AddControl(container, child, false, false);
    }

    public static object GetSafeDateTimeString(object p)
    {
        try
        {
            DateTime safeDateTime = Convert.ToDateTime(p);
            if (safeDateTime == DateTime.MinValue)
                return string.Empty;

            return safeDateTime.ToString(DateFormat);
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    public static bool LoadUserInformationBySubmissionID(int submissionID)
    {
        HttpContext.Current.Session["SubmissionId"] = submissionID;

        //Get & Set Company Information
        BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
        dm.QueryCriteria.And(BCS.Biz.JoinPath.Submission.Columns.Id, submissionID);

        BCS.Biz.Submission submission = dm.GetSubmission(BCS.Biz.FetchPath.Submission.CompanyNumber.Company);

        if (submission != null)
        {
            Common.DeleteCookie("CompanySelected");
            HttpContext.Current.Session[SessionKeys.CompanyId] = submission.CompanyNumber.CompanyId.ToString();
            Common.AddCookie("CompanySelected", submission.CompanyNumber.CompanyId.ToString());

            Common.DeleteCookie("CompanyNumberSelected");
            Common.AddCookie("CompanyNumberSelected", submission.CompanyNumberId.ToString());
            HttpContext.Current.Session[SessionKeys.CompanyNumberId] = submission.CompanyNumberId.ToString();

            HttpContext.Current.Session[SessionKeys.CompanyNumber] = submission.CompanyNumber.PropertyCompanyNumber.ToString();
            return true;
        }
        else
        {
            Common.DeleteCookie("CompanySelected");
            HttpContext.Current.Session.Remove(SessionKeys.CompanyId);

            Common.DeleteCookie("CompanyNumberSelected");
            HttpContext.Current.Session.Remove(SessionKeys.CompanyNumberId);

            HttpContext.Current.Session.Remove(SessionKeys.CompanyNumber);
            return false;
        }

    }

    public static bool IsClientReadOnly()
    {
        if (Security.IsUserReadOnly() || GetSafeBoolFromSession("clientdeleted"))
            return true;

        return false;
    }

    /// <summary>
    /// converts an uppercase or lowercase string to a nice format (making the first letter capital after space and starting letter of the string)
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string formatString(string value)
    {
        char[] array = value.ToLower().ToCharArray();
        if (array.Length >= 1)
            array[0] = char.ToUpper(array[0]);

        for (int i = 1; i < array.Length; i++)
        {
            if (array[i - 1] == ' ')
            {
                if (char.IsLower(array[i]))
                    array[i] = char.ToUpper(array[i]);
            }
        }

        return new string(array);
    }

}

public class ListItemComparer : System.Collections.IComparer
{
    int System.Collections.IComparer.Compare(Object x, Object y)
    {
        if (x == null || y == null)
            throw new ArgumentNullException();
        if (x is ListItem && y is ListItem)
        {
            ListItem xli = x as ListItem;
            ListItem yli = y as ListItem;
            int res = ((new System.Collections.CaseInsensitiveComparer()).Compare(xli.Text, yli.Text));
            return res;
        }
        throw new ArgumentException("Both the items to be compared must be of type System.Web.UI.WebControls.ListItem");
    }
}