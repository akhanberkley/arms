using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using BCS.Core.Clearance.BizObjects;
using BCS.WebApp.Components;
using BTS.LogFramework;
using System.Collections.Generic;
using BCS.Core.Clearance.Admin;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using Structures = BTS.ClientCore.Wrapper.Structures;
using BCS.DAL;
using System.Linq;
using System.Web.UI.HtmlControls;

namespace BCS.WebApp.ScriptServices
{
    [ScriptService]
    [WebService(Namespace = "http://intranet.wrbts.ads.wrberkley.com/BCS/WebApp/SubmissionService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class SubmissionService : System.Web.Services.WebService
    {
        public SubmissionService()
        {
            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

        [WebMethod(EnableSession = true)]
        public string VerifySubmission()
        {
            string tabString = "&nbsp;&nbsp;&nbsp;&nbsp;";
            string staTag = "<{0}>";
            string staTagWAttr = "<{0} {1}>";
            string endTag = "</{0}>";
            string fulTag = "<{0}/>";

            List<string> listOfMessages = new List<string>();

            try
            {
                StringBuilder htmlStringBuilder = new StringBuilder();

                htmlStringBuilder.AppendFormat(staTagWAttr, "div", "id=\"divVerify\"");

                if (Session[SessionKeys.VerifyingSubmission] != null)
                {
                    #region Read submission and dispose
                    Core.Clearance.BizObjects.Submission subToVerify = (Core.Clearance.BizObjects.Submission)Session[SessionKeys.VerifyingSubmission];
                    Session.Contents.Remove(SessionKeys.VerifyingSubmission);
                    #endregion

                    BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(BCS.Biz.JoinPath.Company.Columns.Id, subToVerify.CompanyId);
                    BCS.Biz.Company company = dm.GetCompany();

                    DateTime effDate = subToVerify.EffectiveDate;
                    if (effDate == DateTime.MinValue)
                        effDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value;

                    #region Agency
                    {
                        if (subToVerify.AgencyId != 0)
                        {
                            BCS.Biz.Agency agency = Core.Clearance.Admin.DataAccess.GetAgency(subToVerify.AgencyId);
                            if (agency != null)
                            {
                                // verify cancelled
                                if (agency.EffectiveDate > effDate || agency.CancelDate < effDate)
                                {
                                    listOfMessages.Add("Agency is cancelled as of submission's effective date.");
                                }

                                // verify referred
                                if (agency.ReferralDate < effDate)
                                {
                                    listOfMessages.Add(string.Format("Agency is referred to {0} as of submission's effective date.", agency.ReferralAgencyNumber));
                                }
                            }
                        }
                        else
                        {
                            listOfMessages.Add("Missing Agency.");
                        }
                    }
                    #endregion

                    #region Line Of Business, Underwriter, Analyst, REMOVED CAUSING CONFUSION THEY SAY.
                    //{
                    //    if (subToVerify.LineOfBusinessId != 0)
                    //    {
                    //        bool lobValid = IsLOBValid(dm, subToVerify.AgencyId, subToVerify.LineOfBusinessId);

                    //        if (lobValid) // if valid check for underwriter and analyst
                    //        {
                    //            #region get role ids used
                    //            int uRoleId = company.UnderwriterRoleId.IsNull ? 0 : company.UnderwriterRoleId.Value;
                    //            int aRoleId = company.AnalystRoleId.IsNull ? 0 : company.AnalystRoleId.Value;
                    //            #endregion
                    //            if (subToVerify.UnderwriterId != 0)
                    //            {
                    //                bool validUnderwriter = IsPersonValid(dm, subToVerify.AgencyId, subToVerify.LineOfBusinessId, subToVerify.UnderwriterId, uRoleId);
                    //                if (!validUnderwriter)
                    //                {
                    //                    listOfMessages.Add("Underwriter is not valid.");
                    //                }
                    //                else // check for retired
                    //                {
                    //                    BCS.Biz.Person person = DataAccess.GetPerson(subToVerify.UnderwriterId);
                    //                    if (person.RetirementDate < effDate)
                    //                    {
                    //                        listOfMessages.Add("Underwriter is retired as of submission's effective date.");
                    //                    }
                    //                }
                    //            }
                    //            else
                    //            {
                    //                listOfMessages.Add("Missing Underwriter.");
                    //            }
                    //            if (subToVerify.AnalystId != 0)
                    //            {
                    //                bool validAnalyst = IsPersonValid(dm, subToVerify.AgencyId, subToVerify.LineOfBusinessId, subToVerify.AnalystId, aRoleId);
                    //                if (!validAnalyst)
                    //                {
                    //                    listOfMessages.Add("Analyst/Assistant is not valid.");
                    //                }
                    //                else // check for retired
                    //                {
                    //                    BCS.Biz.Person person = DataAccess.GetPerson(subToVerify.AnalystId);
                    //                    if (person.RetirementDate < effDate)
                    //                    {
                    //                        listOfMessages.Add("Analyst/Assistant is retired as of submission's effective date.");
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        else // else underwriter and analyst cannot be valid
                    //        {
                    //            listOfMessages.Add("Underwriting unit id is not valid.");
                    //            listOfMessages.Add("Underwriter is not valid.");
                    //            listOfMessages.Add("Analyst/Assistant is not valid.");
                    //        }
                    //    }
                    //    else
                    //    {
                    //        listOfMessages.Add("Missing Underwriting unit.");
                    //    }
                    //}
                    #endregion

                    #region Agent
                    {
                        if (subToVerify.AgentId != 0)
                        {
                            BCS.Biz.Agent agent = Core.Clearance.Admin.DataAccess.GetAgent(subToVerify.AgentId);
                            if (agent.AgencyId != subToVerify.AgencyId)
                            {
                                listOfMessages.Add("Invalid Agent. Does not belong to the agency.");
                            }
                            else
                            {
                                // verify cancelled
                                if (agent.EffectiveDate > effDate || agent.CancelDate < effDate)
                                {
                                    listOfMessages.Add("Agent is cancelled as of submission's effective date.");
                                }
                            }
                        }
                        else
                        {
                            if (company.SupportsAgentUnspecifiedReasons.IsTrue)
                            {
                                if (subToVerify.AgentUnspecifiedReasonList == null || subToVerify.AgentUnspecifiedReasonList.Length == 0)
                                {
                                    listOfMessages.Add("Agent unspecified reason(s) not entered.");
                                }
                            }
                        }
                    }
                    #endregion

                    #region Contact
                    {
                        if (subToVerify.ContactId != 0)
                        {
                            BCS.Biz.Contact contact = Core.Clearance.Admin.DataAccess.GetContact(subToVerify.ContactId);
                            if (contact.AgencyId != subToVerify.AgencyId)
                            {
                                listOfMessages.Add("Invalid Contact. Does not belong to the agency.");
                            }
                            if (!contact.RetirementDate.IsNull && contact.RetirementDate.Value < subToVerify.EffectiveDate)
                            {
                                listOfMessages.Add("Agency contact is retired as of submission's effective date.");
                            }
                        }
                    }
                    #endregion

                    #region Status Reason
                    if (subToVerify.SubmissionStatusReasonId != 0)
                    {
                        dm.QueryCriteria.Clear();
                        dm.QueryCriteria.And(BCS.Biz.JoinPath.SubmissionStatusSubmissionStatusReason.Columns.SubmissionStatusReasonId, subToVerify.SubmissionStatusReasonId);
                        dm.QueryCriteria.And(BCS.Biz.JoinPath.SubmissionStatusSubmissionStatusReason.Columns.SubmissionStatusId, subToVerify.SubmissionStatusId);

                        // somehow reason id is not having affect on the Biz query, so get collection and filter
                        BCS.Biz.SubmissionStatusSubmissionStatusReasonCollection reasons = dm.GetSubmissionStatusSubmissionStatusReasonCollection();
                        reasons = reasons.FilterBySubmissionStatusId(
                            subToVerify.SubmissionStatusId).FilterBySubmissionStatusReasonId(
                            subToVerify.SubmissionStatusReasonId);

                        BCS.Biz.SubmissionStatusSubmissionStatusReason aReason = null;
                        if (reasons.Count == 1)
                            aReason = reasons[0];
                        if (null == aReason)
                        {
                            listOfMessages.Add(string.Format("Invalid status reason {0}.", subToVerify.SubmissionStatusReason));
                        }
                        else
                        {
                            if (!aReason.Active.Value)
                            {
                                listOfMessages.Add(string.Format("Inactive status reason {0}.", subToVerify.SubmissionStatusReason));
                            }
                        }
                    }
                    #endregion

                    #region Policy Number
                    if (company.RequiresPolicyNumber.IsTrue)
                    {
                        if (string.IsNullOrEmpty(subToVerify.PolicyNumber))
                        {
                            listOfMessages.Add("Missing policy number.");
                        }
                    }
                    #endregion

                    #region Company Codes
                    {
                        if (null != subToVerify.SubmissionCodeTypess)
                        {
                            List<int> submissionCodeTypeIds = new List<int>();
                            foreach (SubmissionCodeType var in subToVerify.SubmissionCodeTypess)
                            {
                                submissionCodeTypeIds.Add(var.CodeId);
                            }

                            dm.QueryCriteria.Clear();
                            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCode.Columns.CompanyNumberId, subToVerify.CompanyNumberId);
                            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCode.Columns.Id, submissionCodeTypeIds, OrmLib.MatchType.In);
                            BCS.Biz.CompanyNumberCodeCollection codes = dm.GetCompanyNumberCodeCollection();

                            if (subToVerify.SubmissionCodeTypess != null)
                            {
                                foreach (SubmissionCodeType var in subToVerify.SubmissionCodeTypess)
                                {
                                    BCS.Biz.CompanyNumberCode aCode = codes.FindById(var.CodeId);
                                    if (aCode == null)
                                    {
                                        listOfMessages.Add(string.Format("The Code [{0}-{1}] is invalid.", var.CodeValue, var.CodeDescription));
                                    }
                                    else
                                    {
                                        // verify referred
                                        if (aCode.ExpirationDate < effDate)
                                        {
                                            listOfMessages.Add(string.Format("The Code [{0}-{1}] is expired as of submission's effective date.",
                                                var.CodeValue, var.CodeDescription));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region LOB Children
                    {
                        if (company.SupportsMultipleLineOfBusinesses.IsTrue)
                        {
                            List<int> submissionLOBGridIds = new List<int>();
                            foreach (SubmissionLineOfBusinessChild var in subToVerify.LineOfBusinessChildList)
                            {
                                submissionLOBGridIds.Add(var.CompanyNumberLOBGridId);
                            }

                            if (subToVerify.LineOfBusinessChildList != null && subToVerify.LineOfBusinessChildList.Length > 0)
                            {
                                #region fetch lobs
                                dm.QueryCriteria.Clear();
                                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberLOBGrid.Columns.CompanyNumberId, subToVerify.CompanyNumberId);
                                dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberLOBGrid.Columns.Id, submissionLOBGridIds, OrmLib.MatchType.In);
                                BCS.Biz.CompanyNumberLOBGridCollection lobs = dm.GetCompanyNumberLOBGridCollection();
                                #endregion

                                #region fetch reasons
                                dm.QueryCriteria.Clear();
                                BCS.Biz.SubmissionStatusSubmissionStatusReasonCollection sssrs = dm.GetSubmissionStatusSubmissionStatusReasonCollection(
                                    BCS.Biz.FetchPath.SubmissionStatusSubmissionStatusReason.SubmissionStatus,
                                    BCS.Biz.FetchPath.SubmissionStatusSubmissionStatusReason.SubmissionStatusReason);
                                #endregion

                                foreach (SubmissionLineOfBusinessChild lobChild in subToVerify.LineOfBusinessChildList)
                                {
                                    #region LOB Grid


                                    BCS.Biz.CompanyNumberLOBGrid aLOB = lobs.FindById(lobChild.CompanyNumberLOBGridId);
                                    if (aLOB == null)
                                    {
                                        listOfMessages.Add(string.Format("The Line Of Business [{0}-{1}] is invalid.",
                                            lobChild.CompanyNumberLOBGrid == null ? "??" : lobChild.CompanyNumberLOBGrid.Code,
                                            lobChild.CompanyNumberLOBGrid == null ? "??" : lobChild.CompanyNumberLOBGrid.Description));
                                    }
                                    else
                                    {
                                        // verify referred
                                        if (aLOB.Active.IsFalse)
                                        {
                                            listOfMessages.Add(string.Format("The Line Of Business [{0}-{1}] is in-active.",
                                                lobChild.CompanyNumberLOBGrid.Code, lobChild.CompanyNumberLOBGrid.Description));
                                        }
                                    }
                                    #endregion

                                    #region Status Reason
                                    //foreach (KeyValuePair<int, int> varStatusStatusReason in submissionLOBGridStatusReasonIds)
                                    {
                                        BCS.Biz.SubmissionStatusSubmissionStatusReasonCollection subSssrs = sssrs.FilterBySubmissionStatusId(lobChild.SubmissionStatusId);

                                        if (subSssrs.Count > 0) // check only if a status has reasons
                                        {
                                            subSssrs = subSssrs.FilterBySubmissionStatusReasonId(lobChild.SubmissionStatusReasonId);
                                            if (subSssrs.Count > 0)
                                            {
                                                BCS.Biz.SubmissionStatusSubmissionStatusReason aSssrs = subSssrs[0]; // there should be only one
                                                if (aSssrs.Active.IsFalse)
                                                {
                                                    listOfMessages.Add(string.Format("The Status Reason [{0}-{1}] is in-active.",
                                                        aSssrs.SubmissionStatus == null ? "??" : aSssrs.SubmissionStatus.StatusCode,
                                                        aSssrs.SubmissionStatusReason == null ? "??" : aSssrs.SubmissionStatusReason.ReasonCode));
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                if (subToVerify.SubmissionTypeId != 2)
                                    listOfMessages.Add("Missing LOB.");
                            }



                        }
                    }
                    #endregion

                    #region Client
                    {
                        if (subToVerify.ClientCoreAddressIds.Length == 0)
                        {
                            listOfMessages.Add("Missing client addresses.");
                        }
                        // TODO: uncomment after BPM, etc adapts
                        //if (subToVerify.ClientCoreNameIds.Length == 0)
                        //{
                        //    listOfMessages.Add("Missing client names.");
                        //}
                    }
                    #endregion

                    #region Build return markup
                    if (subToVerify.Id == 0)
                        return string.Empty;


                    if (listOfMessages.Count > 0)
                    {
                        htmlStringBuilder.AppendFormat(staTagWAttr, "ul",
                            "style=\"list-style-type: circle;\"");

                        foreach (string var in listOfMessages)
                        {
                            htmlStringBuilder.AppendFormat(staTag, "li");
                            htmlStringBuilder.Append(var);
                            htmlStringBuilder.AppendFormat(endTag, "li");
                        }

                        htmlStringBuilder.AppendFormat(endTag, "ul");
                    }
                    #endregion
                }
                htmlStringBuilder.AppendFormat(endTag, "div");
                return htmlStringBuilder.ToString();
            }
            catch (Exception ex)
            {
                string message = "Unable to Verify Submission.";

                StringBuilder htmlStringBuilder = new StringBuilder();
                htmlStringBuilder.AppendFormat(staTagWAttr, "div", "id=\"divVerify\"");
                htmlStringBuilder.Append(message);
                htmlStringBuilder.AppendFormat(endTag, "div");

                LogCentral.Current.LogFatal(message, ex);
                //throw ex;
                return htmlStringBuilder.ToString();
            }

        }

        public bool IsLOBValid(BCS.Biz.DataManager dm, int agencyId, int lobId)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLineOfBusiness.Columns.AgencyId, agencyId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLineOfBusiness.Columns.LineOfBusinessId, lobId);

            return dm.GetAgencyLineOfBusiness() != null;
        }

        public bool IsPersonValid(BCS.Biz.DataManager dm, int agencyId, int lobId, int personId, int roleId)
        {
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.AgencyId, agencyId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.LineOfBusinessId, lobId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.PersonId, personId);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.AgencyLineOfBusinessPersonUnderwritingRolePerson.Columns.UnderwritingRoleId, roleId);

            return dm.GetAgencyLineOfBusinessPersonUnderwritingRolePerson() != null;

        }

        [WebMethod(EnableSession = true)]
        public SubmissionsInfo GetSubmissionCount(string numbers, string lookupType)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return new SubmissionsInfo(0);
            }
            string[] numbersArray = numbers.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = numbersArray[i].Trim();
            }
            switch (lookupType)
            {
                case "SubmissionNumber":
                    {
                        BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
                        dm.QueryCriteria.And(BCS.Biz.JoinPath.Submission.Columns.CompanyNumberId, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                        dm.QueryCriteria.And(BCS.Biz.JoinPath.Submission.Columns.SubmissionNumber, numbersArray, OrmLib.MatchType.In);
                        BCS.Biz.SubmissionCollection submissions = dm.GetSubmissionCollection();

                        return new SubmissionsInfo(submissions.Count);
                    }
                case "PolicyNumber":
                    {
                        BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
                        dm.QueryCriteria.And(BCS.Biz.JoinPath.Submission.Columns.CompanyNumberId, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                        dm.QueryCriteria.And(BCS.Biz.JoinPath.Submission.Columns.PolicyNumber, numbersArray, OrmLib.MatchType.In);
                        BCS.Biz.SubmissionCollection submissions = dm.GetSubmissionCollection();

                        return new SubmissionsInfo(submissions.Count);
                    }
                default:
                    {
                        return new SubmissionsInfo(0);
                    }
                    break;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SearchClassCodes(string code)
        {
            int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
            var cn = BTS.WFA.BCS.Application.CompanyMap.Values.First(c => c.CompanyNumberId == companyNumberId);
            var customizations = BTS.WFA.BCS.Services.CompanyCustomizationService.GetCustomizations(cn).Where(c => c.Usage == BTS.WFA.BCS.ViewModels.CompanyCustomizationUsage.ClassCode);
            var customizationCodeTypes = customizations.Where(c => c.DataType == BTS.WFA.BCS.ViewModels.CompanyCustomizationDataType.List);
            var classCodes = BTS.WFA.BCS.Services.LegacyService.ClassCodeSearch(cn, code, 50);

            var table = new HtmlTable() { Border = 1 };
            var tr = new HtmlTableRow();
            tr.Style.Add("font-weight", "bold");
            tr.Cells.Add(new HtmlTableCell() { InnerText = "Code" });
            tr.Cells.Add(new HtmlTableCell() { InnerText = "Description" });
            tr.Cells.Add(new HtmlTableCell() { InnerText = "SIC Code" });
            tr.Cells.Add(new HtmlTableCell() { InnerText = "NAICS Code" });
            foreach (var c in customizationCodeTypes)
                tr.Cells.Add(new HtmlTableCell() { InnerText = c.Description });
            table.Rows.Add(tr);

            if (classCodes.Count == 0)
            {
                tr = new HtmlTableRow();
                tr.Cells.Add(new HtmlTableCell() { ColSpan = 4 + customizations.Count(), InnerText = "No class codes found." });
                table.Rows.Add(tr);
            }
            else
                foreach (var cc in classCodes)
                {
                    tr = new HtmlTableRow();
                    tr.Cells.Add(new HtmlTableCell() { InnerText = cc.CodeValue });
                    tr.Cells.Add(new HtmlTableCell() { InnerText = cc.Description });
                    tr.Cells.Add(new HtmlTableCell() { InnerText = (cc.SicCode == null) ? null : cc.SicCode.Code });
                    tr.Cells.Add(new HtmlTableCell() { InnerText = (cc.NaicsCode == null) ? null : cc.NaicsCode.Code });
                    foreach (var ct in customizationCodeTypes)
                    {
                        var ccCustomization = cc.CompanyCustomizations.FirstOrDefault(c => c.Customization == ct.Description && c.CodeValue != null);
                        if (ccCustomization == null)
                            tr.Cells.Add(new HtmlTableCell() { InnerText = "" });
                        else
                            tr.Cells.Add(new HtmlTableCell() { InnerText = ccCustomization.CodeValue.Code });
                    }

                    table.Rows.Add(tr);
                }

            var sb = new StringBuilder();
            table.RenderControl(new System.Web.UI.HtmlTextWriter(new System.IO.StringWriter(sb)));
            return sb.ToString();
        }


        #region Agency auto completion
        [WebMethod(EnableSession = true), ScriptMethodAttribute()]
        public string[] GetCompletionList(string prefixText, int count, string contextKey)
        {
            Regex reg = new Regex(string.Format("^{0}.*", prefixText), RegexOptions.IgnoreCase);

            string suggestionKey = contextKey.Split("|".ToCharArray())[0];
            string dateTimeKey = contextKey.Split("|".ToCharArray())[1];

            BCS.Biz.AgencyCollection agencies = GetAgencies(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId), Convert.ToDateTime(dateTimeKey));
            BCS.Biz.AgencyCollection fagencies = new BCS.Biz.AgencyCollection();
            int aCount = 0;
            foreach (BCS.Biz.Agency agency in agencies)
            {
                System.Text.RegularExpressions.Match m = reg.Match(agency[suggestionKey].ToString());
                if (m.Success)
                {
                    fagencies.Add(agency);
                    aCount++;
                    if (aCount == count)
                        break;
                }
            }
            return AsStringArray("{0} ({1}) | {2}, {3}", fagencies);
        }
        private BCS.Biz.AgencyCollection GetAgencies(int companyNumberId, DateTime asOfDate)
        {
            string tablename = "Agency";
            BCS.WebApp.UserControls.AgencyDropDownList agddl = new BCS.WebApp.UserControls.AgencyDropDownList();
            agddl.CompanyNumberId = companyNumberId;
            DataSet cachedDataSet = agddl.GetCachedSource();
            BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
            string expr = string.Format("(CancelDate IS NULL OR CancelDate >= '{0}') AND (EffectiveDate IS NULL OR EffectiveDate <= '{0}')",
                asOfDate.ToShortDateString());
            DataRow[] selectedRows = cachedDataSet.Tables[tablename].Select(expr);
            DataSet filterDataSet = cachedDataSet.Clone();
            foreach (DataRow drow in selectedRows)
                filterDataSet.Tables[tablename].Rows.Add(drow.ItemArray);
            filterDataSet.AcceptChanges();

            dm.DataSet = filterDataSet;
            return dm.GetAgencyCollectionFromDataSet();
        }

        private string[] AsStringArrayBy(string property, BCS.Biz.AgencyCollection collection)
        {
            List<string> items = new List<string>(collection.Count);

            for (int i = 0; i < collection.Count; i++)
            {
                items.Add(
                    AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(collection[i][property].ToString(),
                    collection[i]["Id"].ToString()));
            }
            return items.ToArray();
        }
        private string[] AsStringArray(string displayFormat, BCS.Biz.AgencyCollection collection)
        {
            List<string> items = new List<string>(collection.Count);

            for (int i = 0; i < collection.Count; i++)
            {
                items.Add(
                    AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(
                    string.Format(displayFormat, collection[i]["AgencyName"], collection[i]["AgencyNumber"], collection[i]["City"], collection[i]["State"]),
                    collection[i]["Id"].ToString()));
            }
            return items.ToArray();
        }
        #endregion

        #region Portfolio Validation
        /// <summary>
        /// Validates the portfolio
        /// </summary>
        /// <param name="portfolioId">Id to be validated</param>
        /// <returns>imageUrl|portfolioName. checked.gif for valid portfolio, cancelBig.gif for invalid.</returns>
        [WebMethod(EnableSession = true)]
        public string Verify(string portfolioId)
        {
            // TODO: relying on client search to validate portfolio. There might be portfolios that has no clients associated. Also performance will 
            // be affected if there are many clients in a portfolio
            ClientProxy.Client clientProxy = new ClientProxy.Client();
            string resXML = clientProxy.GetClientsByPortfolioId(Common.GetSafeStringFromSession(SessionKeys.CompanyNumber), portfolioId);

            Structures.Search.Vector o = (Structures.Search.Vector)BCS.Core.XML.Deserializer.Deserialize(resXML, typeof(Structures.Search.Vector));
            string result = string.Empty;
            if ((string.IsNullOrEmpty(o.Error) || string.IsNullOrEmpty(o.Message)) && (o.Clients != null))
            {
                result = string.Concat(Common.GetImageUrl("checked.gif"), "|", string.IsNullOrEmpty(o.Clients[0].Portfolio.Name) ?
                    "(not set)" : o.Clients[0].Portfolio.Name);
            }
            else
            {
                result = string.Concat(Common.GetImageUrl("cancelBig.gif"), "|", "(not found)");
            }

            return result;

        }

        #endregion
    }

    public class SubmissionsInfo
    {
        public SubmissionsInfo(int count)
        {
            Count = count;
        }
        int countField;

        public int Count
        {
            get { return countField; }
            set { countField = value; }
        }
    }
}