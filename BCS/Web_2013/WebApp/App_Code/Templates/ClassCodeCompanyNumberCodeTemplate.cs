﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using BCS.WebApp.UserControls;
using System.Web.UI;

namespace BCS.WebApp.Templates
{
    /// <summary>
    /// Summary description for ClientClassCodeCompanyNumberCodeTemplate
    /// </summary>
    public class ClassCodeCompanyNumberCodeTemplate : ITemplate
    {
        private DataControlRowState state;
        private DataControlRowType type;
        private string field;
        private string id;
        private int cnId; //Company Number Id
        private string[] assIdsAndFields;
        private string ddlDataTextField;
        private UserControls.Specific specifc;
        private int codeTypeId;
        private bool isDetailGrid;
        

        public ClassCodeCompanyNumberCodeTemplate(
            DataControlRowType controlRowType, 
            int companyNumberCodeTypeId, 
            UserControls.Specific specificType,
            DataControlRowState controlRowState,
            string fieldName,
            bool detailGrid)
        {
            state = controlRowState;
            type = controlRowType;
            codeTypeId = companyNumberCodeTypeId;
            specifc = specificType;
            isDetailGrid = detailGrid;
            field = fieldName;
        }

        public ClassCodeCompanyNumberCodeTemplate(
            DataControlRowType controlRowType,
            int companyNumberCodeTypeId,
            UserControls.Specific specificType,
            DataControlRowState controlRowState,
            string fieldName)
        {
            state = controlRowState;
            type = controlRowType;
            codeTypeId = companyNumberCodeTypeId;
            specifc = specificType;
            isDetailGrid = false;
            field = fieldName;
        }

        //public ClientClassCodeCompanyNumberCodeTemplate(DataControlRowState rowState, DataControlRowType rowType(l

        #region ITemplate Members

        void ITemplate.InstantiateIn(Control container)
        {
            switch (state)
            {
                case DataControlRowState.Edit:
                    {
                        switch (type)
                        {
                            case DataControlRowType.DataRow:
                                {
                                    CompanyNumberCodeDropDownList cnddl = new CompanyNumberCodeDropDownList(specifc, codeTypeId, null);
                                    cnddl.AutoPostBack = false;
                                    cnddl.DataBind();
                                    cnddl.DataBinding += new EventHandler(ddl_DataBinding);
                                    container.Controls.Add(cnddl);
                                    break;
                                }
                        }
                    break;
                    }
                case DataControlRowState.Insert:
                    {
                        CompanyNumberCodeDropDownList cnddl = new CompanyNumberCodeDropDownList(specifc, codeTypeId, null);
                        cnddl.AutoPostBack = false;
                        cnddl.DataBind();
                        container.Controls.Add(cnddl);
                        break;
                    }
                case DataControlRowState.Normal:
                    {
                        switch (type)
                        {
                            case DataControlRowType.DataRow:
                                {
                                    Label cnlbl = new Label();
                                    cnlbl.ID = string.Format("{0}lbl", codeTypeId);
                                    cnlbl.DataBinding += new EventHandler(lbl_DataBinding);
                                    container.Controls.Add(cnlbl);
                                    break;
                                }
                        }
                        break;
                    }
            }
        }

        #endregion

        void ddl_DataBinding(object sender, EventArgs e)
        {
            CompanyNumberCodeDropDownList cnddl = (CompanyNumberCodeDropDownList)sender;
            int classCodeId;
            if (isDetailGrid)
            {
                DetailsView row = (DetailsView)cnddl.NamingContainer;
                classCodeId = (int)DataBinder.Eval(row.DataItem, field);
            }
            else
            {
                GridViewRow row = (GridViewRow)cnddl.NamingContainer;
                classCodeId = (int)DataBinder.Eval(row.DataItem, field);
            }
        
            Biz.DataManager dm = new Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.ClassCodeCompanyNumberCode.Columns.ClassCodeId, classCodeId);
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.CompanyCodeTypeId, codeTypeId);

            Biz.CompanyNumberCodeCollection codeColl = dm.GetCompanyNumberCodeCollection();

            if (codeColl != null && codeColl.Count > 0)
            {
                if (cnddl.Items.FindByValue(codeColl[0].Id.ToString()) != null)
                {
                    cnddl.Items.FindByValue(codeColl[0].Id.ToString()).Selected = true;
                }
            }
        }

        void lbl_DataBinding(object sender, EventArgs e)
        {
            Label l = (Label)sender;

            int classCodeId;
            try
            {
                if (isDetailGrid)
                {
                    DetailsView row = (DetailsView)l.NamingContainer;
                    classCodeId = (int)DataBinder.Eval(row.DataItem, field);
                }
                else
                {
                    GridViewRow row = (GridViewRow)l.NamingContainer;
                    classCodeId = (int)DataBinder.Eval(row.DataItem, field);
                }
            }
            catch (Exception ex)
            {
                return;
            }

            Biz.DataManager dm = new Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.ClassCodeCompanyNumberCode.Columns.ClassCodeId, classCodeId);
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCode.Columns.CompanyCodeTypeId, codeTypeId);

            Biz.CompanyNumberCodeCollection codeColl = dm.GetCompanyNumberCodeCollection();

            if (codeColl != null && codeColl.Count > 0)
                l.Text = codeColl[0].Code;
        }
    }
}