using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCS.WebApp.Templates
{
    

    /// <summary>
    /// Summary description for LabelTemplate
    /// </summary>
    public class LabelTemplate : ITemplate
    {
        private DataControlRowType templateType;
        private string columnName;
        private string field;
        private string id;
        private FormatType formattype = FormatType.Text;

        public LabelTemplate(DataControlRowType type, string headerText, string dataField, string controlId)
        {
            templateType = type;
            columnName = headerText;
            field = dataField;
            id = controlId;
        }

        public LabelTemplate(DataControlRowType type, string headerText, string dataField, string formatType, string controlId)
        {
            templateType = type;
            columnName = headerText;
            field = dataField;
            id = controlId;
            try
            {
                formattype = (FormatType)Enum.Parse(typeof(FormatType), formatType, true);
            }
            catch (Exception) { }
        }

        #region ITemplate Members

        void ITemplate.InstantiateIn(Control container)
        {
            switch (templateType)
            {
                case DataControlRowType.Header:
                    // Create the controls to put in the headerText
                    // section and set their properties.
                    Literal lc = new Literal();
                    lc.Text = "<b>" + columnName + "</b>";

                    // Add the controls to the Controls collection
                    // of the container.
                    container.Controls.Add(lc);
                    break;
                case DataControlRowType.DataRow:
                    // Create the controls to put in a data row
                    // section and set their properties.
                    Label l = new Label();
                    if (id != null && id.Length >0)
                        l.ID = id;

                    // To support data binding, register the event-handling methods
                    // to perform the data binding. Each control needs its own event
                    // handler.
                    l.DataBinding += new EventHandler(l_DataBinding);
                    
                    container.Controls.Add(l);
                    break;

                // Insert cases to create the content for the other 
                // row types, if desired.

                default:
                    // Insert code to handle unexpected values.
                    break;
            }
        }

        void l_DataBinding(object sender, EventArgs e)
        {
            // Get the Label control to bind the value. The Label control
            // is contained in the object that raised the DataBinding 
            // event (the sender parameter).
            Label l = (Label)sender;

            // Get the GridViewRow object that contains the Label control. 
            GridViewRow row = (GridViewRow)l.NamingContainer;

            // Get the field value from the GridViewRow object and 
            // assign it to the Text property of the Label control.
            switch (formattype)
            {
                case FormatType.Date:
                    {
                        DateTime dt = Convert.ToDateTime(DataBinder.Eval(row.DataItem, field));
                        l.Text = dt == DateTime.MinValue ? string.Empty : dt.ToString("MM/dd/yyyy");
                        break;
                    }
                case FormatType.Money:
                    {
                        decimal dt = Convert.ToDecimal(DataBinder.Eval(row.DataItem, field));
                        l.Text = string.Format("{0:c}", dt);
                        break;
                    }
                case FormatType.Text:
                    {
                        l.Text = DataBinder.Eval(row.DataItem, field).ToString();
                        break;
                    }
                default:
                    break;
            }
            
        }

        #endregion
    }
}