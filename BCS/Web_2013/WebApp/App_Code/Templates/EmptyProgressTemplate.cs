using System.Web.UI;

namespace BCS.WebApp.Templates
{
    /// <summary>
    /// Summary description for EmptyProgressTemplate
    /// </summary>
    public class EmptyProgressTemplate : ITemplate
    {

        #region ITemplate Members

        public void InstantiateIn(Control container)
        {
        }

        #endregion
    } 
}
