using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using BCS.WebApp.UserControls;
using AjaxControlToolkit;

namespace BCS.WebApp.Templates
{
    public class ClassCodeDropDownListTemplate : ITemplate
    {
        private DataControlRowState state;
        private DataControlRowType type;
        private string field;
        private string id;
        private int cnId;
        private string[] assIdsAndFields;
        private string ddlDataTextField;

        public ClassCodeDropDownListTemplate(int companyNumberId,
            DataControlRowState templateState, DataControlRowType templateType, string dataField, string dropdownDataTextField, string controlId, params string[] associatedControlIdsAndFields)
        {
            type = templateType;
            state = templateState;
            field = dataField;
            id = controlId;
            cnId = companyNumberId;
            assIdsAndFields = associatedControlIdsAndFields;
            ddlDataTextField = dropdownDataTextField;
        }

        #region ITemplate Members

        void ITemplate.InstantiateIn(Control container)
        {
            switch (state)
            {
                case DataControlRowState.Alternate:
                    break;
                case DataControlRowState.Edit:
                    {
                        switch (type)
                        {
                            case DataControlRowType.DataRow:
                                {
                                    ClassCodeDropDownList sc = new ClassCodeDropDownList();
                                    if (!string.IsNullOrEmpty(id))
                                        sc.ID = id;
                                    if (!string.IsNullOrEmpty(ddlDataTextField))
                                        sc.DataTextField = ddlDataTextField;
                                    sc.Style.Add(HtmlTextWriterStyle.MarginTop, "15px");
                                    sc.CompanyNumberId = cnId;
                                    sc.AutoPostBack = true;
                                    sc.DataBind();
                                    sc.DataBinding += new EventHandler(sc_DataBinding);
                                    sc.SelectedIndexChanged += new EventHandler(sc_SelectedIndexChanged);
                                    container.Controls.Add(sc);

                                    ListSearchExtender lse = new ListSearchExtender();
                                    lse.ID = Guid.NewGuid().ToString().Replace("-", "");
                                    lse.TargetControlID = sc.ID;                               
                                    container.Controls.Add(lse);
                                    break;
                                }
                            case DataControlRowType.EmptyDataRow:
                                break;
                            case DataControlRowType.Footer:
                                break;
                            case DataControlRowType.Header:
                                break;
                            case DataControlRowType.Pager:
                                break;
                            case DataControlRowType.Separator:
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                case DataControlRowState.Insert:
                    {
                        switch (type)
                        {
                            case DataControlRowType.DataRow:
                                {
                                    break;
                                }
                            case DataControlRowType.EmptyDataRow:
                                {
                                    break;
                                }
                            case DataControlRowType.Footer:
                                {
                                    ClassCodeDropDownList sc = new ClassCodeDropDownList();
                                    if (!string.IsNullOrEmpty(id))
                                        sc.ID = id;
                                    if (!string.IsNullOrEmpty(ddlDataTextField))
                                        sc.DataTextField = ddlDataTextField;
                                    sc.Style.Add(HtmlTextWriterStyle.MarginTop, "15px");
                                    sc.AutoPostBack = true;
                                    sc.CompanyNumberId = cnId;
                                    sc.DataBind();
                                    container.Controls.Add(sc);
                                    sc.SelectedIndexChanged += new EventHandler(sc_SelectedIndexChanged);
                                    
                                    ListSearchExtender lse = new ListSearchExtender();
                                    lse.ID = Guid.NewGuid().ToString().Replace("-", "");
                                    lse.TargetControlID = sc.ID;
                                    lse.SkinID = "4Footer";
                                    container.Controls.Add(lse);
                                    break;
                                }
                            case DataControlRowType.Header:
                                break;
                            case DataControlRowType.Pager:
                                break;
                            case DataControlRowType.Separator:
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                case DataControlRowState.Normal:
                    break;
                case DataControlRowState.Selected:
                    break;
                default:
                    break;
            }
        }

        void sc_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClassCodeDropDownList l = (ClassCodeDropDownList)sender;

            // Get the GridViewRow object that contains the Label control. 
            GridViewRow row = (GridViewRow)l.NamingContainer;

            Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, l.SelectedValue);
            Biz.ClassCode cc = dm.GetClassCode(Biz.FetchPath.ClassCode.SicCodeList,Biz.FetchPath.ClassCode.NaicsCodeList);

            if (cc != null)
            {
                string[] ids = assIdsAndFields;
                try
                {
                    foreach (string ctrlIdAndField in ids)
                    {
                        string[] idAndFieldArray = ctrlIdAndField.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        string ctrlId = string.Empty;
                        string field = string.Empty;

                        if (idAndFieldArray == null || idAndFieldArray.Length == 0)
                            continue;

                        if (string.IsNullOrEmpty(idAndFieldArray[0]))
                            continue;
                        else
                            ctrlId = idAndFieldArray[0];

                        if (idAndFieldArray.Length == 2)
                        {
                            if (string.IsNullOrEmpty(idAndFieldArray[1]))
                                field = "Id";
                            else
                                field = idAndFieldArray[1];
                        }
                        else
                        {
                            field = "Id";
                        }

                        Control c = row.FindControl(ctrlId);

                        System.ComponentModel.AttributeCollection attributes = System.ComponentModel.TypeDescriptor.GetAttributes(c);
                        ControlValuePropertyAttribute attribute = (ControlValuePropertyAttribute)attributes[typeof(ControlValuePropertyAttribute)];

                        string finalField = field;
                        object target = cc;

                        Type t = cc.GetType();

                        while (finalField.Contains("."))
                        {
                            string firstField = finalField.Substring(0, finalField.IndexOf('.'));
                            string restOfField = finalField.Substring(finalField.IndexOf('.') + 1);
                            target = t.InvokeMember(firstField, System.Reflection.BindingFlags.GetProperty, null, target, null);
                            t = t.GetProperty(firstField).PropertyType;
                            finalField = restOfField;
                        }

                        object o = null;

                        if (target != null)
                        {
                            o = t.InvokeMember(finalField, System.Reflection.BindingFlags.GetProperty, null,
                               target, null);

                            c.GetType().GetProperty(attribute.Name).SetValue(c, o.ToString(), null);
                        }
                        else
                            c.GetType().GetProperty(attribute.Name).SetValue(c, o, null);


                    }
                }
                catch (Exception ex)
                { }
            }
            
            ScriptManager scriptManager = ScriptManager.GetCurrent(l.Page);
            if (null != scriptManager)
                scriptManager.SetFocus(l);
            else
                l.Page.SetFocus(l);
        }



        void sc_DataBinding(object sender, EventArgs e)
        {
            DropDownList l = (DropDownList)sender;

            // Get the GridViewRow object that contains the Label control. 
            GridViewRow row = (GridViewRow)l.NamingContainer;

            string value = string.Empty;

            try
            {
                if (DataBinder.Eval(row.DataItem, field) != null)
                    value = DataBinder.Eval(row.DataItem, field).ToString();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                // TODO :: suppressing exception due to one of the class codes
                // for a company number may not have entries in companynumberclasscodehazardgradevalues table
                value = (string)ex.ActualValue;
            }

            if (l.Items.FindByValue(value) != null)
            {
                l.Items.FindByValue(value).Selected = true;
            }
        }
        #endregion
    }
}
