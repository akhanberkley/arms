using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using BCS.WebApp.UserControls;

/// <summary>
/// Summary description for DropDownListTemplate
/// </summary>
namespace BCS.WebApp.Templates
{
    public class ScaleDropDownListTemplate : ITemplate
    {
        private DataControlRowState state;
        private DataControlRowType type;
        private string field;
        private string id;

        private bool _useMethod;

        public bool UseMethod
        {
            get
            { return _useMethod; }
            set
            { _useMethod = value; }
        }

        private string _finalField;

        public string FinalField
        {
            get
            { return _finalField; }
            set
            { _finalField = value; }
        }

        private object[] _methodParameters;

        public object[] MethodParameters
        {
            get
            { return _methodParameters; }
            set
            { _methodParameters = value; }
        }
        private string _methodName;

        public string MethodName
        {
            get
            { return _methodName; }
            set
            { _methodName = value; }
        }

        public ScaleDropDownListTemplate(DataControlRowState templateState, DataControlRowType templateType, string dataField, string controlId)
        {
            type = templateType;
            state = templateState;
            field = dataField;
            id = controlId;
        }

        #region ITemplate Members

        void ITemplate.InstantiateIn(Control container)
        {
            switch (state)
            {
                case DataControlRowState.Alternate:
                    break;
                case DataControlRowState.Edit:
                    {
                        switch (type)
                        {
                            case DataControlRowType.DataRow:
                                {
                                    ScaleDropDownList sc = new ScaleDropDownList();
                                    sc.ID = id;
                                    sc.DataBinding += new EventHandler(sc_DataBinding);
                                    container.Controls.Add(sc);                                    
                                    break;
                                }
                            case DataControlRowType.EmptyDataRow:
                                break;
                            case DataControlRowType.Footer:
                                break;
                            case DataControlRowType.Header:
                                break;
                            case DataControlRowType.Pager:
                                break;
                            case DataControlRowType.Separator:
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                case DataControlRowState.Insert:
                    {
                        switch (type)
                        {
                            case DataControlRowType.DataRow:
                                {
                                    ScaleDropDownList sc = new ScaleDropDownList();
                                    sc.ID = id;
                                    container.Controls.Add(sc);
                                    break;
                                }
                            case DataControlRowType.EmptyDataRow:
                                {
                                    ScaleDropDownList sc = new ScaleDropDownList();
                                    sc.ID = id;
                                    container.Controls.Add(sc);
                                    break;
                                }
                            case DataControlRowType.Footer:
                                {
                                    ScaleDropDownList sc = new ScaleDropDownList();
                                    sc.ID = id;
                                    container.Controls.Add(sc);
                                    break;
                                }
                            case DataControlRowType.Header:
                                break;
                            case DataControlRowType.Pager:
                                break;
                            case DataControlRowType.Separator:
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                case DataControlRowState.Normal:
                    break;
                case DataControlRowState.Selected:
                    break;
                default:
                    break;
            }
        }

        void sc_DataBinding(object sender, EventArgs e)
        {
            DropDownList l = (DropDownList)sender;

            // Get the GridViewRow object that contains the Label control. 
            GridViewRow row = (GridViewRow)l.NamingContainer;

            string value = string.Empty;

            try
            {
                if (UseMethod)
                {
                    object fieldObject = DataBinder.Eval(row.DataItem, field);
                    Type t = fieldObject.GetType();
                    object o = t.InvokeMember(MethodName, System.Reflection.BindingFlags.InvokeMethod, null,
                        fieldObject, MethodParameters);
                    //object no = DataBinder.Eval((o as System.Collections.IList)[0], FinalField);
                    object no = DataBinder.Eval(o, FinalField);
                    l.Text = no.ToString();
                    return;
                }
                if (DataBinder.Eval(row.DataItem, field) != null)
                    value = DataBinder.Eval(row.DataItem, field).ToString();
            }
            catch (Exception)
            {
                // TODO :: suppressing exception due to one of the class codes
                // for a company number may not have entries in companynumberclasscodehazardgradevalues table                
            }

            if (l.Items.FindByValue(value) != null)
            {
                l.Items.FindByValue(value).Selected = true; 
            }
        }

        #endregion
    }
}