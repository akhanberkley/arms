using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCS.WebApp.Templates
{
    /// <summary>
    /// Summary description for ImageTemplate
    /// </summary>
    public class ImageTemplate : ITemplate
    {
       private DataControlRowType templateType;
        private string columnName;
        private string field;
        public ImageTemplate(DataControlRowType type, string headerText, string dataField)
        {
            templateType = type;
            columnName = headerText;
            field = dataField;
        }

        #region ITemplate Members

        void ITemplate.InstantiateIn(Control container)
        {
            switch (templateType)
            {
                case DataControlRowType.Header:
                    // Create the controls to put in the headerText
                    // section and set their properties.
                    Literal lc = new Literal();
                    lc.Text = "<b>" + columnName + "</b>";

                    // Add the controls to the Controls collection
                    // of the container.
                    container.Controls.Add(lc);
                    break;
                case DataControlRowType.EmptyDataRow:
                    // Create the controls to put in a data row
                    // section and set their properties.
                    Image img = new Image();
                    img.ID = "btnHistory";
                    img.ImageUrl = Common.GetImageUrl("folder_find.gif");

                    //// To support data binding, register the event-handling methods
                    //// to perform the data binding. Each control needs its own event
                    //// handler.
                    //firstName.DataBinding += new EventHandler(this.FirstName_DataBinding);
                    //lastName.DataBinding += new EventHandler(this.LastName_DataBinding);

                    //// Add the controls to the Controls collection
                    //// of the container.
                    //container.Controls.Add(firstName);
                    //container.Controls.Add(spacer);
                    //container.Controls.Add(lastName);
                    container.Controls.Add(img);
                    break;

                // Insert cases to create the content for the other 
                // row types, if desired.

                default:
                    // Insert code to handle unexpected values.
                    break;
            }
        }

        #endregion
    }
}