using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCS.WebApp.Templates
{
    /// <summary>
    /// Summary description for PagerTemplate. Provides a Numeric, First, Last, Previous, Next, ellipses. 
    /// basically a combination of paging mechanisms not provided by Default gridView
    /// </summary>
    public class PagerTemplate : ITemplate
    {
        public PagerTemplate()
        { }

        #region ITemplate Members

        public void InstantiateIn(Control container)
        {
            PlaceHolder phPager = new PlaceHolder();
            phPager.Init += new EventHandler(phPager_Init);
            container.Controls.Add(phPager);
        }

        protected void phPager_Init(object sender, EventArgs e)
        {
            PlaceHolder ph = (PlaceHolder)sender;
            GridViewRow gridRow = (GridViewRow)ph.NamingContainer;
            GridView grid = (GridView)gridRow.NamingContainer;


            int buttonCount = grid.PagerSettings.PageButtonCount;

            Table table = new Table();
            Common.AddControl(ph, table, false, false);

            TableRow row = new TableRow();
            table.Rows.Add(row);

            #region FirstPrev
            // first page link 
            TableCell cell = new TableCell();
            WebControl lb = (WebControl)GetPagerButton(grid, "First");
            (lb as IButtonControl).CommandArgument = "First";
            if(lb is LinkButton)
                lb.Attributes.Add("href", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, (lb as IButtonControl).CommandArgument));
            if(lb is ImageButton)
                lb.Attributes.Add("onclick", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, (lb as IButtonControl).CommandArgument));
            lb.Visible = !(IsInRange(grid.PageIndex, 0, buttonCount));
            Common.AddControl(cell, lb, false, false);
            row.Cells.Add(cell);

            // prev page link
            cell = new TableCell();
            lb = (WebControl)GetPagerButton(grid, "Prev");
            (lb as IButtonControl).CommandArgument = "Prev";
            lb.Attributes.Add("href", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, (lb as IButtonControl).CommandArgument));
            if (lb is ImageButton)
                lb.Attributes.Add("onclick", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, (lb as IButtonControl).CommandArgument));
            lb.Visible = grid.PageIndex > 0;
            Common.AddControl(cell, lb, false, false);
            row.Cells.Add(cell);
            #endregion

            #region PrevEllipses
            TableCell cellEllPrev = new TableCell();
            LinkButton lbEllPrev = GetPagerLinkButton("...");
            cellEllPrev.Controls.Add(lbEllPrev);
            row.Cells.Add(cellEllPrev);
            #endregion

            #region NextEllipses
            TableCell cellEllNext = new TableCell();
            LinkButton lbEllNext = GetPagerLinkButton("...");
            cellEllNext.Controls.Add(lbEllNext);
            #endregion

            #region Remaining things
            int start = 0;
            int end = buttonCount;

            if (IsInRange(grid.PageIndex, 0, Math.Min(buttonCount, grid.PageCount)))
            {
                lbEllPrev.Visible = false;
            }
            else
            {
                start = grid.PageIndex;
                end = start + buttonCount;
            }
            if (IsInRange(grid.PageIndex, grid.PageCount - buttonCount, grid.PageCount))
            {
                start = grid.PageCount - buttonCount;
                end = grid.PageCount;
                lbEllNext.Visible = false;
            }
            lbEllPrev.CommandArgument = (start - buttonCount + 1) < 1 ? 1.ToString() : (start - buttonCount + 1).ToString();
            if(lbEllPrev is LinkButton)
                lbEllPrev.Attributes.Add("href", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, lbEllPrev.CommandArgument));
            if (lbEllPrev is ImageButton)
                lbEllPrev.Attributes.Add("onclick", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, lbEllPrev.CommandArgument));
            lbEllNext.CommandArgument = (start + buttonCount + 1).ToString();
            if(lbEllNext is LinkButton)
                lbEllNext.Attributes.Add("href", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, lbEllNext.CommandArgument));
            if (lb is ImageButton)
                lbEllNext.Attributes.Add("onclick", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, lbEllNext.CommandArgument));
            if (start < 0)
            {
                start = 0;
                end = Math.Min(grid.PageCount, buttonCount);
            }
            for (int i = start; i < end; i++)
            {
                int pageNumber = i + 1;
                cell = new TableCell();
                if (i == grid.PageIndex)
                {
                    Label l = new Label();
                    l.Text = pageNumber.ToString();
                    cell.Controls.Add(l);
                    row.Cells.Add(cell);
                    continue;
                }

                lb = GetPagerLinkButton(pageNumber.ToString());
                (lb as IButtonControl).CommandArgument = pageNumber.ToString();
                if(lb is LinkButton)
                    lb.Attributes.Add("href", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, (lb as IButtonControl).CommandArgument));
                if(lb is ImageButton)
                    lb.Attributes.Add("onclick", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, (lb as IButtonControl).CommandArgument));
                cell.Controls.Add(lb);
                row.Cells.Add(cell);
            }
            // this needs to be added after numeric pager controls
            row.Cells.Add(cellEllNext);
            #endregion

            #region NextLast
            // next page link
            cell = new TableCell();
            lb = (WebControl)GetPagerButton(grid, "Next");
            (lb as IButtonControl).CommandArgument = "Next";
            if(lb is LinkButton)
                lb.Attributes.Add("href", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, (lb as IButtonControl).CommandArgument));
            if(lb is ImageButton)
                lb.Attributes.Add("onclick", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, (lb as IButtonControl).CommandArgument));
            lb.Visible = grid.PageIndex != grid.PageCount - 1;
            Common.AddControl(cell, lb, false, false);
            row.Cells.Add(cell);

            // last page link 
            cell = new TableCell();
            lb = (WebControl)GetPagerButton(grid, "Last");
            (lb as IButtonControl).CommandArgument = "Last";
            if (lb is LinkButton)
                lb.Attributes.Add("href", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, (lb as IButtonControl).CommandArgument));
            if (lb is ImageButton)
                lb.Attributes.Add("onclick", string.Format("javascript:__doPostBack('{0}','Page${1}');", grid.UniqueID, (lb as IButtonControl).CommandArgument));
            lb.Visible = !(IsInRange(grid.PageIndex, grid.PageCount - buttonCount, grid.PageCount));
            Common.AddControl(cell, lb, false, false);
            row.Cells.Add(cell);
            #endregion
        }

        private bool IsInRange(int p, int p_2, int p_3)
        {
            return p >= p_2 && p < p_3;
        }
        private LinkButton GetPagerLinkButton(string text)
        {
            LinkButton lb = new LinkButton();
            lb.Text = text;
            lb.CommandName = "Page";
            lb.CausesValidation = false;

            return lb;
        }

        private IButtonControl GetPagerButton(GridView gv, string type)
        {
            IButtonControl ibc;
            switch (type)
            {
                case "First":
                    {
                        if (!string.IsNullOrEmpty(gv.PagerSettings.FirstPageImageUrl))
                        {
                            ImageButton ib = new ImageButton();
                            ib.ImageUrl = gv.PagerSettings.FirstPageImageUrl;
                            if (!string.IsNullOrEmpty(gv.PagerSettings.FirstPageText))
                                ib.AlternateText = gv.PagerSettings.FirstPageText;
                            else
                                ib.AlternateText = type;

                            ibc = ib;
                        }
                        else
                        {
                            LinkButton lb = new LinkButton();
                            if (!string.IsNullOrEmpty(gv.PagerSettings.FirstPageText))
                                lb.Text = gv.PagerSettings.FirstPageText;
                            else
                                lb.Text = type;

                            ibc = lb;
                        }
                        break;
                    }
                case "Last":
                    {
                        if (!string.IsNullOrEmpty(gv.PagerSettings.LastPageImageUrl))
                        {
                            ImageButton ib = new ImageButton();
                            ib.ImageUrl = gv.PagerSettings.LastPageImageUrl;
                            if (!string.IsNullOrEmpty(gv.PagerSettings.LastPageText))
                                ib.AlternateText = gv.PagerSettings.LastPageText;
                            else
                                ib.AlternateText = type;

                            ibc = ib;
                        }
                        else
                        {
                            LinkButton lb = new LinkButton();
                            if (!string.IsNullOrEmpty(gv.PagerSettings.LastPageText))
                                lb.Text = gv.PagerSettings.LastPageText;
                            else
                                lb.Text = type;

                            ibc = lb;
                        }
                        break;
                    }
                case "Prev":
                    {
                        if (!string.IsNullOrEmpty(gv.PagerSettings.PreviousPageImageUrl))
                        {
                            ImageButton ib = new ImageButton();
                            ib.ImageUrl = gv.PagerSettings.PreviousPageImageUrl;
                            if (!string.IsNullOrEmpty(gv.PagerSettings.PreviousPageText))
                                ib.AlternateText = gv.PagerSettings.PreviousPageText;
                            else
                                ib.AlternateText = type;

                            ibc = ib;
                        }
                        else
                        {
                            LinkButton lb = new LinkButton();
                            if (!string.IsNullOrEmpty(gv.PagerSettings.PreviousPageText))
                                lb.Text = gv.PagerSettings.PreviousPageText;
                            else
                                lb.Text = type;

                            ibc = lb;
                        }
                        break;
                    }
                case "Next":
                    {
                        if (!string.IsNullOrEmpty(gv.PagerSettings.NextPageImageUrl))
                        {
                            ImageButton ib = new ImageButton();
                            ib.ImageUrl = gv.PagerSettings.NextPageImageUrl;
                            if (!string.IsNullOrEmpty(gv.PagerSettings.NextPageText))
                                ib.AlternateText = gv.PagerSettings.NextPageText;
                            else
                                ib.AlternateText = type;

                            ibc = ib;
                        }
                        else
                        {
                            LinkButton lb = new LinkButton();
                            if (!string.IsNullOrEmpty(gv.PagerSettings.NextPageText))
                                lb.Text = gv.PagerSettings.NextPageText;
                            else
                                lb.Text = type;

                            ibc = lb;
                        }
                        break;
                    }
                default:
                    {
                        throw new ApplicationException("Not Supported");
                    }
            }
            ibc.CommandName = "Page";
            ibc.CausesValidation = false;
            return ibc;
        }

        #endregion
    }
}