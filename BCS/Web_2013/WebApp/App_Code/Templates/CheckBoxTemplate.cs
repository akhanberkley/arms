using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCS.WebApp.Templates
{
    public enum HeaderType
    {
        /// <summary>
        /// select all header
        /// </summary>
        SelectAll = 1,        
        Text
    }
    /// <summary>
    /// Summary description for CheckBoxTemplate
    /// </summary>
    public class CheckBoxTemplate : ITemplate
    {
        private DataControlRowType templateType;
        private HeaderType headerType;
        private string columnName;
        private string field;

        public CheckBoxTemplate(DataControlRowType type, HeaderType headertype, string header, string dataField )
        {
            templateType = type;
            columnName = header;
            field = dataField;
            headerType = headertype;
        }

        #region ITemplate Members

        void ITemplate.InstantiateIn(Control container)
        {
                // Create the content for the different row types.
            switch (templateType)
            {
                case DataControlRowType.Header:
                    switch (headerType)
                    {
                        case HeaderType.SelectAll:
                            {
                                CheckBox cbSelectAll = new CheckBox();
                                cbSelectAll.ID = "cbSelectAll";
                                cbSelectAll.Init += new EventHandler(cbSelectAll_Init);

                                container.Controls.Add(cbSelectAll);
                                break;
                            }
                        case HeaderType.Text:
                            {
                                // Create the controls to put in the header
                                // section and set their properties.
                                Literal lc = new Literal();
                                lc.Text = "<b>" + columnName + "</b>";

                                // Add the controls to the Controls collection
                                // of the container.
                                container.Controls.Add(lc);
                                break;
                            }
                        default:
                            break;
                    }
                    
                    break;
                case DataControlRowType.EmptyDataRow:
                    // Create the controls to put in a data row
                    // section and set their properties.
                    CheckBox cb = new CheckBox();
                    cb.ID = "cbDup";

                    //// To support data binding, register the event-handling methods
                    //// to perform the data binding. Each control needs its own event
                    //// handler.
                    //firstName.DataBinding += new EventHandler(this.FirstName_DataBinding);
                    //lastName.DataBinding += new EventHandler(this.LastName_DataBinding);

                    //// Add the controls to the Controls collection
                    //// of the container.
                    //container.Controls.Add(firstName);
                    //container.Controls.Add(spacer);
                    //container.Controls.Add(lastName);
                    container.Controls.Add(cb);
                    break;
                case DataControlRowType.DataRow:
                    // Create the controls to put in a data row
                    // section and set their properties.
                    cb = new CheckBox();
                    cb.ID = "cbDup";

                    // To support data binding, register the event-handling methods
                    // to perform the data binding. Each control needs its own event
                    // handler.
                    cb.DataBinding += new EventHandler(cb_DataBinding);

                    //// Add the controls to the Controls collection
                    //// of the container.
                    //container.Controls.Add(firstName);
                    //container.Controls.Add(spacer);
                    //container.Controls.Add(lastName);
                    container.Controls.Add(cb);
                    break;

                // Insert cases to create the content for the other 
                // row types, if desired.

                default:
                    // Insert code to handle unexpected values.
                    break;
            }
        }

        void cbSelectAll_Init(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            cb.AutoPostBack = true;
            cb.CheckedChanged += new EventHandler(cb_CheckedChanged);
        }

        void cb_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            GridViewRow row = (GridViewRow)cb.NamingContainer;
            GridView grid = (GridView)row.NamingContainer;
            foreach(GridViewRow gvr in grid.Rows)
            {
                CheckBox cbr = gvr.FindControl("cbDup") as CheckBox;
                cbr.Checked = cb.Checked;
            }            
        }

        void cb_DataBinding(object sender, EventArgs e)
        {
            // Get the Label control to bind the value. The Label control
            // is contained in the object that raised the DataBinding 
            // event (the sender parameter).
            CheckBox cb = (CheckBox)sender;

            // Get the GridViewRow object that contains the Label control. 
            GridViewRow row = (GridViewRow)cb.NamingContainer;

            // Get the field value from the GridViewRow object and 
            // assign it to the Text property of the Label control.
            cb.Checked = Convert.ToBoolean(DataBinder.Eval(row.DataItem, field));            
        }

        #endregion
    }
}
