using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for CommandTemplate
/// </summary>
namespace BCS.WebApp.Templates
{
    /// <summary>
    /// Summary description for ImageTemplate
    /// </summary>
    public class CommandTemplate : ITemplate
    {
        private DataControlRowType templateType;
        private DataControlRowState templateState;
        private string columnName;

        public CommandTemplate(DataControlRowType type, DataControlRowState state, string headerText)
        {
            templateType = type;
            templateState = state;
            columnName = headerText;
        }

        #region ITemplate Members

        void ITemplate.InstantiateIn(Control container)
        {
            switch (templateType)
            {
                case DataControlRowType.Header:
                    // Create the controls to put in the headerText
                    // section and set their properties.
                    Literal lc = new Literal();
                    lc.Text = "<b>" + columnName + "</b>";

                    // Add the controls to the Controls collection
                    // of the container.
                    container.Controls.Add(lc);
                    break;
                case DataControlRowType.Footer:
                    // Create the controls to put in a data row
                    // section and set their properties.
                    Button b = new Button();
                    b.CommandName = "New";
                    b.Text = "Add";
                    container.Controls.Add(b);

                    b = new Button();
                    b.Text = "Cancel";
                    b.CommandName = "Cancel";
                    b.Click += new EventHandler(Cancel_Click);
                    container.Controls.Add(b);

                    break;

                case DataControlRowType.DataRow:
                    {
                        switch (templateState)
                        {
                            case DataControlRowState.Alternate:
                                break;
                            case DataControlRowState.Edit:
                                {
                                    b = new Button();
                                    b.CommandName = "Update";
                                    b.Text = "Update";
                                    container.Controls.Add(b);

                                    b = new Button();
                                    b.CommandName = "Cancel";
                                    b.Text = "Cancel";
                                    container.Controls.Add(b);
                                    break;
                                }
                            case DataControlRowState.Insert:
                                break;
                            case DataControlRowState.Normal:
                                {
                                    b = new Button();
                                    b.CommandName = "Edit";
                                    b.Text = "Edit";
                                    container.Controls.Add(b);
                                    break;
                                }
                            case DataControlRowState.Selected:
                                break;
                            default:
                                break;
                        }

                        break;
                    }
                // Insert cases to create the content for the other 
                // row types, if desired.

                default:
                    // Insert code to handle unexpected values.
                    break;
            }
        }

        void Cancel_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            // Get the GridViewRow object that contains the Label control. 
            GridViewRow row = (GridViewRow)b.NamingContainer;

            ((GridView)row.NamingContainer).ShowFooter = false;
            //((GridView)row.NamingContainer).DataBind();
        }

        #endregion
    }
}
