using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BCS.WebApp.Templates
{
    enum FormatType
    {
        /// <summary>
        /// MM/dd/yyyy
        /// </summary>
        Date = 1,
        /// <summary>
        /// $#.##
        /// </summary>
        Money,
        Bit,
        Text,
        ClipboardCopy
    }
    /// <summary>
    /// Summary description for GenericTemplate
    /// </summary>
    public class GenericTemplate : ITemplate
    {
        private DataControlRowType templateType;
        private string columnName;
        private string field;
        private string id;
        private FormatType formattype = FormatType.Text;
       
        private bool _useMethod;

        public bool UseMethod
        {
            get
            { return _useMethod; }
            set
            { _useMethod = value; }
        }

        private string _finalField;

        public string FinalField
        {
            get
            { return _finalField; }
            set
            { _finalField = value; }
        }

        private object[] _methodParameters;

        public object[] MethodParameters
        {
            get
            { return _methodParameters; }
            set
            { _methodParameters = value; }
        }
        private string _methodName;

        public string MethodName
        {
            get
            { return _methodName; }
            set
            { _methodName = value; }
        }
        

        public GenericTemplate(DataControlRowType type, string headerText, string dataField, string controlId)
        {
            templateType = type;
            columnName = headerText;
            field = dataField;
            id = controlId;
        }

        public GenericTemplate(DataControlRowType type, string headerText, string dataField, string formatType, string controlId)
        {
            templateType = type;
            columnName = headerText;
            field = dataField;
            id = controlId;
            try
            {
                formattype = (FormatType)Enum.Parse(typeof(FormatType), formatType, true);
            }
            catch (Exception) { }
        }

        #region ITemplate Members

        void ITemplate.InstantiateIn(Control container)
        {
            switch (templateType)
            {
                case DataControlRowType.Header:
                    // Create the controls to put in the headerText
                    // section and set their properties.
                    Literal lc = new Literal();
                    lc.Text = "<b>" + columnName + "</b>";

                    // Add the controls to the Controls collection
                    // of the container.
                    container.Controls.Add(lc);
                    break;

                case DataControlRowType.Footer:
                    // Create the controls to put in the headerText
                    // section and set their properties.
                    Label l = new Label();
                    if (id != null && id.Length > 0)
                        l.ID = id;

                    // Add the controls to the Controls collection
                    // of the container.
                    container.Controls.Add(l);
                    break;

                case DataControlRowType.DataRow:
                    switch (formattype)
                    {
                        case FormatType.Date:
                        case FormatType.Money:
                        case FormatType.Text:
                            {
                                // Create the controls to put in a data row
                                // section and set their properties.
                                l = new Label();
                                if (id != null && id.Length > 0)
                                    l.ID = id;

                                // To support data binding, register the event-handling methods
                                // to perform the data binding. Each control needs its own event
                                // handler.
                                l.DataBinding += new EventHandler(l_DataBinding);

                                container.Controls.Add(l);
                                break;
                            }
                        case FormatType.Bit:
                            // Create the controls to put in a data row
                            // section and set their properties.
                            CheckBox cb = new CheckBox();
                            if (id != null && id.Length > 0)
                                cb.ID = id;

                            // To support data binding, register the event-handling methods
                            // to perform the data binding. Each control needs its own event
                            // handler.
                            cb.DataBinding += new EventHandler(cb_DataBinding);

                            container.Controls.Add(cb);
                            break;
                        case FormatType.ClipboardCopy:
                            {
                                ImageButton btn = new ImageButton();
                                btn.ImageUrl = Common.GetImageUrl("copy.gif");
                                btn.DataBinding += new EventHandler(cbc_DataBinding);

                                btn.ToolTip = string.Format("Copy {0} to ClipBoard",field.Replace('_', ' '));

                                container.Controls.Add(btn);
                                break;
                            }
                        default:
                            break;
                    }
                    break;
                    

                // Insert cases to create the content for the other 
                // row types, if desired.

                default:
                    // Insert code to handle unexpected values.
                    break;
            }
        }

        void l_DataBinding(object sender, EventArgs e)
        {
            // Get the Label control to bind the value. The Label control
            // is contained in the object that raised the DataBinding 
            // event (the sender parameter).
            Label l = (Label)sender;

            // Get the GridViewRow object that contains the Label control. 
            GridViewRow row = (GridViewRow)l.NamingContainer;

            // Get the field value from the GridViewRow object and 
            // assign it to the Text property of the Label control.
            switch (formattype)
            {
                case FormatType.Date:
                    {
                        try
                        {
                            DateTime dt = Convert.ToDateTime(DataBinder.Eval(row.DataItem, field));
                            l.Text = dt == DateTime.MinValue ? string.Empty : dt.ToString("MM/dd/yyyy");
                        }
                        catch (Exception ex)
                        { 
                            // TODO: commented to avoid generating too many log entries
                            //BTS.LogFramework.LogCentral.Current.LogWarn(ex); 
                        }
                        break;
                    }
                case FormatType.Money:
                    {
                        try
                        {
                            decimal dt = Convert.ToDecimal(DataBinder.Eval(row.DataItem, field));
                            l.Text = string.Format("{0:c}", dt);
                        }
                        catch (Exception ex)
                        { 
                            // TODO: commented to avoid generating too many log entries
                            //BTS.LogFramework.LogCentral.Current.LogWarn(ex);
                        }
                        break;
                    }
                case FormatType.Text:
                    {
                        try
                        {
                            if (UseMethod)
                            {
                                object fieldObject = DataBinder.Eval(row.DataItem, field);
                                Type t = fieldObject.GetType();
                                object o = t.InvokeMember(MethodName, System.Reflection.BindingFlags.InvokeMethod, null,
                                   fieldObject, MethodParameters);
                                //object no = DataBinder.Eval((o as System.Collections.IList)[0], FinalField);
                                object no = DataBinder.Eval(o , FinalField);
                                l.Text = no.ToString();
                                
                                break;
                            }

                            l.Text = DataBinder.Eval(row.DataItem, field).ToString();
                        }
                        catch (Exception ex)
                        {
                            // TODO: commented to avoid generating too many log entries
                            //BTS.LogFramework.LogCentral.Current.LogWarn(ex);
                        } 
                        break;
                    }
                default:
                    break;
            }

        }

        void cbc_DataBinding(object sender, EventArgs e)
        {
            ImageButton btn = (ImageButton)sender;

            GridViewRow row = (GridViewRow)btn.NamingContainer;

            try
            {
                btn.OnClientClick = string.Format("javascript:copyErrorToClipboard('{0}');return false;", DataBinder.Eval(row.DataItem, field).ToString());
            }
            catch (Exception ex)
            {
                // To do:
            }
        }

        void cb_DataBinding(object sender, EventArgs e)
        {
            // Get the Label control to bind the value. The CheckBox control
            // is contained in the object that raised the DataBinding 
            // event (the sender parameter).
            CheckBox cb = (CheckBox)sender;

            // Get the GridViewRow object that contains the CheckBox control. 
            GridViewRow row = (GridViewRow)cb.NamingContainer;

            try
            {
                cb.Checked = Convert.ToBoolean(DataBinder.Eval(row.DataItem, field));
            }
            catch (Exception ex)
            {
                // TODO: commented to avoid generating too many log entries
                //BTS.LogFramework.LogCentral.Current.LogWarn(ex);
            }

        }

        #endregion
    }
}