using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Security.Principal;
using System.Web;
using System.Web.SessionState;

using BCS.Biz;
using BCS.Core;
using BTS.LogFramework;

using OrmLib;
using TSHAK.Components;
using BCS.WebApp.Components;
using BCS.Core.Security;

namespace BCS.WebApp 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		
		public Global()
		{
            
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{            
            Application["ActiveUsers"] = 0;
            BTS.WFA.BCS.Application.Initialize<BTS.WFA.BCS.Data.SqlDb>();
		}
 
		protected void Session_Start(Object sender, EventArgs e)
        {
            Application.Lock();
            Application["ActiveUsers"] = Convert.ToInt32(Application["ActiveUsers"]) + 1;
            Application.UnLock();   

            UserCompanyCollection userCompanies = Components.Security.GetUserCompanies(User.Identity.Name);
            if (userCompanies == null)
            {
                // unconfigured roles
                TSHAK.Components.SecureQueryString qs = new TSHAK.Components.SecureQueryString(BCS.Core.Core.Key);
                qs["SecurityEvent"] = "Your account has no companies associated with it.";
                LogCentral.Current.LogWarn(string.Format("Unconfigured user : Username {0}", User.Identity.Name));
                Response.Redirect("~/NoAccess.aspx?data=" + HttpUtility.UrlEncode(qs.ToString()));
                // return stmt for cache manager
                return;
            }
            if (userCompanies.Count == 1)
            {
                Common.AddCookie(SessionKeys.CompanyId, userCompanies[0].CompanyId.ToString());
                Session[SessionKeys.CompanyId] = userCompanies[0].CompanyId.ToString();
                BCS.Core.Clearance.BizObjects.CompanyNumberList userCompanyNumbers =
                    Core.Clearance.Lookups.GetCompanyNumbersInternal(userCompanies[0].CompanyId);

                if (userCompanyNumbers.List.Length == 1)
                {
                    Common.AddCookie(SessionKeys.CompanyNumberId, userCompanyNumbers.List[0].Id.ToString());
                    Session[SessionKeys.CompanyNumberId] = userCompanyNumbers.List[0].Id.ToString();
                }
            }
		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}
        protected void Application_OnPostAuthenticateRequest(object sender, EventArgs e)
        {
        }
		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{
			if (HttpContext.Current.Request.IsAuthenticated)
			{
                if (null == (User as CustomPrincipal))
                {
                    long agency = 0;
                    Core.Security.CustomPrincipal principal = new BCS.Core.Security.CustomPrincipal(User.Identity,
                        Components.Security.GetUserRoles(User.Identity.Name, out agency), agency);

                    HttpContext.Current.User = principal;
                }
			}
		}
        
		protected void Application_Error(Object sender, EventArgs e)
		{
            //BCS.Core.Security.CustomPrincipal user = User as BCS.Core.Security.CustomPrincipal;
            string pageUrl = HttpContext.Current.Request.Path;
            Exception errorInfo = HttpContext.Current.Server.GetLastError();

            string message = "Url: " + pageUrl + Environment.NewLine + " Error: " + errorInfo.ToString();
            try
            {
                if (HttpContext.Current.Session["CompanyNumberSelected"] != null)
                    message = string.Format("[{0}] {1}", "CompanyNumberId :" + HttpContext.Current.Session["CompanyNumberSelected"].ToString(),
                        message);
                if (HttpContext.Current.Session[SessionKeys.CompanyNumber] != null)
                    message = string.Format("[{0}] {1}", "CompanyNumber :" + HttpContext.Current.Session[SessionKeys.CompanyNumber].ToString(),
                        message);
                if (pageUrl == "/BCS/Submission.aspx" || pageUrl == "/Submission.aspx")
                    if (HttpContext.Current.Session["SubmissionId"] != null)
                        message = string.Format("[{0}] {1}", "SubmissionId : " + HttpContext.Current.Session["SubmissionId"].ToString(),
                            message);

                message = string.Format("[{0}] {1}", "User : " + HttpContext.Current.User.Identity.Name,
                                message);

//#if DEBUG
                HttpContext.Current.Session["Application_Error"] = message;
//#endif

            }
            catch { };

            string msg = string.Empty;  
            // Log to the LogFramework:
            if (message.Contains("The client disconnected."))
            {    // does not affect the user experience.
                //LogCentral.Current.LogWarn(message, errorInfo);
            }
            else
            {
                LogCentral.Current.LogFatal(message, errorInfo);
                msg = QCI.ExceptionManagement.ExceptionManager.PublishEmail(errorInfo);
                //Email.SendErrorEmail(errorInfo);
            }
#if !DEBUG
            HttpContext.Current.Server.ClearError();
            Response.Redirect("~/Error.aspx?page=" +
                    HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.Url.ToString()) + "&msg=" + msg);
#endif
		}

		protected void Session_End(Object sender, EventArgs e)
		{
            Application.Lock();
            Application["ActiveUsers"] = Convert.ToInt32(Application["ActiveUsers"]) - 1;
            Application.UnLock();
		}

		protected void Application_End(Object sender, EventArgs e)
		{

        }
        public override string GetVaryByCustomString(HttpContext context, string custom)
        {            
            try
            {
                switch (custom)
                {
                    case "UserRoles":
                        {
                            // TODO: proposed on menu user control, Policy Number Generator issue : does not work on cached copy
                            // consider moving of the Policy Number Generator to a control right beside menu control
                            BCS.Core.Security.CustomPrincipal user = context.User as BCS.Core.Security.CustomPrincipal;
                            return string.Join(",", user.Roles);
                        }
                    case "UserCompanies":
                        {
                            // TODO: proposed on company selector user control, Master.FindControl("CompanySelector1").FindControl("CompanyDropDownList1"),
                            // does not seem to work when using this (can be tested from subsearch.aspx). extracting the same values
                            // from session (since the control is already loaded) should be enough. THIS SHOULD BE A BIG PERFORMANCE GAIN.
                            UserCompanyCollection userCompanies = Components.Security.GetUserCompanies(context.User.Identity.Name);
                            System.Text.StringBuilder sb = new System.Text.StringBuilder();                            
                            foreach (UserCompany uc in userCompanies)
                            {
                                sb.AppendFormat("{0}", uc.CompanyId);
                            }
                            sb.AppendFormat("{0}{1}", Common.GetSafeIntFromSession("CompanySelected"), Common.GetSafeIntFromSession("CompanyNumberSelected"));
                            return sb.ToString();
                        }
                    default:
                        return base.GetVaryByCustomString(context, custom);                        
                }
            }
            catch (Exception)
            {
                return base.GetVaryByCustomString(context, custom);
            }
        }		
	}
}

