using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BTS.ClientCore.Wrapper.Structures.Search;
using System.Collections.Generic;

/// <summary>
/// structure to hold and restore search criteria, to be replaced by SearchCriteria
/// </summary>
public struct SavedSearch
{
    /// <summary>
    /// container to store client attributes on which search was based.
    /// </summary>
    public ClientSearch SearchedClient;

    /// <summary>
    /// container to store if searched on policy number
    /// </summary>
    public string PolicyNumber;

    /// <summary>
    /// container to store if searched on multiple taxids
    /// </summary>
    public string TaxIds;

    /// <summary>
    /// container to store if searched on multiple PortfolioIds
    /// </summary>
    public string PortfolioIds;

    /// <summary>
    /// container to store if a client was selected. (example:- when canceled from submission screen, need to restore the selected client)
    /// </summary>
    public string SelectedClientId;

    /// <summary>
    /// container to store if a client address was selected. (example:- when canceled from submission screen, need to restore the selected client and address)
    /// </summary>
    public string SelectedClientAddressId;

}

public struct ClientNameSearch
{
    public ClientName ClientName;

    public string City;

    public string State;
}

public struct ClientSearch
{
    // this is copied from client structure from client core wrapper
    public ClientInfo Info;

    //[XmlArray("communications"), XmlArrayItem("communication", typeof(Communication))]
    //public Communication[] Communications;

    public ClientNameSearch[] Names;

    public AddressVersion[] Addresses;

    public GroupOwnerInfoDO Group;
}



#region helper classes
[Serializable]
public class SearchName
{
    private int id;

    public int Id
    {
        get { return id; }
        set { id = value; }
    }
    private string name, city, state;

    public string State
    {
        get { return state; }
        set { state = value; }
    }

    public string City
    {
        get { return city; }
        set { city = value; }
    }

    public string Name
    {
        get { return name; }
        set { name = value; }
    }
}
[Serializable]
public class SearchAddress
{
    private int id;

    public int Id
    {
        get { return id; }
        set { id = value; }
    }

    private string number, name, city, state, zip;

    public string Number
    {
        get { return number; }
        set { number = value; }
    }

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    public string City
    {
        get { return city; }
        set { city = value; }
    }

    public string State
    {
        get { return state; }
        set { state = value; }
    }

    public string Zip
    {
        get { return zip; }
        set { zip = value; }
    }
}
[Serializable]
public class SearchSubmissionNumber
{
    private string submissionNumber, sequence;

    public string SubmissionNumber
    {
        get { return submissionNumber; }
        set { submissionNumber = value; }
    }

    public string Sequence
    {
        get { return sequence; }
        set { sequence = value; }
    }
}
[Serializable]
public class SearchPortfolio
{
    private string text, type;

    public string Text
    {
        get { return text; }
        set { text = value; }
    }

    public string Type
    {
        get { return type; }
        set { type = value; }
    }
}
[Serializable]
public class SearchCriteria
{
    private SearchAddress[] addresses;

    public SearchAddress[] Addresses
    {
        get { return addresses; }
        set { addresses = value; }
    }
    private SearchName[] names;

    public SearchName[] Names
    {
        get { return names; }
        set { names = value; }
    }
    private SearchSubmissionNumber submissionNumber;

    public SearchSubmissionNumber SubmissionNumber
    {
        get { return submissionNumber; }
        set { submissionNumber = value; }
    }
    private string clientId;

    public string ClientId
    {
        get { return clientId; }
        set { clientId = value; }
    }
    private string taxIds;

    public string TaxIds
    {
        get { return taxIds; }
        set { taxIds = value; }
    }
    private string policyNumber;

    public string PolicyNumber
    {
        get { return policyNumber; }
        set { policyNumber = value; }
    }

    private SearchPortfolio portfolio;

    public SearchPortfolio Portfolio
    {
        get { return portfolio; }
        set { portfolio = value; }
    }

    private string selectedClientId;

    public string SelectedClientId
    {
        get { return selectedClientId; }
        set { selectedClientId = value; }
    }
    private string selectedClientAddressId;

    public string SelectedClientAddressId
    {
        get { return selectedClientAddressId; }
        set { selectedClientAddressId = value; }
    }

    public ReassignScreen ReassignScreen;
}
public class ReassignScreen
{
    private string drivenBy, fromEff, toEff, fromExp, toExp, premiumComparisonType, possibleUpdates, selectedAll, sort,
        submissionDtFrom, submissionDtTo;
    private int pageIndex;

    public string SubmissionDateFrom
    {
        get { return submissionDtFrom; }
        set { submissionDtFrom = value; }
    }
    public string SubmissionDateTo
    {
        get { return submissionDtTo; }
        set { submissionDtTo = value; }
    }

    public string ModifiedDateFrom
    {
        get;
        set;
    }

    public string ModifiedDateTo
    {
        get;
        set;
    }

    public string CreatedByUser
    {
        get;
        set;
    }

    public string ModifiedByUser
    {
        get;
        set;
    }

    public System.Collections.Generic.List<KeyValuePair<string, string>> Attributes
    {
        get;set;
    }

    public int PageIndex
    {
        get { return pageIndex; }
        set { pageIndex = value; }
    }

    public string Sort
    {
        get { return sort; }
        set { sort = value; }
    }

    public string SelectedAll
    {
        get { return selectedAll; }
        set { selectedAll = value; }
    }

    public string PossibleUpdates
    {
        get { return possibleUpdates; }
        set { possibleUpdates = value; }
    }

    public string DrivenBy
    {
        get { return drivenBy; }
        set { drivenBy = value; }
    }

    public string FromEff
    {
        get { return fromEff; }
        set { fromEff = value; }
    }

    public string ToEff
    {
        get { return toEff; }
        set { toEff = value; }
    }

    public string FromExp
    {
        get { return fromExp; }
        set { fromExp = value; }
    }

    public string ToExp
    {
        get { return toExp; }
        set { toExp = value; }
    }

    public string PremiumComparisonType
    {
        get { return premiumComparisonType; }
        set { premiumComparisonType = value; }
    }

    private decimal premium;

    public decimal Premium
    {
        get { return premium; }
        set { premium = value; }
    }

    private string roleToReassign;

    public string RoleToReassign
    {
        get { return roleToReassign; }
        set { roleToReassign = value; }
    }
    private int agencyId, underwriterId1, lobId, typeId, statusId, underwriterId2, underwriterId3, analystId, technicianId;

    public int AgencyId
    {
        get { return agencyId; }
        set { agencyId = value; }
    }

    public int UnderwriterId1
    {
        get { return underwriterId1; }
        set { underwriterId1 = value; }
    }

    public int LobId
    {
        get { return lobId; }
        set { lobId = value; }
    }

    public int TypeId
    {
        get { return typeId; }
        set { typeId = value; }
    }

    public int StatusId
    {
        get { return statusId; }
        set { statusId = value; }
    }

    public int UnderwriterId2
    {
        get { return underwriterId2; }
        set { underwriterId2 = value; }
    }

    public int UnderwriterId3
    {
        get { return underwriterId3; }
        set { underwriterId3 = value; }
    }

    public int AnalystId
    {
        get { return analystId; }
        set { analystId = value; }
    }

    public int TechnicianId
    {
        get { return technicianId; }
        set { technicianId = value; }
    }
}
#endregion