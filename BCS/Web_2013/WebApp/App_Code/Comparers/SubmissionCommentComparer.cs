using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.Core.Clearance.BizObjects;
using aps = BCS.Core.Clearance.BizObjects.APS;
using System.Collections.Generic;


namespace BCS.WebApp.Comparers
{
    public class SubmissionCommentComparer : IComparer<SubmissionComment>
    {
        #region IComparer<SubmissionComment> Members

        int IComparer<SubmissionComment>.Compare(SubmissionComment xli, SubmissionComment yli)
        {
            int res = DateTime.Compare(yli.EntryDt, xli.EntryDt);
            return res;
        }

        #endregion
    }
    public class APSSubmissionCommentComparer : IComparer<aps.SubmissionComment>
    {
        #region IComparer<SubmissionComment> Members

        int IComparer<aps.SubmissionComment>.Compare(aps.SubmissionComment xli, aps.SubmissionComment yli)
        {
            int res = DateTime.Compare(yli.EntryDt, xli.EntryDt);
            return res;
        }

        #endregion
    } 
}
