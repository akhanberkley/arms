using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.Collections;

namespace BCS.WebApp.Components
{
    /// <summary>
    /// Summary description for ValidationEngine
    /// </summary>
    public class ValidationEngine
    {
        public static void CacheValidation()
        {
            int companyID = Common.GetSafeIntFromSession(SessionKeys.CompanyId);

            BCS.Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);

            if (HttpContext.Current.Cache[GetCacheKey("Validations")] == null)
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.Validation.Columns.CompanyID, companyID);
                HttpContext.Current.Cache[GetCacheKey("Validations")] = dm.GetValidationCollection();
            }
            
            if (HttpContext.Current.Cache[GetCacheKey("Conditions")] == null)
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.ValidationCondition.Validation.Columns.CompanyID, companyID);
                HttpContext.Current.Cache[GetCacheKey("Conditions")] = dm.GetValidationConditionCollection(Biz.FetchPath.ValidationCondition.ValidationField
                                                        , Biz.FetchPath.ValidationCondition.ValidationField);
            }

            if (HttpContext.Current.Cache[GetCacheKey("Rules")] == null)
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.ValidationRule.Validation.Columns.CompanyID, companyID);
                HttpContext.Current.Cache[GetCacheKey("Rules")] = dm.GetValidationRuleCollection(Biz.FetchPath.ValidationRule.ValidationField
                                                                                    , Biz.FetchPath.ValidationRule.ValidationRuleValue);
            }

            if (HttpContext.Current.Cache[GetCacheKey("Fields")] == null)
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(Biz.JoinPath.ValidationField.Columns.CompanyID, companyID);
                HttpContext.Current.Cache[GetCacheKey("Fields")] = dm.GetValidationFieldCollection(Biz.FetchPath.ValidationField.ValidationCondition
                                                                , Biz.FetchPath.ValidationField.ValidationRule
                                                                , Biz.FetchPath.ValidationField.ValidationRule.ValidationRuleValue);
            }

            return;
        }

        private static string GetCacheKey(string key)
        {
            int companyID = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
            return string.Format("{0}|{1}", companyID,key);
        }

        public static BCS.Biz.ValidationConditionCollection GetValidationConditions(string controlName)
        {
            CacheValidation();

            if (HttpContext.Current.Cache[GetCacheKey("Fields")] == null) return null;

            Biz.ValidationFieldCollection fields = (Biz.ValidationFieldCollection)HttpContext.Current.Cache[GetCacheKey("Fields")];
            //Biz.ValidationConditionCollection conditions = new Biz.ValidationConditionCollection();
            int filterID = fields.FindByControlID(controlName).ID;

            try
            {
                return GetValidationConditions().FilterByFieldID(filterID).FilterByDisplayOrder("1");
            }
            catch
            {
            }
            return null;

            /*
            conditions = fields.FindBy(controlName);

            if (conditions != null) return conditions.FilterByDisplayOrder(1);

            foreach(Biz.ValidationField field in fields.FilterByControlID(controlName))
            {
                Biz.ValidationCondition condition = field.ValidationConditions.FindByDisplayOrder("1");
                if (condition != null)
                    conditions.Add(condition);
            }

            return conditions;
             * */
        }

        public static BCS.Biz.ValidationConditionCollection GetValidationConditions(int validationID)
        {
            Biz.ValidationConditionCollection conditions = GetValidationConditions();

            if (conditions == null) return null;

            return conditions.FilterByValidationID(validationID);
        }

        public static BCS.Biz.ValidationConditionCollection GetValidationConditions()
        {
            CacheValidation();

            Biz.ValidationConditionCollection conditions = (Biz.ValidationConditionCollection)HttpContext.Current.Cache[GetCacheKey("Conditions")];
            return conditions;
        }

        public static BCS.Biz.ValidationCondition GetValidationCondition(int conditionID)
        {
            Biz.ValidationConditionCollection conditions = GetValidationConditions();
            if (conditions == null)
                return null;

            return conditions.FindByID(conditionID);
        }

        public static BCS.Biz.ValidationRuleCollection GetValidationRules(int validationID)
        {
            CacheValidation();

            Biz.ValidationRuleCollection rules = (Biz.ValidationRuleCollection)HttpContext.Current.Cache[GetCacheKey("Rules")];
            if (rules == null) return null;

            return rules.FilterByValidationID(validationID);
        }

        public static BCS.Biz.ValidationRule GetValidationRule(int ruleID, int validationID)
        {
            Biz.ValidationRuleCollection rules = GetValidationRules(validationID);

            if (rules == null) return null; ;

            return rules.FindByID(ruleID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public static void BindRuleValues(Biz.ValidationRule rule, ref DropDownList ddl)
        {
            if (rule == null) return;

            List<ListItem> items = new List<ListItem>();
            /*foreach (Biz.ValidationRuleValue ruleValue in rule.ValidationRuleValues)
            {
                #region process display text, sometimes code or description may be absent
                string itemText = string.Concat(ruleValue.CompanyNumberCode.Code, "-", ruleValue.CompanyNumberCode.Description).Trim();
                string[] itemTextArray = itemText.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                itemText = string.Join(" - ", itemTextArray);
                #endregion
                items.Add(new ListItem(itemText, ruleValue.CompanyNumberCode.Id.ToString()));
            }
            */
            ddl.DataSource = items;
            ddl.DataBind();
        }

        public static bool HasValidationConditions(string controlName)
        {
            Biz.ValidationConditionCollection conditions = GetValidationConditions(controlName);
            if (conditions.Count > 0)
                return true;
            else
                return false;
        }

        public static bool ApplyValidation()
        {
            int companyID = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
            string sessionKey = string.Format("{0}|ApplyValidation", companyID);

            bool result = true;
            if (HttpContext.Current.Session[sessionKey] != null)
                result = (bool)HttpContext.Current.Session[sessionKey];

            return result;
        }

        public static bool EvaluateValdationCondition(BCS.Biz.ValidationCondition condition, string compareValue)
        {
            bool result = false;

            switch (condition.OperatorCode)
            {
                case "EQ":
                    {
                        if (condition.CompareValue == compareValue) return true;
                        break;
                    }
                case "SW":
                    {
                        if (Left(compareValue, condition.CompareValue.Length) == condition.CompareValue) return true;
                        break;
                    }
            }

            return result;  
        }

        private static string Left(string value, int length)
        {
            if (string.IsNullOrEmpty(value)) return value;

            string res = value;
            if (value.Length > length)
                res = value.Substring(0, length);
            return res;
        }
    }
}