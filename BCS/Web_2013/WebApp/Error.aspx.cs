using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace BCS.WebApp
{
    /// <summary>
    /// Summary description for Error.
    /// </summary>
    public partial class Error : System.Web.UI.Page
    {

        protected void Page_Load(object sender, System.EventArgs e)
        {

            if (User.IsInRole("System Administrator"))
            {
                lblErrorMessage.Text = Common.GetSafeStringFromSession("lasterrormessage");
                Session.Remove("lasterrormessage");
                pnlCustomMessage.Visible = true;
                pnlDefaultError.Visible = false;    
            }
            else
            {
                lblErrorMessage.Visible = false;
            }

            if (Request.QueryString["msg"] != null && !string.IsNullOrEmpty(Request.QueryString["msg"]))
            {
                try
                {
                    lblMessage.Text = Request.QueryString["msg"].ToString();
                    pnlCustomMessage.Visible = true;
                    pnlDefaultError.Visible = false;
                }
                catch (Exception ex)
                {

                }
            }
        }


    }
}
