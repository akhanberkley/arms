<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="Portfolios.aspx.cs" Inherits="Portfolios" Title="Berkley Clearance System - Portfolio Maintenance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table cellpadding="0" cellspacing="0" class="box">
            <tbody>
            <tr>
                <td class="TL"><div /></td>
                <td class="TC">Portfolios</td>
                <td class="TR"><div /></td>
            </tr>
            <tr>
                <td class="ML"><div /></td>
                <td class="MC">
                    <table>
                        <tr>
                            <td>
                                Code</td>
                            <td>
                                <asp:TextBox ID="txFilterCode" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Description</td>
                            <td>
                                <asp:TextBox ID="txFilterDescription" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="Button1" runat="server" Text="Filter"  OnClick="Button1_Click" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <UserControls:Grid ID="gridPortfolios" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="ObjectDataSource1" EmptyDataText="No Portfolios were found.">
                        <Columns>
                            <asp:BoundField DataField="Code" HeaderText="Code" SortExpression="Code" />
                            <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
                            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                        </Columns>
                        <PagerTemplate>
                            <asp:PlaceHolder runat="server"></asp:PlaceHolder>
                        </PagerTemplate>
                    </UserControls:Grid>
                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetPortfolios"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txFilterCode" Name="code" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txFilterDescription" Name="description" PropertyName="Text"
                                Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <br />
                    <asp:Button ID="btnAdd" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.Agency) %>' runat="server" Text="Add"  OnClick="btnAdd_Click" />
                    <br />
                    <br />
                    <asp:DetailsView ID="detPortfolio" runat="server" AutoGenerateRows="False" DataSourceID="odsPortfolio">
                        <Fields>
                            <asp:BoundField DataField="Code" HeaderText="Code" SortExpression="Code" />
                            <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
                            <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                            <asp:CommandField ButtonType="Button" ShowEditButton="True" />
                        </Fields>
                    </asp:DetailsView>
                    <asp:ObjectDataSource ID="odsPortfolio" runat="server" SelectMethod="GetPortfolio"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="gridPortfolios" Name="id" PropertyName="SelectedValue"
                                Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </td>
                <td class="MR"><div /></td>
            </tr>
            <tr>
                <td class="BL"><div/></td>
                <td class="BC"><div/></td>
                <td class="BR"><div/></td>
            </tr>
            </tbody>
        </table>
</asp:Content>

