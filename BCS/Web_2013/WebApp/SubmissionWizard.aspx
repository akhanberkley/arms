<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="SubmissionWizard.aspx.cs" Inherits="SubmissionWizard" ClientIDMode="AutoID" Title="Berkley Clearance System : Submission Wizard" Theme="DefaultTheme" ValidateRequest="False" %>
<%@ Register Src="~/WebUserControls/AutoNumberGenerator.ascx" TagName="AutoNumberGenerator" TagPrefix="bcs" %>
<%@ Register Src="~/WebUserControls/SubmissionStatusSubmissionStatusReason.ascx" TagName="submissionStatusAndReason" TagPrefix="bcs" %>

<%--<script runat="server">

    //Moved to .cs
    //protected override void OnInit(EventArgs e)
    //{
    //    if (ConfigValues.GetCompanyParameter(Common.GetSafeIntFromSession(SessionKeys.CompanyId)).LinkAfterSubStatus.IsFalse)
    //    {
    //        submissionStatusReason.Dispose();
    //    }
        
    //    if (!IsPostBack)
    //    {
    //        AgencyDropDownList1.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
    //        Session.Remove("CopiedSubmissionId");
    //        Session.Remove("copystatushistories");
    //    }
    //    else if (!(Session["copystatushistories"] != null && (bool)Session["copystatushistories"]) && Session["CopiedSubmissionId"] != null)
    //    {
    //        Session.Remove("CopiedSubmissionId");
    //    }
                              
    //    base.OnInit(e);
    //}

    //Moved to .cs
    //protected void chkCopyStatusHistory_CheckChanged(object sender, EventArgs e)
    //{
    //    if (chkCopyStatusHistory.Checked)
    //        Session["copystatushistories"] = true;
    //    else
    //        Session.Remove("copystatushistories");
    //}
    
</script>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ajx:ToolkitScriptManager ID="ScriptManager1" runat="server" CombineScripts="True" OnAsyncPostBackError="ScriptManager1_AsyncPostBackError" >
<Scripts>
    <aje:ScriptReference Path='<%$ Code: string.Concat(DefaultValues.ScriptPath, "WebUIValidationExtended.js") %>' />
</Scripts>
<Services>
    <aje:ServiceReference  Path="WebServices/SubmissionService.asmx" />
</Services>
</ajx:ToolkitScriptManager>
<script src="Javascripts/jquery-1.4.4.js" type="text/javascript"></script>
<script type="text/javascript" id="autocomplete">
    var restorePreviousAutoCompleteElementText = "";
    var restorePreviousAutoCompleteElementValue = "";
    
    function itemSelected( source, eventArgs )
    {           
       restorePreviousAutoCompleteElementText = eventArgs.get_text();
       
       // 'AgencyName (AgencyNumber) | City, State' is the format returned by the web service used by autocomplete
       var agencyNumberElement = $get('<%= txFilterAgencyNumber.ClientID %>');
       var agencyNameElement = $get('<%= txFilterAgencyName.ClientID %>');
       var cityElement = $get('<%= txFilterCity.ClientID %>');
       var stateElement = $get('<%= txFilterState.ClientID %>');
                     
       var splits = restorePreviousAutoCompleteElementText.split('|');
       var nameNumber = splits[0];
       var cityState = splits[1];
       
       var name = nameNumber.substring(0, nameNumber.indexOf('('));
       var number = nameNumber.substring(nameNumber.indexOf('(') + 1, nameNumber.indexOf(')'));
       
       var city = cityState.split(',')[0];
       var state = cityState.split(',')[1];
       
       agencyNumberElement.value=number.trim();
       agencyNameElement.value=name.trim();
       cityElement.value=city.trim();
       stateElement.value = state.trim();
    }
   
</script>
<script type="text/javascript" id="misc">
function defaultStatusDate()
{
    var idBox1 = $get('<%= spSubmissionStatusIdBox.ClientID %>');
    var idBox2 = $get('<%= spSubmissionStatusDateIdLabel.ClientID %>');
    var today = new Date();
    idBox1.value = today.format('MM/dd/yyyy');
    idBox2.value = today.format('MM/dd/yyyy');
}
function setCBLValue(srcElement,tarElement)
{
    try
    {
        srcElement = document.getElementById(srcElement);
        tarElement = document.getElementById(tarElement);
         var cBoxes = srcElement.getElementsByTagName('input');
         var joined = '';
         var noneChecked = true;
         for(var i = 0; i < cBoxes.length; i++)
         {
            if(cBoxes[i].checked)
            {
               joined += cBoxes[i].value; // right now the populated control is just used for validation, if finished from this step, we are populating data and executing it so it should work fine.
               noneChecked = false;
            }
         }
         tarElement.value = joined;
    }
    catch(err) {};
}

function setValue(srcElement,tarElementId)
{
    try
    {
        var tarElement = document.getElementById(tarElementId);
        if(tarElement.type == "checkbox")
        {
            tarElement.checked = srcElement.checked;
        }
        else
        {
            tarElement.value = srcElement.value;
        }
    }
    catch(err) {alert(err);};
}

function mimicComboBox(elemSelect,elemTextBox)
{
    try
    {
        elemTextBox.value = elemSelect.value;
    }
    catch(err) {};
}
function checkAllCBL(me, cblid)
{
   me.nextSibling.innerHTML = me.checked ? 'de-' + me.nextSibling.innerHTML : me.nextSibling.innerHTML.substring(3, me.nextSibling.innerHTML.length);
   var cblist = document.getElementById (cblid);
   var cboxes = cblist.getElementsByTagName("input");
   for(var i=0;i<cboxes.length;i++)
    {
        cboxes[i].checked = me.checked;                   
    }
}

function checkAllCBLGV(me, cbList)
{
    me.nextSibling.innerHTML = me.checked ? 'de-' + me.nextSibling.innerHTML : me.nextSibling.innerHTML.substring(3, me.nextSibling.innerHTML.length);
    var list = cbList.split("|");
    for (var i=0;i<list.length;i++)
    {
        document.getElementById(list[i]).checked = me.checked;
    }   
}

function confirmPageLeave(scwEvt , confirmationMessage)
{
    if ($get('<%= SavedState.ClientID %>').value == 0)
    {
     if ( ! confirm(confirmationMessage) )
     {
        cancelEvent(scwEvt);
     }
    }
}
function InsuredNameOrDBAValidate(sender, args) {
    args.IsValid = true;
    if ($get('<%=spInsuredNameIdLabel.ClientID  %>').value == '' &&
        $get('<%=spDBAIdLabel.ClientID %>').value == '') {
        args.IsValid = false;
    }
}

function Page_LicensedStateValidate() {

        if (!Page_ClientValidate('')) {
                ShowMessageBox(true);
                return false;
            }
        if ($get('<%=EnforceLicStateWarning.ClientID %>').value == "1") {

            var agencyStatesId = '<%=AgencyLicensedStates.ClientID %>';
            var selectedStatesId = '<%=SelectedStates.ClientID %>';
            
            // TODO: can be optimized. quickly tried to remove the postbacks.
            var sourceSelectedStates = '<%=AddressList4StateWarning.ClientID %>';
            var clientStates = '';
            sourceSelectedStates = $get(sourceSelectedStates).getElementsByTagName('input');
            for (var i = 0; i < sourceSelectedStates.length; i++) {
   
                if (sourceSelectedStates[i].checked) {
                    clientStates = clientStates + sourceSelectedStates[i].nextSibling.innerHTML + '|';
                }
            }
            $get(selectedStatesId).value = clientStates;
            
            
            // read values
            var aStates = $get(agencyStatesId).value;
            var sStates = $get(selectedStatesId).value;    
            

            // pad agency states with pipes for easy seaching
            if(aStates.charAt(0) != '|')
            {   aStates = '|' + aStates;    }
            if(aStates.charAt(aStates.length-1) != '|')
            {   aStates = aStates + '|' ;    }
            
            var allClear = true;
           
           // split selected states
           var splits = sStates.split('|');
            for (var i = 0; i < splits.length; i++) {
            var sState = splits[i];
            
            // ignore empty entries
            if (sState.length == 0)
                continue;
                
            // pad each selected state with pipes for easy searching
            if(sState.charAt(0) != '|')
            {   sState = '|' + sState;    }
            if(sState.charAt(sState.length-1) != '|')
            {   sState = sState + '|' ;    }
            
                if (aStates.indexOf(sState) == -1) {
                allClear = false;
                break;
            }
           }
            if (!allClear) {
                aStates = aStates.substring(1,aStates.length-1);
                var re = /\|/gi;
                
                aStates = aStates.replace(re, ", ");
                
                alert('Review state entered. Only these states : (' + aStates + ') are authorized for this agency');
           }
            else {
            return true;
           }
        }

        return Page_ClientValidate('');
    }
    function NameOrAddressListSelectionChanged(srcId, tarIds, selectAllId)
    {
        var allChecked = true;
        
        var srcChkList = document.getElementById(srcId);
        var srcChkBoxes = srcChkList.getElementsByTagName('input');
        
        for(var j = 0; j < srcChkBoxes.length; j++)
        {
            if(!srcChkBoxes[j].checked)
                allChecked = false;
            for(var i = 0; i < tarIds.length; i++)
            {
                if(tarIds[i] != '')
                {
                    var tarChkList = document.getElementById(tarIds[i]);
                    if(tarChkList != null)
                    {
                        var tarChkBoxes = tarChkList.getElementsByTagName('input');
                        tarChkBoxes[j].checked = srcChkBoxes[j].checked;
                    }
                }
            }
        }
        var selectAllChk = document.getElementById(selectAllId);
        selectAllChk.checked = allChecked;
        
        if(allChecked)
        {
            if(selectAllChk.nextSibling.innerHTML.startsWith('de-'))
            {
                // its good as it is.
            }
            else
            {
                selectAllChk.nextSibling.innerHTML = 'de-' + selectAllChk.nextSibling.innerHTML;
            }
        }
        else
        {
            if(selectAllChk.nextSibling.innerHTML.startsWith('de-'))
            {
                selectAllChk.nextSibling.innerHTML = selectAllChk.nextSibling.innerHTML.substring(3, selectAllChk.nextSibling.innerHTML.length);
            }
        }
    }
    
    function NameOrAddressListSelectionChangedGV(srcIds, tarIds, selectAllId)
    {
        var allChecked = true;
        
        if (srcIds == null || srcIds == '' || tarIds == null)
        {   return; }
        
        var srcList = srcIds.split("|");
        
        for(var j = 0; j < srcList.length; j++)
        {
            var srcCheckBox = document.getElementById(srcList[j]);
            
            if (srcCheckBox == null)
                {continue;}
            
            if(!srcCheckBox.checked)
                allChecked = false;
            
            for(var i = 0; i < tarIds.length; i++)
            {
                if(tarIds[i] != '')
                {
                    var tarChkList = document.getElementById(tarIds[i]);
                    if(tarChkList != null)
                    {
                        var tarChkBoxes = tarChkList.getElementsByTagName('input');
                        tarChkBoxes[j].checked = srcCheckBox.checked;
                    }
                }
            }
                    
        }
        var selectAllChk = document.getElementById(selectAllId);
        selectAllChk.checked = allChecked;
        
        if(allChecked)
        {
            if(selectAllChk.nextSibling.innerHTML.startsWith('de-'))
            {
                // its good as it is.
            }
            else
            {
                selectAllChk.nextSibling.innerHTML = 'de-' + selectAllChk.nextSibling.innerHTML;
            }
        }
        else
        {
            if(selectAllChk.nextSibling.innerHTML.startsWith('de-'))
            {
                selectAllChk.nextSibling.innerHTML = selectAllChk.nextSibling.innerHTML.substring(3, selectAllChk.nextSibling.innerHTML.length);
            }
        }
    }
</script>

<script type="text/javascript" id="verifying">
var submissionVerified = false;
var verifiedResult = '';
var gotOtherSubmissions = false;

Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);

function ShowMessageBox(display) {
    if (display == false && document.getElementById('divMessagePlaceHolder').innerHTML.trim().length > 127)
    { display = true;}
    
    var tblMessages = document.getElementById('tblMessages');
    
    if (display == true) {
        tblMessages.style.display = 'block';
    }
    else { tblMessages.style.display = 'none'; }

}

function displayVerifiedResult(result) {
    if (result.length > 26 || displayErrorForCustomValidator)
    {ShowMessageBox(true); }
    else {ShowMessageBox(false); }
}

var displayErrorForCustomValidator = false;

function pageLoaded(sender, args) {

    if(!submissionVerified) {
        var RsltElem = document.getElementById('VerifyingPanel');
        RsltElem.style.display = 'block';
        BCS.WebApp.ScriptServices.SubmissionService.VerifySubmission(SucceededCallback);
        BlockActions();
    }
    else
    {
        var RsltElem = document.getElementById('VerifyingPanel');
        RsltElem.innerHTML = verifiedResult;
        displayVerifiedResult(verifiedResult);
        document.getElementById('workaroundIE6').style.display = 'none';
    }

    var customValidator = document.getElementById('<%=customPolicyNumberValidator.ClientID %>');

    if (customValidator.innerHTML != null && customValidator.innerHTML.length > 0) {
        var tblMessages = document.getElementById('tblMessages');
        if (tblMessages.style.display != 'block') {
            ShowMessageBox(true);
            displayErrorForCustomValidator = true;
        }
    }
    else {
        displayErrorForCustomValidator = false;
    }

    customValidator = document.getElementById('<%=cvAgentRequired.ClientID %>');

    if (customValidator.innerHTML != null && customValidator.innerHTML.length > 0) {
        var tblMessages = document.getElementById('tblMessages');
        if (tblMessages.style.display != 'block') {
            ShowMessageBox(true);
            displayErrorForCustomValidator = true;
        }
    }
    else {
        displayErrorForCustomValidator = false;
    }

    // fix gaps in wizard side bar for invisible sections
    var tables = document.getElementById('wizard').getElementsByTagName('table')
    for(var i = 0; i < tables.length; i++)
    {
        if(tables[i].id.endsWith('SideBarList'))
        {
            var cells = tables[i].getElementsByTagName('td');
            for(var j = 0; j < cells.length; j++)
            {
                if(cells[j].childNodes.length == 0 || (cells[j].childNodes.length == 1 && cells[j].childNodes[0].nodeType != 1))
                {
                    if(cells[j].parentNode.style.visibility)
                        cells[j].parentNode.style.visibility = 'collapse';
                    else
                        cells[j].parentNode.style.display = 'none';
                }
                else
                {
                    if(cells[j].parentNode.style.visibility)
                        cells[j].parentNode.style.visibility = '';
                    else
                        cells[j].parentNode.style.display = '';
                }
            }
        }
    }
    // end fix gaps in wizard side bar for invisible sections
}
function handleCopy() {
    var txCopyIds = document.getElementById('<%=txCopyIds.ClientID %>');

    var splits = txCopyIds.value.split(',');
    for(var i = 0; i < splits.length; i++)
    {
        var id1Id2 = splits[i].split('|');
        var targetElement = document.getElementById(id1Id2[0]);
        var sourceElement = document.getElementById(id1Id2[1]);
        if(targetElement.type == "checkbox")
        { targetElement.checked = sourceElement.checked; }
        else {
                targetElement.value = sourceElement.value; 
        }
    }
    //WebForm_AutoFocus('<%=CopyPanel.ClientID %>');
}
function BlockActions()
{   
    var ElemdivMenu = document.getElementById('<%=divMenu.ClientID %>');
    if (ElemdivMenu != null) ElemdivMenu.style.visibility = 'hidden';

   
}

function UnBlockActions()
{
    var ElemdivMenu = document.getElementById('<%=divMenu.ClientID %>');
    if (ElemdivMenu != null) ElemdivMenu.style.visibility = '';

    
}
    
// This is the callback function invoked if the Web service
// succeeded.
// It accepts the result object as a parameter.
function SucceededCallback(result, eventArgs)
{
    // Page element to display feedback.
    var RsltElem = document.getElementById('VerifyingPanel');
    RsltElem.innerHTML = result;
    verifiedResult = result;
    submissionVerified = true;
    UnBlockActions();

    displayVerifiedResult(result);
}


// This is the callback function invoked if the Web service
// failed.
// It accepts the error object as a parameter.
function FailedCallback(error)
{
    // Display the error.    
    var RsltElem = 
        document.getElementById('VerifyingPanel');
    RsltElem.innerHTML = 
    "Service Error: " + error.get_message();
    UnBlockActions();
    displayVerifiedResult(RsltElem.innerHTML);
}

if (typeof(Sys) !== "undefined") Sys.Application.notifyScriptLoaded();
</script>
<script language="javascript" type="text/javascript">
    <!-- 
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    
    prm.add_initializeRequest(InitializeRequest);
    prm.add_endRequest(EndRequest);
    
    function InitializeRequest(sender, args) {
            CancelIfInAsyncPostBack();
        var pop = $find("mpeUpdateProgressBehavior");
        pop.show();
    }
   
    function EndRequest(sender, args) {
        var pop = $find("mpeUpdateProgressBehavior");
        pop.hide();
    }
    function CancelIfInAsyncPostBack() {
        if (prm.get_isInAsyncPostBack()) {
          prm.abortPostBack();
        }
    }
        
    if (typeof(Sys) !== "undefined") Sys.Application.notifyScriptLoaded();
    // -->


    //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SetStaticSubmissionStatusDisplay);
    </script>

<script id="postbackRemovalAttempts" type="text/javascript">
var previousValue;
var elementProcesssed = null;
function savePreviousValue(elem)
{
    previousValue = elem.value;
}
function updateLOBCount(elem, evt)
{
    if(previousValue == 0 && elem.value != 0)
    {
        $get('<%=EffectiveLOBCount.ClientID %>').value++;
    }
    if(previousValue != 0 && elem.value == 0)
    {
        $get('<%=EffectiveLOBCount.ClientID %>').value--;
    }
}
</script>
<aje:UpdatePanel ID="MainUpdatePanel" runat="server">
<ContentTemplate>


<asp:TextBox ID="txCopyIds" runat="server" CssClass="displayedNone"></asp:TextBox>
<asp:TextBox ID="AgencyLicensedStates" runat="server" CssClass="displayedNone"></asp:TextBox>
<asp:TextBox ID="SelectedStates" runat="server" CssClass="displayedNone"></asp:TextBox>
<asp:TextBox ID="ShowStaticAsteriskInSnapshot" runat="server" CssClass="displayedNone" Text="False"></asp:TextBox>
<asp:TextBox ID="LogInfoMessages" runat="server" CssClass="displayedNone" Text="False"></asp:TextBox>
<asp:TextBox ID="SavedState" runat="server" CssClass="displayedNone" Text="1"></asp:TextBox>
<asp:CheckBoxList ID="AddressList4StateWarning" runat="server" CssClass="displayedNone">    
</asp:CheckBoxList>
        
<asp:TextBox ID="EnforceLicStateWarning" runat="server" CssClass="displayedNone"
Text='<%$ Code: DefaultValues.EnforceAgencyLicensedState(Common.GetSafeIntFromSession("CompanySelected")) ? "1" : "0" %>'>
</asp:TextBox>

<!--<div id="floatingBox" runat="server" style="width:27%;padding-right:10px;float:right;margin-top:5px;"></div>-->

<table cellpadding="0" cellspacing="0" border="0" width="100%" >
    <tr>
        <td valign="top" style="width:70%;padding:5px 20px 0px 0px;">

    <%--adding a dummy required field validator to overcome RFV's deficiency in registering client functions when stand alone--%>
            <asp:TextBox ID="dummy" runat="server" Columns="100" CssClass="displayedNone"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfv2" runat="server" ControlToValidate="dummy" InitialValue="0"
                SetFocusOnError="False" Display="None" ErrorMessage="dummy" Text="dummy">
            </asp:RequiredFieldValidator>
<%--<asp:Panel ID="BackToSearchPanel" runat="server" Width="125px" CssClass="modalPopup">
    <asp:LinkButton ID="lnkBackToSearch" runat="server" CausesValidation="False"
        AccessKey="R" Text="<-- To Search Results" OnClick="lnkBackToSearch_OnClick"
        OnClientClick="confirmPageLeave(event, 'Are you sure you want to continue?\r\n\r\nYou are currently adding/editing a submission that is not saved.\r\n\r\nPress OK to continue, or Cancel to stay on the current page.');">
    </asp:LinkButton>
</asp:Panel>--%>
<%--<ajx:AlwaysVisibleControlExtender ID="avce" runat="server"
    TargetControlID="BackToSearchPanel"
    VerticalOffset="100"
    HorizontalSide="Right">
</ajx:AlwaysVisibleControlExtender>--%> 
    
<asp:HiddenField ID="hfDisplayText" runat="server" Value='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' />

<table id="wizard" cellpadding="0" cellspacing="0" class="box" style="margin-top: 5px;">
    <tbody>
    <tr>
        <td class="TLBIG"><div /></td>
        <td class="TCBIG">
            <div id="divAddHeader" runat="server">
                <table width="100%">
                    <tr>
                        <td align="left">
                            <span style="font-size:12px;">Add Submission </span>
                        </td>
                        <td style="font-size:12px;">
                            Client Id:<span style="font-size:16px; "> <asp:Label ID="hdrClientIdLabelAdd" runat="server" Text='<%$ Code : Session["ClientId"]%>'>
                            </asp:Label><img id="imgCopyCCAdd"   onclick="javascript:copyErrorToClipboard(document.getElementById('<%= hdrClientIdLabelAdd.ClientID %>').innerHTML);" style="vertical-align: top;" src='<%=GetImageUrl("copy2.gif") %>'
                                alt="copy" title="Copy Client Id to clipboard" /></span>
                        </td>
                    </tr>
                </table>
            </div>
        <div id="divEditHeader" visible="false" runat="server">
            <table width="100%">
                <tr>
                    <td align="left" style="white-space:nowrap"><span style="font-size:12px;">Submission
                    <span style="font-size:16px; ">(#
                    <asp:Label ID="SubmissionNumberLabel" runat="server"></asp:Label><img  style=" vertical-align: top;" onclick="javascript:copyErrorToClipboard(document.getElementById('<%= SubmissionNumberLabel.ClientID %>').innerHTML);" src='<%= GetImageUrl("copy2.gif") %>' id="copy" alt="copy" title="Copy Submission Number to clipboard" />
                            -<asp:Label ID="SubmissionNumberSequenceLabel" runat="server"></asp:Label>)
                        </span>
                    </span></td>
                    <td style="white-space:nowrap;font-size:12px;">
                        Client Id: 
                        <span style="font-size:16px; ">
                            <asp:Label ID="hdrClientIdLabelEdt" runat="server" Text='<%$ Code : Session["ClientId"]%>'></asp:Label><img id="imgCopyCCEdt" style=" vertical-align: top;" src='<%= GetImageUrl("copy2.gif") %>' onclick="javascript:copyErrorToClipboard(document.getElementById('<%= hdrClientIdLabelEdt.ClientID %>').innerHTML);" alt="copy" title="Copy Client Id to clipboard" />
                        </span>
                    </td>
                    <td align="right" style="white-space:nowrap">
                        <span style="">Created On:&nbsp;&nbsp;<asp:Label ID="SubmissionDateLabel" runat="server"></asp:Label></span>
                    </td>
                
                </tr>
            </table>
        </div></td>
        <td class="TRBIG"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC" style="width:100%">
        <%--content start--%>
        <asp:Panel runat="server" id="pnlWizard">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <div id="divMenu" runat="server" visible="false">
                        <asp:LinkButton ID="lnkEditSubmission" runat="server" CausesValidation="False" OnClick="lnkEditSubmission_OnClick">Edit Submission</asp:LinkButton>&nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp
                        <asp:LinkButton ID="lnkEditClient" runat="server" CausesValidation="False" OnClick="lnkEditClient_OnClick">Edit Client</asp:LinkButton>&nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp
                        <asp:LinkButton ID="lnkCopySubmission" runat="server" CausesValidation="False" Enabled='<%$ Code: BCS.WebApp.Components.Security.CanUserAdd() %>'  OnClick="lnkCopySubmission_OnClick" AccessKey="Y">Copy Submission</asp:LinkButton>
                        <asp:HyperLink NavigateUrl="#" ID="lnkCopy" runat="server" onclick="handleCopy()" Enabled='<%$ Code: BCS.WebApp.Components.Security.CanUserAdd() %>' >Copy Submission</asp:HyperLink>&nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp
                        <asp:Panel ID="CopyPanel" runat="server" CssClass="modalPopup" style="display:none; width:400px;z-index: 1;" DefaultButton="btnCopied">
                            <div id="DragCopy" style="cursor: move; background-color: #00563F;"><span style="color: white;">DRAG</span></div>
                            <asp:Panel ID="CopyContainerPanel" runat="server" >                            
                                <asp:CheckBox runat="server" ID="chkCopyStatusHistory" Text="Copy Submission Statuses" Checked="false" OnCheckedChanged="chkCopyStatusHistory_CheckChanged" />
                            </asp:Panel>
                            <asp:Button ID="btnCopied" runat="server" Text="OK" OnClick="CopyDone" CausesValidation="False" UseSubmitBehavior="False" />
                        </asp:Panel>
                        <ajx:PopupControlExtender ID="popupCopy" runat="server"
                            TargetControlID="lnkCopy"
                            PopupControlID="CopyPanel" 
                            Position="Left, Bottom"
                            BehaviorID="copy"
                            OnLoad="CopyLoaded">
                        </ajx:PopupControlExtender>
                        <ajx:DragPanelExtender ID="dgeCopy" runat="server"
                            DragHandleID="DragCopy"
                            TargetControlID="CopyPanel">
                        </ajx:DragPanelExtender>
                        <script type="text/javascript">
                        function focusClientIdBox()
                        {
                            $get('<%= SwitchClientTextBox.ClientID %>').focus();
                        }
                        </script>
                        <asp:HyperLink NavigateUrl="#" ID="lnkSwitchClient" runat="server" onclick="focusClientIdBox();">Switch Client</asp:HyperLink>
                        <asp:Panel ID="SwitchPanel" runat="server" DefaultButton="btnLoadSwitched" BorderColor="Maroon" BorderStyle="Solid" BorderWidth="1" CssClass="modalPopup" style="display:none; width: 300px;">
                           &nbsp;&nbsp;&nbsp;&nbsp; Enter Client Id :  <asp:TextBox ID="SwitchClientTextBox" runat="server" ValidationGroup="switch"></asp:TextBox> &nbsp;&nbsp;
                           <asp:RequiredFieldValidator ID="rfvSwitch" runat="server" ControlToValidate="SwitchClientTextBox"
                               Display="Dynamic" SetFocusOnError="True" ValidationGroup="switch" ErrorMessage="* is required">
                           </asp:RequiredFieldValidator>
                           <asp:Button ID="btnLoadSwitched" runat="server" Text="Go" OnClick="btnLoadSwitched_Click" ValidationGroup="switch" UseSubmitBehavior="False" />
                        </asp:Panel>
                        &nbsp&nbsp&nbsp|
                        <ajx:PopupControlExtender ID="popupSwitch" runat="server" 
                            TargetControlID="lnkSwitchClient"
                            PopupControlID="SwitchPanel"
                            Position="Bottom" BehaviorID="switch">
                        </ajx:PopupControlExtender>
                         <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False"
                        AccessKey="R" Text="<-- To Search Results" OnClick="lnkBackToSearch_OnClick"
                        OnClientClick="confirmPageLeave(event, 'Are you sure you want to continue?\r\n\r\nYou are currently adding/editing a submission that is not saved.\r\n\r\nPress OK to continue, or Cancel to stay on the current page.');">
                    </asp:LinkButton>
                        </div>
                    </td>
                </tr>
            </table>
            <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="2"
                BackColor="#F7F6F3" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
                Font-Names="Verdana" Font-Size="0.8em" CellPadding="1" CellSpacing="1"
                OnNextButtonClick="Wizard1_NextButtonClick"
                OnPreviousButtonClick="Wizard1_PreviousButtonClick"
                OnSideBarButtonClick="Wizard1_SideBarButtonClick" Width="100%" Height="100px"
                OnFinishButtonClick="Wizard1_FinishButtonClick"
                FinishDestinationPageUrl="~/SubmissionWizard.aspx"
                OnCancelButtonClick="Wizard1_CancelButtonClick"
                CancelDestinationPageUrl="~/SubmissionWizard.aspx">
            <WizardSteps>
                <asp:WizardStep ID="DatesWizardStep" runat="server" Title="Type -&amp;- Dates" StepType="Start">
                    <table>
                        <tr class="submission_row_submission_type">
                            <td>
                                <span>Submission Type</span><span class="errorText">*</span>
                            </td>
                            <td style="padding-left:5px;">
                                <UserControls:SubmissionTypeDropDownList CssClass="cbo"
                                    ID="SubmissionTypeDropDownList1" runat="server"
                                    CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                    OnSelectedIndexChanged="SubmissionTypeDropDownList1_SelectedIndexChanged" >
                                </UserControls:SubmissionTypeDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender2" runat="server"
                                    TargetControlID="SubmissionTypeDropDownList1">
                                </ajx:ListSearchExtender>
                                <asp:Label ID="TypeLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                            </td>
                        </tr>
                        <tr class="submission_row_submission_type"><td>&nbsp;</td></tr>
                        <tr class="submission_row_effective_date">
                            <td align="right">
                                <span>Effective Date</span><span class="errorText">*</span>
                            </td>
                            <td style="padding-left:5px;">
                                <UserControls:DateBox ID="txEffectiveDate" runat="server" AutoPostBack="True" OnTextChanged="txEffectiveDate_Changed" CssClass="txt"
                                    Columns="10">
                                </UserControls:DateBox>                                
                                <span class="submission_row_effective_date_TBD">
                                    <span id="spanTest" runat="server" >&nbsp;&nbsp;&nbsp;-&nbsp;or&nbsp;-&nbsp;&nbsp;</span>
                                    <asp:CheckBox ID="TBDCheckBox" runat="server" Text="TBD"
                                        AutoPostBack="True" OnCheckedChanged="cbTBD_CheckedChanged">
                                    </asp:CheckBox>
                                </span>
                            </td>
                        </tr>
                        <tr class="submission_row_expiration_date">
                            <td align="right">
                                <span>Expiration Date</span>
                            </td>
                            <td style="padding-left:5px;">
                                <UserControls:DateBox ID="txExpirationDate" runat="server" AutoPostBack="False" CssClass="txt"
                                     Columns="10"></UserControls:DateBox>
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="AgencyWizardStep" runat="server" Title="Agency">
                    <table class="submission_row_agency" width="100%">
                        <tr id="rowAgencyDropDown" runat="server" OnInit="SetAgencyDropDownDisplay">
                            <td id="Td1" style="vertical-align:top; white-space: nowrap;" runat="server">
                                <span>Agency</span><span class="errorText">*</span>
                            </td>
                            <td id="Td2" style="padding-left:5px;" runat="server">
                                <UserControls:AgencyDropDownList CssClass="cbo"
                                    ID="AgencyDropDownList1" runat="server" AutoPostBack="True"
                                    ActiveOnly="True" DisplayFormat = "{0} ({1}) | {2}, {3}"
                                    AgencyType="NonMastersOnly" DataTextField="Text" DataValueField="Value" Extra="0"
                                    IdentifyInactive="False" ItemAsEmpty="False" SortBy=""
                                    CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                    CancelDate='<%$ Code:DateTime.MaxValue.Subtract(TimeSpan.FromSeconds(1)) %>'
                                    OnSelectedIndexChanged="AgencyDropDownList1_SelectedIndexChanged">
                                </UserControls:AgencyDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender1" runat="server"
                                    TargetControlID="AgencyDropDownList1">
                                </ajx:ListSearchExtender>
                                <asp:ImageButton ID="lookUpAgencies" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("lookup.gif") %>' OnClick="ToggleAgencyDisplay" CausesValidation="False" />
                                <br />
                                <br />
                                <asp:Label ID="AgencyComments" runat="server" CssClass="ImpInfo" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr id="rowAgencyGridFilter" runat="server" OnInit="SetAgencyGridDisplay">
                            <td id="Td3" colspan="2" runat="server">
                                <asp:Panel ID="panelJust4DefaultButton" runat="server" DefaultButton="btnFilter">
                                <fieldset>
                                    <legend style="color: #00563f; font-weight: bold;">Search<asp:ImageButton ID="DropDownAgencies" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("lookup.gif") %>' OnClick="ToggleAgencyDisplay" CausesValidation="False" /></legend>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="right" style="white-space: nowrap;">
                                                &nbsp;<span>Number</span>
                                            </td>
                                            <td style="padding-left:5px;">
                                               <asp:TextBox ID="txFilterAgencyNumber" runat="server" CssClass="txt" Columns="10"></asp:TextBox>
                                            </td>
                                            <td align="right" style="white-space: nowrap;">
                                                &nbsp;<span>Name</span>
                                            </td>
                                            <td style="padding-left:5px;">
                                               <asp:TextBox ID="txFilterAgencyName" runat="server" CssClass="txt" Columns="35" AutoPostBack="True"
                                                autocomplete="off" OnTextChanged="btnFilter_OnClick">
                                               </asp:TextBox>
                                               <ajx:AutoCompleteExtender ID="aceFilterAgencyName" runat="server"
                                                    ServicePath="WebServices/SubmissionService.asmx" ServiceMethod="GetCompletionList" ContextKey="AgencyName"
                                                    TargetControlID="txFilterAgencyName"
                                                    UseContextKey="True"
                                                    FirstRowSelected="False"
                                                    MinimumPrefixLength="1"
                                                    CompletionSetCount="30" 
                                                    CompletionInterval="100"
                                                    CompletionListCssClass="autocomplete_completionListElement" 
                                                    CompletionListItemCssClass="autocomplete_listItem" 
                                                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                    OnClientItemSelected="itemSelected"                                                    
                                                    OnLoad="aceFilterAgencyName_Load" >
                                                </ajx:AutoCompleteExtender>
                                            </td>
                                            <td align="right" style="white-space: nowrap;">
                                                &nbsp;<span>City</span>
                                            </td>
                                            <td style="padding-left:5px;">
                                                <asp:TextBox ID="txFilterCity" runat="server" CssClass="txt" Columns="20"></asp:TextBox>
                                            </td>
                                            <td align="right" style="white-space: nowrap;">
                                                &nbsp;<span>State</span>
                                            </td>
                                            <td style="padding-left:5px;">
                                                <asp:TextBox ID="txFilterState" runat="server" CssClass="txt" Columns="3"></asp:TextBox>
                                            </td>
                                            <td>
                                                &nbsp;<asp:Button ID="btnFilter" Text="Filter" CssClass="button" runat="server"
                                                    CausesValidation="False" OnClick="btnFilter_OnClick" />
                                            </td>
                                            <td>
                                                    &nbsp;<asp:Button ID="btnClearFilter" Text="Clear" CssClass="button" runat="server"
                                                    CausesValidation="False" OnClick="btnClearFilter_OnClick" />
                                            </td>
                                        </tr>    
                                    </table>
                                </fieldset>                                
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr id="rowAgencyGrid" runat="server" OnInit="SetAgencyGridDisplay">
                            <td colspan="2">
                                <UserControls:Grid ID="gridAgencies" runat="server" AllowPaging="True"
                                    EmptyDataText="No Agencies were found." AutoGenerateColumns="False"
                                    DataKeyNames="Id,AgencyNumber,AgencyName,City,State" CaptionAlign="Left" OnRowDataBound="gridAgencies_RowDataBound" 
                                    OnSelectedIndexChanged="gridAgencies_SelectedIndexChanged"
                                    OnPageIndexChanging="gridAgencies_PageIndexChanging"
                                    OnDataBound="gridAgencies_DataBound"
                                    Visible='<%$ Code: ShowGrid %>'
                                    Enabled='<%$ Code: ShowGrid %>' Width="100%">
                                    <RowStyle CssClass="item" />
                                    <Columns>
                                        <asp:ButtonField DataTextField="AgencyNumber" CommandName="Select" HeaderText="Number" >
                                            <itemstyle forecolor="#0040BF" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:Label Id="lblAgencyName" runat="server" Text='<%# Bind("AgencyName") %>' ></asp:Label>
                                            
</ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="City">
                                            <ItemTemplate>
                                                <asp:Label Id="lblAgencyCity" runat="server" Text='<%# Bind("City") %>' ></asp:Label>
                                            
</ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State">
                                            <ItemTemplate>
                                                <asp:Label Id="lblAgencyState" runat="server" Text='<%# Bind("State") %>' ></asp:Label>
                                            
</ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments">
                                            <ItemTemplate>
                                                <asp:Label Id="lblAgencyComments" runat="server" Text='<%# Bind("Comments") %>' ></asp:Label>
                                            
</ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Branch">
                                            <ItemTemplate>
                                                <asp:Label Id="lblAgencyBranch" runat="server" Text='<%# Bind("AgencyBranch.Name") %>' ></asp:Label>
                                            
</ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerTemplate>
                                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                                    </PagerTemplate>
                                </UserControls:Grid>
                                <asp:ObjectDataSource ID="odsAgencies" runat="server" SelectMethod="GetAgenciesByNameNumberCityStateAndUseRenewalCancelDate"
                                    TypeName="BCS.Core.Clearance.Admin.DataAccess" EnableCaching="True"
                                    CacheKeyDependency='<%$ Code : string.Format(CacheKeys.CompanyNumberAgencies, Common.GetSafeStringFromSession(SessionKeys.CompanyNumberId)) %>'
                                    OnInit="odsAgencies_Init">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected"
                                            Type="Int32" />
                                        <asp:Parameter Name="date" Type="DateTime" 
                                            DefaultValue='<%$ Code:DateTime.MaxValue.Subtract(TimeSpan.FromSeconds(1)) %>' />
                                        <asp:ControlParameter ControlID="txFilterAgencyName" Name="agencyName" PropertyName="Text"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="txFilterAgencyNumber" Name="agencyNumber" PropertyName="Text"
                                            Type="String" />
                                        <asp:ControlParameter ControlID="txFilterCity" Name="city" PropertyName="Text" Type="String" />
                                        <asp:ControlParameter ControlID="txFilterState" Name="state" PropertyName="Text"
                                            Type="String" />
                                        <asp:Parameter Name="masterAgencyId" Type="Int32" />
                                        <asp:Parameter Name="Extra" Type="Int32" DefaultValue="0" />
                                        <asp:Parameter Name="UseRenewalCancelDate" Type="Boolean" DefaultValue="false" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                                
                                <UserControls:Grid ID="gv" runat="server" AllowPaging="false" DataKeyNames="Id" 
                                    SkinID="none" AutoGenerateColumns="False" EnableViewState="False">
                                    <Columns>
                                        <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
                                    </Columns>
                                </UserControls:Grid>
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="AgentWizardStep" runat="server" Title="Agent">
                    <table>
                        <tr class="submission_row_agent">
                            <td align="right" valign="top" style="white-space: nowrap; vertical-align: top;">
                                <span>Agent</span>
                            </td>
                            <td style="padding-left:5px;">
                                <UserControls:AgentDropDownList ID="AgentDropDownList1" runat="server"
                                    CancelDate='<%$ Code:DateTime.MaxValue.Subtract(TimeSpan.FromSeconds(1)) %>'
                                    AutoPostBack="False"
                                    CssClass="cbo"
                                    AgencyId="0" Extra="0">
                                </UserControls:AgentDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender3" runat="server"
                                    TargetControlID="AgentDropDownList1">
                                </ajx:ListSearchExtender>
                                <asp:Label ID="AgentInfo" runat="server" CssClass="Warning" ></asp:Label>
                                <asp:Label ID="AgentLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                                <div class="submission_row_agent_reasons">
                                <UserControls:AgentUnspecifiedReasonsCheckBoxList id="cblAgentUnspecifiedReasons" runat="server"
                                    AutoPostBack="False" RepeatLayout="Flow"
                                    CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'>
                                </UserControls:AgentUnspecifiedReasonsCheckBoxList>
                                <asp:Label ID="AgentReasonLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                                </div>
                            </td>
                        </tr>
                        <tr class="submission_row_contact">
                            <td align="right" style="white-space: nowrap;">
                                <span>Contact</span>
                            </td>
                            <td style="padding-left:5px;">
                                <UserControls:ContactDropDownList ID="ContactDropDownList1" runat="server" CssClass="cbo"
                                    RetirementDate='<%$ Code:DateTime.MaxValue.Subtract(TimeSpan.FromSeconds(1)) %>'>
                                </UserControls:ContactDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender4" runat="server"
                                    TargetControlID="ContactDropDownList1">
                                </ajx:ListSearchExtender>
                                <asp:Label ID="ContactLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="LOBWizardStep" runat="server" Title="Underwriting">
                    <table>
                        <tr class="submission_row_underwriting_unit">
                            <td align="right" style="white-space: nowrap;">
                                <span>Underwriting Unit</span><span class="errorText">*</span>
                            </td>
                            <td style="padding-left:5px;">
                                <UserControls:LineOfBusinessDropDownList ID="LineOfBusinessDropDownList1" CssClass="cbo" runat="server" AutoPostBack="True" OnSelectedIndexChanged="LineOfBusinessDropDownList1_SelectedIndexChanged">
                                </UserControls:LineOfBusinessDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender5" runat="server"
                                    TargetControlID="LineOfBusinessDropDownList1">
                                </ajx:ListSearchExtender>
                                <asp:Label ID="lobLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                            </td>
                        </tr>
                        <tr class="submission_row_classcode_search">
                            <td>
                                <a runat="server" Visible='<%$ Code: Common.IsCWG() %>' href="ClassCodeSearch.aspx" target="_blank">Class Code Search</a>
                                <div runat="server" Visible='<%$ Code: !Common.IsCWG() %>'>
                                    <asp:LinkButton runat="server" ID="lnkClassCodeSearch" Text="Class Code Search" />
                                    <ajx:ModalPopupExtender ID="mpeClassCodeSearch" ClientIDMode="Static" runat="server" BackgroundCssClass="modalBackground" TargetControlID="lnkClassCodeSearch" PopupControlID="pnlClassCodeSearch">
                                    </ajx:ModalPopupExtender>
                                    <asp:Panel runat="server" Width="650" BackColor="White" BorderColor="#676767" BorderWidth="4" Height="300" ScrollBars="Auto" ID="pnlClassCodeSearch">
                                        <div style="float:right;width:10px;height:12px;background-color:#676767;color:#ffffff;padding:0px 0px 4px 2px;font-weight:bold;text-align:center;cursor:pointer;"
                                            onclick="$find('mpeClassCodeSearch').hide()">x</div>
                                    
                                        <iframe src="ClassCodeSearch.aspx" width="650" height="275" frameborder="0"></iframe>
    
                                    </asp:Panel>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        <tr class="submission_row_underwriting_unit"><td style="font-size: 4px;">&nbsp;</td></tr> <%--br with font-size helps in some gap for list search ui--%>
                        <tr class="submission_row_underwriter">
                            <td align="right" style="white-space: nowrap;">
                                 <span><UserControls:UnderwritingRoleLabel Type="UW" runat="server" ID="UnderwritingRoleLabel2"                            
                                ></UserControls:UnderwritingRoleLabel></span>
                                <asp:Label runat="server" ID="lbUWRequired" CssClass="errorText" Text="*" 
                                     Visible='<%$ Code : ConfigValues.RequiresUnderWriter(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                                    ></asp:Label>
                                <%--<span class="errorText">*</span>--%>
                            </td>
                           <td style="padding-left:5px;">
                                <UserControls:UnderwritersDropDownList ID="UnderwriterDropDownList1" runat="server"
                                    CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                    AutoPostBack="False"  CssClass="cbo" Role="Underwriter"
                                    ActiveOnly="True" Extra="0"
                                    RetirementDate="1753-01-01"
                                    FillAll='<%$ Code: DefaultValues.FillAllUnderwriters(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'>
                                </UserControls:UnderwritersDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender6" runat="server"
                                    TargetControlID="UnderwriterDropDownList1">
                                </ajx:ListSearchExtender>
                                <asp:Label ID="underwriterLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                            </td>
                        </tr>
                        <tr class="submission_row_underwriter"><td style="font-size: 4px;">&nbsp;</td></tr>
                        <tr class="submission_row_analyst">
                            <td align="right" style="white-space: nowrap;">
                                <span><UserControls:UnderwritingRoleLabel Type="AN" runat="server" ID="lbUWAssistant"                            
                                ></UserControls:UnderwritingRoleLabel></span>
                                <asp:Label runat="server" ID="lbUWAssistantRequired" CssClass="errorText" text="*"
                                 Visible='<%$ Code : ConfigValues.RequiresAnalyst(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                                ></asp:Label>
                            </td>
                           <td style="padding-left:5px;">
                                <UserControls:UnderwritersDropDownList ID="AnalystDropDownList1" runat="server"
                                    CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                    AutoPostBack="False"  CssClass="cbo" Role="Analyst"
                                    ActiveOnly="True" Extra="0" FillAll="False" RetirementDate="1753-01-01">
                                </UserControls:UnderwritersDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender7" runat="server"
                                    TargetControlID="AnalystDropDownList1">
                                </ajx:ListSearchExtender>
                                <asp:Label ID="analystLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                            </td>
                        </tr>
                        <tr class="submission_row_analyst"><td style="font-size: 4px;">&nbsp;</td></tr> <%--br with font-size helps in some gap for list search ui--%>
                        <tr class="submission_row_technician" style="margin-top: 15px;">
                            <td align="right" style="white-space: nowrap;">
                                <span><UserControls:UnderwritingRoleLabel Type="TN" runat="server" ID="UnderwritingRoleLabel1"                            
                                ></UserControls:UnderwritingRoleLabel></span>
                            </td>
                           <td style="padding-left:5px;">
                                <UserControls:UnderwritersDropDownList ID="TechnicianDropDownList1" runat="server"
                                    CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                    AutoPostBack="False"  CssClass="cbo" Role="Technician"
                                    ActiveOnly="True" Extra="0" FillAll="False" RetirementDate="1753-01-01">
                                </UserControls:UnderwritersDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender17" runat="server"
                                    TargetControlID="TechnicianDropDownList1">
                                </ajx:ListSearchExtender>
                                <asp:Label ID="technicianLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                            </td>
                        </tr>                        
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="PolicyWizardStep" runat="server" Title="Policy Information">
                    <table>
                        <tr class="submission_row_policy_system">
                            <td id="Td4" align="right" style="white-space: nowrap;" runat="server">
                                <span>Policy System</span>
                            </td>
                            <td id="Td5" style="padding-left:5px;" runat="server">
                                &nbsp;<UserControls:PolicySystemDropDownList ID="PolicySystemDropDownList1" runat="server" CssClass="cbo"
                                    AutoPostBack="True" 
                                    CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                    OnSelectedIndexChanged="PolicySystemDropDownList1_SelectedIndexChanged">
                                </UserControls:PolicySystemDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender8" runat="server"
                                    TargetControlID="PolicySystemDropDownList1">
                                </ajx:ListSearchExtender>
                                <asp:Label ID="policySystemLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                            </td>
                        </tr>
                        <tr class="submission_row_policy_number">
                            <td align="right" style="white-space: nowrap;">
                                <span>Policy Number</span>
                                <span id="spanPolicyNumRequired" runat="server" class="errorText">*</span>
                            </td>
                            <td style="padding-left:5px;">
                                <bcs:AutoNumberGenerator ID="txPolicyNumber" runat="server" TextBoxCssClass="txt"
                                    NumberControlTypeName="Policy Number" ReadableText="Policy Number"
                                    GeneratedType="PolicyNumber"
                                    ShowStaticAsterisk='<%$ Code : Convert.ToBoolean(ShowStaticAsteriskInSnapshot.Text) %>'
                                    CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                    AdditionalControlId="spPolicyNumberIdLabel"
                                    Required="False"
                                     
                                     />
                                
                            </td>
                        </tr>                        
                        <tr class="submission_row_premium">
                            <td align="right" style="white-space: nowrap;">
                                <span>Premium</span>
                            </td>
                            <td style="padding-left:5px;">
                                &nbsp;<UserControls:MoneyBox ID="txEstimatedPremium" runat="server" CssClass="txt"></UserControls:MoneyBox>
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="StatusWizardStep" runat="server" Title="Status">
                    <table>
                        <tr class="submission_row_status">
                            <td align="right" style="white-space: nowrap;" valign="top">
                                <span>Status</span>
                            </td>
                            <td style="padding-left:5px;">
                                <UserControls:SubmissionStatusDropDownList ID="SubmissionStatusDropDownList1" runat="server"
                                    AutoPostBack="True" CssClass="cbo"
                                    CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                    OnSelectedIndexChanged="SubmissionStatusDropDownList1_SelectedIndexChanged">
                                </UserControls:SubmissionStatusDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender9" runat="server"
                                    TargetControlID="SubmissionStatusDropDownList1">
                                </ajx:ListSearchExtender>
                                <asp:Label ID="statusLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                            </td>
                        </tr>
                        <tr class="submission_row_status_date">
                            <td align="right" style="white-space: nowrap;" valign="top">
                                <span>Status Date</span>
                            </td>
                            <td style="padding-left:5px;">
                                <UserControls:DateBox id="txSubmissionStatusDate" runat="server" CssClass="txt" Columns="10" />
                            </td>
                        </tr>
                        <tr class="submission_row_status_reason">
                            <td align="right" style="white-space: nowrap;">
                                <span style=" margin-top: 15px;">Status Reason</span>
                            </td>
                            <td style="padding-left:5px;">
                                <UserControls:SubmissionStatusReasonDropDownList ID="SubmissionStatusReasonDropDownList1" runat="server"
                                    CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                    ActiveOnly="True"
                                    CssClass="cbo" Extra="0" StatusId="0" style=" margin-top: 8px;">
                                </UserControls:SubmissionStatusReasonDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender10" runat="server"
                                    TargetControlID="SubmissionStatusReasonDropDownList1">
                                </ajx:ListSearchExtender>
                                <asp:Label ID="statusReasonLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                            </td>
                        </tr>
                        <tr class="submission_row_status_reason_comment">
                            <td align="right" style="white-space: nowrap;" valign="top">
                                <span><asp:Label runat="server" ID="lbReasonComment">Reason Comment</asp:Label></span>
                            </td>
                            <td style="padding-left:5px;">
                                <asp:TextBox ID="txSubmissionStatusReasonOther" runat="server" Columns="60" Rows="6" TextMode="MultiLine" CssClass="txt" onkeyup="ValidateCount(this, 255, event);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="CodesWizardStep" runat="server" Title="Attributes">
                    <asp:Label ID="CodeTypesLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                    <asp:Panel ID="CodeTypesContainerPanel" runat="server"></asp:Panel>
                    <asp:Panel ID="newCodeTypesContainerPanel" runat="server"></asp:Panel>
                    <asp:Label ID="CodeTypesInfo" runat="server" CssClass="Warning" EnableViewState="False"></asp:Label>
                </asp:WizardStep>
                <asp:WizardStep ID="OtherCarrierWizardStep" runat="server" Title="Other Carrier">
                    <table>
                        <tr class="submission_row_other_carrier">
                            <td align="right" style="white-space: nowrap;" valign="top">
                                <span>Other Carrier</span>
                            </td>
                            <td style="padding-left:5px;">
                                <UserControls:OtherCarrierDropDownList ID="OtherCarrierDropDownList1" runat="server"
                                    CssClass="cbo"
                                    CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>' SortBy="Order">
                                </UserControls:OtherCarrierDropDownList>
                                <ajx:ListSearchExtender ID="ListSearchExtender11" runat="server"
                                    TargetControlID="OtherCarrierDropDownList1">
                                </ajx:ListSearchExtender>
                                <asp:Label ID="OtherCarrierLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                            </td>
                        </tr>
                        <tr class="submission_row_other_carrier_premium">
                            <td align="right" style="white-space: nowrap;" valign="top">
                                <span>Other Carrier Premium</span>
                            </td>
                            <td style="padding-left:5px;">
                                <UserControls:MoneyBox ID="txOtherCarrierPremium" runat="server" CssClass="txt"></UserControls:MoneyBox>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <asp:Label ID="codeTypesLoadedOtherCarrier" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                                <asp:Panel ID="codeTypesContainerPanelOtherCarrier" runat="server"></asp:Panel>
                                <asp:Panel ID="newCodeTypesContainerPanelOtherCarrier" runat="server"></asp:Panel>
                                <asp:Label ID="codeTypesInfoOtherCarrier" runat="server" CssClass="Warning" EnableViewState="False"></asp:Label>                                
                            </td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="LOBListWizardStep" runat="server" Title="LOB">
                    <asp:Panel ID="CodeTypesContainerInLOBGridPanel" runat="server"></asp:Panel>
                    <asp:Panel ID="newCodeTypesContainerInLOBGridPanel" runat="server"></asp:Panel>
                    <UserControls:Grid ID="LobList1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" 
                        OnRowDataBound="LobList1_RowDataBound"
                        OnRowDeleting="RemoveLOB"
                        OnDataBound="LobList1_DataBound"
                        Width="100%">
                        <EmptyDataTemplate>
                        <div class="errorText" style="width: 100%; display: block;"> NO LOBs exist.
                        <asp:ImageButton ID="ibAdd" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("plus.gif") %>' CausesValidation="False" OnClick="AddLOB"
                            AlternateText="Click this icon to add a line of business" ToolTip="Click this icon to add a line of business" />
                        </div>
                        </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Id" Visible="False">
                                    <itemtemplate>
                                        <asp:TextBox runat="server" Text='<%# Bind("Id") %>' id="TextBox2"></asp:TextBox>
                                    </itemtemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="LOB">
                                <ItemStyle Width="29%"></ItemStyle>
                                    <itemtemplate>                                                    
                                        <UserControls:LineOfBusinessChildrenDropDownList ID="LineOfBusinessChildrenDropDownList1" runat="server"
                                            CompanyNumberId='<%$ Code: Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                            CssClass="cbo" style="margin-top: 10px"
                                            AutoPostBack="False"
                                            ActiveOnly="True" 
                                            onfocus="savePreviousValue(this);"
                                            onchange="updateLOBCount(this, event);" >
                                        </UserControls:LineOfBusinessChildrenDropDownList>
                                        <ajx:ListSearchExtender ID="ListSearchExtender12" runat="server"
                                            TargetControlID="LineOfBusinessChildrenDropDownList1">
                                        </ajx:ListSearchExtender>
                                    </itemtemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" >
                                    <ItemStyle Width="22%"></ItemStyle>
                                    <itemtemplate>
                                        <UserControls:SubmissionStatusDropDownList ID="SubmissionStatusDropDownListLOB1" runat="server"
                                            Default="True"
                                            SelectedValue='<%# Bind("SubmissionStatusId") %>' CssClass="cbo" style="margin-top: 10px"
                                            CompanyNumberId='<%$ Code: Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                            AutoPostBack="true" OnSelectedIndexChanged="SubmissionStatusDropDownListLOB1_SelectedIndexChanged" >
                                        </UserControls:SubmissionStatusDropDownList>
                                        <ajx:ListSearchExtender ID="ListSearchExtender13" runat="server"
                                            TargetControlID="SubmissionStatusDropDownListLOB1">
                                        </ajx:ListSearchExtender>
                                    </itemtemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reason">
                                    <ItemStyle Width="44%"></ItemStyle>
                                    <itemtemplate>
                                        <UserControls:SubmissionStatusReasonDropDownList ID="SubmissionStatusReasonDropDownListLOB1" runat="server"
                                            CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                            AutoPostBack="False"
                                            CssClass="cbo"
                                            ActiveOnly="True" 
                                            style="margin-top: 10px">
                                        </UserControls:SubmissionStatusReasonDropDownList>
                                        <ajx:ListSearchExtender ID="ListSearchExtender14" runat="server"
                                            TargetControlID="SubmissionStatusReasonDropDownListLOB1">
                                        </ajx:ListSearchExtender>
                                    </itemtemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemStyle Width="5%"></ItemStyle>
                                    <HeaderTemplate>
                                        <asp:ImageButton ID="ibAdd" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("plus.gif") %>' CausesValidation="False" OnClick="AddLOB"
                                        AlternateText="Click this icon to add a line of business" ToolTip="Click this icon to add a line of business" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="ibRemove" runat="server" CommandName="Delete" ImageUrl='<%$ Code : Common.GetImageUrl("minus.gif") %>' CausesValidation="false" 
                                            AlternateText="remove line of business" ToolTip="Click this icon to remove a line of business" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                    </UserControls:Grid>
                    <UserControls:Grid ID="LOBFooter" runat="server" ShowFooter="True" ShowHeader="False" AutoGenerateColumns="False"
                        OnRowDataBound="LOBFooter_RowDataBound" Width="100%" SkinID="none">
                        <Columns>
                            <asp:TemplateField HeaderText="LOB" FooterStyle-Width="29%">
                                <footertemplate>
                                    <b>New Status and Reason</b>
                                </footertemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" FooterStyle-Width="22%">
                                <footertemplate>
                                    <asp:DropDownList ID="LOBStatusesDDL" runat="server" AutoPostBack="True"
                                        DataSourceID="odsLOBStatuses" DataTextField="Text" DataValueField="Value" CssClass="cbo" style="margin-top: 10px"
                                        OnSelectedIndexChanged="LOBStatusesDDL_SelectedIndexChanged">
                                    </asp:DropDownList>
                                        <ajx:ListSearchExtender ID="ListSearchExtender15" runat="server"
                                            TargetControlID="LOBStatusesDDL" SkinID="ListSearchExtenderPrompt4Footer">
                                        </ajx:ListSearchExtender>
                                    <asp:ObjectDataSource ID="odsLOBStatuses" runat="server" SelectMethod="GetLOBGridUpdateStatuses"
                                        TypeName="BCS.WebApp.UserControls.Helper">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="CompanyId" SessionField="CompanySelected" Type="Int32" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </footertemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reason" FooterStyle-Width="44%">
                                <footertemplate>
                                    <UserControls:SubmissionStatusReasonDropDownList ID="LOBStatusReasonsDDL" runat="server"
                                    CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                    ActiveOnly="True"
                                    CssClass="cbo" style="margin-top: 10px">
                                    </UserControls:SubmissionStatusReasonDropDownList>
                                    <ajx:ListSearchExtender ID="ListSearchExtender16" runat="server"
                                        TargetControlID="LOBStatusReasonsDDL" SkinID="ListSearchExtenderPrompt4Footer">
                                    </ajx:ListSearchExtender>
                                </footertemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False" FooterStyle-Width="5%">
                                <footertemplate>
                                <asp:ImageButton ID="ibUpdate" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("reload.gif") %>' CausesValidation="False" OnClick="UpdateStatusesFromLOB"
                                    AlternateText="Click this icon to update statuses" ToolTip="Click this icon to update statuses" Enabled="False" />
                                </footertemplate>
                            </asp:TemplateField>
                        </Columns>
                    </UserControls:Grid>
                    <asp:Label ID="LOBListLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                    <asp:Label ID="CodeTypesInLOBGridLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                </asp:WizardStep>
                <asp:WizardStep ID="CommentsWizardStep" runat="server" Title="Comment">
                    <table>
                        <tr class="submission_row_comments">
                            <td align="right" style="white-space: nowrap;" valign="top">
                                <span>Comment</span>
                            </td>
                            <td style="padding-left:5px;">
                                <asp:TextBox ID="txComments" runat="server" Columns="80" Rows="6" TextMode="MultiLine" CssClass="txt" onkeyup="ValidateCount(this, 2000, event);"></asp:TextBox>
                                <asp:TextBox ID="CurrentCommentId" runat="server" CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>
                    </table>           
                </asp:WizardStep>
                <asp:WizardStep ID="ClientWizardStep" runat="server" Title="Client" StepType="Finish">
                    <asp:Panel ID="pnlClientInfo" runat="server"></asp:Panel>
                    <div id="Div1" style="width:100%; background-color: #F5E8C5; word-spacing: 2px;">
                        <span><b>Client Id:</b></span> <asp:Label ID="ClientIdLabel" runat="server"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <span style="width:70px;"><b>Portfolio Id:</b></span> <asp:Label ID="PortfolioIdLabel" runat="server"></asp:Label>
                    </div>
                    <br />
                    <table width="100%">
                        <tr>
                            <td valign="top" style="width: 50%;">
                                <div id="SectionInsuredName" runat="server" class="submission_row_insured_name">
                                    <table width="100%">
                                        <tr>
                                            <td valign="top" style="width: 5%; white-space: nowrap;"><span>Insured Name</span></td>
                                            <td valign="top "style="width: 95%;">
                                                <asp:TextBox ID="txInsuredName" runat="server" CssClass="txt" autocomplete="off" Columns="49"></asp:TextBox> <%--resorting to columns prop instead of width to overcome IE6 autoexpanding to fit a long name--%>
                                                <span>
                                                    <asp:ListBox ID="lbInsuredNames" runat="server"  OnInit="lbInsuredNames_Init" style="width: 92%;" CssClass="txt"></asp:ListBox>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td valign="top" style="width: 50%;">
                                <div class="submission_row_dba">
                                    <table width="100%">
                                        <tr>
                                            <td valign="top" style="width: 5%; white-space: nowrap;"><span>Dba</span></td>
                                            <td valign="top" style="width: 95%;">
                                                <asp:TextBox ID="txDBA" runat="server" autocomplete="off" Columns="57" CssClass="txt"></asp:TextBox>
                                                <span id="submission_row_dba_dbas">
                                                    <asp:ListBox ID="lbDbaNames" runat="server"  OnInit="lbDbaNames_Init" style="width: 92%;" CssClass="txt"></asp:ListBox>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    
                    <br />
                    <hr />
                    <div style="color:#ff0000;">Note: Changes to the Name Type or Address Type will be reflected in all submissions for this client.</div>
                    <asp:Label ID="Label7" runat="server" Text="Names" Font-Bold="True"></asp:Label>&nbsp;&nbsp;&nbsp;
                    (<asp:CheckBox id="NameListSelectAll" ForeColor="Blue" runat="server" Text="select all names" OnPreRender="AllCheck_Init"
                        AutoPostBack="False"
                        OnCheckedChanged="cbSelectAllAddresses_CheckedChanged">
                    </asp:CheckBox>)
                    
                <div id="Div2">
                            
                            <asp:GridView ID="gvClientNames" runat="server" ShowHeader="False" AutoGenerateColumns="False" SkinID="none" GridLines="None" BorderWidth="0"
                                EmptyDataText="No Client selected yet." style="margin: 5px;" DataKeyNames="Sequence" OnPreRender="GridViewCL_Load">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="nameCheckBox" runat="server" Checked='<%# Bind("Selected") %>' Text='<%# Bind("Text") %>'
                                               />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ClientNameType" SelectedValue='<%# Bind("Type") %>'>
                                            <asp:ListItem Value="" Text="NA"></asp:ListItem>
                                            <asp:ListItem Text="DBA" Value="DBA"></asp:ListItem>
                                            <asp:ListItem Text="Insured" Value="Insured"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                    <hr />
                    <asp:Label ID="Label8" runat="server" Text="Addresses" Font-Bold="True"></asp:Label>&nbsp;&nbsp;&nbsp;
                    (<asp:CheckBox ForeColor="blue" ID="AddressListSelectAll" runat="server" Text="select all addresses" OnPreRender="AllCheck_Init"
                        AutoPostBack="False"
                        OnCheckedChanged="cbSelectAllAddresses_CheckedChanged">
                    </asp:CheckBox>)

                    
                    <asp:GridView ID="gvClientAddresses" runat="server" ShowHeader="False" AutoGenerateColumns="False" SkinID="none" GridLines="None" BorderWidth="0"
                                EmptyDataText="No Client selected yet." style="margin: 5px;" DataKeyNames="addressID" OnPreRender="GridViewCL_Load">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="addressCheckBox" runat="server" Checked='<%# Bind("Selected") %>' Text='<%# Bind("Text") %>'
                                               />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ClientAddressType" SelectedValue='<%# Bind("Type") %>'>
                                            <asp:ListItem Value="" Text="NA"></asp:ListItem>
                                            <asp:ListItem Text="Mailing" Value="Mailing"></asp:ListItem>
                                            <asp:ListItem Text="Physical" Value="Physical"></asp:ListItem>
                                            <asp:ListItem Text="Both" Value="Both"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    
                    <asp:Label ID="ClientLoaded" runat="server" Text="False" CssClass="displayedNone"></asp:Label>
                </asp:WizardStep>
            </WizardSteps>
                <StepStyle BackColor="#F7F6F3" BorderColor="#E6E2D8" BorderStyle="None" BorderWidth="1px" CssClass="wizStepStyle" Height="105px" VerticalAlign="Top" />
                <SideBarStyle BackColor="#00563F" Font-Size="0.9em" VerticalAlign="Top" CssClass="wizSideBarStyle" Width="175px" />
                <NavigationButtonStyle BackColor="#E6E2D8" BorderColor="#C5BBAF" BorderStyle="Solid"
                    BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#00563F"/>
                <SideBarButtonStyle ForeColor="White" />
                <HeaderStyle BackColor="#666666" BorderColor="#E6E2D8" BorderStyle="Solid" BorderWidth="1px"
                    Font-Bold="True" Font-Size="0.9em" ForeColor="White" HorizontalAlign="Center" />
                <NavigationStyle BackColor="#F7F6F3" BorderStyle="None" Width="100%" CssClass="wizStyle" HorizontalAlign="Right" BorderWidth="0px"  VerticalAlign="Bottom" />                
                <StartNavigationTemplate>
                    <asp:Button ID="StepPreviousButton" runat="server" Text="Back" Height="20px" Width="70px" Enabled="false" CausesValidation="False" AccessKey="B" />&nbsp;
                    <asp:Button ID="StepNextButton" runat="server" Text="Next" Height="20px" Width="70px" CommandName="MoveNext" CausesValidation="False" AccessKey="N" />&nbsp;
                    <asp:Button ID="StepFinishButton" runat="server" Text="Finish" Height="20px" Width="70px" AccessKey="F"
                        OnClientClick="if(Page_LicensedStateValidate()) {submissionVerified = false;}" OnClick="Finish_Click" />&nbsp;
                    <asp:Button ID="StepCancelButton" runat="server" Text="Cancel" Height="20px" Width="70px" CausesValidation="False" CommandName="Cancel" AccessKey="C"
                        OnClientClick="confirmPageLeave(event, 'Are you sure you want to continue?\r\n\r\nYou are currently adding/editing a submission that is not saved.\r\n\r\nPress OK to continue, or Cancel to stay on the current page.');" />
                </StartNavigationTemplate>
                <StepNavigationTemplate>
                    <asp:Button ID="StepPreviousButton" runat="server" Text="Back" Height="20px" Width="70px" CommandName="MovePrevious" CausesValidation="False" AccessKey="B"/>&nbsp;
                    <asp:Button ID="StepNextButton" runat="server" Text="Next" Height="20px" Width="70px" CommandName="MoveNext" CausesValidation="False" AccessKey="N"/>&nbsp;
                    <asp:Button ID="StepFinishButton" runat="server" Text="Finish" Height="20px" Width="70px" CommandArgument="0" AccessKey="F" 
                        OnClientClick="if(Page_LicensedStateValidate()) {submissionVerified = false;}" OnClick="Finish_Click" />&nbsp;
                    <asp:Button ID="StepCancelButton" runat="server" Text="Cancel" Height="20px" Width="70px" CausesValidation="False" CommandName="Cancel" AccessKey="C"
                        OnClientClick="confirmPageLeave(event, 'Are you sure you want to continue?\r\n\r\nYou are currently adding/editing a submission that is not saved.\r\n\r\nPress OK to continue, or Cancel to stay on the current page.');" />
                </StepNavigationTemplate>
                <FinishNavigationTemplate>
                    <asp:Button ID="StepPreviousButton" runat="server" Text="Back" Height="20px" Width="70px" CommandName="MovePrevious" CausesValidation="False" AccessKey="B"/>&nbsp;
                    <asp:Button ID="StepNextButton" runat="server" Text="Next" Height="20px" Width="70px" Enabled="false" AccessKey="N" CausesValidation="False"/>&nbsp;
                    <asp:Button ID="StepFinishButton" runat="server" Text="Finish" Height="20px" Width="70px" CommandName="MoveComplete" AccessKey="F"
                        OnClientClick="if(Page_LicensedStateValidate()) {submissionVerified = false;}"  />&nbsp;
                    <asp:Button ID="StepCancelButton" runat="server" Text="Cancel" Height="20px" Width="70px" CausesValidation="False" CommandName="Cancel" AccessKey="C"
                        OnClientClick="confirmPageLeave(event, 'Are you sure you want to continue?\r\n\r\nYou are currently adding/editing a submission that is not saved.\r\n\r\nPress OK to continue, or Cancel to stay on the current page.');" />
                </FinishNavigationTemplate>
                <SideBarTemplate>
                    <asp:DataList ID="SideBarList" runat="server" Width="100%" OnItemDataBound="SideBarList_ItemDataBound">
                        <ItemStyle VerticalAlign="Bottom" />
                        <SelectedItemStyle Font-Bold="True" CssClass="selectedWizardStep"/>
                        <ItemTemplate>
                            <asp:LinkButton ID="SideBarButton" runat="server" ForeColor="White" CausesValidation="False" ></asp:LinkButton>
                            <asp:Image ID="ImageStatusSuccess" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("checked.gif") %>' Visible="false"/>
                            <asp:Image ID="ImageStatusFailure" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("unchecked.gif") %>' Visible="false" />                            
                        </ItemTemplate>
                        <SelectedItemTemplate>
                            <div>
                                <asp:LinkButton ID="SideBarButton" runat="server" ForeColor="#00563f" OnClientClick="return false;" style=" cursor: text;"></asp:LinkButton>
                                <asp:Image ID="ImageStatusSuccess" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("checked.gif") %>' Visible="false" />
                                <asp:Image ID="ImageStatusFailure" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("unchecked.gif") %>' Visible="false" />
                            </div>
                        </SelectedItemTemplate>
                    </asp:DataList>
                </SideBarTemplate>
            </asp:Wizard>
        </asp:Panel>
        <%--content end--%>
            </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
<asp:HiddenField runat="server" ID="hdnSubmissionIsDeleted" />
<table id="submisionView" cellpadding="0" cellspacing="0" class="box" width="100%">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Submission Information</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC" style="width: 100%">
        <%--content start--%>
       
        <asp:Panel ID="SnapshotPanel" runat="server">     
        <table cellpadding="5" cellspacing="0" width="100%" style="border: white 1px solid;">
            <tr>
                <td id="TabIndex_Section_10" runat="server" valign="top" style="width:50%;">
                    <table cellpadding="4" cellspacing="0" style="border: #D0D0BF 1px solid;" width="100%">
                        <tr id="submission_row_submission_type">
                        <%--giving the width as 20% so that this col appears to the left--%>
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Submission Type:
                            </td>
                            <td>
                                <asp:Label ID="spSubmissionTypeLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spSubmissionTypeIdLabel" runat="server" Text="0" CssClass="displayedNone"></asp:TextBox>
                                <UserControls:RFV ID="rfvType" runat="server" ControlToValidate="spSubmissionTypeIdLabel" InitialValue="0"
                                    SetFocusOnError="False" Display="None" ErrorMessage="Submission Type is required."
                                    ShowStaticAsterisk='<%$ Code : Convert.ToBoolean(ShowStaticAsteriskInSnapshot.Text) %>'>
                                </UserControls:RFV>
                            </td>
                        </tr>
                        <tr id="submission_row_effective_date">
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Effective Date:
                            </td>
                            <td>
                               <asp:Label ID="spEffectiveDateLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                               <asp:TextBox ID="spEffectiveDateIdLabel" runat="server" CssClass="displayedNone"></asp:TextBox>
                                <UserControls:RFV ID="rfvEff" runat="server" ControlToValidate="spEffectiveDateIdLabel"
                                    SetFocusOnError="False" Display="None" ErrorMessage="Effective Date is required." Text=""
                                    ShowStaticAsterisk='<%$ Code : Convert.ToBoolean(ShowStaticAsteriskInSnapshot.Text) %>'>
                                </UserControls:RFV>
                            </td>
                        </tr>
                        <tr id="submission_row_expiration_date">
                            <td align="right" style="white-space: nowrap;">
                                Expiration Date:
                            </td>
                            <td>
                                <asp:Label ID="spExpirationDateLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spExpirationDateIdLabel" runat="server" CssClass="displayedNone"></asp:TextBox>
                             </td>
                        </tr>
                        <tr id="submission_row_agency">
                            <td align="right" valign="top" style="white-space: nowrap;">
                                Agency:
                            </td>
                            <td>
                                <asp:Label ID="spAgencyNameNumberLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spAgencyIdLabel" runat="server" Text="0" CssClass="displayedNone"></asp:TextBox>
                                <UserControls:RFV ID="rfvAgency" runat="server" ControlToValidate="spAgencyIdLabel" InitialValue="0"
                                    SetFocusOnError="False" Display="None" ErrorMessage="Agency is required." Text=""
                                    ShowStaticAsterisk='<%$ Code : Convert.ToBoolean(ShowStaticAsteriskInSnapshot.Text) %>'>
                                </UserControls:RFV>
                                <br />
                                <asp:Label ID="spAgencyCityStateZipLabel" runat="server" Text=""></asp:Label>
                                <asp:Label runat="server" ID="AgencyCommentLabel" CssClass="ImpInfo" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr id="submission_row_agent">
                            <td align="right" style="white-space: nowrap; vertical-align: top;">
                                Agent:
                            </td>
                            <td>
                                <asp:Label ID="spAgentLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spAgentIdLabel" runat="server" Text="0" CssClass="displayedNone"></asp:TextBox>
                                <asp:BulletedList ID="spAgentReasons" runat="server"
                                    style="padding-left: 15px; margin-left: 5px; margin-top: 1px; margin-bottom: 1px;">
                                </asp:BulletedList>
                                <asp:TextBox ID="spAgentUnspecifiedIdLabel" runat="server" CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="submission_row_contact">
                            <td align="right">
                                Contact:
                            </td>
                            <td>
                                <asp:Label ID="spContactLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spContactIdLabel" runat="server" Text="0" CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="submission_row_underwriting_unit">
                            <td align="right" style="white-space: nowrap;">
                                Underwriting Unit:
                            </td>
                            <td>
                                <asp:Label ID="spLineOfBusinessLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spLineOfBusinessIdLabel" runat="server" Text="0" CssClass="displayedNone"></asp:TextBox>
                                <UserControls:RFV ID="rfvLineOfBusiness" runat="server" ControlToValidate="spLineOfBusinessIdLabel" InitialValue="0"
                                    Display="None" SetFocusOnError="False" ErrorMessage="Underwriting Unit is required."
                                    ShowStaticAsterisk='<%$ Code : Convert.ToBoolean(ShowStaticAsteriskInSnapshot.Text) %>'>
                                </UserControls:RFV>
                                <asp:Label ID="spLineOfBusinessInfo" runat="server" EnableViewState="False" CssClass="errorText"></asp:Label>
                            </td>
                        </tr>
                        <tr id="submission_row_underwriter">
                            <td align="right">
                                <UserControls:UnderwritingRoleLabel runat="server" Type="UW" id="UnderwritingRoleLabel3"></UserControls:UnderwritingRoleLabel>:
                            </td>
                            <td>
                                <asp:Label ID="spUnderwriterLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spUnderwriterIdLabel" runat="server" Text="0" CssClass="displayedNone"></asp:TextBox>
                                <UserControls:RFV ID="rfvUnderwriter" runat="server" ControlToValidate="spUnderwriterIdLabel" InitialValue="0"
                                    SetFocusOnError="False" Display="None" ErrorMessage="Underwriter is required"
                                    Enabled='<%$ Code : ConfigValues.RequiresUnderWriter(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                                    ShowStaticAsterisk='<%$ Code : Convert.ToBoolean(ShowStaticAsteriskInSnapshot.Text) %>'>
                                </UserControls:RFV>
                            </td>
                        </tr>
                        <tr id="submission_row_underwriter_previous" runat="server" class="submission_row_underwriter_previous">
                            <td align="right">
                                Previous  <UserControls:UnderwritingRoleLabel runat="server" Type="UW" id="UnderwritingRoleLabel4"></UserControls:UnderwritingRoleLabel>:
                            </td>
                            <td>
                                <asp:Label ID="spPreviousUnderwriterLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                            </td>
                        </tr>
                        <tr id="submission_row_analyst">
                            <td align="right" style="white-space: nowrap;">
                                <!-- Analyst -->
                                <UserControls:UnderwritingRoleLabel runat="server" Type="AN" id="lbSubmissionRowAnalyst"></UserControls:UnderwritingRoleLabel>:
                            </td>
                            <td>
                                <asp:Label ID="spAnalystLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spAnalystIdLabel" runat="server" Text="0" CssClass="displayedNone"></asp:TextBox>
                                <UserControls:RFV ID="rfvUnderwriterAssistant" runat="server" ControlToValidate="spAnalystIdLabel" InitialValue="0"
                                    SetFocusOnError="False" Display="None" ErrorMessage="Assistant/Analyst is required"
                                    Enabled='<%$ Code : ConfigValues.RequiresAnalyst(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                                    ShowStaticAsterisk='<%$ Code : Convert.ToBoolean(ShowStaticAsteriskInSnapshot.Text) %>'>
                                </UserControls:RFV>
                            </td>
                        </tr>
                        <tr id="submission_row_technician">
                            <td align="right" style="white-space: nowrap;">
                                <UserControls:UnderwritingRoleLabel runat="server" Type="TN" id="UnderwritingRoleLabel5"></UserControls:UnderwritingRoleLabel>:
                            </td>
                            <td>
                                <asp:Label ID="spTechnicianLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spTechnicianIdLabel" runat="server" Text="0" CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td id="TabIndex_Section_50" runat="server" valign="top" style="width:50%;">
                    <table cellpadding="4" cellspacing="0" style="border: #D0D0BF 1px solid;" width="100%">
                        <tr id="submission_row_policy_system">
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Policy System:
                            </td>
                            <td>
                                <asp:Label ID="spPolicySystemLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spPolicySystemIdLabel" runat="server" Text="0" CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="submission_row_policy_number">
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Policy Number:
                            </td>
                            <td>
                                <asp:Label ID="spPolicyNumberLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox  ID="spPolicyNumberIdLabel" runat="server" CssClass="displayedNone"></asp:TextBox>
                                <UserControls:RFV ID="rfvPolicyNumber" runat="server" ControlToValidate="spPolicyNumberIdLabel"
                                    SetFocusOnError="False" Display="None" ErrorMessage="Policy Number is required." Text=""
                                    ShowStaticAsterisk='<%$ Code : Convert.ToBoolean(ShowStaticAsteriskInSnapshot.Text) %>'>
                                </UserControls:RFV>
                                <asp:RegularExpressionValidator ID="regexPolicyNumber" runat="server"
                                    ControlToValidate="spPolicyNumberIdLabel" Display="None" ErrorMessage="Incorrect policy number format." Text=""
                                    ValidationExpression='<%$ Code: ConfigValues.GetPolicyNumberRegex(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'>
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>                       
                        <bcs:submissionStatusAndReason runat="server" id="submissionStatusReason" WizardSection="ReadOnly"></bcs:submissionStatusAndReason>
                        <tr id="submission_row_status" runat="server">
                            <%--giving the width as 30% so that this col appears to the left--%>
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Status:
                            </td>
                            <td>
                                <asp:Label ID="spSubmissionStatusLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <input type="text" id="workaroundIE6" class="displayedNone" />
                                <asp:TextBox ID="spSubmissionStatusIdLabel" runat="server" Text='<%$ Code: this.DefaultStatus %>' CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="submission_row_status_reason" runat="server">
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Status Reason:
                            </td>
                            <td>
                                <asp:Label ID="spSubmissionStatusReasonLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spSubmissionStatusReasonIdLabel" runat="server" Text="0" CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="submission_row_status_date" runat="server" >
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Status Date:
                            </td>
                            <td>
                                <asp:Label ID="spSubmissionStatusDateLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <UserControls:DateBox ID="spSubmissionStatusIdBox" runat="server" Visible="False"></UserControls:DateBox>
                                <asp:TextBox ID="spSubmissionStatusDateIdLabel" runat="server" CssClass="displayedNone"
                                     Text='<%$ Code : DateTime.Today.ToString(Common.DateFormat) %>'>
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr id="submission_row_cancellation_date">
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Cancellation Date:
                            </td>
                            <td>
                                <asp:Label ID="spCancellationDateLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                            </td>
                        </tr>                        
                        <tr id="submission_row_status_reason_comment">
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Status Reason Comment:
                            </td>
                            <td>
                                <asp:Label ID="spSubmissionStatusReasonOtherLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spSubmissionStatusReasonOtherIdLabel" runat="server" Text="" CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>
                        <%--<tr id="submission_row_other_carrier">
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Other Carrier:
                            </td>
                            <td>
                                <asp:Label ID="spOtherCarrierLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spOtherCarrierIdLabel" Text="0" runat="server" CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="submission_row_other_carrier_premium">
                            <td align="right" style="width: 20%; white-space: nowrap;">
                               Other Carrier Premium:
                            </td>
                            <td>
                                <asp:Label ID="spOtherCarrierPremiumLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spOtherCarrierPremiumIdLabel" runat="server" CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>--%>
                        <tr id="submission_row_insured_name">
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Insured Name:
                            </td>
                            <td>
                                <asp:Label ID="spInsuredNameLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spInsuredNameIdLabel" runat="server" CssClass="displayedNone"></asp:TextBox>
                                <UserControls:RFV ID="rfvInsuredName" runat="server" ControlToValidate="spInsuredNameIdLabel"
                                    Enabled='<%$ Code : ConfigValues.RequiresInsuredName(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                                    SetFocusOnError="False" Display="None" ErrorMessage="Insured Name is required." Text=""
                                    ShowStaticAsterisk='<%$ Code : Convert.ToBoolean(ShowStaticAsteriskInSnapshot.Text) %>'>
                                </UserControls:RFV>
                                <asp:CustomValidator runat="server" ID="cvInsuredOrDBA" ErrorMessage="Please select an Insured or DBA name."
                                      Enabled='<%$ Code : ConfigValues.RequiresInsuredOrDBAName(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                                      ClientValidationFunction="InsuredNameOrDBAValidate" Display="none"></asp:CustomValidator>
                            </td>
                        </tr>
                        <tr id="submission_row_dba">
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Dba:
                            </td>
                            <td>
                                <asp:Label ID="spDBALabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spDBAIdLabel" runat="server" CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="submission_row_premium">
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Premium:
                            </td>
                            <td>
                                <asp:Label ID="spEstimatedPremiumLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spEstimatedPremiumIdLabel" runat="server" CssClass="displayedNone"></asp:TextBox>
                            </td>
                         </tr>
                         
                        
                    </table>
                </td>                    
            </tr>
            <tr>
                <td colspan="2" valign="top">
                    <fieldset>
                        <legend style="color: #00563f; font-weight: bold;">Attributes</legend>
                            <asp:Panel ID="spCodeTypesContainerPanel" runat="server"></asp:Panel>
                            <asp:Panel ID="newspCodeTypesContainerPanel" runat="server"></asp:Panel>                            
                            <asp:HiddenField ID="AttrIdsHidden" runat="server" />
                    </fieldset>
                    <fieldset id="fsOtherCarrier" style="margin-top:10px;padding:10px;">
                        <legend style="color: #00563f; font-weight: bold;">Other Carrier</legend>
                        <table width="100%" style="margin:10px 0px 0px 6px;">
                        <tr id="submission_row_other_carrier">
                            <td align="right" style="width: 20%; white-space: nowrap;">
                                Other Carrier:&nbsp;
                            </td>
                            <td>
                                <asp:Label ID="spOtherCarrierLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spOtherCarrierIdLabel" Text="0" runat="server" CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="submission_row_other_carrier_premium">
                            <td align="left" style="width: 20%; white-space: nowrap;">
                               Other Carrier Premium:&nbsp;
                            <td>
                                <asp:Label ID="spOtherCarrierPremiumLabel" runat="server" Text='<%$ Code: ConfigValues.GetDisplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'></asp:Label>
                                <asp:TextBox ID="spOtherCarrierPremiumIdLabel" runat="server" CssClass="displayedNone"></asp:TextBox>
                            </td>
                        </tr>
                        </table>
                        <asp:Panel ID="spCodeTypesContainerPanelOtherCarrier" runat="server"></asp:Panel>
                        <asp:HiddenField ID="AttrIdsHiddenOtherCarrier" runat="server" />
                    </fieldset>
                </td>
            </tr>
        </table>
        </asp:Panel>
        
        <%--content end--%>
            </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>

<asp:Panel ID="pnlLobVisible" runat="server"  Visible='<%$ Code: DefaultValues.SupportsMultipleLineOfBusinesses(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'>
<table id="lobDisplay"  cellpadding="0" cellspacing="0" class="box" width="100%"
    visible="false"> 
   
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Line Of Business</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC" style="width: 100%">
            <asp:Panel ID="spCodeTypesContainerInLOBGridPanel" runat="server"></asp:Panel>
            <asp:Panel ID="newspCodeTypesContainerInLOBGridPanel" runat="server"></asp:Panel>
            <%--content start--%>
            <UserControls:Grid ID="LOBList4Display" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" 
                OnRowDataBound="LobList1_RowDataBound4Display"
                OnRowDeleting="RemoveLOB"
                style="width:100%">
                <EmptyDataTemplate>
                    <div class="errorText" style="width: 100%; display: block;"> No LOBs selected yet.</div>                                                
                </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Id" Visible="False">
                            <itemtemplate>
                                <asp:TextBox runat="server" Text='<%# Bind("Id") %>' id="TextBox2" Visible="False"></asp:TextBox>
                            </itemtemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="LOB">
                            <ItemStyle Width="29%"></ItemStyle>
                            <itemtemplate>                                                    
                                <UserControls:LineOfBusinessChildrenDropDownList ID="LineOfBusinessChildrenDropDownList1" runat="server"
                                    CompanyNumberId='<%$ Code: Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                    SelectedValue='<%# Bind("CompanyNumberLOBGridId") %>' Visible="False" >
                                </UserControls:LineOfBusinessChildrenDropDownList>
                                <asp:Label runat="server" id="LineOfBusinessChildrenTextBox1"></asp:Label>
                            </itemtemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" >
                            <ItemStyle Width="22%"></ItemStyle>
                            <itemtemplate>
                                <UserControls:SubmissionStatusDropDownList 
                                    ID="SubmissionStatusDropDownListLOB1" runat="server" 
                                    SelectedValue='<%# Bind("SubmissionStatusId") %>'
                                    CompanyNumberId='<%$ Code: Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>' Visible="False" >
                                </UserControls:SubmissionStatusDropDownList>
                                <asp:Label runat="server" id="SubmissionStatusTextBox1"></asp:Label>
                        </itemtemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reason">
                            <ItemStyle Width="44%"></ItemStyle>
                            <itemtemplate>
                            <UserControls:SubmissionStatusReasonDropDownList ID="SubmissionStatusReasonDropDownListLOB1" runat="server" 
                                CompanyNumberId='<%$ Code:Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                                CssClass="displayedNone">
                            </UserControls:SubmissionStatusReasonDropDownList>
                            <asp:RequiredFieldValidator ID="rfvReason" runat="server" ErrorMessage="*" Display="None"
                                ControlToValidate="SubmissionStatusReasonDropDownListLOB1"  InitialValue="0" >
                            </asp:RequiredFieldValidator>
                            <asp:Label runat="server" id="SubmissionStatusReasonTextBox1"></asp:Label></itemtemplate>
                        </asp:TemplateField>
                    </Columns>
            </UserControls:Grid>
            <asp:TextBox ID="EffectiveLOBCount" runat="server" Text="0" CssClass="displayedNone">
            </asp:TextBox>
            <asp:RequiredFieldValidator ID="LOBSRequired" runat="server" InitialValue="0"
                Enabled='<%$ Code: DefaultValues.SupportsMultipleLineOfBusinesses(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                Display="None" ErrorMessage="At least one line of business is required" 
                ControlToValidate="EffectiveLOBCount">
            </asp:RequiredFieldValidator>
            <%--content end--%>
           </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
   
</table>
</asp:Panel>

<table id="statusHistory" cellpadding="0" cellspacing="0" class="box" width="100%">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Status History (<asp:Label ID="StatusHistoryCountLabel" runat="server"></asp:Label>)</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC" style="width: 100%">
        <%--content start--%>
        <asp:PlaceHolder ID="HistoryStatusPH" runat="server"></asp:PlaceHolder>
        <UserControls:Grid id="gridStatusHistory" runat="server"
            AllowPaging="True" AutoGenerateColumns="False"
            DataSourceID="odsStatusHistory" DataKeyNames="Id,PreviousSubmissionStatusId"
            OnRowDataBound="gridStatusHistory_RowDataBound"
            OnSelectedIndexChanged="gridStatusHistory_SelectedIndexChanged"
            OnPageIndexChanging="gridStatusHistory_PageIndexChanging"
            Width="100%">
         <EmptyDataTemplate>
            <div>(none)</div>
        </EmptyDataTemplate>
         <Columns>
            <asp:BoundField DataField="Id" Visible="False" HeaderText="Id"></asp:BoundField>
            <asp:BoundField DataField="SubmissionId" Visible="False" HeaderText="SubmissionId"></asp:BoundField>
            <asp:BoundField DataField="PreviousSubmissionStatusId" Visible="False" HeaderText="PreviousSubmissionStatusId"></asp:BoundField>
            <asp:TemplateField HeaderText="Status Date">
                <ItemTemplate>
                    <asp:Label ID="lblStatusDate" runat="server" Text='<%# Bind("SubmissionStatusDate", "{0:MM/dd/yyyy}") %>' ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Previous Submission Status">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "SubmissionStatus.StatusCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status Changed Date">
                <ItemTemplate>
                    <asp:Label ID="lblDate" runat="server" Text='<%# Bind("StatusChangeDt", "{0:MM/dd/yyyy HH:mm}") %>' ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="StatusChangeBy" HeaderText="Status Changed By"></asp:BoundField>
            <asp:TemplateField HeaderText="Active" ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton runat="server" Text="Active" CommandName="Select" CausesValidation="false" ID="LinkButton1"></asp:LinkButton>
                    <ajx:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="LinkButton1"
                    ConfirmText="Are you sure you want to make this status history inactive, this cannot be undone?"></ajx:ConfirmButtonExtender>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerTemplate>
        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
        </PagerTemplate>
        </UserControls:Grid>
        <asp:ObjectDataSource id="odsStatusHistory" runat="server" TypeName="BCS.Core.Clearance.Submissions"
            SelectMethod="GetSubmissionStatusHistory" OldValuesParameterFormatString="original_{0}"
            OnSelected="odsStatusHistory_Selected">
            <SelectParameters>
                <asp:SessionParameter Name="submissionId" SessionField="SubmissionId" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
	     <%--content end--%>   
        </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
<table id="clientView" cellpadding="0" cellspacing="0" class="box" width="100%">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Client Information</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC" style="width: 100%">
        <%--content start--%>
        <asp:Panel ID="ClientPanel" runat="server" width="100%">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td colspan="3" valign="top">
                        <div id="clientInfo" style="width:100%; background-color: #F5E8C5; word-spacing: 2px;">
                            <span><b>Client Id:</b></span> <asp:Label ID="spClientIdLabel" runat="server" Text='<%$ Code : Session["ClientId"]%>'></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <span style="width:70px;"><b>Portfolio Id:</b></span> <asp:Label ID="spPortfolioIdLabel" runat="server">(not specified)</asp:Label>
                        </div>
                    </td>
                </tr>
                <tr style="padding-top:10px;">
                    <td valign="top" style="width:50%;">
                        <div id="clientNames">
                            <fieldset>
                                <legend style="color: #00563f; font-weight: bold;"><span>Names</span></legend>
                                <asp:GridView ID="gridSelectedNames" runat="server" ShowHeader="False" AutoGenerateColumns="False" SkinID="none" GridLines="None" BorderWidth="0"
                                    EmptyDataText="No Client selected yet." style="margin: 5px;">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBoxName1" runat="server" Checked='<%# Bind("Selected") %>' Text='<%# string.Format("{0} ({1})",DataBinder.Eval(Container.DataItem,"Text"), (!string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem,"Type").ToString()) ? DataBinder.Eval(Container.DataItem,"Type") : "NA")) %>'
                                                    OnInit="CheckBox4Toggle_Init" />
                                                <ajx:ToggleButtonExtender ID="tbe1" runat="server"
                                                    TargetControlID="CheckBoxName1"
                                                    ImageHeight="11"
                                                    ImageWidth="11"
                                                    CheckedImageUrl='<%$ Code : Common.GetImageUrl("checked.gif") %>'
                                                    UncheckedImageUrl='<%$ Code : Common.GetImageUrl("unchecked.gif") %>'>
                                                </ajx:ToggleButtonExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <UserControls:CheckBoxList4BCS ID="NameList4Validation" runat="server" CssClass="displayedNone">
                                </UserControls:CheckBoxList4BCS>
                                <UserControls:RequiredFieldValidatorForCheckBoxLists ID="requiredName" runat="server" 
                                    ControlToValidate="NameList4Validation" Display="None" ErrorMessage="At least one name is required." >
                                </UserControls:RequiredFieldValidatorForCheckBoxLists>
                            </fieldset>
                        </div>
                    </td>
                    <td>&nbsp;</td>
                    <td valign="top" style="width:50%;">
                        <div id="clientAddresses">
                        <fieldset>
                                <legend style="color: #00563f; font-weight: bold;"><span> Addresses </span></legend>
                                <asp:GridView ID="gridSelectedAddresses" runat="server" ShowHeader="False" AutoGenerateColumns="False" SkinID="none" GridLines="None" BorderWidth="0"
                                    EmptyDataText="No Client selected yet." style="margin: 5px;">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBoxAddr1" runat="server" Checked='<%# Bind("Selected") %>' Text='<%# string.Format("{0} ({1})",DataBinder.Eval(Container.DataItem,"Text"), (!string.IsNullOrEmpty(DataBinder.Eval(Container.DataItem,"Type").ToString()) ? DataBinder.Eval(Container.DataItem,"Type") : "NA")) %>' 
                                                    OnInit="CheckBox4Toggle_Init" />
                                                <ajx:ToggleButtonExtender ID="tbe1" runat="server"
                                                    TargetControlID="CheckBoxAddr1"
                                                    ImageHeight="11"
                                                    ImageWidth="11"
                                                    CheckedImageUrl='<%$ Code : Common.GetImageUrl("checked.gif") %>'
                                                    UncheckedImageUrl='<%$ Code : Common.GetImageUrl("unchecked.gif") %>'>
                                                </ajx:ToggleButtonExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <UserControls:CheckBoxList4BCS ID="AddressList4Validation" runat="server" CssClass="displayedNone">
                                </UserControls:CheckBoxList4BCS>
                                <UserControls:RequiredFieldValidatorForCheckBoxLists ID="requiredAddress" runat="server" 
                                    ControlToValidate="AddressList4Validation" Display="None" ErrorMessage="At least one address is required.">
                                </UserControls:RequiredFieldValidatorForCheckBoxLists>
                        </fieldset>
                    </div>
                    <%--<asp:HiddenField runat="server" ID="SelectedStates"></asp:HiddenField>--%>
                    </td>
                </tr>
            </table>
    </asp:Panel>

        <%--content end--%>
            </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
<div class="submission_row_comments">
<table id="comment" cellpadding="0" cellspacing="0" class="box" width="100%">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Comments (<asp:Label ID="CommentCountLabel" runat="server"></asp:Label>)</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC" style="width: 100%">
        <%--content start--%>        
        <UserControls:Grid id="gridComments4Display" runat="server" 
            AllowPaging="True" AutoGenerateColumns="False" ShowHeader="False"
            DataKeyNames="Id"
            GridLines="None"
            OnDataBound="gridComments_DataBound"
            OnRowDeleting="RemoveComment"
            OnPageIndexChanging="gridComments4Display_PageIndexChanging"
            BorderWidth="0" Width="100%">
         <EmptyDataTemplate>
            <div>(none)</div>
        </EmptyDataTemplate>
         <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <table width="100%" style="margin-top: 5px; padding-top: 5px;">
                                <tr>
                                    <td>
                                        <table  width="100%">
                                        <tr>
                                        <td align="left">
                                            <asp:Label Id="cmtUser" runat="server" Text='<%# Bind("EntryBy") %>' Font-Bold="True"></asp:Label> 
                                            - 
                                            <asp:Label Id="cmtTime" runat="server" Text='<%# Bind("EntryDt") %>' Font-Bold="True"></asp:Label>
                                        </td>
                                            <td align="right">
                                            <asp:ImageButton ID="ibRemoveComment" runat="server" CommandName="Delete" ImageUrl='<%$ Code : Common.GetImageUrl("minus.gif") %>'
                                                CausesValidation="false" AlternateText="remove comment" style="position: relative; right: 2px;"
                                                ToolTip="Click this icon to remove a comment" 
                                                Visible='<%$ Code: BCS.WebApp.Components.Security.CanDelete() %>'/>
                                            <ajx:ConfirmButtonExtender ID="cbeDeleteComment" runat="server"
                                                TargetControlID="ibRemoveComment"
                                                ConfirmText="Are you sure you want to remove this comment?">
                                            </ajx:ConfirmButtonExtender>
                                            </td>
                                        </tr>
                                        </table>
                                    </td>   
                                </tr>
                                <tr>
                                    <td style="width: 90%; max-width: 90%;">
                                    <asp:Label Id="cmtComm" runat="server" Text='<%# Bind("Comment") %>'></asp:Label>
                                        <hr style="margin : 0px; padding: 0px;" />
                                    </td>
                                </tr>
                            </table>
                </ItemTemplate>                
            </asp:TemplateField>
         </Columns>
        </UserControls:Grid>
        <%--content end--%>
        </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
</div>
</td>
        <td valign="top" style="padding:10px 0px 0px 0px;">
            <table id="tblMessages" style="display:none" cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Messages</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC" style="width: 100%">
        <%--content start--%>
        
        <div id="divMessagePlaceHolder">
            <asp:PlaceHolder ID="messagePlaceHolder"  runat="server" Visible="True"></asp:PlaceHolder>
            <asp:Label ID="AgencyInfo" runat="server" Visible="False" CssClass="Warning" EnableViewState="False" ></asp:Label>
            <div style="color:#ff0000;font-weight:bold;"><asp:Label runat="server" ID="lblDeletedInformation"></asp:Label></div>
        </div>

        <asp:Panel id="VerifyingPanelContainer" runat="server">
        <span id="VerifyingPanel" style="width: 100%; display:block;">
            <img id="Img1" runat="server" src='<%$ Code : Common.GetImageUrl("indicator.gif") %>' alt="!!" title="Please wait." /> Processing. Please Wait.
        </span>

        <asp:ValidationSummary  ID="ValidationSummary1" runat="server" style="width: 100%; display: block; margin-left: -25px;"
            SkinID="NewOneToBeDefaulted" >
        </asp:ValidationSummary>
        
                </asp:Panel>
        <%--content end--%>
            </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
<table id="otherSubmissions"  cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Other Submissions</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC" style="width: 100%">
        <%--content start--%>
        <asp:HiddenField ID="hdnSubmission" Value="" runat="server" />
        <asp:HiddenField ID="hdnCopiedSubmission" Value="" runat="server" />
        <asp:Label runat="server" ID="lbOtherSubmissions" Text="(none)"></asp:Label>
        
        <UserControls:Grid runat="server" ID="gvSubmissions" 
         AutoGenerateColumns="false" OnInit="gvSubmissions_Init"
         ShowHeader="true" Width="100%" HeaderStyle-HorizontalAlign="Left"
         >
         <Columns>
            <asp:TemplateField HeaderText="Sub #" HeaderStyle-HorizontalAlign="left">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkSubmissionID" runat="server" CausesValidation="False"
                        Text='<%# DataBinder.Eval(Container.DataItem,"submission_number") + " - " + DataBinder.Eval(Container.DataItem,"submission_number_sequence")  %>' CommandArgument='<%# Bind("id")  %>' OnClick="lnkOtherSubmission_Click"
                        OnClientClick="confirmPageLeave(event, 'Are you sure you want to continue?\r\n\r\nYou are currently adding/editing a submission that is not saved.\r\n\r\nPress OK to continue, or Cancel to stay on the current page.');">
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField> 
        </Columns>
        </UserControls:Grid>

            <asp:CheckBox runat="server" ID="chkShowAllOtherSubmissions" Checked="false" Visible="false" Text="Display All Submissions" OnCheckedChanged="chkOtherSubmissions_onCheckChanged" AutoPostBack="true" />
        <%--content end--%>
            </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
        </td>
    </tr>
</table>

 <asp:CustomValidator id="customPolicyNumberValidator" style="display:none" runat="server" OnServerValidate="customPolicyNumberValidator_ServerValidate"></asp:CustomValidator>
 <asp:CustomValidator id="cvAgentRequired" style="display:none" runat="server" OnServerValidate="cvAgentRequired_ServerValidate"></asp:CustomValidator>
</ContentTemplate>
</aje:UpdatePanel>

<aje:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" DynamicLayout="True" >
     <ProgressTemplate>
            <asp:Panel ID="PNLProgress" runat="server" style="width:400px; background-color:White; border-width:2px; border-color:Black; border-style:solid; padding:20px;">
                <img id="processing" runat="server" alt="processing" src='<%$ Code : Common.GetImageUrl("indicator.gif") %>' /> &nbsp; Processing... Please Wait....
            </asp:Panel>
            <ajx:ModalPopupExtender BackgroundCssClass="modalBackground" ID="mpeUpdateProgress" TargetControlID="PNLProgress" runat="server"
                PopupControlID="UpdateProgress1" BehaviorID="mpeUpdateProgressBehavior">
    </ajx:ModalPopupExtender>
        </ProgressTemplate>
</aje:UpdateProgress>
</asp:Content>