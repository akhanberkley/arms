using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.Components;

public partial class Links : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Grid1.SelectedIndex = -1;
        DetailsView1.ChangeMode(DetailsViewMode.Insert);
    }

    protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        e.Values.Add("CompanyId", Common.GetSafeIntFromSession(SessionKeys.CompanyId));
    }
    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        Grid1.DataBind();
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        Grid1.DataBind();
    }

    protected void ObjectDataSource2_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            ShowMessage(e.ReturnValue.ToString());
        }
    }

    protected void ObjectDataSource2_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            ShowMessage(e.ReturnValue.ToString());
        }
    }

    protected void Grid1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        if (BCS.WebApp.Components.Security.CanDelete())
            DetailsView1.ChangeMode(DetailsViewMode.Edit);
    }
    private void ShowMessage(string message)
    {
        ValidationTable.Style.Add("display", "");
        Common.SetStatus(messagePlaceHolder, message);
    }
    protected void ValidationButton_Init(object sender, EventArgs e)
    {
        Button btn = sender as Button;
        btn.Attributes.Add("onclick", string.Format("if(!Page_ClientValidate('{0}')) {{  $find('alwaysVisible').get_element().style.display = ''; $find('alwaysVisible').initialize(); }}", btn.ValidationGroup));
    }
}
