using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class CompanyNoLOBs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        ListItem[] items = Common.GetSelectedItems(ListBox1);
        ListBox2.Items.AddRange(items);
        foreach (ListItem item in items)
            ListBox1.Items.Remove(item);
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        ListItem[] items = Common.GetSelectedItems(ListBox2);
        ListBox1.Items.AddRange(items);
        foreach (ListItem item in items)
            ListBox2.Items.Remove(item);
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        object[] olobids = Common.GetValues(ListBox1);
        int[] lobids = new int[olobids.Length];
        for (int i = 0; i < olobids.Length; i++)
        {
            lobids[i] = Convert.ToInt32(olobids[i]);
        }
        if (Session["CompanyNumberSelected"] == null || Convert.ToInt32(Session["CompanyNumberSelected"]) == 0)
        {
            Common.SetStatus(messagePlaceHolder, "Please select a company and company number.");
            return;
        }
        string res = BCS.Core.Clearance.Admin.DataAccess.UpdateCompanyNumberLineOfBusiness(
             Convert.ToInt32(Session["CompanyNumberSelected"]), lobids);
        Common.SetStatus(messagePlaceHolder, res);
    }
}
