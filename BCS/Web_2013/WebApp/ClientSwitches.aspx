<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true"
    CodeFile="ClientSwitches.aspx.cs" Inherits="ClientSwitches" Title="Berkley Clearance System - Bulk Client Switch"
    Theme="DefaultTheme" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <ajx:ToolkitScriptManager ID="ScriptManager1" runat="server" CombineScripts="True">
        <Services>
            <aje:ServiceReference Path="WebServices/SubmissionService.asmx" />
        </Services>
    </ajx:ToolkitScriptManager>
    <aje:UpdatePanel ID="MainUpdatePanel" runat="server">
        <ContentTemplate>

            <script language="javascript" type="text/javascript">
<!-- 
var prm = Sys.WebForms.PageRequestManager.getInstance();

prm.add_initializeRequest(InitializeRequest);
prm.add_endRequest(EndRequest);

function InitializeRequest(sender, args) {
        CancelIfInAsyncPostBack();
    var pop = $find("mpeUpdateProgressBehavior");
    pop.show();
}

function EndRequest(sender, args) {
    var pop = $find("mpeUpdateProgressBehavior");
    pop.hide();
}
function CancelIfInAsyncPostBack() {
    if (prm.get_isInAsyncPostBack()) {
      prm.abortPostBack();
    }
}
    
if (typeof(Sys) !== "undefined") Sys.Application.notifyScriptLoaded();
// -->
            </script>

            <script type="text/javascript">
function mimicComboBox(elemSelect,elemTextBox)
{
    try
    {
        elemTextBox.value = elemSelect.value;
    }
    catch(err) {};
}
function showPopup(str, evt)
{
    setClientLoadTarget(str); // set target (submission number or policy number
    $find('beh').show(); // show popup
    $get('<%= targetClientIdTextBox.ClientID %>').focus(); // focus on the client id text box;
    cancelEvent(evt); // cancel event to prevent unneeded postback, since we are using image button for tabbing, focus and help in using space bar to invoke
}
function setClientLoadTarget(str)
{
    $get('<%=ClientTarget.ClientID %>').value = str;
}
var serviceProxy;
function warnOfSubmissions(elem, lookupType)
{
    serviceProxy = new BCS.WebApp.ScriptServices.SubmissionService();
    serviceProxy.set_defaultSucceededCallback(SucceededCallback);
    serviceProxy.set_defaultFailedCallback(FailedCallback);
    switch(lookupType)
    {
    case 'SubmissionNumber':
      {
        serviceProxy.set_defaultUserContext('<%= SubmissionNumberInfoPanel.ClientID  %>' + '|' +  '<%= SubmissionNumberSubmissionCount.ClientID  %>' + '|' +  '<%= txSubmissionNumbersNewClientIds.ClientID  %>' +  '|' + '<%= Button1.ClientID  %>');
        break;
      }
    case 'PolicyNumber':
      {
        serviceProxy.set_defaultUserContext('<%= PolicyNumberInfoPanel.ClientID  %>' + '|' +  '<%= PolicyNumberSubmissionCount.ClientID  %>' + '|' +  '<%= txPolicyNumbersNewClientIds.ClientID  %>' +  '|' + '<%= Button2.ClientID  %>' +  '|');
        break;
      }
    default:
      {
        break;
      }
    }
    serviceProxy.GetSubmissionCount(elem.value, lookupType); 
}
function SucceededCallback(result, eventArgs)
{    
    var resultCount = result.Count;
    var ctrlIds = serviceProxy.get_defaultUserContext();
    var arrCtrlIds = ctrlIds.split('|');
    $get(arrCtrlIds[0]).innerHTML = '#Submissions : ';
    $get(arrCtrlIds[1]).innerHTML = resultCount;

    if(ctrlIds.endsWith('Button1'))
    {
        StoRestore = resultCount;
    }
    if(ctrlIds.endsWith('Button2') || ctrlIds.endsWith('Button2|'))
    {
        PtoRestore = resultCount;
    }
    if(resultCount != 0 && $get(arrCtrlIds[2]).value != '')
    {
        $get(arrCtrlIds[3]).removeAttribute('disabled');
    }
    else
    {
        $get(arrCtrlIds[3]).setAttribute('disabled', 'true');
    }
}
function FailedCallback(error)
{
    var ctrlIds = serviceProxy.get_defaultUserContext();
    var arrCtrlIds = ctrlIds.split('|');  
    $get(arrCtrlIds[0]).innerHTML = '#Submissions : ';
    $get(arrCtrlIds[1]).innerHTML = '0';
    if(ctrlIds.endsWith('Button1'))
    {
        StoRestore = '0';
    }
    if(ctrlIds.endsWith('Button2'))
    {
        PtoRestore = '0';
    }
    $get(arrCtrlIds[3]).setAttribute('disabled', 'true');
}
var StoRestore = '';
var PtoRestore = '';
Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);
function pageLoaded(sender, args)
{
    if($find("beh") != null)
        $find("beh").add_shown(PopShown);
    if(StoRestore != '')
    {
        $get('<%= SubmissionNumberInfoPanel.ClientID  %>').innerHTML = '#Submissions : ';
        $get('<%= SubmissionNumberSubmissionCount.ClientID  %>').innerHTML = StoRestore;
        
        var btn = $get('<%= Button1.ClientID %>');
        if(StoRestore != '0' && $get('<%= txSubmissionNumbersNewClientIds.ClientID  %>').value != '')
        {
            btn.removeAttribute('disabled');
        }
        else
        {
            btn.setAttribute('disabled', 'true');
        }
    }
    if(PtoRestore != '')
    {
        $get('<%= PolicyNumberInfoPanel.ClientID  %>').innerHTML = '#Submissions : ';
        $get('<%= PolicyNumberSubmissionCount.ClientID  %>').innerHTML = PtoRestore;
        
        var btn = $get('<%= Button2.ClientID %>');
        if(PtoRestore != '0' && $get('<%= txPolicyNumbersNewClientIds.ClientID  %>').value != '')
        {
            btn.removeAttribute('disabled');
        }
        else
        {
            btn.setAttribute('disabled', 'true');
        }
    }
    if(!((Sys.Browser.agent === Sys.Browser.InternetExplorer) && (Sys.Browser.version < 7)))
    {
        $get('SubmissionNumberDiv').style.height = 'auto';
        $get('SubmissionNumberDiv').style.minHeight = '100px';
        $get('PolicyNumberDiv').style.height = 'auto';
        $get('PolicyNumberDiv').style.minHeight = '100px';
        $get('<%=programmaticPopup.ClientID %>').style.height = 'auto';
    }
}

function PopShown(sender, args)
{
    if($get('<%=programmaticPopup.ClientID %>').style.maxHeight == null); // try maxHeight behavior for IE6
    {
        if($get('<%=programmaticPopup.ClientID %>').offsetHeight > window.screen.availHeight - 100) // - 100, some arbitrary value
        {
            $get('<%=programmaticPopup.ClientID %>').style.height = '600px';
        }
    }
}
            </script>

            <asp:TextBox ID="ClientTarget" runat="server" CssClass="displayedNone">
            </asp:TextBox>
            <table cellpadding="0" cellspacing="0" class="box" width="70%" style="margin-top: 5px">
                <tbody>
                    <tr>
                        <td class="TL">
                            <div />
                        </td>
                        <td class="TC">
                            Switch Submissions</td>
                        <td class="TR">
                            <div />
                        </td>
                    </tr>
                    <tr>
                        <td class="ML">
                            <div />
                        </td>
                        <td class="MC" style="width: 100%">
                            <em>* Separate multiple values by comma.<br />
                                * All sequences will be switched.</em><br />
                            <br />
                            <table id="atable" style="width: 100%" rules="rows" cellpadding="5" border="0">
                                <tr>
                                    <td style="width: 30%">
                                        Enter submission number(s)
                                        <asp:TextBox ID="txSubmissionNumbers" runat="server" CssClass="txt" Rows="7" TextMode="MultiLine"
                                            Style="width: 90%; overflow: auto;" onchange="warnOfSubmissions(this, 'SubmissionNumber');"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfvSNs" runat="server" ControlToValidate="txSubmissionNumbers"
                                            Display="None" ErrorMessage="Please enter a submission number." ValidationGroup="S"
                                            SetFocusOnError="True">
                                        </asp:RequiredFieldValidator>
                                        <span id="SubmissionNumberInfoPanel" runat="server" class="Warning"></span><span
                                            id="SubmissionNumberSubmissionCount" runat="server" class="Warning"></span>
                                    </td>
                                    <td style="vertical-align: top;">
                                        Enter new client id
                                        <%-- Using image button with return false; so that it helps in focus and click on space pressed--%>
                                        <asp:ImageButton ID="AddImageSubmissionNumber" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("plus.gif") %>'
                                            AlternateText="Add" OnClientClick="showPopup('SubmissionNumber', event);" />
                                        <asp:ImageButton ID="RemoveImageSubmissionNumber" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("minus.gif") %>'
                                            AlternateText="Remove" Visible="False" OnCommand="RemoveClient" CommandName="SubmissionNumber" />
                                        <div class="txt" style="background-color: #EEEEEE;" id="SubmissionNumberDiv" style="height: 100px;">
                                            <asp:TextBox ID="txSubmissionNumbersNewClientIds" runat="server" CssClass="displayedNone"
                                                TextMode="MultiLine">
                                            </asp:TextBox>
                                            <asp:GridView ID="gridSubmissionNumbersNewClients" runat="server" AutoGenerateColumns="False"
                                                ShowHeader="False" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td style="width: 50%">
                                                                        Insured Name :
                                                                        <asp:Label ID="lblInsuredName" runat="server" Text='<%# Bind("InsuredName") %>'></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%">
                                                                        DBA :
                                                                        <asp:Label ID="lblDba" runat="server" Text='<%# Bind("Dba") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <hr />
                                                            <asp:GridView ID="names" runat="server" AutoGenerateColumns="False" DataSource='<%# Bind("Names") %>'
                                                                ShowHeader="False" SkinID="none" GridLines="None" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ckName" runat="server" Checked='<%# Bind("Selected") %>' Enabled="False" />
                                                                            <ajx:ToggleButtonExtender ID="tgAddress" runat="server" TargetControlID="ckName"
                                                                                CheckedImageUrl='<%$ Code : Common.GetImageUrl("checked.gif") %>' UncheckedImageUrl='<%$ Code : Common.GetImageUrl("unchecked.gif") %>'
                                                                                ImageHeight="15" ImageWidth="15">
                                                                            </ajx:ToggleButtonExtender>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="DisplayString" />
                                                                    <asp:BoundField DataField="Id" Visible="False" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <hr />
                                                            <asp:GridView ID="addresses" runat="server" AutoGenerateColumns="False" DataSource='<%# Bind("Addresses") %>'
                                                                ShowHeader="False" SkinID="none" GridLines="None" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ckAddress" runat="server" Checked='<%# Bind("Selected") %>' Enabled="False" />
                                                                            <ajx:ToggleButtonExtender ID="tgAddress" runat="server" TargetControlID="ckAddress"
                                                                                CheckedImageUrl='<%$ Code : Common.GetImageUrl("checked.gif") %>' UncheckedImageUrl='<%$ Code : Common.GetImageUrl("unchecked.gif") %>'
                                                                                ImageHeight="15" ImageWidth="15">
                                                                            </ajx:ToggleButtonExtender>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="DisplayString" />
                                                                    <asp:BoundField DataField="Id" Visible="False" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <asp:RequiredFieldValidator ID="rfvSNClientIds" runat="server" ControlToValidate="txSubmissionNumbersNewClientIds"
                                                Display="None" ErrorMessage="Please enter a client id." ValidationGroup="S" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </td>
                                    <td>
                                        <asp:Button ID="Button1" runat="server" Text="Switch" ValidationGroup="S" OnCommand="MakeSwitch"
                                            AccessKey="1" ToolTip="Alt+1" OnInit="ValidationButton_Init" Enabled="False" OnLoad="SubmitButtons_OnLoad" />
                                        <asp:HiddenField runat="server" ID="hdfSubmission" Value="false"  />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 30%">
                                        Enter policy number(s)
                                        <asp:TextBox ID="txPolicyNumbers" runat="server" CssClass="txt" Rows="7" TextMode="MultiLine"
                                            Style="width: 90%; overflow: auto;" onchange="warnOfSubmissions(this, 'PolicyNumber');"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfvPNs" runat="server" ControlToValidate="txPolicyNumbers"
                                            Display="None" ErrorMessage="Please enter a policy number." ValidationGroup="P"
                                            SetFocusOnError="True">
                                        </asp:RequiredFieldValidator>
                                        <span id="PolicyNumberInfoPanel" runat="server" class="Warning"></span><span id="PolicyNumberSubmissionCount"
                                            runat="server" class="Warning"></span>
                                    </td>
                                    <td style="vertical-align: top;">
                                        Enter new client id
                                        <%-- Using image button with return false; so that it helps in focus and click on space pressed--%>
                                        <asp:ImageButton ID="AddImagePolicyNumber" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("plus.gif") %>'
                                            AlternateText="Add" OnClientClick="showPopup('PolicyNumber', event);" />
                                        <asp:ImageButton ID="RemoveImagePolicyNumber" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("minus.gif") %>'
                                            AlternateText="Remove" Visible="False" OnCommand="RemoveClient" CommandName="PolicyNumber" />
                                        <div class="txt" style="background-color: #EEEEEE;" id="PolicyNumberDiv" style="height: 100px;">
                                            <asp:TextBox ID="txPolicyNumbersNewClientIds" runat="server" CssClass="displayedNone"
                                                TextMode="MultiLine">
                                            </asp:TextBox>
                                            <asp:GridView ID="gridPolicyNumbersNewClients" runat="server" AutoGenerateColumns="False"
                                                ShowHeader="False" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td style="width: 50%">
                                                                        Insured Name :
                                                                        <asp:Label ID="lblInsuredName" runat="server" Text='<%# Bind("InsuredName") %>'></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%">
                                                                        DBA :
                                                                        <asp:Label ID="lblDba" runat="server" Text='<%# Bind("Dba") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <hr />
                                                            <asp:GridView ID="names" runat="server" AutoGenerateColumns="False" DataSource='<%# Bind("Names") %>'
                                                                ShowHeader="False" SkinID="none" GridLines="None" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ckName" runat="server" Checked='<%# Bind("Selected") %>' Enabled="False" />
                                                                            <ajx:ToggleButtonExtender ID="tgAddress" runat="server" TargetControlID="ckName"
                                                                                CheckedImageUrl='<%$ Code : Common.GetImageUrl("checked.gif") %>' UncheckedImageUrl='<%$ Code : Common.GetImageUrl("unchecked.gif") %>'
                                                                                ImageHeight="15" ImageWidth="15">
                                                                            </ajx:ToggleButtonExtender>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="DisplayString" />
                                                                    <asp:BoundField DataField="Id" Visible="False" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <hr />
                                                            <asp:GridView ID="addresses" runat="server" AutoGenerateColumns="False" DataSource='<%# Bind("Addresses") %>'
                                                                ShowHeader="False" SkinID="none" GridLines="None" Width="100%">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="5%">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ckAddress" runat="server" Checked='<%# Bind("Selected") %>' Enabled="False" />
                                                                            <ajx:ToggleButtonExtender ID="tgAddress" runat="server" TargetControlID="ckAddress"
                                                                                CheckedImageUrl='<%$ Code : Common.GetImageUrl("checked.gif") %>' UncheckedImageUrl='<%$ Code : Common.GetImageUrl("unchecked.gif") %>'
                                                                                ImageHeight="15" ImageWidth="15">
                                                                            </ajx:ToggleButtonExtender>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="DisplayString" />
                                                                    <asp:BoundField DataField="Id" Visible="False" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <asp:RequiredFieldValidator ID="rfvPNClientIds" runat="server" ControlToValidate="txPolicyNumbersNewClientIds"
                                                Display="None" ErrorMessage="Please enter a client id." ValidationGroup="P" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </td>
                                    <td>
                                        <asp:Button ID="Button2" runat="server" Text="Switch" ValidationGroup="P" OnCommand="MakeSwitch"
                                            AccessKey="2" ToolTip="Alt+2" OnInit="ValidationButton_Init" Enabled="False"  OnLoad="SubmitButtons_OnLoad"/>
                                        <asp:HiddenField runat="server" ID="hdfPolicy" Value="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="MR">
                            <div />
                        </td>
                    </tr>
                    <tr>
                        <td class="BL">
                            <div />
                        </td>
                        <td class="BC">
                            <div />
                        </td>
                        <td class="BR">
                            <div />
                        </td>
                    </tr>
                </tbody>
            </table>
            <table id="ValidationTable" runat="server" cellpadding="0" cellspacing="0" class="box"
                width="30%" style="display: none;">
                
                    <tr>
                        <td class="TL">
                            <div />
                        </td>
                        <td class="TC">
                            Validation</td>
                        <td class="TR">
                            <div />
                        </td>
                    </tr>
                    <tr>
                        <td class="ML">
                            <div />
                        </td>
                        <td class="MC" style="width: 100%">
                 
                            <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="S"
                                    SkinID="NewOneToBeDefaulted"></asp:ValidationSummary>
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="P"
                                    SkinID="NewOneToBeDefaulted"></asp:ValidationSummary>
                            </asp:PlaceHolder>
                            
                            <asp:LinkButton ID="lnkClientIDSearch" runat="server" CausesValidation="False"
                                AccessKey="R" Text="Get Submissions for this Client" OnClick="lnkClientIDSearch_OnClick"
                                visible="false"
                            ></asp:LinkButton>
                        </td>
                        <td class="MR">
                            <div />
                        </td>
                    </tr>
                    <tr>
                        <td class="BL">
                            <div />
                        </td>
                        <td class="BC">
                            <div />
                        </td>
                        <td class="BR">
                            <div />
                        </td>
                    </tr>
                
            </table>
            <ajx:AlwaysVisibleControlExtender ID="avceVerify" runat="server" TargetControlID="ValidationTable"
                VerticalOffset="100" HorizontalSide="Right">
            </ajx:AlwaysVisibleControlExtender>
            <span id="dummySpan4modalPopupTarget" runat="server"></span>
            <ajx:ModalPopupExtender runat="server" ID="programmaticModalPopup" TargetControlID="dummySpan4modalPopupTarget"
                PopupControlID="programmaticPopup" BackgroundCssClass="modalBackground" DropShadow="True"
                BehaviorID="beh" RepositionMode="RepositionOnWindowScroll" CancelControlID="CancelButton">
            </ajx:ModalPopupExtender>
            <asp:Panel ID="programmaticPopup" runat="server" CssClass="modalPopup" Width="700px" style="display:none; max-height: 600px;" ScrollBars="Auto">
                <table runat="server" id="tblClientInfo" cellpadding="8" cellspacing="0" width="100%">
                    <tr>
                        <th style="border: gray 1px dotted; vertical-align: top; color: white; background-color: #00563f;">
                            Load</th>
                    </tr>
                    <tr>
                        <td style="border: gray 1px dotted; vertical-align: top;">
                            <%--target client--%>
                            <h3 class="sectionGroupHeader">
                                Client Information: #
                                <asp:TextBox ID="targetClientIdTextBox" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvRequiredId" runat="server" Text="*" ErrorMessage="*"
                                    ValidationGroup="merge" ControlToValidate="targetClientIdTextBox" Display="Dynamic"
                                    SetFocusOnError="True">
                                </asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cvSameId" runat="server" ControlToValidate="targetClientIdTextBox"
                                    ValidationGroup="merge" ErrorMessage="!!" Text="same client" Display="Dynamic"
                                    Operator="NotEqual" SetFocusOnError="True">
                                </asp:CompareValidator>
                                &nbsp;
                                <%-- onclient click is needed for IE6, since modal popup hides list controls and we have modal popup (update progress) over modal popup--%>
                                <asp:Button ID="loadTargetClientButton" runat="server" Text="Load Client" OnClick="loadTargetClientButton_Click"
                                    ValidationGroup="merge" OnClientClick="if(Page_ClientValidate('merge')) { $find('beh').hide(); }" />
                            </h3>
                            <asp:Panel ID="targetPanel" runat="server" Visible="False">
                                <table width="100%">
                                    <tr>
                                        <td style="vertical-align:top; white-space:nowrap; width: 5%;">Insured Name :</td>
                                        <td style="width: 40%;">
                                            <asp:TextBox ID="txInsuredName" runat="server" autocomplete="off" CssClass="txt" Columns="47"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvInsuredName" runat="server" ControlToValidate="txInsuredName"
                                                Enabled='<%$ Code : ConfigValues.RequiresInsuredName(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'
                                                SetFocusOnError="True" Display="Dynamic" ErrorMessage="*" Text="" ValidationGroup="switching">
                                            </asp:RequiredFieldValidator><br />
                                            <asp:ListBox ID="lbInsuredNames" runat="server" CssClass="txt" style="width: 92%;" OnInit="lbInsuredNames_Init"></asp:ListBox>
                                        </td>
                                        <td style="vertical-align: top; white-space: nowrap; width: 5%;">DBA :</td>
                                        <td style="width: 40%;">
                                            <asp:TextBox ID="txDBA" runat="server" autocomplete="off" CssClass="txt" Columns="47"></asp:TextBox><br />
                                            <asp:ListBox ID="lbDbaNames" runat="server" CssClass="txt" style="width: 92%;" OnInit="lbDbaNames_Init"></asp:ListBox>
                                        </td>
                                    </tr>
                                </table>                                
                                <h4 class="sectionGroupHeader">
                                    Client Names</h4>
                                <UserControls:CheckBoxList4BCS ID="cblNames1" runat="server" ValidationGroup="switching">
                                </UserControls:CheckBoxList4BCS>
                                <UserControls:RequiredFieldValidatorForCheckBoxLists ID="requiredName" runat="server"
                                    ControlToValidate="cblNames1" Display="Dynamic" SetFocusOnError="True" ErrorMessage="At least one name is required."
                                    ValidationGroup="switching">
                                </UserControls:RequiredFieldValidatorForCheckBoxLists>
                                <h4 class="sectionGroupHeader">
                                    Client Addresses</h4>
                                <UserControls:CheckBoxList4BCS ID="cblAddresses1" runat="server" ValidationGroup="switching">
                                </UserControls:CheckBoxList4BCS>
                                <UserControls:RequiredFieldValidatorForCheckBoxLists ID="requiredAddress" runat="server"
                                    ControlToValidate="cblAddresses1" Display="Dynamic" SetFocusOnError="True" ErrorMessage="At least one address is required."
                                    ValidationGroup="switching">
                                </UserControls:RequiredFieldValidatorForCheckBoxLists>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <p style="text-align: center;">
                    <%-- onclient click is needed for IE6, since modal popup hides list controls and we have modal popup (update progress) over modal popup--%>
                    <asp:Button ID="OkButton" runat="server" Text="OK" CausesValidation="True" ValidationGroup="switching"
                        OnClick="Merge" Enabled="False" OnClientClick="if(Page_ClientValidate('switching')) { $find('beh').hide(); }" />
                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" CausesValidation="False" />
                </p>
            </asp:Panel>
        </ContentTemplate>
    </aje:UpdatePanel>
    <aje:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            <asp:Panel ID="PNLProgress" runat="server" Style="width: 400px; background-color: White;
                border-width: 2px; border-color: Black; border-style: solid; padding: 20px;">
                <img id="imgProcessimg" runat="server" alt="processing" src='<%$ Code : Common.GetImageUrl("indicator.gif") %>' />
                &nbsp; Processing... Please Wait....
            </asp:Panel>
            <ajx:ModalPopupExtender BackgroundCssClass="modalBackground" ID="mpeUpdateProgress"
                BehaviorID="mpeUpdateProgressBehavior" TargetControlID="PNLProgress" runat="server"
                PopupControlID="UpdateProgress1">
            </ajx:ModalPopupExtender>
        </ProgressTemplate>
    </aje:UpdateProgress>
</asp:Content>
