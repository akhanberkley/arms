using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.Core.Clearance.BizObjects;
using BCS.Core.Clearance.Admin;

public partial class AgencyLOBs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void gridAgencies_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void gridAgencies_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        gridAgencies.SelectedIndex = -1;        
    }
  
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        ListItem[] items = Common.GetSelectedItems(ListBox2);
        ListBox1.Items.AddRange(items);
        foreach (ListItem item in items)
            ListBox2.Items.Remove(item);
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        ListItem[] items = Common.GetSelectedItems(ListBox1);
        ListBox2.Items.AddRange(items);
        foreach (ListItem item in items)
            ListBox1.Items.Remove(item);
    }
    protected void gridAgencies_PageIndexChanged(object sender, EventArgs e)
    {
        gridAgencies.SelectedIndex = -1;
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if ((sender as Button).Text == "Cancel")
        {
            gridAgencies.SelectedIndex = -1;
            return;
        }
        object[] olobids = Common.GetValues(ListBox1);
        int[] lobids = new int[olobids.Length];
        for (int i = 0; i < olobids.Length; i++)
        {
            lobids[i] = Convert.ToInt32(olobids[i]);
        }
        string res = BCS.Core.Clearance.Admin.DataAccess.UpdateAgencyLineOfBusiness(
            Convert.ToInt32(gridAgencies.SelectedDataKey.Value), lobids);
        Common.SetStatus(messagePlaceHolder, res);
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        atable.Visible = gridAgencies.SelectedIndex >= 0;        
    }
      
    protected void DropDownList1_PreRender(object sender, EventArgs e)
    {
        if (DropDownList1.Items.FindByText("") == null)
            DropDownList1.Items.Insert(0, new ListItem("", "0"));
    }
}
