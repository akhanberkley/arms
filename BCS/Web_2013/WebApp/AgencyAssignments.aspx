<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="AgencyAssignments.aspx.cs" Inherits="AgencyAssignments" Title="Berkley Clearance System - Agency Assignments" Theme="DefaultTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<ajx:ToolkitScriptManager ID="ScriptManager1" runat="server" CombineScripts="True" >
</ajx:ToolkitScriptManager>
<asp:PlaceHolder runat="server" ID="messagePlaceHolder">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
    <br />
</asp:PlaceHolder>
<script type="text/javascript">
   function Confirm(source, clientside_arguments)
   {
      if (document.getElementById('<%=DropDownList2.ClientID%>').value == 0 || document.getElementById('<%=ddlAgencies.ClientID%>').value == 0 )
      {
         clientside_arguments.IsValid=false;
         source.innerText = "Please select From and To Agencies.";
         if (document.getElementById('_ctl0_ContentPlaceHolder1_errMessage') != null)
            document.getElementById('_ctl0_ContentPlaceHolder1_errMessage').innerText = '';
         return;
      }
      clientside_arguments.IsValid = confirm('Existing persons of the destination agency will be overwritten?');
      source.innerText = '';
      return;
   }
</script>
<table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Agency Assignments</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
<table width="100%">
        <tr>
            <td style="vertical-align: bottom; white-space:nowrap;"><span> Select Agency : </span><br />
                <UserControls:AgencyDropDownList ID="ddlAgencies" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAgencies_SelectedIndexChanged" OnPreRender="ddl_PreRender" ValidationGroup="Save Copy Move" OnInit="ddlAgencies_Init" IdentifyInactive="True">
                </UserControls:AgencyDropDownList>
                <ajx:ListSearchExtender ID="ListSearchExtender1" runat="server" TargetControlID="ddlAgencies" PromptPosition="Right"></ajx:ListSearchExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlAgencies"
                    Display="Dynamic" ErrorMessage="&nbsp;" Text="*" InitialValue="0" ValidationGroup="Save"></asp:RequiredFieldValidator></td>
            <td style="vertical-align: bottom; white-space:nowrap;">
                <asp:Button ID="btnCopy" runat="server"  Text="Copy To" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' OnClick="btnCopy_Click" ValidationGroup="CopyMove" />                
            </td>
            <td style="vertical-align: bottom; white-space:nowrap;">
                &nbsp;<asp:Button ID="btnMove" runat="server"  Text="Move To" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' OnClick="btnMove_Click" ValidationGroup="CopyMove" />&nbsp;
                <UserControls:AgencyDropDownList ID="DropDownList2" runat="server" OnPreRender="ddl_PreRender" OnInit="ddlAgencies_Init" IdentifyInactive="True">
                </UserControls:AgencyDropDownList>
                <ajx:ListSearchExtender ID="ListSearchExtender2" runat="server" TargetControlID="DropDownList2"></ajx:ListSearchExtender>
            </td>            
        </tr>        
        <tr>
            <td> &nbsp; </td><td style="width: 75px"></td><td></td>
        </tr>
        <tr>
            <td> &nbsp; </td><td style="width: 75px"></td><td></td>
        </tr>
        <tr>
            <td style="width:45%; vertical-align: top;"><span>Assigned Persons</span><br />
                <asp:ListBox ID="ListBox1" runat="server" DataTextField="FullName" DataValueField="Id" Width="100%" Rows="7" ValidationGroup="Save" AutoPostBack="True" OnSelectedIndexChanged="ListBox1_SelectedIndexChanged" DataSourceID="odsP" OnPreRender="SortOnPreRender"></asp:ListBox>
                <ajx:ListSearchExtender ID="ListSearchExtender3" runat="server" TargetControlID="ListBox1" PromptPosition="Right"></ajx:ListSearchExtender>
                <asp:ObjectDataSource ID="odsP" runat="server" SelectMethod="GetPersonsOfAgency"
                    TypeName="BCS.Core.Clearance.Admin.DataAccess" OnSelected="odsP_Selected">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlAgencies" Name="agencyId" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ListBox1"
                    ErrorMessage="&nbsp;" Text="* Please select the person to be saved." ValidationGroup="Save"></asp:RequiredFieldValidator>
                </td>
            <td align="center" style="width: 75px">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:Button ID="btnAddPerson" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly()  && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' Text=">" OnClick="btnAddPerson_Click" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Button ID="btnRemovePerson" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly()  && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' Text="<" OnClick="btnRemovePerson_Click" />
                        </td>
                    </tr>
                </table>
            </td>       
            <td style="width:45%; vertical-align: top;"><span>Available Persons</span><br />
                <asp:ListBox ID="ListBox2" runat="server" DataTextField="FullName" DataValueField="Id" SelectionMode="Multiple" Width="100%" Rows="7" DataSourceID="odsNP" OnPreRender="SortOnPreRender"></asp:ListBox>
                <ajx:ListSearchExtender ID="ListSearchExtender4" runat="server" TargetControlID="ListBox2" PromptPosition="Right"></ajx:ListSearchExtender>
                <asp:ObjectDataSource ID="odsNP" runat="server" SelectMethod="GetPersonsNotOfAgency"
                    TypeName="BCS.Core.Clearance.Admin.DataAccess" OnSelected="odsNP_Selected">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlAgencies" Name="agencyId" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                Assigned Underwriting Units
            </td>
            <td style="width: 75px">
                
            </td>
            <td>
                Available Underwriting Units
            </td>
        </tr>
        <tr>
            <td width="45%">
                <asp:ListBox ID="ListBox3" runat="server" DataTextField="Code"
                    DataValueField="Id" OnDataBound="ListBox1_DataBound" Width="100%" Rows="10" ValidationGroup="Save" OnPreRender="SortOnPreRender" AutoPostBack="True" OnSelectedIndexChanged="ListBox3_SelectedIndexChanged" DataSourceID="odsLob" SelectionMode="Multiple">
                </asp:ListBox><asp:ObjectDataSource ID="odsLob" runat="server" SelectMethod="GetLineOfBusinessesByAgencyIds"
                    TypeName="BCS.Core.Clearance.Admin.DataAccess">
                    <SelectParameters>
                        <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                        <asp:ControlParameter ControlID="ListBox1" Name="personId" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="ddlAgencies" Name="agencyIds" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                </td>
            <td align="center" style="width: 75px">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:Button ID="btnAddLOB" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text=">" OnClick="btnAddLOB_Click" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Button ID="btnRemoveLOB" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text="<" OnClick="btnRemoveLOB_Click" />
                        </td>
                    </tr>
                </table>
            </td>       
            <td width="45%">
                <asp:ListBox ID="ListBox4" runat="server" DataTextField="Code"
                    DataValueField="Id" SelectionMode="Multiple" Width="100%" Rows="10" OnPreRender="SortOnPreRender" DataSourceID="odsNLOB"></asp:ListBox><asp:ObjectDataSource ID="odsNLOB" runat="server" SelectMethod="GetLineOfBusinessesNotByAgencyIds"
                    TypeName="BCS.Core.Clearance.Admin.DataAccess">
                    <SelectParameters>
                        <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                        <asp:ControlParameter ControlID="ListBox1" Name="personId" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="ddlAgencies" Name="agencyIds" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                Assigned
                Roles</td>
            <td style="width: 75px">
                
            </td>
            <td>
                Available Roles</td>
        </tr>
        <tr>        
            <td width="45%">
                <asp:ListBox ID="ListBox5" runat="server" DataTextField="RoleName" DataValueField="Id" SelectionMode="Multiple" Width="100%" ValidationGroup="Save" AutoPostBack="True" OnSelectedIndexChanged="ListBox5_SelectedIndexChanged" DataSourceID="odsPR" ></asp:ListBox>
                <asp:ObjectDataSource ID="odsPR" runat="server" SelectMethod="GetRolesOfPerson"
                    TypeName="BCS.Core.Clearance.Admin.DataAccess">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ListBox1" Name="personid" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="ddlAgencies" Name="agencyId" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td align="center" style="width: 75px">
                <table>
                    <tr>
                        <td valign="top">
                            <asp:Button ID="btnAddRole" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text=">" OnClick="btnAddRole_Click" /></td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Button ID="btnRemoveRole" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text="<" OnClick="btnRemoveRole_Click" />
                        </td>
                    </tr>
                </table>
            </td>       
            <td width="45%">
                <asp:ListBox ID="ListBox6" runat="server" DataTextField="RoleName" DataValueField="Id" SelectionMode="Multiple" Width="100%" DataSourceID="odsPNR" ></asp:ListBox>
                <asp:ObjectDataSource ID="odsPNR" runat="server" SelectMethod="GetRolesNotOfPerson"
                    TypeName="BCS.Core.Clearance.Admin.DataAccess">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ListBox1" Name="personid" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="ddlAgencies" Name="agencyId" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>                   
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:CheckBox ID="cbPrimary" runat="server" Text="Set as primary" />&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">                
                <asp:Button ID="btnSave" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text="Save" OnClick="btnSave_Click" ValidationGroup="Save"/>&nbsp;
                <asp:Button ID="Button1" runat="server"  Text="Cancel" OnClick="Button1_Click" CausesValidation="False"/></td>
        </tr>
    </table>
    <div>
    <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="Confirm" CssClass="Error" Display="Dynamic" ValidationGroup="CopyMove" OnServerValidate="CustomValidator1_ServerValidate"></asp:CustomValidator>
    </div>
    
    </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
</asp:Content>

