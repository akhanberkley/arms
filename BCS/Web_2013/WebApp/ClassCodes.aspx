<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true"
    CodeFile="ClassCodes.aspx.cs" Inherits="ClassCodes" Title="Berkley Clearance System - Class Codes"
    Theme="DefaultTheme" %>

<%@ Register TagPrefix="UserControls" Namespace="BCS.WebApp.UserControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--

TODO:

Temporary (hidden field) workaround for linkbutton clicks not restoring parent as active tab, instead restores to last active tab other than parent.
http://forums.asp.net/p/1068120/1550723.aspx#1550723

Caution :: using VS designer with ajax tab container and panel adds unnecessary attributes to tags, and sets unintended properties.

--%>

    <script type="text/javascript">
    function ActiveTabChanged(sender, e) {
        var activeTab = $get('<%=activeTab.ClientID%>');
        activeTab.value = sender.get_activeTabIndex();
    }
    </script>

    <asp:HiddenField ID="activeTab" runat="server" />
    <aje:ScriptManager id="ScriptManager1" runat="server" OnAsyncPostBackError="ScriptManager1_AsyncPostBackError">
    </aje:ScriptManager>
    <aje:UpdateProgress ID="UpdateProgress1" runat="server">
        <progresstemplate>
            <asp:Panel ID="PNLProgress" runat="server" style="width:400px; background-color:White; border-width:2px; border-color:Black; border-style:solid; padding:20px;">
                <img id="imgProcessing" runat="server" alt="processing" src='<%$ Code : Common.GetImageUrl("indicator.gif") %>' /> &nbsp; Processing... Please Wait....
            </asp:Panel>
            <ajx:ModalPopupExtender BackgroundCssClass="modalBackground" ID="mpeUpdateProgress" TargetControlID="PNLProgress" runat="server" PopupControlID="UpdateProgress1">
    </ajx:ModalPopupExtender>
        </progresstemplate>
    </aje:UpdateProgress>
    <table cellpadding="0" cellspacing="0" class="box">
        <tbody>
            <tr>
                <td class="TL"><div /></td>
                <td class="TC">Class Codes</td>
                <td class="TR"><div /></td>
            </tr>
            <tr>
                <td class="ML">
                    <div />
                </td>
                <td class="MC">
                    <ajx:TabContainer ID="TabContainer1" runat="server" OnClientActiveTabChanged="ActiveTabChanged"
                        ActiveTabIndex="0">
                        <ajx:TabPanel ID="TabPanel1" runat="server" HeaderText="Class Codes">
                            <ContentTemplate>
                                <aje:UpdatePanel id="UpdatePanel2" runat="server">
                                    <contenttemplate>
                                    <asp:Panel id="Panel2" runat="server" CssClass="tabcontent">
                                    <%--region filters--%>
                                    <br />
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>Code</td>
                                                <td><asp:TextBox id="txFilterCode" runat="server"></asp:TextBox></td>
                                                <td>Description</td>
                                                <td><asp:TextBox id="txFilterDescription" runat="server"></asp:TextBox></td>
                                                <td><asp:Button id="btnFilterClassCodeTab" onclick="btnFilterClassCodeTab_Click" runat="server" CausesValidation="false" Text="Filter"></asp:Button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br />
                                    <br />
                                    <%--end region filters--%>
                                    <UserControls:Grid id="gvClassCodes" runat="server" EmptyDataText="No Class Codes exist for the Company Number entered." 
                                        DataSourceID="ObjectDataSource2" AllowPaging="True" DataKeyNames="Id" AutoGenerateColumns="False" OnInit="gvClassCodes_Init"
                                        OnSelectedIndexChanging="gvClassCodes_SelectedIndexChanging" OnRowDeleted="gvClassCodes_RowDeleted">
                                        <Columns>
            <asp:CommandField ButtonType="Button" ShowSelectButton="True" SelectText="Edit" ControlStyle-Width="40px" />
                                            <asp:BoundField DataField="Id" Visible="False" HeaderText="Id"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Code Value">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label22" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CodeValue") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                <asp:Label ID="lblDescription" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "Description") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SIC Code"><ItemTemplate>
                                                                                        <asp:Label ID="Label29" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "SicCodeList.Code") %>'></asp:Label>
                                                                                    
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SIC Description"><ItemTemplate>
                                                                                        <asp:Label ID="Label21" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "SicCodeList.CodeDescription") %>'></asp:Label>
                                                                                    
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NAICS Code">
                                          <ItemTemplate>
                                            <asp:Label ID="lblNAICSCode" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NAICSCodeList.Code") %>'></asp:Label>
                                          </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NAICS Description">
                                          <ItemTemplate>
                                            <asp:Label ID="lblNAICSDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NAICSCodeList.CodeDescription") %>'></asp:Label>
                                          </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False"><ItemTemplate>
                                                                                        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete" 
                                                                                         Text="Delete" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() %>' />
                                                                                        <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="btnDelete"
                                                                                            ConfirmText='<%# DataBinder.Eval (Container.DataItem, "CodeValue", "Are you sure you want to delete this Class Code {0}?") %>'>
                                                                                        </ajx:ConfirmButtonExtender>
                                                                                     
                                        </ItemTemplate>
                                        </asp:TemplateField>
                                        </Columns>
                                    <PagerTemplate>
                                    <asp:PlaceHolder runat="server"></asp:PlaceHolder>
                                    </PagerTemplate>
                                    </UserControls:Grid> <asp:ObjectDataSource id="ObjectDataSource2" runat="server" OnDeleted="ObjectDataSource2_Deleted" SelectMethod="GetClassCodes" TypeName="BCS.Core.Clearance.Admin.DataAccess" DeleteMethod="DeleteClassCode"><DeleteParameters>
                                    <asp:Parameter Type="Int32" Name="id"></asp:Parameter>
                                    </DeleteParameters>
                                    <SelectParameters>
                                    <asp:ControlParameter PropertyName="Text" Type="String" Name="filterCodeValue" ControlID="txFilterCode"></asp:ControlParameter>
                                    <asp:ControlParameter PropertyName="Text" Type="String" Name="filterDesc" ControlID="txFilterDescription"></asp:ControlParameter>
                                    <asp:SessionParameter SessionField="CompanyNumberSelected" Type="Int32" DefaultValue="" Name="companyNumberId"></asp:SessionParameter>
                                    </SelectParameters>
                                    </asp:ObjectDataSource> <BR /><asp:Button id="Button2" onclick="Button2_Click" runat="server" Text="Add" Enabled="<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() %>"></asp:Button> <BR /><BR /><asp:PlaceHolder id="ccMessagePlaceHolder" runat="server">
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ></asp:ValidationSummary>
                                            <br />
                                        </asp:PlaceHolder> <asp:DetailsView id="dvClassCodes" runat="server" OnInit="dvClassCodes_Init" DataSourceID="odsClassCode" DataKeyNames="Id" OnModeChanging="dvClassCodes_ModeChanging" OnItemUpdated="dvClassCodes_ItemUpdated" OnItemInserted="dvClassCodes_ItemInserted" AutoGenerateRows="False"><Fields>
                                    <asp:TemplateField Visible="False" HeaderText="Id"><EditItemTemplate>
                                                                                        <asp:Label ID="Labelx1" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                    
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                                                                        <asp:Label ID="Labelx1" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                    
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                                                                        <asp:Label id="Labelx1" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                                                                    
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SIC Code"><EditItemTemplate>
                                                                                        <UserControls:SicCodeDropDownList id="SicCodeDropDownList2" runat="server" SelectedValue='<%# Bind("SicCodeId") %>'></UserControls:SicCodeDropDownList>
                                                                                    
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                                                                        <UserControls:SicCodeDropDownList ID="SicCodeDropDownList1" runat="server" SelectedValue='<%# Bind("SicCodeId") %>'>
                                                                                        </UserControls:SicCodeDropDownList>
                                                                                    
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                                                                        <asp:Label id="Labelsic" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "SicCodeList.CodeDescription") %>'></asp:Label> 
                                                                                    
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NAICS Code">
                                       <EditItemTemplate>
                                                                                       <UserControls:NaicsCodeDropDownList ID="NaicsCodeDropDownList2" runat="server" SelectedValue='<%# Bind("NaicsCodeId") %>'></UserControls:NaicsCodeDropDownList>
                                       </EditItemTemplate>
                                       <InsertItemTemplate>
                                                                                       <UserControls:NaicsCodeDropDownList ID="NaicsCodeDropDownList1" runat="server" SelectedValue='<%# Bind("NaicsCodeId") %>'></UserControls:NaicsCodeDropDownList>
                                       </InsertItemTemplate>
                                       <ItemTemplate>
                                                                                       <asp:Label ID="lblNaics" runat="server" Text='<%# DataBinder.Eval(Container.DataItem , "NAICSCodeList.CodeDescription") %>'></asp:Label>
                                       </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code Value"><EditItemTemplate>
                                                                                        <asp:TextBox id="TextBox2" runat="server" Text='<%# Bind("CodeValue") %>'></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="&nbsp;" Text="*" Display="Dynamic" ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
                                                                                    
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                                                                        <asp:TextBox id="TextBox2" runat="server" Text='<%# Bind("CodeValue") %>'></asp:TextBox> <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ErrorMessage="&nbsp;" Text="*" Display="Dynamic" ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
                                                                                    
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" Text='<%# Bind("CodeValue") %>' id="Label1"></asp:Label>
                                                                                    
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description"><EditItemTemplate>
                                                                                        <asp:TextBox id="TextBox222" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                                                                                    
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                                                                        <asp:TextBox id="TextBox222" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                                                                                    
                                    </InsertItemTemplate>
                                    <ItemTemplate>
                                                                                        <asp:Label runat="server" Text='<%# Bind("Description") %>' id="Label122"></asp:Label>
                                                                                    
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Button" ShowInsertButton="True" ShowEditButton="True"></asp:CommandField>
                                    </Fields>
                                    </asp:DetailsView> <asp:ObjectDataSource id="odsClassCode" runat="server" SelectMethod="GetClassCode" TypeName="BCS.Core.Clearance.Admin.DataAccess" OnInserting="odsClassCode_Inserting" OnUpdated="odsClassCode_Updated" OnInserted="odsClassCode_Inserted" UpdateMethod="UpdateClassCode" DataObjectTypeName="BCS.Core.Clearance.BizObjects.ClassCode" InsertMethod="AddClassCode">
                                                                            <SelectParameters>
                                                                                <asp:ControlParameter PropertyName="SelectedValue" Type="Int32" Name="id" ControlID="gvClassCodes"></asp:ControlParameter>
                                                                            </SelectParameters>
                                                                        </asp:ObjectDataSource>
                                    </asp:Panel> 
</contenttemplate>
                                </aje:UpdatePanel>
                            </ContentTemplate>
                        </ajx:TabPanel>
                        <ajx:TabPanel ID="TabPanel2" runat="server" HeaderText="Class Codes Hazard Grades">
                            <ContentTemplate>
                                <aje:UpdatePanel id="UpdatePanel1" runat="server">
                                    <contenttemplate>
                                    <br />
                                    <asp:Panel id="Panel1" runat="server" CssClass="tabcontent">
                                        <asp:TextBox id="TextBox1" runat="server"></asp:TextBox>&nbsp;
                                        <asp:Button id="Button1" onclick="Button1_Click" runat="server" Text="Filter by Class Code"></asp:Button>
                                        <br /><br />
                                        <UserControls:Grid id="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" AllowPaging="True"
                                         DataSourceID="ObjectDataSource1" OnRowEditing="GridView1_RowEditing" OnRowDataBound="GridView1_RowDataBound" 
                                         OnRowUpdated="GridView1_RowUpdated" EmptyDataText="No Hazard Grade Values exist for the criteria entered." 
                                         OnRowUpdating="GridView1_RowUpdating" OnRowCommand="GridView1_RowCommand" OnInit="GridView1_Init" ShowFooter="false">
                                        </UserControls:Grid>
                                        <br /><br />
                                        <asp:Button id="btnAdd" onclick="btnAdd_Click" runat="server" Text="Add" Enabled="<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() %>"></asp:Button>
                                        <br />
    <asp:PlaceHolder ID="ccHgvMessagePlaceHolder" runat="server">
        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ></asp:ValidationSummary>
        <br />
    </asp:PlaceHolder>
    
                                        <asp:ObjectDataSource id="ObjectDataSource1" runat="server" DeleteMethod="DeleteClassCodeHV" TypeName="BCS.Core.Clearance.Admin.DataAccess" SelectMethod="GetClassCodesHV" OnDeleted="ObjectDataSource1_Deleted" OnDeleting="ObjectDataSource1_Deleting">
                                            <SelectParameters>
                                                <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected" Type="Int32" />
                                                <asp:ControlParameter ControlID="TextBox1" Name="filterCodeValue" PropertyName="Text" />
                                            </SelectParameters>
                                            <DeleteParameters>
                                                <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected" Type="Int32" />
                                                <asp:Parameter Name="classCodeId" Type="Int32" />
                                            </DeleteParameters>
                                        </asp:ObjectDataSource>
                                    </asp:Panel>
                                    </contenttemplate>
                                </aje:UpdatePanel>
                            </ContentTemplate>
                        </ajx:TabPanel>
                    </ajx:TabContainer>
                </td>
                <td class="MR">
                    <div />
                </td>
            </tr>
            <tr>
                <td class="BL">
                    <div />
                </td>
                <td class="BC">
                    <div />
                </td>
                <td class="BR">
                    <div />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
