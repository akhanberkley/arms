<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="UWU.aspx.cs" Inherits="UWU" Title="Berkley Clearance System - Underwriting Units" Theme="DefaultTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <aje:ScriptManager id="ScriptManager1" runat="server"></aje:ScriptManager>
<table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Underwriting Units</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        <%--content--%>        
        
    <UserControls:Grid ID="Grid1" runat="server" DataSourceID="ObjectDataSource1" EmptyDataText="No Underwriting units were found." AllowPaging="True"        
     AutoGenerateColumns="False" DataKeyNames="id" OnPageIndexChanged="Grid1_PageIndexChanged" OnRowDeleted="Grid1_RowDeleted"
     OnSelectedIndexChanging="Grid1_SelectedIndexChanging">
    <Columns>
        <asp:CommandField ButtonType="Button" ShowSelectButton="True" SelectText='<%$ Code: BCS.WebApp.Components.ConfigValues.GetEditButtonDixplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId),false) %>' ControlStyle-Width="50px" />
        <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
        <asp:BoundField DataField="Line" HeaderText="Line" />
        <asp:BoundField DataField="Code" HeaderText="Code" />
        <asp:BoundField DataField="Description" HeaderText="Description" />
        <asp:TemplateField ShowHeader="False">
            <ItemTemplate>
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                     Text="Delete" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly()  && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'/>
                <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="btnDelete"
                    ConfirmText='<%# DataBinder.Eval(Container.DataItem, "Code", "Are you sure you want to delete this line of business \"{0}\"?")%> '>
                </ajx:ConfirmButtonExtender>
            </ItemTemplate>
         </asp:TemplateField>
    </Columns>
    </UserControls:Grid>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetLineOfBusinesses"
        TypeName="BCS.Core.Clearance.Admin.DataAccess" DeleteMethod="DeleteCompanyNumberLOB" OnDeleted="ObjectDataSource1_Deleted">
        <SelectParameters>
            <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected"
                Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
    <br />
    <asp:Button ID="btnAdd" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' runat="server" Text="Add"  OnClick="btnAdd_Click" />
    <br />
    <br />
    <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
    <br />
    </asp:PlaceHolder>
    <asp:DetailsView ID="DetailsView1" runat="server"
        AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="ObjectDataSource2" Height="50px"
        OnItemInserted="DetailsView1_ItemInserted" OnItemUpdated="DetailsView1_ItemUpdated" OnItemUpdating="DetailsView1_ItemUpdating" OnModeChanging="DetailsView1_ModeChanging">
        <Fields>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
            <asp:TemplateField HeaderText="Line">
                <EditItemTemplate>
                    <asp:TextBox ID="txtLine" runat="server" Text='<%# Bind("Line") %>' MaxLength="2"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvLine" runat="server" ControlToValidate="txtLine"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*" ></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtLine" runat="server" Text='<%# Bind("Line") %>' MaxLength="2"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvLine" runat="server" ControlToValidate="txtLine"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblLine" runat="server" Text='<%# Bind("Line") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Code">
                <EditItemTemplate>
                    <asp:TextBox ID="txtCode" runat="server" Text='<%# Bind("Code") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtCode"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*" ></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtCode" runat="server" Text='<%# Bind("Code") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtCode"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCode" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description">
                <EditItemTemplate>
                    <asp:TextBox ID="txtDesc" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ControlToValidate="txtDesc"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*" ></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtDesc" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ControlToValidate="txtDesc"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:CommandField ButtonType="Button" ShowEditButton='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' ShowInsertButton='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetLineOfBusiness"
        TypeName="BCS.Core.Clearance.Admin.DataAccess" InsertMethod="AddCompanyNumberLOB" UpdateMethod="UpdateCompanyNumberLOB" OnInserted="ObjectDataSource2_Inserted" OnUpdated="ObjectDataSource2_Updated">
        <SelectParameters>
            <asp:ControlParameter ControlID="Grid1" Name="id" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="id" Type="Int32" />
            <asp:Parameter Name="code" Type="String" />
            <asp:Parameter Name="description" Type="String" />
            <asp:Parameter Name="line" Type="String" />
        </UpdateParameters>
        <InsertParameters>
            <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected" Type="Int32" />
            <asp:Parameter Name="code" Type="String" />
            <asp:Parameter Name="description" Type="String" />
            <asp:Parameter Name="line" Type="String" />
        </InsertParameters>
        
    </asp:ObjectDataSource>
    
    <%--end content--%>
        </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
</asp:Content>

