#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.Components;
#endregion

public partial class Agents : System.Web.UI.Page
{
    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        if (Session["CompanyChanged"] != null)
        {
            Session.Contents.Remove("CompanyChanged");
            gridAgents.SelectedIndex = -1;
            DetailsView1.DataBind();
        }
        base.OnLoadComplete(e);
    } 
    #endregion

    #region Control Events
    protected void Button1_Click(object sender, EventArgs e)
    {
        gridAgents.SelectedIndex = -1;
    }
    protected void gridAgents_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }
    protected void gridAgents_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gridAgents_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        if (!BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.Agency) && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
            DetailsView1.ChangeMode(DetailsViewMode.Edit);
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        gridAgents.DataBind();
        DetailsView1.DataBind();
    }
    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        gridAgents.DataBind();
        DetailsView1.DataBind();
    }
    protected void DetailsView1_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
    {
        gridAgents.DataBind();
        DetailsView1.DataBind();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Master.GetCompanySelected() == 0)
        {
            Common.SetStatus(messagePlaceHolder, "Please select a company before adding an agency.");
            return;
        }
        DetailsView1.ChangeMode(DetailsViewMode.Insert);
    }

    protected void odsAgent_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        BCS.Core.Clearance.BizObjects.Agent agent = e.InputParameters[0] as BCS.Core.Clearance.BizObjects.Agent;
        agent.ModifiedBy = Common.GetUser();
        // TODO: does not handle phone number extensions
        if (agent.PrimaryPhone != null && agent.PrimaryPhone.Length > 0)
        {
            agent.PrimaryPhone = Common.DeFormatPhone(agent.PrimaryPhone);
            string ppac = agent.PrimaryPhone.Substring(0, 3);
            string pp = agent.PrimaryPhone.Substring(3);
            agent.PrimaryPhone = pp;
            agent.PrimaryPhoneAreaCode = ppac;
        }
        if (agent.SecondaryPhone != null && agent.SecondaryPhone.Length > 0)
        {
            agent.SecondaryPhone = Common.DeFormatPhone(agent.SecondaryPhone);
            string spac = agent.SecondaryPhone.Substring(0, 3);
            string sp = agent.SecondaryPhone.Substring(3);
            agent.SecondaryPhone = sp;
            agent.SecondaryPhoneAreaCode = spac;
        }
        if (agent.Fax != null && agent.Fax.Length > 0)
        {
            agent.Fax = Common.DeFormatPhone(agent.Fax);
            string spac = agent.Fax.Substring(0, 3);
            string sp = agent.Fax.Substring(3);
            agent.Fax = sp;
            agent.FaxAreaCode = spac;
        }
    }
    protected void odsAgent_Updating(object sender, ObjectDataSourceMethodEventArgs e)
    {
        BCS.Core.Clearance.BizObjects.Agent agent = e.InputParameters[0] as BCS.Core.Clearance.BizObjects.Agent;
        agent.ModifiedBy = Common.GetUser();
        // TODO: does not handle phone number extensions
        if (agent.PrimaryPhone != null && agent.PrimaryPhone.Length > 0)
        {
            agent.PrimaryPhone = Common.DeFormatPhone(agent.PrimaryPhone);
            string ppac = agent.PrimaryPhone.Substring(0, 3);
            string pp = agent.PrimaryPhone.Substring(3);
            agent.PrimaryPhone = pp;
            agent.PrimaryPhoneAreaCode = ppac;
        }
        if (agent.SecondaryPhone != null && agent.SecondaryPhone.Length > 0)
        {
            agent.SecondaryPhone = Common.DeFormatPhone(agent.SecondaryPhone);
            string spac = agent.SecondaryPhone.Substring(0, 3);
            string sp = agent.SecondaryPhone.Substring(3);
            agent.SecondaryPhone = sp;
            agent.SecondaryPhoneAreaCode = spac;
        }
        if (agent.Fax != null && agent.Fax.Length > 0)
        {
            agent.Fax = Common.DeFormatPhone(agent.Fax);
            string spac = agent.Fax.Substring(0, 3);
            string sp = agent.Fax.Substring(3);
            agent.Fax = sp;
            agent.FaxAreaCode = spac;
        }
    }

    protected void gridAgents_PageIndexChanged(object sender, EventArgs e)
    {
        gridAgents.SelectedIndex = -1;
    }
    protected void odsAgent_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }

    }
    protected void odsAgent_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void odsAgent_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        bool b = Common.ShouldUpdate(e);

        e.Cancel = b;

        if (b)
        {
            // there was nothing to update. Just show the user the success message.
            Common.SetStatus(messagePlaceHolder, "Agent was Sucessfully updated.");
            DetailsView1.DataBind();
        }

    }
    protected void DatePickerTextBox_Init(object sender, EventArgs e)
    {
        TextBox tb = sender as TextBox;
        tb.Attributes.Add("onclick", "scwShow(this,this);");
        tb.Attributes.Add("onkeydown", "hideCalOnTab(event);");
        tb.Attributes.Add("onblur", "this.value = purgeDate(this);");
    }
    protected void DatePicker_InitToday(object sender, EventArgs e)
    {
        DatePickerTextBox_Init(sender, e);
        (sender as TextBox).Text = DateTime.Today.ToString("MM/dd/yyyy");
    }
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        e.Cancel = BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.Agency) || ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
    }
    protected void PhoneFormatControl_DataBinding(object sender, EventArgs e)
    {
        ITextControl tx = sender as ITextControl;
        tx.Text = Common.FormatPhone(tx.Text);
    }
    protected void gridAgents_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        gridAgents.DataBind();
        DetailsView1.DataBind();
    }
    protected void odsAgents_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }

    #endregion
    protected void odsAgents_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        //BCS.Biz.AgentCollection ac = e.ReturnValue as BCS.Biz.AgentCollection;
        //foreach (BCS.Biz.Agent var in ac)        
        //{
        //    if (var.PrimaryPhone != null)
        //    {
        //        var.PrimaryPhone = string.Format(
        //            "({0}){1}-{2}",
        //            var.PrimaryPhoneAreaCode,
        //            var.PrimaryPhone.Substring(0, 3),
        //            var.PrimaryPhone.Substring(3));
        //    }
        //}

        DataTable dt = e.ReturnValue as DataTable;

        if (dt != null)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["PrimaryPhone"] != null && dr["PrimaryPhone"].ToString() != string.Empty)
                {
                    try
                    {
                        dr["PrimaryPhone"] = string.Format("({0}){1}-{2}", dr["PrimaryPhoneAreaCode"],
                                               dr["PrimaryPhone"].ToString().Substring(0, 3),
                                               dr["PrimaryPhone"].ToString().Substring(3));
                    }
                    catch (Exception ex)
                    {
                        //throw;
                    }
                }
            }
        }
    }
    protected void rvDOB_Init(object sender, EventArgs e)
    {
        RangeValidator rv = sender as RangeValidator;
        Common.SetDateOfBirthRange(rv);
    }
}
