<%@ Page Language="C#" Theme="DefaultTheme" MasterPageFile="~/Site.PATS.master" Title="Berkley Clearance System : Agencies" AutoEventWireup="true" CodeFile="Agencies.aspx.cs" Inherits="Agencies"  %>
<%@ MasterType VirtualPath="~/Site.PATS.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<ajx:ToolkitScriptManager id="ScriptManager1" runat="server" OnAsyncPostBackError="ScriptManager1_AsyncPostBackError"></ajx:ToolkitScriptManager>
<script type="text/javascript">        
    function requireAnother(elem1, elem2, validator1, validator2)
    {
        if ($get(elem1).value == '' && $get(elem2).value == '')
        {
            ValidatorEnable(Page_Validators[Array.indexOf(Page_Validators, $get(validator1))], false);
            ValidatorEnable(Page_Validators[Array.indexOf(Page_Validators, $get(validator2))], false);
        }
        else
        {
            ValidatorEnable(Page_Validators[Array.indexOf(Page_Validators, $get(validator1))], true);
            ValidatorEnable(Page_Validators[Array.indexOf(Page_Validators, $get(validator2))], true);
        }
    }
    
</script>
    <table cellpadding="0" cellspacing="0" class="box" width="70%">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Agencies</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        
        <table>
            <tr>
                <td>Agency Number</td>
                <td>
                    <asp:TextBox ID="txFilterAgencyNumber" runat="server"></asp:TextBox>
                </td>
                <td>Agency Name</td>
                <td>
                    <asp:TextBox ID="txFilterAgencyName" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Filter"  OnClick="Button1_Click" CausesValidation="false" />
                </td>
                <td>
                    <asp:Button ID="btnSyncAgency" runat="server" Text="Sync by Number"  OnClick="btnSyncAgency_Click" CausesValidation="false" />
                </td>
                <td>
                    <asp:Label ID="lblSyncResult" runat="server" />
                </td>
            </tr>
        </table>

    &nbsp;    
    <br />
    <br />
    <UserControls:Grid ID="gridAgencies" runat="server" AllowPaging="True" EmptyDataText="No Agencies were found." AutoGenerateColumns="False" DataKeyNames="Id" CaptionAlign="Left" DataSourceID="odsAgencies" OnSelectedIndexChanging="gridAgencies_SelectedIndexChanging" OnPageIndexChanged="gridAgencies_PageIndexChanged" OnRowDeleted="gridAgencies_RowDeleted">
        <Columns>
            <asp:CommandField ButtonType="Button" ShowSelectButton="True" SelectText='<%$ Code: BCS.WebApp.Components.ConfigValues.GetEditButtonDixplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId),true) %>'  ControlStyle-Width="50px" />
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
            <asp:BoundField DataField="CompanyNumberId" HeaderText="Company Number" Visible="False"/>
             <asp:TemplateField HeaderText="Company Number">                
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CompanyNumber.PropertyCompanyNumber") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="AgencyName" HeaderText="Agency Name" />
            <asp:BoundField DataField="AgencyNumber" HeaderText="Agency Number" />
            <asp:BoundField DataField="ContactPerson" HeaderText="Contact Person" />
            <asp:BoundField DataField="FEIN" HeaderText="FEIN" />
            <asp:BoundField DataField="City" HeaderText="City" />
            <asp:BoundField DataField="State" HeaderText="State" />
            <asp:BoundField DataField="Zip" HeaderText="Zip" />
            <asp:TemplateField HeaderText="Cancel Date">
                <ItemTemplate>
                    <asp:Label ID="Label288" runat="server" Text='<%# Bind("CancelDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Referral Date">
                <ItemTemplate>
                    <asp:Label ID="Label188" runat="server" Text='<%# Bind("ReferralDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Comments" HeaderText="Comments" />
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                         Text="Delete"/>
                    <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="btnDelete"
                        ConfirmText='<%# DataBinder.Eval (Container.DataItem, "AgencyNumber", "Are you sure you want to delete this Agency \"{0}\"?") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </UserControls:Grid>
    <asp:ObjectDataSource ID="odsAgencies" runat="server" SelectMethod="GetAgencies"
        TypeName="BCS.Core.Clearance.Admin.DataAccess" DeleteMethod="DeleteAgency" OnDeleted="odsAgencies_Deleted">
        <SelectParameters>
            <asp:ControlParameter ControlID="txFilterAgencyNumber" DefaultValue="" Name="filterAgencyNumber" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txFilterAgencyName" DefaultValue="" Name="filterAgencyName" PropertyName="Text"
            Type="String" />
            <asp:SessionParameter DefaultValue="0" Name="companyId" SessionField="CompanySelected"
                Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>   
    <br />
    <asp:Button ID="btnAdd" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.Agency) && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' runat="server" Text="Add"  OnClick="btnAdd_Click" AccessKey="A" />
    <br />
    <br />
   
    <asp:DetailsView ID="DetailsView1" runat="server" DataSourceID="odsAgency"
        Height="50px" CaptionAlign="Left" AutoGenerateRows="False" OnItemInserted="DetailsView1_ItemInserted" OnItemUpdated="DetailsView1_ItemUpdated" OnItemUpdating="DetailsView1_ItemUpdating" DataKeyNames="Id" OnModeChanging="DetailsView1_ModeChanging" OnItemInserting="DetailsView1_ItemInserting" OnDataBound="DetailsView1_DataBound" >
        <CommandRowStyle BorderColor="White" />
        <Fields>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id"
                Visible="False" />
            <asp:TemplateField HeaderText="Company Number">
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlCompanyNumbers" runat="server" 
                    Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'
                    DataSourceID="odsCompanyNumbers" DataTextField="PropertyCompanyNumber" DataValueField="Id" SelectedValue='<%# Bind("CompanyNumberId") %>' OnPreRender="Sort_PreRender">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="odsCompanyNumbers" runat="server" SelectMethod="GetCompanyNumbers"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                            <asp:Parameter DefaultValue="True" Type="boolean" Name="companyDependent" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlCompanyNumbers" runat="server" 
                    Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'
                    DataSourceID="odsCompanyNumbers" DataTextField="PropertyCompanyNumber" DataValueField="Id" SelectedValue='<%# Bind("CompanyNumberId") %>' OnSelectedIndexChanged="Sort_PreRender">
                    </asp:DropDownList><asp:ObjectDataSource ID="odsCompanyNumbers" runat="server" SelectMethod="GetCompanyNumbers"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                            <asp:Parameter DefaultValue="True" Type="boolean" Name="companyDependent" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CompanyNumber.PropertyCompanyNumber") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Master Agency">
                <EditItemTemplate>
                    <UserControls:AgencyDropDownList ID="AgencyDropDownList1" runat="server" 
                    Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'
                    CompanyNumberId='<%$ Code: Common.GetSafeIntFromSession("CompanyNumberSelected") %>'
                        SelectedValue='<%# Bind("MasterAgencyId") %>' ItemAsEmpty="True" AgencyType="MastersOnly"></UserControls:AgencyDropDownList>                    
                </EditItemTemplate>
                <InsertItemTemplate>
                    <UserControls:AgencyDropDownList ID="AgencyDropDownList1" runat="server" 
                    Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'
                    CompanyNumberId='<%$ Code: Common.GetSafeIntFromSession("CompanyNumberSelected") %>'
                        SelectedValue='<%# Bind("MasterAgencyId") %>' ItemAsEmpty="True" AgencyType="MastersOnly"></UserControls:AgencyDropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblMasterAgency" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ParentRelationAgency.AgencyName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Height="101%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Master">
                <EditItemTemplate>
                    <asp:CheckBox ID="cbActive" runat="server" Checked='<%# Bind("IsMaster") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:CheckBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:CheckBox ID="cbActive" runat="server" Checked='<%# Bind("IsMaster") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:CheckBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="cbActive" runat="server" Checked='<%# Bind("IsMaster") %>' Enabled="false"></asp:CheckBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="FEIN/Tax ID#">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FEIN") %>' MaxLength="11" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox1"
                        ErrorMessage="Invalid FEIN format." Text="* ######### or FEIN Format ##-####### or Social Security Number format ###-##-####" ValidationExpression="\d{9}|[a-zA-Z\d]{2}-[a-zA-Z\d]{7}|\d{3}-\d{2}-\d{4}" Display="Dynamic"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FEIN") %>' MaxLength="11" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox1" ValidationExpression="\d{9}|[a-zA-Z\d]{2}-[a-zA-Z\d]{7}|\d{3}-\d{2}-\d{4}"
                        ErrorMessage="Invalid FEIN format." Text="* ######### or FEIN Format ##-####### or Social Security Number format ###-##-####" Display="Dynamic"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("FEIN") %>'></asp:Label>
                </ItemTemplate>                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="State Tax Id">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox14" runat="server" MaxLength="11" Text='<%# Bind("StateTaxId") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox14" runat="server" MaxLength="11" Text='<%# Bind("StateTaxId") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label18" runat="server" Text='<%# Bind("StateTaxId") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Agency Number">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("AgencyNumber") %>' MaxLength="15" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox3"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Agency Number is required." Text="*"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("AgencyNumber") %>' MaxLength="15" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox3"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Agency Number is required." Text="*"></asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("AgencyNumber") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>            
            <asp:TemplateField HeaderText="Agency Name">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("AgencyName") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBox2"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Agency Name is required." Text="*"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("AgencyName") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBox2"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Agency Name is required." Text="*"></asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("AgencyName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Referral Agency Number">
                <EditItemTemplate>
                    <UserControls:AgencyNumberDropdownList CompanyNumberId='<%$ Code: Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'  Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'
                     ID="AgencyNumberDropdownList1" runat="server" SelectedValue='<%# Bind("ReferralAgencyNumber") %>' CancelDate='<%$ Code: DateTime.Now %>'
                     OnInit="AgencyNumberDropdownList1_Init" AutoPostBack="false" OnSelectedIndexChanged="AgencyNumberDropdownList1_SelectedIndexChanged" >
                    </UserControls:AgencyNumberDropdownList>
                    <asp:RequiredFieldValidator ID="rfvReferralAgency" runat="server" ControlToValidate="AgencyNumberDropdownList1"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Referral Agency Number is required." Text="*" ></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <UserControls:AgencyNumberDropdownList Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' CompanyNumberId='<%$ Code: Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'
                     ID="AgencyNumberDropdownList1" runat="server" SelectedValue='<%# Bind("ReferralAgencyNumber") %>' CancelDate='<%$ Code: DateTime.Now %>'
                     OnInit="AgencyNumberDropdownList1_Init" AutoPostBack="false" OnSelectedIndexChanged="AgencyNumberDropdownList1_SelectedIndexChanged" >
                    </UserControls:AgencyNumberDropdownList>
                    <asp:RequiredFieldValidator ID="rfvReferralAgency" runat="server" ControlToValidate="AgencyNumberDropdownList1"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Referral Agency Number is required." Text="*" ></asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label77" runat="server" Text='<%# Bind("ReferralAgencyNumber") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Referral Date" SortExpression="ReferralDate">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox88" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' runat="server" OnInit="DatePicker_Init1" Text='<%# Bind("ReferralDate", "{0:MM/dd/yyyy}") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvReferralDate" runat="server" ControlToValidate="TextBox88"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Referral Date is required." Text="*" ></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator88" runat="server" ControlToValidate="TextBox88" Display="Dynamic"
                        SetFocusOnError="True" ErrorMessage="MM/dd/yyyy" Text="Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>                    
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox88" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' runat="server" OnInit="DatePicker_Init1" Text='<%# Bind("ReferralDate", "{0:MM/dd/yyyy}") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvReferralDate" runat="server" ControlToValidate="TextBox88"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Referral Date is required." Text="*" ></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator88" runat="server" ControlToValidate="TextBox88" Display="Dynamic"
                        SetFocusOnError="True" ErrorMessage="MM/dd/yyyy" Text="Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label88" runat="server" Text='<%# Bind("ReferralDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Address" HeaderText="Address" ReadOnly='<%$ Code: ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' />
            <asp:BoundField DataField="Address2" HeaderText="Address Line 2" ReadOnly='<%$ Code: ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' />
            <asp:BoundField DataField="City" HeaderText="City" ReadOnly='<%$ Code: ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' />
            <asp:TemplateField HeaderText="State">                
                <EditItemTemplate>
                    <UserControls:StateDropDownList ID="ddlStates" runat="server" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'
                    DataTextField="Abbreviation" DataValueField="Abbreviation" SelectedValue='<%# Bind("State") %>'>
                    </UserControls:StateDropDownList>                   
                </EditItemTemplate>
                <InsertItemTemplate>
                    <UserControls:StateDropDownList ID="ddlStates" runat="server" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'
                    DataTextField="Abbreviation" DataValueField="Abbreviation" SelectedValue='<%# Bind("State") %>'>
                    </UserControls:StateDropDownList>                    
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("State") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="State Of Incorporation">
                <EditItemTemplate>
                    <UserControls:StateDropDownList ID="ddlStatesOI" runat="server" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'
                    DataTextField="Abbreviation" DataValueField="Abbreviation" SelectedValue='<%# Bind("StateOfIncorporation") %>'>
                    </UserControls:StateDropDownList>                   
                </EditItemTemplate>
                <InsertItemTemplate>
                    <UserControls:StateDropDownList ID="ddlStatesOI" runat="server" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'
                    DataTextField="Abbreviation" DataValueField="Abbreviation" SelectedValue='<%# Bind("StateOfIncorporation") %>'>
                    </UserControls:StateDropDownList>                    
                </InsertItemTemplate>                
                <ItemTemplate>
                    <asp:Label ID="Label12" runat="server" Text='<%# Bind("StateOfIncorporation") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Zip">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Zip") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBox6"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Zip." Text="* Wrong format.." ValidationExpression="\d{5}(-\d{4})|\d{5}(\d{4})?"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Zip") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBox6"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Zip." Text="* Wrong format.." ValidationExpression="\d{5}(-\d{4})|\d{5}(\d{4})?"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("Zip") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Phone">
                <EditItemTemplate>                    
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Phone") %>' OnDataBinding="PhoneFormatControl_DataBinding" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox4"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Phone." Text="* Wrong format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Phone") %>' OnDataBinding="PhoneFormatControl_DataBinding" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox4"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Phone." Text="* Wrong format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Phone") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fax">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Fax") %>' OnDataBinding="PhoneFormatControl_DataBinding" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server" ControlToValidate="TextBox5"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Fax." Text="* Wrong format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Fax") %>' OnDataBinding="PhoneFormatControl_DataBinding" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server" ControlToValidate="TextBox5"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Fax." Text="* Wrong format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Bind("Fax") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ImageFax" HeaderText="Image Fax" ReadOnly='<%$ Code: ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' />
            <asp:TemplateField HeaderText="Email">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("Email") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="TextBox7"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Email." Text="* Wrong format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("Email") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="TextBox7"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Email." Text="* Wrong format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label10" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ContactPerson" HeaderText="Contact Person" ReadOnly='<%$ Code: ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' />
            <asp:TemplateField HeaderText="Contact Person Phone">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("ContactPersonPhone") %>' OnDataBinding="PhoneFormatControl_DataBinding" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="TextBox9"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Phone." Text="* Wrong format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("ContactPersonPhone") %>' OnDataBinding="PhoneFormatControl_DataBinding" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="TextBox9"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Phone." Text="* Wrong format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label13" runat="server" Text='<%# Bind("ContactPersonPhone") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Contact Person Extension">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox12" runat="server" Text='<%# Bind("ContactPersonExtension") %>'  Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                        SetFocusOnError="True" ControlToValidate="TextBox12" Display="Dynamic" ErrorMessage="Invalid Contact Person Extension." Text="* Wrong format (1 - 4 digits)"
                        ValidationExpression="\d{1,4}"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox12" runat="server" Text='<%# Bind("ContactPersonExtension") %>'  Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                        SetFocusOnError="True" ControlToValidate="TextBox12" Display="Dynamic" ErrorMessage="Invalid Contact Person Extension." Text="* Wrong format (1 - 4 digits)"
                        ValidationExpression="\d{1,4}"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label16" runat="server" Text='<%# Bind("ContactPersonExtension") %>'  Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Contact Person Fax">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("ContactPersonFax") %>' OnDataBinding="PhoneFormatControl_DataBinding" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="TextBox10"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Contact Person Fax." Text="* Wrong format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("ContactPersonFax") %>' OnDataBinding="PhoneFormatControl_DataBinding" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="TextBox10"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Contact Person Fax." Text="* Wrong format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label14" runat="server" Text='<%# Bind("ContactPersonFax") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Contact Person Email">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("ContactPersonEmail") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="TextBox11"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Contact Person Email." Text="* Wrong format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("ContactPersonEmail") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="TextBox11"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Contact Person Email." Text="* Wrong format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label15" runat="server" Text='<%# Bind("ContactPersonEmail") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="BranchId" HeaderText="Branch Id"  ReadOnly='<%$ Code: ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'/>
            <asp:TemplateField HeaderText="Effective Date" SortExpression="EffectiveDate">
                <EditItemTemplate>
                    <asp:TextBox ID="txEffectiveDate" runat="server" OnInit="DatePicker_InitToday" Text='<%# Bind("EffectiveDate", "{0:MM/dd/yyyy}") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEffectiveDate" runat="server" ControlToValidate="txEffectiveDate"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Effective Date is required." Text="*" ></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvEffectiveDate" runat="server" ControlToValidate="txEffectiveDate" Display="Dynamic"
                         ErrorMessage="Invalid Effective Date." Text="MM/dd/yyyy" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txEffectiveDate" runat="server" OnInit="DatePicker_InitToday" Text='<%# Bind("EffectiveDate", "{0:MM/dd/yyyy}") %>' Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEffectiveDate" runat="server" ControlToValidate="txEffectiveDate"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Effective Date is required." Text="*" ></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvEffectiveDate" runat="server" ControlToValidate="txEffectiveDate"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Effective Date." Text="MM/dd/yyyy" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbEffectiveDate" runat="server" Text='<%# Bind("EffectiveDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cancel Date" SortExpression="CancelDate">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox8" runat="server" OnInit="DatePicker_Init" Text='<%# Bind("CancelDate", "{0:MM/dd/yyyy}") %>'  Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'></asp:TextBox>
                    <asp:CompareValidator
                        ID="CompareValidator1" runat="server" ControlToValidate="TextBox8" Display="Dynamic"
                        SetFocusOnError="True" ErrorMessage="Invalid Cancel Date." Text="MM/dd/yyyy" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox8" runat="server" OnInit="DatePicker_Init" Text='<%# Bind("CancelDate", "{0:MM/dd/yyyy}") %>'></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBox8"
                        SetFocusOnError="True" Display="Dynamic" ErrorMessage="Invalid Cancel Date." Text="MM/dd/yyyy" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label11" runat="server" Text='<%# Bind("CancelDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Comments">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox13" TextMode="MultiLine" runat="server" MaxLength="250" Text='<%# Bind("Comments") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox13" TextMode="MultiLine" runat="server" MaxLength="250" Text='<%# Bind("Comments") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label17" runat="server" Text='<%# Bind("Comments") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Licensed States" SortExpression="State">
                <EditItemTemplate>
                    <UserControls:StateListBox ID="StateListBox1" runat="server" Rows="10" Width="100%"  Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' >
                    </UserControls:StateListBox>
                </EditItemTemplate>
                <InsertItemTemplate>                    
                    <UserControls:StateListBox ID="StateListBox1" runat="server" Rows="10" Width="100%" >
                     </UserControls:StateListBox>
                </InsertItemTemplate>
                <ItemTemplate>                    
                    <UserControls:StateListBox ID="StateListBox1" runat="server" Rows="10" Width="100%" >
                     </UserControls:StateListBox>
                </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:Button ID="Button1" runat="server" CausesValidation="True" CommandName="Update" Enabled='<%$ Code: !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'
                                OnInit="ValidationButton_Init" Text="Update" AccessKey="S" />&nbsp;<asp:Button ID="Button2" runat="server"
                                    CausesValidation="False" CommandName="Cancel" Text="Cancel" AccessKey="C" />
                        </EditItemTemplate>
                        <InsertItemTemplate>
                            <asp:Button ID="Button1" runat="server" CausesValidation="True" CommandName="Insert"
                                OnInit="ValidationButton_Init" Text="Insert" AccessKey="S" />&nbsp;<asp:Button ID="Button2" runat="server"
                                    CausesValidation="False" CommandName="Cancel" Text="Cancel" AccessKey="C" />
                        </InsertItemTemplate>
                        <ItemTemplate>
                            <asp:Button ID="Button1" runat="server" CausesValidation="False" CommandName="Edit"
                                Text="Edit" AccessKey="S" />&nbsp;<asp:Button ID="Button2" runat="server"
                                    CausesValidation="False" CommandName="New" Text="New" AccessKey="N" />
                        </ItemTemplate>
                </asp:TemplateField>
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="odsAgency" runat="server" SelectMethod="GetAgency" TypeName="BCS.Core.Clearance.Admin.DataAccess" InsertMethod="AddAgency" DataObjectTypeName="BCS.Core.Clearance.BizObjects.Agency" OldValuesParameterFormatString="original_{0}" OnInserted="odsAgency_Inserted" OnInserting="odsAgency_Inserting" UpdateMethod="UpdateAgency" OnUpdated="odsAgency_Updated" OnUpdating="odsAgency_Updating" >
        <SelectParameters>
            <asp:ControlParameter ControlID="gridAgencies" Name="id" PropertyName="SelectedValue"
                Type="Int32"  />
        </SelectParameters>
    </asp:ObjectDataSource>
    
    </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>

<table id="ValidationTable" runat="server" cellpadding="0" cellspacing="0" class="box" width="30%" style="display: none;">
    
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Validation</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC" style="width: 100%">
            <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" SkinID="NewOneToBeDefaulted"></asp:ValidationSummary>
            </asp:PlaceHolder>
        </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
   
</table>
<ajx:AlwaysVisibleControlExtender ID="avceVerify" runat="server"
    BehaviorID="alwaysVisible"
    TargetControlID="ValidationTable"
    VerticalOffset="100"
    HorizontalSide="Right">
</ajx:AlwaysVisibleControlExtender>
</asp:Content>

