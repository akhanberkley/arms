using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.Components;
using structures = BTS.ClientCore.Wrapper.Structures;
using System.Text;
using System.Collections.Generic;

public partial class ClientSwitches : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ScriptManager1.SetFocus(txSubmissionNumbers);
        }
    }
    protected void MakeSwitch(object sender, CommandEventArgs e)
    {
        if (!Page.IsValid)
            return;
        SubmissionProxy.Submission service = Common.GetSubmissionProxy();
        Button btn = sender as Button;
        switch (btn.ValidationGroup)
        {
            case "S":
                {
                    string s = service.SwitchClient(Common.GetSafeStringFromSession(SessionKeys.CompanyNumber),
                        txSubmissionNumbers.Text, txSubmissionNumbersNewClientIds.Text, "SubmissionNumber");

                    BCS.Core.Clearance.BizObjects.Response response =
                        (BCS.Core.Clearance.BizObjects.Response)BCS.Core.XML.Deserializer.Deserialize(s, typeof(BCS.Core.Clearance.BizObjects.Response));

                    ShowMessage(response.Message);

                    ViewState["ClientID"] = txSubmissionNumbersNewClientIds.Text.Substring(1, txSubmissionNumbersNewClientIds.Text.IndexOf(':')-1);

                    txSubmissionNumbers.Text = string.Empty;
                    RemoveClient(RemoveImageSubmissionNumber, new CommandEventArgs("SubmissionNumber",null));
                    //SubmissionNumberSubmissionCount.InnerHtml = string.Empty;
                    //SubmissionNumberInfoPanel.InnerHtml = "0";
                    break;
                }
            case "P":
                {
                    string s = service.SwitchClient(Common.GetSafeStringFromSession(SessionKeys.CompanyNumber),
                        txPolicyNumbers.Text, txPolicyNumbersNewClientIds.Text, "PolicyNumber");

                    BCS.Core.Clearance.BizObjects.Response response =
                        (BCS.Core.Clearance.BizObjects.Response)BCS.Core.XML.Deserializer.Deserialize(s, typeof(BCS.Core.Clearance.BizObjects.Response));

                    ShowMessage(response.Message);

                    ViewState["ClientID"] = txPolicyNumbersNewClientIds.Text.Substring(1, txPolicyNumbersNewClientIds.Text.IndexOf(':') - 1);
                    
                    txPolicyNumbers.Text = string.Empty;
                    RemoveClient(RemoveImagePolicyNumber, new CommandEventArgs("PolicyNumber", null));
                    //PolicyNumberInfoPanel.InnerHtml = string.Empty;
                    //PolicyNumberSubmissionCount.InnerHtml = string.Empty;
                    break;
                }
            default:
                break;
        }
        lnkClientIDSearch.Text = string.Format("Get Submissions for ClientID {0}", (string)ViewState["ClientID"]);
        lnkClientIDSearch.Visible = true;
    }

    protected void lnkClientIDSearch_OnClick(object sender, EventArgs e)
    {
        SavedSearch saveSearch = new SavedSearch();
        saveSearch.SelectedClientId = (string)ViewState["ClientID"];
        saveSearch.SearchedClient = new ClientSearch();
        saveSearch.SearchedClient.Info = new BTS.ClientCore.Wrapper.Structures.Search.ClientInfo();
        saveSearch.SearchedClient.Info.ClientId = (string)ViewState["ClientID"];
        saveSearch.SearchedClient.Names = new ClientNameSearch[0];
        saveSearch.SearchedClient.Addresses = new BTS.ClientCore.Wrapper.Structures.Search.AddressVersion[0];

        Session["LoadSavedSearch"] = true;
        Session["SavedSearch"] = saveSearch;

        Response.Redirect("default.aspx");
    }

    protected void loadTargetClientButton_Click(object sender, EventArgs e)
    {   
        string id = targetClientIdTextBox.Text;
        string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
        ClientProxy.Client clientproxy = Common.GetClientProxy();
        string clientxml = clientproxy.GetClientById(companyNumber, id);

        BCS.Core.Clearance.BizObjects.ClearanceClient clearanceclient =
            (BCS.Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
            clientxml, typeof(BCS.Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

        structures.Info.ClientInfo client = clearanceclient.ClientCoreClient;

        lbDbaNames.Items.Clear();
        lbInsuredNames.Items.Clear();

        // load names for merge feature
        cblNames1.Items.Clear();
        if (client.Client.Names != null)
        {
            foreach (structures.Info.Name name in client.Client.Names)
            {
                StringBuilder sb = new StringBuilder();
                if (name.NameBusiness != null && name.NameBusiness.Length > 0)
                    sb.AppendFormat("{0} ({1})", name.NameBusiness, string.IsNullOrEmpty(name.ClearanceNameType) ? "NA" : name.ClearanceNameType);
                else
                    sb.AppendFormat("{0} {1} {2}", name.NameFirst, name.NameMiddle, name.NameLast);

                if (name.ClearanceNameType == "DBA")
                    lbDbaNames.Items.Add(new ListItem(name.NameBusiness, name.NameBusiness));
                else // populate both insured and unassigned on insured list
                    lbInsuredNames.Items.Add(new ListItem(name.NameBusiness, name.NameBusiness));

                cblNames1.Items.Add(new ListItem(sb.ToString(), name.SequenceNumber));
            }
        }

        if (lbInsuredNames.Items.Count == 0)
        {
            lbInsuredNames.Enabled = false;
            lbInsuredNames.Items.Add(new ListItem("No Insured Names Exist."));
        }
        if (lbDbaNames.Items.Count == 0)
        {
            lbDbaNames.Enabled = false;
            lbDbaNames.Items.Add(new ListItem("No DBA names Exist."));
        }

        // load addresses for merge feature
        cblAddresses1.Items.Clear();
        if (client.Addresses != null)
        {
            foreach (structures.Info.Address addr in client.Addresses)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0} {1} {2} {3} {4} {5} ({6})", addr.HouseNumber, addr.Address1, addr.Address2,
                    addr.City, addr.StateProvinceId, addr.PostalCode, string.IsNullOrEmpty(addr.ClearanceAddressType) ? "NA" : addr.ClearanceAddressType);

                cblAddresses1.Items.Add(new ListItem(sb.ToString(), addr.AddressId));
            }
        }
        targetPanel.Visible = true;
        programmaticModalPopup.Show();
        OkButton.Enabled = true;
    }
    protected void Merge(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            programmaticModalPopup.Show();
            return;
        }
        ClientItem clientItem = new ClientItem();
        clientItem.Dba = txDBA.Text;
        clientItem.InsuredName = txInsuredName.Text;
        clientItem.Id = targetClientIdTextBox.Text;
        clientItem.Names = new ClientNameItem[cblNames1.Items.Count];
        for (int i = 0; i < cblNames1.Items.Count; i++)
        {
            clientItem.Names[i] = new ClientNameItem();
            clientItem.Names[i].DisplayString = cblNames1.Items[i].Text;
            clientItem.Names[i].Id = cblNames1.Items[i].Value;
            clientItem.Names[i].Selected = cblNames1.Items[i].Selected;
        }
        clientItem.Addresses = new ClientAddressItem[cblAddresses1.Items.Count];
        for (int i = 0; i < cblAddresses1.Items.Count; i++)
        {
            clientItem.Addresses[i] = new ClientAddressItem();
            clientItem.Addresses[i].DisplayString = cblAddresses1.Items[i].Text;
            clientItem.Addresses[i].Id = cblAddresses1.Items[i].Value;
            clientItem.Addresses[i].Selected = cblAddresses1.Items[i].Selected;
        }

        string[] addresses = Common.GetSelectedValues(cblAddresses1);
        string[] names = Common.GetSelectedValues(cblNames1);
        string clientId = targetClientIdTextBox.Text;
        string ins = txInsuredName.Text;
        string dba = txDBA.Text;

        switch (ClientTarget.Text)
        {
            case "SubmissionNumber":
                {
                    txSubmissionNumbersNewClientIds.Text += string.Format(",{0}:{1}:{2}:{3}:{4}", clientId, string.Join("|", names), string.Join("|", addresses),
                        ins, dba);

                    List<ClientItem> storedItems = SClients;
                    storedItems.Add(clientItem);
                    SClients = storedItems;

                    gridSubmissionNumbersNewClients.DataSource = SClients;
                    gridSubmissionNumbersNewClients.DataBind();

                    RemoveImageSubmissionNumber.Visible = true;
                    AddImageSubmissionNumber.Visible = false;
                    ScriptManager1.SetFocus(Button1);
                    break;
                }
            case "PolicyNumber":
                {
                    txPolicyNumbersNewClientIds.Text += string.Format(",{0}:{1}:{2}:{3}:{4}", clientId, string.Join("|", names), string.Join("|", addresses),
                        ins, dba);

                    List<ClientItem> storedItems = PClients;
                    storedItems.Add(clientItem);
                    PClients = storedItems;

                    gridPolicyNumbersNewClients.DataSource = PClients;
                    gridPolicyNumbersNewClients.DataBind();

                    RemoveImagePolicyNumber.Visible = true;
                    AddImagePolicyNumber.Visible = false;
                    ScriptManager1.SetFocus(Button2);
                    break;
                }
            default:
                break;
        }
        cblAddresses1.Items.Clear();
        cblNames1.Items.Clear();
        targetClientIdTextBox.Text = string.Empty;
        txInsuredName.Text = string.Empty;
        txDBA.Text = string.Empty;
        targetPanel.Visible = false;
        programmaticModalPopup.Hide();
        OkButton.Enabled = false;
    }
    protected void RemoveClient(object sender, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "SubmissionNumber":
                {
                    SClients = null;
                    gridSubmissionNumbersNewClients.DataSource = null;
                    gridSubmissionNumbersNewClients.DataBind();
                    txSubmissionNumbersNewClientIds.Text = string.Empty;
                    AddImageSubmissionNumber.Visible = true;
                    RemoveImageSubmissionNumber.Visible = false;
                    Button1.Enabled = false;
                    OkButton.Enabled = false;
                    lnkClientIDSearch.Visible = false;
                    break;
                }
            case "PolicyNumber":
                {
                    PClients = null;
                    gridPolicyNumbersNewClients.DataSource = null;
                    gridPolicyNumbersNewClients.DataBind();
                    txPolicyNumbersNewClientIds.Text = string.Empty;
                    AddImagePolicyNumber.Visible = true;
                    RemoveImagePolicyNumber.Visible = false;
                    Button1.Enabled = false;
                    OkButton.Enabled = false;
                    lnkClientIDSearch.Visible = false;
                    break;
                }
            default:
                break;
        }
    }

    protected void ValidationButton_Init(object sender, EventArgs e)
    {
        Button btn = sender as Button;
        string value = string.Format("if(!Page_ClientValidate('{0}')) {{  $find('alwaysVisible').get_element().style.display = ''; $find('alwaysVisible').initialize(); }}", btn.ValidationGroup);
        btn.Attributes.Add("onclick", value);
    
    }

    protected void SubmitButtons_OnLoad(object sender, EventArgs e)
    {
        Button btn = sender as Button;
        string value = string.Empty;
        if (btn.ID == "Button1")
            value = "StoRestore = 0;";
        else if (btn.ID == "Button2")
            value = "PtoRestore = 0;";

        btn.Attributes.Add("onclick", value);
    }
    protected void lbDbaNames_Init(object sender, EventArgs e)
    {
        lbDbaNames.Attributes.Add("onchange", string.Format("mimicComboBox({0},{1}); this.selectedIndex = -1;", lbDbaNames.ClientID, txDBA.ClientID));
    }
    protected void lbInsuredNames_Init(object sender, EventArgs e)
    {
        lbInsuredNames.Attributes.Add("onchange", string.Format("mimicComboBox({0},{1}); this.selectedIndex = -1;", lbInsuredNames.ClientID, txInsuredName.ClientID));
    }

    private void ShowMessage(string message)
    {
        ValidationTable.Style.Add("display", "");
        Common.SetStatus(messagePlaceHolder, message);
    }

    public List<ClientItem> SClients
    {
        get
        {
            if (ViewState["SClients"] == null)
                return new List<ClientItem>();
            return (List<ClientItem>)ViewState["SClients"];
        }
        set
        {
            ViewState["SClients"] = value;
        }
    }
    public List<ClientItem> PClients
    {
        get
        {
            if (ViewState["PClients"] == null)
                return new List<ClientItem>();
            return (List<ClientItem>)ViewState["PClients"];
        }
        set
        {
            ViewState["PClients"] = value;
        }
    }
}

[Serializable]
public class ClientItem
{
    private string id, insuredName, dba;

    public string Id
    {
        get { return id; }
        set { id = value; }
    }

    public string InsuredName
    {
        get { return insuredName; }
        set { insuredName = value; }
    }

    public string Dba
    {
        get { return dba; }
        set { dba = value; }
    }

    private ClientAddressItem[] addresses;

    public ClientAddressItem[] Addresses
    {
        get { return addresses; }
        set { addresses = value; }
    }
    private ClientNameItem[] names;

    public ClientNameItem[] Names
    {
        get { return names; }
        set { names = value; }
    }
}

[Serializable]
public class ClientAddressItem
{
    private string displayString, id;

    public string DisplayString
    {
        get { return displayString; }
        set { displayString = value; }
    }

    public string Id
    {
        get { return id; }
        set { id = value; }
    }

    private bool selected;

    public bool Selected
    {
        get { return selected; }
        set { selected = value; }
    }
}
[Serializable]
public class ClientNameItem
{
    private string displayString, id;

    public string DisplayString
    {
        get { return displayString; }
        set { displayString = value; }
    }

    public string Id
    {
        get { return id; }
        set { id = value; }
    }

    private bool selected;

    public bool Selected
    {
        get { return selected; }
        set { selected = value; }
    }
}