using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using structures = BTS.ClientCore.Wrapper.Structures;
using BCS.WebApp.UserControls;
using BCS.WebApp.Components;

public partial class SwitchClient : System.Web.UI.Page
{
    protected string DBA
    {
        get { return ViewState["DBA"] != null ? (string)ViewState["DBA"] : string.Empty; }
        set { ViewState["DBA"] = value; }
    }

    public string InsuredName
    {
        get { return ViewState["InsuredName"] != null ? (string)ViewState["InsuredName"] : string.Empty; }
        set { ViewState["InsuredName"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        info.InnerText = string.Format("Please enter a valid client id below, to switch this submission (#{0}) to.", Session["SubmissionNumber"]);
    }
    protected void btnGetClient_Click(object sender, EventArgs e)
    {
        LoadClient(txtClientId.Text);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;
        int id = Convert.ToInt32(Session["SubmissionId"]);
        SubmissionProxy.Submission s = Common.GetSubmissionProxy();
        BCS.Core.Clearance.BizObjects.SubmissionList sl = (BCS.Core.Clearance.BizObjects.SubmissionList)
            BCS.Core.XML.Deserializer.Deserialize(
            s.GetSubmissionById(id), typeof(BCS.Core.Clearance.BizObjects.SubmissionList));

        string codetypes = string.Empty;

        if (sl.List[0].SubmissionCodeTypess != null)
        {
            string[] ctypeids = new string[sl.List[0].SubmissionCodeTypess.Length];
            for (int i = 0; i < ctypeids.Length; i++)
            {
                ctypeids[i] = sl.List[0].SubmissionCodeTypess[i].CodeId.ToString();
            }
            codetypes = string.Join("|", ctypeids);
        }
        string attributes = string.Empty;

        if (sl.List[0].AttributeList != null)
        {
            string[] attrs = new string[sl.List[0].AttributeList.Length];
            for (int i = 0; i < attrs.Length; i++)
            {
                attrs[i] = string.Format("{0}={1}", sl.List[0].AttributeList[i].CompanyNumberAttributeId, sl.List[0].AttributeList[i].AttributeValue);
            }
            attributes = string.Join("|", attrs);
        }

        // still we are using single LOB
        //string lobs = string.Empty;

        //if (sl.List[0].LineOfBusinessList != null)
        //{
        //    string[] lobss = new string[sl.List[0].LineOfBusinessList.Length];
        //    for (int i = 0; i < lobss.Length; i++)
        //    {
        //        lobss[i] = string.Format("{0}", sl.List[0].LineOfBusinessList[i].Id);
        //    }
        //    lobs = string.Join("|", lobss);
        //}

        #region Line Of Business Children
        string lobchildren = string.Empty;
        BCS.Core.Clearance.BizObjects.Submission _submission = sl.List[0];
        if (_submission.LineOfBusinessChildList != null && _submission.LineOfBusinessChildList.Length > 0)
        {
            string[] slobss = new string[_submission.LineOfBusinessChildList.Length];
            for (int i = 0; i < _submission.LineOfBusinessChildList.Length; i++)
            {
                slobss[i] = string.Format("{0}={1}={2}={3}={4}",
                    _submission.LineOfBusinessChildList[i].Id,
                    _submission.LineOfBusinessChildList[i].CompanyNumberLOBGridId,
                    _submission.LineOfBusinessChildList[i].SubmissionStatusId,
                    _submission.LineOfBusinessChildList[i].SubmissionStatusReasonId,
                    _submission.LineOfBusinessChildList[i].Deleted);
            }
            lobchildren = string.Join("|", slobss);
        }
        #endregion
        #region Agent Unspecified Reasons
        string agentunspecifiedreasons = string.Empty;
        if (_submission.AgentUnspecifiedReasonList != null && _submission.AgentUnspecifiedReasonList.Length > 0)
        {
            string[] arr = new string[_submission.AgentUnspecifiedReasonList.Length];
            for (int i = 0; i < _submission.AgentUnspecifiedReasonList.Length; i++)
            {
                arr[i] = string.Format("{0}", _submission.AgentUnspecifiedReasonList[i].CompanyNumberAgentUnspecifiedReasonId);
            }
            agentunspecifiedreasons = string.Join("|", arr);
        }
        #endregion

        string[] arrSelectedAddresses = Common.GetSelectedValues(cblAddresses);
        string SelectedAddresses = string.Join("|", arrSelectedAddresses);

        string[] arrSelectedNames = Common.GetSelectedValues(cblNames);
        string SelectedNames = string.Join("|", arrSelectedNames);

        string edtXML = s.ModifySubmissionByIds(id.ToString(), Convert.ToInt64(hfClientId.Value),
            string.Empty, // stop gap for other apps
            SelectedAddresses, SelectedNames, hfPortfolioId.Value.Length == 0 ? 0 : Convert.ToInt64(hfPortfolioId.Value),
            sl.List[0].CompanyId, sl.List[0].SubmissionTypeId, sl.List[0].AgencyId, sl.List[0].AgentId, agentunspecifiedreasons, sl.List[0].ContactId,
            sl.List[0].CompanyNumberId, sl.List[0].SubmissionStatusId,
            sl.List[0].SubmissionStatusDate == DateTime.MinValue ? null : sl.List[0].SubmissionStatusDate.ToString(),
            sl.List[0].SubmissionStatusReasonId,
            sl.List[0].SubmissionStatusReasonOther,
            sl.List[0].PolicyNumber,
            codetypes, attributes,
            sl.List[0].EstimatedWrittenPremium,
            sl.List[0].EffectiveDate == DateTime.MinValue ? null : sl.List[0].EffectiveDate.ToString(),
            sl.List[0].ExpirationDate == DateTime.MinValue ? null : sl.List[0].ExpirationDate.ToString(),
            sl.List[0].UnderwriterId, sl.List[0].AnalystId, sl.List[0].TechnicianId, sl.List[0].PolicySystemId, sl.List[0].Comments, Common.GetUser(),
            sl.List[0].LineOfBusinessList[0].Id, lobchildren, InsuredName, DBA, sl.List[0].OtherCarrierId,
            sl.List[0].OtherCarrierPremium);

        try
        {
            BCS.Core.Clearance.BizObjects.Submission coresub = 
                (BCS.Core.Clearance.BizObjects.Submission)BCS.Core.XML.Deserializer.Deserialize(
                edtXML, typeof(BCS.Core.Clearance.BizObjects.Submission));
            if (coresub != null)
                if (coresub.Message != null)
                {
                    Session["SubmissionId"] = coresub.Id.ToString();
                    //Session["StatusMessage"] = string.Format("{0}, Submission Number : {1}", coresub.Message, coresub.SubmissionNumber);
                    Common.SetStatus(Master.FindControl("contentplaceholder1"), coresub.Message);

                    // update submission structure used to restore page state.
                    if (Session["temporarySubmission"] != null)
                    {
                        BCS.Core.Clearance.BizObjects.Submission _s = (BCS.Core.Clearance.BizObjects.Submission)Session["temporarySubmission"];
                        int[] intarrSelectedAddresses = Array.ConvertAll<string, int>(arrSelectedAddresses, new Converter<string, int>(Convert.ToInt32));
                        _s.ClientCoreAddressIds = intarrSelectedAddresses;
                        int[] intarrSelectedNames = Array.ConvertAll<string, int>(arrSelectedNames, new Converter<string, int>(Convert.ToInt32));
                        _s.ClientCoreNameIds = intarrSelectedNames;

                        _s.InsuredName = InsuredName;
                        _s.Dba = DBA;

                        Session["temporarySubmission"] = _s;
                    }
                }
            Cache.Remove("SearchedCacheDependency");
        }
        catch (Exception)
        {
            Common.SetStatus(Master.FindControl("contentplaceholder1"),
                "Submission Edit not successful. ");
        }
            

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Submission.aspx?op=edit");
    }

    private structures.Info.ClientInfo LoadClient(string clientid)
    {
        //CompanyNumberDropDownList cnddl = Master.FindControl("companyselector1").FindControl("CompanyNumberDropDownList1") as CompanyNumberDropDownList;
                
        ClientProxy.Client clientproxy = Common.GetClientProxy();
        string clientxml = clientproxy.GetClientById(Common.GetSafeStringFromSession(SessionKeys.CompanyNumber), clientid);

        try
        {
            //structures.Info.ClientInfo client = (structures.Info.ClientInfo)BCS.Core.XML.Deserializer.Deserialize(
            //clientxml, typeof(structures.Info.ClientInfo), "seq_nbr");

            BCS.Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                (BCS.Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                clientxml, typeof(BCS.Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

            structures.Info.ClientInfo client = clearanceclient.ClientCoreClient;

            cblAddresses.Items.Clear();
            cblNames.Items.Clear();

            hfClientId.Value = client.Client.ClientId;
            hfPortfolioId.Value = client.Client.PortfolioId;
            pnlClientInfo.Visible = true;
            tblClientInfo.Visible = true;
            if (client.Client.ClientId == clientid)
            {
                Session["Client"] = client;
                int dbaCount = 0;
                int insuredCount = 0;
                foreach (structures.Info.Name name in client.Client.Names)
                {
                    StringBuilder sb = new StringBuilder();
                    if (name.NameBusiness != null && name.NameBusiness.Length > 0)
                        sb.Append(name.NameBusiness);
                    else
                        sb.AppendFormat("{0} {1} {2}", name.NameFirst, name.NameMiddle, name.NameLast);

                    if (name.ClearanceNameType == "DBA")
                    {
                        DBA = name.NameBusiness;
                        dbaCount++;
                    }

                    if (name.ClearanceNameType == "Insured")
                    {
                        InsuredName = name.NameBusiness;
                        insuredCount++;
                    }
                    
                    //rblNames.Items.Add(new ListItem(sb.ToString(), name.SequenceNumber));
                    cblNames.Items.Add(new ListItem(Server.HtmlEncode(sb.ToString()), name.SequenceNumber));
                }
                if (cblNames.Items.Count == 1)
                    cblNames.Items[0].Selected = true;
                if (dbaCount > 1)
                    DBA = string.Empty;
                if (insuredCount > 1)
                    InsuredName = string.Empty;

                if (client.Addresses != null)
                {
                    foreach (structures.Info.Address addr in client.Addresses)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0} {1} {2} {3} {4} {5}", addr.HouseNumber, addr.Address1, addr.Address2,
                            addr.City, addr.StateProvinceId, addr.PostalCode);

                        cblAddresses.Items.Add(new ListItem(Server.HtmlEncode(sb.ToString()), addr.AddressId));
                    }
                    if (cblAddresses.Items.Count == 1)
                        cblAddresses.Items[0].Selected = true;
                }
            }
            return client;

        }
        catch (Exception ex)
        {
            BTS.LogFramework.LogCentral.Current.LogWarn(string.Format("The client associated with the submission either does not exist or is inaccessible. Id = {0} CompanyNumberId = {1}", clientid, Session["CompanyNumberSelected"]), ex);
            lblPortfolioId.Visible = false;

            tblClientInfo.Visible = false;

            pnlClientInfo.Visible = true;
            Common.SetStatus(pnlClientInfo,
                "The client associated with the submission either does not exist or is inaccessible.");
        }
        return new BTS.ClientCore.Wrapper.Structures.Info.ClientInfo();
    }

   
}
