#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using BCS.WebApp.Components;
using System.Text.RegularExpressions;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using BTS.WFA.BCS.ViewModels;

#endregion

namespace BCS.WebApp.UserControls
{
    public partial class UserControls_ClientAddress : System.Web.UI.UserControl
    {
        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            colAddr22.Visible = Mode != Common.Mode.Search;
            colAddr21.Visible = Mode != Common.Mode.Search;
            colCounty1.Visible = Mode != Common.Mode.Search;
            colCounty2.Visible = Mode != Common.Mode.Search;
            rowFirst.Visible = Mode != Common.Mode.Search && Components.DefaultValues.SupportsInternationalAddresses(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        }

        protected override void OnInit(EventArgs e)
        {
            this.btnSave.Visible = Mode == Common.Mode.Modify && !Components.ConfigValues.AddressEditable(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            this.btnRemove.Visible = Mode == Common.Mode.Add || (Mode == Common.Mode.Modify && this.AddressIdValue == null);
            rblAddressType.Visible = !(Mode == Common.Mode.Search);
            rfvAddressType.Visible = !(Mode == Common.Mode.Search);

            StateDropDownList1.DataBind();
            CountryDropDownList1.DataBind();
            rblAddressType.DataBind();

            //this.txAddress2.MaxLength = Components.ConfigValues.GetAddress2Length(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            // this was already set to 10 regardless of the client system
            //this.txHouse.MaxLength = Components.ConfigValues.GetStreetnumberLength(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            //this.txStreetname.MaxLength = Components.ConfigValues.GetStreetnameLength(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            //this.txCity.MaxLength = Components.ConfigValues.GetCityLength(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

            base.OnInit(e);
        }
        #endregion

        #region Properties

        public Common.Mode Mode
        {
            set
            {
                ViewState["Mode"] = value;
                this.btnSave.Visible = Mode == Common.Mode.Modify && ConfigValues.AddressEditable(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                this.btnRemove.Visible = Mode == Common.Mode.Add || (Mode == Common.Mode.Modify && this.AddressIdValue == null);
                rblAddressType.Visible = !(Mode == Common.Mode.Search);
                rfvAddressType.Visible = !(Mode == Common.Mode.Search);

                colAddr22.Visible = Mode != Common.Mode.Search;
                colAddr21.Visible = Mode != Common.Mode.Search;
                colCounty1.Visible = Mode != Common.Mode.Search;
                colCounty2.Visible = Mode != Common.Mode.Search;
                rowFirst.Visible = Mode != Common.Mode.Search && Components.DefaultValues.SupportsInternationalAddresses(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

                rfvAddressType_Init(rfvAddressType, EventArgs.Empty);

                BCS.Core.Security.CustomPrincipal cPrincpal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
                bool shouldEnable = (Mode == Common.Mode.Add  // mode is add or
                    || (Mode == Common.Mode.Modify && string.IsNullOrEmpty(this.AddressIdValue)))
                    || (ConfigValues.AddressEditable(Common.GetSafeIntFromSession(SessionKeys.CompanyId))// mode is edit, but the address is new or
                        ? true : cPrincpal.IsInAnyRoles("Company Administrator", "System Administrator") // either company allows edit OR if Company/System administrator
                        );
                this.txHouse.Enabled = shouldEnable;
                this.txStreetname.Enabled = shouldEnable;
                this.txAddress2.Enabled = shouldEnable;
                this.txCity.Enabled = shouldEnable;
                this.StateDropDownList1.Enabled = shouldEnable;
                this.txZipCode.Enabled = shouldEnable;
                //this.txCounty.Enabled = shouldEnable;
            }
            get
            {
                if (ViewState["Mode"] == null)
                    return Common.Mode.Search;
                return (Common.Mode)ViewState["Mode"];
            }
        }

        public string AddressIdValue
        {
            get { return ViewState["AddressIDValue"] as string; }
            set { ViewState["AddressIDValue"] = value; }

        }

        public string HouseValue
        {
            get { return this.txHouse.Text; }
            set { this.txHouse.Text = value; }
        }

        public string StreetnameValue
        {
            get { return this.txStreetname.Text; }
            set { this.txStreetname.Text = value; }
        }
        public string Address2Value
        {
            get { return this.txAddress2.Text; }
            set { this.txAddress2.Text = value; }
        }

        public string CityValue
        {
            get { return this.txCity.Text; }
            set { this.txCity.Text = value; }
        }

        // based on text since we have disparate ids in client core and clearance databases
        public string StateValue
        {
            get
            {
                bool domestic = RadioButtonList1.SelectedValue == "Domestic";
                if (domestic)
                    return this.StateDropDownList1.SelectedItem.Text;
                else
                    return this.txState.Text;
            }
            set
            {
                bool domestic = RadioButtonList1.SelectedValue == "Domestic";
                if (domestic)
                {
                    if (value != null)
                        StateDropDownList1.SelectedValue = StateDropDownList1.Items.FindByText(value).Value;
                    else StateDropDownList1.SelectedIndex = 0;
                }
                else
                    this.txState.Text = value;

            }
        }

        public string CountryValue
        {
            get { return this.CountryDropDownList1.SelectedValue; }
            set
            {
                this.CountryDropDownList1.Items.FindByValue(value).Selected = true;
                if (value == "USA")
                { RadioButtonList1.Items.FindByValue("Domestic").Selected = true; ChangeUI(); }
                else
                { RadioButtonList1.Items.FindByValue("Foreign").Selected = true; ChangeUI(); }
            }
        }

        public string ZipCodeValue
        {
            get { return this.txZipCode.Text; }
            set { this.txZipCode.Text = value; }
        }
        public string CountyValue
        {
            get { return this.txCounty.Text; }
            set { this.txCounty.Text = value; }
        }

        public string ClearanceAddressTypeValue
        {
            get
            {
                return rblAddressType.SelectedItem == null ? null : rblAddressType.SelectedItem.Text;
            }
            set
            {
                rblAddressType.Items.FindByText(value).Selected = true;
            }
        }

        public bool Enabled
        {
            set
            {
                foreach (Control c in this.Controls)
                {
                    if (c is WebControl)
                        (c as WebControl).Enabled = value;
                }
                B.Enabled = value;
            }
        }

        public void SetGeoCodeDescription(string latitude, string longitude)
        {
            lblCoordinates.Text = GetGeoCodeDescription(latitude, longitude);
        }
        public string GetGeoCodeDescription(string latitude, string longitude)
        {
            string desc = "";
            if (!String.IsNullOrEmpty(longitude) && !String.IsNullOrEmpty(latitude))
                desc = String.Format("Address coordinates found ({0}, {1})", latitude, longitude);

            return desc;
        }

        public bool PrimaryAddress
        {
            set
            { rdoPrimaryAddress.Checked = value; }
            get
            { return rdoPrimaryAddress.Checked; }
        }

        public bool PrimaryFlagEditable
        {
            set
            { hfPrimaryFlagEditable.Value = value.ToString(); }
            get
            {
                string value = hfPrimaryFlagEditable.Value;
                return (String.IsNullOrEmpty(value)) ? false : bool.Parse(value);
            }
        }

        #endregion

        #region Control Events
        protected void btnRemove_Click(object sender, ImageClickEventArgs e)
        {
            if (this.AddressIdValue == null || this.AddressIdValue.Length == 0)
                btnRemove.Parent.Parent.Controls.Remove(btnRemove.Parent.Parent.FindControl(this.ID));
        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeUI();
        }

        protected void CountryDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CountryDropDownList1.SelectedValue == "USA")
            {
                RadioButtonList1.ClearSelection();
                RadioButtonList1.Items.FindByText("Domestic").Selected = true;
                ChangeUI();
            }
        }
        #endregion

        #region Other Methods
        private void ChangeUI()
        {
            bool domestic = RadioButtonList1.SelectedValue == "Domestic";

            StateDropDownList1.Visible = domestic;
            txState.Visible = !domestic;
            tdCountryText.Visible = !domestic;
            tdCountryValue.Visible = !domestic;
        }

        public void SetFocus()
        {
            if (null != ScriptManager.GetCurrent(this.Page))
            {
                ScriptManager.GetCurrent(this.Page).SetFocus(txHouse);
            }
            else
            {
                txHouse.Focus();
            }
        }
        #endregion


        protected void Panel1_Load(object sender, EventArgs e)
        {
            if (!(sender as Panel).Enabled)
            {
                // in case of gis web services not working, just disable the panel. and make image invisible.
                GRIDID.DataSource = null;
                GRIDID.DataBind();
                return;
            }

            if (IsPostBack)
            {
                var addr1 = txStreetname.Text.Trim();
                var buildingNumber = txHouse.Text.Trim();
                if (!String.IsNullOrEmpty(buildingNumber))
                    addr1 = buildingNumber + " " + addr1;
                var searchCriteria = new BTS.WFA.BCS.ViewModels.AddressLookupCriteriaViewModel() { Address1 = addr1, Address2 = txAddress2.Text.Trim(), City = txCity.Text.Trim(), State = StateDropDownList1.SelectedValue, PostalCode = txZipCode.Text.Trim(), County = txCounty.Text.Trim() };

                if (!searchCriteria.IsEmpty)
                {
                    var companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
                    var cn = BTS.WFA.BCS.Application.CompanyMap.Values.First(c => c.Summary.CompanyNumber == companyNumber);
                    var results = BTS.WFA.BCS.Services.AddressService.Lookup(cn, searchCriteria);
                    var list = new List<LegacyClientAddressViewModel>();
                    foreach (var m in results.Matches)
                    {
                        var splitAddrLine = BTS.WFA.BCS.Services.AddressService.SplitBuildNumberAddressLine(m.Address1);
                        list.Add(new LegacyClientAddressViewModel()
                        {
                            BuildingNumber = splitAddrLine.Item1,
                            Address1 = splitAddrLine.Item2,
                            Address2 = m.Address2,
                            City = m.City,
                            State = m.State,
                            PostalCode = m.PostalCode,
                            County = m.County,
                            Latitude = m.Latitude,
                            Longitude = m.Longitude
                        });
                    }
                    GRIDID.DataSource = list;
                    GRIDID.DataBind();
                }
            }
        }
        protected void Rows_Bound(object sender, GridViewRowEventArgs e)
        {
            WebControl wc = e.Row.FindControl("Button11") as WebControl;
            if (wc != null)
            {
                Label l = e.Row.FindControl("lblBuildingNumber") as Label;
                string buildingNumber = l.Text.Trim();

                l = e.Row.FindControl("lblAddress1") as Label;
                string addr1 = l.Text.Trim();

                l = e.Row.FindControl("lblAddress2") as Label;
                string addr2 = l.Text.Trim();

                l = e.Row.FindControl("lblCity") as Label;
                string cityValue = l.Text.Trim();

                l = e.Row.FindControl("lblState") as Label;
                string stateValue = l.Text.Trim();

                l = e.Row.FindControl("lblZipCode") as Label;
                string zipValue = l.Text.Trim();

                l = e.Row.FindControl("lblCounty") as Label;
                string countyValue = l.Text.Trim();

                string longitude = ((HiddenField)e.Row.FindControl("hfLongitude")).Value;
                string latitude = ((HiddenField)e.Row.FindControl("hfLatitude")).Value;

                StringBuilder script = new StringBuilder();
                script.AppendFormat("document.getElementById('{0}').value = '{1}';", txCity.ClientID, cityValue.Replace("'", @"\'"));
                script.AppendFormat("document.getElementById('{0}').value = '{1}';", txZipCode.ClientID, zipValue.Replace("'", @"\'"));
                script.AppendFormat("document.getElementById('{0}').value = '{1}';", StateDropDownList1.ClientID, stateValue.Replace("'", @"\'"));
                if (!String.IsNullOrEmpty(buildingNumber))
                    script.AppendFormat("document.getElementById('{0}').value = '{1}';", txHouse.ClientID, buildingNumber.Replace("'", @"\'"));
                if (!String.IsNullOrEmpty(addr1))
                    script.AppendFormat("document.getElementById('{0}').value = '{1}';", txStreetname.ClientID, addr1.Replace("'", @"\'"));

                if (Mode != Common.Mode.Search)
                {
                    script.AppendFormat("document.getElementById('{0}').value = '{1}';", txCounty.ClientID, countyValue.Replace("'", @"\'"));
                    if (!String.IsNullOrEmpty(addr2))
                        script.AppendFormat("document.getElementById('{0}').value = '{1}';", txAddress2.ClientID, addr2.Replace("'", @"\'"));
                    script.AppendFormat("document.getElementById('{0}').innerHTML = '{1}';", lblCoordinates.ClientID, GetGeoCodeDescription(latitude, longitude));
                }

                script.AppendFormat("hideModalPopupViaClient(event, '{0}'); cancelEvent(event);", mpeLookup.BehaviorID);

                wc.Attributes.Add("onclick", script.ToString());
            }
        }

        protected void PageIndex_Changing(object sender, GridViewPageEventArgs e)
        {
            GRIDID.PageIndex = e.NewPageIndex;
            GRIDID.DataBind();
            mpeLookup.Show();


            //GRID1.PageIndex = e.NewPageIndex;
            //GRID1.DataBind();

        }
        protected void PageIndex_Changed(object sender, EventArgs e)
        {

        }
        protected void B_Click(object sender, ImageClickEventArgs e)
        {
            mpeLookup.Show();

        }
        protected void mpeLookup_Init(object sender, EventArgs e)
        {
            mpeLookup.BehaviorID = this.ClientID + "programmaticModalPopupBehavior";
        }
        protected void rfvAddressType_Init(object sender, EventArgs e)
        {
            rfvAddressType.Enabled = DefaultValues.RequiresAddressType(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) && Mode != Common.Mode.Search;
        }
    }
}