<%@ Control Language="C#" CodeFile="~/WebUserControls/CompanyNoCodesControl.ascx.cs" Inherits="CompanyNoCodesControl"%>
    <aje:ScriptManager id="ScriptManager1" runat="server">
        <Scripts>
            <aje:ScriptReference Path='<%$ Code : string.Concat(DefaultValues.ScriptPath, "WebUIValidationExtended.js") %>' />  
        </Scripts>
    </aje:ScriptManager>
<%--<script src="Javascripts/WebUIValidationExtended.js" type="text/javascript" ></script>--%>

<span class="heading">Company Codes</span>
   <table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">&nbsp;</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        
    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="odsCodeTypes" DataTextField="CodeName"
        DataValueField="Id" OnPreRender="DropDownList1_PreRender">
    </asp:DropDownList>&nbsp;<asp:Button ID="Button1" runat="server"  Text="Filter By Code Type" OnClick="Button1_Click" />
    
    <asp:ObjectDataSource ID="odsCodeTypes" runat="server" SelectMethod="GetCompanyNumberCodeTypes"
        TypeName="BCS.Core.Clearance.Admin.DataAccess">
        <SelectParameters>
            <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected"
                Type="Int32" />
            <asp:Parameter DefaultValue="false" Name="companyDependent" Type="Boolean" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    <br />
    <br />
    <UserControls:Grid ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="odsCNCodes" OnPageIndexChanged="GridView1_PageIndexChanged" OnRowDeleted="GridView1_RowDeleted" EmptyDataText="No Codes exist for the Company and Company Number Selected." OnSelectedIndexChanged="GridView1_SelectedIndexChanged" OnSelectedIndexChanging="GridView1_SelectedIndexChanging">
        <Columns>
            <asp:CommandField ButtonType="Button" ShowSelectButton="True" SelectText="Edit" ControlStyle-Width="40px" />
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />            
            <asp:BoundField DataField="CompanyNumberId" HeaderText="CompanyNumberId" Visible="False" />
            <asp:BoundField DataField="CompanyCodeTypeId" HeaderText="CompanyCodeTypeId" Visible="False" />
            <asp:TemplateField HeaderText="Company Number">                
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CompanyNumber.PropertyCompanyNumber") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Code Type">                
                <ItemTemplate>
                    <asp:Label ID="Label16" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CompanyNumberCodeType.CodeName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>            
            <asp:BoundField DataField="Code" HeaderText="Code" />
            <asp:BoundField DataField="Description" HeaderText="Description" />            
            <asp:TemplateField HeaderText="Expiration Date" >
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("ExpirationDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                        Text="Delete" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() %>'/>
                <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="btnDelete"
                    ConfirmText='<%# DataBinder.Eval (Container.DataItem, "Code", "Are you sure you want to delete this Code {0}?") %>'>
                </ajx:ConfirmButtonExtender>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </UserControls:Grid>
    <br />
    <asp:Button ID="btnAdd" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() %>' OnClick="btnAdd_Click"
        Text="Add" /><br />
    <br />
    
    <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
        <br />
    </asp:PlaceHolder>
    
    <asp:ObjectDataSource ID="odsCNCodes" runat="server" SelectMethod="GetCompanyNumberCodes"
        TypeName="BCS.Core.Clearance.Admin.DataAccess" DeleteMethod="DeleteCompanyNumberCode" OnDeleted="odsCNCodes_Deleted">
        <SelectParameters>
            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="filterCompanyNumberId" SessionField="CompanyNumberSelected"
                Type="Int32" />
            <asp:ControlParameter ControlID="DropDownList1" DefaultValue="" Name="filterCodeTypeId"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
    <asp:DetailsView ID="DetailsView1" runat="server" DataSourceID="odsCompanyNoCode"
        AutoGenerateRows="False" OnModeChanging="DetailsView1_ModeChanging" OnItemInserted="DetailsView1_ItemInserted" DataKeyNames="Id" OnItemUpdating="DetailsView1_ItemUpdating" OnItemInserting="DetailsView1_ItemInserting" OnItemUpdated="DetailsView1_ItemUpdated">
        <Fields>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" Visible="False" />            
            <asp:TemplateField HeaderText="Company Code Type">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="ObjectDataSource2"
                        DataTextField="CodeName" DataValueField="Id" SelectedValue='<%# Bind("CompanyCodeTypeId") %>' OnPreRender="DropDownList2_PreRender">
                    </asp:DropDownList><asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetCompanyNumberCodeTypes"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected"
                                Type="Int32" />
                            <asp:Parameter DefaultValue="false" Name="companyDependent" Type="Boolean" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList3" runat="server" DataSourceID="ObjectDataSource2" DataTextField="CodeName"
                        DataValueField="Id" SelectedValue='<%# Bind("CompanyCodeTypeId") %>' OnPreRender="DropDownList2_PreRender">
                    </asp:DropDownList><asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetCompanyNumberCodeTypes"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess" OldValuesParameterFormatString="original_{0}">
                        <SelectParameters>
                            <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected"
                                Type="Int32" />
                            <asp:Parameter DefaultValue="false" Name="companyDependent" Type="Boolean" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                   <asp:Label ID="Label15" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CompanyNumberCodeType.CodeName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Code">
                <EditItemTemplate>
                    <asp:TextBox ID="txtCode" runat="server" Text='<%# Bind("Code") %>'></asp:TextBox>
                    <UserControls:MultipleFieldsValidator ID="MultipleFieldsValidator" ErrorMessage="&nbsp;" Text="* Code or Description" 
                        runat="server" Condition="Or" ControlsToValidate="txtCode,txtDescription" Display="Dynamic">
                    </UserControls:MultipleFieldsValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtCode" runat="server" Text='<%# Bind("Code") %>'></asp:TextBox>
                    <UserControls:MultipleFieldsValidator ID="MultipleFieldsValidator" ErrorMessage="&nbsp;" Text="* Code or Description"
                        runat="server" Condition="Or" ControlsToValidate="txtCode,txtDescription" Display="Dynamic">
                    </UserControls:MultipleFieldsValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCode" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField><asp:TemplateField HeaderText="Description">
                <EditItemTemplate>
                    <asp:TextBox ID="txtDescription" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtDescription" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Expiration Date">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("ExpirationDate", "{0:MM/dd/yyyy}") %>' OnInit="DatePickerTextBox_Init"></asp:TextBox>
                    <asp:CompareValidator
                        ID="CompareValidator10" runat="server" ControlToValidate="TextBox10" Display="Dynamic"
                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("ExpirationDate", "{0:MM/dd/yyyy}") %>' OnInit="DatePickerTextBox_Init"></asp:TextBox>
                    <asp:CompareValidator
                        ID="CompareValidator10" runat="server" ControlToValidate="TextBox10" Display="Dynamic"
                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label13" runat="server" Text='<%# Bind("ExpirationDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ButtonType="Button" ShowEditButton="True" ShowInsertButton="True" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="odsCompanyNoCode" runat="server" SelectMethod="GetCompanyNumberCode"
        TypeName="BCS.Core.Clearance.Admin.DataAccess" DataObjectTypeName="BCS.Core.Clearance.BizObjects.CompanyNumberCode" InsertMethod="AddCompanyNumberCode" UpdateMethod="UpdateCompanyNumberCode" OnInserted="odsCompanyNoCode_Inserted" OnUpdated="odsCompanyNoCode_Updated">
        <SelectParameters>
            <asp:ControlParameter ControlID="GridView1" Name="id" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    
    </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>