using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.UserControls;
using BCS.Core.Clearance.BizObjects;
using APSBO = BCS.Core.Clearance.BizObjects.APS;
using BCS.WebApp.Components;
using System.Collections.Specialized;
using BCS.Biz;

public partial class WebUserControls_LOBList : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ddlStatuses.Items.Add(new ListItem("","0"));
            // bind statuses
            string[] arrIds = DefaultValues.GetStatusIds(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            
            DataManager dm = new DataManager(DefaultValues.DSN);
            dm.QueryCriteria.And(JoinPath.SubmissionStatus.Columns.Id, arrIds, OrmLib.MatchType.In);
            SubmissionStatusCollection statuses = dm.GetSubmissionStatusCollection();
            statuses = statuses.SortByStatusCode(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.SubmissionStatus var in statuses)
            {
                ddlStatuses.Items.Add(new ListItem(var.StatusCode, var.Id.ToString()));
            }
            
            //AddItem();
            AddEmptyItem();
        }
                
        ibUpdate.Enabled = ddlStatuses.SelectedIndex > 0;
    }    
    protected void ibAdd_Click(object sender, ImageClickEventArgs e)
    {
        if (DynamicControlsPlaceHolder1.Controls.Count == 1)
        {
            if (! (DynamicControlsPlaceHolder1.Controls[0] is BCS.WebApp.WebUserControls.WebUserControls_LOBListItem))
            {
                DynamicControlsPlaceHolder1.Controls.Clear();
            }
        }
        AddItem();
    }

    protected void btn_Command(object sender, CommandEventArgs e)
    {
        Button btn = sender as Button;
        DropDownList ddl = FindControl(btn.CommandArgument) as DropDownList;
        SetStatus(btn.CommandName, ddl == null ? string.Empty : ddl.SelectedItem.Value);
        SetFocus(btn);
    }

    protected void SetStatus(string status, string reason)
    {
        foreach (Control item in DynamicControlsPlaceHolder1.Controls)
        {
            if (item is BCS.WebApp.WebUserControls.WebUserControls_LOBListItem)
            {
                BCS.WebApp.WebUserControls.WebUserControls_LOBListItem listitem = item as BCS.WebApp.WebUserControls.WebUserControls_LOBListItem;
                if (!listitem.IsBlank())
                    listitem.SetStatus(status, reason); 
            }
        }

        // update status controls on the page itself
        Control searchControl = Page;
        if (Page.Master != null)
            searchControl = Page.Master.FindControl("contentplaceholder1");

        Control c = searchControl.FindControl("SubmissionStatusDropDownList1");
        SubmissionStatusDropDownList submissionStatusDropDownList = c as SubmissionStatusDropDownList;

        
        string excludeStatusesBasedOn = "Value";
        string isExcludedStatus = submissionStatusDropDownList.SelectedItem.Text;
        excludeStatusesBasedOn = hdnExcludedStatusesBasedOn.Value;
        if (excludeStatusesBasedOn == "Value")
        { isExcludedStatus = submissionStatusDropDownList.SelectedItem.Value; }
        else if (excludeStatusesBasedOn == "Text")
        { isExcludedStatus = submissionStatusDropDownList.SelectedItem.Text; }
        else
        {
            throw new ApplicationException("Excluded Statuses must be based on Id (List Item value) or StatusCode (List Item text)");
        }
        
        // don't update status if it is one of excluded statuses
        if (submissionStatusDropDownList.SelectedItem != null && IsAutoUpdateExcludedStatus(isExcludedStatus))
            return;

        submissionStatusDropDownList.ClearSelection();
        ListItem wantedListItem = submissionStatusDropDownList.Items.FindByValue(status);
        if (wantedListItem != null)
        {
            wantedListItem.Selected = true;

            c = searchControl.FindControl("SubmissionStatusReasonDropDownList1");
            SubmissionStatusReasonDropDownList submissionStatusReasonDropDownList = c as SubmissionStatusReasonDropDownList;
            submissionStatusReasonDropDownList.StatusId = Convert.ToInt32(wantedListItem.Value);
            submissionStatusReasonDropDownList.DataBind();

            if (!string.IsNullOrEmpty(reason))
                submissionStatusReasonDropDownList.Items.FindByValue(reason).Selected = true;
        }
    }
    public bool IsValid()
    {
        string message;
        foreach (Control item in DynamicControlsPlaceHolder1.Controls)
        {
            if (item is BCS.WebApp.WebUserControls.WebUserControls_LOBListItem)
            {
                BCS.WebApp.WebUserControls.WebUserControls_LOBListItem listitem = item as BCS.WebApp.WebUserControls.WebUserControls_LOBListItem;
                if (!listitem.IsValid(out message))
                {
                    LOBListValidator.IsValid = false;
                    LOBListValidator.Text = message;
                    LOBListValidator.ErrorMessage = "&nbsp;";
                    return false;
                }
            }
        }
        SubmissionLineOfBusinessChild[] allItems = GetItems(false);
        if (allItems == null || allItems.Length == 0)
        {
            SetFocus(ibAdd);
            message = "* There should be at least one line of business row entered.";
            LOBListValidator.IsValid = false;
            LOBListValidator.Text = message;
            LOBListValidator.ErrorMessage = "&nbsp;";
            return false;
        }
        else // there are items but they might be deleted
        {
            bool atLeastOneVisibleLOBPresent = false;
            foreach (SubmissionLineOfBusinessChild var in allItems)
            {
                if (!var.Deleted)
                {
                    atLeastOneVisibleLOBPresent = true;
                    break;
                }
            }
            if (!atLeastOneVisibleLOBPresent)
            {
                SetFocus(ibAdd);
                message = "* There should be at least one line of business row entered.";
                LOBListValidator.IsValid = false;
                LOBListValidator.Text = message;
                LOBListValidator.ErrorMessage = "&nbsp;";
                return false;
            }
        }
        
        bool atLeastOneLOBPresent = false;
        foreach (SubmissionLineOfBusinessChild var in allItems)
        {
            if (var.CompanyNumberLOBGridId > 0)
            {
                atLeastOneLOBPresent = true;
                break;
            }
        }
        if (!atLeastOneLOBPresent)
        {
            SetItemFocus();
            message = "* Please select a line of business.";
            LOBListValidator.IsValid = false;
            LOBListValidator.Text = message;
            LOBListValidator.ErrorMessage = "&nbsp;";
            return false;
        }
        LOBListValidator.IsValid = true;
        return true;
    }

    public string GetItemsAsString(bool excludeBlanks)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        foreach (Control item in DynamicControlsPlaceHolder1.Controls)
        {
            if (item is BCS.WebApp.WebUserControls.WebUserControls_LOBListItem)
            {
                BCS.WebApp.WebUserControls.WebUserControls_LOBListItem listitem = item as BCS.WebApp.WebUserControls.WebUserControls_LOBListItem;
                if (excludeBlanks)
                {
                    if (!listitem.IsBlank())
                        sb.AppendFormat("{0}|", listitem.GetItemAsString());
                }
                else
                {
                    sb.AppendFormat("{0}|", listitem.GetItemAsString());
                }
            }
        }
        if (sb.Length > 0)
            if (sb[sb.Length-1] == '|')
                sb.Remove(sb.Length - 1, 1);
        return sb.ToString();
    }

    public SubmissionLineOfBusinessChild[] GetItems(bool excludeBlanks)
    {
        System.Collections.Generic.List<SubmissionLineOfBusinessChild> listItems = new System.Collections.Generic.List<SubmissionLineOfBusinessChild>();
        foreach (Control item in DynamicControlsPlaceHolder1.Controls)
        {
            if (item is BCS.WebApp.WebUserControls.WebUserControls_LOBListItem)
            {
                BCS.WebApp.WebUserControls.WebUserControls_LOBListItem listitem = item as BCS.WebApp.WebUserControls.WebUserControls_LOBListItem;
                if (excludeBlanks)
                {
                    if (!listitem.IsBlank())
                        listItems.Add(listitem.GetItem());
                }
                else
                {
                    listItems.Add(listitem.GetItem());
                }
            }
        }
        SubmissionLineOfBusinessChild[] retItems = new SubmissionLineOfBusinessChild[listItems.Count];
        listItems.CopyTo(retItems);
        return retItems;
    }
    public APSBO.SubmissionLineOfBusinessChild[] GetAPSItems(bool excludeBlanks)
    {
        System.Collections.Generic.List<APSBO.SubmissionLineOfBusinessChild> listItems = new System.Collections.Generic.List<APSBO.SubmissionLineOfBusinessChild>();
        foreach (Control item in DynamicControlsPlaceHolder1.Controls)
        {
            if (item is BCS.WebApp.WebUserControls.WebUserControls_LOBListItem)
            {
                BCS.WebApp.WebUserControls.WebUserControls_LOBListItem listitem = item as BCS.WebApp.WebUserControls.WebUserControls_LOBListItem;
                if (excludeBlanks)
                {
                    if (!listitem.IsBlank())
                        listItems.Add(listitem.GetAPSItem());
                }
                else
                {
                    listItems.Add(listitem.GetAPSItem());
                }
            }
        }
        APSBO.SubmissionLineOfBusinessChild[] retItems = new APSBO.SubmissionLineOfBusinessChild[listItems.Count];
        listItems.CopyTo(retItems);
        return retItems;
    }

    public void SetItems(SubmissionLineOfBusinessChild[] items, bool clear)
    {
        if (clear)
            DynamicControlsPlaceHolder1.Controls.Clear();
        if (null == items || 0 == items.Length)
        {
            // set empty data text
            AddEmptyItem();
            return;
        }
        foreach (SubmissionLineOfBusinessChild item in items)
        {
            BCS.WebApp.WebUserControls.WebUserControls_LOBListItem listitem = AddItem() as BCS.WebApp.WebUserControls.WebUserControls_LOBListItem;
            listitem.SetItem(item);
        }
    }
    public void SetItems(APSBO.SubmissionLineOfBusinessChild[] items, bool clear)
    {
        if (clear)
            DynamicControlsPlaceHolder1.Controls.Clear();

        if (null == items || 0 == items.Length)
        {
            // set empty data text
            AddEmptyItem();
            return;
        }
        foreach (APSBO.SubmissionLineOfBusinessChild item in items)
        {
            BCS.WebApp.WebUserControls.WebUserControls_LOBListItem listitem = AddItem() as BCS.WebApp.WebUserControls.WebUserControls_LOBListItem;
            listitem.SetItem(item);
        }
    }

    private void AddEmptyItem()
    {
        if (DynamicControlsPlaceHolder1.Controls.Count > 0)
            return;
        TableRow tr = new TableRow();
        DynamicControlsPlaceHolder1.Controls.Add(tr);
        
        Label l = new Label();        
        TableCell tc = new TableCell();
        tc.Controls.Add(l);
        tr.Cells.Add(tc);

        //l.CssClass = "Warning";
        l.Text = EmptyDataText;

        tc.ColumnSpan = 5;
    }


    public Control AddItem()
    {
        Control c = LoadControl("LOBListItem.ascx");
        c.ID = Guid.NewGuid().ToString().Replace("-", string.Empty);
        DynamicControlsPlaceHolder1.Controls.Add(c);
        if (IsFirstItem(c))
        {
            //c.FindControl("lblReqIndicator").Visible = true;
            (c.FindControl("rfvLOB") as WebControl).Enabled = true;
        }
        else
        {
            //(c.FindControl("rfvLOB") as WebControl).Enabled = false; 
        }
        if (TabIndex != 0)
            Common.SetTabOrder(c, TabIndex);
        return c;
    }

    public void ClearItems()
    {
        DynamicControlsPlaceHolder1.Controls.Clear();        
    }

    private bool IsFirstItem(Control c)
    {
        if (c.Parent.Controls.Count == 1)
        {
            if (c.Parent.Controls[0] is BCS.WebApp.WebUserControls.WebUserControls_LOBListItem)
                return true;
        }
        return false;
    }

    private bool IsAutoUpdateExcludedStatus(string p)
    {
        StringCollection excludedStatuses = DefaultValues.GetLOBGridExcludedUpdateStatuses(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        if (excludedStatuses.Contains(p))
            return true;

        return false;
    }

    /// <summary>
    /// focuses on the Line of business dropdown of the first row
    /// </summary>
    public void SetItemFocus()
    {
        foreach (Control item in DynamicControlsPlaceHolder1.Controls)
        {
            if (item is BCS.WebApp.WebUserControls.WebUserControls_LOBListItem)
            {
                BCS.WebApp.WebUserControls.WebUserControls_LOBListItem listitem = item as BCS.WebApp.WebUserControls.WebUserControls_LOBListItem;
                listitem.SetFocus();
                break;
            }
        }
    }

    public string EmptyDataText
    {
        set
        { ViewState["EmptyDataText"] = value; }
        get
        { return (string)ViewState["EmptyDataText"] ?? "No line of businesses exist."; }
    }

    public short TabIndex
    {
        set
        { ViewState["TabIndex"] = value; }
        get
        { return (short?)ViewState["TabIndex"] ?? 0; }
    }
    protected void ObjectDataSource1_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        BCS.Biz.CompanyNumberCodeCollection codes = e.ReturnValue as BCS.Biz.CompanyNumberCodeCollection;
        
        foreach (BCS.Biz.CompanyNumberCode code in codes)
        {
            // if both code and desc are present
            if (!string.IsNullOrEmpty(code.Code) && !string.IsNullOrEmpty(code.Description))
                code.Code = code.Code + " - " + code.Description;
            else
            {
                // if only description is present
                if (!string.IsNullOrEmpty(code.Description))
                {
                    code.Code = code.Description;
                }
            }
        }
    }
    protected void DropDownList1_InsertItem(object sender, EventArgs e)
    {
        DropDownList ddl = sender as DropDownList;
        if (ddl.Items.FindByValue("0") == null)
            ddl.Items.Insert(0, new ListItem("", "0"));
    }

    public void SetCodeTypeValues(SubmissionCodeType[] types)
    {
        if (types != null && types.Length > 0)
        {
            foreach (BCS.Core.Clearance.BizObjects.SubmissionCodeType codeType in types)
            {
                int i = codeType.CodeId;
                if (phCodeTypes.Controls.Count > 0)
                {
                    if (phCodeTypes.Controls[0] is HtmlTable)
                    {
                        HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                        foreach (HtmlTableRow row in table.Rows)
                        {
                            foreach (HtmlTableCell cell in row.Cells)
                            {
                                foreach (Control c in cell.Controls)
                                {
                                    if (c is DropDownList)
                                    {
                                        DropDownList ddl = c as DropDownList;
                                        ListItem li = ddl.Items.FindByValue(i.ToString());
                                        if (li != null)
                                        {
                                            ddl.ClearSelection();
                                            li.Selected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else // there might be some default values for these code types, clear out these values if we are loading a submission with no codes assigned.
        {
            if (phCodeTypes.Controls.Count > 0)
            {
                if (phCodeTypes.Controls[0] is HtmlTable)
                {
                    HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                    foreach (HtmlTableRow row in table.Rows)
                    {
                        foreach (HtmlTableCell cell in row.Cells)
                        {
                            foreach (Control c in cell.Controls)
                            {
                                if (c is DropDownList)
                                {
                                    DropDownList ddl = c as DropDownList;
                                    ddl.ClearSelection();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void SetCodeTypeValues(APSBO.SubmissionCodeType[] types)
    {
        if (types != null && types.Length > 0)
        {
            foreach (BCS.Core.Clearance.BizObjects.APS.SubmissionCodeType codeType in types)
            {
                int i = codeType.CodeId;
                if (phCodeTypes.Controls.Count > 0)
                {
                    if (phCodeTypes.Controls[0] is HtmlTable)
                    {
                        HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                        foreach (HtmlTableRow row in table.Rows)
                        {
                            foreach (HtmlTableCell cell in row.Cells)
                            {
                                foreach (Control c in cell.Controls)
                                {
                                    if (c is DropDownList)
                                    {
                                        DropDownList ddl = c as DropDownList;
                                        ListItem li = ddl.Items.FindByValue(i.ToString());
                                        if (li != null)
                                        {
                                            ddl.ClearSelection();
                                            li.Selected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else // there might be some default values for these code types, clear out these values if we are loading a submission with no codes assigned.
        {
            if (phCodeTypes.Controls.Count > 0)
            {
                if (phCodeTypes.Controls[0] is HtmlTable)
                {
                    HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                    foreach (HtmlTableRow row in table.Rows)
                    {
                        foreach (HtmlTableCell cell in row.Cells)
                        {
                            foreach (Control c in cell.Controls)
                            {
                                if (c is DropDownList)
                                {
                                    DropDownList ddl = c as DropDownList;
                                    ddl.ClearSelection();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void SetCodeTypeValues(int[] types)
    {
        if (types != null && types.Length > 0)
        {
            foreach (int i in types)
            {
                if (phCodeTypes.Controls.Count > 0)
                {
                    if (phCodeTypes.Controls[0] is HtmlTable)
                    {
                        HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                        foreach (HtmlTableRow row in table.Rows)
                        {
                            foreach (HtmlTableCell cell in row.Cells)
                            {
                                foreach (Control c in cell.Controls)
                                {
                                    if (c is DropDownList)
                                    {
                                        DropDownList ddl = c as DropDownList;
                                        ListItem li = ddl.Items.FindByValue(i.ToString());
                                        if (li != null)
                                        {
                                            ddl.ClearSelection();
                                            li.Selected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public string[] GetCodeTypeValues()
    {
        ArrayList alids = new ArrayList();
        if (phCodeTypes.Controls.Count > 0)
        {
            HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
            foreach (HtmlTableRow row in table.Rows)
            {
                foreach (HtmlTableCell cell in row.Cells)
                {
                    foreach (Control c in cell.Controls)
                    {
                        if (c is DropDownList)
                        {
                            DropDownList ddl = c as DropDownList;
                            if (ddl.SelectedIndex > 0)
                            {
                                alids.Add(ddl.SelectedValue);
                            }
                        }
                    }
                }
            }
        }
        //int[] codeids = GetCompanyNumerCodes(al, cnt.SelectedItem.Text);
        string[] codeids = new string[alids.Count];
        alids.CopyTo(codeids);

        return codeids;
    }
    public void LoadCodeTypes(CompanyNumberCodeTypeCollection types, DateTime date)
    {
        #region save selections to restore
        ArrayList alids = new ArrayList();
        if (phCodeTypes.Controls.Count > 0)
        {
            if (phCodeTypes.Controls[0] is HtmlTable)
            {
                HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                foreach (HtmlTableRow row in table.Rows)
                {
                    foreach (HtmlTableCell cell in row.Cells)
                    {
                        foreach (Control c in cell.Controls)
                        {
                            if (c is DropDownList)
                            {
                                DropDownList ddl = c as DropDownList;
                                if (ddl.SelectedIndex > 0)
                                {
                                    alids.Add(Common.GetSafeSelectedValue(ddl));
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion
        phCodeTypes.Controls.Clear();
        if (types != null && types.Count > 0)
        {
            HtmlTable table = new HtmlTable();
            table.ID = "tblLOBCodeTypes";
            //table.Border = 1;
            //table.BorderColor = "red";
            phCodeTypes.Controls.Add(table);
            foreach (CompanyNumberCodeType type in types)
            {
                CompanyNumberCodeCollection codes = type.CompanyNumberCodes;
                if (codes != null && codes.Count > 0)
                {
                    HtmlTableRow row = new HtmlTableRow();
                    table.Rows.Add(row);
                    HtmlTableCell cell = new HtmlTableCell();
                    row.Cells.Add(cell);
                    cell.InnerText = type.CodeName;

                    cell = new HtmlTableCell();
                    row.Cells.Add(cell);
                    DropDownList ddl = new DropDownList();
                    ddl.ID = type.CodeName + "ddl";
                    //cell.Controls.Add(ddl);
                    Common.AddControl(cell, ddl, false, true);
                    ListItemCollection lic = new ListItemCollection();
                    foreach (BCS.Biz.CompanyNumberCode code in codes)
                    {
                        if (code.ExpirationDate.IsNull || code.ExpirationDate > date)
                            lic.Add(new ListItem(string.Concat(code.Code, " - ", code.Description), code.Id.ToString()));
                    }
                    lic.Insert(0, new ListItem("", "0"));

                    ddl.Items.AddRange(Common.SortListItems(lic));
                    foreach (int var in alids)
                    {
                        if (var != 0)
                        {
                            ListItem li = ddl.Items.FindByValue(var.ToString());
                            if (null != li)
                            {
                                li.Selected = true;
                                alids.Remove(var);
                                break;
                            }
                        }
                    }
                    if (!type.DefaultCompanyNumberCodeId.IsNull)
                        Common.SetSafeSelectedValue(ddl, type.DefaultCompanyNumberCodeId);

                    // if only one real option, set it as default. #850                    
                    if (ddl.Items.Count == 2)
                        ddl.Items[1].Selected = true;

                    if (type.Required)
                    {
                        RFV rfv = new RFV();
                        Common.AddControl(cell, rfv, false, false);
                        rfv.Display = ValidatorDisplay.None;
                        rfv.ControlToValidate = ddl.ID;
                        rfv.InitialValue = "0";
                        rfv.SetFocusOnError = true;
                        rfv.Text = " *";
                        rfv.ErrorMessage = "&nbsp;";
                    }
                }
            }
            phCodeTypes.Visible = true;
            if (TabIndex != 0)
                Common.SetTabOrder(phCodeTypes, TabIndex);
        }
    }
    protected void ddlStatuses_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlReasons.StatusId = Convert.ToInt32(ddlStatuses.SelectedValue);
        ddlReasons.DataBind();
        SetFocus(ddlStatuses);
    }
    protected void ibUpdate_Click(object sender, ImageClickEventArgs e)
    {
        if (ddlStatuses.SelectedIndex > 0)
        {
            SetStatus(ddlStatuses.SelectedValue, ddlReasons.SelectedValue);
        }
    }
    protected void ddlStatuses_Init(object sender, EventArgs e)
    {
        DropDownList ddl = sender as DropDownList;
        ddl.Attributes.Add("onchange",
            string.Format("SetActive('{0}','{1}');",
            ddlStatuses.ClientID,
            ibUpdate.ClientID));
    }
    protected void ddlReasons_Init(object sender, EventArgs e)
    {

    }

    private void SetFocus(Control ctrl)
    {
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        if (null != scriptManager)
            scriptManager.SetFocus(ctrl);
        else
            ctrl.Focus();
    }
}
