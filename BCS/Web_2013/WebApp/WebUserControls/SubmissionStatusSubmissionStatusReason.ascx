﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubmissionStatusSubmissionStatusReason.ascx.cs" Inherits="WebUserControls_SubmissionStatusSubmissionStatusReason" %>
<%@ Register TagPrefix="UserControls" Assembly="App_Code" Namespace="BCS.WebApp.UserControls" %>

<script type="text/javascript">
    function SubmissionStatusChange() {

        if ($("#ddlSubmissionStatus").val() == $("#hdnSelectedStatus").val()) {
            return;
        }

        var statusId = $("#ddlSubmissionStatus").val();
        $("#hdnSelectedStatus").val(statusId);
        //Clear current values
        $("#ddlSubmissionStatusReason > option").remove();
        $("#hdnStatusReasonSelectedValue").val("");

        var rows = new Array();

        //Get all status reasons
        rows = $("#hdnStatusReason").val().split('*');

        //Set default option
        $("#ddlSubmissionStatusReason").append($("<option></option>").val("").html(""));

        // Add new values
        var columns = new Array();
        for (var i = 0; i < rows.length;i++ ) {
            columns = rows[i].split('|');

            if (columns[0] == statusId) {
                $("#ddlSubmissionStatusReason").append($("<option></option>").val(columns[1]).html(columns[2]));
            }
        }

    }

    function StatusReasonChange() {
        $("#hdnStatusReasonSelectedValue").val($("#ddlSubmissionStatusReason").val());
    }

</script>


    <tr> 
        <td align="right">
            Status:
        </td>
        <td>
            <ajx:ListSearchExtender ID="ListSearchExtender1" runat="server"  TargetControlID="ddlSubmissionStatus"></ajx:ListSearchExtender>
            <UserControls:SubmissionStatusDropDownList runat="server" onchange="SubmissionStatusChange()" ID="ddlSubmissionStatus" ClientIDMode="Static"></UserControls:SubmissionStatusDropDownList>
            <asp:HiddenField runat="server" id="hdnStatusReason" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hdnSubmissionStatusVisible" />
            <asp:HiddenField runat="server" ID="hdnSelectedStatus" ClientIDMode="Static" />
            <asp:LinkButton runat="server" ID="lnkUpdateStatusAndReasonTop" BorderColor="0" Text="(update status)" OnClick="UpdateStatusAndReason" Visible="false"></asp:LinkButton>            
        </td>
    </tr>

    <tr> 
        <td align="right">
            Reason:
        </td>
        <td>
            <ajx:ListSearchExtender ID="ListSearchExtender9" runat="server"  TargetControlID="ddlSubmissionStatusReason"></ajx:ListSearchExtender>
            <UserControls:SubmissionStatusReasonDropDownList onchange="StatusReasonChange()" runat="server" ID="ddlSubmissionStatusReason" ClientIDMode="Static"></UserControls:SubmissionStatusReasonDropDownList>
            <asp:HiddenField runat="server" id="hdnStatusReasonSelectedValue" ClientIDMode="Static" />
        </td>
    </tr>
    <tr>
        <td align="right" style="white-space:nowrap;">
            Status Date:
        </td>
        <td>
            <UserControls:DateBox ID="submissionStatusDateBox" runat="server"  CssClass="txt" Columns="10"  />
        </td>
    </tr>
    <tr>
        <td>
        
        </td>
        <td>
            <asp:LinkButton runat="server" ID="lnkUpdateStatusAndReason" BorderColor="0" Text="(update status)" OnClick="UpdateStatusAndReason"></asp:LinkButton>            
        </td>
    </tr>




