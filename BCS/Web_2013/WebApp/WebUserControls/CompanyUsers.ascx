<%@ Control CodeFile="~/WebUserControls/CompanyUsers.ascx.cs" Inherits="BCS.WebApp.WebUserControls.CompanyUsers" Language="C#" %>
<span class="heading">User Admin</span>
<table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">&nbsp;</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        <%--content--%>
        <div id="removeThisDIVjustaholder">
        
        <table>
            <tr>
                <td>User name</td>
                <td>
                    <asp:TextBox ID="txFilterUserName" runat="server"></asp:TextBox>
                </td>                
                <td>
                    <asp:Button ID="btnFilter" runat="server" Text="Filter" CausesValidation="false" />
                </td>
            </tr>
        </table>
    &nbsp;    
    <br />
    <br />
    <UserControls:Grid ID="gridUsers" runat="server" AllowPaging="True" EmptyDataText="No Users were found." AutoGenerateColumns="False" DataKeyNames="Id" CaptionAlign="Left" DataSourceID="odsUsers" OnSelectedIndexChanging="gridUsers_SelectedIndexChanging" OnPageIndexChanged="gridUsers_PageIndexChanged" OnRowDataBound="gridUsers_RowDataBound">
        <Columns>
            <asp:CommandField ButtonType="Button" ShowSelectButton="True" SelectText="Edit" ControlStyle-Width="40px" />
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
            <asp:BoundField DataField="Username" HeaderText="Username" />
            <asp:CheckBoxField DataField="Active" HeaderText="Active" />
            <asp:TemplateField HeaderText="Roles">                
                <ItemTemplate>
                    <asp:Label ID="lblUserRoles" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Notes" HeaderText="Notes" />
            
        </Columns>
    </UserControls:Grid>
    <asp:ObjectDataSource ID="odsUsers" runat="server" SelectMethod="GetUsers"
        TypeName="BCS.Core.Clearance.Admin.DataAccess" OnSelected="odsUsers_Selected">
        <SelectParameters>
            <asp:ControlParameter ControlID="txFilterUserName" DefaultValue="" Name="filterUserName" PropertyName="Text"
                Type="String" />
            <asp:SessionParameter DefaultValue="0" Name="companyId" SessionField="CompanySelected"
                Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>   
    <br />
    <asp:Button ID="btnAdd" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.NonAgency) %>' runat="server" Text="Add"  OnClick="btnAdd_Click" />
    <br />
    <br />
    
    
    <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
    <br />
</asp:PlaceHolder>
<br />    
    <asp:DetailsView ID="DetailsView1" runat="server" DataSourceID="odsUser"
        CaptionAlign="Left" AutoGenerateRows="False" DataKeyNames="Id"
        OnItemInserted="DetailsView1_ItemInserted" OnItemUpdated="DetailsView1_ItemUpdated" OnItemUpdating="DetailsView1_ItemUpdating"
        OnModeChanging="DetailsView1_ModeChanging" OnItemInserting="DetailsView1_ItemInserting" OnDataBound="DetailsView1_DataBound" Width="50%"  >
        <FieldHeaderStyle Width="10%" />
        <RowStyle Width="90%" />
        <EmptyDataRowStyle Width="90%" />
        <CommandRowStyle BorderColor="White" />
        <Fields>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" Visible="False" />
            <asp:BoundField DataField="Username" HeaderText="Username" SortExpression="Username" />
            <asp:TemplateField HeaderText="Active">
                <EditItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Active") %>' />
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Active") %>' />
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Active") %>' Enabled="false" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Roles">
                <EditItemTemplate>
                    <UserControls:RoleListBox ID="RoleListBox1" runat="server" Rows="10" OnDataBound="FilterOutRoles" >
                    </UserControls:RoleListBox>
                </EditItemTemplate>
                <InsertItemTemplate>                    
                    <UserControls:RoleListBox ID="RoleListBox1" runat="server" Rows="10" OnDataBound="FilterOutRoles" >
                     </UserControls:RoleListBox>
                </InsertItemTemplate>
                <ItemTemplate>                    
                    <UserControls:RoleListBox ID="RoleListBox1" runat="server" Rows="10" OnDataBound="FilterOutRoles" >
                     </UserControls:RoleListBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Agency">
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlAgencies" runat="server" OnInit="LoadAgencies" AppendDataBoundItems="True">
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                    </asp:DropDownList>
                    <%--<aps:AgencyDropDownList ID="ddlApsAgencies" runat="server" ItemAsEmpty="True" ActiveOnly="False"
                        DisplayFormat="{0} ({1})" CompanyId='<%$ Code : Common.GetSafeIntFromSession(SessionKeys.CompanyId) %>' OnInit="LoadAPSAgencies" >
                    </aps:AgencyDropDownList>--%>
                    <br />
                    <asp:Label ID="lblAgencyComment" runat="server" Visible="False" CssClass="ImpInfo"></asp:Label>
                </EditItemTemplate>
                <InsertItemTemplate>                    
                    <asp:DropDownList ID="ddlAgencies" runat="server" SelectedValue='<%# Bind("AgencyId") %>' OnInit="LoadAgencies" AppendDataBoundItems="True">
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                    </asp:DropDownList>
                    <%--<aps:AgencyDropDownList ID="ddlApsAgencies" runat="server" SelectedValue='<%# Bind("AgencyId") %>' ItemAsEmpty="True" ActiveOnly="False"
                        DisplayFormat="{0} ({1})" CompanyId='<%$ Code : Common.GetSafeIntFromSession(SessionKeys.CompanyId) %>' OnInit="LoadAPSAgencies" >
                    </aps:AgencyDropDownList>--%>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblAgencyIdItem" runat="server" Text='<%# Bind("AgencyId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Agency Segmentation">
                <EditItemTemplate>
                    <asp:TextBox ID="txtAgencySegmentation" runat="server" Text='<%#Bind("Segmentation")%>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtAgencySegmentation" runat="server" Text='<%#Bind("Segmentation")%>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:TextBox ID="txtAgencySegmentation" runat="server" Text='<%#Bind("Segmentation")%>' Enabled="false"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Notes" SortExpression="Notes">
                <EditItemTemplate>
                    <asp:TextBox ID="txNotes" runat="server" TextMode="MultiLine" Text='<%# Bind("Notes") %>' Rows="10" Width="99%"></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txNotes" runat="server" TextMode="MultiLine" Text='<%# Bind("Notes") %>' Rows="10" Width="99%"></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:TextBox ID="txNotes" runat="server" TextMode="MultiLine" Text='<%# Bind("Notes") %>' Enabled="False" Rows="10" Width="99%"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ButtonType="Button" ShowEditButton="True" ShowInsertButton="True"/>            
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="odsUser" runat="server" SelectMethod="GetUser" TypeName="BCS.Core.Clearance.Admin.DataAccess" 
        InsertMethod="AddUser" DataObjectTypeName="BCS.Core.Clearance.BizObjects.User" 
        OldValuesParameterFormatString="original_{0}" OnInserted="odsUser_Inserted" OnInserting="odsUser_Inserting" UpdateMethod="UpdateUser" OnUpdated="odsUser_Updated" OnUpdating="odsUser_Updating" >
        <SelectParameters>
            <asp:ControlParameter ControlID="gridUsers" Name="id" PropertyName="SelectedValue"
                Type="Int32"  />
        </SelectParameters>
    </asp:ObjectDataSource>
    
    </div>
        <%--end content--%>
        </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>