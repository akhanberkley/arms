using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.Components;
using System.ComponentModel;
using System.Text;
using BCS.DAL;
using System.Linq;

[DefaultProperty("Text")]
[ControlValueProperty("Text", "")]
public partial class WebUserControls_AutoNumberGenerator : System.Web.UI.UserControl
{
    #region Properties

    public string TextBoxCssClass
    {
        get
        { return TextBoxPolicyNumber.CssClass; }
        set
        { TextBoxPolicyNumber.CssClass = value; }
    }
    /// <summary>
    /// sets the generated value to an additional control, currently only works for ITextControl
    /// </summary>
    public string AdditionalControlId
    {
        get
        { return (string)ViewState["AdditionalControl"]; }
        set
        { ViewState["AdditionalControl"] = value; }
    }
    public int CompanyNumberId
    {
        get
        { return (int?)ViewState["CompanyNumberId"] ?? 0; }
        set
        { ViewState["CompanyNumberId"] = value; }
    }
    public string NumberControlTypeName
    {
        get
        { return (string)ViewState["NumberControlTypeName"] ?? string.Empty; }
        set
        { ViewState["NumberControlTypeName"] = value; }
    }
    public string ReadableText
    {
        get
        { return (string)ViewState["ReadableText"] ?? "number"; }
        set
        { ViewState["ReadableText"] = value; }
    }
    [DefaultValue("")]
    public string Text
    {
        get
        { return TextBoxPolicyNumber.Text; }
        set
        { TextBoxPolicyNumber.Text = value; }
    }
    public bool Required
    {
        get
        { return rfvAutoNumber.Enabled; }
        set
        { rfvAutoNumber.Enabled = value; }
    }
    public bool ReadOnly
    {
        get
        { return TextBoxPolicyNumber.ReadOnly; }
        set
        { TextBoxPolicyNumber.ReadOnly = value; }
    }
    public bool FreehandOnly
    {
        get
        { return tdGenerator.Visible; }
        set
        { tdGenerator.Visible = !value; }
    }
    public bool ShowStaticAsterisk
    {
        get
        { return rfvAutoNumber.ShowStaticAsterisk; }
        set
        { rfvAutoNumber.ShowStaticAsterisk = value; }
    }
    public bool Enabled
    {
        set
        {
            SetEnabled(this.Controls, value);
        }
    }

    private void SetEnabled(ControlCollection controlCollection, bool value)
    {
        foreach (Control c in controlCollection)
        {
            if (c is WebControl)
            {
                (c as WebControl).Enabled = value;
            }
            if (c.HasControls())
                SetEnabled(c.Controls, value);
        }
    }
    public bool Generated
    {
        get
        { return (bool?)ViewState["Generated"] ?? false; }
    }

    public Generatables GeneratedType
    {
        get
        {
            return (Generatables)ViewState["GeneratedType"];
        }
        set
        {
            ViewState["GeneratedType"] = value;
        }
    }

    public bool AutoPostBack
    {
        set
        {
            TextBoxPolicyNumber.AutoPostBack = value;
        }
    }
    #endregion

    protected override void OnInit(EventArgs e)
    {
        rfvAutoNumber.ControlToValidate = this.TextBoxPolicyNumber.ID;
        base.OnInit(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        SetPolicyValidation();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        BCS.Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
        dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumber.Columns.Id, CompanyNumberId);
        BCS.Biz.CompanyNumber cn = dm.GetCompanyNumber();

        if (cn != null)
        {
            LookupProxy.Lookup lookup = new LookupProxy.Lookup();
            string nextNumber = lookup.GetNextNumberControlValueByCompanyNumberAndTypeName(cn.PropertyCompanyNumber, NumberControlTypeName, 0);
            TextBoxPolicyNumber.Text = nextNumber.ToString();
            #region addl control processing
            if (AdditionalControlId != null)
            {
                Control c = Common.FindControlRecursive(Page, AdditionalControlId);
                if (c != null)
                {
                    if (c is ITextControl)
                    {
                        (c as ITextControl).Text = TextBoxPolicyNumber.Text;
                    }
                }
            }
            #endregion
            ViewState["Generated"] = true;
        }
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        if (null != scriptManager)
            scriptManager.SetFocus(sender as Control);
        else
            (sender as Control).Focus();

        if (TextBoxPolicyNumber.AutoPostBack)
            TextBoxPolicyNumber_OnTextChanged(null, new EventArgs());
    }
    protected void ImageButton1_Init(object sender, EventArgs e)
    {
        WebControl wc = sender as WebControl;
        wc.Visible = Security.CanUserGenerate(GeneratedType);
        wc.Attributes.Add("onclick", string.Format("confirmOverwrite(event, '{0}', '{1}');", TextBoxPolicyNumber.ClientID,
            string.Format("Are you sure you would like to generate another {0}?", ReadableText)));

        img1.Alt = string.Format("Click to generate a new {0}", ReadableText);
    }

    protected override void Render(HtmlTextWriter writer)
    {
        base.Render(writer);

        StringBuilder script = new StringBuilder();
        //function confirmOverwrite
        script.Append("function confirmOverwrite(scwEvt , controlToCheck, confirmationMessage)");
        script.Append("{");
        script.Append("if ($get(controlToCheck).value.length != 0)");
        script.Append("{");
        script.Append("if ( ! confirm(confirmationMessage) )");
        script.Append("{");
        script.Append("cancelEvent(scwEvt);");
        script.Append("}");
        script.Append("}");
        script.Append("}");

        //function fireClickOnSpace
        script.Append(" function fireClickOnSpace(element, evt)");
        script.Append(" {");
        script.Append(" if(evt.keyCode === Sys.UI.Key.space)");
        script.Append("    {");
        script.Append("/*       element.click(); // only works in IE */       ");
        script.Append("        f = new Function(element.href);");
        script.Append("        f();");
        script.Append("    }");
        script.Append("}");

        //function preventScrollOnSpace
        script.Append("function preventScrollOnSpace(evt)");
        script.Append("{");
        script.Append("    if(evt.keyCode === Sys.UI.Key.space)");
        script.Append("    {");
        script.Append("        cancelEvent(evt);");
        script.Append("    }");
        script.Append("}");

        ScriptManager sm = ScriptManager.GetCurrent(Page);
        if (sm != null)
        {
            ScriptManager.RegisterStartupScript(this, typeof(string), UniqueID, script.ToString(), true);
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(typeof(string), UniqueID, script.ToString(), true);
        }
    }

    protected void TextBoxPolicyNumber_OnTextChanged(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(TextBoxPolicyNumber.Text))
            return;

        int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        int submissionId = Common.GetSafeIntFromSession("SubmissionId");

        using (BCSContext bc = DataAccess.NewBCSContext())
        {
            lbPolicyWarning.Visible = bc.Submission.Where(s => s.PolicyNumber == TextBoxPolicyNumber.Text 
                && s.CompanyNumberId == companyNumberId
                && s.Id != submissionId).Count() > 0;
        }
    }

    public void PolicyTextBoxOnTextChangedAdd(EventHandler e)
    {
        TextBoxPolicyNumber.TextChanged += e;
    }

    public void PolicyTextBoxOnTextChangedRemove(EventHandler e)
    {
        TextBoxPolicyNumber.TextChanged -= e;
    }

    private void SetPolicyValidation()
    {
        string policyValidation = (string)Cache[string.Format("PolicyValidation|{0}", Common.GetSafeIntFromSession(SessionKeys.CompanyId))];
        if (string.IsNullOrEmpty(policyValidation))
        {
            BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyParameter.Columns.CompanyID, Common.GetSafeIntFromSession(SessionKeys.CompanyId));

            BCS.Biz.CompanyParameter cp = dm.GetCompanyParameter();

            policyValidation = cp.PolicyValidation.ToString();

            Cache[string.Format("PolicyValidation|{0}", Common.GetSafeIntFromSession(SessionKeys.CompanyId))] = policyValidation;
        }

        bool applyValidation;
        bool.TryParse(policyValidation, out applyValidation);

        if (applyValidation)
        {
            PolicyTextBoxOnTextChangedAdd(TextBoxPolicyNumber_OnTextChanged);
            TextBoxPolicyNumber.AutoPostBack = true;
        }
        else
        {
            PolicyTextBoxOnTextChangedRemove(TextBoxPolicyNumber_OnTextChanged);
            TextBoxPolicyNumber.AutoPostBack = false;
        }
    }
}
