<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LOBList.ascx.cs" Inherits="WebUserControls_LOBList" %>
<%@ Register Src="~/WebUserControls/LOBListItem.ascx" TagName="LOBListItem" TagPrefix="uc1" %>
<script type="text/javascript">
//document.onreadystatechange =new function(){alert(document.readyState);};
       
       function SetActive(verifyControlId, targetControlId)
       {
       
        var activate = false;
       
        if($get(verifyControlId) && $get(verifyControlId).tagName === 'SELECT')
        {
            $get(verifyControlId).value != 0 ? activate = true : activate = false;            
        }
        else
        {
            $get(verifyControlId).value.length > 0 ? activate = true : activate = false;
        }
        //$get(targetControlId).disabled = !activate;       
        if (activate)
            $get(targetControlId).removeAttribute('disabled',1);
        else
            $get(targetControlId).disabled = 'disabled';
       }
</script>
<asp:HiddenField ID="hdnExcludedStatusesBasedOn" runat="server" Value="Value" />
<table border="1" cellpadding="3" style="border-collapse:collapse;" cellspacing="3">
    <tr>
        <td colspan="4">
            <b>Line Of Business</b>
        </td>
    </tr>
    <tr>
        <td colspan="4">
             <asp:PlaceHolder ID="phCodeTypes" runat="server">
             </asp:PlaceHolder>            
        </td>
    </tr>    
    <tr>
        <td>
            LOB
        </td>
        <td>
           Status
        </td>
        <td>
            <span> Status Reason </span>
        </td>
        <%--<td>
            Premium
        </td>--%>
        <td>
            <asp:ImageButton ID="ibAdd" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("plus.gif") %>' OnClick="ibAdd_Click" CausesValidation="False" TabIndex="500"
            AlternateText="Click this icon to add a line of business" ToolTip="Click this icon to add a line of business" />
        </td>
    </tr>

    <UserControls:DynamicControlsPlaceHolder ID="DynamicControlsPlaceHolder1" runat="server" ControlsWithoutIDs="persist" >
    </UserControls:DynamicControlsPlaceHolder>
    
    <tr>
        <td colspan="4">
            <asp:CustomValidator ID="LOBListValidator" runat="server" ErrorMessage="&nbsp;"
             Text="Each Line of Business must have a LOB, a Reason if the status is 'Closed' or 'Declined'" Display="Dynamic"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td>  <b> New Status & Reason</b> </td>
        <td>
            <asp:DropDownList ID="ddlStatuses" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStatuses_SelectedIndexChanged" OnInit="ddlStatuses_Init">
            </asp:DropDownList>
        </td>
        <td>
            <UserControls:SubmissionStatusReasonDropDownList ID="ddlReasons" runat="server" OnInit="ddlReasons_Init" >
            </UserControls:SubmissionStatusReasonDropDownList>
        </td>
        <td>
            <asp:ImageButton ID="ibUpdate" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("reload.gif") %>' CausesValidation="False" OnClick="ibUpdate_Click"
            AlternateText="Click this icon to update statuses" ToolTip="Click this icon to update statuses" />
        </td>
    </tr>    
</table>