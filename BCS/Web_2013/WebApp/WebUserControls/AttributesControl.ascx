<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttributesControl.ascx.cs" Inherits="AttributesControl"  %>
    <aje:ScriptManager id="ScriptManager1" runat="server"></aje:ScriptManager>
<span class="heading">Attributes</span>

   <table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">&nbsp;</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
<UserControls:Grid ID="gridAttributes" runat="server" DataSourceID="odsAttributes" EmptyDataText="No Attributes were found." AutoGenerateColumns="False" DataKeyNames="id" OnPageIndexChanged="gridAttributes_PageIndexChanged" OnRowDeleted="gridAttributes_RowDeleted" OnSelectedIndexChanging="gridAttributes_SelectedIndexChanging">
    <Columns>
        <asp:CommandField ButtonType="Button" ShowSelectButton="True" SelectText="Edit" ControlStyle-Width="40px" />
        <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
        <asp:BoundField DataField="Label" HeaderText="Label" />
        <asp:TemplateField HeaderText="Attribute Data Type" Visible="False">
            <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "AttributeDataType.DataTypeName") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Company Number">            
            <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CompanyNumber.PropertyCompanyNumber") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:CheckBoxField DataField="ClientSpecific" HeaderText="ClientSpecific" ReadOnly="True" />
        <asp:CheckBoxField DataField="Required" HeaderText="Required" ReadOnly="True" />
        <asp:TemplateField ShowHeader="False">
            <ItemTemplate>
                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                     Text="Delete" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() %>'/>
                <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="btnDelete"
                    ConfirmText='<%# DataBinder.Eval (Container.DataItem, "Label", "Are you sure you want to delete this Attribute \"{0}\"?") %>' />
            </ItemTemplate>
         </asp:TemplateField>
    </Columns>
    </UserControls:Grid>
    <asp:ObjectDataSource ID="odsAttributes" runat="server" SelectMethod="GetCompanyNumberAttributes"
        TypeName="BCS.Core.Clearance.Admin.DataAccess" DeleteMethod="DeleteCompanyNumberAttribute" OnDeleted="ObjectDataSource1_Deleted">
        <SelectParameters>
            <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected"
                Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
    <br />
    <asp:Button ID="btnAdd" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() %>' runat="server" Text="Add"  OnClick="btnAdd_Click" />
    <br />
    <br />
    <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
        <br />
    </asp:PlaceHolder>
    <asp:DetailsView ID="DetailsView1" runat="server"
        AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="ObjectDataSource2" Height="50px"
        OnItemInserted="DetailsView1_ItemInserted" OnItemUpdated="DetailsView1_ItemUpdated" OnItemUpdating="DetailsView1_ItemUpdating" OnModeChanging="DetailsView1_ModeChanging">
        <Fields>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
            <asp:TemplateField HeaderText="Company Number">
                <EditItemTemplate>
                    <asp:DropDownList ID="ddlCompanyNumbers" runat="server" DataSourceID="odsCompanyNumbers" DataTextField="PropertyCompanyNumber" DataValueField="Id" SelectedValue='<%# Bind("CompanyNumberId") %>' OnPreRender="Sort_PreRender">
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="odsCompanyNumbers" runat="server" SelectMethod="GetCompanyNumbers"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                            <asp:Parameter DefaultValue="True" Type="boolean" Name="companyDependent" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlCompanyNumbers" runat="server" DataSourceID="odsCompanyNumbers" DataTextField="PropertyCompanyNumber" DataValueField="Id" SelectedValue='<%# Bind("CompanyNumberId") %>' OnPreRender="Sort_PreRender">
                    </asp:DropDownList><asp:ObjectDataSource ID="odsCompanyNumbers" runat="server" SelectMethod="GetCompanyNumbers"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                            <asp:Parameter DefaultValue="True" Type="boolean" Name="companyDependent" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </InsertItemTemplate>               
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CompanyNumber.PropertyCompanyNumber") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Attribute Data Type">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="ObjectDataSource1"
                        DataTextField="DataTypeName" DataValueField="Id" SelectedValue='<%# Bind("AttributeDataTypeId") %>' OnPreRender="Sort_PreRender">
                    </asp:DropDownList><asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAttributeDataTypes"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess"></asp:ObjectDataSource>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="ObjectDataSource1"
                        DataTextField="DataTypeName" DataValueField="Id" SelectedValue='<%# Bind("AttributeDataTypeId") %>' OnPreRender="Sort_PreRender">
                    </asp:DropDownList><asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAttributeDataTypes"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess"></asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "AttributeDataType.DataTypeName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Label">
                <EditItemTemplate>
                    <asp:TextBox ID="txtLabel" runat="server" Text='<%# Bind("Label") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvLabel" runat="server" ControlToValidate="txtLabel"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*" ></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtLabel" runat="server" Text='<%# Bind("Label") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvLabel" runat="server" ControlToValidate="txtLabel"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblLabel" runat="server" Text='<%# Bind("Label") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CheckBoxField DataField="ClientSpecific" HeaderText="ClientSpecific" />
            <asp:CheckBoxField DataField="Required" HeaderText="Required" />
            <asp:CheckBoxField DataField="ReadOnlyProperty" HeaderText="Read Only" />
            <asp:TemplateField HeaderText="Display Order">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("DisplayOrder") %>'></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TextBox1"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* !!" OnInit="SetRange" Type="Integer"></asp:RangeValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("DisplayOrder") %>'></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="TextBox1"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* !!" OnInit="SetRange" SetFocusOnError="True"
                        Type="Integer"></asp:RangeValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("DisplayOrder") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="UIAttributes" HeaderText="UI Attributes" />
            <asp:CommandField ButtonType="Button" ShowEditButton="True" ShowInsertButton="True" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetCompanyNumberAttribute"
        TypeName="BCS.Core.Clearance.Admin.DataAccess" InsertMethod="AddCompanyNumberAttribute" UpdateMethod="UpdateCompanyNumberAttribute" OnInserted="ObjectDataSource2_Inserted" OnUpdated="ObjectDataSource2_Updated">
        <SelectParameters>
            <asp:ControlParameter ControlID="gridAttributes" Name="id" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="companyNumberId" Type="Int32" />
            <asp:Parameter Name="attributeDataTypeId" Type="Int32" />
            <asp:Parameter Name="label" Type="String" />
            <asp:Parameter Name="clientspecific" Type="Boolean" />            
            <asp:Parameter Name="readOnlyProperty" Type="Boolean" />
            <asp:Parameter Name="displayOrder" Type="Int16" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="id" Type="Int32" />
            <asp:Parameter Name="companyNumberId" Type="Int32" />
            <asp:Parameter Name="attributeDataTypeId" Type="Int32" />
            <asp:Parameter Name="label" Type="String" />
            <asp:Parameter Name="clientspecific" Type="Boolean" />
            <asp:Parameter Name="required" Type="Boolean" />
            <asp:Parameter Name="readOnlyProperty" Type="Boolean" />
            <asp:Parameter Name="displayOrder" Type="Int16" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    
</td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
