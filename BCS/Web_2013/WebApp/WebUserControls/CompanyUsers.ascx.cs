using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.Components;
using BCS.WebApp.UserControls;
using BCS.Biz;
using System.Collections.Generic;

namespace BCS.WebApp.WebUserControls
{
    public partial class CompanyUsers : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (Common.GetSafeIntFromSession(SessionKeys.CompanyId) == 0)
            {
                Common.SetStatus(messagePlaceHolder, "Please select a company before adding a user.");
                return;
            }
            gridUsers.SelectedIndex = -1;
            DetailsView1.ChangeMode(DetailsViewMode.Insert);
        }

        protected void gridUsers_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            if (!BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.NonAgency))
                DetailsView1.ChangeMode(DetailsViewMode.Edit);
        }
        protected void gridUsers_PageIndexChanged(object sender, EventArgs e)
        {
            gridUsers.SelectedIndex = -1;
        }
        protected void DetailsView1_DataBound(object sender, EventArgs e)
        {
            if (DetailsView1.DataItem != null)
            {
                #region Load Roles
                RoleListBox rlb = DetailsView1.FindControl("RoleListBox1") as RoleListBox;
                foreach (BCS.Biz.UserRole aUserRole in (DetailsView1.DataItem as BCS.Biz.User).UserRoles)
                {
                    rlb.Items.FindByValue(aUserRole.RoldId.ToString()).Selected = true;
                }
                ViewState["OriginalRolesList"] = ToRoleList(Common.GetSelectedValues(rlb));
                #endregion

                #region Set Agency
                BCS.Biz.User thisUser = DetailsView1.DataItem as BCS.Biz.User;
                ViewState["OriginalAgency"] = thisUser.AgencyId.IsNull ? "0" : thisUser.AgencyId.Value.ToString();

                //DropDownList ddlAgencies = ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) ?
                //DetailsView1.FindControl("ddlAPSAgencies") as DropDownList : DetailsView1.FindControl("ddlAgencies") as DropDownList;

                DropDownList ddlAgencies = DetailsView1.FindControl("ddlAgencies") as DropDownList;

                if (ddlAgencies != null)
                {
                    ListItem li = ddlAgencies.Items[0];
                    if (!thisUser.AgencyId.IsNull)
                        li = ddlAgencies.Items.FindByValue(thisUser.AgencyId.ToString());
                    if (null != li)
                    {
                        ddlAgencies.ClearSelection();
                        li.Selected = true;
                    }
                    else
                    {
                        Label labelAgencyComment = DetailsView1.FindControl("lblAgencyComment") as Label;
                        labelAgencyComment.Visible = true;
                        labelAgencyComment.Text = "The agency associated with this user does not belong to the company.";
                    }
                }
                #endregion
            }
            if (DetailsView1.CurrentMode == DetailsViewMode.Insert)
            {
                (DetailsView1.FindControl("CheckBox1") as CheckBox).Checked = true;
            }
        }
        protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
        {
            gridUsers.DataBind();
            DetailsView1.DataBind();
        }
        protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
        {
            gridUsers.DataBind();
            DetailsView1.DataBind();
        }
        protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {
            e.Values.Add("RoleList", ToRoleList(Common.GetSelectedValues(DetailsView1.FindControl("RoleListBox1") as RoleListBox)));
        }
        protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            // add an input parameter, since we are not default binding the agency on edits because of users agency might belong to a different company
            //DropDownList ddlAgencies = ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) ?
                //DetailsView1.FindControl("ddlAPSAgencies") as DropDownList : DetailsView1.FindControl("ddlAgencies") as DropDownList;

            DropDownList ddlAgencies = DetailsView1.FindControl("ddlAgencies") as DropDownList;

            e.OldValues.Add("AgencyId", ViewState["OriginalAgency"].ToString());
            e.NewValues.Add("AgencyId", string.IsNullOrEmpty(ddlAgencies.SelectedValue) ? "0" : ddlAgencies.SelectedValue);


            e.OldValues.Add("RoleList", (BCS.Core.Clearance.BizObjects.RoleList)ViewState["OriginalRolesList"]);
            e.NewValues.Add("RoleList", ToRoleList(Common.GetSelectedValues(DetailsView1.FindControl("RoleListBox1") as RoleListBox)));

            bool b = Common.ShouldUpdate(e);
            e.Cancel = b;

            if (b)
            {
                // there was nothing to update. Just show the user the success message.
                Common.SetStatus(messagePlaceHolder, "User Sucessfully updated.");
                DetailsView1.DataBind();
            }
            else
            {
                Page.Cache.Remove(string.Format(CacheKeys.UserAgency, e.OldValues["Username"]));
                Page.Cache.Remove(string.Format(CacheKeys.UserCompanies, e.OldValues["Username"]));
                Page.Cache.Remove(string.Format(CacheKeys.UserRoles, e.OldValues["Username"]));
            }
        }
        protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
        {
            e.Cancel = BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.NonAgency);
        }
        protected void odsUser_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            (e.InputParameters[0] as BCS.Core.Clearance.BizObjects.User).PrimaryCompanyId = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
        }
        protected void odsUser_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            (e.InputParameters[0] as BCS.Core.Clearance.BizObjects.User).PrimaryCompanyId = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
        }
        protected void odsUser_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null && e.ReturnValue != null)
            {
                Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
                // remove the cached agencies 
                // TODO: why was  this here?
                //Cache.Remove(string.Format(CacheKeys.CompanyNumberAgencies, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId)));
            }
        }
        protected void odsUser_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null && e.ReturnValue != null)
            {
                Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
                // remove the cached agencies
                // TODO: why was  this here?
                Cache.Remove(string.Format(CacheKeys.CompanyNumberAgencies, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId)));
            }
        }
        private BCS.Core.Clearance.BizObjects.RoleList ToRoleList(string[] roleids)
        {
            BCS.Core.Clearance.BizObjects.RoleList roleList = new BCS.Core.Clearance.BizObjects.RoleList();
            roleList.List = new BCS.Core.Clearance.BizObjects.Role[roleids.Length];
            for (int i = 0; i < roleids.Length; i++)
            {
                BCS.Core.Clearance.BizObjects.Role role = new BCS.Core.Clearance.BizObjects.Role();
                role.Id = Convert.ToInt32(roleids[i]);
                roleList.List[i] = role;
            }
            return roleList;
        }
        protected void LoadAgencies(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            //if (!ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
            //{
                DataManager dm = new DataManager(DefaultValues.DSN);
                dm.QueryCriteria.And(JoinPath.Agency.CompanyNumber.Company.Columns.Id, Common.GetSafeIntFromSession(SessionKeys.CompanyId));
                AgencyCollection agencies = dm.GetAgencyCollection();
                agencies = agencies.SortByAgencyName(OrmLib.SortDirection.Ascending);
                ddl.DataTextField = "AgencyName";
                ddl.DataValueField = "Id";
                ddl.DataSource = agencies;
            //}
            //else
            //{
                //ddl.Visible = false;
            //}
        }
        protected void LoadAPSAgencies(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;

            // just hide it (data bind is taked care of by detailsview, unlike non-aps dropdown list)
            ddl.Visible = ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        }
        protected void gridUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblUserRoles = e.Row.FindControl("lblUserRoles") as Label;
                User aUser = e.Row.DataItem as User;
                List<string> userRoleList = new List<string>();
                RoleCollection allRoles = Security.GetAllRoles();
                foreach (UserRole var in aUser.UserRoles)
                {
                    userRoleList.Add(allRoles.FindById(var.RoldId).RoleName);
                }
                string[] userRoleArray = userRoleList.ToArray();
                lblUserRoles.Text = string.Join(", ", userRoleArray);
            }
        }
        protected void FilterOutRoles(object sender, EventArgs e)
        {
            ListControl ddl = sender as ListControl;
            string[] filteredRoles = new string[] { "System Administrator" };
            for (int i = 0; i < filteredRoles.Length; i++)
            {
                ListItem li = ddl.Items.FindByText(filteredRoles[i]);
                if (li != null)
                    ddl.Items.Remove(li);
            }
        }
        protected void odsUsers_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            #region filter out users with particular roles. currently only sys admin.
            RoleCollection allRoles = Security.GetAllRoles();
            string[] filteredUserRoles = new string[] { "System Administrator" };
            int[] filteredRoleIds = new int[filteredUserRoles.Length];
            for (int i = 0; i < filteredUserRoles.Length; i++)
            {
                filteredRoleIds[i] = allRoles.FindByRoleName(filteredUserRoles[i]).Id;
            }

            UserCollection users = e.ReturnValue as UserCollection;
            List<User> removableUserList = new List<User>();
            foreach (User user in users)
            {
                foreach (UserRole userRole in user.UserRoles)
                {
                    if (Array.BinarySearch(filteredRoleIds, userRole.RoldId) > -1)
                    {
                        removableUserList.Add(user);
                        break;
                    }
                }
            }
            foreach (User var in removableUserList)
            {
                users.Remove(var);
            }
            #endregion
        }
    } 
}
