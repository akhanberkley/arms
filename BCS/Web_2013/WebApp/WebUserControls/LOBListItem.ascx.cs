using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using BCS.WebApp.Components;
using APSBO = BCS.Core.Clearance.BizObjects.APS;

namespace BCS.WebApp.WebUserControls
{
    public partial class WebUserControls_LOBListItem : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            LineOfBusinessChildrenDropDownList1.DataBindByCompanyNumber(Common.GetSafeIntFromSession("CompanyNumberSelected"));
            SubmissionStatusDropDownList1.DataBind();
            SubmissionStatusReasonDropDownList1.StatusId = Convert.ToInt32(SubmissionStatusDropDownList1.SelectedValue);
            SubmissionStatusReasonDropDownList1.DataBind();

            SetFocus(LineOfBusinessChildrenDropDownList1);
        }

        public void SetFocus(Control ctrl)
        {
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            if (null != scriptManager)
                scriptManager.SetFocus(ctrl);
            else
                ctrl.Focus();
        }

        public void SetFocus()
        {
            SetFocus(LineOfBusinessChildrenDropDownList1);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void SubmissionStatusDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SubmissionStatusReasonDropDownList1.StatusId = Convert.ToInt32(SubmissionStatusDropDownList1.SelectedValue);
            SubmissionStatusReasonDropDownList1.DataBind();
            SetFocus(SubmissionStatusDropDownList1);
        }
        protected void ibRemove_Click(object sender, ImageClickEventArgs e)
        {
            Visible = false;
            if (this.Id == 0)
            {
                Parent.Controls.Remove(Parent.FindControl(this.ID));
            }
        }
        protected void SubmissionStatusDropDownList1_Init(object sender, EventArgs e)
        {
            SubmissionStatusDropDownList1.CompanyNumberId = Common.GetSafeIntFromSession("CompanyNumberSelected");
        }

        public void SetStatus(string status, string reason)
        {
            string excludeStatusesBasedOn = "Value";
            string isExcludedStatus = this.SubmissionStatusDropDownList1.SelectedItem.Text;
            HiddenField hdnExcludedStatusesBasedOn = Parent.FindControl("hdnExcludedStatusesBasedOn") as HiddenField;
            excludeStatusesBasedOn = hdnExcludedStatusesBasedOn.Value;
            if (excludeStatusesBasedOn == "Value")
            { isExcludedStatus = this.SubmissionStatusDropDownList1.SelectedItem.Value; }
            else if (excludeStatusesBasedOn == "Text")
            { isExcludedStatus = this.SubmissionStatusDropDownList1.SelectedItem.Text; }
            else
            {
                throw new ApplicationException("Excluded Statuses must be based on Id (List Item value) or StatusCode (List Item text)");
            }
            // don't update status if it is one of excluded statuses
            if (SubmissionStatusDropDownList1.SelectedItem != null &&
                IsAutoUpdateExcludedStatus(isExcludedStatus))
                return;

            SubmissionStatusDropDownList1.ClearSelection();
            ListItem wantedListItem = SubmissionStatusDropDownList1.Items.FindByValue(status);
            if (wantedListItem != null)
            {
                wantedListItem.Selected = true;

                SubmissionStatusReasonDropDownList1.StatusId = Convert.ToInt32(wantedListItem.Value);
                SubmissionStatusReasonDropDownList1.DataBind();

                if (!string.IsNullOrEmpty(reason))
                    SubmissionStatusReasonDropDownList1.Items.FindByValue(reason).Selected = true;
            }
        }

        public bool IsValid(out string message)
        {
            message = string.Empty;
            if (!Visible)
                return true;
            if (IsBlank())
                return true;
            if (LineOfBusinessChildrenDropDownList1.SelectedIndex <= 0)
            {
                message = "* Each Line of Business must have a LOB.";
                SetFocus(LineOfBusinessChildrenDropDownList1);
                return false;
            }

            string excludeStatusesBasedOn = "Value";
            string isExcludedStatus = this.SubmissionStatusDropDownList1.SelectedItem.Text;
            HiddenField hdnExcludedStatusesBasedOn = Parent.FindControl("hdnExcludedStatusesBasedOn") as HiddenField;
            excludeStatusesBasedOn = hdnExcludedStatusesBasedOn.Value;
            if (excludeStatusesBasedOn == "Value")
            { isExcludedStatus = this.SubmissionStatusDropDownList1.SelectedItem.Value; }
            else if (excludeStatusesBasedOn == "Text")
            { isExcludedStatus = this.SubmissionStatusDropDownList1.SelectedItem.Text; }
            else
            {
                throw new ApplicationException("Excluded Statuses must be based on Id (List Item value) or StatusCode (List Item text)");
            }
            
            // reason is required for excluded statuses
            if (SubmissionStatusReasonDropDownList1.SelectedIndex <= 0 &&
                SubmissionStatusReasonDropDownList1.Items.Count > 1)
            {
                message = "* A Status reason is required.";
                SetFocus(SubmissionStatusReasonDropDownList1);
                return false;
            }

            return true;
        }

        public bool IsBlank()
        {
            if (LineOfBusinessChildrenDropDownList1.SelectedIndex < 1)
                return true;
            return false;
        }


        public string GetItemAsString()
        {
            return string.Format("{0}={1}={2}={3}={4}",
                Id,
                LineOfBusinessChildrenDropDownList1.SelectedValue,
                SubmissionStatusDropDownList1.SelectedValue,
                SubmissionStatusReasonDropDownList1.SelectedValue,                
                !Visible);
        }

        public BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild GetItem()
        {
            BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild item = new BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild();
            item.Id = this.Id;
            item.CompanyNumberLOBGridId = Convert.ToInt32(this.LineOfBusinessChildrenDropDownList1.SelectedValue);
            item.SubmissionStatusId = Convert.ToInt32(this.SubmissionStatusDropDownList1.SelectedValue);
            item.SubmissionStatusReasonId = Convert.ToInt32(this.SubmissionStatusReasonDropDownList1.SelectedValue);
            item.Deleted = !Visible;

            return item;
        }
        public APSBO.SubmissionLineOfBusinessChild GetAPSItem()
        {
            APSBO.SubmissionLineOfBusinessChild item = new APSBO.SubmissionLineOfBusinessChild();
            item.Id = this.Id;
            item.CompanyNumberLOBGridId = Convert.ToInt32(this.LineOfBusinessChildrenDropDownList1.SelectedValue);
            item.SubmissionStatusId = Convert.ToInt32(this.SubmissionStatusDropDownList1.SelectedValue);
            item.SubmissionStatusReasonId = Convert.ToInt32(this.SubmissionStatusReasonDropDownList1.SelectedValue);
            item.Deleted = !Visible;

            return item;
        }

        public void SetItem(BCS.Core.Clearance.BizObjects.SubmissionLineOfBusinessChild item)
        {
            Id = item.Id;

            Common.SetSafeSelectedValue(LineOfBusinessChildrenDropDownList1, item.CompanyNumberLOBGridId);
            // If there is a real value, the item may be inactive, so try once again
            if (item.CompanyNumberLOBGridId != 0)
            {
                LineOfBusinessChildrenDropDownList1.Extra = item.CompanyNumberLOBGridId;
                LineOfBusinessChildrenDropDownList1.DataBindByCompanyNumber(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                Common.SetSafeSelectedValue(LineOfBusinessChildrenDropDownList1, item.CompanyNumberLOBGridId);
            }

            Common.SetSafeSelectedValue(SubmissionStatusDropDownList1, item.SubmissionStatusId);

            SubmissionStatusReasonDropDownList1.StatusId = Convert.ToInt32(SubmissionStatusDropDownList1.SelectedValue);
            // If there is a real value, the item may be inactive, so set the extra to be loaded in case its inactive
            if (item.SubmissionStatusReasonId != 0)
            {
                SubmissionStatusReasonDropDownList1.Extra = item.SubmissionStatusReasonId;
            }
            SubmissionStatusReasonDropDownList1.DataBind();
            Common.SetSafeSelectedValue(SubmissionStatusReasonDropDownList1, item.SubmissionStatusReasonId);

            Visible = !item.Deleted;
        }
        public void SetItem(APSBO.SubmissionLineOfBusinessChild item)
        {
            Id = item.Id;

            Common.SetSafeSelectedValue(LineOfBusinessChildrenDropDownList1, item.CompanyNumberLOBGridId);
            // If there is a real value, the item may be inactive, so try once again
            if (item.CompanyNumberLOBGridId != 0)
            {
                LineOfBusinessChildrenDropDownList1.Extra = item.CompanyNumberLOBGridId;
                LineOfBusinessChildrenDropDownList1.DataBindByCompanyNumber(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                Common.SetSafeSelectedValue(LineOfBusinessChildrenDropDownList1, item.CompanyNumberLOBGridId);
            }

            Common.SetSafeSelectedValue(SubmissionStatusDropDownList1, item.SubmissionStatusId);

            SubmissionStatusReasonDropDownList1.StatusId = Convert.ToInt32(SubmissionStatusDropDownList1.SelectedValue);
            // If there is a real value, the item may be inactive, so set the extra to be loaded in case its inactive
            if (item.SubmissionStatusReasonId != 0)
            {
                SubmissionStatusReasonDropDownList1.Extra = item.SubmissionStatusReasonId;
            }
            SubmissionStatusReasonDropDownList1.DataBind();
            Common.SetSafeSelectedValue(SubmissionStatusReasonDropDownList1, item.SubmissionStatusReasonId);

            Visible = !item.Deleted;
        }

        private bool IsAutoUpdateExcludedStatus(string p)
        {
            StringCollection excludedStatuses = DefaultValues.GetLOBGridExcludedUpdateStatuses(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            if (excludedStatuses.Contains(p))
                return true;

            return false;
        }

        public int Id
        {
            set
            { ViewState["Id"] = value; }
            get
            { return (int?)ViewState["Id"] ?? 0; }
        }
    }
}