using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using BCS.Core.Clearance.Admin;
using BCS.WebApp.UserControls;
using Templates = BCS.WebApp.Templates;
using Biz = BCS.Biz;

public partial class ClassCodesControl : System.Web.UI.UserControl
{

    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected override void Render(HtmlTextWriter writer)
    {
        if (GridView1.Controls.Count > 0)
        {
            Table table = GridView1.Controls[0] as Table;
            foreach (GridViewRow grow in GridView1.Rows)
            {
                if (grow.RowType != DataControlRowType.DataRow)
                    continue;

                grow.FindControl("txtSicCodeDesc").Visible = false;

                TableRow row = grow as TableRow;

                int realIndex = table.Rows.GetRowIndex(row);

                GridViewRow newRow = new GridViewRow(realIndex, realIndex, DataControlRowType.DataRow, grow.RowState);
                newRow.Visible = grow.Visible;

                TableCell newCell = new TableCell();
                //filler cell for edit/add column
                newRow.Cells.Add(newCell);                

                newCell = new TableCell();
                newCell.ColumnSpan = GridView1.Columns.Count - 3;
                newCell.CssClass = "txt";
                newRow.Cells.Add(newCell);
                newCell.Text = "&nbsp;" + (grow.FindControl("txtSicCodeDesc") as Label).Text;

                newCell = new TableCell();
                //filler cell for delete column
                newRow.Cells.Add(newCell);                

                table.Controls.AddAt(realIndex + 1, newRow);
            }

            if (GridView1.ShowFooter && GridView1.FooterRow != null)
            {
                GridViewRow fgrow = GridView1.FooterRow;
                fgrow.FindControl("txtSicCodeDesc").Visible = false;
                TableRow frow = fgrow as TableRow;

                int frealIndex = table.Rows.GetRowIndex(frow);


                GridViewRow fnewRow = new GridViewRow(frealIndex, frealIndex, fgrow.RowType, fgrow.RowState);

                TableCell fnewCell = new TableCell();
                //filler cell for edit/add column
                fnewRow.Cells.Add(fnewCell);

                fnewCell = new TableCell();
                fnewCell.ColumnSpan = GridView1.Columns.Count - 3;
                fnewCell.CssClass = "txt";
                fnewRow.Cells.Add(fnewCell);

                Label l = new Label();
                l.Text = "&nbsp;" + (fgrow.FindControl("txtSicCodeDesc") as Label).Text;
                fnewCell.Controls.Add(l);

                fnewCell = new TableCell();
                //filler cell for delete column
                fnewRow.Cells.Add(fnewCell);

                table.Controls.AddAt(frealIndex + 1, fnewRow);
            }            
            if (GridView1.HeaderRow != null && GridView1.Rows.Count > 0)
            {
                GridViewRow tpgrow = GridView1.HeaderRow;

                TableRow tprow = tpgrow as TableRow;

                int frealIndex = table.Rows.GetRowIndex(tprow);

                GridViewRow fnewRow = new GridViewRow(frealIndex, frealIndex, tpgrow.RowType, tpgrow.RowState);

                TableCell fnewCell = new TableCell();
                //filler cells for edit/add column, class code, sic code, no need for sic code description column since it is hidden
                for (int i = 0; i < 3; i++)
                {
                    fnewCell = new TableCell();
                    fnewRow.Cells.Add(fnewCell);
                }

                fnewCell = new TableCell();
                fnewCell.ColumnSpan = GridView1.Columns.Count - 5;
                fnewCell.HorizontalAlign = HorizontalAlign.Center;                
                fnewCell.Style.Add("border-bottom", "solid 1px gray");
                fnewCell.Text = "Hazard Grades";

                fnewRow.Cells.Add(fnewCell);

                fnewCell = new TableCell();
                //filler cell for delete column
                                
                fnewRow.Cells.Add(fnewCell);

                table.Controls.AddAt(1, fnewRow);
            }
        }

        //GridView1.Columns[3].Visible = false;

        base.Render(writer);
    }
    #endregion

    #region Control Events
    protected void GridView1_Init(object sender, EventArgs e)
    {
        Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
        dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberHazardGradeType.Columns.CompanyNumberId, GetCurrentCompanyNumberId());
        Biz.CompanyNumberHazardGradeTypeCollection coll = dm.GetCompanyNumberHazardGradeTypeCollection(Biz.FetchPath.CompanyNumberHazardGradeType.HazardGradeType);

        // count of colums before for loop, so that columns in for loop may be inserted at appropriate place
        int initColCount = 4;

        // edit, update cancel command field
        CommandField cf = new CommandField();
        //cf = new CommandField();
        //cf.ShowEditButton = true;
        //cf.ButtonType = ButtonType.Button;
        //cf.ControlStyle.CssClass = "button";
        //GridView1.Columns.Add(cf);

        TemplateField tf = new TemplateField();
        tf.ItemTemplate = new Templates.CommandTemplate(DataControlRowType.DataRow, DataControlRowState.Normal, "");
        tf.EditItemTemplate = new Templates.CommandTemplate(DataControlRowType.DataRow, DataControlRowState.Edit, "");
        tf.FooterTemplate = new Templates.CommandTemplate(DataControlRowType.Footer, DataControlRowState.Insert, "");
        GridView1.Columns.Add(tf);
        
        tf = new TemplateField();
        tf.ItemTemplate = new Templates.GenericTemplate(
                DataControlRowType.DataRow, string.Empty,
                "CodeValue",
                "text", string.Empty);
        tf.FooterTemplate = new Templates.ClassCodeDropDownListTemplate(GetCurrentCompanyNumberId(),
            DataControlRowState.Insert, DataControlRowType.Footer, "", "", "", "txtSicCode|SicCodeList.Code", "txtSicCodeDesc|SicCodeList.CodeDescription");
        tf.HeaderText = "CodeValue";
        GridView1.Columns.Add(tf);

        tf = new TemplateField();
        tf.ItemTemplate = new Templates.GenericTemplate(
                DataControlRowType.DataRow, string.Empty,
                "SicCodeList.Code",
                "text", "txtSicCode");
        tf.FooterTemplate = new Templates.GenericTemplate(
                DataControlRowType.DataRow, string.Empty,
                "SicCodeList.Code",
                "text", "txtSicCode");
        tf.HeaderText = "SIC Code";
        GridView1.Columns.Add(tf);

        tf = new TemplateField();
        tf.ItemTemplate = new Templates.GenericTemplate(
                DataControlRowType.DataRow, string.Empty,
                "SicCodeList.CodeDescription",
                "text", "txtSicCodeDesc");

        tf.Visible = false;
        tf.FooterTemplate = new Templates.GenericTemplate(
                DataControlRowType.DataRow, string.Empty,
                "SicCodeList.CodeDescription",
                "text", "txtSicCodeDesc");
        //tf.HeaderText = "SIC Code Desc";        
        GridView1.Columns.Add(tf);

        for (int i = 0; i < coll.Count; i++)
        {
            TemplateField atf = new TemplateField();
            
            Templates.GenericTemplate itemTemplate = new Templates.GenericTemplate(
                DataControlRowType.DataRow, string.Empty,
                "CompanyNumberClassCodeHazardGradeValues",
                "text", string.Empty);
            itemTemplate.UseMethod = true;
            itemTemplate.MethodName = "FindByHazardGradeTypeId";
            itemTemplate.FinalField = "HazardGradeValue";
            itemTemplate.MethodParameters = new object[] { string.Format("{0}", coll[i].HazardGradeTypeId) };
            atf.ItemTemplate = itemTemplate;

            Templates.ScaleDropDownListTemplate editTemplate = new Templates.ScaleDropDownListTemplate(
                DataControlRowState.Edit, DataControlRowType.DataRow,
                "CompanyNumberClassCodeHazardGradeValues",
                string.Format("CompanyNumberClassCodeHazardGradeValues[{0}].{1}", i, coll[i].HazardGradeTypeId));
            editTemplate.UseMethod = true;
            editTemplate.MethodName = "FindByHazardGradeTypeId";
            editTemplate.FinalField = "HazardGradeValue";
            editTemplate.MethodParameters = new object[] { string.Format("{0}", coll[i].HazardGradeTypeId) };
            atf.EditItemTemplate = editTemplate;

            atf.FooterTemplate = new Templates.ScaleDropDownListTemplate(DataControlRowState.Insert, DataControlRowType.Footer,
                "", string.Format("CompanyNumberClassCodeHazardGradeValues[{0}].{1}", i, coll[i].HazardGradeTypeId));

            atf.HeaderText = coll[i].HazardGradeType.TypeName;            
            GridView1.Columns.Insert(i + initColCount, atf);
        }

        // delete command field
        cf = new CommandField();
        cf.ShowDeleteButton = true;
        cf.ButtonType = ButtonType.Button;
        cf.ControlStyle.CssClass = "button";
        GridView1.Columns.Add(cf);
    }
    protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        GridView1.DataBind();
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "New")
        {
            System.Collections.Specialized.HybridDictionary dict = new System.Collections.Specialized.HybridDictionary();
            string classCodeId = string.Empty;

            //Button btnAdd = e.CommandSource as Button;
            //GridViewRow row = (GridViewRow)btnAdd.NamingContainer;

            GridViewRow row = GridView1.FooterRow;

            foreach (TableCell cell in row.Cells)
            {
                if (cell.Controls.Count > 0)
                {
                    Control c = cell.Controls[0];
                    if (c is ScaleDropDownList)
                    {
                        string id = c.ID.Split(".".ToCharArray())[1];
                        string value = (c as ScaleDropDownList).SelectedValue;
                        dict.Add(id, value);
                    }
                    if (c is ClassCodeDropDownList)
                    {
                        classCodeId = (c as ClassCodeDropDownList).SelectedValue;
                    }
                }
            }
            if (classCodeId == "0")
            {
                Common.SetStatus(ccHgvMessagePlaceHolder, "Please select a class code.");
                return;
            }
            foreach (System.Collections.DictionaryEntry o in dict)
            {
            }
            string result = DataAccess.AddClassCodeHV(GetCurrentCompanyNumberId(), Convert.ToInt32(classCodeId), dict);

            Common.SetStatus(ccHgvMessagePlaceHolder, result);

            GridView1.ShowFooter = false;
            GridView1.DataSourceID = "ObjectDataSource1";
            GridView1.DataBind();
        }
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        {
            System.Collections.Specialized.HybridDictionary dict = new System.Collections.Specialized.HybridDictionary();
            string classCodeId = string.Empty;

            GridViewRow row = GridView1.Rows[e.RowIndex];

            foreach (TableCell cell in row.Cells)
            {
                if (cell.Controls.Count > 0)
                {
                    Control c = cell.Controls[0];
                    if (c is ScaleDropDownList)
                    {
                        string id = c.ID.Split(".".ToCharArray())[1];
                        string value = (c as ScaleDropDownList).SelectedValue;
                        dict.Add(id, value);
                    }
                    if (c is ClassCodeDropDownList)
                    {
                        classCodeId = (c as ClassCodeDropDownList).SelectedValue;
                    }
                }
            }
            classCodeId = e.Keys["Id"].ToString();

            if (classCodeId == "0")
            {
                Common.SetStatus(ccHgvMessagePlaceHolder, "Please select a class code.");
                e.Cancel = true;
                return;
            }
            string result = DataAccess.UpdateClassCodeHV(GetCurrentCompanyNumberId(), Convert.ToInt32(classCodeId), dict);

            Common.SetStatus(ccHgvMessagePlaceHolder, result);

            //GridView1.DataBind();
        }
    }
    protected void GridView1_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        e.ExceptionHandled = true;

        GridView1.DataBind();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState != DataControlRowState.Edit)
        {
            if (e.Row.Cells[e.Row.Cells.Count - 1].Controls.Count > 0)
            {
                Button btnDelete = (e.Row.Cells[e.Row.Cells.Count - 1].Controls[0] as Button);
                btnDelete.Enabled = !BCS.WebApp.Components.Security.IsUserReadOnly();
                btnDelete.Attributes.Add("onclick", "var v = confirm('Are you sure you want to delete these hazard grade values?'); if (v == false) { return false;}");
            }
            if (e.Row.Cells[0].Controls.Count > 0)
            {
                Button btnEdit = (e.Row.Cells[0].Controls[0] as Button);
                btnEdit.Enabled = !BCS.WebApp.Components.Security.IsUserReadOnly();
            }
        }
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.ShowFooter = false;
    }

    protected void ObjectDataSource1_Deleting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        int companyNumberId = Convert.ToInt32(Session["CompanyNumberSelected"]);
        e.InputParameters["classCodeId"] = e.InputParameters["Id"];
        e.InputParameters.Remove("Id");

    }
    protected void ObjectDataSource1_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(ccHgvMessagePlaceHolder, e.ReturnValue.ToString());
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        GridView1.ShowFooter = true;
        GridView1.EditIndex = -1;

        if (GridView1.FooterRow == null)
        {
            Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberHazardGradeType.Columns.CompanyNumberId, GetCurrentCompanyNumberId());
            Biz.CompanyNumberHazardGradeTypeCollection cnhgtcoll = dm.GetCompanyNumberHazardGradeTypeCollection();
            if (cnhgtcoll.Count == 0)
            {
                Common.SetStatus(ccHgvMessagePlaceHolder, "The company number selected does not have hazard grade types defined.");
                return;
            }

            Biz.ClassCodeCollection coll = new BCS.Biz.ClassCodeCollection();
            Biz.CompanyNumber dummy = null;
            coll.Add(dm.NewClassCode("", dummy));
            foreach (Biz.CompanyNumberHazardGradeType cnhgt in cnhgtcoll)
            {
                //coll[0].AddCompanyNumberClassCodeHazardGradeValue();
            }

            GridView1.DataSourceID = null;
            GridView1.DataSource = coll;
            GridView1.DataBind();

            GridView1.Rows[0].Visible = false;
            //GridView1.Rows[1].Visible = false;
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        GridView1.EditIndex = -1;
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        gvClassCodes.SelectedIndex = -1;
        dvClassCodes.ChangeMode(DetailsViewMode.Insert);
    }

    protected void gvClassCodes_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        Cache.Remove(string.Concat("ClassCodes4", GetCurrentCompanyNumberId()));
        gvClassCodes.DataBind();
        dvClassCodes.DataBind();
    }
    protected void gvClassCodes_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        if (!BCS.WebApp.Components.Security.IsUserReadOnly())
            dvClassCodes.ChangeMode(DetailsViewMode.Edit);
    }
    protected void ObjectDataSource2_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(ccMessagePlaceHolder, e.ReturnValue.ToString());
        }
    }

    protected void odsClassCode_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(ccMessagePlaceHolder, e.ReturnValue.ToString());
        }

    }
    protected void odsClassCode_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(ccMessagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void odsClassCode_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        BCS.Core.Clearance.BizObjects.ClassCode cc = e.InputParameters[0] as BCS.Core.Clearance.BizObjects.ClassCode;
        cc.CompanyNumberId = Common.GetSafeIntFromSession("CompanyNumberSelected");
    }    

    protected void dvClassCodes_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        Cache.Remove(string.Concat("ClassCodes4", GetCurrentCompanyNumberId()));
        gvClassCodes.DataBind();
        dvClassCodes.DataBind();
    }
    protected void dvClassCodes_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        Cache.Remove(string.Concat("ClassCodes4", GetCurrentCompanyNumberId()));
        gvClassCodes.DataBind();
        dvClassCodes.DataBind();
    }
    protected void dvClassCodes_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        e.Cancel = BCS.WebApp.Components.Security.IsUserReadOnly();
    }
    
    #endregion

    #region Other Methods
    private int GetCurrentCompanyNumberId()
    {
        try
        {
            return Convert.ToInt32(Session["CompanyNumberSelected"]);
        }
        catch (Exception)
        {
            return 0;
        }
    }
    #endregion
    protected void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        throw new ApplicationException("Ajax Error", e.Exception);
    }
    protected void btnFilterClassCodeTab_Click(object sender, EventArgs e)
    {

    }
}
