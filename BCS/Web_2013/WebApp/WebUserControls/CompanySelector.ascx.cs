namespace BCS.WebApp.UserControls
{
	using System;    
    using System.Collections.Specialized;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using BCS.Biz;
	using BCS.Core;
	using BCS.WebApp.Components;

	/// <summary>
	///	Summary description for CompanySelector.
	/// </summary>
	public partial class CompanySelector : System.Web.UI.UserControl
	{
		private BCS.Core.Security.CustomPrincipal m_customPrincipal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;

		protected void Page_Load(object sender, System.EventArgs e)
		{
           if (!IsPostBack)
			{
               CompanyDropDownList1.DataBind(m_customPrincipal.Identity.Name);

                if (Common.GetCookie("CompanySelected") != null)
                {
                    if (null != CompanyDropDownList1.Items.FindByValue(Common.GetCookie("CompanySelected")))
                    {
                        CompanyDropDownList1.Items.FindByValue(Common.GetCookie("CompanySelected")).Selected = true;
                        CompanyNumberDropDownList1.CompanyId = Convert.ToInt32(Common.GetCookie("CompanySelected"));
                        CompanyNumberDropDownList1.DataBind();
                        Session[SessionKeys.CompanyId] = CompanyDropDownList1.SelectedValue;
                    }
                }
                if (Common.GetCookie("CompanyNumberSelected") != null)
                {
                    if (null != CompanyNumberDropDownList1.Items.FindByValue(Common.GetCookie("CompanyNumberSelected")))
                    {
                        CompanyNumberDropDownList1.Items.FindByValue(Common.GetCookie("CompanyNumberSelected")).Selected = true;
                        // company number id
                        Session[SessionKeys.CompanyNumberId] = CompanyNumberDropDownList1.SelectedValue;
                        // company number itself
                        Session[SessionKeys.CompanyNumber] = CompanyNumberDropDownList1.SelectedItem.Text; 
                    }
                }
			}
		}		
		protected void CompanyDropDownList1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            if (CompanyDropDownList1.SelectedValue.Length > 0 && CompanyDropDownList1.SelectedValue != "0" )
			{
                Session[SessionKeys.CompanyId] = CompanyDropDownList1.SelectedValue;
                Common.AddCookie("CompanySelected", CompanyDropDownList1.SelectedValue);
				CompanyNumberDropDownList1.CompanyId = Convert.ToInt32( CompanyDropDownList1.SelectedValue );
				CompanyNumberDropDownList1.DataBind();
                CompanyNumberDropDownList1_SelectedIndexChanged(CompanyNumberDropDownList1, EventArgs.Empty);
                
			}
			else
			{
                Helper.ClearDBItems(CompanyNumberDropDownList1);
                Session.Contents.Remove(SessionKeys.CompanyId);                
                Common.DeleteCookie("CompanySelected");
			}
            Session["CompanyChanged"] = true;
		}
        protected void CompanyNumberDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["CompanyChanged"] = true;
            if (CompanyNumberDropDownList1.SelectedValue.Length > 0 && CompanyNumberDropDownList1.SelectedValue != "0")
            {
                Session[SessionKeys.CompanyNumberId] = CompanyNumberDropDownList1.SelectedValue;
                Session[SessionKeys.CompanyNumber] = CompanyNumberDropDownList1.SelectedItem.Text;
                Common.AddCookie("CompanyNumberSelected", CompanyNumberDropDownList1.SelectedValue);
            }
            else
            {
                Session.Contents.Remove("CompanyNumberSelected");
                Session.Contents.Remove(SessionKeys.CompanyNumber);
                Common.DeleteCookie("CompanyNumberSelected");
            }
        }
}
}
