<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AutoNumberGenerator.ascx.cs" Inherits="WebUserControls_AutoNumberGenerator" %>
<table>
    <tr>
    <td>
        <asp:TextBox ID="TextBoxPolicyNumber" runat="server" autocomplete="off"></asp:TextBox>
    </td>
    <td>
        <UserControls:RFV ID="rfvAutoNumber" runat="server" ErrorMessage="" Text="*" SetFocusOnError="True" Display="None" Enabled="True">
        </UserControls:RFV>
    </td>
    <td id="tdGenerator" runat="server">
        <asp:LinkButton ID="LinkButton1" runat="server" style="border-width: 0px;" 
            CausesValidation="False" OnInit="ImageButton1_Init" OnClick="LinkButton1_Click" onkeydown="preventScrollOnSpace(event);" onkeyup="fireClickOnSpace(this, event);" >
            <img id="img1" runat="server" src='<%$ Code : Common.GetImageUrl("calculator.png") %>' alt="Click to generate a new number" style="border-width: 0px;" />
        </asp:LinkButton>
        <asp:Label runat="server" id="lbPolicyWarning" ForeColor="red" Visible="false" >
        The policy number you've entered is being used on another submission.  
    </asp:Label>
    </td>
    </tr>
    
</table>
