﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BCS.Core.Clearance.Admin;
using BCS.WebApp.UserControls;
using Biz = BCS.Biz;
using BCS.WebApp.Components;

public partial class WebUserControls_SubmissionStatusSubmissionStatusReason : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                PopulateHiddenField();
                ddlSubmissionStatus.CompanyNumberId = CompanyNumberId;
                ddlSubmissionStatus.DataBind();

                ddlSubmissionStatusReason.CompanyNumberId = CompanyNumberId;

                SetSavedValues();

                SetUpdateButtonDisplay();
            }
            catch (Exception ex)
            {

            }
        }
    }

    #region variables
    public enum Section
    {
         Wizard,
         ReadOnly
    }

    private Section _section;
    #endregion

    #region properties

    [System.ComponentModel.Browsable(true)]
    public Section WizardSection
    {
        get { return _section; }
        set { _section = value; }
    }

    [System.ComponentModel.Browsable(true)]
    public int CompanyNumberId
    {
        get
        {
            return Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        }
    }

    private int SubmissionId
    {
        get
        {
            return Common.GetSafeIntFromSession("SubmissionId");
        }
    }

    public bool Display
    {
        get {return _section == Section.ReadOnly;}
    }
   
    [System.ComponentModel.Browsable(true)]
    public int SelectedStatusValue
    {
        get
        {
            int val = 0;
            Int32.TryParse(ddlSubmissionStatus.SelectedValue, out val);
            return val;
        }
        set
        {
            ddlSubmissionStatus.SelectedValue = value.ToString();
            hdnSelectedStatus.Value = value.ToString();
        }
    }

    [System.ComponentModel.Browsable(true)]
    public int SelectedReasonValue
    {
        get
        {            
            int val = 0;
            Int32.TryParse(hdnStatusReasonSelectedValue.Value, out val);
            return val;
        }
        set
        {
            hdnStatusReasonSelectedValue.Value = value.ToString();
            ddlSubmissionStatusReason.SelectedValue = value.ToString();
        }
    }

    [System.ComponentModel.Browsable(true)]
    public string StatusDate
    {
        get
        {
            return submissionStatusDateBox.Text;
        }
        set
        {
            submissionStatusDateBox.Text = value;
        }
    }

    #endregion


    private void SetUpdateButtonDisplay()
    {
        if (WizardSection == Section.Wizard)
        {
            lnkUpdateStatusAndReason.Visible = lnkUpdateStatusAndReasonTop.Visible = false;
            return;
        }

        Biz.CompanyParameter param = ConfigValues.GetCompanyParameter(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

        if (param != null && param.LinkAfterSubStatus)
        {
            lnkUpdateStatusAndReason.Visible = false;
            lnkUpdateStatusAndReasonTop.Visible = true;
        }
        else
        {
            lnkUpdateStatusAndReasonTop.Visible = false;
            lnkUpdateStatusAndReason.Visible = true;
        }
    }

    public void SetFocus()
    {
        Page.SetFocus(ddlSubmissionStatus);
    }

    private void PopulateHiddenField()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);

        dm.QueryCriteria.Clear();
        dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatusSubmissionStatusReason.SubmissionStatus.CompanyNumberSubmissionStatus.Columns.CompanyNumberId, CompanyNumberId);
        dm.QueryCriteria.And(Biz.JoinPath.SubmissionStatusSubmissionStatusReason.Columns.Active, true);

        Biz.SubmissionStatusSubmissionStatusReasonCollection reasons = dm.GetSubmissionStatusSubmissionStatusReasonCollection(Biz.FetchPath.SubmissionStatusSubmissionStatusReason.SubmissionStatusReason);

        foreach (Biz.SubmissionStatusSubmissionStatusReason reason in reasons)
        {
            sb.Append(string.Format("{0}|{1}|{2}*", reason.SubmissionStatusId, reason.SubmissionStatusReasonId, reason.SubmissionStatusReason.ReasonCode));
        }

        hdnStatusReason.Value = sb.ToString();
    }

    private void SetSavedValues()
    {
        BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
        dm.QueryCriteria.Clear();

        dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Id, SubmissionId);

        Biz.Submission submission = dm.GetSubmission();

        if (submission == null)
            return;

        if (!submission.SubmissionStatusId.IsNull)
        {
            SelectedStatusValue = (int)submission.SubmissionStatusId;

            ddlSubmissionStatusReason.StatusId = SelectedStatusValue;
            ddlSubmissionStatusReason.DataBind();
        }

        if (!submission.SubmissionStatusDate.IsNull)
        {
            DateTime statusDate = (DateTime)submission.SubmissionStatusDate;
            submissionStatusDateBox.Text = statusDate.ToString("MM/dd/yyyy");
        }

        if (!submission.SubmissionStatusReasonId.IsNull)
        {
            SelectedReasonValue = (int)submission.SubmissionStatusReasonId;
        }
    }

    public void UpdateStatusAndReason()
    {
        UpdateStatusAndReason(null, null);
    }

    protected void UpdateStatusAndReason(object sender, EventArgs e)
    {
        bool update = false;
        BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
        dm.QueryCriteria.Clear();

        dm.QueryCriteria.And(Biz.JoinPath.Submission.Columns.Id, SubmissionId);

        Biz.Submission submission = dm.GetSubmission();

        if (submission == null)
            return;

        if ((SelectedStatusValue > 0 && submission.SubmissionStatusId.IsNull) || (submission.SubmissionStatusId != SelectedStatusValue))
        {
            update = true;           
            submission.SubmissionStatusId = SelectedStatusValue;
        }

        if (Common.GetSafeDateTime(submission.SubmissionStatusDate.ToString())!= Common.GetSafeDateTime(submissionStatusDateBox.Text))
        {
            update = true;
            submission.SubmissionStatusDate = Common.GetSafeDateTime(submissionStatusDateBox.Text);
        }

        if (update)
        {
            //Add the status history
            Biz.SubmissionStatusHistory statusHistory = submission.NewSubmissionStatusHistory();
            statusHistory.Active = true;
            statusHistory.PreviousSubmissionStatusId = SelectedStatusValue;
            statusHistory.StatusChangeDt = DateTime.Now;
            statusHistory.StatusChangeBy = HttpContext.Current.User.Identity.Name;
            statusHistory.SubmissionId = submission.Id;
            statusHistory.SubmissionStatusDate = submission.SubmissionStatusDate;
        }
               
        if ((SelectedStatusValue > 0 && submission.SubmissionStatusReasonId.IsNull) || (submission.SubmissionStatusReasonId != SelectedReasonValue))
        {
            update = true;
            if (SelectedReasonValue == 0)
            {
                submission.SubmissionStatusReasonId = System.Data.SqlTypes.SqlInt32.Null;
            }
            else
            {
                submission.SubmissionStatusReasonId = SelectedReasonValue;
            }
        }

        if (update)
        {            
            //set the modifying user & time
            submission.UpdatedDt = DateTime.Now;
            submission.UpdatedBy = HttpContext.Current.User.Identity.Name;

            dm.CommitAll();

            //post back to the page otheriwise the history isn't displayed.
            Response.Redirect(Request.Url.AbsolutePath);
        }
    }

}