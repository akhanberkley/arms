#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls; 
#endregion

public partial class CompanyNoCodesControl : System.Web.UI.UserControl
{

    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {

    }     
    #endregion

    #region Conrol Events
    protected void DropDownList1_PreRender(object sender, EventArgs e)
    {
        if (DropDownList1.Items.FindByValue("0") == null)
            DropDownList1.Items.Insert(0, new ListItem("", "0"));
    }

    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        e.Cancel = BCS.WebApp.Components.Security.IsUserReadOnly();
        if (e.NewMode == DetailsViewMode.Edit || e.NewMode == DetailsViewMode.Insert)
            if (Session["CompanyNumberSelected"] == null || Session["CompanyNumberSelected"].ToString() == "0")
            {
                e.Cancel = true;
                Common.SetStatus(messagePlaceHolder, "Please select a company number before Add/Modify.");
            }
    }
    protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        GridView1.DataBind();
    }

    protected void GridView1_PageIndexChanged(object sender, EventArgs e)
    {
        GridView1.SelectedIndex = -1;
    }

    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        GridView1.DataBind();
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        GridView1.DataBind();
    }

    protected void odsCompanyNoCode_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }

    protected void odsCompanyNoCode_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Session["CompanyNumberSelected"] == null || Session["CompanyNumberSelected"].ToString() == "0")
        {
            Common.SetStatus(messagePlaceHolder, "Please select a company number before Add/Modify.");
            return;
        }
        DetailsView1.ChangeMode(DetailsViewMode.Insert);
    }
    protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        e.Values.Add("CompanyNumberId", Session["CompanyNumberSelected"] != null ? Session["CompanyNumberSelected"] : 0);
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        bool b = Common.ShouldUpdate(e);

        e.Cancel = b;
        e.NewValues.Add("CompanyNumberId", Session["CompanyNumberSelected"] != null ? Session["CompanyNumberSelected"] : 0);

        if (b)
        {
            // there was nothing to update. Just show the user the success message.
            Common.SetStatus(messagePlaceHolder, "Company Number Code Sucessfully updated.");
        }

    }

    protected void DropDownList2_PreRender(object sender, EventArgs e)
    {
        if ((sender as DropDownList).Items.Count == 0)
        {
            DetailsView1.Visible = false;
            //Common.SetStatus(messagePlaceHolder, "No code types exist for the company number selected.");
        }
        else
        { DetailsView1.Visible = true; }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Session["CompanyNumberSelected"] == null || Session["CompanyNumberSelected"].ToString() == "0")
        {
            Common.SetStatus(messagePlaceHolder, "Please select a company number before modify.");
            return;
        }
        if (!BCS.WebApp.Components.Security.IsUserReadOnly())
            DetailsView1.ChangeMode(DetailsViewMode.Edit);
    }
    protected void odsCNCodes_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        if (!BCS.WebApp.Components.Security.IsUserReadOnly())
            DetailsView1.ChangeMode(DetailsViewMode.Edit);
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        GridView1.SelectedIndex = -1;
    }
    protected void DatePickerTextBox_Init(object sender, EventArgs e)
    {
        TextBox tb = sender as TextBox;
        tb.Attributes.Add("onclick", "scwShow(this,this);");
        tb.Attributes.Add("onkeydown", "hideCalOnTab(event);");
        tb.Attributes.Add("onblur", "this.value = purgeDate(this);");
    }
    #endregion
    
}
