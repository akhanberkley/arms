<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LOBListItem.ascx.cs" Inherits="BCS.WebApp.WebUserControls.WebUserControls_LOBListItem" %>
<%--<table>--%>
    <tr>
        <td>
            <UserControls:LineOfBusinessChildrenDropDownList ID="LineOfBusinessChildrenDropDownList1" runat="server">
            </UserControls:LineOfBusinessChildrenDropDownList><%--<label runat="server" id="lblReqIndicator" visible="false" class="errorText">*</label>--%>
            <UserControls:RFV ID="rfvLOB" runat="server" SetFocusOnError="True" ErrorMessage="&nbsp;" Text="*" Enabled="False" Display="None"
             ControlToValidate="LineOfBusinessChildrenDropDownList1" InitialValue="0"></UserControls:RFV>
             <%--<asp:RequiredFieldValidator ID="rfvLOB" runat="server" SetFocusOnError="True" ErrorMessage="&nbsp;" Text="*" Enabled="False" Display="None"
             ControlToValidate="LineOfBusinessChildrenDropDownList1" InitialValue="0"></asp:RequiredFieldValidator>--%>
             
        </td>
        <td>
            <UserControls:SubmissionStatusDropDownList ID="SubmissionStatusDropDownList1" runat="server"
                AutoPostBack="true" OnInit="SubmissionStatusDropDownList1_Init" OnSelectedIndexChanged="SubmissionStatusDropDownList1_SelectedIndexChanged">
            </UserControls:SubmissionStatusDropDownList>
        </td>
        <td>
            <UserControls:SubmissionStatusReasonDropDownList ID="SubmissionStatusReasonDropDownList1" runat="server"
                StatusId='<%# Convert.ToInt32(SubmissionStatusDropDownList1.SelectedValue) %>' >
            </UserControls:SubmissionStatusReasonDropDownList>
        </td>
        <%--<td>
            <asp:TextBox ID="txPremium" runat="server" OnInit="txPremium_Init"></asp:TextBox>
        </td>--%>
        <td>
            <asp:ImageButton ID="ibRemove" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("minus.gif") %>' OnClick="ibRemove_Click" CausesValidation="false" TabIndex="500"
            AlternateText="Click this icon to remove a line of business" ToolTip="Click this icon to remove a line of business" />            
        </td>
    </tr>
<%--</table>--%>
