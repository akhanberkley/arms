<%@ Page language="c#" MasterPageFile="~/Site.Basic.PATS.master" Title="Berkley Clearance System : Error" Inherits="BCS.WebApp.Error" CodeFile="Error.aspx.cs" Theme="DefaultTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table cellpadding="0" cellspacing="0" class="box">
        <tbody>
            <tr>
                <td class="TL"><div /></td>
                <td class="TC">Our apologies.</td>
                <td class="TR"><div /></td>
            </tr>
            <tr>
                <td class="ML"><div /></td>
                <td class="MC">

                <asp:Panel runat="server" ID="pnlCustomMessage" Visible="false">
                    
                        <div style="color:#ff0000;font-weight:bold;padding:0px 20px 0px 20px;margin-top:10px;">
                            <div style="color:#0066cc;font-weight:normal;font-size:14px;">Error Message:</div>
                            
                            <asp:Label runat="server" ID="lblMessage"></asp:Label>
                        </div>

                        <p style="padding:0px 20px 0px 20px;">
                            If this problem persists, or if you have any questions, 
	                        please                                  
                            <A href="mailto:btsprod@service-now.com">
                            contact the BTS HelpDesk
                            </A> 
                            for 
	                        further assistance.&nbsp;You may&nbsp;wish&nbsp;to return to the 
                            <A href="javascript:history.go(-1);">
		                    previous page.
                            </A>
                            <br /><br />
                            Thank you for your patience and 
	                        understanding. We apologize for the inconvenience this error may have caused.
                        </p>

                        <asp:Label id="lblErrorMessage" runat="server"></asp:Label>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlDefaultError">
                    <p style="PADDING-LEFT: 10px; PADDING-TOP: 10px">An error has occurred on the page 
	                    you were requesting. If this problem persists, or if you have any questions, 
	                    please <A href="mailto:btsprod@service-now.com">contact the BTS HelpDesk</A> for 
	                    further assistance.&nbsp;You may&nbsp;wish&nbsp;to return to the <A href="javascript:history.go(-1);">
		                    previous page.</A></p>
                    <P style="PADDING-LEFT: 10px; PADDING-TOP: 10px">Thank you for your patience and 
	                    understanding. We apologize for the inconvenience this error may have caused.</P>
                   <%-- <P style="PADDING-LEFT: 10px; PADDING-TOP: 10px"><table cellSpacing="0" border="1" bordercolor="#ff0000" id="ErrorTable" runat="server">
		                    <tr>
			                    <td><table cellspacing="0" cellpadding="5" style="background-color: #ffffcc">
					                    <tr>
						                    <td style="font-size: 2"><strong>Error Message (<A runat="Server" id="copy" href="javascript:copyErrorToClipboard(document.getElementById('lblErrorMessage').innerHTML);"
										                    onmouseover="return showStatus('Copy error message to clipboard');" onclick="return showStatus('Copy error message to clipboard');" onmouseout="return showStatus('');">Copy 
										                    to Clipboard</A>):</strong></td>
					                    </tr>
					                    <tr>
					                        <td style="font-size: 2">
								                    
                                            </td>
					                    </tr>
				                    </table>
			                    </td>
		                    </tr>
	                    </table>
                    </P>--%>
                </asp:Panel>
                
                </td>
                <td class="MR"><div /></td>
            </tr>
            <tr>
                <td class="BL"><div/></td>
                <td class="BC"><div/></td>
                <td class="BR"><div/></td>
            </tr>
        </tbody>
    </table>
    
</asp:Content>