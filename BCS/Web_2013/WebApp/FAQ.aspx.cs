#region Using
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using BCS.Biz;
using BCS.WebApp.Components;
using BCS.WebApp.UserControls;
using BCS.DAL;
using System.Linq;
using BCS.Core.Clearance;
using System.Collections.Generic;


#endregion

namespace BCS.WebApp
{
    /// <summary>
    /// Summary description for Clients.
    /// </summary>
    public partial class FAQ : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Common.Forward();                
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!IsPostBack || Session["CompanyChanged"] != null)
            {
                PopulateFAQ();
                Session.Remove("CompanyChanged");
            }
            base.OnPreRender(e);
        }

        private void PopulateFAQ()
        {
            List<BCS.DAL.FAQ> faqs = FAQManager.GetFAQsForCompany(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));

            lvFAQ.DataSource = faqs;
            lvFAQ.DataBind();
        }

        protected void lvFAQ_OnDataBound(object sender, ListViewItemEventArgs e)
        {
            BCS.DAL.FAQ faq = (BCS.DAL.FAQ)e.Item.DataItem;
            LinkButton btn = (LinkButton)Common.FindControlRecursive(e.Item, "lnkShowAnswer");
            if (btn != null)
                btn.OnClientClick = string.Format("javascript:return ToggleDisplay('pnlAnswer{0}');", faq.Id);

            Panel pnl = (Panel)Common.FindControlRecursive(e.Item, "pnlAnswer");
            if (pnl != null)
                pnl.ID = "pnlAnswer" + faq.Id.ToString();

            if (faq.DisplayUsersWithAccess && faq.Role != null && faq.Role.Count > 0)
            {
                Panel pnlUsers = (Panel)Common.FindControlRecursive(e.Item, "pnlUsersWithAccess");
                pnlUsers.Visible = true;                

                HtmlTable tbl = (HtmlTable)Common.FindControlRecursive(e.Item, "tblUsers");
                List<BCS.DAL.User> users = FAQManager.GetUserForRoles(faq);

                double columns = 4;
                double rows = users.Count / columns;
                rows = Math.Ceiling(rows);

                HtmlTableRow tr = new HtmlTableRow();
                HtmlTableCell td = new HtmlTableCell();

                tbl.Rows.Add(tr);
                tr.Cells.Add(td);
                for (int i = 0; i < rows * columns; i++)
                {
                    if (i % 4 == 0)
                    {
                        tr = new HtmlTableRow();
                        tbl.Rows.Add(tr);
                    }

                    if (i >= users.Count)
                    {
                        td.InnerHtml = "&nbsp;";
                    }
                    else
                    {
                        td.InnerHtml = users[i].Username;
                    }
                    tr.Cells.Add(td);
                    td = new HtmlTableCell();
                }
            }
        }

    }
}
