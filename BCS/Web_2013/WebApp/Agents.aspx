<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="Agents.aspx.cs" Inherits="Agents" Title="Berkley Clearance System - Agents" Theme="DefaultTheme" %>
<%@ MasterType VirtualPath="~/Site.PATS.master" %>
<%@ Register Src="WebUserControls/AutoNumberGenerator.ascx" TagName="AutoNumberGenerator" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <aje:ScriptManager id="ScriptManager1" runat="server"></aje:ScriptManager>
<table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Agents</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
   <%-- <asp:TextBox ID="txFilter" runat="server"></asp:TextBox>&nbsp;
    <asp:Button ID="Button1" runat="server" Text="Filter By Agency Number"  OnClick="Button1_Click" />--%>
    <table>
            <tr>
                <td>Agency Number</td>
                <td>
                    <asp:TextBox ID="txFilterAgencyNumber" runat="server"></asp:TextBox>
                </td>
                <td>Agency Name</td>
                <td>
                    <asp:TextBox ID="txFilterAgencyName" runat="server"></asp:TextBox>
                </td>
                <td>Agent Name</td>
                <td>
                    <asp:TextBox ID="txFilterAgentName" runat="server"></asp:TextBox>
                </td>
                <td>Agent Number</td>
                <td>
                    <asp:TextBox ID="txFilterAgentNumber" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Filter"  OnClick="Button1_Click" />
                </td>
            </tr>
        </table>
    <br />
    <br />
    <UserControls:Grid ID="gridAgents" runat="server" AllowPaging="True" EmptyDataText="No Agents were found." AutoGenerateColumns="False" OnRowDataBound="gridAgents_RowDataBound" CaptionAlign="Left" DataSourceID="odsAgents" OnSelectedIndexChanged="gridAgents_SelectedIndexChanged" OnSelectedIndexChanging="gridAgents_SelectedIndexChanging" DataKeyNames="Id" OnPageIndexChanged="gridAgents_PageIndexChanged" OnRowDeleted="gridAgents_RowDeleted">
        <Columns>
            <asp:CommandField ButtonType="Button" ShowSelectButton="True" SelectText='<%$ Code: BCS.WebApp.Components.ConfigValues.GetEditButtonDixplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId),false) %>' ControlStyle-Width="50px" />
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
            <asp:BoundField DataField="AgencyId" HeaderText="Agency Id" Visible="False" />
             <asp:TemplateField HeaderText="Agency Name">                
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "AgencyName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Agency Number">                
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval (Container.DataItem,"AgencyNumber") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="BrokerNo" HeaderText="Agent Number" />
            <asp:BoundField DataField="FirstName" HeaderText="First Name" />
            <asp:BoundField DataField="Surname" HeaderText="Last Name" />
            <asp:BoundField HeaderText="Primary Phone" DataField="PrimaryPhone" />                
            <%--<asp:CheckBoxField DataField="Active" ReadOnly="true" />--%>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                         Text="Delete" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.Agency) && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>'/>
                    <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="btnDelete"
                        ConfirmText='<%# DataBinder.Eval (Container.DataItem, "Surname", "Are you sure you want to delete this Agent {0}?") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </UserControls:Grid>
    <asp:ObjectDataSource ID="odsAgents" runat="server" DeleteMethod="DeleteAgent" SelectMethod="GetAgents"
        TypeName="BCS.Core.Clearance.Admin.DataAccess" OnDeleted="odsAgents_Deleted" OnSelected="odsAgents_Selected">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="txFilterAgencyNumber" Name="filterAgencyNumber" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txFilterAgencyName" Name="filterAgencyName" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txFilterAgentName" Name="filterAgentName" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txFilterAgentNumber" Name="filterAgentNumber" PropertyName="Text"
                Type="String" />
                <%--<asp:Parameter Name="filterAgentNumber" Type="String" />--%>
            <asp:SessionParameter Name="companyNumber" SessionField="CompanyNumber" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>    
    &nbsp;
    <br />
    <asp:Button ID="btnAdd" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.Agency) && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text="Add"  OnClick="btnAdd_Click" /><br />
    <br />
    
    <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
        <br />
    </asp:PlaceHolder>
    
    <asp:DetailsView ID="DetailsView1" runat="server"
        AutoGenerateRows="False" DataSourceID="odsAgent" Height="50px" OnItemDeleted="DetailsView1_ItemDeleted" OnItemInserted="DetailsView1_ItemInserted" OnItemUpdated="DetailsView1_ItemUpdated" OnItemUpdating="DetailsView1_ItemUpdating" DataKeyNames="Id" OnModeChanging="DetailsView1_ModeChanging" >
        <Fields>
            <asp:TemplateField HeaderText="Id" Visible="False">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Id") %>'></asp:Label>                 
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="FEIN/SSN" SortExpression="TaxId">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("TaxId") %>'></asp:TextBox>
                    <asp:RegularExpressionValidator
                        ID="RegularExpressionValidator111" runat="server" ControlToValidate="TextBox9"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* ######### or FEIN Format ##-####### or Social Security Number format ###-##-####"
                        ValidationExpression="\d{9}|[a-zA-Z\d]{2}-[a-zA-Z\d]{7}|\d{3}-\d{2}-\d{4}">
                    </asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("TaxId") %>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator111" runat="server" ControlToValidate="TextBox9"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* ######### or FEIN Format ##-####### or Social Security Number format ###-##-####"
                        ValidationExpression="\d{9}|[a-zA-Z\d]{2}-[a-zA-Z\d]{7}|\d{3}-\d{2}-\d{4}">
                    </asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label12" runat="server" Text='<%# Bind("TaxId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Agency">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="odsAgencies" DataTextField="AgencyName"
                        DataValueField="Id" SelectedValue='<%# Bind("AgencyId") %>'>
                    </asp:DropDownList><asp:ObjectDataSource ID="odsAgencies" runat="server" SelectMethod="GetAgencies"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="odsAgencies" DataTextField="AgencyName"
                        DataValueField="Id" SelectedValue='<%# Bind("AgencyId") %>'>
                    </asp:DropDownList><asp:ObjectDataSource ID="odsAgencies" runat="server" SelectMethod="GetAgencies"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" 
                    Text='<%# DataBinder.Eval (Container.DataItem, "Agency.AgencyName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Agent Number / Broker No">
                <EditItemTemplate>
                    <uc1:AutoNumberGenerator ID="txAgentNumber" runat="server" NumberControlTypeName="Agent Number" ReadableText="Agent Number"
                        Text='<%# Bind("BrokerNo") %>' Required="True" ShowStaticAsterisk="False" GeneratedType="AgentNumber"
                        CompanyNumberId='<%$ Code: Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>' />
                </EditItemTemplate>
                <InsertItemTemplate>
                    <uc1:AutoNumberGenerator ID="txAgentNumber" runat="server" NumberControlTypeName="Agent Number" ReadableText="Agent Number"
                        Text='<%# Bind("BrokerNo") %>' Required="True" ShowStaticAsterisk="False" GeneratedType="AgentNumber"
                        CompanyNumberId='<%$ Code: Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) %>'/>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Bind("BrokerNo") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Prefix" HeaderText="Prefix" />
            <asp:TemplateField HeaderText="First Name">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox4"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox4"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*"></asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Middle" HeaderText="Middle Initial" />
            <asp:TemplateField HeaderText="Last Name">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Surname") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="TextBox3"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Surname") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="TextBox3"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*"></asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Surname") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="SuffixTitle" HeaderText="Suffix" />
            <asp:TemplateField HeaderText="Date of Birth">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("DOB", "{0:MM/dd/yyyy}") %>' OnInit="DatePickerTextBox_Init"></asp:TextBox>
                    <asp:CompareValidator
                        ID="CompareValidator10" runat="server" ControlToValidate="TextBox10" Display="Dynamic"
                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                    <asp:RangeValidator ID="rvDOB" runat="server" SetFocusOnError="True" Display="Dynamic" ControlToValidate="TextBox10"
                        Type="Date" OnInit="rvDOB_Init" ErrorMessage="&nbsp;" Text="* Date of Birth cannot be greater than 12 years ago."></asp:RangeValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("DOB", "{0:MM/dd/yyyy}") %>' OnInit="DatePickerTextBox_Init"></asp:TextBox>
                    <asp:CompareValidator
                        ID="CompareValidator10" runat="server" ControlToValidate="TextBox10" Display="Dynamic"
                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                    <asp:RangeValidator ID="rvDOB" runat="server" SetFocusOnError="True" Display="Dynamic" ControlToValidate="TextBox10"
                        Type="Date" OnInit="rvDOB_Init" ErrorMessage="&nbsp;" Text="* Date of Birth cannot be greater than 12 years ago."></asp:RangeValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label13" runat="server" Text='<%# Bind("DOB", "{0:MM/dd/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Primary Phone">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PrimaryPhone") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox1"
                        ErrorMessage="&nbsp;" Text="* Wrong format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}" Display="Dynamic"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("PrimaryPhone") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox1"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>                    
                    <asp:Label ID="Label65" runat="server" Text='<%# Bind("PrimaryPhone") %> ' OnDataBinding="PhoneFormatControl_DataBinding"></asp:Label>                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fax">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox22" runat="server" Text='<%# Bind("Fax") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator33" runat="server" ControlToValidate="TextBox22"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Format." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox22" runat="server" Text='<%# Bind("Fax") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator44" runat="server" ControlToValidate="TextBox22"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Format" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label25" runat="server" Text='<%# Bind("Fax") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email" SortExpression="EmailAddress">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("EmailAddress") %>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="TextBox8"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("EmailAddress") %>'></asp:TextBox><asp:RegularExpressionValidator
                        ID="RegularExpressionValidator6" runat="server" ControlToValidate="TextBox8"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label11" runat="server" Text='<%# Bind("EmailAddress") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Secondary Phone">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("SecondaryPhone") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TextBox2"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Format" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("SecondaryPhone") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TextBox2"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Format" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))+\d{3}-\d{4}"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("SecondaryPhone") %>' OnDataBinding="PhoneFormatControl_DataBinding"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ProfessionalTitle" HeaderText="Professional Title" />
            <asp:BoundField DataField="FunctionalTitle" HeaderText="Functional Title" />
            <asp:BoundField DataField="DepartmentName" HeaderText="Department Name" />
            <asp:BoundField DataField="MailStopCode" HeaderText="Mail Stop Code" />
            <asp:TemplateField HeaderText="Effective Date" SortExpression="EffectiveDate">
                <EditItemTemplate>
                    <asp:TextBox ID="txEffectiveDate" runat="server" OnInit="DatePicker_InitToday" Text='<%# Bind("EffectiveDate", "{0:MM/dd/yyyy}") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEffectiveDate" runat="server" ControlToValidate="txEffectiveDate"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvEffectiveDate" runat="server" ControlToValidate="txEffectiveDate" Display="Dynamic"
                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txEffectiveDate" runat="server" OnInit="DatePicker_InitToday" Text='<%# Bind("EffectiveDate", "{0:MM/dd/yyyy}") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEffectiveDate" runat="server" ControlToValidate="txEffectiveDate"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvEffectiveDate" runat="server" ControlToValidate="txEffectiveDate"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbEffectiveDate" runat="server" Text='<%# Bind("EffectiveDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cancel Date" SortExpression="CancelDate">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" OnInit="DatePickerTextBox_Init" Text='<%# Bind("CancelDate", "{0:MM/dd/yyyy}") %>'></asp:TextBox>
                    <asp:CompareValidator
                        ID="CompareValidator1" runat="server" ControlToValidate="TextBox7" Display="Dynamic"
                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" OnInit="DatePickerTextBox_Init" Text='<%# Bind("CancelDate", "{0:MM/dd/yyyy}") %>'></asp:TextBox>
                    <asp:CompareValidator
                        ID="CompareValidator1" runat="server" ControlToValidate="TextBox7" Display="Dynamic"
                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label10" runat="server" Text='<%# Bind("CancelDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="PrimaryPhoneAreaCode" HeaderText="PrimaryPhoneAreaCode" Visible="False" />
            <asp:BoundField DataField="SecondaryPhoneAreaCode" HeaderText="SecondaryPhoneAreaCode" Visible="False" />
            <asp:CommandField ButtonType="Button" ShowEditButton="True" ShowInsertButton="True" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="odsAgent" runat="server" SelectMethod="GetAgent" TypeName="BCS.Core.Clearance.Admin.DataAccess" InsertMethod="AddAgent" OnInserting="odsAgent_Inserting" OnUpdating="odsAgent_Updating" UpdateMethod="UpdateAgent" DataObjectTypeName="BCS.Core.Clearance.BizObjects.Agent" OnDeleted="odsAgent_Deleted" OnInserted="odsAgent_Inserted" OnUpdated="odsAgent_Updated">
        <SelectParameters>
            <asp:ControlParameter ControlID="gridAgents" Name="id" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
    
</asp:Content>
