<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="SwitchClient.aspx.cs" Inherits="SwitchClient" Title="Berkley Clearance System - Switch Client" Theme="DefaultTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <table cellpadding="0" cellspacing="0" class="box">
        <tbody>
            <tr>
                <td class="TL"><div /></td>
                <td class="TC"><span>Switch Client <asp:Label ID="lblOperation" runat="server"></asp:Label></span></td>
                <td class="TR"><div /></td>
            </tr>
            <tr>
                <td class="ML"><div /></td>
                <td class="MC">
                <span id="info" runat="server"></span>
                    <table cellpadding="5">        
                        <tr>
                            <td align="right">
                                Client Id:
                            </td>
                            <td>
                                <asp:TextBox ID="txtClientId" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtClientId"
                                    Display="Dynamic" ErrorMessage="*" ValidationGroup="Get"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="btnGetClient" runat="server" Text="Switch Client" OnClick="btnGetClient_Click" ValidationGroup="Get" AccessKey="S"/>&nbsp;
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click"/>
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="pnlClientInfo" Visible="false" runat="server">
                        <table runat="server" id="tblClientInfo" cellpadding="8" cellspacing="0" width="100%" style="border: gray 1px dotted;">
                            <tr>
                                <td>
                                    <asp:HiddenField ID="hfClientId" runat="server" />
                                    <asp:HiddenField ID="hfPortfolioId" runat="server" />
                                    <h3 class="sectionGroupHeader">Client Information:</h3>
                                    <asp:Panel ID="pnlClientNames" runat="server">
                                        
                                        <h4 class="sectionGroupHeader">Client Names</h4>                                        
                                        <%-- region old client name handling--%>
                                        <%--<asp:BulletedList ID="blNames" runat="server"></asp:BulletedList>
                                        <asp:RadioButtonList ID="rblNames" runat="server"></asp:RadioButtonList>
                                        <asp:RequiredFieldValidator ID="rfvNames" runat="server" ControlToValidate="rblNames"
                                            ErrorMessage="Please select a Name." Display="Dynamic" ValidationGroup="Save" Enabled="False"></asp:RequiredFieldValidator>--%>
                                        <%-- end region old client name handling--%>
                                        <%-- region new client name handling--%>
                                        <asp:CheckBoxList ID="cblNames" runat="server"></asp:CheckBoxList>
                                        <UserControls:RequiredFieldValidatorForCheckBoxLists ID="RequiredFieldValidator3" runat="server" ControlToValidate="cblNames"
                                            ErrorMessage="Please select a Name." Display="Dynamic" ValidationGroup="Save" SetFocusOnError="true"></UserControls:RequiredFieldValidatorForCheckBoxLists>
                                        <%-- end region new client name handling--%>
                                        
                                        <h4 class="sectionGroupHeader">Client Addresses</h4>
                                        <asp:Panel ID="pnlClientAddress" runat="server">
                                        <%--region new client address handling--%>
                                        <asp:CheckBoxList ID="cblAddresses" runat="server"></asp:CheckBoxList>
                                        <UserControls:RequiredFieldValidatorForCheckBoxLists ID="RequiredFieldValidator2" runat="server" ControlToValidate="cblAddresses"
                                            ErrorMessage="Please select an Address." Display="Dynamic" ValidationGroup="Save" SetFocusOnError="true"></UserControls:RequiredFieldValidatorForCheckBoxLists>
                                        
                                        <%-- end region new client address handling--%>
                                        <%--region old client address handling--%>
                                        <%--<asp:RadioButtonList ID="rblAddresses" runat="server"></asp:RadioButtonList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="rblAddresses"
                                            ErrorMessage="Please select an Address." Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>--%>
                                        <%--end region old client address handling--%>
                                        </asp:Panel>
                                        <asp:Label ID="lblPortfolioId" runat="server"></asp:Label>
                                        <br />
                                        <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" OnClick="btnSave_Click" />
                                        <asp:Button ID="Button1" runat="server" CausesValidation="False" OnClick="btnCancel_Click" Text="Cancel" /></asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td class="MR"><div /></td>
            </tr>
            <tr>
                <td class="BL"><div /></td>
                <td class="BC"><div /></td>
                <td class="BR"><div /></td>
            </tr>
        </tbody>
    </table>
</asp:Content>

