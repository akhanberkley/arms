#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.Components;
#endregion

public partial class AgencyAssignments : System.Web.UI.Page
{
    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlAgencies.DataBind();
            DropDownList2.DataBind(); 
        }
    } 
    #endregion

    #region Control Events
    protected void ddlAgencies_Init(object sender, EventArgs e)
    {
        (sender as BCS.WebApp.UserControls.AgencyDropDownList).CompanyNumberId = Session["CompanyNumberSelected"] != null ? Convert.ToInt32(Session["CompanyNumberSelected"]) : 0;
    }
    protected void ddlAgencies_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbPrimary.Checked = IsPrimaryCombination();
        DropDownList2.DataBind();
        DropDownList2.Items.Remove(ddlAgencies.SelectedItem);
        btnCopy.Enabled = DropDownList2.Items.Count > 1 && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        btnMove.Enabled = DropDownList2.Items.Count > 1 && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
    }
    protected void ddl_PreRender(object sender, EventArgs e)
    {
        DropDownList ddl = sender as DropDownList;
        if (ddl.Items.FindByText("") == null)
            ddl.Items.Insert(0, new ListItem("", "0"));
    }
    protected void btnCopy_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;
        if (ddlAgencies.SelectedValue == DropDownList2.SelectedValue)
            return;
        string result = BCS.Core.Clearance.Admin.DataAccess.CopyPersonLOBURAgency(
            Convert.ToInt32(ddlAgencies.SelectedValue),
            Convert.ToInt32(DropDownList2.SelectedValue));

        Common.SetStatus(messagePlaceHolder, result, true);
    }
    protected void btnMove_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;
        string result = BCS.Core.Clearance.Admin.DataAccess.MovePersonLOBURAgency(
            Convert.ToInt32(ddlAgencies.SelectedValue),
            Convert.ToInt32(DropDownList2.SelectedValue));
        Common.SetStatus(messagePlaceHolder, result, true);

        ListBox1.DataBind();
        ListBox2.DataBind();
        ListBox3.DataBind();
        ListBox4.DataBind();
        ListBox5.DataBind();
        ListBox6.DataBind();
    }
    protected void ListBox1_DataBound(object sender, EventArgs e)
    {

    }
    protected void SortOnPreRender(object sender, EventArgs e)
    {
        ListBox lb = sender as ListBox;
        ListItem[] items = Common.SortListItems(lb.Items);

        lb.Items.Clear();
        lb.Items.AddRange(items);
    }
    protected void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListBox3.ClearSelection();
        ListBox5.ClearSelection();
        cbPrimary.Checked = IsPrimaryCombination();
    }    
    protected void ListBox3_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbPrimary.Checked = IsPrimaryCombination();
    }    
    protected void ListBox5_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbPrimary.Checked = IsPrimaryCombination();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        if (Common.GetValues(ListBox3).Length == 0)
        {
            Common.SetStatus(messagePlaceHolder,
                "Please add line of business to save.");
            return;
        }
        if (Common.GetValues(ListBox5).Length == 0)
        {
            Common.SetStatus(messagePlaceHolder,
                "Please add roles to save.");
            return;
        }
        if (cbPrimary.Checked)
        {
            if (
                ListBox1.GetSelectedIndices().Length == 0 ||
                ListBox3.GetSelectedIndices().Length == 0 ||
                ListBox5.GetSelectedIndices().Length == 0)
            {
                Common.SetStatus(messagePlaceHolder,
                "Please select the line of business and role to be set as primary.");
                return;
            }
            if (
               ListBox3.GetSelectedIndices().Length > 1 ||
               ListBox5.GetSelectedIndices().Length > 1)
            {
                Common.SetStatus(messagePlaceHolder,
               "Please select only one line of business and one role as primary.");
                return;
            }
        }
        
        string result = BCS.Core.Clearance.Admin.DataAccess.UpdateAgencyLOBURPerson(
            Convert.ToInt32(ListBox1.SelectedValue),
            ConvertToInt32Array(new string[] { ddlAgencies.SelectedValue }),
            ConvertToInt32Array(Common.GetValues(ListBox3)),
            ConvertToInt32Array(Common.GetValues(ListBox5)),
            ListBox1.SelectedIndex > -1 && ListBox3.SelectedIndex > -1 && ListBox5.SelectedIndex > -1
            ? string.Concat(ddlAgencies.SelectedValue, "|", ListBox3.SelectedValue, "|", ListBox5.SelectedValue) : string.Empty,
            cbPrimary.Checked);

        Common.SetStatus(messagePlaceHolder, result, true);
        if (result == "Agency Line of Business Underwriting Person Roles were successfully updated.")
        {
            btnCopy.Attributes.Remove("onclick");
            btnMove.Attributes.Remove("onclick");
        }
        RemoveFromUnsavedPersonAssignments(ListBox1.SelectedValue);
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("AgencyAssignments.aspx");
    }

    protected void btnAddPerson_Click(object sender, EventArgs e)
    {
        ListItem[] items = Common.GetSelectedItems(ListBox1);
        if (items.Length == 0)
            return;

        ListBox2.Items.AddRange(items);
        foreach (ListItem item in items)
        {
            ListBox1.Items.Remove(item);            
        }

        if (!IsUnsavedPersonAssignment(items[0].Value))
        {
            string result = BCS.Core.Clearance.Admin.DataAccess.UpdateAgencyLOBURPerson(
                Convert.ToInt32(items[0].Value),
                new int[] { Convert.ToInt32(ddlAgencies.SelectedValue) },
                new int[] { },
                new int[] { }, string.Empty, false);

            Common.SetStatus(messagePlaceHolder, result, true);
        }

        foreach (ListItem item in items)
        {
            RemoveFromUnsavedPersonAssignments(item.Value);
        }

        LoadLOBs();
        if (UnsavedPersonAssignmentsExist())
        {
            btnCopy.Attributes.Add("onclick", "javascript:alert('Please save before copying.'); return false;");
            btnMove.Attributes.Add("onclick", "javascript:alert('Please save before copying.'); return false;");
        }
        else
        {
            btnCopy.Attributes.Remove("onclick");
            btnMove.Attributes.Remove("onclick");
        }
    }
    protected void btnRemovePerson_Click(object sender, EventArgs e)
    {
        ListItem[] items = Common.GetSelectedItems(ListBox2);

        foreach (ListItem item in items)
        {
            item.Selected = false;
        }
        ListBox1.Items.AddRange(items);
        foreach (ListItem item in items)
        {
            ListBox2.Items.Remove(item);
            AddToUnsavedPersonAssignments(item.Value);
        }

        if (UnsavedPersonAssignmentsExist())
        {
            btnCopy.Attributes.Add("onclick", "javascript:alert('Please save before copying.'); return false;");
            btnMove.Attributes.Add("onclick", "javascript:alert('Please save before copying.'); return false;");
        }
        else
        {
            btnCopy.Attributes.Remove("onclick");
            btnMove.Attributes.Remove("onclick");
        }
    }
    protected void btnAddLOB_Click(object sender, EventArgs e)
    {
        ListItem[] items = Common.GetSelectedItems(ListBox3);

        ListBox4.Items.AddRange(items);
        foreach (ListItem item in items)
            ListBox3.Items.Remove(item);
        btnCopy.Attributes.Add("onclick", "javascript:alert('Please save before copying.'); return false;");
        btnMove.Attributes.Add("onclick", "javascript:alert('Please save before copying.'); return false;");
    }
    protected void btnRemoveLOB_Click(object sender, EventArgs e)
    {
        ListItem[] items = Common.GetSelectedItems(ListBox4);

        ListBox3.Items.AddRange(items);
        foreach (ListItem item in items)
            ListBox4.Items.Remove(item);
        btnCopy.Attributes.Add("onclick", "javascript:alert('Please save before copying.'); return false;");
        btnMove.Attributes.Add("onclick", "javascript:alert('Please save before copying.'); return false;");
    }
    protected void btnAddRole_Click(object sender, EventArgs e)
    {
        ListItem[] items = Common.GetSelectedItems(ListBox5);
        ListBox6.Items.AddRange(items);
        foreach (ListItem item in items)
            ListBox5.Items.Remove(item);
        btnCopy.Attributes.Add("onclick", "javascript:alert('Please save before copying.'); return false;");
        btnMove.Attributes.Add("onclick", "javascript:alert('Please save before copying.'); return false;");
    }
    protected void btnRemoveRole_Click(object sender, EventArgs e)
    {
        ListItem[] items = Common.GetSelectedItems(ListBox6);
        if (items == null || items.Length == 0)
            return;
        ListBox5.Items.AddRange(items);
        foreach (ListItem item in items)
            ListBox6.Items.Remove(item);
        btnCopy.Attributes.Add("onclick", "javascript:alert('Please save before copying.'); return false;");
        btnMove.Attributes.Add("onclick", "javascript:alert('Please save before copying.'); return false;");
    }
    protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
    {
        if (ddlAgencies.SelectedIndex < 1 || DropDownList2.SelectedIndex < 1)
        {
            args.IsValid = false;
            CustomValidator1.ErrorMessage = "Please select From and To agencies.";
            return;
        }
    }
    protected void odsP_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        IdentifyInactive(e.ReturnValue as BCS.Biz.PersonCollection);
    }

    protected void odsNP_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        IdentifyInactive(e.ReturnValue as BCS.Biz.PersonCollection);
    }
    #endregion
    
    #region Other Methods
    private void LoadLOBs()
    {
        ListBox3.Items.Clear();
        odsLob.SelectParameters["agencyIds"].DefaultValue = ddlAgencies.SelectedValue;
        odsLob.DataBind();
        ListBox3.DataBind();

        ListBox4.Items.Clear();
        odsNLOB.SelectParameters["agencyIds"].DefaultValue = ddlAgencies.SelectedValue;
        odsNLOB.DataBind();
        ListBox4.DataBind();
    }
    private bool IsPrimaryCombination()
    {
        if (ListBox1.SelectedIndex > -1 && ListBox3.SelectedIndex > -1 && ListBox5.SelectedIndex > -1)
        {
            return BCS.Core.Clearance.Admin.DataAccess.IsPrimaryPerson(
                Convert.ToInt32(ListBox1.SelectedValue), Convert.ToInt32(ddlAgencies.SelectedValue),
                Convert.ToInt32(ListBox3.SelectedValue), Convert.ToInt32(ListBox5.SelectedValue));
        }
        return false;
    }
    private System.Collections.Specialized.StringCollection GetUnsavedPersonAssignments()
    {
        if (ViewState["UnsavedPersonAssignments"] == null)
        {
            return new System.Collections.Specialized.StringCollection();
        }
        else
        {
            return ViewState["UnsavedPersonAssignments"] as System.Collections.Specialized.StringCollection;
        }
    }
    private void AddToUnsavedPersonAssignments(string Id)
    {
        if (ViewState["UnsavedPersonAssignments"] == null)
        {
            System.Collections.Specialized.StringCollection sc = new System.Collections.Specialized.StringCollection();
            sc.Add(Id);
            ViewState["UnsavedPersonAssignments"] = sc;
        }
        else
        {
            System.Collections.Specialized.StringCollection Ids =
                ViewState["UnsavedPersonAssignments"] as System.Collections.Specialized.StringCollection;
            Ids.Add(Id);
        }
    }
    private void RemoveFromUnsavedPersonAssignments(string Id)
    {
        System.Collections.Specialized.StringCollection sc = ViewState["UnsavedPersonAssignments"] as System.Collections.Specialized.StringCollection;
        if (sc != null)
        {
            sc.Remove(Id);
        }
        ViewState["UnsavedPersonAssignments"] = sc;
    }
    private bool UnsavedPersonAssignmentsExist()
    {
        System.Collections.Specialized.StringCollection sc = ViewState["UnsavedPersonAssignments"] as System.Collections.Specialized.StringCollection;
        if (sc == null || sc.Count == 0)
            return false;
        return true;
    }
    private bool IsUnsavedPersonAssignment(string Id)
    {
        System.Collections.Specialized.StringCollection sc = ViewState["UnsavedPersonAssignments"] as System.Collections.Specialized.StringCollection;
        if (sc == null || sc.Count == 0)
            return false;
        return sc.Contains(Id);
    }
    private int[] ConvertToInt32Array(string[] values)
    {
        ArrayList al = new ArrayList();
        foreach (string s in values)
        {
            al.Add(Convert.ToInt32(s));
        }
        int[] ivalues = new int[values.Length];
        al.CopyTo(ivalues);
        return ivalues;
    }
    private void IdentifyInactive(BCS.Biz.PersonCollection persons)
    {
        foreach (BCS.Biz.Person person in persons)
        {
            if (!Common.IsActive(person, DateTime.Today))
                person.FullName = string.Format("{0} ({1})", person.FullName, "inactive");
        }
    }
    #endregion
}
