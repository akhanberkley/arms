<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="AgencyLOBs.aspx.cs" Inherits="AgencyLOBs" Title="Berkley Clearance System - Agency Line Of Business" Theme="DefaultTheme" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
<asp:PlaceHolder runat="server" ID="messagePlaceHolder">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
    <br />
</asp:PlaceHolder>
   <table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Agency Underwriting Units</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        
    <table>
        <tr>
            <td>
                Agency Number
            </td>
            <td>
                <asp:TextBox ID="txFilter" runat="server" ToolTip="Enter text to filter by Agency Number"></asp:TextBox>
            </td>
            <td>
                Underwriting Unit</td>
            <td>
                 <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="ObjectDataSource1"
                    DataTextField="Code" DataValueField="Id" OnPreRender="DropDownList1_PreRender"
                    ToolTip="Select to filter by Line Of Business">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetLineOfBusinessesNotByAgencyId"
                    TypeName="BCS.Core.Clearance.Admin.DataAccess">
                    <SelectParameters>
                        <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                        <asp:Parameter DefaultValue="-1" Name="agencyId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td>
                <asp:CheckBox ID="chkUnassignedOnly" runat="server" Text="Unassigned Only" />
            </td>
            <td>
            <asp:Button ID="Button1" runat="server"  OnClick="Button1_Click" Text="Filter" />
            </td>
        </tr>
    </table>
    <br />
    <UserControls:Grid ID="gridAgencies" runat="server" AllowPaging="True" AutoGenerateColumns="False" CaptionAlign="Left" DataKeyNames="Id"
        DataSourceID="odsAgencies" EmptyDataText="No Agencies were found." OnRowDataBound="gridAgencies_RowDataBound"
        OnSelectedIndexChanging="gridAgencies_SelectedIndexChanging" OnPageIndexChanged="gridAgencies_PageIndexChanged">
        <Columns>
            <asp:CommandField ButtonType="Button" ShowSelectButton="True" SelectText='<%$ Code: BCS.WebApp.Components.ConfigValues.GetEditButtonDixplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId),false) %>' ControlStyle-Width="50px" />
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
            <asp:BoundField DataField="CompanyNumberId" HeaderText="Company Number Id" Visible="False" />
            <asp:TemplateField HeaderText="Company Number">                
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CompanyNumber.PropertyCompanyNumber") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="AgencyName" HeaderText="Agency Name" />
            <asp:BoundField DataField="AgencyNumber" HeaderText="Agency Number" />
            <asp:BoundField DataField="ContactPerson" HeaderText="Contact Person" />
            <asp:BoundField DataField="FEIN" HeaderText="FEIN" />
            <asp:TemplateField ShowHeader="False" Visible="False">
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                         Text="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </UserControls:Grid>
    <asp:ObjectDataSource ID="odsAgencies" runat="server"
        SelectMethod="GetAgencies" TypeName="BCS.Core.Clearance.Admin.DataAccess" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:ControlParameter ControlID="txFilter" Name="filterAgencyNumber" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="DropDownList1" Name="filterLOBId" PropertyName="SelectedValue"
                Type="Int32" />
            <asp:ControlParameter ControlID="chkUnassignedOnly" Name="unassignedOnly" PropertyName="Checked" />
            <asp:SessionParameter DefaultValue="0" Name="companyId" SessionField="CompanySelected"
                Type="Int32" />
        </SelectParameters>       
    </asp:ObjectDataSource>
    
    <table id="atable" runat="server">
        <tr>
            <td>Agency UW Units</td>
            <td></td>
            <td>All UW Units</td>
        </tr>
        <tr>
            <td>
                <asp:ListBox ID="ListBox1" runat="server" DataSourceID="odsLOBAgency" DataTextField="Code"
                    DataValueField="Id" SelectionMode="Multiple" Width="250px"></asp:ListBox><asp:ObjectDataSource ID="odsLOBAgency" runat="server"
                        SelectMethod="GetLineOfBusinessesByAgencyId" TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                            <asp:ControlParameter ControlID="gridAgencies" Name="agencyId" PropertyName="SelectedValue"
                                Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
            </td>
             <td>
                <table cellspacing="4">
                    <tr>
                        <td>
                            <asp:Button ID="btnRemove" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text=">" OnClick="btnRemove_Click" />
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text="<" OnClick="btnAdd_Click" />
                        </td>
                    </tr>
                </table>
             </td>            
             <td>
                 <asp:ListBox ID="ListBox2" runat="server" DataSourceID="odsLOBs" DataTextField="Code"
                     DataValueField="Id" SelectionMode="Multiple" Width="250px"></asp:ListBox><asp:ObjectDataSource ID="odsLOBs" runat="server"
                         SelectMethod="GetLineOfBusinessesNotByAgencyId" TypeName="BCS.Core.Clearance.Admin.DataAccess">
                         <SelectParameters>
                             <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                             <asp:ControlParameter ControlID="gridAgencies" Name="agencyId" PropertyName="SelectedValue"
                                 Type="Int32" />
                         </SelectParameters>
                     </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:Button ID="btnSave" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId))%>' OnClick="btnSave_Click" Text="Update" />
                <asp:Button ID="btnCancel" runat="server"  OnClick="btnSave_Click" Text="Cancel" />
            </td>
        </tr>
    </table>
    
    </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
</asp:Content>

