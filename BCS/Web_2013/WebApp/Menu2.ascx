<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Menu2.ascx.cs" Inherits="BCS.WebApp.Menu2" %>
<table cellspacing="0" cellpadding="0" border="0" class="tabControl">
    <tbody>
    <tr>
        <% if (showSearchTab)
           { %>
        <td class="tdMenuOffLeft"><div /></td>
        <td nowrap="nowrap" class="tabTextOff">
            <% if (showSearch) { %>
            <a href=".?clearsearch=true" accesskey="h"  onmouseover="showMenu('sub_search_nav',6)" onfocus="showMenu('sub_search_nav',6)" onmouseout="hideMenu('sub_search_nav',6);" onblur="hideMenu('sub_search_nav',6);">SEARC<span style="text-decoration: underline;">H</span></a>

            <br />
            <div id="sub_search_nav" class="sub_navigation" style="width:125px;">
                <ul>
                     <% if ((Page.User as BCS.Core.Security.CustomPrincipal).IsInAnyRoles("User", "Partial User", "Auditor", "System Administrator")) { %>
                    <li onmouseover="showMenu('sub_search_nav',6)" onmouseout="hideMenu('sub_search_nav',6);"><a href="default.aspx">Client Search</a></li>
                    <li onmouseover="showMenu('sub_search_nav',6)" onmouseout="hideMenu('sub_search_nav',6);"><a href="SubmissionSearchNew.aspx">Submission Search</a></li>
                    <% } %>
                </ul>
            </div>

            <% } else { %>
            <span class="disabled">SEARCH</span>
            <% } %>
        </td>
        <td class="tdMenuOffRight"><div /></td>
        <% } %>
        
        <% if (showSubmissionsTab)
           { %>
        <td class="tdMenuOffLeft"><div /></td>
        <td nowrap="nowrap" class="tabTextOff">
            <a href="#" accesskey="u" onclick="return false;" onmouseover="showMenu('sub_submission_nav',1)" onfocus="showMenu('sub_submission_nav',1)" onmouseout="hideMenu('sub_submission_nav',1);" onblur="hideMenu('sub_submission_nav',1);">S<span style="text-decoration: underline;">U</span>BMISSIONS</a>
            <br />
            <div id="sub_submission_nav" class="sub_navigation">
                <ul>
                    <% if (showLoad) { %>
                    <li onmouseover="showMenu('sub_submission_nav',1)" onmouseout="hideMenu('sub_submission_nav',1);"><a href="SubSearch.aspx">Load Submission</a></li>
                    <% } %>
                    <% if (showReAssignLink) { %>
                    <li onmouseover="showMenu('sub_submission_nav',1)" onmouseout="hideMenu('sub_submission_nav',1);"><a href="SubmissionAssignments.aspx">Reassign Submissions</a></li>
                    <% } %>
                    <% if (showSwitchLink) { %>
                    <li onmouseover="showMenu('sub_submission_nav',1)" onmouseout="hideMenu('sub_submission_nav',1);"><a href="ClientSwitches.aspx">Switch Client</a></li>
                    <% } %>
                    <% if (showGeneratorLink) { %>
                    <li onmouseover="showMenu('sub_submission_nav',1)" onmouseout="hideMenu('sub_submission_nav',1);"><asp:LinkButton runat="server" ID="lbPolicyNumberGenerator" OnClientClick="confirmGen(event);" OnClick="lbPolicyNumberGenerator_Click" CausesValidation="false">Policy Number Generator</asp:LinkButton></li>
                    <% } %>
                </ul>
            </div>
        <td class="tdMenuOffRight"><div /></td>
        <% } %>

        <% if (showAgenciesTab)
           { %>
        <td class="tdMenuOffLeft"><div /></td>
        <td nowrap class="tabTextOff">
        <a href="#" accesskey="i" onclick="return false;" onmouseover="showMenu('sub_agency_nav',2)" onfocus="showMenu('sub_agency_nav',2)" onmouseout="hideMenu('sub_agency_nav',2);" onblur="hideMenu('sub_agency_nav',2);">AGENC<span style="text-decoration: underline;">I</span>ES</a>
            <br />
            <div id="sub_agency_nav" class="sub_navigation">
                <ul>
                    <% if (showAgencyLink) { %>
                    <li onmouseover="showMenu('sub_agency_nav',2)" onmouseout="hideMenu('sub_agency_nav',2);"><a href="Agencies.aspx">Agencies</a></li>
                    <% } %>
                    <% if (showAgentLink) { %>
                    <li onmouseover="showMenu('sub_agency_nav',2)" onmouseout="hideMenu('sub_agency_nav',2);"><a href="Agents.aspx">Agents</a></li>
                    <% } %>
                    <% if (showContactLink) { %>
                    <li onmouseover="showMenu('sub_agency_nav',2)" onmouseout="hideMenu('sub_agency_nav',2);"><a href="Contacts.aspx">Agency Contacts</a></li>
                    <% } %>
                </ul>
            </div>
        </td>       
        <td class="tdMenuOffRight"><div /></td>
        <% } %>
        
        <% if (showUWsTab)
           { %>
        <td class="tdMenuOffLeft"><div /></td>
        <td nowrap="nowrap" class="tabTextOff">
            <a href="#" accesskey="g" onclick="return false;" onmouseover="showMenu('sub_underwritingunits_nav',3)" onfocus="showMenu('sub_underwritingunits_nav',3)" onmouseout="hideMenu('sub_underwritingunits_nav',3);" onblur="hideMenu('sub_underwritingunits_nav',3);">UNDERWRITIN<span style="text-decoration: underline;">G</span> UNITS</a>
            <br />
            <div id="sub_underwritingunits_nav" class="sub_navigation">
                <ul>
                    <% if (showUWULink) { %>
                    <li onmouseover="showMenu('sub_underwritingunits_nav',3)" onmouseout="hideMenu('sub_underwritingunits_nav',3);"><a href="UWU.aspx">UW Units</a></li>
                    <% } %>
                    <% if (showAgencyLOBLink) { %>
                    <li onmouseover="showMenu('sub_underwritingunits_nav',3)" onmouseout="hideMenu('sub_underwritingunits_nav',3);"><a href="AgencyLOBs.aspx">Agency</a></li>
                    <% } %>
                </ul>
            </div>
        </td>
        <td class="tdMenuOffRight"><div /></td>
        <% } %>
        
        <% if (showAssignmentsTab)
           { %>
        <td class="tdMenuOffLeft"><div /></td>
        <td nowrap="nowrap" class="tabTextOff">
            <a href="#" accesskey="t" onclick="return false;" onmouseover="showMenu('sub_assignments_nav',4)" onfocus="showMenu('sub_assignments_nav',4)" onmouseout="hideMenu('sub_assignments_nav',4);" onblur="hideMenu('sub_assignments_nav',4);">ASSIGNMEN<span style="text-decoration: underline;">T</span>S</a>
            <br />
            <div id="sub_assignments_nav" class="sub_navigation">
                <ul>
                    <% if (showPersonAssignLink) { %>	
                    <li onmouseover="showMenu('sub_assignments_nav',4)" onmouseout="hideMenu('sub_assignments_nav',4);"><a href="PersonAssignments.aspx">By Person</a></li>
                    <% } %>
                    <% if (showAgencyAssignLink) { %>
                    <li onmouseover="showMenu('sub_assignments_nav',4)" onmouseout="hideMenu('sub_assignments_nav',4);"><a href="AgencyAssignments.aspx">By Agency</a></li>
                    <% } %>
                </ul>
            </div>
        </td>
        <td class="tdMenuOffRight"><div /></td>
        <% } %>
        
        <% if (showAdministrationTab) { %>
        <td class="tdMenuOffLeft"><div /></td>
        <td nowrap="nowrap" class="tabTextOff">
            <a href="#" accesskey="m" onclick="return false;" onmouseover="showMenu('sub_administration_nav',5)" onfocus="showMenu('sub_administration_nav',5)" onmouseout="hideMenu('sub_administration_nav',5);" onblur="hideMenu('sub_administration_nav',5);">AD<span style="text-decoration: underline;">M</span>INISTRATION</a>
            <br />
            <div id="sub_administration_nav" class="sub_navigation">
                <ul>
                    <% if (showAttrLink) { %>
                    <li onmouseover="showMenu('sub_administration_nav',5)" onmouseout="hideMenu('sub_administration_nav',5);"><a href="Attributes.aspx">Attributes</a></li>
                    <% } %>
                    <% if (showClassCodesLink) { %>
                    <li onmouseover="showMenu('sub_administration_nav',5)" onmouseout="hideMenu('sub_administration_nav',5);"><a href="ClassCodes.aspx">Class Codes</a></li>
                    <% } %>
                    <% if (showCompanyNumberCodeLink) { %>
                    <li onmouseover="showMenu('sub_administration_nav',5)" onmouseout="hideMenu('sub_administration_nav',5);"><a href="CompanyNoCodes.aspx">Company Codes</a></li>
                    <% } %>
                    <% if (showPersonLink) { %>
                    <li onmouseover="showMenu('sub_administration_nav',5)" onmouseout="hideMenu('sub_administration_nav',5);"><a href="Persons.aspx">Personnel Editor</a></li>
                    <% } %>
                    <% if (showUserLink) { %>
                    <li onmouseover="showMenu('sub_administration_nav',5)" onmouseout="hideMenu('sub_administration_nav',5);"><a href="CompanyUsers.aspx">Users</a></li>
                    <% } %>
                    <% if (showLinkLink) { %>
                    <li onmouseover="showMenu('sub_administration_nav',5)" onmouseout="hideMenu('sub_administration_nav',5);"><a href="Links.aspx">Links</a></li>
                    <% } %>
                </ul>
            </div>
        </td>
        <td class="tdMenuOffRight"><div /></td>
        <% } %>
        <% if (usesFAQ)
           { %>
            <td class="tdMenuOffLeft"><div /></td>
            <td nowrap="nowrap" class="tabTextOff">
                <a href="/faq.aspx" accesskey="h">FAQ</a>
            </td>
            <td class="tdMenuOffRight"><div /></td>
        <%} %>
        <% if (showAdminLink) { %>
        <td><div class="tdMenuOnLeft"><div /></td>
        <td nowrap="nowrap" class="tabTextOn"><asp:HyperLink AccessKey="n" ID="lnkAdmin" runat="server" Target="_blank" NavigateUrl="~/AdminWeb/Users.aspx">SYSTEM ADMI<span style="text-decoration: underline;">N</span>ISTRATION</asp:HyperLink></td>
        <td class="tdMenuOnRight"><div /></td>
        <% } %>
    </tr>
    </tbody>
</table>

<script type="text/javascript">
    var menuTimers = new Array(7);
    
    function showMenu(id,index){
        if (menuTimers[index]) clearTimeout(menuTimers[index]);
        
        document.getElementById(id).style.display = "block";
    }
    
    function hideMenu(id,index){
        if (menuTimers[index]) clearTimeout();
        
        menuTimers[index] = setTimeout("closeMenu('"+id+"');",600);
    }
    
    function closeMenu(id){
        document.getElementById(id).style.display = "none";
    }
</script>
<script type="text/javascript">
function confirmGen(scwEvt)
{
    if ( ! confirm('Are you sure you want to generate a policy number?' ) )
     {         
         if (scwEvt.stopPropagation)
                scwEvt.stopPropagation();    // Capture phase
         else   scwEvt.cancelBubble = true;  // Bubbling phase

         if (scwEvt.preventDefault)
                scwEvt.preventDefault();     // Capture phase
         else   scwEvt.returnValue=false;    // Bubbling phase
     }
}
</script>