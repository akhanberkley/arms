<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" Async="true" CodeFile="SubmissionShell.aspx.cs" Inherits="SubmissionShell" Title="Berkley Clearance System - Create Shell Submission" Theme="DefaultTheme" %>
<%@ MasterType VirtualPath="~/Site.PATS.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        function openAgencySearch(ctl, c, dt)
        {
            openWindow('SubmissionAgencySearch.aspx?ctl='+ctl+'&c='+c+'&dt='+dt,'SubmissionAgencySearch',600,230);
        }
    </script>    
    <aje:ScriptManager id="ScriptManager1" runat="server" OnAsyncPostBackError="ScriptManager1_AsyncPostBackError"></aje:ScriptManager>
    <table cellpadding="0" cellspacing="0" class="box">
        <tbody>
            <tr>
                <td class="TL"><div /></td>
                <td class="TC">Create Shell Submission:</td>
                <td class="TR"><div /></td>
            </tr>
            <tr>
                <td class="ML"><div /></td>
                <td class="MC">
                    <p><em>Note that you can change the submission details at a later time.</em></p>
                    <table>
                        <tr>
                            <td>Using Client Id:&nbsp;<asp:Label Font-Bold="true" ID="lblClientId" runat="server"></asp:Label></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td>Submission Type:<br /><UserControls:SubmissionTypeDropDownList id="SubmissionTypeDropDownList1" runat="server" AutoPostBack="False"></UserControls:SubmissionTypeDropDownList>
                            <asp:RequiredFieldValidator ID="rfvSubmissionType" runat="server" ControlToValidate="SubmissionTypeDropDownList1"
                                ErrorMessage="*" SetFocusOnError="True" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr id="submission_row_effective_date">
                            <td>Effective Date:<br />
                                <asp:TextBox ID="txEffectiveDate" runat="server" AutoCompleteType="Disabled" AutoPostBack="True" OnTextChanged="txEffectiveDate_TextChanged"></asp:TextBox>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txEffectiveDate"
                                    Display="Dynamic" ErrorMessage="mm/dd/yyyy" Operator="DataTypeCheck" SetFocusOnError="True"></asp:CompareValidator></td>
                        </tr>
                        <tr>
                            <td><span>Agency:</span><br />
                            <UserControls:AgencyCityStateDropDownList id="AgencyDropDownList1" runat="server" SortBy="AgencyName" AutoPostBack="True" OnSelectedIndexChanged="AgencyDropDownList1_SelectedIndexChanged" ActiveOnly="True"></UserControls:AgencyCityStateDropDownList>
                            <asp:RequiredFieldValidator ID="rfvAgency" runat="server" ControlToValidate="AgencyDropDownList1" ErrorMessage="*" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>                                        
                            <ajx:ListSearchExtender ID="ListSearchExtender2" runat="server"
                                TargetControlID="AgencyDropDownList1" PromptPosition="Right">
                            </ajx:ListSearchExtender>
                            <br />
                            <asp:Label runat="server" ID="lblAgencyComment"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Underwriting Unit:<br />
                                <UserControls:LineOfBusinessDropDownList ID="LineOfBusinessDropDownList1" runat="server" AutoPostBack="True" OnPreRender="LineOfBusinessDropDownList1_PreRender" OnSelectedIndexChanged="LineOfBusinessDropDownList1_SelectedIndexChanged"></UserControls:LineOfBusinessDropDownList>
                                <asp:RequiredFieldValidator ID="rfvLineOfBusiness" runat="server" ErrorMessage="*" ControlToValidate="LineOfBusinessDropDownList1" SetFocusOnError="True" InitialValue="0"></asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td>Submission Status:<br />
                            <UserControls:SubmissionStatusDropDownList id="SubmissionStatusDropDownList1" runat="server"></UserControls:SubmissionStatusDropDownList></td>
                        </tr>
                        <tr>
                            <td><br />
                            <asp:Button ID="btnSubmitSubmission" runat="server" Text="Add Submission (Shell)"  AccessKey="S" OnClick="btnSubmitSubmission_Click" />
                                &nbsp;&nbsp;
                                <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" CausesValidation="False" /></td>
                        </tr>
                    </table>
                </td>
                <td class="MR"><div /></td>
            </tr>
            <tr>
                <td class="BL"><div /></td>
                <td class="BC"><div /></td>
                <td class="BR"><div /></td>
            </tr>
        </tbody>
    </table>
</asp:Content>

