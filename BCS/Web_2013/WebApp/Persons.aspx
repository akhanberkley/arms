<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="Persons.aspx.cs" Inherits="Persons" Title="Berkley Clearance System : Persons" Theme="DefaultTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <aje:ScriptManager id="ScriptManager1" runat="server"></aje:ScriptManager>
<table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Personnel Editor</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        User Name &nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="tbUsername" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
        Full Name &nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="tbFullname" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnFilter" runat="server" Text="Filter"  OnClick="btnFilter_Click" />
        <br /><br />
        
        <UserControls:Grid ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id"
        DataSourceID="odsPersons" OnPageIndexChanged="GridView1_PageIndexChanged" OnRowDeleted="GridView1_RowDeleted" AllowPaging="True" EmptyDataText="No persons exist for the company and company number selected." OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
        <Columns>
            <asp:CommandField ButtonType="Button" ShowSelectButton="True" SelectText='<%$ Code: BCS.WebApp.Components.ConfigValues.GetEditButtonDixplayText(Common.GetSafeIntFromSession(SessionKeys.CompanyId),false) %>' ControlStyle-Width="50px" />
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
            <asp:BoundField DataField="CompanyNumberId" HeaderText="CompanyNumberId" Visible="False" />
            <asp:TemplateField HeaderText="Company Number">                
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CompanyNumber.PropertyCompanyNumber") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="UserName" HeaderText="User Name" />
            <asp:BoundField DataField="FullName" HeaderText="Full Name" />
            <asp:BoundField DataField="EmailAddress" HeaderText="Email Address" />            
            <asp:TemplateField HeaderText="Retirement Date">
                <ItemTemplate>
                    <asp:Label ID="Label131" runat="server" Text='<%# Bind("RetirementDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" CommandName="Delete" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>'  Text="Delete" />
                <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="btnDelete"
                    ConfirmText='<%# DataBinder.Eval (Container.DataItem, "FullName", "Are you sure you want to delete this person \"{0}\"?") %>'>
                </ajx:ConfirmButtonExtender>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </UserControls:Grid>
    <asp:ObjectDataSource ID="odsPersons" runat="server" SelectMethod="GetPersons" TypeName="BCS.Core.Clearance.Admin.DataAccess" DeleteMethod="DeletePerson" OnDeleted="odsPersons_Deleted">
        <SelectParameters>
            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
            <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected" Type="Int32" />
            <asp:ControlParameter ControlID="tbUsername" Name="username" PropertyName="Text" />
            <asp:ControlParameter ControlID="tbFullname" Name="fullname" PropertyName="Text" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
    <br />
    <asp:Button ID="btnAdd" runat="server"  OnClick="btnAdd_Click" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' Text="Add" /><br />
    <br />
    <asp:PlaceHolder runat="server" ID="messagePlaceHolder">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
        <br />
    </asp:PlaceHolder>
    
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="odsPerson"
        OnItemInserted="DetailsView1_ItemInserted" OnItemUpdated="DetailsView1_ItemUpdated" OnItemUpdating="DetailsView1_ItemUpdating" OnItemInserting="DetailsView1_ItemInserting" OnModeChanging="DetailsView1_ModeChanging">
        <Fields>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
            <asp:TemplateField HeaderText="CompanyNumberId" Visible="False">
                <EditItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="ObjectDataSource1"
                        DataTextField="PropertyCompanyNumber" DataValueField="Id" SelectedValue='<%# Bind("CompanyNumberId") %>'>
                    </asp:DropDownList><asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetCompanyNumbers"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                            <asp:Parameter DefaultValue="false" Name="companyDependent" Type="Boolean" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="ObjectDataSource1"
                        DataTextField="PropertyCompanyNumber" DataValueField="Id" SelectedValue='<%# Bind("CompanyNumberId") %>'>
                    </asp:DropDownList><asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetCompanyNumbers"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:SessionParameter Name="companyId" SessionField="CompanySelected" Type="Int32" />
                            <asp:Parameter DefaultValue="false" Name="companyDependent" Type="Boolean" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("CompanyNumberId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="User Name">
                <EditItemTemplate>
                    <asp:TextBox ID="txtUserName" runat="server" Text='<%# Bind("UserName") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ControlToValidate="txtUserName"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Email Format" SetFocusOnError="True" ></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="txtUserName" runat="server" Text='<%# Bind("UserName") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ControlToValidate="txtUserName"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="*" SetFocusOnError="True" ></asp:RequiredFieldValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="FullName" HeaderText="Full Name" />
            <asp:BoundField DataField="Initials" HeaderText="Initials" />
            <asp:TemplateField HeaderText="Email Address">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("EmailAddress") %>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox1"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Email Format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("EmailAddress") %>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox1"
                        Display="Dynamic" ErrorMessage="&nbsp;" Text="* Wrong Email Format" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("EmailAddress") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>            
            <asp:TemplateField HeaderText="Retirement Date">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("RetirementDate", "{0:MM/dd/yyyy}") %>' OnInit="DatePickerTextBox_Init"></asp:TextBox>
                    <asp:CompareValidator
                        ID="CompareValidator10" runat="server" ControlToValidate="TextBox10" Display="Dynamic"
                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("RetirementDate", "{0:MM/dd/yyyy}") %>' OnInit="DatePickerTextBox_Init"></asp:TextBox>
                    <asp:CompareValidator
                        ID="CompareValidator10" runat="server" ControlToValidate="TextBox10" Display="Dynamic"
                        ErrorMessage="&nbsp;" Text="* Invalid Date" Operator="DataTypeCheck" Type="Date"></asp:CompareValidator>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label13" runat="server" Text='<%# Bind("RetirementDate", "{0:MM/dd/yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField HeaderText="Active">
                <EditItemTemplate>
                    <asp:CheckBox ID="cbActive" runat="server" Checked='<%# Bind("Active") %>'></asp:CheckBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:CheckBox ID="cbActive" runat="server" Checked='<%# Bind("Active") %>'></asp:CheckBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Active") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:CommandField ButtonType="Button" ShowEditButton='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' ShowInsertButton='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) %>' />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource ID="odsPerson" runat="server" SelectMethod="GetPerson" TypeName="BCS.Core.Clearance.Admin.DataAccess" DataObjectTypeName="BCS.Core.Clearance.BizObjects.Person" InsertMethod="AddPerson" OnInserted="odsPerson_Inserted" OnUpdated="odsPerson_Updated" UpdateMethod="UpdatePerson">
        <SelectParameters>
            <asp:ControlParameter ControlID="GridView1" Name="id" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
    
</asp:Content>

