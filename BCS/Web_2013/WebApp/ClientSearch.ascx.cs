#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Policy;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;
using Microsoft.Web.Services2.Security.X509;

using BTS.ClientCore.Wrapper;
using System.Collections.Generic;
using Structures = BTS.ClientCore.Wrapper.Structures;
using BCS.WebApp.Components;
#endregion

namespace BCS.WebApp.UserControls
{
    public partial class UserControls_ClientSearch : System.Web.UI.UserControl
    {
        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            //txtPhoneNum.Attributes.Add("onInit", "callScript()");
            if (!IsPostBack)
            {
                LoadAdvClientSearchOptions();
                // Add initial.
                UserControls_ClientNameSelector nameCtrl = AddClientNameWebcontrol();
                UserControls_ClientAddress addrCtrl = AddClientAddressWebcontrol();
                
                BCS.Core.Security.CustomPrincipal user = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
                bool isRemoteAgency = user.HasAgency();
                
                addrCtrl.Enabled = !isRemoteAgency;
                ibAddAddress.Enabled = !isRemoteAgency;
                ibRemoveName.Enabled = false;
                ibRemoveAddress.Enabled = false;
                txClientId.Enabled = !isRemoteAgency;
                txPolicyNumber.Enabled = !isRemoteAgency;

                nameCtrl.SetFocus();
            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            if( string.IsNullOrEmpty(somelit.Text))
            {
                // since the master page is loaded after all page controls, this text might escape when the control is being loaded.
                // so if there is no text query again
                somelit.Text = DefaultValues.GetSpecialSearchText(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
            }
            base.OnPreRender(e);
        }
        #endregion

        #region Control Events
        protected void ibAddName_Click(object sender, ImageClickEventArgs e)
        {
            UserControls_ClientNameSelector nameCtrl = AddClientNameWebcontrol();
            nameCtrl.SetFocus();
            // has more than one name control, so enable remove
            ibRemoveName.Enabled = true;
        }
        protected void ibRemoveName_Click(object sender, ImageClickEventArgs e)
        {
            Control controltoremove = null;
            int i = 0;
            foreach (Control c in SearchByNamePlaceHolder.Controls)
            {
                if (c is BCS.WebApp.UserControls.UserControls_ClientNameSelector)
                {
                    i++;
                    controltoremove = c;
                }
            }
            if (i > 1)
                SearchByNamePlaceHolder.Controls.Remove(controltoremove);

            // disable removing if only one name control
            ibRemoveName.Enabled = (i - 1 != 1);
        }
        protected void ibAddAddress_Click(object sender, ImageClickEventArgs e)
        {
            UserControls_ClientAddress addrCtrl = AddClientAddressWebcontrol();
            addrCtrl.SetFocus();
            // has more than one address control, so enable remove
            ibRemoveAddress.Enabled = true;
        }
        protected void ibRemoveAddress_Click(object sender, ImageClickEventArgs e)
        {
            Control controltoremove = null;
            int i = 0;
            foreach (Control c in SearchByAddressPlaceHolder.Controls)
            {
                if (c is BCS.WebApp.UserControls.UserControls_ClientAddress)
                {
                    i++;
                    controltoremove = c;
                }
            }
            if (i > 1)
                SearchByAddressPlaceHolder.Controls.Remove(controltoremove);

            // disable removing if only one address control
            ibRemoveAddress.Enabled = (i - 1 != 1);
        }

        protected void btnAddThisClient_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Client.aspx?op=addPhone");
        }
        #endregion

        #region Other Methods
        private UserControls_ClientNameSelector AddClientNameWebcontrol()
        {
            Control clientNameSelectorUC = LoadControl("ClientNameSelector.ascx");
            clientNameSelectorUC.ID = string.Concat("ClientNameSelector",GetNextNameControlId());            

            // Add the controls
            SearchByNamePlaceHolder.Controls.Add(clientNameSelectorUC);
            ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)clientNameSelectorUC).ShowFullName = false;

            return (UserControls_ClientNameSelector)clientNameSelectorUC;
        }
        private UserControls_ClientAddress AddClientAddressWebcontrol()
        {
            Control sba = LoadControl("ClientAddress.ascx");
            sba.ID = string.Concat("ClientAddress", GetNextAddressControlId());

            SearchByAddressPlaceHolder.Controls.Add(sba);

            ((UserControls_ClientAddress)sba).CountryValue = "USA";

            return (UserControls_ClientAddress)sba;
        }

        private int GetNextNameControlId()
        {
            if (ViewState["NextNameControlId"] == null)
            {
                ViewState["NextNameControlId"] = 0;
                return 0;
            }
            int idSeq = (int)ViewState["NextNameControlId"];
            ViewState["NextNameControlId"] = ++idSeq;
            return idSeq;
        }

        private int GetNextAddressControlId()
        {
            if (ViewState["NextAddressControlId"] == null)
            {
                ViewState["NextAddressControlId"] = 0;
                return 0;
            }
            int idSeq = (int)ViewState["NextAddressControlId"];
            ViewState["NextAddressControlId"] = ++idSeq;
            return idSeq;
        }
        private void LoadSearchCriteria(SavedSearch backToSearch)
        {
            ViewState["NextAddressControlId"] = 0;
            ViewState["NextNameControlId"] = 0;

            txPolicyNumber.Text = backToSearch.PolicyNumber;
            txFein.Text = backToSearch.TaxIds;
            txClientId.Text = backToSearch.SearchedClient.Info.ClientId;
            txPortfolioId.Text = backToSearch.PortfolioIds;

            AddClientAddressWebcontrol(SearchByAddressPlaceHolder, backToSearch.SearchedClient.Addresses);
            AddClientNameWebcontrol(SearchByNamePlaceHolder, backToSearch.SearchedClient.Names);
        }
        private void AddClientAddressWebcontrol(Control parent, BTS.ClientCore.Wrapper.Structures.Search.AddressVersion[] addresses)
        {
            if (addresses.Length > 0)
                parent.Controls.Clear();
            if (addresses != null)
            {
                foreach (BTS.ClientCore.Wrapper.Structures.Search.AddressVersion av in addresses)
                {
                    #region required controls to display a client address
                    Control clientAddressUC = LoadControl("ClientAddress.ascx");
                    clientAddressUC.ID = string.Concat("ClientAddress", GetNextAddressControlId());

                    parent.Controls.Add(clientAddressUC);

                    #endregion
                    if (av.HouseNumber != null)
                        ((UserControls_ClientAddress)parent.FindControl(clientAddressUC.ID)).HouseValue = av.HouseNumber;
                    if (av.Address1 != null)
                        ((UserControls_ClientAddress)parent.FindControl(clientAddressUC.ID)).StreetnameValue = av.Address1;
                    if (av.Address2 != null)
                        ((UserControls_ClientAddress)parent.FindControl(clientAddressUC.ID)).Address2Value = av.Address2;
                    if (av.City != null)
                        ((UserControls_ClientAddress)parent.FindControl(clientAddressUC.ID)).CityValue = av.City;
                    if (av.CountryCode != null)
                        ((UserControls_ClientAddress)parent.FindControl(clientAddressUC.ID)).CountryValue = av.CountryCode;
                    if (av.County != null)
                        ((UserControls_ClientAddress)parent.FindControl(clientAddressUC.ID)).CountyValue = av.County;
                    if (av.StateProvCode != null)
                        ((UserControls_ClientAddress)parent.FindControl(clientAddressUC.ID)).StateValue = av.StateProvCode;
                    if (av.PostalCode != null)
                        ((UserControls_ClientAddress)parent.FindControl(clientAddressUC.ID)).ZipCodeValue = av.PostalCode;
                }
            }
        }
        private void AddClientNameWebcontrol(Control parent, ClientNameSearch[] names)
        {
            if (names.Length > 0)
                parent.Controls.Clear();
            foreach (ClientNameSearch name in names)
            {
                #region required controls to display a client name
                Control clientNameSelectorUC = LoadControl("ClientNameSelector.ascx");
                clientNameSelectorUC.ID = string.Concat("ClientNameSelector", GetNextNameControlId());

                // Add the controls
                parent.Controls.Add(clientNameSelectorUC);
                ((UserControls_ClientNameSelector)clientNameSelectorUC).ShowFullName = false;


                #endregion

                if (name.ClientName.NameBusiness != null)
                {
                    ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).BusinessNameValue = name.ClientName.NameBusiness;
                    ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).CityValue = name.City;
                    ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).StateValue = name.State;

                }
                else
                {
                    ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).IsPersonal = true;

                    if (name.ClientName.NameFirst != null)
                        ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).FirstNameValue = name.ClientName.NameFirst;
                    if (name.ClientName.NameLast != null)
                        ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).LastNameValue = name.ClientName.NameLast;
                    if (name.ClientName.NameMiddle != null)
                        ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).MiddleNameValue = name.ClientName.NameMiddle;
                    if (name.ClientName.NamePrefix != null)
                        ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).NamePrefixValue = name.ClientName.NamePrefix;
                    if (name.ClientName.NameSuffix != null)
                        ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).NameSuffixValue = name.ClientName.NameSuffix;
                }
            }
        }

        private void LoadAdvClientSearchOptions()
        {
            int companyId = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
            BCS.DAL.CompanyClientAdvancedSearch advSrchAttributes = BCS.Core.Clearance.SubmissionsEF.GetAdvSearchAttributesByCompany(companyId);
            if(advSrchAttributes!= null)
              pnlAdvClientSearch.Visible = advSrchAttributes.UsesReversePhoneLookup;
        }
        #endregion

        #region Properties
        public SavedSearch SearchCriteria
        {
            set
            {
                LoadSearchCriteria(value);
            }
        }

        public string PortfolioIds
        {
            get { return this.txPortfolioId.Text; }
            set { this.txPortfolioId.Text = value; }
        }
        #endregion

        #region Readonly Properties (control accessibility)
        public string TaxIds
        {
            get { return this.txFein.Text; }
        }

        public string PortfolioSearchType
        {
            get { return this.rblPortfolioSearchType.SelectedValue; }
        }

        public string ClientId
        {
            get { return this.txClientId.Text; }
        }

        public string PolicyNumber
        {
            get { return this.txPolicyNumber.Text; }
        }

        public string PhoneNumber
        {
            get { return this.txtPhoneNum.Text.Trim(); }
        }

        #region search params
        public ArrayList AddressVersionsToSearchOld
        {
            get
            {
                ArrayList al = new ArrayList();

                foreach (Control control in SearchByAddressPlaceHolder.Controls)
                {
                    if (control is BCS.WebApp.UserControls.UserControls_ClientAddress)
                    {
                        string houseValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).HouseValue;
                        string streetnameValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).StreetnameValue;
                        string address2value = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).Address2Value;
                        string cityValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).CityValue;
                        string stateValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).StateValue;
                        string zipCodeValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).ZipCodeValue;

                        BTS.ClientCore.Wrapper.Structures.Search.AddressVersion av =
                            new BTS.ClientCore.Wrapper.Structures.Search.AddressVersion();

                        av.HouseNumber = houseValue;
                        av.Address1 = streetnameValue;
                        av.Address2 = address2value;
                        av.City = cityValue;
                        av.StateProvCode = stateValue;
                        av.PostalCode = zipCodeValue;
                        if (av.HouseNumber.Length == 0 && av.Address1.Length == 0 && av.Address2.Length == 0 && av.City.Length == 0
                            && av.PostalCode.Length == 0)
                            continue;

                        al.Add(av);
                    }
                }

                return al;
            }
        }


        public ArrayList ClientNamesToSearchOld
        {
            get
            {
                ArrayList al = new ArrayList();

                foreach (Control control in SearchByNamePlaceHolder.Controls)
                {
                    if (control is BCS.WebApp.UserControls.UserControls_ClientNameSelector)
                    {
                        string nameBusiness = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).BusinessNameValue;
                        string nameFirst = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).FirstNameValue;
                        string nameLast = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).LastNameValue;
                        string city = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).CityValue;
                        string state = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).StateValue;

                        ClientNameSearch cn = new ClientNameSearch();
                        cn.ClientName = new BTS.ClientCore.Wrapper.Structures.Search.ClientName();

                        if (((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).IsBusiness)
                        {
                            cn.ClientName.NameBusiness = nameBusiness;
                            cn.City = city;
                            cn.State = state;
                        }
                        else
                        {
                            cn.ClientName.NameFirst = nameFirst;
                            cn.ClientName.NameLast = nameLast;
                        }
                        BCS.Core.Security.CustomPrincipal user = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
                        bool isRemoteAgency = user.HasAgency();
                        if (isRemoteAgency)
                        {
                            // if remote agency, then anything not entered in the name control would be incomplete search criteria. not handled for individual clients
                            if (string.IsNullOrEmpty(cn.ClientName.NameBusiness) || string.IsNullOrEmpty(cn.City) || string.IsNullOrEmpty(cn.State))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            if (cn.ClientName.NameLast == null && cn.ClientName.NameFirst == null && cn.ClientName.NameBusiness == null)
                                continue;
                        }
                        al.Add(cn);
                    }
                }

                return al;
            }
        }

        public List<Structures.Search.AddressVersion> AddressVersionsToSearch
        {
            get
            {
                List<Structures.Search.AddressVersion> al = new List<Structures.Search.AddressVersion>();

                foreach (Control control in SearchByAddressPlaceHolder.Controls)
                {
                    if (control is BCS.WebApp.UserControls.UserControls_ClientAddress)
                    {
                        string houseValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).HouseValue;
                        string streetnameValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).StreetnameValue;
                        string address2value = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).Address2Value;
                        string cityValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).CityValue;
                        string stateValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).StateValue;
                        string zipCodeValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).ZipCodeValue;

                        BTS.ClientCore.Wrapper.Structures.Search.AddressVersion av =
                            new BTS.ClientCore.Wrapper.Structures.Search.AddressVersion();

                        av.HouseNumber = houseValue;
                        av.Address1 = streetnameValue;
                        av.Address2 = address2value;
                        av.City = cityValue;
                        av.StateProvCode = stateValue;
                        av.PostalCode = zipCodeValue;
                        if (av.HouseNumber.Length == 0 && av.Address1.Length == 0 && av.Address2.Length == 0 && av.City.Length == 0
                            && av.PostalCode.Length == 0)
                            continue;

                        al.Add(av);
                    }
                }

                return al;
            }
        }


        public List<ClientNameSearch> ClientNamesToSearch
        {
            get
            {
                List<ClientNameSearch> al = new List<ClientNameSearch>();

                foreach (Control control in SearchByNamePlaceHolder.Controls)
                {
                    if (control is BCS.WebApp.UserControls.UserControls_ClientNameSelector)
                    {
                        string nameBusiness = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).BusinessNameValue;
                        string nameFirst = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).FirstNameValue;
                        string nameLast = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).LastNameValue;
                        string city = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).CityValue;
                        string state = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).StateValue;

                        ClientNameSearch cn = new ClientNameSearch();
                        cn.ClientName = new BTS.ClientCore.Wrapper.Structures.Search.ClientName();

                        if (((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).IsBusiness)
                        {
                            cn.ClientName.NameBusiness = nameBusiness;
                            cn.City = city;
                            cn.State = state;
                        }
                        else
                        {
                            cn.ClientName.NameFirst = nameFirst;
                            cn.ClientName.NameLast = nameLast;
                        }
                        BCS.Core.Security.CustomPrincipal user = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
                        bool isRemoteAgency = user.HasAgency();
                        if (isRemoteAgency)
                        {
                            // if remote agency, then anything not entered in the name control would be incomplete search criteria. not handled for individual clients
                            if (string.IsNullOrEmpty(cn.ClientName.NameBusiness) || string.IsNullOrEmpty(cn.City) || string.IsNullOrEmpty(cn.State))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            if (cn.ClientName.NameLast == null && cn.ClientName.NameFirst == null && cn.ClientName.NameBusiness == null)
                                continue;
                        }
                        al.Add(cn);
                    }
                }

                return al;
            }
        }
        #endregion

        #region add params
        public ArrayList AddressVersionsToAdd
        {
            get
            {
                ArrayList al = new ArrayList();

                foreach (Control control in SearchByAddressPlaceHolder.Controls)
                {
                    if (control is BCS.WebApp.UserControls.UserControls_ClientAddress)
                    {
                        string houseValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).HouseValue;
                        string streetnameValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).StreetnameValue;
                        string address2value = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).Address2Value;
                        string cityValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).CityValue;
                        string stateValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).StateValue;
                        string zipCodeValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).ZipCodeValue;

                        BTS.ClientCore.Wrapper.Structures.Add.AddressVersion av =
                            new BTS.ClientCore.Wrapper.Structures.Add.AddressVersion();

                        av.HouseNumber = houseValue;
                        av.Address1 = streetnameValue;
                        av.Address2 = address2value;
                        av.City = cityValue;
                        av.StateProvCode = stateValue;
                        av.PostalCode = zipCodeValue;
                        if (av.HouseNumber.Length == 0 && av.Address1.Length == 0 && av.City.Length == 0
                            && av.StateProvCode.Length == 0 && av.PostalCode.Length == 0)
                            continue;

                        al.Add(av);
                    }
                }

                return al;
            }
        }


        public ArrayList ClientNamesToAdd
        {
            get
            {
                ArrayList al = new ArrayList();

                foreach (Control control in SearchByNamePlaceHolder.Controls)
                {
                    if (control is BCS.WebApp.UserControls.UserControls_ClientNameSelector)
                    {
                        string nameBusiness = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).BusinessNameValue;
                        string nameFirst = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).FirstNameValue;
                        string nameLast = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).LastNameValue;

                        BTS.ClientCore.Wrapper.Structures.Add.ClientName cn =
                            new BTS.ClientCore.Wrapper.Structures.Add.ClientName();

                        if (((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).IsBusiness)
                        {
                            cn.NameBusiness = nameBusiness;
                        }
                        else
                        {
                            cn.NameFirst = nameFirst;
                            cn.NameLast = nameLast;
                        }
                        al.Add(cn);
                    }                   
                }

                return al;
            }
        }
        #endregion

        #region modify params
        public ArrayList AddressVersionsToModify
        {
            get
            {
                ArrayList al = new ArrayList();

                foreach (Control control in SearchByAddressPlaceHolder.Controls)
                {
                    if (control is BCS.WebApp.UserControls.UserControls_ClientAddress)
                    {
                        string houseValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).HouseValue;
                        string streetnameValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).StreetnameValue;
                        string address2value = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).Address2Value;
                        string cityValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).CityValue;
                        string stateValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).StateValue;
                        string zipCodeValue = ((BCS.WebApp.UserControls.UserControls_ClientAddress)control).ZipCodeValue;

                        BTS.ClientCore.Wrapper.Structures.Import.AddressInfo av =
                            new BTS.ClientCore.Wrapper.Structures.Import.AddressInfo();

                        av.HouseNumber = houseValue;
                        av.ADDR1 = streetnameValue;
                        av.ADDR2 = address2value;
                        av.CityName = cityValue;
                        av.StateCode = stateValue;
                        av.Zip = zipCodeValue;

                        if (av.HouseNumber.Length == 0 && av.ADDR1.Length == 0 && av.ADDR2.Length == 0 && av.CityName.Length == 0
                           && av.StateCode.Length == 0 && av.Zip.Length == 0)
                            continue;

                        al.Add(av);
                    }
                }

                return al;
            }
        }


        public ArrayList ClientNamesToModify
        {
            get
            {
                ArrayList al = new ArrayList();

                foreach (Control control in SearchByNamePlaceHolder.Controls)
                {
                    if (control is BCS.WebApp.UserControls.UserControls_ClientNameSelector)
                    {
                        string nameBusiness = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).BusinessNameValue;
                        string nameFirst = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).FirstNameValue;
                        string nameLast = ((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).LastNameValue;

                        BTS.ClientCore.Wrapper.Structures.Import.NameInfo cn =
                            new BTS.ClientCore.Wrapper.Structures.Import.NameInfo();

                        if (((BCS.WebApp.UserControls.UserControls_ClientNameSelector)control).IsBusiness)
                        {
                            cn.BusinessName = nameBusiness;
                        }
                        else
                        {
                            cn.FirstName = nameFirst;
                            cn.LastName = nameLast;
                        }
                        al.Add(cn);
                    }
                }

                return al;
            }
        }
        #endregion

        #endregion

}
}