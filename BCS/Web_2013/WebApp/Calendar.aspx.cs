using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace BCS.WebApp
{
	/// <summary>
	/// Summary description for Calendar.
	/// </summary>
	public partial class Calendar : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			control.Value = HttpContext.Current.Request.QueryString["control"].ToString();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void Calendar1_SelectionChanged(object sender, System.EventArgs e)
		{
			string calendarCode = "<script>window.opener.document.forms(0)." + control.Value + ".value = '";
			calendarCode += Calendar1.SelectedDate.ToString("MM/dd/yyyy");
			calendarCode += "';self.close()";
			calendarCode += "</script>"; //Don't ask, tool bug.

			ClientScript.RegisterClientScriptBlock(this.GetType(),"__runCalendar", calendarCode, false);
		}
	}
}
