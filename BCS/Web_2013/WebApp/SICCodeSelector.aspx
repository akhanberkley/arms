<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SICCodeSelector.aspx.cs" StylesheetTheme="defaultTheme" Theme="DefaultTheme" Title="Select SIC Code" Inherits="SICCodeSelector" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Berkley Clearance System - Select SIC Code</title>
    <link href="Styles/pats_style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
    function CloseFormOK()
    {
        if (window.opener && !window.opener.closed)
        {
            var controlid1 = queryString('control1');
            var controlid2 = queryString('control2');
            var controlid3 = queryString('control3');
            var controlid4 = queryString('control4');
            var controlid5 = queryString('control5');
            var controlid6 = queryString('control6');
            
            var ss = document.getElementById('ddlCodes').value.split("|");
            window.opener.document.getElementById(controlid1).value = ss[1];
            window.opener.document.getElementById(controlid2).value = ss[0];
            var v = document.getElementById('ddlCodes').options[document.getElementById('ddlCodes').selectedIndex].text;
            window.opener.document.getElementById(controlid3).value = v.substring(0, v.indexOf('('));

            var sp = document.getElementById('ddlNaicsCodes').value.split("|");
            window.opener.document.getElementById(controlid4).value = sp[1];
            window.opener.document.getElementById(controlid5).value = sp[0];
            var s = document.getElementById('ddlNaicsCodes').options[document.getElementById('ddlNaicsCodes').selectedIndex].text;
            window.opener.document.getElementById(controlid6).value = s.substring(0, s.indexOf('('));



        }
        self.close();
    }
    function CloseFormCancel()
    {
        self.close();
    }
    
    function CheckParentWindow()
    {
        if (window.opener == null || window.opener.closed)
        {
            self.close();
        }
    }
    
    function PageQuery(q) {
        if(q.length > 1) this.q = q.substring(1, q.length);
        else this.q = null;
        this.keyValuePairs = new Array();
        if(q) {
            for(var i=0; i < this.q.split("&").length; i++) {
            this.keyValuePairs[i] = this.q.split("&")[i];
            }
        }
        this.getKeyValuePairs = function() { return this.keyValuePairs; }
        this.getValue = function(s) {
        for(var j=0; j < this.keyValuePairs.length; j++) {
            if(this.keyValuePairs[j].split("=")[0] == s)
                return this.keyValuePairs[j].split("=")[1];
        }
        return false;
        }
        this.getParameters = function() {
        var a = new Array(this.getLength());
        for(var j=0; j < this.keyValuePairs.length; j++) {
        a[j] = this.keyValuePairs[j].split("=")[0];
        }
        return a;
        }
        this.getLength = function() { return this.keyValuePairs.length; }
    }
    function queryString(key){
        var page = new PageQuery(window.location.search);
        return unescape(page.getValue(key));
    }
    </script>
</head>
<body onfocus="CheckParentWindow();" class="pageBody">
    <form id="form1" runat="server">
        
        <table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Select SIC Code Below</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
    
        <table cellpadding="2" cellspacing="2" width="100%" border="1" class="grid">
            <tr>
                <td align="right">Division</td>
                <td width="100%"><asp:DropDownList Font-Size="11px" ID="ddlDivisions" runat="server" OnSelectedIndexChanged="ddlDivisions_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></td>
            </tr>
            <tr> 
                <td align="right">Group</td>
                <td width="100%"><asp:DropDownList Font-Size="11px" ID="ddlGroups" runat="server" OnSelectedIndexChanged="ddlGroups_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></td>
            </tr>
            <tr>
                <td align="right">SIC Code</td>
                <td width="100%"><asp:DropDownList Font-Size="11px" ID="ddlCodes" runat="server" OnSelectedIndexChanged="ddlCodes_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></td>
            </tr>
            <tr>
             <td align="right">NAICS Code</td>
             <td width="100%"><asp:DropDownList Font-Size="11px" ID="ddlNaicsCodes" runat="server"></asp:DropDownList></td>
            </tr>
        </table>
        <br />
            <input id="Button3" type="button" value="Use Selected SIC Code" class="button" onclick="CloseFormOK();" />&nbsp;
            <input id="Button2" type="button" value="Cancel" class="button" onclick="CloseFormCancel();" />
     
     </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
     
    </form>
</body>
</html>
