using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using BCS.WebApp.Components;
using BCS.Core;

using TSHAK.Components;
using System.Security.Principal;

namespace BCS.WebApp
{
	/// <summary>
	/// Summary description for NoAccess.
	/// </summary>
	public partial class NoAccess : System.Web.UI.Page
	{
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Request.QueryString.Count > 0 && Request.QueryString["data"] != null)
			{
				SecureQueryString qs = new SecureQueryString(Core.Core.Key, Request.QueryString["data"]);

				lblSecurityEvent.Text = qs["SecurityEvent"].ToString();
			}
		}
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            long agency = 0;
            Core.Security.CustomPrincipal principal = new BCS.Core.Security.CustomPrincipal(User.Identity,
                    Components.Security.GetUserRoles(tbUser.Text, out agency), agency);

            if (principal.Roles != null && principal.Roles.Length > 0)
            {
                bool b = System.Web.Security.FormsAuthentication.Authenticate(tbUser.Text, tbPassword.Text);
                if (b)
                {
                    System.Web.Security.FormsAuthentication.SetAuthCookie(tbUser.Text, false);
                    System.Web.Security.FormsAuthentication.RedirectFromLoginPage(tbUser.Text, false);
                }
                else
                {
                    lblSecurityEvent.Text = "Invalid Login";
                    System.Web.Security.FormsAuthentication.SignOut();
                }
            }
            
            HttpContext.Current.User = principal;
        }
}
}
