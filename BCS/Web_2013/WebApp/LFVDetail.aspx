<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LFVDetail.aspx.cs" Inherits="LFVDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Log Entry Detail</title>
    <style type="text/css">
        .pageTitle
        {
            border-top-width: thin;
            font-weight: bold;
            border-left-width-value: thin;
            border-left-width-ltr-source: physical;
            border-left-width-rtl-source: physical;
            font-size: 20pt;
            border-left-color-value: black;
            border-left-color-ltr-source: physical;
            border-left-color-rtl-source: physical;
            border-bottom-width: thin;
            border-bottom-color: black;
            width: 100%;
            color: white;
            border-top-color: black;
            background-color: #000033;
            text-align: center;
            border-right-width-value: thin;
            border-right-width-ltr-source: physical;
            border-right-width-rtl-source: physical;
            border-right-color-value: black;
            border-right-color-ltr-source: physical;
            border-right-color-rtl-source: physical;
        }
        .ERROR
        {
            color : Red;
        }
        input
        {
	        border: #ab975c 1px solid;
	        font-size: 10pt;
	        filter: progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr= '#FFFFFFFF' , EndColorStr= '#FFEEDDAA' );
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <span class="pageTitle">Log Entry Detail</span>
        <br /><br /><br />
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" CellPadding="4" ForeColor="#333333" GridLines="None" OnDataBound="DetailsView1_DataBound" >
            <Fields>
                <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True"
                    SortExpression="Id" Visible="False" />
                <asp:BoundField DataField="Date" HeaderText="Date" SortExpression="Date" />
                <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level" />
                <asp:BoundField DataField="Logger" HeaderText="Logger" SortExpression="Logger" />
                <asp:BoundField DataField="Application" HeaderText="Application" SortExpression="Application" />
                <asp:BoundField DataField="Environment" HeaderText="Environment" SortExpression="Environment" />
                <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" />
                <asp:BoundField DataField="Exception" HeaderText="Exception" SortExpression="Exception" />
            </Fields>
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
            <EditRowStyle BackColor="#7C6F57" />
            <RowStyle BackColor="#E3EAEB" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:DetailsView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString='"data source=BTSDEDEVSQL20;initial catalog=bcs_log;password=report;persist security info=True;user id=bcs_logger;pooling=true"'
            ProviderName="System.Data.SqlClient" SelectCommand="BLF_GetLogEntry" DataSourceMode="DataReader" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:QueryStringParameter Name="Id" QueryStringField="data" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    </form>
</body>
</html>
