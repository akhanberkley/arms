<%@ Control Language="C#" CompilationMode="always" AutoEventWireup="true" CodeFile="~/ClientAddress.ascx.cs" Inherits="BCS.WebApp.UserControls.UserControls_ClientAddress" %>


<script type="text/javascript">
    var styleToSelect;
    function onOk() {
    }

    function hideModalPopupViaClient(ev, behaviorID) {
        var modalPopupBehavior = $find(behaviorID);
        modalPopupBehavior.hide();
    }
</script>
<asp:HiddenField ID="hfPrimaryFlagEditable" runat="server" />
<table>
    <tr>
        <td style='width: 52px; display: <%= (PrimaryFlagEditable) ? "" : "none" %>; text-align: center'>
            <UserControls:GroupedRadioButton ID="rdoPrimaryAddress" runat="server" GroupName="PrimaryAddressSelect" />
        </td>
        <td>
            <table cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td>
                            <table width="60%" cellspacing="0" cellpadding="2" class="grid">
                                <tr class="header">
                                    <td rowspan="3" runat="server" id="rowFirst">Select:
                            <asp:RadioButtonList CssClass="no_border_grid" ID="RadioButtonList1" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" AutoPostBack="True">
                                <asp:ListItem>Domestic</asp:ListItem>
                                <asp:ListItem>Foreign</asp:ListItem>
                            </asp:RadioButtonList>
                                    </td>
                                    <td></td>
                                    <td>Number</td>
                                    <td>Street Name</td>
                                    <td runat="server" id="colAddr21">Address Line 2</td>
                                    <td>City</td>
                                    <td>State</td>
                                    <td>Zip</td>
                                    <td runat="server" id="colCounty1">County</td>
                                    <td id="tdCountryText" runat="server" visible="false">Country</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <% if (!PrimaryFlagEditable && Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) != 25)
                                           { %>
                                        <asp:ImageButton ID="btnSave" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("save.gif") %>' Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() %>' />
                                        <% } %>
                                        <aje:UpdatePanel ID="UpdatePanel811" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="B" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("folder_find.gif") %>' OnClick="B_Click" />
                                                <asp:HiddenField ID="LinkButton1" runat="server" Value="B" />

                                                <ajx:ModalPopupExtender ID="mpeLookup" runat="server"
                                                    TargetControlID="LinkButton1"
                                                    PopupControlID="Panel1"
                                                    BackgroundCssClass="modalBackground"
                                                    OkControlID="OkButton"
                                                    OnOkScript="onOk()"
                                                    CancelControlID="CancelButton"
                                                    DropShadow="true" OnInit="mpeLookup_Init" />

                                            </ContentTemplate>
                                        </aje:UpdatePanel>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txHouse" runat="server" Width="100px" MaxLength="10"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txStreetname" runat="server" Width="120px" MaxLength="40"></asp:TextBox></td>
                                    <td runat="server" id="colAddr22">
                                        <asp:TextBox ID="txAddress2" MaxLength="40" runat="server" Width="120px"></asp:TextBox></td>
                                    <td>
                                        <asp:TextBox ID="txCity" runat="server" MaxLength="40" Width="120px"></asp:TextBox></td>
                                    <td>
                                        <UserControls:StateDropDownList ID="StateDropDownList1" runat="server" DataValueField="Abbreviation" AutoPostBack="False" Width="60px"></UserControls:StateDropDownList>
                                        <ajx:ListSearchExtender ID="ListSearchExtender1" runat="server" TargetControlID="StateDropDownList1"></ajx:ListSearchExtender>
                                        <asp:TextBox ID="txState" runat="server" Visible="false"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txZipCode" runat="server" Width="80px"></asp:TextBox>
                                    </td>
                                    <td runat="server" id="colCounty2">
                                        <asp:TextBox ID="txCounty" runat="server" Width="80px"></asp:TextBox>
                                    </td>
                                    <td id="tdCountryValue" runat="server" visible="false">
                                        <UserControls:CountryDropDownList ID="CountryDropDownList1" runat="server" Width="80px" AutoPostBack="True" OnSelectedIndexChanged="CountryDropDownList1_SelectedIndexChanged"></UserControls:CountryDropDownList>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="btnRemove" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("cancelbig.gif") %>' OnClick="btnRemove_Click" CausesValidation="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="border: none;">
                                        <UserControls:AddressTypeRadioButtonList CssClass="no_border_grid" ID="rblAddressType" RepeatDirection="horizontal" runat="server"></UserControls:AddressTypeRadioButtonList>
                                        <asp:RequiredFieldValidator ID="rfvAddressType" runat="server" ErrorMessage="&nbsp;" ControlToValidate="rblAddressType" SetFocusOnError="True"
                                            Display="Dynamic" OnInit="rfvAddressType_Init">* Address Type</asp:RequiredFieldValidator>
                                    </td>
                                    <td colspan="5" style="border: none; text-align: center">
                                        <asp:Label ID="lblCoordinates" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" Style="display: none; width: auto" OnLoad="Panel1_Load">
    <div>
        <aje:UpdatePanel ID="UpdatePanel81" runat="server">
            <ContentTemplate>
                <UserControls:Grid ID="GRIDID" runat="server" EmptyDataText="Nothing found" OnRowDataBound="Rows_Bound" AutoGenerateColumns="False" AllowPaging="True"
                    OnPageIndexChanging="PageIndex_Changing" OnPageIndexChanged="PageIndex_Changed">
                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:LinkButton ID="Button11" runat="server" Text="Select" CommandName="Select"
                                    CausesValidation="False"></asp:LinkButton>
                                <asp:HiddenField ID="hfLatitude" runat="server" Value='<%# Bind("Latitude") %>' />
                                <asp:HiddenField ID="hfLongitude" runat="server" Value='<%# Bind("Longitude") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Building" SortExpression="BuildingNumber">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Bind("BuildingNumber") %>' ID="lblBuildingNumber"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Address" SortExpression="Addr1">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Bind("Address1") %>' ID="lblAddress1"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Address Line 2" SortExpression="Addr2">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Bind("Address2") %>' ID="lblAddress2"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="City" SortExpression="City">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Bind("City") %>' ID="lblCity"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State" SortExpression="State">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Bind("State") %>' ID="lblState"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="County" SortExpression="County">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Bind("County") %>' ID="lblCounty"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ZipCode" SortExpression="ZipCode">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Bind("PostalCode") %>' ID="lblZipCode"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerTemplate>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                    </PagerTemplate>
                </UserControls:Grid>
            </ContentTemplate>
        </aje:UpdatePanel>

        <p style="text-align: center;">
            <asp:Button ID="OkButton" runat="server" Text="OK" Style="display: none;" />
            <asp:Button ID="CancelButton" runat="server" Text="Cancel" />
        </p>
    </div>
</asp:Panel>
