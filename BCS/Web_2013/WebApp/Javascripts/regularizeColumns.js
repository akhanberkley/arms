﻿//This is a  column regularizing script.
// Call this function like so: regularizeWRColumns('colTwo', 'colOne', 'colThree')
// The first id passed in should be the tallest column.  The script will make the others as tall as it.
function regularizeColumns() 
{
	var height = 0;
	if(arguments.length > 1)
	{
		var tallest = document.getElementById(arguments[0]);
		if(tallest){ height = tallest.offsetHeight; }
		
		if(height > 0)
		{
			for(x = 1; x < arguments.length; x++)
			{
				var column = document.getElementById(arguments[x]);	 
				column.style.height = height + 'px'; //if you get a javascript error here, it is probably because you passed in an invalid ID for a column
			}
		}
	}
}
	