<%@ Page Language="C#" MasterPageFile="Site.PATS.master" AutoEventWireup="true" CodeFile="CompanyNoCodeTypes.aspx.cs" Inherits="CompanyNoCodeTypes" Title="Berkley Clearance System : Company Number Code Types" Theme="DefaultTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <aje:ScriptManager id="ScriptManager1" runat="server"></aje:ScriptManager>
    <span class="heading">Admin: Company Code Types</span>
    <table cellpadding="0" cellspacing="0" class="box">
        <tbody>
        <tr>
            <td class="TL"><div /></td>
            <td class="TC">&nbsp;</td>
            <td class="TR"><div /></td>
        </tr>
        <tr>
            <td class="ML"><div /></td>
            <td class="MC">
        
                <table class="grid">
                    <tr>
                        <td>
                            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="odsCompanies"
                                DataTextField="CompanyName" DataValueField="Id" OnPreRender="DropDownList1_InsertItem">
                            </asp:DropDownList><asp:ObjectDataSource ID="odsCompanies" runat="server" SelectMethod="GetCompanies"
                                TypeName="BCS.Core.Clearance.Admin.DataAccess"></asp:ObjectDataSource>
                             <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="odsCompany" DataTextField="PropertyCompanyNumber"
                                DataValueField="Id" OnPreRender="DropDownList1_InsertItem">
                            </asp:DropDownList><asp:ObjectDataSource ID="odsCompany" runat="server" SelectMethod="GetCompanyNumbers"
                                TypeName="BCS.Core.Clearance.Admin.DataAccess">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DropDownList1" Name="companyId" PropertyName="SelectedValue"
                                        Type="Int32" />
                                    <asp:Parameter DefaultValue="True" Type="boolean" Name="companyDependent" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:Button ID="Button1" runat="server" CssClass="button" Text="Filter" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="odsCodeTypes" DataKeyNames="Id" AllowPaging="True"
                                OnRowDeleting="GridView1_RowDeleting"
                                OnRowDeleted="GridView1_RowDeleted"
                                OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                OnPageIndexChanged="GridView1_PageIndexChanged">
                                <Columns>
                                    <asp:CommandField ButtonType="Button" SelectText="Edit" ShowSelectButton="True" />
                                    <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
                                    <asp:BoundField DataField="CompanyNumberId" HeaderText="CompanyNumberId" Visible="False" />
                                    <asp:TemplateField HeaderText="Company Number">                
                                        <ItemTemplate>
                                            <asp:Label ID="CNLabel" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CompanyNumber.PropertyCompanyNumber") %>'></asp:Label>
                                            <asp:Label ID="CNIdLabel" runat="server" Text='<%# Bind ("CompanyNumberId") %>' CssClass="displayedNone"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CodeName" HeaderText="Code Name" />
                                    <asp:TemplateField HeaderText="Client Specific" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ckClientSpecific" runat="server" Checked='<%# Bind("ClientSpecific") %>' Enabled="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Class Code Specific" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ckClassCodeSpecific" runat="server" Checked='<%# Bind("ClassCodeSpecific") %>' Enabled="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CheckBoxField DataField="Required" HeaderText="Required" ReadOnly="True"
                                        SortExpression="Required">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:CheckBoxField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                CssClass="button" Text="Delete"/>
                                            <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="btnDelete"
                                                ConfirmText='<%# DataBinder.Eval (Container.DataItem, "CodeName", "Are you sure you want to delete this Code Name {0}?") %>' />
                                                
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="odsCodeTypes" runat="server" SelectMethod="GetCompanyNumberCodeTypes"
                                TypeName="BCS.Core.Clearance.Admin.DataAccess" DeleteMethod="DeleteCompanyNumberCodeType" OnDeleted="odsCodeTypes_Deleted">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DropDownList1" DefaultValue="" Name="companyId"
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="DropDownList2" DefaultValue="" Name="companyNumberId"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                                <DeleteParameters>
                                    <asp:Parameter Name="id" Type="Int32" />
                                </DeleteParameters>
                            </asp:ObjectDataSource>  
                        </td> 
                    </tr>
                    <tr>
                        <td><asp:Button ID="btnAdd" runat="server" CssClass="button" OnClick="btnAdd_Click" Text="Add" /></td>
                    </tr>
                </table>
                <br />
                <asp:DetailsView ID="DetailsView1" runat="server"
                    AutoGenerateRows="False" DataKeyNames="Id" DataSourceID="odsCodeType" OnItemInserted="DetailsView1_ItemInserted" OnItemUpdated="DetailsView1_ItemUpdated" OnItemUpdating="DetailsView1_ItemUpdating">        
                    <Fields>
                        <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
                        <asp:TemplateField HeaderText="Company Number">
                            <EditItemTemplate>
                                <asp:DropDownList ID="CNddl" runat="server" DataSourceID="ObjectDataSource1"
                                    DataTextField="PropertyCompanyNumber" DataValueField="Id" SelectedValue='<%# Bind("CompanyNumberId") %>'>
                                </asp:DropDownList><asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetCompanyNumbers"
                                    TypeName="BCS.Core.Clearance.Admin.DataAccess">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DropDownList1" Name="companyId" PropertyName="SelectedValue"
                                            Type="Int32" />
                                        <asp:Parameter DefaultValue="false" Name="companyDependent" Type="Boolean" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:DropDownList ID="CNddl" runat="server" DataSourceID="ObjectDataSource1"
                                    DataTextField="PropertyCompanyNumber" DataValueField="Id" SelectedValue='<%# Bind("CompanyNumberId") %>'>
                                </asp:DropDownList><asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetCompanyNumbers"
                                    TypeName="BCS.Core.Clearance.Admin.DataAccess">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DropDownList1" Name="companyId" PropertyName="SelectedValue"
                                            Type="Int32" />
                                        <asp:Parameter DefaultValue="false" Name="companyDependent" Type="Boolean" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval (Container.DataItem, "CompanyNumber.PropertyCompanyNumber") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Code Name">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CodeName") %>'></asp:TextBox><asp:RequiredFieldValidator
                                    ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" Display="Dynamic"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("CodeName") %>'></asp:TextBox><asp:RequiredFieldValidator
                                    ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox1" Display="Dynamic"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("CodeName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Client Specific">
                            <EditItemTemplate>
                                <asp:CheckBox ID="ckClientSpecific" runat="server" Checked='<%# Bind("ClientSpecific") %>' />
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:CheckBox ID="ckClientSpecific" runat="server" Checked='<%# Bind("ClientSpecific") %>' />
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="ckClientSpecific" runat="server" Checked='<%# Bind("ClientSpecific") %>' Enabled="False" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Class Code Specific">
                            <EditItemTemplate>
                                <asp:CheckBox ID="ckClassCodeSpecific" runat="server" Checked='<%# Bind("ClassCodeSpecific") %>' />
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:CheckBox ID="ckClassCodeSpecific" runat="server" Checked='<%# Bind("ClassCodeSpecific") %>' />
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="ckClassCodeSpecific" runat="server" Checked='<%# Bind("ClassCodeSpecific") %>' Enabled="False" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CheckBoxField DataField="Required" HeaderText="Required" SortExpression="Required" />
                        <asp:TemplateField HeaderText="Display Order" SortExpression="DisplayOrder">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("DisplayOrder") %>'></asp:TextBox>
                                <asp:RangeValidator ID="RangeValidator1" runat="server" SetFocusOnError="true" Display="dynamic"
                                 ControlToValidate="TextBox2" OnInit="SetRange" ErrorMessage="!!" Type="Integer">
                                </asp:RangeValidator>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("DisplayOrder") %>'></asp:TextBox>
                                <asp:RangeValidator ID="RangeValidator1" runat="server" SetFocusOnError="true" Display="dynamic"
                                 ControlToValidate="TextBox2" OnInit="SetRange" ErrorMessage="!!" Type="Integer">
                                </asp:RangeValidator>
                            </InsertItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("DisplayOrder") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CheckBoxField DataField="VisibleInLOBGridSection" HeaderText="Display in LOB Grid?" SortExpression="VisibleInLOBGridSection" />
                        <asp:TemplateField HeaderText="Default Code">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlDefaultCode" runat="server" SelectedValue='<%# Bind("DefaultCompanyNumberCodeId") %>' DataTextField="Code" DataValueField="Id"  DataSourceID="odsCNCodes"
                                 AppendDataBoundItems="true">
                                    <asp:ListItem></asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:ObjectDataSource ID="odsCNCodes" runat="server" SelectMethod="GetCompanyNumberCodes"
                                    TypeName="BCS.Core.Clearance.Admin.DataAccess" >
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DropDownList1" Name="companyId" PropertyName="SelectedValue"
                                            Type="Int32" />
                                        <asp:ControlParameter ControlID="DropDownList2" DefaultValue="" Name="filterCompanyNumberId"
                                            PropertyName="SelectedValue" Type="Int32" />
                                        <asp:ControlParameter ControlID="GridView1" DefaultValue="" Name="filterCodeTypeId"
                                            PropertyName="SelectedValue" Type="Int32" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>--%>
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                <asp:DropDownList ID="ddlDefaultCode" runat="server" SelectedValue='<%# Bind("DefaultCompanyNumberCodeId") %>' DataTextField="Code" DataValueField="Id"  DataSourceID="odsCNCodes"
                                 AppendDataBoundItems="true">
                                    <asp:ListItem></asp:ListItem>
                                </asp:DropDownList>
                            </InsertItemTemplate>
                            <ItemTemplate>                                
                                <asp:DropDownList ID="ddlDefaultCode" runat="server" SelectedValue='<%# Bind("DefaultCompanyNumberCodeId") %>' DataTextField="Code" DataValueField="Id"  DataSourceID="odsCNCodes"
                                 AppendDataBoundItems="true" Enabled="false">
                                    <asp:ListItem></asp:ListItem>
                                </asp:DropDownList>
                            </InsertItemTemplate>
                            </ItemTemplate>                            
                        </asp:TemplateField>                        
                        <asp:CommandField ButtonType="Button" ShowEditButton="True" ShowInsertButton="True" />
                    </Fields>
                </asp:DetailsView>
                
                <asp:ObjectDataSource ID="odsCNCodes" runat="server" SelectMethod="GetCompanyNumberCodes"
                    TypeName="BCS.Core.Clearance.Admin.DataAccess" OnSelected="SetCompanyCodeDisplay">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList1" Name="companyId" PropertyName="SelectedValue"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="DropDownList2" DefaultValue="" Name="filterCompanyNumberId"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="GridView1" DefaultValue="" Name="filterCodeTypeId"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="odsCodeType" runat="server" DataObjectTypeName="BCS.Core.Clearance.BizObjects.CodeType"
                    InsertMethod="AddCompanyNumberCodeType" OnInserted="odsCodeType_Inserted" OnUpdated="odsCodeType_Updated"
                    SelectMethod="GetCompanyNumberCodeType" TypeName="BCS.Core.Clearance.Admin.DataAccess"
                    UpdateMethod="UpdateCompanyNumberCodeType">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GridView1" Name="id" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
        
            </td>
            <td class="MR"><div /></td>
        </tr>
        <tr>
            <td class="BL"><div/></td>
            <td class="BC"><div/></td>
            <td class="BR"><div/></td>
        </tr>
        </tbody>
    </table>
</asp:Content>

