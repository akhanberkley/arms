<%@ Page language="c#" MasterPageFile="Site.PATS.master" Title="Berkley Clearance System : User Detail" Inherits="BCS.WebApp.Admin.UserDetail" CodeFile="UserDetail.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <aje:ScriptManager id="ScriptManager1" runat="server"></aje:ScriptManager>
    <span class="heading">Admin: User Detail</span>
    <table cellpadding="0" cellspacing="0" class="box">
        <tbody>
        <tr>
            <td class="TL"><div /></td>
            <td class="TC">&nbsp;</td>
            <td class="TR"><div /></td>
        </tr>
        <tr>
            <td class="ML"><div /></td>
            <td class="MC">
            
            <table border="1" cellpadding="5" cellspacing="0" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; MARGIN-TOP: 20px; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid; BACKGROUND-COLOR: #eee">
	            <tr>
		            <td>
			            Editing:
			            <asp:Label id="lblUser" runat="server" Font-Bold="true"></asp:Label>&nbsp;(
			            <asp:LinkButton id="linkbutton1" runat="server" Text="Delete User" CommandName="Delete" CausesValidation="false" onclick="Confirmlinkbutton1_Click"></asp:LinkButton>
			            <ajx:ConfirmButtonExtender ID="ConfirmDisable" runat="server" TargetControlID="linkbutton1">
                        </ajx:ConfirmButtonExtender>)
		            </td>
	            </tr>
	            <tr><td>Primary Company: 
                    <asp:DropDownList ID="ddlPrimaryCompany" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPrimaryCompany_SelectedIndexChanged">
                    </asp:DropDownList></td></tr>
                <tr><td>Agency: 
                    <asp:DropDownList ID="ddlAgency" runat="server" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlAgency_SelectedIndexChanged" OnInit="ddlAgency_Init">
                    </asp:DropDownList>
                    </td></tr>
	            <tr>
		            <td style="background-color: #a9a9a9">Companies:</td>
	            </tr>
	            <tr>
		            <td align="center">
			            <table>
				            <tr>
					            <td>Available:</td>
					            <td>&nbsp;</td>
					            <td>Assigned:</td>
				            </tr>
				            <tr>
					            <td style="width: 48%">
						            <asp:ListBox id="lbAllCompanies" runat="server" Rows="5" Width="100%" SelectionMode="Multiple"></asp:ListBox></td>
					            <td style="width: 4%" valign="middle"><asp:Button id="btnAddCompany" runat="server" Text=">" CssClass="button" onclick="btnAddCompany_Click"></asp:Button><br />
						            <br />
						            <asp:Button id="btnRemoveCompany" runat="server" Text="<" CssClass="button" onclick="btnRemoveCompany_Click"></asp:Button></td>
					            <td style="width: 48%">
						            <asp:ListBox id="lbAssignedCompanies" runat="server" Rows="5" Width="100%" SelectionMode="Multiple"></asp:ListBox>
					            </td>
				            </tr>
			            </table>
		            </td>
	            </tr>
	            <tr>
		            <td style="background-color: #a9a9a9">Roles:</td>
	            </tr>
	            <tr>
		            <td align="center">
			            <table>
				            <tr>
					            <td>Available:</td>
					            <td>&nbsp;</td>
					            <td>Assigned:</td>
				            </tr>
				            <tr>
					            <td style="width: 48%">
						            <asp:ListBox id="lbAllRoles" runat="server" Rows="5" Width="100%"></asp:ListBox></td>
					            <td style="width: 4%" valign="middle"><asp:Button id="btnAddRole" runat="server" Text=">" CssClass="button" onclick="btnAddRole_Click"></asp:Button><br />
						            <br />
						            <asp:Button id="btnRemoveRole" runat="server" Text="<" CssClass="button" onclick="btnRemoveRole_Click"></asp:Button></td>
					            <td style="width: 48%">
						            <asp:ListBox id="lbAssignedRoles" runat="server" Rows="5" Width="100%" SelectionMode="Multiple"></asp:ListBox>
					            </td>
				            </tr>
			            </table>
		            </td>
	            </tr>
	            <tr>
		            <td style="background-color: #a9a9a9">Notes:</td>
	            </tr>
	            <tr>
		            <td>
                        <asp:TextBox ID="txNotes" runat="server" TextMode="MultiLine" Rows="10" Width="100%"></asp:TextBox>
		            </td>
	            </tr>
	            <tr>
		            <td align="center">
		                <asp:Button id="btnUpdate" runat="server" Text="Update" CssClass="button" CausesValidation="False" onclick="btnUpdate_Click"></asp:Button>
		                &nbsp;
		                <asp:Button id="btnCancel" runat="server" Text="Back to Users" CssClass="button" CausesValidation="False" onclick="btnCancel_Click"></asp:Button>
		            </td>
	            </tr>
            </table>

            </td>
            <td class="MR"><div /></td>
        </tr>
        <tr>
            <td class="BL"><div/></td>
            <td class="BC"><div/></td>
            <td class="BR"><div/></td>
        </tr>
        </tbody>
    </table>
</asp:Content>
