using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using BCS.Biz;

using OrmLib;

namespace BCS.WebApp.Admin
{

    using Components;
	/// <summary>
	/// Summary description for _Default.
	/// </summary>
	public partial class Default : System.Web.UI.Page
    {
        private DataManager m_dataManager = new DataManager(DefaultValues.DSN);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BindAddressTypes();
                BindGeographicDirectionals();
                BindStreetSuffixes();
            }

            if (!string.IsNullOrEmpty(activeTab.Value))
                TabContainer1.ActiveTabIndex = int.Parse(activeTab.Value);
        }

        private void BindGeographicDirectionals()
        {
            dgGeographicDirectionals.DataSource =
                m_dataManager.GetGeographicDirectionalCollection().SortByPropertyGeographicDirectional(OrmLib.SortDirection.Ascending);
            dgGeographicDirectionals.DataBind();
        }

        private void BindStreetSuffixes()
        {
            dgStreetSuffixes.DataSource =
                m_dataManager.GetStreetSuffixCollection().SortByPropertyStreetSuffix(OrmLib.SortDirection.Ascending);
            dgStreetSuffixes.DataBind();
        }
        #region dgGeographicDirectionals Events
        protected void dgGeographicDirectionals_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            dgGeographicDirectionals.EditItemIndex = -1;
            dgGeographicDirectionals.Columns[dgGeographicDirectionals.Columns.Count - 1].Visible = true;	// Show the delete column
            BindGeographicDirectionals();
        }

        protected void dgGeographicDirectionals_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(JoinPath.GeographicDirectional.Columns.Id, e.Item.Cells[1].Text, MatchType.Exact);

            GeographicDirectional geographicDirectional = dataManager.GetGeographicDirectional(FetchPath.GeographicDirectional);
            geographicDirectional.Delete();

            dataManager.CommitAll();
            dataManager.QueryCriteria.Clear();

            // Reset the grid
            dgGeographicDirectionals.EditItemIndex = -1;
            BindGeographicDirectionals();
        }

        protected void dgGeographicDirectionals_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            dgGeographicDirectionals.EditItemIndex = e.Item.ItemIndex;
            dgGeographicDirectionals.Columns[dgGeographicDirectionals.Columns.Count - 1].Visible = false;	// Hide the delete column
            BindGeographicDirectionals();
        }

        protected void dgGeographicDirectionals_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(JoinPath.GeographicDirectional.Columns.Id, e.Item.Cells[1].Text, MatchType.Exact);

            GeographicDirectional geographicDirectional = dataManager.GetGeographicDirectional(FetchPath.GeographicDirectional);
            geographicDirectional.PropertyGeographicDirectional = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
            geographicDirectional.Abbreviation = ((TextBox)e.Item.Cells[3].Controls[0]).Text;

            dataManager.CommitAll();
            dataManager.QueryCriteria.Clear();

            // Reset the grid
            dgGeographicDirectionals.EditItemIndex = -1;
            dgGeographicDirectionals.Columns[dgGeographicDirectionals.Columns.Count - 1].Visible = true;	// Show the delete column
            BindGeographicDirectionals();
        }

        protected void btAddGeographicDirectional_Click(object sender, System.EventArgs e)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);
            GeographicDirectional geographicDirectional = dataManager.NewGeographicDirectional(txAddGeographicDirectional.Text,
                txAddGeographicDirectionalAbbreviation.Text);
            dataManager.CommitAll();

            txAddGeographicDirectional.Text = "";
            txAddGeographicDirectionalAbbreviation.Text = "";

            BindGeographicDirectionals();
        }
        #endregion

        #region dgStreetSuffixes Events
        protected void dgStreetSuffixes_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            dgStreetSuffixes.EditItemIndex = -1;
            dgStreetSuffixes.Columns[dgStreetSuffixes.Columns.Count - 1].Visible = true;	// Show the delete column
            BindStreetSuffixes();
        }

        protected void dgStreetSuffixes_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(JoinPath.StreetSuffix.Columns.Id, e.Item.Cells[1].Text, MatchType.Exact);

            StreetSuffix streetSuffix = dataManager.GetStreetSuffix(FetchPath.StreetSuffix);
            streetSuffix.Delete();

            dataManager.CommitAll();
            dataManager.QueryCriteria.Clear();

            // Reset the grid
            dgStreetSuffixes.EditItemIndex = -1;
            BindStreetSuffixes();
        }

        protected void dgStreetSuffixes_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            dgStreetSuffixes.EditItemIndex = e.Item.ItemIndex;
            dgStreetSuffixes.Columns[dgStreetSuffixes.Columns.Count - 1].Visible = false;	// Hide the delete column
            BindStreetSuffixes();
        }

        protected void dgStreetSuffixes_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);

            dataManager.QueryCriteria.Clear();
            dataManager.QueryCriteria.And(JoinPath.StreetSuffix.Columns.Id, e.Item.Cells[1].Text, MatchType.Exact);

            StreetSuffix streetSuffix = dataManager.GetStreetSuffix(FetchPath.StreetSuffix);
            streetSuffix.ApprovedAbbreviation = ((TextBox)e.Item.Cells[2].Controls[0]).Text;
            streetSuffix.CommonAbbreviation = ((TextBox)e.Item.Cells[3].Controls[0]).Text;
            streetSuffix.PropertyStreetSuffix = ((TextBox)e.Item.Cells[4].Controls[0]).Text;

            dataManager.CommitAll();
            dataManager.QueryCriteria.Clear();

            // Reset the grid
            dgStreetSuffixes.EditItemIndex = -1;
            dgStreetSuffixes.Columns[dgStreetSuffixes.Columns.Count - 1].Visible = true;	// Show the delete column
            BindStreetSuffixes();
        }

        protected void btAddStreetSuffix_Click(object sender, System.EventArgs e)
        {
            DataManager dataManager = new DataManager(DefaultValues.DSN);
            //GeographicDirectional geographicDirectional = dataManager.NewGeographicDirectional(txAddGeographicDirectional.Text,
            //	txAddGeographicDirectionalAbbreviation.Text);

            StreetSuffix streetSuffix = dataManager.NewStreetSuffix(txAddStreetSuffix.Text, string.Empty, string.Empty);
            streetSuffix.ApprovedAbbreviation = txAddStreetSuffixApprovedAbbreviation.Text;
            streetSuffix.CommonAbbreviation = txAddStreetSuffixCommonAbbreviation.Text;
            dataManager.CommitAll();

            txAddStreetSuffix.Text = "";
            txAddStreetSuffixApprovedAbbreviation.Text = "";
            txAddStreetSuffixCommonAbbreviation.Text = "";

            BindStreetSuffixes();
        }

        protected void dgStreetSuffixes_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            dgStreetSuffixes.CurrentPageIndex = e.NewPageIndex;
            BindStreetSuffixes();
        }
        #endregion
        protected void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
        {
            throw new ApplicationException("Ajax Error", e.Exception);
        }
}
}
