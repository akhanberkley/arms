<%@ Page language="c#" MasterPageFile="Site.PATS.master" Title="Berkley Clearance System : Admin" Inherits="BCS.WebApp.Admin.Default" CodeFile="Default.aspx.cs" Theme="DefaultTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <%--

    TODO:

    Temporary (hidden field) workaround for linkbutton clicks not restoring parent as active tab, instead restores to last active tab other than parent.
    http://forums.asp.net/p/1068120/1550723.aspx#1550723

    Caution :: using VS designer with ajax tab container and panel adds unnecessary attributes to tags, and sets unintended properties.

    --%>
    <script type="text/javascript">
        function ActiveTabChanged(sender, e) {
            var activeTab = $get('<%=activeTab.ClientID%>');
            activeTab.value = sender.get_activeTabIndex();
        }
    </script>
    <asp:HiddenField ID="activeTab" runat="server" />
    <aje:ScriptManager id="ScriptManager1" runat="server" OnAsyncPostBackError="ScriptManager1_AsyncPostBackError"></aje:ScriptManager>	
    <span class="heading">Admin: Lookup Tables</span>
    <table cellpadding="0" cellspacing="0" class="box">
        <tbody>
        <tr>
            <td class="TL"><div /></td>
            <td class="TC">&nbsp;</td>
            <td class="TR"><div /></td>
        </tr>
        <tr>
            <td class="ML"><div /></td>
            <td class="MC">
        
                <ajx:TabContainer ID="TabContainer1" OnClientActiveTabChanged="ActiveTabChanged" runat="server" CssClass="bcstabs">
                    <ajx:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>Geographic Directionals</HeaderTemplate>
                        <ContentTemplate>
                            <aje:UpdatePanel id="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table width="100%" class="grid">
                                        <tr>
                                            <td bordercolor="white" style="padding-left:10px;padding-bottom:15px;padding-top:0px;">
                                                <h3 style="padding-top:10px;">Geographic Directionals</h3>			
                                                <asp:DataGrid id="dgGeographicDirectionals" runat="server" BorderWidth="1px" BorderStyle="None" BorderColor="#999999" OnUpdateCommand="dgGeographicDirectionals_UpdateCommand" OnEditCommand="dgGeographicDirectionals_EditCommand" OnDeleteCommand="dgGeographicDirectionals_DeleteCommand" OnCancelCommand="dgGeographicDirectionals_CancelCommand" AutoGenerateColumns="False" GridLines="Vertical" CellPadding="3" BackColor="White">
	                                                <FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
	                                                <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
	                                                <AlternatingItemStyle BackColor="#DCDCDC"></AlternatingItemStyle>
	                                                <ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
	                                                <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#00563f"></HeaderStyle>
	                                                <Columns>
	                                                    <asp:EditCommandColumn ButtonType="PushButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
		                                                <asp:BoundColumn Visible="False" DataField="Id" ReadOnly="True" HeaderText="Id"></asp:BoundColumn>
		                                                <asp:BoundColumn DataField="PropertyGeographicDirectional" HeaderText="Geographic Directional"></asp:BoundColumn>
		                                                <asp:BoundColumn DataField="Abbreviation" HeaderText="Abbreviation"></asp:BoundColumn>
		                                                <asp:TemplateColumn ItemStyle-CssClass="blackLinks">
			                                                <HeaderStyle HorizontalAlign="Center" CssClass="headerGrad"></HeaderStyle>
			                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
			                                                <ItemTemplate>
				                                                <asp:Button id="linkbutton2" runat="server" Text="Delete" CommandName="Delete" CausesValidation="false"></asp:Button>
					                                            <ajx:ConfirmButtonExtender ID="ConfirmDelete" runat="server" TargetControlID="linkbutton2" ConfirmText="Delete this record?">
					                                            </ajx:ConfirmButtonExtender>
			                                                </ItemTemplate>
		                                                </asp:TemplateColumn>                    
	                                                </Columns>
	                                                <PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
                                                </asp:DataGrid>
                                                <br />
                                                <table cellpadding="3" cellspacing="0" style="BORDER: #999999 1px solid;">
	                                                <tr>
		                                                <td bgcolor="#00563f"><font color="#ffffff">Geographic Directional:</font></td>
		                                                <td bgcolor="#00563f" colspan="2"><font color="#ffffff">Abbreviation:</font></td>
	                                                </tr>
	                                                <tr>
		                                                <td>
			                                                <asp:TextBox id="txAddGeographicDirectional" runat="server" ValidationGroup="AddGeographicDirectionals"></asp:TextBox>
			                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txAddGeographicDirectional" ErrorMessage="*" Display="Dynamic" ValidationGroup="AddGeographicDirectionals"></asp:RequiredFieldValidator>
			                                            </td>
		                                                <td>
			                                                <asp:TextBox id="txAddGeographicDirectionalAbbreviation" runat="server" ValidationGroup="Add"></asp:TextBox>
			                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txAddGeographicDirectionalAbbreviation" ErrorMessage="*" Display="Dynamic" ValidationGroup="AddGeographicDirectionals"></asp:RequiredFieldValidator>
			                                            </td>
		                                                <td>
			                                                <asp:Button id="btAddGeographicDirectional" runat="server" Text="Add" CssClass="button" onclick="btAddGeographicDirectional_Click" ValidationGroup="AddGeographicDirectionals"></asp:Button></td>
	                                                </tr>
	                                            </table>
	                                        </td>
                                        </tr>
                                    </table>	                            
                                </ContentTemplate>
                            </aje:UpdatePanel>
                        </ContentTemplate>
                    </ajx:TabPanel>
                    <ajx:TabPanel ID="TabPanel2" runat="server">
                       <HeaderTemplate>Street Suffixes</HeaderTemplate>
                       <ContentTemplate>
                           <aje:UpdatePanel id="UpdatePanel2" runat="server">
                               <ContentTemplate>
                                   <table width="100%" class="grid">
                                        <tr>
                                            <td bordercolor="white" style="padding-left:10px;padding-bottom:15px;padding-top:0px;">
                                                <h3>Street Suffixes</h3>
                                                <asp:DataGrid id="dgStreetSuffixes" runat="server" BorderWidth="1px" BorderStyle="None" BorderColor="#999999" OnUpdateCommand="dgStreetSuffixes_UpdateCommand" OnEditCommand="dgStreetSuffixes_EditCommand" OnDeleteCommand="dgStreetSuffixes_DeleteCommand" OnCancelCommand="dgStreetSuffixes_CancelCommand" AutoGenerateColumns="False" GridLines="Vertical" CellPadding="3" BackColor="White" OnPageIndexChanged="dgStreetSuffixes_PageIndexChanged" AllowPaging="True">
                                                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>
                                                    <SelectedItemStyle BackColor="#008A8C" ForeColor="White" Font-Bold="True"></SelectedItemStyle>
                                                    <PagerStyle Mode="NumericPages" BackColor="#999999" ForeColor="Black" Position="TopAndBottom" HorizontalAlign="Center"></PagerStyle>
                                                    <AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
                                                    <ItemStyle BackColor="#EEEEEE" ForeColor="Black"></ItemStyle>
                                                    <HeaderStyle BackColor="#00563F" ForeColor="White" Font-Bold="True"></HeaderStyle>
                                                    <Columns>
                                                        <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit"></asp:EditCommandColumn>
                                                        <asp:BoundColumn DataField="Id" HeaderText="Id" ReadOnly="True" Visible="False"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ApprovedAbbreviation" HeaderText="Approved Abbreviation"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="CommonAbbreviation" HeaderText="Common Abbreviation"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="PropertyStreetSuffix" HeaderText="Street Suffix"></asp:BoundColumn>                                            
                                                        <asp:TemplateColumn ItemStyle-CssClass="blackLinks">
                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton id="linkbutton6" runat="server" Text="Delete" CommandName="Delete" Message="Delete this record?"
		                                                            CausesValidation="false"></asp:LinkButton>
		                                                        <ajx:ConfirmButtonExtender ID="ConfirmDeleteSS" runat="server" TargetControlID="linkbutton6" ConfirmText="Delete this record?">
					                                            </ajx:ConfirmButtonExtender>
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="headerGrad" HorizontalAlign="Center"></HeaderStyle>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                                <br />
                                                <table cellpadding="3" cellspacing="0" style="BORDER: #999999 1px solid;">
                                                    <tr>
                                                       <td bgcolor="#00563f"><font color="#ffffff">Approved Abbreviation:</font></td>
                                                       <td bgcolor="#00563f"><font color="#ffffff">Common Abbreviation:</font></td>
                                                       <td bgcolor="#00563f" colspan="2"><font color="#ffffff">Street Suffix:</font></td>
                                                   </tr>
                                                   <tr>
                                                       <td>
                                                        <asp:TextBox id="txAddStreetSuffixApprovedAbbreviation" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txAddStreetSuffixApprovedAbbreviation" ErrorMessage="*" Display="Dynamic" ValidationGroup="AddStreetSuffixes"></asp:RequiredFieldValidator>
                                                       </td>
                                                       <td>
                                                        <asp:TextBox id="txAddStreetSuffixCommonAbbreviation" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txAddStreetSuffixCommonAbbreviation" ErrorMessage="*" Display="Dynamic" ValidationGroup="AddStreetSuffixes"></asp:RequiredFieldValidator>
                                                       </td>
                                                       <td>
                                                        <asp:TextBox id="txAddStreetSuffix" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txAddStreetSuffix" ErrorMessage="*" Display="Dynamic" ValidationGroup="AddStreetSuffixes"></asp:RequiredFieldValidator>
                                                       </td>
                                                       <td>
                                                        <asp:Button id="btAddStreetSuffix" runat="server" Text="Add" CssClass="button" onclick="btAddStreetSuffix_Click" ValidationGroup="AddStreetSuffixes"></asp:Button>                                                
                                                       </td>
                                                   </tr>
                                              </table>
                                         </td>
                                     </tr>
                                   </table>
                               </ContentTemplate>
                            </aje:UpdatePanel>
                        </ContentTemplate>
                    </ajx:TabPanel>
                </ajx:TabContainer>
        
	        </td>
            <td class="MR"><div /></td>
        </tr>
        <tr>
            <td class="BL"><div/></td>
            <td class="BC"><div/></td>
            <td class="BR"><div/></td>
        </tr>
        </tbody>
    </table>
</asp:Content>
