using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.Components;

public partial class CompanyNoCodeTypes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        DetailsView1.ChangeMode(DetailsViewMode.Insert);
    }
   
    protected void DropDownList1_InsertItem(object sender, EventArgs e)
    {
        if (DropDownList1.Items.FindByValue("0") == null)
            DropDownList1.Items.Insert(0, new ListItem("", "0"));
        if (DropDownList2.Items.FindByValue("0") == null)
            DropDownList2.Items.Insert(0, new ListItem("", "0"));
        DetailsView1.DataBind();
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string key = (GridView1.Rows[e.RowIndex].FindControl("ckClientSpecific") as CheckBox).Checked ?
            string.Format(CacheKeys.ClientSpecificCompanyNumberCodeTypes, (GridView1.Rows[e.RowIndex].FindControl("CNIdLabel") as Label).Text) :
            string.Format(CacheKeys.SubmissionSpecificCompanyNumberCodeTypes, (GridView1.Rows[e.RowIndex].FindControl("CNIdLabel") as Label).Text);
        Cache.Remove(key);
    }
    protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        GridView1.DataBind();
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        string key = (DetailsView1.FindControl("ckClientSpecific") as CheckBox).Checked ?
            string.Format(CacheKeys.ClientSpecificCompanyNumberCodeTypes, (DetailsView1.FindControl("CNddl") as DropDownList).SelectedValue) :
            string.Format(CacheKeys.SubmissionSpecificCompanyNumberCodeTypes, (DetailsView1.FindControl("CNddl") as DropDownList).SelectedValue);
        Cache.Remove(key);
        GridView1.DataBind();
    }
    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        string key = (DetailsView1.FindControl("ckClientSpecific") as CheckBox).Checked ?
            string.Format(CacheKeys.ClientSpecificCompanyNumberCodeTypes, (DetailsView1.FindControl("CNddl") as DropDownList).SelectedValue) :
            string.Format(CacheKeys.SubmissionSpecificCompanyNumberCodeTypes, (DetailsView1.FindControl("CNddl") as DropDownList).SelectedValue);
        Cache.Remove(key);
        GridView1.DataBind();
    }
    
    protected void odsCodeTypes_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(Master.FindControl("contentplaceholder1"), e.ReturnValue.ToString());
        }
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        DetailsView1.ChangeMode(DetailsViewMode.Edit);
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {        
    }
    protected void odsCodeType_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(Master.FindControl("contentplaceholder1"), e.ReturnValue.ToString());
        }
    }
    protected void odsCodeType_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(Master.FindControl("contentplaceholder1"), e.ReturnValue.ToString());
        }
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        bool b = Common.ShouldUpdate(e);
        
        e.Cancel = b;

        if (b)
        {
            // there was nothing to update. Just show the user the success message.
            Common.SetStatus(Master.FindControl("contentplaceholder1"), "Company Code Type Sucessfully updated.");
            DetailsView1.DataBind();
        }

    }
    protected void GridView1_PageIndexChanged(object sender, EventArgs e)
    {
        GridView1.SelectedIndex = -1;
    }
    protected void SetRange(object sender, EventArgs e)
    {
        (sender as RangeValidator).MinimumValue = "0";
        (sender as RangeValidator).MaximumValue = "100";
    }
    protected void SetCompanyCodeDisplay(object sender, ObjectDataSourceStatusEventArgs e)
    {
        BCS.Biz.CompanyNumberCodeCollection cncs = e.ReturnValue as BCS.Biz.CompanyNumberCodeCollection;
        foreach (BCS.Biz.CompanyNumberCode cnc in cncs)
        {
            string displaystring = string.Empty;
            if (!string.IsNullOrEmpty(cnc.Code))
                displaystring += cnc.Code;
            if (!string.IsNullOrEmpty(cnc.Description))
            {
                if (!string.IsNullOrEmpty(cnc.Code))
                    displaystring += " - " + cnc.Description;
                else
                    displaystring = cnc.Description;
            }
            cnc.Code = displaystring;
        }
    }
}
