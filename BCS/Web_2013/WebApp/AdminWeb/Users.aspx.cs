using System;
using System.Collections;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using BCS.Biz;
using BCS.Core;

using OrmLib;
using TSHAK.Components;

namespace BCS.WebApp.Admin
{
    using Components;
	/// <summary>
	/// Summary description for Users.
	/// </summary>
	public partial class Users : System.Web.UI.Page
    {

        private DataManager m_dataManager = new DataManager(DefaultValues.DSN);

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                txSearchName.Text = ConfigurationManager.AppSettings["DefaultNetworkDomain"].ToString();
                txAddUsername.Text = ConfigurationManager.AppSettings["DefaultNetworkDomain"].ToString();

                BindSearchData();

                LoadPrimaryCompaniesList();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgSearchUsers.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgSearchUsers_PageIndexChanged);
            this.dgSearchUsers.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgSearchUsers_ItemDataBound);

        }
        #endregion

        private void BindSearchData()
        {
            #region Build DataTable
            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("Id");
            dataTable.Columns.Add("Active");
            dataTable.Columns.Add("Username");
            dataTable.Columns.Add("CompanyName");
            dataTable.Columns.Add("RoleName");
            #endregion

           if (txSearchName.Text.Length > 0 && 
               txSearchName.Text != ConfigurationManager.AppSettings["DefaultNetworkDomain"].ToString())
           {
               m_dataManager.QueryCriteria.Clear();
               m_dataManager.QueryCriteria.And(JoinPath.User.Columns.Username, txSearchName.Text, MatchType.Partial);
            }

            UserCollection searchUsers = m_dataManager.GetUserCollection(FetchPath.User.UserCompany.Company, FetchPath.User.UserRole.Role).SortByUsername(OrmLib.SortDirection.Ascending);

            DataRow row;
            foreach (User user in searchUsers)
            {
                row = dataTable.NewRow();

                row[0] = user.Id;
                row[1] = user.Active;
                row[2] = user.Username;
                
                #region CompanyNames
                string userCompaniesList = string.Empty;
                //UserCompanyCollection userCompanies = user.UserCompanys;
                foreach (UserCompany company in user.UserCompanys)
                {
                    userCompaniesList += company.Company.CompanyName + ",";
                }
                if (userCompaniesList.Length > 0)
                {
                    userCompaniesList = userCompaniesList.Substring(0, userCompaniesList.Length - 1);
                    userCompaniesList = userCompaniesList.Length > 20 ? userCompaniesList.Substring(0, 20) + "..." : userCompaniesList;
                }
                row[3] = userCompaniesList;
                #endregion

                #region RoleNames
                string userRolesList = string.Empty;
                //UserRoleCollection userRoles = user.UserRoles;
                foreach (UserRole role in user.UserRoles)
                {
                    userRolesList += role.Role.RoleName + ",";
                }
                if (userRolesList.Length > 0)
                {
                    userRolesList = userRolesList.Substring(0, userRolesList.Length - 1);
                    userRolesList = userRolesList.Length > 20 ? userRolesList.Substring(0, 20) + "..." : userRolesList;
                }
                row[4] = userRolesList;
                #endregion

                dataTable.Rows.Add(row);
            }

            dgSearchUsers.DataSource = dataTable;
            dgSearchUsers.DataBind();
        }

        private void LoadPrimaryCompaniesList()
        {
            m_dataManager.QueryCriteria.Clear();
            CompanyCollection allCompanies = m_dataManager.GetCompanyCollection().SortByCompanyName(OrmLib.SortDirection.Ascending);

            foreach (Company company in allCompanies)
            {
                ddlPrimaryCompany.Items.Add(new ListItem(company.CompanyName, company.Id.ToString()));
            }
        }

        protected void btsSearch_Click(object sender, System.EventArgs e)
        {
            dgSearchUsers.CurrentPageIndex = 0;
            BindSearchData();
        }

        protected void btnClear_Click(object sender, System.EventArgs e)
        {
            txSearchName.Text = ConfigurationManager.AppSettings["DefaultNetworkDomain"].ToString();
            BindSearchData();
        }

        private void dgSearchUsers_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            dgSearchUsers.CurrentPageIndex = e.NewPageIndex;
            BindSearchData();
        }

        private void dgSearchUsers_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                SecureQueryString qs = new SecureQueryString(Core.Core.Key);

                qs["Id"] = ((DataRowView)e.Item.DataItem).Row.ItemArray[0].ToString();

                ((HtmlInputButton)e.Item.FindControl("btnEdit")).Attributes.Add("onclick",
                    "self.location = 'UserDetail.aspx?data=" + HttpUtility.UrlEncode(qs.ToString()) + "'");

                if (((DataRowView)e.Item.DataItem).Row.ItemArray[1] != System.DBNull.Value)
                {
                    if (!Convert.ToBoolean(((DataRowView)e.Item.DataItem).Row.ItemArray[1]))
                    {
                        // Not Active
                        e.Item.BackColor = System.Drawing.Color.Yellow;
                        e.Item.Attributes.Add("title", "User has been disabled.");
                    }
                }
            }
        }

        protected void btnAddUser_Click(object sender, System.EventArgs e)
        {
            if (txAddUsername.Text != string.Empty &&
                txAddUsername.Text != ConfigurationManager.AppSettings["DefaultNetworkDomain"].ToString())
            {
                m_dataManager.QueryCriteria.Clear();
                m_dataManager.QueryCriteria.And(JoinPath.User.Columns.Username, txAddUsername.Text, MatchType.Exact);
                User existingUser = m_dataManager.GetUser();

                if (existingUser == null)
                {
                    m_dataManager.QueryCriteria.Clear();
                    User newUser = m_dataManager.NewUser(txAddUsername.Text);
                    newUser.PrimaryCompanyId = Convert.ToInt32(ddlPrimaryCompany.SelectedValue);

                    if (txPassword.Text != string.Empty)
                        newUser.Password = txPassword.Text;

                    m_dataManager.CommitAll();

                    SecureQueryString qs = new SecureQueryString(Core.Core.Key);
                    qs["Id"] = newUser.Id.ToString();

                    Response.Redirect("UserDetail.aspx?data=" + HttpUtility.UrlEncode(qs.ToString()));
                }
                else
                {
                    lblAddUserStatus.Text = "<br />User already exists, please try again.";
                }
            }
            else
            { lblAddUserStatus.Text = "<br />Please enter a user name."; }
        }

        //		this.btsSearch.Click += new System.EventHandler(this.btsSearch_Click);
        //		this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
        //		this.dgSearchUsers.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgSearchUsers_PageIndexChanged);
        //		this.dgSearchUsers.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgSearchUsers_ItemDataBound);
        //		this.btnAddUser.Click += new System.EventHandler(this.btnAddUser_Click);
        //		this.Load += new System.EventHandler(this.Page_Load);

        protected void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
        {
            throw new ApplicationException("Ajax Error", e.Exception);
        }
}
}
