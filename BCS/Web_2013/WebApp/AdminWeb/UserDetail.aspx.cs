using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using BCS.Biz;
using BCS.Core;

using OrmLib;
using TSHAK.Components;

namespace BCS.WebApp.Admin
{
    using Components;
    /// <summary>
    /// Summary description for UserDetail.
    /// </summary>
    public partial class UserDetail : System.Web.UI.Page
    {
        private string m_id = string.Empty;
        private DataManager m_dataManager = new DataManager(DefaultValues.DSN);
        private User m_user;
        protected System.Web.UI.WebControls.Button btnDeleteRole;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (Request.QueryString.Count > 0 && Request.QueryString["data"] != null)
            {
                SecureQueryString qs = new SecureQueryString(Core.Core.Key, Request.QueryString["data"]);
                this.m_id = qs["Id"];
            }

            // No ID? Terminate page
            if (this.m_id == null || this.m_id.Length <= 0)
            {
                throw (new Exception("Unknown security group. Id doesn't not exist in the database."));
            }
            else
            {
                m_dataManager.QueryCriteria.Clear();
                m_dataManager.QueryCriteria.And(JoinPath.User.Columns.Id, Int32.Parse(this.m_id), MatchType.Exact);
                this.m_user = m_dataManager.GetUser(FetchPath.User);

                lblUser.Text = this.m_user.Username;

                // Set up the enable/disable button
                if (this.m_user.Active)
                {
                    linkbutton1.Text = "Disable User";
                    ConfirmDisable.ConfirmText = "Disable this user?";
                }
                else
                {
                    linkbutton1.Text = "Enable User";
                    ConfirmDisable.ConfirmText = "Enable this user?";
                }
            }

            if (!IsPostBack)
            {
                ddlAgency.DataBind();
                //ddlAPSAgency.DataBind();

                // Companies
                LoadAllCompanies();
                LoadAssignedCompanies();

                // Roles
                LoadAllRoles();
                LoadAssignedRoles();

                // Let see if we have an assigned primary company:
                if (this.m_user != null)
                {
                    if (this.m_user.PrimaryCompanyId != 0)
                    {
                        ddlPrimaryCompany.SelectedValue = this.m_user.PrimaryCompanyId.ToString();

                        //ddlAPSAgency.CompanyId = this.m_user.PrimaryCompanyId.Value;
                        //ddlAPSAgency.DataBind();
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "noprimary",
                            "alert('Please select a primary company.');", true);
                    }
                    //SetAgencyType(this.m_user.PrimaryCompanyId);
                    LoadAgencies();
                    if (this.m_user.AgencyId != 0)
                    {
                        ddlAgency.SelectedValue = this.m_user.AgencyId.ToString();
                        //ddlAPSAgency.SelectedValue = this.m_user.AgencyId.ToString();
                    }
                    this.txNotes.Text = this.m_user.Notes;
                }
            }
        }

        //private void SetAgencyType(System.Data.SqlTypes.SqlInt32 sqlInt32)
        //{
        //    int cId = sqlInt32.IsNull ? 0 : sqlInt32.Value;
        //    bool isAps = ConfigValues.UsesAPS(cId);
        //    ddlAgency.Visible = !isAps;
        //}

        protected void btnCancel_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("Users.aspx");
        }


        #region Load All/Primary/Assigned Companies
        private void LoadAllCompanies()
        {
            string prCompany = ddlPrimaryCompany.SelectedValue;
            lbAllCompanies.Items.Clear();

            // Primary:
            ddlPrimaryCompany.Items.Clear();
            ddlPrimaryCompany.Items.Add("");

            m_dataManager.QueryCriteria.Clear();
            CompanyCollection allCompanies = m_dataManager.GetCompanyCollection().SortByCompanyName(OrmLib.SortDirection.Ascending);

            foreach (Company company in allCompanies)
            {
                lbAllCompanies.Items.Add(new ListItem(company.CompanyName, company.Id.ToString()));

                ddlPrimaryCompany.Items.Add(new ListItem(company.CompanyName, company.Id.ToString()));
            }
            if (ddlPrimaryCompany.Items.FindByValue(prCompany) != null)
            {
                ddlPrimaryCompany.Items.FindByValue(prCompany).Selected = true;
            }
        }

        private void LoadAssignedCompanies()
        {
            lbAssignedCompanies.Items.Clear();

            m_dataManager.QueryCriteria.Clear();
            m_dataManager.QueryCriteria.And(JoinPath.User.Columns.Id, this.m_user.Id, MatchType.Exact);
            User userC = m_dataManager.GetUser(FetchPath.User.UserCompany.Company);

            ListItemCollection lis = new ListItemCollection();
            foreach (UserCompany assignedCompany in userC.UserCompanys)
            {
                //lbAssignedCompanies.Items.Add(new ListItem(assignedCompany.Company.CompanyName, assignedCompany.CompanyId.ToString()));
                lis.Add(new ListItem(assignedCompany.Company.CompanyName, assignedCompany.CompanyId.ToString()));
                // Remove
                lbAllCompanies.Items.Remove(new ListItem(assignedCompany.Company.CompanyName, assignedCompany.CompanyId.ToString()));
            }
            lbAssignedCompanies.Items.AddRange(Common.SortListItems(lis));
        }
        #endregion

        #region Load All/Assigned Roles
        private void LoadAllRoles()
        {
            lbAllRoles.Items.Clear();

            m_dataManager.QueryCriteria.Clear();
            RoleCollection allRoles = m_dataManager.GetRoleCollection().SortByRoleName(OrmLib.SortDirection.Ascending);

            foreach (Role role in allRoles)
            {
                lbAllRoles.Items.Add(new ListItem(role.RoleName, role.Id.ToString()));
            }
        }

        private void LoadAssignedRoles()
        {
            lbAssignedRoles.Items.Clear();

            m_dataManager.QueryCriteria.Clear();
            m_dataManager.QueryCriteria.And(JoinPath.User.Columns.Id, this.m_user.Id, MatchType.Exact);
            User userR = m_dataManager.GetUser(FetchPath.User.UserRole.Role);

            ListItemCollection lis = new ListItemCollection();
            foreach (UserRole assignedRole in userR.UserRoles)
            {
                //lbAssignedRoles.Items.Add(new ListItem(assignedRole.Role.RoleName, assignedRole.RoldId.ToString()));
                lis.Add(new ListItem(assignedRole.Role.RoleName, assignedRole.RoldId.ToString()));
                // Remove
                lbAllRoles.Items.Remove(new ListItem(assignedRole.Role.RoleName, assignedRole.RoldId.ToString()));
            }
            lbAssignedRoles.Items.AddRange(Common.SortListItems(lis));
        }
        #endregion

        #region Add/Remove Companies
        protected void btnAddCompany_Click(object sender, System.EventArgs e)
        {
            foreach (ListItem item in lbAllCompanies.Items)
            {
                if (item.Selected)
                {
                    //m_dataManager.QueryCriteria.Clear();
                    //m_dataManager.QueryCriteria.And(JoinPath.Company.Columns.Id, Int32.Parse(item.Value), MatchType.Exact);
                    //Company company = this.m_dataManager.GetCompany(FetchPath.Company);

                    //UserCompany uc = m_dataManager.NewUserCompany(m_user, company);
                    UserCompany uc = m_user.NewUserCompany();
                    uc.CompanyId = Int32.Parse(item.Value);
                    Commit();
                }
            }
            // Companies
            LoadAllCompanies();
            LoadAssignedCompanies();
        }

        protected void btnRemoveCompany_Click(object sender, System.EventArgs e)
        {
            foreach (ListItem item in lbAssignedCompanies.Items)
            {
                if (item.Selected)
                {
                    m_dataManager.QueryCriteria.Clear();
                    m_dataManager.QueryCriteria.And(JoinPath.UserCompany.Columns.UserId, this.m_user.Id, MatchType.Exact);
                    //.And(JoinPath.UserCompany.Columns.CompanyId, Int32.Parse(item.Value), MatchType.Exact);

                    UserCompanyCollection userCompanys = m_dataManager.GetUserCompanyCollection();


                    UserCompany userCompany = userCompanys.FindByCompanyId(item.Value);

                    userCompany.Delete();
                    Commit();

                    if (item.Equals(ddlPrimaryCompany.SelectedItem))
                    {
                        ddlPrimaryCompany.SelectedIndex = 0;
                        ddlPrimaryCompany_SelectedIndexChanged(null, System.EventArgs.Empty);
                    }
                }
            }

            // Companies
            LoadAllCompanies();
            LoadAssignedCompanies();
        }
        #endregion

        #region Add/Remove Roles
        protected void btnAddRole_Click(object sender, System.EventArgs e)
        {
            foreach (ListItem item in lbAllRoles.Items)
            {
                if (item.Selected)
                {
                    m_dataManager.QueryCriteria.Clear();
                    m_dataManager.QueryCriteria.And(JoinPath.Role.Columns.Id, Int32.Parse(item.Value), MatchType.Exact);
                    Role role = this.m_dataManager.GetRole(FetchPath.Role);

                    //UserRole ur = m_dataManager.NewUserRole(this.m_user, role);
                    UserRole ur = m_dataManager.NewUserRole( role,this.m_user);
                    Commit();
                }
            }
            // Roles
            LoadAllRoles();
            LoadAssignedRoles();
        }

        protected void btnRemoveRole_Click(object sender, System.EventArgs e)
        {
            foreach (ListItem item in lbAssignedRoles.Items)
            {
                if (item.Selected)
                {
                    m_dataManager.QueryCriteria.Clear();
                    m_dataManager.QueryCriteria.And(JoinPath.UserRole.Columns.UserId, this.m_user.Id, MatchType.Exact);
                    UserRoleCollection userRoles = this.m_dataManager.GetUserRoleCollection();

                    UserRole userRole = userRoles.FindByRoldId(item.Value);

                    userRole.Delete();
                    Commit();
                }
            }
            // Roles
            LoadAllRoles();
            LoadAssignedRoles();
        }
        #endregion

        #region Enable/Disable User
        protected void Confirmlinkbutton1_Click(object sender, System.EventArgs e)
        {
            // Validate that no companies, or roles selected
            //if (lbAssignedCompanies.Items.Count <= 0 || lbAssignedRoles.Items.Count <= 0)
            if (this.m_user.Active)
            {
                this.m_user.Active = false;
                Commit();
            }
            else
            {
                this.m_user.Active = true;
                Commit();
            }

            Response.Redirect("Users.aspx");

            #region Failed attempts to auto delete Companies/Roles
            //m_dataManager.QueryCriteria.And(JoinPath.User.Columns.Id, this.m_user.Id, MatchType.Exact);
            //.And(JoinPath.User.UserCompany.Columns.UserId, this.m_user.Id, MatchType.Exact)
            //.And(JoinPath.User.UserRole.Columns.UserId, this.m_user.Id, MatchType.Exact);

            //			m_dataManager.QueryCriteria.Clear();
            //			m_dataManager.QueryCriteria.And(JoinPath.User.UserCompany.Columns.UserId, this.m_user.Id, MatchType.Exact);
            //			this.m_user = m_dataManager.GetUser(FetchPath.User.UserCompany);
            //			UserCompanyCollection ucc = this.m_user.UserCompanys;
            //			foreach (UserCompany uc in ucc)
            //			{
            //				uc.Delete();
            //			}
            //
            //			m_dataManager.QueryCriteria.Clear();
            //			m_dataManager.QueryCriteria.And(JoinPath.User.UserRole.Columns.UserId, this.m_user.Id, MatchType.Exact);
            //			this.m_user = m_dataManager.GetUser(FetchPath.User.UserRole);
            //			UserRoleCollection urc = this.m_user.UserRoles;
            //			foreach (UserRole ur in urc)
            //			{
            //				ur.Delete();
            //			}

            //			// Delete any UserRole records.
            //			m_dataManager.QueryCriteria.Clear();
            //			m_dataManager.QueryCriteria.And(JoinPath.User.Columns.Id, this.m_user.Id, MatchType.Exact);
            //			//User userR = m_dataManager.GetUser(FetchPath.User.UserRole);
            //			UserRoleCollection userRoles = this.m_dataManager.GetUserRoleCollection();
            //
            //			if (userRoles != null)
            //			{
            //				foreach (UserRole ur in userRoles)
            //				{
            //					ur.Delete();
            //				}
            //			}

            // Should be okay to delete the user now
            //m_dataManager.QueryCriteria.Clear();
            //m_dataManager.QueryCriteria.And(JoinPath.User.Columns.Id, Int32.Parse(this.m_id), MatchType.Exact);
            //this.m_user = m_dataManager.GetUser(FetchPath.User);

            //Commit();

            //Response.Redirect("Users.aspx");
            #endregion
        }
        #endregion

        protected void ddlPrimaryCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            int companySelected = ddlPrimaryCompany.SelectedIndex > 0 ? Convert.ToInt32(ddlPrimaryCompany.SelectedValue) : 0;
            //SetAgencyType(companySelected);
            //if (ConfigValues.UsesAPS(companySelected))
            //{
            //    ddlAPSAgency.CompanyId = ddlPrimaryCompany.SelectedIndex > 0 ? Convert.ToInt32(ddlPrimaryCompany.SelectedValue) : 0;
            //    ddlAPSAgency.DataBind();
            //}
            //else
            //{
                LoadAgencies();
            //}

            m_dataManager.QueryCriteria.Clear();
            m_dataManager.QueryCriteria.And(JoinPath.User.Columns.Id, this.m_user.Id, MatchType.Exact);
            User modifyUser = m_dataManager.GetUser();
            modifyUser.PrimaryCompanyId = System.Data.SqlTypes.SqlInt32.Null;
            modifyUser.AgencyId = System.Data.SqlTypes.SqlInt32.Null;
            if (ddlPrimaryCompany.SelectedValue.Length > 0)
            {
                modifyUser.PrimaryCompanyId = Convert.ToInt32(ddlPrimaryCompany.SelectedValue);
            }
            Commit();
        }
        protected void ddlAgency_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            m_dataManager.QueryCriteria.Clear();
            m_dataManager.QueryCriteria.And(JoinPath.User.Columns.Id, this.m_user.Id, MatchType.Exact);
            User modifyUser = m_dataManager.GetUser();
            modifyUser.AgencyId = System.Data.SqlTypes.SqlInt32.Null;
            if (ddl.SelectedValue.Length > 0)
            {
                modifyUser.AgencyId = Convert.ToInt32(ddl.SelectedValue);
            }
            Commit();
        }
        protected void ddlAgency_Init(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            //if (ConfigValues.UsesAPS(ddlPrimaryCompany.SelectedIndex > 0 ? Convert.ToInt32(ddlPrimaryCompany.SelectedValue) : 0))
            //{

            //}
            //else
            //{
                ddl.DataSource = GetSource();
                ddl.DataTextField = "Text";
                ddl.DataValueField = "Value";
            //}
        }

        private void LoadAgencies()
        {
            ddlAgency.DataSource = GetSource();
            ddlAgency.DataBind();
        }
        private ListItemCollection GetSource()
        {
            m_dataManager.QueryCriteria.Clear();
            //m_dataManager.QueryCriteria.And(JoinPath.Agency.Columns.Active, true);            
            m_dataManager.QueryCriteria.And(JoinPath.Agency.CompanyNumber.Company.Columns.Id,
                ddlPrimaryCompany.SelectedIndex > -1 ? ddlPrimaryCompany.SelectedValue : "");
            AgencyCollection ac = m_dataManager.GetAgencyCollection();

            ListItemCollection lic = new ListItemCollection();
            lic.Add(new ListItem("", ""));
            foreach (Agency a in ac)
            {
                lic.Add(new ListItem(string.Format("{0} ({1})", a.AgencyName, a.AgencyNumber), a.Id.ToString()));
            }
            ListItemCollection slic = new ListItemCollection();
            slic.AddRange(Common.SortListItems(lic));

            lic = slic;
            return lic;
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            this.m_user.Notes = txNotes.Text;
            Commit();            
        }
        private void Commit()
        {
            m_dataManager.CommitAll();
            Cache.Remove(string.Format(CacheKeys.UserRoles, m_user.Username));
            Cache.Remove(string.Format(CacheKeys.UserCompanies, m_user.Username));
            Cache.Remove(string.Format(CacheKeys.UserAgency, m_user.Username));
        }
}
}
