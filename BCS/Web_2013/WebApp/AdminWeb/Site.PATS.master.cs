namespace BCS.WebApp.Admin
{
    using System;
    using System.Data;
    using System.Configuration;
    using System.Collections;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;
    using System.Web.UI.HtmlControls;
    using Components;

    public partial class Site_PATS : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Session alert handling
            bool implementSessionAlert = false;
            try
            {
                implementSessionAlert = Convert.ToBoolean(ConfigurationManager.AppSettings["ImplementSessionAlert"]);
            }
            catch
            { /*do nothing */ }
            if (implementSessionAlert)
            {
                Session[SessionKeys.UserSession] = DateTime.Now;
                object objSection = ConfigurationManager.GetSection("system.web/sessionState");

                // Get the section related object.
                System.Web.Configuration.SessionStateSection sessionStateSection =
                    (System.Web.Configuration.SessionStateSection)objSection;


                bool addAlert = true;
                int minutesBeforePrefernce = 4;
                try
                {
                    minutesBeforePrefernce = Convert.ToInt32(ConfigurationManager.AppSettings["MinutesBeforePrefernce"]);
                    if (minutesBeforePrefernce >= sessionStateSection.Timeout.Minutes)
                    {
                        // TODO: log
                        addAlert = false;
                    }
                }
                catch
                { /*do nothing */ }

                if (addAlert)
                {
                    DateTime sessionStartTime = Common.GetSafeDateTimeFromSession(SessionKeys.UserSession);
                    DateTime alertAfterTime = sessionStartTime.AddMinutes(sessionStateSection.Timeout.Minutes - minutesBeforePrefernce);
                    TimeSpan alertAfter = alertAfterTime.Subtract(sessionStartTime);

                    string openBrace = "{";
                    string closBrace = "}";
                    string newLine = "\\\\r\\\\n\\\\r\\\\n";
                    string tab = "\\\\r";

                    string jsAlertScript =
                        string.Format("var timeOutId; timeOutId = setTimeout(\" var confirmResult = confirm('Session will end at {2} in {0} minutes. {5}{6}OK - to go to the landing page and start a new Session. {5}{6}Cancel - to stay on the current page. Canceling and interaction after session ends might cause unexpected results.'); if(confirmResult) {3} self.location = '.'; {4} /*else {3} alert(new Date().toLocaleTimeString()); {4}*/\", {1});",
                        minutesBeforePrefernce, alertAfter.TotalMilliseconds, sessionStartTime.AddMinutes(sessionStateSection.Timeout.Minutes).ToLongTimeString(), openBrace, closBrace, newLine, tab);

                    string jsDisplayScript =
                                        string.Format("var timeOutId4display; timeOutId4display = setTimeout(\" var confirmResult = confirm('Session will end at {2} in {0} minutes. {5}{6}OK - to go to the landing page and start a new Session. {5}{6}Cancel - to stay on the current page. Canceling and interaction after session ends might cause unexpected results.'); if(confirmResult) {3} self.location = '.'; {4} /*else {3} alert(new Date().toLocaleTimeString()); {4}*/\", {1});",
                                        minutesBeforePrefernce, alertAfter.TotalMilliseconds, sessionStartTime.AddMinutes(sessionStateSection.Timeout.Minutes).ToLongTimeString(), openBrace, closBrace, newLine, tab);

                    Page.ClientScript.RegisterStartupScript(
                        typeof(string),
                        "SessionTimeOutAlert",
                        jsAlertScript,
                        true);
                }
            }
            #endregion
            this.lblEnvironment.Text = DefaultValues.EnvironmentTitle;

            #region Apply Theme
            // get theme.
            int companyId = 0;
            Int32.TryParse(Common.GetCookie(SessionKeys.CompanyId), out companyId);
            string themeStyleSheeet =
                BCS.WebApp.Components.DefaultValues.ThemeStyleSheet(companyId);
            this.page_head.Controls.Add(new LiteralControl("<link href=\"" + themeStyleSheeet + "\" rel=\"stylesheet\" type=\"text/css\" />"));
            #endregion

            #region Apply javascripts

            HtmlGenericControl Include = new HtmlGenericControl("script");
            Include.Attributes.Add("type", "text/javascript");
            Include.Attributes.Add("src", string.Concat(DefaultValues.ScriptPath, "Calendar.js"));

            this.page_head.Controls.Add(Include);
            #endregion
        }
    }
}