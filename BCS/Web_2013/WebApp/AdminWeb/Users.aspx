<%@ Page language="c#" MasterPageFile="Site.PATS.master" Title="Berkley Clearance System : Users" Inherits="BCS.WebApp.Admin.Users" CodeFile="Users.aspx.cs" Theme="DefaultTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
	<aje:ScriptManager id="ScriptManager1" runat="server" OnAsyncPostBackError="ScriptManager1_AsyncPostBackError"></aje:ScriptManager>
    <span class="heading">Admin: Users</span>
    <table cellpadding="0" cellspacing="0" class="box">
        <tbody>
        <tr>
            <td class="TL"><div /></td>
            <td class="TC">&nbsp;</td>
            <td class="TR"><div /></td>
        </tr>
        <tr>
            <td class="ML"><div /></td>
            <td class="MC">
            
	            <ajx:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                    <ajx:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>Search</HeaderTemplate>
                        <ContentTemplate>
		                    <table width="100%" class="grid">
		                        <tr>
		                            <td bordercolor="white" style="padding-left:10px;padding-bottom:15px;padding-top:20px;">
		                                <P>
		                                <asp:textbox id="txSearchName" runat="server" CssClass="largeText"></asp:textbox>&nbsp;
		                                <asp:button id="btsSearch" runat="server" CssClass="button" Text="Search" onclick="btsSearch_Click"></asp:button>&nbsp;
		                                <asp:Button id="btnClear" runat="server" Text="Clear Search" CssClass="button" onclick="btnClear_Click"></asp:Button>
		                                </P>
	                                    <p>
		                                <asp:datagrid id="dgSearchUsers" runat="server" AllowPaging="True" PageSize="10" BorderColor="#999999"
			                                BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="3" GridLines="Vertical"
			                                AutoGenerateColumns="False">
			                                <FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
			                                <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
			                                <AlternatingItemStyle BackColor="#dcdcdc"></AlternatingItemStyle>
			                                <ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
			                                <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#00563F"></HeaderStyle>
			                                <Columns>
				                                <asp:TemplateColumn>
					                                <ItemTemplate>
						                                <input type="button" value="Edit" runat="server" ID="btnEdit" NAME="btnEdit" class="button"
							                                title="Click to edit" />
					                                </ItemTemplate>
				                                </asp:TemplateColumn>
				                                <asp:BoundColumn Visible="False" DataField="Id" ReadOnly="True" HeaderText="Id"></asp:BoundColumn>
				                                <asp:BoundColumn Visible="False" DataField="Active" HeaderText="Active"></asp:BoundColumn>
				                                <asp:BoundColumn DataField="Username" HeaderText="Username"></asp:BoundColumn>
				                                <asp:BoundColumn DataField="CompanyName" HeaderText="Companies"></asp:BoundColumn>
				                                <asp:BoundColumn DataField="RoleName" HeaderText="Roles"></asp:BoundColumn>
			                                </Columns>
			                                <PagerStyle HorizontalAlign="Center" ForeColor="Black" Position="TopAndBottom" BackColor="#999999"
				                                Mode="NumericPages"></PagerStyle>
		                                </asp:datagrid>
		                                </p>
		                            </td>
		                        </tr>
		                    </table>
		                </ContentTemplate>
                    </ajx:TabPanel>
                    <ajx:TabPanel ID="TabPanel2" runat="server">
                        <HeaderTemplate>Add User</HeaderTemplate>
                        <ContentTemplate>
                            <table width="100%">
		                        <tr>
		                            <td bordercolor="white" style="padding-left:10px;padding-bottom:15px;padding-top:20px;">
			                            <p>Please provide the new username below and click the Add button.</p>
			                            <P>
		                                <asp:TextBox id="txAddUsername" runat="server" CssClass="largeText"></asp:TextBox>&nbsp;
		                                <asp:Button id="btnAddUser" runat="server" Text="Add User" CssClass="button" onclick="btnAddUser_Click"></asp:Button>&nbsp;<asp:Label id="lblAddUserStatus" runat="server" ForeColor="#ff0000"></asp:Label><br />
		                                <asp:TextBox id="txPassword" runat="server" CssClass="largeText"></asp:TextBox><span class="light"> Password: required for external users.</span>
		                                </P>
	                                    <p>
	                                    <span style="margin-bottom:10px;font-weight:bold;">Select Primary Company:</span><br />
	                                    <asp:DropDownList ID="ddlPrimaryCompany" runat="server" CssClass="largeText"></asp:DropDownList>&nbsp;<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="ddlPrimaryCompany"></asp:RequiredFieldValidator>
	                                    </p>
			                            <p><I>If the username is accepted, you will then have the ability assign associated 
					                            companies and security roles.</I>
			                            </p>
		                            </td>
	                            </tr>
	                        </table>
                        </ContentTemplate>
                    </ajx:TabPanel>
                </ajx:TabContainer>
	    
	        </td>
            <td class="MR"><div /></td>
        </tr>
        <tr>
            <td class="BL"><div/></td>
            <td class="BC"><div/></td>
            <td class="BR"><div/></td>
        </tr>
        </tbody>
    </table>
</asp:Content>
