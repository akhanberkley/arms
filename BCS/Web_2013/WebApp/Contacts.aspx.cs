#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using BCS.WebApp.Components;
using System.Web.UI.HtmlControls;
#endregion

public partial class Contacts : System.Web.UI.Page
{
    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #endregion

    #region Control Events
    protected void Button1_Click(object sender, EventArgs e)
    {
        gridContacts.SelectedIndex = -1;
    }
    protected void gridContacts_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        if (!BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.Agency) && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
            DetailsView1.ChangeMode(DetailsViewMode.Edit);
    }
    protected void gridContacts_PageIndexChanged(object sender, EventArgs e)
    {
        gridContacts.SelectedIndex = -1;
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Master.GetCompanySelected() == 0)
        {
            Common.SetStatus(messagePlaceHolder, "Please select a company before adding an agency.");
            return;
        }
        DetailsView1.ChangeMode(DetailsViewMode.Insert);
    }
    protected void gridContacts_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        gridContacts.DataBind();
        DetailsView1.DataBind();
    }
    protected void odsContacts_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void DatePickerTextBox_Init(object sender, EventArgs e)
    {
        TextBox tb = sender as TextBox;
        tb.Attributes.Add("onclick", "scwShow(this,this);");
        tb.Attributes.Add("onblur", "this.value = purgeDate(this);");
        tb.Attributes.Add("onkeydown", "hideCalOnTab(event);");
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        gridContacts.DataBind();
        DetailsView1.DataBind();
    }
    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        gridContacts.DataBind();
        DetailsView1.DataBind();
    }
    protected void DetailsView1_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
    {
        gridContacts.DataBind();
        DetailsView1.DataBind();
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        bool b = Common.ShouldUpdate(e);

        e.Cancel = b;

        if (b)
        {
            // there was nothing to update. Just show the user the success message.
            Common.SetStatus(messagePlaceHolder, "Contact was Sucessfully updated.");
            DetailsView1.DataBind();
        }
    }
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        e.Cancel = BCS.WebApp.Components.Security.IsUserReadOnly(BCS.WebApp.Components.ClearanceReadOnlyContext.Agency) || ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
    }
    protected void odsContact_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void odsContact_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }

    }
    protected void odsContact_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        BCS.Core.Clearance.BizObjects.Contact contact = e.InputParameters[0] as BCS.Core.Clearance.BizObjects.Contact;
        if (contact.PrimaryPhone != null && contact.PrimaryPhone.Length > 0)
        {
            contact.PrimaryPhone = Common.DeFormatPhone(contact.PrimaryPhone);
        }
        if (contact.SecondaryPhone != null && contact.SecondaryPhone.Length > 0)
        {
            contact.SecondaryPhone = Common.DeFormatPhone(contact.SecondaryPhone);
        }
        if (contact.Fax != null && contact.Fax.Length > 0)
        {
            contact.Fax = Common.DeFormatPhone(contact.Fax);
        }
    }
    protected void odsContact_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void odsContact_Updating(object sender, ObjectDataSourceMethodEventArgs e)
    {
        BCS.Core.Clearance.BizObjects.Contact contact = e.InputParameters[0] as BCS.Core.Clearance.BizObjects.Contact;
        if (contact.PrimaryPhone != null && contact.PrimaryPhone.Length > 0)
        {
            contact.PrimaryPhone = Common.DeFormatPhone(contact.PrimaryPhone);
        }
        if (contact.SecondaryPhone != null && contact.SecondaryPhone.Length > 0)
        {
            contact.SecondaryPhone = Common.DeFormatPhone(contact.SecondaryPhone);
        }
        if (contact.Fax != null && contact.Fax.Length > 0)
        {
            contact.Fax = Common.DeFormatPhone(contact.Fax);
        }
    }
    protected void PhoneFormatControl_DataBinding(object sender, EventArgs e)
    {
        ITextControl tx = sender as ITextControl;
        tx.Text = Common.FormatPhone(tx.Text);
    } 
    #endregion

    protected void rvDOB_Init(object sender, EventArgs e)
    {
        RangeValidator rv = sender as RangeValidator;
        Common.SetDateOfBirthRange(rv);
    }
}
