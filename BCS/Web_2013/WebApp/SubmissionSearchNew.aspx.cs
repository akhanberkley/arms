#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.IO;

using OrmLib;
using BCS.Biz;
using BCS.Core;
using BCS.WebApp.Components;
using BCS.WebApp.UserControls;
using BCS.WebApp;
using Templates = BCS.WebApp.Templates;
using System.Drawing;
using BOA.Util.Extensions;
using AjaxControlToolkit;
using System.Linq;

#endregion

public partial class SubmissionSearchNew : System.Web.UI.Page
{
    #region Page Events
    private const string key4Searched = "Searched";
    private const string key4PreviousSortExpression = "PreviousSortExpression";
    private const string key4PreviousSortDirection = "PreviousSortDirection";
    private const string key4DefaultSort = "DefaultSort";
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void HyperLink1_Init(object sender, EventArgs e)
    {
        HyperLink1.Visible = (User as BCS.Core.Security.CustomPrincipal).IsInAnyRoles("User", "Partial User", "Auditor", "System Administrator");
    }

    protected override void OnInit(EventArgs e)
    {
        LoadAttributesAndCompanyCodes();
        base.OnInit(e);
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        if (Session["CompanyChanged"] != null)
        {
            Session.Contents.Remove("CompanyChanged");
            Response.Redirect("SubmissionSearchNew.aspx");
        }
        if (Session["StatusMessage"] != null)
        {
            Common.SetStatus(messagePlaceHolder);
            Session.Remove("StatusMessage");
        }
        LoadSearch();
        //btable.Visible = Session["FilterCriteria"] != null && gvSubmissions.Rows.Count != 0;
        base.OnLoadComplete(e);
    }

    protected override void OnLoad(EventArgs e)
    {
        if (!IsPostBack)
        {
            AgencyDropDownList1.DataBind();
            LineOfBusinessDropDownList1.DataBindByCompanyNumber(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));

            SubmissionTypeDropDownList1.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
            SubmissionTypeDropDownList1.DataBind();

            SubmissionStatusDropDownList1.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
            SubmissionStatusDropDownList1.DataBind();
        }
        ApplyCompanySpecificSearchFilters();
        base.OnLoad(e);
    }
    #endregion

    #region Control Events

    protected void DateBox_Init(object sender, EventArgs e)
    {
        (sender as TextBox).Attributes.Add("onclick", "scwShow(this,this);");
        (sender as TextBox).Attributes.Add("onkeydown", "hideCalOnTab(event);");
        (sender as TextBox).Attributes.Add("onblur", "this.value = purgeDate(this);");

    }

    protected void gvSubmissions_Init(object sender, EventArgs e)
    {
        #region required fields independent of company

        // col id, required
        BoundField bf = new BoundField();
        bf.HeaderText = "Id";
        bf.DataField = "Id";
        bf.Visible = false;
        gvSubmissions.Columns.Add(bf);

        //// col duplicate selection, required for duplicate selection along with the submission number and sequence
        TemplateField tf = new TemplateField();
        //tf.ItemTemplate = new Templates.CheckBoxTemplate(DataControlRowType.EmptyDataRow, BCS.WebApp.Templates.HeaderType.Text, "", "");
        //tf.HeaderTemplate = new Templates.CheckBoxTemplate(DataControlRowType.Header, BCS.WebApp.Templates.HeaderType.SelectAll, "", "");
        //gvSubmissions.Columns.Add(tf);

        // col client core client id, TODO: probably not needed
        bf = new BoundField();
        bf.HeaderText = "clientcore_client_id";
        bf.DataField = "clientcore_client_id";
        bf.Visible = false;
        gvSubmissions.Columns.Add(bf);

        // col submission no
        ButtonField snf = new ButtonField();
        snf.CommandName = "Select";
        snf.HeaderText = "Submission Number";
        snf.DataTextField = "submission_number";
        snf.ItemStyle.ForeColor = Color.FromArgb(00, 40, Convert.ToInt32("BF", 16));
        if (gvSubmissions.AllowSorting)
            snf.SortExpression = snf.DataTextField;
        gvSubmissions.Columns.Add(snf);

        // col submission no sequence
        //BoundField snsf = new BoundField();
        //snsf.HeaderText = "Sequence";
        //snsf.DataField = "submission_number_sequence";
        //if (gvSubmissions.AllowSorting)
        //    snsf.SortExpression = snsf.DataField;
        //gvSubmissions.Columns.Add(snsf);

        // col client address, required for making associated submissions visually distinct
        tf = new TemplateField();
        tf.ItemTemplate = new Templates.LabelTemplate(DataControlRowType.DataRow, "address", "clientcore_address_id", "lblClientAddress");
        tf.Visible = false;
        gvSubmissions.Columns.Add(tf);


        #endregion
        BCS.Biz.SubmissionDisplayGridCollection dfields = ConfigValues.GetSubmissionGridDisplaySC(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));

        #region Some Customization on required fields #3709
        // see if any customization is there on the submission_number column
        BCS.Biz.SubmissionDisplayGrid customField = dfields.FindByFieldName("submission_number");
        if (customField != null)
        {
            snf.HeaderText = string.IsNullOrEmpty(customField.HeaderText) ? customField.FieldName : customField.HeaderText;
            dfields.Remove(customField);
        }
        // see if any customization is there on the submission_number_sequence column
        //customField = dfields.FindByFieldName("submission_number_sequence");
        //if (customField != null)
        //{
        //    snsf.HeaderText = string.IsNullOrEmpty(customField.HeaderText) ? customField.FieldName : customField.HeaderText;
        //    dfields.Remove(customField);
        //}
        #endregion
        foreach (SubmissionDisplayGrid cfield in dfields)
        {
            TemplateField atf = new TemplateField();
            if (gvSubmissions.AllowSorting)
                atf.SortExpression = cfield.FieldName;
            atf.ItemTemplate = new Templates.GenericTemplate(DataControlRowType.DataRow, string.Empty, cfield.FieldName, cfield.AttributeDataType.DataTypeName, string.Empty);
            //atf.HeaderTemplate = new Templates.GenericTemplate(DataControlRowType.Header, string.IsNullOrEmpty(cfield.HeaderText) ? cfield.FieldName : cfield.HeaderText, string.Empty, string.Empty);
            atf.HeaderText = string.IsNullOrEmpty(cfield.HeaderText) ? cfield.FieldName : cfield.HeaderText;


            if (atf.HeaderText == "Status History")
                gvSubmissions.Columns[0].Visible = true;
            else
                gvSubmissions.Columns.Add(atf);


            // customisaion for Client Id column. Need to be displayed it as a link button. so add new column and remove column added by default
            if (atf.HeaderText == "Client ID")
            {
                ButtonField buf = new ButtonField();
                buf.HeaderText = cfield.HeaderText;
                buf.DataTextField = "clientcore_client_id";
                buf.CommandName = "EditClient";
                buf.ItemStyle.ForeColor = Color.FromArgb(00, 40, Convert.ToInt32("BF", 16));
                if (gvSubmissions.AllowSorting)
                    snf.SortExpression = buf.DataTextField;
                gvSubmissions.Columns.Add(buf);
                gvSubmissions.Columns.Remove(atf);
            }


        }

        //Need to display the history icon next to status column. so re arranging the column sequence here 
        if (gvSubmissions.Columns[0].Visible == true)
        {
            var colToRemove = gvSubmissions.Columns[0];
            int targetColIndex;
            foreach (DataControlField col in gvSubmissions.Columns)
            {
                if (col.HeaderText == "Status")
                {
                    targetColIndex = gvSubmissions.Columns.IndexOf(col);
                    gvSubmissions.Columns.RemoveAt(0);
                    gvSubmissions.Columns.Insert(targetColIndex, colToRemove);
                    break;
                }
            }

        }
    }
    protected void gvSubmissions_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ViewState["PossibleUpdates"] = BuildPossibleUpdates();
        string sort = string.Empty;
        if (ViewState[key4PreviousSortExpression] != null && ViewState[key4PreviousSortDirection] != null)
        {
            sort = string.Format("{0} {1}", ViewState[key4PreviousSortExpression], ViewState[key4PreviousSortDirection]);
        }
        if (string.IsNullOrEmpty(sort))
        {
            sort = (string)ViewState[key4DefaultSort];
        }
        LoadSubmissions(e.NewPageIndex, sort);
    }
    protected void gvSubmissions_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void gvSubmissions_DataBound(object sender, EventArgs e)
    {
        dataRowCount.Value = gvSubmissions.Rows.Count.ToString();
    }
    protected void gvSubmissions_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditClient")
        {
            SaveSearch();
            gvSubmissions.SelectedIndex = Convert.ToInt32(e.CommandArgument);
            Session["SubmissionId"] = gvSubmissions.SelectedDataKey.Values["id"];
            Session["ClientId"] = gvSubmissions.SelectedDataKey.Values["ClientCore_Client_Id"];
            Session["subref"] = "SubmissionSearchNew.aspx";
            Session["cliref"] = "SubmissionSearchNew.aspx";
            Response.Redirect("Client.aspx?op=edit");

        }
    }

    protected void AgencyDropDownList1_Init(object sender, EventArgs e)
    {
        Type t = sender.GetType();
        System.Reflection.PropertyInfo pi = t.GetProperty("CompanyNumberId");
        if (pi != null)
        {
            pi.SetValue(sender, Convert.ToInt32(Session["CompanyNumberSelected"]), new object[] { });
        }
        //AgencyDropDownList1.DataBind();
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        if (Common.GetSafeIntFromSession(SessionKeys.CompanyId) == 0)
        {
            Common.SetStatus(messagePlaceHolder, "Please select a company.");
            return;
        }
        if (Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) == 0)
        {
            Common.SetStatus(messagePlaceHolder, "Please select a company number.");
            return;
        }

        Session.Contents.Remove("FilterCriteria");
        Hidden1.Value = "0";
        if (Page.IsPostBack) // if only user clicked the button, this same method is also used when restore results back from other screens
            selectAllState.Value = "0";

        LoadSubmissions(0, string.Empty);
    }
    private void LoadSubmissions(int pageIndex, string sort)
    {
        string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);

        List<BCS.DAL.EFilter> efilters = new List<BCS.DAL.EFilter>();

        ArrayList filters = new ArrayList();

        if (txSubmissionStartDate.Text.Length > 0)
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.SubmissionDt, txSubmissionStartDate.Text, OrmLib.MatchType.GreaterOrEqual));
            efilters.Add(new BCS.DAL.EFilter("SubmissionDt", txSubmissionStartDate.Text.ToDateTime(), BCS.DAL.MatchType.GreaterThanOrEqual));
        }

        if (txSubmissionEndDate.Text.Length > 0)
        {
            DateTime date;
            DateTime.TryParse(txSubmissionEndDate.Text, out date);

            date = date.AddDays(1);

            filters.Add(new Triplet(JoinPath.Submission.Columns.SubmissionDt, date.ToString(), OrmLib.MatchType.LesserOrEqual));
            efilters.Add(new BCS.DAL.EFilter("SubmissionDt", date, BCS.DAL.MatchType.LessThanOrEqual));
        }

        if (AgencyDropDownList1.SelectedIndex > 0)
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.AgencyId, AgencyDropDownList1.SelectedValue, OrmLib.MatchType.Exact));
            efilters.Add(new BCS.DAL.EFilter("AgencyId", AgencyDropDownList1.SelectedValue, BCS.DAL.MatchType.Exact));
        }

        if (uddl.SelectedIndex > 0)
        {
            if (uddl.UsesAPS)
            {
                filters.Add(new Triplet(JoinPath.Submission.Columns.UnderwriterPersonId,
                BCS.Core.Clearance.Admin.DataAccess.GetPersonIdByAPSId(long.Parse(uddl.SelectedValue), Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId))
                , OrmLib.MatchType.Exact));

                efilters.Add(new BCS.DAL.EFilter("UnderwriterPersonId", BCS.Core.Clearance.Admin.DataAccess.GetPersonIdByAPSId(long.Parse(uddl.SelectedValue), Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId))
                    , BCS.DAL.MatchType.Exact));
            }
            else
            {
                filters.Add(new Triplet(JoinPath.Submission.Columns.UnderwriterPersonId,
                    uddl.SelectedValue, OrmLib.MatchType.Exact));

                efilters.Add(new BCS.DAL.EFilter("UnderwriterPersonId", uddl.SelectedValue
                    , BCS.DAL.MatchType.Exact));
            }
        }

        if (LineOfBusinessDropDownList1.SelectedIndex > 0)
        {
            filters.Add(new Triplet(JoinPath.Submission.SubmissionLineOfBusiness.Columns.LineOfBusinessId,
                LineOfBusinessDropDownList1.SelectedValue, OrmLib.MatchType.Exact));

            efilters.Add(new BCS.DAL.EFilter("SubmissionLineOfBusiness.LineOfBusinessId"
                    , LineOfBusinessDropDownList1.SelectedValue
                    , BCS.DAL.MatchType.Exact));
        }
        if (SubmissionTypeDropDownList1.SelectedIndex > 0)
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.SubmissionTypeId,
                SubmissionTypeDropDownList1.SelectedValue, OrmLib.MatchType.Exact));

            //filters.Add(new BCS.DAL.EFilter("SubmissionTypeId"
            //        , SubmissionTypeDropDownList1.SelectedValue
            //        , BCS.DAL.MatchType.Exact));
        }
        if (SubmissionStatusDropDownList1.SelectedIndex > 0)
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.SubmissionStatusId,
                SubmissionStatusDropDownList1.SelectedValue, OrmLib.MatchType.Exact));

            //filters.Add(new BCS.DAL.EFilter("SubmissionStatusId"
            //        , SubmissionStatusDropDownList1.SelectedValue
            //        , BCS.DAL.MatchType.Exact));
        }
        if (txEffFrom.Text.Length > 0)// TODO: VALIDATE
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.EffectiveDate, txEffFrom.Text, OrmLib.MatchType.GreaterOrEqual));
            filters.Add(new Triplet(JoinPath.Submission.Columns.EffectiveDate, txEffTo.Text, OrmLib.MatchType.LesserOrEqual));

            //filters.Add(new BCS.DAL.EFilter("EffectiveDate"
            //        , txEffFrom.Text
            //        , BCS.DAL.MatchType.GreaterThanOrEqual));
            //filters.Add(new BCS.DAL.EFilter("EffectiveDate"
            //        , txEffTo.Text
            //        , BCS.DAL.MatchType.LessThanOrEqual));
        }
        if (txExpFrom.Text.Length > 0)// TODO: VALIDATE
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.ExpirationDate, txExpFrom.Text, OrmLib.MatchType.GreaterOrEqual));
            filters.Add(new Triplet(JoinPath.Submission.Columns.ExpirationDate, txExpTo.Text, OrmLib.MatchType.LesserOrEqual));

            //filters.Add(new BCS.DAL.EFilter("ExpirationDate"
            //        , txExpFrom.Text
            //        , BCS.DAL.MatchType.GreaterThanOrEqual));
            //filters.Add(new BCS.DAL.EFilter("ExpirationDate"
            //        , txExpTo.Text
            //        , BCS.DAL.MatchType.LessThanOrEqual));
        }
        if (txPremium.Text.Length > 0)
        {
            decimal d = decimal.Zero;
            decimal.TryParse(txPremium.Text, out d);
            OrmLib.MatchType m = (OrmLib.MatchType)Enum.Parse(typeof(OrmLib.MatchType), ddlOperators.SelectedValue);
            filters.Add(new Triplet(JoinPath.Submission.Columns.EstimatedWrittenPremium, d, m));

            //filters.Add(new BCS.DAL.EFilter("EffectiveDate"
            //        , txEffFrom.Text
            //        , BCS.DAL.MatchType.GreaterThanOrEqual));
        }

        if (ddlUserCreatedBy.SelectedIndex > 0)
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.SubmissionBy, ddlUserCreatedBy.SelectedValue, MatchType.Exact));

            efilters.Add(new BCS.DAL.EFilter("SubmissionBy", ddlUserCreatedBy.SelectedValue, BCS.DAL.MatchType.Exact));
        }

        if (ddlUserModifiedBy.SelectedIndex > 0)
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.UpdatedBy, ddlUserModifiedBy.SelectedValue, MatchType.Exact));


            efilters.Add(new BCS.DAL.EFilter("UpdatedBy", ddlUserModifiedBy.SelectedValue, BCS.DAL.MatchType.Exact));
        }

        if (!string.IsNullOrWhiteSpace(txLastModifiedStart.Text))
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.UpdatedDt, txLastModifiedStart.Text, OrmLib.MatchType.GreaterOrEqual));

            efilters.Add(new BCS.DAL.EFilter("UpdatedDt", txLastModifiedStart.Text, BCS.DAL.MatchType.GreaterThanOrEqual));
        }

        if (!string.IsNullOrWhiteSpace(txLastModifiedEnd.Text))
        {
            filters.Add(new Triplet(JoinPath.Submission.Columns.UpdatedDt, txLastModifiedEnd.Text, OrmLib.MatchType.LesserOrEqual));

            efilters.Add(new BCS.DAL.EFilter("UpdatedDt", txLastModifiedEnd.Text, BCS.DAL.MatchType.LessThanOrEqual));
        }


        //Company Codes
        List<Triplet> codeFilters = new List<Triplet>();
        GetCompanyCodeFilters(plAttributesLeft, codeFilters);
        GetCompanyCodeFilters(plAttributesRight, codeFilters);

        if (codeFilters.Count > 0)
        {
            foreach (Triplet filter in codeFilters)
            {
                filters.Add(filter);
            }
        }

        Dictionary<int, string> attributeFilters = new Dictionary<int, string>();
        GetAttributeFilters(plAttributesLeft, attributeFilters);
        GetAttributeFilters(plAttributesRight, attributeFilters);

        if (filters.Count == 0 && attributeFilters.Count == 0)
        {
            gvSubmissions.DataSource = null;
            gvSubmissions.DataBind();
            return;
        }

        //Company Codes EF
        //List<BCS.DAL.EFilter> codeFiltersEF = new List<BCS.DAL.EFilter>();
        //GetCompanyCodeFiltersEF(plCodes, ref codeFiltersEF);

        //if (codeFiltersEF.Count > 0)
        //{
        //    foreach (BCS.DAL.EFilter filter in codeFiltersEF)
        //    {
        //        efilters.Add(filter);
        //    }
        //}


        Triplet[] criterion = new Triplet[filters.Count];
        filters.CopyTo(criterion);
        Session["FilterCriteria"] = criterion;

        string xml = string.Empty;

        //DateTime start = DateTime.Now;
        xml = BCS.Core.Clearance.Submissions.GetSubmissions(companyNumber, false, criterion, attributeFilters);
        DataTable dt = Common.GenerateDataTable(companyNumber, "submission", xml);


        ExtractIds(dt);

        if (dt != null)
        {
            lblResultCount.Text = string.Format("{0} Results", dt.Rows.Count);
            lblResultCount.Visible = true;
            ibtnExportToExcel.Visible = true;
            DataTable ndt = dt.Clone();
            ndt.Columns["submission_number"].DataType = typeof(int);
            ndt.Columns["submission_number_sequence"].DataType = typeof(int);
            foreach (DataRow dr in dt.Rows)
                ndt.ImportRow(dr);
            ndt.AcceptChanges();
            if (!string.IsNullOrEmpty(sort.Trim()))
            {
                ndt.DefaultView.Sort = sort;
            }
            else
            {
                ndt.DefaultView.Sort = dt.DefaultView.Sort;
                ViewState[key4DefaultSort] = dt.DefaultView.Sort;
            }

            gvSubmissions.PageIndex = pageIndex;
            gvSubmissions.DataSource = ndt;
            gvSubmissions.DataBind();
            ViewState["SearchResults"] = ndt;
        }
        else
        {
            lblResultCount.Text = string.Empty;
            lblResultCount.Visible = false;
            gvSubmissions.DataSource = dt;
            gvSubmissions.DataBind();
        }
    }
    protected void gvSubmissions_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["PossibleUpdates"] = BuildPossibleUpdates();

        string sortDirection = " ASC";
        if ((string)ViewState[key4PreviousSortExpression] == e.SortExpression)
        {
            if ((string)ViewState[key4PreviousSortDirection] == sortDirection)
            {
                sortDirection = " DESC";
            }
        }
        ViewState[key4PreviousSortDirection] = sortDirection;
        ViewState[key4PreviousSortExpression] = e.SortExpression;

        string sort = e.SortExpression.ToString() + sortDirection;

        LoadSubmissions(gvSubmissions.PageIndex, sort);

    }

    private void ExtractIds(DataTable dataTable)
    {
        if (dataTable != null)
        {
            StringBuilder dups = new StringBuilder();

            foreach (DataRow var in dataTable.Rows)
            {
                dups.AppendFormat("{0},", var["Id"].ToString());
            }
            ViewState["AllIds"] = dups;
        }
    }
    protected void btnReassign_Click(object sender, EventArgs e)
    {
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        AgencyDropDownList1.SelectedIndex = 0;
        txSubmissionEndDate.Text = string.Empty;
        txSubmissionStartDate.Text = string.Empty;
        LineOfBusinessDropDownList1.SelectedIndex = 0;
        SubmissionStatusDropDownList1.SelectedIndex = 0;
        SubmissionTypeDropDownList1.SelectedIndex = 0;

        txLastModifiedEnd.Text = string.Empty;
        txLastModifiedStart.Text = string.Empty;

        ddlUserCreatedBy.SelectedIndex = -1;
        ddlUserModifiedBy.SelectedIndex = -1;

        uddl.SelectedIndex = -1;

        ResetCompanyAttributes(plAttributesLeft);
        ResetCompanyAttributes(plAttributesRight);

        txEffFrom.Text = string.Empty;
        txEffTo.Text = string.Empty;
        txExpFrom.Text = string.Empty;
        txExpTo.Text = string.Empty;
        txPremium.Text = string.Empty;

        btnFilter_Click(null, EventArgs.Empty);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("SubmissionSearchNew.aspx");
    }
    protected void btnReassign_Init(object sender, EventArgs e)
    {
    }

    protected void uddl_Init(object sender, EventArgs e)
    {
        (sender as UnderwritersDropDownList).CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        (sender as UnderwritersDropDownList).DataBindByCompanyNumber();
    }

    protected void gvSubmissions_SelectedIndexChanged(object sender, EventArgs e)
    {
        SaveSearch();
        GridView gv = sender as GridView;
        Session["SubmissionId"] = gv.SelectedDataKey.Value;
        Session["SubmissionId"] = gv.SelectedDataKey.Values["id"];
        Session["ClientId"] = gv.SelectedDataKey.Values["ClientCore_Client_Id"];
        Session["subref"] = "SubmissionSearchNew.aspx";
        Session["cliref"] = "SubmissionSearchNew.aspx";
        if (DefaultValues.UseWizard)
            Response.Redirect("SubmissionWizard.aspx");
        Response.Redirect("Submission.aspx?op=edit");
    }

    protected void ValidationButton_Init(object sender, EventArgs e)
    {
        //Button btn = sender as Button;
        //btn.Attributes.Add("onclick", string.Format("if(!Page_ClientValidate('{0}')) {{  $find('alwaysVisible').get_element().style.display = ''; $find('alwaysVisible').initialize(); }}", btn.ValidationGroup));
    }
    #endregion

    #region Other Methods

    private void SaveSearch()
    {
        ReassignScreen thisScreen = new ReassignScreen();
        thisScreen.AgencyId = Common.GetSafeSelectedValue(AgencyDropDownList1);
        thisScreen.FromEff = txEffFrom.Text;
        thisScreen.ToEff = txEffTo.Text;
        thisScreen.FromExp = txExpFrom.Text;
        thisScreen.ToExp = txExpTo.Text;
        thisScreen.LobId = Common.GetSafeSelectedValue(LineOfBusinessDropDownList1);
        thisScreen.TypeId = Common.GetSafeSelectedValue(SubmissionTypeDropDownList1);
        thisScreen.StatusId = Common.GetSafeSelectedValue(SubmissionStatusDropDownList1);

        thisScreen.SubmissionDateFrom = txSubmissionStartDate.Text;
        thisScreen.SubmissionDateTo = txSubmissionEndDate.Text;

        if (uddl.UsesAPS)
        {
            thisScreen.UnderwriterId1 = BCS.Core.Clearance.Admin.DataAccess.GetPersonIdByAPSId(Common.GetSafeSelectedValueLong(uddl), Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
            //thisScreen.AnalystId = BCS.Core.Clearance.Admin.DataAccess.GetPersonIdByAPSId(Common.GetSafeSelectedValueLong(AnalystDropDownList1), Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
            //thisScreen.TechnicianId = BCS.Core.Clearance.Admin.DataAccess.GetPersonIdByAPSId(Common.GetSafeSelectedValueLong(TechnicianDropDownList1), Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
        }
        else
        {
            thisScreen.UnderwriterId1 = Common.GetSafeSelectedValue(uddl);
            //thisScreen.AnalystId = Common.GetSafeSelectedValue(AnalystDropDownList1);
            //thisScreen.TechnicianId = Common.GetSafeSelectedValue(TechnicianDropDownList1);
        }

        thisScreen.FromExp = txEffFrom.Text;
        thisScreen.ToExp = txEffTo.Text;
        thisScreen.FromExp = txExpFrom.Text;
        thisScreen.ToExp = txExpTo.Text;
        thisScreen.PremiumComparisonType = ddlOperators.Text;
        decimal d = decimal.Zero;
        decimal.TryParse(txPremium.Text, out d);
        thisScreen.Premium = d;
        thisScreen.PossibleUpdates = BuildPossibleUpdates();
        thisScreen.SelectedAll = selectAllState.Value;
        thisScreen.Sort = string.Format("{0} {1}", ViewState[key4PreviousSortExpression], ViewState[key4PreviousSortDirection]);
        thisScreen.PageIndex = gvSubmissions.PageIndex;

        if (ddlUserCreatedBy.SelectedIndex > 0)
            thisScreen.CreatedByUser = ddlUserCreatedBy.SelectedValue;

        if (ddlUserModifiedBy.SelectedIndex > 0)
            thisScreen.ModifiedByUser = ddlUserModifiedBy.SelectedValue;

        thisScreen.ModifiedDateFrom = txLastModifiedStart.Text;
        thisScreen.ModifiedDateTo = txLastModifiedEnd.Text;

        List<KeyValuePair<string, string>> attributes = new List<KeyValuePair<string, string>>();
        GetAttributeValues(plAttributesLeft, ref attributes);
        GetAttributeValues(plAttributesRight, ref attributes);
        thisScreen.Attributes = attributes;

        SearchCriteria sc = new SearchCriteria();
        sc.ReassignScreen = thisScreen;
        Session[SessionKeys.SearchedCriteria] = sc;
    }
    private void LoadSearch()
    {
        if (null != Session["LoadSavedSearch"])
        {
            Session.Contents.Remove("LoadSavedSearch");
            if (null != Session[SessionKeys.SearchedCriteria])
            {
                // TODO: 
                SearchCriteria sc = (SearchCriteria)Session[SessionKeys.SearchedCriteria];
                AgencyDropDownList1.Text = sc.ReassignScreen.AgencyId.ToString();
                LineOfBusinessDropDownList1.Text = sc.ReassignScreen.LobId.ToString();
                SubmissionTypeDropDownList1.Text = sc.ReassignScreen.TypeId.ToString();
                SubmissionStatusDropDownList1.Text = sc.ReassignScreen.StatusId.ToString();
                txEffFrom.Text = sc.ReassignScreen.FromEff;
                txEffTo.Text = sc.ReassignScreen.ToEff;
                txExpFrom.Text = sc.ReassignScreen.FromExp;
                txExpTo.Text = sc.ReassignScreen.ToExp;
                txPremium.Text = sc.ReassignScreen.Premium == decimal.Zero ? string.Empty : string.Format("{0:c}", sc.ReassignScreen.Premium);
                ViewState["PossibleUpdates"] = sc.ReassignScreen.PossibleUpdates;
                selectAllState.Value = sc.ReassignScreen.SelectedAll;
                txSubmissionStartDate.Text = sc.ReassignScreen.SubmissionDateFrom;
                txSubmissionEndDate.Text = sc.ReassignScreen.SubmissionDateTo;

                uddl.Text = sc.ReassignScreen.UnderwriterId1.ToString();

                ddlUserCreatedBy.SelectedValue = sc.ReassignScreen.CreatedByUser;
                ddlUserModifiedBy.SelectedValue = sc.ReassignScreen.ModifiedByUser;

                txLastModifiedStart.Text = sc.ReassignScreen.ModifiedDateFrom;
                txLastModifiedEnd.Text = sc.ReassignScreen.ModifiedDateTo;

                foreach (var pair in sc.ReassignScreen.Attributes)
                {
                    Control ctrl = Common.FindControlRecursive(plAttributesLeft, pair.Key);
                    ctrl = (ctrl == null) ? Common.FindControlRecursive(plAttributesRight, pair.Key) : ctrl;
                    if (ctrl != null)
                    {
                        if (ctrl is DropDownList)
                            (ctrl as DropDownList).SelectedValue = pair.Value;
                        else if (ctrl is TextBox)
                            (ctrl as TextBox).Text = pair.Value;
                        else if (ctrl is RadioButtonList)
                            (ctrl as RadioButtonList).SelectedValue = pair.Value;
                    }
                }

                LoadSubmissions(sc.ReassignScreen.PageIndex, sc.ReassignScreen.Sort);
            }
        }
    }
    private bool ReadyState()
    {
        return Hidden1.Value != "0" || selectAllState.Value != "0" ||
            (ViewState["PossibleUpdates"] != null && ViewState["PossibleUpdates"].ToString().Length > 0);
    }
    private string BuildPossibleUpdates()
    {
        return string.Empty;
    }

    private void ApplyCompanySpecificSearchFilters()
    {
        int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        BCS.DAL.SubmissionSearchPage filters = BCS.Core.Clearance.SubmissionsEF.GetSubmissionSearchPageFiltersByCompanyNumberID(companyNumberId);
        if (filters != null)
        {
            List<System.Web.UI.HtmlControls.HtmlTableRow> allControls = new List<System.Web.UI.HtmlControls.HtmlTableRow>();
            GetControlList(Page.Controls, allControls);

            foreach (System.Web.UI.HtmlControls.HtmlTableRow trow in allControls)
                if (!String.IsNullOrEmpty(trow.ID))
                    if (filters.GetValueFromEntity("Display" + trow.ID).ToString() == "False")
                        trow.Attributes.CssStyle.Add(HtmlTextWriterStyle.Display, "none");
            //trow.Visible = String.IsNullOrEmpty(filters.GetValueFromEntity("Display" + trow.ID).ToString()) ? true : (bool)filters.GetValueFromEntity("Display" + trow.ID);                 
        }

    }


    private void GetControlList<T>(ControlCollection coll, List<T> resultColl)
    where T : System.Web.UI.HtmlControls.HtmlTableRow
    {
        foreach (Control control in coll)
        {
            if (control is T)
                resultColl.Add((T)control);

            if (control.HasControls())
                GetControlList(control.Controls, resultColl);
        }
    }

    private void LoadAttributesAndCompanyCodes()
    {
        //Get both lists and determine the sorting order
        int companyNumber = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        BCS.Biz.CompanyNumberAttributeCollection attributes = BCS.Core.Clearance.Lookups.GetCompanyNumberAttributesByCompanyNumberId(companyNumber.ToString(), false);
        attributes = attributes.SortByDisplayOrder(OrmLib.SortDirection.Ascending);
        List<BCS.DAL.CompanyNumberCodeType> types = BCS.Core.Clearance.SubmissionsEF.GetCompanyNumberCodeTypes(companyNumber);

        int totalCount = 0;
        List<int> listDisplayOrder = new List<int>();
        if (types != null)
        {
            totalCount += types.Count;
            foreach (var t in types)
                if (!listDisplayOrder.Contains(t.DisplayOrder))
                    listDisplayOrder.Add(t.DisplayOrder);
        }
        if (attributes != null)
        {
            totalCount += attributes.Count;
            foreach (BCS.Biz.CompanyNumberAttribute var in attributes)
                if (!listDisplayOrder.Contains(var.DisplayOrder.Value))
                    listDisplayOrder.Add(var.DisplayOrder.Value);
        }
        listDisplayOrder.Sort();

        int halfCount = (totalCount / 2) + ((totalCount % 2 == 0) ? 0 : 1);

        plAttributesLeft.Controls.Clear();
        plAttributesLeft.Controls.Add(new HtmlTable());
        plAttributesRight.Controls.Clear();
        plAttributesRight.Controls.Add(new HtmlTable());

        int currentControlCount = 0;
        foreach (int currentDisplayIndex in listDisplayOrder)
        {
            if (types != null)
            {
                foreach (var type in types.Where(t => t.DisplayOrder == currentDisplayIndex))
                {
                    currentControlCount++;
                    HtmlTableRow tr = new HtmlTableRow();
                    ((HtmlTable)((currentControlCount <= halfCount) ? plAttributesLeft.Controls[0] : plAttributesRight.Controls[0])).Rows.Add(tr);

                    HtmlTableCell td = new HtmlTableCell();
                    tr.Cells.Add(td);
                    td.InnerText = type.CodeName;

                    td = new HtmlTableCell();
                    tr.Cells.Add(td);

                    DropDownList ddl = new BCS.WebApp.UserControls.CompanyNumberCodeTypeDropDown(type);
                    ddl.ID += "C";
                    AjaxControlToolkit.ListSearchExtender lse = new AjaxControlToolkit.ListSearchExtender();
                    lse.TargetControlID = ddl.ID;
                    lse.ID = ddl.ID + "LSE";

                    td.Controls.Add(ddl);
                    td.Controls.Add(lse);

                    if (type.CodeName == "Policy Symbol")
                    {
                        hdnPolicySymbolddlID.Value = string.Format("ddl-{0}", type.Id);
                        ddl.Attributes.Add("onfocus", "FilterPolicySymbolDropDownByEffectiveDate(this);");
                        ddl.Attributes.Add("onmouseover", "FilterPolicySymbolDropDownByEffectiveDate(this);");
                        ddl.Width = Unit.Pixel((int)55);
                    }
                }
            }

            if (attributes != null)
            {
                foreach (BCS.Biz.CompanyNumberAttribute attr in attributes)
                    if (attr.DisplayOrder.Value == currentDisplayIndex)
                    {
                        currentControlCount++;
                        HtmlTableRow row = new HtmlTableRow();
                        ((HtmlTable)((currentControlCount <= halfCount) ? plAttributesLeft.Controls[0] : plAttributesRight.Controls[0])).Rows.Add(row);

                        HtmlTableCell cell = new HtmlTableCell();
                        row.Cells.Add(cell);
                        cell.InnerText = attr.Label;

                        cell = new HtmlTableCell();
                        row.Cells.Add(cell);

                        Type t = Type.GetType(string.Concat(attr.AttributeDataType.UserControl, ", System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), false, true);
                        System.Reflection.ConstructorInfo ci = t.GetConstructor(new Type[] { });
                        WebControl c = (WebControl)ci.Invoke(null);
                        //If this is a checkbox, we need to turn it into a radiobuttonlist so the user can specify they don't want to filter on this
                        if (c is CheckBox)
                            c = new RadioButtonList()
                            {
                                RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal,
                                Items = { new ListItem("Any", ""), new ListItem("No", false.ToString()), new ListItem("Yes", true.ToString()) },
                                SelectedIndex = 0
                            };

                        c.ID = attr.Id.ToString() + "A";
                        if (attr.AttributeDataType.DataTypeName.Equals("Date", StringComparison.CurrentCultureIgnoreCase))
                        {
                            c.Attributes.Add("onclick", "scwShow(this,this);");
                            c.Attributes.Add("onkeydown", "hideCalOnTab(event);");
                            c.Attributes.Add("onblur", "this.value = purgeDate(this);");
                        }
                        if (attr.AttributeDataType.DataTypeName.Equals("Money", StringComparison.CurrentCultureIgnoreCase))
                            c.Attributes.Add("onblur", "this.value=formatCurrency(this.value);");

                        Common.AddControl(cell, c, false, true);
                        if (attr.AttributeDataType.DataTypeName.Equals("Date", StringComparison.CurrentCultureIgnoreCase))
                        {
                            CompareValidator cv = new CompareValidator();
                            Common.AddControl(cell, cv, false, false);
                            cv.ControlToValidate = c.ID;
                            cv.SetFocusOnError = true;
                            cv.Type = ValidationDataType.Date;
                            cv.Operator = ValidationCompareOperator.DataTypeCheck;
                            cv.ErrorMessage = "&nbsp;";
                            cv.Text = "* MM/dd/yyyy";
                        }
                        if (attr.ReadOnlyProperty)
                            c.Enabled = false;
                    }
            }
        }
    }

    public int GetIdFromControl(Control ctrl)
    {
        if (ctrl == null) return 0;

        string id = ctrl.ID.Split('-')[1];

        return id.ToInt();
    }

    private void GetCompanyCodeFilters(Control ctrl, List<Triplet> filters)
    {
        if (ctrl != null && ctrl.Controls != null & ctrl.Controls.Count > 0)
        {
            foreach (Control cctrl in ctrl.Controls)
            {
                if (cctrl is DropDownList)
                {
                    if ((cctrl as DropDownList).SelectedIndex > 0)
                        filters.Add(new Triplet(JoinPath.Submission.SubmissionCompanyNumberCode.Columns.CompanyNumberCodeId, (cctrl as DropDownList).SelectedValue.ToInt(), OrmLib.MatchType.Exact));
                }
                else if (cctrl.HasControls())
                    GetCompanyCodeFilters(cctrl, filters);
            }
        }
    }

    private void GetAttributeFilters(Control ctrl, Dictionary<int, string> attributeFilters)
    {
        if (ctrl != null && ctrl.Controls != null & ctrl.Controls.Count > 0)
        {
            foreach (Control cctrl in ctrl.Controls)
            {
                if (cctrl is TextBox)
                {
                    var textBox = cctrl as TextBox;
                    if (!String.IsNullOrEmpty(textBox.Text))
                        attributeFilters.Add(int.Parse(cctrl.ID.Substring(0, cctrl.ID.Length - 1)), textBox.Text.Trim());
                }
                else if (cctrl is RadioButtonList)
                {
                    var radioButtonList = cctrl as RadioButtonList;
                    if (!String.IsNullOrEmpty(radioButtonList.SelectedValue))
                        attributeFilters.Add(int.Parse(cctrl.ID.Substring(0, cctrl.ID.Length - 1)), radioButtonList.SelectedValue);
                }
                else if (cctrl.HasControls())
                    GetAttributeFilters(cctrl, attributeFilters);
            }
        }
    }

    private void GetCompanyCodeFiltersEF(Control ctrl, ref List<BCS.DAL.EFilter> filters)
    {
        if (ctrl != null && ctrl.Controls != null & ctrl.Controls.Count > 0)
        {
            foreach (Control cctrl in ctrl.Controls)
            {
                if (cctrl is DropDownList)
                {
                    if ((cctrl as DropDownList).SelectedIndex > 0)
                        filters.Add(new BCS.DAL.EFilter("CompanyNumberCode.Id", (cctrl as DropDownList).SelectedValue.ToInt(), BCS.DAL.MatchType.Exact));
                }
                else if (cctrl.HasControls())
                    GetCompanyCodeFiltersEF(cctrl, ref filters);
            }
        }
    }

    private void GetAttributeValues(Control ctrl, ref List<KeyValuePair<string, string>> items)
    {
        if (ctrl != null && ctrl.Controls != null & ctrl.Controls.Count > 0)
        {
            foreach (Control cctrl in ctrl.Controls)
            {
                if (cctrl is DropDownList)
                {
                    if ((cctrl as DropDownList).SelectedIndex > 0)
                        items.Add(new KeyValuePair<string, string>(cctrl.ID, (cctrl as DropDownList).SelectedValue));
                }
                else if (cctrl is TextBox)
                {
                    var textBox = cctrl as TextBox;
                    if (!String.IsNullOrEmpty(textBox.Text))
                        items.Add(new KeyValuePair<string, string>(cctrl.ID, textBox.Text.Trim()));
                }
                else if (cctrl is RadioButtonList)
                {
                    var radioButtonList = cctrl as RadioButtonList;
                    if (!String.IsNullOrEmpty(radioButtonList.SelectedValue))
                        items.Add(new KeyValuePair<string, string>(cctrl.ID, radioButtonList.SelectedValue));
                }
                else if (cctrl.HasControls())
                    GetAttributeValues(cctrl, ref items);
            }
        }
    }

    private void ResetCompanyAttributes(Control ctrl)
    {
        if (ctrl != null && ctrl.Controls != null & ctrl.Controls.Count > 0)
        {
            foreach (Control cctrl in ctrl.Controls)
            {
                if (cctrl is DropDownList)
                {
                    (cctrl as DropDownList).SelectedIndex = -1;
                }
                else if (cctrl is TextBox)
                {
                    (cctrl as TextBox).Text = "";
                }
                else if (cctrl is RadioButtonList)
                {
                    (cctrl as RadioButtonList).SelectedValue = "";
                }
                else if (cctrl.HasControls())
                    ResetCompanyAttributes(cctrl);
            }
        }
    }

    [System.Web.Services.WebMethod()]
    public static string PopulatePolicySymbolByEffectiveDates(string effectiveDate, string iD)
    {

        string ddlId = iD;
        List<BCS.DAL.CompanyNumberCode> cmpCodes = null;
        StringBuilder sb = new StringBuilder();
        int codeTypeId = Convert.ToInt32(iD.Split('-')[1]);

        if (!(string.IsNullOrEmpty(effectiveDate.Trim())))
        {
            DateTime? fromDate = (DateTime)Convert.ToDateTime(effectiveDate.Split('|')[0]);
            DateTime? toDate = (DateTime)Convert.ToDateTime(effectiveDate.Split('|')[1]);
            using (BCS.DAL.BCSContext bc = BCS.DAL.DataAccess.NewBCSContext())
            {
                cmpCodes = bc.CompanyNumberCode.Where(c => c.CompanyCodeTypeId == codeTypeId && ((c.ExpirationDate >= fromDate && c.ExpirationDate <= toDate) || c.ExpirationDate == null)).ToList();
            }
        }
        else
        {
            using (BCS.DAL.BCSContext bc = BCS.DAL.DataAccess.NewBCSContext())
            {
                cmpCodes = bc.CompanyNumberCode.Where(c => c.CompanyCodeTypeId == codeTypeId).ToList();
            }

        }

        foreach (BCS.DAL.CompanyNumberCode code in cmpCodes)
        {
            sb.Append(code.Code + '|' + (string)code.Id.ToString());
            sb.Append('~');
        }

        return sb.ToString();

    }
    #endregion

    #region modal popup for submission history
    protected void Panel1_Load(object sender, EventArgs e)
    {
        string sid = gvSubmissions.DataKeys[((sender as Control).NamingContainer as GridViewRow).RowIndex].Values["id"].ToString();
        ObjectDataSource1.SelectParameters["submissionId"].DefaultValue = sid;

        lblSubmissionNumber.Text = string.Format("Submission # : {0}-{1}", gvSubmissions.DataKeys[((sender as Control).NamingContainer as GridViewRow).RowIndex].Values["submission_number"],
            gvSubmissions.DataKeys[((sender as Control).NamingContainer as GridViewRow).RowIndex].Values["submission_number_sequence"]);
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView historyGrid = sender as GridView;
        if (historyGrid.SelectedIndex == -1)
            return;
        string result = BCS.Core.Clearance.Submissions.ToggleSubmissionStatusHistoryActive(Convert.ToInt32(historyGrid.SelectedDataKey["Id"]), Common.GetUser());
        Common.SetStatus(HistoryStatusPH, result, true);
        historyGrid.SelectedIndex = -1;
        historyGrid.DataBind();

        gvSubmissions_PageIndexChanging(gvSubmissions, new GridViewPageEventArgs(gvSubmissions.PageIndex));
        //gvSubmissions_PageIndexChanged(gvSubmissions, EventArgs.Empty);

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            SubmissionStatusHistory aHistory = e.Row.DataItem as SubmissionStatusHistory;
            if (!aHistory.Active)
            {
                foreach (TableCell var in e.Row.Cells)
                {
                    var.Style.Add(HtmlTextWriterStyle.TextDecoration, "line-through");
                }
            }

            LinkButton lb = e.Row.FindControl("LinkButton1") as LinkButton;
            ConfirmButtonExtender confirmButtonExtender = e.Row.FindControl("ConfirmButtonExtender1") as ConfirmButtonExtender;
            if (lb != null)
            {
                lb.Text = aHistory.Active ? "active" : "inactive";
                lb.Enabled = BCS.WebApp.Components.Security.CanActivateStatus() && (aHistory.Active.Value || User.IsInRole("System Administrator"));
            }
            if (confirmButtonExtender != null)
            {
                confirmButtonExtender.Enabled = BCS.WebApp.Components.Security.CanActivateStatus() && (aHistory.Active.Value || User.IsInRole("System Administrator"));
                if (User.IsInRole("System Administrator"))
                {
                    confirmButtonExtender.ConfirmText = "Are you sure you want to change this status history?";
                }
            }
        }
    }

    protected void lbtnExportToExcel_Click(object sender, EventArgs e)
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.AddHeader("content-disposition",
            string.Format("attachment;filename={0}.xls", "Submissions"));
        HttpContext.Current.Response.Charset = "";
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);

        DataTable dt = ViewState["SearchResults"] as DataTable;
        BCS.Biz.SubmissionDisplayGridCollection dfields = ConfigValues.GetSubmissionGridDisplaySC(Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
        //dfields = dfields.FilterByRemoteAgencyVisible(false);
        GridView gvToExport = new GridView();

        gvToExport.AutoGenerateColumns = false;
        gvToExport.AllowPaging = false;

        BoundField snf = new BoundField();
        snf.DataField = "submission_status_history";
        snf.HeaderText = "Status History";
        snf.Visible = false;
        gvToExport.Columns.Insert(0, snf);


        BoundField snsf = new BoundField();
        snsf.HeaderText = "Submission Number";
        snsf.DataField = "submission_number";
        snsf.HtmlEncode = false;
        gvToExport.Columns.Insert(1, snsf);
        int j = 2;
        foreach (BCS.Biz.SubmissionDisplayGrid df in dfields)
        {
            TemplateField atf = new TemplateField();
            if (df.FieldName == "client_core_client_id")
                atf.ItemTemplate = new Templates.GenericTemplate(DataControlRowType.DataRow, string.Empty, "clientcore_client_id", df.AttributeDataType.DataTypeName, string.Empty);
            else
                atf.ItemTemplate = new Templates.GenericTemplate(DataControlRowType.DataRow, string.Empty, df.FieldName, df.AttributeDataType.DataTypeName, string.Empty);

            atf.HeaderText = string.IsNullOrEmpty(df.HeaderText) ? df.FieldName : df.HeaderText;
            if (df.FieldName == "submission_status_history" || df.FieldName == "submission_number")
                atf.Visible = false;
            gvToExport.Columns.Insert(j, atf);
            j++;
        }

        gvToExport.DataSource = dt;
        gvToExport.DataBind();

        gvToExport.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();

    }
    #endregion
}
