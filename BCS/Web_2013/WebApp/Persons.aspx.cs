#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.Components;
#endregion

public partial class Persons : System.Web.UI.Page
{

    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected override void OnLoadComplete(EventArgs e)
    {
        if (Session["CompanyChanged"] != null)
        {
            Session.Contents.Remove("CompanyChanged");
            GridView1.SelectedIndex = -1;
            DetailsView1.DataBind();
        }
        base.OnLoadComplete(e);
    } 
    #endregion

    #region Control Events

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Session["CompanyNumberSelected"] == null || Session["CompanyNumberSelected"].ToString() == "0")
        {
            Common.SetStatus(messagePlaceHolder, "Please select a company number before modify.");
            return;
        }
        GridView1.SelectedIndex = -1;
        DetailsView1.ChangeMode(DetailsViewMode.Insert);

    }
    protected void btnFilter_Click(object sender, EventArgs e)
    {
        GridView1.SelectedIndex = -1;
    }
    protected void GridView1_PageIndexChanged(object sender, EventArgs e)
    {
        GridView1.SelectedIndex = -1;
    }
    protected void odsPerson_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void odsPerson_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void odsPersons_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        GridView1.SelectedIndex = -1;
        GridView1.DataBind();
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        GridView1.DataBind();
    }
    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        GridView1.DataBind();
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        bool b = Common.ShouldUpdate(e);
        e.NewValues.Add("CompanyNumberId", Session["CompanyNumberSelected"] != null ? Session["CompanyNumberSelected"] : 0);

        e.Cancel = b;

        if (b)
        {
            // there was nothing to update. Just show the user the success message.
            Common.SetStatus(messagePlaceHolder, "Person Sucessfully updated.");
        }
    }
    protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        e.Values.Add("CompanyNumberId", Session["CompanyNumberSelected"] != null ? Session["CompanyNumberSelected"] : 0);
    }
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        e.Cancel = BCS.WebApp.Components.Security.IsUserReadOnly() || ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        if (e.NewMode == DetailsViewMode.Edit || e.NewMode == DetailsViewMode.Insert)
            if (Session["CompanyNumberSelected"] == null || Session["CompanyNumberSelected"].ToString() == "0")
            {
                e.Cancel = true;
                Common.SetStatus(messagePlaceHolder, "Please select a company number before modify.");
            }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Session["CompanyNumberSelected"] == null || Session["CompanyNumberSelected"].ToString() == "0")
        {
            Common.SetStatus(messagePlaceHolder, "Please select a company number before modify.");
            return;
        }
        if (!BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
            DetailsView1.ChangeMode(DetailsViewMode.Edit);
    }
    protected void DatePickerTextBox_Init(object sender, EventArgs e)
    {
        TextBox tb = sender as TextBox;
        tb.Attributes.Add("onclick", "scwShow(this,this);");
        tb.Attributes.Add("onkeydown", "hideCalOnTab(event);");
        tb.Attributes.Add("onblur", "this.value = purgeDate(this);");
    }
    #endregion
}
