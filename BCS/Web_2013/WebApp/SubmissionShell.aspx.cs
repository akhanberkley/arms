#region Using
using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Policy;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;
using Microsoft.Web.Services2.Security.X509;

using OrmLib;
using BCS.Biz;
using BCS.WebApp.Components;
using BCS.WebApp.UserControls;

using structures = BTS.ClientCore.Wrapper.Structures;
using System.Collections.Generic;
using System.Reflection;
#endregion

public partial class SubmissionShell : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        #region Set Disabled based on company config
        string[] addDisabledFields = ConfigValues.GetAddDisabledFields(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
        foreach (string var in addDisabledFields)
        {
            // such controls seem to be all on pnlAddSubmission, so trying to optimize by sending pnlAddSubmission instead of Page
            // as parameter to FindControlRecursive method
            Control c = Common.FindControlRecursive(Page, var);
            if (c != null)
            {
                if (c is WebControl)
                {
                    WebControl wc = c as WebControl;
                    wc.Enabled = false;
                }
                if (c is UserControl)
                {
                    PropertyInfo pi = c.GetType().GetProperty("Enabled");
                    if (pi != null)
                        pi.SetValue(c, false, null);
                }
            }
        }
        #endregion
        base.OnInit(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        txEffectiveDate.Attributes.Add(
            "onclick",
            "scwShow(this,this); processAfter=new Function(\"__doPostBack(\'_ctl0$CompanySelector1$CompanyDropDownList1\',\'\'); \")");
        txEffectiveDate.Attributes.Add("onkeydown", "hideCalOnTab(event);");
        txEffectiveDate.Attributes.Add("onblur", "this.value = purgeDate(this);"); 
        if (!IsPostBack)
        {
            Common.SetStatus(Master.FindControl("ContentPlaceHolder1"));

            lblClientId.Text = Session["ClientId"].ToString();

            if (Session["CompanySelected"] != null)
            {
                SubmissionTypeDropDownList1.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
                SubmissionTypeDropDownList1.DataBind();

                AgencyDropDownList1.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
                AgencyDropDownList1.DataBind();

                SubmissionStatusDropDownList1.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
                SubmissionStatusDropDownList1.DataBind();
            }

            this.SubmissionTypeDropDownList1.Focus();
        }
    }
    protected override void OnLoadComplete(EventArgs e)
    {
        if (Session["CompanyChanged"] != null)
        {
            Session.Contents.Remove("CompanyChanged");
            Response.Redirect("Default.aspx");
        }
    }
    protected void AgencyDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (AgencyDropDownList1.SelectedIndex > -1)
        {
            BCS.Core.Clearance.BizObjects.Agency ag = BCS.Core.Clearance.Lookups.GetAgency(Convert.ToInt32(AgencyDropDownList1.SelectedValue));
            lblAgencyComment.Text = ag != null ? ag.Comments : string.Empty;
            lblAgencyComment.Visible = !string.IsNullOrEmpty(lblAgencyComment.Text);

            DateTime effDate = txEffectiveDate.Text.Length > 0 ? Convert.ToDateTime(txEffectiveDate.Text) : DateTime.MinValue;
            DateTime refDate = ag != null && ag.ReferralDate == DateTime.MinValue ? ag.ReferralDate : DateTime.MinValue.AddDays(1);
            if (ag != null && !string.IsNullOrEmpty(ag.ReferralAgencyNumber) && refDate <= effDate)
            {
                AgencyDropDownList1.ClearSelection();
                if (null != AgencyDropDownList1.Items.FindByValue(ag.ReferralAgencyId.ToString()))
                {
                    AgencyDropDownList1.Items.FindByValue(ag.ReferralAgencyId.ToString()).Selected = true;
                    AgencyDropDownList1_SelectedIndexChanged(sender, e);
                    if (IsPostBack)
                    {
                        ClientScript.RegisterStartupScript(typeof(string), "referredAgency",
                                   "alert('Displaying referral agency.');", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(typeof(string), "missingReferredAgency",
                               "alert('Unable to display the agency referred by the selected agency. Please contact your system administrator for further information.');", true);
                }
            }

            LineOfBusinessDropDownList1.DataBindByAgency(Convert.ToInt32(AgencyDropDownList1.SelectedValue));
            LineOfBusinessDropDownList1.Focus();
        }
    }
    protected void LineOfBusinessDropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SubmissionStatusDropDownList1.Focus();
    }
    protected void btnSubmitSubmission_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        int companyId = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
        int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumber);
        string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);

        if (companyId == 0 || companyNumberId == 0)
        {
            Common.SetStatus(Master.FindControl("contentplaceholder1"), "Please select a company number.");
            return;
        }
        BCS.Core.Clearance.BizObjects.Submission coresub;

        SubmissionProxy.Submission sp = Common.GetSubmissionProxy();

        string clientid = Session["ClientId"] as string;
        string clientaddressids = "";
        ClientProxy.Client client = new ClientProxy.Client();
        string clientxml = client.GetClientById(companyNumber, clientid);
        BCS.Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                (BCS.Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                clientxml, typeof(BCS.Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");
        if (clearanceclient.ClientCoreClient.Addresses != null)
        {
            List<string> listAddresses = new List<string>();
            foreach (structures.Info.Address addr in clearanceclient.ClientCoreClient.Addresses)
            {
                listAddresses.Add(addr.AddressId.ToString());
            }
            string[] arrAddresses = new string[listAddresses.Count];
            listAddresses.CopyTo(arrAddresses);
            clientaddressids = string.Join("|", arrAddresses);
        }


        
        string clientnameid = "";
        string clientportfolioid = "0";
        string user = Common.GetUser();
        int subtypeid = Convert.ToInt32(SubmissionTypeDropDownList1.SelectedValue);
        int substatusid = Convert.ToInt32(SubmissionStatusDropDownList1.SelectedValue);
        int substatusreasonid = 0;
        int agencyid = Convert.ToInt32(AgencyDropDownList1.SelectedValue);
        int agentid = 0;
        int contactid = 0;
        decimal dpremium = 0;
        string dteff = txEffectiveDate.Text;
        string dtexp = "";
        string dtstatusdate = DateTime.Now.ToString("MM/dd/yyyy");
        int underwriterid = 0;
        int analystid = 0;
        int technicianid = 0;
        int policysystemid = 0;
        string comments = string.Empty;
        string policynumber = "";
        int lineofbusinessid = Convert.ToInt32(LineOfBusinessDropDownList1.SelectedValue);
        string addinsuredname = "";
        string dba = "";
        string otherreason = "";
        int othercarrierid = 0;
        decimal ocpremium = 0;
        string agentunspecifiedreasons = string.Empty;
        string submissionNo = string.Empty;

        string addXML = sp.AddSubmissionByIds(clientid,
            string.Empty, // stop gap for other apps
            clientaddressids, clientnameid, clientportfolioid,
            user, // stop gap for other apps
            companyId, // stop gap for other apps
            user, string.Empty, 
            subtypeid, agencyid, agentid, agentunspecifiedreasons,
            contactid, companyNumber, substatusid, dtstatusdate,
            substatusreasonid, otherreason, policynumber, 
            "", //codeids
            "", //attributes
            dpremium, dteff, dtexp, 
            underwriterid, analystid, technicianid, policysystemid, comments,
            lineofbusinessid,
            "", // lobchildren
            addinsuredname, dba, submissionNo, othercarrierid, ocpremium);

        try
        {
            coresub = (BCS.Core.Clearance.BizObjects.Submission)BCS.Core.XML.Deserializer.Deserialize(
                addXML, typeof(BCS.Core.Clearance.BizObjects.Submission));

            if (coresub != null)
                if (coresub.Message != null)
                {
                    btnSubmitSubmission.Text = "Add Another Submission (Shell)?";

                    Common.SetStatus(Master.FindControl("contentplaceholder1"),
                        string.Format("Shell {0}, Shell Submission Number : {1}.", coresub.Message, coresub.SubmissionNumber));


                    Common.SetStatus(Master.FindControl("contentplaceholder1"), "Please load submission and complete all required data fields.");

                    Cache.Remove("SearchedCacheDependency");
                }
        }
        catch (Exception ex)
        {
            Common.SetStatus(Master.FindControl("contentplaceholder1"),
                "Submission Add not successful.\n<!--"+ex.Message+"-->");
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session["LoadSavedSearch"] = true;
        Response.Redirect("Default.aspx");
    }
    protected void LineOfBusinessDropDownList1_PreRender(object sender, EventArgs e)
    {

    }
    protected void txEffectiveDate_TextChanged(object sender, EventArgs e)
    {
        int selAgency = Common.GetSafeSelectedValue(AgencyDropDownList1);

        DateTime eff = DateTime.MinValue;
        try
        {
            eff = Convert.ToDateTime(txEffectiveDate.Text);
        }
        catch (FormatException)
        {
            CompareValidator1.IsValid = false;
            CompareValidator1.Focus();
            return;
        }

        AgencyDropDownList1.CompanyNumberId = Common.GetSafeIntFromSession("CompanyNumberSelected");
        AgencyDropDownList1.CancelDate = txEffectiveDate.Text.Length > 0 ? eff : DateTime.MaxValue.Subtract(TimeSpan.FromSeconds(1));
        AgencyDropDownList1.DataBind();

        if (AgencyDropDownList1.Items.FindByValue(selAgency.ToString()) != null)
        {
            AgencyDropDownList1.ClearSelection();
            AgencyDropDownList1.Items.FindByValue(selAgency.ToString()).Selected = true;
            AgencyDropDownList1_SelectedIndexChanged(AgencyDropDownList1, EventArgs.Empty);
        }
        else
        {
            ClientScript.RegisterStartupScript(typeof(string), "obsoleteAgency1",
                "alert('Agency previously selected, is no longer available based on the effective date entered. Please select another.');", true);
            BCS.WebApp.UserControls.Helper.ClearDBItems(LineOfBusinessDropDownList1);            
        }
        txEffectiveDate.Focus();
    }
    protected void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        throw new ApplicationException("Ajax Error", e.Exception);
    }
}
