<%@ Page language="c#" Inherits="BCS.WebApp.Calendar" CodeFile="Calendar.aspx.cs" Theme="DefaultTheme" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Select Date</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<style type="text/css">
		.headerGrad { FILTER: progid:dximagetransform.microsoft.gradient (gradienttype=0,startcolorstr= '#ffffffff' ,endcolorstr= '#ffcccccc' ) }
		.headerGradGrey { FILTER: progid:dximagetransform.microsoft.gradient (gradienttype=0,startcolorstr= '#ffffffff' ,endcolorstr= '#ffdcdcdc' ) }
		</style>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:Calendar id="Calendar1" runat="server" BackColor="White" Width="200px" DayNameFormat="FirstLetter"
				ForeColor="Black" Height="180px" Font-Size="8pt" Font-Names="Verdana" BorderColor="#999999"
				CellPadding="4" onselectionchanged="Calendar1_SelectionChanged">
				<TodayDayStyle ForeColor="Black" BackColor="#CCCCCC"></TodayDayStyle>
				<SelectorStyle BackColor="#CCCCCC"></SelectorStyle>
				<NextPrevStyle VerticalAlign="Bottom"></NextPrevStyle>
				<DayHeaderStyle Font-Size="7pt" Font-Bold="True" CssClass="headerGradGrey"></DayHeaderStyle>
				<SelectedDayStyle Font-Bold="True" ForeColor="White" BackColor="#666666"></SelectedDayStyle>
				<TitleStyle Font-Bold="True" BorderColor="Black" CssClass="headerGrad"></TitleStyle>
				<WeekendDayStyle BackColor="Gray"></WeekendDayStyle>
				<OtherMonthDayStyle ForeColor="Gray"></OtherMonthDayStyle>
			</asp:Calendar>
			<input type="hidden" runat="server" id="control" NAME="control">
		</form>
	</body>
</HTML>
