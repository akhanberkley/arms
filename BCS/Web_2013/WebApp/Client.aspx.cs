#region Using
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Policy;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;
using Microsoft.Web.Services2.Security.X509;

using OrmLib;
using BCS.Biz;
using BCS.WebApp.Components;
using BCS.WebApp.UserControls;

using structures = BTS.ClientCore.Wrapper.Structures;
using System.Collections.Generic;
using BCS.Core.Clearance.Admin;
using AjaxControlToolkit;
using System.Linq;

#endregion

namespace BCS.WebApp
{
    /// <summary>
    /// Summary description for Clients.
    /// </summary>
    public partial class Client : System.Web.UI.Page
    {
        private string _operation = "";

        private string OldCodeValues
        {
            set
            {
                ViewState["OldCodeValues"] = value;
            }
            get
            {
                return ViewState["OldCodeValues"] == null ? string.Empty : (string)ViewState["OldCodeValues"];
            }
        }

        public bool IsDeleted
        {
            get
            {
                bool deleted = false;
                if (Session["clientdeleted"] != null)
                    bool.TryParse(Session["clientdeleted"].ToString(), out deleted);
                return deleted;
            }
            set
            {
                Session["clientdeleted"] = value;
            }
        }

        public bool AllowPrimaryNameChange
        {
            get
            {
                var value = ViewState["AllowPrimaryNameChange"];
                return (bool)(value ?? true);
            }
            set
            {
                ViewState["AllowPrimaryNameChange"] = value;
            }
        }

        #region Page Events
        protected void Page_Load(object sender, System.EventArgs e)
        {
            copy.Attributes.Add("onclick", string.Format("javascript:copyErrorToClipboard(document.getElementById('{0}').value);", txtPort.ClientID));

            if (!string.IsNullOrEmpty(activeTab.Value))
                TabContainer1.ActiveTabIndex = int.Parse(activeTab.Value);

            if (Request.QueryString.Count > 0)
            {
                if (Request.QueryString["op"] != null)
                {
                    this._operation = Request.QueryString["op"].ToString();
                    lblOperation.Text =
                        System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this._operation);
                }
                if (Request.QueryString["clientid"] != null && Request.QueryString["op"] == null && Request.QueryString["companynumber"] != null)
                {
                    this._operation = "edit";
                }
            }
            else
            {
                throw new ApplicationException("The page cannot be directly accessed.");
            }

            if (!IsPostBack)
            {
                AllowPrimaryNameChange = BTS.WFA.BCS.Services.LegacyService.AllowPrimaryNameChange(Common.GetSafeIntFromSession(SessionKeys.CompanyId));

                if (Request.QueryString["clientid"] != null && Request.QueryString["op"] == null && Request.QueryString["companynumber"] != null)
                {
                    Session["ClientId"] = Request.QueryString["clientid"];
                    string clientid = Request.QueryString["clientid"];
                    string companyNumber = Request.QueryString["companynumber"];
                    Session["CompanyNumber"] = companyNumber;
                    int cmpNumberid = BCS.DAL.CompanyNumber.GetCompanyNumberId(companyNumber);
                    // Session["CompanyNumberSelected"] = cmpNumberid;
                    BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
                    dm.QueryCriteria.And(JoinPath.Company.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                    Company cmp = dm.GetCompany();
                    CompanyDropDownList ct = Master.FindControl("CompanySelector1").FindControl("CompanyDropDownList1") as CompanyDropDownList;
                    CompanyNumberDropDownList cnt = Master.FindControl("CompanySelector1").FindControl("CompanyNumberDropDownList1") as CompanyNumberDropDownList;
                    BCS.Core.Security.CustomPrincipal m_customPrincipal = HttpContext.Current.User as BCS.Core.Security.CustomPrincipal;
                    System.Web.UI.UserControl CmpSelector = Master.FindControl("CompanySelector1") as System.Web.UI.UserControl;
                    Common.AddCookie("CompanySelected", cmp.Id.ToString());
                    Session[SessionKeys.CompanyId] = cmp.Id;
                    ct.DataBind(m_customPrincipal.Identity.Name);
                    this._operation = "edit";
                    Session["cliref"] = "Default.aspx?clientid=" + clientid;
                }
            }
            CheckForDeletedClient();

            txtPort.Enabled = (User as BCS.Core.Security.CustomPrincipal).IsInAnyRoles("Portfolio Administrator", "System Administrator");
            // Based on the operation
            switch (this._operation.ToLower())
            {
                case "add":
                    this.rowPortfolio.Visible = false;
                    if (!IsPostBack)
                    {
                        // Add initial.
                        AddClientNameWebcontrol(phNames);
                        AddClientAddressWebcontrol(phAddresses);
                        GridView1.DataSourceID = "";

                        var savedSearch = Session["SavedSearch"];
                        if (savedSearch != null)
                        {
                            var clientSearch = ((SavedSearch)savedSearch).SearchedClient;
                            var uiClient = (UserControls_ClientNameSelector)phNames.Controls[0];
                            var uiAddress = (UserControls_ClientAddress)phAddresses.Controls[0];
                            if (clientSearch.Names.Length > 0)
                            {
                                uiClient.BusinessNameValue = clientSearch.Names[0].ClientName.NameBusiness;
                                uiAddress.CityValue = clientSearch.Names[0].City;
                                uiAddress.StateValue = clientSearch.Names[0].State;
                            }
                            if (clientSearch.Addresses.Length > 0)
                            {
                                uiAddress.HouseValue = clientSearch.Addresses[0].HouseNumber;
                                uiAddress.StreetnameValue = clientSearch.Addresses[0].Address1;
                                uiAddress.CityValue = clientSearch.Addresses[0].City;
                                uiAddress.StateValue = clientSearch.Addresses[0].StateProvCode;
                                uiAddress.ZipCodeValue = clientSearch.Addresses[0].PostalCode;
                            }
                        }
                    }
                    break;
                case "edit":
                    this.btnSaveNew.Text = "Update Client";
                    if (!IsPostBack)
                        LoadEdit();
                    break;
                case "addphone":
                    this.rowPortfolio.Visible = false;
                    if (!IsPostBack)
                    {
                        // Add initial.
                        AddClientNameWebcontrol(phNames);
                        AddClientAddressWebcontrol(phAddresses);

                        AdvClientSearch.AdvClientSearchRslt_Type advClientSrchResult = (AdvClientSearch.AdvClientSearchRslt_Type)Session["advClientSearch"];
                        if (advClientSrchResult != null)
                            addAdvClientSearchDetails(advClientSrchResult);
                        GridView1.DataSourceID = "";
                    }
                    this._operation = "add";
                    break;
                default:
                    {
                        throw new System.NotSupportedException("Invalid operation requested.");
                        //break;
                    }
            }
            Common.SetStatus(messagePlaceHolder);
        }

        protected override void OnInit(EventArgs e)
        {
            #region Code to dynamically generate and populate the Attributes
            int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
            LoadCodeTypes(companyNumberId);
            LoadAttributes(companyNumberId);
            #endregion
            base.OnInit(e);
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            if (Session["CompanyChanged"] != null)
            {
                Session.Contents.Remove("CompanyChanged");
                Session.Contents.Remove("LoadSavedSearch");
                Session.Contents.Remove("Clients");
                Session.Contents.Remove("ClientId");
                Response.Redirect("Default.aspx");
            }
            base.OnLoadComplete(e);
        }
        protected override bool OnBubbleEvent(object source, EventArgs args)
        {
            if (source is ImageButton)
            {
                if ((source as ImageButton).Parent is UserControls_ClientAddress)
                {
                    Session["LoadSavedSearch"] = true;
                    SaveAddress((source as ImageButton).Parent);
                }
                if ((source as ImageButton).Parent is UserControls_ClientNameSelector)
                {
                    Session["LoadSavedSearch"] = true;
                    SaveName((source as ImageButton).Parent);
                }
            }
            return base.OnBubbleEvent(source, args);
        }


        #endregion

        #region Control Events

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Session["LoadSavedSearch"] = true;
            if (Session["cliref"] != null)
            {
                string ss = Session["cliref"].ToString();
                Session.Contents.Remove("cliref");
                Response.Redirect(ss);
            }
            Response.Redirect("Default.aspx");
        }

        protected void txtSICLookup_Init(object sender, EventArgs e)
        {
            txtSICLookup.OnClientClick = string.Format("openWindow('SICCodeSelector.aspx?control1={0}&control2={1}&control3={2}&control4={3}&control5={4}&control6={5}', 'siccodeselector', 700, 300); return false;", txSICCode.ClientID, hdnSICCode.ClientID, litSICCodeDesc.ClientID, txtNAICSCode.ClientID, hdnNAICSCode.ClientID, litNAICSCodeDesc.ClientID);
        }

        protected void btnAddAddress_Click(object sender, EventArgs e)
        {
            AddClientAddressWebcontrol(phAddresses);
        }
        protected void btnAddName_Click(object sender, EventArgs e)
        {
            AddClientNameWebcontrol(phNames);
        }
        protected void btnSaveNew_Click(object sender, EventArgs e)
        {
            GridView1_RowCommand(GridView1, new GridViewCommandEventArgs(null, new CommandEventArgs("New", null)));
            GridView1_RowCommand(GridView1, new GridViewCommandEventArgs(null, new CommandEventArgs("Update", null)));

            Session["LoadSavedSearch"] = true;

            CompanyDropDownList ct = Master.FindControl("CompanySelector1").FindControl("CompanyDropDownList1") as CompanyDropDownList;
            CompanyNumberDropDownList cnt = Master.FindControl("CompanySelector1").FindControl("CompanyNumberDropDownList1") as CompanyNumberDropDownList;
            int companyid = Convert.ToInt32(ct.SelectedValue);
            if (cnt.SelectedItem == null || cnt.SelectedItem.Text.Length == 0)
            {
                Common.SetStatus(messagePlaceHolder, "Please select a company number.");
                return;

            }
            Button button = sender as Button;
            if (button.Text == "Update Client")
            {
                string effectivedate = DateTime.Now.ToString();
                #region Collect our addresses and build ArrayList
                ArrayList addressVersionsToAdd = new ArrayList();
                StringBuilder addresses = new StringBuilder();

                foreach (Control control in phAddresses.Controls)
                {
                    if (control is UserControls_ClientAddress)
                    {
                        string houseValue = ((UserControls_ClientAddress)control).HouseValue;
                        string streetnameValue = ((UserControls_ClientAddress)control).StreetnameValue;
                        string address2Value = ((UserControls_ClientAddress)control).Address2Value;
                        string cityValue = ((UserControls_ClientAddress)control).CityValue;
                        string stateValue = ((UserControls_ClientAddress)control).StateValue;
                        string countryValue = ((UserControls_ClientAddress)control).CountryValue;
                        string countyValue = ((UserControls_ClientAddress)control).CountyValue;
                        string zipCodeValue = ((UserControls_ClientAddress)control).ZipCodeValue;
                        string addressIdValue = ((UserControls_ClientAddress)control).AddressIdValue;
                        string clearanceAddressTypeValue = ((UserControls_ClientAddress)control).ClearanceAddressTypeValue;

                        BTS.ClientCore.Wrapper.Structures.Import.AddressInfo av =
                            new BTS.ClientCore.Wrapper.Structures.Import.AddressInfo();

                        av.HouseNumber = houseValue;
                        av.ADDR1 = streetnameValue;
                        av.ADDR2 = address2Value;
                        av.CityName = cityValue;
                        av.StateCode = stateValue;
                        av.Zip = zipCodeValue;
                        av.CCAddressId = addressIdValue;
                        av.EffectiveDate = effectivedate;
                        av.ClearanceAddressType = clearanceAddressTypeValue;

                        // TODO :: duplicate address option
                        av.DuplicateAddressOption = "1";

                        av.County = countyValue;
                        av.Country = countryValue;

                        av.Primary = ((UserControls_ClientAddress)control).PrimaryAddress ? "Y" : "N";

                        if (av.HouseNumber.Length == 0 && av.ADDR1.Length == 0 && av.CityName.Length == 0
                           && av.StateCode.Length == 0 && av.Zip.Length == 0)
                        { continue; }
                        else
                            addressVersionsToAdd.Add(av);

                        string addr = BCS.Core.XML.Serializer.SerializeAsXml(av);
                        addresses.AppendFormat("{0}+-", addr);
                    }
                }
                if (addresses.Length > 0)
                    addresses.Remove(addresses.Length - 2, 2);
                #endregion
                #region Collect our names and build ArrayList
                ArrayList clientNamesToAdd = new ArrayList();
                StringBuilder names = new StringBuilder();
                int businessNamesCount = 0;
                int indNamesCount = 0;

                foreach (Control control in phNames.Controls)
                {
                    //if (control.GetType().Name.ToLower() == "clientnameselector_ascx")
                    if (control is UserControls_ClientNameSelector)
                    {
                        BTS.ClientCore.Wrapper.Structures.Import.NameInfo cn =
                            new BTS.ClientCore.Wrapper.Structures.Import.NameInfo();

                        string seqNum = ((UserControls_ClientNameSelector)control).NameSequenceOrID;
                        string nameType = ((UserControls_ClientNameSelector)control).NameTypeValue;
                        string clearanceNameType = ((UserControls_ClientNameSelector)control).ClearanceNameTypeValue;

                        if (((UserControls_ClientNameSelector)control).IsBusiness)
                        {
                            businessNamesCount++;
                            string businessNameValue = ((UserControls_ClientNameSelector)control).BusinessNameValue;
                            cn.BusinessName = businessNameValue;
                        }

                        if (((UserControls_ClientNameSelector)control).IsPersonal)
                        {
                            indNamesCount++;
                            string namePrefixValue = ((UserControls_ClientNameSelector)control).NamePrefixValue;
                            string firstNameValue = ((UserControls_ClientNameSelector)control).FirstNameValue;
                            string middleNameValue = ((UserControls_ClientNameSelector)control).MiddleNameValue;
                            string lastNameValue = ((UserControls_ClientNameSelector)control).LastNameValue;
                            string nameSuffixValue = ((UserControls_ClientNameSelector)control).NameSuffixValue;

                            cn.NamePrefix = namePrefixValue;
                            cn.FirstName = firstNameValue;
                            cn.MiddleName = middleNameValue;
                            cn.LastName = lastNameValue;
                            cn.NameSuffix = nameSuffixValue;
                        }
                        cn.NameSeqNum = seqNum;
                        cn.NameType = nameType;
                        cn.ClearanceNameType = clearanceNameType;
                        cn.DisplayFlag = ((UserControls_ClientNameSelector)control).PrimaryName ? "Y" : "N";


                        // Add to the object Array:
                        if (cn.LastName == null && cn.FirstName == null && cn.BusinessName == null)
                        { continue; }
                        else
                            clientNamesToAdd.Add(cn);
                        string name = BCS.Core.XML.Serializer.SerializeAsXml(cn);
                        names.AppendFormat("{0}+-", name);
                    }
                }

                if (names.Length > 0)
                    names.Remove(names.Length - 2, 2);
                if (indNamesCount > 0 && businessNamesCount > 0)
                {
                    Common.SetStatus(messagePlaceHolder, "There cannot be both business names and individual names.");
                }
                #endregion

                if (clientNamesToAdd.Count == 0 || addressVersionsToAdd.Count == 0)
                {
                    Common.SetStatus(messagePlaceHolder, "Please enter at least one name and address.", true);
                    return;
                }
                bool phvalidation = isPhoneRequired();
                if (phvalidation)
                {
                    if (String.IsNullOrEmpty(txtBusinessPhone.Text))
                    {
                        Common.SetStatus(messagePlaceHolder, "Please enter a Business Phone Number", true);
                        return;
                    }
                }
                Save(names.ToString(), addresses.ToString(),
                    string.Empty // since we are using grid and saving on itself, it renders this param obsolete
                    , true);
                return;
            }

            Add();
        }

        protected void ClassCodeTab_Init(object sender, EventArgs e)
        {
            if (!DefaultValues.SupportsClassCodes(Common.GetSafeIntFromSession("CompanySelected")))
            {
                (sender as AjaxControlToolkit.TabPanel).Visible = false;

                //this is needed, Bug in tab panel
                (sender as AjaxControlToolkit.TabPanel).HeaderText = string.Empty;
            }
        }

        protected void txSICCode_Init(object sender, EventArgs e)
        {
            // need to set autocomplete explicitly since the ajax script does not work for FF. (AutoCompleteBehavior.js -> element.autcomplete = "off")
            // element.setAttribute("autocomplete","off",0); seems to work. but can workaround as below.
            //txSICCode.AutoCompleteType = AutoCompleteType.Disabled; //does not work for firefox, so set the attribute.
            txSICCode.Attributes.Add(" autocomplete", "off");

            txSICCode.Attributes.Add("onblur", "sicCodeSelectionIgnored(this);");
            txSICCode.Attributes.Add("onfocus", "sicCodeSelectionInit(this);");
        }

        protected void txtNAICSCode_Init(object sender, EventArgs e)
        {
            // need to set autocomplete explicitly since the ajax script does not work for FF. (AutoCompleteBehavior.js -> element.autcomplete = "off")
            // element.setAttribute("autocomplete","off",0); seems to work. but can workaround as below.
            //txSICCode.AutoCompleteType = AutoCompleteType.Disabled; //does not work for firefox, so set the attribute.
            txtNAICSCode.Attributes.Add(" autocomplete", "off");

            txtNAICSCode.Attributes.Add("onblur", "naicsCodeSelectionIgnored(this);");
            txtNAICSCode.Attributes.Add("onfocus", "naicsCodeSelectionInit(this);");
        }
        #endregion

        #region Other Methods


        private void LoadEdit()
        {
            string id = Session["ClientId"] as string;
            string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
            int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);

            ClientProxy.Client clientproxy = Common.GetClientProxy();
            string clientxml = clientproxy.GetClientById(companyNumber, id);

            Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                (Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                clientxml, typeof(Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

            structures.Info.ClientInfo client = clearanceclient.ClientCoreClient;


            Session["Client"] = client;

            phid.Visible = true;
            lblClientId.Text = client.Client.ClientId;

            hdnSICCode.Value = clearanceclient.SICCodeId.ToString();
            txSICCode.Text = clearanceclient.SICCode;
            litSICCodeDesc.Value = clearanceclient.SICCodeDescription;
            txtBusinessDescription.Text = clearanceclient.BusinessDescription;
            txtBusinessPhone.Text = clearanceclient.PhoneNumber;
            txtFEIN.Text = client.Client.TaxId;

            hdnNAICSCode.Value = clearanceclient.NAICSCodeId.ToString();
            txtNAICSCode.Text = clearanceclient.NAICSCode;
            litNAICSCodeDesc.Value = clearanceclient.NAICSCodeDescription;

            rblTaxType.ClearSelection();
            if (string.IsNullOrEmpty(client.Client.TaxIdType))
            {
                // default to FEIN
                rblTaxType.Items.FindByText("FEIN").Selected = true;
            }
            else
            {
                if (rblTaxType.Items.FindByText(client.Client.TaxIdType) != null)
                    rblTaxType.Items.FindByText(client.Client.TaxIdType).Selected = true;
                else
                {
                    // default to FEIN
                    rblTaxType.Items.FindByText("FEIN").Selected = true;
                }
            }
            txtPort.Text = client.Client.PortfolioId;
            string oldCodes = string.Empty;
            if (clearanceclient.CompanyNumberCodes != null)
            {
                foreach (BCS.Core.Clearance.BizObjects.ClientCompanyNumberCode i in clearanceclient.CompanyNumberCodes)
                {
                    oldCodes += i.CompanyNumberCodeId.ToString() + "|";
                    if (phCodeTypes.Controls.Count > 0)
                    {
                        HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                        foreach (HtmlTableRow row in table.Rows)
                        {
                            foreach (HtmlTableCell cell in row.Cells)
                            {
                                foreach (Control c in cell.Controls)
                                {
                                    if (c is DropDownList)
                                    {
                                        DropDownList ddl = c as DropDownList;
                                        ListItem li = ddl.Items.FindByValue(i.CompanyNumberCodeId.ToString());
                                        if (li != null)
                                        {
                                            ddl.ClearSelection();
                                            li.Selected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // store them, so that they can be restored while saving a single name or address 
            OldCodeValues = oldCodes;
            if (clearanceclient.CompanyNumberAttributes != null)
            {
                foreach (BCS.Core.Clearance.BizObjects.ClientCompanyNumberAttributeValue sa in clearanceclient.CompanyNumberAttributes)
                {
                    if (phAttributes.Controls.Count > 0)
                    {
                        HtmlTable table = phAttributes.Controls[0] as HtmlTable;
                        foreach (HtmlTableRow row in table.Rows)
                        {
                            foreach (HtmlTableCell cell in row.Cells)
                            {
                                foreach (Control c in cell.Controls)
                                {
                                    if (c is WebControl)
                                    {
                                        System.ComponentModel.AttributeCollection attributes = System.ComponentModel.TypeDescriptor.GetAttributes(c);
                                        ControlValuePropertyAttribute attribute = (ControlValuePropertyAttribute)attributes[typeof(ControlValuePropertyAttribute)];

                                        if (c.ID == sa.AttributeId.ToString())
                                            c.GetType().GetProperty(attribute.Name).SetValue(c, Convert.ChangeType(sa.AttributeValue, c.GetType().GetProperty(attribute.Name).PropertyType), null);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // display names
            AddClientNameWebcontrol(phNames, client.Client.Names);

            // display addresses
            AddClientAddressWebcontrol(phAddresses, client.Addresses);

            sourceClientIdLabel.Text = client.Client.ClientId;
            // load names for merge feature
            if (client.Client.Names != null)
            {
                foreach (structures.Info.Name name in client.Client.Names)
                {
                    StringBuilder sb = new StringBuilder();
                    if (name.NameBusiness != null && name.NameBusiness.Length > 0)
                        sb.Append(name.NameBusiness);
                    else
                        sb.AppendFormat("{0} {1} {2}", name.NameFirst, name.NameMiddle, name.NameLast);

                    cblNames.Items.Add(new ListItem(Server.HtmlEncode(sb.ToString()), name.SequenceNumber));
                }
            }

            // load addresses for merge feature
            if (client.Addresses != null)
            {
                foreach (structures.Info.Address addr in client.Addresses)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat("{0} {1} {2} {3} {4} {5}", addr.HouseNumber, addr.Address1, addr.Address2,
                        addr.City, addr.StateProvinceId, addr.PostalCode);

                    cblAddresses.Items.Add(new ListItem(Server.HtmlEncode(sb.ToString()), addr.AddressId));
                }
            }
            cvSameId.ValueToCompare = client.Client.ClientId;
        }

        private void SaveAddress(Control control)
        {
            string effectivedate = DateTime.Now.ToString();
            #region Collect our addresses and build ArrayList
            ArrayList addressVersionsToAdd = new ArrayList();
            StringBuilder addresses = new StringBuilder();

            if (control is UserControls_ClientAddress)
            {
                string houseValue = ((UserControls_ClientAddress)control).HouseValue;
                string streetnameValue = ((UserControls_ClientAddress)control).StreetnameValue;
                string address2Value = ((UserControls_ClientAddress)control).Address2Value;
                string cityValue = ((UserControls_ClientAddress)control).CityValue;
                string stateValue = ((UserControls_ClientAddress)control).StateValue;
                string countryValue = ((UserControls_ClientAddress)control).CountryValue;
                string countyValue = ((UserControls_ClientAddress)control).CountyValue;
                string zipCodeValue = ((UserControls_ClientAddress)control).ZipCodeValue;
                string addressIdValue = ((UserControls_ClientAddress)control).AddressIdValue;
                string clearanceAddressTypeValue = ((UserControls_ClientAddress)control).ClearanceAddressTypeValue;

                BTS.ClientCore.Wrapper.Structures.Import.AddressInfo av =
                    new BTS.ClientCore.Wrapper.Structures.Import.AddressInfo();

                av.HouseNumber = houseValue;
                av.ADDR1 = streetnameValue;
                av.ADDR2 = address2Value;
                av.CityName = cityValue;
                av.StateCode = stateValue;
                av.Zip = zipCodeValue;
                av.CCAddressId = addressIdValue;
                av.EffectiveDate = effectivedate;
                av.ClearanceAddressType = clearanceAddressTypeValue;

                // TODO :: duplicate address option
                av.DuplicateAddressOption = "1";

                av.County = countyValue;
                av.Country = countryValue;

                av.Primary = ((UserControls_ClientAddress)control).PrimaryAddress ? "Y" : "N";

                if (av.HouseNumber.Length == 0 && av.ADDR1.Length == 0 && av.CityName.Length == 0
                   && av.StateCode.Length == 0 && av.Zip.Length == 0)
                {
                    Common.SetStatus(messagePlaceHolder, "Please enter the address before save.", true);
                    return;
                }
                else
                    addressVersionsToAdd.Add(av);

                string addr = BCS.Core.XML.Serializer.SerializeAsXml(av);
                addresses.AppendFormat("{0}+-", addr);
            }

            if (addresses.Length > 0)
                addresses.Remove(addresses.Length - 2, 2);

            #endregion

            Save(string.Empty, addresses.ToString(), string.Empty, false);
        }
        private void SaveName(Control control)
        {
            string effectivedate = DateTime.Now.ToString();
            #region Collect our names and build ArrayList. corrected display flag
            ArrayList clientNamesToAdd = new ArrayList();
            StringBuilder names = new StringBuilder();
            int businessNamesCount = 0;
            int indNamesCount = 0;
            if (control is UserControls_ClientNameSelector)
            {
                BTS.ClientCore.Wrapper.Structures.Import.NameInfo cn =
                    new BTS.ClientCore.Wrapper.Structures.Import.NameInfo();

                string seqNum = ((UserControls_ClientNameSelector)control).NameSequenceOrID;
                string nameType = ((UserControls_ClientNameSelector)control).NameTypeValue;
                string clearanceNameType = ((UserControls_ClientNameSelector)control).ClearanceNameTypeValue;

                if (((UserControls_ClientNameSelector)control).IsBusiness)
                {
                    businessNamesCount++;
                    string businessNameValue = ((UserControls_ClientNameSelector)control).BusinessNameValue;
                    cn.BusinessName = businessNameValue;
                }

                if (((UserControls_ClientNameSelector)control).IsPersonal)
                {
                    indNamesCount++;
                    string namePrefixValue = ((UserControls_ClientNameSelector)control).NamePrefixValue;
                    string firstNameValue = ((UserControls_ClientNameSelector)control).FirstNameValue;
                    string middleNameValue = ((UserControls_ClientNameSelector)control).MiddleNameValue;
                    string lastNameValue = ((UserControls_ClientNameSelector)control).LastNameValue;
                    string nameSuffixValue = ((UserControls_ClientNameSelector)control).NameSuffixValue;

                    cn.NamePrefix = namePrefixValue;
                    cn.FirstName = firstNameValue;
                    cn.MiddleName = middleNameValue;
                    cn.LastName = lastNameValue;
                    cn.NameSuffix = nameSuffixValue;
                }
                cn.NameSeqNum = seqNum;
                cn.NameType = nameType;
                cn.ClearanceNameType = clearanceNameType;
                cn.DisplayFlag = ((UserControls_ClientNameSelector)control).PrimaryName ? "Y" : "N";


                // Add to the object Array:
                if (cn.LastName == null && cn.FirstName == null && cn.BusinessName == null)
                {
                    Common.SetStatus(messagePlaceHolder, "Please enter a name before save.", true);
                    return;
                }
                else
                    clientNamesToAdd.Add(cn);
                string name = BCS.Core.XML.Serializer.SerializeAsXml(cn);
                names.AppendFormat("{0}+-", name);
            }
            if (names.Length > 0)
                names.Remove(names.Length - 2, 2);
            if (indNamesCount > 0 && businessNamesCount > 0)
            {
                Common.SetStatus(messagePlaceHolder, "There cannot be both business names and individual names.");
            }
            #endregion

            Save(names.ToString(), string.Empty, string.Empty, false);
        }
        private void Save(string names, string addresses, string classCodes, bool otherInfo)
        {
            structures.Info.ClientInfo sclient = (structures.Info.ClientInfo)Session["Client"];
            string effectivedate = DateTime.Now.ToString();
            string businessPhone = txtBusinessPhone.Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            structures.Import.ClientImportData iclient = new BTS.ClientCore.Wrapper.Structures.Import.ClientImportData();
            iclient.Client = new BTS.ClientCore.Wrapper.Structures.Import.ImportClient();
            iclient.Client.CCClientId = sclient.Client.ClientId;
            iclient.Client.EffectiveDate = effectivedate;
            iclient.Client.ClientCategory = sclient.Client.ClientCategory;
            iclient.Client.ClientType = sclient.Client.ClientType;
            iclient.Client.Owner = sclient.Client.Group.GroupCode;
            iclient.Client.Status = sclient.Client.ClientStatus;
            iclient.Client.VendorFlag = "N";

            if (otherInfo)
            {
                iclient.Client.PortfolioId = txtPort.Text;

                iclient.Client.PersonalInfo = new BTS.ClientCore.Wrapper.Structures.Import.PersonalInfo();
                iclient.Client.PersonalInfo.TaxId = txtFEIN.Text;
                iclient.Client.PersonalInfo.TaxIdTypeCode = rblTaxType.SelectedValue;
            }

            CompanyNumberDropDownList cnddl = Master.FindControl("companyselector1").FindControl("companynumberdropdownList1") as CompanyNumberDropDownList;


            //call the web service
            ClientProxy.Client client = Common.GetClientProxy();
            string result = client.ModifyClient(iclient.Client.CCClientId, cnddl.SelectedItem.Text, iclient.Client.PortfolioId,
                iclient.Client.ClientCategory,
                iclient.Client.ClientType, iclient.Client.EffectiveDate, iclient.Client.Owner,
                iclient.Client.Status, iclient.Client.PersonalInfo.TaxId, iclient.Client.PersonalInfo.TaxIdTypeCode,
                names, addresses,
                otherInfo ? BuildCodesString() : OldCodeValues,
                otherInfo ? BuildAttributeString() : string.Empty,
                classCodes,
                Convert.ToInt32(hdnSICCode.Value), Convert.ToInt32(hdnNAICSCode.Value), businessPhone, txtBusinessDescription.Text, Common.GetUser());

            try
            {
                structures.Import.Vector editedclient = (structures.Import.Vector)BCS.Core.XML.Deserializer.Deserialize(result, typeof(structures.Import.Vector));

                if (sclient.Client.PortfolioId != iclient.Client.PortfolioId)
                {
                    SubmissionProxy.Submission sp = Common.GetSubmissionProxy();
                    long pid = txtPort.Text == null || txtPort.Text.Length == 0 ? 0 : Convert.ToInt64(txtPort.Text);
                    sp.UpdatePortfolioId(Common.GetSafeStringFromSession(SessionKeys.CompanyNumber), Convert.ToInt64(editedclient.ClientId), pid, Common.GetUser());
                }

                Session["StatusMessage"] = "Client successfully edited.";
            }
            catch (Exception ex)
            {

                throw ex;
            }

            if (Session["cliref"] != null && Session["deleterequest"] == null)
            {
                string ss = Session["cliref"].ToString();
                Session.Contents.Remove("cliref");
                Response.Redirect(ss);
            }
            Common.SetStatus(messagePlaceHolder);
        }

        private void AddClientAddressWebcontrol(Control parent)
        {
            Control clientAddressUC = LoadControl("ClientAddress.ascx");
            clientAddressUC.ID = Guid.NewGuid().ToString().Replace("-", string.Empty);

            ((UserControls_ClientAddress)clientAddressUC).PrimaryFlagEditable = AllowPrimaryNameChange;
            ((UserControls_ClientAddress)clientAddressUC).PrimaryAddress = (parent.Controls.Count == 0);

            parent.Controls.Add(clientAddressUC);
            parent.Controls.Add(new LiteralControl("<br />"));
            parent.Controls.Add(new LiteralControl("<br />"));
            if (Session["ClientID"] == null)
                ((UserControls_ClientAddress)clientAddressUC).Mode = Common.Mode.Add;
            else
                ((UserControls_ClientAddress)clientAddressUC).Mode = Common.Mode.Modify;

            ((UserControls_ClientAddress)parent.FindControl(clientAddressUC.ID)).CountryValue = "USA";
            ((UserControls_ClientAddress)parent.FindControl(clientAddressUC.ID)).SetFocus();


        }
        private void AddClientAddressWebcontrol(Control parent, BTS.ClientCore.Wrapper.Structures.Info.Address[] addresses)
        {
            parent.Controls.Clear();
            if (addresses != null && addresses.Length > 0)
            {
                //Client Core could say the client has no primary address or has multiple; correct for that
                if (!addresses.Any(a => a.Primary == "Y"))
                    addresses[0].Primary = "Y";

                bool primaryAddressSet = false;
                foreach (structures.Info.Address av in addresses)
                {
                    #region required controls to display a client address
                    Control clientAddressUC = LoadControl("ClientAddress.ascx");
                    clientAddressUC.ID = Guid.NewGuid().ToString().Replace("-", string.Empty);
                    ((UserControls_ClientAddress)clientAddressUC).PrimaryFlagEditable = AllowPrimaryNameChange;

                    parent.Controls.Add(clientAddressUC);

                    #endregion

                    var addressControl = (UserControls_ClientAddress)parent.FindControl(clientAddressUC.ID);

                    if (av.HouseNumber != null)
                        addressControl.HouseValue = av.HouseNumber;
                    if (av.Address1 != null)
                        addressControl.StreetnameValue = av.Address1;
                    if (av.Address2 != null)
                        addressControl.Address2Value = av.Address2;
                    if (av.City != null)
                        addressControl.CityValue = av.City;
                    if (av.Country != null)
                        addressControl.CountryValue = av.Country;
                    if (av.County != null)
                        addressControl.CountyValue = av.County;
                    if (av.StateProvinceId != null)
                        addressControl.StateValue = av.StateProvinceId;
                    if (av.PostalCode != null)
                        addressControl.ZipCodeValue = av.PostalCode;
                    if (av.AddressId != null)
                        addressControl.AddressIdValue = av.AddressId;
                    if (av.ClearanceAddressType != null)
                        addressControl.ClearanceAddressTypeValue = av.ClearanceAddressType;

                    long clientCoreId = 0;
                    if (long.TryParse(lblClientId.Text, out clientCoreId))
                    {
                        var coordinates = BTS.WFA.BCS.Services.LegacyService.GetAddressCoordinates(clientCoreId, int.Parse(av.AddressId));
                        addressControl.SetGeoCodeDescription(coordinates.Item1.ToString(), coordinates.Item2.ToString());
                    }

                    if (!primaryAddressSet && av.Primary == "Y")
                    { 
                        ((UserControls_ClientAddress)parent.FindControl(clientAddressUC.ID)).PrimaryAddress = true;
                        primaryAddressSet = true;
                    }

                    ((UserControls_ClientAddress)clientAddressUC).Mode = Common.Mode.Modify;
                }
            }
        }

        private void AddClientNameWebcontrol(Control parent)
        {
            Control clientNameSelectorUC = LoadControl("ClientNameSelector.ascx");
            clientNameSelectorUC.ID = Guid.NewGuid().ToString().Replace("-", string.Empty);

            ((UserControls_ClientNameSelector)clientNameSelectorUC).ShowFullName = true;
            ((UserControls_ClientNameSelector)clientNameSelectorUC).PrimaryFlagEditable = AllowPrimaryNameChange;
            ((UserControls_ClientNameSelector)clientNameSelectorUC).PrimaryName = (parent.Controls.Count == 0);
            parent.Controls.Add(clientNameSelectorUC);

            if (Session["ClientID"] == null)
                ((UserControls_ClientNameSelector)clientNameSelectorUC).Mode = Common.Mode.Add;
            else
                ((UserControls_ClientNameSelector)clientNameSelectorUC).Mode = Common.Mode.Modify;
            ((UserControls_ClientNameSelector)clientNameSelectorUC).SetFocus();
        }
        private void AddClientNameWebcontrol(Control parent, BTS.ClientCore.Wrapper.Structures.Info.Name[] names)
        {
            parent.Controls.Clear();
            foreach (structures.Info.Name name in names)
            {
                #region required controls to display a client name
                Control clientNameSelectorUC = LoadControl("ClientNameSelector.ascx");
                clientNameSelectorUC.ID = Guid.NewGuid().ToString().Replace("-", string.Empty);

                // Add the controls
                parent.Controls.Add(clientNameSelectorUC);
                ((UserControls_ClientNameSelector)clientNameSelectorUC).ShowFullName = true;
                ((UserControls_ClientNameSelector)clientNameSelectorUC).PrimaryFlagEditable = AllowPrimaryNameChange;


                #endregion

                if (name.NameBusiness != null)
                {
                    ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).BusinessNameValue = name.NameBusiness;
                }
                else
                {
                    ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).IsPersonal = true;

                    if (name.NameFirst != null)
                        ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).FirstNameValue = name.NameFirst;
                    if (name.NameLast != null)
                        ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).LastNameValue = name.NameLast;
                    if (name.NameMiddle != null)
                        ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).MiddleNameValue = name.NameMiddle;
                    if (name.NamePrefix != null)
                        ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).NamePrefixValue = name.NamePrefix;
                    if (name.NameSuffix != null)
                        ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).NameSuffixValue = name.NameSuffix;
                }
                if (name.SequenceNumber != null)
                    ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).NameSequenceOrID = name.SequenceNumber;
                if (name.ClearanceNameType != null)
                    ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).ClearanceNameTypeValue = name.ClearanceNameType;

                ((UserControls_ClientNameSelector)parent.FindControl(clientNameSelectorUC.ID)).PrimaryName = (name.PrimaryNameFlag == "Y");

                ((UserControls_ClientNameSelector)clientNameSelectorUC).Mode = Common.Mode.Modify;
            }
        }

        private string BuildAttributeString()
        {
            ArrayList alids = new ArrayList();
            if (phAttributes.Controls.Count > 0)
            {
                HtmlTable table = phAttributes.Controls[0] as HtmlTable;
                foreach (HtmlTableRow row in table.Rows)
                {
                    foreach (HtmlTableCell cell in row.Cells)
                    {
                        foreach (Control c in cell.Controls)
                        {
                            if (c is IValidator)
                                continue;
                            if (c is WebControl)
                            {
                                Type t = c.GetType();
                                // Gets the attributes for the collection.
                                System.ComponentModel.AttributeCollection attrs = System.ComponentModel.TypeDescriptor.GetAttributes(c);

                                /* Prints the name of the default property by retrieving the 
                                 * DefaultPropertyAttribute from the AttributeCollection. */
                                ControlValuePropertyAttribute myAttribute =
                                   (ControlValuePropertyAttribute)attrs[typeof(ControlValuePropertyAttribute)];

                                System.Reflection.PropertyInfo pi = t.GetProperty(myAttribute.Name);
                                object value = pi.GetValue(c, null);
                                alids.Add(string.Format("{0}={1}", c.ID, value));
                            }
                        }
                    }
                }
            }
            string[] attributes = new string[alids.Count];
            alids.CopyTo(attributes);
            return string.Join("|", attributes);
        }
        private string BuildCodesString()
        {

            ArrayList alids = new ArrayList();
            if (phCodeTypes.Controls.Count > 0)
            {
                HtmlTable table = phCodeTypes.Controls[0] as HtmlTable;
                foreach (HtmlTableRow row in table.Rows)
                {
                    foreach (HtmlTableCell cell in row.Cells)
                    {
                        foreach (Control c in cell.Controls)
                        {
                            if (c is DropDownList)
                            {
                                DropDownList ddl = c as DropDownList;
                                if (ddl.SelectedIndex > 0)
                                {
                                    alids.Add(ddl.SelectedValue);
                                }
                            }
                        }
                    }
                }
            }
            string[] codeids = new string[alids.Count];
            alids.CopyTo(codeids);

            return string.Join("|", codeids);

        }
        private bool isPhoneRequired()
        {
            int companyId = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
            BCS.DAL.CompanyClientAdvancedSearch advSrchAttributes = BCS.Core.Clearance.SubmissionsEF.GetAdvSearchAttributesByCompany(companyId);
            if (advSrchAttributes != null)
            {
                return advSrchAttributes.IsPhoneRequiredField;
            }
            return false;
        }
        private void LoadAttributes(int companyNumberId)
        {
            // add validation to Phone number text box if it is a required field
            int companyId = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
            BCS.DAL.CompanyClientAdvancedSearch advSrchAttributes = BCS.Core.Clearance.SubmissionsEF.GetAdvSearchAttributesByCompany(companyId);

            if (advSrchAttributes != null)
            {
                if (advSrchAttributes.IsPhoneRequiredField)
                {
                    RequiredFieldValidator rfv = new RequiredFieldValidator();
                    rfv.ControlToValidate = "txtBusinessPhone";
                    rfv.Text = " *";
                    rfv.ID = "reqPhone";
                    rfv.ErrorMessage = "&nbsp;";
                    rfv.ValidationGroup = "merge";
                }
            }
            BCS.Biz.CompanyNumberAttributeCollection attributes =
                    ConfigValues.GetCompanyAttributes(companyNumberId, true);
            phAttributes.Controls.Clear();
            if (attributes.Count > 0)
            {
                HtmlTable table = new HtmlTable();
                table.Width = "100%";
                table.ID = "tblAttributes";
                table.CellPadding = 0;
                table.CellSpacing = 0;
                phAttributes.Controls.Add(table);
                foreach (BCS.Biz.CompanyNumberAttribute attr in attributes)
                {
                    #region Text Label
                    HtmlTableRow row = new HtmlTableRow();
                    table.Rows.Add(row);
                    HtmlTableCell cell = new HtmlTableCell();

                    cell = new HtmlTableCell();
                    cell.Style.Add(HtmlTextWriterStyle.WhiteSpace, "nowrap");
                    row.Cells.Add(cell);

                    cell.InnerText = attr.Label + ":";
                    #endregion

                    #region Control Row
                    row = new HtmlTableRow();
                    table.Rows.Add(row);
                    cell = new HtmlTableCell();
                    row.Cells.Add(cell);

                    Type t = Type.GetType(string.Concat(attr.AttributeDataType.UserControl, ", System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"), false, true);
                    System.Reflection.ConstructorInfo ci = t.GetConstructor(new Type[] { });
                    WebControl c = (WebControl)ci.Invoke(null);
                    c.ID = attr.Id.ToString();

                    if (attr.AttributeDataType.DataTypeName.Equals("Date", StringComparison.CurrentCultureIgnoreCase))
                    {
                        c.Attributes.Add("onclick", "scwShow(this,this);");
                        c.Attributes.Add("onkeydown", "hideCalOnTab(event);");
                        c.Attributes.Add("onblur", "this.value = purgeDate(this);");
                        (c as TextBox).AutoCompleteType = AutoCompleteType.Disabled;
                    }

                    if (attr.AttributeDataType.DataTypeName.Equals("Money", StringComparison.CurrentCultureIgnoreCase))
                    {
                        c.Attributes.Add("onblur", "this.value=formatCurrency(this.value);");
                    }

                    if (!attr.AttributeDataType.DataTypeName.Equals("Bit", StringComparison.CurrentCultureIgnoreCase))
                    {
                        c.Width = new Unit("190px");
                    }
                    cell.Controls.Add(c);
                    if (attr.Required)
                    {
                        RequiredFieldValidator rfv = new RequiredFieldValidator();
                        Common.AddControl(cell, rfv, false, false);
                        rfv.ControlToValidate = c.ID;
                        rfv.SetFocusOnError = true;
                        rfv.Text = " *";
                        rfv.ErrorMessage = "&nbsp;";
                    }
                    if (attr.AttributeDataType.DataTypeName.Equals("Date", StringComparison.CurrentCultureIgnoreCase))
                    {
                        CompareValidator cv = new CompareValidator();
                        Common.AddControl(cell, cv, false, false);
                        cv.ControlToValidate = c.ID;
                        cv.SetFocusOnError = true;
                        cv.Type = ValidationDataType.Date;
                        cv.Operator = ValidationCompareOperator.DataTypeCheck;
                        cv.ErrorMessage = "&nbsp;";
                        cv.Text = "* MM/dd/yyyy";
                    }
                    if (attr.ReadOnlyProperty)
                        c.Enabled = false;

                    #endregion
                }
                phAttributes.Visible = table.Rows.Count > 0;
            }
            else
            { phAttributes.Visible = false; }
        }
        private ListItem[] GetItems(int id, DateTime effDate, int extra)
        {
            BCS.Biz.DataManager dm = new BCS.Biz.DataManager(DefaultValues.DSN);
            dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumberCodeType.Columns.Id, id);
            BCS.Biz.CompanyNumberCodeType companyNumberCodeType = dm.GetCompanyNumberCodeType(
                BCS.Biz.FetchPath.CompanyNumberCodeType.CompanyNumberCode);

            List<ListItem> lic = new List<ListItem>();
            lic.Add(new ListItem("", "0"));

            int defaultId = 0;
            if (!companyNumberCodeType.DefaultCompanyNumberCodeId.IsNull)
            {
                defaultId = companyNumberCodeType.DefaultCompanyNumberCodeId.Value;
            }
            BCS.Biz.CompanyNumberCodeCollection sorted = companyNumberCodeType.CompanyNumberCodes.SortByCode(OrmLib.SortDirection.Ascending);
            foreach (BCS.Biz.CompanyNumberCode code in sorted)
            {
                if (code.ExpirationDate.IsNull || (code.ExpirationDate.Value > effDate) || code.Id == extra)
                {
                    #region process display text, sometimes code or description may be absent
                    string itemText = string.Concat(code.Code, "-", code.Description);
                    itemText = itemText.Trim();
                    string[] itemTextArray = itemText.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    itemText = string.Join(" - ", itemTextArray);
                    #endregion
                    ListItem li = new ListItem(itemText, code.Id.ToString());
                    if (code.Id == defaultId)
                    {
                        li.Selected = true;
                    }
                    lic.Add(li);
                }
            }

            return lic.ToArray();
        }
        private void LoadCodeTypes(int companyNumberId)
        {
            CompanyNumberCodeTypeCollection types = ConfigValues.GetCompanyCodeTypes(companyNumberId, true);

            phCodeTypes.Controls.Clear();

            if (types.Count > 0)
            {
                HtmlTable table = new HtmlTable();
                table.Width = "100%";
                table.ID = "tblCodeTypes";
                table.CellPadding = 0;
                table.CellSpacing = 0;
                phCodeTypes.Controls.Add(table);
                foreach (CompanyNumberCodeType type in types)
                {
                    ListItem[] lic = GetItems(type.Id, DateTime.MinValue, 0);
                    #region Text Label
                    HtmlTableRow row = new HtmlTableRow();
                    table.Rows.Add(row);

                    HtmlTableCell cell = new HtmlTableCell();
                    cell.Style.Add(HtmlTextWriterStyle.WhiteSpace, "nowrap");
                    row.Cells.Add(cell);

                    // using a label instead of inner text, since List search extender needs some element instead of text node to display prompt text beside it.
                    Label l = new Label();
                    l.Text = type.CodeName + ":";
                    cell.Controls.Add(l);
                    #endregion

                    #region Control
                    cell.Controls.Add(new LiteralControl("<br />"));


                    DropDownList ddl = new DropDownList();
                    ddl.ID = type.Id + "ddl";
                    cell.Controls.Add(ddl);
                    ddl.Width = new Unit("190px");
                    ddl.Items.AddRange(lic);
                    if (type.Required)
                    {
                        RequiredFieldValidator rfv = new RequiredFieldValidator();
                        Common.AddControl(cell, rfv, false, false);
                        rfv.ControlToValidate = ddl.ID;
                        rfv.InitialValue = "0";
                        rfv.SetFocusOnError = true;
                        rfv.Text = " *";
                        rfv.ErrorMessage = "&nbsp;";
                    }
                    ListSearchExtender lse = new ListSearchExtender();
                    lse.PromptPosition = ListSearchPromptPosition.Right;
                    lse.TargetControlID = ddl.ID;
                    lse.ID = Guid.NewGuid().ToString().Replace("-", "");
                    //lse.SkinID = "WithExtraMargin";
                    cell.Controls.Add(lse);
                    #endregion
                }

                phCodeTypes.Visible = table.Rows.Count > 0;

            }
            else
            { phCodeTypes.Visible = false; }
        }
        private void Add()
        {
            string effectivedate = DateTime.Now.ToString();
            string businessPhone = txtBusinessPhone.Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            structures.Import.ClientImportData iclient = new BTS.ClientCore.Wrapper.Structures.Import.ClientImportData();
            iclient.Client = new BTS.ClientCore.Wrapper.Structures.Import.ImportClient();
            iclient.Client.EffectiveDate = effectivedate;
            iclient.Client.Owner = null;
            iclient.Client.Status = "A";
            iclient.Client.VendorFlag = "N";

            #region Collect our addresses and build ArrayList
            ArrayList addressVersionsToAdd = new ArrayList();
            var addressList = new List<BTS.ClientCore.Wrapper.Structures.Import.AddressInfo>();

            foreach (Control control in phAddresses.Controls)
            {
                if (control is UserControls_ClientAddress)
                {
                    string houseValue = ((UserControls_ClientAddress)control).HouseValue;
                    string streetnameValue = ((UserControls_ClientAddress)control).StreetnameValue;
                    string address2Value = ((UserControls_ClientAddress)control).Address2Value;
                    string cityValue = ((UserControls_ClientAddress)control).CityValue;
                    string stateValue = ((UserControls_ClientAddress)control).StateValue;
                    string countryValue = ((UserControls_ClientAddress)control).CountryValue;
                    string countyValue = ((UserControls_ClientAddress)control).CountyValue;
                    string zipCodeValue = ((UserControls_ClientAddress)control).ZipCodeValue;
                    string addressIdValue = ((UserControls_ClientAddress)control).AddressIdValue;
                    string clearanceAddressTypeValue = ((UserControls_ClientAddress)control).ClearanceAddressTypeValue;

                    BTS.ClientCore.Wrapper.Structures.Import.AddressInfo av =
                        new BTS.ClientCore.Wrapper.Structures.Import.AddressInfo();

                    av.HouseNumber = houseValue;
                    av.ADDR1 = streetnameValue;
                    av.ADDR2 = address2Value;
                    av.CityName = cityValue;
                    av.StateCode = stateValue;
                    av.Zip = zipCodeValue;
                    av.CCAddressId = addressIdValue;
                    av.EffectiveDate = effectivedate;
                    av.ClearanceAddressType = clearanceAddressTypeValue;

                    // TODO :: duplicate address option
                    av.DuplicateAddressOption = "1";

                    av.County = countyValue;
                    av.Country = countryValue;

                    av.Primary = ((UserControls_ClientAddress)control).PrimaryAddress ? "Y" : "N";

                    if (av.HouseNumber.Length == 0 && av.ADDR1.Length == 0 && av.CityName.Length == 0
                       && av.StateCode.Length == 0 && av.Zip.Length == 0)
                    { continue; }
                    else
                        addressVersionsToAdd.Add(av);

                    addressList.Add(av);
                }
            }

            StringBuilder addresses = new StringBuilder();
            foreach (var a in addressList.OrderByDescending(c => c.Primary))
            {
                string addr = BCS.Core.XML.Serializer.SerializeAsXml(a);
                addresses.AppendFormat("{0}+-", addr);
            }

            if (addresses.Length > 0)
                addresses.Remove(addresses.Length - 2, 2);
            #endregion

            #region Collect our names and build ArrayList
            var clientNamesToAdd = new List<structures.Import.NameInfo>();
            List<string> names = new List<string>();
            int businessNamesCount = 0;
            int indNamesCount = 0;
            foreach (Control control in phNames.Controls)
            {
                //if (control.GetType().Name.ToLower() == "clientnameselector_ascx")
                if (control is UserControls_ClientNameSelector)
                {
                    BTS.ClientCore.Wrapper.Structures.Import.NameInfo cn =
                        new BTS.ClientCore.Wrapper.Structures.Import.NameInfo();

                    string seqNum = ((UserControls_ClientNameSelector)control).NameSequenceOrID;

                    if (((UserControls_ClientNameSelector)control).IsBusiness)
                    {
                        businessNamesCount++;
                        string businessNameValue = ((UserControls_ClientNameSelector)control).BusinessNameValue;
                        cn.BusinessName = businessNameValue;
                        iclient.Client.ClientType = "Seg Bus";
                    }

                    if (((UserControls_ClientNameSelector)control).IsPersonal)
                    {
                        indNamesCount++;
                        string namePrefixValue = ((UserControls_ClientNameSelector)control).NamePrefixValue;
                        string firstNameValue = ((UserControls_ClientNameSelector)control).FirstNameValue;
                        string middleNameValue = ((UserControls_ClientNameSelector)control).MiddleNameValue;
                        string lastNameValue = ((UserControls_ClientNameSelector)control).LastNameValue;
                        string nameSuffixValue = ((UserControls_ClientNameSelector)control).NameSuffixValue;

                        cn.NamePrefix = namePrefixValue;
                        cn.FirstName = firstNameValue;
                        cn.MiddleName = middleNameValue;
                        cn.LastName = lastNameValue;
                        cn.NameSuffix = nameSuffixValue;
                        iclient.Client.ClientType = "Seg Ind";
                    }
                    //cn.NameSeqNum = seqNum;                    

                    cn.DisplayFlag = (((UserControls_ClientNameSelector)control).PrimaryName) ? "Y" : "N";

                    // TODO: What about the nametype? (Insured/DBA)
                    string nameTypeValue = ((UserControls_ClientNameSelector)control).NameTypeValue;
                    cn.NameType = nameTypeValue;

                    string clearanceNameTypeValue = ((UserControls_ClientNameSelector)control).ClearanceNameTypeValue;
                    cn.ClearanceNameType = clearanceNameTypeValue;

                    // Add to the object Array:
                    if (cn.LastName == null && cn.FirstName == null && cn.BusinessName == null)
                        continue;
                    clientNamesToAdd.Add(cn);
                }
            }

            //Client Core automatically sets the first name to primary, so we need to order these with the primary first
            foreach (var c in clientNamesToAdd.OrderByDescending(c => c.DisplayFlag))
            {
                string name = BCS.Core.XML.Serializer.SerializeAsXml(c);
                names.Add(name);
            }

            if (indNamesCount > 0 && businessNamesCount > 0)
            {
                Common.SetStatus(messagePlaceHolder, "There cannot be both business names and individual names.");
            }

            #endregion

            #region Collect class codes from grid
            StringBuilder classCodes = new StringBuilder();

            foreach (GridViewRow var in GridView1.Rows)
            {
                string classCodeId = string.Empty;
                try
                {
                    classCodeId = (var.FindControl("txtClassCodeId") as Label).Text;
                }
                catch (Exception)
                {
                    classCodeId = "0";
                }
                // since this is a new client clientclasscodeid would be 0
                classCodes.AppendFormat("0={0}|", classCodeId);
            }
            #endregion



            if (clientNamesToAdd.Count == 0 || addressVersionsToAdd.Count == 0)
            {
                Common.SetStatus(messagePlaceHolder, "Please enter at least one name and address.", true);
                return;
            }

            CompanyNumberDropDownList cnddl = Master.FindControl("companyselector1").FindControl("companynumberdropdownList1") as CompanyNumberDropDownList;

            if (!string.IsNullOrEmpty(txtFEIN.Text))
            {
                iclient.Client.PersonalInfo = new BTS.ClientCore.Wrapper.Structures.Import.PersonalInfo();
                iclient.Client.PersonalInfo.TaxId = txtFEIN.Text;
                iclient.Client.PersonalInfo.TaxIdTypeCode = rblTaxType.SelectedValue;

            }
            ClientProxy.Client client = Common.GetClientProxy();

            string result = client.AddClientWithImport(cnddl.SelectedItem.Text, iclient.Client.ClientCategory,
                string.Empty, // client type is derived from company config. pass something
                iclient.Client.EffectiveDate,
                string.Empty, // owner is derived from company config. pass something
                iclient.Client.Status, iclient.Client.VendorFlag, iclient.Client.PersonalInfo.TaxId,
                iclient.Client.PersonalInfo.TaxIdTypeCode, 1, String.Join("+-", names), addresses.ToString(), BuildCodesString(), BuildAttributeString(),
                classCodes.ToString(), Convert.ToInt32(hdnSICCode.Value), Convert.ToInt32(hdnNAICSCode.Value), businessPhone, txtBusinessDescription.Text, Common.GetUser());

            try
            {
                structures.Import.Vector addedclient = (structures.Import.Vector)BCS.Core.XML.Deserializer.Deserialize(result, typeof(structures.Import.Vector));
                Session["ClientId"] = addedclient.ClientId;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            Session.Contents.Remove("SubmissionId");
            if (DefaultValues.UseWizard)
                Response.Redirect("SubmissionWizard.aspx");
            Response.Redirect("Submission.aspx?op=add");
        }
        private static Biz.SicCodeListCollection GetSicCodes()
        {
            return new DataManager(DefaultValues.DSN).GetSicCodeListCollection();
        }

        private static Biz.NaicsCodeListCollection GetNaicsCodes()
        {
            return new DataManager(DefaultValues.DSN).GetNaicsCodeListCollection();
        }

        private static string[] AsStringArrayBy(string property, BCS.Biz.SicCodeListCollection collection)
        {
            string[] sics = new string[collection.Count];

            System.Collections.Generic.List<string> items = new System.Collections.Generic.List<string>(collection.Count);

            for (int i = 0; i < collection.Count; i++)
            {
                items.Add(
                    AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(collection[i][property].ToString(),
                    string.Concat(collection[i]["Id"].ToString(), "|", collection[i]["CodeDescription"].ToString())));
            }
            return items.ToArray();
        }

        private static string[] AsNewStringArrayBy(string property, BCS.Biz.NaicsCodeListCollection collection)
        {
            string[] naics = new string[collection.Count];

            System.Collections.Generic.List<string> items = new System.Collections.Generic.List<string>(collection.Count);

            for (int i = 0; i < collection.Count; i++)
            {
                items.Add(
                    AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(collection[i][property].ToString(),
                    string.Concat(collection[i]["Id"].ToString(), "|", collection[i]["CodeDescription"].ToString())));
            }
            return items.ToArray();
        }

        // return type and parameters must be same for auto complete extender to work
        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string[] GetCompletionList(string prefixText, int count, string contextKey)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(string.Format("^{0}.*", prefixText));
            string cProperty = "Code";
            BCS.Biz.SicCodeListCollection sics = GetSicCodes();
            BCS.Biz.SicCodeListCollection fsics = new BCS.Biz.SicCodeListCollection();
            int cnt = 0;
            foreach (BCS.Biz.SicCodeList sic in sics)
            {
                System.Text.RegularExpressions.Match m = reg.Match(sic.Code);
                if (m.Success)
                {
                    fsics.Add(sic);
                    cnt++;
                    if (cnt == count) break;
                }

            }
            return AsStringArrayBy(cProperty, fsics);
        }

        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string[] GetNAICSCompletionList(string prefixText, int count, string contextKey)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(string.Format("^{0}.*", prefixText));
            string cProperty = "Code";
            BCS.Biz.NaicsCodeListCollection naics = GetNaicsCodes();
            BCS.Biz.NaicsCodeListCollection fnaicss = new BCS.Biz.NaicsCodeListCollection();
            int cnt = 0;
            foreach (BCS.Biz.NaicsCodeList naic in naics)
            {
                System.Text.RegularExpressions.Match m = reg.Match(naic.Code);
                if (m.Success)
                {
                    fnaicss.Add(naic);
                    cnt++;
                    if (cnt == count) break;
                }

            }
            return AsNewStringArrayBy(cProperty, fnaicss);
        }

        [System.Web.Services.WebMethod()]
        public static string GetSicCodeAndDescription(string naicsCodeId)
        {
            string siccode = GetSicCodeByNaicsCode(Convert.ToInt32(naicsCodeId));
            return siccode;
        }
        private void addAdvClientSearchDetails(AdvClientSearch.AdvClientSearchRslt_Type result)
        {
            AdvClientSearch.GeneralPartyInfo_Type1 genralInfo = result.ClienInfo.InsuredOrPrincipal.GeneralPartyInfo;
            AdvClientSearch.NameInfo_Type nameInfo = new AdvClientSearch.NameInfo_Type();
            if (genralInfo != null)
            {
                AdvClientSearch.Communications_Type1 comType = genralInfo.Communications;
                txtBusinessPhone.Text = comType.PhoneInfo[0].PhoneNumber.Value.ToString();
                if (genralInfo.NameInfo != null)
                {
                    BTS.ClientCore.Wrapper.Structures.Info.Name[] names = new BTS.ClientCore.Wrapper.Structures.Info.Name[genralInfo.NameInfo.Select(p => p).Count()];
                    int i = 0;
                    foreach (AdvClientSearch.NameInfo_Type1 nameType in genralInfo.NameInfo)
                    {
                        if (nameType.CommlName != null)
                        {
                            if (nameType.CommlName.CommercialName != null)
                                if (!string.IsNullOrEmpty(nameType.CommlName.CommercialName.Value.ToString()))
                                    names[i].NameBusiness = CheckForNameFormating(Common.formatString(nameType.CommlName.CommercialName.Value.ToString()));

                        }
                        else if (nameType.FamilyName != null)
                        {
                            if (nameType.FamilyName.Surname != null)
                                if (!string.IsNullOrEmpty(nameType.FamilyName.Surname.Value.ToString()))
                                    names[i].NameBusiness = CheckForNameFormating(Common.formatString(nameType.FamilyName.Surname.Value.ToString()));
                        }
                        else if (nameType.PersonName != null)
                        {
                            if (nameType.PersonName.GivenName != null)
                            {
                                if (!string.IsNullOrEmpty(nameType.PersonName.GivenName.Value.ToString()))
                                    names[i].NameBusiness = CheckForNameFormating(Common.formatString(nameType.PersonName.GivenName.Value.ToString()));
                            }
                            else if (nameType.PersonName.Surname != null)
                            {
                                if (!string.IsNullOrEmpty(nameType.PersonName.Surname.Value.ToString()))
                                    names[i].NameBusiness = CheckForNameFormating(Common.formatString(nameType.PersonName.Surname.Value.ToString()));
                            }
                        }
                        i++;
                    }
                    AddClientNameWebcontrol(phNames, names);
                }

                int j = 0;
                if (genralInfo.Addr != null)
                {
                    BTS.ClientCore.Wrapper.Structures.Info.Address[] addresses = new BTS.ClientCore.Wrapper.Structures.Info.Address[genralInfo.Addr.Select(p => p).Count()];
                    foreach (AdvClientSearch.Addr_Type addressType in genralInfo.Addr)
                    {
                        if (addressType.Addr1 != null)
                            if (!string.IsNullOrEmpty(addressType.Addr1.Value.ToString()))
                            {
                                // split address string to sepearte street number
                                string street = addressType.Addr1.Value.ToString().Split(' ').First();
                                double res;
                                bool isNumeric = Double.TryParse(street, out res);
                                if (isNumeric)
                                {
                                    addresses[j].HouseNumber = street;
                                    addresses[j].Address1 = Common.formatString(addressType.Addr1.Value.ToString().Replace(street, "").TrimStart());
                                }
                                else
                                    addresses[j].Address1 = Common.formatString(addressType.Addr1.Value.ToString());
                            }
                        if (addressType.Addr2 != null)
                            if (!string.IsNullOrEmpty(addressType.Addr2.Value.ToString()))
                                addresses[j].Address2 = Common.formatString(addressType.Addr2.Value.ToString());
                        if (addressType.City != null)
                            if (!string.IsNullOrEmpty(addressType.City.Value.ToString()))
                                addresses[j].City = Common.formatString(addressType.City.Value.ToString());
                        if (addressType.County != null)
                            if (!string.IsNullOrEmpty(addressType.County.Value.ToString()))
                                addresses[j].County = Common.formatString(addressType.County.Value.ToString());
                        if (addressType.StateProv != null)
                            if (!string.IsNullOrEmpty(addressType.StateProv.Value.ToString()))
                                addresses[j].StateProvinceId = addressType.StateProv.Value.ToString();
                        if (addressType.PostalCode != null)
                            if (!string.IsNullOrEmpty(addressType.PostalCode.Value.ToString()))
                                addresses[j].PostalCode = addressType.PostalCode.Value.ToString().Substring(0, 5);
                        if (addressType.Country != null)
                            if (!string.IsNullOrEmpty(addressType.Country.Value.ToString()))
                                addresses[j].Country = addressType.Country.Value.ToString();

                        j++;
                    }
                    AddClientAddressWebcontrol(phAddresses, addresses);
                }
            }

        }
        #endregion
        protected void ScriptManager1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
        {
            throw new ApplicationException("Ajax Error", e.Exception);
        }

        protected void btnAddClassCodeOnGrid_Click(object sender, EventArgs e)
        {
            GridView1_RowCommand(GridView1, new GridViewCommandEventArgs(null, new CommandEventArgs("New", null)));
            GridView1_RowCommand(GridView1, new GridViewCommandEventArgs(null, new CommandEventArgs("Update", null)));

            GridView1.ShowFooter = true;
            GridView1.EditIndex = -1;

            if (GridView1.FooterRow == null)
            {
                Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
                dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberHazardGradeType.Columns.CompanyNumberId, GetCurrentCompanyNumberId());
                Biz.CompanyNumberHazardGradeTypeCollection cnhgtcoll = dm.GetCompanyNumberHazardGradeTypeCollection();
                if (cnhgtcoll.Count == 0)
                {
                    Common.SetStatus(messagePlaceHolder, "The company number selected does not have hazard grade types defined.");
                    return;
                }



                ClientClassCodeCollection coll = new ClientClassCodeCollection();
                CompanyNumber dummy = null;
                coll.Add(dm.NewClientClassCode(dm.NewClassCode("", dummy), dm.NewClient(0, dummy)));
                GridView1.DataSourceID = null;
                GridView1.DataSource = coll;
                GridView1.DataBind();


                GridView1.Rows[0].Visible = false;
            }
            else
            {
                if (_operation == "add")
                {
                    Biz.DataManager dm = new DataManager(DefaultValues.DSN);
                    // get all class codes for this company number, TODO: since we are already caching this, utilize it
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.CompanyNumberId, GetCurrentCompanyNumberId());
                    ClassCodeCollection allClassCodes = dm.GetClassCodeCollection(FetchPath.ClassCode.SicCodeList, FetchPath.ClassCode.NaicsCodeList);

                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.CompanyNumberId, GetCurrentCompanyNumberId());
                    CompanyNumberClassCodeHazardGradeValueCollection hgValues = dm.GetCompanyNumberClassCodeHazardGradeValueCollection();


                    // get all class codes for the client yet to be added from the ViewState
                    Core.Clearance.BizObjects.ClientClassCode[] clientClassCodesArray = ClientClassCodeForAddClient;
                    List<Core.Clearance.BizObjects.ClientClassCode> clientClassCodesList = new List<BCS.Core.Clearance.BizObjects.ClientClassCode>();
                    clientClassCodesList.AddRange(clientClassCodesArray);


                    // convert them to Biz.ClientClassCodeCollection so that they follow what we are doing on edit and binds easily
                    ClientClassCodeCollection addCCC = new ClientClassCodeCollection();
                    foreach (Core.Clearance.BizObjects.ClientClassCode var in clientClassCodesList)
                    {
                        // find the class code
                        ClassCode foundClassCode = allClassCodes.FindById(var.ClassCodeId);
                        foundClassCode.SicCodeId = allClassCodes.FindById(var.ClassCodeId).SicCodeId;
                        foundClassCode.NaicsCodeId = allClassCodes.FindById(var.ClassCodeId).NaicsCodeId;
                        Biz.ClientClassCode foundClientClassCode = foundClassCode.NewClientClassCode();

                        CompanyNumberClassCodeHazardGradeValueCollection hgValuesForThisClassCode = hgValues.FilterByClassCodeId(var.ClassCodeId);
                        foreach (CompanyNumberClassCodeHazardGradeValue subVar in hgValuesForThisClassCode)
                        {
                            CompanyNumberClassCodeHazardGradeValue companyNumberClassCodeHazardGradeValueVar = foundClientClassCode.ClassCode.NewCompanyNumberClassCodeHazardGradeValue();
                            companyNumberClassCodeHazardGradeValueVar.HazardGradeTypeId = subVar.HazardGradeTypeId;
                            companyNumberClassCodeHazardGradeValueVar.HazardGradeValue = subVar.HazardGradeValue;
                        }

                        // fake the client class code
                        addCCC.Add(foundClassCode.NewClientClassCode());
                    }

                    GridView1.DataSourceID = null;
                    GridView1.DataSource = addCCC;
                    GridView1.DataBind();
                }
            }
        }
        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            ExistingClassCodes.Value = string.Empty;
            foreach (GridViewRow eRow in GridView1.Rows)
            {
                Label idLabel = Common.FindControlRecursive(eRow, "txtClassCodeId") as Label;
                if (idLabel != null)
                {
                    if (!string.IsNullOrEmpty(idLabel.Text))
                        ExistingClassCodes.Value += "," + idLabel.Text;
                }

                // code ddl
                DropDownList ddlC = Common.FindControlRecursive(eRow, "ClassCodeDropDownList1") as DropDownList;
                if (ddlC != null)
                {
                    ddlC.Attributes.Add("onfocus", "storeClassClode(this);");
                    ddlC.Attributes.Add("onchange", "if(! classCodeValidate(this, event) ) return false;");
                }

                // description ddl
                DropDownList ddlD = Common.FindControlRecursive(eRow, "ClassCodeDescDropDownList1") as DropDownList;
                if (ddlD != null)
                {
                    ddlD.Attributes.Add("onfocus", "storeClassClode(this);");
                    ddlD.Attributes.Add("onchange", "if(! classCodeValidate(this, event) ) return false;");
                }
            }
            if (GridView1.FooterRow != null)
            {
                // code ddl
                DropDownList ddlC = GridView1.FooterRow.FindControl("ClassCodeDropDownList1") as DropDownList;
                if (ddlC != null)
                {
                    ScriptManager1.SetFocus(ddlC);
                    ddlC.Attributes.Add("onfocus", "storeClassClode(this);");
                    ddlC.Attributes.Add("onchange", "if(! classCodeValidate(this, event) ) return false;");
                }

                // description ddl
                DropDownList ddlD = GridView1.FooterRow.FindControl("ClassCodeDescDropDownList1") as DropDownList;
                if (ddlD != null)
                {
                    ddlD.Attributes.Add("onfocus", "storeClassClode(this);");
                    ddlD.Attributes.Add("onchange", "if(! classCodeValidate(this, event) ) return false;");
                }
            }
        }


        protected override void Render(HtmlTextWriter writer)
        {
            if (GridView1.Controls.Count > 0)
            {
                Table table = GridView1.Controls[0] as Table;
                #region Rows handling
                foreach (GridViewRow grow in GridView1.Rows)
                {
                    if (grow.RowType != DataControlRowType.DataRow)
                        continue;

                    grow.FindControl("txtSicCodeDesc").Visible = false;
                    grow.FindControl("txtNaicsCodeDesc").Visible = false;
                    TableRow row = grow as TableRow;

                    int realIndex = table.Rows.GetRowIndex(row);

                    GridViewRow newRow = new GridViewRow(realIndex, realIndex, DataControlRowType.DataRow, grow.RowState);
                    newRow.Visible = grow.Visible;

                    TableCell newCell = new TableCell();
                    //filler cell for edit/add column
                    newRow.Cells.Add(newCell);

                    newCell = new TableCell();
                    newCell.ColumnSpan = GridView1.Columns.Count - 13;
                    //newCell.CssClass = "txt";
                    newRow.Cells.Add(newCell);
                    if (grow.RowState == DataControlRowState.Normal || grow.RowState == DataControlRowState.Alternate)
                    {
                        Table footerTable = new Table();
                        footerTable.Style.Add(HtmlTextWriterStyle.BorderCollapse, "collapse");
                        footerTable.Style.Add(HtmlTextWriterStyle.Width, "100%");
                        footerTable.CssClass = "txt";
                        TableRow footerTableHeaderRow = new TableRow();
                        TableRow footerTableDetailRow = new TableRow();

                        foreach (TableCell var in grow.Cells)
                        {
                            if (!(var as DataControlFieldCell).ContainingField.Visible)
                            {

                                if ((var as DataControlFieldCell).ContainingField.HeaderText != "CodeId")
                                {
                                    //newCell.Text += (var as DataControlFieldCell).ContainingField.HeaderText + " - ";
                                    TableCell footerTableHeaderRowCell = new TableCell();
                                    footerTableHeaderRowCell.CssClass = "txt";
                                    footerTableHeaderRowCell.Text = (var as DataControlFieldCell).ContainingField.HeaderText;
                                    footerTableHeaderRow.Cells.Add(footerTableHeaderRowCell);

                                    if (var.Controls.Count > 0)
                                    {
                                        if (var.Controls[0] is Label)
                                        {
                                            //newCell.Text += (var.Controls[0] as Label).Text + " ";
                                            TableCell footerTableDetailRowCell = new TableCell();
                                            footerTableDetailRowCell.CssClass = "txt";
                                            footerTableDetailRowCell.Text = (var.Controls[0] as Label).Text;
                                            footerTableDetailRow.Cells.Add(footerTableDetailRowCell);
                                        }
                                    }
                                }
                            }
                        }
                        footerTable.Rows.Add(footerTableHeaderRow);
                        footerTable.Rows.Add(footerTableDetailRow);
                        newCell.Controls.Add(footerTable);
                    }
                    else
                    {
                        // add sic code desc and hazard values control to this row as footer

                        //newCell.Text = "&nbsp;";
                        DropDownList ddl = grow.FindControl("ClassCodeDropDownList1") as DropDownList;

                        if (ddl != null && ddl.SelectedItem != null)
                        {
                            Table footerTable = new Table();
                            footerTable.Style.Add(HtmlTextWriterStyle.BorderCollapse, "collapse");
                            footerTable.Style.Add(HtmlTextWriterStyle.Width, "100%");
                            footerTable.CssClass = "txt";
                            TableRow footerTableHeaderRow = new TableRow();
                            TableRow footerTableDetailRow = new TableRow();

                            Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
                            dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, ddl.SelectedValue);
                            Biz.ClassCode cc = dm.GetClassCode(Biz.FetchPath.ClassCode.SicCodeList,
                                Biz.FetchPath.ClassCode.CompanyNumberClassCodeHazardGradeValue,
                                Biz.FetchPath.ClassCode.CompanyNumberClassCodeHazardGradeValue.HazardGradeType);

                            foreach (Biz.CompanyNumberClassCodeHazardGradeValue var in cc.CompanyNumberClassCodeHazardGradeValues)
                            {
                                //newCell.Text += string.Format("{0} - {1} ", var.HazardGradeType.TypeName, var.HazardGradeValue);
                                TableCell footerTableHeaderRowCell = new TableCell();
                                footerTableHeaderRowCell.CssClass = "txt";
                                footerTableHeaderRowCell.Text = var.HazardGradeType.TypeName;
                                footerTableHeaderRow.Cells.Add(footerTableHeaderRowCell);

                                TableCell footerTableDetailRowCell = new TableCell();
                                footerTableDetailRowCell.CssClass = "txt";
                                footerTableDetailRowCell.Text = var.HazardGradeValue;
                                footerTableDetailRow.Cells.Add(footerTableDetailRowCell);
                            }



                            //newCell.Text += string.Format("SIC Code Desc - {0} ", cc.SicCodeList.CodeDescription);
                            TableCell footerTableHeaderRowCellsic = new TableCell();
                            footerTableHeaderRowCellsic.CssClass = "txt";
                            footerTableHeaderRowCellsic.Text = "SIC Code Desc";
                            footerTableHeaderRow.Cells.Add(footerTableHeaderRowCellsic);

                            TableCell footerTableDetailRowCellsic = new TableCell();
                            footerTableDetailRowCellsic.CssClass = "txt";
                            footerTableDetailRowCellsic.Text = cc.SicCodeList.CodeDescription;
                            footerTableDetailRow.Cells.Add(footerTableDetailRowCellsic);


                            TableCell footerTableHeaderRowCellnaics = new TableCell();
                            footerTableHeaderRowCellnaics.CssClass = "txt";
                            footerTableHeaderRowCellnaics.Text = "NAICS Code Desc";
                            footerTableHeaderRow.Cells.Add(footerTableHeaderRowCellnaics);

                            TableCell footerTableDetailRowCellnaics = new TableCell();
                            footerTableDetailRowCellnaics.CssClass = "txt";
                            footerTableDetailRowCellnaics.Text = cc.NaicsCodeList != null ? cc.NaicsCodeList.CodeDescription : "";
                            footerTableDetailRow.Cells.Add(footerTableDetailRowCellnaics);


                            footerTable.Rows.Add(footerTableHeaderRow);
                            footerTable.Rows.Add(footerTableDetailRow);
                            newCell.Controls.Add(footerTable);
                        }
                    }

                    newCell = new TableCell();
                    //filler cell for delete column
                    newRow.Cells.Add(newCell);

                    table.Controls.AddAt(realIndex + 1, newRow);
                }
                #endregion

                #region Footer handling
                if (GridView1.ShowFooter && GridView1.FooterRow != null)
                {
                    GridViewRow fgrow = GridView1.FooterRow;
                    //fgrow.FindControl("txtSicCodeDesc").Visible = false;
                    TableRow frow = fgrow as TableRow;

                    int frealIndex = table.Rows.GetRowIndex(frow);


                    GridViewRow fnewRow = new GridViewRow(frealIndex, frealIndex, fgrow.RowType, fgrow.RowState);

                    TableCell fnewCell = new TableCell();
                    //filler cell for edit/add column
                    fnewRow.Cells.Add(fnewCell);

                    fnewCell = new TableCell();
                    fnewCell.ColumnSpan = GridView1.Columns.Count - 13;
                    fnewCell.CssClass = "txt";
                    fnewRow.Cells.Add(fnewCell);

                    //Label l = new Label();
                    //l.Text = "&nbsp;" + (fgrow.FindControl("txtSicCodeDesc") as Label).Text;
                    //fnewCell.Controls.Add(l);

                    // add sic code desc and hazard values control to this row as footer

                    //fnewCell.Text = "&nbsp;";
                    DropDownList ddl = fgrow.FindControl("ClassCodeDropDownList1") as DropDownList;

                    if (ddl != null && ddl.SelectedItem != null && ddl.SelectedIndex > 0)
                    {
                        Table footerTable = new Table();
                        footerTable.Style.Add(HtmlTextWriterStyle.BorderCollapse, "collapse");
                        footerTable.Style.Add(HtmlTextWriterStyle.Width, "100%");
                        footerTable.CssClass = "txt";
                        TableRow footerTableHeaderRow = new TableRow();
                        TableRow footerTableDetailRow = new TableRow();

                        Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
                        dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.Id, ddl.SelectedValue);
                        Biz.ClassCode cc = dm.GetClassCode(Biz.FetchPath.ClassCode.SicCodeList,
                            Biz.FetchPath.ClassCode.NaicsCodeList,
                            Biz.FetchPath.ClassCode.CompanyNumberClassCodeHazardGradeValue,
                            Biz.FetchPath.ClassCode.CompanyNumberClassCodeHazardGradeValue.HazardGradeType);

                        foreach (Biz.CompanyNumberClassCodeHazardGradeValue var in cc.CompanyNumberClassCodeHazardGradeValues)
                        {
                            //newCell.Text += string.Format("{0} - {1} ", var.HazardGradeType.TypeName, var.HazardGradeValue);
                            TableCell footerTableHeaderRowCell = new TableCell();
                            footerTableHeaderRowCell.CssClass = "txt";
                            footerTableHeaderRowCell.Text = var.HazardGradeType.TypeName;
                            footerTableHeaderRow.Cells.Add(footerTableHeaderRowCell);

                            TableCell footerTableDetailRowCell = new TableCell();
                            footerTableDetailRowCell.CssClass = "txt";
                            footerTableDetailRowCell.Text = var.HazardGradeValue;
                            footerTableDetailRow.Cells.Add(footerTableDetailRowCell);
                        }



                        //newCell.Text += string.Format("SIC Code Desc - {0} ", cc.SicCodeList.CodeDescription);
                        TableCell footerTableHeaderRowCellsic = new TableCell();
                        footerTableHeaderRowCellsic.CssClass = "txt";
                        footerTableHeaderRowCellsic.Text = "SIC Code Desc";
                        footerTableHeaderRow.Cells.Add(footerTableHeaderRowCellsic);

                        TableCell footerTableDetailRowCellsic = new TableCell();
                        footerTableDetailRowCellsic.CssClass = "txt";
                        footerTableDetailRowCellsic.Text = cc.SicCodeList.CodeDescription;
                        footerTableDetailRow.Cells.Add(footerTableDetailRowCellsic);

                        TableCell footerTableHeaderRowCellnaics = new TableCell();
                        footerTableHeaderRowCellnaics.CssClass = "txt";
                        footerTableHeaderRowCellnaics.Text = "NAICS Code Desc";
                        footerTableHeaderRow.Cells.Add(footerTableHeaderRowCellnaics);

                        TableCell footerTableDetailRowCellnaics = new TableCell();
                        footerTableDetailRowCellnaics.CssClass = "txt";
                        footerTableDetailRowCellnaics.Text = cc.NaicsCodeList != null ? cc.NaicsCodeList.CodeDescription : "";
                        footerTableDetailRow.Cells.Add(footerTableDetailRowCellnaics);


                        footerTable.Rows.Add(footerTableHeaderRow);
                        footerTable.Rows.Add(footerTableDetailRow);
                        fnewCell.Controls.Add(footerTable);

                        try
                        {
                            (fgrow.FindControl("txtSicCode") as Label).Text = cc.SicCodeList.Code;
                            (fgrow.FindControl("txtNaicsCode") as Label).Text = cc.NaicsCodeList.Code;
                        }
                        catch (Exception)
                        {
                        }
                    }

                    fnewCell = new TableCell();
                    //filler cell for delete column
                    fnewRow.Cells.Add(fnewCell);

                    table.Controls.AddAt(frealIndex + 1, fnewRow);
                }
                #endregion
            }

            base.Render(writer);
        }

        protected void GridView1_Init(object sender, EventArgs e)
        {
            Biz.DataManager dm = new BCS.Biz.DataManager(BCS.WebApp.Components.DefaultValues.DSN);
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberHazardGradeType.Columns.CompanyNumberId, GetCurrentCompanyNumberId());
            Biz.CompanyNumberHazardGradeTypeCollection coll = dm.GetCompanyNumberHazardGradeTypeCollection(Biz.FetchPath.CompanyNumberHazardGradeType.HazardGradeType);

            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.CompanyNumberId, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumberCodeType.Columns.ClassCodeSpecific, true);

            Biz.CompanyNumberCodeTypeCollection codeTypes = dm.GetCompanyNumberCodeTypeCollection();

            // count of colums before for loop, so that columns in for loop may be inserted at appropriate place
            int initColCount = 5;

            // edit, update cancel command field
            CommandField cf = new CommandField();

            TemplateField tf = new TemplateField();
            tf.ItemTemplate = new Templates.CommandTemplate(DataControlRowType.DataRow, DataControlRowState.Normal, "");
            tf.EditItemTemplate = new Templates.CommandTemplate(DataControlRowType.DataRow, DataControlRowState.Edit, "");
            tf.FooterTemplate = new Templates.CommandTemplate(DataControlRowType.Footer, DataControlRowState.Insert, "");
            GridView1.Columns.Add(tf);

            // class code's id field
            tf = new TemplateField();
            tf.ItemTemplate = new Templates.GenericTemplate(
                    DataControlRowType.DataRow, string.Empty,
                    "ClassCode.Id",
                    "text", "txtClassCodeId");
            tf.HeaderText = "CodeId";
            tf.Visible = false;
            GridView1.Columns.Add(tf);

            // class code's code field
            tf = new TemplateField();
            tf.ItemTemplate = new Templates.GenericTemplate(
                    DataControlRowType.DataRow, string.Empty,
                    "ClassCode.CodeValue",
                    "text", "txtClassCodeCodeValue");
            tf.EditItemTemplate = new Templates.ClassCodeDropDownListTemplate(
                Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId), DataControlRowState.Edit, DataControlRowType.DataRow,
                "ClassCode.Id", null, "ClassCodeDropDownList1", "ClassCodeDescDropDownList1");
            tf.FooterTemplate = new Templates.ClassCodeDropDownListTemplate(GetCurrentCompanyNumberId(),
                DataControlRowState.Insert, DataControlRowType.Footer, "", "", "ClassCodeDropDownList1", "ClassCodeDescDropDownList1");
            tf.HeaderText = "CodeValue";
            GridView1.Columns.Add(tf);

            // class code's description field to aid in showing of the description in footer
            tf = new TemplateField();
            tf.ItemTemplate = new Templates.GenericTemplate(
                    DataControlRowType.DataRow, string.Empty,
                    "ClassCode.Description",
                    "text", "txtClassCodeDescription");
            tf.EditItemTemplate = new Templates.ClassCodeDropDownListTemplate(
                Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId), DataControlRowState.Edit, DataControlRowType.DataRow,
                "ClassCode.Id", "Description", "ClassCodeDescDropDownList1", "ClassCodeDropDownList1");
            tf.FooterTemplate = new Templates.ClassCodeDropDownListTemplate(
                Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId), DataControlRowState.Insert, DataControlRowType.Footer,
                "ClassCode.Id", "Description", "ClassCodeDescDropDownList1", "ClassCodeDropDownList1");
            tf.HeaderText = "Description";
            //tf.Visible = false;
            GridView1.Columns.Add(tf);

            // sic code field
            tf = new TemplateField();
            tf.ItemTemplate = new Templates.GenericTemplate(
                    DataControlRowType.DataRow, string.Empty,
                    "ClassCode.SicCodeList.Code",
                    "text", "txtSicCode");
            tf.EditItemTemplate = new Templates.GenericTemplate(
                    DataControlRowType.DataRow, string.Empty,
                    "ClassCode.SicCodeList.Code",
                    "text", "txtSicCode");
            tf.FooterTemplate = new Templates.GenericTemplate(
                    DataControlRowType.DataRow, string.Empty,
                    "ClassCode.SicCodeList.Code",
                    "text", "txtSicCode");
            tf.HeaderText = "SIC Code";
            GridView1.Columns.Add(tf);

            // sic code description field
            tf = new TemplateField();
            tf.ItemTemplate = new Templates.GenericTemplate(
                    DataControlRowType.DataRow, string.Empty,
                    "ClassCode.SicCodeList.CodeDescription",
                    "text", "txtSicCodeDesc");

            tf.Visible = false;

            tf.HeaderText = "SIC Code Desc";
            GridView1.Columns.Add(tf);

            //Naics code field
            tf = new TemplateField();
            tf.ItemTemplate = new Templates.GenericTemplate(
                    DataControlRowType.DataRow, string.Empty,
                    "ClassCode.NAICSCodeList.Code",
                    "text", "txtNaicsCode");
            tf.EditItemTemplate = new Templates.GenericTemplate(
                    DataControlRowType.DataRow, string.Empty,
                    "ClassCode.NAICSCodeList.Code",
                    "text", "txtNaicsCode");
            tf.FooterTemplate = new Templates.GenericTemplate(
                    DataControlRowType.DataRow, string.Empty,
                    "ClassCode.NAICSCodeList.Code",
                    "text", "txtNaicsCode");
            tf.HeaderText = "NAICS Code";
            GridView1.Columns.Add(tf);

            //Naics code description field
            tf = new TemplateField();
            tf.ItemTemplate = new Templates.GenericTemplate(
                DataControlRowType.DataRow, string.Empty,
                "ClassCode.NAICSCodeList.CodeDescription",
                "text", "txtNaicsCodeDesc");
            tf.HeaderText = "NAICS Code Desc";
            tf.Visible = false;
            GridView1.Columns.Add(tf);

            if (codeTypes != null)
            {
                foreach (Biz.CompanyNumberCodeType codeType in codeTypes)
                {
                    tf = new TemplateField();
                    tf.ItemTemplate = new Templates.ClassCodeCompanyNumberCodeTemplate(DataControlRowType.DataRow
                       , codeType.Id
                       , UserControls.Specific.ClassCode
                       , DataControlRowState.Normal
                       , "ClassCode.Id");
                    tf.EditItemTemplate = new Templates.ClassCodeCompanyNumberCodeTemplate(DataControlRowType.DataRow
                        , codeType.Id
                        , UserControls.Specific.ClassCode
                        , DataControlRowState.Normal
                        , "ClassCode.Id");
                    //tf.InsertItemTemplate = new Templates.ClassCodeCompanyNumberCodeTemplate(DataControlRowType.DataRow
                    //    , codeType.Id
                    //    , UserControls.Specific.ClassCode
                    //    , DataControlRowState.Insert);
                    tf.FooterTemplate = new Templates.ClassCodeCompanyNumberCodeTemplate(DataControlRowType.DataRow
                        , codeType.Id
                        , UserControls.Specific.ClassCode
                        , DataControlRowState.Normal
                        , "ClassCode.Id");
                    tf.HeaderText = codeType.CodeName;
                    GridView1.Columns.Add(tf);
                }
            }

            for (int i = 0; i < coll.Count; i++)
            {
                TemplateField atf = new TemplateField();

                Templates.GenericTemplate itemTemplate = new Templates.GenericTemplate(
                    DataControlRowType.DataRow, string.Empty,
                    "ClassCode.CompanyNumberClassCodeHazardGradeValues",
                    "text", string.Empty);
                itemTemplate.UseMethod = true;
                itemTemplate.MethodName = "FindByHazardGradeTypeId";
                itemTemplate.FinalField = "HazardGradeValue";
                itemTemplate.MethodParameters = new object[] { string.Format("{0}", coll[i].HazardGradeTypeId) };
                atf.ItemTemplate = itemTemplate;

                Templates.ScaleDropDownListTemplate editTemplate = new Templates.ScaleDropDownListTemplate(
                    DataControlRowState.Edit, DataControlRowType.DataRow,
                    "ClassCode.CompanyNumberClassCodeHazardGradeValues",
                    string.Format("ClassCode.CompanyNumberClassCodeHazardGradeValues[{0}].{1}", i, coll[i].HazardGradeTypeId));
                editTemplate.UseMethod = true;
                editTemplate.MethodName = "FindByHazardGradeTypeId";
                editTemplate.FinalField = "HazardGradeValue";
                editTemplate.MethodParameters = new object[] { string.Format("{0}", coll[i].HazardGradeTypeId) };
                //atf.EditItemTemplate = editTemplate;

                //atf.FooterTemplate = new Templates.ScaleDropDownListTemplate(DataControlRowState.Insert, DataControlRowType.Footer,
                //    "", string.Format("ClassCode.CompanyNumberClassCodeHazardGradeValues[{0}].{1}", i, coll[i].HazardGradeTypeId));

                atf.Visible = false;

                atf.HeaderText = coll[i].HazardGradeType.TypeName;
                GridView1.Columns.Insert(i + initColCount, atf);
            }

            // delete command field
            cf = new CommandField();
            cf.ShowDeleteButton = true;
            cf.ButtonType = ButtonType.Button;
            cf.ControlStyle.CssClass = "button";
            GridView1.Columns.Add(cf);
        }

        protected void GridView1_RowCancelingEdit(Object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.SelectedIndex = -1;
        }

        protected void GridView1_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            GridView1.DataBind();
        }
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            #region Canceled
            if (e.CommandName == "Cancel")
            {
                if (_operation == "add")
                {
                    // get all class codes for this company number, TODO: since we are already caching this, utilize it
                    DataManager dm = new DataManager(DefaultValues.DSN);
                    dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.CompanyNumberId, GetCurrentCompanyNumberId());
                    ClassCodeCollection allClassCodes = dm.GetClassCodeCollection(FetchPath.ClassCode.SicCodeList);

                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.CompanyNumberId, GetCurrentCompanyNumberId());
                    CompanyNumberClassCodeHazardGradeValueCollection hgValues = dm.GetCompanyNumberClassCodeHazardGradeValueCollection();

                    // get all class codes for the client yet to be added from the ViewState
                    Core.Clearance.BizObjects.ClientClassCode[] clientClassCodesArray = ClientClassCodeForAddClient;
                    List<Core.Clearance.BizObjects.ClientClassCode> clientClassCodesList = new List<BCS.Core.Clearance.BizObjects.ClientClassCode>();
                    clientClassCodesList.AddRange(clientClassCodesArray);


                    // convert them to Biz.ClientClassCodeCollection so that they follow what we are doing on edit and binds easily
                    ClientClassCodeCollection addCCC = new ClientClassCodeCollection();
                    foreach (Core.Clearance.BizObjects.ClientClassCode var in clientClassCodesList)
                    {
                        // find the class code
                        ClassCode foundClassCode = allClassCodes.FindById(var.ClassCodeId);
                        foundClassCode.SicCodeId = allClassCodes.FindById(var.ClassCodeId).SicCodeId;
                        Biz.ClientClassCode foundClientClassCode = foundClassCode.NewClientClassCode();

                        CompanyNumberClassCodeHazardGradeValueCollection hgValuesForThisClassCode = hgValues.FilterByClassCodeId(var.ClassCodeId);
                        foreach (CompanyNumberClassCodeHazardGradeValue subVar in hgValuesForThisClassCode)
                        {
                            CompanyNumberClassCodeHazardGradeValue companyNumberClassCodeHazardGradeValueVar = foundClientClassCode.ClassCode.NewCompanyNumberClassCodeHazardGradeValue();
                            companyNumberClassCodeHazardGradeValueVar.HazardGradeTypeId = subVar.HazardGradeTypeId;
                            companyNumberClassCodeHazardGradeValueVar.HazardGradeValue = subVar.HazardGradeValue;
                        }

                        // fake the client class code
                        addCCC.Add(foundClassCode.NewClientClassCode());
                    }

                    GridView1.ShowFooter = false;
                    GridView1.DataSource = addCCC;
                    GridView1.DataBind();
                }

            }
            #endregion

            #region Added
            if (e.CommandName == "New")
            {
                #region Add a new class code for an existing client
                if (_operation == "edit")
                {
                    string classCodeId = string.Empty;
                    structures.Info.ClientInfo sclient = (structures.Info.ClientInfo)Session["Client"];
                    string clientId = sclient.Client.ClientId;
                    GridViewRow row = GridView1.FooterRow;
                    //Dictionary<string, string> dict = new Dictionary<string, string>();

                    if (row == null)
                        return;

                    foreach (TableCell cell in row.Cells)
                    {
                        if (cell.Controls.Count > 0)
                        {
                            Control c = cell.Controls[0];
                            if (c is ClassCodeDropDownList)
                            {
                                classCodeId = (c as ClassCodeDropDownList).SelectedValue;
                                //break;
                            }
                            //else if (c is CompanyNumberCodeDropDownList)
                            //{
                            //    CompanyNumberCodeDropDownList ccddl = (CompanyNumberCodeDropDownList)c;
                            //    dict.Add(ccddl.CompanyNumberCodeTypeId.ToString(), ccddl.SelectedValue);
                            //}
                        }
                    }

                    if (classCodeId == "0")
                    {
                        Common.SetStatus(messagePlaceHolder, "Please select a class code.");
                        return;
                    }

                    string result = DataAccess.AddClassCodeHVOfAClient(GetCurrentCompanyNumberId(), Convert.ToInt64(clientId), Convert.ToInt32(classCodeId));

                    //foreach (KeyValuePair<string, string> entry in dict)
                    //{
                    //    if (!string.IsNullOrEmpty(entry.Value))
                    //        DataAccess.AddClientClassCodeCompanyNumberCode(Convert.ToInt64(clientId), Convert.ToInt32(classCodeId), GetCurrentCompanyNumberId(), Convert.ToInt32(entry.Value));
                    //}

                    Common.SetStatus(messagePlaceHolder, result);

                    GridView1.ShowFooter = false;
                    GridView1.DataSourceID = "ObjectDataSource1";
                    GridView1.DataBind();
                }
                #endregion
                #region Add a new class code for a new client
                else if (_operation == "add")
                {
                    // get the newly added class code
                    GridViewRow row = GridView1.FooterRow;
                    if (row == null)
                        return;

                    string classCodeId = string.Empty;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (cell.Controls.Count > 0)
                        {
                            Control c = cell.Controls[0];
                            if (c is ClassCodeDropDownList)
                            {
                                classCodeId = (c as ClassCodeDropDownList).SelectedValue;
                                break;
                            }
                        }
                    }
                    // no real value selected, return
                    if (classCodeId == "0")
                    {
                        return;
                    }

                    // get all class codes for this company number, TODO: since we are already caching this, utilize it
                    DataManager dm = new DataManager(DefaultValues.DSN);
                    dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.CompanyNumberId, GetCurrentCompanyNumberId());
                    ClassCodeCollection allClassCodes = dm.GetClassCodeCollection(FetchPath.ClassCode.SicCodeList, FetchPath.ClassCode.NaicsCodeList);

                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(Biz.JoinPath.ClassCode.Columns.CompanyNumberId, GetCurrentCompanyNumberId());
                    CompanyNumberClassCodeHazardGradeValueCollection hgValues = dm.GetCompanyNumberClassCodeHazardGradeValueCollection();


                    // get all class codes for the client yet to be added from the ViewState
                    Core.Clearance.BizObjects.ClientClassCode[] clientClassCodesArray = ClientClassCodeForAddClient;
                    List<Core.Clearance.BizObjects.ClientClassCode> clientClassCodesList = new List<BCS.Core.Clearance.BizObjects.ClientClassCode>();
                    clientClassCodesList.AddRange(clientClassCodesArray);


                    // convert them to Biz.ClientClassCodeCollection so that they follow what we are doing on edit and binds easily
                    ClientClassCodeCollection addCCC = new ClientClassCodeCollection();
                    foreach (Core.Clearance.BizObjects.ClientClassCode var in clientClassCodesList)
                    {
                        // find the class code
                        ClassCode foundClassCode = allClassCodes.FindById(var.ClassCodeId);
                        foundClassCode.SicCodeId = allClassCodes.FindById(var.ClassCodeId).SicCodeId;
                        foundClassCode.NaicsCodeId = allClassCodes.FindById(var.ClassCodeId).NaicsCodeId;
                        Biz.ClientClassCode foundClientClassCode = foundClassCode.NewClientClassCode();

                        CompanyNumberClassCodeHazardGradeValueCollection hgValuesForThisClassCode = hgValues.FilterByClassCodeId(var.ClassCodeId);
                        foreach (CompanyNumberClassCodeHazardGradeValue subVar in hgValuesForThisClassCode)
                        {
                            CompanyNumberClassCodeHazardGradeValue companyNumberClassCodeHazardGradeValueVar = foundClientClassCode.ClassCode.NewCompanyNumberClassCodeHazardGradeValue();
                            companyNumberClassCodeHazardGradeValueVar.HazardGradeTypeId = subVar.HazardGradeTypeId;
                            companyNumberClassCodeHazardGradeValueVar.HazardGradeValue = subVar.HazardGradeValue;
                        }

                        // fake the client class code
                        addCCC.Add(foundClientClassCode);
                    }

                    // get the newly created class code and add it the collection
                    ClassCode addedCC = allClassCodes.FindById(classCodeId);
                    Biz.ClientClassCode addedCCC = addedCC.NewClientClassCode();

                    addCCC.Add(addedCCC);

                    GridView1.ShowFooter = false;
                    GridView1.DataSource = addCCC;
                    GridView1.DataBind();

                    // convert them back to serializable bizobects.ClientClassCode[]
                    clientClassCodesList.Clear();
                    foreach (Biz.ClientClassCode var in addCCC)
                    {
                        Core.Clearance.BizObjects.ClientClassCode cccVar = new BCS.Core.Clearance.BizObjects.ClientClassCode();
                        cccVar.ClassCodeId = var.ClassCodeId.Value;
                        clientClassCodesList.Add(cccVar);
                    }
                    clientClassCodesArray = new BCS.Core.Clearance.BizObjects.ClientClassCode[clientClassCodesList.Count];
                    clientClassCodesList.CopyTo(clientClassCodesArray);
                    ClientClassCodeForAddClient = clientClassCodesArray;
                }
                #endregion
            }
            #endregion

            #region Edited
            if (e.CommandName == "Update")
            {
                string classCodeId = string.Empty;
                //Dictionary<string, string> dict = new Dictionary<string, string>(); 

                if (GridView1.EditIndex == -1)
                    return;

                GridViewRow row = GridView1.Rows[GridView1.EditIndex];


                string id = GridView1.SelectedDataKey.Values["Id"].ToString();


                foreach (TableCell cell in row.Cells)
                {
                    if (cell.Controls.Count > 0)
                    {
                        Control c = cell.Controls[0];
                        if (c is ClassCodeDropDownList)
                        {
                            classCodeId = (c as ClassCodeDropDownList).SelectedValue;
                            //break;
                        }
                        //else if (c is CompanyNumberCodeDropDownList)
                        //{
                        //    CompanyNumberCodeDropDownList cnddl = (CompanyNumberCodeDropDownList)c;
                        //    dict.Add(cnddl.CompanyNumberCodeTypeId.ToString(), cnddl.SelectedValue);
                        //}
                    }
                }

                if (classCodeId == "0")
                {
                    Common.SetStatus(messagePlaceHolder, "Please select a class code.");
                    return;
                }

                //foreach (KeyValuePair<string,string> entry in dict)
                //{
                //    if (!string.IsNullOrEmpty(entry.Value.ToString()))
                //        DataAccess.UpdateClientClassCodeCompanyNumberCode(Convert.ToInt32(id), Int32.Parse(entry.Key.ToString()), Int32.Parse(entry.Value.ToString()));
                //    else
                //        DataAccess.DeleteClientClassCodeCompanyNumberCode(Convert.ToInt32(id), Int32.Parse(entry.Key.ToString()));
                //}

                string result = DataAccess.UpdateClassCodeHVOfAClient(Convert.ToInt32(id), Convert.ToInt32(classCodeId));


                Common.SetStatus(messagePlaceHolder, result);

                GridView1.ShowFooter = false;
                GridView1.DataSourceID = "ObjectDataSource1";
                GridView1.DataBind();

                GridView1.SelectedIndex = -1;
                GridView1.EditIndex = -1;
            }
            #endregion
        }
        protected void GridView1_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            e.ExceptionHandled = true;

            GridView1.DataBind();
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState != DataControlRowState.Edit)
            {
                if (e.Row.Cells[e.Row.Cells.Count - 1].Controls.Count > 0)
                {
                    Button btnDelete = (e.Row.Cells[e.Row.Cells.Count - 1].Controls[0] as Button);
                    btnDelete.Enabled = !BCS.WebApp.Components.Security.IsUserReadOnly() && _operation == "edit";
                    btnDelete.Attributes.Add("onclick", "var v = confirm('Are you sure you want to delete this class code?'); if (v == false) { return false;}");
                }
                if (e.Row.Cells[0].Controls.Count > 0)
                {
                    Button btnEdit = (e.Row.Cells[0].Controls[0] as Button);
                    btnEdit.Enabled = !BCS.WebApp.Components.Security.IsUserReadOnly() && _operation == "edit";
                }
            }
            if (e.Row.RowState.ToString().Contains("Edit") || e.Row.RowType == DataControlRowType.Footer)
            {
                (e.Row.FindControl("ClassCodeDropDownList1") as WebControl).Style.Add(HtmlTextWriterStyle.Width, "90px");
            }
        }
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.ShowFooter = false;
            GridView1.EditIndex = e.NewEditIndex;
            GridView1.SelectedIndex = e.NewEditIndex;
        }


        private int GetCurrentCompanyNumberId()
        {
            return Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
        }

        private void BindClassCodes(BCS.Core.Clearance.BizObjects.ClearanceClient clearanceclient, int pageIndex)
        {
            Biz.DataManager dm = new DataManager(DefaultValues.DSN);
            dm.QueryCriteria.And(JoinPath.ClientClassCode.Columns.ClientId, clearanceclient.Id);
            ClientClassCodeCollection clientClassCodes = dm.GetClientClassCodeCollection(
                FetchPath.ClientClassCode.ClassCode,
                FetchPath.ClientClassCode.ClassCode.SicCodeList,
                FetchPath.ClientClassCode.ClassCode.NaicsCodeList,
                FetchPath.ClientClassCode.ClassCode.CompanyNumberClassCodeHazardGradeValue);

            GridView1.DataSource = clientClassCodes;
            GridView1.DataBind();
        }

        protected void ObjectDataSource1_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null && e.ReturnValue != null)
            {
                Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
            }
        }

        protected Core.Clearance.BizObjects.ClientClassCode[] ClientClassCodeForAddClient
        {
            get
            {
                if (ViewState["ClientClassCodeForAddClient"] != null)
                {
                    return (Core.Clearance.BizObjects.ClientClassCode[])ViewState["ClientClassCodeForAddClient"];
                }
                else
                {
                    return new Core.Clearance.BizObjects.ClientClassCode[0];
                }
            }
            set
            {
                ViewState["ClientClassCodeForAddClient"] = value;
            }
        }
        protected void loadTargetClientButton_Click(object sender, EventArgs e)
        {
            string id = targetClientIdTextBox.Text;
            string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
            ClientProxy.Client clientproxy = Common.GetClientProxy();
            string clientxml = clientproxy.GetClientById(companyNumber, id);

            Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                (Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                clientxml, typeof(Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

            structures.Info.ClientInfo client = clearanceclient.ClientCoreClient;

            // load names for merge feature
            cblNames1.Items.Clear();
            if (client.Client.Names != null)
            {
                foreach (structures.Info.Name name in client.Client.Names)
                {
                    StringBuilder sb = new StringBuilder();
                    if (name.NameBusiness != null && name.NameBusiness.Length > 0)
                        sb.Append(name.NameBusiness);
                    else
                        sb.AppendFormat("{0} {1} {2}", name.NameFirst, name.NameMiddle, name.NameLast);

                    cblNames1.Items.Add(new ListItem(sb.ToString(), name.SequenceNumber));
                }
            }

            // load addresses for merge feature
            cblAddresses1.Items.Clear();
            if (client.Addresses != null)
            {
                foreach (structures.Info.Address addr in client.Addresses)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat("{0} {1} {2} {3} {4} {5}", addr.HouseNumber, addr.Address1, addr.Address2,
                        addr.City, addr.StateProvinceId, addr.PostalCode);

                    cblAddresses1.Items.Add(new ListItem(sb.ToString(), addr.AddressId));
                }
            }
            targetPanel.Visible = true;
            programmaticModalPopup.Show();
        }
        protected void MergeImage_Init(object sender, EventArgs e)
        {
            (sender as Control).Visible = DefaultValues.IsBCSAsClientCore(Common.GetSafeIntFromSession(SessionKeys.CompanyId)) && BCS.WebApp.Components.Security.CanMerge() && Request.QueryString["op"] == "edit";
        }
        protected void Merge(object sender, EventArgs e)
        {
            if (cblAddresses1.Items.Count == 0 && cblNames1.Items.Count == 0)
            {
                // some reason client requested is not available, just return
                return;
            }
            // update client system info
            string[] names = Common.GetSelectedValues(cblNames);
            string[] addrs = Common.GetSelectedValues(cblAddresses);

            ClientSystemProxy.Service s = new ClientSystemProxy.Service();
            s.Url = ConfigValues.GetCurrentCompany(Common.GetSafeIntFromSession(SessionKeys.CompanyId)).ClientCoreEndPointUrl;
            string result = s.mergeClients(sourceClientIdLabel.Text, targetClientIdTextBox.Text, string.Join("|", names), string.Join("|", addrs));

            // update bcs info
            BCS.Biz.DataManager dm = new DataManager(DefaultValues.DSN);
            dm.QueryCriteria.And(JoinPath.Submission.Columns.CompanyNumberId, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
            dm.QueryCriteria.And(JoinPath.Submission.Columns.ClientCoreClientId, sourceClientIdLabel.Text);
            SubmissionCollection bcsSubmissions = dm.GetSubmissionCollection(BCS.Biz.FetchPath.Submission.SubmissionClientCoreClientAddress,
                BCS.Biz.FetchPath.Submission.SubmissionClientCoreClientName);

            // delete old associations
            foreach (BCS.Biz.Submission submission in bcsSubmissions)
            {
                int le = submission.SubmissionClientCoreClientAddresss.Count;

                for (int i = 0; i < le; i++)
                {
                    submission.SubmissionClientCoreClientAddresss[(le - i) - 1].Delete();
                }
                le = submission.SubmissionClientCoreClientNames.Count;

                for (int i = 0; i < le; i++)
                {
                    submission.SubmissionClientCoreClientNames[(le - i) - 1].Delete();
                }
            }

            dm.CommitAll();

            // add new associations
            // get updated good client's names and addrs
            string id = targetClientIdTextBox.Text;
            string companyNumber = Common.GetSafeStringFromSession(SessionKeys.CompanyNumber);
            ClientProxy.Client clientproxy = Common.GetClientProxy();
            string clientxml = clientproxy.GetClientById(companyNumber, id);

            Core.Clearance.BizObjects.ClearanceClient clearanceclient =
                (Core.Clearance.BizObjects.ClearanceClient)BCS.Core.XML.Deserializer.Deserialize(
                clientxml, typeof(Core.Clearance.BizObjects.ClearanceClient), "seq_nbr");

            structures.Info.ClientInfo client = clearanceclient.ClientCoreClient;

            foreach (BCS.Biz.Submission var in bcsSubmissions)
            {
                if (client.Client.Names != null)
                {
                    foreach (structures.Info.Name nName in client.Client.Names)
                    {
                        BCS.Biz.SubmissionClientCoreClientName bcsName = var.NewSubmissionClientCoreClientName();
                        bcsName.ClientCoreNameId = Convert.ToInt32(nName.SequenceNumber); ;
                    }
                }
                foreach (structures.Info.Address nAddr in client.Addresses)
                {
                    BCS.Biz.SubmissionClientCoreClientAddress bcsName = var.NewSubmissionClientCoreClientAddress();
                    bcsName.ClientCoreClientAddressId = Convert.ToInt32(nAddr.AddressId);
                }
            }

            foreach (BCS.Biz.Submission var in bcsSubmissions)
            {
                var.ClientCoreClientId = Convert.ToInt64(targetClientIdTextBox.Text);
            }
            dm.CommitAll();

            #region update name/address type

            // bad client not in clearance, do nothing, since it will not have name or address types
            dm.QueryCriteria.Clear();
            dm.QueryCriteria.And(JoinPath.Client.Columns.CompanyNumberId, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
            dm.QueryCriteria.And(JoinPath.Client.Columns.ClientCoreClientId, sourceClientIdLabel.Text);
            BCS.Biz.Client badClient = dm.GetClient(FetchPath.Client.ClientCoreAddressAddressType, FetchPath.Client.ClientCoreNameClientNameType);
            if (badClient == null)
            { }
            else
            {
                dm.QueryCriteria.Clear();
                dm.QueryCriteria.And(JoinPath.Client.Columns.CompanyNumberId, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                dm.QueryCriteria.And(JoinPath.Client.Columns.ClientCoreClientId, targetClientIdTextBox.Text);
                BCS.Biz.Client goodClient = dm.GetClient(FetchPath.Client.ClientCoreAddressAddressType, FetchPath.Client.ClientCoreNameClientNameType);
                if (goodClient == null) // not in clearance yet.
                {
                    dm.QueryCriteria.Clear();
                    dm.QueryCriteria.And(JoinPath.CompanyNumber.Columns.Id, Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId));
                    CompanyNumber cnum = dm.GetCompanyNumber();

                    goodClient = dm.NewClient(Convert.ToInt64(targetClientIdTextBox.Text), cnum);
                    goodClient.EntryBy = User.Identity.Name;
                    dm.CommitAll();
                }

                #region name ids are actually sequence numbers (OrderIndex), so need to be handled differently.
                string[] toNames = Common.GetValues(cblNames1);
                int[] nameInts = Array.ConvertAll(toNames, new Converter<string, int>(Convert.ToInt32));
                Array.Sort(nameInts);
                Array.Reverse(nameInts);

                for (int i = 0; i < badClient.ClientCoreNameClientNameTypes.Count; i++)
                {
                    int nameId = badClient.ClientCoreNameClientNameTypes[i].ClientCoreNameId;
                    if (Array.IndexOf(names, nameId.ToString()) < 0)
                        continue;

                    ClientCoreNameClientNameType newClientCoreNameClientNameType = goodClient.NewClientCoreNameClientNameType();

                    // since client system services orders by adding to the maximum order index before merge and name ints are sorted
                    nameId = nameId + nameInts[0];

                    newClientCoreNameClientNameType.ClientCoreNameId = nameId;
                    newClientCoreNameClientNameType.ClientNameTypeId = badClient.ClientCoreNameClientNameTypes[i].ClientNameTypeId;
                }
                #endregion

                #region addresses
                for (int i = 0; i < badClient.ClientCoreAddressAddressTypes.Count; i++)
                {
                    int addressId = badClient.ClientCoreAddressAddressTypes[i].AddressTypeId;
                    if (Array.IndexOf(addrs, addressId.ToString()) < 0)
                        continue;
                    ClientCoreAddressAddressType newClientCoreAddressAddressType = goodClient.NewClientCoreAddressAddressType();

                    newClientCoreAddressAddressType.ClientCoreAddressId = badClient.ClientCoreAddressAddressTypes[i].ClientCoreAddressId;
                    newClientCoreAddressAddressType.AddressTypeId = addressId;
                }
                #endregion

                dm.CommitAll();
            }

            #endregion

            Session["StatusMessage"] = result;
            Session["ClientId"] = targetClientIdTextBox.Text;
            Response.Redirect("Client.aspx?op=edit");
        }
        protected void MergeCancel(object sender, EventArgs e)
        {
            targetPanel.Visible = false;
            targetClientIdTextBox.Text = string.Empty;
            cblAddresses1.Items.Clear();
            cblNames1.Items.Clear();
        }

        protected void RequestClientDelete(object sender, EventArgs e)
        {
            long clientId = Int64.Parse((string)Session["ClientId"]);
            int companyId = Common.GetSafeIntFromSession(SessionKeys.CompanyId);
            int companyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);

            using (BCS.DAL.BCSContext bc = BCS.DAL.DataAccess.NewBCSContext())
            {
                if (bc.Submission.Where(s => s.ClientCoreClientId == clientId && s.CompanyNumberId == companyNumberId && !s.Deleted).Count() > 0)
                {
                    lblDeleteMsg.Text = "<br /><br /><span style='font-weight:bold;color:#ff0000;'>This client is associated with one or more submissions and cannot be deleted.</span>";
                    lblDeleteMsg.Visible = true;
                    return;
                }

                BCS.DAL.Client client = bc.Client.Where(c => c.ClientCoreClientId == clientId && c.CompanyNumberId == companyNumberId).SingleOrDefault();

                if (client == null)
                {
                    //add the client
                    client = new DAL.Client();
                    client.CompanyNumberId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);
                    client.SicCodeId = null;
                    client.ClientCoreClientId = clientId;
                    client.EntryBy = Common.GetUser();
                    client.EntryDt = DateTime.Now;
                    client.RequestToDelete = true;
                    bc.Client.AddObject(client);
                    bc.SaveChanges();


                    //Trigger an update so it saves the client to the database. 
                    //Session["deleterequest"] = true;
                    //btnSaveNew_Click(btnSaveNew, null);
                    //Session.Remove("deleterequest");
                    //client = bc.Client.Where(c => c.ClientCoreClientId == clientId && c.CompanyNumberId == companyNumberId).SingleOrDefault();
                }

                if (client != null)
                {
                    client.RequestToDelete = true;
                    bc.SaveChanges();
                }

                Email.SendRequestToDeleteClientId(clientId, bc.Company.Where(c => c.Id == companyId).Select(c => c.CompanyName).SingleOrDefault());

                CheckForDeletedClient();
            }
        }

        protected void CheckForDeletedClient()
        {
            if (_operation.ToLower() == "add" || _operation.ToLower() == "addphone")
            {
                btnRequestDelete.Visible = false;
                return;
            }
            long clientId = Int64.Parse((string)Session["ClientId"]);
            int companyId = Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId);

            bool deleted = false;

            using (BCS.DAL.BCSContext bc = BCS.DAL.DataAccess.NewBCSContext())
            {
                deleted = bc.Client.Where(c => c.ClientCoreClientId == clientId && c.CompanyNumberId == companyId).Select(c => c.RequestToDelete).SingleOrDefault();
            }

            if (deleted)
            {
                btnSaveNew.Visible = btnSaveNew.Enabled = false;
                Button2.Visible = Button2.Enabled = false;
                btnRequestDelete.Visible = btnRequestDelete.Enabled = false;
                lblDeleteMsg.Text = "<span style='font-weight:bold;color:#ff0000;'>A Delete Request has been submitted for this client. <br />(Note - the delete request may take serveral business days to be completed.)</span>";
                lblDeleteMsg.Visible = true;
            }

        }


        public static string GetSicCodeByNaicsCode(int naicsCodeId)
        {
            string siccode = "";
            using (BCS.DAL.BCSContext bc = BCS.DAL.DataAccess.NewBCSContext())
            {
                var sic = bc.NaicsCodeList.Include("SicCodeList").Where(c => c.Id == naicsCodeId).FirstOrDefault();
                siccode = sic.SicCodeList.Id + "|" + sic.SicCodeList.Code + "|" + sic.SicCodeList.CodeDescription;
            }

            return siccode;
        }

        public string CheckForNameFormating(string nameToFormat)
        {
            string nameToModify = nameToFormat;
            string[] nameArray = nameToModify.Split(' ');

            for (int i = 0; i < nameArray.Length - 1; i++)
            {
                string currName = nameArray[i];
                string nextName = nameArray[i + 1];
                string newName = "";
                if (currName.Length == 1)
                {
                    if ((currName.ToUpper() == "O") && (nextName.Length > 1))
                    {
                        newName = currName + "'" + nextName;
                        nameToModify = nameToModify.Replace(currName + ' ' + nextName, newName);
                    }
                }
            }

            nameArray = nameToModify.Split(' ');
            for (int i = 0; i < nameArray.Length - 1; i++)
            {
                string currName = nameArray[i];
                string nextName = nameArray[i + 1];
                string newName = "";
                if (nextName.Length == 1)
                {
                    if ((nextName.ToUpper() == "S") && (currName.Length > 1))
                    {
                        newName = currName + "'" + nextName.ToLower();
                        nameToModify = nameToModify.Replace(currName + ' ' + nextName, newName);
                    }
                }
            }
            return nameToModify;
        }

    }
}
