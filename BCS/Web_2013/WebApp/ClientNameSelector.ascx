<%@ Control Language="C#" AutoEventWireup="true" CompilationMode="always" Inherits="BCS.WebApp.UserControls.UserControls_ClientNameSelector" CodeFile="~/ClientNameSelector.ascx.cs" %>
<%@ Register TagPrefix="UserControls" Namespace="BCS.WebApp.UserControls" %>
<div>
    <asp:HiddenField ID="hfPrimaryFlagEditable" runat="server" />
    <table>
        <tr>
            <td style='width: 52px; display: <%= (PrimaryFlagEditable) ? "" : "none" %>; text-align: center'>
                <UserControls:GroupedRadioButton ID="rdoPrimaryName" runat="server" GroupName="PrimaryNameSelect" />
            </td>
            <td>
                <table cellpadding="5" cellspacing="0" style="display: inline-block">
                    <tr>
                        <td id="Td1" nowrap="nowrap"
                            visible='<%$ Code: BCS.WebApp.Components.DefaultValues.SupportsIndividualClients(Common.GetSafeIntFromSession("CompanySelected")) %>'
                            runat="server">Select:<br />
                            <asp:RadioButton ID="rbBusiness" runat="server" Checked="true" OnCheckedChanged="rbBusiness_CheckedChanged"
                                AutoPostBack="True" GroupName="BorP" />Business
                <br />
                            <asp:RadioButton ID="rbPersonal" runat="server" AutoPostBack="True" OnCheckedChanged="rbPersonal_CheckedChanged"
                                GroupName="BorP" />Person
            
                        </td>
                        <td>
                            <asp:Table ID="ClientData1" runat="server" CellPadding="2" CellSpacing="0">
                                <asp:TableRow>
                                    <asp:TableCell>Business Name:</asp:TableCell>
                                    <asp:TableCell>City:</asp:TableCell>
                                    <asp:TableCell>State:</asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txBusinessName" runat="server" Width="260px" AccessKey="B"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txCity" runat="server" Width="260px"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <UserControls:StateDropDownList ID="StateDropDownList1" runat="server"></UserControls:StateDropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow Visible="false">
                                    <asp:TableCell VerticalAlign="top">
                                        <asp:Table ID="ClientData2" runat="server" CellPadding="2" CellSpacing="0">
                                            <asp:TableRow>
                                                <asp:TableCell>Prefix:</asp:TableCell>
                                                <asp:TableCell>First:</asp:TableCell>
                                                <asp:TableCell>Middle:</asp:TableCell>
                                                <asp:TableCell>Last:</asp:TableCell>
                                                <asp:TableCell>Suffix:</asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txPrefix" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txFirst" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txMiddle" runat="server" Width="50px"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txLast" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:TextBox ID="txSuffix" runat="server"></asp:TextBox>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <asp:HiddenField ID="hiddenNameSequenceOrId" runat="server" />
                            <asp:HiddenField ID="hiddenNameType" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <UserControls:ClientNameTypeRadioButtonList ID="rblClientNameType" runat="server" RepeatDirection="Horizontal">
                            </UserControls:ClientNameTypeRadioButtonList>
                            <asp:RequiredFieldValidator ID="rfvAddressType" runat="server" ErrorMessage="&nbsp;" ControlToValidate="rblClientNameType" SetFocusOnError="True"
                                Display="Dynamic" OnInit="rfvAddressType_Init">* Name Type</asp:RequiredFieldValidator>
                        </td>
                        <td align="right">
                            <% if (!PrimaryFlagEditable && Common.GetSafeIntFromSession(SessionKeys.CompanyNumberId) != 25)
                               { %>
                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("save.gif") %>' Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() %>' />
                            <% } %>
                            <asp:ImageButton ID="btnRemove" runat="server" ImageUrl='<%$ Code : Common.GetImageUrl("cancelbig.gif") %>'
                                OnClick="btnRemove_Click" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

