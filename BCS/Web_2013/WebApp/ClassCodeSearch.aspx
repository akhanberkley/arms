﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClassCodeSearch.aspx.cs" Inherits="ClassCodeSearch" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Berkley Clearance System - Class Code Search</title>
    <script src="Javascripts/jquery-1.4.4.js" type="text/javascript"></script>
    <style>
        body
        {
            margin: 0px;
            font-family: Tahoma, Arial, Sans-Serif;
            font-size: 11px;
        }
        table
        {
            border-collapse: collapse;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <ajx:ToolkitScriptManager ID="ScriptManager1" runat="server" CombineScripts="True">
            <Scripts>
                <aje:ScriptReference Path='<%$ Code: string.Concat(DefaultValues.ScriptPath, "WebUIValidationExtended.js") %>' />
            </Scripts>
            <Services>
                <aje:ServiceReference Path="WebServices/SubmissionService.asmx" />
            </Services>
        </ajx:ToolkitScriptManager>
        <script type="text/javascript">
            function ClearText() {
                var txtBox = document.getElementById('txtClassCode');
                if (txtBox.value == 'Enter Code or Description') {
                    txtBox.value = '';
                }
            }

            function ClassCodeSearch() {
                $("#divSearchingImage").css("display", "");
                $("#divClassCodeResults").html('');
                BCS.WebApp.ScriptServices.SubmissionService.SearchClassCodes($("#txtClassCode").val(), function (result, eventArgs) {
                    $("#divSearchingImage").css("display", "none");
                    $("#divClassCodeResults").html(result);
                });
            }
        </script>

        <div style="padding: 10px;">
            <table border="0" style="width: 100%">
                <tr>
                    <td style="text-align: center;">
                        <asp:Panel ID="pnlTitle" runat="server" DefaultButton="lnkClassCodeAttributeSearch">
                            <div style="font-size: 14px; color: #898989; padding: 5px 0px 10px 0px;">
                                Class Code Attribute Search
                            </div>
                            <input type="text" maxlength="200" id="txtClassCode" onclick="ClearText()" onchange="ClearText()" onfocus="ClearText()" value="Enter Code or Description" />
                            &nbsp;&nbsp;
                        <asp:LinkButton runat="server" ID="lnkClassCodeAttributeSearch" OnClientClick="javascript:ClassCodeSearch();return false;" Text="Search"></asp:LinkButton>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 15px;">
                        <div id="divSearchingImage" style="display: none;">
                            <img id="imgSearching" runat="server" src='<%$ Code : Common.GetImageUrl("indicator.gif") %>' alt="!!" title="Please wait." />
                            Searching. Please Wait.
                        </div>
                        <div id="divClassCodeResults">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div style="clear: both"></div>
    </form>
</body>
</html>
