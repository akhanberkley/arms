using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.Components;

public partial class UWU : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Grid1.SelectedIndex = -1;
        DetailsView1.ChangeMode(DetailsViewMode.Insert);
    }
    protected void Grid1_PageIndexChanged(object sender, EventArgs e)
    {
        Grid1.SelectedIndex = -1;
    }
    protected void Grid1_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        Grid1.DataBind();
        DetailsView1.DataBind();
    }
    protected void Grid1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        if (!BCS.WebApp.Components.Security.IsUserReadOnly() && !ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId)))
            DetailsView1.ChangeMode(DetailsViewMode.Edit);
    }
    protected void ObjectDataSource1_Deleted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        Grid1.DataBind();
        DetailsView1.DataBind();
    }
    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        Grid1.DataBind();
        DetailsView1.DataBind();
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        bool b = Common.ShouldUpdate(e);

        e.Cancel = b;

        if (b)
        {
            // there was nothing to update. Just show the user the success message.
            Common.SetStatus(messagePlaceHolder, "Company Number line of business Sucessfully updated.");
        }
    }
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        e.Cancel = BCS.WebApp.Components.Security.IsUserReadOnly() || ConfigValues.UsesAPS(Common.GetSafeIntFromSession(SessionKeys.CompanyId));
    }
    protected void ObjectDataSource2_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }

    }
    protected void ObjectDataSource2_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        if (e.Exception == null && e.ReturnValue != null)
        {
            Common.SetStatus(messagePlaceHolder, e.ReturnValue.ToString());
        }
    }
}
