<%@ Page Language="C#" MasterPageFile="~/Site.PATS.master" AutoEventWireup="true" CodeFile="CompanyNoLOBs.aspx.cs" Inherits="CompanyNoLOBs" Title="Berkley Clearance System - Company Underwriting Units" Theme="DefaultTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:PlaceHolder runat="server" ID="messagePlaceHolder">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
    <br />
</asp:PlaceHolder>
   <table cellpadding="0" cellspacing="0" class="box">
    <tbody>
    <tr>
        <td class="TL"><div /></td>
        <td class="TC">Company Underwriting Units</td>
        <td class="TR"><div /></td>
    </tr>
    <tr>
        <td class="ML"><div /></td>
        <td class="MC">
        
    <table id="atable" runat="server">
        <tr>
            <td>
                Company Number UW Units</td>
            <td></td>
            <td>All UW Units</td>
        </tr>
        <tr>
            <td>
                <asp:ListBox ID="ListBox1" runat="server" DataSourceID="odsCompanyNumberLOBs" DataTextField="Code"
                    DataValueField="Id" SelectionMode="Multiple" Width="250px"></asp:ListBox><asp:ObjectDataSource
                        ID="odsCompanyNumberLOBs" runat="server" SelectMethod="GetLineOfBusinessesByCompanyNumberId"
                        TypeName="BCS.Core.Clearance.Admin.DataAccess">
                        <SelectParameters>
                            <asp:SessionParameter Name="companyNumberId" SessionField="CompanyNumberSelected"
                                Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
            </td>
             <td>
                <table cellspacing="4">
                    <tr>
                        <td>
                            <asp:Button ID="btnRemove" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() %>' Text=">" OnClick="btnRemove_Click" />
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() %>' Text="<" OnClick="btnAdd_Click" />
                        </td>
                    </tr>
                </table>
             </td>            
             <td>
                 <asp:ListBox ID="ListBox2" runat="server" DataSourceID="odsLOBs" DataTextField="Code"
                     DataValueField="Id" SelectionMode="Multiple" Width="250px"></asp:ListBox><asp:ObjectDataSource
                         ID="odsLOBs" runat="server" SelectMethod="GetLineOfBusinessesNotByCompanyNumberId" TypeName="BCS.Core.Clearance.Admin.DataAccess">
                         <SelectParameters>
                             <asp:SessionParameter DefaultValue="" Name="companyNumberId" SessionField="CompanyNumberSelected"
                                 Type="Int32" />
                         </SelectParameters>
                     </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:Button ID="btnSave" runat="server" Enabled='<%$ Code: !BCS.WebApp.Components.Security.IsUserReadOnly() %>' OnClick="btnSave_Click" Text="Update" />
                <asp:Button ID="btnCancel" runat="server"  OnClick="btnSave_Click" Text="Cancel" Visible="False" />
            </td>
        </tr>
    </table>
    
    </td>
        <td class="MR"><div /></td>
    </tr>
    <tr>
        <td class="BL"><div/></td>
        <td class="BC"><div/></td>
        <td class="BR"><div/></td>
    </tr>
    </tbody>
</table>
</asp:Content>

