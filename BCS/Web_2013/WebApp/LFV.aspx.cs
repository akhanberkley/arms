using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class LFV : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void EntriesGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItem != null)
        {
            #region set styles
            if ((e.Row.DataItem as DataRowView)["Level"].ToString() == "FATAL")
            {
                (e.Row as TableRow).Style.Add(HtmlTextWriterStyle.Color, System.Drawing.Color.Red.Name);
            }
            if ((e.Row.DataItem as DataRowView)["Level"].ToString() == "ERROR")
            {
                (e.Row as TableRow).Style.Add(HtmlTextWriterStyle.BackgroundColor, System.Drawing.Color.Tan.Name);
            }
            if ((e.Row.DataItem as DataRowView)["Level"].ToString() == "WARN")
            {
                (e.Row as TableRow).Style.Add(HtmlTextWriterStyle.BackgroundColor, System.Drawing.Color.DarkKhaki.Name);
            }
            if ((e.Row.DataItem as DataRowView)["Level"].ToString() == "INFO")
            {
                (e.Row as TableRow).Style.Add(HtmlTextWriterStyle.BackgroundColor, System.Drawing.Color.LightGreen.Name);
            } 
            #endregion

            Label l = e.Row.FindControl("lblMessage") as Label;
            l.Text = LimitMessage(e.Row.DataItem);

            Button b = e.Row.FindControl("btnFullDetails") as Button;
            b.Attributes.Add("onclick", "openDetailWindow('" + ((DataRowView)e.Row.DataItem).Row.ItemArray[0].ToString() + "')");
        }
    }
    protected string LimitMessage(object o)
    {
        DataRowView drv = o as DataRowView;
        if (drv == null)
            return string.Empty;

        if (drv["Message"] == null)
            return string.Empty;

        if (string.IsNullOrEmpty(drv["Message"].ToString()))
            return string.Empty;

        if (drv["Message"].ToString().Length <= 255)
            return drv["Message"].ToString();

        return drv["Message"].ToString().Substring(0, 255);
    }
    protected void EntriesSource_Selected(object sender, SqlDataSourceStatusEventArgs e)
    {
        //string prevSel = PageDDL.SelectedValue;
        //object totalRows = e.Command.Parameters["@totalCount"].Value;
        //if (totalRows != null && totalRows != DBNull.Value)
        //{
        //    int total = Convert.ToInt32(totalRows);
        //    if (total>1)
        //        PageDDL.Items.Clear();
        //    int totalPages = (total / 10) + 1;
        //    for (int i = 1; i < totalPages; i++)
        //    {
        //        PageDDL.Items.Add(new ListItem(i.ToString()));
        //    }
        //}
        //if (PageDDL.Items.FindByValue(prevSel) != null)
        //    PageDDL.Items.FindByValue(prevSel).Selected = true;
    }
}
