#region Using
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BCS.WebApp.UserControls; 
#endregion

public partial class UserControls_ClientName : System.Web.UI.UserControl
{
    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (NameType == "Insured")
        {
            txNameFirst.Visible = true;
            lbNameFirst.Visible = true;

            lbNameLast.Text = "Last Name";
        }
    } 
    #endregion

    #region Control Events
    protected void ClientNameTypeRadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClientNameTypeRadioButtonList rbl = sender as ClientNameTypeRadioButtonList;
        if (rbl.SelectedItem.Text == "DBA")
        {
            txNameFirst.Visible = false;
            lbNameFirst.Visible = false;

            lbNameLast.Text = "Name";
        }
        else
        {
            txNameFirst.Visible = true;
            lbNameFirst.Visible = true;

            lbNameLast.Text = "Last Name";
        }

    } 
    #endregion

    #region Properties
    public string NameBusiness
    {
        get
        {
            if (this.ClientNameTypeRadioButtonList1.SelectedItem.Text != "DBA")
                return null;
            return this.txNameLast.Text;
        }
    }

    public string NameFirst
    {
        get { return this.txNameFirst.Text; }
    }

    public string NameLast
    {
        get { return this.txNameLast.Text; }
    }

    public string NameType
    {
        get { return this.ClientNameTypeRadioButtonList1.SelectedItem.Text; }
    } 
    #endregion
    
}