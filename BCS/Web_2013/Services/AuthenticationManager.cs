using System;
using System.Xml;
using System.Security.Permissions;

using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;

using BCS.Biz;
using BCS.Core;
using BCS.WebApp.Services.Components;

namespace BCS.WebApp.Services
{
	/// <summary>
	/// By implementing UsernameTokenManager we can verify the signature
	/// on messages received.
	/// </summary>
	/// <remarks>
	/// This class includes this demand to ensure that any untrusted
	/// assemblies cannot invoke this code. This helps mitigate
	/// brute-force discovery attacks.
	/// </remarks>
	[SecurityPermissionAttribute(SecurityAction.Demand, Flags=SecurityPermissionFlag.UnmanagedCode)]
	public class AuthenticationManager : UsernameTokenManager
	{
		/// <summary>
		/// Constructs an instance of this security token manager.
		/// </summary>
		public AuthenticationManager()
		{}

		/// <summary>
		/// Constructs an instance of this security token manager.
		/// </summary>
		/// <param name="nodes">An XmlNodeList containing XML elements from a configuration file.</param>
		public AuthenticationManager(XmlNodeList nodes) 	: base(nodes)
		{}

		/// <summary>
		/// Returns the password or password equivalent for the username provided.
		/// </summary>
		/// <param name="token">The username token</param>
		/// <returns>The password (or password equivalent) for the username</returns>
		protected override string AuthenticateToken( UsernameToken token )
		{
			// Ensure the SOAP message contained a UsernameToken.
			if (token == null)
				throw new ArgumentNullException();

			System.Diagnostics.Debug.WriteLine("AuthenticateToken by: " + token.Username);

//			DataManager dataManager = new DataManager(DefaultValues.DSN);
//			Core.Security.CustomPrincipal principal = new BCS.Core.Security.CustomPrincipal(token.Principal.Identity,
//				Components.Security.GetUserRoles(token.Principal.Identity.Name));
//
//			// Set the context
//			System.Web.HttpContext.Current.User = principal;

			// This is a very simple provider.
			// In most production systems the following code 
			// typically consults an external database to obtain the password or
			// password equivalent for a given user name.

			byte[] password = System.Text.Encoding.UTF8.GetBytes(token.Username);
			Array.Reverse(password);

			return Convert.ToBase64String(password);
		}
	}
}
