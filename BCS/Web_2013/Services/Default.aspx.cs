using System;
using System.Collections.Specialized;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.XPath;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Security.Tokens;

using structures = BTS.ClientCore.Wrapper.Structures;


using BCS.WebApp.Services;

/// <summary>
/// just a test page.
/// </summary>
public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
}
