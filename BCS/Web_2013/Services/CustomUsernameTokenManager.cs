using System;
using System.Xml;
using System.Security.Permissions;

using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;

namespace BCS.WebApp.Services
{
	/// <summary>
	/// By implementing UsernameTokenManager we can verify the signature
	/// on messages received.
	/// </summary>
	/// <remarks>
	/// This class includes this demand to ensure that any untrusted
	/// assemblies cannot invoke this code. This helps mitigate
	/// brute-force discovery attacks.
	/// </remarks>
	[SecurityPermissionAttribute(SecurityAction.Demand, Flags=SecurityPermissionFlag.UnmanagedCode)]
	public class CustomUsernameTokenManager : UsernameTokenManager
	{
		/// <summary>
		/// Constructs an instance of this security token manager.
		/// </summary>
		public CustomUsernameTokenManager()
		{
		}

		/// <summary>
		/// Constructs an instance of this security token manager.
		/// </summary>
		/// <param name="nodes">An XmlNodeList containing XML elements from a configuration file.</param>
		public CustomUsernameTokenManager(XmlNodeList nodes)
			: base(nodes)
		{
		}

		/// <summary>
		/// Returns the password or password equivalent for the username provided.
		/// </summary>
		/// <param name="token">The username token</param>
		/// <returns>The password (or password equivalent) for the username</returns>
		protected override string AuthenticateToken( UsernameToken token )
		{
			// This is a very simple manager.
			// In most production systems the following code
			// typically consults an external database of (username,password) pairs where
			// the password is often not the real password but a password equivalent
			// (for example, the hash of the password). Provided that both client and
			// server can generate the same value for a particular username, there is
			// no requirement that the password be the actual password for the user.
			// For this sample the password is simply the reverse of the username.
			byte[] password = System.Text.Encoding.UTF8.GetBytes( token.Username );

			Array.Reverse( password );

			return Convert.ToBase64String( password );
		}

//		private bool AccessIsAllowed()
//		{
//			SecurityElementCollection elements = RequestSoapContext.Current.Security.Elements;
//
//			// Loop through all of the security tokens (could be more than one)
//			foreach( ISecurityElement securityElement in elements ) 
//			{ 
//				if( securityElement is MessageSignature ) 
//				{ 
//					MessageSignature sig = (MessageSignature)securityElement; 
//					if( (sig.SignatureOptions & SignatureOptions.IncludeSoapBody) != 0 ) 
//					{ 
//						SecurityToken sigToken = sig.SigningToken; 
//						// Find the token used to sign this message
//						if( sigToken is UsernameToken ) 
//						{
//							return token.Principal.IsInRole( @"BUILTIN\Users" ); 
//						}
//						else if( sigToken is KerberosToken )
//						{
//							return token.Principal.Identity.IsAuthenticated;
//						}
//					} 
//				} 
//			} 
//			return false; 
//		}
	}
}
