<%@ Page language="c#" CodeFile="Error.aspx.cs" AutoEventWireup="false" Inherits="BCS.WebApp.Services.Error" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Error</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script type="text/javascript">
			function copyErrorToClipboard(errmessage) {
				if (errmessage != "") {
					window.clipboardData.setData("Text", errmessage);
				}
			}
			
			function showStatus(message) {
				window.status = message ;
				return true ;
			}
		</script>
		<LINK href="../Style.css" type="text/css" rel="stylesheet">
		<script src="../javascripts/prototype.js" type="text/javascript"></script>
		<script src="../javascripts/scriptaculous.js" type="text/javascript"></script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<div id="header">
				<a href="."><IMG id="pageIcon" alt="BTS Logo" src="../images/bts_logo.gif" border="0"></a>
				<span class="pageTitle">Berkley Clearance System {Admin}</span>&nbsp;
			</div>
			<div id="content">
				<span class="sectionTitle">An error has occurred...</span>
				<p style="PADDING-LEFT: 10px; PADDING-TOP: 10px">An error has occurred on the page 
					you were requesting. If this problem persists, or if you have any questions, 
					please <A href="mailto:btsprod@service-now.com">contact the BTS HelpDesk</A> for 
					further assistance.&nbsp;You may&nbsp;wish&nbsp;to return to the <A href="javascript:history.go(-1);">
						previous page.</A></p>
				<P style="PADDING-LEFT: 10px; PADDING-TOP: 10px">Thank you for your patience and 
					understanding. We apologize for the inconvenience this error may have caused.</P>
				<P style="PADDING-LEFT: 10px; PADDING-TOP: 10px"><table cellSpacing="0" border="1" bordercolor="#ff0000" id="ErrorTable" runat="server">
						<tr>
							<td><table cellSpacing="0" cellPadding="5" bgColor="#ffffcc">
									<tr>
										<td><FONT size="2"><STRONG>Error Message (<A href="javascript:copyErrorToClipboard(document.getElementById('lblErrorMessage').innerHTML);"
														onmouseover="return showStatus('Copy error message to clipboard');" onmouseout="return showStatus('');">Copy 
														to Clipboard</A>):</STRONG></FONT></td>
									</tr>
									<tr>
										<td>
											<FONT size="2">
												<asp:Label id="lblErrorMessage" runat="server"></asp:Label></FONT></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</P>
			</div>
			<table class="footer" cellSpacing="0" cellPadding="5" width="100%" border="0" id="Table1">
				<tr>
					<td align="center">� 2005 Berkley Technology Services LLC. - Unauthorized use is 
						strictly prohibited.</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
