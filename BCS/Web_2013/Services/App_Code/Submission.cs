using System;
using System.Web;
using System.Collections;
using System.Collections.Specialized;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using BCS.Core.Clearance;
//using APS = BCS.Core.Clearance.APS;

namespace BCS.WebApp.Services
{
    /// <summary>
    /// Summary description for Submission
    /// </summary>
    [WebService(Namespace = "http://intranet.wrbts.ads.wrberkley.com/BCS/Submission")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1, EmitConformanceClaims = true)]
    public class Submission : System.Web.Services.WebService
    {

        public Submission()
        {
            //Uncomment the following line if using designed components 
            //InitializeComponent();            
        }

        #region Get Submission Methods

        [WebMethod(Description = "Gets submissions by Id.")]
        public string GetSubmissionById(long id)
        {
            return new Submissions().GetSubmissionById(id);
        }

        [WebMethod(Description = "Gets submissions made by an agent.")]
        public string GetSubmissionByAgentId(string companyNumber, int agentid)
        {
            ListDictionary values = new ListDictionary();
            values.Add("AgentId", agentid);

            string result = new Submissions().GetSubmissions(companyNumber, values);
            return result;
        }

        [WebMethod(Description = "gets submissions made from a particular agency.")]
        public string GetSubmissionByAgencyId(string companyNumber, int agencyid)
        {
            ListDictionary values = new ListDictionary();
            values.Add("AgencyId", agencyid);

            string result = new Submissions().GetSubmissions(companyNumber, values);
            return result;
        }

        [WebMethod(Description = "gets submissions by Effective Date, Company Number, ClientID, AgencyID, Underwriter Unit Code")]
        public string GetSubmissionbyEffectiveCompanyNumberClientAgencyUnit(string effectiveDate, string companyNumber, string clientID
                                                            , string agencyNumber, string underwriterUnitCode)
        {
            DateTime effDate;
            DateTime.TryParse(effectiveDate, out effDate);
            int cID;
            Int32.TryParse(clientID, out cID);
            string result = Submissions.GetSubmissionbyEffectiveCompanyNumberClientAgencyUnit(effDate, companyNumber, cID, agencyNumber, underwriterUnitCode);
            return result;
        }

        [WebMethod(Description = "Gets submissions made by a client.")]
        public string GetSubmissionsByClientId(string companyNumber, string clientid)
        {
            ListDictionary values = new ListDictionary();
            values.Add("ClientId", clientid);

            string result = new Submissions().GetSubmissions(companyNumber, values);
            return result;
        }

        [WebMethod(Description = "Gets submissions made by a comma seperated list of client ids.")]
        public string GetSubmissionsByClientIds(string companyNumber, string clientids)
        {
            ListDictionary values = new ListDictionary();
            values.Add("ClientIds", clientids);

            string result = new Submissions().GetSubmissions(companyNumber, values);
            return result;
        }

        public string GetSubmissionsByFEIN(string companyNumber, string fein)
        {
            ListDictionary values = new ListDictionary();
            values.Add("FEIN", fein);

            string result = new Submissions().GetSubmissions(companyNumber, values);
            return result;
        }

        [WebMethod(Description = "Gets submissions made by client based on taxid")]
        public string GetSubmissionsByClientTaxId(string companyNumber, string taxid)
        {
            ListDictionary values = new ListDictionary();
            values.Add("ClientTaxId", taxid);

            string result = new Submissions().GetSubmissions(companyNumber, values);
            return result;
        }

        [WebMethod(Description = "Gets submissions made by a individual or business client.")]
        public string GetSubmissionsByName(string companyNumber, string last, string first, string business)
        {
            ListDictionary values = new ListDictionary();
            values.Add("NameFirst", first);
            values.Add("NameLast", last);
            values.Add("NameBusiness", business);

            string result = new Submissions().GetSubmissions(companyNumber, values);
            return result;
        }

        [WebMethod(Description = "Gets submissions based by an address.")]
        public string GetSubmissionsByAddress(string companyNumber, string houseno, string address1, string address2, string address3,
            string address4, string city, string state, string county, string country, string zip)
        {
            ListDictionary values = new ListDictionary();
            values.Add("HouseNo", houseno);
            values.Add("Address1", address1);
            values.Add("Address2", address2);
            values.Add("Address3", address3);
            values.Add("Address4", address4);
            values.Add("City", city);
            values.Add("State", state);
            values.Add("County", county);
            values.Add("Country", country);
            values.Add("Zip", zip);

            string result = new Submissions().GetSubmissions(companyNumber, values);
            return result;
        }

        [WebMethod]
        public string GetSubmissionsByPolicyNumberAndCompanyNumber(string policyNumber, string companyNumber)
        {
            return Submissions.GetSubmissionsByPolicyNumberAndCompanyNumber(policyNumber, companyNumber);
        }

        [WebMethod(Description = "Gets Submission by Submission Number and Company Number")]
        public string GetSubmissionBySubmissionNumberAndCompanyNumber(
            string submissionNumber, string companyNumber, string sequenceNo)
        {
            if (sequenceNo == null || sequenceNo.Length == 0)
                return Submissions.GetSubmissionBySubmissionNumberAndCompanyNumber(submissionNumber, companyNumber);

            return Submissions.GetSubmissionBySubmissionNumberAndCompanyNumber(submissionNumber, companyNumber, sequenceNo);
        }

        [WebMethod(Description = "Gets Submission by Company Number and attribute value")]
        public string GetSubmissionsByPolicyAndAttribute(string companyNumber, string policyNumber, string attributeLabel, string attributeValue)
        {
            return Submissions.GetSubmissionsByPolicyAndAttribute(companyNumber, policyNumber, attributeLabel, attributeValue);
        }

        #region submission searches
        [WebMethod(Description = "Get all Submissions limit by row count.")]
        public string SearchSubmissions(string companyNumber, int rows)
        {
            return Submissions.SearchSubmissions(companyNumber, rows);
        }

        [WebMethod(Description = "Get all Submissions limit by row count.")]
        public string SearchSubmissionsPaged(string companyNumber, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissions(companyNumber, pageSize, pageNo);
        }

        [WebMethod(Description = "Search Submissions By CompanyNumber.")]
        public string SearchSubmissionsByCompanyNumber(string companyNumber)
        {
            return Submissions.SearchSubmissionsByCompanyNumber(companyNumber, 0, 0);
        }
        [WebMethod(Description = "Search Submissions By CompanyNumber.")]
        public string SearchSubmissionsByCompanyNumberPaged(string companyNumber, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsByCompanyNumber(companyNumber, pageSize, pageNo);
        }

        [WebMethod(Description = "Search Submissions By AgencyNumber.")]
        public string SearchSubmissionsByAgencyNumber(string companyNumber, string agencyNumber)
        {
            return Submissions.SearchSubmissionsByAgencyNumber(companyNumber, agencyNumber, 0, 0);
        }

        [WebMethod(Description = "Search Submissions By AgencyNumber.")]
        public string SearchSubmissionsByAgencyNumberPaged(string companyNumber, string agencyNumber, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsByAgencyNumber(companyNumber, agencyNumber, pageSize, pageNo);
        }

        [WebMethod(Description = "Search Submissions By AgencyName.")]
        public string SearchSubmissionsByAgencyName(string companyNumber, string agencyName)
        {
            return Submissions.SearchSubmissionsByAgencyName(companyNumber, agencyName, 0, 0);
        }

        [WebMethod(Description = "Search Submissions By AgencyName.")]
        public string SearchSubmissionsByAgencyNamePaged(string companyNumber, string agencyName, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsByAgencyName(companyNumber, agencyName, pageSize, pageNo);
        }

        [WebMethod(Description = "Search Submissions By ClientCoreClientId.")]
        public string SearchSubmissionsByClientCoreClientId(string companyNumber, long clientid)
        {
            return Submissions.SearchSubmissionsByClientCoreClientId(companyNumber, clientid, 0, 0);
        }

        [WebMethod(Description = "Search Submissions By ClientCoreClientId.")]
        public string SearchSubmissionsByClientCoreClientIdPaged(string companyNumber, long clientid, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsByClientCoreClientId(companyNumber, clientid, pageSize, pageNo);
        }

        [WebMethod(Description = "Search Submissions By ClientCoreClientPortfolioId.")]
        public string SearchSubmissionsByClientCoreClientPortfolioId(string companyNumber, long portfolioid)
        {
            return Submissions.SearchSubmissionsByClientCoreClientPortfolioId(companyNumber, portfolioid, 0, 0);
        }

        [WebMethod(Description = "Search Submissions By ClientCoreClientPortfolioId.")]
        public string SearchSubmissionsByClientCoreClientPortfolioIdPaged(string companyNumber, long portfolioid, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsByClientCoreClientPortfolioId(companyNumber, portfolioid, pageSize, pageNo);
        }

        [WebMethod(Description = "Search Submissions By Agency Contact Person.")]
        public string SearchSubmissionsByAgencyContactPerson(string companyNumber, string person)
        {
            return Submissions.SearchSubmissionsByAgencyContactPerson(companyNumber, person, 0, 0);
        }
        [WebMethod(Description = "Search Submissions By Agency Contact Person.")]
        public string SearchSubmissionsByAgencyContactPersonPaged(string companyNumber, string person, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsByAgencyContactPerson(companyNumber, person, pageSize, pageNo);
        }

        [WebMethod(Description = "Search Submissions By Submission Status.")]
        public string SearchSubmissionsBySubmissionStatus(string companyNumber, string status)
        {
            return Submissions.SearchSubmissionsBySubmissionStatus(companyNumber, status, 0, 0);
        }

        [WebMethod(Description = "Search Submissions By Submission Status.")]
        public string SearchSubmissionsBySubmissionStatusPaged(string companyNumber, string status, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsBySubmissionStatus(companyNumber, status, pageSize, pageNo);
        }

        [WebMethod(Description = "Search Submissions By Submission Number.")]
        public string SearchSubmissionsBySubmissionNumber(string companyNumber, long number)
        {
            return Submissions.SearchSubmissionsBySubmissionNumber(companyNumber, number, 0, 0, false);
        }

        [WebMethod(Description = "Search Submissions By Submission Number.")]
        public string SearchSubmissionsBySubmissionNumberPaged(string companyNumber, long number, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsBySubmissionNumber(companyNumber, number, pageSize, pageNo, false);
        }

        [WebMethod(Description = "Search Submissions By Submission Number and sequence number.")]
        public string SearchSubmissionsBySubmissionNumberAndSequence(string companyNumber, long number, int sequence)
        {
            return Submissions.SearchSubmissionsBySubmissionNumberAndSequence(companyNumber, number, sequence, 0, 0, false);
        }

        [WebMethod(Description = "Search Submissions By Submission Number and sequence number.")]
        public string SearchSubmissionsBySubmissionNumberAndSequencePaged(string companyNumber, long number, int sequence, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsBySubmissionNumberAndSequence(companyNumber, number, sequence, pageSize, pageNo, false);
        }

        [WebMethod(Description = "Search Submissions By Policy Number.")]
        public string SearchSubmissionsByPolicyNumber(string companyNumber, string number)
        {
            return Submissions.SearchSubmissionsByPolicyNumber(companyNumber, number, 0, 0, false);
        }

        [WebMethod(Description = "Search Submissions By Policy Number.")]
        public string SearchSubmissionsByPolicyNumberPaged(string companyNumber, string number, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsByPolicyNumber(companyNumber, number, pageSize, pageNo, false);
        }

        [WebMethod(Description = "Search Submissions By Estimated Premium Range.")]
        public string SearchSubmissionsByPremiumRange(string companyNumber, decimal from, decimal to)
        {
            return Submissions.SearchSubmissionsByPremiumRange(companyNumber, from, to, 0, 0, false);
        }

        [WebMethod(Description = "Search Submissions By Estimated Premium Range.")]
        public string SearchSubmissionsByPremiumRangePaged(string companyNumber, decimal from, decimal to, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsByPremiumRange(companyNumber, from, to, pageSize, pageNo, false);
        }

        [WebMethod(Description = "Search Submissions By Effective Date Range.")]
        public string SearchSubmissionsByEffectiveDateRange(string companyNumber, DateTime from, DateTime to)
        {
            return Submissions.SearchSubmissionsByEffectiveDateRange(companyNumber, from, to, 0, 0, false);
        }
        [WebMethod(Description = "Search Submissions By Effective Date Range.")]
        public string SearchSubmissionsByEffectiveDateRangePaged(string companyNumber, DateTime from, DateTime to, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsByEffectiveDateRange(companyNumber, from, to, pageSize, pageNo, false);
        }


        [WebMethod(Description = "Search Submissions By Expiration Date Range.")]
        public string SearchSubmissionsByExpirationDateRange(string companyNumber, DateTime from, DateTime to)
        {
            return Submissions.SearchSubmissionsByExpirationDateRange(companyNumber, from, to, 0, 0, false);
        }

        [WebMethod(Description = "Search Submissions By Expiration Date Range.")]
        public string SearchSubmissionsByExpirationDateRangePaged(string companyNumber, DateTime from, DateTime to, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsByExpirationDateRange(companyNumber, from, to, pageSize, pageNo, false);
        }

        [WebMethod(Description = "Search Submissions By Submission Date Range.")]
        public string SearchSubmissionsBySubmissionDateRange(string companyNumber, DateTime from, DateTime to)
        {
            return Submissions.SearchSubmissionsBySubmissionDateRange(companyNumber, from, to, 0, 0, false);
        }

        [WebMethod(Description = "Search Submissions By Submission Date Range.")]
        public string SearchSubmissionsBySubmissionDateRangePaged(string companyNumber, DateTime from, DateTime to, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsBySubmissionDateRange(companyNumber, from, to, pageSize, pageNo, false);
        }

        [WebMethod(Description = "Search Submissions By Insured Name.")]
        public string SearchSubmissionsByInsuredName(string companyNumber, string name)
        {
            return Submissions.SearchSubmissionsByInsuredName(companyNumber, name, 0, 0);
        }

        [WebMethod(Description = "Search Submissions By Insured Name.")]
        public string SearchSubmissionsByInsuredNamePaged(string companyNumber, string name, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsByInsuredName(companyNumber, name, pageSize, pageNo);
        }

        [WebMethod(Description = "Search Submissions By DBA.")]
        public string SearchSubmissionsByDBA(string companyNumber, string dba)
        {
            return Submissions.SearchSubmissionsByDBA(companyNumber, dba, 0, 0);
        }
        [WebMethod(Description = "Search Submissions By DBA.")]
        public string SearchSubmissionsByDBAPaged(string companyNumber, string dba, int pageSize, int pageNo)
        {
            return Submissions.SearchSubmissionsByDBA(companyNumber, dba, pageSize, pageNo);
        }


        #endregion


        #endregion

        #region Add Submission Methods
        [WebMethod(Description = "Adds a submission to the database based on the ids of attributes like submission type, status etc.")]
        public string AddSubmissionByIdsWithTypes(string clientid, string clientaddressid, string clientaddressids, string clientnameids, string clientportfolioid,
            string by, int companyid, string submissionBy, string submissionDate, int typeid, int agencyid, int agentid, string agentreasonids,
            int contactid, string companynumber, int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber, string companynumbercodeids, string attributes,
            decimal premium, string effective, string expiration, int underwriterid, int analystid, int technicianid, int policysystemid, string comments,
            int lineofbusinessid, string lobchildren, string insuredname, string dba, string subno, int othercarrierid,
            decimal othercarrierpremium, string clientAddressIDsAndTypes, string clientNameIdsAndTypes)
        {
            #region TODO : stop gap until other apps catch up, remove by, clientaddressid, companyid after that
            string tempSubmissionBy = string.Empty;
            if (!string.IsNullOrEmpty(by))
                tempSubmissionBy = by;
            if (!string.IsNullOrEmpty(submissionBy))
                tempSubmissionBy = submissionBy;

            if (!string.IsNullOrEmpty(clientaddressid))
            {
                if (!string.IsNullOrEmpty(clientaddressids))
                    clientaddressids = clientaddressid;
                else
                    clientaddressids += "|" + clientaddressid;
            }
            #endregion


            return Submissions.AddSubmission(clientid, clientaddressids, clientnameids, clientportfolioid, tempSubmissionBy, submissionDate, typeid, agencyid, agentid,
                agentreasonids, contactid,
                companynumber, statusid, statusdate, statusreasonid, statusreasonother, policynumber, premium, companynumbercodeids, attributes, effective,
                expiration, underwriterid, analystid, technicianid, policysystemid, comments, lineofbusinessid.ToString(), lobchildren, insuredname, dba, subno,
                othercarrierid, othercarrierpremium, clientAddressIDsAndTypes, clientNameIdsAndTypes);
        }

        [WebMethod(Description = "Adds a submission to the database based on the ids of attributes like submission type, status etc.")]
        public string AddSubmissionByIds(string clientid, string clientaddressid, string clientaddressids, string clientnameids, string clientportfolioid,
            string by, int companyid, string submissionBy, string submissionDate, int typeid, int agencyid, int agentid, string agentreasonids,
            int contactid, string companynumber, int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber, string companynumbercodeids, string attributes,
            decimal premium, string effective, string expiration, int underwriterid, int analystid, int technicianid, int policysystemid, string comments,
            int lineofbusinessid, string lobchildren, string insuredname, string dba, string subno, int othercarrierid,
            decimal othercarrierpremium)
        {
            #region TODO : stop gap until other apps catch up, remove by, clientaddressid, companyid after that
            string tempSubmissionBy = string.Empty;
            if (!string.IsNullOrEmpty(by))
                tempSubmissionBy = by;
            if (!string.IsNullOrEmpty(submissionBy))
                tempSubmissionBy = submissionBy;

            if (!string.IsNullOrEmpty(clientaddressid))
            {
                if (!string.IsNullOrEmpty(clientaddressids))
                    clientaddressids = clientaddressid;
                else
                    clientaddressids += "|" + clientaddressid;
            }
            #endregion


            return Submissions.AddSubmission(clientid, clientaddressids, clientnameids, clientportfolioid, tempSubmissionBy, submissionDate, typeid, agencyid, agentid,
                agentreasonids, contactid,
                companynumber, statusid, statusdate, statusreasonid, statusreasonother, policynumber, premium, companynumbercodeids, attributes, effective,
                expiration, underwriterid, analystid, technicianid, policysystemid, comments, lineofbusinessid.ToString(), lobchildren, insuredname, dba, subno,
                othercarrierid, othercarrierpremium);
        }
        //[WebMethod(Description = "Adds a submission (aps) to the database based on the ids of attributes like submission type, status etc.")]
        //public string AddAPSSubmissionByIds(string clientid, string clientaddressids, string clientnameids, string clientportfolioid,
        //    string submissionBy, string submissionDate, int typeid, long agencyid, string agentid, string agentreasonids,
        //    string contactid, string companynumber, int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber, string companynumbercodeids, string attributes,
        //    decimal premium, string effective, string expiration, string underwriterid, string analystid, string technicianid, int policysystemid, string comments,
        //    long lineofbusinessid, string lobchildren, string insuredname, string dba, string subno, int othercarrierid,
        //    decimal othercarrierpremium)
        //{
        //    return new Core.Clearance.APS.Submissions().AddSubmission(clientid, clientaddressids, clientnameids, clientportfolioid, submissionBy, submissionDate, typeid, agencyid, agentid,
        //        agentreasonids, contactid,
        //        companynumber, statusid, statusdate, statusreasonid, statusreasonother, policynumber, premium, companynumbercodeids, attributes, effective,
        //        expiration, underwriterid, analystid, technicianid, policysystemid, comments, lineofbusinessid, lobchildren, insuredname, dba, subno,
        //        othercarrierid, othercarrierpremium);
        //}

        [WebMethod(Description = "Adds a submission, accepts xml serializable to type BCS.Core.Clearance.BizObjects.Submission")]
        public string AddSubmissionByXml(string xml)
        {
            BCS.Core.Clearance.BizObjects.Submission _submission;
            try
            {
                _submission = (BCS.Core.Clearance.BizObjects.Submission)
                            BCS.Core.XML.Deserializer.Deserialize(
                            xml, typeof(BCS.Core.Clearance.BizObjects.Submission));

            }
            catch (Exception ex)
            {
                _submission = new BCS.Core.Clearance.BizObjects.Submission();
                _submission.Message = "Unable to extract submission from xml.";
                _submission.Status = "Failure";
                BTS.LogFramework.LogCentral.Current.LogError(ex);
                return Core.XML.Serializer.SerializeAsXml(_submission);
            }

            #region Code types
            string codeids = string.Empty;
            if (_submission.SubmissionCodeTypess != null && _submission.SubmissionCodeTypess.Length > 0)
            {
                string[] scodeids = new string[_submission.SubmissionCodeTypess.Length];
                for (int i = 0; i < _submission.SubmissionCodeTypess.Length; i++)
                {
                    scodeids[i] = _submission.SubmissionCodeTypess[i].CodeId.ToString();
                }
                codeids = string.Join("|", scodeids);
            }
            #endregion

            #region Attributes
            string attributes = string.Empty;
            if (_submission.AttributeList != null && _submission.AttributeList.Length > 0)
            {
                string[] sattributes = new string[_submission.AttributeList.Length];
                for (int i = 0; i < _submission.AttributeList.Length; i++)
                {
                    //sattributes[i] = _submission.AttributeList[i].CompanyNumberAttributeId.ToString();
                    sattributes[i] = string.Format("{0}={1}", _submission.AttributeList[i].CompanyNumberAttributeId, _submission.AttributeList[i].AttributeValue);
                }
                attributes = string.Join("|", sattributes);
            }
            #endregion

            #region Line Of Businesses
            string lobss = string.Empty;
            if (_submission.LineOfBusinessList != null && _submission.LineOfBusinessList.Length > 0)
            {
                string[] slobss = new string[_submission.LineOfBusinessList.Length];
                for (int i = 0; i < _submission.LineOfBusinessList.Length; i++)
                {
                    slobss[i] = string.Format("{0}", _submission.LineOfBusinessList[i].Id);
                }
                lobss = string.Join("|", slobss);
            }

            #endregion

            #region Company Number
            Biz.DataManager dm = Common.GetDataManager();
            dm.QueryCriteria.And(Biz.JoinPath.CompanyNumber.Columns.Id, _submission.CompanyNumberId);
            Biz.CompanyNumber cnum = dm.GetCompanyNumber();
            if (cnum == null)
            {
                _submission = new BCS.Core.Clearance.BizObjects.Submission();
                _submission.Message = "The Company number supplied does not exist.";
                _submission.Status = "Failure";

                return Core.XML.Serializer.SerializeAsXml(_submission);
            }
            string companynumber = cnum.PropertyCompanyNumber;
            #endregion

            #region Line Of Business Children
            string lobchildren = string.Empty;
            if (_submission.LineOfBusinessChildList != null && _submission.LineOfBusinessChildList.Length > 0)
            {
                string[] slobss = new string[_submission.LineOfBusinessChildList.Length];
                for (int i = 0; i < _submission.LineOfBusinessChildList.Length; i++)
                {
                    slobss[i] = string.Format("{0}={1}={2}={3}={4}={5}",
                        _submission.LineOfBusinessChildList[i].Id,
                        _submission.LineOfBusinessChildList[i].CompanyNumberLOBGridId,
                        _submission.LineOfBusinessChildList[i].SubmissionStatusId,
                        _submission.LineOfBusinessChildList[i].SubmissionStatusReasonId,
                        _submission.LineOfBusinessChildList[i].Deleted);
                }
                lobchildren = string.Join("|", slobss);
            }
            #endregion

            #region Client Core Addresses
            string ClientCoreAddressIds = string.Empty;
            if (_submission.ClientCoreAddressIds != null)
            {
                string[] arrClientCoreAddressIds = Array.ConvertAll<int, string>(_submission.ClientCoreAddressIds, new Converter<int, string>(Convert.ToString));
                ClientCoreAddressIds = string.Join("|", arrClientCoreAddressIds);
            }
            #endregion
            #region Client Core Names
            string ClientCoreNameIds = string.Empty;
            if (_submission.ClientCoreNameIds != null)
            {
                string[] arrClientCoreNameIds = Array.ConvertAll<int, string>(_submission.ClientCoreNameIds, new Converter<int, string>(Convert.ToString));
                ClientCoreNameIds = string.Join("|", arrClientCoreNameIds);
            }
            #endregion

            #region Agent Unspecified Reasons
            string agentunspecifiedreasons = string.Empty;
            if (_submission.AgentUnspecifiedReasonList != null && _submission.AgentUnspecifiedReasonList.Length > 0)
            {
                string[] arr = new string[_submission.AgentUnspecifiedReasonList.Length];
                for (int i = 0; i < _submission.AgentUnspecifiedReasonList.Length; i++)
                {
                    arr[i] = string.Format("{0}", _submission.AgentUnspecifiedReasonList[i].CompanyNumberAgentUnspecifiedReasonId);
                }
                agentunspecifiedreasons = string.Join("|", arr);
            }
            #endregion

            return Submissions.AddSubmission(
                _submission.ClientCoreClientId.ToString(), ClientCoreAddressIds, ClientCoreNameIds, _submission.ClientCorePortfolioId.ToString(),
                _submission.SubmissionBy, _submission.SubmissionDt == DateTime.MinValue ? DateTime.Now.ToString() : _submission.SubmissionDt.ToString(),
                _submission.SubmissionTypeId, _submission.AgencyId, _submission.AgentId, agentunspecifiedreasons, _submission.ContactId,
                companynumber, _submission.SubmissionStatusId, _submission.SubmissionStatusDate.ToString(), _submission.SubmissionStatusReasonId, _submission.SubmissionStatusReasonOther,
                _submission.PolicyNumber, _submission.EstimatedWrittenPremium, codeids, attributes, _submission.EffectiveDate.ToString(),
                _submission.ExpirationDate.ToString(), _submission.UnderwriterId, _submission.AnalystId, _submission.TechnicianId, _submission.PolicySystemId, _submission.Comments,
                lobss, lobchildren, _submission.InsuredName, _submission.Dba,
                _submission.SubmissionNumber.ToString(), _submission.OtherCarrierId, _submission.OtherCarrierPremium);
        }

        //[WebMethod(Description = "Adds a submission to the database based on the values of attributes like submission type, status etc.")]
        //public string AddSubmissionByValues(string clientid, string clientaddressid, string clientportfolioid, string by,
        //    int companynumber,string typeabbr, int agentid, string statuscode, string policynumber,
        //    decimal premium, string codename, string effective, string expiration, int underwriterid, int analystid,
        //    string comments, string updatedby, string lineofbusinesscode, string insuredname, string dba, string subno)
        //{
        //    return Submissions.AddSubmission(clientid, clientaddressid, clientportfolioid, by, companynumber, typeabbr, agentid, statuscode, policynumber,
        //        premium, codename, effective, expiration, underwriterid, analystid, comments, updatedby, lineofbusinesscode, insuredname, dba, subno);
        //}

        #endregion

        #region Modify Submission Methods

        [WebMethod(Description = "Modifies a submission to the database based on the ids of attributes like submission type, status etc. companycodeids passed as | delimited numbers")]
        public string ModifySubmissionByIdsWithTypes(string id, long clientid, string clientaddressid, string clientaddressids, string clientnameids, long clientportfolioid,
            int companyid, int typeid, int agencyid, int agentid, string agentreasonids, int contactid,
            int companynumberid, int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber, string companycodeids, string attributes,
            decimal premium, string effective, string expiration, int underwriterid, int analystid, int technicianid, int policysystemid, string comments,
            string updatedby, int lineofbusinessid, string lobchildren, string insuredname, string dba, int othercarrierid,
            decimal othercarrierpremium, string clientAddressIDsAndTypes, string clientNameIdsAndTypes)
        {
            #region TODO : stop gap until other apps catch up, remove clientaddressid
            if (!string.IsNullOrEmpty(clientaddressid))
            {
                if (!string.IsNullOrEmpty(clientaddressids))
                    clientaddressids = clientaddressid;
                else
                    clientaddressids += "|" + clientaddressid;
            }
            #endregion

            return Submissions.ModifySubmission(id, clientid, clientaddressids, clientnameids, clientportfolioid, companyid, typeid, agencyid, agentid,
                agentreasonids, contactid,
                companynumberid, statusid, statusdate, statusreasonid, statusreasonother, policynumber, companycodeids, attributes, premium, effective, expiration,
                underwriterid, analystid, technicianid, policysystemid, comments, updatedby, lineofbusinessid.ToString(), lobchildren, insuredname, dba, othercarrierid,
                othercarrierpremium, clientAddressIDsAndTypes, clientNameIdsAndTypes);
        }

        [WebMethod(Description = "Modifies a submission to the database based on the ids of attributes like submission type, status etc. companycodeids passed as | delimited numbers")]
        public string ModifySubmissionByIds(string id, long clientid, string clientaddressid, string clientaddressids, string clientnameids, long clientportfolioid,
            int companyid, int typeid, int agencyid, int agentid, string agentreasonids, int contactid,
            int companynumberid, int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber, string companycodeids, string attributes,
            decimal premium, string effective, string expiration, int underwriterid, int analystid, int technicianid, int policysystemid, string comments,
            string updatedby, int lineofbusinessid, string lobchildren, string insuredname, string dba, int othercarrierid,
            decimal othercarrierpremium)
        {
            #region TODO : stop gap until other apps catch up, remove clientaddressid
            if (!string.IsNullOrEmpty(clientaddressid))
            {
                if (!string.IsNullOrEmpty(clientaddressids))
                    clientaddressids = clientaddressid;
                else
                    clientaddressids += "|" + clientaddressid;
            }
            #endregion

            return Submissions.ModifySubmission(id, clientid, clientaddressids, clientnameids, clientportfolioid, companyid, typeid, agencyid, agentid,
                agentreasonids, contactid,
                companynumberid, statusid, statusdate, statusreasonid, statusreasonother, policynumber, companycodeids, attributes, premium, effective, expiration,
                underwriterid, analystid, technicianid, policysystemid, comments, updatedby, lineofbusinessid.ToString(), lobchildren, insuredname, dba, othercarrierid,
                othercarrierpremium);
        }
        //[WebMethod(Description = "Modifies an aps submission to the database based on the ids of attributes like submission type, status etc. companycodeids passed as | delimited numbers")]
        //public string ModifyAPSSubmissionByIds(string id, long clientid, string clientaddressids, string clientnameids, long clientportfolioid,
        //    int typeid, long agencyid, string agentid, string agentreasonids, string contactid,
        //    int companynumberid, int statusid, string statusdate, int statusreasonid, string statusreasonother, string policynumber, string companycodeids, string attributes,
        //    decimal premium, string effective, string expiration, string underwriterid, string analystid, string technicianid, int policysystemid, string comments,
        //    string updatedby, long lineofbusinessid, string lobchildren, string insuredname, string dba, int othercarrierid,
        //    decimal othercarrierpremium)
        //{
        //    return new Core.Clearance.APS.Submissions().ModifySubmission(id, clientid, clientaddressids, clientnameids, clientportfolioid, typeid, agencyid, agentid,
        //        agentreasonids, contactid,
        //        companynumberid, statusid, statusdate, statusreasonid, statusreasonother, policynumber, companycodeids, attributes, premium, effective, expiration,
        //        underwriterid, analystid, technicianid, policysystemid, comments, updatedby, lineofbusinessid, lobchildren, insuredname, dba, othercarrierid,
        //        othercarrierpremium);
        //}

        [WebMethod(Description = "Modifies a submission, accepts xml serializable as type BCS.Core.Clearance.BizObjects.Submission")]
        public string ModifySubmissionByXml(string xml)
        {
            BCS.Core.Clearance.BizObjects.Submission _submission;
            try
            {
                _submission = (BCS.Core.Clearance.BizObjects.Submission)
                            BCS.Core.XML.Deserializer.Deserialize(
                            xml, typeof(BCS.Core.Clearance.BizObjects.Submission));

            }
            catch (Exception ex)
            {
                _submission = new BCS.Core.Clearance.BizObjects.Submission();
                _submission.Message = "Unable to extract submission from xml. Please check the input.";
                _submission.Status = "Failure";

                BTS.LogFramework.LogCentral.Current.LogError(ex);
                return Core.XML.Serializer.SerializeAsXml(_submission);
            }

            #region Code types
            string codeids = string.Empty;
            if (_submission.SubmissionCodeTypess != null && _submission.SubmissionCodeTypess.Length > 0)
            {
                string[] scodeids = new string[_submission.SubmissionCodeTypess.Length];
                for (int i = 0; i < _submission.SubmissionCodeTypess.Length; i++)
                {
                    scodeids[i] = _submission.SubmissionCodeTypess[i].CodeId.ToString();
                }
                codeids = string.Join("|", scodeids);
            }
            #endregion

            #region Attributes
            string attributes = string.Empty;
            if (_submission.AttributeList != null && _submission.AttributeList.Length > 0)
            {
                string[] sattributes = new string[_submission.AttributeList.Length];
                for (int i = 0; i < _submission.AttributeList.Length; i++)
                {
                    sattributes[i] = string.Format("{0}={1}", _submission.AttributeList[i].CompanyNumberAttributeId, _submission.AttributeList[i].AttributeValue);
                }
                attributes = string.Join("|", sattributes);
            }
            #endregion

            #region Line Of Businesses
            string lobss = string.Empty;
            if (_submission.LineOfBusinessList != null && _submission.LineOfBusinessList.Length > 0)
            {
                string[] slobss = new string[_submission.LineOfBusinessList.Length];
                for (int i = 0; i < _submission.LineOfBusinessList.Length; i++)
                {
                    slobss[i] = string.Format("{0}", _submission.LineOfBusinessList[i].Id);
                }
                lobss = string.Join("|", slobss);
            }

            #endregion

            #region Line Of Business Children
            string lobchildren = string.Empty;
            if (_submission.LineOfBusinessChildList != null && _submission.LineOfBusinessChildList.Length > 0)
            {
                string[] slobss = new string[_submission.LineOfBusinessChildList.Length];
                for (int i = 0; i < _submission.LineOfBusinessChildList.Length; i++)
                {
                    slobss[i] = string.Format("{0}={1}={2}={3}={4}={5}",
                        _submission.LineOfBusinessChildList[i].Id,
                        _submission.LineOfBusinessChildList[i].CompanyNumberLOBGridId,
                        _submission.LineOfBusinessChildList[i].SubmissionStatusId,
                        _submission.LineOfBusinessChildList[i].SubmissionStatusReasonId,
                        _submission.LineOfBusinessChildList[i].Deleted);
                }
                lobchildren = string.Join("|", slobss);
            }
            #endregion

            #region Client Core Addresses
            string ClientCoreAddressIds = string.Empty;
            if (_submission.ClientCoreAddressIds != null)
            {
                string[] arrClientCoreAddressIds = Array.ConvertAll<int, string>(_submission.ClientCoreAddressIds, new Converter<int, string>(Convert.ToString));
                ClientCoreAddressIds = string.Join("|", arrClientCoreAddressIds);
            }
            #endregion
            #region Client Core Names
            string ClientCoreNameIds = string.Empty;
            if (_submission.ClientCoreNameIds != null)
            {
                string[] arrClientCoreNameIds = Array.ConvertAll<int, string>(_submission.ClientCoreNameIds, new Converter<int, string>(Convert.ToString));
                ClientCoreNameIds = string.Join("|", arrClientCoreNameIds);
            }
            #endregion

            #region Agent Unspecified Reasons
            string agentunspecifiedreasons = string.Empty;
            if (_submission.AgentUnspecifiedReasonList != null && _submission.AgentUnspecifiedReasonList.Length > 0)
            {
                string[] arr = new string[_submission.AgentUnspecifiedReasonList.Length];
                for (int i = 0; i < _submission.AgentUnspecifiedReasonList.Length; i++)
                {
                    arr[i] = string.Format("{0}", _submission.AgentUnspecifiedReasonList[i].CompanyNumberAgentUnspecifiedReasonId);
                }
                agentunspecifiedreasons = string.Join("|", arr);
            }
            #endregion

            return Submissions.ModifySubmission(_submission.Id.ToString(),
                _submission.ClientCoreClientId, ClientCoreAddressIds, ClientCoreNameIds, _submission.ClientCorePortfolioId, _submission.CompanyId,
                _submission.SubmissionTypeId, _submission.AgencyId, _submission.AgentId, agentunspecifiedreasons, _submission.ContactId, _submission.CompanyNumberId,
                _submission.SubmissionStatusId, _submission.SubmissionStatusDate.ToString(), _submission.SubmissionStatusReasonId, _submission.SubmissionStatusReasonOther,
                _submission.PolicyNumber, codeids, attributes, _submission.EstimatedWrittenPremium, _submission.EffectiveDate.ToString(),
                _submission.ExpirationDate.ToString(), _submission.UnderwriterId, _submission.AnalystId, _submission.TechnicianId, _submission.PolicySystemId,
                _submission.Comments, _submission.UpdatedBy, lobss, lobchildren, _submission.InsuredName, _submission.Dba,
                _submission.OtherCarrierId, _submission.OtherCarrierPremium, string.Empty, string.Empty);
        }


        [WebMethod(Description = "Updates all submissions associated with this client with the client portfolio Id.")]
        public string UpdatePortfolioId(string companyNumber, long clientid, long portfoilioid, string user)
        {
            return Submissions.UpdatePortfolioId(companyNumber, clientid, portfoilioid, user);
        }

        [WebMethod(Description = "Updates all submissions associated with the old client with the new client and if passed insured name and dba. ")]
        public string UpdateClientId(string companyNumber, string oldClientId, string newClientId, string insuredName, string dba, string user)
        {
            return new Submissions().UpdateClientId(companyNumber, oldClientId, newClientId, insuredName, dba, user, false);
        }

        [WebMethod]
        public string SwitchClient(string companyNumber, string numbers, string clientIds, string numberType)
        {
            return new Submissions().SwitchClient(companyNumber, numbers, clientIds, numberType);
        }

        [WebMethod(Description = "Returns all the Submission status history for a given submission")]
        public string GetSubmissionHistoryBySubmissionNumberAndCompanyNumber(string submissionNumber, string companyNumber, string sequence)
        {
            if (!string.IsNullOrEmpty(submissionNumber.ToString()) && !string.IsNullOrEmpty(companyNumber))
            {
                if (sequence == null || sequence.Length == 0)
                    return BCS.Core.Clearance.SubmissionsEF.GetSubmissionStatusHistoryBySubmissionNumberAndCompanyNumber(Convert.ToInt64(submissionNumber), companyNumber);
                else
                    return BCS.Core.Clearance.SubmissionsEF.GetSubmissionStatusHistoryBySubmissionNumberAndCompanyNumber(Convert.ToInt64(submissionNumber), companyNumber, Convert.ToInt16(sequence));
            }
            return "No results found";
        }
        #endregion
    }
}