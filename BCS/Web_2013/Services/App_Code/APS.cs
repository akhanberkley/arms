using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;

using BCS.Biz;
using BCS.Core;
using BCS.Core.Clearance;
using BCS.WebApp.Services.Components;
using BTS.LogFramework;
using CacheEngine = BTS.CacheEngine.V2.CacheEngine;

using com.bts.aps.domain;
using com.bts.aps.domain.publish;

using System.Xml;
using System.Net.Mail;
using System.Configuration;
using System.Collections.Specialized;
using System.Reflection;
using System.Data.SqlTypes;


namespace BCS.WebApp.Services
{
    /// <summary>
    /// Summary description for APS
    /// </summary>
    [WebService(Namespace = "http://intranet.wrbts.ads.wrberkley.com/BCS/APS")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class APS : System.Web.Services.WebService
    {
        private static XmlDocument mappingDoc;

        public APS()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

        [WebMethod(Description = "Processes Agency Changes")]
        public void processAgencyMessage(String companyNumber, String apsChangeMessage)
        {
            string apsXmlFile = string.Empty;
            string initFileName = string.Empty;

            XmlDocument apsMessage = new XmlDocument();
            apsMessage.LoadXml(apsChangeMessage);
            XmlNode idNode = apsMessage.SelectSingleNode("com.bts.aps.domain.publish.APSChange/id");
            initFileName = idNode.InnerText;
            initFileName = initFileName != String.Empty ? initFileName : System.DateTime.Now.Date.Ticks.ToString();

            apsXmlFile = WriteFile(companyNumber, apsChangeMessage, initFileName);
            if (apsXmlFile.Length > 0)
            {
                string successMsg = string.Format("SAVED: Received APSChange, XMLFile - {0}", apsXmlFile);
                Log(successMsg);
            }
            else
            {
                string failureMsg = "ERROR: Issues saving APSChanges to XML File";
                //Log(failureMsg, null, LogMessageType.Error);
                Components.ExceptionManager.Notify(failureMsg, null, MailPriority.High);

            }
            
        }

        #region Utilities

        /// <summary>
        /// Writing the APSChange message to Disk
        /// </summary>
        /// <param name="data">string</param>
        /// <param name="fileName">string</param>
        /// <returns>FileName -> String</returns>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Assert)]
        private string WriteFile(string data, string fileName)
        {
            try
            {
                string currentDirectory = Components.DefaultValues.XmlDataFilesDirectory;
                    //System.IO.Directory.GetCurrentDirectory();
                bool b = System.IO.Directory.Exists(currentDirectory + "\\XML");

                if (!b)
                    System.IO.Directory.CreateDirectory(currentDirectory + "\\XML");

                string actualFileName = string.Format("{0}\\{1}.{2}", currentDirectory + "\\XML",
                    string.IsNullOrEmpty(fileName) ? DateTime.Now.Ticks.ToString() : (fileName + "_" + DateTime.Now.Ticks.ToString()), "xml");

                System.IO.StreamWriter outFile = System.IO.File.CreateText(actualFileName);
                outFile.WriteLine(data);
                outFile.Close();

                return actualFileName;
            }
            catch (Exception ex)
            {
                Log("unable to write message file", ex, LogMessageType.Warn);
                string body = Components.ExceptionManager.GetFullDetailsString(ex);
                Components.ExceptionManager.Notify(ex.Message, body, MailPriority.High);
                return string.Empty;
            }
        }

        /// <summary>
        /// Writing the APSChange message to Disk
        /// </summary>
        /// <param name="data">string</param>
        /// <param name="fileName">string</param>
        /// <returns>FileName -> String</returns>
        [System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Assert)]
        private string WriteFile(string companyNumber, string data, string fileName)
        {
            try
            {
                string currentDirectory = Components.DefaultValues.XmlDataFilesDirectory;
                //System.IO.Directory.GetCurrentDirectory();
                bool b = System.IO.Directory.Exists(currentDirectory + "\\XML");

                if (!b)
                    System.IO.Directory.CreateDirectory(currentDirectory + "\\XML");

                string actualFileName = string.Format("{0}\\{1}.{2}", currentDirectory + "\\XML",
                    string.IsNullOrEmpty(fileName) ? DateTime.Now.Ticks.ToString() : (companyNumber + "_" + fileName + "_" + DateTime.Now.Ticks.ToString()), "xml");

                System.IO.StreamWriter outFile = System.IO.File.CreateText(actualFileName);
                outFile.WriteLine(data);
                outFile.Close();

                return actualFileName;
            }
            catch (Exception ex)
            {
                Log("unable to write message file", ex, LogMessageType.Warn);
                string body = Components.ExceptionManager.GetFullDetailsString(ex);
                Components.ExceptionManager.Notify(ex.Message, body, MailPriority.High);
                return string.Empty;
            }
        }

        #endregion

        #region Logging

        [System.Runtime.CompilerServices.MethodImplAttribute(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)]
        static void Log(String data)
        {
            //System.IO.StreamWriter outFile = System.IO.File.AppendText(ConfigurationSettings.Instance.ProcessingFile);
            //outFile.WriteLine(DateTime.Now.ToString() + "\t" + data);
            //outFile.Close();
            Log(data, null, LogMessageType.Info);
        }

        static void LogCache(String data)
        {
            string logCacheFile = Components.DefaultValues.XmlDataFilesDirectory + "LogCache.txt";
            System.IO.StreamWriter outFile = System.IO.File.AppendText(logCacheFile);
            outFile.WriteLine(DateTime.Now.ToString() + "\t" + data);
            outFile.Close();
        }

        static void Log(string message, Exception exception, LogMessageType logCriticality)
        {
            switch (logCriticality)
            {
                case LogMessageType.Fatal:
                    LogCentral.Current.LogFatal(message, exception);
                    break;
                case LogMessageType.Error:
                    LogCentral.Current.LogError(message, exception);
                    break;
                case LogMessageType.Warn:
                    LogCentral.Current.LogWarn(message, exception);
                    break;
                case LogMessageType.Info:
                    LogCentral.Current.LogInfo(message, exception);
                    break;
                default:
                    break;
            }
        }
        
        enum LogMessageType
        {
            Fatal,
            Error,
            Warn,
            Info
        }

        #endregion

    }
}

