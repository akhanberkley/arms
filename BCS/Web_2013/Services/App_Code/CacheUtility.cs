using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;

using BCS.Biz;
using BCS.Core;
using BCS.Core.Clearance;
using BCS.WebApp.Services.Components;
using BTS.LogFramework;
using CacheEngine = BTS.CacheEngine.V2.CacheEngine;

using com.bts.aps.domain;
using com.bts.aps.domain.publish;

using System.Xml;
using System.Net.Mail;
using System.Configuration;
using System.Collections.Specialized;
using System.Reflection;
using System.Data.SqlTypes;


namespace BCS.WebApp.Services
{
    /// <summary>
    /// Summary description for APS
    /// </summary>
    [WebService(Namespace = "http://intranet.wrbts.ads.wrberkley.com/BCS/APS")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class CacheUtility : System.Web.Services.WebService
    {
        private static XmlDocument mappingDoc;
        private static BCS.Biz.DataManager dm = CommonGetDataManager();

        public CacheUtility()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

        [WebMethod(Description = "Processes Cache additions & deletions")]
        public void ProcessAgencyCache(string companyNumber, CacheChangeType changeType)
        {
            switch (changeType)
            {
                case CacheChangeType.Remove:
                    break;
                case CacheChangeType.Insert:
                default:
                    try
                    {
                        int objCacheTimeinMinutes = 1;
                        //Getting Company Number
                        dm.QueryCriteria.Clear();
                        dm.QueryCriteria.And(BCS.Biz.JoinPath.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
                        BCS.Biz.CompanyNumber companyNo = dm.GetCompanyNumber();

                        HttpContext curr = HttpContext.Current;
                        string AgencyCacheKey = string.Format("Agencies4CompanyNumber{0}", companyNo.Id);

                        //Creating Cache Engine and clearing Cache
                        CacheEngine cacheEngine = new CacheEngine(AgencyCacheKey, objCacheTimeinMinutes);
                        if (cacheEngine.CachedItemExist(AgencyCacheKey))
                        {
                            cacheEngine.RemoveItemFromCache(AgencyCacheKey);
                        }
                        //else
                        //{
                        //    //Adding to Cache
                        //    dm.QueryCriteria.Clear();
                        //    dm.QueryCriteria.And(JoinPath.Agency.Columns.CompanyNumberId, companyNo.Id);
                        //    AgencyCollection agencyCollection = dm.GetAgencyCollection(BCS.Biz.FetchPath.Agency.AgencyLicensedState);
                        //    cacheEngine.AddItemToCache(AgencyCacheKey, agencyCollection);
                        //}
                    }
                    catch (Exception ex)
                    {
                        string message = "Exception adding to Cache."; ;

                        ExceptionManager.Notify(message, ExceptionManager.GetFullDetailsString(ex), MailPriority.High);

                    }
                    break;
            }
        }

        #region Logging
        private static BCS.Biz.DataManager CommonGetDataManager()
        {
            return new BCS.Biz.DataManager(DefaultValues.DSN);
        }

        static void LogCache(String data)
        {
            string logCacheFile = Components.DefaultValues.LogCacheFile;
            System.IO.StreamWriter outFile = System.IO.File.AppendText(logCacheFile);
            outFile.WriteLine(DateTime.Now.ToString() + "\t" + data);
            outFile.Close();
        }

        public enum CacheChangeType
        {
            Insert,
            Remove
        }

        #endregion

    }
}

