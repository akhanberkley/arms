using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;

using structures = BTS.ClientCore.Wrapper.Structures;
using BCS.Core.Clearance;

namespace BCS.WebApp.Services
{    
    /// <summary>
    /// Summary description for Lookup
    /// </summary>
    [WebService(Namespace = "http://intranet.wrbts.ads.wrberkley.com/BCS/Lookup")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1,EmitConformanceClaims=true)]
    public class Lookup : System.Web.Services.WebService
    {

        public Lookup()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }
        [WebMethod]
        public string GetSicCodes()
        {
            Lookups ll = new Lookups();
            return ll.GetSicCodes(0);
        }

        [WebMethod]
        public string GetSicCodeBySicCode(int sicCode)
        {
            Lookups ll = new Lookups();
            return ll.GetSicCodes(sicCode);
        }

        [WebMethod]
        public string GetClassCodesByCompanyNumber(string companyNumber)
        {
            Lookups ll = new Lookups();
            return ll.GetClassCodeByCodeValueAndCompanyNumber(companyNumber, string.Empty);
        }
        [WebMethod]
        public string GetClassCodeByCodeValueAndCompanyNumber(string companyNumber, string codeValue)
        {
            Lookups ll = new Lookups();
            return ll.GetClassCodeByCodeValueAndCompanyNumber(companyNumber, codeValue);
        }

        [WebMethod]
        public string GetCompanyCodeTypesByCompanyNumber(string companyNo)
        {
            return LookupsEF.GetCompanyCodeTypesByCompanyNumber(companyNo);
        }

        [WebMethod]
        public string GetAttributesByCompanyNumber(string companyNumber)
        {
            return Lookups.GetAttributesByCompanyNumber(companyNumber);
        }

        [WebMethod]
        public string GetCompanyCodesByCompanyCodeType(string codeName, string companyNo)
        {
            return Lookups.GetCompanyCodesByCompanyCodeType(codeName, companyNo);
        }

        [WebMethod]
        public string GetSubmissionTypes()
        {
            object o = Common.GetFromCache("SubmissionTypes");
            if (o != null)
            {
                return (string)o;
            }
            else
                return Lookups.GetSubmissionTypes();
        }
        [WebMethod]
        public string GetSubmissionTypesByCompanyNumber(string companyNumber)
        {
            object o = Common.GetFromCache("SubmissionTypes");
            if (o != null)
            {
                return (string)o;
            }
            else
                return Lookups.GetSubmissionTypes(companyNumber);
        }

        [WebMethod]
        public string GetSubmissionStatuses()
        {
            object o = Common.GetFromCache("SubmissionStatuses");
            if (o != null)
            {
                return (string)o;
            }
            else
                return Lookups.GetSubmissionStatuses();
        }
        [WebMethod]
        public string GetSubmissionStatusReasons()
        {
            return Lookups.GetSubmissionStatusReasons(null);
        }

        [WebMethod]
        public string GetSubmissionStatusReasonsForStatus(string statusCode)
        {
            return Lookups.GetSubmissionStatusReasons(statusCode);
        }

        [WebMethod]
        public string GetSubmissionStatusesByCompanyNumber(string companyNumber)
        {
            return Lookups.GetSubmissionStatuses(companyNumber);
        }
        [WebMethod]
        public string GetSubmissionStatusReasonsByCompanyNumberAndStatusCode(string companyNumber, string statusCode)
        {
            return Lookups.GetSubmissionStatusReasonsByCompanyNumberAndStatusCode(companyNumber, statusCode);
        }

        [WebMethod]
        public string GetCCCities()
        {
            object o = Common.GetFromCache("Cities");
            if (o != null)
            {
                return (string)o;
            }
            else
            {
                return Common.getUtililtyWebService().getAllCities();
            }
        }

        [WebMethod]
        public string GetCCCompanies(string effectiveDate)
        {
            return Common.getUtililtyWebService().getAllCompanies(Common.ConvertToClientCoreDateTime(effectiveDate));
        }
        

        [WebMethod]
        public string GetCCCorporations(string effectiveDate)
        {
            object o = Common.GetFromCache("Corporations");
            if (o != null)
            {
                return (string)o;
            }
            else
            {
                return Common.getUtililtyWebService().getAllCorporations(Common.ConvertToClientCoreDateTime(effectiveDate));
            }
        }

        [WebMethod]
        public string GetCCCounties()
        {
            object o = Common.GetFromCache("Counties");
            if (o != null)
            {
                return (string)o;
            }
            else
            {
                return Common.getUtililtyWebService().getAllCounties();
            }
        }

        [WebMethod]
        public string GetCCCountries()
        {
            object o = Common.GetFromCache("Countries");
            if (o != null)
            {
                return (string)o;
            }
            else
            {
                return Common.getUtililtyWebService().getAllCountries();
            }
        }

        [WebMethod]
        public string GetCCStates()
        {
            return Common.getUtililtyWebService().getAllStates();
        }


        [WebMethod]
        public string GetCCCompaniesOfCorp(string effectiveDate, int corpId)
        {
            return Common.getUtililtyWebService().getCompaniesOfCorp(
                Common.ConvertToClientCoreDateTime(effectiveDate), corpId);
        }

        [WebMethod]
        public string GetAddressTypes()
        {
            object o = Common.GetFromCache("AddressTypes");
            if (o != null)
            {
                return (string)o;
            }
            else
            {
                return Lookups.GetAddressTypes();
            }
        }

        [WebMethod]
        public string GetAgencies(string companyNumber)
        {
            return Lookups.GetAgencies(companyNumber);
        }

        [WebMethod]
        public string GetAgents(string companyNumber)
        {
            return Lookups.GetAgents(companyNumber);
        }

        [WebMethod]
        public string GetAgentsByAgencyNumber(string companyNumber, string agencyNumber)
        {
            return Lookups.GetAgentsByAgencyNumber(companyNumber, agencyNumber);
        }

        [WebMethod]
        public string GetAgencyContactsByAgencyNumber(string companyNumber, string agencyNumber)
        {
            return Lookups.GetContactsByAgencyNumber(companyNumber, agencyNumber);
        }

        [WebMethod]
        public string GetClientNameTypes()
        {
            return Lookups.GetClientNameTypes();
        }

        [WebMethod]
        public string GetGeographicDirectional()
        {
            return Lookups.GetGeographicDirectional();
        }

        [WebMethod]
        public string GetLOBsByCompanyNumber(string companyNumber)
        {
            return Lookups.GetLOBsByCompanyNumber(companyNumber);
        }

        [WebMethod]
        public string GetNamePrefixes()
        {
            return Lookups.GetPrefixes();
        }

        [WebMethod]
        public string GetSuffixTitles()
        {
            return Lookups.GetSuffixes();
        }

        [WebMethod]
        public string GetStreetSuffixes()
        {
            return Lookups.GetStreetSuffixes();
        }

        [WebMethod]
        public string GetCompanyNumbersByCompanyName(string companyName)
        {
            return Lookups.GetCompanyNumbers(companyName);
        }

         [WebMethod]
        public string GetCompanyNumbersByCompanyId(int companyId)
        {
            return Lookups.GetCompanyNumbers(companyId);
        }        

        [WebMethod]
        public string GetAgencyLOBsByAgency(int agencyId)
        {
            return Lookups.GetAgencyLOBsByAgency(agencyId);
        }

        //[WebMethod]
        //public string GetPersonsByAgencyLOB(int agencyId, int LobId)
        //{
        //    return Lookups.GetPersonsByAgencyLOB(agencyId, LobId);
        //}

        [WebMethod]
        public string GetStates()
        {
            object o = Common.GetFromCache("States");
            if (o != null)
            {
                return (string)o;
            }
            else
            {
                return Lookups.GetStates();
            }
        }

        [WebMethod]
        public string GetCompanies()
        {
            object o = Common.GetFromCache("Companies");
            if (o != null)
            {
                return (string)o;
            }
            else
            {
                return Lookups.GetCompanies();
            }
        }

        [WebMethod]
        public string GetCompanyNumbers()
        {
            object o = Common.GetFromCache("CompaniesNumbers");
            if (o != null)
            {
                return (string)o;
            }
            else
            {
                return Lookups.GetCompanyNumbers();
            }
        }

        [WebMethod]
        public string GetUnderwritingRoles()
        {
            object o = Common.GetFromCache("UnderwritingRoles");
            if (o != null)
            {
                return (string)o;
            }
            else
            {
                return Lookups.GetUnderwritingRoles();
            }
        }

        [WebMethod]
        public string GetPersonsByAgencyLOBUnderwritingRole(int agencyId, int lobId, int uroleId)
        {
            return Lookups.GetPersonsByAgencyLOBUnderwritingRole(agencyId, lobId, uroleId);
        }

        [WebMethod]
        public string GetPersonsByAgencyLOB(int agencyId, int lobId)
        {
            return Lookups.GetPersonsByAgencyLOBUnderwritingRole(agencyId, lobId);
        }

        [WebMethod]
        public string GetPersonsByInitialsAndCompanyNumber(string initials, string companyNumber)
        {
            return Lookups.GetPersonsByInitialsAndCompanyNumber(initials, companyNumber);
        }

        [WebMethod]
        public int GetPersonsByUsernameAndCompanyNumber(string username, string companyNumber)
        {
            return Lookups.GetPersonsByUsernameAndCompanyNumber(username, companyNumber);
        }

        [WebMethod]
        public string GetAgencyById(int id)
        {
            return Lookups.GetAgencyById(id);
        }

        [WebMethod]
        public string GetAgentById(int id)
        {
            return Lookups.GetAgentById(id);
        }

        [WebMethod]
        public string GetCompanyById(int id)
        {
            return Lookups.GetCompanyById(id);
        }

        [WebMethod]
        public string GetPersonById(int id)
        {
            return Lookups.GetPersonById(id);
        }

        [WebMethod]
        public string GetCompanyNumberCodeByCompanyNumberCodeId(int id)
        { return Lookups.GetCompanyNumberCodeByCompanyNumberCodeId(id); }

        [WebMethod]
        public string GetCompanyNumberCodeTypeByCodeTypeId(int id)
        {
            return Lookups.GetCompanyNumberCodeTypeByCodeTypeId(id);
        }

        [WebMethod]
        public string GetLOBById(int id)
        {
            return Lookups.GetLOBById(id);
        }

        [WebMethod]
        public string GetNumberControlTypesByCompanyNumber(string companyNumber)
        {
            Lookups ll = new Lookups();
            return ll.GetNumberControlTypesByCompanyNumber(companyNumber);
        }

        [WebMethod]
        public string GetNextNumberControlValueByCompanyNumberAndTypeName(string companyNumber, string typeName, int incrementBy)
        {
            Lookups ll = new Lookups();
            return ll.GetNextNumberControlValueByCompanyNumberAndTypeName(companyNumber, typeName, incrementBy);
        }

        [WebMethod]
        public string GetPolicySystems(string companyNumber)
        {
            Lookups ll = new Lookups();
            return ll.GetPolicySystems(companyNumber);
        }

        [WebMethod]
        public string GetPolicySystem(int id)
        {
            Lookups ll = new Lookups();
            return ll.GetPolicySystem(id);
        }

        [WebMethod]
        public int GetCompanyIdbyCompanyNumber(string companyNumber)
        {
            Lookups ll = new Lookups();
            return ll.GetCompanyIdbyCompanyNumber(companyNumber);
        }

        [WebMethod]
        public int GetAgencyIdByCompanyNumberIdAgencyNumber(int companyNumberId, string agencyNumber)
        {
            Lookups ll = new Lookups();
            return ll.GetAgencyIdByCompanyNumberIdAgencyNumber(companyNumberId, agencyNumber);
        }

        [WebMethod]
        public int GetAgencyIdByCompanyNumberAgencyNumber(string companyNumber, string agencyNumber)
        {
            Lookups ll = new Lookups();
            return ll.GetAgencyIdByCompanyNumberAgencyNumber(companyNumber, agencyNumber);
        }


        [WebMethod]
        public string GetAssignment(string companyNumber, string agencyNumber, string underwritingUnitCode, string roleCode, bool isPrimary)
        {
            Lookups ll = new Lookups();
            return ll.GetAssignment(companyNumber, agencyNumber, underwritingUnitCode, roleCode, isPrimary);
        }

        [WebMethod]
        public string GetHazardGradesByClassCode(string companyNumber, string classCode)
        {
            Lookups ll = new Lookups();
            return ll.GetHazardGradesByClassCode(companyNumber, classCode);
        }

        [WebMethod]
        public string GetOtherCarriersByCompanyNumber(string companyNumber)
        {
            Lookups ll = new Lookups();
            return ll.GetOtherCarriersByCompanyNumber(companyNumber);
        }
    }

}