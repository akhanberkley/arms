using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;

using BTS.LogFramework;

namespace BCS.WebApp.Services 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		public Global()
		{
			InitializeComponent();
            BTS.WFA.BCS.Application.Initialize<BTS.WFA.BCS.Data.SqlDb>();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{

		}
 
		protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{
//			if (HttpContext.Current.Request.IsAuthenticated)
//			{
//				System.Diagnostics.Debug.WriteLine(User.Identity.Name);
//			}
		}

		protected void Application_Error(Object sender, EventArgs e)
		{
            string pageUrl = HttpContext.Current.Request.Path;
            Exception errorInfo = HttpContext.Current.Server.GetLastError();
            HttpContext.Current.Server.ClearError();

            string message = "Url: " + pageUrl + Environment.NewLine + " Error: " + errorInfo.ToString();
            HttpContext.Current.Session["Application_Error"] = message;

            LogCentral.Current.LogFatal(message, errorInfo);

            Response.Redirect("Error.aspx?page=" +
                HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.Url.ToString()));
		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

