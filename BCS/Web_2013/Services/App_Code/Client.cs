#region Using
using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;

using BCS.Biz;
using BCS.Core;
using BCS.Core.Clearance;
using BCS.WebApp.Services.Components;

using BTS.ClientCore.Wrapper;
using structures = BTS.ClientCore.Wrapper.Structures; 
#endregion

namespace BCS.WebApp.Services
{
    /// <summary>
    /// Summary description for Client
    /// </summary>
    [WebService(Namespace="http://intranet.wrbts.ads.wrberkley.com/BCS/Client")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1, EmitConformanceClaims = true)]
    public class Client : System.Web.Services.WebService
    {
        #region Constructor
        public Client()
        {
            //Uncomment the following line if using designed components 
            //InitializeComponent();

            
        } 
        #endregion
        
        #region Get Client Methods
        [WebMethod(Description = "Gets business clients by name")]
        public string GetClientByBusinessName(string companyNumber, string businessName)
        {
            return Clients.GetClientByBusinessName(companyNumber, businessName);
        }

        [WebMethod(Description = "Gets business clients by name")]
        public string GetClient(string companyNumber, string taxid, string houseNo, string address1,
            string city, string state, string zip, string firstname, string lastname, string businessname)
        {
            return Clients.GetClient(companyNumber, taxid, houseNo, address1,
             city, state, zip, firstname, lastname, businessname);
        }

        [WebMethod(Description = "Gets clients by tax id")]
        public string GetClientByTaxId(string companyNumber, string taxid)
        {
            return Clients.GetClientByTaxId(companyNumber, taxid);
        }

        [WebMethod(Description = "Gets clients by client core clientid")]
        public string GetClientById(string companyNumber, string id)
        {
            return Clients.GetClientById(companyNumber, id, true, false);
        }

        [WebMethod(Description = "Gets clients by client core clientid")]
        public string GetClientByIdForSearchScreen(string companyNumber, string id, bool isSearchScreen)
        {
            return Clients.GetClientById(companyNumber, id, true, isSearchScreen);
        }


        [WebMethod(Description = "Gets clients by client core clientid exludes the information from the client core.")]
        public string GetClientByIdExcludeClientCoreInfo(string companyNumber, string id)
        {
            return Clients.GetClientById(companyNumber, id, false, false);
        }

        [WebMethod(Description = "Gets clients by client core clientid exludes the information from the client core.")]
        public string GetClientByIdExcludeClientCoreInfoForSearchScreen(string companyNumber, string id, bool isSearchScreen)
        {
            return Clients.GetClientById(companyNumber, id, false, isSearchScreen);
        }

        [WebMethod]
        public string GetClientsByAddress(string companyNumber, string houseNo, string address1, string address2, string address3,
            string address4, string city, string state, string county, string country, string zip)
        {
            return Clients.GetClientsByAddress(companyNumber, 
                houseNo, address1, address2, address3, address4, city, state, county, country, zip);
        }

        [WebMethod(Description = "Gets clients by name")]
        public string GetClientsByName(string companyNumber, string first, string last)
        {
            return Clients.GetClientsByName(companyNumber, first, last);
        }

        [WebMethod(Description = "Gets clients by portfolio id")]
        public string GetClientsByPortfolioId(string companyNumber, string portfolioId)
        {
            return Clients.GetClientsByPortfolioId(companyNumber, portfolioId);
        }

        [WebMethod(Description = "Gets clients by portfolio name")]
        public string GetClientsByPortfolioName(string companyNumber, string portfolioname)
        {
            return Clients.GetClientsByPortfolioName(companyNumber, portfolioname);
        } 
        #endregion

        #region Add Client Methods

        [WebMethod(Description = "Adds a client with single address, group and communication values.")]
        public string AddAClient(string companyNumber, 
            string effectiveDate,
            string corpId,
            string taxId,
            string taxIdType,
            string membershipGroup,
            string membershipFlag,
            string category,
            string status,
            string type,
            string maritalstatus,
            string driverLicenseNo,
            string driverLicenseCountry,
            string driverLicenseState,
            string gender,
            string creditRating,
            string birthDate,
            string communicationType, string communicationValue,
            string nameFirst, string nameLast, string nameMiddle, string namePrefix, string namePrefixCode,
            string nameSuffix, string nameSuffixCode, string nameType, bool primaryName,
            string houseNo, string address1, string address2, string address3, string address4,
            string countryCode, string stateProvCode, string city, string county, int postalCode,
            string groupAccessKeyId, bool matches)
        {
            if (!Core.Utility.GeneralFunctions.IsDateTime(effectiveDate))
            {
                throw new ArgumentException("effective date is an invalid date");
            }
            if (!Core.Utility.GeneralFunctions.IsDateTime(birthDate))
            {
                throw new ArgumentException("birth date is an invalid date");
            }

            return
                Clients.AddClient(companyNumber, DateTime.Parse(effectiveDate), corpId, taxId, taxIdType, membershipGroup, membershipFlag, category,
                status, type, maritalstatus, driverLicenseNo, driverLicenseCountry, driverLicenseState, gender,
                creditRating, DateTime.Parse(birthDate), communicationType, communicationValue, nameFirst, nameLast, nameMiddle, namePrefix,
                namePrefixCode, nameSuffix, nameSuffixCode, nameType, primaryName, houseNo, address1, address2,
                address3, address4, countryCode, stateProvCode, city, county, postalCode, groupAccessKeyId, matches);
        }

        [WebMethod(Description = "Adds a client with single address, group and communication values.")]
        public string AddABusinessClient(string companyNumber, 
            string effectiveDate,
            string corpId,
            string taxId,
            string taxIdType,
            string membershipGroup,
            string membershipFlag,
            string category,
            string status,
            string type,
            string maritalstatus,
            string driverLicenseNo,
            string driverLicenseCountry,
            string driverLicenseState,
            string gender,
            string creditRating,
            string birthDate,
            string communicationType, string communicationValue,
            string nameBusiness, string nameType, bool primaryName,
            string houseNo, string address1, string address2, string address3, string address4,
            string countryCode, string stateProvCode, string city, string county, int postalCode,
            string groupAccessKeyId, bool matches)
        {
            if (!Core.Utility.GeneralFunctions.IsDateTime(effectiveDate))
            {
                throw new ArgumentException("effective date is an invalid date");
            }
            if (birthDate.Length == 0)
                birthDate = DateTime.MinValue.ToString();
            if (!Core.Utility.GeneralFunctions.IsDateTime(birthDate))
            {
                throw new ArgumentException("birth date is an invalid date");
            }
            

            return
                Clients.AddBusinessClient(companyNumber, DateTime.Parse(effectiveDate), corpId, taxId, taxIdType, membershipGroup, membershipFlag, category,
                status, type, maritalstatus, driverLicenseNo, driverLicenseCountry, driverLicenseState, gender,
                creditRating, DateTime.Parse(birthDate), communicationType, communicationValue, nameBusiness, 
                nameType, primaryName, houseNo, address1, address2,
                address3, address4, countryCode, stateProvCode, city, county, postalCode, groupAccessKeyId, matches);
        }

        [WebMethod(Description = "Adds a client.")]
        public string AddClient(string companyNumber, string clientXml, bool matches)
        {
            return Clients.AddClient(companyNumber, clientXml, matches);
        } 
        #endregion

        #region Modify Client Methods

        [WebMethod(Description = "Client Modification.")]
        public string ModifyClient(string clientid, string companyNumber, string portfolioid, string category, string clientType, string effectiveDate,
            string owner,string status, string taxid, string taxidtype,
            string nameversions, string addressversions, string cocodeids, string attributes, string classCodes,
            int siccodeid,int naicscode, string phoneNumber, string businessdesc, string by)
        {
            int dup = 1;
            return Clients.ModifyClient(clientid, companyNumber, portfolioid, category, clientType, effectiveDate, owner, status, "N",
                taxid, taxidtype, dup, nameversions, addressversions, cocodeids, attributes, classCodes, siccodeid,naicscode, phoneNumber, businessdesc, by, false);
        }

        [WebMethod(Description = "Client Add with Import Method. clientType and owner are now derived from company table. Need to remove those.")]
        public string AddClientWithImport(string companyNumber, string category, string clientType,
          string effectiveDate, string owner, string status, string vendorFlag, string taxid,
          string taxidtype, int dupliateClientOption, string nameversions, string addressversions, string cocodeids, string attributes,
           string classCodes, int siccodeid,int naicscodeid, string phoneNumber, string businessdesc, string by)
        {
            return Clients.AddClientWithImport(companyNumber, category, effectiveDate, status, vendorFlag,
                taxid, taxidtype, dupliateClientOption, nameversions, addressversions, cocodeids, attributes, classCodes, siccodeid,naicscodeid, phoneNumber,businessdesc,
                by,false);
        }

        [WebMethod(Description = "Modify Membership information for a client (ARMS)")]
        public string ModifyClientMembershipInfo(string companyNumber, string clientId, string membershipGroup)
        {
            return Clients.ModifyClientMembershipInfo(companyNumber, clientId, membershipGroup);
        }

        #endregion

        #region Other Client Methods
        [WebMethod(Description = "Gets maximum values of each hazard grades associated with class codes assigned to a client for a company number.")]
        public string GetMaxHazardGrade(string companyNumber, string clientId)
        {
            return Clients.GetMaxHazardGrade(companyNumber, clientId);
        } 
        #endregion

        #region Product Relation Methods

        [WebMethod(Description = "Gets product relations for a given client")]
        public string GetProductRelations(string companyNumber, string clientId)
        {
            return Clients.GetClientProductRelation(companyNumber, clientId);
        }

        #endregion

        [WebMethod]
        public string UpdatePortfolio(string companyNumber, string portfolioId, string newPortfolioName)
        {
            return Clients.UpdatePortFolio(companyNumber, portfolioId, newPortfolioName);
        }
    }
}