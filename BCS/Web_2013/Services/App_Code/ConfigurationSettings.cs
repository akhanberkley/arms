using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using com.bts.aps.domain;

namespace BCS.WebApp.Services
{
    class ConfigurationSettings
    {
        private static ConfigurationSettings m_objInstance = null;

        private ConfigurationSettings() {}

        public static ConfigurationSettings Instance
        {
            get
            {
                if (m_objInstance == null)
                    m_objInstance = new ConfigurationSettings();
                return m_objInstance;
            }
        }

        internal System.Collections.Specialized.StringDictionary GetCommunicationDictionary()
        {
            String commValues = ConfigurationManager.AppSettings["CommunicationValues"];
            String[] sa = commValues.Split('|');
            
            StringDictionary sd = new StringDictionary();
            foreach (string var in sa)
            {
                string[] nameValue = var.Split(':');
                sd.Add(nameValue[0], nameValue[1]);
            }
            return sd;
        }
        internal string[] GetMappedRoleType(AgencyPersonnelRole[] agencyPersonnelRoles)
        {
            String agencyRoles = ConfigurationManager.AppSettings["AgencyPersonRoles"];
            String[] sa = agencyRoles.Split('|');

            StringDictionary sd = new StringDictionary();
            foreach (string var in sa)
            {
                string[] nameValue = var.Split(':');
                sd.Add(nameValue[0], nameValue[1]);
            }

            List<string> sc = new List<string>();
            foreach (AgencyPersonnelRole var in agencyPersonnelRoles)
            {
                if (sd.ContainsKey(var.agencyPersonnelRoleType.label))
                {
                    sc.Add(sd[var.agencyPersonnelRoleType.label]);
                }
            }
            return sc.ToArray();
        }
    }
}
