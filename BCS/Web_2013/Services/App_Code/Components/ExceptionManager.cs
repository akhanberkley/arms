using System;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Configuration;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mail;
using System.Data.SqlTypes;
using System.Collections.Specialized;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Security.Principal;
using NameValueCollection = System.Collections.Specialized.NameValueCollection;

namespace BCS.WebApp.Services.Components
{
    public sealed class ExceptionManager
    {
        /// <summary>
        /// Notifying Failure Messages
        /// </summary>
        /// <param name="subject">string</param>
        /// <param name="body">string</param>
        /// <param name="priority">MailPriority</param>
        public static void Notify(string subject, string body, MailPriority priority)
        {
            MailAddress messageFrom = new MailAddress(DefaultValues.MailFrom);
            MailAddress messageTo = new MailAddress(DefaultValues.MailTo);

            // Create a message and set up the recipients.
            MailMessage message = new MailMessage(messageFrom, messageTo);

            string ccAddresses = DefaultValues.MailCC;
            string[] ccAddrArray = ccAddresses.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string var in ccAddrArray)
            {
                string trimmedVar = var.Trim();
                if (OrmLib.Email.ValidateEmail(trimmedVar))
                    message.CC.Add(trimmedVar);
            }

            SmtpClient client = new SmtpClient(DefaultValues.MailServer);

            //message.Subject = string.Format(DefaultValues.MailSubject, emailType);
            message.Subject = subject;
            message.IsBodyHtml = false;
            message.Priority = priority;

            message.Body = body;

            client.Send(message);
        }

        #region FormatException

        /// <summary>
        /// Returns a formatted, multi-line string containing complete details of the 
        /// specified exception and additional information.
        /// </summary>
        /// <param name="exception">Exception to evaluate.</param>
        /// <param name="additionalInfo">Additional information about the exception.  Null is allowed.</param>
        /// <returns>String containing the exception details.</returns>
        public static string GetFullDetailsString(Exception exception)
        {
            const string TEXT_SEPARATOR = "----------------------------------------------------------------------";

            // create a StringBuilder to hold the exception details
            StringBuilder sb = new StringBuilder();


            #region -- Record Contents of AdditionalInfo Collection ---

            // record the contents of the AdditionalInfo collection.
            NameValueCollection additionalInfo = null;
            additionalInfo = GetAdditionalAppInfo();

            if (additionalInfo != null)
            {
                // record general information.
                sb.Append(TEXT_SEPARATOR + "\r\n");
                sb.AppendFormat("General Information\r\n");
                sb.Append(TEXT_SEPARATOR + "\r\n");

                foreach (string str in additionalInfo)
                {
                    sb.AppendFormat("{0}: {1}\r\n", str, additionalInfo[str]);
                }

                sb.Append(TEXT_SEPARATOR + "\r\n");
            }

            #endregion

            #region --- Record Exception Details ---

            // see if an exception object was passed
            if (exception == null) // no exception passed
            {
                sb.AppendFormat("{0}{0}No Exception object has been provided.{0}", "\r\n");
            }
            else // an exception object was passed
            {
                // build a stack of the exception chain (so the root exception is on top)
                Stack stack = new Stack();
                Exception ex = exception;
                while (ex != null)
                {
                    stack.Push(ex);
                    ex = ex.InnerException;
                }

                // pop each exception off the stack and add the details to the string builder.
                int count = 0;
                while (stack.Count > 0)
                {
                    // get the next exception in the stack; increment the count
                    ex = (Exception)stack.Pop();
                    count += 1;

                    // write title information for the exception object.
                    sb.Append("\r\n\r\n");
                    sb.Append(TEXT_SEPARATOR + "\r\n");
                    sb.AppendFormat("{0}) {1}:  {2}\r\n", count, ex.GetType().FullName, ex.Message);
                    sb.Append(TEXT_SEPARATOR + "\r\n");

                    #region --- Record All Public Properties ---

                    // Loop through the public properties of the exception object and record their value.
                    PropertyInfo[] aryPublicProperties = ex.GetType().GetProperties();
                    foreach (PropertyInfo p in aryPublicProperties)
                    {
                        // Do not log information for the Message, InnerException, or StackTrace properties. 
                        // This information is captured later in the process.
                        if (p.Name != "Message" && p.Name != "InnerException" && p.Name != "StackTrace")
                        {
                            if (p.GetValue(ex, null) == null)
                            {
                                sb.AppendFormat("{0}: NULL\r\n", p.Name);
                            }
                            else
                            {
                                // Loop through the collection of AdditionalInformation if the exception type is a BaseApplicationException.
                                if (p.Name == "AdditionalInformation")
                                {
                                    // Verify the collection is not null.
                                    if (p.GetValue(ex, null) != null)
                                    {
                                        // Cast the collection into a local variable.
                                        NameValueCollection currentAdditionalInfo = (NameValueCollection)p.GetValue(ex, null);

                                        // Check if the collection contains values.
                                        if (currentAdditionalInfo.Count > 0)
                                        {
                                            sb.Append("AdditionalInformation:\r\n");

                                            // Loop through the collection adding the information to the string builder.
                                            for (int i = 0; i < currentAdditionalInfo.Count; i++)
                                            {
                                                sb.AppendFormat("{0}: {1}", currentAdditionalInfo.GetKey(i), currentAdditionalInfo[i]);
                                            }
                                        }
                                    }
                                }
                                else // Otherwise just write the ToString() value of the property.
                                {
                                    sb.AppendFormat("{0}: {1}\r\n", p.Name, p.GetValue(ex, null));
                                }
                            }
                        }
                    }
                    #endregion

                    // record the StackTrace with separate label.
                    if (ex.StackTrace != null)
                    {
                        sb.Append("\r\n");
                        sb.Append("StackTrace:\r\n");
                        sb.Append("-----------\r\n");
                        sb.Append(ex.StackTrace + "\r\n");
                    }

                    // finish with another seperator
                    sb.Append(TEXT_SEPARATOR + "\r\n");
                }
            }

            #endregion


            // return the exception details as a string
            return sb.ToString();
        }
        #endregion

        #region Load the AdditionalInformation Collection with environment data.

        private static NameValueCollection GetAdditionalAppInfo()
        {

            // Create the Additional Information collection if it does not exist.
            NameValueCollection additionalInfo = new NameValueCollection();

            // Add environment information to the information collection.
            try
            {
                additionalInfo.Add("MachineName", Environment.MachineName);
            }
            catch
            {
                //additionalInfo.Add(EXCEPTIONMANAGER_NAME + ".MachineName", resourceManager.GetString("RES_EXCEPTIONMANAGEMENT_INFOACCESS_EXCEPTION"));
            }

            try
            {
                additionalInfo.Add("TimeStamp:", DateTime.Now.ToString());
            }
            catch
            {
                //additionalInfo.Add(EXCEPTIONMANAGER_NAME + ".TimeStamp", resourceManager.GetString("RES_EXCEPTIONMANAGEMENT_INFOACCESS_EXCEPTION"));
            }

            try
            {
                additionalInfo.Add("ThreadIdentity:", Thread.CurrentPrincipal.Identity.Name);
            }
            catch
            {
                //additionalInfo.Add(EXCEPTIONMANAGER_NAME + ".ThreadIdentity", resourceManager.GetString("RES_EXCEPTIONMANAGEMENT_INFOACCESS_EXCEPTION"));
            }

            try
            {
                additionalInfo.Add("WindowsIdentity", WindowsIdentity.GetCurrent().Name);
            }
            catch
            {
                //additionalInfo.Add(EXCEPTIONMANAGER_NAME + ".WindowsIdentity", resourceManager.GetString("RES_EXCEPTIONMANAGEMENT_INFOACCESS_EXCEPTION"));
            }

            return additionalInfo;
        }

        #endregion
    }
}
