using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Timers;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using Microsoft.Win32;

using Microsoft.Web.Services2;
using Microsoft.Web.Services2.Security;
using Microsoft.Web.Services2.Security.Tokens;

namespace BCS.WebApp.Services.Components
{
    /// <summary>
    /// Summary description for Globals.
    /// </summary>
    public sealed class Globals
    {
        /// <summary>
        /// Private ctor
        /// </summary>
        private Globals() { }

        /// <summary>
        /// Date Format
        /// </summary>
        public static string DateFormat = "yyyyMMdd";

        /// <summary>
        /// Retrieve appsettings string from the web.config file
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetApplicationSetting(string key)
        {
            if (key != null && key.Length != 0)
            {
                try
                {
                    if (ConfigurationManager.AppSettings[key] != null)
                    {
                        return ConfigurationManager.AppSettings[key].ToString();
                    }
                    else
                    {
                        throw new ArgumentException("ConfigurationManager.AppSettings[key] is null.", "key");
                    }
                }
                catch (Exception ex)
                {
                    string returnMsg = "Caught exception in Globals.GetApplicationSetting trying to retrieve the requested application key from the Web.config file";
                    BTS.LogFramework.LogCentral.Current.LogError(returnMsg, ex);

                    return returnMsg;
                }
            }
            else
            {
                throw new ArgumentException("key equals an empty string.", "key");
            }
        }

        /// <summary>
        /// Retrieve ConnectionString string from the web.config file
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConnectionString(string key)
        {
            if (key != null && key.Length != 0)
            {
                try
                {
                    if (ConfigurationManager.ConnectionStrings[key] != null)
                    {
                        return ConfigurationManager.ConnectionStrings[key].ToString();
                    }
                    else
                    {
                        throw new ArgumentException("ConfigurationManager.ConnectionStrings[key] is null.", "key");
                    }
                }
                catch (Exception ex)
                {
                    string returnMsg = "Caught exception in Globals.GetApplicationSetting trying to retrieve the requested connection string key from the Web.config file";
                    BTS.LogFramework.LogCentral.Current.LogError(returnMsg, ex);

                    return returnMsg;
                }
            }
            else
            {
                throw new ArgumentException("key equals an empty string.", "key");
            }
        }

        /// <summary>
        /// ValidateSoapRequest
        /// </summary>
        /// <param name="requestContext"></param>
        /// <returns></returns>
        public static bool ValidateSoapRequest(SoapContext requestContext)
        {
            // okay, until provide guilty
            bool isValidSoapRequest = true;

            if (requestContext == null)
            {
                BTS.LogFramework.LogCentral.Current.LogError("SoapContext is null. Only SOAP requests are permitted.");
                isValidSoapRequest = false;
            }
            else
            {
                if (requestContext.Security == null)
                {
                    BTS.LogFramework.LogCentral.Current.LogError("SoapContext.Security is null. SOAP security header is invalid, or doesn't exist.");
                    isValidSoapRequest = false;
                }
                else
                {

                    if (requestContext.Security.Tokens.Count <= 0)
                    {
                        BTS.LogFramework.LogCentral.Current.LogError("No SoapContext.Security.Tokens attached to the request.");
                        isValidSoapRequest = false;
                    }
                }
            }

            return isValidSoapRequest;
        }

    }
}
