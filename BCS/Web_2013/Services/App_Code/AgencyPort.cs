using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using clearance = BCS.Core.Clearance;

using structures = BTS.ClientCore.Wrapper.Structures;
using System.Collections.Generic;
using BCS.Biz;

namespace BCS.WebApp.Services
{
    /// <summary>
    /// Summary description for AgencyPort
    /// </summary>
    [WebService(Namespace = "http://intranet.wrbts.ads.wrberkley.com/BCS/AgencyPort")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class AgencyPort : System.Web.Services.WebService
    {

        public AgencyPort()
        { }

        //[WebMethod]
        //public string Submit(string agencyLocationNumber, string clientId, string customerName, string customerDBA, string customerLegalEntity,
        //    string customerAddressXML, string customerPhone, string taxId,
        //    string companyNumber, string callType, string matchType, int workItemId, string submissionXML)
        //{
        //    BCS.Core.Clearance.CallType iCallType = (BCS.Core.Clearance.CallType)Enum.Parse(typeof(BCS.Core.Clearance.CallType), callType);

        //    clearance.AgencyPortStructures.AddressType customerAddress = null;
        //    if (!string.IsNullOrEmpty(customerAddressXML))
        //    {
        //        customerAddress = (clearance.AgencyPortStructures.AddressType)
        //            Core.XML.Deserializer.Deserialize(customerAddressXML,
        //            typeof(clearance.AgencyPortStructures.AddressType));
        //    }

        //    BCS.Core.Clearance.MatchType iMatchType = BCS.Core.Clearance.MatchType.AddressOrName;
        //    if (matchType.Equals("AND", StringComparison.CurrentCultureIgnoreCase))
        //    {
        //        iMatchType = BCS.Core.Clearance.MatchType.AddressAndName;
        //    }

        //    clearance.AgencyPort agencyPort = new clearance.AgencyPort();
        //    clearance.BizObjects.Response resAgencyPort = agencyPort.Submit(
        //        agencyLocationNumber, clientId, customerName, customerDBA, customerLegalEntity, customerAddress, customerPhone, taxId,
        //        companyNumber, iCallType, iMatchType, workItemId, submissionXML);
        //    return Core.XML.Serializer.SerializeAsXml(resAgencyPort);
        //}

        [WebMethod]
        public string Submit(string agencyLocationNumber, string clientId, string customerName, string customerDBA, string customerLegalEntity,
            string customerAddressXML, string customerPhone, string taxId,
            string companyNumber, string callType, string matchType, int workItemId, string submissionXML,bool checkMultipleSubmissions,string policyType)
        {
            BCS.Core.Clearance.CallType iCallType = (BCS.Core.Clearance.CallType)Enum.Parse(typeof(BCS.Core.Clearance.CallType), callType);

            clearance.AgencyPortStructures.AddressType customerAddress = null;
            if (!string.IsNullOrEmpty(customerAddressXML))
            {
                customerAddress = (clearance.AgencyPortStructures.AddressType)
                    Core.XML.Deserializer.Deserialize(customerAddressXML,
                    typeof(clearance.AgencyPortStructures.AddressType));
            }

            BCS.Core.Clearance.MatchType iMatchType = BCS.Core.Clearance.MatchType.AddressOrName;
            if (matchType.Equals("AND", StringComparison.CurrentCultureIgnoreCase))
            {
                iMatchType = BCS.Core.Clearance.MatchType.AddressAndName;
            }

            clearance.AgencyPort agencyPort = new clearance.AgencyPort();
            clearance.BizObjects.Response resAgencyPort = agencyPort.SubmitNew(
                agencyLocationNumber, clientId, customerName, customerDBA, customerLegalEntity, customerAddress, customerPhone, taxId,
                companyNumber, iCallType, iMatchType, workItemId, submissionXML,checkMultipleSubmissions,policyType);
            return Core.XML.Serializer.SerializeAsXml(resAgencyPort);
        }

        [WebMethod]
        public string Search(string companyNumber, string agencyNumber, string dba, string state, string clientName)
        {
            clearance.AgencyPort agencyPort = new clearance.AgencyPort();
            return agencyPort.SearchClients(companyNumber, agencyNumber, dba, state, clientName, false);
        }

        [WebMethod]
        public List<long> GetClientIdsByAgencyNumber(string companyNumber, string agencyNumber)
        {
            return clearance.AgencyPort.GetAllClientIdsForAgency(companyNumber, agencyNumber);
        }

        #region utility methods
        [WebMethod]
        public string ConvertToAddressXML(string addr1, string addr2, string addr3, string addr4, string country, string houseNumber, string city, string county, string state, string zip)
        {
            BCS.Core.Clearance.AgencyPortStructures.AddressType address = new BCS.Core.Clearance.AgencyPortStructures.AddressType();
            address.ADDR1 = addr1;
            address.ADDR2 = addr2;
            address.ADDR3 = addr3;
            address.ADDR4 = addr4;
            address.Country = country;
            address.HouseNumber = houseNumber;
            address.Item = city;
            address.Item1 = county;
            address.State = state;
            address.Zip=zip;

            return Core.XML.Serializer.SerializeAsXml(address);

        }

        [WebMethod]
        public string ConvertToSubmissionXML(string companyNumber, string agencyNumber, string analystName, string comments, string dba,
            DateTime effDate, DateTime expDate, string insuredName, decimal premium, string policyNumber,string companyNumberCodeIds,string submissionAttributes, string submissionBy,
            DateTime submissionDt, string statusCode, DateTime statusDate, string reasonCode, string otherReason,
            string typeName, string underwriterName, string updatedBy, DateTime updateDt, string lobCode, string agentFN, string agentLN)
        {
            string separator = "|";
                        
            BCS.Core.Clearance.BizObjects.Submission aSubmission = new BCS.Core.Clearance.BizObjects.Submission();

            #region Fill in submission attributes from parameters

            aSubmission.Agent = new BCS.Core.Clearance.BizObjects.Agent();
            aSubmission.Agent.FirstName = agentFN;
            aSubmission.Agent.Surname = agentLN;
            aSubmission.AnalystName = analystName;
            aSubmission.Comments = comments;
            aSubmission.Dba = dba;
            aSubmission.EffectiveDate = effDate;
            aSubmission.EstimatedWrittenPremium = premium;
            aSubmission.ExpirationDate = expDate;
            aSubmission.InsuredName = insuredName;
            aSubmission.PolicyNumber = policyNumber;
            aSubmission.SubmissionBy = submissionBy;
            aSubmission.SubmissionDt = submissionDt;
            aSubmission.SubmissionStatusCode = statusCode;
            aSubmission.SubmissionStatusDate = statusDate;
            aSubmission.SubmissionStatusReason = reasonCode;
            aSubmission.SubmissionStatusReasonOther = otherReason;
            aSubmission.SubmissionTypeName = typeName;
            aSubmission.UnderwriterName = underwriterName;
            aSubmission.UpdatedBy = updatedBy;
            aSubmission.UpdatedDt = updateDt;
            aSubmission.LineOfBusinessList = new BCS.Core.Clearance.BizObjects.LineOfBusiness[1];
            aSubmission.LineOfBusinessList[0] = new BCS.Core.Clearance.BizObjects.LineOfBusiness();
            aSubmission.LineOfBusinessList[0].Code = lobCode;
            aSubmission.CompanyNumberCodeIds = companyNumberCodeIds;

            if(!string.IsNullOrEmpty(submissionAttributes))
            {
                string[] attrArray = submissionAttributes.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                BCS.Core.Clearance.BizObjects.SubmissionAttribute[] attributeList = new clearance.BizObjects.SubmissionAttribute[attrArray.Length];
                int i = 0;
                foreach (string attr in attrArray)
                {
                    string[] childAttr = attr.Split("=".ToCharArray());
                    BCS.Core.Clearance.BizObjects.SubmissionAttribute subAttr = new clearance.BizObjects.SubmissionAttribute();
                    subAttr.CompanyNumberAttributeId = Convert.ToInt32(childAttr[0]);
                    subAttr.AttributeValue = childAttr[1];
                    attributeList[i] = subAttr;
                    i++;
                }
                if (i > 0)
                    aSubmission.AttributeList = attributeList;
            }
            #endregion


            //#region Agency
            //clearance.AgencyPort ap = new BCS.Core.Clearance.AgencyPort();
            //Agency agency = ap.GetAgency(companyNumber, agencyNumber);
            //#endregion
            
            //#region Agent Unspecified Reasons
            //string agentunspecifiedreasons = string.Empty;
            //DataManager dm = Common.GetDataManager();
            //if (aSubmission.AgentUnspecifiedReasonList != null && aSubmission.AgentUnspecifiedReasonList.Length > 0)
            //{
            //    dm.QueryCriteria.And(JoinPath.AgentUnspecifiedReason.CompanyNumberAgentUnspecifiedReason.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            //    AgentUnspecifiedReasonCollection allAgentReasons = dm.GetAgentUnspecifiedReasonCollection();

            //    List<string> ids = new List<string>();
            //    for (int i = 0; i < aSubmission.AgentUnspecifiedReasonList.Length; i++)
            //    {
            //        AgentUnspecifiedReason reason = allAgentReasons.FindByReason(aSubmission.AgentUnspecifiedReasonList[i].Reason);
            //        if (reason != null)
            //        {
            //            ids.Add(reason.Id.ToString());
            //        }
            //    }
            //    string[] arrAgentReasons = new string[ids.Count];
            //    ids.CopyTo(arrAgentReasons);
            //    agentunspecifiedreasons = string.Join(separator, arrAgentReasons);
            //}
            //#endregion

            //#region Code types
            //string codeids = string.Empty;


            //if (aSubmission.SubmissionCodeTypess != null && aSubmission.SubmissionCodeTypess.Length > 0)
            //{
            //    #region Get all code types for this company number
            //    dm = Common.GetDataManager();
            //    dm.QueryCriteria.And(JoinPath.CompanyNumberCodeType.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            //    CompanyNumberCodeTypeCollection allTypes = dm.GetCompanyNumberCodeTypeCollection();
            //    #endregion

            //    #region Get all codes for this company number
            //    dm.QueryCriteria.Clear();
            //    dm.QueryCriteria.And(JoinPath.CompanyNumberCode.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            //    CompanyNumberCodeCollection allCodes = dm.GetCompanyNumberCodeCollection();
            //    #endregion

            //    List<string> listcodeids = new List<string>();
            //    for (int i = 0; i < aSubmission.SubmissionCodeTypess.Length; i++)
            //    {
            //        CompanyNumberCodeType cncType = allTypes.FindByCodeName(aSubmission.SubmissionCodeTypess[i].CodeName);
            //        if (cncType != null)
            //        {
            //            CompanyNumberCode cnCode = allCodes.FindByCompanyCodeTypeId(cncType.Id);
            //            if (cnCode != null)
            //            {
            //                listcodeids.Add(cnCode.Id.ToString());
            //            }
            //        }
            //    }

            //    string[] scodeids = new string[listcodeids.Count];
            //    listcodeids.CopyTo(scodeids);
            //    codeids = string.Join(separator, scodeids);
            //}
            //#endregion

            //#region Line Of Businesses
            //string lobss = string.Empty;
            //if (aSubmission.LineOfBusinessList != null && aSubmission.LineOfBusinessList.Length > 0)
            //{
            //    dm = Common.GetDataManager();
            //    dm.QueryCriteria.And(JoinPath.LineOfBusiness.CompanyNumberLineOfBusiness.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            //    LineOfBusinessCollection allLobs = dm.GetLineOfBusinessCollection();

            //    List<string> ids = new List<string>();
            //    for (int i = 0; i < aSubmission.LineOfBusinessList.Length; i++)
            //    {
            //        LineOfBusiness lob = allLobs.FindByCode(aSubmission.LineOfBusinessList[i].Code);
            //        if (lob != null)
            //        {
            //            ids.Add(lob.Id.ToString());
            //        }
            //    }
            //    string[] arr = new string[ids.Count];
            //    ids.CopyTo(arr);
            //    lobss = string.Join(separator, arr);
            //}
            //#endregion

            //#region TODO: Line Of Business Children
            //string lobchildren = string.Empty;

            //#endregion

            //#region Attributes
            //string attributes = string.Empty;
            //if (aSubmission.AttributeList != null && aSubmission.AttributeList.Length > 0)
            //{
            //    string[] sattributes = new string[aSubmission.AttributeList.Length];
            //    for (int i = 0; i < aSubmission.AttributeList.Length; i++)
            //    {
            //        //sattributes[i] = aSubmission.AttributeList[i].CompanyNumberAttributeId.ToString();
            //        sattributes[i] = string.Format("{0}={1}", aSubmission.AttributeList[i].CompanyNumberAttributeId, aSubmission.AttributeList[i].AttributeValue);
            //    }
            //    attributes = string.Join("|", sattributes);
            //}
            //#endregion

            //#region Underwriter
            //if (!string.IsNullOrEmpty(aSubmission.UnderwriterName))
            //{
            //    dm = Common.GetDataManager();
            //    dm.QueryCriteria.And(JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            //    dm.QueryCriteria.And(JoinPath.Person.Columns.FullName, aSubmission.UnderwriterName);
            //    Person underwriter = dm.GetPerson();
            //    if (underwriter != null)
            //        aSubmission.UnderwriterId = underwriter.Id;
            //}
            //#endregion

            //#region Analyst
            //if (!string.IsNullOrEmpty(aSubmission.AnalystName))
            //{
            //    dm = Common.GetDataManager();
            //    dm.QueryCriteria.And(JoinPath.Person.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            //    dm.QueryCriteria.And(JoinPath.Person.Columns.FullName, aSubmission.AnalystName);
            //    Person analyst = dm.GetPerson();
            //    if (analyst != null)
            //        aSubmission.AnalystId = analyst.Id;
            //}
            //#endregion

            //#region Submission Status
            //if (!string.IsNullOrEmpty(aSubmission.SubmissionStatusCode))
            //{
            //    dm = Common.GetDataManager();
            //    dm.QueryCriteria.And(JoinPath.SubmissionStatus.CompanyNumberSubmissionStatus.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            //    dm.QueryCriteria.And(JoinPath.SubmissionStatus.Columns.StatusCode, aSubmission.SubmissionStatusCode);
            //    SubmissionStatus status = dm.GetSubmissionStatus();
            //    if (status != null)
            //    {
            //        aSubmission.SubmissionStatusId = status.Id;

            //        #region Submission Status Reason
            //        if (!string.IsNullOrEmpty(aSubmission.SubmissionStatusReason))
            //        {
            //            dm = Common.GetDataManager();
            //            dm.QueryCriteria.And(JoinPath.SubmissionStatusReason.SubmissionStatusSubmissionStatusReason.Columns.SubmissionStatusId, status.Id);
            //            dm.QueryCriteria.And(JoinPath.SubmissionStatusReason.Columns.ReasonCode, aSubmission.SubmissionStatusReason);
            //            SubmissionStatusReason statusReason = dm.GetSubmissionStatusReason();
            //            if (statusReason != null)
            //                aSubmission.SubmissionStatusReasonId = statusReason.Id;
            //        }
            //        #endregion
            //    }
            //}
            //#endregion

            //#region Submission Type
            //if (!string.IsNullOrEmpty(aSubmission.SubmissionTypeName))
            //{
            //    dm = Common.GetDataManager();
            //    dm.QueryCriteria.And(JoinPath.SubmissionType.CompanyNumberSubmissionType.CompanyNumber.Columns.PropertyCompanyNumber, companyNumber);
            //    dm.QueryCriteria.And(JoinPath.SubmissionType.Columns.TypeName, aSubmission.SubmissionTypeName);
            //    SubmissionType type = dm.GetSubmissionType();
            //    if (type != null)
            //        aSubmission.SubmissionTypeId = type.Id;
            //}
            //#endregion

            //#region Agent
            //if (aSubmission.Agent != null && !string.IsNullOrEmpty(aSubmission.Agent.Surname))
            //{
            //    dm = Common.GetDataManager();
            //    dm.QueryCriteria.And(JoinPath.Agent.Columns.AgencyId, agency.Id);
            //    dm.QueryCriteria.And(JoinPath.Agent.Columns.Surname, aSubmission.Agent.Surname);
            //    Agent agent = dm.GetAgent();
            //    if (agent != null)
            //        aSubmission.AgentId = agent.Id;
            //}
            //#endregion

            //#region Contact
            //if (aSubmission.Contact != null && !string.IsNullOrEmpty(aSubmission.Contact.Name))
            //{
            //    dm = Common.GetDataManager();
            //    dm.QueryCriteria.And(JoinPath.Contact.Columns.AgencyId, agency.Id);
            //    dm.QueryCriteria.And(JoinPath.Contact.Columns.Name, aSubmission.Contact.Name);
            //    Contact contact = dm.GetContact();
            //    if (contact != null)
            //        aSubmission.ContactId = contact.Id;
            //}
            //#endregion

            return Core.XML.Serializer.SerializeAsXml(aSubmission);
        }
        #endregion

    } 
}

