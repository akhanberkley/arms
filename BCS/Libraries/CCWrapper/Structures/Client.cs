using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Old
{
    [Serializable()]
    [XmlRoot("CLI_CLIENT")]
    public struct Client
    {
        [XmlElement("CLIENT_ID")]
        public string ClientId;

        [XmlElement("CORP_ID")]
        public string CorpId;

        [XmlElement("SEGMENTED_FLAG")]
        public string SegmentedFlag;

        [XmlElement("PROD_UPDATED_BY")]
        public string ProdUpdatedBy;

        [XmlElement("CLIENT_EXTERNAL_ID")]
        public string ClientExternalId;

        [XmlElement("UPDATED_BY")]
        public string UpdatedBy;

        [XmlElement("INSERT_DT")]
        public string InsertDt;

        [XmlElement("BIRTH_DATE")]
        public string BirthDate;

        #region Related Objects:
        [XmlElement("CLI_CLNT_COMMS")]
        public Communication[] Communications;

        [XmlElement("CLI_CLNT_INFOS")]
        public ClientInfo[] ClientInfos;

        [XmlElement("CLI_CLNT_NAMES")]
        public ClientName[] ClientNames;

        [XmlElement("CLI_CLNT_ADDRS")]
        public ClientAddress[] ClientAddresses;
        #endregion
    }
}
