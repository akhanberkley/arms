using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Old
{
    [Serializable()]
    [XmlRoot("CLI_CLNT_ADDR_V")]
    public struct ClientAddress
    {
        [XmlElement("CLIENT_ADDR_ID")]
        public string ClientAddressId;

        [XmlElement("CLIENT_ID")]
        public string ClientId;

        [XmlElement("ADDRESS_ID")]
        public string AddressId;

        [XmlElement("VERS_CNTL_NBR")]
        public string VersionControlNumber;

        [XmlElement("DELETED_FLAG")]
        public string DeletedFlag;

        [XmlElement("PRIMARY_ADDR_FLAG")]
        public string PrimaryAddressFlag;

        [XmlElement("PROD_UPDATED_BY")]
        public string ProdUpdatedBy;

        [XmlElement("UPDATED_BY")]
        public string UpdatedBy;

        [XmlElement("INSERT_DT")]
        public string InsertDt;

        [XmlElement("UPDATE_DT")]
        public string UpdateDt;

        #region Related Objects
        [XmlElement("CLI_ADDRESSES")]
        public Address[] Addresses;
        #endregion
    }
}
