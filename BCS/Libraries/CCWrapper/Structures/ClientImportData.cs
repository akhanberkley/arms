using System;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Old
{
    public struct ClientImportData
    {
        [XmlElement("Signature")]
        public Signature Signature;

        [XmlElement("Client")]
        public ImportClient Client;

        
        
    }

    public struct Signature
    {
        public Signature(string recordType, string versionNumber, string localeCode)
        {
            RecordType = recordType;
            VersionNumber = versionNumber;
            LocaleCode = localeCode;
        }

        [XmlElement("record_type")]
        public string RecordType;

        [XmlElement("version-number")]
        public string VersionNumber;

        [XmlElement("Locale-Code")]
        public string LocaleCode;
    }

    public struct ImportClient
    {
        [XmlElement("DuplicateClientOption")]
        public int DuplicateClientOption;

        [XmlElement("CCClientId")]
        public string CCClientId;

        [XmlElement("ClientType")]
        public string ClientType;

        [XmlElement("CorporationCode")]
        public string CorporationCode;

        [XmlElement("VendorFlag")]
        public string VendorFlag;

        [XmlElement("ClientCategory")]
        public string ClientCategory;

        [XmlElement("CliEffectiveDate")]
        public string CliEffectiveDate;

        [XmlElement("Owner")]
        public string Owner;

        [XmlElement("Status")]
        public string Status;

        [XmlElement("NameInfo")]
        public NameInfo[] NameInfos;

        public PersonalInfo PersonalInfo;

        [XmlElement("AddressInfo")]
        public AddressInfo[] AddressInfos;
        
    }

    public struct NameInfo
    {
        public string NameSeqNum;

        public string BusinessName;

        public string LastName;

        public string FirstName;
        
        public string MiddleName;

        public string NameType;

        public string NamePrefix;

        public string NameSuffix;

        public string DisplayFlag;

        public string DeleteFlag;

    }

    public struct PersonalInfo
    {
        public string TaxIdTypeCode;

        public string TaxId;

        public string Gender;

        [XmlElement("DOB")]
        public string BirthDate;

        public string DeceasedDate;


        [XmlElement("DrivLicNo")]
        public string DriverLicenseNo;

        [XmlElement("DrivLicCountry")]
        public string DriverLicenseCountry;

        [XmlElement("DrivLicState")]
        public string DriverLicenseState;

        public string MaritalStatus;
    }

    public struct AddressInfo
    {
        public string DuplicateAddressOption;

        [XmlElement("AddrEffectiveDate")]
        public string EffectiveDate;

        public string HouseNumber;

        public string CCAddressId;

        public string ADDR1;

        public string ADDR2;

        public string ADDR3;

        public string ADDR4;

        public string CityName;

        //public string CityCode';

        [XmlElement("State")]
        public string StateCode;

        public string Zip;

        public string County;

        public string Country;

        public string ExternalAddressType;

        public string DisplayFlag;

        public string DeleteFlag;
    }
}
