using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Old
{
    [Serializable()]
    [XmlRoot("name")]
    public struct ClientName
    {
        [XmlElement(ElementName = "nameFirst")]
        public string NameFirst;

        [XmlElement("nameLast")]
        public string NameLast;

        [XmlElement("nameMiddle")]
        public string NameMiddle;

        [XmlElement("namePrefix")]
        public string NamePrefix;


        [XmlElement("namePrefixCode")]
        public string NamePrefixCode;

        [XmlElement("nameSuffix")]
        public string NameSuffix;

        [XmlElement("nameSuffixCode")]
        public string NameSuffixCode;

        [XmlElement("nameBusiness")]
        public string NameBusiness;

        [XmlElement("nameType")]
        public string NameType;
        
        [XmlElement("primary_name_flag")]
        public string PrimaryNameFlag;

        [XmlElement("client_Name_Id")]
        public string ClientNameId;

        [XmlElement("client_Id")]
        public string ClientId;

        [XmlElement("vers_Cntl_Nbr")]
        public string VersionControlNumber;

        [XmlElement("seq_Nbr")]
        public string SequenceNumber;

        [XmlElement("deleted_Flag")]
        public string DeletedFlag;

       

       

        [XmlElement("name_First_Upper")]
        public string NameFirstUpper;

        

        [XmlElement("name_Middle_Upper")]
        public string NameMiddleUpper;

       

        [XmlElement("name_Last_Upper")]
        public string NameLastUpper;

     

     

        [XmlElement("name_Bus_Upper")]
        public string NameBusinessUpper;

        [XmlElement("prod_Updated_By")]
        public string ProdUpdatedBy;

     
        [XmlElement("updated_By")]
        public string UpdatedBy;

        [XmlElement("insert_Dt")]
        public string InsertDt;

        [XmlElement("update_Dt")]
        public string UpdateDt;
    }
}
