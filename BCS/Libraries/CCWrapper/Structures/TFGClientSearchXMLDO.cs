using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Old
{
    [Serializable()]
    [XmlRoot("TFGClientSearchXMLDO")]
    public struct TFGClientSearchXMLDO
    {
        [XmlElement("searchType")]
        public string SearchType;

        [XmlElement("clientType")]
        public string ClientType;

        [XmlElement("firstName")]
        public string FirstName;

        [XmlElement("lastName")]
        public string LastName;

        [XmlElement("middleName")]
        public string MiddleName;

        [XmlElement("businessName")]
        public string BusinessName;

        [XmlElement("taxID")]
        public string TaxId;

        [XmlElement("taxIDType")]
        public string TaxIdType;

        #region Address Elements
        [XmlElement("houseNumber")]
        public string HouseNumber;

        [XmlElement("address1")]
        public string Address1;

        [XmlElement("countryCD")]
        public string CountryName;

        [XmlElement("countryID")]
        public string CountryId;

        [XmlElement("stateCD")]
        public string StateName;

        [XmlElement("stateID")]
        public string StateId;

        [XmlElement("cityName")]
        public string CityName;

        [XmlElement("cityID")]
        public string CityId;

        [XmlElement("countyName")]
        public string CountyName;

        [XmlElement("countyID")]
        public string CountyId;

        [XmlElement("zipCode")]
        public string ZipCode;
        #endregion
    }
}
