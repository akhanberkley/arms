using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Old
{
    [XmlRoot("vector")]
    public struct Vector
    {
        [XmlElement("TFGClientSearchResultXMLDO", typeof(TFGClientSearchResultXMLDO))]
        public TFGClientSearchResultXMLDO[] TFGClientSearchResultXMLDOs;

        [XmlElement("ERROR", typeof(string))]
        public string Error;

        [XmlElement("string")]
        public string Message;
    }
}
