using System;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Import
{
    [XmlRoot("vector")]
    public struct Vector
    {
        [XmlElement("big-decimal")]
        public string ClientId;
    }
}
