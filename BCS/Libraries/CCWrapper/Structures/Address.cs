using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Old
{
    [Serializable()]
    [XmlRoot("CLI_ADDRESS")]
    public struct Address
    {
        [XmlElement("ADDRESS_ID")]
        public string AddressId;

        [XmlElement("CORP_ID")]
        public string CorpId;

        [XmlElement("SEGMENTED_FLAG")]
        public string SegmentedFlag;

        [XmlElement("PROD_UPDATED_BY")]
        public string ProdUpdatedBy;

        [XmlElement("ADDR_EXTERNAL_ID")]
        public string AddressExternalId;

        [XmlElement("UPDATED_BY")]
        public string UpdatedBy;

        [XmlElement("INSERT_DT")]
        public string InsertDt;

        #region Related Objects
        [XmlElement("CLI_ADDRESS_VS")]
        public AddressVersion[] AddressVersions;
        #endregion
    }
}
