using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures
{
    [Serializable()]
    [XmlRoot("TFGGlobalBasicXMLDO")]
    public struct TFGGlobalBasicXMLDO
    {
        [XmlElement("systemCd")]
        public string SystemCd;

        [XmlElement("username")]
        public string Username;

        [XmlElement("password")]
        public string Password;

        [XmlElement("localID")]
        public string LocalId;
    }
}
