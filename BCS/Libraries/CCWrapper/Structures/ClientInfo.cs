using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Old
{
    [Serializable()]
    [XmlRoot("client-info")]
    public struct ClientInfo
    {
        [XmlElement("corp_id")]
        public string CorpId;

        [XmlElement("segmented-client")]
        public string Segmented;        

        [XmlElement("client-iD")]
        public string ClientId;

        [XmlElement("VERS_CNTL_NBR")]
        public string VersionControlNumber;

        [XmlElement("client_type")]
        public string ClientType;

        [XmlElement("CLIENT_CATEGORY")]
        public string ClientCategory;

        [XmlElement("SATISFACTION_LEVEL")]
        public string SatisfactionLevel;

        [XmlElement("client_status")]
        public string ClientStatus;

        [XmlElement("TAX_ID")]
        public string TaxId;

        [XmlElement("TAX_ID_TYPE")]
        public string TaxIdType;

        [XmlElement("vendor_flag")]
        public string VendorFlag;

        [XmlElement("CREDIT_RATING")]
        public string CreditRating;

        [XmlElement("PREFERENCE_LANGUAG")]
        public string PreferenceLanguage;

        [XmlElement("PREFERENCE_COMM")]
        public string PreferenceCommunication;

        [XmlElement("REFERRED_BY")]
        public string ReferredBy;

        [XmlElement("PROD_UPDATED_BY")]
        public string ProdUpdatedBy;

        [XmlElement("UPDATED_BY")]
        public string UpdatedBy;

        [XmlElement("INSERT_DT")]
        public string InsertDt;

        [XmlElement("UPDATE_DT")]
        public string UpdateDt;

        [XmlElement("membership-flag")]
        public string MembershipFlag;

        [XmlElement("PRIMARY_MBR_F")]
        public string PrimaryMemberFlag;
    }
}
