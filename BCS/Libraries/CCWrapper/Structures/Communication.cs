using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Old
{
    [XmlRoot("communication")]
    public struct Communication
    {
        [XmlElement("communication_id")]
        public string ID;
        [XmlElement("communication_type_code")]
        public string TypeCode;
        [XmlElement("communication_value")]
        public string Value;
        [XmlElement("communication_v_id")]
        public string ValueId;
        
    }
}
