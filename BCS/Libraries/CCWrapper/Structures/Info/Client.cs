using System;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Info
{
    [XmlRoot("TFGClientQuickAddResultXMLDO")]
    public struct ClientInfo
    {
        [XmlElement("address-do")]
        public Address[] Addresses;

        [XmlElement("client")]
        public Client Client;
    }

    public struct Client
    {
        [XmlElement("segmentedclient")]
        public string SegmentedClient;

        [XmlElement("membership-flag")]
        public string MembershipFlag;

        [XmlElement("client_type")]
        public string ClientType;

        [XmlElement("client_status")]
        public string ClientStatus;

        [XmlElement("client_category")]
        public string ClientCategory;

        [XmlElement("client_id")]
        public string ClientId;

        [XmlElement("portfolio-id")]
        public string PortfolioId;

        [XmlElement("corp_id")]
        public string CorpId;

        [XmlElement("tax_id")]
        public string TaxId;

        [XmlElement("tax_id_type")]
        public string TaxIdType;


        [XmlElement("names")]
        public Name[] Names;

        [XmlElement("group-owner-info-dO")]
        public GroupOwnerInfoDO Group;

        [XmlElement("portfolio_summary")]
        public Search.Portfolio Portfolio;

        [XmlElement("membership-group")]
        public string MembershipGroup;

    }

    public struct Name
    {
        [XmlAttribute("seq_nbr")]
        public string SequenceNumber;

        [XmlElement("primary_name_flag")]
        public string PrimaryNameFlag;

        [XmlElement("name_type")]
        public string NameType;

        public string ClearanceNameType;

        [XmlElement("name_last")]
        public string NameLast;

        [XmlElement("name_first")]
        public string NameFirst;

        [XmlElement("name_middle")]
        public string NameMiddle;

        [XmlElement("client_id")]
        public string ClientId;

        [XmlElement("name_business")]
        public string NameBusiness;

        [XmlElement("name_prefix")]
        public string NamePrefix;

        [XmlElement("name_suffix")]
        public string NameSuffix;

        [XmlElement("effective_date")]
        public string EffectiveDate;

    }

    public struct Address
    {

        [XmlElement("address_id")]
        public string AddressId;

        [XmlElement("house_nbr")]
        public string HouseNumber;

        [XmlElement("address1")]
        public string Address1;

        [XmlElement("address2")]
        public string Address2;

        [XmlElement("address3")]
        public string Address3;

        [XmlElement("address4")]
        public string Address4;

        [XmlElement("city")]
        public string City;

        [XmlElement("state_prov_cd")]
        public string StateProvinceId;

        [XmlElement("state_prov")]
        public string StateProvince;

        [XmlElement("county_id")]
        public string CountyId;

        [XmlElement("county")]
        public string County;

        [XmlElement("country_cd")]
        public string Country;

        [XmlElement("country")]
        public string CountryId;

        [XmlElement("postal_code")]
        public string PostalCode;

        [XmlElement("primary_address_flag")]
        public string Primary;

        public string ClearanceAddressType;

    }

    public struct MemberShipInfo
    {
        public string MembershipGroup;

        public string PrimaryMember;
    }

}
