using System;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Info
{
    /// <summary>
    /// struct used as input to get client info
    /// </summary>
    public struct TFGClientExportIDInfoDO
    {
        public string ClientID;

        public string EffectiveDate;

        public string SeqNbr;

        public string ClientType;

        public string corpID;
    }


}
