using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Old
{
    [XmlRoot("groupOwnerInfoDO")]
    public struct GroupOwnerInfoDO
    {
        [XmlElement("accessKeyID")]
        public string AccessKeyID;

        [XmlElement("inactiveDT")]
        public string InactiveDate;

        [XmlElement("userGroupDescription")]
        public string GroupDescription;

        [XmlElement("userGroupCode")]
        public string GroupCode;
    }
}
