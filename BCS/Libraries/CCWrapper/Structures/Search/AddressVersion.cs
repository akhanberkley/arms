using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Search
{
    [Serializable()]
    [XmlRoot("CLI_ADDRESS_V")]
    public struct AddressVersion
    {
        [XmlElement("corp_Id")]
        public string CorpId;

        [XmlElement("effective_-date")]
        public string EffectiveDate;

        [XmlElement("house_nbr")]
        public string HouseNumber;

        [XmlElement("address1")]
        public string Address1;

        [XmlElement("address2")]
        public string Address2;

        [XmlElement("address3")]
        public string Address3;

        [XmlElement("address4")]
        public string Address4;

        [XmlElement("country_cd")]
        public string CountryCode;

        [XmlElement("state_prov_cd")]
        public string StateProvCode;

        [XmlElement("city")]
        public string City;

        [XmlElement("county")]
        public string County;

        [XmlElement("postal_code")]
        public string PostalCode;

        [XmlElement("address_v_id")]
        public string AddressVersionId;

        [XmlElement("address_id")]
        public string AddressId;

        [XmlElement("ver_cntl_nbr")]
        public string VersionControlNumber;




        [XmlElement("address1_Upper")]
        public string Address1Upper;



        [XmlElement("address2_Upper")]
        public string Address2Upper;







        [XmlElement("city_Upper")]
        public string CityUpper;


        [XmlElement("state_Prov_Upper")]
        public string StateProvUpper;



        [XmlElement("country_id")]
        public string CountryId;

        [XmlElement("county_id")]
        public string CountyId;


        [XmlElement("scrub_Address_Flag")]
        public string ScrubAddressFlag;

        [XmlElement("city_id")]
        public string CityId;

        [XmlElement("state_prov_id")]
        public string StateProvId;



        [XmlElement("county_Upper")]
        public string CountyUpper;

        [XmlElement("prod_Updated_By")]
        public string ProdUpdatedBy;

        [XmlElement("updated_By")]
        public string UpdatedBy;

        [XmlElement("insert_Dt")]
        public string InsertDt;

        [XmlElement("update_Dt")]
        public string UpdateDt;
    }
}
