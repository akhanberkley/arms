using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Search
{
    [XmlRoot("TFGClientSearchResultXMLDO")]
    public struct Client
    {
        #region Related Objects:

        [XmlElement("client-info", typeof(ClientInfo))]
        public ClientInfo Info;

        //[XmlArray("communications"), XmlArrayItem("communication", typeof(Communication))]
        //public Communication[] Communications;

        [XmlArray("names"), XmlArrayItem("name", typeof(ClientName))]
        public ClientName[] Names;

        [XmlArray("addresses"), XmlArrayItem("address", typeof(AddressVersion))]
        public AddressVersion[] Addresses;

        [XmlElement("group-owner-info-dO")]
        public GroupOwnerInfoDO Group;

        [XmlElement("portfolio_summary")]
        public Portfolio Portfolio;

        #endregion
    }

    public struct Portfolio
    {
        /// <summary>
        /// Gets or Sets the portfolio id
        /// </summary>
        [XmlElement("portfolio_id")]
        public string Id;

        /// <summary>
        /// Gets or Sets the portfolio name
        /// </summary>
        [XmlElement("portfolio_name")]
        public string Name;

        /// <summary>
        /// Gets or Sets the client count in this portfolio
        /// </summary>
        [XmlElement("client_count")]
        public string ClientCount;
    }
}
