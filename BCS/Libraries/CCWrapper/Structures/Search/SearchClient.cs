using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Search
{
    [Serializable()]
    [XmlRoot("TFGClientSearchXMLDO")]
    public struct SearchClient
    {
        //[XmlElement("clientId")]
        [XmlIgnore]
        public string ClientId;

        [XmlElement("searchType")]
        public string SearchType;

        [XmlElement("clientType")]
        public string ClientType;

        [XmlElement("firstName")]
        public string FirstName;

        [XmlElement("lastName")]
        public string LastName;

        [XmlElement("middleName")]
        public string MiddleName;

        [XmlElement("businessName")]
        public string BusinessName;

        [XmlElement("taxID")]
        public string TaxId;

        [XmlElement("taxIDType")]
        public string TaxIdType;

        #region Address Elements
        [XmlElement("houseNumber")]
        public string HouseNumber;

        [XmlElement("address1")]
        public string Address1;
        
        [XmlElement("countryCD")]
        public string CountryName;

        [XmlElement("countryID")]
        public string CountryId;

        [XmlElement("stateCD")]
        public string StateName;

        [XmlElement("stateID")]
        public string StateId;

        [XmlElement("cityName")]
        public string CityName;

        [XmlElement("cityID")]
        public string CityId;

        [XmlElement("countyName")]
        public string CountyName;

        [XmlElement("countyID")]
        public string CountyId;

        [XmlElement("zipCode")]
        public string ZipCode;
        #endregion

        [XmlElement("fillData")]
        public string FillData;

        [XmlElement("corpID")]
        public string CorpID;
    }

    [Serializable()]    
    [XmlRoot("request_portfolio_search")]
    public struct SearchPortfolio
    {
        /// <summary>
        /// Gets or Sets the portfolio id
        /// </summary>
        [XmlElement("portfolio_id")]
        public string Id;

        /// <summary>
        /// Gets or Sets the portfolio name
        /// </summary>
        [XmlElement("portfolio_name")]
        public string Name;

        /// <summary>
        /// Gets or Sets the effective date
        /// </summary>
        [XmlElement("effective_date")]
        public string EffectiveDate;

        /// <summary>
        /// Gets or Sets the effective date
        /// </summary>
        [XmlElement("corp_id")]
        public string CorpId;
    }
}
