using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Search
{
    [XmlRoot("groupOwnerInfoDO")]
    public struct GroupOwnerInfoDO
    {
        [XmlElement("access-key-iD")]
        public string AccessKeyID;

        [XmlElement("inactiveDT")]
        public string InactiveDate;

        [XmlElement("user-group-description")]
        public string GroupDescription;

        [XmlElement("user-group-code")]
        public string GroupCode;
    }
}
