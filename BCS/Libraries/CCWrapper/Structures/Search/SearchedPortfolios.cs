using System;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Search
{

    [Serializable()]
    [XmlRoot("portfolio_list")]
    public class SearchedPortfolios
    {
        //[XmlArray("portfolios"), XmlArrayItem("portfolio", typeof(SearchedPortfolio))]
        //public SearchedPortfolio[] Portfolios;

        [XmlElement("portfolio")]
        public SearchedPortfolio[] Portfolios;

        [XmlElement("ERROR", typeof(string))]
        public string Error;

        [XmlElement("message")]
        public string Message;
    }
    [Serializable()]
    [XmlRoot("portfolio")]
    public class SearchedPortfolio
    {
        /// <summary>
        /// Gets or Sets the portfolio id
        /// </summary>
        [XmlElement("portfolio_id")]
        public string Id;

        /// <summary>
        /// Gets or Sets the portfolio name
        /// </summary>
        [XmlElement("portfolio_name")]
        public string Name;
    }
}
