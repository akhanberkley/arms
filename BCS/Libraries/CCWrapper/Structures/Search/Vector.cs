using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Search
{
    [XmlRoot("vector")]
    public struct Vector
    {
        [XmlElement("TFGClientSearchResultXMLDO", typeof(Client))]
        public Client[] Clients;

        [XmlElement("ERROR", typeof(string))]
        public string Error;

        [XmlElement("string")]
        public string Message;
    }
}
