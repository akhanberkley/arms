using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Add
{
    [XmlRoot("vector")]
    public struct Vector
    {
        //Verify if this is required.
        [XmlElement("TFGClientSearchResultXMLDO", typeof(Client))]
        public Client[] Clients;

        [XmlElement("ERROR", typeof(string))]
        public string Error;

        [XmlElement("string")]
        public string Message;
    }
}
