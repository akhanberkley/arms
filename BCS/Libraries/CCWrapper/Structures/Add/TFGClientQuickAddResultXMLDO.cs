using System;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Add
{
    [Serializable()]
    [XmlRoot("TFGClientQuickAddResultXMLDO")]
    public struct TFGClientQuickAddResultXMLDO
    {
        [XmlAttribute("matches-found")]
        public string matchesFound;

        [XmlElement("client")]
        public Client2 Client;

        [XmlElement("address-do")]
        public Address[] Address;
    }

    #region Client
    public struct Client2
    {
        [XmlAttribute("health_care_flag")]
        public string healthCareFlag;

        [XmlAttribute("bulk_flag")]
        public string bulkFlag;

        [XmlAttribute("eft_flag")]
        public string eftFlag;

        [XmlAttribute("vendor_flag")]
        public string vendorFlag;

        [XmlAttribute("individual")]
        public string individual;

        [XmlAttribute("dup-updatable")]
        public string dupUpdatable;

        [XmlAttribute("require_1099_flag")]
        public string require1099Flag;

        [XmlAttribute("surcharge_flag")]
        public string surchargeFlag;

        [XmlAttribute("membership_flag")]
        public string membershipFlag;

        [XmlAttribute("recomnd_vend_flag")]
        public string recommendVendorFlag;

        [XmlAttribute("financial_seq_nbr")]
        public string financialSequenceNumber;

        [XmlAttribute("duplicate")]
        public string duplicate;

        [XmlAttribute("seq_nbr")]
        public string sequenceNumber;

        [XmlAttribute("effective_-date")]
        public string effectiveDate;

        [XmlAttribute("process-date")]
        public string processDate;

        [XmlAttribute("display-format")]
        public string displayFormat;

        [XmlAttribute("dirty")]
        public string dirty;

        [XmlAttribute("compare-key")]
        public string compareKey;

        [XmlElement("name-dOs")]
        public NameDO[] nameDo;

        [XmlElement("portfolio-id")]
        public string portfolioId;

        [XmlElement("tax_id_type")]
        public string taxIdType;

        [XmlElement("extended-formatted-name-last-first-or-bus-with-type")]
        public string extendedFormattedNameLastFirstOrBusinessWithType;

        [XmlElement("names")]
        public Names[] names;

        [XmlElement("tax_id")]
        public string taxId;

        [XmlElement("segmented-client")]
        public string segmentedClient;

        [XmlElement("group-owner-info-dO")]
        public GroupOwnerInfoDO2 groupOwnerInfoDO;

        [XmlElement("corp_id")]
        public string corpId;

        [XmlElement("alt_seq_nbr")]
        public string altSequenceNumber;

        [XmlElement("membership-flag")]
        public string membershipFlagYorN;

        [XmlElement("client_type")]
        public string clientType;

        [XmlElement("client_status")]
        public string clientStatus;

        [XmlElement("client_status_value")]
        public string clientStatusValue;

        [XmlElement("primary_name_flag")]
        public string primaryNameFlag;

        [XmlElement("name_business")]
        public string nameBusiness;

        [XmlElement("formatted-name-last-first-or-bus-with-type")]
        public string formattedNameLastFirstOrBusinessWithType;

        [XmlElement("formatted-name-last-first-or-bus")]
        public string formattedNameLastFirstOrBusiness;

        [XmlElement("client_id")]
        public string clientId;

        [XmlElement("locale-iD")]
        public string localId;
    }

    public struct NameDO
    {
        [XmlAttribute("duplicate")]
        public string duplicate;

        [XmlAttribute("seq_nbr")]
        public string sequenceNumber;

        [XmlAttribute("effective_-date")]
        public string effectiveDate;

        [XmlAttribute("process-date")]
        public string processDate;

        [XmlAttribute("display-format")]
        public string displayFormat;

        [XmlAttribute("dirty")]
        public string dirty;

        [XmlAttribute("compare-key")]
        public string comparyKey;

        [XmlElement("primary_name_flag")]
        public string primaryNameFlag;

        [XmlElement("name_business")]
        public string nameBusiness;

        [XmlElement("formatted-name-last-first-or-bus-with-type")]
        public string formattedNameLastFirstOrBusinessWithType;

        [XmlElement("formatted-name-last-first-or-bus")]
        public string formattedNameLastFirstOrBusiness;

        [XmlElement("client_id")]
        public string clientId;

        [XmlElement("locale-iD")]
        public string localId;
    }

    public struct Names
    {
        [XmlAttribute("duplicate")]
        public string duplicate;

        [XmlAttribute("seq_nbr")]
        public string sequenceNumber;

        [XmlAttribute("effective_-date")]
        public string effectiveDate;

        [XmlAttribute("process-date")]
        public string processDate;

        [XmlAttribute("display-format")]
        public string displayFormat;

        [XmlAttribute("dirty")]
        public string dirty;

        [XmlAttribute("compare-key")]
        public string comparyKey;

        [XmlElement("primary_name_flag")]
        public string primaryNameFlag;

        [XmlElement("name_business")]
        public string nameBusiness;

        [XmlElement("formatted-name-last-first-or-bus-with-type")]
        public string formattedNameLastFirstOrBusinessWithType;

        [XmlElement("formatted-name-last-first-or-bus")]
        public string formattedNameLastFirstOrBusiness;

        [XmlElement("client_id")]
        public string clientId;

        [XmlElement("locale-iD")]
        public string localId;
    }

    public struct GroupOwnerInfoDO2
    {
        [XmlAttribute("display-format")]
        public string displayFormat;

        [XmlAttribute("dirty")]
        public string dirty;

        [XmlAttribute("compare-key")]
        public string compareKey;

        [XmlElement("user-group-description")]
        public string userGroupDescription;

        [XmlElement("access-key-iD")]
        public string accessKeyId;

        [XmlElement("user-group-code")]
        public string userGroupCode;
    }
    #endregion

    #region Address
    public struct Address
    {
        [XmlAttribute("address_added")]
        public string addressAdded;

        [XmlElement("user_id")]
        public string userId;

        [XmlElement("postal_code")]
        public string postalCode;

        [XmlElement("house_nbr")]
        public string houseNumber;

        [XmlElement("state_prov_cd")]
        public string stateProvCode;

        [XmlElement("address_id")]
        public string addressId;

        [XmlElement("city")]
        public string city;

        [XmlElement("system_do")]
        public SystemDO systemDO;

        [XmlElement("corp_id")]
        public string corpId;

        [XmlElement("country_cd")]
        public string countryCode;

        [XmlElement("effective_-date")]
        public string effectiveDate;
    }

    public struct SystemDO
    {
        [XmlAttribute("display-format")]
        public string displayFormat;

        [XmlAttribute("dirty")]
        public string dirty;

        [XmlAttribute("compare-key")]
        public string comparyKey;

        [XmlElement("system_name")]
        public string systemName;

        [XmlElement("accept-cCNotify")]
        public string acceptCCNotify;

        [XmlElement("system_code")]
        public string systemCode;

        [XmlElement("base-system-flag")]
        public string baseSystemFlag;

        [XmlElement("system_id")]
        public string systemId;
    }
    #endregion

    #region Not Used
    //public struct Address
    //{
    //    [XmlElement("corp_id")]
    //    public string CorpId;

    //    [XmlElement("user_id")]
    //    public string UserId;

    //    [XmlElement("address_id")]
    //    public string AddressId;
               
    //    [XmlElement("effective_-date")]
    //    public string EffectiveDate;
    //}

    //public struct ClientExport
    //{
    //    [XmlElement("ClientID")]
    //    public string ClientId;

    //    [XmlElement("SeqNbr")]
    //    public string SegNbr;

    //    [XmlElement("EffectiveDate")]
    //    public string EffectiveDate;

    //    [XmlElement("corpID")]
    //    public string CorpId;

    //}
    #endregion
}
