using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Add
{
    [Serializable()]
    [XmlRoot("CLI_ADDRESS_V")]
    public struct AddressVersion
    {
        [XmlElement("corp_Id")]
        public string CorpId;

        [XmlElement("effective_Dt")]
        public string EffectiveDate;

        [XmlElement("house_Nbr")]
        public string HouseNumber;

        [XmlElement("address1")]
        public string Address1;

        [XmlElement("address2")]
        public string Address2;

        [XmlElement("address3")]
        public string Address3;

        [XmlElement("address4")]
        public string Address4;

        [XmlElement("country_Cd")]
        public string CountryCode;

        [XmlElement("state_Prov_Cd")]
        public string StateProvCode;

        [XmlElement("city")]
        public string City;

        [XmlElement("county")]
        public string County;

        [XmlElement("postal_Code")]
        public string PostalCode;

        [XmlElement("address_V_Id")]
        public string AddressVersionId;

        [XmlElement("address_Id")]
        public string AddressId;

        [XmlElement("ver_Cntl_Nbr")]
        public string VersionControlNumber;




        [XmlElement("address1_Upper")]
        public string Address1Upper;



        [XmlElement("address2_Upper")]
        public string Address2Upper;







        [XmlElement("city_Upper")]
        public string CityUpper;


        [XmlElement("state_Prov_Upper")]
        public string StateProvUpper;



        [XmlElement("country_Id")]
        public string CountryId;

        [XmlElement("county_Id")]
        public string CountyId;


        [XmlElement("scrub_Address_Flag")]
        public string ScrubAddressFlag;

        [XmlElement("city_Id")]
        public string CityId;

        [XmlElement("state_Prov_Id")]
        public string StateProvId;



        [XmlElement("county_Upper")]
        public string CountyUpper;

        [XmlElement("prod_Updated_By")]
        public string ProdUpdatedBy;

        [XmlElement("updated_By")]
        public string UpdatedBy;

        [XmlElement("insert_Dt")]
        public string InsertDt;

        [XmlElement("update_Dt")]
        public string UpdateDt;

        [XmlElement("DisplayFlag")]
        public string Primary;
    }
}
