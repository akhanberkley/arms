using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Add
{
    [Serializable()]
    [XmlRoot("TFGClientXMLDO")]
    public struct TFGClientXMLDO
    {
        [XmlElement("effective_Date")]
        public string EffectiveDate;

        [XmlElement("corp_Id")]
        public string CorpId;

        [XmlElement("tax_Id")]
        public string TaxId;

        [XmlElement("tax_Id_Type")]
        public string TaxIdType;

        [XmlElement("membershipGroup")]
        public string MembershipGroup;

        [XmlElement("membershipFlag")]
        public string MembershipFlag;

        [XmlElement("client_Category")]
        public string Category;

        [XmlElement("client_Status")]
        public string Status;

        [XmlElement("client_Type_Value")]
        public string ClientType;

        [XmlElement("maritalStatus")]
        public string MaritalStatus;

        [XmlElement("driverLicenseNumber")]
        public string DriverLicenseNumber;

        [XmlElement("driverLicenseCountry")]
        public string DriverLicenseCountry;

        [XmlElement("driverLicenseState")]
        public string DriverLicenseState;

        [XmlElement("gender")]
        public string Gender;

        [XmlElement("credit_Rating")]
        public string CreditRating;

        [XmlElement("birth_Date")]
        public string BirthDate;

        #region Related Objects:

        [XmlElement("communication")]
        public Communication[] Communications;

        [XmlElement("name")]
        public ClientName[] Names;

        [XmlElement("address")]
        public AddressVersion[] Addresses;

        [XmlElement("groupOwnerInfoDO")]
        public GroupOwnerInfoDO[] Groups;




        #endregion

        [XmlElement("bus_Start_Date")]
        public string StartDate;

        [XmlElement("ICC_End_NBR")]
        public string ICC_End_NBR;

        [XmlElement("inter_State_Id")]
        public string InterStateId;

        [XmlElement("intra_State_Id")]
        public string IntraStateId;

        [XmlElement("MC_Docket_Nbr")]
        public string MC_Docket_Nbr;

        [XmlElement("NCCI_Nbr")]
        public string NCCI_Nbr;
    }
}
