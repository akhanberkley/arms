using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace BTS.ClientCore.Wrapper.Structures.Add
{
    [XmlRoot("TFGClientSearchResultXMLDO")]
    public struct Client
    {
        #region Related Objects:

        [XmlElement("client-info", typeof(ClientInfo))]
        public ClientInfo infos;

        //[XmlArray("communications"), XmlArrayItem("communication", typeof(Communication))]
        //public Communication[] Communications;

        [XmlArray("names"), XmlArrayItem("name", typeof(ClientName))]
        public ClientName[] Names;

        [XmlArray("addresses"), XmlArrayItem("address", typeof(AddressVersion))]
        public AddressVersion[] Addresses;

        //[XmlElement("groupOwnerInfoDO")]
        //public GroupOwnerInfoDO[] Groups;


        #endregion
    }
}
