using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencySummaryView
    {
        private String agencyNameField;
        private AddressView mailingAddressField;
        private AddressView physicalAddressField;
        private String agencyPhoneNumberField;
        private String agencySpeedDialField;
        private String agencyTollFreeNumberField;
        private String appointmentDateField;
        private String agencyFoundingDateField;
        private String agencyRankField;
        private String agencyStatusField;
        private double currentYearPremiumGoalField;
        private PersonnelView[] principalsField;
        private PersonnelView[] areaPrincipalsField;
        private AnnualPremiumGoalView[] currentGoalsField;

        public String agencyName
        {
            get
            {
                return this.agencyNameField;
            }
            set
            {
                this.agencyNameField = value;
            }
        }

        public String agencyPhoneNumber
        {
            get
            {
                return this.agencyPhoneNumberField;
            }
            set
            {
                this.agencyPhoneNumberField = value;
            }
        }

        public String agencyRank
        {
            get
            {
                return this.agencyRankField;
            }
            set
            {
                this.agencyRankField = value;
            }
        }

        public String appointmentDate
        {
            get
            {
                return this.appointmentDateField;
            }
            set
            {
                this.appointmentDateField = value;
            }
        }

        public double currentYearPremiumGoal
        {
            get
            {
                return this.currentYearPremiumGoalField;
            }
            set
            {
                this.currentYearPremiumGoalField = value;
            }
        }

        public String agencyFoundingDate
        {
            get
            {
                return this.agencyFoundingDateField;
            }
            set
            {
                this.agencyFoundingDateField = value;
            }
        }

        public PersonnelView[] areaPrincipals
        {
            get
            {
                return this.areaPrincipalsField;
            }
            set
            {
                this.areaPrincipalsField = value;
            }
        }

        public PersonnelView[] principals
        {
            get
            {
                return this.principalsField;
            }
            set
            {
                this.principalsField = value;
            }
        }

        public AddressView mailingAddress
        {
            get
            {
                return this.mailingAddressField;
            }
            set
            {
                this.mailingAddressField = value;
            }
        }

        public AddressView physicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        public AnnualPremiumGoalView[] currentGoals
        {
            get
            {
                return this.currentGoalsField;
            }
            set
            {
                this.currentGoalsField = value;
            }
        }

        public String agencySpeedDial
        {
            get
            {
                return this.agencySpeedDialField;
            }
            set
            {
                this.agencySpeedDialField = value;
            }
        }

        public String agencyTollFreeNumber
        {
            get
            {
                return this.agencyTollFreeNumberField;
            }
            set
            {
                this.agencyTollFreeNumberField = value;
            }
        }

        public String agencyStatus
        {
            get
            {
                return this.agencyStatusField;
            }
            set
            {
                this.agencyStatusField = value;
            }
        }
    }
}
