using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class ProducerWritingCompany
    {
        private long idField;
        private String appointmentEffectiveDateField;
        private String appointmentExpirationDateField;
        private ProducerLicense producerLicenseField;
        private WritingCompany writingCompanyField;

        public String appointmentEffectiveDate
        {
            get
            {
                return this.appointmentEffectiveDateField;
            }
            set
            {
                this.appointmentEffectiveDateField = value;
            }
        }

        public String appointmentExpirationDate
        {
            get
            {
                return this.appointmentExpirationDateField;
            }
            set
            {
                this.appointmentExpirationDateField = value;
            }
        }

        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        public ProducerLicense producerLicense
        {
            get
            {
                return this.producerLicenseField;
            }
            set
            {
                this.producerLicenseField = value;
            }
        }

        public WritingCompany writingCompany
        {
            get
            {
                return this.writingCompanyField;
            }
            set
            {
                this.writingCompanyField = value;
            }
        }
    }
}
