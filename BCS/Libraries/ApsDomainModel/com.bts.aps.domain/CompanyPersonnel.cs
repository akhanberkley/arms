using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class CompanyPersonnel
    {

        private CompanyPersonnelRole[] companyPersonnelRolesField;

        private Status statusField;

        private string firstNameField;

        private long idField;

        private string lastNameField;

        private String retirementDateField;

        private bool retirementDateFieldSpecified;

        private string userGuidField;

        private string userIdField;

        private string userInitialsField;
        

        /// <remarks/>
        public CompanyPersonnelRole[] companyPersonnelRoles
        {
            get
            {
                return this.companyPersonnelRolesField;
            }
            set
            {
                this.companyPersonnelRolesField = value;
            }
        }

        // <remarks/>
        public Status status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public String retirementDate
        {
            get
            {
                return this.retirementDateField;
            }
            set
            {
                this.retirementDateField = value;
            }
        }

        /// <remarks/>
        public bool retirementDateSpecified
        {
            get
            {
                return this.retirementDateFieldSpecified;
            }
            set
            {
                this.retirementDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string userGuid
        {
            get
            {
                return this.userGuidField;
            }
            set
            {
                this.userGuidField = value;
            }
        }

        /// <remarks/>
        public string userId
        {
            get
            {
                return this.userIdField;
            }
            set
            {
                this.userIdField = value;
            }
        }

        /// <remarks/>
        public string userInitials
        {
            get
            {
                return this.userInitialsField;
            }
            set
            {
                this.userInitialsField = value;
            }
        }
    }
}
