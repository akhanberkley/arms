using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class CustomEntity
    {

        private CustomAttribute[] customAttributesField;

        private string entityLabelField;

        private string entityNameField;

        private long idField;

        private string parentDomainObjectField;

        private long sortOrderField;

        /// <remarks/>
        public CustomAttribute[] customAttributes
        {
            get
            {
                return this.customAttributesField;
            }
            set
            {
                this.customAttributesField = value;
            }
        }

        /// <remarks/>
        public string entityLabel
        {
            get
            {
                return this.entityLabelField;
            }
            set
            {
                this.entityLabelField = value;
            }
        }

        /// <remarks/>
        public string entityName
        {
            get
            {
                return this.entityNameField;
            }
            set
            {
                this.entityNameField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string parentDomainObject
        {
            get
            {
                return this.parentDomainObjectField;
            }
            set
            {
                this.parentDomainObjectField = value;
            }
        }

        /// <remarks/>
        public long sortOrder
        {
            get
            {
                return this.sortOrderField;
            }
            set
            {
                this.sortOrderField = value;
            }
        }
    }
}
