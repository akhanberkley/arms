using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AssociationView
    {

        private Agency[] agenciesField;

        private AssociationType associationTypeField;

        /// <remarks/>
        public Agency[] agencies
        {
            get
            {
                return this.agenciesField;
            }
            set
            {
                this.agenciesField = value;
            }
        }

        /// <remarks/>
        public AssociationType associationType
        {
            get
            {
                return this.associationTypeField;
            }
            set
            {
                this.associationTypeField = value;
            }
        }
    }
}
