using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class Automation
    {

        private Agency agencyField;

        private AutomationCustomValue[] automationCustomValuesField;

        private long idField;

        private string operatingSystemField;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public AutomationCustomValue[] automationCustomValues
        {
            get
            {
                return this.automationCustomValuesField;
            }
            set
            {
                this.automationCustomValuesField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string operatingSystem
        {
            get
            {
                return this.operatingSystemField;
            }
            set
            {
                this.operatingSystemField = value;
            }
        }
    }
}
