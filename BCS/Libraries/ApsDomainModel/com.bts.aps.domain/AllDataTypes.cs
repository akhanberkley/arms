using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AllDataTypes
    {

        private Affiliation[] affiliationTypesField;

        private AgencyCancelReason[] agencyCancelReasonsField;

        private AgencyCategory[] agencyCategoryField;

        private AgencyGroupType[] agencyGroupTypeField;

        private AgencyPersonnelRoleType[] agencyPersonnelRoleTypeField;

        private AgencyRank[] agencyRankField;

        private AgencyStatementType[] agencyStatementTypeField;

        private AgencyTerritory[] agencyTerritoryField;

        private AgencyType[] agencyTypeField;

        private AssociationType[] associationTypeField;

        private BillType[] billTypeField;

        private Branch[] branchesField;

        private CompanyPersonnelRoleType[] companyPersonnelRoleTypeField;

        private Designation[] designationTypesField;

        private Gender[] genderTypesField;

        private LocationCommunicationType[] locationCommunicationTypeField;

        private NamePrefix[] namePrefixTypesField;

        private NameSuffix[] nameSuffixTypesField;

        private OtherCompany[] otherCompaniesField;

        private PersonnelCommunicationType[] personnelCommunicationTypeField;

        private PersonnelNoteCatType[] personnelNoteCategoryTypeField;

        private ProducerRank[] producerRankField;

        private Program[] programTypesField;

        private Company[] servicingCompaniesField;

        private USState[] USStateField;

        /// <remarks/>
        public Affiliation[] affiliationTypes
        {
            get
            {
                return this.affiliationTypesField;
            }
            set
            {
                this.affiliationTypesField = value;
            }
        }

        /// <remarks/>
        public AgencyCancelReason[] agencyCancelReasons
        {
            get
            {
                return this.agencyCancelReasonsField;
            }
            set
            {
                this.agencyCancelReasonsField = value;
            }
        }

        /// <remarks/>
        public AgencyCategory[] agencyCategory
        {
            get
            {
                return this.agencyCategoryField;
            }
            set
            {
                this.agencyCategoryField = value;
            }
        }

        /// <remarks/>
        public AgencyGroupType[] agencyGroupType
        {
            get
            {
                return this.agencyGroupTypeField;
            }
            set
            {
                this.agencyGroupTypeField = value;
            }
        }

        /// <remarks/>
        public AgencyPersonnelRoleType[] agencyPersonnelRoleType
        {
            get
            {
                return this.agencyPersonnelRoleTypeField;
            }
            set
            {
                this.agencyPersonnelRoleTypeField = value;
            }
        }

        /// <remarks/>
        public AgencyRank[] agencyRank
        {
            get
            {
                return this.agencyRankField;
            }
            set
            {
                this.agencyRankField = value;
            }
        }

        /// <remarks/>
        public AgencyStatementType[] agencyStatementType
        {
            get
            {
                return this.agencyStatementTypeField;
            }
            set
            {
                this.agencyStatementTypeField = value;
            }
        }

        /// <remarks/>
        public AgencyTerritory[] agencyTerritory
        {
            get
            {
                return this.agencyTerritoryField;
            }
            set
            {
                this.agencyTerritoryField = value;
            }
        }

        /// <remarks/>
        public AgencyType[] agencyType
        {
            get
            {
                return this.agencyTypeField;
            }
            set
            {
                this.agencyTypeField = value;
            }
        }

        /// <remarks/>
        public AssociationType[] associationType
        {
            get
            {
                return this.associationTypeField;
            }
            set
            {
                this.associationTypeField = value;
            }
        }

        /// <remarks/>
        public BillType[] billType
        {
            get
            {
                return this.billTypeField;
            }
            set
            {
                this.billTypeField = value;
            }
        }

        /// <remarks/>
        public Branch[] branches
        {
            get
            {
                return this.branchesField;
            }
            set
            {
                this.branchesField = value;
            }
        }

        /// <remarks/>
        public CompanyPersonnelRoleType[] companyPersonnelRoleType
        {
            get
            {
                return this.companyPersonnelRoleTypeField;
            }
            set
            {
                this.companyPersonnelRoleTypeField = value;
            }
        }

        /// <remarks/>
        public Designation[] designationTypes
        {
            get
            {
                return this.designationTypesField;
            }
            set
            {
                this.designationTypesField = value;
            }
        }

        /// <remarks/>
        public Gender[] genderTypes
        {
            get
            {
                return this.genderTypesField;
            }
            set
            {
                this.genderTypesField = value;
            }
        }

        /// <remarks/>
        public LocationCommunicationType[] locationCommunicationType
        {
            get
            {
                return this.locationCommunicationTypeField;
            }
            set
            {
                this.locationCommunicationTypeField = value;
            }
        }

        /// <remarks/>
        public NamePrefix[] namePrefixTypes
        {
            get
            {
                return this.namePrefixTypesField;
            }
            set
            {
                this.namePrefixTypesField = value;
            }
        }

        /// <remarks/>
        public NameSuffix[] nameSuffixTypes
        {
            get
            {
                return this.nameSuffixTypesField;
            }
            set
            {
                this.nameSuffixTypesField = value;
            }
        }

        /// <remarks/>
        public OtherCompany[] otherCompanies
        {
            get
            {
                return this.otherCompaniesField;
            }
            set
            {
                this.otherCompaniesField = value;
            }
        }

        /// <remarks/>
        public PersonnelCommunicationType[] personnelCommunicationType
        {
            get
            {
                return this.personnelCommunicationTypeField;
            }
            set
            {
                this.personnelCommunicationTypeField = value;
            }
        }

        /// <remarks/>
        public PersonnelNoteCatType[] personnelNoteCategoryType
        {
            get
            {
                return this.personnelNoteCategoryTypeField;
            }
            set
            {
                this.personnelNoteCategoryTypeField = value;
            }
        }

        /// <remarks/>
        public ProducerRank[] producerRank
        {
            get
            {
                return this.producerRankField;
            }
            set
            {
                this.producerRankField = value;
            }
        }

        /// <remarks/>
        public Program[] programTypes
        {
            get
            {
                return this.programTypesField;
            }
            set
            {
                this.programTypesField = value;
            }
        }

        /// <remarks/>
        public Company[] servicingCompanies
        {
            get
            {
                return this.servicingCompaniesField;
            }
            set
            {
                this.servicingCompaniesField = value;
            }
        }

        /// <remarks/>
        public USState[] USState
        {
            get
            {
                return this.USStateField;
            }
            set
            {
                this.USStateField = value;
            }
        }
    }
}
