using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyName
    {

        private Agency agencyField;

        private String businessEffectiveDateField;

        private bool businessEffectiveDateFieldSpecified;

        private string dbaNameField;

        private long idField;

        private string legalNameField;

        private string shortNameField;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public String businessEffectiveDate
        {
            get
            {
                return this.businessEffectiveDateField;
            }
            set
            {
                this.businessEffectiveDateField = value;
            }
        }

        /// <remarks/>
        public bool businessEffectiveDateSpecified
        {
            get
            {
                return this.businessEffectiveDateFieldSpecified;
            }
            set
            {
                this.businessEffectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string dbaName
        {
            get
            {
                return this.dbaNameField;
            }
            set
            {
                this.dbaNameField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string legalName
        {
            get
            {
                return this.legalNameField;
            }
            set
            {
                this.legalNameField = value;
            }
        }

        /// <remarks/>
        public string shortName
        {
            get
            {
                return this.shortNameField;
            }
            set
            {
                this.shortNameField = value;
            }
        }
    }
}
