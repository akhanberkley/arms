using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyPersonnel
    {

        private Agency agencyField;

        private AgencyPersonnelRole[] agencyPersonnelRolesField;

        private String effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private String expirationDateField;

        private bool expirationDateFieldSpecified;

        private long idField;

        private bool isPrimaryAgencyField;

        private LocationAgencyPersonnel[] locationAgencyPersonsField;

        private Personnel personnelField;

        private long yearStartedField;

        private string agentNumberField;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public AgencyPersonnelRole[] agencyPersonnelRoles
        {
            get
            {
                return this.agencyPersonnelRolesField;
            }
            set
            {
                this.agencyPersonnelRolesField = value;
            }
        }

        /// <remarks/>
        public String effectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        public bool effectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        public bool expirationDateSpecified
        {
            get
            {
                return this.expirationDateFieldSpecified;
            }
            set
            {
                this.expirationDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public bool isPrimaryAgency
        {
            get
            {
                return this.isPrimaryAgencyField;
            }
            set
            {
                this.isPrimaryAgencyField = value;
            }
        }

        /// <remarks/>
        public LocationAgencyPersonnel[] locationAgencyPersons
        {
            get
            {
                return this.locationAgencyPersonsField;
            }
            set
            {
                this.locationAgencyPersonsField = value;
            }
        }

        /// <remarks/>
        public Personnel personnel
        {
            get
            {
                return this.personnelField;
            }
            set
            {
                this.personnelField = value;
            }
        }

        /// <remarks/>
        public long yearStarted
        {
            get
            {
                return this.yearStartedField;
            }
            set
            {
                this.yearStartedField = value;
            }
        }

        public string agentNumber
        {
            get { return this.agentNumberField; }
            set
            { this.agentNumberField = value; }
        }
    }
}
