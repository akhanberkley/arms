using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyCustomValue
    {

        private Agency agencyField;

        private bool booleanValueField;

        private CustomAttribute customAttributeField;

        private CustomAttributeChoice customAttributeChoiceField;

        private long idField;

        private string valueField;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public bool booleanValue
        {
            get
            {
                return this.booleanValueField;
            }
            set
            {
                this.booleanValueField = value;
            }
        }

        /// <remarks/>
        public CustomAttribute customAttribute
        {
            get
            {
                return this.customAttributeField;
            }
            set
            {
                this.customAttributeField = value;
            }
        }

        /// <remarks/>
        public CustomAttributeChoice customAttributeChoice
        {
            get
            {
                return this.customAttributeChoiceField;
            }
            set
            {
                this.customAttributeChoiceField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }
}
