using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class Personnel
    {

        private bool addUserField;

        private AgencyPersonnel[] agencyPersonsField;

        private string alternateNameField;

        private string challengeAnswerField;

        private string challengeQuestionField;

        private bool companyPersonField;

        private String dateOfBirthField;

        private bool dateOfBirthFieldSpecified;

        private String dateTermsAcceptedField;

        private bool dateTermsAcceptedFieldSpecified;

        private string firstNameField;

        private Gender genderField;

        private long idField;

        private bool isNewsletterField;

        private string lastNameField;

        private string middleNameField;

        private NamePrefix namePrefixField;

        private NameSuffix nameSuffixField;

        private string passwordField;

        private PersonnelAddress[] personnelAddressesField;

        private PersonnelCommunication[] personnelCommunicationsField;

        private PersonnelCustomValue[] personnelCustomValuesField;

        private PersonnelDesignation[] personnelDesignationsField;

        private PersonnelNote[] personnelNotesField;

        private string producerCodeField;

        private ProducerLicense[] producerLicensesField;

        private ProducerRank producerRankField;

        private bool pwdChangeRequiredField;

        private string significantOtherField;

        private string socialSecurityNumberField;

        private string userIdField;

        /// <remarks/>
        public bool addUser
        {
            get
            {
                return this.addUserField;
            }
            set
            {
                this.addUserField = value;
            }
        }

        /// <remarks/>
        public AgencyPersonnel[] agencyPersons
        {
            get
            {
                return this.agencyPersonsField;
            }
            set
            {
                this.agencyPersonsField = value;
            }
        }

        /// <remarks/>
        public string alternateName
        {
            get
            {
                return this.alternateNameField;
            }
            set
            {
                this.alternateNameField = value;
            }
        }

        /// <remarks/>
        public string challengeAnswer
        {
            get
            {
                return this.challengeAnswerField;
            }
            set
            {
                this.challengeAnswerField = value;
            }
        }

        /// <remarks/>
        public string challengeQuestion
        {
            get
            {
                return this.challengeQuestionField;
            }
            set
            {
                this.challengeQuestionField = value;
            }
        }

        /// <remarks/>
        public bool companyPerson
        {
            get
            {
                return this.companyPersonField;
            }
            set
            {
                this.companyPersonField = value;
            }
        }

        /// <remarks/>
        public String dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public bool dateOfBirthSpecified
        {
            get
            {
                return this.dateOfBirthFieldSpecified;
            }
            set
            {
                this.dateOfBirthFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String dateTermsAccepted
        {
            get
            {
                return this.dateTermsAcceptedField;
            }
            set
            {
                this.dateTermsAcceptedField = value;
            }
        }

        /// <remarks/>
        public bool dateTermsAcceptedSpecified
        {
            get
            {
                return this.dateTermsAcceptedFieldSpecified;
            }
            set
            {
                this.dateTermsAcceptedFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public Gender gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public bool isNewsletter
        {
            get
            {
                return this.isNewsletterField;
            }
            set
            {
                this.isNewsletterField = value;
            }
        }

        /// <remarks/>
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string middleName
        {
            get
            {
                return this.middleNameField;
            }
            set
            {
                this.middleNameField = value;
            }
        }

        /// <remarks/>
        public NamePrefix namePrefix
        {
            get
            {
                return this.namePrefixField;
            }
            set
            {
                this.namePrefixField = value;
            }
        }

        /// <remarks/>
        public NameSuffix nameSuffix
        {
            get
            {
                return this.nameSuffixField;
            }
            set
            {
                this.nameSuffixField = value;
            }
        }

        /// <remarks/>
        public string password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public PersonnelAddress[] personnelAddresses
        {
            get
            {
                return this.personnelAddressesField;
            }
            set
            {
                this.personnelAddressesField = value;
            }
        }

        /// <remarks/>
        public PersonnelCommunication[] personnelCommunications
        {
            get
            {
                return this.personnelCommunicationsField;
            }
            set
            {
                this.personnelCommunicationsField = value;
            }
        }

        /// <remarks/>
        public PersonnelCustomValue[] personnelCustomValues
        {
            get
            {
                return this.personnelCustomValuesField;
            }
            set
            {
                this.personnelCustomValuesField = value;
            }
        }

        /// <remarks/>
        public PersonnelDesignation[] personnelDesignations
        {
            get
            {
                return this.personnelDesignationsField;
            }
            set
            {
                this.personnelDesignationsField = value;
            }
        }

        /// <remarks/>
        public PersonnelNote[] personnelNotes
        {
            get
            {
                return this.personnelNotesField;
            }
            set
            {
                this.personnelNotesField = value;
            }
        }

        /// <remarks/>
        public string producerCode
        {
            get
            {
                return this.producerCodeField;
            }
            set
            {
                this.producerCodeField = value;
            }
        }

        /// <remarks/>
        public ProducerLicense[] producerLicenses
        {
            get
            {
                return this.producerLicensesField;
            }
            set
            {
                this.producerLicensesField = value;
            }
        }

        /// <remarks/>
        public ProducerRank producerRank
        {
            get
            {
                return this.producerRankField;
            }
            set
            {
                this.producerRankField = value;
            }
        }

        /// <remarks/>
        public bool pwdChangeRequired
        {
            get
            {
                return this.pwdChangeRequiredField;
            }
            set
            {
                this.pwdChangeRequiredField = value;
            }
        }

        /// <remarks/>
        public string significantOther
        {
            get
            {
                return this.significantOtherField;
            }
            set
            {
                this.significantOtherField = value;
            }
        }

        /// <remarks/>
        public string socialSecurityNumber
        {
            get
            {
                return this.socialSecurityNumberField;
            }
            set
            {
                this.socialSecurityNumberField = value;
            }
        }

        /// <remarks/>
        public string userId
        {
            get
            {
                return this.userIdField;
            }
            set
            {
                this.userIdField = value;
            }
        }
    }
}
