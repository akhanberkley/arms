using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class Competitor
    {

        private Agency agencyField;

        private long clwpField;

        private OtherCompany companyField;

        private long idField;

        private long lrPercentField;

        private String validDateField;

        private bool validDateFieldSpecified;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public long clwp
        {
            get
            {
                return this.clwpField;
            }
            set
            {
                this.clwpField = value;
            }
        }

        /// <remarks/>
        public OtherCompany company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public long lrPercent
        {
            get
            {
                return this.lrPercentField;
            }
            set
            {
                this.lrPercentField = value;
            }
        }

        /// <remarks/>
        public String validDate
        {
            get
            {
                return this.validDateField;
            }
            set
            {
                this.validDateField = value;
            }
        }

        /// <remarks/>
        public bool validDateSpecified
        {
            get
            {
                return this.validDateFieldSpecified;
            }
            set
            {
                this.validDateFieldSpecified = value;
            }
        }
    }
}
