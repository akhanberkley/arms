using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class PersonnelView
    {
        private String firstNameField;
        private String middleNameField;
        private String lastNameField;
        private String directPhoneField;
        private String homePhoneField;
        private String cellPhoneField;
        private String emailField;
        private String significantOtherField;
        private String birthdateField;
        private String[] rolesField;
        private AddressView primaryLocationAddressField;
        private String primaryLocationPhoneField;
        private String primaryLocationSpeedDialField;
        private String primaryLocationTollFreeNumberField;

        public string birthdate
        {
            get
            {
                return this.birthdateField;
            }
            set
            {
                this.birthdateField = value;
            }
        }

        public string cellPhone
        {
            get
            {
                return this.cellPhoneField;
            }
            set
            {
                this.cellPhoneField = value;
            }
        }

        public string directPhone
        {
            get
            {
                return this.directPhoneField;
            }
            set
            {
                this.directPhoneField = value;
            }
        }

        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        public string homePhone
        {
            get
            {
                return this.homePhoneField;
            }
            set
            {
                this.homePhoneField = value;
            }
        }

        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        public string middleName
        {
            get
            {
                return this.middleNameField;
            }
            set
            {
                this.middleNameField = value;
            }
        }

        public String[] roles
        {
            get
            {
                return this.rolesField;
            }
            set
            {
                this.rolesField = value;
            }
        }

        public string significantOther
        {
            get
            {
                return this.significantOtherField;
            }
            set
            {
                this.significantOtherField = value;
            }
        }

        public AddressView primaryLocationAddress
        {
            get
            {
                return this.primaryLocationAddressField;
            }
            set
            {
                this.primaryLocationAddressField = value;
            }
        }

        public String primaryLocationPhone
        {
            get
            {
                return this.primaryLocationPhoneField;
            }
            set
            {
                this.primaryLocationPhoneField = value;
            }
        }

        public String primaryLocationSpeedDial
        {
            get
            {
                return this.primaryLocationSpeedDialField;
            }
            set
            {
                this.primaryLocationSpeedDialField = value;
            }
        }

        public String primaryLocationTollFreeNumber
        {
            get
            {
                return this.primaryLocationTollFreeNumberField;
            }
            set
            {
                this.primaryLocationTollFreeNumberField = value;
            }
        }
    }
}
