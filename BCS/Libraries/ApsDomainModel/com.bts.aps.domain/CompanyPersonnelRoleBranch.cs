using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class CompanyPersonnelRoleBranch
    {

        private Branch branchField;

        private CompanyPersonnelRole companyPersonnelRoleField;

        private String effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private String expirationDateField;

        private bool expirationDateFieldSpecified;

        private long idField;

        /// <remarks/>
        public Branch branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        public CompanyPersonnelRole companyPersonnelRole
        {
            get
            {
                return this.companyPersonnelRoleField;
            }
            set
            {
                this.companyPersonnelRoleField = value;
            }
        }

        /// <remarks/>
        public String effectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        public bool effectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        public bool expirationDateSpecified
        {
            get
            {
                return this.expirationDateFieldSpecified;
            }
            set
            {
                this.expirationDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }
}
