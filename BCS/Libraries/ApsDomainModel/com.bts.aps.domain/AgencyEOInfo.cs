using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyEOInfo
    {

        private Agency agencyField;

        private String eoExpirationDateField;

        private bool eoExpirationDateFieldSpecified;

        private string eocarrierField;

        private string eocommentsField;

        private String eofollowupDateField;

        private bool eofollowupDateFieldSpecified;

        private String eoinceptionDateField;

        private bool eoinceptionDateFieldSpecified;

        private long eolimitAggregateField;

        private long eolimitPerClaimField;

        private string eopolicyNumberField;

        private String eoretroactiveDateField;

        private bool eoretroactiveDateFieldSpecified;

        private long idField;

        private bool isNonCompliantField;

        private string marketingRepNoteField;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public String eoExpirationDate
        {
            get
            {
                return this.eoExpirationDateField;
            }
            set
            {
                this.eoExpirationDateField = value;
            }
        }

        /// <remarks/>
        public bool eoExpirationDateSpecified
        {
            get
            {
                return this.eoExpirationDateFieldSpecified;
            }
            set
            {
                this.eoExpirationDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string eocarrier
        {
            get
            {
                return this.eocarrierField;
            }
            set
            {
                this.eocarrierField = value;
            }
        }

        /// <remarks/>
        public string eocomments
        {
            get
            {
                return this.eocommentsField;
            }
            set
            {
                this.eocommentsField = value;
            }
        }

        /// <remarks/>
        public String eofollowupDate
        {
            get
            {
                return this.eofollowupDateField;
            }
            set
            {
                this.eofollowupDateField = value;
            }
        }

        /// <remarks/>
        public bool eofollowupDateSpecified
        {
            get
            {
                return this.eofollowupDateFieldSpecified;
            }
            set
            {
                this.eofollowupDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String eoinceptionDate
        {
            get
            {
                return this.eoinceptionDateField;
            }
            set
            {
                this.eoinceptionDateField = value;
            }
        }

        /// <remarks/>
        public bool eoinceptionDateSpecified
        {
            get
            {
                return this.eoinceptionDateFieldSpecified;
            }
            set
            {
                this.eoinceptionDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long eolimitAggregate
        {
            get
            {
                return this.eolimitAggregateField;
            }
            set
            {
                this.eolimitAggregateField = value;
            }
        }

        /// <remarks/>
        public long eolimitPerClaim
        {
            get
            {
                return this.eolimitPerClaimField;
            }
            set
            {
                this.eolimitPerClaimField = value;
            }
        }

        /// <remarks/>
        public string eopolicyNumber
        {
            get
            {
                return this.eopolicyNumberField;
            }
            set
            {
                this.eopolicyNumberField = value;
            }
        }

        /// <remarks/>
        public String eoretroactiveDate
        {
            get
            {
                return this.eoretroactiveDateField;
            }
            set
            {
                this.eoretroactiveDateField = value;
            }
        }

        /// <remarks/>
        public bool eoretroactiveDateSpecified
        {
            get
            {
                return this.eoretroactiveDateFieldSpecified;
            }
            set
            {
                this.eoretroactiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public bool isNonCompliant
        {
            get
            {
                return this.isNonCompliantField;
            }
            set
            {
                this.isNonCompliantField = value;
            }
        }

        /// <remarks/>
        public string marketingRepNote
        {
            get
            {
                return this.marketingRepNoteField;
            }
            set
            {
                this.marketingRepNoteField = value;
            }
        }
    }
}
