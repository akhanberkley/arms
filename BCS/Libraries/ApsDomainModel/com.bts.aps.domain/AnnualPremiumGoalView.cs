using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AnnualPremiumGoalView
    {
        private long yearField;
        private String goalTypeField;
        private long valueField;

        public string goalType
        {
            get
            {
                return this.goalTypeField;
            }
            set
            {
                this.goalTypeField = value;
            }
        }

        public long value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }

        public long year
        {
            get
            {
                return this.yearField;
            }
            set
            {
                this.yearField = value;
            }
        }
    }
}
