using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class UnderwritingUnit
    {

        private AgencyUnderwritingUnit[] agencyUnderwritingUnitsField;

        private string codeField;

        private Company companyField;

        private string descriptionField;

        private long idField;

        /// <remarks/>
        public AgencyUnderwritingUnit[] agencyUnderwritingUnits
        {
            get
            {
                return this.agencyUnderwritingUnitsField;
            }
            set
            {
                this.agencyUnderwritingUnitsField = value;
            }
        }

        /// <remarks/>
        public string code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public Company company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }
}
