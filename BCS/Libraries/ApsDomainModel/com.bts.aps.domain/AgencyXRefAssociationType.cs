using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyXRefAssociationType
    {

        private AgencyXRef agencyXRefField;

        private AssociationType associationTypeField;

        private long idField;

        /// <remarks/>
        public AgencyXRef agencyXRef
        {
            get
            {
                return this.agencyXRefField;
            }
            set
            {
                this.agencyXRefField = value;
            }
        }

        /// <remarks/>
        public AssociationType associationType
        {
            get
            {
                return this.associationTypeField;
            }
            set
            {
                this.associationTypeField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }
}
