using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class CompanyPersonnelView
    {

        private string companyPersonnelRoleTypeField;

        private string firstNameField;

        private string lastNameField;

        private string underwritingUnitCodeField;

        private string underwritingUnitDescriptionField;

        private string userIdField;

        /// <remarks/>
        public string companyPersonnelRoleType
        {
            get
            {
                return this.companyPersonnelRoleTypeField;
            }
            set
            {
                this.companyPersonnelRoleTypeField = value;
            }
        }

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string underwritingUnitCode
        {
            get
            {
                return this.underwritingUnitCodeField;
            }
            set
            {
                this.underwritingUnitCodeField = value;
            }
        }

        /// <remarks/>
        public string underwritingUnitDescription
        {
            get
            {
                return this.underwritingUnitDescriptionField;
            }
            set
            {
                this.underwritingUnitDescriptionField = value;
            }
        }

        /// <remarks/>
        public string userId
        {
            get
            {
                return this.userIdField;
            }
            set
            {
                this.userIdField = value;
            }
        }
    }
}
