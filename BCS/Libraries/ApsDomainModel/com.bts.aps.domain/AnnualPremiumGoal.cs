using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AnnualPremiumGoal
    {
        private Agency agencyField;

        private AnnualPremiumGoalType annualPremiumGoalTypeField;

        private long idField;

        private long valueField;

        private long yearField;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public AnnualPremiumGoalType annualPremiumGoalType
        {
            get
            {
                return this.annualPremiumGoalTypeField;
            }
            set
            {
                this.annualPremiumGoalTypeField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public long value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }

        /// <remarks/>
        public long year
        {
            get
            {
                return this.yearField;
            }
            set
            {
                this.yearField = value;
            }
        }
    }
}
