using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class Company
    {

        private long idField;

        private string nameField;

        private bool servicingCompanyIndField;

        private UnderwritingUnit[] underwritingUnitsField;

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public bool servicingCompanyInd
        {
            get
            {
                return this.servicingCompanyIndField;
            }
            set
            {
                this.servicingCompanyIndField = value;
            }
        }

        /// <remarks/>
        public UnderwritingUnit[] underwritingUnits
        {
            get
            {
                return this.underwritingUnitsField;
            }
            set
            {
                this.underwritingUnitsField = value;
            }
        }
    }
}
