using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class ProducerLicense
    {

        private string commentsField;

        private bool counterSignatureField;

        private long idField;

        private String licenseEffectiveDateField;

        private bool licenseEffectiveDateFieldSpecified;

        private String licenseExpirationDateField;

        private bool licenseExpirationDateFieldSpecified;

        private string licenseNumberField;

        private LicenseResidencyType licenseResidencyTypeField;

        private USState licenseStateField;

        private ProducerLicenseType licenseTypeField;

        private Personnel personnelField;

        /// <remarks/>
        public string comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        /// <remarks/>
        public bool counterSignature
        {
            get
            {
                return this.counterSignatureField;
            }
            set
            {
                this.counterSignatureField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public String licenseEffectiveDate
        {
            get
            {
                return this.licenseEffectiveDateField;
            }
            set
            {
                this.licenseEffectiveDateField = value;
            }
        }

        /// <remarks/>
        public bool licenseEffectiveDateSpecified
        {
            get
            {
                return this.licenseEffectiveDateFieldSpecified;
            }
            set
            {
                this.licenseEffectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String licenseExpirationDate
        {
            get
            {
                return this.licenseExpirationDateField;
            }
            set
            {
                this.licenseExpirationDateField = value;
            }
        }

        /// <remarks/>
        public bool licenseExpirationDateSpecified
        {
            get
            {
                return this.licenseExpirationDateFieldSpecified;
            }
            set
            {
                this.licenseExpirationDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string licenseNumber
        {
            get
            {
                return this.licenseNumberField;
            }
            set
            {
                this.licenseNumberField = value;
            }
        }

        /// <remarks/>
        public LicenseResidencyType licenseResidencyType
        {
            get
            {
                return this.licenseResidencyTypeField;
            }
            set
            {
                this.licenseResidencyTypeField = value;
            }
        }

        /// <remarks/>
        public USState licenseState
        {
            get
            {
                return this.licenseStateField;
            }
            set
            {
                this.licenseStateField = value;
            }
        }

        /// <remarks/>
        public ProducerLicenseType licenseType
        {
            get
            {
                return this.licenseTypeField;
            }
            set
            {
                this.licenseTypeField = value;
            }
        }

        /// <remarks/>
        public Personnel personnel
        {
            get
            {
                return this.personnelField;
            }
            set
            {
                this.personnelField = value;
            }
        }
    }
}
