using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class LocationAddress
    {

        private string cityField;

        private long idField;

        private bool isBillingField;

        private bool isIgnoreAddressValidationField;

        private bool isMailingField;

        private bool isOtherField;

        private string line1Field;

        private string line2Field;

        private string line3Field;

        private string line4Field;

        private Location locationField;

        private USState USStateField;

        private string zipField;

        /// <remarks/>
        public string city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public bool isBilling
        {
            get
            {
                return this.isBillingField;
            }
            set
            {
                this.isBillingField = value;
            }
        }

        /// <remarks/>
        public bool isIgnoreAddressValidation
        {
            get
            {
                return this.isIgnoreAddressValidationField;
            }
            set
            {
                this.isIgnoreAddressValidationField = value;
            }
        }

        /// <remarks/>
        public bool isMailing
        {
            get
            {
                return this.isMailingField;
            }
            set
            {
                this.isMailingField = value;
            }
        }

        /// <remarks/>
        public bool isOther
        {
            get
            {
                return this.isOtherField;
            }
            set
            {
                this.isOtherField = value;
            }
        }

        /// <remarks/>
        public string line1
        {
            get
            {
                return this.line1Field;
            }
            set
            {
                this.line1Field = value;
            }
        }

        /// <remarks/>
        public string line2
        {
            get
            {
                return this.line2Field;
            }
            set
            {
                this.line2Field = value;
            }
        }

        /// <remarks/>
        public string line3
        {
            get
            {
                return this.line3Field;
            }
            set
            {
                this.line3Field = value;
            }
        }

        /// <remarks/>
        public string line4
        {
            get
            {
                return this.line4Field;
            }
            set
            {
                this.line4Field = value;
            }
        }

        /// <remarks/>
        public Location location
        {
            get
            {
                return this.locationField;
            }
            set
            {
                this.locationField = value;
            }
        }

        /// <remarks/>
        public USState USState
        {
            get
            {
                return this.USStateField;
            }
            set
            {
                this.USStateField = value;
            }
        }

        /// <remarks/>
        public string zip
        {
            get
            {
                return this.zipField;
            }
            set
            {
                this.zipField = value;
            }
        }
    }
}
