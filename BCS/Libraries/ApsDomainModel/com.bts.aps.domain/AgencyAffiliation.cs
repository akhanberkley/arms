using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyAffiliation
    {

        private Affiliation affiliationField;

        private long idField;

        /// <remarks/>
        public Affiliation affiliation
        {
            get
            {
                return this.affiliationField;
            }
            set
            {
                this.affiliationField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }
}
