using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class LocationAgencyPersonnel
    {

        private AgencyPersonnel agencyPersonnelField;

        private long idField;

        private bool isPrimaryLocationField;

        private Location locationField;

        /// <remarks/>
        public AgencyPersonnel agencyPersonnel
        {
            get
            {
                return this.agencyPersonnelField;
            }
            set
            {
                this.agencyPersonnelField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public bool isPrimaryLocation
        {
            get
            {
                return this.isPrimaryLocationField;
            }
            set
            {
                this.isPrimaryLocationField = value;
            }
        }

        /// <remarks/>
        public Location location
        {
            get
            {
                return this.locationField;
            }
            set
            {
                this.locationField = value;
            }
        }
    }
}
