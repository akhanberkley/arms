using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyNote
    {

        private Agency agencyField;

        private AgencyNoteCategoryType agencyNoteCategoryTypeField;

        private string authorField;

        private String dateEnteredField;

        private bool dateEnteredFieldSpecified;

        private string detailField;

        private long idField;

        private String occurrenceDateField;

        private bool occurrenceDateFieldSpecified;

        private string summaryField;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public AgencyNoteCategoryType agencyNoteCategoryType
        {
            get
            {
                return this.agencyNoteCategoryTypeField;
            }
            set
            {
                this.agencyNoteCategoryTypeField = value;
            }
        }

        /// <remarks/>
        public string author
        {
            get
            {
                return this.authorField;
            }
            set
            {
                this.authorField = value;
            }
        }

        /// <remarks/>
        public String dateEntered
        {
            get
            {
                return this.dateEnteredField;
            }
            set
            {
                this.dateEnteredField = value;
            }
        }

        /// <remarks/>
        public bool dateEnteredSpecified
        {
            get
            {
                return this.dateEnteredFieldSpecified;
            }
            set
            {
                this.dateEnteredFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string detail
        {
            get
            {
                return this.detailField;
            }
            set
            {
                this.detailField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public String occurrenceDate
        {
            get
            {
                return this.occurrenceDateField;
            }
            set
            {
                this.occurrenceDateField = value;
            }
        }

        /// <remarks/>
        public bool occurrenceDateSpecified
        {
            get
            {
                return this.occurrenceDateFieldSpecified;
            }
            set
            {
                this.occurrenceDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string summary
        {
            get
            {
                return this.summaryField;
            }
            set
            {
                this.summaryField = value;
            }
        }
    }
}
