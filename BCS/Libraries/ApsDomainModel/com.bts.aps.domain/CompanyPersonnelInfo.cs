using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class CompanyPersonnelInfo
    {
        private long idField;
        private String userInitialsField;
        private String userGuidField;
        private String retirementDateField;
        private CompanyPersonnel companyPersonnelField;

        public CompanyPersonnel companyPersonnel
        {
            get
            {
                return this.companyPersonnelField;
            }
            set
            {
                this.companyPersonnelField = value;
            }
        }
        
        public String userGuid
        {
            get
            {
                return this.userGuidField;
            }
            set
            {
                this.userGuidField = value;
            }
        }

        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        public String userInitials
        {
            get
            {
                return this.userInitialsField;
            }
            set
            {
                this.userInitialsField = value;
            }
        }

        public String retirementDate
        {
            get
            {
                return this.retirementDateField;
            }
            set
            {
                this.retirementDateField = value;
            }
        }
    }
}
