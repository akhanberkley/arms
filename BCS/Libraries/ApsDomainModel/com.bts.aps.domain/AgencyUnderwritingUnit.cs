using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyUnderwritingUnit
    {

        private Agency agencyField;

        private AgencyUWPersonRole[] agencyUWPersonsField;

        private String effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private String expirationDateField;

        private bool expirationDateFieldSpecified;

        private long idField;

        private UnderwritingUnit underwritingUnitField;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public AgencyUWPersonRole[] agencyUWPersons
        {
            get
            {
                return this.agencyUWPersonsField;
            }
            set
            {
                this.agencyUWPersonsField = value;
            }
        }

        /// <remarks/>
        public String effectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        public bool effectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public String expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        public bool expirationDateSpecified
        {
            get
            {
                return this.expirationDateFieldSpecified;
            }
            set
            {
                this.expirationDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public UnderwritingUnit underwritingUnit
        {
            get
            {
                return this.underwritingUnitField;
            }
            set
            {
                this.underwritingUnitField = value;
            }
        }
    }
}
