using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyWritingCompany
    {
        private long idField;
        private String appointmentEffectiveDateField;
        private String appointmentExpirationDateField;
        private AgencyLicense agencyLicenseField;
        private WritingCompany writingCompanyField;

        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        public AgencyLicense agencyLicense
        {
            get
            {
                return this.agencyLicenseField;
            }
            set
            {
                this.agencyLicenseField = value;
            }
        }

        public String appointmentEffectiveDate
        {
            get
            {
                return this.appointmentEffectiveDateField;
            }
            set
            {
                this.appointmentEffectiveDateField = value;
            }
        }

        public String appointmentExpirationDate
        {
            get
            {
                return this.appointmentExpirationDateField;
            }
            set
            {
                this.appointmentExpirationDateField = value;
            }
        }

        public WritingCompany writingCompany
        {
            get
            {
                return this.writingCompanyField;
            }
            set
            {
                this.writingCompanyField = value;
            }
        }
    }
}
