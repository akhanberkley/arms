using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class TotalPremiumBusinessMix
    {

        private Agency agencyField;

        private long commercialLinesPercentField;

        private long idField;

        private long noOfAgencyEmployeesField;

        private long noOfAgencyLocationsField;

        private long personalLinesPercentField;

        private long premiumValueField;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public long commercialLinesPercent
        {
            get
            {
                return this.commercialLinesPercentField;
            }
            set
            {
                this.commercialLinesPercentField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public long noOfAgencyEmployees
        {
            get
            {
                return this.noOfAgencyEmployeesField;
            }
            set
            {
                this.noOfAgencyEmployeesField = value;
            }
        }

        /// <remarks/>
        public long noOfAgencyLocations
        {
            get
            {
                return this.noOfAgencyLocationsField;
            }
            set
            {
                this.noOfAgencyLocationsField = value;
            }
        }

        /// <remarks/>
        public long personalLinesPercent
        {
            get
            {
                return this.personalLinesPercentField;
            }
            set
            {
                this.personalLinesPercentField = value;
            }
        }

        /// <remarks/>
        public long premiumValue
        {
            get
            {
                return this.premiumValueField;
            }
            set
            {
                this.premiumValueField = value;
            }
        }
    }
}
