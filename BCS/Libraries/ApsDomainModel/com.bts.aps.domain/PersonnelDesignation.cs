using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class PersonnelDesignation
    {

        private Designation designationField;

        private long idField;

        /// <remarks/>
        public Designation designation
        {
            get
            {
                return this.designationField;
            }
            set
            {
                this.designationField = value;
            }
        }

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }
}
