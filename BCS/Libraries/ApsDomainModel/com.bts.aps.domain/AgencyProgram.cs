using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyProgram
    {

        private long idField;

        private Program programField;

        /// <remarks/>
        public long id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        public Program program
        {
            get
            {
                return this.programField;
            }
            set
            {
                this.programField = value;
            }
        }
    }
}
