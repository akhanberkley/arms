using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AddressView
    {
        private String line1Field;
        private String line2Field;
        private String cityField;
        private String stateField;
        private String zipField;

        public String city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        public String line1
        {
            get
            {
                return this.line1Field;
            }
            set
            {
                this.line1Field = value;
            }
        }

        public String line2
        {
            get
            {
                return this.line2Field;
            }
            set
            {
                this.line2Field = value;
            }
        }

        public String state
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        public String zip
        {
            get
            {
                return this.zipField;
            }
            set
            {
                this.zipField = value;
            }
        }
    }
}
