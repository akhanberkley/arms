using System;
using System.Collections.Generic;
using System.Text;

namespace com.bts.aps.domain
{
    public class AgencyAssociationsView
    {

        private Agency agencyField;

        private AssociationView[] associationViewField;

        /// <remarks/>
        public Agency agency
        {
            get
            {
                return this.agencyField;
            }
            set
            {
                this.agencyField = value;
            }
        }

        /// <remarks/>
        public AssociationView[] associationView
        {
            get
            {
                return this.associationViewField;
            }
            set
            {
                this.associationViewField = value;
            }
        }
    }
}
